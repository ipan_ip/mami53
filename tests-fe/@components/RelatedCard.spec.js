import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import mockAxios from '../utils/mock-axios';
import mockVLazy from '../utils/mock-v-lazy';
import 'tests-fe/utils/mock-vue';
import { swiper, swiperSlide } from 'vue-awesome-swiper';
import RelatedCard from 'Js/@components/RelatedCard';
import RelatedListKost from './__mocks__/related-list-kost.json';
import mockWindowProperty from '../utils/mock-window-property';
import detailStore from 'tests-fe/_detail/components/__mocks__/detailStore';
import { makeAPICall } from 'Js/@utils/makeAPICall.js';
jest.mock('Js/@utils/makeAPICall.js');

mockWindowProperty(
	'addEventListener',
	jest.fn((event, cb) => {
		cb();
	})
);
mockWindowProperty('axios', mockAxios);
mockWindowProperty('open', jest.fn());
mockWindowProperty('authCheck', {
	all: false,
	owner: false,
	tenant: false
});
const setSwiperOptionsEachSpy = jest.spyOn(
	RelatedCard.methods,
	'setSwiperOptionsEach'
);

const handleRelatedDataSpy = jest.spyOn(
	RelatedCard.methods,
	'handleRelatedData'
);

const updateSwiperSpy = jest.spyOn(RelatedCard.methods, 'updateSwiper');

const roomCardStub = {
	template: `<button @click="$emit('openRoom')" class="open-room" />`
};

describe('RelatedCard.vue', () => {
	const localVue = createLocalVue();
	const store = new Vuex.Store(detailStore);
	mockVLazy(localVue);
	const propsData = {
		params: {
			seq: 30387165
		}
	};
	const stubs = {
		swiper,
		swiperSlide,
		'room-card': roomCardStub
	};

	const generateWrapper = ({ mocksProp = {} }) => {
		const mountProp = {
			localVue,
			propsData,
			stubs,
			mocks: { ...mocksProp },
			store
		};
		return shallowMount(RelatedCard, mountProp);
	};

	it('should render relatedCard', () => {
		const wrapper = generateWrapper({
			mocksProp: { navigator: { isMobile: true } }
		});
		expect(wrapper.find('#relatedCard').exists()).toBe(true);
	});

	it('should set swiper options properly', async () => {
		makeAPICall.mockImplementation(() => true);
		const wrapper = generateWrapper({
			mocksProp: { navigator: { isMobile: true } }
		});

		await wrapper.setData({ relatedKost: RelatedListKost, isLoading: false });

		expect(handleRelatedDataSpy).toBeCalled();
		expect(updateSwiperSpy).toBeCalled();
		expect(setSwiperOptionsEachSpy).toBeCalledWith(1, 0);
	});

	it('should set related kost to empty array if suggestions null', () => {
		makeAPICall.mockImplementation(() => true);
		const wrapper = generateWrapper({
			mocksProp: { navigator: { isMobile: true } }
		});

		expect(wrapper.vm.relatedKost.length).toBe(0);
	});

	it('should assign slide per view count', () => {
		const wrapper = generateWrapper({
			mocksProp: { navigator: { isMobile: false } }
		});
		wrapper.vm.setSwiperOptionsEach(1, 0);
		expect(wrapper.vm.swiperOptionKost.slidesPerView).toBe(1);
		expect(wrapper.vm.swiperOptionKost.spaceBetween).toBe(0);
	});

	it('should set data kos', () => {
		const wrapper = generateWrapper({
			mocksProp: { navigator: { isMobile: true } }
		});
		const rooms = [
			{ title: 'SinggahSini', price_title_format: { discount: 50000 } }
		];
		wrapper.setData({
			relatedFilter: {
				gender: 'female',
				rent: 'monthly'
			}
		});
		wrapper.vm.setData(rooms);
		expect(wrapper.vm.relatedKost.length).toBe(1);
	});

	describe('Open Detail', () => {
		it('should call window open with slug as an argument', async () => {
			const wrapper = generateWrapper({
				mocksProp: { navigator: { isMobile: true } }
			});
			await wrapper.setData({
				relatedKost: [
					{
						index: 1,
						data: [
							{
								room: 1
							}
						]
					}
				],
				isLoading: false
			});
			const cardElement = wrapper.find('.open-room');

			expect(cardElement.exists()).toBe(true);

			await cardElement.trigger('click');

			expect(open).toBeCalled();
		});
	});
});
