import { createLocalVue, shallowMount } from '@vue/test-utils';
import ModalMamiLoading from 'Js/@components/ModalMamiLoading.vue';

describe('ModalMamiLoading.vue', () => {
	const localVue = createLocalVue();
	const propsData = {
		showModal: true
	};
	const wrapper = shallowMount(ModalMamiLoading, { localVue, propsData });

	it('should render modalMamiLoading', () => {
		expect(wrapper.find('#modalMamiLoading').exists()).toBe(true);
		expect(wrapper.find('#modalMamiLoading').attributes().value).toBe('true');
		wrapper.setProps({ showModal: false });
		wrapper.vm.$nextTick(() => {
			expect(wrapper.find('#modalMamiLoading').attributes().value).toBeFalsy();
		});
	});
});
