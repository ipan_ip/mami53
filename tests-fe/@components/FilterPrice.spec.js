import { mount } from '@vue/test-utils';

import FilterPrice from 'Js/@components/FilterPrice.vue';

const render = (options = {}) => {
	return mount(FilterPrice, {
		...options
	});
};

test("should emit 'filterPriceMin' event when users change the minimal price form value", async () => {
	const wrapper = render();

	const priceMinForm = wrapper.find('[data-testid="filterPriceMin"]');

	priceMinForm.element.value = 100;

	await priceMinForm.trigger('input');

	expect(wrapper.emitted().filterPriceMin[0]).toEqual([100]);
});

test("should emit 'filterPriceMax' event when users change the maximal price form value", async () => {
	const wrapper = render();

	const priceMinForm = wrapper.find('[data-testid="filterPriceMax"]');

	priceMinForm.element.value = 500000;

	await priceMinForm.trigger('input');

	expect(wrapper.emitted().filterPriceMax[0]).toEqual([500000]);
});

test("when click the submit button, it should emit 'filterPrice' event and also set a default value both for minimal & maximal price form if those form are empty", async () => {
	const div = document.createElement('div');
	div.id = 'root';
	document.body.appendChild(div);

	const wrapper = render({
		attachTo: document.getElementById('root')
	});

	const priceMinForm = wrapper.find('[data-testid="filterPriceMin"]');
	const priceMaxForm = wrapper.find('[data-testid="filterPriceMax"]');

	// Change the form values
	priceMinForm.element.value = 10000;
	await priceMinForm.trigger('input');
	priceMaxForm.element.value = 50000;
	await priceMaxForm.trigger('input');

	// simulate the submit button
	await wrapper.find('button').trigger('click');

	// Expect 'filterPrice' event emitted with the correct paramater
	expect(wrapper.emitted().filterPrice[0]).toEqual([
		{
			max: 50000,
			min: 10000
		}
	]);

	// Try to change the forms with empty value to test the default value handler
	priceMinForm.element.value = '';
	await priceMinForm.trigger('input');
	priceMaxForm.element.value = '';
	await priceMaxForm.trigger('input');

	// simulate the submit button again
	await wrapper.find('button').trigger('click');

	// Expect 'filterPrice' event emitted with the default value
	expect(wrapper.emitted().filterPrice[0]).toEqual([
		{
			max: 100000000,
			min: 10000
		}
	]);

	// Make sure the forms value should also be filled with the default value, so users won't confused
	expect(priceMinForm.element.value).toBe('Rp 10.000');
	expect(priceMaxForm.element.value).toBe('Rp 100.000.000');

	// make sure we destroy the wrapper instance
	wrapper.destroy();
});

test("all forms should be disabled when 'isDisabled' prop is 'true'", () => {
	const wrapper = render({
		propsData: {
			isDisabled: true
		}
	});

	// price minimal form should be disabled
	expect(wrapper.find('[data-testid="filterPriceMin"]').element.disabled).toBe(
		true
	);

	// price maximum form should be disabled
	expect(wrapper.find('[data-testid="filterPriceMax"]').element.disabled).toBe(
		true
	);

	// button should be disabled
	expect(wrapper.find('button').element.disabled).toBe(true);
});

test("label should be hidden when 'label' prop is false", async () => {
	const wrapper = render({
		propsData: {
			label: false
		}
	});

	// make sure users don't see the label
	expect(wrapper.find('label').exists()).toBe(false);
});

test("button should be hidden when 'displayButton' prop is false", () => {
	const wrapper = render({
		propsData: {
			displayButton: false
		}
	});

	// check via css class
	expect(wrapper.find('button').classes('display-button-false')).toBe(true);
});

describe('Initial value for minimal and maximal price form', () => {
	describe("Based on 'firstValue' prop", () => {
		it("should not fill the forms when 'noReset' prop is 'false'", () => {
			const wrapper = render({
				propsData: {
					noReset: false,
					firstValue: [1000, 5000]
				}
			});

			expect(
				wrapper.find('[data-testid="filterPriceMin"]').element.value
			).not.toBe(1000);

			expect(
				wrapper.find('[data-testid="filterPriceMax"]').element.value
			).not.toBe(5000);
		});

		it("should fill the forms when 'noReset' prop is 'true'", async () => {
			const wrapper = render({
				propsData: {
					noReset: true,
					firstValue: [1000, 5000]
				}
			});

			await wrapper.vm.$nextTick();

			expect(wrapper.find('[data-testid="filterPriceMin"]').element.value).toBe(
				'Rp 1.000'
			);

			expect(wrapper.find('[data-testid="filterPriceMax"]').element.value).toBe(
				'Rp 5.000'
			);
		});
	});

	describe("Based on 'propertyType' prop value, when it's 'kost'", () => {
		const wrapper = render({
			propsData: {
				propertyType: 'kost'
			}
		});

		it("should set maximal price form to Rp 5.000.000 when 'rentType' prop is 0", async () => {
			await wrapper.setProps({ rentType: 0 });

			expect(wrapper.find('[data-testid="filterPriceMax"]').element.value).toBe(
				'Rp 5.000.000'
			);
		});

		it("should set maximal price form to Rp 10.000.000 when 'rentType' prop is 1", async () => {
			await wrapper.setProps({ rentType: 1 });

			expect(wrapper.find('[data-testid="filterPriceMax"]').element.value).toBe(
				'Rp 10.000.000'
			);
		});

		it("should set maximal price form to Rp 15.000.000 when 'rentType' prop is 2", async () => {
			await wrapper.setProps({ rentType: 2 });

			expect(wrapper.find('[data-testid="filterPriceMax"]').element.value).toBe(
				'Rp 15.000.000'
			);
		});

		it("should set maximal price form to Rp 50.000.000 when 'rentType' prop is 2", async () => {
			await wrapper.setProps({ rentType: 3 });

			expect(wrapper.find('[data-testid="filterPriceMax"]').element.value).toBe(
				'Rp 50.000.000'
			);
		});
	});

	describe("Based on 'propertyType' prop value, when it's 'apartment'", () => {
		const wrapper = render({
			propsData: {
				propertyType: 'apartment'
			}
		});

		it("should set maximal price form to Rp 10.000.000 when 'rentType' prop is 0", async () => {
			await wrapper.setProps({ rentType: 0 });

			expect(wrapper.find('[data-testid="filterPriceMax"]').element.value).toBe(
				'Rp 10.000.000'
			);
		});

		it("should set maximal price form to Rp 25.000.000 when 'rentType' prop is 1", async () => {
			await wrapper.setProps({ rentType: 1 });

			expect(wrapper.find('[data-testid="filterPriceMax"]').element.value).toBe(
				'Rp 25.000.000'
			);
		});

		it("should set maximal price form to Rp 50.000.000 when 'rentType' prop is 2", async () => {
			await wrapper.setProps({ rentType: 2 });

			expect(wrapper.find('[data-testid="filterPriceMax"]').element.value).toBe(
				'Rp 50.000.000'
			);
		});

		it("should set maximal price form to Rp 500.000.000 when 'rentType' prop is 2", async () => {
			await wrapper.setProps({ rentType: 3 });

			expect(wrapper.find('[data-testid="filterPriceMax"]').element.value).toBe(
				'Rp 500.000.000'
			);
		});
	});

	describe("Based on 'propertyType' prop value, when it's 'house'", () => {
		it('should set maximal price form to Rp 50.000.000.000', async () => {
			const wrapper = render({
				propsData: {
					propertyType: 'house'
				}
			});

			await wrapper.vm.$nextTick();

			expect(wrapper.find('[data-testid="filterPriceMax"]').element.value).toBe(
				'Rp 50.000.000.000'
			);
		});
	});
});
