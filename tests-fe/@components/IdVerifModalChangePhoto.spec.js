import { createLocalVue, shallowMount } from '@vue/test-utils';
import IdVerifModalChangePhoto from 'Js/@components/IdVerifModalChangePhoto.vue';

describe('IdVerifModalChangePhoto.vue', () => {
	const localVue = createLocalVue();
	const propsData = {
		showModal: true
	};
	const wrapper = shallowMount(IdVerifModalChangePhoto, {
		localVue,
		propsData
	});

	const buttonActions = wrapper.findAll('.button-actions > button');

	it('should render idVerifModalShowPhoto', () => {
		expect(wrapper.find('#idVerifModalShowPhoto').exists()).toBe(true);
	});

	it("should emit show when 'lihat foto' button clicked", () => {
		buttonActions.at(0).trigger('click');
		expect(wrapper.emitted().show).toBeTruthy();
	});

	it("should emit change when 'ubah' button clicked", () => {
		buttonActions.at(1).trigger('click');
		expect(wrapper.emitted().change).toBeTruthy();
	});

	it("should emit delete when 'hapus' button clicked", () => {
		buttonActions.at(2).trigger('click');
		expect(wrapper.emitted().delete).toBeTruthy();
	});

	it("should emit close when 'batal' button clicked", () => {
		wrapper.find('.button-cancel button').trigger('click');
		expect(wrapper.emitted().close).toBeTruthy();
	});
});
