import { createLocalVue, shallowMount } from '@vue/test-utils';
import '../utils/mock-vue';
import room from './__mocks__/room.json';
import _get from 'lodash/get';
import RoomCard from 'Js/@components/RoomCardMobile.vue';
import mockVLazy from '../utils/mock-v-lazy';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

describe('RoomCard.vue', () => {
	mockWindowProperty('location', { pathname: null, href: '' });
	const localVue = createLocalVue();
	mockVLazy(localVue);
	const openRoom = function() {
		const url = _get(room, 'share_url');
		window.location.href = url;
	};
	const propsData = {
		cover: room.photo_url.medium,
		title: room['room-title'],
		genderId: room.gender,
		city: room.subdistrict,
		discount: room.price_title_format.discount,
		priceAfterDiscount: room.price_title_time,
		priceBeforeDiscount: room.price_title_time,
		isKost: true,
		roomProjectId: 0,
		rating: room.rating_string,
		lastUpdateLabel: room.online,
		isBookable: room.is_booking,
		isMamirooms: false,
		isPremium: room.is_premium_owner,
		isPromoted: room.is_promoted,
		isVerified: true,
		isMoneyBackGuarantee: true,
		hasPromo: Boolean(room.promo_title),
		roomLevelId: 4,
		okeKostId: 4,
		superKostId: 5,
		openRoom: openRoom
	};
	let wrapper;
	beforeEach(() => {
		wrapper = shallowMount(RoomCard, { localVue, propsData });
		RoomCard.computed.isOkeLevel.call(localVue);
		RoomCard.computed.isSuperLevel.call(localVue);
		RoomCard.computed.genderLabel.call(localVue);
		RoomCard.computed.genderColorVariant.call(localVue);
		RoomCard.computed.isRating.call(localVue);
		RoomCard.computed.hasBadges.call(localVue);
	});

	it('should render room-card', () => {
		expect(wrapper.find('.room-card').exists()).toBe(true);
	});

	it('should open the detail page on click', () => {
		const button = wrapper.find('button.room-card');
		const url = _get(room, 'share_url');
		button.trigger('click');
		expect(window.location.href).toEqual(url);
	});

	it('should render discount label correctly', () => {
		const { discount, hasPromo } = propsData;
		if (discount) {
			expect(wrapper.find('.discount-percentage').exists()).toBe(true);
		}
		if (hasPromo) {
			expect(wrapper.find('.has-promo').exists()).toBe(true);
		}
	});
});
