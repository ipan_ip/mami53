import { createLocalVue, shallowMount } from '@vue/test-utils';
import FilterKostType from 'Js/@components/FilterKostType.vue';

describe('FilterKostType.vue', () => {
	const localVue = createLocalVue();
	const propsData = {
		label: true,
		genderVal: [1]
	};
	const wrapper = shallowMount(FilterKostType, { localVue, propsData });

	it('should render filter-type class', () => {
		expect(wrapper.find('.filter-type').exists()).toBe(true);
	});

	it('should render props properly', () => {
		expect(wrapper.find('label').exists()).toBe(propsData.label);
		expect(wrapper.vm.gender).toBe(propsData.genderVal);
	});

	it('should emit setFilter when select option changed', () => {
		const gender = [0, 1, 2];
		wrapper.setData({ gender });
		wrapper.vm.$nextTick(() => {
			expect(wrapper.emitted().setFilter[0]).toEqual([gender]);
		});
	});
});
