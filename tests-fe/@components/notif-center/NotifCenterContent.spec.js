import Vuex from 'vuex';
import { createLocalVue, shallowMount } from '@vue/test-utils';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import NotifCenterContent from 'Js/@components/notif-center/NotifCenterContent.vue';
import userStore from 'user-store';

jest.mock('Js/@global/components/LoginUser', () => ({
	render() {
		return '';
	}
}));

const notifOptions = {
	isEmpty: true,
	isLoading: false,
	page: 1,
	hasNext: false,
	categoryId: 0,
	categoryLabel: ''
};

describe('NotifCenterContent.vue', () => {
	let store, wrapper, localVue;
	it('should render properly', () => {
		localVue = createLocalVue();
		mockVLazy(localVue);
		localVue.directive('infinite-scroll', (el, binding) => {
			return el.addEventListener('scroll', binding.value);
		});
		localVue.use(Vuex);
		store = new Vuex.Store(userStore);
		wrapper = shallowMount(NotifCenterContent, {
			localVue,
			propsData: {
				isDesktop: true,
				notifOptions,
				notifData: []
			},
			store
		});

		expect(wrapper.vm.isDesktop).toBeTruthy();
		expect(wrapper.vm.authCheckUser).toBeTruthy();
	});

	describe('Notification Type', () => {
		beforeEach(() => {
			store.replaceState({
				...userStore.state
			});
		});

		it("User isn't logged in yet", done => {
			store.commit('setAuthCheck', {
				...store.state.authCheck,
				user: null
			});
			wrapper.vm.$nextTick(() => {
				expect(wrapper.find('.notif-empty-text-title').text()).toBe(
					'Login dulu yuk!'
				);
				done();
			});
		});

		it('Empty notif & booking category', done => {
			wrapper.setProps({
				notifOptions: { ...notifOptions, categoryLabel: 'booking' }
			});
			wrapper.vm.$nextTick(() => {
				expect(wrapper.find('.notif-empty-text-title').text()).toBe(
					'Yuk Booking!'
				);
				done();
			});
		});

		it('Empty notif & not booking category', done => {
			wrapper.setProps({
				notifOptions: { ...notifOptions, categoryLabel: '' }
			});
			wrapper.vm.$nextTick(() => {
				expect(wrapper.find('.notif-empty-text-title').text()).toBe(
					'Belum ada notifikasi...'
				);
				done();
			});
		});

		it('has notif', done => {
			wrapper.setProps({
				notifOptions: { ...notifOptions, isEmpty: false },
				notifData: [
					{
						is_read: false,
						url: '',
						icon: '',
						title: 'title-notif',
						datetime: '2020/02/02',
						message: 'message-notif',
						buttons: []
					}
				]
			});
			wrapper.vm.$nextTick(() => {
				expect(wrapper.find('.notif-text-caption').text()).toBe(
					'message-notif'
				);
				done();
			});
		});
	});

	it('should loadMore when scrolling', () => {
		wrapper.vm.loadMore();

		expect(wrapper.emitted()['load-more']).toBeTruthy();
	});

	it('should open and close modal properly', () => {
		store.commit('setAuthCheck', {
			...store.state.authCheck,
			user: null
		});

		wrapper.vm.$nextTick(() => {
			wrapper.find('.btn.btn-primary').trigger('click');
			expect(wrapper.vm.isModalLogin).toBeTruthy();

			wrapper.vm.closeModalLogin();
			expect(wrapper.vm.isModalLogin).toBeFalsy();
		});
	});
});
