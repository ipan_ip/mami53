export default {
	state: {
		authCheck: { all: true, user: true, owner: false, admin: false }
	},
	mutations: {
		setAuthCheck(state, payload) {
			state.authCheck = payload;
		}
	}
};
