import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import NotifCenterMenu from 'Js/@components/notif-center/NotifCenterMenu';
import mockAxios from 'tests-fe/utils/mock-axios';
import userStore from 'user-store';

describe('NotifCenterMenu.vue', () => {
	let wrapper, store, localVue;
	const notifData = [
		{
			id: 0,
			name: 'All',
			icon: '',
			order: 0
		}
	];
	const notifCounter = [
		{
			category_id: 0,
			unread: 0
		}
	];
	const props = {
		notifCategory: {
			id: 0,
			notifCategoryData: notifData,
			notifCategoryCounter: notifCounter,
			notifCategoryClicked: [false]
		},
		isHomepage: false
	};
	localVue = createLocalVue();
	localVue.use(Vuex);
	store = new Vuex.Store(userStore);

	beforeEach(() => {
		wrapper = shallowMount(NotifCenterMenu, {
			localVue,
			propsData: props,
			store
		});
	});

	it('should render NotifCenterMenu', () => {
		expect(wrapper.find('.notif-center-menu').exists()).toBe(true);
	});

	describe('should getUnreadbyCategory', () => {
		wrapper = shallowMount(NotifCenterMenu, {
			localVue,
			propsData: props,
			store
		});
		const getUnreadFunction = wrapper.vm.getUnreadByCategory();

		// if  there's no unread notif
		it('should get none unread notif by category', () => {
			wrapper.setProps({
				notifCategory: {
					id: 1,
					notifCategoryData: notifData,
					notifCategoryCounter: [
						{
							category_id: 1,
							unread: 0
						}
					],
					notifCategoryClicked: [true, false]
				}
			});
			expect(getUnreadFunction).toBe(false);
		});

		// if there's unread notif
		it('should get unread notif by category', () => {
			wrapper.setProps({
				notifCategory: {
					id: 1,
					notifCategoryData: notifData,
					notifCategoryCounter: [
						{
							category_id: 1,
							unread: 3
						}
					],
					notifCategoryClicked: [true, false]
				}
			});

			wrapper.vm.$nextTick(() => {
				const unreadIndicator = wrapper.find('.unread-dot');
				expect(unreadIndicator.exists()).toBe(true);
			});
		});
	});

	it('should select category', () => {
		wrapper.setMethods({
			getUnreadByCategory: jest.fn().mockReturnValue(true),
			sendNotifCategoryRead: jest.fn()
		});

		const btn = wrapper.find('button');
		btn.trigger('click');
		expect(wrapper.vm.sendNotifCategoryRead).toHaveBeenCalled();
	});

	it('should send notif', () => {
		global.axios = mockAxios;
		const bugsnagClient = {
			notify: jest.fn()
		};
		window.bugsnagClient = bugsnagClient;
		axios.mockResolve({
			data: ''
		});
		axios.mockReject({
			bugsnagClient
		});
		wrapper.vm.sendNotifCategoryRead();
	});
});
