import { shallowMount } from '@vue/test-utils';
import NofitCenterHeadComponent from 'Js/@components/notif-center/NotifCenterHead';

describe('NotifCenterHead.vue', () => {
	const wrapper = shallowMount(NofitCenterHeadComponent, {
		propsData: {
			notifCount: 0
		}
	});

	it('should render notif center head', () => {
		const notifCenterHead = wrapper.find('.notif-center-head');
		expect(notifCenterHead.exists()).toBe(true);
	});

	it('should render props properly', () => {
		wrapper.setProps({ notifCount: 2 });
		expect(wrapper.vm.notifCount).toBe(2);
	});

	it('should emit close when hideNotifCenter press', () => {
		const buttonClose = wrapper.find('.head-close');
		buttonClose.trigger('click');
		wrapper.vm.$nextTick(() => {
			expect(wrapper.emitted('closed')).toBeTruthy();
			expect(wrapper.emitted('closed').length).toBe(1);
		});
	});
});
