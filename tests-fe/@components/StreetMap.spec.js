import { createLocalVue, shallowMount } from '@vue/test-utils';
import '../utils/mock-vue';
import StreetMap from 'Js/@components/StreetMap.vue';
import { mockComponent, mockComponentWithAttrs } from '../utils/mock-component';
jest.mock('vue2-google-maps', () => {});

describe('StreetMap.vue', () => {
	const localVue = createLocalVue();
	const wrapper = shallowMount(StreetMap, {
		stubs: {
			VueGoogleMaps: mockComponent,
			GmapStreetViewPanorama: mockComponentWithAttrs({
				class: 'gmap-street-view-panorama'
			})
		},
		localVue,
		propsData: {
			grid: 'sm-6',
			position: {}
		}
	});

	it('should render gmap street view panorama', () => {
		expect(wrapper.find('.gmap-street-view-panorama').exists()).toBe(true);
	});
});
