import { createLocalVue, shallowMount } from '@vue/test-utils';
import BreadcrumbPills from 'Js/@components/BreadcrumbPills.vue';
import Vuex from 'vuex';

describe('BreadcrumbPills.vue', () => {
	const localVue = createLocalVue();
	localVue.use(Vuex);

	const grid = 'col-xs-3';
	const contents = ['Home', 'Search', 'Detail'];
	const store = {
		state: {
			section: 2
		}
	};
	const wrapper = shallowMount(BreadcrumbPills, {
		localVue,
		propsData: { grid, contents },
		store: new Vuex.Store(store)
	});

	it('should render all contents', () => {
		const steps = wrapper.findAll('.bs-wizard-step');
		expect(steps.length).toBe(contents.length);
		expect(steps.at(0).classes(grid)).toBe(true);
		expect(steps.at(0).classes('complete')).toBe(true);
		expect(steps.at(1).classes('active')).toBe(true);
		expect(steps.at(2).classes('disabled')).toBe(true);
		expect(
			steps
				.at(2)
				.find('.bs-wizard-info.text-center')
				.text()
		).toBe(contents[2]);
	});
});
