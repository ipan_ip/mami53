import { createLocalVue, shallowMount } from '@vue/test-utils';
import FilterCloseButton from 'Js/@components/FilterCloseButton.vue';

describe('FilterCloseButton.vue', () => {
	const localVue = createLocalVue();
	const map = {};
	global.addEventListener = jest.fn((event, cb) => (map[event] = cb));
	const wrapper = shallowMount(FilterCloseButton, { localVue });
	const buttonCloseFilter = wrapper.find('.btn-close-filter');
	const emitPress = () => wrapper.emitted().press;
	const ESC_KEY_CODE = 27;

	it('should render close button filter', () => {
		expect(buttonCloseFilter.exists()).toBe(true);
	});

	it('should emit press with false value when clicked', () => {
		buttonCloseFilter.trigger('click');
		expect(emitPress()[0]).toEqual([false]);
	});

	it('should emit press with false value when clicked', () => {
		map.keyup({ keyCode: ESC_KEY_CODE });
		expect(emitPress()[1]).toEqual([false]);
		map.keyup({ keyCode: 21 });
		expect(emitPress()[2]).toBeFalsy();
	});
});
