import { shallowMount, createLocalVue } from '@vue/test-utils';
import DetachedPagination from 'Js/@components/DetachedPagination.vue';

const localVue = createLocalVue();

describe('DetachedPagination', () => {
	const propsData = {
		value: 1,
		totalPage: 3
	};

	const vuejsPaginate = {
		template: `<div class="vuejs-paginate"><li v-for="i in pageCount" @click="$emit('input', i)"></li></div>`,
		props: ['pageCount']
	};

	let wrapper;

	beforeEach(() => {
		wrapper = shallowMount(DetachedPagination, {
			localVue,
			propsData,
			stubs: { pagination: vuejsPaginate }
		});
	});

	it('should wrap vuejs-paginate', () => {
		expect(wrapper.find('.vuejs-paginate').exists()).toBe(true);
	});

	it('should update page when vuejs-paginate page updated', async () => {
		jest.clearAllMocks();
		const page2 = wrapper.findAll('.vuejs-paginate li').at(1);
		page2.trigger('click');
		await wrapper.vm.$nextTick();
		expect(wrapper.vm.page).toBe(2);
		expect(wrapper.emitted('input')[0][0]).toBe(2);
	});

	it('should update current page v-model change', async () => {
		jest.clearAllMocks();
		wrapper.setProps({ value: 3 });
		await wrapper.vm.$nextTick();
		expect(wrapper.vm.page).toBe(3);
		expect(wrapper.emitted('input')[0][0]).toBe(3);
	});
});
