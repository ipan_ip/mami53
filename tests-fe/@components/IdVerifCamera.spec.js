import { createLocalVue, shallowMount } from '@vue/test-utils';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import IdVerifCamera from 'Js/@components/IdVerifCamera.vue';

const localVue = createLocalVue();
mockVLazy(localVue);

describe('IdVerifCamera.vue', () => {
	let mediaDevicesMock = {
		getUserMedia: jest.fn().mockResolvedValue({
			getTracks: jest.fn(() => {
				return [{ stop: jest.fn }];
			})
		})
	};
	global.navigator.mediaDevices = mediaDevicesMock;
	const propsData = { snapType: 'card' };

	const generateWrapper = ({ methodsProp = {}, mocksProp = {} } = {}) => {
		const shallowMountProp = {
			localVue,
			propsData,
			mocks: { ...mocksProp },
			methods: { ...methodsProp }
		};
		return shallowMount(IdVerifCamera, shallowMountProp);
	};

	it('should load the component', () => {
		const wrapper = generateWrapper({
			mocksProp: { navigator: { isMobile: true } }
		});
		expect(wrapper.find('.input-camera-container').exists()).toBe(true);
	});

	it('should emit cancel capture event', () => {
		const wrapper = generateWrapper({
			mocksProp: { navigator: { isMobile: true } }
		});
		wrapper.vm.cancelCapture();
		expect(wrapper.emitted('cancel-capture')).toBeTruthy();
	});

	it('should close modal webcam', () => {
		const wrapper = generateWrapper({
			mocksProp: { navigator: { isMobile: true } }
		});
		wrapper.vm.closeModalWebcam();
		expect(wrapper.vm.isModalWebcam).toBe(false);
	});

	it('should return smaller media size device is desktop', () => {
		const wrapper = generateWrapper({
			mocksProp: { navigator: { isMobile: false } }
		});
		expect(wrapper.vm.mediaWidth).toBe(300);
		expect(wrapper.vm.mediaHeight).toBe(225);
	});

	describe('test create video element function', () => {
		it('should create video element', async () => {
			const wrapper = generateWrapper({
				mocksProp: { navigator: { isMobile: false } }
			});
			await wrapper.vm.createVideoElement();
			expect(navigator.mediaDevices.getUserMedia).toBeCalled();
		});

		it('should show warning modal', async () => {
			mediaDevicesMock = {
				getUserMedia: jest.fn().mockRejectedValue(new Error())
			};
			global.navigator.mediaDevices = mediaDevicesMock;
			const wrapper = generateWrapper({
				mocksProp: { navigator: { isMobile: true } }
			});
			wrapper.vm.createVideoElement();
			await wrapper.vm.$nextTick();
			expect(wrapper.vm.isModalWebcam).toBe(true);
			expect(wrapper.vm.isHasPermission).toBe(false);
		});
	});
});
