import { createLocalVue, shallowMount } from '@vue/test-utils';
import ListPlaceholder from 'Js/@components/ListPlaceholder.vue';

describe('ListPlaceholder.vue', () => {
	const localVue = createLocalVue();
	const propsData = {
		mode: 1,
		type: 'job'
	};
	let wrapper;
	beforeEach(() => {
		wrapper = shallowMount(ListPlaceholder, {
			localVue,
			propsData
		});
	});

	it('should render the placeholder correctly', () => {
		const listPlaceholder = wrapper.find('.placeholder-wrapper');
		expect(listPlaceholder.exists()).toBe(true);
	});

	it('should render the placeholder mode 1 for "job" type correctly', () => {
		const ListPlaceholderContainers = wrapper.findAll(
			'.placeholder-container-job'
		);
		expect(ListPlaceholderContainers.length).toBe(3);
	});

	it('should render the placeholder mode 2 for "job" type correctly', () => {
		wrapper = shallowMount(ListPlaceholder, {
			localVue,
			propsData: {
				mode: 2,
				type: 'job'
			}
		});
		const ListPlaceholderContainers = wrapper.findAll(
			'.placeholder-container-job'
		);
		expect(ListPlaceholderContainers.length).toBe(2);
	});

	it('should render the placeholder mode 1 for "not-job" type correctly', () => {
		wrapper = shallowMount(ListPlaceholder, {
			localVue,
			propsData: {
				mode: 1,
				type: 'not-job'
			}
		});
		const ListPlaceholderContainers = wrapper.findAll('.placeholder-container');
		expect(ListPlaceholderContainers.length).toBe(3);
	});

	it('should render the placeholder mode 2 for "not-job" type correctly', () => {
		wrapper = shallowMount(ListPlaceholder, {
			localVue,
			propsData: {
				mode: 2,
				type: 'not-job'
			}
		});
		const ListPlaceholderContainers = wrapper.findAll('.placeholder-container');
		expect(ListPlaceholderContainers.length).toBe(2);
	});

	it('should render the placeholder mode 4 for "not-job" type correctly', () => {
		wrapper = shallowMount(ListPlaceholder, {
			localVue,
			propsData: {
				mode: 4,
				type: 'not-job'
			}
		});
		const ListPlaceholderContainers = wrapper.findAll('.placeholder-container');
		expect(ListPlaceholderContainers.length).toBe(4);
	});

	it('should render the placeholder mode for "only-pic" type correctly', () => {
		wrapper = shallowMount(ListPlaceholder, {
			localVue,
			propsData: {
				mode: 1,
				type: 'only-pic'
			}
		});
		const ListPlaceholderContainers = wrapper.findAll(
			'.placeholder-container.for-pic'
		);
		expect(ListPlaceholderContainers.length).toBe(3);
	});
});
