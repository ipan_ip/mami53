import { createLocalVue, shallowMount } from '@vue/test-utils';
import '../utils/mock-vue';
import mockAxios from 'tests-fe/utils/mock-axios';
import room from './__mocks__/room.json';
import RoomList from 'Js/@components/RoomList.vue';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import Vuex from 'vuex';
window.Vue = require('vue');
window.swal = require('sweetalert2/dist/sweetalert2.js');
const MixinSwalLoading = require('Js/@mixins/MixinSwalLoading');
const MixinSwalSuccessError = require('Js/@mixins/MixinSwalSuccessError');
// eslint-disable-next-line
const mixinNavigator = require('Js/@mixins/MixinNavigatorIsMobile');
describe('RoomList.vue', () => {
	const localVue = createLocalVue();
	localVue.mixin([mixinNavigator, MixinSwalLoading, MixinSwalSuccessError]);
	const propsData = {
		rooms: [room],
		grids: 'col-md-4 col-sm-6 col-xs-12',
		limit: 10,
		isHorizontal: false,
		forRelated: false,
		relatedUrl: 'cari',
		tracking: ''
	};
	global.axios = mockAxios;
	const bugsnagClient = {
		notify: jest.fn()
	};
	global.bugsnagClient = bugsnagClient;
	const Moengage = {
		track_event: jest.fn()
	};
	global.Moengage = Moengage;
	const open = jest.fn();
	global.open = open;
	global.okeKostId = 4;
	global.superKostId = 5;
	const Cookies = {
		get: jest.fn(() => undefined),
		set: jest.fn(),
		remove: jest.fn(),
		getJSON: jest.fn(() => [])
	};
	global.Cookies = Cookies;

	mockWindowProperty('open', open);
	mockWindowProperty('location', {
		pathname: '/cari'
	});
	mockWindowProperty('document.head', {
		querySelector: jest.fn(() => {
			return {
				content: 'ASD1720980ASKJDSADAD'
			};
		})
	});
	mockWindowProperty('tracker', jest.fn());
	mockWindowProperty('authCheck', {
		owner: false,
		all: false
	});
	mockWindowProperty('authData', {
		all: null
	});
	const favComponentStub = {
		template: `<button class="fav-click" @click="$emit('toggleLove', {})"/>`
	};
	const $toasted = {
		show: jest.fn()
	};
	const stubComponent = {
		'room-card': favComponentStub
	};
	const mocks = {
		$toasted
	};
	let wrapper;
	beforeEach(() => {
		wrapper = shallowMount(RoomList, {
			localVue,
			propsData,
			stubs: stubComponent,
			mocks
		});
	});

	it('Computed token should return correctly', () => {
		expect(wrapper.vm.token).toEqual('ASD1720980ASKJDSADAD');
	});

	it('Computed authData should return global authData if store not exist', () => {
		expect(wrapper.vm.authData).toEqual(authData);
	});

	it('Computed authCheck should return global authCheck if store not exist', () => {
		expect(wrapper.vm.authCheck).toEqual(authCheck);
	});

	it('Computed authCheck should return from store if exist', () => {
		localVue.use(Vuex);
		const store = new Vuex.Store({
			state: {
				authCheck: {
					owner: false,
					all: false
				},
				authData: {
					all: null
				}
			}
		});
		wrapper = shallowMount(RoomList, {
			localVue,
			propsData,
			store,
			stubs: stubComponent
		});
		expect(wrapper.vm.authCheck).toEqual(wrapper.vm.$store.state.authCheck);
	});

	it('Computed authData should return from store if exist', () => {
		localVue.use(Vuex);
		const store = new Vuex.Store({
			state: {
				authCheck: {
					owner: false,
					all: false
				},
				authData: {
					all: null
				}
			}
		});
		wrapper = shallowMount(RoomList, {
			localVue,
			propsData,
			store,
			stubs: stubComponent
		});
		expect(wrapper.vm.authData).toEqual(wrapper.vm.$store.state.authData);
	});

	it('Should call method loveThisPage on click favorit', async () => {
		const loveThisPageSpy = jest.spyOn(wrapper.vm, 'loveThisPage');
		wrapper.setMethods({ loveThisPage: loveThisPageSpy });
		await wrapper.vm.$nextTick();

		expect(wrapper.find('button.fav-click').exists()).toBe(true);

		wrapper.find('button.fav-click').trigger('click');

		expect(loveThisPageSpy).toBeCalled();
		expect(wrapper.emitted().toggleLoginModal).toBeTruthy();
	});

	it('Should call method loveThisPage and return error if not user', async () => {
		mockWindowProperty('authCheck', {
			owner: true,
			user: false,
			all: true
		});
		mockWindowProperty('authData', {
			all: {
				id: 1
			}
		});

		wrapper = shallowMount(RoomList, {
			localVue,
			propsData,
			stubs: stubComponent
		});
		const loveThisPageSpy = jest.spyOn(wrapper.vm, 'loveThisPage');
		const swalErrorSpy = jest.spyOn(wrapper.vm, 'swalError');
		wrapper.setMethods({
			loveThisPage: loveThisPageSpy,
			swalError: swalErrorSpy
		});
		await wrapper.vm.$nextTick();

		expect(wrapper.find('button.fav-click').exists()).toBe(true);

		wrapper.find('button.fav-click').trigger('click');

		expect(loveThisPageSpy).toBeCalled();
		expect(swalErrorSpy).toBeCalledWith(
			null,
			'Silakan logout akun Pemilik dan login akun Pencari terlebih dahulu'
		);
	});

	it('Should call method loveThisPage and call method toggler to update love status and go into if statement', async () => {
		mockWindowProperty('authCheck', {
			owner: false,
			user: true,
			all: true
		});
		mockWindowProperty('authData', {
			all: {
				id: 1
			}
		});
		axios.mockResolve({
			data: {
				status: 200,
				success: 'love'
			}
		});
		wrapper = shallowMount(RoomList, {
			localVue,
			propsData,
			stubs: stubComponent,
			methods: {
				setLoveStatus: jest.fn()
			},
			mocks: {
				$toasted: {
					show: jest.fn()
				}
			}
		});
		const loveThisPageSpy = jest.spyOn(wrapper.vm, 'loveThisPage');
		const toggleLovePostSpy = jest.spyOn(wrapper.vm, 'toggleLovePost');
		wrapper.setMethods({
			loveThisPage: loveThisPageSpy,
			toggleLovePost: toggleLovePostSpy
		});
		await wrapper.vm.$nextTick();

		expect(wrapper.find('button.fav-click').exists()).toBe(true);

		wrapper.find('button.fav-click').trigger('click');

		expect(loveThisPageSpy).toBeCalled();
		expect(toggleLovePostSpy).toBeCalled();
	});

	it('Should call method loveThisPage and call method toggler to update love status and go into else if statement', async () => {
		axios.mockResolve({
			data: {
				status: 200,
				success: 'unlove'
			}
		});
		const loveThisPageSpy = jest.spyOn(wrapper.vm, 'loveThisPage');
		const toggleLovePostSpy = jest.spyOn(wrapper.vm, 'toggleLovePost');
		wrapper.setMethods({
			loveThisPage: loveThisPageSpy,
			toggleLovePost: toggleLovePostSpy
		});
		await wrapper.vm.$nextTick();

		expect(wrapper.find('button.fav-click').exists()).toBe(true);

		wrapper.find('button.fav-click').trigger('click');
		await wrapper.vm.$nextTick();

		expect(loveThisPageSpy).toBeCalled();
		expect(toggleLovePostSpy).toBeCalled();
		expect($toasted.show).not.toHaveBeenCalled();
	});

	it('Should call method loveThisPage and call method toggler to update love status and go into else statement', async () => {
		axios.mockResolve({
			data: {
				status: 200,
				success: 'typo'
			}
		});
		const loveThisPageSpy = jest.spyOn(wrapper.vm, 'loveThisPage');
		const toggleLovePostSpy = jest.spyOn(wrapper.vm, 'toggleLovePost');
		wrapper.setMethods({
			loveThisPage: loveThisPageSpy,
			toggleLovePost: toggleLovePostSpy
		});
		await wrapper.vm.$nextTick();

		expect(wrapper.find('button.fav-click').exists()).toBe(true);

		wrapper.find('button.fav-click').trigger('click');
		await wrapper.vm.$nextTick();

		expect(loveThisPageSpy).toBeCalled();
		expect(toggleLovePostSpy).toBeCalled();
		expect($toasted.show).not.toHaveBeenCalled();
	});

	it('Should call method loveThisPage and return false', async () => {
		axios.mockResolve({
			data: {
				status: false
			}
		});
		const loveThisPageSpy = jest.spyOn(wrapper.vm, 'loveThisPage');
		const toggleLovePostSpy = jest.spyOn(wrapper.vm, 'toggleLovePost');
		wrapper.setMethods({
			loveThisPage: loveThisPageSpy,
			toggleLovePost: toggleLovePostSpy
		});
		await wrapper.vm.$nextTick();

		expect(wrapper.find('button.fav-click').exists()).toBe(true);

		wrapper.find('button.fav-click').trigger('click');
		await wrapper.vm.$nextTick();

		expect(loveThisPageSpy).toBeCalled();
		expect(toggleLovePostSpy).toBeCalled();
		expect($toasted.show).toBeCalledWith('Gagal, silahkan coba lagi', {
			className: 'fav-share-widget-custom-toast-fail',
			position: 'bottom-center',
			duration: 2000
		});
	});

	it('Should call method loveThisPage and call catch statement', async () => {
		axios.mockReject('error');
		const loveThisPageSpy = jest.spyOn(wrapper.vm, 'loveThisPage');
		const toggleLovePostSpy = jest.spyOn(wrapper.vm, 'toggleLovePost');
		wrapper.setMethods({
			loveThisPage: loveThisPageSpy,
			toggleLovePost: toggleLovePostSpy
		});
		await wrapper.vm.$nextTick();

		expect(wrapper.find('button.fav-click').exists()).toBe(true);

		wrapper.find('button.fav-click').trigger('click');

		expect(loveThisPageSpy).toBeCalled();
		expect(toggleLovePostSpy).toBeCalled();
		expect(bugsnagClient.notify).toBeCalled();
	});

	it('should not setupTrackingClassKost and moengageEventTrack there is room', () => {
		wrapper.setProps({ rooms: [] });
		wrapper.setMethods({
			setupTrackingClassKost: jest.fn()
		});
		wrapper.vm.$nextTick(() => {
			expect(wrapper.vm.setupTrackingClassKost).toBeCalledTimes(0);
		});
	});

	it('should update idkamar cookies when kost-list-container clicked', () => {
		wrapper.vm.openRoom(room);
		wrapper.vm.openRelated();
		wrapper.vm.$nextTick(() => {
			expect(Cookies.get).toBeCalled();
			expect(Cookies.set).toBeCalledWith('idkamar', room._id.toString(), {
				expires: 15
			});
			Cookies.get = jest.fn(() => '1');
			global.Cookies = Cookies;
			wrapper.vm.openRoom(room);
			wrapper.vm.$nextTick(() => {
				expect(Cookies.get).toBeCalled();
				expect(Cookies.remove).toBeCalled();
				expect(Cookies.set).toBeCalledWith(
					'idkamar',
					'1,' + room._id.toString(),
					{
						expires: 15
					}
				);
			});
		});
	});

	it('should not call window.open when device is standalone', () => {
		jest.clearAllMocks();
		mockWindowProperty(
			'matchMedia',
			jest.fn(() => {
				return { matches: true };
			})
		);
		wrapper.vm.openRoom(room);
		wrapper.vm.openRelated();
		expect(open).not.toBeCalled();
	});

	it('should call openRoom function when material card is clicked', () => {
		const kostListCard = wrapper.find('.kost-list-container.material-card-1');
		wrapper.setMethods({ openRoom: jest.fn() });
		if (kostListCard.exists()) {
			kostListCard.trigger('click');
			expect(wrapper.vm.openRoom).toBeCalled();
		}
	});
});
