import { createLocalVue, shallowMount } from '@vue/test-utils';
import IdVerifModalWarning from 'Js/@components/IdVerifModalWarning.vue';
import mockVLazy from '../utils/mock-v-lazy';
import mockComponent from '../utils/mock-component';

describe('IdVerifModalWarning.vue', () => {
	const localVue = createLocalVue();
	mockVLazy(localVue);
	const propsData = {
		showModal: true
	};
	const wrapper = shallowMount(IdVerifModalWarning, {
		stubs: { FilterCloseButton: mockComponent },
		localVue,
		propsData
	});

	it('should render idVerifModalWarning', () => {
		expect(wrapper.find('#idVerifModalWarning').exists()).toBe(true);
	});

	it('should emit close when close button clicked', () => {
		wrapper.find('.booking-webcam-close').trigger('click');
		expect(wrapper.emitted().close).toBeTruthy();
	});

	it('should emit next when next button clicked', () => {
		wrapper.find('.booking-webcam-button > button').trigger('click');
		expect(wrapper.emitted().next).toBeTruthy();
	});
});
