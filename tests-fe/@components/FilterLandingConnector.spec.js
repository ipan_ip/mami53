import { createLocalVue, shallowMount } from '@vue/test-utils';
import FilterLandingConnector from 'Js/@components/FilterLandingConnector.vue';

describe('FilterLandingConnector.vue', () => {
	const localVue = createLocalVue();
	const propsData = {
		landingBreadcrumb: [{ name: 'home' }, { name: 'search' }],
		landingConnector: [
			{ title: 'kost', slug: 'kost' },
			{ title: 'cari apartemen', slug: 'cari-apartemen' }
		]
	};

	const wrapper = shallowMount(FilterLandingConnector, {
		localVue,
		propsData
	});

	it('should render filter-connector class', () => {
		expect(wrapper.find('.filter-connector').exists()).toBe(true);
	});

	it('should render props properly', () => {
		const landingConnectorOptions = wrapper.findAll(
			'#landingConnector > option'
		);
		expect(landingConnectorOptions.at(0).text()).toBe(
			propsData.landingBreadcrumb[1].name
		);
		expect(landingConnectorOptions.at(1).text()).toBe(
			propsData.landingConnector[0].title
		);
		expect(landingConnectorOptions.at(1).attributes().value).toBe(
			propsData.landingConnector[0].slug
		);
		expect(landingConnectorOptions.at(2).text()).toBe(
			propsData.landingConnector[1].title
		);
		expect(landingConnectorOptions.at(2).attributes().value).toBe(
			propsData.landingConnector[1].slug
		);
	});

	it('should watch landingCategory changes', () => {
		global.open = jest.fn();
		wrapper.setData({ landingCategory: 'cari' });
		wrapper.vm.$nextTick(() => {
			expect(global.open).toBeCalledWith('cari', '_self');
			wrapper.setData({ landingCategory: 'active' });
			wrapper.vm.$nextTick(() => {
				expect(global.open).toHaveBeenCalledTimes(1);
			});
		});
	});

	it('should remove pulsing-animation class when landingConnector clicked', () => {
		const mockedLandingConnectorDOM = {
			classList: { contains: jest.fn(), remove: jest.fn(), add: jest.fn() }
		};
		global.document.getElementById = () => mockedLandingConnectorDOM;
		wrapper.find('#landingConnector').trigger('click');
		wrapper.vm.$nextTick(() => {
			expect(mockedLandingConnectorDOM.classList.remove).toBeCalledWith(
				'pulsing-animation'
			);
		});
	});
});
