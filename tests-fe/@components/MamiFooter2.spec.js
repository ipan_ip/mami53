import { createLocalVue, shallowMount } from '@vue/test-utils';
import MamiFooter2 from 'Js/@components/MamiFooter2.vue';
import mockVLazy from '../utils/mock-v-lazy';

describe('MamiFooter2.vue', () => {
	const localVue = createLocalVue();
	mockVLazy(localVue);
	global.authCheck = { owner: true };
	const wrapper = shallowMount(MamiFooter2, {
		localVue
	});

	it('should render page footer', () => {
		expect(wrapper.find('.page-footer').exists()).toBe(true);
	});

	it('should render correct instagram url', () => {
		const instagramUsername = {
			career: 'mamikos_talent',
			default: 'mamikosapp'
		};

		expect(
			wrapper.find('.social-footer [href*="instagram.com"]').attributes().href
		).toBe('https://www.instagram.com/' + instagramUsername.default + '/');

		wrapper.setProps({ isCareerPage: true });
		wrapper.vm.$nextTick(() => {
			expect(
				wrapper.find('.social-footer [href*="instagram.com"]').attributes().href
			).toBe('https://www.instagram.com/' + instagramUsername.career + '/');
		});
	});
});
