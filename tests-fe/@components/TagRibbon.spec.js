import { createLocalVue, shallowMount } from '@vue/test-utils';
import TagRibbon from 'Js/@components/TagRibbon.vue';

describe('TagRibbon.vue', () => {
	const localVue = createLocalVue();
	const wrapper = shallowMount(TagRibbon, {
		localVue,
		propsData: {
			text: 'ribbon text',
			variant: 'green'
		}
	});

	it('should render correctly', () => {
		expect(wrapper.find('.tag-ribbon').exists()).toBe(true);
		expect(wrapper.find('.tag-ribbon').text()).toBe('ribbon text');
		expect(wrapper.find('.tag-ribbon').classes()).toContain('--green');
	});
});
