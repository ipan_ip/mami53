import { shallowMount, createLocalVue } from '@vue/test-utils';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import EmptyState from 'Js/@components/EmptyState.vue';

const localVue = createLocalVue();
mockVLazy(localVue);

describe('ConsultantEmptyState.vue', () => {
	let createWrapper = Function;

	beforeEach(() => {
		createWrapper = (options = {}) => {
			return shallowMount(EmptyState, {
				localVue,
				...options
			});
		};
	});

	it('Should render empty state component and match snapshot', () => {
		const wrapper = createWrapper();
		expect(wrapper.element).toMatchSnapshot();
	});

	it("Should has't action button when no button slot", () => {
		const wrapper = createWrapper();
		expect(wrapper.find('.action-button').exists()).toBe(false);
	});

	it('Should render button slot and match snapshot', () => {
		const buttonText = 'Button slot text';
		const wrapper = createWrapper({
			slots: {
				button: buttonText
			}
		});
		expect(wrapper.find('.action-button').text()).toBe(buttonText);
		expect(wrapper.element).toMatchSnapshot();
	});
});
