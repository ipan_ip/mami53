import { createLocalVue, shallowMount } from '@vue/test-utils';
import DbetTenantInputPhone from 'Js/_dbet-tenant/components/DbetTenantForm/DbetTenantInputPhone.vue';

const localVue = createLocalVue();

const stubs = {
	DbetTenantField: { template: '<div><slot/></div>' },
	BgInput: { template: '<input />' }
};

describe('DbetTenantInputPhone.vue', () => {
	const wrapper = shallowMount(DbetTenantInputPhone, {
		localVue,
		stubs,
		propsData: { value: '' }
	});

	it('should render dbet tenant input phone', () => {
		expect(wrapper.find('.dbet-tenant-input-phone').exists()).toBe(true);
	});

	it('should emit input when name set', () => {
		wrapper.findComponent(stubs.BgInput).vm.$emit('input', '08272');
		expect(wrapper.emitted('input')[0][0]).toBe('08272');
	});

	it('should validate when value changed', async () => {
		const spy = jest.spyOn(wrapper.vm, 'validate');
		await wrapper.setProps({ value: '08272123' });
		expect(spy).toBeCalled();
		spy.mockRestore();
	});

	it('should validate properly', async () => {
		const { errorMessageOptions } = wrapper.vm;

		await wrapper.setProps({ value: '081aaaa' });
		expect(wrapper.vm.errorMessage).toBe(errorMessageOptions.number);

		await wrapper.setProps({ value: '0999222' });
		expect(wrapper.vm.errorMessage).toBe(errorMessageOptions.format);

		await wrapper.setProps({ value: '082112' });
		expect(wrapper.vm.errorMessage).toBe(errorMessageOptions.min);

		await wrapper.setProps({ value: '08188233322212' });
		expect(wrapper.vm.errorMessage).toBe(errorMessageOptions.max);

		await wrapper.setProps({ value: '' });
		expect(wrapper.vm.errorMessage).toBe(errorMessageOptions.required);

		await wrapper.setProps({ value: '081882333222' });
		expect(wrapper.vm.errorMessage).toBe('');
	});
});
