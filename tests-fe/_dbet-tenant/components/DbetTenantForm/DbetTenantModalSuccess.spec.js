import { createLocalVue, shallowMount } from '@vue/test-utils';
import DbetTenantModalSuccess from 'Js/_dbet-tenant/components/DbetTenantForm/DbetTenantModalSuccess.vue';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

const localVue = createLocalVue();

const BgModal = { template: '<div><slot/><slot name="footer"/></div>' };
const BgButton = {
	template: `<button @click="$emit('click')"><slot/></button>`
};

const locationAssign = jest.fn();
mockWindowProperty('window.location', { assign: locationAssign });

describe('DbetTenantModalSuccess.vue', () => {
	const wrapper = shallowMount(DbetTenantModalSuccess, {
		localVue,
		stubs: { BgModal, BgButton }
	});

	it('should render success modal', () => {
		expect(wrapper.find('.dbet-modal-success').exists()).toBe(true);
	});

	it('should navigate to homepage on modal close', () => {
		wrapper.findComponent(BgModal).vm.$emit('close');
		expect(locationAssign).toBeCalledWith('/');
	});

	it('should navigate to homepage when action button clicked', () => {
		jest.clearAllMocks();
		wrapper.find('button').trigger('click');
		expect(locationAssign).toBeCalledWith('/');
	});
});
