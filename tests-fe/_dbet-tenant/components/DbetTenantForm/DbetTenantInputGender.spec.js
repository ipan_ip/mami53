import { createLocalVue, shallowMount } from '@vue/test-utils';
import DbetTenantInputGender from 'Js/_dbet-tenant/components/DbetTenantForm/DbetTenantInputGender.vue';

const localVue = createLocalVue();

const stubs = {
	DbetTenantField: { template: '<div><slot/></div>' },
	DbetTenantTagRadio: { template: '<input type="radio" />' }
};

describe('DbetTenantInputGender.vue', () => {
	const wrapper = shallowMount(DbetTenantInputGender, {
		localVue,
		stubs,
		propsData: {
			value: 'male'
		}
	});

	it('should render dbet input gender', () => {
		expect(wrapper.find('.dbet-tenant-input-gender').exists()).toBe(true);
	});

	it('should emit input when gender inputted', () => {
		wrapper.findComponent(stubs.DbetTenantTagRadio).vm.$emit('input', 'male');
		expect(wrapper.emitted('input')[0][0]).toBe('male');
	});

	it('should validate input gender properly', async () => {
		const { errorMessageRequired: required } = wrapper.vm;

		await wrapper.setProps({ value: '' });
		expect(wrapper.vm.errorMessage).toBe(required);

		await wrapper.setProps({ value: 'mix' });
		expect(wrapper.vm.errorMessage).toBe(required);

		await wrapper.setProps({ value: 'female', disabledGender: 'female' });
		expect(wrapper.vm.errorMessage).toBe(required);

		await wrapper.setProps({ value: 'male', disabledGender: '' });
		expect(wrapper.vm.errorMessage).toBe('');
	});
});
