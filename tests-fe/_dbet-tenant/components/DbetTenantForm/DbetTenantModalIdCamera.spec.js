import { createLocalVue, shallowMount } from '@vue/test-utils';
import DbetTenantModalIdCamera from 'Js/_dbet-tenant/components/DbetTenantForm/DbetTenantModalIdCamera.vue';

const localVue = createLocalVue();

const stubs = {
	IdVerifCamera: { template: '<div class="id-verif-camera" />' },
	DbetTenantNavbar: { template: '<nav />' },
	BgModal: { template: '<div><slot/></div>' }
};

describe('DbetTenantModalIdCamera.vue', () => {
	const wrapper = shallowMount(DbetTenantModalIdCamera, { localVue, stubs });

	it('should render modal id camera', () => {
		expect(wrapper.find('.dbet-tenant-id-camera').exists()).toBe(true);
	});

	it('should emit image when idVerifCamera saved', () => {
		wrapper
			.findComponent(stubs.IdVerifCamera)
			.vm.$emit('save-card', 'image-id-verif-camera');
		expect(wrapper.emitted('save')[0][0]).toBe('image-id-verif-camera');
	});

	it('should emit input close when navbar close clicked', () => {
		wrapper.findComponent(stubs.DbetTenantNavbar).vm.$emit('close');
		expect(wrapper.emitted('input')[0][0]).toBe(false);
	});
});
