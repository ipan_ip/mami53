import { createLocalVue, shallowMount } from '@vue/test-utils';
import DbetTenantInputName from 'Js/_dbet-tenant/components/DbetTenantForm/DbetTenantInputName.vue';

const localVue = createLocalVue();

const stubs = {
	DbetTenantField: { template: '<div><slot/></div>' },
	BgInput: { template: '<input />' }
};

describe('DbetTenantInputName.vue', () => {
	const wrapper = shallowMount(DbetTenantInputName, {
		localVue,
		stubs,
		propsData: { value: '' }
	});

	it('should render dbet tenant input name', () => {
		expect(wrapper.find('.dbet-tenant-input-name').exists()).toBe(true);
	});

	it('should emit input when name set', () => {
		wrapper.findComponent(stubs.BgInput).vm.$emit('input', 'nama');
		expect(wrapper.emitted('input')[0][0]).toBe('nama');
	});

	it('should validate when value changed', async () => {
		const spy = jest.spyOn(wrapper.vm, 'validate');
		await wrapper.setProps({ value: 'mamikos' });
		expect(spy).toBeCalled();
		spy.mockRestore();
	});

	it('should validate properly', async () => {
		const { errorMessageOptions } = wrapper.vm;

		await wrapper.setProps({ value: 'valid name' });
		expect(wrapper.vm.errorMessage).toBe('');

		await wrapper.setProps({ value: 'inv@lid name' });
		expect(wrapper.vm.errorMessage).toBe(errorMessageOptions.alphabet);

		await wrapper.setProps({ value: '' });
		expect(wrapper.vm.errorMessage).toBe(errorMessageOptions.required);

		await wrapper.setProps({ value: 'a' });
		expect(wrapper.vm.errorMessage).toBe(errorMessageOptions.min);

		const moreThan50Name = new Array(51).fill('a').join('');
		await wrapper.setProps({ value: moreThan50Name });
		expect(wrapper.vm.errorMessage).toBe(errorMessageOptions.max);
	});
});
