import { createLocalVue, shallowMount } from '@vue/test-utils';
import DbetTenantModalSelectAdditional from 'Js/_dbet-tenant/components/DbetTenantForm/DbetTenantModalSelectAdditional.vue';
import Vuex from 'vuex';
import store from 'Js/_dbet-tenant/store/storeData';

const localVue = createLocalVue();
localVue.use(Vuex);

const additionals = () => [
	{
		id: 1,
		name: 'Laundry',
		price: 'Rp25.000',
		checked: false
	},
	{
		id: 2,
		name: 'Wifi',
		price: 'Rp50.000',
		checked: false
	},
	{
		id: 3,
		name: 'Listrik',
		price: 'Rp100.000',
		checked: true
	},
	{
		id: 4,
		name: 'Sampah',
		price: 'Rp20.000',
		checked: false
	},
	{
		id: 5,
		name: 'Keamanan',
		price: 'Rp30.000',
		checked: false
	}
];

const stubs = {
	BgModal: { template: '<div><slot/><slot name="footer"/></div>' },
	BgButton: { template: `<button @click="$emit('click')"><slot/></button>` }
};

describe('DbetTenantModalSelectAdditional.vue', () => {
	const wrapper = shallowMount(DbetTenantModalSelectAdditional, {
		localVue,
		propsData: { additionalPrice: additionals(), value: true, selectedIds: [] },
		stubs,
		store: new Vuex.Store(store)
	});

	it('should render dbet additional modal', () => {
		expect(wrapper.find('.dbet-modal-additional').exists()).toBe(true);
	});

	it('should show all additional data', () => {
		expect(wrapper.findAll('.dbet-modal-additional__item').length).toBe(
			additionals().length
		);
	});

	it('should reset data when cancelled', async () => {
		const editedAdditionalPrices = additionals();
		[0, 2, 4].forEach(i => {
			editedAdditionalPrices[i].checked = true;
		});
		wrapper.setData({ innerAdditionalPrice: editedAdditionalPrices });

		wrapper.findComponent(stubs.BgModal).vm.$emit('close');
		expect(wrapper.vm.innerAdditionalPrice).toEqual(additionals());
	});

	it('should close modal and not call reset data when button save clicked', () => {
		jest.clearAllMocks();
		const spyResetData = jest.spyOn(wrapper.vm, 'resetData');
		wrapper.find('.dbet-modal-additional__action-save').trigger('click');

		expect(wrapper.emitted('update:isShow')[0][0]).toBe(false);
		expect(spyResetData).not.toBeCalled();
		spyResetData.mockRestore();
	});
});
