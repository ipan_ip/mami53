import { createLocalVue, shallowMount } from '@vue/test-utils';
import DbetTenantInputJob from 'Js/_dbet-tenant/components/DbetTenantForm/DbetTenantInputJob.vue';

const localVue = createLocalVue();

const stubs = {
	DbetTenantTagRadio: { template: '<input type="radio" />' },
	DbetTenantField: { template: '<div><slot/></div>' },
	BgSelect: { template: '<select />' },
	BgInput: { template: '<input />' }
};

describe('DbetTenantInputJob.vue', () => {
	const wrapper = shallowMount(DbetTenantInputJob, { localVue, stubs });

	const getLastEmitValue = event => {
		const emit = wrapper.emitted(event);
		return emit[emit.length - 1][0];
	};

	it('should render dbet tenant input job', () => {
		expect(wrapper.find('.dbet-tenant-input-job').exists()).toBe(true);
	});

	it('should render job select options if job has options', async () => {
		await wrapper.setProps({ job: 'kuliah' });
		expect(wrapper.find('.dbet-tenant-input-job__select').exists()).toBe(true);
	});

	it('should render input other job if job options `Lainnya` selected', async () => {
		await wrapper.setProps({ job: 'kuliah', workPlace: 'Lainnya' });
		expect(wrapper.find('.dbet-tenant-input-job__other').exists()).toBe(true);
	});

	it('should emit update job and reset selection if job is changed', () => {
		wrapper.findComponent(stubs.DbetTenantTagRadio).vm.$emit('input', 'kerja');

		expect(getLastEmitValue('update:job')).toBe('kerja');
		expect(getLastEmitValue('update:workPlace')).toBe('');
		expect(getLastEmitValue('update:otherWorkPlace')).toBe('');
	});

	it('should assign correct job format if workplace is registered', () => {
		const workPlace = wrapper.vm.jobOptions.kuliah.options[0].val;
		wrapper.setProps({ job: 'kuliah', workPlace });
		wrapper.vm.assignJob();

		expect(wrapper.vm.selectedJob).toBe('kuliah');
		expect(wrapper.vm.selectedWorkPlace).toBe(workPlace);
		expect(getLastEmitValue('update:otherWorkPlace')).toBe('');
	});

	it('should assign correct job format if workplace is unregistered', () => {
		wrapper.setProps({ job: 'kuliah', workPlace: 'unregistered on list' });
		wrapper.vm.assignJob();

		expect(wrapper.vm.selectedJob).toBe('kuliah');
		expect(getLastEmitValue('update:workPlace', 'Lainnya'));
		expect(getLastEmitValue('update:otherWorkPlace', 'unregistered on list'));
	});

	it('should validate job input properly', () => {
		wrapper.setProps({ job: '' });
		let isValid = wrapper.vm.validate();
		expect(wrapper.vm.errorMessage).toBe(wrapper.vm.errorMessageRequired);
		expect(isValid).toBe(false);

		wrapper.setProps({ job: 'kuliah' });
		isValid = wrapper.vm.validate();
		expect(wrapper.vm.errorMessage).toBe('');
		expect(isValid).toBe(true);
	});
});
