import { createLocalVue, shallowMount } from '@vue/test-utils';
import DbetTenantModalRentPriceCustom from 'Js/_dbet-tenant/components/DbetTenantForm/DbetTenantModalRentPriceCustom.vue';
import Vuex from 'vuex';
import store from 'Js/_dbet-tenant/store/storeData';

const localVue = createLocalVue();
localVue.use(Vuex);

const stubs = {
	BgModal: { template: `<div><slot/><slot name="footer"/></div>` },
	BgButton: { template: `<button @click="$emit('click')"><slot/></button>` },
	BgSelect: { template: `<select />` },
	VueNumeric: {
		template: `<div><input ref="numeric" class="vue-numeric" type="tel" /></div>`
	}
};

describe('DbetTenantModalRentPriceCustom.vue', () => {
	const wrapper = shallowMount(DbetTenantModalRentPriceCustom, {
		localVue,
		stubs,
		store: new Vuex.Store(store),
		propsData: { rentCountType: 'weekly' }
	});

	it('should render modal custom price', () => {
		expect(wrapper.find('.modal-custom-price').exists()).toBe(true);
	});

	it('should set given prop to default selected rent price', () => {
		wrapper.setProps({ isShow: true });
		expect(wrapper.vm.selectedRentType).toBe(wrapper.vm.rentCountType);
	});

	it('should validate custom price properly', async () => {
		wrapper.findComponent(stubs.VueNumeric).vm.$emit('input', 3000);
		await wrapper.find('.modal-custom-price__action-save').trigger('click');
		expect(wrapper.vm.invalidCustomPriceMessage).not.toBe('');

		wrapper.findComponent(stubs.VueNumeric).vm.$emit('input', 10000);
		await wrapper.find('.modal-custom-price__action-save').trigger('click');
		expect(wrapper.vm.invalidCustomPriceMessage).toBe('');
	});

	it('should reset invalid rent type message when rent type changed', async () => {
		wrapper.findComponent(stubs.BgSelect).vm.$emit('input', 'monthly');
		expect(wrapper.vm.invalidRentTypeMessage).toBe('');
	});

	it('should validate rent type properly', async () => {
		wrapper.findComponent(stubs.BgSelect).vm.$emit('input', '');
		await wrapper.find('.modal-custom-price__action-save').trigger('click');
		expect(wrapper.vm.invalidRentTypeMessage).not.toBe('');

		wrapper.findComponent(stubs.BgSelect).vm.$emit('input', 'monthly');
		await wrapper.find('.modal-custom-price__action-save').trigger('click');
		expect(wrapper.vm.invalidRentTypeMessage).toBe('');
	});
});
