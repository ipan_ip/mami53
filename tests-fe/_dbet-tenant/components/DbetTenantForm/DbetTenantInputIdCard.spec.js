import { createLocalVue, shallowMount } from '@vue/test-utils';
import DbetTenantInputIdCard from 'Js/_dbet-tenant/components/DbetTenantForm/DbetTenantInputIdCard.vue';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

const localVue = createLocalVue();
mockVLazy(localVue);

const stubs = {
	DbetTenantField: { template: '<div><slot/></div>' },
	BgButton: { template: `<button @click="$emit('click')"/>` },
	IdVerifModalWarning: { template: `<div><slot/></div>` },
	DbetTenantModalIdCamera: { template: `<div><slot/></div>` }
};

describe('DbetTenantInputIdCard.vue', () => {
	const wrapper = shallowMount(DbetTenantInputIdCard, { localVue, stubs });

	it('should render input id card', () => {
		expect(wrapper.find('.dbet-tenant-input-id').exists()).toBe(true);
	});

	it('should render action card if is not uploaded yet', async () => {
		await wrapper.setData({ idImage: '' });
		expect(wrapper.find('.dbet-tenant-input-id__action').exists()).toBe(true);
	});

	it('should show modal warning when card action clicked', async () => {
		await wrapper.find('.dbet-tenant-input-id__action').trigger('click');
		expect(wrapper.vm.isShowModalWarning).toBe(true);
	});

	it('should close modal warning when modal camera openened', () => {
		wrapper.findComponent(stubs.IdVerifModalWarning).vm.$emit('next');
		expect(wrapper.vm.isShowModalWarning).toBe(false);
		expect(wrapper.vm.isShowModalIdCamera).toBe(true);
	});

	it('should set image when camera emitted save', () => {
		wrapper
			.findComponent(stubs.DbetTenantModalIdCamera)
			.vm.$emit('save', { src: 'some-image' });

		expect(wrapper.vm.idImage).toBe('some-image');
	});

	it('should render update card if is uploaded', async () => {
		await wrapper.setData({ idImage: 'some-image' });
		expect(wrapper.find('.dbet-tenant-input-id__card').exists()).toBe(true);
	});

	it('should validate data properly', () => {
		wrapper.setProps({ isRequired: false });
		wrapper.setData({ idImage: '' });
		let isValid = wrapper.vm.validate();

		expect(isValid).toBe(true);
		expect(wrapper.vm.errorMessage).toBe('');

		wrapper.setProps({ isRequired: true });
		wrapper.setData({ idImage: '' });
		isValid = wrapper.vm.validate();

		expect(isValid).toBe(false);
		expect(wrapper.vm.errorMessage).toBe(wrapper.vm.errorMessageRequired);
	});
});
