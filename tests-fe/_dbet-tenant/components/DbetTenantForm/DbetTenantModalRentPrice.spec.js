import { createLocalVue, shallowMount } from '@vue/test-utils';
import DbetTenantModalRentPrice from 'Js/_dbet-tenant/components/DbetTenantForm/DbetTenantModalRentPrice.vue';
import Vuex from 'vuex';
import store from 'Js/_dbet-tenant/store/storeData';
import prices from './__mocks__/prices.json';

const localVue = createLocalVue();
localVue.use(Vuex);

const stubs = {
	BgModal: { template: `<div><slot/><slot name="footer"/></div>` },
	BgButton: { template: `<button @click="$emit('click')"><slot/></button>` },
	BgRadio: { template: `<input type="radio" />` }
};

describe('DbetTenantModalRentPrice.vue', () => {
	const wrapper = shallowMount(DbetTenantModalRentPrice, {
		localVue,
		stubs,
		propsData: { value: 'weekly' },
		store: new Vuex.Store(store)
	});

	it('should render modal rent price', () => {
		expect(wrapper.find('.modal-rent-price').exists()).toBe(true);
	});

	it('should set rent price from props when created', () => {
		expect(wrapper.vm.selectedRentPrice).toBe('weekly');
	});

	it('should set rent price from props when modal showed', async () => {
		await wrapper.setProps({ isShow: false });
		await wrapper.setProps({ value: 'yearly' });
		await wrapper.setProps({ isShow: true });
		expect(wrapper.vm.selectedRentPrice).toBe('yearly');
	});

	it('should render all rent price', async () => {
		wrapper.vm.$store.state.room.roomPrices = prices;
		await wrapper.vm.$nextTick();

		expect(wrapper.findAll('.modal-rent-price__option-radio').length).toBe(
			prices.length + 1
		);
	});

	it('should set rent price when rent price option selected', async () => {
		wrapper.vm.$store.state.room.roomPrices = prices;
		await wrapper.vm.$nextTick();

		wrapper
			.findAllComponents(stubs.BgRadio)
			.at(1)
			.vm.$emit('input', wrapper.vm.priceOptions[1].type);

		expect(wrapper.vm.selectedRentPrice).toBe(wrapper.vm.priceOptions[1].type);
	});

	it('should close modal and not emit custom when not custom price selected', async () => {
		jest.clearAllMocks();
		await wrapper.setData({ selectedRentPrice: 'yearly' });
		wrapper.find('.modal-rent-price__action-save').trigger('click');

		expect(wrapper.emitted('custom')).toBeFalsy();
		expect(wrapper.emitted('update:isShow')[0][0]).toBe(false);
	});

	it('should emit custom when custom price selected', async () => {
		await wrapper.setData({ selectedRentPrice: 'custom' });
		wrapper.find('.modal-rent-price__action-save').trigger('click');

		expect(wrapper.emitted('custom')[0]).toBeTruthy();
	});
});
