import { createLocalVue, shallowMount } from '@vue/test-utils';
import DbetTenantModalDueDate from 'Js/_dbet-tenant/components/DbetTenantForm/DbetTenantModalDueDate.vue';
import Vuex from 'vuex';
import store from 'Js/_dbet-tenant/store/storeData';

const localVue = createLocalVue();
localVue.use(Vuex);

const stubs = {
	BgModal: { template: `<div><slot/><slot name="footer"/></div>` },
	BgButton: { template: `<button @click="$emit('click')"><slot/></button>` },
	BgSelect: { template: `<select />` },
	DbetTenantField: { template: '<div><slot/></div>' }
};

describe('DbetTenantModalDueDate.vue', () => {
	const wrapper = shallowMount(DbetTenantModalDueDate, {
		localVue,
		stubs,
		store: new Vuex.Store(store)
	});

	it('should render modal due date', () => {
		expect(wrapper.find('.dbet-due-date').exists()).toBe(true);
	});

	it('should return correct select options', () => {
		expect(wrapper.vm.dueDateOptions.length).toBe(31);
		expect(wrapper.vm.dueDateOptions[0]).toEqual({ val: 1 });
		expect(wrapper.vm.dueDateOptions[30]).toEqual({ val: 31 });
	});

	it('should show required message if no option selected when button save clicked', async () => {
		await wrapper.setData({ selectedDueDate: 0 });
		wrapper.find('.dbet-due-date__action-save').trigger('click');
		await wrapper.vm.$nextTick();

		expect(
			wrapper.find('.dbet-due-date__select').attributes('error-message')
		).toBe(wrapper.vm.errorMessageRequired);
	});

	it('should close modal if valid selected option and button save clicked', async () => {
		await wrapper.setData({ selectedDueDate: 12 });
		wrapper.find('.dbet-due-date__action-save').trigger('click');

		expect(wrapper.vm.$store.state.dbet.dueDate).toBe(12);
		expect(wrapper.vm.$store.state.saved.savedDueDate).toBe(true);
		expect(wrapper.emitted('update:isShow')[0][0]).toBe(false);
	});

	it('should close modal if user click cancel button', () => {
		wrapper.find('.dbet-due-date__action-cancel').trigger('click');

		expect(wrapper.emitted('update:isShow')[1][0]).toBe(false);
	});
});
