import { createLocalVue, shallowMount } from '@vue/test-utils';
import DbetTenantForm from 'Js/_dbet-tenant/components/DbetTenantForm/DbetTenantForm.vue';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mockSwalLoading from 'tests-fe/utils/mock-swal-loading';
import Vuex from 'vuex';
import store from 'Js/_dbet-tenant/store/storeData';
import prices from './__mocks__/prices.json';

const localVue = createLocalVue();

window.Vue = localVue;
mockSwalLoading();
mockVLazy(localVue);

localVue.use(Vuex);

const BgButton = {
	template: `<button class="bg-button" @click="$emit('click')"><slot/></button>`
};

const $router = { push: jest.fn() };

const mockStore = Object.assign({}, store);
mockStore.state.roomId = 999;

const axios = {
	post: jest
		.fn()
		.mockResolvedValueOnce({ data: { status: true } })
		.mockResolvedValueOnce({
			data: { status: false, meta: { message: 'error message' } }
		})
		.mockRejectedValueOnce(new Error('error'))
};

global.axios = axios;
global.bugsnagClient = { notify: jest.fn() };
global.tracker = jest.fn();

describe('DbetTenantForm.vue', () => {
	const wrapper = shallowMount(DbetTenantForm, {
		localVue,
		stubs: { BgButton, BgLink: BgButton },
		store: new Vuex.Store(mockStore),
		mocks: { $router, navigator: { isMobile: true } }
	});

	const setValid = isValid => {
		[
			'savedUser',
			'savedRoomNumber',
			'savedDueDate',
			'savedRentPrice',
			'savedAdditionalPrice'
		].forEach(savedState => {
			wrapper.vm.$store.state.saved[savedState] = isValid;
		});
	};

	it('should render dbet tenant form', () => {
		expect(wrapper.find('.dbet-tenant-form').exists()).toBe(true);
	});

	it('should set valid to false when data is not fulfilled', () => {
		setValid(false);
		expect(wrapper.vm.isValid).toBe(false);
	});

	it('should set valid to true when data is fulfilled', () => {
		setValid(true);
		expect(wrapper.vm.isValid).toBe(true);
	});

	it('should render rent price label properly', () => {
		wrapper.vm.$store.state.room.roomPrices = prices;
		wrapper.vm.$store.state.dbet.rentType = 'custom';
		wrapper.vm.$store.state.dbet.rentTypeCustom = prices[2].type;
		wrapper.vm.$store.state.dbet.price = prices[2].price;
		wrapper.vm.$store.state.saved.savedRentPrice = true;

		expect(wrapper.vm.rentPriceLabel).toBe('Rp600.000 Per 3 Bulan');
	});

	it('should render additional price label', () => {
		wrapper.vm.$store.state.dbet.rentType = 'monthly';
		wrapper.vm.$store.state.dbet.selectedAdditionalIds = [
			prices[1].additionals[0].id,
			prices[1].additionals[1].id
		];

		expect(wrapper.vm.additionalPriceLabel).toBe(
			`${prices[1].additionals[0].name}, ${prices[1].additionals[1].name}`
		);
	});

	it('should show toast when save button clicked if there is uncomplete data', async () => {
		setValid(false);
		await wrapper.find('.dbet-tenant-form__action-save').trigger('click');
		expect(wrapper.vm.isShowUncompleteFormToast).toBe(true);
	});

	it('should save data properly', async () => {
		const spyError = jest.spyOn(wrapper.vm, 'swalError');
		const spyNotify = jest.spyOn(global.bugsnagClient, 'notify');

		// success
		await wrapper.setData({ isShowModalSuccess: false });
		await wrapper.vm.submitBookingRequestData();
		expect(wrapper.vm.isShowModalSuccess).toBe(true);

		// status false
		await wrapper.setData({ isShowModalSuccess: false });
		await wrapper.vm.submitBookingRequestData();
		expect(wrapper.vm.isShowModalSuccess).toBe(false);
		expect(spyError).toBeCalledWith(null, 'error message');

		// failed
		await wrapper.setData({ isShowModalSuccess: false });
		await wrapper.vm.submitBookingRequestData();
		expect(wrapper.vm.isShowModalSuccess).toBe(false);
		expect(spyNotify).toBeCalled();

		spyError.mockRestore();
		spyNotify.mockRestore();
	});

	it('should show modal when action item clicked with modal prop', () => {
		wrapper
			.findAll('.dbet-tenant-form__info-item-action')
			.at(1)
			.trigger('click');

		expect(wrapper.vm.isShowModalSelectRoom).toBe(true);
	});

	it('should go to specified route when action item clicked with route prop', () => {
		wrapper
			.findAll('.dbet-tenant-form__info-item-action')
			.at(0)
			.trigger('click');

		expect($router.push).toBeCalledWith(wrapper.vm.infoList[0].route);
	});
});
