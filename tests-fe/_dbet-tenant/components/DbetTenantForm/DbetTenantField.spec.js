import { createLocalVue, shallowMount } from '@vue/test-utils';
import DbetTenantField from 'Js/_dbet-tenant/components/DbetTenantForm/DbetTenantField.vue';

const localVue = createLocalVue();

const stubs = {
	BgText: {
		template: '<span><slot/></span>'
	}
};

describe('DbetTenantField.vue', () => {
	const wrapper = shallowMount(DbetTenantField, {
		localVue,
		stubs
	});

	it('should render dbet tenant input', () => {
		expect(wrapper.find('.dbet-tenant-field').exists()).toBe(true);
	});
});
