import { createLocalVue, shallowMount } from '@vue/test-utils';
import DbetTenantTagRadio from 'Js/_dbet-tenant/components/DbetTenantForm/DbetTenantTagRadio.vue';

const localVue = createLocalVue();

describe('DbetTenantTagRadio.vue', () => {
	const wrapper = shallowMount(DbetTenantTagRadio, {
		localVue,
		propsData: {
			value: '',
			selectedValue: 'male',
			label: 'label',
			disabled: false
		}
	});

	it('should render tag radio', () => {
		expect(wrapper.find('.dbet-tenant-tag-radio').exists()).toBe(true);
	});

	it('should emit input when clicked if not disabled', () => {
		wrapper.find('.dbet-tenant-tag-radio').trigger('click');
		expect(wrapper.emitted('input')[0][0]).toBe('male');
	});

	it('should not emit input when clicked if disabled', async () => {
		await wrapper.setProps({ disabled: true });
		expect(wrapper.find('.dbet-tenant-tag-radio--disabled').exists()).toBe(
			true
		);
		wrapper.find('.dbet-tenant-tag-radio').trigger('click');
		expect(wrapper.emitted('input')[1]).toBeFalsy();
	});
});
