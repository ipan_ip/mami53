import { createLocalVue, shallowMount } from '@vue/test-utils';
import DbetTenantModalSelectRoom from 'Js/_dbet-tenant/components/DbetTenantForm/DbetTenantModalSelectRoom.vue';
import Vuex from 'vuex';
import store from 'Js/_dbet-tenant/store/storeData';
import mockRooms from './__mocks__/room-number-list.json';
import { flushPromises } from 'tests-fe/utils/promises';

const localVue = createLocalVue();
localVue.use(Vuex);

const BgModal = { template: '<div><slot/><slot name="footer"/></div>' };
const BgButton = {
	template: `<button @click="$emit('click')"><slot/></button>`
};
const BgRadio = { template: '<div><slot /></div>' };

const axios = {
	get: jest.fn().mockResolvedValue({
		data: {
			status: true,
			data: {
				units: mockRooms
			}
		}
	})
};
global.axios = axios;

const mocks = {
	openSwalLoading: jest.fn(),
	swalError: jest.fn(),
	closeSwalLoading: jest.fn()
};

const mockStore = { ...store };

const bugsnagClient = { notify: jest.fn() };
global.bugsnagClient = bugsnagClient;

describe('DbetTenantModalSelectRoom.vue', () => {
	const createWrapper = async () => {
		mockStore.state.isLoadedRoomNumberList = false;

		const wrapper = shallowMount(DbetTenantModalSelectRoom, {
			localVue,
			stubs: { BgModal, BgButton, BgRadio },
			store: new Vuex.Store(mockStore),
			mocks
		});

		await flushPromises();
		await wrapper.vm.$nextTick();
		return wrapper;
	};

	it('should render modal select room', async () => {
		const wrapper = await createWrapper();
		expect(wrapper.find('.modal-select-room').exists()).toBe(true);
	});

	it('should emit input close when cancel button clicked', async () => {
		const wrapper = await createWrapper();
		wrapper.find('.modal-select-room__action-cancel').trigger('click');
		expect(wrapper.emitted('update:isShow')[0][0]).toBe(false);
	});

	it('should set designer room on button save clicked', async () => {
		const wrapper = await createWrapper();
		const selectedRoom = wrapper.vm.roomOptions[3].id;

		wrapper.findComponent(BgRadio).vm.$emit('input', selectedRoom);
		wrapper.find('.modal-select-room__action-save').trigger('click');
		await wrapper.vm.$nextTick();

		expect(wrapper.findComponent(BgRadio).vm.$attrs.value).toBe(selectedRoom);
	});

	it('should handle response status false on fetching room list properly', async () => {
		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: {
					status: false
				}
			})
		};
		const wrapper = await createWrapper();
		await wrapper.vm.$nextTick();
		await flushPromises();

		expect(mocks.swalError).toBeCalled();
	});

	it('should handle error on fetching room list properly', async () => {
		jest.clearAllMocks();
		global.axios = {
			get: jest.fn().mockRejectedValue(new Error('error'))
		};
		const wrapper = await createWrapper();
		await wrapper.vm.$nextTick();
		await flushPromises();

		expect(mocks.swalError).toBeCalled();
		expect(bugsnagClient.notify).toBeCalled();
	});
});
