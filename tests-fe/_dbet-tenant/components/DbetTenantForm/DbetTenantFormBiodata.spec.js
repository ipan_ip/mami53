import { createLocalVue, shallowMount } from '@vue/test-utils';
import DbetTenantFormBiodata from 'Js/_dbet-tenant/components/DbetTenantForm/DbetTenantFormBiodata.vue';
import Vuex from 'vuex';
import mockSwalLoading from 'tests-fe/utils/mock-swal-loading';
import { flushPromises } from 'tests-fe/utils/promises';
import store from 'Js/_dbet-tenant/store/storeData';

const localVue = createLocalVue();
localVue.use(Vuex);
window.Vue = localVue;
mockSwalLoading();

const stubs = {
	BgButton: { template: `<button @click="$emit('click')" />` },
	DbetTenantInputIdCard: { template: '<div />' }
};

const mocks = {
	$router: { go: jest.fn() },
	$refs: {
		customPriceInput: {
			$refs: {
				numeric: { blur: jest.fn() }
			}
		}
	}
};

const mockGlobal = {
	axios: {
		post: jest.fn().mockResolvedValue({
			data: {
				status: true,
				media: { id: 123, urls: { medium: 'image-large.jpg' } }
			}
		})
	},
	bugsnagClient: { notify: jest.fn() }
};

Object.keys(mockGlobal).forEach(key => {
	global[key] = mockGlobal[key];
});

describe('DbetTenantFormBiodata.vue', () => {
	const wrapper = shallowMount(DbetTenantFormBiodata, {
		localVue,
		stubs,
		mocks,
		store: new Vuex.Store(store)
	});

	it('should render tenant biodata form', () => {
		expect(wrapper.find('.dbet-tenant-form-biodata').exists()).toBe(true);
	});

	it('should redirect to prevoius page properly', () => {
		wrapper.vm.$refs = {};

		['fullName', 'phone', 'gender', 'job', 'id'].forEach(ref => {
			wrapper.vm.$refs[ref] = { validate: () => false };
		});

		wrapper.setData({ identityIdUrl: 'some-image-url' });

		wrapper.find('.dbet-tenant-form-biodata__action-save').trigger('click');
		expect(mocks.$router.go).not.toBeCalledWith(-1);

		['fullName', 'phone', 'gender', 'job', 'id'].forEach(ref => {
			wrapper.vm.$refs[ref] = { validate: () => true };
		});

		wrapper.find('.dbet-tenant-form-biodata__action-save').trigger('click');
		expect(mocks.$router.go).toBeCalledWith(-1);
	});

	it('should reset image url when new image added', () => {
		const newimage = 'data:image/png;base64,abcdef';
		wrapper.setData({
			identityIdUrl: 'some/image-url.jpg',
			identityIdImageBase64: ''
		});
		wrapper
			.findComponent(stubs.DbetTenantInputIdCard)
			.vm.$emit('input', newimage);

		expect(wrapper.vm.identityIdUrl).toBe(null);
		expect(wrapper.vm.identityIdImageBase64).toBe(newimage);
	});

	it('should upload file first and get image url on save when no image url', async () => {
		wrapper.setData({
			identityIdUrl: null,
			identityIdImageBase64: 'data:image/png;base64,iasdlkj'
		});
		wrapper.find('.dbet-tenant-form-biodata__action-save').trigger('click');
		await flushPromises();

		expect(wrapper.vm.identityIdUrl).toEqual({ medium: 'image-large.jpg' });
	});

	it('should handle on upload image properly', async () => {
		// status false
		const spyError = jest.spyOn(wrapper.vm, 'swalError');
		axios.post = jest.fn().mockResolvedValue({ data: { status: false } });
		wrapper.setData({
			identityIdUrl: null,
			identityIdImageBase64: 'data:image/png;base64,iasdlkj'
		});
		wrapper.find('.dbet-tenant-form-biodata__action-save').trigger('click');
		await flushPromises();

		expect(spyError).toBeCalled();
		spyError.mockRestore();

		// catch error
		const spyBugsnag = jest.spyOn(global.bugsnagClient, 'notify');
		axios.post = jest.fn().mockRejectedValue(new Error('error'));
		wrapper.setData({
			identityIdUrl: null,
			identityIdImageBase64: 'data:image/png;base64,iasdlkj'
		});
		wrapper.find('.dbet-tenant-form-biodata__action-save').trigger('click');
		await flushPromises();

		expect(spyBugsnag).toBeCalled();
		spyError.mockRestore();
	});
});
