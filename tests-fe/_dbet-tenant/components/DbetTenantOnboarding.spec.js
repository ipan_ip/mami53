import { createLocalVue, shallowMount } from '@vue/test-utils';
import DbetTenantOnboarding from 'Js/_dbet-tenant/components/DbetTenantOnboarding.vue';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import Vuex from 'vuex';
import store from 'Js/_dbet-tenant/store/storeData';
import { flushPromises } from 'tests-fe/utils/promises';

const localVue = createLocalVue();
localVue.use(Vuex);

const locationReplace = jest.fn();
mockWindowProperty('window.location', {
	replace: locationReplace,
	href: 'mamikos.com/ob/dbet-123/onboarding'
});

global.oxWebUrl = 'owner.test.com';

const axios = {
	get: jest.fn().mockRejectedValue(new Error('error'))
};
global.axios = axios;

global.bugsnagClient = { notify: jest.fn() };
global.tracker = jest.fn();

const stubs = {
	DbetTenantModalSwiper: { template: `<div><slot /></div>` }
};

const mocks = {
	$router: { replace: jest.fn(), push: jest.fn() },
	$route: { query: { step: 1 } },
	$store: { commit: jest.fn() },
	navigator: { isMobile: jest.fn() },
	openSwalLoading: jest.fn(),
	closeSwalLoading: jest.fn()
};

describe('DbetTenantOnboarding.vue', () => {
	const createWrapper = async () => {
		const wrapper = shallowMount(DbetTenantOnboarding, {
			localVue,
			stubs,
			mocks,
			store: new Vuex.Store(store)
		});
		await flushPromises();
		await wrapper.vm.$nextTick();
		return wrapper;
	};

	it('should render onboarding component', async () => {
		const wrapper = await createWrapper();
		expect(wrapper.find('.dbet-tenant-onboarding').exists()).toBe(true);
	});

	it('should go to form page if next button clicked in last step and has logged in as user', async () => {
		const wrapper = await createWrapper();
		const spy = jest.spyOn(wrapper.vm.$router, 'push');
		wrapper.vm.$store.state.authCheck.user = true;
		wrapper
			.findComponent(stubs.DbetTenantModalSwiper)
			.vm.$emit('last-step-next-clicked');

		expect(spy).toBeCalledWith({ name: 'form' });
		spy.mockRestore();
	});

	it('should go to owner dashboard if next button clicked in last step and has logged in as owner', async () => {
		const wrapper = await createWrapper();
		const spy = jest.spyOn(wrapper.vm.$router, 'push');
		wrapper.vm.$store.state.authCheck = {
			user: false,
			owner: true
		};

		await wrapper.vm.$nextTick();

		wrapper
			.findComponent(stubs.DbetTenantModalSwiper)
			.vm.$emit('last-step-next-clicked');

		expect(spy).not.toBeCalled();
		expect(locationReplace).toBeCalledWith('owner.test.com');
		spy.mockRestore();
	});

	it('should go to form page using window location replace if next button clicked in last step and has not logged in yet', async () => {
		const wrapper = await createWrapper();
		wrapper.vm.$store.state.authCheck = {
			user: false,
			owner: false
		};
		wrapper.vm.$store.state.dbet.dbetCodeFormatted = 'dbet-T35T';

		await wrapper.vm.$nextTick();

		wrapper
			.findComponent(stubs.DbetTenantModalSwiper)
			.vm.$emit('last-step-next-clicked');

		expect(locationReplace).toBeCalledWith('/ob/dbet-T35T/form');
	});

	it('should go to homepage when modal closed', async () => {
		const wrapper = await createWrapper();
		wrapper.findComponent(stubs.DbetTenantModalSwiper).vm.$emit('close');

		expect(locationReplace).toBeCalledWith('/');
	});

	it('should go to prev button clicked on first page', async () => {
		const wrapper = await createWrapper();
		jest.clearAllMocks();
		wrapper
			.findComponent(stubs.DbetTenantModalSwiper)
			.vm.$emit('first-step-prev-clicked');

		expect(locationReplace).toBeCalledWith('/');
	});

	it('should set dynamic onboarding properly when successfully fetch data', async () => {
		const dynamicOnboardingContent = 'dynamic onboarding content';
		global.axios.get = jest.fn().mockResolvedValue({
			data: {
				status: true,
				data: {
					dynamic_onboarding_label: dynamicOnboardingContent
				}
			}
		});
		const wrapper = await createWrapper();
		wrapper.vm.$store.state.dbet.promoContent = '';
		wrapper.vm.$store.state.dbet.isLoadedPromo = false;
		await wrapper.vm.getDynamicOnboarding();
		await flushPromises();

		expect(wrapper.vm.onboardingStepsRender.length).toBe(
			wrapper.vm.staticOnboardingSteps.length + 1
		);
		expect(wrapper.vm.onboardingStepsRender[0]).toEqual(
			expect.objectContaining({ content: dynamicOnboardingContent })
		);
	});

	it('should not set dynamic onboarding if request response false', async () => {
		global.axios.get = jest.fn().mockResolvedValue({
			data: {
				status: false,
				data: {
					dynamic_onboarding_label: ''
				}
			}
		});
		const wrapper = await createWrapper();
		wrapper.vm.$store.state.dbet.promoContent = '';
		wrapper.vm.$store.state.dbet.isLoadedPromo = false;
		await wrapper.vm.getDynamicOnboarding();
		await flushPromises();

		expect(wrapper.vm.onboardingStepsRender.length).toBe(
			wrapper.vm.staticOnboardingSteps.length
		);
	});
});
