import { createLocalVue, shallowMount } from '@vue/test-utils';
import DbetTenantNavbar from 'Js/_dbet-tenant/components/DbetTenantNavbar.vue';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

const localVue = createLocalVue();
mockVLazy(localVue);

describe('DbetTenantNavbar.vue', () => {
	const wrapper = shallowMount(DbetTenantNavbar, {
		localVue,
		propsData: { isAllowBack: true }
	});

	it('should render navbar', () => {
		expect(wrapper.find('.dbet-navbar').exists()).toBe(true);
		expect(wrapper.find('.dbet-navbar__icon-back').exists()).toBe(true);
		expect(wrapper.find('.dbet-navbar__brand').exists()).toBe(true);
	});
});
