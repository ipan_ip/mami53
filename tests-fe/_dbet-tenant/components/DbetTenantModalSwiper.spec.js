import { createLocalVue, shallowMount } from '@vue/test-utils';
import DbetTenantModalSwiper from 'Js/_dbet-tenant/components/DbetTenantModalSwiper.vue';

const localVue = createLocalVue();

const stubs = {
	BgModal: { template: `<div><slot /><slot name="footer"/></div>` },
	BgIllustration: { template: `<div />` },
	BgButton: {
		template: `<button @click="$emit('click')"><slot/></button>`
	},
	swiper: { template: `<div><slot/></div>` }
};

const propsData = {
	steps: [
		{
			title: 'Step 1',
			content: 'step 1 content',
			illustration: 'step-1-illustration'
		},
		{
			title: 'Step 2',
			content: 'step 2 content',
			illustration: 'step-1-illustration'
		},
		{
			title: 'Step 3',
			content: 'step 3 content',
			illustration: 'step-3-illustration'
		}
	]
};

describe('DbetTenantModalSwiper.vue', () => {
	const wrapper = shallowMount(DbetTenantModalSwiper, {
		localVue,
		propsData,
		stubs,
		mocks: { $route: { query: {} } }
	});

	it('should render modal swiper', () => {
		expect(wrapper.find('.dbet-modal-swiper').exists()).toBe(true);
	});

	it('should set activeSlideIndex when swiper slides', () => {
		wrapper.vm.$refs = {
			dbetModalSwiper: {
				swiper: {
					activeIndex: 2
				}
			}
		};
		wrapper.findComponent(stubs.swiper).vm.$emit('slideChange');
		expect(wrapper.vm.activeSlideIndex).toBe(2);

		wrapper.vm.$refs = null;
		wrapper.findComponent(stubs.swiper).vm.$emit('slideChange');
		expect(wrapper.vm.activeSlideIndex).toBe(0);
	});

	it('should set activeSlideIndex when swiper slides', () => {
		wrapper.vm.$refs = {
			dbetModalSwiper: {
				swiper: {
					activeIndex: 2
				}
			}
		};
		wrapper.findComponent(stubs.swiper).vm.$emit('slideChange');
		expect(wrapper.vm.activeSlideIndex).toBe(2);

		wrapper.vm.$refs = null;
		wrapper.findComponent(stubs.swiper).vm.$emit('slideChange');
		expect(wrapper.vm.activeSlideIndex).toBe(0);
	});

	it('should set active index from query params step', async () => {
		const dbetModalSwiper = {
			swiper: {
				activeIndex: 0
			},
			update: jest.fn()
		};
		wrapper.vm.$route.query.step = 2;
		wrapper.vm.$refs = { dbetModalSwiper };
		wrapper.vm.setActiveIndex();
		expect(wrapper.vm.activeSlideIndex).toBe(1);
	});

	it('should emit last-step-next-clicked when next button clicked on last step', () => {
		wrapper.setData({
			activeSlideIndex: wrapper.vm.steps.length - 1
		});
		wrapper.vm.onNextButtonClicked();
		expect(wrapper.emitted('last-step-next-clicked')[0]).toBeTruthy();
	});

	it('should emit close when modal closed', () => {
		wrapper.findComponent(stubs.BgModal).vm.$emit('close');
		expect(wrapper.emitted('close')[0]).toBeTruthy();
	});

	it('should emit first-step-prev-clicked when prev button clicked on first step', async () => {
		await wrapper.setData({ activeSlideIndex: 0 });
		wrapper.find('.dbet-modal-swiper__button-prev').trigger('click');

		expect(wrapper.emitted('first-step-prev-clicked')[0]).toBeTruthy();
	});
});
