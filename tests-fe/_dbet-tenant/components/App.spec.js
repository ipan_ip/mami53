import { createLocalVue, shallowMount } from '@vue/test-utils';
import App from 'Js/_dbet-tenant/components/App.vue';
import Vuex from 'vuex';
import store from 'Js/_dbet-tenant/store/storeData';

const localVue = createLocalVue();
localVue.use(Vuex);

const $route = {
	meta: {
		noBack: false,
		isCloseTypeNav: false
	}
};

const $router = {
	go: jest.fn(),
	replace: jest.fn()
};

const stubs = {
	DbetTenantNavbar: { template: '<nav />' },
	RouterView: { template: '<div />' }
};

global.dbetCode = 'dbet-123';
global.dbetData = { id: 1, user: null };

describe('App.vue', () => {
	const mount = ({ mockStore = null, mockRoute = {} } = {}) =>
		shallowMount(App, {
			localVue,
			stubs,
			mocks: { $router, $route: { ...$route, ...mockRoute } },
			store: new Vuex.Store(mockStore || store)
		});

	const wrapper = mount();

	it('should render dbet tenant', () => {
		expect(wrapper.find('.dbet-tenant').exists()).toBe(true);
	});

	it('should set mobile icon back properly', () => {
		wrapper.vm.$route.meta.isCloseTypeNav = true;
		expect(wrapper.vm.navBackIcon).toBe('close');
	});

	it('should go back when navbar emit back', () => {
		wrapper.findComponent(stubs.DbetTenantNavbar).vm.$emit('back');
		expect($router.go).toBeCalledWith(-1);
	});

	it('should go back to onboarding in form page', () => {
		const mockStore = { ...store };
		mockStore.state.isOnboardingFinished = false;
		const wrapper = mount({
			mockStore,
			mockRoute: { name: 'form', meta: { width: '200px' } }
		});

		wrapper.findComponent(stubs.DbetTenantNavbar).vm.$emit('back');
		expect($router.replace).toBeCalledWith({
			name: 'onboarding',
			query: { step: 'last' }
		});
	});

	it('should not store dbet data if no dbetData', () => {
		jest.clearAllMocks();
		global.dbetData = null;
		const wrapper = mount();

		expect(wrapper.vm.$store.state.dbet.id).toBe(0);
	});
});
