import { createLocalVue, shallowMount } from '@vue/test-utils';
import DbetTenantStickyTitle from 'Js/_dbet-tenant/components/DbetTenantStickyTitle.vue';

const localVue = createLocalVue();

describe('DbetTenantStickyTitle.vue', () => {
	let wrapper;

	beforeEach(() => {
		wrapper = shallowMount(DbetTenantStickyTitle, {
			localVue,
			data() {
				return {
					navigator: {
						isMobile: true
					}
				};
			}
		});
	});

	it('should render dbet sticky title', () => {
		expect(wrapper.find('.dbet-sticky-title').exists()).toBe(true);
	});

	it('should set sticky', () => {
		wrapper.vm.$refs = {
			stickyTitle: {
				$el: {
					getBoundingClientRect: () => {
						return {
							top: 16
						};
					}
				}
			}
		};

		wrapper.vm.setStickyTitle();
		expect(wrapper.vm.isStickOnTop).toBe(true);
	});

	it('should remove sticky when is not on top anymore', () => {
		wrapper.vm.$refs = {
			stickyTitle: {
				$el: {
					getBoundingClientRect: () => {
						return {
							top: 32
						};
					}
				}
			}
		};

		wrapper.vm.setStickyTitle();
		expect(wrapper.vm.isStickOnTop).toBe(false);
	});

	it('should remove scroll listener on before destroy', () => {
		const spy = jest.spyOn(window, 'removeEventListener');
		wrapper.vm.$destroy();
		expect(spy).toBeCalledWith('scroll', expect.any(Function));
		spy.mockRestore();
	});
});
