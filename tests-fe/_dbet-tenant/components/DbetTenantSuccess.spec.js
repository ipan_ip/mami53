import { createLocalVue, shallowMount } from '@vue/test-utils';
import DbetTenantSuccess from 'Js/_dbet-tenant/components/DbetTenantSuccess.vue';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

const localVue = createLocalVue();

const locationReplace = jest.fn();
mockWindowProperty('window.location', { replace: locationReplace });

const stubs = {
	DbetTenantModalSwiper: { template: `<div><slot /></div>` }
};

describe('DbetTenantSuccess.vue', () => {
	const wrapper = shallowMount(DbetTenantSuccess, {
		localVue,
		stubs
	});

	it('should render onboarding component', () => {
		expect(wrapper.find('.dbet-tenant-success').exists()).toBe(true);
	});

	it('should go to kost saya page if next button clicked in last step', () => {
		wrapper
			.findComponent(stubs.DbetTenantModalSwiper)
			.vm.$emit('last-step-next-clicked');

		expect(locationReplace).toBeCalledWith('/user/kost-saya');
	});
});
