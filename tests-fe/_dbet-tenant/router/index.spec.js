import 'tests-fe/utils/mock-vue';
import '../__mocks__/windowVariable';
import { createLocalVue, shallowMount } from '@vue/test-utils';
import router from 'Js/_dbet-tenant/router';
import store from 'Js/_dbet-tenant/store';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

const mockLocation = {
	href: 'https://test.com',
	replace: jest.fn(),
	hostname: 'test'
};

mockWindowProperty('window.location', mockLocation);
mockWindowProperty('window.scrollTo', jest.fn());

const mockComponent = {
	template: `<div class="mock-component"></div>`
};

const localVue = createLocalVue();

describe('_dbet-tenant/router', () => {
	const wrapper = shallowMount(mockComponent, { localVue, router, store });

	it('should render mock component', () => {
		expect(wrapper.find('.mock-component').exists()).toBe(true);
	});

	describe('auth required', () => {
		const wrapper = shallowMount(mockComponent, { localVue, router, store });

		it('should user location replace when user has no auth', async () => {
			wrapper.vm.$store.state.dbet.dbetCodeFormatted = 'dbet-123';
			wrapper.vm.$store.state.authCheck = { owner: false, user: false };

			wrapper.vm.$router.push({ name: 'form' });
			await wrapper.vm.$nextTick();

			expect(mockLocation.replace).toBeCalledWith('/ob/dbet-123/form');
		});

		it('should redirected to owner dashboard when login with owner', async () => {
			wrapper.vm.$store.state.authCheck = { owner: true, user: false };

			wrapper.vm.$router.push({ name: 'form' });
			await wrapper.vm.$nextTick();

			expect(mockLocation.replace).toBeCalledWith(global.oxWebUrl);
		});

		it('should navigated to intended route when logged in with tenant account', async () => {
			wrapper.vm.$store.state.authCheck = { owner: false, user: true };
			wrapper.vm.$router.push({ name: 'form' });
			await wrapper.vm.$nextTick();

			expect(wrapper.vm.$route.name).toBe('form');
		});

		it('should redirected to form page if user try to access biodata page without form page visited first', async () => {
			wrapper.vm.$store.state.isFormVisited = false;
			wrapper.vm.$router.push({ name: 'biodata' });
			await wrapper.vm.$nextTick();

			expect(wrapper.vm.$route.name).toBe('form');
		});

		it('should navigated to onboarding page without auth', async () => {
			wrapper.vm.$router.push({ name: 'onboarding' });
			await wrapper.vm.$nextTick();

			expect(wrapper.vm.$route.name).toBe('onboarding');
		});
	});
});
