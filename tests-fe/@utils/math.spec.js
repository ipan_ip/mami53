import { round } from 'Js/@utils/math.js';

it('should able to truncate decimal based on 2nd parameter', () => {
	expect(round(1.5123434, 2)).toBe(1.51);
	expect(round(10.2, 1)).toBe(10.2);
	expect(round(10.6, 1)).toBe(10.6);
	expect(round(106.81238651275635, 6)).toBe(106.812387);
});

it('should be able to round a number to get rid of decimals', () => {
	expect(round(1.423123123)).toBe(1);
	expect(round(1.6234)).toBe(2);
});

it('should be able to handle negative value', () => {
	expect(round(-6.296188782911225, 6)).toBe(-6.296189);
	expect(round(-1.5123434, 2)).toBe(-1.51);
	expect(round(-10.2, 1)).toBe(-10.2);
	expect(round(-10.6, 1)).toBe(-10.6);
	expect(round(-106.81238651275635, 6)).toBe(-106.812387);
	expect(round(-0.49)).toBe(0);
	expect(round(-0.5)).toBe(0);
	expect(round(-0.6)).toBe(-1);
});
