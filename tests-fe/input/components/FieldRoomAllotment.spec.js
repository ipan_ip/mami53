import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import FieldRoomAllotment from 'Js/input/components/FieldRoomAllotment.vue';

describe('TagText.vue', () => {
	const dataRooms = [
		{
			name: '',
			floor: '',
			available: true,
			number: 0,
			gp_badge: {
				show: true,
				text: 'goldplus'
			}
		}
	];
	const mockStore = {
		state: {
			params: {
				room_available: 0
			},
			room_unit: []
		},
		mutations: {
			setParamsRoomAvailable() {}
		}
	};

	const $toasted = { show: jest.fn() };

	const localVue = createLocalVue();
	localVue.use(Vuex);
	const wrapper = shallowMount(FieldRoomAllotment, {
		localVue,
		mocks: { $toasted },
		store: new Vuex.Store(mockStore),
		propsData: {
			dataRooms,
			roomTotal: 1,
			currentPage: 1,
			limit: 10
		}
	});

	it('trigger inputChanged', () => {
		const input = wrapper.find(
			'.content-room-allotment .item-room-allotment input[type="text"]'
		);
		input.setValue('testing');
		expect(wrapper.vm.dataError).toBeTruthy();
	});

	it('trigger inputCheckboc', () => {
		const input = wrapper.find(
			'.content-room-allotment .item-room-allotment input[type="checkbox"]'
		);
		input.trigger('change');
		expect($toasted.show).toBeCalledWith('Kamar Kosong Bertambah 1', {
			theme: 'toasted-primary',
			position: 'bottom-center',
			containerClass: 'room-allotment-toasted-container',
			duration: 2000
		});
	});

	it('should show goldplus badge', () => {
		expect(wrapper.find('.goldplus-badge').exists()).toBeTruthy();
	});
});
