import { createLocalVue, shallowMount } from '@vue/test-utils';
import mockDebounce from 'tests-fe/utils/mock-debounce';
import mockAxios from 'tests-fe/utils/mock-axios';
import Vuex from 'vuex';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mockComponent from 'tests-fe/utils/mock-component';
import ContainerInputEditApartment from 'Js/input/components/input-edit-apartment/ContainerInputEditApartment.vue';
import mixinScrollTop from 'Js/@mixins/MixinScrollTop';
import mixinMoengageInputProperti from 'Js/@mixins/MixinMoengageInputProperti';

const mockStore = {
	state: {
		extras: {
			fac_tags: {
				unit_type: 'test',
				min_payment: '1',
				unit: 'test unit',
				roomt: 'test room',
				photoExample: 'test.jpg'
			}
		},
		params: {
			project_name: 'test'
		}
	},

	mutations: {
		changeSection: jest.fn(),
		setExtrasFacTags: jest.fn()
	}
};

const stubs = {
	sectionTitle: mockComponent,
	fieldName: mockComponent,
	fieldUnitName: mockComponent,
	fieldUnitNumber: mockComponent,
	fieldType: mockComponent,
	fieldFloor: mockComponent,
	fieldSize: mockComponent,
	fieldPrice: mockComponent,
	fieldPriceExtra: mockComponent,
	fieldFacilityUnit: mockComponent,
	filedFacilityRoom: mockComponent,
	fieldPhotos: mockComponent,
	sectionAlert: mockComponent,
	fieldFacilityRoom: mockComponent
};

describe('ContainerInputEditApartment.vue', () => {
	const localVue = createLocalVue();
	global.axios = mockAxios;
	localVue.use(Vuex);
	global.debounce = mockDebounce;
	window.alert = jest.fn();
	window.bugsnagClient = { notify: jest.fn() };
	window.scrollTo = jest.fn();
	global.$ = jest.fn(() => {
		return {
			on: jest.fn(),
			modal: jest.fn(() => {
				window.open = jest.fn();
			})
		};
	});
	localVue.mixin([mixinScrollTop, mixinMoengageInputProperti]);
	mockVLazy(localVue);

	const mountComponent = () => {
		return shallowMount(ContainerInputEditApartment, {
			localVue,
			stubs,
			store: new Vuex.Store(mockStore)
		});
	};

	let wrapper;

	beforeEach(() => {
		wrapper = mountComponent();
	});

	describe('render ContainerInputEditApartment component correctly', () => {
		it('should render section corrctly', () => {
			expect(wrapper.find('.section-container')).toBeTruthy();
		});
	});

	describe('execute method in ContainerInputEditApartment components ', () => {
		it('should submit data that user are input and call modal when request are succesed', () => {
			axios.mockResolve({
				data: {
					status: true,
					data: {
						dialog_message: 'success'
					}
				}
			});
			wrapper.vm.submitInputData('test');
			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.message.success).toBeTruthy();
				wrapper.vm.submitInputData('test');
			});
		});

		it('should set value message to blank when status false from server ', () => {
			axios.mockResolve({
				data: {
					status: false,
					messages: 'failed'
				}
			});
			wrapper.vm.submitInputData('test');
			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.message.failed).toBeTruthy();
			});
		});
	});
});
