import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import FieldLastEducation from 'Js/input/components/input-vacancy/FieldLastEducation.vue';

const mockStore = {
	state: {
		extras: {
			tags: {
				education_options: 'test'
			}
		},
		params: {
			last_education: 'test'
		},

		seq: 'test seq'
	},
	mutations: {
		setParamsLastEducation: jest.fn()
	}
};

const mocks = {
	errors: {
		has: jest.fn(),
		first: jest.fn()
	}
};

const propsData = {
	label: 'test Label',
	placeholder: 'test placeholder',
	tags: ['test1', 'test2']
};

describe('FieldLastEducation.vue', () => {
	const localVue = createLocalVue();
	localVue.use(Vuex);

	mockVLazy(localVue);

	const mountComponent = () => {
		return shallowMount(FieldLastEducation, {
			localVue,
			mocks,
			propsData,
			store: new Vuex.Store(mockStore)
		});
	};

	let wrapper;

	beforeEach(() => {
		wrapper = mountComponent();
	});

	describe('render component FieldLastEducation correctly', () => {
		it('should render label with content "pilih salah satu"', () => {
			const label = wrapper.find('label');
			expect(label.text()).toBe('test Label *pilih salah satu');
		});
	});
	describe('computed value in FieldLastEducation components', () => {
		it('should return last education data from store', () => {
			expect(wrapper.vm.lastEdu).toBe('test');
		});

		it('should set value in store with value "test" with computed value', () => {
			wrapper.vm.lastEdu = 'test';
			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.lastEdu).toBe('test');
			});
		});

		it('should return education option from store', () => {
			expect(wrapper.vm.educationOptions).toBe('test');
		});

		it('should return seq data from store', () => {
			expect(wrapper.vm.seq).toBe('test seq');
		});
	});
});
