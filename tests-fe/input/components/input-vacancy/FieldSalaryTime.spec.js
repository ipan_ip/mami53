import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import FieldSalaryTime from 'Js/input/components/input-vacancy/FieldSalaryTime.vue';

const mockStore = {
	state: {
		params: {
			jobs_salary_time: 1
		}
	},
	mutations: { setParamsJobsSalaryTime: jest.fn() }
};

const propsData = {
	label: 'test',
	placeholder: 'test placeholder',
	tags: ['test', ' tags']
};

const mocks = {
	errors: {
		has: jest.fn(),
		first: jest.fn()
	}
};

describe('FieldSalaryTime', () => {
	const localVue = createLocalVue();
	localVue.use(Vuex);
	mockVLazy(localVue);

	const mountComponent = () => {
		return shallowMount(FieldSalaryTime, {
			localVue,
			mocks,
			propsData,
			store: new Vuex.Store(mockStore)
		});
	};

	let wrapper;

	beforeEach(() => {
		wrapper = mountComponent();
	});

	describe('should render component FieldSalaryTime correctly', () => {
		it('should render label correctly', () => {
			expect(wrapper.find('label').exists()).toBeTruthy();
		});
	});

	describe('watch variable in component FielSalaryTime', () => {
		it('should set value in store when variable "salaryTime" are changed', () => {
			wrapper.vm.salaryTime = 2;
			expect(wrapper.vm.$store.state.params.jobs_salary_time).toBe(1);
		});
	});
});
