import { createLocalVue, shallowMount } from '@vue/test-utils';
import FilterRoomAllotment from 'Js/input/components/FillterRoomAllotment.vue';

describe('TagText.vue', () => {
	const localVue = createLocalVue();
	const wrapper = shallowMount(FilterRoomAllotment, {
		localVue,
		propsData: {
			roomAll: 15,
			roomAvailable: 5
		}
	});

	it('should render correctly', () => {
		expect(wrapper.vm.roomAll).toBe(15);
		expect(wrapper.vm.roomAvailable).toBe(5);
	});

	it('should click correctly', () => {
		const button = wrapper.find('.filter-room-allotment a');
		button.trigger('click');
		expect(wrapper.emitted('filtered')).toBeTruthy();
	});
});
