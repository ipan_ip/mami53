import { createLocalVue, shallowMount } from '@vue/test-utils';
import BalanceTopUp from 'Js/owner/components/BalanceTopUp.vue';

const localVue = createLocalVue();

describe('BalanceTopUp.vue', () => {
	let wrapper;

	beforeEach(() => {
		wrapper = shallowMount(BalanceTopUp, {
			localVue
		});
	});

	it('Should mounts properly', () => {
		expect(wrapper.find('.topup-container').exists()).toBe(true);
		expect(wrapper).toMatchSnapshot();
	});
});
