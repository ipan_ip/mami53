import { createLocalVue, shallowMount } from '@vue/test-utils';
import ContainerOwnerPage from 'Js/owner/components/ContainerOwnerPage.vue';
import mockComponent from 'tests-fe/utils/mock-component';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import MixinGetQueryString from 'Js/@mixins/MixinGetQueryString';
import OwnerMixinSupportChat from 'Js/owner/mixins/OwnerMixinSupportChat';
import ownerStore from 'tests-fe/owner/components/__mocks__/ownerStore.js';
import { alert } from 'vue-strap';
import jquery from 'jquery';
import Vuex from 'vuex';

const localVue = createLocalVue();
localVue.mixin([MixinGetQueryString, OwnerMixinSupportChat]);
localVue.use(Vuex);

global.alert = jest.fn();
global.zE = jest.fn();
global.$ = jquery;

mockWindowProperty('Cookies', {
	get: jest.fn().mockReturnValue(true),
	set: jest.fn()
});

const stubs = {
	alert,
	OwnerHeader: mockComponent,
	OwnerMenu: mockComponent,
	OwnerBody: mockComponent,
	OwnerModalEmailVerification: mockComponent
};

const store = new Vuex.Store({
	...ownerStore
});

describe('ContainerOwnerPage.vue', () => {
	let wrapper;

	const checkEmailVerificationSpy = jest.spyOn(
		ContainerOwnerPage.methods,
		'checkEmailVerification'
	);

	const addEmailSpy = jest.spyOn(ContainerOwnerPage.methods, 'addEmail');

	const toggleAlertSpy = jest.spyOn(ContainerOwnerPage.methods, 'toggleAlert');

	const openZendeskChatSpy = jest.spyOn(
		OwnerMixinSupportChat.methods,
		'openZendeskChat'
	);

	beforeEach(() => {
		wrapper = shallowMount(ContainerOwnerPage, {
			localVue,
			stubs,
			store,
			methods: {
				getQueryString() {
					return 'true';
				}
			},
			mocks: {
				$route: { name: 'balance_confirmation', path: '/' },
				$router: {
					push: jest.fn()
				}
			}
		});
	});

	it('Should mounts properly', () => {
		expect(wrapper.find('#containerOwnerPage').exists()).toBe(true);
	});

	it('Should return base computed value properly got from storeData', () => {
		expect(wrapper.vm.email).toEqual('bamb@ng.com');
		expect(wrapper.vm.isOnPurchaseDetail).toBeTruthy();
	});

	it('should call all method when the component rendered', async () => {
		await wrapper.vm.$nextTick();
		// created scope
		expect(toggleAlertSpy).toHaveBeenCalled();
		expect(window.currentChatSource).toEqual('owner dashboard');

		// mounted scope
		expect(Cookies.get).toBeCalled();
		expect(checkEmailVerificationSpy).toHaveBeenCalled();
		expect(openZendeskChatSpy).toHaveBeenCalled();
	});

	it('should call addEmail when button triggered', async () => {
		const addEmailButton = wrapper.find('.alert-message');

		expect(addEmailButton.exists()).toBe(true);

		addEmailButton.trigger('click');
		await wrapper.vm.$nextTick();

		expect(addEmailSpy).toHaveBeenCalled();
		expect(wrapper.vm.$router.push).toBeCalled();
	});

	it('should return isAlert value properly', () => {
		wrapper.vm.$store.state.profile.user.email = 'coba@gmail.com';
		wrapper.vm.toggleAlert();

		expect(wrapper.vm.isAlert).toBeFalsy();

		wrapper.vm.$store.state.profile.user.email = '';
		wrapper.vm.toggleAlert();

		expect(wrapper.vm.isAlert).toBeTruthy();
	});

	it('should call Cookies.set when banner is hidden ', async () => {
		global.Cookies.get = jest.fn().mockReturnValue(true);
		expect(global.Cookies.set).toBeCalled();
	});

	it('should toggle openModal when window location href contain needed path', async () => {
		mockWindowProperty('window.location', {
			host: 'mamikos.com',
			pathname: '/test?email_verification=success',
			href: 'mamikos.com/test?email_verification=success'
		});

		wrapper.vm.$refs = {
			ownerModalEmailVerification: {
				openModal: jest.fn()
			}
		};
		await wrapper.vm.checkEmailVerification();
		await wrapper.vm.$nextTick();

		expect(wrapper.vm.$refs.ownerModalEmailVerification.openModal).toBeCalled();
	});
});
