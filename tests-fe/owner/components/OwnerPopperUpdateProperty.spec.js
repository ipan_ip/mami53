import { createLocalVue, shallowMount } from '@vue/test-utils';
import '@babel/polyfill';
import 'tests-fe/utils/mock-vue';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import OwnerPopperUpdateProperty from 'Js/owner/components/OwnerPopperUpdateProperty.vue';

window.Vue = require('vue');
window.Vue.config.silent = true;

const MixinNavigatorIsMobile = require('Js/@mixins/MixinNavigatorIsMobile');

const localVue = createLocalVue();
localVue.mixin(MixinNavigatorIsMobile);

const generateWrapper = () => {
	return shallowMount(OwnerPopperUpdateProperty, {
		localVue,
		propsData: {
			description: 'mamikos'
		}
	});
};

describe('OwnerSettings.vue', () => {
	it('Should mounts properly', () => {
		const wrapper = generateWrapper();
		expect(wrapper.find('.popper-wrapper').exists()).toBe(true);
	});

	it('should set trigger to click if device is Mobile', () => {
		mockWindowProperty('navigator.isMobile', true);

		const wrapper = generateWrapper();

		expect(wrapper.vm.trigger).toBe('click');
	});

	it('should set trigger to hover if device is not Mobile', () => {
		mockWindowProperty('navigator.isMobile', false);

		const wrapper = generateWrapper();

		expect(wrapper.vm.trigger).toBe('hover');
	});

	it('should close popper when hide method is called', async () => {
		const wrapper = generateWrapper();

		wrapper.vm.$refs = {
			popper: {
				doClose: jest.fn()
			}
		};

		await wrapper.vm.$nextTick();

		const spyDoClose = jest.spyOn(wrapper.vm.$refs.popper, 'doClose');

		wrapper.vm.hide();
		expect(spyDoClose).toBeCalled();
	});
});
