import { createLocalVue, shallowMount } from '@vue/test-utils';
import OwnerActiveData from './__mocks__/owner-active-data.json';
import MamiLoadingInline from 'Js/@components/MamiLoadingInline';
import OwnerActive from 'Js/owner/components/OwnerActive.vue';

global.axios = {
	get: jest.fn().mockResolvedValue({ status: true, data: OwnerActiveData })
};
global.bugsnagClient = { notify: jest.fn() };
global.alert = jest.fn();

const localVue = createLocalVue();

const stubs = {
	MamiLoadingInline
};

const generateWrapper = () => {
	return shallowMount(OwnerActive, {
		localVue,
		stubs
	});
};

describe('OwnerActive.vue', () => {
	const wrapper = generateWrapper();

	it('Should mounts properly', () => {
		expect(wrapper.find('.stretch').exists()).toBe(true);
	});

	it('should render active owner data', async () => {
		await wrapper.vm.$nextTick();
		expect(wrapper.vm.active).not.toBe({});
	});

	it('should call alert when alertPremium method called', async () => {
		wrapper.vm.alertPremium();
		expect(alert).toBeCalledWith(
			'Saat ini Premium Membership hanya berlaku untuk data iklan kost dan unita apartemen.'
		);
	});

	it('should call bugsnag if catch an error from fetching active owner data', async () => {
		global.axios = { get: jest.fn().mockRejectedValue(new Error('error')) };
		const wrapper = generateWrapper();
		await wrapper.vm.$nextTick();
		expect(alert).toBeCalledWith('Terjadi galat. Silakan coba lagi.');
		expect(bugsnagClient.notify).toBeCalled();
	});
});
