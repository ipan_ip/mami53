import { createLocalVue, shallowMount } from '@vue/test-utils';
import OwnerPropertyUpdate from 'Js/owner/components/OwnerPropertyUpdate.vue';
import mockComponent from 'tests-fe/utils/mock-component';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import ownerStore from 'tests-fe/owner/components/__mocks__/ownerStore.js';
import vSelect from 'vue-select';
import jquery from 'jquery';
import mockAxios from 'tests-fe/utils/mock-axios';
import Vuex from 'vuex';

const localVue = createLocalVue();
localVue.use(Vuex);

global.alert = jest.fn();
global.$ = jquery;
global.axios = mockAxios;
global.window.scroll = jest.fn();

mockWindowProperty('bugsnagClient', {
	notify: jest.fn()
});

const stubs = {
	vSelect,
	MamiLoadingInline: mockComponent,
	OwnerDetailBack: mockComponent,
	OwnerPropertyUpdateList: mockComponent
};

const store = new Vuex.Store({
	...ownerStore
});

const propsData = {
	propertyType: 'apartment'
};

describe('OwnerPropertyUpdate.vue', () => {
	let wrapper;

	const getRoomListApiSpy = jest.spyOn(
		OwnerPropertyUpdate.methods,
		'getRoomListApi'
	);
	const clearRoomListSpy = jest.spyOn(
		OwnerPropertyUpdate.methods,
		'clearRoomList'
	);
	const toTopSpy = jest.spyOn(OwnerPropertyUpdate.methods, 'toTop');

	beforeEach(() => {
		wrapper = shallowMount(OwnerPropertyUpdate, {
			localVue,
			stubs,
			store,
			propsData,
			mocks: {
				$router: {
					push: jest.fn()
				}
			}
		});
	});

	it('Should mounts properly', () => {
		expect(wrapper.find('.owner-property-update-container').exists()).toBe(
			true
		);
	});

	it('Should return base computed value properly got from storeData', () => {
		expect(wrapper.vm.selectedRoom).toBe(0);
	});

	it('should call all method when the component rendered', () => {
		expect(getRoomListApiSpy).toHaveBeenCalled();
	});

	it('Watch roomSuggestions to call related method correctly', async () => {
		await wrapper.setData({
			roomSuggestions: 'mock suggestion'
		});

		expect(clearRoomListSpy).toHaveBeenCalled();
		expect(getRoomListApiSpy).toHaveBeenCalled();
	});

	it('should return url clearly when calling generateUrlApi method', async () => {
		const roomId = 2;

		expect(wrapper.vm.generateUrlApi(roomId)).toBe(
			'/owner/data/list/update?limit=2&for=apartment&offset=0&room_id=2'
		);

		await wrapper.setData({
			propertyType: 'kos'
		});

		expect(wrapper.vm.generateUrlApi(roomId)).toBe(
			'/owner/data/list/update?limit=2&offset=0&room_id=2'
		);
	});

	it('should call getMoreRoomList method properly', async () => {
		await wrapper.setData({
			hasMore: true
		});
		const previousOffset = wrapper.vm.offset;
		wrapper.vm.getMoreRoomList();

		expect(wrapper.vm.busy).toBeTruthy();
		expect(wrapper.vm.offset).toBe(previousOffset + wrapper.vm.limit);
		expect(getRoomListApiSpy).toHaveBeenCalled();
	});

	it('should clear offset and room list when calling clearRoomList method', () => {
		wrapper.vm.clearRoomList();

		expect(wrapper.vm.offset).toBe(0);
		expect(wrapper.vm.rooms.length).toBe(0);
	});

	it('should return roomListSuggestions when calling createSearchOptionsSuggestions method', () => {
		const mockResponse = [
			{
				name: 'mock name 1',
				id: 1
			},
			{
				name: 'mock name 2',
				id: 2
			}
		];
		wrapper.vm.createSearchOptionsSuggestions(mockResponse);

		expect(wrapper.vm.createSearchOptionsSuggestions(mockResponse)).not.toBe(
			[]
		);
	});

	it('should scroll window when calling toTop method', done => {
		wrapper.vm.toTop();
		setTimeout(() => {
			expect(window.scroll).toBeCalledWith({
				behavior: 'smooth',
				top: 0
			});
			done();
		}, 300);
	});

	it('should call swal and window.reload when calling updatePassword method on successful response', async () => {
		const roomId = 2;
		axios.mockResolve({
			data: {
				status: true,
				'has-more': true,
				rooms: [
					{
						is_apartment: true
					}
				]
			}
		});

		await wrapper.vm.getRoomListApi(roomId);

		expect(wrapper.vm.hasMore).toBeTruthy();
		expect(wrapper.vm.busy).toBeFalsy();
		expect(toTopSpy).toHaveBeenCalled();
	});
});
