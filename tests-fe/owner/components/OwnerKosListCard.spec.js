import 'tests-fe/utils/mock-vue';
import mockAxios from 'tests-fe/utils/mock-axios';
import { createLocalVue, shallowMount } from '@vue/test-utils';

import mockComponent from 'tests-fe/utils/mock-component';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

import OwnerKosListCard from 'Js/owner/components/OwnerKosListCard.vue';
import dataKos from './__mocks__/dataKos.json';

window.swal = jest.fn().mockResolvedValue(true);
window.axios = mockAxios;
window.bugsnagClient = { notify: jest.fn() };

describe('OwnerKosListCard.vue', () => {
	window.oxWebUrl = 'http://mamikos.com';
	const localVue = createLocalVue();

	mockWindowProperty('navigator', {
		isMobile: false
	});
	mockWindowProperty('alert', jest.fn());

	// mock stubbed component
	const stubs = {
		alert: mockComponent
	};

	const $router = {
		push: jest.fn()
	};

	const propsData = {
		kos: dataKos
	};

	const $store = {
		state: {
			profile: { membership: { status: 'Konfirmasi Pembayaran' } },
			invoice: { status: 'Konfirmasi Pembayaran' }
		},
		getters: {
			invoiceUrl: 'https://gaplek3.pay.kerupux.com/'
		}
	};

	// mount function
	const mount = () => {
		return shallowMount(OwnerKosListCard, {
			localVue,
			stubs,
			propsData,
			mocks: {
				$router,
				$store,
				openSwalLoading: jest.fn(),
				closeSwalLoading: jest.fn()
			}
		});
	};
	const wrapper = mount();

	describe('render component correctly', () => {
		it('should render container properly', () => {
			expect(wrapper.find('.kos-card').exists()).toBeTruthy();
			expect(wrapper.vm.isMobile).toBeFalsy();
		});

		it('should show status label according to kos status', () => {
			// kos hidden
			wrapper.setProps({ kos: { ...dataKos, hidden: true } });
			expect(wrapper.vm.kosStatusComputed.isRejectedStatus).toBe(true);
			expect(wrapper.vm.showKosDetail).toBe(false);

			// kos unverified
			wrapper.setProps({
				kos: { ...dataKos, status_kos: 'unverified', hidden: false }
			});
			expect(wrapper.vm.kos.status_kos).toBe('unverified');
			expect(wrapper.vm.kosStatusComputed.isRejectedStatus).toBe(true);
			expect(wrapper.vm.kosStatusComputed.text).toBe('Data Kos Ditolak');

			// kos incomplete
			wrapper.setProps({
				kos: { ...dataKos, status_kos: 'incomplete' }
			});
			expect(wrapper.vm.kos.status_kos).toBe('incomplete');
			expect(wrapper.vm.kosStatusComputed.isRejectedStatus).toBe(true);
			expect(wrapper.vm.kosStatusComputed.text).toBe('Draft');

			// kos add
			wrapper.setProps({
				kos: { ...dataKos, status_kos: 'add', bbk_request_status: null }
			});
			expect(wrapper.vm.kos.status_kos).toBe('add');
			expect(wrapper.vm.kosStatusComputed.isRejectedStatus).toBe(true);
			expect(wrapper.vm.kosStatusComputed.text).toBe('Lengkapi Data Pribadi');
		});

		it('should show gender label according to data kos', () => {
			wrapper.setProps({
				kos: { ...dataKos, gender: 2 }
			});
			expect(wrapper.vm.gender.text).toBe('Kos Putri');

			wrapper.setProps({
				kos: { ...dataKos, gender: 0 }
			});
			expect(wrapper.vm.gender.text).toBe('Kos Campur');

			wrapper.setProps({
				kos: { ...dataKos, gender: null }
			});
			expect(wrapper.vm.gender.text).toBe('');
		});
	});

	describe('has action properly', () => {
		it('should open detail info when button is pressed', () => {
			const expandableButton = wrapper.find('.kos-card__expandable');
			expandableButton.trigger('click');
			expect(expandableButton.text()).toBe('Lihat Selengkapnya');
			expect(wrapper.vm.isExpanded).toBe(true);
		});

		it('should go to edit price and room page', () => {
			wrapper.setProps({
				kos: { ...dataKos, status_kos: 'edited' }
			});
			wrapper.find('.detail-kos__update-price').trigger('click');
		});

		it('should handle deleting kos', async () => {
			wrapper.setProps({
				kos: { ...dataKos, status_kos: 'unverified' }
			});
			window.axios.mockResolve({ data: { status: true } });
			window.location.reload = jest.fn();

			// Success state
			expect(wrapper.vm.kosStatusComputed.isRejectedStatus).toBe(true);
			await wrapper.vm.$nextTick;

			const deleteButton = wrapper.find('.kos-card__rejected-actions button');
			deleteButton.trigger('click');
			expect(window.swal).toBeCalled();
			window.swal.mockClear();

			await window.swal;
			expect(wrapper.vm.openSwalLoading).toBeCalled();
			await window.axios.delete;
			expect(wrapper.vm.closeSwalLoading).toBeCalled();
			expect(window.location.reload).toBeCalled();

			wrapper.vm.openSwalLoading.mockClear();
			wrapper.vm.closeSwalLoading.mockClear();

			// Error state
			window.axios.mockReject('error');
			deleteButton.trigger('click');

			await window.swal;
			expect(wrapper.vm.openSwalLoading).toBeCalled();
			await window.axios.delete;
			await Promise.prototype.then;
			expect(window.bugsnagClient.notify).toBeCalled();

			window.bugsnagClient.notify.mockClear();
			wrapper.vm.openSwalLoading.mockClear();
			wrapper.vm.closeSwalLoading.mockClear();
			window.location.reload.mockRestore();
		});

		it('should handle clicking on premium', async () => {
			wrapper.setProps({
				kos: dataKos
			});
			await wrapper.vm.$nextTick;
			const expandableButton = wrapper.find('.kos-card__expandable');
			expandableButton.trigger('click');

			wrapper.find('.detail-kos__manage-premium-link').trigger('click');
			expect(window.swal).toBeCalled();
		});

		it('should get statistic by selected id', async () => {
			// Success State
			window.axios.mockResolve({ data: { status: true } });
			const expandableButton = wrapper.find('.kos-card__expandable');
			expandableButton.trigger('click');
			await wrapper.vm.$nextTick;
			wrapper.find('.statistic__choice option').setSelected();
			await wrapper.vm.$nextTick;
			expect(wrapper.vm.openSwalLoading).toBeCalled();
			await window.axios.get;
			expect(wrapper.vm.closeSwalLoading).toBeCalled();
			wrapper.vm.closeSwalLoading.mockClear();
			wrapper.vm.openSwalLoading.mockClear();

			// Error State
			window.axios.mockReject('error');
			wrapper.find('.statistic__choice option:nth-child(2)').setSelected();
			await wrapper.vm.$nextTick;
			expect(wrapper.vm.openSwalLoading).toBeCalled();
			await window.axios.delete;
			await Promise.prototype.then;
			expect(wrapper.vm.closeSwalLoading).toBeCalled();
			expect(window.bugsnagClient.notify).toBeCalled();
			window.bugsnagClient.notify.mockClear();
			wrapper.vm.closeSwalLoading.mockClear();
			wrapper.vm.openSwalLoading.mockClear();
		});
	});
});
