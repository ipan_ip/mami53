import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import mockAxios from 'tests-fe/utils/mock-axios';

import OwnerModalSurveyRoom from 'Js/owner/components/OwnerModalSurveyRoom.vue';

describe('OwnerModalSurveyRoom.vue', () => {
	const localVue = createLocalVue();
	localVue.use(Vuex);

	mockWindowProperty(
		'$',
		jest.fn().mockReturnValue({
			modal: jest.fn(),
			on: jest.fn((eventName, cb) => cb && cb())
		})
	);

	global.axios = mockAxios;
	global.bugsnagClient = {
		notify: jest.fn()
	};
	global.alert = jest.fn();

	const mocks = {
		$emit: jest.fn()
	};

	const methods = {
		openSwalLoading: jest.fn(),
		closeSwalLoading: jest.fn(),
		swalSuccessTimer: jest.fn(),
		swalError: jest.fn()
	};

	const storeData = {
		state: {
			token: 'token'
		}
	};

	const propsData = {
		el: 'ownerModalSurveyRoom',
		paramId: 2,
		dataFor: '',
		hasQueryString: true
	};

	// mount function
	const mount = () => {
		return shallowMount(OwnerModalSurveyRoom, {
			localVue,
			mocks,
			propsData,
			methods,
			store: new Vuex.Store(storeData)
		});
	};

	describe('render component correctly', () => {
		let wrapper;

		beforeEach(() => {
			wrapper = mount();
		});

		it('should render container properly', () => {
			expect(wrapper.find('#ownerModalSurveyRoom').exists()).toBeTruthy();
		});
	});

	describe('render computed data properly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = mount();
		});

		it('should return token from $store', () => {
			expect(wrapper.vm.token).toBe('token');
		});

		it('should return postUrl value properly', () => {
			// default: hasQueryString is true
			expect(wrapper.vm.postUrl).toBe(
				'/owner/data/survey/updateroom?premium_survey=true'
			);

			// alt: hasQueryString is true
			wrapper.setProps({
				hasQueryString: false
			});
			expect(wrapper.vm.postUrl).toBe('/owner/data/survey/updateroom');
		});
	});

	describe('call functions in the watch block correctly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = mount();
		});

		it('should call $emit on isClosed value change, and the value is truthy ', async () => {
			const emitSpy = jest.spyOn(wrapper.vm, '$emit');
			await wrapper.setData({
				isClosed: false
			});
			await wrapper.setData({
				isClosed: true
			});

			expect(emitSpy).toBeCalled();
		});

		it('should call $emit on isClosed value change, and the value is falsy', async () => {
			const emitSpy = jest.spyOn(wrapper.vm, '$emit');
			await wrapper.setData({
				isClosed: true
			});
			await wrapper.setData({
				isClosed: false
			});

			expect(emitSpy).toBeCalled();
		});
	});

	describe('call functions correctly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = mount();
		});

		it('should call $() when calling openModal method', async () => {
			await wrapper.vm.openModal();
			expect(global.$).toBeCalled();
		});

		it('should call $() when calling closeModal method', async () => {
			await wrapper.vm.closeModal();
			expect(global.$).toBeCalled();
		});

		it('should do nothing when calling submitSurvey method and the count was  an empty string', async () => {
			wrapper.setData({
				count: ''
			});
			const openSwalLoadingSpy = jest.spyOn(wrapper.vm, 'openSwalLoading');
			await wrapper.vm.submitSurvey();
			await wrapper.vm.$nextTick();

			expect(openSwalLoadingSpy).not.toBeCalled();
			openSwalLoadingSpy.mockRestore();
		});

		it('should set isChanged value and call swalSuccessTimer when calling submitSurvey method on successful api request, and the count was not empty', async () => {
			wrapper.setData({
				count: 10
			});
			const swalSuccessTimerSpy = jest.spyOn(wrapper.vm, 'swalSuccessTimer');
			await axios.mockResolve({
				data: {
					status: true
				}
			});
			await wrapper.vm.submitSurvey();

			expect(swalSuccessTimerSpy).toBeCalled();
			expect(wrapper.vm.isChanged).toBeTruthy();

			swalSuccessTimerSpy.mockRestore();
		});

		it('should call swalError when calling submitSurvey method on successful api request, but falsy status, and the count was not empty', async () => {
			wrapper.setData({
				count: 10
			});
			const swalErrorSpy = jest.spyOn(wrapper.vm, 'swalError');
			await axios.mockResolve({
				data: {
					status: false,
					meta: {
						message: 'test'
					}
				}
			});
			await wrapper.vm.submitSurvey();

			expect(swalErrorSpy).toBeCalled();
			swalErrorSpy.mockRestore();
		});

		it('should call bugsnag, alert, and closeSwalLoading when calling submitSurvey method on failed api request, and the count was not empty', async () => {
			wrapper.setData({
				count: 10
			});
			const closeSwalLoadingSpy = jest.spyOn(wrapper.vm, 'closeSwalLoading');
			await axios.mockReject('error');
			await wrapper.vm.submitSurvey();
			await wrapper.vm.$nextTick();

			expect(closeSwalLoadingSpy).toBeCalled();
			expect(global.alert).toBeCalled();
			expect(global.bugsnagClient.notify).toBeCalled();

			closeSwalLoadingSpy.mockRestore();
		});
	});
});
