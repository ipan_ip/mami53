import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import mockAxios from 'tests-fe/utils/mock-axios';

import OwnerModalSurveyPremium from 'Js/owner/components/OwnerModalSurveyPremium.vue';

describe('OwnerModalSurveyPremium.vue', () => {
	const localVue = createLocalVue();
	localVue.use(Vuex);

	mockWindowProperty(
		'$',
		jest.fn().mockReturnValue({
			modal: jest.fn(),
			on: jest.fn((eventName, cb) => cb && cb())
		})
	);

	global.axios = mockAxios;
	global.bugsnagClient = {
		notify: jest.fn()
	};
	global.alert = jest.fn();

	const mocks = {
		$emit: jest.fn()
	};

	const methods = {
		openSwalLoading: jest.fn(),
		closeSwalLoading: jest.fn(),
		swalSuccessTimer: jest.fn(),
		swalError: jest.fn()
	};

	const storeData = {
		state: {
			token: 'token'
		}
	};

	const propsData = {
		packageName: 'packageName',
		packageId: 0,
		surveys: [
			'Kamar masih terisi penuh',
			'Harga dan keuntungan tidak sesuai',
			'Lainnya'
		]
	};

	// mount function
	const mount = () => {
		return shallowMount(OwnerModalSurveyPremium, {
			localVue,
			mocks,
			propsData,
			methods,
			store: new Vuex.Store(storeData)
		});
	};

	describe('render component correctly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = mount();
		});

		it('should render container properly', () => {
			expect(wrapper.find('#ownerModalSurveyPremium').exists()).toBeTruthy();
		});
	});

	describe('render computed data properly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = mount();
		});

		it('should return token from $store', () => {
			expect(wrapper.vm.token).toBe('token');
		});
	});

	describe('call functions in the watch block correctly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = mount();
		});

		it('should call $emit on isClosed value change, and the value is truthy ', async () => {
			const emitSpy = jest.spyOn(wrapper.vm, '$emit');
			await wrapper.setData({
				isClosed: false
			});
			await wrapper.setData({
				isClosed: true
			});

			expect(emitSpy).toBeCalled();
		});

		it('should call $emit on isClosed value change, and the value is falsy', async () => {
			const emitSpy = jest.spyOn(wrapper.vm, '$emit');
			await wrapper.setData({
				isClosed: true
			});
			await wrapper.setData({
				isClosed: false
			});

			expect(emitSpy).toBeCalled();
		});

		it('should set isRoomCountShow as true on reason value change, and the reason includes "Kamar masih terisi penuh"', async () => {
			await wrapper.setData({
				reason: []
			});
			await wrapper.setData({
				reason: ['Kamar masih terisi penuh']
			});
			expect(wrapper.vm.isRoomCountShow).toBeTruthy();
		});

		it('should set isRoomCountShow as false on reason value change, and the reason not included "Kamar masih terisi penuh"', async () => {
			await wrapper.setData({
				reason: ['Kamar masih terisi penuh']
			});
			await wrapper.setData({
				reason: []
			});
			expect(wrapper.vm.isRoomCountShow).toBeFalsy();
		});

		it('should set isDesiredPriceShow as true on reason value change, and the reason includes "Harga dan keuntungan tidak sesuai"', async () => {
			await wrapper.setData({
				reason: []
			});
			await wrapper.setData({
				reason: ['Harga dan keuntungan tidak sesuai']
			});
			expect(wrapper.vm.isDesiredPriceShow).toBeTruthy();
		});

		it('should set isDesiredPriceShow as false on reason value change, and the reason not included "Kamar masih terisi penuh"', async () => {
			await wrapper.setData({
				reason: ['Harga dan keuntungan tidak sesuai']
			});
			await wrapper.setData({
				reason: []
			});
			expect(wrapper.vm.isDesiredPriceShow).toBeFalsy();
		});
	});

	describe('call functions correctly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = mount();
		});

		it('should call $() when calling openModal method', async () => {
			await wrapper.vm.openModal();
			expect(global.$).toBeCalled();
		});

		it('should call $() when calling closeModal method', async () => {
			await wrapper.vm.closeModal();
			expect(global.$).toBeCalled();
		});

		it('should do nothing when calling submitSurvey method and the reason was  empty', async () => {
			wrapper.setData({
				reason: []
			});
			const openSwalLoadingSpy = jest.spyOn(wrapper.vm, 'openSwalLoading');
			await wrapper.vm.submitSurvey();
			await wrapper.vm.$nextTick();

			expect(openSwalLoadingSpy).not.toBeCalled();
			openSwalLoadingSpy.mockRestore();
		});

		it('should set isChanged value and call swalSuccessTimer when calling submitSurvey method on successful api request, and the reason was not empty', async () => {
			wrapper.setData({
				reason: ['test']
			});
			const swalSuccessTimerSpy = jest.spyOn(wrapper.vm, 'swalSuccessTimer');
			await axios.mockResolve({
				data: {
					status: true
				}
			});
			await wrapper.vm.submitSurvey();

			expect(swalSuccessTimerSpy).toBeCalled();
			expect(wrapper.vm.isChanged).toBeTruthy();

			swalSuccessTimerSpy.mockRestore();
		});

		it('should call swalError when calling submitSurvey method on successful api request, but falsy status, and the reason was not empty', async () => {
			wrapper.setData({
				reason: ['test']
			});
			const swalErrorSpy = jest.spyOn(wrapper.vm, 'swalError');
			await axios.mockResolve({
				data: {
					status: false,
					meta: {
						message: 'test'
					}
				}
			});
			await wrapper.vm.submitSurvey();

			expect(swalErrorSpy).toBeCalled();
			swalErrorSpy.mockRestore();
		});

		it('should call bugsnag, alert, and closeSwalLoading when calling submitSurvey method on failed api request, and the reason was not empty', async () => {
			wrapper.setData({
				reason: ['test']
			});
			const closeSwalLoadingSpy = jest.spyOn(wrapper.vm, 'closeSwalLoading');
			await axios.mockReject('error');
			await wrapper.vm.submitSurvey();
			await wrapper.vm.$nextTick();

			expect(closeSwalLoadingSpy).toBeCalled();
			expect(global.alert).toBeCalled();
			expect(global.bugsnagClient.notify).toBeCalled();

			closeSwalLoadingSpy.mockRestore();
		});
	});
});
