import { createLocalVue, shallowMount } from '@vue/test-utils';
import OwnerPhoneNumberOTPModal from 'Js/owner/components/settings/OwnerPhoneNumberOTPModal.vue';
import mockComponent from 'tests-fe/utils/mock-component';
import { makeAPICall } from 'Js/@utils/makeAPICall.js';

const localVue = createLocalVue();

const stubs = {
	BgModal: mockComponent,
	OtpInputWrapper: mockComponent
};
const propsData = {
	isShown: false,
	contactValue: '0812435464'
};
const mocks = {
	swalError: jest.fn()
};

global.tracker = jest.fn();

jest.mock('Js/@utils/makeAPICall.js');
jest.useFakeTimers();

describe('OwnerPhoneNumberOTPModal.vue', () => {
	const startResendCountdownSpy = jest.spyOn(
		OwnerPhoneNumberOTPModal.methods,
		'startResendCountdown'
	);
	let wrapper;
	const createComponent = options => {
		wrapper = shallowMount(OwnerPhoneNumberOTPModal, {
			localVue,
			stubs,
			propsData,
			mocks,
			...options
		});
	};

	it('Should mounts properly', () => {
		createComponent();
		expect(wrapper.find('.owner-phone-number-otp-modal').exists()).toBe(true);
	});

	it('Should start resend countdown on modal shown', async () => {
		createComponent();

		wrapper.setProps({ isShown: true });

		await wrapper.vm.$nextTick();

		expect(startResendCountdownSpy).toBeCalled();
	});

	it('Should handle resend OTP properly', async () => {
		createComponent();

		wrapper.vm.handleResendOTP();

		await wrapper.vm.$nextTick();

		expect(wrapper.emitted('resend-code')).toBeTruthy();
	});

	it('Should handle verify OTP properly', async () => {
		createComponent();

		makeAPICall.mockReturnValueOnce({
			status: 200,
			data: true
		});

		const mockValue = '9999';

		wrapper.vm.handleVerifyOTP(mockValue);

		await wrapper.vm.$nextTick();

		expect(wrapper.emitted('verified')).toBeTruthy();
	});

	it('Should set OTP status to invalid based on API', async () => {
		createComponent();

		makeAPICall.mockReturnValueOnce({
			status: false
		});

		const mockValue = '9999';

		wrapper.vm.handleVerifyOTP(mockValue);

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.isOTPInvalid).toBe(true);
	});

	it('Should handle resend countown timer properly', async () => {
		const clearIntervalSpy = jest.spyOn(window, 'clearInterval');

		window.setInterval = jest.fn();
		wrapper.vm.startResendCountdown();

		// to simulate 1s of remaining time
		wrapper.setData({ resendCounter: 1 });

		// run first argument of first call, to simulate reaching 0 second
		window.setInterval.mock.calls[0][0]();

		expect(window.setInterval).toBeCalled();
		expect(clearIntervalSpy).toBeCalled();
	});
});
