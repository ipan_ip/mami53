import { createLocalVue, shallowMount } from '@vue/test-utils';
import OwnerPersonalInfoForm from 'Js/owner/components/settings/OwnerPersonalInfoForm.vue';
import VeeValidate, { Validator } from 'vee-validate';
import id from 'vee-validate/dist/locale/id';
import mockComponent from 'tests-fe/utils/mock-component';
import md5 from 'md5';

const localVue = createLocalVue();

global.md5 = md5;
window.validator = new Validator();
Validator.localize('id', id);

localVue.use(VeeValidate, {
	locale: 'id'
});

const stubs = {
	BgField: mockComponent,
	BgInput: mockComponent,
	BgInputPassword: mockComponent,
	BgButton: mockComponent
};
const propsData = {
	itemKey: '',
	itemValue: '',
	itemPlaceholder: '',
	itemLabel: ''
};

describe('OwnerPersonalInfoForm.vue', () => {
	let wrapper;

	beforeEach(() => {
		wrapper = shallowMount(OwnerPersonalInfoForm, {
			localVue,
			propsData,
			stubs
		});
	});

	it('Should mounts properly', () => {
		expect(wrapper.find('.owner-personal-info-form').exists()).toBe(true);
	});

	it('Should return proper field label', () => {
		const testCases = [
			{
				key: 'password',
				label: 'Password Baru'
			},
			{
				key: 'not_password',
				label: `Masukkan ${wrapper.vm.itemLabel}`
			}
		];

		testCases.forEach(tc => {
			wrapper.setProps({ itemKey: tc.key });
			expect(wrapper.vm.fieldLabel).toBe(tc.label);
		});
	});

	it('Should handle submit properly', () => {
		const testCases = [
			{
				key: 'password',
				payload: {
					old_password: md5(wrapper.vm.oldPasswordValue),
					new_password: md5(wrapper.vm.newValue)
				}
			},
			{
				key: 'not_password',
				payload: { not_password: wrapper.vm.newValue }
			}
		];

		testCases.forEach(async tc => {
			wrapper.setProps({ itemKey: tc.key });
			wrapper.vm.handleSubmit();

			await wrapper.vm.$nextTick();

			expect(wrapper.emitted('submit')).toBeTruthy();
		});
	});
});
