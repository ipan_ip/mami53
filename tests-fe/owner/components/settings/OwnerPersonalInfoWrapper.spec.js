import { createLocalVue, shallowMount } from '@vue/test-utils';
import OwnerPersonalInfoWrapper from 'Js/owner/components/settings/OwnerPersonalInfoWrapper.vue';
import trackerMixin from 'Js/owner/mixins/OwnerMixinSettingsTrackers';
import ownerStore from 'tests-fe/owner/components/__mocks__/ownerStore.js';
import mockComponent from 'tests-fe/utils/mock-component';
import Vuex from 'vuex';
import { makeAPICall } from 'Js/@utils/makeAPICall.js';

const localVue = createLocalVue();

localVue.use(Vuex);
localVue.mixin([trackerMixin]);

const stubs = {
	BgGrid: mockComponent,
	BgGridItem: mockComponent,
	BgText: mockComponent,
	BgLink: mockComponent,
	EditForm: mockComponent
};
const mocks = {
	openSwalLoading: jest.fn(),
	closeSwalLoading: jest.fn(),
	swalError: jest.fn()
};

global.tracker = jest.fn();

jest.mock('Js/@utils/makeAPICall.js');

describe('OwnerPersonalInfoWrapper.vue', () => {
	const onUpdateInfoSucceededSpy = jest.spyOn(
		OwnerPersonalInfoWrapper.methods,
		'onUpdateInfoSucceeded'
	);
	let wrapper;

	beforeEach(() => {
		wrapper = shallowMount(OwnerPersonalInfoWrapper, {
			localVue,
			store: new Vuex.Store(ownerStore),
			stubs,
			mocks
		});
	});

	it('Should mounts properly', () => {
		expect(wrapper.find('.owner-personal-info-wrapper').exists()).toBe(true);
	});

	it('Should handle edit form toggling properly', () => {
		const mockKey = 'mock_key';

		wrapper.vm.toggleEditForm(mockKey);
		expect(wrapper.vm.shownEditForms).toEqual([mockKey]);
		wrapper.vm.toggleEditForm(mockKey);
		expect(wrapper.vm.shownEditForms).toEqual([]);
	});

	it('Should handle update setting properly', async () => {
		makeAPICall.mockReturnValueOnce({
			status: 200,
			data: true
		});

		const mockData = {
			key: 'name',
			payload: { name: 'Naburo Uzumaki' }
		};

		wrapper.vm.handleUpdateInfo(mockData);

		await wrapper.vm.$nextTick();

		expect(onUpdateInfoSucceededSpy).toBeCalled();
	});

	it('Should show OTP modal on phone changes request', async () => {
		makeAPICall.mockReturnValueOnce({
			status: 200,
			data: true
		});

		const mockData = {
			key: 'phone_number',
			payload: { phone_number: '081223546478' }
		};

		wrapper.vm.handleUpdateInfo(mockData);

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.isPhoneOTPModalShown).toBe(true);
	});

	it('Should handle unknown error on update setting properly', async () => {
		makeAPICall.mockImplementation(() => false);

		const mockData = {
			key: 'name',
			payload: { name: 'Naburo Uzumaki' }
		};

		wrapper.vm.handleUpdateInfo(mockData);

		await wrapper.vm.$nextTick();

		expect(mocks.swalError).toBeCalled();
	});

	it('Should emit success flag properly for email data', async () => {
		const mockData = {
			key: 'email',
			label: 'Email',
			config: {
				needReload: false,
				message: 'Mohon cek email yang baru Anda masukkan untuk verifikasi.'
			}
		};

		wrapper.vm.onUpdateInfoSucceeded(mockData.key, mockData.label);
		await wrapper.vm.$nextTick();
		expect(wrapper.emitted('update-succeeded')[0][0]).toEqual(mockData.config);
	});

	it('Should emit success flag properly for non-email data', async () => {
		const mockData = {
			key: 'not_email',
			label: 'Bukan Email',
			config: {
				needReload: true,
				message: 'Bukan Email berhasil diubah.'
			}
		};

		wrapper.vm.onUpdateInfoSucceeded(mockData.key, mockData.label);
		await wrapper.vm.$nextTick();
		expect(wrapper.emitted('update-succeeded')[0][0]).toEqual(mockData.config);
	});
});
