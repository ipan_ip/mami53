import { createLocalVue, shallowMount } from '@vue/test-utils';
import OwnerNotificationSettingsWrapper from 'Js/owner/components/settings/OwnerNotificationSettingsWrapper.vue';
import mockComponent from 'tests-fe/utils/mock-component';
import { makeAPICall } from 'Js/@utils/makeAPICall.js';

const localVue = createLocalVue();

const stubs = {
	BgText: mockComponent,
	BgSkeleton: mockComponent,
	BgCheckbox: mockComponent
};
const mocks = {
	swalError: jest.fn()
};

jest.mock('Js/@utils/makeAPICall.js');

describe('OwnerNotificationSettingsWrapper.vue', () => {
	let wrapper;

	beforeEach(() => {
		wrapper = shallowMount(OwnerNotificationSettingsWrapper, {
			localVue,
			stubs,
			mocks
		});
	});

	it('Should mounts properly', () => {
		expect(wrapper.find('.owner-notification-settings-wrapper').exists()).toBe(
			true
		);
	});

	it('Should handle update setting to truthy value properly', async () => {
		makeAPICall.mockReturnValueOnce({
			status: 200,
			data: true
		});

		const mockItem = {
			label: 'Mock Label',
			key: 'mock_key',
			value: true
		};
		const config = {
			needReload: false,
			message: 'Mock Label berhasil diaktifkan'
		};

		wrapper.vm.handleUpdateSetting(mockItem);
		await wrapper.vm.$nextTick();
		expect(wrapper.emitted('update-succeeded')[0][0]).toEqual(config);
	});

	it('Should handle update setting to falsy value properly', async () => {
		makeAPICall.mockReturnValueOnce({
			status: 200,
			data: true
		});

		const mockItem = {
			label: 'Mock Label',
			key: 'mock_key',
			value: false
		};
		const config = {
			needReload: false,
			message: 'Mock Label berhasil dinonaktifkan'
		};

		wrapper.vm.handleUpdateSetting(mockItem);
		await wrapper.vm.$nextTick();
		expect(wrapper.emitted('update-succeeded')[0][0]).toEqual(config);
	});
});
