import { createLocalVue, shallowMount } from '@vue/test-utils';
import OwnerSettingsDocumentVerification from 'Js/owner/components/settings/OwnerSettingsDocumentVerification.vue';
import ownerStore from 'tests-fe/owner/components/__mocks__/ownerStore.js';
import documentVerificationStore from 'tests-fe/owner/components/__mocks__/ownerSettingsDocumentVerificationStore.js';
import mockComponent from 'tests-fe/utils/mock-component';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import Vuex from 'vuex';

const MixinHideSBWidget = require('Js/@mixins/MixinHideSBWidget');

global.tracker = jest.fn();

mockWindowProperty('window.location', {
	replace: jest.fn()
});

const localVue = createLocalVue();

localVue.use(Vuex);
localVue.mixin([MixinHideSBWidget]);

const $router = { replace: jest.fn() };
const stubs = {
	OwnerSettingsDocumentVerificationIntercept: mockComponent,
	OwnerSettingsDocumentVerificationInput: mockComponent,
	OwnerSettingsDocumentVerificationUpload: mockComponent,
	OwnerSettingsDocumentVerificationCapture: mockComponent,
	OwnerSettingsDocumentVerificationReview: mockComponent,
	OwnerSettingsDocumentVerificationFinish: mockComponent,
	OwnerSettingsDocumentVerificationGuide: mockComponent
};
const store = new Vuex.Store({
	...ownerStore,
	modules: {
		settingsDocumentVerification: documentVerificationStore
	}
});

describe('OwnerSettingsDocumentVerification.vue', () => {
	let wrapper;
	const trackGuideOpenedSpy = jest.spyOn(
		OwnerSettingsDocumentVerification.methods,
		'trackGuideOpened'
	);

	beforeEach(() => {
		wrapper = shallowMount(OwnerSettingsDocumentVerification, {
			localVue,
			store,
			stubs,
			mocks: {
				$router
			}
		});
	});

	it('Should mounts properly', () => {
		expect(wrapper.find('.owner-settings-document-verification').exists()).toBe(
			true
		);
	});

	it('Should handle fetch document type list request properly', async () => {
		const commitSpy = jest.spyOn(wrapper.vm.$store, 'commit');
		const dispatchSpy = jest.spyOn(wrapper.vm.$store, 'dispatch');
		wrapper.vm.handleFetchDocumentTypeList();
		expect(commitSpy).toBeCalled();
		expect(dispatchSpy).toBeCalled();

		commitSpy.mockRestore();
		dispatchSpy.mockRestore();
	});

	it('Should handle reverseState function properly', () => {
		const dispatchSpy = jest.spyOn(wrapper.vm.$store, 'dispatch');

		wrapper.vm.reverseState();
		expect(window.location.replace).toHaveBeenCalledWith('/ownerpage/settings');

		wrapper.vm.$store.state.settingsDocumentVerification.currentState =
			'finish';
		wrapper.vm.reverseState();
		expect(window.location.replace).toHaveBeenCalledWith('/ownerpage/settings');

		wrapper.vm.$store.state.settingsDocumentVerification.currentState = 'other';
		wrapper.vm.reverseState();
		expect(dispatchSpy).toBeCalled();
	});

	it('Should open guide modal properly', () => {
		wrapper.vm.showGuideScreen();
		expect(wrapper.vm.isGuideModalShown).toBe(true);
		expect(trackGuideOpenedSpy).toBeCalled();
	});
});
