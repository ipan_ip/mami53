import { createLocalVue, shallowMount } from '@vue/test-utils';
import OwnerSettingsDocumentVerificationInputBody from 'Js/owner/components/settings/document-verification/input-state/OwnerSettingsDocumentVerificationInputBody.vue';
import Vuex from 'vuex';

const localVue = createLocalVue();
localVue.use(Vuex);

describe('OwnerSettingsDocumentVerificationInputBody.vue', () => {
	let wrapper;

	beforeEach(() => {
		wrapper = shallowMount(OwnerSettingsDocumentVerificationInputBody, {
			localVue,
			store: new Vuex.Store({
				modules: {
					settingsDocumentVerification: {
						namespaced: true,
						state: {
							currentPhotoStep: 1,
							selectedDocument: {}
						}
					}
				}
			})
		});
	});

	it('Should mounts properly', () => {
		expect(
			wrapper.find('.owner-settings-document-verification-input-body').exists()
		).toBe(true);
	});

	it("Should emits 'show-document-type-list' when document list toggle button clicked", async () => {
		const button = wrapper.find('.bottom-section-button');
		button.trigger('click');
		await wrapper.vm.$nextTick();
		expect(wrapper.emitted('show-document-type-list')).toBeTruthy();
	});
});
