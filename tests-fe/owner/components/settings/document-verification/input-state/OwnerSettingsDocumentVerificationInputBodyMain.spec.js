import { createLocalVue, shallowMount } from '@vue/test-utils';
import OwnerSettingsDocumentVerificationInputBodyMain from 'Js/owner/components/settings/document-verification/input-state/OwnerSettingsDocumentVerificationInputBodyMain.vue';
import mockComponent from 'tests-fe/utils/mock-component';
import Vuex from 'vuex';

const localVue = createLocalVue();
localVue.use(Vuex);

const mockStore = new Vuex.Store({
	modules: {
		settingsDocumentVerification: {
			namespaced: true,
			state: {
				selectedMethod: 'uploader',
				currentPhotoStep: 1,
				selectedDocument: 'ktp'
			},
			getters: {
				completedSteps: () => {
					return [1];
				},
				documentLabel: () => {
					return jest.fn(() => {
						return 'Mock Label';
					});
				}
			}
		}
	}
});

describe('OwnerSettingsDocumentVerificationInputBodyMain.vue', () => {
	const stateClassSpy = jest.spyOn(
		OwnerSettingsDocumentVerificationInputBodyMain.methods,
		'stateClass'
	);

	let wrapper;

	beforeEach(() => {
		wrapper = shallowMount(OwnerSettingsDocumentVerificationInputBodyMain, {
			localVue,
			store: mockStore,
			stubs: {
				CheckIcon: mockComponent
			}
		});
	});

	it('Should mount properly', () => {
		expect(
			wrapper
				.find('.owner-settings-document-verification-input-body-main')
				.exists()
		).toBe(true);
	});

	it('Should return proper stateClass', () => {
		const testCases = [
			{
				step: 1,
				expectedState: 'completed'
			},
			{
				step: 2,
				expectedState: 'active'
			},
			{
				step: 3,
				expectedState: 'disabled'
			}
		];

		wrapper.vm.$store.state.settingsDocumentVerification.currentPhotoStep = 2;

		testCases.forEach(tc => {
			wrapper.vm.stateClass(tc.step);
			expect(stateClassSpy).toHaveReturnedWith(tc.expectedState);
		});
	});
});
