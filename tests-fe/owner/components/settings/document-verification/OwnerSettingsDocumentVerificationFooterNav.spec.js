import { createLocalVue, shallowMount } from '@vue/test-utils';
import OwnerSettingsDocumentVerificationFooterNav from 'Js/owner/components/settings/document-verification//OwnerSettingsDocumentVerificationFooterNav.vue';
import mockComponent from 'tests-fe/utils/mock-component';

const localVue = createLocalVue();

describe('OwnerSettingsDocumentVerificationFooterNav.vue', () => {
	let wrapper;

	const propsData = {
		onlyGoToNext: false,
		goToPrevText: 'Kembali',
		goToNextText: 'Lanjutkan',
		isDisabledGoToPrev: false,
		isDisabledGoToNext: false
	};

	beforeEach(() => {
		wrapper = shallowMount(OwnerSettingsDocumentVerificationFooterNav, {
			localVue,
			propsData,
			stubs: {
				MamiButton: mockComponent
			}
		});
	});

	it('Should mount properly', () => {
		expect(
			wrapper.find('.owner-settings-document-verification-footer-nav').exists()
		).toBe(true);
	});

	it('should emit back', () => {
		const { handleGoBack } = wrapper.vm;

		handleGoBack();
		expect(wrapper.emitted('back')).toBeTruthy();
	});

	it('should emit continue', () => {
		const { handleContinue } = wrapper.vm;

		handleContinue();
		expect(wrapper.emitted('continue')).toBeTruthy();
	});
});
