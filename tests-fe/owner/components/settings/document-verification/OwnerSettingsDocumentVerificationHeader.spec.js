import { createLocalVue, shallowMount } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import OwnerSettingsDocumentVerificationHeader from 'Js/owner/components/settings/document-verification//OwnerSettingsDocumentVerificationHeader.vue';

window.Vue = require('vue');
window.Vue.config.silent = true;

const localVue = createLocalVue();

describe('OwnerSettingsDocumentVerificationHeader.vue', () => {
	let wrapper;

	const propsData = {
		titleText: 'Mock Title Text'
	};

	beforeEach(() => {
		wrapper = shallowMount(OwnerSettingsDocumentVerificationHeader, {
			localVue,
			propsData
		});
	});

	it('returns header title text correctly', () => {
		const title = wrapper.find('.owner-settings-document-verification-header');
		expect(title.text()).toBe(wrapper.vm.titleText);
	});
});
