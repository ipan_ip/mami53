import { createLocalVue, shallowMount } from '@vue/test-utils';
import OwnerSettingsDocumentVerificationSelect from 'Js/owner/components/settings/document-verification//OwnerSettingsDocumentVerificationSelect.vue';
import mockComponent from 'tests-fe/utils/mock-component';
import Vuex from 'vuex';
import documentVerificationStore from 'tests-fe/owner/components/__mocks__/ownerSettingsDocumentVerificationStore.js';

const localVue = createLocalVue();
localVue.use(Vuex);

const store = new Vuex.Store({
	modules: {
		settingsDocumentVerification: documentVerificationStore
	}
});

describe('OwnerSettingsDocumentVerificationSelect.vue', () => {
	let wrapper;

	beforeEach(() => {
		wrapper = shallowMount(OwnerSettingsDocumentVerificationSelect, {
			localVue,
			store,
			stubs: {
				modal: mockComponent,
				MamiRadio: mockComponent,
				OwnerSettingsDocumentVerificationHeader: mockComponent,
				OwnerSettingsDocumentVerificationFooterNav: mockComponent
			},
			propsData: {
				isModalShown: false
			}
		});
	});

	it('Should exists', () => {
		expect(
			wrapper.find('.owner-settings-document-verification-select').exists()
		).toBe(true);
	});

	it('Should resets localSelectedDocument value on modal hidden', async () => {
		wrapper.setProps({ isModalShown: true });
		await wrapper.vm.$nextTick();
		expect(wrapper.vm.localSelectedDocument).toBe(null);
	});

	it('Should closes properly', async () => {
		wrapper.vm.closeModal();
		await wrapper.vm.$nextTick();
		expect(wrapper.emitted('modal-closed')[0][0]).toBe(
			wrapper.vm.localSelectedDocument
		);
	});
});
