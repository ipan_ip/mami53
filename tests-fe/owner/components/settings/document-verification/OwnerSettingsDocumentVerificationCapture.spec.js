import { createLocalVue, shallowMount } from '@vue/test-utils';
import OwnerSettingsDocumentVerificationCapture from 'Js/owner/components/settings/document-verification//OwnerSettingsDocumentVerificationCapture.vue';
import mockComponent from 'tests-fe/utils/mock-component';
import Vuex from 'vuex';
import documentVerificationStore from 'tests-fe/owner/components/__mocks__/ownerSettingsDocumentVerificationStore.js';

const localVue = createLocalVue();
localVue.use(Vuex);

global.alert = jest.fn();

const store = new Vuex.Store({
	modules: {
		settingsDocumentVerification: documentVerificationStore
	}
});

describe('OwnerSettingsDocumentVerificationCapture.vue', () => {
	let wrapper;
	const handleCameraErrorSpy = jest.spyOn(
		OwnerSettingsDocumentVerificationCapture.methods,
		'handleCameraError'
	);

	beforeEach(() => {
		wrapper = shallowMount(OwnerSettingsDocumentVerificationCapture, {
			localVue,
			store,
			stubs: {
				DeviceCameraDisplay: mockComponent,
				OwnerSettingsDocumentVerificationCaptureControls: mockComponent,
				OwnerSettingsDocumentVerificationCaptureTimer: mockComponent
			}
		});
	});

	it('Should exists', () => {
		expect(
			wrapper.find('.owner-settings-document-verification-capture').exists()
		).toBe(true);
	});

	it('Should return proper helper text', () => {
		const testCases = [
			{
				currentPhotoStep: 1,
				text:
					'Gunakan bagian data diri di kartu identitas sebagai obyek foto. Arahkan ke area foto, lalu ambil foto.'
			},
			{
				currentPhotoStep: 2,
				text:
					'Arahkan wajah dan kartu identitas ke area foto yang telah disediakan, lalu ambil foto.'
			}
		];

		testCases.forEach(tc => {
			wrapper.vm.$store.state.settingsDocumentVerification.currentPhotoStep =
				tc.currentPhotoStep;
			expect(wrapper.vm.helperText).toBe(tc.text);
		});
	});

	it('Should return proper camera ideal resolution', () => {
		const testCases = [
			{
				isSwitchCamera: true,
				resolution: 720
			},
			{
				isSwitchCamera: false,
				resolution: 1024
			}
		];

		testCases.forEach(tc => {
			wrapper.vm.$store.state.settingsDocumentVerification.isSwitchCamera =
				tc.isSwitchCamera;
			expect(wrapper.vm.idealResolution).toBe(tc.resolution);
		});
	});

	it('Should handle camera errors properly', () => {
		const testCases = [
			{
				type: 'no-support',
				message:
					'Browser anda tidak mendukung akses ke kamera perangkat, mohon gunakan browser lain atau aplikasi Mamikos.'
			},
			{
				type: 'no-permission',
				message:
					'Mohon berikan izin akses ke kamera perangkat agar fitur dapat digunakan.'
			},
			{
				type: null,
				message: null
			}
		];

		testCases.forEach(tc => {
			wrapper.vm.handleCameraError(tc.type);

			tc.type
				? expect(global.alert).toBeCalledWith(tc.message)
				: expect(handleCameraErrorSpy).toHaveReturnedWith(null);
		});
	});

	it('Should handle on finished capture properly', () => {
		jest.useFakeTimers();
		const dispatchSpy = jest.spyOn(wrapper.vm.$store, 'dispatch');
		const commitSpy = jest.spyOn(wrapper.vm.$store, 'commit');
		const mockImage = {
			src: 'mocksrc'
		};

		wrapper.vm.handleFinishCapture(mockImage);

		expect(dispatchSpy).toBeCalledWith(
			'settingsDocumentVerification/handlePhotoValue',
			mockImage.src
		);
		expect(commitSpy).toBeCalledWith(
			'settingsDocumentVerification/setIsPreviewPhoto',
			true,
			undefined
		);
		expect(setTimeout).toHaveBeenCalledTimes(1);
		expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 1000);
		jest.runAllTimers();
		dispatchSpy.mockRestore();
		commitSpy.mockRestore();
	});
});
