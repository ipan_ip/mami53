import { createLocalVue, shallowMount } from '@vue/test-utils';
import OwnerSettingsDocumentVerificationFinish from 'Js/owner/components/settings/document-verification//OwnerSettingsDocumentVerificationFinish.vue';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import mockComponent from 'tests-fe/utils/mock-component';

const localVue = createLocalVue();

mockWindowProperty('window.location', {
	replace: jest.fn()
});

describe('OwnerSettingsDocumentVerificationFinish.vue', () => {
	let wrapper;

	beforeEach(() => {
		wrapper = shallowMount(OwnerSettingsDocumentVerificationFinish, {
			localVue,
			stubs: {
				OwnerSettingsDocumentVerificationHeader: mockComponent,
				OwnerSettingsDocumentVerificationFooterNav: mockComponent
			}
		});
	});

	it('Should exists', () => {
		expect(
			wrapper.find('.owner-settings-document-verification-finish').exists()
		).toBe(true);
	});

	it('Should returns to settings page properly', () => {
		wrapper.vm.returnToSettingsPage();
		expect(window.location.replace).toHaveBeenCalledWith('/ownerpage/settings');
	});
});
