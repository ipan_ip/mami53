import { createLocalVue, shallowMount } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import OwnerSettingsDocumentVerificationReview from 'Js/owner/components/settings/document-verification//OwnerSettingsDocumentVerificationReview.vue';
import mockComponent from 'tests-fe/utils/mock-component';
import Vuex from 'vuex';
import ownerStore from 'tests-fe/owner/components/__mocks__/ownerStore.js';
import documentVerificationStore from 'tests-fe/owner/components/__mocks__/ownerSettingsDocumentVerificationStore.js';
import mockSwalLoading from 'tests-fe/utils/mock-swal-loading';

window.Vue = require('vue');
window.Vue.config.silent = true;

const localVue = createLocalVue();
localVue.use(Vuex);

mockSwalLoading();
const storeData = {
	...ownerStore,
	modules: {
		settingsDocumentVerification: documentVerificationStore
	}
};
const axios = {
	post: jest.fn().mockResolvedValue(),
	all: jest.fn().mockResolvedValue(),
	spread: jest.fn().mockReturnValue()
};

global.tracker = jest.fn();
global.axios = axios;

describe('OwnerSettingsDocumentVerificationReview.vue', () => {
	let wrapper;
	const mountData = {
		localVue,
		store: new Vuex.Store(storeData),
		stubs: {
			OwnerSettingsDocumentVerificationFooterNav: mockComponent
		}
	};
	const handleSubmitPhotosSpy = jest.spyOn(
		OwnerSettingsDocumentVerificationReview.methods,
		'handleSubmitPhotos'
	);

	beforeEach(() => {
		wrapper = shallowMount(OwnerSettingsDocumentVerificationReview, mountData);
	});

	afterEach(() => {
		jest.clearAllMocks();
	});

	it('Should exists', () => {
		expect(
			wrapper.find('.owner-settings-document-verification-review').exists()
		).toBe(true);
	});

	it('Should returns proper back button text', () => {
		const testCases = [
			{
				selectedMethod: 'upload',
				text: 'Unggah Ulang'
			},
			{
				selectedMethod: 'capture',
				text: 'Foto Ulang'
			}
		];

		testCases.forEach(tc => {
			wrapper.vm.$store.state.settingsDocumentVerification.selectedMethod =
				tc.selectedMethod;
			expect(wrapper.vm.backButtonText).toBe(tc.text);
		});
	});

	it('Should returns proper message text', () => {
		const testCases = [
			{
				step: 1,
				text:
					'Mohon periksa kembali hasil foto berikut. Pastikan tulisan dan seluruh isi kartu identitas terlihat jelas.'
			},
			{
				step: 2,
				text:
					'Mohon periksa kembali hasil foto berikut. Pastikan wajah dan kartu identitas Anda terlihat jelas.'
			}
		];

		testCases.forEach(tc => {
			wrapper.vm.$store.state.settingsDocumentVerification.currentPhotoStep =
				tc.step;
			expect(wrapper.vm.messageText).toBe(tc.text);
		});
	});

	it('Should call handleSubmitPhotos in the last photo step', () => {
		wrapper.destroy();

		storeData.modules.settingsDocumentVerification.getters.isLastPhotoStep = () =>
			jest.fn().mockReturnValue(true);

		wrapper = shallowMount(OwnerSettingsDocumentVerificationReview, mountData);

		wrapper.vm.handleNextStep();
		expect(handleSubmitPhotosSpy).toHaveBeenCalled();
	});

	it('should save id verification images', async () => {
		await wrapper.vm.handleSubmitPhotos();

		expect(axios.post).toHaveBeenCalledTimes(2);
		expect(axios.spread).toHaveBeenCalledWith(expect.any(Function));
	});
});
