import { createLocalVue, shallowMount } from '@vue/test-utils';
import OwnerSettingsDocumentVerificationInput from 'Js/owner/components/settings/document-verification//OwnerSettingsDocumentVerificationInput.vue';
import mockComponent from 'tests-fe/utils/mock-component';
import Vuex from 'vuex';
import ownerStore from 'tests-fe/owner/components/__mocks__/ownerStore.js';
import documentVerificationStore from 'tests-fe/owner/components/__mocks__/ownerSettingsDocumentVerificationStore.js';

const localVue = createLocalVue();
localVue.use(Vuex);

global.tracker = jest.fn();

const store = new Vuex.Store({
	...ownerStore,
	modules: {
		settingsDocumentVerification: documentVerificationStore
	}
});

describe('OwnerSettingsDocumentVerificationInput.vue', () => {
	let wrapper;
	const trackCaptureStateSpy = jest.spyOn(
		OwnerSettingsDocumentVerificationInput.methods,
		'trackCaptureState'
	);

	beforeEach(() => {
		wrapper = shallowMount(OwnerSettingsDocumentVerificationInput, {
			localVue,
			store,
			stubs: {
				OwnerSettingsDocumentVerificationHeader: mockComponent,
				OwnerSettingsDocumentVerificationInputBody: mockComponent,
				OwnerSettingsDocumentVerificationFooterNav: mockComponent,
				OwnerSettingsDocumentVerificationSelect: mockComponent
			}
		});
	});

	it('Should exists', () => {
		expect(
			wrapper.find('.owner-settings-document-verification-input').exists()
		).toBe(true);
	});

	it('Should return proper title text', () => {
		const documentLabel =
			wrapper.vm.$store.state.settingsDocumentVerification.selectedDocument
				.label;
		const testCases = [
			{
				currentPhotoStep: 1,
				text: `Masukkan kartu identitas (${documentLabel}) Anda`
			},
			{
				currentPhotoStep: 2,
				text: `Masukkan foto diri bersama kartu identitas (${documentLabel})`
			}
		];

		testCases.forEach(tc => {
			wrapper.vm.$store.state.settingsDocumentVerification.currentPhotoStep =
				tc.currentPhotoStep;
			expect(wrapper.vm.titleText).toBe(tc.text);
		});
	});

	it('Should show document type list modal properly', () => {
		wrapper.vm.showDocumentTypeList();
		expect(wrapper.vm.isDocumentTypeListModalShown).toBe(true);
	});

	it('Should handle shifting to next state properly', () => {
		const dispatchSpy = jest.spyOn(wrapper.vm.$store, 'dispatch');

		wrapper.vm.toNextState();
		expect(dispatchSpy).toBeCalledWith(
			'settingsDocumentVerification/shiftState',
			true
		);
		expect(trackCaptureStateSpy).toHaveBeenCalled();
	});

	it('Should handle select new document type properly', () => {
		jest.useFakeTimers();
		const commitSpy = jest.spyOn(wrapper.vm.$store, 'commit');

		wrapper.vm.handleSelectNewType('ktp');

		expect(wrapper.vm.isDocumentTypeListModalShown).toBe(false);
		expect(setTimeout).toHaveBeenCalledTimes(1);
		expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 300);
		jest.runAllTimers();
		expect(commitSpy).toHaveBeenCalled();
	});

	it('Should handle track start capture photo event properly', () => {
		const storeState = wrapper.vm.$store.state;
		const testCases = [
			{
				photoStep: 1,
				isMobile: true,
				eventName: '[Owner] ID Verification - Foto ID'
			},
			{
				photoStep: 2,
				isMobile: false,
				eventName: '[Owner] ID Verification - Foto Diri'
			}
		];
		const mockTrackerData = {
			owner_name: storeState.profile.user.name,
			owner_phone_number: storeState.profile.user.phone_number,
			verification_status:
				storeState.mamipayDetail.verification_data.identity_card_status,
			type_id: storeState.settingsDocumentVerification.selectedDocument.label,
			flow_type: storeState.settingsDocumentVerification.selectedMethod
		};

		testCases.forEach(tc => {
			navigator.isMobile = tc.isMobile;

			mockTrackerData.interface = tc.isMobile ? 'mobile' : 'desktop';
			storeState.settingsDocumentVerification.currentPhotoStep = tc.photoStep;

			wrapper.vm.trackCaptureState();

			expect(global.tracker).toHaveBeenCalledWith('moe', [
				tc.eventName,
				mockTrackerData
			]);
		});
	});
});
