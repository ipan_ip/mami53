import { createLocalVue, shallowMount } from '@vue/test-utils';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import OwnerSettingsDocumentVerificationGuide from 'Js/owner/components/settings/document-verification/OwnerSettingsDocumentVerificationGuide.vue';

const localVue = createLocalVue();
mockVLazy(localVue);

describe('OwnerSettingsDocumentVerificationGuide.vue', () => {
	let wrapper;

	const propsData = {
		photoStep: 1,
		isModalShown: true
	};

	beforeEach(() => {
		wrapper = shallowMount(OwnerSettingsDocumentVerificationGuide, {
			localVue,
			propsData
		});
	});

	it('Should mounts properly', () => {
		expect(
			wrapper.find('.owner-settings-document-verification-guide').exists()
		).toBe(true);
	});

	it('Should returns proper sampleImages data', () => {
		const testCases = [
			{
				photoStep: 1,
				image: '/general/img/pictures/kyc-owner/photo_preview_id_front.png',
				imageAlt: 'Right card only sample image',
				image2: '/general/img/pictures/kyc-owner/photo_preview_id_back.png',
				imageAlt2: 'Wrong card only sample image'
			},
			{
				photoStep: 2,
				image: '/general/img/pictures/kyc-owner/photo_preview_selfie_front.png',
				imageAlt: 'Right selfie sample image',
				image2: '/general/img/pictures/kyc-owner/photo_preview_selfie_back.png',
				imageAlt2: 'Wrong selfie sample image'
			}
		];

		testCases.forEach(tc => {
			wrapper.setProps({ photoStep: tc.photoStep });
			expect(wrapper.vm.sampleImages[0].image).toEqual(tc.image);
			expect(wrapper.vm.sampleImages[0].imageAlt).toEqual(tc.imageAlt);
			expect(wrapper.vm.sampleImages[1].image).toEqual(tc.image2);
			expect(wrapper.vm.sampleImages[1].imageAlt).toEqual(tc.imageAlt2);
		});
	});

	it('Should returns proper textContentItems data', () => {
		const testCases = [
			{
				photoStep: 1,
				expectedContent: [
					'Bagian yang difoto adalah bagian yang berisi nama lengkap dan informasi data diri lainnya.',
					'Ambil foto di tempat dengan cahaya cukup dan pastikan tulisan di kartu identitas terlihat jelas.'
				]
			},
			{
				photoStep: 2,
				expectedContent: [
					'Pastikan kartu identitas Anda tidak menutupi wajah saat mengambil foto.',
					'Ambil foto di tempat dengan cahaya cukup agar wajah dan kartu identitas Anda terlihat jelas.'
				]
			}
		];

		testCases.forEach(tc => {
			wrapper.setProps({ photoStep: tc.photoStep });
			expect(wrapper.vm.textContentItems).toEqual(tc.expectedContent);
		});
	});

	it('Should emit handle closeModal method properly', async () => {
		wrapper.vm.closeModal();
		await wrapper.vm.$nextTick();
		expect(wrapper.emitted('update:isModalShown')[0][0]).toBe(false);
		expect(wrapper.vm.$refs.modalBody.scrollTop).toBe(0);
	});
});
