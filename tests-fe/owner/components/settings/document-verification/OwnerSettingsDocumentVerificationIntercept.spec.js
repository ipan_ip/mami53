import { createLocalVue, shallowMount } from '@vue/test-utils';
import OwnerSettingsDocumentVerificationIntercept from 'Js/owner/components/settings/document-verification//OwnerSettingsDocumentVerificationIntercept.vue';
import mockComponent from 'tests-fe/utils/mock-component';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import Vuex from 'vuex';
import ownerStore from 'tests-fe/owner/components/__mocks__/ownerStore.js';
import documentVerificationStore from 'tests-fe/owner/components/__mocks__/ownerSettingsDocumentVerificationStore.js';

const localVue = createLocalVue();
localVue.use(Vuex);

global.tracker = jest.fn();

mockWindowProperty('window.location', {
	replace: jest.fn()
});

const store = new Vuex.Store({
	...ownerStore,
	modules: {
		settingsDocumentVerification: documentVerificationStore
	}
});

describe('OwnerSettingsDocumentVerificationIntercept.vue', () => {
	let wrapper;
	const trackSelectedVerificationMethodSpy = jest.spyOn(
		OwnerSettingsDocumentVerificationIntercept.methods,
		'trackSelectedVerificationMethod'
	);

	beforeEach(() => {
		wrapper = shallowMount(OwnerSettingsDocumentVerificationIntercept, {
			localVue,
			store,
			stubs: {
				BgModal: mockComponent,
				MamiRadio: mockComponent,
				OwnerSettingsDocumentVerificationHeader: mockComponent,
				OwnerSettingsDocumentVerificationFooterNav: mockComponent
			}
		});
	});

	it('Should exists', () => {
		expect(
			wrapper.find('.owner-settings-document-verification-intercept').exists()
		).toBe(true);
	});

	it('Should handle return to settings page properly', () => {
		wrapper.vm.returnToSettingsPage();
		expect(window.location.replace).toHaveBeenCalledWith('/ownerpage/settings');
	});

	it('Should handle shifting to next state properly', () => {
		const commitSpy = jest.spyOn(wrapper.vm.$store, 'commit');
		const dispatchSpy = jest.spyOn(wrapper.vm.$store, 'dispatch');

		wrapper.vm.toNextState();

		expect(commitSpy).toHaveBeenCalledWith(
			'settingsDocumentVerification/setSelectedMethod',
			wrapper.vm.localSelectedMethod,
			undefined
		);
		expect(dispatchSpy).toHaveBeenCalledWith(
			'settingsDocumentVerification/shiftState',
			true
		);
		expect(trackSelectedVerificationMethodSpy).toHaveBeenCalled();

		commitSpy.mockRestore();
		dispatchSpy.mockRestore();
	});

	it('Should open mobile app properly', () => {
		wrapper.vm.openMobileApp();
		expect(window.location.replace).toHaveBeenCalledWith(
			'bang.kerupux.com://home'
		);
	});

	it('Should close suggestion modal properly', () => {
		wrapper.setData({
			isSuggestionModalShown: true
		});
		wrapper.vm.closeSuggestionModal();
		expect(wrapper.vm.isSuggestionModalShown).toBe(false);
	});
});
