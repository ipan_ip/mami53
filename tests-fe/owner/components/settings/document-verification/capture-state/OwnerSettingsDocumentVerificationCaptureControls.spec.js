import { createLocalVue, shallowMount } from '@vue/test-utils';
import OwnerSettingsDocumentVerificationCaptureControls from 'Js/owner/components/settings/document-verification/capture-state/OwnerSettingsDocumentVerificationCaptureControls.vue';
import Vuex from 'vuex';

const localVue = createLocalVue();
localVue.use(Vuex);

describe('OwnerSettingsDocumentVerificationCaptureControls.vue', () => {
	let wrapper;
	const capturePhotoSpy = jest.spyOn(
		OwnerSettingsDocumentVerificationCaptureControls.methods,
		'capturePhoto'
	);
	const switchCameraSpy = jest.spyOn(
		OwnerSettingsDocumentVerificationCaptureControls.methods,
		'switchCamera'
	);

	beforeEach(() => {
		wrapper = shallowMount(OwnerSettingsDocumentVerificationCaptureControls, {
			localVue,
			store: new Vuex.Store({
				modules: {
					settingsDocumentVerification: {
						namespaced: true,
						state: {
							isSwitchCamera: true
						},
						actions: {
							switchCamera: () => jest.fn(),
							capturePhoto: () => jest.fn()
						}
					}
				}
			})
		});
	});

	it('Should mounts properly', () => {
		expect(
			wrapper
				.find('.owner-settings-document-verification-capture-controls')
				.exists()
		).toBe(true);
	});

	it('Should return proper switch camera control icon data', () => {
		const testCases = [
			{
				isSwitchCamera: true,
				iconPath: '/general/img/icons/ic_switch_camera_filled_white.svg'
			},
			{
				isSwitchCamera: false,
				iconPath: '/general/img/icons/ic_switch_camera_outline_white.svg'
			}
		];

		testCases.forEach(tc => {
			wrapper.vm.$store.state.settingsDocumentVerification.isSwitchCamera =
				tc.isSwitchCamera;
			expect(wrapper.vm.controlItems[1].iconPath).toEqual(tc.iconPath);
		});
	});

	it('Should handle proper control actions', () => {
		const testCases = [
			{
				value: 'capture',
				action: capturePhotoSpy
			},
			{
				value: 'switch',
				action: switchCameraSpy
			}
		];

		testCases.forEach(tc => {
			wrapper.vm.onControlClicked(tc.value);
			expect(tc.action).toBeCalled();
		});
	});
});
