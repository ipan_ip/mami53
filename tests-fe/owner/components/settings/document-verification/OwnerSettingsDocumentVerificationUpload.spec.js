import { createLocalVue, shallowMount } from '@vue/test-utils';
import OwnerSettingsDocumentVerificationUpload from 'Js/owner/components/settings/document-verification//OwnerSettingsDocumentVerificationUpload.vue';
import mockComponent from 'tests-fe/utils/mock-component';
import Vuex from 'vuex';
import documentVerificationStore from 'tests-fe/owner/components/__mocks__/ownerSettingsDocumentVerificationStore.js';
import mixinUploadImage from 'Js/@mixins/MixinUploadImage';

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.mixin([mixinUploadImage]);

const store = new Vuex.Store({
	modules: {
		settingsDocumentVerification: documentVerificationStore
	}
});

describe('OwnerSettingsDocumentVerificationUpload.vue', () => {
	let wrapper;
	const startImageUploadSpy = jest.spyOn(
		OwnerSettingsDocumentVerificationUpload.methods,
		'startImageUpload'
	);

	beforeEach(() => {
		wrapper = shallowMount(OwnerSettingsDocumentVerificationUpload, {
			localVue,
			store,
			stubs: {
				BgIcon: mockComponent,
				OwnerSettingsDocumentVerificationHeader: mockComponent
			}
		});
	});

	it('Should exists', () => {
		expect(
			wrapper.find('.owner-settings-document-verification-upload').exists()
		).toBe(true);
	});

	it('Should starts image upload functions when dropzone is clicked', async () => {
		const dropzone = wrapper.find('.image-dropzone');

		expect(dropzone.exists()).toBe(true);
		expect(wrapper.vm.isLoadingImage).toBe(false);

		dropzone.trigger('click');
		await wrapper.vm.$nextTick();
		expect(startImageUploadSpy).toHaveBeenCalled();
	});

	it('Should handle image upload events properly', () => {
		const mockEvent = {
			target: {
				files: new Blob(
					[
						{
							type: 'image/png'
						}
					],
					{ type: 'application/json' }
				)
			}
		};
		const uploadImageSpy = jest.spyOn(wrapper.vm, 'uploadImage');

		wrapper.vm.handleImageInputChanges(mockEvent);
		expect(uploadImageSpy).toHaveBeenCalled();
	});

	it('Should shifts to next state after loadedImage value is provided', async () => {
		const dispatchSpy = jest.spyOn(wrapper.vm.$store, 'dispatch');

		wrapper.setData({
			loadedImage: 'mockImage'
		});

		await wrapper.vm.$nextTick();

		expect(dispatchSpy).toHaveBeenCalledTimes(2);
		expect(dispatchSpy).toHaveBeenCalledWith(
			'settingsDocumentVerification/handlePhotoValue',
			wrapper.vm.loadedImage
		);
		expect(dispatchSpy).toHaveBeenCalledWith(
			'settingsDocumentVerification/shiftState',
			true
		);

		dispatchSpy.mockRestore();
	});
});
