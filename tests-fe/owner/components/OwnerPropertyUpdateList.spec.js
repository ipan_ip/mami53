import { createLocalVue, shallowMount } from '@vue/test-utils';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import ownerStore from 'tests-fe/owner/components/__mocks__/ownerStore.js';
import mockAxios from 'tests-fe/utils/mock-axios';
import 'tests-fe/utils/mock-vue';
import OwnerPropertyUpdateList from 'Js/owner/components/OwnerPropertyUpdateList.vue';
import Vuex from 'vuex';

const localVue = createLocalVue();
localVue.use(Vuex);

global.alert = jest.fn();
global.axios = mockAxios;
global.swal = jest.fn().mockResolvedValue(true);

const swalError = jest.fn();
const closeSwalLoading = jest.fn();

mockWindowProperty('bugsnagClient', {
	notify: jest.fn()
});

const propsData = {
	room: {
		price_daily: 50000,
		price_weekly: 130000,
		price_monthly: 500000,
		price_yearly: 1500000,
		price_3_month: 3500000,
		price_6_month: 5000000,
		photo_url: {
			small: 'small.jpeg'
		}
	},
	parentKey: 1,
	propertyType: 'apartment'
};

const store = new Vuex.Store({
	...ownerStore
});

describe('OwnerPropertyUpdateList.vue', () => {
	let wrapper;

	const sendRoomDataSpy = jest.spyOn(
		OwnerPropertyUpdateList.methods,
		'sendRoomData'
	);

	beforeEach(() => {
		wrapper = shallowMount(OwnerPropertyUpdateList, {
			localVue,
			propsData,
			store,
			mocks: {
				swalError,
				closeSwalLoading,
				$router: {
					push: jest.fn()
				}
			}
		});
	});

	it('Should mounts properly', () => {
		expect(wrapper.find('.col-separator').exists()).toBe(true);
	});

	it('Should return base computed value properly got from storeData', () => {
		expect(wrapper.vm.token).toBe('mocktoken12345');
		expect(wrapper.vm.prices.length).not.toBe(0);
		expect(wrapper.vm.canUpdate).toBeTruthy();
		expect(wrapper.vm.selectedDuration.length).not.toBe(0);
		expect(wrapper.vm.validatePriceInput).toBeTruthy();
		expect(wrapper.vm.getUpdateParams).not.toBeNull();

		wrapper.setData({
			isUpdateStatus: 'busy',
			price: {
				dailyCheck: false,
				weeklyCheck: false,
				monthlyCheck: false,
				yearlyCheck: false,
				threeMonthlyCheck: false,
				sixMonthlyCheck: false
			}
		});

		expect(wrapper.vm.canUpdate).toBeFalsy();
		expect(wrapper.vm.getUpdateParams).toEqual({
			_token: wrapper.vm.token,
			room_id: wrapper.vm.room._id,
			room_available: wrapper.vm.availableRoom,
			price_daily: 0,
			price_weekly: 0,
			price_monthly: 0,
			price_yearly: 0,
			'3_month': 0,
			'6_month': 0
		});
	});

	it('should handle submitRoomData method properly', async () => {
		wrapper.vm.submitRoomData();

		expect(sendRoomDataSpy).toHaveBeenCalled();

		await wrapper.setProps({
			room: {
				price_daily: 1,
				price_weekly: 1,
				price_monthly: 1,
				price_yearly: 1,
				price_3_month: 1,
				price_6_month: 1,
				photo_url: {
					small: 'small.jpeg'
				}
			},
			parentKey: 1,
			propertyType: 'apartment'
		});
		wrapper.vm.setDefaultPrice();
		wrapper.vm.submitRoomData();

		expect(global.swal).toBeCalled();
	});

	it('should change isUpdateStatus to success when calling updatePassword method on successful response', async () => {
		axios.mockResolve({
			data: {
				status: true
			}
		});

		await wrapper.vm.sendRoomData();

		expect(wrapper.vm.isUpdateStatus).toBe('success');
	});

	it('should handle error action when calling updatePassword method on failed response', async () => {
		axios.mockResolve({
			data: {
				status: false
			}
		});

		await wrapper.vm.sendRoomData();

		expect(wrapper.vm.isUpdateStatus).toBe('error');
	});

	it('should call bugsnagClient when calling sendRoomData method on error response', async () => {
		axios.mockReject('error');

		await wrapper.vm.sendRoomData();

		expect(global.bugsnagClient.notify).toBeCalled();
		expect(closeSwalLoading).toBeCalled();
	});

	it('should update available room properly when calling changeRoomAvailability method', async () => {
		let param;
		param = 'penuh';
		wrapper.vm.changeRoomAvailability(param);

		expect(wrapper.vm.availableRoom).toBe(0);

		param = 'tersedia';
		wrapper.vm.changeRoomAvailability(param);

		expect(wrapper.vm.availableRoom).toBe(1);
	});
});
