import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';

import 'tests-fe/utils/mock-cookies';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import mockAxios from 'tests-fe/utils/mock-axios';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

import OwnerModalTrial from 'Js/owner/components/OwnerModalTrial.vue';

describe('OwnerModalTrial.vue', () => {
	const localVue = createLocalVue();
	localVue.use(Vuex);
	mockVLazy(localVue);

	global.axios = mockAxios;

	mockWindowProperty('open', jest.fn());
	mockWindowProperty(
		'$',
		jest.fn().mockReturnValue({
			modal: jest.fn()
		})
	);

	global.bugsnagClient = {
		notify: jest.fn()
	};
	global.alert = jest.fn();
	global.swal = jest.fn().mockResolvedValue(true);

	const storeData = {
		state: {
			token: 'token'
		}
	};

	const methods = {
		openSwalLoading: jest.fn(),
		closeSwalLoading: jest.fn()
	};

	const mocks = {
		$emit: jest.fn()
	};

	// mount function
	const mount = () => {
		return shallowMount(OwnerModalTrial, {
			localVue,
			store: new Vuex.Store(storeData),
			mocks,
			methods
		});
	};

	describe('render component correctly', () => {
		let wrapper;

		beforeEach(() => {
			wrapper = mount();
		});

		it('should render container properly', () => {
			expect(wrapper.find('#ownerModalTrial').exists()).toBeTruthy();
		});
	});

	describe('render computed value properly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = mount();
		});

		it('should return token value properly', () => {
			expect(wrapper.vm.token).toBe('token');
		});
	});

	describe('call functions correctly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = mount();
		});

		it('should call $() when calling openModal method', async () => {
			await wrapper.vm.openModal();
			expect(global.$).toBeCalled();
		});

		it('should call $() when calling closeModal method', async () => {
			await wrapper.vm.closeModal();

			expect(global.$).toBeCalled();
			expect(global.Cookies.set).toBeCalled();
		});

		it('should not call swal and window.open when calling tryTrial method on successful response, but received falsy status', async () => {
			axios.mockResolve({
				data: {
					status: false
				}
			});

			await wrapper.vm.tryTrial();
			await wrapper.vm.$nextTick();
			expect(global.swal).not.toBeCalled();
			expect(window.open).not.toBeCalled();
		});

		it('should call swal and window.open when calling tryTrial method on successful response', async () => {
			axios.mockResolve({
				data: {
					status: true
				}
			});

			await wrapper.vm.tryTrial();
			await wrapper.vm.$nextTick();
			expect(global.swal).toBeCalled();
			expect(window.open).toBeCalled();
		});

		it('should call swal and window.open when calling tryTrial method on failed response', async () => {
			axios.mockReject('error');

			await wrapper.vm.tryTrial();
			await wrapper.vm.$nextTick();

			expect(global.bugsnagClient.notify).toBeCalled();
			expect(global.alert).toBeCalled();
		});
	});
});
