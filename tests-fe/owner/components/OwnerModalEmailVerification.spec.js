import { createLocalVue, shallowMount } from '@vue/test-utils';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

import OwnerModalEmailVerification from 'Js/owner/components/OwnerModalEmailVerification.vue';

describe('OwnerModalEmailVerification.vue', () => {
	const localVue = createLocalVue();

	mockWindowProperty('open', jest.fn());
	mockWindowProperty(
		'$',
		jest.fn().mockReturnValue({
			modal: jest.fn()
		})
	);

	const mocks = {
		$emit: jest.fn(),
		$router: {
			push: jest.fn()
		}
	};

	// mount function
	const mount = () => {
		return shallowMount(OwnerModalEmailVerification, {
			localVue,
			mocks
		});
	};

	describe('render component correctly', () => {
		let wrapper;

		beforeEach(() => {
			wrapper = mount();
		});

		it('should render container properly', () => {
			expect(
				wrapper.find('#ownerModalEmailVerification').exists()
			).toBeTruthy();
		});
	});

	describe('call functions correctly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = mount();
		});

		it('should call $() when calling openModal method', async () => {
			await wrapper.vm.openModal();
			expect(global.$).toBeCalled();
		});

		it('should call $() when calling closeModal method', async () => {
			await wrapper.vm.closeModal();

			expect(global.$).toBeCalled();
		});

		it('should call $router.push and closeModal when calling goToSetting method', async () => {
			const closeModalSpy = jest.spyOn(wrapper.vm, 'closeModal');
			const pushSpy = jest.spyOn(wrapper.vm.$router, 'push');

			await wrapper.vm.goToSetting();

			expect(closeModalSpy).toBeCalled();
			expect(pushSpy).toBeCalled();

			closeModalSpy.mockRestore();
			pushSpy.mockRestore();
		});
	});
});
