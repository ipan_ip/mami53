import { shallowMount, createLocalVue } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';

import Vuex from 'vuex';
import OwnerMenu from 'Js/owner/components/OwnerMenu';
import NavigatorIsMobileMixin from 'Js/@mixins/MixinNavigatorIsMobile';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

import * as storage from 'Js/@utils/storage';

const profileData = {
	status: true,
	is_mamirooms: true,
	is_booking_all_room: true,
	membership: {
		is_mamipay_user: true,
		expired: false,
		status: 'Premium'
	},
	user: {
		kost_total_active: 2
	}
};

const axios = jest.fn().mockResolvedValue({
	status: 200,
	data: {
		...profileData
	}
});

global.axios = axios;
global.bugsnagClient = { notify: jest.fn() };

describe('OwnerMenu.vue', () => {
	const localVue = createLocalVue();
	localVue.use(Vuex);

	// mock global functions
	global.tracker = jest.fn();
	global.swal = jest.fn().mockResolvedValue('success');
	const navigator = {
		isMobile: false
	};
	global.navigator = navigator;

	// mock window
	mockWindowProperty('open', jest.fn().mockReturnValue(true));
	mockWindowProperty('location', {
		href: 'owner.mamikos.com/'
	});
	mockWindowProperty('oxWebUrl', 'local.mamikos.com');

	// mock directive
	localVue.directive('lazy', (el, binding) => {});

	const stubs = {
		RouterLink: true
	};

	const store = {
		modules: {
			isBookingAllRoom: {
				state: true
			},
			profile: {
				namespaced: true,
				state: {
					is_blacklist_mamipoin: false,
					membership: {
						is_mamipay_user: true
					},
					user: {
						kost_total: 20
					}
				}
			}
		}
	};

	const mocks = {
		$toasted: { show: jest.fn() },
		$route: { name: '', path: '' },
		$router: { push: jest.fn() }
	};

	const parentComponent = {
		data() {
			return {
				state: {
					authData: {}
				}
			};
		}
	};

	const propsData = {
		isApartementActive: true,
		isKosActive: true
	};

	const mountData = {
		localVue,
		stubs,
		mocks,
		mixins: [NavigatorIsMobileMixin],
		store: new Vuex.Store(store),
		propsData
	};

	const mount = (adtMountData = {}) => {
		return shallowMount(OwnerMenu, {
			parentComponent,
			...mountData,
			...adtMountData
		});
	};

	let wrapper;
	beforeEach(() => {
		wrapper = mount();
	});

	it('should render OwnerMenu', () => {
		expect(wrapper.find('.owner-menu').exists()).toBe(true);
	});

	it('should return ownerDashboardUrl from oxWebUrl', () => {
		expect(wrapper.vm.ownerDashboardUrl).toBe(window.oxWebUrl);
	});

	it('should render profile properly', () => {
		expect(wrapper.vm.profile).toBeDefined();
	});

	it('should call window.open when calling openBookingOwner method', async () => {
		wrapper.vm.$store.state.profile.membership.is_mamipay_user = true;
		wrapper.vm.$store.state.profile.is_booking_all_room = true;

		await wrapper.vm.openBookingOwner();

		expect(window.open).toHaveBeenCalled();
	});

	it('should toggle sidebar showLeft when calling openOwnerSidebar method', async () => {
		wrapper.vm.$refs = {
			ownerSidebar: {
				showLeft: false
			}
		};
		await wrapper.vm.openOwnerSidebar();

		expect(wrapper.vm.$refs.ownerSidebar.showLeft).toBeTruthy();
	});

	it('should call tracker when calling sendMamipoinTrackerEvent method', async () => {
		// desktop view
		await wrapper.vm.sendMamipoinTrackerEvent();
		expect(tracker).toHaveBeenCalled();
		const desktopCall = tracker.mock.calls[0];
		expect(desktopCall[0]).toBe('moe');
		expect(desktopCall[1][1].interface).toBe('desktop');

		// mobile view
		global.navigator.isMobile = true;
		wrapper = mount();
		await wrapper.vm.sendMamipoinTrackerEvent();
		expect(tracker).toHaveBeenCalled();
		const mobileCall = tracker.mock.calls[1];
		expect(mobileCall[0]).toBe('moe');
		expect(mobileCall[1][1].interface).toBe('mobile');
	});

	it('should get & set localStorage when calling openBookingOwner', async () => {
		wrapper.vm.$store.state.profile.membership.is_mamipay_user = true;
		wrapper.vm.$store.state.profile.is_booking_all_room = true;

		storage.local.removeItem('activate_booking_redirection_source');

		const spyStorageLocalGetItem = jest.spyOn(storage.local, 'getItem');
		const spyStorageLocalSetItem = jest.spyOn(storage.local, 'setItem');

		await wrapper.vm.openBookingOwner();

		expect(spyStorageLocalGetItem).toBeCalled();
		expect(spyStorageLocalSetItem).toBeCalled();

		spyStorageLocalGetItem.mockRestore();
		spyStorageLocalSetItem.mockRestore();
	});

	it('should fetch profile data when visit statistics page', async () => {
		mockWindowProperty('window.location', {
			href: 'https://mamikos.com/ownerpage/statistics'
		});
		wrapper = mount();
		await new Promise(resolve => setImmediate(resolve));

		expect(wrapper.vm.profileData).toEqual(profileData);
	});

	it('should return isBooking value properly', () => {
		// try block
		expect(wrapper.vm.isBooking).toBe(wrapper.vm.$store.state.isBookingAllRoom);

		// catch block
		wrapper.vm.$store.replaceState(null);
		expect(wrapper.vm.isBooking).toBeFalsy();
		wrapper.vm.$store.replaceState({ profile: {} });
	});

	it('should return isBlacklistMamipoin value from profile.is_blacklist_mamipoin', () => {
		expect(wrapper.vm.isBlacklistMamipoin).toBe(
			!!wrapper.vm.profile.is_blacklist_mamipoin
		);

		wrapper.vm.$store.state.profile = null;

		expect(wrapper.vm.isBlacklistMamipoin).toBeFalsy();
	});

	it('Should handle internal item properly', () => {
		mockWindowProperty('window.location', {
			pathname: 'ownerpage/apartment'
		});
		wrapper.vm.$route.path = '/apartment';

		const routeItem = {
			type: 'internal',
			data: '/kos'
		};

		wrapper.vm.handleItemClicked({ ...routeItem });
		expect(mocks.$router.push).toBeCalledWith('/kos');
	});

	it('Should handle pms and external item properly', () => {
		mockWindowProperty('window.location', {
			pathname: 'ownerpage/apartment',
			href: ''
		});

		wrapper.vm.$route.path = '/apartment';

		const routeItem = {
			type: 'pms',
			data: '/kos'
		};

		wrapper.vm.handleItemClicked({ ...routeItem });
		expect(global.location.href).toBe('local.mamikos.com/kos');

		const routeItemExternal = {
			type: 'external',
			data: 'https://google.com'
		};

		wrapper.vm.handleItemClicked({ ...routeItemExternal });
		expect(global.location.href).toBe('https://google.com');
	});

	it('Should handle action type item properly', () => {
		mockWindowProperty('window.location', {
			pathname: '',
			href: ''
		});
		const routeItem = {
			data(ctx) {
				ctx.$router.push('/hello');
			},
			type: 'action'
		};

		wrapper.vm.handleItemClicked({ ...routeItem });
		expect(mocks.$router.push).toBeCalledWith('/hello');

		wrapper.vm.$route.name = '';
		mockWindowProperty('window.location', {
			pathname: '',
			href: ''
		});

		const routeItem2 = {
			type: 'internal',
			data: '/test',
			base: '/ownerpage'
		};

		wrapper.vm.handleItemClicked({ ...routeItem2 });
		expect(window.location.href).toBe('/ownerpage/test');
	});

	it('Should find selected item properly', () => {
		wrapper.vm.$route.name = 'one';

		const routeItem = {
			urlNames: ['one', 'two'],
			type: 'internal'
		};

		expect(wrapper.vm.findSelected(routeItem)).toBe(true);

		wrapper.vm.$route.name = 'two';
		const routeItem2 = {
			urlNames: ['one', 'two'],
			type: 'action',
			data() {}
		};

		expect(wrapper.vm.findSelected(routeItem2)).toBe(true);
	});

	it('Should send tracker properly', () => {
		wrapper.vm.sendSidebarODTracker('test', { redirection: 'test' });

		expect(global.tracker).toBeCalledWith('moe', [
			'test',
			{
				...wrapper.vm.sidebarTrackerData,
				redirection: 'test'
			}
		]);
	});

	it('Should handle base equality properly', async () => {
		wrapper.vm.$route.path = '';

		expect(wrapper.vm.hasSameBaseWithCurrent('/ownerpage')).toBe(false);

		mockWindowProperty('window.location', {
			href: 'http://mamikos.com/ownerpage/test',
			pathname: 'ownerpage/test'
		});

		wrapper.vm.$route.path = '/ownerpage/test';
		await wrapper.vm.$nextTick();

		expect(wrapper.vm.hasSameBaseWithCurrent('/manage/test')).toBe(false);
	});
});
