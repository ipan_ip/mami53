import { createLocalVue, shallowMount } from '@vue/test-utils';
import OwnerVacancy from 'Js/owner/components/OwnerVacancy.vue';
import mockComponent from 'tests-fe/utils/mock-component';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import mockAxios from 'tests-fe/utils/mock-axios';
import Vuex from 'vuex';

const localVue = createLocalVue();
localVue.use(Vuex);

global.alert = jest.fn();
global.axios = mockAxios;

mockWindowProperty('bugsnagClient', {
	notify: jest.fn()
});
mockWindowProperty('open', jest.fn());

const stubs = {
	MamiLoadingInline: mockComponent,
	OwnerListVacancy: mockComponent,
	OwnerListNull: mockComponent
};

describe('OwnerVacancy.vue', () => {
	let wrapper;

	const getJobsSpy = jest.spyOn(OwnerVacancy.methods, 'getJobs');

	beforeEach(() => {
		wrapper = shallowMount(OwnerVacancy, {
			localVue,
			stubs
		});
	});

	it('Should mounts properly', () => {
		expect(wrapper).toMatchSnapshot();
	});

	it('Should return base computed value properly', () => {
		const jobs = {
			vacancy: [],
			total_page: 1,
			total: 100,
			limit: 10
		};
		wrapper.setData({ jobs });

		expect(wrapper.vm.totalPage).toBe(10);
	});

	it('should get responses data properly when job vacancy length is zero', async () => {
		axios.mockResolve({
			data: {
				vacancy: [],
				total_page: 1
			}
		});
		await wrapper.vm.$nextTick();

		expect(wrapper.vm.isLoading).toBeFalsy();
	});

	it('should get responses data properly when job vacancy length is not zero', async () => {
		axios.mockResolve({
			data: {
				vacancy: [
					{
						name: 'mock vacancy'
					}
				],
				total_page: 2
			}
		});
		await wrapper.vm.$nextTick();

		expect(wrapper.vm.isLoading).toBeFalsy();
	});
	it('should call bugsnagClient and alert when got error response from api', async () => {
		axios.mockReject('error');
		await wrapper.vm.$nextTick();

		expect(global.bugsnagClient.notify).toBeCalled();
		expect(global.alert).toBeCalled();
	});

	it('should call getJobs when pageChanged method called', () => {
		wrapper.vm.pageChanged(2);

		expect(wrapper.vm.currentPage).toBe(2);
		expect(getJobsSpy).toHaveBeenCalled();
	});

	it('should open new window when openInputVacancy called', () => {
		wrapper.vm.openInputVacancy();

		expect(window.open).toBeCalled();
	});
});
