import { createLocalVue, shallowMount } from '@vue/test-utils';

import mockComponent from 'tests-fe/utils/mock-component';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

import OwnerListActive from 'Js/owner/components/OwnerListActive.vue';

describe('OwnerListActive.vue', () => {
	const localVue = createLocalVue();

	mockWindowProperty('navigator', {
		isMobile: true
	});
	mockWindowProperty('alert', jest.fn());

	// mock stubbed component
	const stubs = {
		alert: mockComponent
	};

	const $router = {
		push: jest.fn()
	};

	const propsData = {
		rooms: [
			{
				room_title: 'room_title',
				_id: 0,
				apartment_project_id: 0,
				kamar_available: 0,
				photo_url: {
					medium: '/path/to/medium/sized/image'
				},
				status_promote: 1,
				percen_promote: 90,
				progress_promote: 90,
				chat_history: {
					title: 'title_1',
					total: 1
				},
				view_history: {
					title: 'title_2',
					total: 2
				}
			},
			{
				room_title: 'room_title',
				_id: 1,
				apartment_project_id: 0,
				kamar_available: 0,
				photo_url: {
					medium: '/path/to/medium/sized/image'
				},
				status_promote: 1,
				percen_promote: 90,
				progress_promote: 90,
				chat_history: {
					title: 'title_1',
					total: 1
				},
				view_history: {
					title: 'title_2',
					total: 2
				}
			},
			{
				room_title: 'room_title',
				_id: 1,
				apartment_project_id: 1,
				kamar_available: 0,
				photo_url: {
					medium: '/path/to/medium/sized/image'
				},
				status_promote: 1,
				percen_promote: 90,
				progress_promote: 90,
				chat_history: {
					title: 'title_1',
					total: 1
				},
				view_history: {
					title: 'title_2',
					total: 2
				}
			}
		]
	};

	// mount function
	const mount = () => {
		return shallowMount(OwnerListActive, {
			localVue,
			stubs,
			propsData,
			mocks: {
				$router
			}
		});
	};

	describe('render component correctly', () => {
		let wrapper;

		beforeEach(() => {
			wrapper = mount();
		});

		it('should render container properly', () => {
			expect(wrapper.find('.card-container').exists()).toBeTruthy();
		});
	});

	describe('render computed value properly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = mount();
		});

		it('should return isMobile value properly', async () => {
			// default: truthy value
			expect(wrapper.vm.isMobile).toBeTruthy();

			// falsy value
			await mockWindowProperty('navigator', {
				isMobile: false
			});
			wrapper.destroy();
			wrapper = mount();
			expect(wrapper.vm.isMobile).toBeFalsy();
		});
	});

	describe('call functions correctly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = mount();
		});

		it('should call alert when clicking on a room card with 0 as its _id value', async () => {
			const cards = wrapper.findAll('.card-container');
			if (cards && cards.length >= 1) {
				const verifiedCard = cards.at(0);
				await verifiedCard.trigger('click');
				expect(global.alert).toBeCalled();
			}
		});

		it('should call $router.push when clicking on a room card with 0 as its apartment_project_id value', async () => {
			const cards = wrapper.findAll('.card-container');
			if (cards && cards.length >= 2) {
				const waitingCard = cards.at(1);
				await waitingCard.trigger('click');

				expect(wrapper.vm.$router.push).toBeCalled();
			}
		});

		it('should call $router.push when clicking on a room card with truthy apartment_project_id value', async () => {
			const cards = wrapper.findAll('.card-container');
			if (cards && cards.length >= 3) {
				const waitingCard = cards.at(2);
				await waitingCard.trigger('click');

				expect(wrapper.vm.$router.push).toBeCalled();
			}
		});
	});
});
