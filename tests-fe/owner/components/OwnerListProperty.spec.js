import { createLocalVue, shallowMount } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import OwnerListProperty from 'Js/owner/components/OwnerListProperty.vue';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import mockComponent from 'tests-fe/utils/mock-component';
import mockAxios from 'tests-fe/utils/mock-axios';
import ownerStore from 'tests-fe/owner/components/__mocks__/ownerStore.js';
import Vuex from 'vuex';

window.oxWebUrl = 'local.mamikos.com';

const swal = jest.fn(() => {
	return new Promise((resolve, reject) => {
		resolve();
	});
});

global.bugsnagClient = { notify: jest.fn() };
global.alert = jest.fn();
global.axios = mockAxios;
global.swal = swal;

mockWindowProperty('open', jest.fn());
mockWindowProperty('location.reload', jest.fn());

const localVue = createLocalVue();
localVue.use(Vuex);

const stubs = {
	MamiLoadingInline: mockComponent
};

const store = new Vuex.Store({
	...ownerStore,
	getters: {
		invoiceUrl: () => {
			return 'https://mamikos.com';
		}
	}
});

const propsData = {
	room: {
		_id: 0,
		name: 'room1',
		room_title: 'roomtitle1',
		unique_code: 123,
		hidden: true,
		photo_url: {
			small: 'small.jpeg'
		},
		permissions: ['write:room', 'read:room']
	},
	propertyType: 'kos'
};

const openSwalLoading = jest.fn();
const closeSwalLoading = jest.fn();

const generateWrapper = (isOpenModalAdd = null) => {
	return shallowMount(OwnerListProperty, {
		localVue,
		stubs,
		store,
		propsData,
		mocks: {
			openSwalLoading,
			closeSwalLoading,
			$router: {
				push: jest.fn()
			}
		}
	});
};

describe('OwnerListProperty.vue', () => {
	const wrapper = generateWrapper();

	const spyCommit = jest.spyOn(wrapper.vm.$store, 'commit');
	const getStatisticsByTimeSpy = jest.spyOn(
		OwnerListProperty.methods,
		'getStatisticsByTime'
	);

	it('Should mounts properly', () => {
		expect(wrapper.find('.desktop-height').exists()).toBe(true);
	});

	it('should return simple data properly', () => {
		expect(wrapper.vm.propertyTypeString).toBe('Kos');
		expect(wrapper.vm.token).toBe('mocktoken12345');
		expect(wrapper.vm.membershipStatus).toBe('Premium');
		expect(wrapper.vm.isMobile).toBeFalsy();
		expect(wrapper.vm.roomTitle).toBe('roomtitle1 123');
		expect(wrapper.vm.tingkatkanKlikOnNonPremium).toBe('#');
		expect(wrapper.vm.isOnBScenario).toBeFalsy();
		expect(wrapper.vm.invoiceUrl).toBe('https://mamikos.com');
	});

	it('should return roomGender properly', () => {
		const testCases = [
			{
				gender: 0,
				text: 'Kost Campur',
				class: 'gender-mx'
			},
			{
				gender: 1,
				text: 'Kost Putra',
				class: 'gender-m'
			},
			{
				gender: 2,
				text: 'Kost Putri',
				class: 'gender-f'
			}
		];

		for (const test of testCases) {
			const wrapper = generateWrapper();
			const room = { ...wrapper.vm.room, gender: test.gender };
			wrapper.setProps({
				room: room
			});

			expect(wrapper.vm.roomGender).toEqual({
				text: test.text,
				class: test.class
			});
		}
	});

	it('should return roomStatus properly', () => {
		const testCases = [
			{
				status_kos: 'add',
				propertyType: 'kos',
				bbk_request_status: 'waiting',
				class: 'unverified-waiting',
				text: 'Diperiksa Admin'
			},
			{
				status_kos: 'add',
				propertyType: 'kos',
				bbk_request_status: 'approve',
				class: 'unverified-waiting',
				text: 'Diperiksa Admin'
			},
			{
				status_kos: 'add',
				propertyType: 'kos',
				bbk_request_status: 'rejected',
				class: 'bbk-incomplete-data',
				text: 'Lengkapi Data Pribadi'
			},
			{
				status_kos: 'add',
				propertyType: 'apartment',
				bbk_request_status: 'approve',
				class: 'unverified-waiting',
				text: 'Diperiksa Admin'
			},
			{
				status_kos: 'verified',
				propertyType: null,
				bbk_request_status: null,
				class: 'verified',
				text: 'Aktif'
			}
		];

		for (const test of testCases) {
			const wrapper = generateWrapper();
			const room = {
				...wrapper.vm.room,
				hidden: false,
				bbk_request_status: test.bbk_request_status,
				status_kos: test.status_kos
			};
			wrapper.setProps({
				room: room
			});
			wrapper.setData({
				propertyType: test.propertyType
			});

			expect(wrapper.vm.roomStatus).toEqual({
				class: test.class,
				text: test.text
			});
		}
	});

	it('should return tingkatkanKlikOnNonPremium properly', () => {
		let wrapper = generateWrapper();

		let abTestResult = {
			is_active: true,
			experiment_id: 2,
			experiment_value: 'variant'
		};
		wrapper.vm.$store.state.abTestResult = abTestResult;
		expect(wrapper.vm.tingkatkanKlikOnNonPremium).toBe(
			'/ownerpage/premium/balance'
		);

		wrapper = generateWrapper();
		const room = { ...wrapper.vm.room, permissions: 'write:click' };
		abTestResult = {
			is_active: true,
			experiment_id: 2,
			experiment_value: 'control'
		};
		wrapper.vm.$store.state.abTestResult = abTestResult;
		wrapper.setProps({
			room
		});
		expect(wrapper.vm.tingkatkanKlikOnNonPremium).toBe(
			'/promosi-kost/premium-package'
		);
	});

	it('method getStatisticsByTime should call get data when success response from api but empty', async () => {
		const room = {
			name: 'room1',
			love_count: 2,
			view_count: 100,
			survey_count: 50,
			message_count: 500,
			review_count: 20
		};

		axios.mockResolve({
			status: true,
			data: {
				show: 20,
				room
			}
		});
		await wrapper.vm.getStatisticsByTime(3);

		expect(openSwalLoading).toBeCalled();
		expect(closeSwalLoading).toBeCalled();
		expect(global.bugsnagClient.notify).not.toBeCalled();
	});

	it('method getStatisticsByTime should call bugsnagClient when got error response from api', async () => {
		axios.mockReject('error');
		await wrapper.vm.getStatisticsByTime(3);

		expect(closeSwalLoading).toBeCalled();
	});

	it('method openRoom should push router with name apartment_update when property type is apartment', async () => {
		wrapper.setProps({
			propertyType: 'apartment'
		});
		await wrapper.vm.$nextTick();
		wrapper.vm.openRoom();

		expect(spyCommit).toBeCalled();
		expect(wrapper.vm.$router.push).toBeCalledWith({
			name: 'apartment_update'
		});
	});

	it('method openRoom should change window location ', async () => {
		mockWindowProperty('navigator.isMobile', true);
		mockWindowProperty('window.innerWidth', 360);
		let wrapper = generateWrapper();
		await wrapper.vm.$nextTick();
		wrapper.vm.openRoom();

		expect(spyCommit).toBeCalled();
		expect(window.location.href).toBe('http://localhost/');

		mockWindowProperty('navigator.isMobile', false);
		wrapper = generateWrapper();
		await wrapper.vm.$nextTick();
		wrapper.vm.openRoom();

		expect(spyCommit).toBeCalled();
		expect(window.location.href).toBe('http://localhost/');
	});

	it('method openPromo should call window open', () => {
		wrapper.vm.openPromo();

		expect(spyCommit).toBeCalled();
		expect(window.open).toBeCalled();
	});

	it('method openPaymentPage should call properly', () => {
		const testCases = [
			{
				experiment_value: 'variant',
				status: 'Menunggu Pembayaran',
				messageText: 'Silakan konfirmasi pembayaran anda terlebih dahulu'
			},
			{
				experiment_value: 'control',
				status: 'Konfirmasi Pembayaran',
				messageText: 'Silakan konfirmasi pembayaran anda terlebih dahulu'
			},
			{
				experiment_value: 'control',
				status: 'Menunggu Pembayaran',
				messageText:
					'Mohon maaf, proses pembayaran paket anda sedang menunggu verifikasi admin.'
			}
		];

		let abTestResult = {
			is_active: true,
			experiment_id: 2,
			experiment_value: ''
		};

		for (const test of testCases) {
			const wrapper = generateWrapper();
			abTestResult = {
				...abTestResult,
				experiment_value: test.experiment_value
			};
			wrapper.vm.$store.state.abTestResult = abTestResult;
			wrapper.vm.openPaymentPage(test.status);

			expect(swal).toBeCalledWith({
				confirmButtonColor: '#27ab27',
				confirmButtonText: 'OK',
				text: test.messageText,
				title: '',
				type: 'info'
			});
			expect(window.open).toBeCalled();
		}
	});

	it('method deleteThisRoomConfirmation should call properly', async () => {
		wrapper.vm.deleteThisRoomConfirmation();

		expect(swal).toBeCalledWith(
			'Tunggu sebentar',
			'Data anda sedang diolah oleh server',
			'warning'
		);

		const room = { ...wrapper.vm.room, _id: 1 };

		wrapper.setProps({
			room: room
		});
		await wrapper.vm.$nextTick();

		wrapper.vm.deleteThisRoomConfirmation();

		expect(swal).toBeCalled();
	});

	it('method deleteThisRoom should handle properly when succeed', async () => {
		axios.mockResolve({
			data: {
				status: true
			}
		});
		await wrapper.vm.deleteThisRoom();

		expect(location.reload).toBeCalled();
	});

	it('method deleteThisRoom should handle properly when response status is false', async () => {
		axios.mockResolve({
			data: {
				status: false
			}
		});
		await wrapper.vm.deleteThisRoom();

		expect(swal).toBeCalledWith('Gagal', 'Gagal menghapus data :(', 'error');
	});

	it('method deleteThisRoom should handle properly when failed response', async () => {
		axios.mockReject('error');
		await wrapper.vm.deleteThisRoom();

		expect(closeSwalLoading).toBeCalled();
	});

	it('should watch statisticId changes', () => {
		wrapper.setData({ statisticId: 6 });
		wrapper.vm.$nextTick(() => {
			expect(getStatisticsByTimeSpy).toHaveBeenCalled();
		});
	});
});
