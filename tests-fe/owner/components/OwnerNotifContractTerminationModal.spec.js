import { createLocalVue, shallowMount } from '@vue/test-utils';
import OwnerNotifContractTerminationModal from 'Js/owner/components/OwnerNotifContractTerminationModal';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import { flushPromises } from 'tests-fe/utils/promises';

const localVue = createLocalVue();
mockVLazy(localVue);

global.bugsnagClient = { notify: jest.fn() };

const windowOpen = jest.fn();
global.open = windowOpen;

global.axios = {
	get: jest.fn()
};

describe('OwnerNotifContractTerminationModal.vue', () => {
	let wrapper;

	const toastedOptions = {
		duration: 5000,
		position: 'bottom-left',
		theme: 'toasted-primary'
	};

	const createWrapper = (isAllowReject = false) =>
		shallowMount(OwnerNotifContractTerminationModal, {
			localVue,
			propsData: {
				tenantName: 'tenant',
				roomNumber: 'kamar 21',
				reason: 'ingin pindah',
				showModal: true,
				terminationId: 99,
				redirectionPath: '/redirect-path',
				isAllowReject
			},
			mocks: {
				swalError: jest.fn(),
				$toasted: { show: jest.fn() }
			},
			stubs: {
				MamiButton: { template: `<button @click="$emit('click')" />` }
			}
		});

	beforeEach(() => {
		wrapper = createWrapper();
	});

	it('should render contract termination modal', () => {
		expect(wrapper.find('.contract-termination-modal').exists()).toBe(true);
	});

	it('should render data correctly', () => {
		expect(wrapper.find('.contract-termination-modal__title').text()).toEqual(
			expect.stringContaining('tenant')
		);

		expect(wrapper.find('.contract-termination-modal__title').text()).toEqual(
			expect.stringContaining('kamar 21')
		);

		expect(
			wrapper.find('.contract-termination-modal__body-reason').text()
		).toEqual(expect.stringContaining('ingin pindah'));
	});

	it('should show button reject correctly', async () => {
		await wrapper.setProps({ isAllowReject: true });
		expect(
			wrapper.find('.contract-terminate-action__button-reject').exists()
		).toBe(true);

		await wrapper.setProps({ isAllowReject: false });
		expect(
			wrapper.find('.contract-terminate-action__button-reject').exists()
		).toBe(false);
	});

	it('should close modal if it is not loading', () => {
		wrapper.setData({ isRejecting: false, isConfirming: false });
		wrapper.vm.handleCloseModal();
		expect(wrapper.emitted('close')[0]).toBeTruthy();
	});

	it('should not close modal if it is loading', () => {
		wrapper.setData({ isRejecting: true });
		wrapper.vm.handleCloseModal();
		expect(wrapper.emitted('close')).toBeFalsy();
	});

	describe('confirming', () => {
		let wrapper;
		const buttonConfirmSelector = '.contract-terminate-action__button-confirm';

		beforeEach(() => {
			wrapper = createWrapper();
		});

		it('should redirect to redirect path if termination confirmed', async () => {
			global.axios.get = jest.fn().mockResolvedValue({
				data: {
					status: true
				}
			});

			const confirmButton = wrapper.find(buttonConfirmSelector);
			await confirmButton.trigger('click');
			await flushPromises();

			expect(windowOpen).toBeCalledWith('/redirect-path');
			expect(wrapper.emitted('close')[0]).toBeTruthy();
		});

		it('should show error when api status return false', async () => {
			global.axios.get = jest.fn().mockResolvedValue({
				data: {
					status: false,
					meta: {
						message: 'message from api'
					}
				}
			});

			const spyToasted = jest.spyOn(wrapper.vm.$toasted, 'show');

			const confirmButton = wrapper.find(buttonConfirmSelector);
			await confirmButton.trigger('click');
			await flushPromises();

			expect(spyToasted).toBeCalledWith('message from api', toastedOptions);
			spyToasted.mockRestore();
		});

		it('should call bugsnag when there is error', async () => {
			global.axios.get = jest.fn().mockRejectedValue(new Error('error'));
			const spyNotify = jest.spyOn(global.bugsnagClient, 'notify');

			const confirmButton = wrapper.find(buttonConfirmSelector);
			await confirmButton.trigger('click');
			await flushPromises();

			expect(spyNotify).toBeCalled();
			spyNotify.mockRestore();
		});

		it('should not proceed confirmation if it is still confirming', async () => {
			jest.clearAllMocks();
			const spyAxiosGet = jest.spyOn(global.axios, 'get');
			await wrapper.setData({ isConfirming: true });
			const confirmButton = wrapper.find(buttonConfirmSelector);
			await confirmButton.trigger('click');
			await flushPromises();

			expect(spyAxiosGet).not.toBeCalled();
			spyAxiosGet.mockRestore();
		});
	});

	describe('rejecting', () => {
		let wrapper;
		const buttonRejectSelector = '.contract-terminate-action__button-reject';

		beforeEach(() => {
			wrapper = createWrapper(true);
		});

		it('should emit close when success rejected', async () => {
			global.axios.post = jest.fn().mockResolvedValue({
				data: {
					status: true
				}
			});

			const rejectButton = wrapper.find(buttonRejectSelector);
			await rejectButton.trigger('click');
			await flushPromises();

			expect(wrapper.emitted('close')[0]).toBeTruthy();
		});

		it('should show error when api status return false', async () => {
			global.axios.post = jest.fn().mockResolvedValue({
				data: {
					status: false,
					meta: {
						message: 'message from api'
					}
				}
			});

			const spyToasted = jest.spyOn(wrapper.vm.$toasted, 'show');

			const rejectButton = wrapper.find(buttonRejectSelector);
			await rejectButton.trigger('click');
			await flushPromises();

			expect(spyToasted).toBeCalledWith('message from api', toastedOptions);
			spyToasted.mockRestore();
		});

		it('should call bugsnag when there is error', async () => {
			global.axios.post = jest.fn().mockRejectedValue(new Error('error'));
			const spyNotify = jest.spyOn(global.bugsnagClient, 'notify');

			const rejectButton = wrapper.find(buttonRejectSelector);
			await rejectButton.trigger('click');
			await flushPromises();

			expect(spyNotify).toBeCalled();
			spyNotify.mockRestore();
		});

		it('should not proceed confirmation if it is still confirming', async () => {
			jest.clearAllMocks();
			const spyAxiosPost = jest.spyOn(global.axios, 'post');
			await wrapper.setData({ isConfirming: true });
			const rejectButton = wrapper.find(buttonRejectSelector);
			await rejectButton.trigger('click');
			await flushPromises();

			expect(spyAxiosPost).not.toBeCalled();
			spyAxiosPost.mockRestore();
		});
	});
});
