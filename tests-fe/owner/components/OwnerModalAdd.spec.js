import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

import OwnerModalAdd from 'Js/owner/components/OwnerModalAdd.vue';

const profile = {
	membership: {
		is_mamipay_user: true
	},
	user: { phone_number: 1 }
};

describe('OwnerModalAdd.vue', () => {
	const localVue = createLocalVue();
	localVue.use(Vuex);

	mockWindowProperty('open', jest.fn());
	mockWindowProperty(
		'$',
		jest.fn().mockReturnValue({
			modal: jest.fn()
		})
	);

	const storeData = {
		state: {
			token: 'token',
			profile,
			bbkKosStatus: { approve: 1, reject: 1, other: 1, waiting: 1 }
		}
	};

	const mocks = {
		$emit: jest.fn()
	};

	// mount function
	const mount = () => {
		return shallowMount(OwnerModalAdd, {
			localVue,
			store: new Vuex.Store(storeData),
			mocks
		});
	};

	describe('render component correctly', () => {
		let wrapper;

		beforeEach(() => {
			wrapper = mount();
		});

		it('should render container properly', () => {
			expect(wrapper.find('#ownerModalAdd').exists()).toBeTruthy();
		});
	});

	describe('render computed value properly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = mount();
		});

		it('should return token value properly', () => {
			expect(wrapper.vm.token).toBe('token');
		});

		it('should return profile value properly', () => {
			expect(wrapper.vm.profile).toEqual(profile);
		});
	});

	describe('call functions correctly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = mount();
		});

		it('should call $() when calling openModal method', async () => {
			await wrapper.vm.openModal();
			expect(global.$).toBeCalled();
			expect(wrapper.vm.listingType).toBe('Kost');

			await wrapper.vm.openModal(true);
			expect(wrapper.vm.listingType).toBe(true);
		});

		it('should call $() when calling closeModal method', async () => {
			await wrapper.vm.closeModal();

			expect(global.$).toBeCalled();
		});

		it('should call window.open when calling openInputVacancy method', async () => {
			await wrapper.vm.openInputVacancy();

			expect(window.open).toBeCalled();
		});

		it('should call $emit and closeModal when calling submitInputProperty method', async () => {
			const emitSpy = jest.spyOn(wrapper.vm, '$emit');
			const closeModalSpy = jest.spyOn(wrapper.vm, 'closeModal');

			await wrapper.vm.submitInputProperty();

			expect(emitSpy).toBeCalled();
			expect(closeModalSpy).toBeCalled();

			emitSpy.mockRestore();
			closeModalSpy.mockRestore();
		});
	});
});
