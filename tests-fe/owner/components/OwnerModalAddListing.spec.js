import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

import OwnerModalAddListing from 'Js/owner/components/OwnerModalAddListing.vue';

describe('OwnerModalAddListing.vue', () => {
	const localVue = createLocalVue();
	localVue.use(Vuex);

	mockWindowProperty('navigator', {
		isMobile: true
	});
	mockWindowProperty(
		'$',
		jest.fn().mockReturnValue({
			modal: jest.fn()
		})
	);

	const storeData = {
		state: {
			token: 'token',
			authData: {
				all: {
					phone_number: '081234567890'
				}
			}
		}
	};

	// mount function
	const mount = () => {
		return shallowMount(OwnerModalAddListing, {
			localVue,
			store: new Vuex.Store(storeData)
		});
	};

	describe('render component correctly', () => {
		let wrapper;

		beforeEach(() => {
			wrapper = mount();
		});

		it('should render container properly', () => {
			expect(wrapper.find('#ownerModalAddListing').exists()).toBeTruthy();
		});
	});

	describe('render computed value properly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = mount();
		});

		it('should return token value properly', () => {
			expect(wrapper.vm.token).toBe('token');
		});

		it('should return authData value properly', () => {
			expect(wrapper.vm.authData).toEqual({
				all: {
					phone_number: '081234567890'
				}
			});
		});
	});

	describe('call functions correctly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = mount();
		});

		it('should call $() when calling openModal method', async () => {
			await wrapper.vm.openModal();

			expect(global.$).toBeCalled();
		});

		it('should call $() when calling closeModal method', async () => {
			await wrapper.vm.closeModal();

			expect(global.$).toBeCalled();
		});
	});
});
