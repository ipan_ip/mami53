import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

import mockAxios from 'tests-fe/utils/mock-axios';
import OwnerModalEdit from 'Js/owner/components/OwnerModalEdit.vue';

describe('OwnerModalEdit.vue', () => {
	const localVue = createLocalVue();
	localVue.use(Vuex);

	mockWindowProperty('open', jest.fn());
	mockWindowProperty(
		'$',
		jest.fn().mockReturnValue({
			modal: jest.fn(),
			on: jest.fn((eventName, cb) => cb && cb())
		})
	);

	global.axios = mockAxios;
	global.confirm = jest.fn().mockReturnValue(true);
	global.bugsnagClient = { notify: jest.fn() };
	global.alert = jest.fn();

	const storeData = {
		state: {
			token: 'token'
		}
	};

	const propsData = {
		propertyType: 'kos',
		room: {
			_id: '1',
			kamar_available: 2,
			price_type: 'idr',
			price_monthly_time: 100000,
			price_monthly_usd: 10,
			price_daily: 100000,
			price_daily_usd: 10,
			price_weekly_time: 100000,
			price_weekly_usd: 10,
			price_yearly_time: 100000,
			price_yearly_usd: 10
		}
	};

	const mocks = {
		$emit: jest.fn(),
		$router: {
			push: jest.fn()
		}
	};

	const methods = {
		openSwalLoading: jest.fn(),
		closeSwalLoading: jest.fn(),
		swalError: jest.fn(),
		swalSuccessTimer: jest.fn()
	};

	// mount function
	const mount = options => {
		return shallowMount(OwnerModalEdit, {
			...{
				localVue,
				mocks,
				propsData,
				methods,
				store: new Vuex.Store(storeData)
			},
			...options
		});
	};

	describe('render component correctly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = mount();
		});

		it('should render container properly', () => {
			expect(wrapper.find('#ownerModalEdit').exists()).toBeTruthy();
		});

		it('should render function in the mounted block properly', async () => {
			expect(wrapper.vm.priceStep).toBe(1000);
			expect(wrapper.vm.price.monthly).toBe(100000);
			expect(wrapper.vm.price.daily).toBe(100000);
			expect(wrapper.vm.price.weekly).toBe(100000);
			expect(wrapper.vm.price.yearly).toBe(100000);

			await wrapper.destroy();
			wrapper = mount({
				propsData: {
					propertyType: 'kos',
					room: {
						_id: '1',
						kamar_available: 2,
						price_type: 'usd',
						price_monthly_time: 100000,
						price_monthly_usd: 10,
						price_daily: 100000,
						price_daily_usd: 10,
						price_weekly_time: 100000,
						price_weekly_usd: 10,
						price_yearly_time: 100000,
						price_yearly_usd: 10
					}
				}
			});

			expect(wrapper.vm.priceStep).toBe(1);
			expect(wrapper.vm.price.monthly).toBe(10);
			expect(wrapper.vm.price.daily).toBe(10);
			expect(wrapper.vm.price.weekly).toBe(10);
			expect(wrapper.vm.price.yearly).toBe(10);
		});
	});

	describe('render computed value properly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = mount();
		});

		it('should return token value properly', () => {
			expect(wrapper.vm.token).toBe('token');
		});

		it('should return propertyTypeTitle value properly', async () => {
			await wrapper.setProps({
				propertyType: 'test'
			});
			expect(wrapper.vm.propertyTypeTitle).toBeFalsy();

			await wrapper.setProps({
				propertyType: 'kos'
			});
			expect(wrapper.vm.propertyTypeTitle).toBe('Kost');

			await wrapper.setProps({
				propertyType: 'apartment'
			});
			expect(wrapper.vm.propertyTypeTitle).toBe('Apartemen');
		});

		it('should return isRadio1Active value properly', async () => {
			await wrapper.setData({
				roomAvailable: 0
			});
			expect(wrapper.vm.isRadio1Active).toBeTruthy();

			await wrapper.setData({
				roomAvailable: 1
			});
			expect(wrapper.vm.isRadio1Active).toBeFalsy();
		});

		it('should return isRadio2Active value properly', async () => {
			await wrapper.setData({
				roomAvailable: 1
			});
			expect(wrapper.vm.isRadio2Active).toBeTruthy();

			await wrapper.setData({
				roomAvailable: 0
			});
			expect(wrapper.vm.isRadio2Active).toBeFalsy();
		});
	});

	describe('call functions in the watch block correctly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = mount();
		});

		it('should call $emit on isClosed value change, and the value is truthy ', async () => {
			const emitSpy = jest.spyOn(wrapper.vm, '$emit');
			await wrapper.setData({
				isClosed: false
			});
			await wrapper.setData({
				isClosed: true
			});

			expect(emitSpy).toBeCalled();
			emitSpy.mockRestore();
		});

		it('should call $emit on isClosed value change, and the value is falsy', async () => {
			const emitSpy = jest.spyOn(wrapper.vm, '$emit');
			await wrapper.setData({
				isClosed: true
			});
			await wrapper.setData({
				isClosed: false
			});

			expect(emitSpy).toBeCalled();
			emitSpy.mockRestore();
		});
	});

	describe('call functions correctly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = mount();
		});

		it('should call $() when calling openModal method', async () => {
			await wrapper.vm.openModal();
			expect(global.$).toBeCalled();
		});

		it('should call $() when calling closeModal method', async () => {
			await wrapper.vm.closeModal();
			expect(global.$).toBeCalled();
		});

		it('should not call openSwalLoading when calling deleteRoom method, and the confirmation was not confirmed', async () => {
			global.confirm = jest.fn().mockReturnValue(false);
			const openSwalLoadingSpy = jest.spyOn(wrapper.vm, 'openSwalLoading');
			await wrapper.vm.deleteRoom();
			expect(openSwalLoadingSpy).not.toBeCalled();
			openSwalLoadingSpy.mockRestore();
		});

		it('should set isChanged value and call swalSuccessTimer when calling submitRoomPrice method on successful api request', async () => {
			const swalSuccessTimerSpy = jest.spyOn(wrapper.vm, 'swalSuccessTimer');
			await axios.mockResolve({
				data: {
					status: true
				}
			});
			await wrapper.vm.submitRoomPrice();
			expect(swalSuccessTimerSpy).toBeCalled();
			expect(wrapper.vm.isChanged).toBeTruthy();
			swalSuccessTimerSpy.mockRestore();
		});

		it('should not set isChanged value when calling submitRoomPrice method on successful api request, but received falsy status', async () => {
			const swalSuccessTimerSpy = jest.spyOn(wrapper.vm, 'swalSuccessTimer');
			await axios.mockResolve({
				data: {
					status: false,
					meta: {
						message: 'test'
					}
				}
			});
			await wrapper.vm.submitRoomPrice();
			expect(swalSuccessTimerSpy).not.toBeCalled();
			expect(wrapper.vm.isChanged).toBeFalsy();
			swalSuccessTimerSpy.mockRestore();
		});

		it('should call bugsnag, alert, and closeSwalLoading when calling submitRoomPrice method on failed api request', async () => {
			const closeSwalLoadingSpy = jest.spyOn(wrapper.vm, 'closeSwalLoading');
			await axios.mockReject('error');
			await wrapper.vm.submitRoomPrice();
			await wrapper.vm.$nextTick();

			expect(closeSwalLoadingSpy).toBeCalled();
			expect(global.alert).toBeCalled();
			expect(global.bugsnagClient.notify).toBeCalled();

			closeSwalLoadingSpy.mockRestore();
		});

		it('should call $router.push and swalSuccessTimer when calling deleteRoom method on successful api request, and the confirmation request was confirmed by user', async () => {
			global.confirm = jest.fn().mockReturnValue(true);
			const swalSuccessTimerSpy = jest.spyOn(wrapper.vm, 'swalSuccessTimer');
			await axios.mockResolve({
				data: {
					status: true
				}
			});
			await wrapper.vm.deleteRoom();
			expect(swalSuccessTimerSpy).toBeCalled();
			expect(wrapper.vm.$router.push).toBeCalled();
			swalSuccessTimerSpy.mockRestore();
		});

		it('should call swalError when calling deleteRoom method on successful api request and the confirmation was confirmed, but the api request status was false', async () => {
			global.confirm = jest.fn().mockReturnValue(true);
			const swalErrorSpy = jest.spyOn(wrapper.vm, 'swalError');
			await axios.mockResolve({
				data: {
					status: false,
					meta: {
						message: 'test'
					}
				}
			});
			await wrapper.vm.deleteRoom();
			expect(swalErrorSpy).toBeCalled();
			swalErrorSpy.mockRestore();
		});

		it('should call bugsnag, alert, and closeSwalLoading when calling deleteRoom method on failed api request, and the confirmation request was confirmed by user', async () => {
			global.confirm = jest.fn().mockReturnValue(true);
			const closeSwalLoadingSpy = jest.spyOn(wrapper.vm, 'closeSwalLoading');
			await axios.mockReject('error');
			await wrapper.vm.deleteRoom();
			await wrapper.vm.$nextTick();

			expect(closeSwalLoadingSpy).toBeCalled();
			expect(global.alert).toBeCalled();
			expect(global.bugsnagClient.notify).toBeCalled();

			closeSwalLoadingSpy.mockRestore();
		});
	});
});
