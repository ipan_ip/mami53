import 'tests-fe/utils/mock-vue';
import { createLocalVue, shallowMount } from '@vue/test-utils';
import { modal } from 'vue-strap';
import Vuex from 'vuex';
import defaultStore from './__mocks__/ownerStore';
import OwnerAutoBookingModal from 'Js/owner/components/OwnerAutoBookingModal';
import * as storage from 'Js/@utils/storage';

window.Vue = require('vue');
window.Vue.config.silent = true;

const MixinNavigatorIsMobile = require('Js/@mixins/MixinNavigatorIsMobile');

global.tracker = jest.fn();

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.mixin([MixinNavigatorIsMobile]);

const store = new Vuex.Store(defaultStore);

describe('OwnerAutoBookingModal.vue', () => {
	let wrapper;

	const trackActionButtonSpy = jest.spyOn(
		OwnerAutoBookingModal.methods,
		'trackActionButton'
	);

	beforeEach(() => {
		wrapper = shallowMount(OwnerAutoBookingModal, {
			localVue,
			store,
			stubs: {
				modal
			}
		});
	});

	it('Should renders properly', () => {
		expect(wrapper.find('.owner-auto-booking-modal').exists()).toBe(true);
	});

	it('Should emit closeRestrictionModal when modal is closed', () => {
		wrapper.vm.closeModal();

		expect(wrapper.emitted('update:showModal')).toBeTruthy();
	});

	it('Should track goToSection event properly', () => {
		const mockActionName = 'Mock Action Name';
		const mockMoEngageData = {
			interface: 'desktop',
			is_booking: wrapper.vm.isBBKActive,
			is_premium: wrapper.vm.isPremium
		};

		wrapper.vm.trackActionButton(mockActionName);

		expect(global.tracker).toBeCalledWith('moe', [
			`[Owner] Autobbk - Click ${mockActionName}`,
			mockMoEngageData
		]);
	});

	it('should update available room properly when calling changeRoomAvailability method', () => {
		const spyStorageLocalSetItem = jest.spyOn(storage.local, 'setItem');

		wrapper.vm.goToMamipayActivationIntro();

		expect(spyStorageLocalSetItem).toHaveBeenCalled();
		expect(trackActionButtonSpy).toHaveBeenCalled();
	});

	it('Should return is computed value properly', () => {
		expect(wrapper.vm.isPremium).toBeTruthy();

		const payload = null;
		wrapper.vm.$store.commit('setProfile', payload);

		expect(wrapper.vm.isPremium).toBeFalsy();
		expect(wrapper.vm.isBBKActive).toBeFalsy();
	});
});
