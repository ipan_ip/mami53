import { createLocalVue, shallowMount } from '@vue/test-utils';
import OwnerSettings from 'Js/owner/components/OwnerSettings.vue';
import trackerMixin from 'Js/owner/mixins/OwnerMixinSettingsTrackers';
import mockComponent from 'tests-fe/utils/mock-component';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import Vuex from 'vuex';
import ownerStore from 'tests-fe/owner/components/__mocks__/ownerStore.js';

const localVue = createLocalVue();
localVue.mixin([trackerMixin]);
localVue.use(Vuex);

const stubs = {
	BgToast: mockComponent,
	PersonalInfo: mockComponent,
	NotificationSettings: mockComponent
};

const Cookies = {
	set: jest.fn(),
	getJSON: jest.fn(),
	remove: jest.fn()
};
const reload = jest.fn();

window.oxOauth2Domain = 'mamikos.com';

global.Cookies = Cookies;
global.tracker = jest.fn();
global.Moengage = { track_event: jest.fn() };
const sb = {
	getTotalUnreadChannelCount: jest.fn(fn => fn(3, null)),
	disconnect: jest.fn(fn => fn())
};
global.sb = sb;

mockWindowProperty('location', { reload: reload, href: '/someslug' });

describe('OwnerSettings.vue', () => {
	const showSuccessToastSpy = jest.spyOn(
		OwnerSettings.methods,
		'showSuccessToast'
	);
	let wrapper;

	beforeEach(() => {
		wrapper = shallowMount(OwnerSettings, {
			localVue,
			store: new Vuex.Store(ownerStore),
			stubs
		});
	});

	it('Should mounts properly', () => {
		expect(wrapper.find('.owner-settings').exists()).toBe(true);
	});

	it('Should show toast on created if there was previously a successful settings update activity', () => {
		/* eslint-disable camelcase */
		const owner_settings_update = {
			message: 'tes'
		};

		global.Cookies.getJSON = jest.fn(() => owner_settings_update);
		wrapper.vm.checkPreviousSettingsUpdate();
		expect(showSuccessToastSpy).toBeCalled();
		expect(Cookies.remove).toBeCalled();
	});

	it('Should set activity cookies after a successful settings update', () => {
		const config = {
			needReload: true,
			message: 'tes'
		};

		wrapper.vm.handleSettingsUpdateSucceeded(config);
		expect(Cookies.set).toBeCalled();
		expect(reload).toBeCalled();
	});

	it('Should show toast immediately after a successful settings update that doesnt need page reload', () => {
		const config = {
			needReload: false,
			message: 'tes'
		};

		wrapper.vm.handleSettingsUpdateSucceeded(config);
		expect(showSuccessToastSpy).toBeCalled();
	});

	it('should disconnect sb when logout is clicked', () => {
		wrapper.vm.$store.state.authCheck.all = true;
		wrapper.vm.$store.state.authCheck.user = true;
		wrapper.vm.handleLogout();
		expect(sb.disconnect).toBeCalled();
	});

	it('should call Moengage when calling handleLogout method', () => {
		wrapper.vm.handleLogout();
		expect(Moengage.track_event).toBeCalled();
	});

	it('should remove oauth token on cookies when calling handleLogout method', () => {
		wrapper.vm.handleLogout();
		expect(Cookies.remove).toBeCalled();
	});
});
