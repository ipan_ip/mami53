import { createLocalVue, shallowMount } from '@vue/test-utils';
import OwnerVacancyDetail from 'Js/owner/components/OwnerVacancyDetail.vue';
import mockComponent from 'tests-fe/utils/mock-component';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import mockAxios from 'tests-fe/utils/mock-axios';
import ownerStore from 'tests-fe/owner/components/__mocks__/ownerStore.js';
import Vuex from 'vuex';

const localVue = createLocalVue();
localVue.use(Vuex);

global.alert = jest.fn();
global.axios = mockAxios;

mockWindowProperty('bugsnagClient', {
	notify: jest.fn()
});
mockWindowProperty('postUrl', '');

const stubs = {
	MamiLoadingInline: mockComponent,
	RouterLink: mockComponent,
	OwnerDetailBack: mockComponent,
	OwnerModalEditVacancy: mockComponent
};
const openSwalLoading = jest.fn();
const closeSwalLoading = jest.fn();
const store = new Vuex.Store({ ...ownerStore });

describe('OwnerVacancyDetail.vue', () => {
	let wrapper;

	const getDetailSpy = jest.spyOn(OwnerVacancyDetail.methods, 'getDetail');

	beforeEach(() => {
		wrapper = shallowMount(OwnerVacancyDetail, {
			localVue,
			stubs,
			store,
			mocks: {
				openSwalLoading,
				closeSwalLoading,
				$router: { push: jest.fn() },
				$route: { params: { slug: 'mock-slug' } }
			}
		});
	});

	it('Should mounts properly', () => {
		expect(wrapper).toMatchSnapshot();
		expect(wrapper.find('.stretch').exists()).toBe(true);
	});

	it('Should return base computed value properly', () => {
		expect(wrapper.vm.token).toBe('mocktoken12345');
		expect(wrapper.vm.slug).toBe('mock-slug');
	});

	it('should call all method when the component rendered', async () => {
		expect(getDetailSpy).toHaveBeenCalled();
	});

	it('should call bugsnagClient and alert when calling getDetail method on failed response', async () => {
		axios.mockReject('error');
		await wrapper.vm.getDetail();

		expect(global.bugsnagClient.notify).toBeCalled();
		expect(global.alert).toBeCalled();
	});

	it('should push to another page when calling openStatistic properly', () => {
		wrapper.vm.openStatistic();

		expect(wrapper.vm.$router.push).toBeCalledWith({
			name: 'vacancy_statistics',
			params: { seq: '1' }
		});
	});

	it('should call getDetail when calling handleModalEditVacancyClose', () => {
		const state = {
			closed: true,
			changed: true
		};
		wrapper.vm.handleModalEditVacancyClose(state);

		expect(getDetailSpy).toHaveBeenCalled();
	});

	it('should toggle openModal ownerModalEditVacancy when calling editVacancy method', async () => {
		wrapper.vm.$refs = {
			ownerModalEditVacancy: {
				openModal: jest.fn()
			}
		};
		await wrapper.vm.editVacancy();

		expect(wrapper.vm.$refs.ownerModalEditVacancy.openModal).toBeCalled();
	});

	it('Watch report type to get postUrl should handle correctly', async () => {
		await wrapper.setData({
			report: {
				type: '5',
				apply: null,
				postUrl: ''
			}
		});

		expect(wrapper.vm.postUrl).toBe('/report');

		await wrapper.setData({
			report: {
				type: '1',
				apply: null,
				postUrl: ''
			}
		});

		expect(wrapper.vm.postUrl).toBe('/report?type=1');
	});

	it('Watch to get response should call bugsnagClient when failed response', async () => {
		expect(openSwalLoading).toBeCalled();

		axios.mockReject('error');
		await wrapper.vm.$nextTick();

		expect(closeSwalLoading).toBeCalled();
		expect(global.bugsnagClient.notify).toBeCalled();
	});

	it('Watch should get response when suceessed response', async () => {
		wrapper.setData({
			report: {
				type: '5',
				apply: null,
				postUrl: ''
			}
		});
		axios.mockResolve({
			data: {
				status: true,
				report: {
					apply: 'mock apply'
				}
			}
		});
		await wrapper.vm.$nextTick();

		expect(closeSwalLoading).toBeCalled();
		expect(wrapper.vm.report.apply).toEqual('mock apply');
	});
});
