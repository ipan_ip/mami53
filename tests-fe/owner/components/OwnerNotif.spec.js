import 'tests-fe/utils/mock-vue';
import { shallowMount, createLocalVue } from '@vue/test-utils';
import OwnerNotif from 'Js/owner/components/OwnerNotif';
import { flushPromises } from 'tests-fe/utils/promises';
import Vuex from 'vuex';
import notifData from './__mocks__/notif-data.json';

global.axios = {
	get: jest.fn().mockResolvedValue({ data: { status: true, data: notifData } })
};

const bugsnagClient = { notify: jest.fn() };
global.bugsnagClient = bugsnagClient;
global.alert = jest.fn();
global.open = jest.fn();

const store = {
	state: {
		profile: {
			membership: {
				expired: true
			}
		}
	},
	mutations: {
		setNotifCount: jest.fn()
	}
};

const $router = { push: jest.fn() };

describe('OwnerNotif.vue', () => {
	const localVue = createLocalVue();
	const OwnerNotifContractTerminationModal = {
		template: '<div class="terminate-modal" />'
	};

	const createWrapper = () =>
		shallowMount(OwnerNotif, {
			localVue,
			stubs: {
				MamiLoadingInline: { template: '<div class="mami-loading-inline"/>' },
				OwnerNotifContractTerminationModal
			},
			mocks: {
				openSwalLoading: jest.fn(),
				closeSwalLoading: jest.fn(),
				swalError: jest.fn(),
				$router
			},
			store: new Vuex.Store(store)
		});

	const wrapper = createWrapper();

	it('should render owner notif', () => {
		expect(wrapper.find('.owner-notif').exists()).toBe(true);
	});

	it('should render all notif data', async () => {
		await flushPromises();
		expect(wrapper.findAll('.notification-list').length).toBe(2);
	});

	it('should call bugsnag if catch an error from fetching notif', async () => {
		global.axios = { get: jest.fn().mockRejectedValue(new Error('error')) };
		const wrapper = createWrapper();
		await flushPromises();
		expect(wrapper.findAll('.notification-list').length).toBe(0);
		expect(bugsnagClient.notify).toBeCalled();
	});

	describe('notif type: review', () => {
		let wrapper;

		const notifData = { type: 'review', room_id: 123 };

		beforeEach(async () => {
			global.axios = {
				get: jest.fn().mockResolvedValue({
					data: {
						status: true,
						data: [notifData]
					}
				})
			};
			wrapper = createWrapper();
			await flushPromises();
		});

		it('should navigate to statistic page', () => {
			const spyOpen = jest.spyOn(global, 'open');
			wrapper
				.findAll('.notification-list')
				.at(0)
				.trigger('click');

			expect(spyOpen).toBeCalledWith('/ownerpage/statistics/123#review');
			spyOpen.mockRestore();
		});
	});

	describe('notif type: premium_*', () => {
		let wrapper;

		const notifData = { type: 'premium_verify' };

		beforeEach(async () => {
			global.axios = {
				get: jest.fn().mockResolvedValue({
					data: {
						status: true,
						data: [notifData]
					}
				})
			};
			wrapper = createWrapper();
			await flushPromises();
		});

		it('should navigate to /invoice', () => {
			wrapper
				.findAll('.notification-list')
				.at(0)
				.trigger('click');

			expect($router.push).toBeCalledWith({ path: '/invoice' });
		});
	});

	describe('notif type: balance_*', () => {
		let wrapper;

		const notifData = { type: 'balance_verify' };

		beforeEach(async () => {
			global.axios = {
				get: jest.fn().mockResolvedValue({
					data: {
						status: true,
						data: [notifData]
					}
				})
			};
			wrapper = createWrapper();
			await flushPromises();
		});

		it('should navigate to /premium/balance/confirmation', () => {
			wrapper
				.findAll('.notification-list')
				.at(0)
				.trigger('click');

			expect($router.push).toBeCalledWith({
				path: '/premium/balance/confirmation'
			});
		});
	});

	describe('notif type: contract_termination', () => {
		let wrapper;

		const contractTerminationData = {
			chat_data: null,
			datetime: 'Hari ini',
			is_read: false,
			photo: 'https://dummy-photo.com/dummy-photo.png',
			room_id: 1,
			room_name: 'Kost Test 1',
			scheme: 'scheme.com://test/1',
			survey_data: null,
			title: 'Terminate Contract',
			type: 'contract_termination',
			url: '/contract_termination/1',
			user_id: 1,
			user_name: 'Tenant Test'
		};

		beforeEach(async () => {
			global.axios = {
				get: jest.fn().mockResolvedValue({
					data: {
						status: true,
						data: [contractTerminationData]
					}
				})
			};
			wrapper = createWrapper();
			await flushPromises();
		});

		it('should open modal when notif clicked and status is waiting', async () => {
			global.axios.get = jest.fn().mockResolvedValue({
				data: {
					status: true,
					data: {
						contract_id: 1,
						terminate_request_id: 123,
						status: 'waiting',
						allow_to_reject: false,
						tenant_name: 'Tenant Name',
						room_number: '2',
						reason: 'Ingin Pindah'
					}
				}
			});
			const contractTerminationNotif = wrapper
				.findAll('.notification-list')
				.at(0);
			await contractTerminationNotif.trigger('click');
			await flushPromises();

			const terminateInfo = wrapper.vm.terminateInfo;

			expect(terminateInfo.terminationId).toBe(123);
			expect(terminateInfo.tenantName).toBe('Tenant Name');
			expect(terminateInfo.roomNumber).toBe('2');
			expect(terminateInfo.allowToReject).toBe(false);
			expect(terminateInfo.reason).toBe('Ingin Pindah');
			expect(terminateInfo.redirectionPath).toBe(contractTerminationData.url);
			expect(wrapper.vm.isShowContractTerminationModal).toBe(true);
		});

		it('should open notif url if status is not waiting', async () => {
			const spyOpen = jest.spyOn(global, 'open');
			global.axios.get = jest.fn().mockResolvedValue({
				data: {
					status: true,
					data: {
						contract_id: 1,
						terminate_request_id: 123,
						status: 'confirmed',
						allow_to_reject: false,
						tenant_name: 'Tenant Name',
						room_number: '2',
						reason: 'Ingin Pindah'
					}
				}
			});
			const contractTerminationNotif = wrapper
				.findAll('.notification-list')
				.at(0);
			await contractTerminationNotif.trigger('click');
			await flushPromises();

			expect(wrapper.vm.isShowContractTerminationModal).toBe(false);
			expect(spyOpen).toBeCalledWith(contractTerminationData.url);
			spyOpen.mockRestore();
		});

		it('should show swal error if there is error on fetching scheme data', async () => {
			const spySwalError = jest.spyOn(wrapper.vm, 'swalError');
			global.axios.get = jest.fn().mockResolvedValue({
				data: {
					status: false,
					meta: {
						message: 'status false'
					}
				}
			});
			wrapper
				.findAll('.notification-list')
				.at(0)
				.trigger('click');
			await flushPromises();

			expect(wrapper.vm.isShowContractTerminationModal).toBe(false);
			expect(spySwalError).toBeCalledWith(null, 'status false');

			// catch error
			const spyNotify = jest.spyOn(global.bugsnagClient, 'notify');
			global.axios.get = jest.fn().mockRejectedValue(new Error('error'));
			wrapper
				.findAll('.notification-list')
				.at(0)
				.trigger('click');
			await flushPromises();

			expect(wrapper.vm.isShowContractTerminationModal).toBe(false);
			expect(spySwalError).toHaveBeenCalled();
			expect(spyNotify).toHaveBeenCalled();
			spySwalError.mockRestore();
			spyNotify.mockRestore();
		});

		it('should reset terminate info when terminate modal closed', () => {
			wrapper.setData({
				terminateInfo: {
					terminationId: 123,
					tenantName: 'tenant name',
					roomNumber: 'room number',
					reason: 'reason',
					redirectionPath: '/',
					allowToReject: true
				}
			});
			wrapper
				.findComponent(OwnerNotifContractTerminationModal)
				.vm.$emit('close');

			expect(wrapper.vm.terminateInfo).toEqual({
				terminationId: 0,
				tenantName: '',
				roomNumber: '',
				reason: '',
				redirectionPath: '',
				allowToReject: false
			});
		});
	});

	describe('notif type: survey', () => {
		let wrapper;

		const notifData = {
			type: 'survey',
			user_name: 'user name',
			room_name: 'room name',
			room_id: 123
		};

		beforeEach(async () => {
			global.axios = {
				get: jest.fn().mockResolvedValue({
					data: {
						status: true,
						data: [notifData]
					}
				})
			};
			wrapper = createWrapper();
			await flushPromises();
		});

		it('should navigate to statistic page if expired', () => {
			const spyOpen = jest.spyOn(global, 'open');
			wrapper.vm.$store.state.profile.membership.expired = true;
			wrapper
				.findAll('.notification-list')
				.at(0)
				.trigger('click');

			expect(spyOpen).toBeCalledWith('/ownerpage/statistics/123#survey');
			spyOpen.mockRestore();
		});
	});

	describe('other type', () => {
		let wrapper;

		const notifData = { type: 'other type', url: '/other-type-url' };

		beforeEach(async () => {
			global.axios = {
				get: jest.fn().mockResolvedValue({
					data: {
						status: true,
						data: [notifData]
					}
				})
			};
			wrapper = createWrapper();
			await flushPromises();
		});

		it('should navigate to notif url', () => {
			const spyOpen = jest.spyOn(global, 'open');
			wrapper
				.findAll('.notification-list')
				.at(0)
				.trigger('click');

			expect(spyOpen).toBeCalledWith('/other-type-url');
			spyOpen.mockRestore();
		});
	});
});
