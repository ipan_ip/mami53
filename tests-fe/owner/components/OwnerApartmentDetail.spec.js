import { createLocalVue, shallowMount } from '@vue/test-utils';
import MamiLoadingInline from 'Js/@components/MamiLoadingInline';
import mockComponent from 'tests-fe/utils/mock-component';
import OwnerApartmentDetail from 'Js/owner/components/OwnerApartmentDetail.vue';

const ownerApartmentDetailData = {
	room: {
		room_title: 'room_title',
		_id: 0,
		photo_url: {
			medium: '/path/to/medium/sized/image'
		},
		price_type: 'idr',
		price_title_time: '200000',
		price_title_time_usd: '14.22'
	}
};

global.axios = {
	get: jest
		.fn()
		.mockResolvedValue({ status: true, data: ownerApartmentDetailData })
};
global.bugsnagClient = { notify: jest.fn() };
global.alert = jest.fn();

const localVue = createLocalVue();

const stubs = {
	OwnerDetailBack: mockComponent,
	MamiLoadingInline
};

const generateWrapper = () => {
	return shallowMount(OwnerApartmentDetail, {
		localVue,
		stubs,
		mocks: {
			$store: { commit: jest.fn() },
			$route: { params: { seq: 'test1' } }
		}
	});
};

describe('OwnerApartmentDetail.vue', () => {
	const wrapper = generateWrapper();

	it('Should mounts properly', () => {
		expect(wrapper.find('.stretch').exists()).toBe(true);
	});

	it('should render owner apartment detail data', async () => {
		await wrapper.vm.$nextTick();
		expect(wrapper.vm.detail).not.toBe({});
	});

	it('should return seq from params properly', () => {
		expect(wrapper.vm.seq).toBe('test1');
	});

	it('should call bugsnag if catch an error from fetching owner apartment detail data', async () => {
		global.axios = { get: jest.fn().mockRejectedValue(new Error('error')) };
		const wrapper = generateWrapper();
		await wrapper.vm.$nextTick();
		expect(alert).toBeCalledWith('Terjadi galat. Silakan coba lagi.');
		expect(bugsnagClient.notify).toBeCalled();
	});
});
