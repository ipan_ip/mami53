import { createLocalVue, shallowMount } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import OwnerChatWidget from 'Js/owner/components/OwnerChatWidget.vue';
import Vuex from 'vuex';

const localVue = createLocalVue();
localVue.use(Vuex);

const mockStore = {
	state: {},
	mutations: {}
};
const store = new Vuex.Store(mockStore);

global.startSendBirdWidget = jest.fn(cb => cb());
global.bugsnagClient = {
	notify: jest.fn()
};
global.isWidgetReady = false;
global.sbWidget = {
	registerLocalHandlerCb: jest.fn((type, cb) => cb())
};
global.document.querySelector = jest.fn().mockReturnValue({
	click: jest.fn(),
	style: {
		display: 'block'
	}
});
global.sb = {
	getTotalUnreadChannelCount: jest.fn(mockCallback => mockCallback(10, null)),
	GroupChannel: {
		createMyGroupChannelListQuery: jest.fn(() => ({
			includeEmpty: true,
			limit: 10,
			hasNext: true,
			next: jest.fn(mockCallback => mockCallback([1], null))
		}))
	}
};

const handleCheckSendbirdStateSpy = jest.spyOn(
	OwnerChatWidget.methods,
	'handleCheckSendbirdState'
);
const handleGetChatUnreadCountSpy = jest.spyOn(
	OwnerChatWidget.methods,
	'handleGetChatUnreadCount'
);
const handleCloseChatSpy = jest.spyOn(
	OwnerChatWidget.methods,
	'handleCloseChat'
);
const handleGetChannelListSpy = jest.spyOn(
	OwnerChatWidget.methods,
	'handleGetChannelList'
);

describe('OwnerChatWidget.vue', () => {
	let wrapper = shallowMount(OwnerChatWidget, {
		localVue,
		store
	});

	it('Should call necessary method on mounted', async () => {
		expect(handleGetChatUnreadCountSpy).toBeCalled();
		expect(handleGetChannelListSpy).toBeCalled();

		await wrapper.vm.$nextTick();
		expect(handleCheckSendbirdStateSpy).toBeCalled();
	});

	it('Should call method handleCloseChat on clicking the close button', async () => {
		wrapper.setProps({ isChatOpen: true });

		const closeElement = wrapper.find('span.owner-sendbird__header-close');

		expect(closeElement.exists()).toBe(true);

		closeElement.trigger('click');

		expect(handleCloseChatSpy).toBeCalled();
	});

	it('Watch isEmptyChatList should check the sendbird state everytime the value is change', async () => {
		await wrapper.setData({ isEmptyChatList: true });

		expect(handleCheckSendbirdStateSpy).toBeCalled();

		await wrapper.setData({ isEmptyChatList: false });

		expect(handleCheckSendbirdStateSpy).toBeCalled();
	});

	it('Should handle the sendbird component correctly when the sendbird widget and sendbird element have falsy value', async () => {
		global.sbWidget = undefined;
		global.sb = {
			getTotalUnreadChannelCount: jest.fn(mockCallback =>
				mockCallback(undefined, 'error')
			),
			GroupChannel: {
				createMyGroupChannelListQuery: jest.fn(() => ({
					includeEmpty: true,
					limit: 10,
					hasNext: true,
					next: jest.fn(mockCallback => mockCallback(undefined, 'error'))
				}))
			}
		};
		global.document.querySelector = undefined;

		wrapper = shallowMount(OwnerChatWidget, {
			localVue,
			store
		});

		expect(bugsnagClient.notify).toBeCalled();
	});

	it('Should send error to bugsnag when some of the sendbird method is not available', async () => {
		global.sb = {
			getTotalUnreadChannelCount: undefined,
			GroupChannel: {
				createMyGroupChannelListQuery: jest.fn(() => ({
					includeEmpty: true,
					limit: 10
				}))
			}
		};

		wrapper = shallowMount(OwnerChatWidget, {
			localVue,
			store
		});

		expect(bugsnagClient.notify).toBeCalled();
	});

	it('Should send error to bugsnag when the sendbird widget is not available', async () => {
		global.sb = undefined;

		wrapper = shallowMount(OwnerChatWidget, {
			localVue,
			store
		});

		expect(bugsnagClient.notify).toBeCalled();
	});
});
