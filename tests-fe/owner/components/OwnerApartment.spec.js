import { createLocalVue, shallowMount } from '@vue/test-utils';
import OwnerApartment from 'Js/owner/components/OwnerApartment.vue';
import mockComponent from 'tests-fe/utils/mock-component';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import ownerStore from 'tests-fe/owner/components/__mocks__/ownerStore.js';
import jquery from 'jquery';
import mockAxios from 'tests-fe/utils/mock-axios';
import Vuex from 'vuex';

const localVue = createLocalVue();
localVue.use(Vuex);

global.alert = jest.fn();
global.$ = jquery;
global.axios = mockAxios;

mockWindowProperty('bugsnagClient', {
	notify: jest.fn()
});

const stubs = {
	MamiLoadingInline: mockComponent,
	OwnerListNull: mockComponent,
	OwnerListProperty: mockComponent
};

const store = new Vuex.Store({
	...ownerStore
});

describe('OwnerApartment.vue', () => {
	let wrapper;

	const getApartmentSpy = jest.spyOn(OwnerApartment.methods, 'getApartment');
	const clearRoomListSpy = jest.spyOn(OwnerApartment.methods, 'clearRoomList');
	const toAddSpy = jest.spyOn(OwnerApartment.methods, 'toAdd');
	const getSuggestionSearchSpy = jest.spyOn(
		OwnerApartment.methods,
		'getSuggestionSearch'
	);

	beforeEach(() => {
		wrapper = shallowMount(OwnerApartment, {
			localVue,
			stubs,
			store,
			mocks: {
				$router: {
					push: jest.fn()
				}
			}
		});
	});

	it('Should mounts properly', () => {
		expect(wrapper).toMatchSnapshot();
	});

	it('Should return base computed value properly got from storeData', () => {
		const apartment = {
			rooms: [],
			page: 1,
			total: 100,
			limit: 10
		};
		wrapper.setData({ apartment });

		expect(wrapper.vm.totalPage).toBe(10);
		expect(wrapper.vm.token).toBe('mocktoken12345');
		expect(wrapper.vm.profile).not.toBe({});
	});

	it('should call all method when the component rendered', () => {
		// created scope
		expect(getApartmentSpy).toHaveBeenCalled();

		// mounted scope
		expect(getSuggestionSearchSpy).toHaveBeenCalled();
	});

	it('Watch report type to get postUrl should handled correctly', async () => {
		await wrapper.setData({
			roomSuggestions: 'mock suggestion'
		});

		expect(clearRoomListSpy).toHaveBeenCalled();
		expect(getApartmentSpy).toHaveBeenCalled();
	});

	it('should add the offset when calling getAppartment with params next', () => {
		const previousOffset = wrapper.vm.offset;
		wrapper.vm.getApartment('next', 3);

		expect(wrapper.vm.offset).toBe(previousOffset + 3);
	});

	it('should add the offset when calling getAppartment with params prev', () => {
		const previousOffset = wrapper.vm.offset;
		wrapper.vm.getApartment('prev', 3);

		expect(wrapper.vm.offset).toBe(previousOffset - 3);
	});

	it('should get data when calling getApartment method on successful response', async () => {
		axios.mockResolve({
			data: {
				status: false,
				rooms: []
			}
		});
		await wrapper.vm.getApartment();

		expect(toAddSpy).toBeCalled();
	});

	it('should call bugsnagClient and alert when calling getApartment method on failed response', async () => {
		axios.mockReject('error');
		await wrapper.vm.getApartment();

		expect(global.bugsnagClient.notify).toBeCalled();
		expect(global.alert).toBeCalled();
	});

	it('should call bugsnagClient and alert when calling getSuggestionApi method on failed response', async () => {
		axios.mockReject('error');
		await wrapper.vm.getSuggestionApi();

		expect(global.bugsnagClient.notify).toBeCalled();
		expect(global.alert).toBeCalled();
	});

	it('should return roomListSuggestions when calling createSearchOptionsSuggestions method', () => {
		const mockResponse = [
			{
				name: 'mock name 1',
				id: 1
			},
			{
				name: 'mock name 2',
				id: 2
			}
		];
		wrapper.vm.createSearchOptionsSuggestions(mockResponse);

		expect(wrapper.vm.createSearchOptionsSuggestions(mockResponse)).not.toBe(
			[]
		);
	});

	it('should update offset when calling pageChanged properly', () => {
		const dataOffset = 2 * wrapper.vm.limit - wrapper.vm.limit;
		wrapper.vm.pageChanged(2);

		expect(wrapper.vm.offset).toEqual(dataOffset);
		expect(getApartmentSpy).toHaveBeenCalled();
	});
});
