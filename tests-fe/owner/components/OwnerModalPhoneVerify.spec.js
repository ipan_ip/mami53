import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import mockAxios from 'tests-fe/utils/mock-axios';

import OwnerModalPhoneVerify from 'Js/owner/components/OwnerModalPhoneVerify.vue';

describe('OwnerModalPhoneVerify.vue', () => {
	const localVue = createLocalVue();
	localVue.use(Vuex);

	mockWindowProperty('open', jest.fn());
	mockWindowProperty(
		'$',
		jest.fn().mockReturnValue({
			modal: jest.fn(),
			on: jest.fn((eventName, cb) => cb && cb())
		})
	);

	global.axios = mockAxios;
	global.bugsnagClient = {
		notify: jest.fn()
	};
	global.alert = jest.fn();

	const mocks = {
		$emit: jest.fn(),
		$router: {
			push: jest.fn()
		}
	};

	const methods = {
		openSwalLoading: jest.fn(),
		closeSwalLoading: jest.fn(),
		swalSuccessTimer: jest.fn(),
		swalError: jest.fn()
	};

	const storeData = {
		state: {
			token: 'token'
		}
	};

	// mount function
	const mount = () => {
		return shallowMount(OwnerModalPhoneVerify, {
			localVue,
			mocks,
			methods,
			store: new Vuex.Store(storeData)
		});
	};

	describe('render component correctly', () => {
		let wrapper;

		beforeEach(() => {
			wrapper = mount();
		});

		it('should render container properly', () => {
			expect(wrapper.find('#ownerModalPhoneVerify').exists()).toBeTruthy();
		});
	});

	describe('call functions in the watch block correctly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = mount();
		});

		it('should call $emit on isClosed value change, and the value is truthy ', async () => {
			const emitSpy = jest.spyOn(wrapper.vm, '$emit');
			await wrapper.setData({
				isClosed: false
			});
			await wrapper.setData({
				isClosed: true
			});

			expect(emitSpy).toBeCalled();
		});

		it('should call $emit on isClosed value change, and the value is falsy', async () => {
			const emitSpy = jest.spyOn(wrapper.vm, '$emit');
			await wrapper.setData({
				isClosed: true
			});
			await wrapper.setData({
				isClosed: false
			});

			expect(emitSpy).toBeCalled();
		});
	});

	describe('call functions correctly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = mount();
		});

		it('should call $() when calling openModal method', async () => {
			await wrapper.vm.openModal();
			expect(global.$).toBeCalled();
		});

		it('should call $() when calling closeModal method', async () => {
			await wrapper.vm.closeModal();
			expect(global.$).toBeCalled();
		});

		it('should set code.requested value and call swalSuccessTimer when calling requestVerifCode method on successful api request', async () => {
			const swalSuccessTimerSpy = jest.spyOn(wrapper.vm, 'swalSuccessTimer');
			await axios.mockResolve({
				data: {
					status: true
				}
			});
			await wrapper.vm.requestVerifCode();

			expect(swalSuccessTimerSpy).toBeCalled();
			expect(wrapper.vm.code.requested).toBeTruthy();

			swalSuccessTimerSpy.mockRestore();
		});

		it('should call swalError when calling requestVerifCode method on successful api request, but falsy status', async () => {
			const swalErrorSpy = jest.spyOn(wrapper.vm, 'swalError');
			await axios.mockResolve({
				data: {
					status: false,
					meta: {
						message: 'test'
					}
				}
			});
			await wrapper.vm.requestVerifCode();

			expect(swalErrorSpy).toBeCalled();

			swalErrorSpy.mockRestore();
		});

		it('should call bugsnag, alert, and closeSwalLoading when calling requestVerifCode method on failed api request', async () => {
			const closeSwalLoadingSpy = jest.spyOn(wrapper.vm, 'closeSwalLoading');
			await axios.mockReject('error');
			await wrapper.vm.requestVerifCode();
			await wrapper.vm.$nextTick();

			expect(closeSwalLoadingSpy).toBeCalled();
			expect(global.alert).toBeCalled();
			expect(global.bugsnagClient.notify).toBeCalled();

			closeSwalLoadingSpy.mockRestore();
		});

		it('should set isChanged value and call swalSuccessTimer when calling storeVerifCode method on successful api request', async () => {
			const swalSuccessTimerSpy = jest.spyOn(wrapper.vm, 'swalSuccessTimer');
			await axios.mockResolve({
				data: {
					status: true
				}
			});
			await wrapper.vm.storeVerifCode();
			await wrapper.vm.$nextTick();

			expect(wrapper.vm.isChanged).toBeTruthy();
			expect(swalSuccessTimerSpy).toBeCalled();

			swalSuccessTimerSpy.mockRestore();
		});

		it('should call swalError when calling storeVerifCode method on successful api request, but falsy status', async () => {
			const swalErrorSpy = jest.spyOn(wrapper.vm, 'swalError');
			await axios.mockResolve({
				data: {
					status: false,
					meta: {
						message: 'test'
					}
				}
			});
			await wrapper.vm.storeVerifCode();
			await wrapper.vm.$nextTick();

			expect(swalErrorSpy).toBeCalled();
			swalErrorSpy.mockRestore();
		});

		it('should call bugsnag, alert, and closeSwalLoading when calling storeVerifCode method on failed api request', async () => {
			const closeSwalLoadingSpy = jest.spyOn(wrapper.vm, 'closeSwalLoading');
			await axios.mockReject('error');
			await wrapper.vm.storeVerifCode();
			await wrapper.vm.$nextTick();

			expect(closeSwalLoadingSpy).toBeCalled();
			expect(global.alert).toBeCalled();
			expect(global.bugsnagClient.notify).toBeCalled();

			closeSwalLoadingSpy.mockRestore();
		});
	});
});
