import { createLocalVue, shallowMount } from '@vue/test-utils';

import mockComponent from 'tests-fe/utils/mock-component';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

import OwnerListVacancy from 'Js/owner/components/OwnerListVacancy.vue';

describe('OwnerListVacancy.vue', () => {
	const localVue = createLocalVue();

	mockWindowProperty('navigator', {
		isMobile: true
	});

	// mock stubbed component
	const stubs = {
		alert: mockComponent
	};

	const $router = {
		push: jest.fn()
	};

	const propsData = {
		vacancies: [
			{
				slug: 'slug',
				status: 'verified', // verified, waiting
				title: 'title',
				type: 'FULL-TIME', // FULL-TIME, PART-TIME, FREELANCE, MAGANG
				salary: 1000000,
				created_at: '2020-08-09 10:11:12',
				place_name: 'place_name'
			},
			{
				slug: 'slug',
				status: 'waiting', // verified, waiting
				title: 'title',
				type: 'PART-TIME', // FULL-TIME, PART-TIME, FREELANCE, MAGANG
				salary: 1000000,
				created_at: '2020-08-09 10:11:12',
				place_name: 'place_name'
			}
		]
	};

	// mount function
	const mount = () => {
		return shallowMount(OwnerListVacancy, {
			localVue,
			stubs,
			propsData,
			mocks: {
				$router
			}
		});
	};

	describe('render component correctly', () => {
		let wrapper;

		beforeEach(() => {
			wrapper = mount();
		});

		it('should render container properly', () => {
			expect(wrapper.find('.card-container').exists()).toBeTruthy();
		});
	});

	describe('render computed value properly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = mount();
		});

		it('should return isMobile value properly', async () => {
			// default: truthy value
			expect(wrapper.vm.isMobile).toBeTruthy();

			// falsy value
			await mockWindowProperty('navigator', {
				isMobile: false
			});
			wrapper.destroy();
			wrapper = mount();
			expect(wrapper.vm.isMobile).toBeFalsy();
		});
	});

	describe('call functions correctly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = mount();
		});

		it('should call $router.push when clicking on a verified vacancy card', async () => {
			const cards = wrapper.findAll('.card-container');
			if (cards && cards.length >= 1) {
				const verifiedCard = cards.at(0);
				await verifiedCard.trigger('click');
				expect(wrapper.vm.$router.push).toBeCalled();
			}
		});

		it('should call $router.push when clicking on a waiting vacancy card', async () => {
			const cards = wrapper.findAll('.card-container');
			if (cards && cards.length >= 2) {
				const waitingCard = cards.at(1);
				await waitingCard.trigger('click');
				expect(wrapper.vm.statusAlert.state).toBeTruthy();
			}
		});
	});
});
