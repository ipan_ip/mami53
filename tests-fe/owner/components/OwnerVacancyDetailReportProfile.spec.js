import { createLocalVue, shallowMount } from '@vue/test-utils';
import OwnerVacancyDetailReportProfile from 'Js/owner/components/OwnerVacancyDetailReportProfile.vue';
import mockAxios from 'tests-fe/utils/mock-axios';
import mockComponent from 'tests-fe/utils/mock-component';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import ownerStore from 'tests-fe/owner/components/__mocks__/ownerStore.js';
import Vuex from 'vuex';

global.axios = mockAxios;
global.alert = jest.fn();

const localVue = createLocalVue();
localVue.use(Vuex);

mockWindowProperty('bugsnagClient', {
	notify: jest.fn()
});

const stubs = {
	MamiLoadingInline: mockComponent,
	OwnerDetailBack: mockComponent
};

const store = new Vuex.Store({
	...ownerStore
});

describe('OwnerVacancyDetailReportProfile.vue', () => {
	let wrapper;

	beforeEach(() => {
		wrapper = shallowMount(OwnerVacancyDetailReportProfile, {
			localVue,
			stubs,
			store,
			mocks: {
				$route: { params: { slug: 'mock-slug', seq: 'mock-seq', number: 1 } }
			}
		});
	});

	it('Should mounts properly', () => {
		expect(wrapper.find('.section-container').exists()).toBe(true);
	});

	it('Should return base computed value properly', () => {
		expect(wrapper.vm.token).toBe('mocktoken12345');
		expect(wrapper.vm.slug).toBe('mock-slug');
		expect(wrapper.vm.seq).toBe('mock-seq');
		expect(wrapper.vm.number).toBe(1);
	});

	it('should get responses data properly', async () => {
		axios.mockResolve({
			data: {
				status: true,
				data: {
					name: 'mock name'
				}
			}
		});
		await wrapper.vm.$nextTick();

		expect(wrapper.vm.isLoading).toBeFalsy();
	});
	it('should call bugsnagClient and alert when got error response from api', async () => {
		axios.mockReject('error');
		await wrapper.vm.$nextTick();

		expect(global.bugsnagClient.notify).toBeCalled();
		expect(global.alert).toBeCalled();
	});
});
