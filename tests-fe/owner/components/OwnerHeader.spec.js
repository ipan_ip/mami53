import { createLocalVue, shallowMount } from '@vue/test-utils';
import OwnerHeader from 'Js/owner/components/OwnerHeader.vue';
import mockComponent from 'tests-fe/utils/mock-component';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import ownerMixinSupportChat from 'Js/owner/mixins/OwnerMixinSupportChat';
import ownerMixinPremiumConfirmCountdown from 'Js/owner/mixins/OwnerMixinPremiumConfirmCountdown';
import mixinBuyBalanceEligibility from 'Js/owner/mixins/OwnerBuyBalanceEligibility';
import mixinVoucherTracker from 'Js/_user/mixins/mixinVoucherTracker';
import ownerStore from 'tests-fe/owner/components/__mocks__/ownerStore.js';
import dayjs from 'dayjs';
import Vuex from 'vuex';

const localVue = createLocalVue();
localVue.prototype.$dayjs = dayjs;
localVue.mixin([
	ownerMixinSupportChat,
	ownerMixinPremiumConfirmCountdown,
	mixinBuyBalanceEligibility,
	mixinVoucherTracker
]);
localVue.use(Vuex);

global.tracker = jest.fn();
global.alert = jest.fn();
global.swal = jest.fn().mockResolvedValue(true);

mockWindowProperty('location', {
	reload: jest.fn(),
	replace: jest.fn()
});
mockWindowProperty('navigator.isMobile', true);

const stubs = {
	OwnerSidebar: mockComponent,
	RouterLink: mockComponent
};

const store = new Vuex.Store({
	...ownerStore
});

describe('OwnerHeader.vue', () => {
	let wrapper;

	const openOwnerSidebarSpy = jest.spyOn(
		OwnerHeader.methods,
		'openOwnerSidebar'
	);

	const moengageEventTrackerSpy = jest.spyOn(
		OwnerHeader.methods,
		'moengageEventTracker'
	);

	beforeEach(() => {
		wrapper = shallowMount(OwnerHeader, {
			localVue,
			stubs,
			store,
			mocks: {
				$route: {
					name: 'settings',
					query: {
						title: 'Kos'
					}
				}
			}
		});
	});

	it('Should mounts properly', () => {
		expect(wrapper.find('#ownerHeader').exists()).toBe(true);
	});

	it('Should return base computed value properly got from storeData', async () => {
		await wrapper.vm.$nextTick();
		wrapper.vm.$store.state.profile.user = {
			is_verify: true,
			kost_total_active: 0
		};

		expect(wrapper.vm.isMobile).toBeTruthy();
		expect(wrapper.vm.routeName).toEqual('settings');
		expect(wrapper.vm.notifCount).toEqual(null);
		expect(wrapper.vm.isVerify).toBeTruthy();
		expect(wrapper.vm.isKostActive).toBe(0);
		expect(wrapper.vm.isVacancyAd).toBeFalsy();
		expect(wrapper.vm.isOnVerifOrConfirmation).toBeFalsy();
		expect(wrapper.vm.isMembershipExpired).toBeFalsy();
		expect(wrapper.vm.isOnAbTesting).toBeFalsy();
		expect(wrapper.vm.abTestingScenario).toBeFalsy();
		expect(wrapper.vm.isOnBScenario).toBeFalsy();
		expect(wrapper.vm.hideMembershipMenu).toBe('');

		await wrapper.vm.$nextTick();
		wrapper.vm.$store.state.abTestResult = {
			is_active: true,
			experiment_id: 2,
			experiment_value: ''
		};

		expect(wrapper.vm.abTestingScenario).toBeTruthy();
	});

	it('should call all method when the component rendered', async () => {
		await wrapper.vm.$nextTick();
		wrapper.vm.$store.state.profile.membership = {
			status: 'Konfirmasi Pembayaran',
			countdown: 1000,
			balance_status: 'verif'
		};

		const scrollSpy = jest.spyOn(window, 'onscroll');
		window.onscroll();
		expect(scrollSpy).toBeCalled();
		scrollSpy.mockRestore();
	});

	it.skip('should call openOwnerSidebar when the button triggered', async () => {
		const openOwnerSidebarButton = wrapper.find('.header-top');
		expect(openOwnerSidebarButton.exists()).toBe(true);

		openOwnerSidebarButton.trigger('click');
		await wrapper.vm.$nextTick();
		expect(openOwnerSidebarSpy).toHaveBeenCalled();

		wrapper.vm.$refs = {
			ownerSidebar: {
				showLeft: false
			}
		};
		await wrapper.vm.openOwnerSidebar();
		expect(wrapper.vm.$refs.ownerSidebar.showLeft).toBeTruthy();
	});

	it('should reload the page', () => {
		wrapper.vm.locationReload();
		expect(location.reload).toBeCalled();
	});

	it('should fo to premium package page', () => {
		wrapper.vm.goToPremiumPackage();
		expect(location.replace).toBeCalled();
		expect(moengageEventTrackerSpy).toHaveBeenCalled();
	});

	it('should call alert when alertPremium method called', () => {
		wrapper.vm.alertPremium();
		expect(alert).toBeCalledWith(
			'Saat ini Premium Membership hanya berlaku untuk data iklan kost dan apartemen.'
		);
	});

	it('should call swal calling openBusyTransaction method', async () => {
		await wrapper.vm.openBusyTransaction();
		await wrapper.vm.$nextTick();
		expect(global.swal).toBeCalled();
	});

	it('should call swal calling openWaitForVerification method', async () => {
		await wrapper.vm.openWaitForVerification();
		await wrapper.vm.$nextTick();
		expect(global.swal).toBeCalled();
	});
});
