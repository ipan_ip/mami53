import { createLocalVue, shallowMount } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import OwnerAdd from 'Js/owner/components/OwnerAdd.vue';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import mockComponent from 'tests-fe/utils/mock-component';
import mockAxios from 'tests-fe/utils/mock-axios';
import ownerStore from 'tests-fe/owner/components/__mocks__/ownerStore.js';
import Vuex from 'vuex';

global.bugsnagClient = { notify: jest.fn() };
global.alert = jest.fn();
global.axios = mockAxios;

const localVue = createLocalVue();
localVue.use(Vuex);

mockWindowProperty(
	'$',
	jest.fn().mockReturnValue({
		tooltip: jest.fn()
	})
);

const stubs = {
	MamiLoadingInline: mockComponent
};

const store = new Vuex.Store({ ...ownerStore });

const openSwalLoading = jest.fn();
const closeSwalLoading = jest.fn();

const generateWrapper = () => {
	return shallowMount(OwnerAdd, {
		localVue,
		stubs,
		store,
		mocks: {
			openSwalLoading,
			closeSwalLoading,
			$route: {
				params: {
					slug: 'mock-slug',
					isKos: true,
					isApartment: false,
					isVacancy: false
				},
				query: {
					'input-property': true
				}
			}
		}
	});
};

describe('OwnerAdd.vue', () => {
	const wrapper = generateWrapper();

	const showBtnTooltipSpy = jest.spyOn(OwnerAdd.methods, 'showBtnTooltip');
	const hideButtonAddTooltipSpy = jest.spyOn(
		OwnerAdd.methods,
		'hideButtonAddTooltip'
	);

	it('Should mounts properly', () => {
		expect(wrapper.find('.search-room-container').exists()).toBe(true);
	});

	it('should return data properly', () => {
		const profile = {
			membership: { expired: false, is_mamipay_user: true, status: 'Premium' },
			user: {
				email: 'bamb@ng.com',
				name: 'Bambang',
				phone_number: '08123456789'
			}
		};

		expect(wrapper.vm.isMobile).toBeFalsy();
		expect(wrapper.vm.token).toBe('mocktoken12345');
		expect(wrapper.vm.isKos).toBeTruthy();
		expect(wrapper.vm.isApartment).toBeFalsy();
		expect(wrapper.vm.isVacancy).toBeFalsy();
		expect(wrapper.vm.profile).toEqual(profile);
		expect(wrapper.vm.isBBKActive).toBeTruthy();
	});

	it('should get suggested data properly', async () => {
		const rooms = [
			{
				id: 1,
				name: 'room1'
			},
			{
				id: 2,
				name: 'room2'
			}
		];

		axios.mockResolve({
			status: true,
			data: {
				show: 20,
				rooms
			}
		});
		const wrapper = generateWrapper();
		await wrapper.vm.$nextTick();

		expect(wrapper.vm.isLoading).toBeFalsy();
		expect(wrapper.vm.isResultExists).toBeTruthy();
		expect(wrapper.vm.result).toEqual(rooms);
	});

	it('should handle suggested data when rooms is empty', async () => {
		const rooms = [];

		axios.mockResolve({
			status: true,
			data: {
				show: 20,
				rooms
			}
		});
		const wrapper = generateWrapper();
		await wrapper.vm.$nextTick();

		expect(wrapper.vm.isLoading).toBeFalsy();
		expect(wrapper.vm.isResultExists).toBeFalsy();
		expect(wrapper.vm.resultAlert).toBe(
			'Data iklan milik Anda belum ditemukan. Silakan cari atau tambahkan iklan baru.'
		);
	});

	it('should call bugsnagClient when calling owner suggested on failed response', async () => {
		axios.mockReject('error');
		const wrapper = generateWrapper();
		await wrapper.vm.$nextTick();

		expect(bugsnagClient.notify).toBeCalled();
	});

	it('method searchRoom should call get data when success response from api but empty', async () => {
		const rooms = [
			{
				id: 1,
				name: 'room1'
			},
			{
				id: 2,
				name: 'room2'
			}
		];

		axios.mockResolve({
			status: true,
			data: {
				show: 20,
				rooms
			}
		});
		const wrapper = generateWrapper();
		wrapper.setData({
			criteria: 'mock criteria'
		});
		await wrapper.vm.$nextTick();
		await wrapper.vm.searchRoom();

		expect(openSwalLoading).toBeCalled();
		expect(wrapper.vm.isResultExists).toBeTruthy();
		expect(wrapper.vm.result).toEqual(rooms);
		expect(closeSwalLoading).toBeCalled();
	});

	it('method searchRoom should call get data when success response from api but empty', async () => {
		const rooms = [];

		axios.mockResolve({
			status: true,
			data: {
				show: 20,
				rooms
			}
		});
		const wrapper = generateWrapper();
		wrapper.setData({
			criteria: 'mock criteria'
		});
		await wrapper.vm.$nextTick();
		await wrapper.vm.searchRoom();

		expect(openSwalLoading).toBeCalled();
		expect(wrapper.vm.isResultExists).toBeFalsy();
		expect(wrapper.vm.resultAlert).toBe(
			'Data iklan tidak ditemukan. Silakan cari dengan kriteria pencarian lainnya atau tambahkan iklan baru.'
		);
		expect(closeSwalLoading).toBeCalled();
	});

	it('method searchRoom should call bugsnagClient when got error response from api', async () => {
		const wrapper = generateWrapper();
		wrapper.setData({
			criteria: 'mock criteria'
		});
		axios.mockReject('error');
		await wrapper.vm.$nextTick();
		await wrapper.vm.searchRoom();

		expect(global.bugsnagClient.notify).toBeCalled();
	});

	it('should call all method when the component rendered', async () => {
		const wrapper = generateWrapper();
		await wrapper.vm.$nextTick();

		expect(showBtnTooltipSpy).toHaveBeenCalled();
	});

	it('mwthod showBtnTooltip should call properly', done => {
		navigator.isMobile = true;
		let wrapper = generateWrapper();

		wrapper.vm.showBtnTooltip();
		expect($('#ownerAddBtnXs').tooltip).toBeCalledWith('show');
		setTimeout(() => {
			expect($('#ownerAddBtnXs').tooltip).toBeCalledWith('hide');
			done();
		}, 5000);

		navigator.isMobile = false;
		wrapper = generateWrapper();

		wrapper.vm.showBtnTooltip();
		expect($('#ownerAddBtnLg').tooltip).toBeCalledWith('show');
		setTimeout(() => {
			expect($('#ownerAddBtnLg').tooltip).toBeCalledWith('hide');
			done();
		}, 5000);
	});

	it('method hideButtonAddTooltip should call properly', () => {
		navigator.isMobile = true;
		let wrapper = generateWrapper();

		wrapper.vm.hideButtonAddTooltip();
		expect($('#ownerAddBtnXs').tooltip).toBeCalledWith('show');

		navigator.isMobile = false;
		wrapper = generateWrapper();

		wrapper.vm.hideButtonAddTooltip();
		expect($('#ownerAddBtnLg').tooltip).toBeCalledWith('show');
	});

	it('method hideButtonAddTooltip should call properly', () => {
		navigator.isMobile = true;
		let wrapper = generateWrapper();

		wrapper.vm.hideButtonAddTooltip();
		expect($('#ownerAddBtnXs').tooltip).toBeCalledWith('hide');

		navigator.isMobile = false;
		wrapper = generateWrapper();

		wrapper.vm.hideButtonAddTooltip();
		expect($('#ownerAddBtnLg').tooltip).toBeCalledWith('hide');
	});

	it('method openModalOwnerAdd should call properly', async () => {
		wrapper.vm.$refs = {
			ownerModalAdd: {
				openModal: jest.fn()
			}
		};
		wrapper.vm.openModalOwnerAdd();
		await wrapper.vm.$nextTick();

		expect(hideButtonAddTooltipSpy).toHaveBeenCalled();
		expect(wrapper.vm.$refs.ownerModalAdd.openModal).toBeCalled();
	});

	it('method openAutoBookingModal should call properly', () => {
		wrapper.vm.openAutoBookingModal();

		expect(wrapper.vm.autoBookingModalActive).toBeTruthy();
	});
});
