import { createLocalVue, shallowMount } from '@vue/test-utils';
import OwnerVacancyDetailReport from 'Js/owner/components/OwnerVacancyDetailReport.vue';
import mockComponent from 'tests-fe/utils/mock-component';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import mockAxios from 'tests-fe/utils/mock-axios';
import ownerStore from 'tests-fe/owner/components/__mocks__/ownerStore.js';
import Vuex from 'vuex';

const localVue = createLocalVue();
localVue.use(Vuex);

global.alert = jest.fn();
global.axios = mockAxios;

mockWindowProperty('bugsnagClient', {
	notify: jest.fn()
});

const stubs = {
	MamiLoadingInline: mockComponent,
	OwnerListNull: mockComponent,
	OwnerDetailBack: mockComponent,
	RouterLink: mockComponent
};

const store = new Vuex.Store({
	...ownerStore
});

describe('OwnerVacancyDetailReport.vue', () => {
	let wrapper;
	const getApplicantDataSpy = jest.spyOn(
		OwnerVacancyDetailReport.methods,
		'getApplicantData'
	);

	beforeEach(() => {
		wrapper = shallowMount(OwnerVacancyDetailReport, {
			localVue,
			stubs,
			store,
			mocks: {
				$route: { params: { slug: 'mock-slug', seq: 'mock-seq' } }
			}
		});
	});

	it('Should mounts properly', () => {
		expect(wrapper.find('.section-container').exists()).toBe(true);
	});

	it('Should return base computed value properly got from storeData', () => {
		expect(wrapper.vm.token).toBe('mocktoken12345');
		expect(wrapper.vm.slug).toBe('mock-slug');
		expect(wrapper.vm.seq).toBe('mock-seq');
	});

	it('should call all method when the component rendered', async () => {
		await wrapper.vm.$nextTick();

		expect(getApplicantDataSpy).toHaveBeenCalled();
	});

	it('should get responses data properly', async () => {
		axios.mockResolve({
			status: true,
			data: {
				show: 20,
				data: {}
			}
		});
		await wrapper.vm.$nextTick();

		expect(wrapper.vm.isLoading).toBeFalsy();
		expect(wrapper.vm.isLoadingMore).toBeFalsy();
	});
	it('should call bugsnagClient and alert when got error response from api', async () => {
		axios.mockReject('error');
		await wrapper.vm.$nextTick();

		expect(global.bugsnagClient.notify).toBeCalled();
		expect(global.alert).toBeCalled();
	});

	it('should call loadMoreApplicantData properly', () => {
		const tempPage = wrapper.vm.page;
		wrapper.vm.loadMoreApplicantData();

		expect(wrapper.vm.page).toBe(tempPage + 1);
		expect(wrapper.vm.isLoadingMore).toBeTruthy();
		expect(getApplicantDataSpy).toHaveBeenCalled();
	});
});
