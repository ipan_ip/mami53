import { createLocalVue, shallowMount } from '@vue/test-utils';
import OwnerListPropertyOld from 'Js/owner/components/OwnerListPropertyOld.vue';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import ownerStore from 'tests-fe/owner/components/__mocks__/ownerStore.js';
import rooms from './__mocks__/rooms.json';
import { alert } from 'vue-strap';
import Vuex from 'vuex';

const localVue = createLocalVue();
localVue.use(Vuex);

global.tracker = jest.fn();
global.alert = jest.fn();

mockWindowProperty('navigator.isMobile', true);

const stubs = {
	alert
};

const propsData = {
	propertyType: 'kos',
	rooms: rooms
};

const store = new Vuex.Store({
	...ownerStore
});

describe('OwnerListPropertyOld.vue', () => {
	let wrapper;

	beforeEach(() => {
		wrapper = shallowMount(OwnerListPropertyOld, {
			localVue,
			stubs,
			store,
			propsData,
			mocks: {
				$router: {
					push: jest.fn()
				}
			}
		});
	});

	it('Should mounts properly', () => {
		expect(wrapper.find('.card-container').exists()).toBe(true);
	});

	it('Should return base computed value properly got from storeData', async () => {
		await wrapper.vm.$nextTick();

		wrapper.vm.$store.state.extras.textToDetail = 'A';
		expect(wrapper.vm.textToDetail).toEqual('Update, Edit, dan Promosikan ');
		expect(wrapper.vm.isMobile).toBeTruthy();
		expect(wrapper.vm.propertyTypeTitle).toEqual('Kost');

		wrapper.vm.$store.state.extras.textToDetail = 'B';
		wrapper.setProps({ propertyType: 'apartment' });
		expect(wrapper.vm.textToDetail).toEqual('Lihat Statistik ');
		expect(wrapper.vm.propertyTypeTitle).toEqual('Apartemen');
	});

	it('should call all method when the component rendered', async () => {
		await wrapper.vm.$nextTick();
		expect(tracker).toBeCalled();
	});

	it('go to another page properly when _id room is not zero AND room status is one of allowed status WITH propertyType is kost', () => {
		const currentProperty = wrapper.vm.rooms[1];

		wrapper.vm.openRoom(currentProperty);
		expect(wrapper.vm.$router.push).toBeCalledWith({
			name: 'kos_update',
			props: { propertyType: wrapper.vm.propertyType }
		});
		expect(tracker).toBeCalled();
	});

	it('go to another page properly when _id room is not zero AND room status is one of allowed status WITH propertyType is apartment', () => {
		wrapper.setProps({ propertyType: 'apartment' });
		const currentProperty = wrapper.vm.rooms[1];

		wrapper.vm.openRoom(currentProperty);
		expect(wrapper.vm.$router.push).toBeCalledWith({
			name: 'apartment_detail',
			params: { seq: currentProperty._id.toString() },
			props: { propertyType: wrapper.vm.propertyType }
		});
		expect(tracker).toBeCalled();
	});

	it('change statusAlert value when _id room is not zero AND room status is edited', () => {
		const currentProperty = wrapper.vm.rooms[3];

		wrapper.vm.openRoom(currentProperty);
		expect(wrapper.vm.statusAlert).toEqual({
			state: true,
			label: 'Tunggu data ' + currentProperty.room_title + ' disetujui'
		});
	});

	it('go to another page properly when _id room is not zero AND room status is unverified WITH propertyType is kost', () => {
		const currentProperty = wrapper.vm.rooms[4];

		wrapper.vm.openRoom(currentProperty);
		expect(wrapper.vm.$router.push).toBeCalledWith({
			name: 'kos_update',
			props: { propertyType: wrapper.vm.propertyType }
		});
	});

	it('go to another page properly when _id room is not zero AND room status is unverified WITH propertyType is kost', () => {
		wrapper.setProps({ propertyType: 'apartment' });
		const currentProperty = wrapper.vm.rooms[4];

		wrapper.vm.openRoom(currentProperty);
		expect(wrapper.vm.$router.push).toBeCalledWith({
			name: 'apartment_detail',
			params: { seq: currentProperty._id.toString() },
			props: { propertyType: wrapper.vm.propertyType }
		});
	});

	it('change readyAlert value when _id room is zero', () => {
		const currentProperty = wrapper.vm.rooms[0];

		wrapper.vm.openRoom(currentProperty);
		expect(wrapper.vm.readyAlert).toEqual({
			state: true,
			label:
				currentProperty.room_title +
				' sedang disiapkan oleh sistem. Silakan coba beberapa saat lagi.'
		});
	});
});
