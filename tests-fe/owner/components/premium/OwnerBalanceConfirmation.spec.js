import { createLocalVue, shallowMount } from '@vue/test-utils';
import OwnerBalanceConfirmation from 'Js/owner/components/premium/OwnerBalanceConfirmation';
import MamiLoadingInline from 'Js/@components/MamiLoadingInline';
import Vuex from 'vuex';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

describe('OwnerBalanceConfirmation.vue', () => {
	let wrapper;

	const localVue = createLocalVue();
	localVue.use(Vuex);

	const axios = {
		get: jest.fn(() => {
			return Promise.resolve({});
		})
	};

	global.axios = axios;
	global.window = mockWindowProperty('bugsnagClient', { notify: jest.fn() });
	global.alert = jest.fn();

	const store = {
		state: {
			token: 'tok3n',
			extras: {
				confirmationData: {
					bankDestination: 7
				}
			},
			profile: {
				user: {
					enable_chat: true
				}
			},
			abTestResult: {
				is_active: false,
				experiment_id: 0,
				experiment_value: ''
			}
		}
	};

	const newStore = {
		state: {
			token: 'token2',
			extras: {
				confirmationData: null
			},
			profile: {
				user: {
					enable_chat: true
				}
			},
			abTestResult: {
				is_active: false,
				experiment_id: 0,
				experiment_value: ''
			}
		}
	};

	const propsData = {
		confirmationType: 'package'
	};

	beforeEach(() => {
		wrapper = shallowMount(OwnerBalanceConfirmation, {
			localVue,
			propsData,
			store: new Vuex.Store(store),
			stubs: {
				MamiLoadingInline
			}
		});
		wrapper.vm.isLoading = false;
	});

	it('should renders components properly', () => {
		expect(wrapper.find('.confirmation-body').exists()).toBeTruthy();
	});

	it('should get payment data properly', () => {
		const confirmationData = { bankDestination: 7 };
		axios.get = jest.fn().mockResolvedValue({
			data: {
				status: true,
				bank: [
					{
						account_name: 'PT MAMA TEKNOLOGI',
						id: 7,
						name: 'Bank BNI'
					}
				],
				membership: {
					status: 'premium',
					balance_price_number: 13000
				}
			}
		});
		expect(wrapper.vm.confirmationData).toEqual(confirmationData);
		expect(wrapper.vm.isLoading).toBeFalsy();
	});

	it('should handle properly if confirmation Data is null', () => {
		wrapper = shallowMount(OwnerBalanceConfirmation, {
			localVue,
			propsData,
			store: new Vuex.Store(newStore),
			stubs: {
				MamiLoadingInline
			}
		});
		wrapper.vm.isLoading = false;
		expect(wrapper.vm.bankList).toEqual(expect.arrayContaining([]));
	});
});
