import { createLocalVue, shallowMount } from '@vue/test-utils';
import OwnerPremiumConfirmation from 'Js/owner/components/premium/OwnerPremiumConfirmation';
import MamiLoadingInline from 'Js/@components/MamiLoadingInline';
import Vuex from 'vuex';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

describe('OwnerPremiumConfirmation', () => {
	let wrapper;

	const localVue = createLocalVue();
	localVue.use(Vuex);

	const axios = {
		get: jest.fn(() => {
			return Promise.resolve({});
		})
	};

	global.axios = axios;
	global.window = mockWindowProperty('bugsnagClient', { notify: jest.fn() });
	global.alert = jest.fn();

	const propsData = {
		confirmationType: 'package'
	};

	const store = {
		state: {
			token: 'tok3n',
			extras: {
				confirmationData: {
					bankDestination: 7
				}
			},
			profile: {
				user: {
					enable_chat: true
				}
			}
		}
	};

	beforeEach(() => {
		wrapper = shallowMount(OwnerPremiumConfirmation, {
			localVue,
			propsData,
			store: new Vuex.Store(store),
			stubs: {
				MamiLoadingInline
			}
		});
		wrapper.vm.isLoading = false;
	});

	it('should renders components properly', () => {
		expect(wrapper.find('.confirmation-body').exists()).toBeTruthy();
	});

	it('should get payment data properly', async () => {
		axios.get = jest.fn().mockResolvedValue({
			data: {
				status: true,
				bank: [
					{
						account_name: 'PT MAMA TEKNOLOGI',
						id: 7,
						name: 'Bank BNI'
					}
				],
				membership: {
					status: 'premium',
					balance_price_number: 13000
				}
			}
		});
		expect(wrapper.vm.isLoading).toBeFalsy();
	});

	it('should compute confirmation data & token properly', () => {
		expect(wrapper.vm.confirmationData).toEqual({ bankDestination: 7 });
		expect(wrapper.vm.token).toEqual('tok3n');
	});
});
