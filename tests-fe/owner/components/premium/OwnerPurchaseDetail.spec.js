import { createLocalVue, shallowMount } from '@vue/test-utils';
import Axios from 'axios';
import OwnerPurchaseDetail from 'Js/owner/components/premium/OwnerPurchaseDetail';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import Vuex from 'vuex';

window.oxWebUrl = 'local.mamikos.com';
window.open = jest.fn();

describe('OwnerPurchaseDetail', () => {
	let wrapper;

	const localVue = createLocalVue();
	localVue.use(Vuex);

	const store = {
		state: {
			token: 'tok3n',
			profile: {
				membership: {
					active_premium_package_id: 1,
					active_premium_package_name: 'Paket 1 Hari'
				},
				user: {
					user_id: 111
				}
			}
		}
	};
	const hideChatBoxSpy = jest.spyOn(OwnerPurchaseDetail.methods, 'hideChatBox');
	const querySelector = jest.fn(() => {
		return {
			style: {
				display: jest.fn(() => {
					return 'none';
				})
			}
		};
	});
	const purchaseData = {
		name: 'Premier Bronze',
		amount: 185000,
		type: 'package',
		views: 50000,
		id: 1004197,
		days_count: 124
	};

	global.axios = Axios;
	global.tracker = jest.fn();

	mockWindowProperty('bugsnagClient', {
		notify: jest.fn()
	});
	mockWindowProperty('document.querySelector', querySelector);
	mockWindowProperty('open', jest.fn());
	mockWindowProperty('data', purchaseData);

	beforeEach(() => {
		wrapper = shallowMount(OwnerPurchaseDetail, {
			localVue,
			store: new Vuex.Store(store)
		});
		wrapper.vm.purchaseData = purchaseData;
	});

	it('should renders components properly', () => {
		expect(wrapper.find('.purchase-detail').exists()).toBeTruthy();
		expect(hideChatBoxSpy).toBeCalled();
	});

	it('should go back properly', () => {
		window.history.back = jest.fn();
		wrapper.find('.back-btn .icon').trigger('click');
		expect(window.history.back).toBeCalled();
	});

	it('should proceed purchase properly', async () => {
		const mockResolved = {
			invoice_url: 'https://gaplek3.pay.kerupux.com',
			invoice_number: 'PREM/2020/211'
		};
		const mockAxiosPost = Promise.resolve({ data: mockResolved });
		axios.post = jest.fn().mockResolvedValue(mockAxiosPost);

		wrapper.find('.purchase-detail__footer .purchase-btn').trigger('click');
		await wrapper.vm.proceedPurchase();
		expect(axios.post).toHaveBeenCalledTimes(1);
	});

	it('should return all computed props properly', () => {
		expect(wrapper.vm.membership).toStrictEqual({
			active_premium_package_id: 1,
			active_premium_package_name: 'Paket 1 Hari'
		});
		expect(wrapper.vm.token).toBe('tok3n');
	});
});
