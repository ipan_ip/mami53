import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

import mockAxios from 'tests-fe/utils/mock-axios';
import OwnerModalEditVacancy from 'Js/owner/components/OwnerModalEditVacancy.vue';

describe('OwnerModalEditVacancy.vue', () => {
	const localVue = createLocalVue();
	localVue.use(Vuex);

	mockWindowProperty('open', jest.fn());
	mockWindowProperty(
		'$',
		jest.fn().mockReturnValue({
			modal: jest.fn(),
			on: jest.fn((eventName, cb) => cb && cb())
		})
	);

	global.axios = mockAxios;
	global.confirm = jest.fn().mockReturnValue(true);
	global.bugsnagClient = { notify: jest.fn() };
	global.alert = jest.fn();

	const storeData = {
		state: {
			token: 'token'
		}
	};

	const propsData = {
		vacancy: {
			id: 0,
			active: 1,
			slug: 'slug'
		}
	};

	const mocks = {
		$emit: jest.fn(),
		$router: {
			push: jest.fn()
		}
	};

	const methods = {
		openSwalLoading: jest.fn(),
		closeSwalLoading: jest.fn(),
		swalSuccessTimer: jest.fn()
	};

	// mount function
	const mount = () => {
		return shallowMount(OwnerModalEditVacancy, {
			localVue,
			mocks,
			propsData,
			methods,
			store: new Vuex.Store(storeData)
		});
	};

	describe('render component correctly', () => {
		let wrapper;

		beforeEach(() => {
			wrapper = mount();
		});

		it('should render container properly', () => {
			expect(wrapper.find('#ownerModalEditVacancy').exists()).toBeTruthy();
		});
	});

	describe('render computed value properly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = mount();
		});

		it('should return token value properly', () => {
			expect(wrapper.vm.token).toBe('token');
		});
	});

	describe('call functions in the watch block correctly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = mount();
		});

		it('should call $emit on isClosed value change, and the value is truthy ', async () => {
			const emitSpy = jest.spyOn(wrapper.vm, '$emit');
			await wrapper.setData({
				isClosed: false
			});
			await wrapper.setData({
				isClosed: true
			});

			expect(emitSpy).toBeCalled();
			emitSpy.mockRestore();
		});

		it('should call $emit on isClosed value change, and the value is falsy', async () => {
			const emitSpy = jest.spyOn(wrapper.vm, '$emit');
			await wrapper.setData({
				isClosed: true
			});
			await wrapper.setData({
				isClosed: false
			});

			expect(emitSpy).toBeCalled();
			emitSpy.mockRestore();
		});
	});

	describe('call functions correctly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = mount();
		});

		it('should call $() when calling openModal method', async () => {
			await wrapper.vm.openModal();
			expect(global.$).toBeCalled();
		});

		it('should call $() when calling closeModal method', async () => {
			await wrapper.vm.closeModal();
			expect(global.$).toBeCalled();
		});

		it('should not call openSwalLoading when calling toggleActiveVacancy method, the vacancy was active, and the confirmation was not confirmed', async () => {
			global.confirm = jest.fn().mockReturnValue(false);
			const openSwalLoadingSpy = jest.spyOn(wrapper.vm, 'openSwalLoading');
			await wrapper.vm.toggleActiveVacancy(1);
			expect(openSwalLoadingSpy).not.toBeCalled();
			openSwalLoadingSpy.mockRestore();
		});

		it('should not call openSwalLoading when calling toggleActiveVacancy method, the vacancy was not active, and the confirmation was not confirmed', async () => {
			global.confirm = jest.fn().mockReturnValue(false);
			const openSwalLoadingSpy = jest.spyOn(wrapper.vm, 'openSwalLoading');
			await wrapper.vm.toggleActiveVacancy(0);
			expect(openSwalLoadingSpy).not.toBeCalled();
			openSwalLoadingSpy.mockRestore();
		});

		it('should not call openSwalLoading when calling deleteVacancy method, and the confirmation was not confirmed', async () => {
			global.confirm = jest.fn().mockReturnValue(false);
			const openSwalLoadingSpy = jest.spyOn(wrapper.vm, 'openSwalLoading');
			await wrapper.vm.deleteVacancy();
			expect(openSwalLoadingSpy).not.toBeCalled();
			openSwalLoadingSpy.mockRestore();
		});

		it('should set isChanged value and call swalSuccessTimer when calling toggleActiveVacancy method on successful api request, the confirmation was confirmed, and the vacancy was active', async () => {
			global.confirm = jest.fn().mockReturnValue(true);
			const swalSuccessTimerSpy = jest.spyOn(wrapper.vm, 'swalSuccessTimer');
			await axios.mockResolve({
				data: {
					status: true
				}
			});
			await wrapper.vm.toggleActiveVacancy(1);
			expect(swalSuccessTimerSpy).toBeCalled();
			expect(wrapper.vm.isChanged).toBeTruthy();
			swalSuccessTimerSpy.mockRestore();
		});

		it('should not set isChanged value when calling toggleActiveVacancy method on successful api request, the confirmation was confirmed, and the vacancy was active, but the api request status was false', async () => {
			global.confirm = jest.fn().mockReturnValue(true);
			const swalSuccessTimerSpy = jest.spyOn(wrapper.vm, 'swalSuccessTimer');
			await axios.mockResolve({
				data: {
					status: false
				}
			});
			await wrapper.vm.toggleActiveVacancy(1);
			expect(swalSuccessTimerSpy).not.toBeCalled();
			expect(wrapper.vm.isChanged).toBeFalsy();
			swalSuccessTimerSpy.mockRestore();
		});

		it('should call bugsnag, alert, and closeSwalLoading when calling toggleActiveVacancy method on failed api request, the confirmation was confirmed, and the vacancy was  active', async () => {
			global.confirm = jest.fn().mockReturnValue(true);
			const closeSwalLoadingSpy = jest.spyOn(wrapper.vm, 'closeSwalLoading');
			await axios.mockReject('error');
			await wrapper.vm.toggleActiveVacancy(1);
			await wrapper.vm.$nextTick();

			expect(closeSwalLoadingSpy).toBeCalled();
			expect(global.alert).toBeCalled();
			expect(global.bugsnagClient.notify).toBeCalled();

			closeSwalLoadingSpy.mockRestore();
		});

		it('should set isChanged value and call swalSuccessTimer when calling toggleActiveVacancy method on successful api request, the confirmation was confirmed, and the vacancy was not active', async () => {
			global.confirm = jest.fn().mockReturnValue(true);
			const swalSuccessTimerSpy = jest.spyOn(wrapper.vm, 'swalSuccessTimer');
			await axios.mockResolve({
				data: {
					status: true
				}
			});
			await wrapper.vm.toggleActiveVacancy(0);

			expect(swalSuccessTimerSpy).toBeCalled();
			expect(wrapper.vm.isChanged).toBeTruthy();

			swalSuccessTimerSpy.mockRestore();
		});

		it('should not set isChanged value when calling toggleActiveVacancy method on successful api request, the confirmation was confirmed, and the vacancy was not active, but the api request status was false', async () => {
			global.confirm = jest.fn().mockReturnValue(true);
			const swalSuccessTimerSpy = jest.spyOn(wrapper.vm, 'swalSuccessTimer');
			await axios.mockResolve({
				data: {
					status: false
				}
			});
			await wrapper.vm.toggleActiveVacancy(0);
			expect(swalSuccessTimerSpy).not.toBeCalled();
			expect(wrapper.vm.isChanged).toBeFalsy();
			swalSuccessTimerSpy.mockRestore();
		});

		it('should call bugsnag, alert, and closeSwalLoading when calling toggleActiveVacancy method on failed api request, the confirmation was confirmed, and the vacancy was not active', async () => {
			global.confirm = jest.fn().mockReturnValue(true);
			const closeSwalLoadingSpy = jest.spyOn(wrapper.vm, 'closeSwalLoading');
			await axios.mockReject('error');
			await wrapper.vm.toggleActiveVacancy(0);
			await wrapper.vm.$nextTick();

			expect(closeSwalLoadingSpy).toBeCalled();
			expect(global.alert).toBeCalled();
			expect(global.bugsnagClient.notify).toBeCalled();

			closeSwalLoadingSpy.mockRestore();
		});

		it('should call $router.push and swalSuccessTimer when calling deleteVacancy method on successful api request, and the confirmation request was confirmed by user', async () => {
			global.confirm = jest.fn().mockReturnValue(true);
			const swalSuccessTimerSpy = jest.spyOn(wrapper.vm, 'swalSuccessTimer');
			await axios.mockResolve({
				data: {
					status: true
				}
			});
			await wrapper.vm.deleteVacancy();

			expect(swalSuccessTimerSpy).toBeCalled();
			expect(wrapper.vm.$router.push).toBeCalled();

			swalSuccessTimerSpy.mockRestore();
		});

		it('should not set isChanged value when calling deleteVacancy method on successful api request and the confirmation was confirmed, but the api request status was false', async () => {
			global.confirm = jest.fn().mockReturnValue(true);
			const swalSuccessTimerSpy = jest.spyOn(wrapper.vm, 'swalSuccessTimer');
			await axios.mockResolve({
				data: {
					status: false
				}
			});
			await wrapper.vm.deleteVacancy();
			expect(swalSuccessTimerSpy).not.toBeCalled();
			expect(wrapper.vm.isChanged).toBeFalsy();
			swalSuccessTimerSpy.mockRestore();
		});

		it('should call bugsnag, alert, and closeSwalLoading when calling deleteVacancy method on failed api request, and the confirmation request was confirmed by user', async () => {
			global.confirm = jest.fn().mockReturnValue(true);
			const closeSwalLoadingSpy = jest.spyOn(wrapper.vm, 'closeSwalLoading');
			await axios.mockReject('error');
			await wrapper.vm.deleteVacancy();
			await wrapper.vm.$nextTick();

			expect(closeSwalLoadingSpy).toBeCalled();
			expect(global.alert).toBeCalled();
			expect(global.bugsnagClient.notify).toBeCalled();

			closeSwalLoadingSpy.mockRestore();
		});
	});
});
