import md5 from 'md5';
import Vuex from 'vuex';

import { createLocalVue, shallowMount } from '@vue/test-utils';

import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import mockAxios from 'tests-fe/utils/mock-axios';

import OwnerModalNeedPassword from 'Js/owner/components/OwnerModalNeedPassword.vue';

describe('OwnerModalNeedPassword.vue', () => {
	const localVue = createLocalVue();
	localVue.use(Vuex);

	mockWindowProperty('open', jest.fn());
	mockWindowProperty(
		'$',
		jest.fn().mockReturnValue({
			modal: jest.fn(),
			on: jest.fn((eventName, cb) => cb && cb())
		})
	);

	global.md5 = md5;
	global.axios = mockAxios;
	global.bugsnagClient = {
		notify: jest.fn()
	};
	global.alert = jest.fn();
	global.confirm = jest.fn().mockReturnValue(true);

	const mocks = {
		$emit: jest.fn()
	};

	const methods = {
		openSwalLoading: jest.fn(),
		closeSwalLoading: jest.fn(),
		swalSuccessTimer: jest.fn(),
		swalError: jest.fn()
	};

	const storeData = {
		state: {
			token: 'token'
		}
	};

	// mount function
	const mount = () => {
		return shallowMount(OwnerModalNeedPassword, {
			localVue,
			mocks,
			methods,
			store: new Vuex.Store(storeData)
		});
	};

	describe('render component correctly', () => {
		let wrapper;

		beforeEach(() => {
			wrapper = mount();
		});

		it('should render container properly', () => {
			expect(wrapper.find('#ownerModalNeedPassword').exists()).toBeTruthy();
		});
	});

	describe('call functions in the watch block correctly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = mount();
		});

		it('should call $emit on isClosed value change, and the value is truthy ', async () => {
			const emitSpy = jest.spyOn(wrapper.vm, '$emit');
			await wrapper.setData({
				isClosed: false
			});
			await wrapper.setData({
				isClosed: true
			});

			expect(emitSpy).toBeCalled();
		});

		it('should call $emit on isClosed value change, and the value is falsy', async () => {
			const emitSpy = jest.spyOn(wrapper.vm, '$emit');
			await wrapper.setData({
				isClosed: true
			});
			await wrapper.setData({
				isClosed: false
			});

			expect(emitSpy).toBeCalled();
		});
	});

	describe('call functions correctly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = mount();
		});

		it('should call $() when calling openModal method', async () => {
			await wrapper.vm.openModal();
			expect(global.$).toBeCalled();
		});

		it('should call $() when calling closeModal method', async () => {
			await wrapper.vm.closeModal();
			expect(global.$).toBeCalled();
		});

		it('should toggle password shown state when calling togglePassword method', async () => {
			wrapper.setData({
				password: {
					shown: true
				}
			});
			await wrapper.vm.togglePassword();
			expect(wrapper.vm.password.shown).toBeFalsy();
		});

		it('should toggle passwordRepeat shown state when calling togglePasswordRepeat method', async () => {
			wrapper.setData({
				passwordRepeat: {
					shown: true
				}
			});
			await wrapper.vm.togglePasswordRepeat();
			expect(wrapper.vm.passwordRepeat.shown).toBeFalsy();
		});

		it('should set isChanged value and call swalSuccessTimer when calling setPassword method on successful api request', async () => {
			global.confirm = jest.fn().mockReturnValue(true);
			const swalSuccessTimerSpy = jest.spyOn(wrapper.vm, 'swalSuccessTimer');
			await axios.mockResolve({
				data: {
					status: true
				}
			});
			await wrapper.vm.setPassword();
			expect(swalSuccessTimerSpy).toBeCalled();
			expect(wrapper.vm.isChanged).toBeTruthy();
			swalSuccessTimerSpy.mockRestore();
		});

		it('should call swalError when calling setPassword method on successful api request, but falsy status', async () => {
			const swalErrorSpy = jest.spyOn(wrapper.vm, 'swalError');
			await axios.mockResolve({
				data: {
					status: false,
					meta: {
						message: 'test'
					}
				}
			});
			await wrapper.vm.setPassword();
			expect(swalErrorSpy).toBeCalled();
			swalErrorSpy.mockRestore();
		});

		it('should call bugsnag, alert, and closeSwalLoading when calling setPassword method on failed api request', async () => {
			const closeSwalLoadingSpy = jest.spyOn(wrapper.vm, 'closeSwalLoading');
			await axios.mockReject('error');
			await wrapper.vm.setPassword();
			await wrapper.vm.$nextTick();
			expect(closeSwalLoadingSpy).toBeCalled();
			expect(global.alert).toBeCalled();
			expect(global.bugsnagClient.notify).toBeCalled();
			closeSwalLoadingSpy.mockRestore();
		});

		it('should set errorAlert and messageAlert when password.setup value is not equal to passwordRepeat.setup value', async () => {
			await wrapper.setData({
				password: {
					setup: 'test'
				},
				passwordRepeat: {
					setup: 'test_2'
				}
			});
			await wrapper.vm.setupPassword();
			expect(wrapper.vm.errorAlert).toBeTruthy();
			expect(wrapper.vm.messageAlert).toBe(
				'Ulangi password tidak sama dengan Password.'
			);
		});

		it('should set errorAlert and messageAlert when password.setup length is  less than minimumLengthPassword value', async () => {
			await wrapper.setData({
				minimumLengthPassword: 5,
				password: {
					setup: 'test'
				},
				passwordRepeat: {
					setup: 'test'
				}
			});
			await wrapper.vm.setupPassword();
			expect(wrapper.vm.errorAlert).toBeTruthy();
			expect(wrapper.vm.messageAlert).toBe('Password minimal 5 karakter.');
		});

		it('should set errorAlert and call setPassword function when password.setup length is not less than minimumLengthPassword value and equal to passwordRepeat.setup value', async () => {
			const setPasswordSpy = jest.spyOn(wrapper.vm, 'setPassword');
			await wrapper.setData({
				minimumLengthPassword: 5,
				password: {
					setup: 'test_2'
				},
				passwordRepeat: {
					setup: 'test_2'
				}
			});
			await wrapper.vm.setupPassword();
			expect(wrapper.vm.errorAlert).toBeFalsy();
			expect(setPasswordSpy).toBeCalled();
			setPasswordSpy.mockRestore();
		});

		it('should not call openSwalLoading when calling setPassword method and the confirmation was denied', async () => {
			global.confirm = jest.fn().mockReturnValue(false);
			const openSwalLoadingSpy = jest.spyOn(wrapper.vm, 'openSwalLoading');
			await wrapper.vm.setPassword();
			expect(openSwalLoadingSpy).not.toBeCalled();
			openSwalLoadingSpy.mockRestore();
		});
	});
});
