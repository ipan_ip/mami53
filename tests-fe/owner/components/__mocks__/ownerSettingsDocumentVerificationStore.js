export default {
	namespaced: true,
	state: {
		availableMethods: [
			{
				key: 'upload',
				description: 'Upload file foto'
			},
			{
				key: 'capture',
				description: 'Ambil foto dari kamera'
			}
		],
		selectedMethod: 'upload',
		currentState: 'intercept',
		documentTypeList: [
			{
				key: 'ktp',
				label: 'KATEPE'
			}
		],
		isLoadingFetchDocumentTypeList: false,
		selectedDocument: {
			key: 'ktp',
			label: 'KATEPE'
		},
		currentPhotoStep: 1,
		photoValue: {
			cardOnly: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/',
			selfie: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/'
		},
		isSwitchCamera: false,
		isCapturePhoto: false,
		timerCount: 0,
		isPreviewPhoto: false
	},
	mutations: {
		setSelectedMethod: jest.fn(),
		setCurrentState: jest.fn(),
		setDocumentTypeList: jest.fn(),
		setIsLoadingFetchDocumentTypeList: jest.fn(),
		setSelectedDocument: jest.fn(),
		setCurrentPhotoStep: jest.fn(),
		setPhotoValue: jest.fn(),
		setIsSwitchCamera: jest.fn(),
		setIsCapturePhoto: jest.fn(),
		setTimerCount: jest.fn(),
		setIsPreviewPhoto: jest.fn()
	},
	getters: {
		availableStates: jest.fn(),
		currentStateComponent: jest.fn(),
		lastCapturedPhotoType: jest.fn(),
		completedSteps: () => {
			return [1];
		},
		isLastPhotoStep: () => jest.fn().mockReturnValue(false),
		documentLabel: () => {
			return jest.fn(() => {
				return 'Mock Label';
			});
		},
		isShowGuideButton: jest.fn()
	},
	actions: {
		fetchDocumentTypeList: () => jest.fn(),
		initSelectedDocumentType: () => jest.fn(),
		shiftPhotoStep: () => jest.fn(),
		shiftState: () => jest.fn(),
		switchCamera: () => jest.fn(),
		capturePhoto: () => jest.fn(),
		handlePhotoValue: () => jest.fn()
	}
};
