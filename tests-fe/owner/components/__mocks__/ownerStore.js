export default {
	state: {
		token: 'mocktoken12345',
		authCheck: {},
		authData: {
			all: {
				name: 'Bambs'
			}
		},
		profile: {
			user: {
				name: 'Bambang',
				email: 'bamb@ng.com',
				phone_number: '08123456789'
			},
			// set default membership mock data to premium and bbk
			membership: {
				expired: false,
				status: 'Premium',
				is_mamipay_user: true
			}
		},
		mamipayDetail: {
			verification_data: {
				identity_card_status: 'waiting'
			}
		},
		isBookingAllRoom: false,
		extras: {
			documentTitle: document.title,
			notifCount: null,
			textToDetail: Math.random() >= 0.5 ? 'A' : 'B',
			selectedRoom: 0,
			confirmationData: null,
			membershipOnConfirmation: false
		},
		abTestResult: {
			is_active: false,
			experiment_id: 0,
			experiment_value: ''
		}
	},
	mutations: {
		setAuthCheck(state, payload) {
			state.authCheck = payload;
		},
		setAuthData(state, payload) {
			state.authData = payload;
		},
		setProfile(state, payload) {
			state.profile = payload;
		},
		setIsBookingAllRoom(state, payload) {
			state.isBookingAllRoom = payload;
		},
		setMembershipStatus(state, payload) {
			payload === 'Konfirmasi Pembayaran'
				? (state.extras.membershipOnConfirmation = true)
				: (state.extras.membershipOnConfirmation = false);
		},
		setNotifCount(state, payload) {
			state.extras.notifCount = payload;

			const documentTitle = state.extras.documentTitle;
			if (payload > 0 && payload < 10) {
				document.title = '(' + payload + ') ' + documentTitle;
			} else if (payload > 10) {
				document.title = '(9+) ' + documentTitle;
			} else if (payload === 0) {
				document.title = documentTitle;
			}
		},
		setSelectedRoom(state, payload) {
			state.extras.selectedRoom = payload;
		},

		setConfirmationData(state, payload) {
			state.extras.confirmationData = payload;
		},

		setMamipayDetail(state, payload) {
			state.mamipayDetail = payload;
		}
	}
};
