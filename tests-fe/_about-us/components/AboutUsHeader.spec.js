import { createLocalVue, shallowMount } from '@vue/test-utils';
import AboutUsHeader from 'Js/_about-us/components/AboutUsHeader.vue';

const localVue = createLocalVue();
localVue.directive('lazy', { bind: jest.fn() });

describe('AboutUsHeader.vue', () => {
	const wrapper = shallowMount(AboutUsHeader, {
		localVue
	});

	it('should render about us header', () => {
		expect(wrapper.find('#aboutUsHeader').exists()).toBe(true);
	});

	it('should render all the images', () => {
		expect(wrapper.findAll('img').length == 2).toBe(true);
	});
});
