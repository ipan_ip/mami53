import { createLocalVue, shallowMount } from '@vue/test-utils';
import AboutUsInfoDescription from 'Js/_about-us/components/AboutUsInfoDescription.vue';

const localVue = createLocalVue();

describe('AboutUsInfoDescription.vue', () => {
	const propsData = {
		title: 'Mamitest',
		description: 'Mamitest description'
	};

	const wrapper = shallowMount(AboutUsInfoDescription, { localVue, propsData });

	it('should render about us info description', () => {
		expect(wrapper.find('.about-us-info-description').exists()).toBe(true);
	});

	it('should render about us info description from props', () => {
		expect(wrapper.find('h2').exists()).toBe(true);
		expect(wrapper.find('h2').text()).toBe(propsData.title);
		expect(wrapper.find('p').exists()).toBe(true);
		expect(wrapper.find('p').text()).toBe(propsData.description);
	});
});
