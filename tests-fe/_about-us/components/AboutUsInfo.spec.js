import { createLocalVue, shallowMount } from '@vue/test-utils';
import AboutUsInfo from 'Js/_about-us/components/AboutUsInfo.vue';

const localVue = createLocalVue();
localVue.directive('lazy', (el, binding) => {
	return el.setAttribute('data-src', binding.value);
});

describe('AboutUsInfo.vue', () => {
	const ABOUT_US_INFO_DESCRIPTION_STUB = 'about-us-info-description-stub';
	const propsData = {
		isReverse: true,
		content: {
			imageUrl: 'mamitest/image.svg',
			title: 'mamitest',
			description: 'mamitest description'
		}
	};
	const wrapper = shallowMount(AboutUsInfo, { localVue, propsData });
	it('should render about us info', () => {
		expect(wrapper.find('.about-us-info').exists()).toBe(true);
	});

	it('should render props data properly', () => {
		expect(wrapper.find('.about-us-info').classes('reverse-col')).toBe(true);
		expect(
			wrapper.find(ABOUT_US_INFO_DESCRIPTION_STUB).attributes().description
		).toBe(propsData.content.description);
		expect(
			wrapper.find(ABOUT_US_INFO_DESCRIPTION_STUB).attributes().title
		).toBe(propsData.content.title);
		wrapper.setProps({ isReverse: false });
		wrapper.vm.$nextTick(() => {
			expect(wrapper.find('.about-us-info').classes('reverse-col')).toBe(false);
		});
	});

	it('should render about us info image', () => {
		expect(wrapper.find('.about-us-info-image').exists()).toBe(true);
		expect(
			wrapper.find('.about-us-info-image > img').attributes()['data-src']
		).toBe(propsData.content.imageUrl);
		expect(wrapper.find('.about-us-info-image > img').attributes()['alt']).toBe(
			propsData.content.title
		);
	});
});
