import { createLocalVue, shallowMount } from '@vue/test-utils';
import AboutUsFooter from 'Js/_about-us/components/AboutUsFooter.vue';

const localVue = createLocalVue();

describe('AboutUsFooter.vue', () => {
	const wrapper = shallowMount(AboutUsFooter, {
		localVue,
		stubs: { MamiFooter: { template: '<div></div>' } }
	});

	it('should render about us footer', () => {
		expect(wrapper.find('#about-us-footer').exists()).toBe(true);
	});
});
