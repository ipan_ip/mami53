import { createLocalVue, shallowMount } from '@vue/test-utils';
import App from 'Js/_about-us/components/App.vue';
import aboutMamikos from './__mocks__/aboutMamikos';

const localVue = createLocalVue();
const ABOUT_US_INFO_STUB = 'about-us-info-stub';
const ABOUT_US_FOOTER_STUB = 'about-us-footer-stub';

describe('App.vue', () => {
	const wrapper = shallowMount(App, {
		localVue,
		data() {
			return {
				aboutMamikos
			};
		},
		stubs: {
			NavbarCustom: true
		}
	});

	it('should render about us mamikos div', () => {
		expect(wrapper.find('#aboutUsMamikos').exists()).toBe(true);
	});

	it('navigations should not empty', () => {
		expect(
			typeof wrapper.vm.navigations === 'object' &&
				wrapper.vm.navigations.length > 0
		).toBe(true);
	});

	it('content should not empty', () => {
		expect(
			typeof wrapper.vm.aboutMamikos === 'object' &&
				wrapper.vm.aboutMamikos.length > 0
		).toBe(true);
	});

	describe('About us info', () => {
		it('should render about us info container', () => {
			expect(wrapper.find('.about-us-info-container').exists()).toBe(true);
		});

		it('should render about us info(s)', () => {
			expect(wrapper.findAll(ABOUT_US_INFO_STUB).length).toBe(
				aboutMamikos.length
			);
		});
	});

	describe('About us footer', () => {
		it('should render about us footer', () => {
			expect(wrapper.find(ABOUT_US_FOOTER_STUB).exists()).toBe(true);
		});
	});
});
