export default [
	{
		imageUrl: '/general/image/mamitest.svg',
		title: 'Mamitest 1',
		description: 'Mamitest description 1'
	},
	{
		imageUrl: '/general/image/mamitest2.svg',
		title: 'Mamitest 2',
		description: 'Mamitest description 2'
	}
];
