export default instance =>
	instance.directive('lazy', (el, binding) => {
		if (binding.value.hasOwnProperty('src'))
			el.setAttribute('data-src', binding.value.src);
		else el.setAttribute('data-src', binding.value);
	});
