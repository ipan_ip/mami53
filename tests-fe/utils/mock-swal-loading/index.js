
/***
 *  Don't forget to assign vue to window above this code like this:
 *  window.Vue = localVue;
 *  mockSwalLoading()
 * 
*/
export default () => {
	require('Js/@mixins/MixinSwalLoading');
	require('Js/@mixins/MixinSwalSuccessError');
	window.swal = require('sweetalert2/dist/sweetalert2.js');
};
