import debounce from 'lodash/debounce';
global.debounce = params => debounce(params, 0);
