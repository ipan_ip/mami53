export default (href, pathname = '') => {
	global.window = Object.create(window);
	Object.defineProperty(window, 'location', {
		value: {
			href,
			pathname
		},
		writable: true
	});
};
