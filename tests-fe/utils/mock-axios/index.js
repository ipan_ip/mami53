/**
 * Mock axios when axios is used by calling function
 * e.g axios({ method: 'GET', url: '/url' })
 * @param {boolean} isResolved
 * @param {Object} data
 *
 * for waiting resolve promise:
 * await Promise.resolve
 *
 * for waiting reject promise
 * await Promise.reject;
 * await Promise.prototype.catch;
 */
export const axiosFn = function(isResolved, data) {
	const action = isResolved ? Promise.resolve(data) : Promise.reject(data);
	return function() {
		return action;
	};
};

/**
 * import this mock:
 * import mockAxios from 'test-fe/utils/mock-axios;
 * then register it as global property:
 * global.axios = mockAxios;
 */

export default {
	data: {},
	resolved: true,
	action: function() {
		return this.resolved
			? Promise.resolve(this.data)
			: Promise.reject(this.data);
	},
	get get() {
		return this.action;
	},
	get delete() {
		return this.action;
	},
	get post() {
		return this.action;
	},
	mockResolve: function(data) {
		this.data = data;
		this.resolved = true;
	},
	mockReject: function(data) {
		this.data = data;
		this.resolved = false;
	}
};
