import mockWindowProperty from 'tests-fe/utils/mock-window-property';

const localStorageMock = (function() {
	let store = {};

	return {
		getItem: function(key) {
			return store[key] || null;
		},
		setItem: function(key, value) {
			store[key] = value.toString();
		},
		clear: function() {
			store = {};
		}
	};
})();

// const localStorageMock = {
// 	getItem: jest.fn(),
// 	setItem: jest.fn(),
// 	clear: jest.fn()
// };

mockWindowProperty('localStorage', localStorageMock);
