export default {
	template: '<div></div>'
};

export const mockComponent = {
	template: '<div></div>'
};

export const mockComponentWithAttrs = (attributes = null) => {
	let strAttributes = '';
	if (attributes !== null && typeof attributes === 'object') {
		strAttributes = Object.keys(attributes).reduce(function(result, key) {
			return (result += ` ${key}="${attributes[key]}"`);
		}, '');
	}
	return {
		template: `<div ${strAttributes}><slot></slot></div>`
	};
};
