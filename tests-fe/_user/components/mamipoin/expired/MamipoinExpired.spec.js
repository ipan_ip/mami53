import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import moment from 'moment';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mockAxios from 'tests-fe/utils/mock-axios';
import mixinMamipoin from 'Js/_user/mixins/mixinMamipoin.js';
import MamipoinExpired from 'Js/_user/components/mamipoin/expired/MamipoinExpired.vue';
import MamipoinExpiredList from 'Js/_user/components/mamipoin/expired/MamipoinExpiredList.vue';

const stubs = {
	MamipoinExpiredList
};

const mocks = {
	swalError: jest.fn(),

	$router: {
		push: jest.fn()
	},

	navigator: {
		isMobile: true
	}
};

const mockStore = {
	state: {
		totalPoin: {
			point: 1,
			near_expired_point: 1,
			near_expired_date: moment()
		}
	}
};

const mockGetElementByid = (wrapper, valueScrollTop) => {
	document.getElementById = p => {
		const el = wrapper.find(`#${p}`).element;
		if (el) {
			el.scrollTop = valueScrollTop;
			el.style.height = 0;
		}
		return el;
	};
};

describe('MamipoinExpired.vue', () => {
	const localVue = createLocalVue();
	localVue.use(Vuex);
	localVue.mixin([mixinMamipoin]);
	mockVLazy(localVue);
	global.innerWidth = 1280;
	global.axios = mockAxios;
	global.tracker = jest.fn();
	global.bugsnagClient = {
		notify: jest.fn()
	};
	const mountComponent = () => {
		return shallowMount(MamipoinExpired, {
			localVue,
			stubs,
			mocks,
			store: new Vuex.Store(mockStore)
		});
	};

	let wrapper;
	beforeEach(() => {
		wrapper = mountComponent();
		mockGetElementByid(wrapper, 80);
	});

	describe('render component MamipoinExpired correctly', () => {
		it('should render title in desktop version correctly', () => {
			expect(wrapper.find('.main-container__title')).toBeTruthy();
		});

		it('should render close button when isMobile value is "true" or width of screen no more than 768', () => {
			global.innerWidth = 375;
			expect(wrapper.find('.b-close')).toBeTruthy();
		});
	});

	describe('computed value in MamipoinExpired Component', () => {
		describe('totalPoint', () => {
			it('should return poin when totalpoin from store is not null or more than 0 ', () => {
				expect(wrapper.vm.totalPoint).toBe('1');
			});

			it('should return 0 when totalpoin from store is  null or 0 ', async () => {
				wrapper.vm.$store.state.totalPoin.point = null;
				expect(wrapper.vm.totalPoint).toBe('0');
			});
		});

		describe('isTenanBlacklist', () => {
			it('should return true when isBlacklistOnExpiredPoin is null', () => {
				wrapper.vm.isBlacklistOnExpiredPoin = null;
				expect(wrapper.vm.isTenanBlacklist).toBeTruthy();
				// restore condition
				wrapper.vm.isBlacklistOnExpiredPoin = 1;
			});

			it('should return true when isBlacklistOnExpiredPoin is null', () => {
				expect(wrapper.vm.isTenanBlacklist).toBeFalsy();
			});
		});
	});

	describe('should execute method in MamipoinExpired correctly', () => {
		describe('closeMamipoinExpired', () => {
			it('should change current route to "/mamipoin"', () => {
				wrapper.vm.closeMamipoinExpired();
				expect(wrapper.vm.$router.push).toHaveBeenCalledWith({
					path: '/mamipoin'
				});
			});
		});

		describe('ChangeTopNavigation', () => {
			it('should set value in variable "scrolled" true when scrollTop more than 74 pixel', () => {
				mockGetElementByid(wrapper, 80);
				wrapper.vm.changeTopNavigation();
				expect(wrapper.vm.scrolled).toBeTruthy();
			});

			it('should set value in variable "scrolled" false when scrollTop no more than 74 pixel', () => {
				mockGetElementByid(wrapper, 20);
				wrapper.vm.changeTopNavigation();
				expect(wrapper.vm.scrolled).toBeFalsy();
			});
		});

		describe('onScroll', () => {
			it('should execute method "changeTopNavigation" whe user scrolled and isMObile value is "true"', () => {
				const changeTopNavigationSpy = jest.spyOn(
					wrapper.vm,
					'changeTopNavigation'
				);
				wrapper.vm.onScroll();
				wrapper.find('.main-container').trigger('scroll');
				expect(changeTopNavigationSpy).toBeCalled();
				changeTopNavigationSpy.mockRestore();
			});
		});

		describe('toThousand', () => {
			it('should return value in thousand format', () => {
				expect(wrapper.vm.toThousand(1000)).toBe('1.000');
			});

			it('should stop function when poin is 0 ', () => {
				expect(wrapper.vm.toThousand(0)).toBeFalsy();
			});
		});

		describe('sendMamipoinExpiredPageTrackerEvent', () => {
			it('should call tracker function send data with desktop interface ', () => {
				const trackerSpy = jest.spyOn(global, 'tracker');
				wrapper.vm.sendMamipoinExpiredPageTrackerEvent();
				expect(trackerSpy).toBeCalled();
				trackerSpy.mockRestore();
				window.innerWidth = 375;
			});
			it('should call tracker function send data with mobile interface ', () => {
				const trackerSpy = jest.spyOn(global, 'tracker');
				wrapper.vm.sendMamipoinExpiredPageTrackerEvent();
				expect(trackerSpy).toBeCalled();
				trackerSpy.mockRestore();
			});
		});

		describe('watching value on isTenanBlacklist , and should redirect when value is changes', () => {
			it('redirect to profile when isBlacklistOnExpiredPoin null', () => {
				wrapper.vm.isBlacklistOnExpiredPoin = null;
				expect(wrapper.vm.$router.push).toBeCalledWith({ path: '/kost-saya' });
			});
		});
	});
});
