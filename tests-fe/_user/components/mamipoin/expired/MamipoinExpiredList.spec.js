import { createLocalVue, shallowMount } from '@vue/test-utils';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import moment from 'moment';
import MamipoinExpiredList from 'Js/_user/components/mamipoin/expired/MamipoinExpiredList.vue';
import MamipoinSkeleton from 'Js/_user/components/mamipoin/MamipoinSkeleton';
import MamipoinExpiredEmpty from 'Js/_user/components/mamipoin/expired/MamipoinExpiredEmpty';

const stubs = {
	MamipoinSkeleton,
	MamipoinExpiredEmpty,
	RouterLink: true
};
const propsData = {
	listData: [{ expired_date: moment(), total_point: 1 }],
	loadListData: false
};

describe('MamipoinExpiredList.vue', () => {
	const localVue = createLocalVue();
	mockVLazy(localVue);

	const mountComponent = () => {
		return shallowMount(MamipoinExpiredList, {
			localVue,
			stubs,
			propsData,
			moment: moment
		});
	};

	let wrapper;
	beforeEach(() => {
		wrapper = mountComponent();
	});

	describe('render MamiponExpiredList correctly', () => {
		it('should render title on MamipoinExpiredList compoenent properly', () => {
			expect(wrapper.find('.table-title')).toBeTruthy();
		});

		it('should render MamipoinExpiredEmpty when list Data is no more than 0', () => {
			wrapper.setProps({ listData: [] });
			expect(wrapper.findComponent(MamipoinExpiredList)).toBeTruthy();
		});

		it('should render MamipoinSkeleton when isLoad is "true"', () => {
			wrapper.setProps({ isLoad: true });
			expect(wrapper.findComponent(MamipoinSkeleton)).toBeTruthy();
		});
	});

	describe('computed value in component MamipoinExpiredList', () => {
		it('should allListData return value "false" when listData no more than 0', () => {
			wrapper.setProps({ listData: [] });
			expect(wrapper.vm.allListData).toBeFalsy();
		});
	});

	describe('methods in component MamipoinExpiredList', () => {
		describe('toMounth', () => {
			it('should return date in Month format', () => {
				expect(wrapper.vm.toMonth(moment())).toBe(
					moment().format('DD MMM YYYY')
				);
			});
		});

		describe('toThousand', () => {
			it('should return poin in thousand format when poin more than 0', () => {
				expect(wrapper.vm.toThousand(1000)).toBe('1.000');
			});

			it('should retun false when poin no more than 0', () => {
				expect(wrapper.vm.toThousand(0)).toBeFalsy();
			});
		});
	});
});
