import Vuex from 'vuex';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import { createLocalVue, shallowMount } from '@vue/test-utils';
import MamipoinExpiredEmpty from 'Js/_user/components/mamipoin/expired/MamipoinExpiredEmpty.vue';

const stubs = {
	RouterLink: true
};

const mockStore = {
	state: {
		totalPoin: {
			point: 1
		}
	}
};
describe('MamipoinExpiredEmpty.vue', () => {
	const localVue = createLocalVue();
	mockVLazy(localVue);
	localVue.use(Vuex);
	global.tracker = jest.fn();

	const mountComponent = () => {
		return shallowMount(MamipoinExpiredEmpty, {
			localVue,
			stubs,
			store: new Vuex.Store(mockStore)
		});
	};

	let wrapper;
	beforeEach(() => {
		wrapper = mountComponent();
	});

	describe('render component MamipoinExpiredEmpty', () => {
		it('should render title properly', () => {
			expect(wrapper.find('.title')).toBeTruthy();
		});
	});

	describe('computed value MamipoinExpiredEmpty', () => {
		it('should isMobile return false when screen more than 768 ', () => {
			window.innerWidth = 1440;
			expect(wrapper.vm.isMobile).toBeFalsy();
		});
		it('should isMobile return true when screen more no than 768 ', () => {
			window.innerWidth = 370;
			expect(wrapper.vm.isMobile).toBeTruthy();
		});
	});

	describe('execute method in MamipoinExpiredEmpty', () => {
		it('should send tracker data with interface mobile when screen no more than 768', () => {
			const trackerSpy = jest.spyOn(global, 'tracker');
			wrapper.vm.sendMamipoinGuidelineTrackerEvent();
			expect(trackerSpy).toBeCalledWith('moe', [
				'[User] MamiPoin Guideline Tab Viewed',
				{
					interface: 'mobile',
					point_total: 1,
					redirection_source: '[User] MamiPoin Expired Info Page'
				}
			]);
			trackerSpy.mockRestore();
		});

		it('should send tracker data with interface mobile when screen more than 768', () => {
			window.innerWidth = 1440;
			const trackerSpy = jest.spyOn(global, 'tracker');
			wrapper.vm.sendMamipoinGuidelineTrackerEvent();
			expect(trackerSpy).toBeCalledWith('moe', [
				'[User] MamiPoin Guideline Tab Viewed',
				{
					interface: 'desktop',
					point_total: 1,
					redirection_source: '[User] MamiPoin Expired Info Page'
				}
			]);
			trackerSpy.mockRestore();
		});
	});
});
