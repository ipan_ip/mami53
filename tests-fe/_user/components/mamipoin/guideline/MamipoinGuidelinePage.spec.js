import { createLocalVue, shallowMount } from '@vue/test-utils';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import MamipoinGuidelinePage from 'Js/_user/components/mamipoin/guideline/MamipoinGuideLinePage.vue';
import MamipoinSkeleton from 'Js/_user/components/mamipoin/MamipoinSkeleton';

const stubs = {
	MamipoinSkeleton,
	MamipoinGuidelinePage
};

const propsData = {
	content: [{ title: 'test', description: '<div>test</div>' }],
	isLoad: false
};

describe('MamipoinGuidelinePage.vue', () => {
	const localVue = createLocalVue();
	mockVLazy(localVue);

	const mountComponent = () => {
		return shallowMount(MamipoinGuidelinePage, {
			stubs,
			propsData
		});
	};

	let wrapper;
	beforeEach(() => {
		wrapper = mountComponent();
	});

	describe('render component MamipoinGuidelinePage', () => {
		it('should render titile Mamipoin Guideline correctly', () => {
			expect(wrapper.find('.title')).toBeTruthy();
		});

		it('should render MamipoinSkeleton.vue when isLoad is "true"', () => {
			wrapper.setProps({ isLoad: true });
			expect(wrapper.findComponent(MamipoinSkeleton)).toBeTruthy();
		});
	});
});
