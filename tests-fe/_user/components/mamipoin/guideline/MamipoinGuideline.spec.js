import { createLocalVue, shallowMount } from '@vue/test-utils';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mockAxios from 'tests-fe/utils/mock-axios';
import mixinMamipoin from 'Js/_user/mixins/mixinMamipoin.js';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import MamipoinGuideline from 'Js/_user/components/mamipoin/guideline/MamipoinGuideline.vue';
import MamipoinGuidelinePage from 'Js/_user/components/mamipoin/guideline/MamipoinGuideLinePage.vue';
import MamipoinTnC from 'Js/_user/components/mamipoin/guideline/MamipoinTnC.vue';

const stubs = {
	MamipoinTnC,
	MamipoinGuidelinePage
};

const mocks = {
	swalError: jest.fn(),
	$store: {
		state: {
			totalPoin: { point: 1 }
		}
	},
	$router: {
		push: jest.fn()
	}
};

const notify = jest.fn();
mockWindowProperty('bugsnagClient', { notify });

describe('MamipoinGuideline.vue', () => {
	const localVue = createLocalVue();
	localVue.mixin([mixinMamipoin]);
	mockVLazy(localVue);
	window.innerWidth = 1440;
	global.axios = mockAxios;
	global.tracker = jest.fn();
	const mountComponent = () => {
		return shallowMount(MamipoinGuideline, {
			stubs,
			mocks
		});
	};

	let wrapper;
	beforeEach(() => {
		wrapper = mountComponent();
	});

	describe('render MamipoinGuideline correctly', () => {
		it('should render tab selection menu', () => {
			expect(wrapper.find('.tab')).toBeTruthy();
		});
	});

	describe('computed value on MamipoinGuideline', () => {
		it('should computed isMobile value is "false" when width screen  more than 768 pixel', () => {
			expect(wrapper.vm.isMobile).toBeFalsy();
		});
	});

	describe('execute methods on MamipoinGuideline', () => {
		describe('currentActive', () => {
			it('should set value on variable "selected" with data that be passed on paramater in currentActive Methods', () => {
				wrapper.vm.curentActive('test');
				expect(wrapper.vm.selected).toBe('test');
			});

			it('should set value on variable "selected" with data that be passed on paramater in currentActive Methods', () => {
				const sendMamipoinGuidelineTrackerEventSpy = jest.spyOn(
					wrapper.vm,
					'sendMamipoinGuidelineTrackerEvent'
				);
				wrapper.vm.curentActive('guideline');
				expect(sendMamipoinGuidelineTrackerEventSpy).toBeCalled();
				sendMamipoinGuidelineTrackerEventSpy.mockRestore();
			});
		});

		describe('closeMamipoinGuideline', () => {
			it('should redirect to "/mamipoin" when function closeMamipoinGuideline called', () => {
				wrapper.vm.closeMamipoinGuideline();
				expect(wrapper.vm.$router.push).toHaveBeenCalledWith({
					path: '/mamipoin'
				});
			});
		});

		describe('sendMamipoinGuidelineTrackerEvent', () => {
			it('should call tracker function send data with desktop interface ', () => {
				const trackerSpy = jest.spyOn(global, 'tracker');
				wrapper.vm.sendMamipoinGuidelineTrackerEvent();
				expect(trackerSpy).toBeCalled();
				trackerSpy.mockRestore();
				window.innerWidth = 375;
			});
			it('should call tracker function send data with mobile interface ', () => {
				const trackerSpy = jest.spyOn(global, 'tracker');
				wrapper.vm.sendMamipoinGuidelineTrackerEvent();
				expect(trackerSpy).toBeCalled();
				trackerSpy.mockRestore();
			});
		});
	});
});
