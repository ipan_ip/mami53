import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import MamipoinTnC from 'Js/_user/components/mamipoin/guideline/MamipoinTnC.vue';
import MamipoinSkeleton from 'Js/_user/components/mamipoin/MamipoinSkeleton';

const mockStore = {
	state: {
		totalPoin: {
			point: 1,
			near_expired_point: 1
		}
	}
};
const stubs = {
	MamipoinSkeleton
};

const propsData = {
	content: '<div>test</div>',
	isLoad: false
};

describe('MamipoinTnC.vue', () => {
	const localVue = createLocalVue();
	localVue.use(Vuex);
	mockVLazy(localVue);
	global.tracker = jest.fn();

	const mountComponent = () => {
		return shallowMount(MamipoinTnC, {
			localVue,
			stubs,
			propsData,
			store: new Vuex.Store(mockStore)
		});
	};

	let wrapper;
	beforeEach(() => {
		wrapper = mountComponent();
	});

	describe('render component MamipoinTnC', () => {
		it('should render page MamipoinTnC correctly', () => {
			expect(wrapper.find('.main-containe')).toBeTruthy();
		});
		it('should render MamipoinSkeleton.vue when isLoad is "true"', () => {
			wrapper.setProps({ isLoad: true });
			expect(wrapper.findComponent(MamipoinSkeleton)).toBeTruthy();
		});
	});

	describe('computed value in Mamipoin TnC', () => {
		describe('isMobile', () => {
			it('should return false when width screen more than 768 ', () => {
				expect(wrapper.vm.isMobile).toBeFalsy();
			});
		});
	});
	describe('Method in MamipoinTnC component', () => {
		describe('sendMamipoinTncTrackerEvent', () => {
			it('should call tracker function send data with desktop interface ', () => {
				const trackerSpy = jest.spyOn(global, 'tracker');
				wrapper.vm.sendMamipoinTncTrackerEvent();
				expect(trackerSpy).toBeCalled();
				trackerSpy.mockRestore();
				window.innerWidth = 375;
			});
			it('should call tracker function send data with mobile interface ', () => {
				const trackerSpy = jest.spyOn(global, 'tracker');
				wrapper.vm.sendMamipoinTncTrackerEvent();
				expect(trackerSpy).toBeCalled();
				trackerSpy.mockRestore();
			});
		});
	});
});
