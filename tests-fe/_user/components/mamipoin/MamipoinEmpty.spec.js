import { createLocalVue, shallowMount } from '@vue/test-utils';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import MamipoinEmpty from 'Js/_user/components/mamipoin/MamipoinEmpty.vue';

describe('MamipoinEmpty.vue', () => {
	const localVue = createLocalVue();
	mockVLazy(localVue);

	const mountComponent = () => {
		return shallowMount(MamipoinEmpty, {
			localVue
		});
	};

	let wrapper;

	beforeEach(() => {
		wrapper = mountComponent();
	});

	describe('render component MamipoinEmpty', () => {
		it('should render  title with content "Riwayat Masih Kosong"', () => {
			const title = wrapper.find('.title');
			expect(title.text()).toBe('Riwayat Masih Kosong');
		});

		it('should render footer card with content "Penerimaan dan penukaran poin Kamu akan tercatat di halaman ini."', () => {
			const footer = wrapper.find('.footer__text');
			expect(footer.text()).toBe(
				'Penerimaan dan penukaran poin Kamu akan tercatat di halaman ini.'
			);
		});
	});
});
