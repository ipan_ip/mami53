import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import moment from 'moment';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mixinMamipoin from 'Js/_user/mixins/mixinMamipoin.js';
import MamipoinEmpty from 'Js/_user/components/mamipoin/MamipoinEmpty.vue';
import MamipoinSkeleton from 'Js/_user/components/mamipoin/MamipoinSkeleton.vue';
import MamipoinListData from 'Js/_user/components/mamipoin/list-poin/MamipoinListData.vue';

const sotreData = {
	state: {
		totalPoin: {
			point: 2,
			near_expired_point: 10,
			near_expired_date: moment()
		}
	}
};
const stubs = {
	MamipoinSkeleton,
	MamipoinEmpty
};

const propsData = {
	history: [
		{
			title: 'test',
			value: -1,
			date: moment(),
			is_recent: true
		},
		{
			title: 'test2',
			value: 1,
			date: moment(),
			is_recent: false
		}
	],
	isLoading: false
};

describe('MamipoinListData.vue', () => {
	const localVue = createLocalVue();
	localVue.mixin([mixinMamipoin]);
	localVue.use(Vuex);
	mockVLazy(localVue);

	const mountComponent = () => {
		return shallowMount(MamipoinListData, {
			localVue,
			stubs,
			propsData,
			moment: moment,
			store: new Vuex.Store(sotreData)
		});
	};

	let wrapper;
	beforeEach(() => {
		wrapper = mountComponent();
	});

	describe('should render component MamipoinListData correctly', () => {
		it('should render title date when is_recent value is "true"', () => {
			expect(wrapper.find('.title')).toBeTruthy();
		});

		it('should render MamipoinSkeleton when isLoading value is true', () => {
			wrapper.setProps({ isLoading: true });
			expect(wrapper.findComponent(MamipoinSkeleton)).toBeTruthy();
		});

		it('should render MamipoinEmpty when isLoading value is false and totalListData no more than 0', () => {
			wrapper.setProps({ isLoading: false, history: [] });
			expect(wrapper.findComponent(MamipoinEmpty)).toBeTruthy();
		});
	});

	describe('computed value in components MamipoinListData', () => {
		describe('totalListData', () => {
			it('should return length of total value in props history', () => {
				expect(wrapper.vm.totalListData).toBe(2);
			});
		});

		describe('isMobile', () => {
			it('should return value when width of screen no more than 768', () => {
				window.innerWidth = 375;
				expect(wrapper.vm.isMobile).toBeTruthy();
			});
		});
	});

	describe('should execute method in MamipoinListData correctly', () => {
		describe('isPoinUsed', () => {
			it('should return true when poin include "-"  ', () => {
				expect(wrapper.vm.isPoinUsed(-1)).toBeTruthy();
			});
		});

		describe('toMonth', () => {
			it('should return only month and year correctly', () => {
				expect(wrapper.vm.toMonth(moment())).toBe(moment().format('MMMM YYYY'));
			});
		});

		describe('toThousand', () => {
			it('should return poin in thousand format', () => {
				expect(wrapper.vm.toThousand(1000)).toBe('1.000');
			});
		});
	});
});
