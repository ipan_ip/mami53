import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mixinMamipoin from 'Js/_user/mixins/mixinMamipoin.js';
import MamipoinHistory from 'Js/_user/components/mamipoin/MamipoinHistory';

const mockPoinHistoryTab = {
	template: "<div class='historyTab'></div>"
};

const mockStore = {
	state: {
		totalPoin: {
			point: 1
		},
		isBlacklistedOnHistory: false
	}
};

const stubs = {
	PoinHistoryTab: mockPoinHistoryTab
};

const mocks = {
	$router: {
		push: jest.fn()
	}
};

describe('MamipoinHistory.vue', () => {
	const localVue = createLocalVue();
	localVue.use(Vuex);
	localVue.mixin([mixinMamipoin]);
	mockVLazy(localVue);
	const mount = () => {
		return shallowMount(MamipoinHistory, {
			localVue,
			stubs,
			mocks,
			store: new Vuex.Store(mockStore)
		});
	};

	let wrapper;
	beforeEach(() => {
		wrapper = mount();
	});

	describe('render component MamipoinHistory', () => {
		it('should render title Riwayat Poin', () => {
			expect(wrapper.find('.title')).toBeTruthy();
		});
		it('should render render component PoinHistoryTab properly', () => {
			expect(wrapper.find('HistoryTab')).toBeTruthy();
		});
	});

	describe('computed value  MamipoinHistory Component', () => {
		it('should return true when isBlacklistedOnHistory is true ', () => {
			wrapper.vm.$store.state.isBlacklistedOnHistory = true;
			expect(wrapper.vm.blacklistFromHistory).toBeTruthy();
		});
		it('should return false when data isBlacklistedOnHistory is false ', () => {
			wrapper.vm.$store.state.isBlacklistedOnHistory = false;
			expect(wrapper.vm.blacklistFromHistory).toBeFalsy();
		});
	});
});
