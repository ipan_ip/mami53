import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import Vuex from 'vuex';
import mockAxios from 'tests-fe/utils/mock-axios';
import mixinMamipoin from 'Js/_user/mixins/mixinMamipoin.js';
import { createLocalVue, shallowMount } from '@vue/test-utils';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import PoinHistoryTab from 'Js/_user/components/mamipoin/PoinHistoryTab';
import MamipoinListData from 'Js/_user/components/mamipoin/list-poin/MamipoinListData';

const mockPoinHistoryTabMobile = {
	template: "<div class='c-mobile'></div>"
};

const stubs = {
	PoinHistoryTabMobile: mockPoinHistoryTabMobile,
	MamipoinListData
};

const mocks = {
	swalError: jest.fn(),
	$store: {
		state: {
			totalPoin: { point: 1 }
		}
	}
};

const mocksStore = {
	state: {
		totalPoin: { point: 1 }
	}
};

describe('PoinHistoryTab.vue', () => {
	const localVue = createLocalVue();
	localVue.use(Vuex);
	mockVLazy(localVue);

	// implement mixin
	localVue.mixin([mixinMamipoin]);

	// global setting components
	global.innerWidth = 1440;
	global.axios = mockAxios;
	global.bugsnagClient = {
		notify: jest.fn()
	};

	const mountData = () => {
		return shallowMount(PoinHistoryTab, {
			stubs,
			mocks,
			store: new Vuex.Store(mocksStore)
		});
	};

	let wrapper;

	beforeEach(() => {
		wrapper = mountData();

		mockWindowProperty('innerWidth', 1440);
		mockWindowProperty('isMobile', false);
	});
	describe('render component PoinHistoryTab correctly', () => {
		it('should render PoinHistoryTab properly when device is not mobile', () => {
			expect(wrapper.find('.b-tab')).toBeTruthy();
		});

		it('should render PoinHistoryTabMobile when isMobile true', () => {
			global.innerWidth = 300;
			expect(wrapper.find('.c-mobile')).toBeTruthy();
		});

		it('should render MamipoinListData correctly', () => {
			expect(wrapper.findComponent(MamipoinListData)).toBeTruthy();
		});
	});

	describe('computed value in MamipoinHistoryTab component', () => {
		it('should computed value isMobile false when screeen more than 768', () => {
			expect(wrapper.vm.isMobile).toBeFalsy();
		});
	});

	describe('run method in MamipoinHistoryTab component', () => {
		describe('currentActive', () => {
			it('should execute getRecievedPoin when menu is "recieved" and loadEarnPoin is false', () => {
				const getRecievedPoinSpy = jest.spyOn(wrapper.vm, 'getRecievedPoin');
				wrapper.vm.curentActive('recieved');
				expect(getRecievedPoinSpy).toBeCalled();
				getRecievedPoinSpy.mockRestore();
			});

			it('should execute getRedeemPoin when menu is "exchanged" and loadRedeemPoin is false', () => {
				const getRedeemPoinspy = jest.spyOn(wrapper.vm, 'getRedeemPoin');
				wrapper.vm.curentActive('exchanged');
				expect(getRedeemPoinspy).toBeCalled();
				getRedeemPoinspy.mockRestore();
			});

			it('should execute getExpiredPoin when menu is "expired" and loadExpiredPoin is false', () => {
				const getExpiredPoinspy = jest.spyOn(wrapper.vm, 'getExpiredPoin');
				wrapper.vm.curentActive('expired');
				expect(getExpiredPoinspy).toBeCalled();
				getExpiredPoinspy.mockRestore();
			});
		});

		describe('infiniteScoll', () => {
			it('should trigger scroll event when user scroll page', () => {
				const scrollSpy = jest.spyOn(window, 'onscroll');
				window.onscroll();
				expect(scrollSpy).toBeCalled();
				scrollSpy.mockRestore();
			});

			it('should run nextAllPoint when isActive is "all" , nextPage is true and scrolled bottomOfWindow is true', () => {
				const nextAllPointSpy = jest.spyOn(wrapper.vm, 'nextAllPoint');
				wrapper.vm.isActive = 'all';
				wrapper.vm.allPoinHasNextPage = true;
				wrapper.vm.loadAllPoin = false;
				window.onscroll();
				expect(nextAllPointSpy).toBeCalled();
				nextAllPointSpy.mockRestore();
			});

			it('should run nextEarnPoint when isActive is "recieved" , nextPage is true and scrolled bottomOfWindow is true', () => {
				const nextEarnPointSpy = jest.spyOn(wrapper.vm, 'nextEarnPoint');
				wrapper.vm.isActive = 'recieved';
				wrapper.vm.earnPoinHasNextPage = true;
				wrapper.vm.loadEarnPoin = false;
				window.onscroll();
				expect(nextEarnPointSpy).toBeCalled();
				nextEarnPointSpy.mockRestore();
			});

			it('should run nextRedeemPoint when isActive is "exchanged" , nextPage is true and scrolled bottomOfWindow is true', () => {
				const nextRedeemPointSpy = jest.spyOn(wrapper.vm, 'nextRedeemPoint');
				wrapper.vm.isActive = 'exchanged';
				wrapper.vm.redeemPoinHasNextPage = true;
				wrapper.vm.loadRedeemPoin = false;
				window.onscroll();
				expect(nextRedeemPointSpy).toBeCalled();
				nextRedeemPointSpy.mockRestore();
			});

			it('should run nextRedeemPoint when isActive is "expired" , nextPage is true and scrolled bottomOfWindow is true', () => {
				const nextExpiredPointSpy = jest.spyOn(wrapper.vm, 'nextExpiredPoint');
				wrapper.vm.isActive = 'expired';
				wrapper.vm.expiredPoinHasNextPage = true;
				wrapper.vm.loadExpiredPoin = false;
				window.onscroll();
				expect(nextExpiredPointSpy).toBeCalled();
				nextExpiredPointSpy.mockRestore();
			});
			it('should stop infinescroll when battomOfWindow is false', () => {
				window.pageYOffset = -1000;
				window.onscroll();
				expect(wrapper.vm.infiniteScoll()).toBeFalsy();
			});
		});
	});
});
