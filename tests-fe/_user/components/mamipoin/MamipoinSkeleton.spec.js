import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import { createLocalVue, shallowMount } from '@vue/test-utils';
import MamipoinSkeleton from 'Js/_user/components/mamipoin/MamipoinSkeleton';
import SkeletonLoader from 'Js/@components/SkeletonLoader';

const propsData = {
	type: 'Data'
};

describe('MamipoinSkeleton.vue', () => {
	const localVue = createLocalVue();
	mockVLazy(localVue);
	global.innerWidth = 300;
	const mount = () => {
		return shallowMount(MamipoinSkeleton, {
			propsData
		});
	};

	let wrapper;

	beforeEach(() => {
		wrapper = mount();
	});

	describe('render component MamipoinSkeleton', () => {
		it('should render component MamipoinSkeleton properly', () => {
			expect(wrapper.find('.skeleton')).toBeTruthy();
		});

		it('should render skeleton loader component correctly', () => {
			expect(wrapper.findComponent(SkeletonLoader)).toBeTruthy();
		});
	});

	describe('computed value MamipoinSkeleton', () => {
		describe('isMobile', () => {
			it('should computed value isMobile true when width of screen no more than 768', () => {
				expect(wrapper.vm.isMobile).toBeTruthy();
			});
		});
	});
});
