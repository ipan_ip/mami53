import { createLocalVue, shallowMount } from '@vue/test-utils';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import MamipoinCardTotalPoin from 'Js/_user/components/mamipoin/MamipoinCardTotalPoin.vue';

const stubs = {
	RouterLink: true
};

const propsData = {
	title: 'test',
	content: 'test',
	url: '/test',
	poin: '50',
	nearExpiredPoin: '0',
	info: 'test'
};

describe('MamipoinCardTotalPoint.vue', () => {
	const localVue = createLocalVue();
	mockVLazy(localVue);

	const mountComponent = () => {
		return shallowMount(MamipoinCardTotalPoin, {
			localVue,
			stubs,
			propsData
		});
	};

	let wrapper;

	beforeEach(() => {
		wrapper = mountComponent();
	});

	describe('should render componen MamipoinCardTotalPoint correctly', () => {
		it('should render title MamipoinCard properly', () => {
			expect(wrapper.find('.card__title')).toBeTruthy();
		});

		it('should render info with content "Tidak ada poin yang tersedia" when nearpoinexpired poin is 0', () => {
			const infoPoin = wrapper.find('.card__info-poin');
			expect(infoPoin.text()).toBe('Tidak ada poin yang tersedia');
		});

		it('should render info with content poin when nearpoinexpired poin is more than 0', () => {
			wrapper.setProps({ nearExpiredPoin: '1' });
			wrapper.vm.$nextTick(() => {
				const infoPoin = wrapper.find('.card__info-poin');
				expect(infoPoin.text()).toBe('test');
			});
		});
	});
});
