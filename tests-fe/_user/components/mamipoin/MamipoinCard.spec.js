import Vuex from 'vuex';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mixinMamipoin from 'Js/_user/mixins/mixinMamipoin.js';
import { createLocalVue, shallowMount } from '@vue/test-utils';
import MamipoinCard from 'Js/_user/components/mamipoin/MamipoinCard.vue';

const stubs = {
	RouterLink: true
};
const propsData = {
	title: 'test',
	content: 'test',
	url: '/test',
	isContentLong: true
};

const mocksStore = {
	state: {
		totalPoin: { point: 1 },
		redirectionSource: 'test'
	}
};
describe('MamipoinCard.vue', () => {
	const localVue = createLocalVue();
	localVue.mixin([mixinMamipoin]);
	localVue.use(Vuex);
	mockVLazy(localVue);
	window.innerWidth = 375;
	global.tracker = jest.fn();

	const mountComponent = () => {
		return shallowMount(MamipoinCard, {
			localVue,
			stubs,
			propsData,
			store: new Vuex.Store(mocksStore)
		});
	};

	let wrapper;
	beforeEach(() => {
		wrapper = mountComponent();
	});

	describe('render component MamipoinCard.vue', () => {
		it('should render title card correctly', () => {
			expect(wrapper.find('.card__title')).toBeTruthy();
		});
	});

	describe('computed value in MamipoinCard components', () => {
		describe('isMobile', () => {
			it('should computed value isMobile false when screeen more than 768', () => {
				window.innerWidth = 1440;
				expect(wrapper.vm.isMobile).toBeFalsy();
			});
		});
	});

	describe('methods in MamipoinCard components', () => {
		describe('sendEvent', () => {
			it('should call sendMamipoinHistoryPageTrackerEvent function when page is "Riwayat Poin"', () => {
				const sendMamipoinHistoryPageTrackerEventSpy = jest.spyOn(
					wrapper.vm,
					'sendMamipoinHistoryPageTrackerEvent'
				);

				wrapper.vm.sendEvent('Riwayat Poin');
				expect(sendMamipoinHistoryPageTrackerEventSpy).toBeCalled();
				sendMamipoinHistoryPageTrackerEventSpy.mockRestore();
			});

			it('should call sendMamipoinGuidelineTrackerEvent function when page is "GuideLine"', () => {
				const sendMamipoinGuidelineTrackerEventSpy = jest.spyOn(
					wrapper.vm,
					'sendMamipoinGuidelineTrackerEvent'
				);

				wrapper.vm.sendEvent('Guideline');
				expect(sendMamipoinGuidelineTrackerEventSpy).toBeCalled();
				sendMamipoinGuidelineTrackerEventSpy.mockRestore();
			});
		});

		describe('sendMamipoinGuidelineTrackerEvent', () => {
			it('should send tracker data with interface desktop when screen more than 768', () => {
				window.innerWidth = 1440;
				wrapper.vm.sendMamipoinGuidelineTrackerEvent();
				wrapper.vm.$nextTick();
				expect(tracker).toBeCalledWith('moe', [
					'[User] MamiPoin History Page Visited',
					{
						interface: 'desktop',
						point_total: '1',
						redirection_source: 'test'
					}
				]);
			});

			it('should send tracker data with interface mobile when screen no more than 768', () => {
				window.innerWidth = 375;
				wrapper.vm.sendMamipoinGuidelineTrackerEvent();
				wrapper.vm.$nextTick();
				expect(tracker).toBeCalled();
			});
		});

		describe('sendMamipoinHistoryPageTrackerEvent', () => {
			it('should send tracker data with interface desktop when screen more than 768', () => {
				wrapper.vm.sendMamipoinHistoryPageTrackerEvent();
				expect(tracker).toBeCalledWith('moe', [
					'[User] MamiPoin History Page Visited',
					{ interface: 'desktop', point_total: '1', redirection_source: 'test' }
				]);
			});
		});
	});
});
