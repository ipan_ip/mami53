import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mockAxios from 'tests-fe/utils/mock-axios';
import mixinMamipoin from 'Js/_user/mixins/mixinMamipoin.js';
import { createLocalVue, shallowMount } from '@vue/test-utils';
import PoinHistoryTabMobile from 'Js/_user/components/mamipoin/PoinHistoryTabMobile';
import MamipoinListData from 'Js/_user/components/mamipoin/list-poin/MamipoinListData';
const stubs = {
	MamipoinListData
};

const mocks = {
	swalError: jest.fn(),
	$store: {
		state: {
			totalPoin: { point: 1 }
		}
	},
	$router: {
		push: jest.fn()
	}
};

global.innerWidth = 375;
global.innerHeight = 812;
const mockGetElementByid = wrapper => {
	document.getElementById = p => {
		const el = wrapper.find(`#${p}`).element;
		el.scrollTop = -20;
		el.style.height = 0;
		return el;
	};
};

describe('PoinHistoryTabMobile.vue', () => {
	const localVue = createLocalVue();
	localVue.mixin([mixinMamipoin]);
	mockVLazy(localVue);

	window.addEventListener = jest.fn();
	global.axios = mockAxios;
	global.bugsnagClient = {
		notify: jest.fn()
	};

	const mountData = () => {
		return shallowMount(PoinHistoryTabMobile, {
			localVue,
			stubs,
			mocks
		});
	};

	let wrapper;
	beforeEach(() => {
		wrapper = mountData();
	});

	describe('render component poinHistoryTabMobile correctly', () => {
		it('should render PointHistoryTabMobile properly when isMobile true', () => {
			expect(wrapper.find('mobile-content')).toBeTruthy();
		});

		it('should render component MamipoinListData correctly', () => {
			expect(wrapper.findComponent(MamipoinListData)).toBeTruthy();
		});
	});

	describe('computed value in MamipoinHistoryTabMobile component', () => {
		it('should computed value isMobile true when screeen no more than 768', () => {
			expect(wrapper.vm.isMobile).toBeTruthy();
		});
	});

	describe('method MamipoinHistoryTabMobile component', () => {
		describe('infiniteScroll', () => {
			it('should execute "onScrollBottom" when user scrolled page', async () => {
				mockGetElementByid(wrapper);
				const onScrollBottomSpy = jest.spyOn(wrapper.vm, 'onScrollBottom');
				wrapper.vm.infiniteScroll();
				await wrapper.trigger('scroll');
				expect(onScrollBottomSpy).toBeCalled();
				onScrollBottomSpy.mockRestore();
			});
		});

		describe('closeHistoryPoin', () => {
			it('should close page when closeHistoryPoin was clicked', () => {
				wrapper.vm.closeHistoryPoin();
				expect(wrapper.vm.$router.push).toHaveBeenCalledWith({
					path: '/mamipoin'
				});
			});
		});

		describe('onScrollBottom', () => {
			const mockGetElementByidTrue = wrapper => {
				document.getElementById = p => {
					const el = wrapper.find(`#${p}`).element;
					el.scrollTop = 0;
					el.style.height = 0;
					return el;
				};
			};
			it('should execute "nextAllPoint" when isActive is "all", nextpage is true and loadAllPoin is false', async () => {
				mockGetElementByidTrue(wrapper);
				const nextAllPointSpy = jest.spyOn(wrapper.vm, 'nextAllPoint');
				wrapper.vm.isActive = 'all';
				wrapper.vm.allPoinHasNextPage = true;
				wrapper.vm.loadAllPoin = false;
				wrapper.vm.onScrollBottom();
				expect(nextAllPointSpy).toBeCalled();
				nextAllPointSpy.mockRestore();
			});

			it('should execute "nextEarnPoint" when isActive is "recieved", nextpage is true and loadEarnPoin is false', async () => {
				const nextEarnPointSpy = jest.spyOn(wrapper.vm, 'nextEarnPoint');
				wrapper.vm.isActive = 'recieved';
				wrapper.vm.earnPoinHasNextPage = true;
				wrapper.vm.loadEarnPoin = false;
				wrapper.vm.onScrollBottom();
				expect(nextEarnPointSpy).toBeCalled();
				nextEarnPointSpy.mockRestore();
			});

			it('should execute "nextRedeemPoint" when isActive is "exchanged", nextpage is true and loadRedeemPoin is false', async () => {
				const nextRedeemPointSpy = jest.spyOn(wrapper.vm, 'nextRedeemPoint');
				wrapper.vm.isActive = 'exchanged';
				wrapper.vm.redeemPoinHasNextPage = true;
				wrapper.vm.loadRedeemPoin = false;
				wrapper.vm.onScrollBottom();
				expect(nextRedeemPointSpy).toBeCalled();
				nextRedeemPointSpy.mockRestore();
			});

			it('should execute "nextExpiredPoint" when isActive is "expired", nextpage is true and loadExpiredPoin is false', async () => {
				const nextExpiredPointSpy = jest.spyOn(wrapper.vm, 'nextExpiredPoint');
				wrapper.vm.isActive = 'expired';
				wrapper.vm.expiredPoinHasNextPage = true;
				wrapper.vm.loadExpiredPoin = false;
				wrapper.vm.onScrollBottom();
				expect(nextExpiredPointSpy).toBeCalled();
				nextExpiredPointSpy.mockRestore();
			});

			it('should stoped function when scrolled didnt get to the bottom ', () => {
				wrapper.find('.container-mobile').trigger('scroll');
			});
		});

		describe('currentActive', () => {
			it('should execute getRecievedPoin when menu is "recieved" and loadEarnPoin is false ', () => {
				const getRecievedPoinSpy = jest.spyOn(wrapper.vm, 'getRecievedPoin');
				wrapper.vm.loadEarnPoin = false;
				wrapper.vm.earnPoin = [];
				wrapper.vm.curentActive('recieved');
				expect(getRecievedPoinSpy).toBeCalled();
				getRecievedPoinSpy.mockRestore();
			});

			it('should execute getRedeemPoin when menu is "exchanged" and loadRedeemPoin is false ', () => {
				const getRedeemPoinSpy = jest.spyOn(wrapper.vm, 'getRedeemPoin');
				wrapper.vm.loadRedeemPoin = false;
				wrapper.vm.redeemPoin = [];
				wrapper.vm.curentActive('exchanged');
				expect(getRedeemPoinSpy).toBeCalled();
				getRedeemPoinSpy.mockRestore();
			});

			it('should execute getExpiredPoin when menu is "expired" and loadExpiredPoin is false ', () => {
				const getExpiredPoinSpy = jest.spyOn(wrapper.vm, 'getExpiredPoin');
				wrapper.vm.loadExpiredPoin = false;
				wrapper.vm.expiredPoin = [];
				wrapper.vm.curentActive('expired');
				expect(getExpiredPoinSpy).toBeCalled();
				getExpiredPoinSpy.mockRestore();
			});
		});
	});
});
