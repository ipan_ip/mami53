import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import VeeValidate from 'vee-validate';

import UserSettingsPasswordChange from 'Js/_user/components/user-profile-settings/UserSettingsPasswordChange.vue';
import mockComponent from 'tests-fe/utils/mock-component';
import mockAxios from 'tests-fe/utils/mock-axios';

describe('UserSettingsPasswordChange.vue', () => {
	const localVue = createLocalVue();
	localVue.use(Vuex);
	localVue.use(VeeValidate, {
		locale: 'id'
	});

	// mock stubbed component
	const stubs = {
		UserTextField: mockComponent,
		MamiButton: mockComponent
	};

	// mock store
	const store = {
		state: {
			token: 'token'
		}
	};

	// mock methods
	const methods = {
		swalError: jest.fn()
	};

	// other mocks
	const mocks = {
		$emit: jest.fn()
	};

	// mock global properties
	global.tracker = jest.fn();
	global.bugsnagClient = {
		notify: jest.fn()
	};
	global.md5 = jest.fn(data => {
		return data;
	});
	global.navigator = {
		isMobile: true
	};
	global.axios = mockAxios;

	const mount = () => {
		return shallowMount(UserSettingsPasswordChange, {
			localVue,
			stubs,
			methods,
			mocks,
			store: new Vuex.Store(store)
		});
	};

	describe('render component correctly', () => {
		let wrapper;

		beforeEach(() => {
			wrapper = mount();
		});

		it('should render container', () => {
			expect(wrapper.find('.user-password-change').exists()).toBeTruthy();
		});
	});

	describe('render computed value correctly', () => {
		let wrapper;

		beforeEach(() => {
			wrapper = mount();
		});

		it('should render newPasswordRule properly', () => {
			expect(wrapper.vm.newPasswordRule).toEqual({
				required: true,
				min: 8,
				max: 25,
				regex: /^(?=.*[0-9])(?=.*[a-zA-Z|!@#$%^&*])([a-zA-Z0-9!@#$%^&*]+)$/,
				different_with_old: true
			});
		});

		it('should render token from $store', () => {
			expect(wrapper.vm.token).toBe('token');
		});

		it('should return inputIsValid properly', async () => {
			await wrapper.setData({
				oldPassword: 'oldPassword',
				newPassword: 'newPassword',
				confirmationPassword: 'confirmationPassword'
			});
			expect(wrapper.vm.inputIsValid).toBeFalsy();
		});

		it('should return userPasswordPayload properly', async () => {
			await wrapper.setData({
				oldPassword: 'oldPassword',
				newPassword: 'newPassword'
			});

			expect(wrapper.vm.userPasswordPayload).toEqual({
				_token: 'token',
				old_password: 'oldPassword',
				new_password: 'newPassword'
			});
		});
	});

	describe('execute functions correctly', () => {
		let wrapper;

		beforeEach(() => {
			wrapper = mount();
		});

		it('should call $validator.extend when extendValidationRules method is called', () => {
			const extendSpy = jest.spyOn(wrapper.vm.$validator, 'extend');
			wrapper.vm.extendValidationRules();
			expect(extendSpy).toBeCalled();
			extendSpy.mockRestore();
		});

		it('should call $validator when calling validateInput method', async () => {
			wrapper.vm.$validator.validate = jest.fn().mockResolvedValue(true);

			const validateSpy = jest.spyOn(wrapper.vm.$validator, 'validate');
			const validateAllSpy = jest.spyOn(wrapper.vm.$validator, 'validateAll');

			// condition below re-validates whether the confirmation password is the same with new password which should be true
			wrapper.setData({
				newPassword: 'new_password'
			});
			await wrapper.vm.validateInput('old_password', 'old_password');
			expect(validateSpy).toBeCalled();

			// condition below re-validates whether the new password isn't the same with old password which should be true
			await wrapper.vm.validateInput('new_password', 'new_password');
			expect(validateAllSpy).toBeCalled();

			validateSpy.mockRestore();
			validateAllSpy.mockRestore();
		});

		it('should give warning when the new password is the same with the old password ', () => {
			wrapper.setData({
				oldPassword: 'test',
				newPassword: 'test'
			});
			const extendSpy = jest.spyOn(wrapper.vm.$validator, 'extend');
			wrapper.vm.extendValidationRules();
			expect(extendSpy).toBeCalled();
			const calls = extendSpy.mock.calls || [];
			if (calls && calls.length) {
				const getMessage = calls[0][1].getMessage();
				const validate = calls[0][1].validate();
				expect(getMessage).toBe('Password tidak boleh sama');
				expect(validate).toBeFalsy();
			}
			extendSpy.mockRestore();
		});

		it('should call errors.add when addNewError method is called', () => {
			const addSpy = jest.spyOn(wrapper.vm.errors, 'add');
			wrapper.vm.addNewError({}, ['test', 'test2', 'test3']);
			expect(addSpy).toBeCalled();
			addSpy.mockRestore();
		});

		it('should call tracker and emitted finishSetting event when calling submitData method on successful response', async () => {
			wrapper.setData({
				oldPassword: 'oldPassword',
				newPassword: 'newPassword',
				confirmationPassword: 'confirmationPassword'
			});
			await axios.mockResolve({
				data: {
					status: true
				}
			});
			await wrapper.vm.submitData();
			await wrapper.vm.$nextTick();
			expect(wrapper.vm.saveLoading).toBeFalsy();
			expect(global.tracker).toBeCalled();
		});

		it('should call addNewError method when calling submitData method on successful response, but received falsy status', async () => {
			// old password failed
			const addNewErrorSpy = jest.spyOn(wrapper.vm, 'addNewError');
			wrapper.setData({
				oldPassword: 'oldPassword',
				newPassword: 'newPassword',
				confirmationPassword: 'confirmationPassword'
			});

			await axios.mockResolve({
				data: {
					status: false,
					meta: {
						messages: {
							old_password: 'test'
						}
					}
				}
			});
			await wrapper.vm.submitData();
			await wrapper.vm.$nextTick();
			expect(wrapper.vm.saveLoading).toBeFalsy();
			expect(addNewErrorSpy).toBeCalled();
			addNewErrorSpy.mockRestore();
		});

		it('should call addNewError method when calling submitData method on successful response, but received falsy status', async () => {
			// new password failed
			const addNewErrorSpy = jest.spyOn(wrapper.vm, 'addNewError');
			wrapper.setData({
				oldPassword: 'oldPassword',
				newPassword: 'newPassword',
				confirmationPassword: 'confirmationPassword'
			});

			await axios.mockResolve({
				data: {
					status: false,
					meta: {
						messages: {
							new_password: 'test'
						}
					}
				}
			});
			await wrapper.vm.submitData();
			await wrapper.vm.$nextTick();
			expect(wrapper.vm.saveLoading).toBeFalsy();

			expect(addNewErrorSpy).toBeCalled();
			addNewErrorSpy.mockRestore();
		});

		it('should call bugsnag when calling submitData method on failed response', async () => {
			const swalErrorSpy = jest.spyOn(wrapper.vm, 'swalError');
			wrapper.setData({
				oldPassword: 'oldPassword',
				newPassword: 'newPassword',
				confirmationPassword: 'confirmationPassword'
			});

			await axios.mockReject('error');
			await wrapper.vm.submitData();
			await wrapper.vm.$nextTick();

			expect(wrapper.vm.saveLoading).toBeFalsy();
			expect(global.bugsnagClient.notify).toBeCalled();
			expect(swalErrorSpy).toBeCalled();

			swalErrorSpy.mockRestore();
		});
	});
});
