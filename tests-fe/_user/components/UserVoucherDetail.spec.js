import Vuex from 'vuex';

import { createLocalVue, shallowMount } from '@vue/test-utils';

import userVoucherDetail from 'Js/_user/components/UserVoucherDetail.vue';
import store from './__mocks__/VoucherDetail.js';
import detail from './__mocks__/detail.json';
import mockVLazy from '../../utils/mock-v-lazy';
import mockAxios from 'tests-fe/utils/mock-axios';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

describe('UserVoucherDetail.vue', () => {
	const localVue = createLocalVue();

	localVue.use(Vuex);
	mockVLazy(localVue);

	const storeVoucher = new Vuex.Store(store);

	global.axios = mockAxios;

	const notify = jest.fn();
	mockWindowProperty('bugsnagClient', { notify });

	mockWindowProperty('scrollTo', jest.fn());

	const swalError = jest.fn();

	describe('Render voucher detail on loading', () => {
		const wrapper = shallowMount(userVoucherDetail, {
			localVue,
			storeVoucher,
			mocks: {
				$router: { push: jest.fn(), path: '/voucherku' },
				$store: { commit: jest.fn() }
			}
		});

		it('should render loading wrapper and match snapshot ', () => {
			expect(wrapper.find('.loading-wrapper').exists()).toBeTruthy();
		});
	});

	describe('Render voucher detail', () => {
		const wrapper = shallowMount(userVoucherDetail, {
			localVue,
			storeVoucher,
			mocks: {
				$router: { push: jest.fn(), path: '/voucherku' },
				$store: { commit: jest.fn() }
			},
			propsData: {
				voucherId: detail.data.id,
				voucherValidDate: 'Kapan Saja'
			},
			methods: {
				swalError
			}
		});

		wrapper.setData({ isLoading: false, detailVoucher: detail.data });

		it('should render user voucher detail and match snapshot', () => {
			expect(wrapper.find('.voucher-detail-title__text').text()).toBe(
				detail.data.title
			);
		});
	});

	describe('Render voucher detail in Mobile', () => {
		let wrapper;

		beforeEach(() => {
			wrapper = shallowMount(userVoucherDetail, {
				localVue,
				storeVoucher,
				mocks: {
					$router: { push: jest.fn(), path: '/voucherku' },
					$store: { commit: jest.fn() }
				},
				propsData: {
					voucherId: detail.data.id,
					voucherValidDate: 'Kapan Saja'
				},
				methods: {
					swalError
				}
			});

			wrapper.setData({ isLoading: false, detailVoucher: detail.data });

			mockWindowProperty('innerWidth', 700);
			mockWindowProperty('isMobile', true);
		});

		it('should return isMobile as true', () => {
			expect(wrapper.vm.isMobile).toBe(true);
		});

		it('should render container with class mobile-container detail', () => {
			expect(wrapper.find('.mobile-container').exists()).toBeTruthy();
		});
	});

	describe('Render with voucher id is null/zero', () => {
		axios.mockResolve({
			data: { data: detail }
		});
		const wrapper = shallowMount(userVoucherDetail, {
			localVue,
			storeVoucher,
			mocks: {
				$router: { push: jest.fn(), path: '/voucherku' },
				$store: { commit: jest.fn() }
			},
			propsData: {
				voucherValidDate: 'Kapan Saja'
			}
		});

		wrapper.setData({ isLoading: false, detailVoucher: detail.data });

		it('should redirected to /voucher if voucher id is zero', () => {
			wrapper.vm.voucherId = 0;

			const $router = {
				path: '/voucherku'
			};

			expect(wrapper.vm.$router.path).toBe($router.path);
		});
	});

	describe('Rejected Promise', () => {
		axios.mockReject('error');
		const wrapper = shallowMount(userVoucherDetail, {
			localVue,
			storeVoucher,
			mocks: {
				$router: { push: jest.fn(), path: '/voucherku' },
				$store: { commit: jest.fn() }
			},
			propsData: {
				voucherId: detail.data.id,
				voucherValidDate: 'Kapan Saja'
			},
			methods: {
				swalError
			}
		});

		wrapper.setData({ isLoading: false, detailVoucher: detail.data });

		it('should call bugsnagClient notify when fetch data', () => {
			wrapper.vm.getVoucherDetail();
			expect(swalError).toBeCalledWith(
				null,
				'Terjadi galat. Silakan coba lagi.'
			);
			expect(notify).toBeCalled();
		});
	});

	describe('Accept Promise', () => {
		axios.mockResolve({
			data: { data: detail }
		});
		const wrapper = shallowMount(userVoucherDetail, {
			localVue,
			storeVoucher,
			mocks: {
				$router: { push: jest.fn(), path: '/voucherku' },
				$store: { commit: jest.fn() }
			},
			propsData: {
				voucherId: detail.data.id,
				voucherValidDate: 'Kapan Saja'
			},
			methods: {
				swalError
			}
		});

		wrapper.setData({ isLoading: false, detailVoucher: detail.data });

		it('should get all fetch data', () => {
			wrapper.vm.getVoucherDetail();
			expect(wrapper.vm.detailVoucher.id).toBe(wrapper.vm.voucherId);
		});
	});

	describe('Accept promise with empty data', () => {
		axios.mockResolve({
			data: { data: { status: false, data: detail.data } }
		});
		const wrapper = shallowMount(userVoucherDetail, {
			localVue,
			storeVoucher,
			mocks: {
				$router: { push: jest.fn(), path: '/voucherku' },
				$store: { commit: jest.fn() }
			},
			propsData: {
				voucherValidDate: 'Kapan Saja'
			}
		});

		wrapper.setData({ isLoading: false, detailVoucher: detail.data });

		it('should show error alert', () => {
			wrapper.vm.getVoucherDetail();
			expect(swalError).toBeCalledWith(
				null,
				'Terjadi galat. Silakan coba lagi.'
			);
		});
	});
});
