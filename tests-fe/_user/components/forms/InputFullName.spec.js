import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex, { mapState } from 'vuex';

import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mockAxios from 'tests-fe/utils/mock-axios';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import debounce from 'tests-fe/utils/mock-debounce';
import VeeValidate, { Validator } from 'vee-validate';
import id from 'vee-validate/dist/locale/id';

import InputFullNameStore from '../__mocks__/InputFullNameStore.js';
import InputFullName from 'Js/_user/components/forms/InputFullName.vue';

describe('InputFullName.vue', () => {
	const localVue = createLocalVue();
	localVue.use(Vuex);
	mockVLazy(localVue);

	window.validator = new Validator();
	Validator.localize('id', id);

	localVue.directive('validate', jest.fn());
	localVue.use(VeeValidate, {
		locale: 'id'
	});

	global.axios = mockAxios;
	global.debounce = debounce;
	global.mapState = mapState;

	const notify = jest.fn();
	mockWindowProperty('bugsnagClient', { notify });

	describe('render component correctly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = shallowMount(InputFullName, {
				localVue,
				store: new Vuex.Store(InputFullNameStore)
			});
		});

		it('should render input container properly', () => {
			expect(
				wrapper.find('.input-control.input-control-custom').exists()
			).toBeTruthy();
		});

		it('should render input properly', () => {
			const inputWrapper = wrapper.find('input');
			if (inputWrapper.exists()) {
				expect(inputWrapper.attributes('name')).toBe('Nama Lengkap');
			}
		});

		it('should add error-field class to input when errorInput is true', async () => {
			await wrapper.setData({ errorInput: true });
			expect(wrapper.find('input').classes()).toContain('error-field');
		});
	});

	describe('call functions correctly', () => {
		let wrapper;

		beforeEach(() => {
			wrapper = shallowMount(InputFullName, {
				localVue,
				store: new Vuex.Store(InputFullNameStore)
			});
		});

		it('should call updateName function on input change', async () => {
			const inputWrapper = wrapper.find('input');
			await wrapper.setMethods({ updateName: jest.fn() });
			await inputWrapper.trigger('input');
			expect(wrapper.vm.updateName).toBeCalled();
		});

		it('should update vuex name on input change', async () => {
			const inputWrapper = wrapper.find('input');
			await inputWrapper.setValue('test');
			await inputWrapper.trigger('input');

			expect(InputFullNameStore.state.userEditProfile.name).toBe('test');
		});

		it('should set errorInput and vuex formState as false when vee-validate validator returns true', async () => {
			const inputWrapper = wrapper.find('input');
			await inputWrapper.setValue('test');
			await inputWrapper.trigger('input');
			await new Promise(resolve => setTimeout(resolve, 0));

			expect(InputFullNameStore.state.formState).toBe(false);
			expect(wrapper.vm.errorInput).toBe(false);
		});

		it('should set errorInput and vuex formState as true when vee-validate validator returns false', async () => {
			const inputWrapper = wrapper.find('input');
			await inputWrapper.setValue('');
			await inputWrapper.trigger('input');
			await new Promise(resolve => setTimeout(resolve, 0));

			expect(InputFullNameStore.state.formState).toBe(true);
			expect(wrapper.vm.errorInput).toBe(true);
		});
	});
});
