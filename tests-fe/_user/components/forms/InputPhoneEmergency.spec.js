import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';

import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mockAxios from 'tests-fe/utils/mock-axios';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import debounce from 'tests-fe/utils/mock-debounce';
import VeeValidate, { Validator } from 'vee-validate';
import id from 'vee-validate/dist/locale/id';

import InputPhoneEmergencyStore from '../__mocks__/InputPhoneEmergencyStore.js';
import InputPhoneEmergency from 'Js/_user/components/forms/InputPhoneEmergency.vue';

describe('InputPhoneEmergency.vue', () => {
	const localVue = createLocalVue();
	localVue.use(Vuex);
	mockVLazy(localVue);

	window.validator = new Validator();
	Validator.localize('id', id);

	localVue.directive('validate', jest.fn());
	localVue.use(VeeValidate, {
		locale: 'id'
	});

	global.axios = mockAxios;
	global.debounce = debounce;

	const notify = jest.fn();
	mockWindowProperty('bugsnagClient', { notify });

	describe('render component correctly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = shallowMount(InputPhoneEmergency, {
				localVue,
				store: new Vuex.Store(InputPhoneEmergencyStore)
			});
		});

		it('should render container properly', () => {
			expect(
				wrapper.find('.input-control.input-control-custom').exists()
			).toBeTruthy();
		});

		it('should render label correctly', () => {
			expect(wrapper.find('.title-input').exists()).toBeTruthy();
			expect(wrapper.find('.title-input').text()).toBe('No. Handphone Darurat');
		});

		it('should render input correctly', () => {
			const inputWrapper = wrapper.find('input');
			expect(inputWrapper.exists()).toBeTruthy();
			expect(inputWrapper.attributes('name')).toBe('No. Handphone Darurat');
		});
	});

	describe('call functions correctly', () => {
		let wrapper;

		beforeEach(() => {
			wrapper = shallowMount(InputPhoneEmergency, {
				localVue,
				store: new Vuex.Store(InputPhoneEmergencyStore)
			});
		});

		it('should set the phone number state value on valuePhone change', () => {
			wrapper.setMethods({ validatorPhone: jest.fn() });
			wrapper.setData({ valuePhone: 1234567890 });

			expect(
				InputPhoneEmergencyStore.state.userEditProfile.phone_number_additional
			).toBe(1234567890);
		});

		it('should set errorInput as true and emit errors when valuePhone length is less than 8 characters', async () => {
			await wrapper.setData({ valuePhone: 0 });
			await new Promise(resolve => setTimeout(resolve, 0));

			await wrapper.vm.errorInput;

			expect(wrapper.vm.errorInput).toBeTruthy();
			expect(wrapper.emitted()['is-error']).toBeTruthy();
		});

		it('should set errorInput as true and emit errors when valuePhone length is more than 14 characters', async () => {
			await wrapper.setData({ valuePhone: 123456789012345 });
			await new Promise(resolve => setTimeout(resolve, 0));

			expect(wrapper.vm.errorInput).toBeTruthy();
			expect(wrapper.emitted()['is-error']).toBeTruthy();
		});

		it('should set errorInput as true and emit errors when valuePhone is not numeric', async () => {
			await wrapper.setData({ valuePhone: 'asdfghjkl' });
			await new Promise(resolve => setTimeout(resolve, 0));

			expect(wrapper.vm.errorInput).toBeTruthy();
			expect(wrapper.emitted()['is-error']).toBeTruthy();
		});
	});
});
