import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import moment from 'moment';

import mockAxios from 'tests-fe/utils/mock-axios';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import mockComponent, {
	mockComponentWithAttrs
} from 'tests-fe/utils/mock-component';

import InputProfession from 'Js/_user/components/forms/InputProfession.vue';

// mock imports
jest.mock('Js/_user/event-bus/event-bus', () => {
	const EventBus = {
		$emit: jest.fn(),
		$on: jest.fn((event, fn) => {
			fn({
				isOpenModal: true
			});
		})
	};
	global.EventBus = EventBus;

	return global.EventBus;
});
jest.mock('Js/_user/mixins/mixinUserKost', () => {
	return {
		methods: {
			getStatusRentTenant: jest.fn()
		}
	};
});

// set vue
window.Vue = require('vue');
window.Vue.config.silent = true;

describe('InputProfession.vue', () => {
	const localVue = createLocalVue();
	localVue.use(Vuex);

	global.axios = mockAxios;

	// mock window
	const notify = jest.fn();
	mockWindowProperty('bugsnagClient', { notify });

	// mock stubbed component
	const stubs = {
		radio: mockComponent,
		'v-select': mockComponent
	};

	const InputProfessionStore = {
		state: {
			userEditProfile: {
				description: 'desc',
				work: 'work',
				work_place: 'work_place',
				semester: 'semester',
				jobs: 'kerja'
			}
		},
		mutations: {
			setJobs(state, jobs) {
				state.userEditProfile.jobs = jobs;
			},
			updateSemester(state, semester) {
				state.userEditProfile.semester = semester;
			},
			updateDescription(state, description) {
				state.userEditProfile.description = description;
			},
			updatePosition(state, work) {
				state.userEditProfile.work = work;
			},
			updateWorkPlace(state, workPlace) {
				state.userEditProfile.work_place = workPlace;
			}
		}
	};

	// spies
	const setEmptySpy = jest.spyOn(InputProfession.methods, 'setEmpty');

	// mount function
	const mount = options => {
		return shallowMount(InputProfession, {
			...{
				localVue,
				stubs,
				mocks: {
					$toasted: {
						show: jest.fn()
					}
				},
				methods: {
					$dayjs: moment,
					swalError: jest.fn(),
					openSwalLoading: jest.fn(),
					closeSwalLoading: jest.fn()
				},
				store: new Vuex.Store(InputProfessionStore)
			},
			...options
		});
	};

	describe('render component correctly', () => {
		let wrapper;

		beforeEach(() => {
			wrapper = mount();
		});

		it('should render container properly', () => {
			expect(wrapper.find('#inputProfessionSection').exists()).toBeTruthy();
		});
	});

	describe('render computed value properly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = mount();
		});

		it('should return semester value from $store', async () => {
			// get block
			expect(wrapper.vm.semester).toBe('semester');

			// set block
			wrapper.vm.semester = 'test';
			await wrapper.vm.$nextTick();
			expect(wrapper.vm.semester).toBe('test');

			// reset
			wrapper.vm.semester = 'semester';
		});

		it('should return university value from $store', async () => {
			// get block
			expect(wrapper.vm.university).toBe('work_place');

			// set block
			wrapper.vm.university = 'test';
			await wrapper.vm.$nextTick();
			expect(wrapper.vm.university).toBe('test');

			// reset
			wrapper.vm.university = 'work_place';
		});

		it('should return major value from $store', async () => {
			// get block
			expect(wrapper.vm.major).toBe('work');

			// set block
			wrapper.vm.major = 'test';
			await wrapper.vm.$nextTick();
			expect(wrapper.vm.major).toBe('test');

			// reset
			wrapper.vm.major = 'work';
		});

		it('should return description value from $store', () => {
			expect(wrapper.vm.description).toBe('desc');
		});

		it('should return position value from $store', async () => {
			// get block
			expect(wrapper.vm.position).toBe('work');

			// set block
			wrapper.vm.position = 'test';
			await wrapper.vm.$nextTick();
			expect(wrapper.vm.position).toBe('test');

			// reset
			wrapper.vm.position = 'work';
		});

		it('should return work_place value from $store', () => {
			expect(wrapper.vm.work_place).toBe('work_place');
		});

		it('should return profession value from $store', async () => {
			// get block
			expect(wrapper.vm.profession).toBe('kerja');

			// set block
			wrapper.vm.profession = 'test';
			await wrapper.vm.$nextTick();
			expect(wrapper.vm.profession).toBe('test');

			// reset
			wrapper.vm.profession = 'kerja';
		});

		it('should set jobs as kuliah when received null jobs value', () => {
			const mockStore = InputProfessionStore;
			mockStore.state.userEditProfile.jobs = null;

			wrapper = mount({ store: new Vuex.Store(mockStore) });
			expect(wrapper.vm.profession).toBe('kuliah');
		});

		it('should return kuliahKerjaValue value from $store', async () => {
			// get block
			wrapper.setData({ isOtherDropdownOptionSelected: true });
			expect(wrapper.vm.kuliahKerjaValue).toBe('Lainnya');

			wrapper.setData({ isOtherDropdownOptionSelected: false });
			expect(wrapper.vm.kuliahKerjaValue).toBe('work_place');

			// set block
			wrapper.vm.kuliahKerjaValue = 'Lainnya';
			await wrapper.vm.$nextTick();
			expect(wrapper.vm.work_place).toBe(null);

			wrapper.vm.kuliahKerjaValue = 'test';
			await wrapper.vm.$nextTick();
			expect(wrapper.vm.work_place).toBe('test');
			expect(wrapper.vm.isOtherDropdownOptionSelected).toBeFalsy();

			// reset
			wrapper.vm.kuliahKerjaValue = 'work_place';
		});
	});

	describe('call functions in watch block correctly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = mount();
		});

		it('should call setEmpty function on description value change', async () => {
			await wrapper.setData({
				description: null
			});
			expect(wrapper.vm.inputError.description).toBeTruthy();
			await wrapper.vm.$nextTick();
			await wrapper.setData({
				description: 'test'
			});
			expect(wrapper.vm.inputError.description).toBeFalsy();
			expect(setEmptySpy).toBeCalled();
		});

		it('should call setEmpty function on university value change', async () => {
			await wrapper.setData({
				university: null
			});
			expect(wrapper.vm.inputError.university).toBeTruthy();
			await wrapper.vm.$nextTick();
			await wrapper.setData({
				university: 'test'
			});
			expect(wrapper.vm.inputError.university).toBeFalsy();
			expect(setEmptySpy).toBeCalled();
		});

		it('should call setEmpty function on work_place value change', async () => {
			await wrapper.setData({
				work_place: null
			});
			expect(wrapper.vm.inputError.work_place).toBeTruthy();
			await wrapper.vm.$nextTick();
			await wrapper.setData({
				work_place: 'test'
			});
			expect(wrapper.vm.inputError.work_place).toBeFalsy();
			expect(setEmptySpy).toBeCalled();
		});
	});

	describe('call functions correctly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = mount();
		});

		it('should reset description and work_place on setEmpty change', async () => {
			await wrapper.vm.setEmpty();

			expect(wrapper.vm.description).toBe('');
			expect(wrapper.vm.work_place).toBe(null);
		});
	});
});
