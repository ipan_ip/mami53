import Vuex from 'vuex';
import { createLocalVue, shallowMount, mount } from '@vue/test-utils';

import InputGender from 'Js/_user/components/forms/InputGender.vue';
import { radio } from 'vue-strap';

import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

describe('InputGender.vue', () => {
	const localVue = createLocalVue();
	localVue.use(Vuex);
	mockVLazy(localVue);

	const notify = jest.fn();
	mockWindowProperty('bugsnagClient', { notify });

	const tracker = jest.fn();
	global.tracker = tracker;

	const $store = {
		state: {
			userEditProfile: {
				gender: 'male'
			}
		},
		commit: jest.fn()
	};

	describe('should render correctly', () => {
		let wrapper = setInputGenderWrapper(localVue, { $store });

		it('should render input gender container correctly', () => {
			expect(wrapper.find('.input-control-custom').exists()).toBeTruthy();
		});

		it('should render radio-wrapper correctly', () => {
			const radioWrappers = wrapper.findAll('.radio-wrapper');
			expect(radioWrappers.length).toBe(2);
		});

		it('should render radio component for "male" correctly', () => {
			const radioWrappers = wrapper.findAll('.radio-wrapper');
			const maleRadioWrapper = radioWrappers.at(0);
			const maleRadioInput = maleRadioWrapper.find("input[type='radio']");

			expect(maleRadioWrapper.find('.radio').classes()).toContain('active');
			if (maleRadioInput.exists()) {
				expect(maleRadioInput.attributes('value')).toBe('male');
			}
		});

		it('should render radio component for "female" correctly', () => {
			const radioWrappers = wrapper.findAll('.radio-wrapper');
			const femaleRadioWrapper = radioWrappers.at(1);
			const femaleRadioInput = femaleRadioWrapper.find("input[type='radio']");
			if (femaleRadioInput.exists()) {
				expect(femaleRadioInput.attributes('value')).toBe('female');
			}
		});

		it('should render alert for unlisted genders correctly', () => {
			const mockStore = {
				...$store,
				...{
					state: {
						userEditProfile: {
							gender: 'unknown'
						}
					}
				}
			};

			wrapper = setInputGenderWrapper(localVue, { $store: mockStore });

			const alert = wrapper.find(
				'.input-control.input-control-custom.no-margin .failed-alert'
			);
			expect(alert.exists()).toBeTruthy();
		});
	});

	describe('should call the functions correctly', () => {
		const wrapper = setInputGenderWrapper(localVue, { $store });

		it('should call the store commit function when received "Perempuan" correctly', () => {
			wrapper.vm.$store.state.userEditProfile.gender = 'Perempuan';
			wrapper.vm.$nextTick(() => {
				expect($store.commit).toBeCalled();
				const calls = $store.commit.mock.calls || [[]];
				expect(calls[0][1]).toBe('female');
			});
		});

		it('should call the store commit function when received "Laki-Laki" correctly', () => {
			wrapper.vm.$store.state.userEditProfile.gender = 'Laki-Laki';
			wrapper.vm.$nextTick(() => {
				expect($store.commit).toBeCalled();
				const calls = $store.commit.mock.calls || [[]];
				expect(calls[calls.length - 1][1]).toBe('male');
			});
		});
	});
});

function setInputGenderWrapper(localVue = {}, mocks = {}) {
	const options = {
		localVue,
		mocks,
		stubs: {
			radio
		}
	};
	const wrapper = shallowMount(InputGender, options);
	return wrapper;
}
