import Vuex from 'vuex';

import { createLocalVue, shallowMount, mount } from '@vue/test-utils';

import UserTextField from 'Js/_user/components/forms/UserTextField.vue';
import mockAxios from 'tests-fe/utils/mock-axios';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

describe('UserTextField.vue', () => {
	const localVue = createLocalVue();
	localVue.use(Vuex);
	mockVLazy(localVue);

	global.axios = mockAxios;

	const notify = jest.fn();
	mockWindowProperty('bugsnagClient', { notify });

	const tracker = jest.fn();
	global.tracker = tracker;

	describe('should render correctly', () => {
		let wrapper;
		const propsData = {
			inputLabel: 'input label',
			inputName: 'inputName',
			inputType: 'email',
			inputPlaceholder: 'input placeholder',
			inputValue: 'value'
		};
		beforeEach(() => {
			wrapper = shallowMount(UserTextField, {
				localVue,
				propsData
			});
		});

		it('should render user text field container correctly', () => {
			expect(wrapper.find('.user-settings-input-text').exists()).toBeTruthy();
		});

		it('should render label correctly', () => {
			const textFieldLabel = wrapper.find('.user-settings-input-text__label');
			expect(textFieldLabel.exists()).toBeTruthy();
			expect(textFieldLabel.text()).toBe('input label');
			expect(textFieldLabel.attributes('for')).toBe('inputName');
		});

		it('should render input correctly', () => {
			const textFieldInput = wrapper.find(
				'.user-settings-input-text__default-input'
			);
			expect(textFieldInput.exists()).toBeTruthy();
			expect(textFieldInput.attributes('name')).toBe('inputName');
			expect(textFieldInput.attributes('placeholder')).toBe(
				'input placeholder'
			);
			expect(textFieldInput.attributes('type')).toBe('email');
		});

		it('should render input type="password" correctly', () => {
			wrapper = shallowMount(UserTextField, {
				localVue,
				propsData: {
					inputType: 'password'
				}
			});
			const textFieldInput = wrapper.find(
				'.user-settings-input-text__default-input'
			);
			const togglePasswordIcon = wrapper.find('.show-password-icon');
			expect(togglePasswordIcon.exists()).toBeTruthy();
			togglePasswordIcon.trigger('click');
			expect(textFieldInput.attributes('type')).toBe('password');
			togglePasswordIcon.trigger('click');
			expect(textFieldInput.attributes('type')).toBe('password');
		});

		it('should render the error message correctly', () => {
			wrapper = shallowMount(UserTextField, {
				localVue,
				propsData: {
					errorMessage: 'error'
				}
			});

			const errorMessageText = wrapper.find(
				'.user-settings-input-text__error-message'
			);
			expect(errorMessageText.exists()).toBeTruthy();
			expect(errorMessageText.text()).toBe('error');
		});
	});

	describe('should call functions correctly', () => {
		let wrapper;
		const propsData = {
			inputType: 'password'
		};
		beforeEach(() => {
			wrapper = shallowMount(UserTextField, {
				localVue,
				propsData
			});
		});

		it('should call the handleShowPassword function on toggle icon click', () => {
			wrapper.setMethods({ handleShowPassword: jest.fn() });
			const togglePasswordIcon = wrapper.find('.show-password-icon');
			if (togglePasswordIcon.exists()) {
				togglePasswordIcon.trigger('click');
				expect(wrapper.vm.handleShowPassword).toBeCalled();
			}
		});

		it('should emit the input event on input change', () => {
			const textFieldInput = wrapper.find(
				'.user-settings-input-text__default-input'
			);
			textFieldInput.setValue('test');
			expect(wrapper.emitted('input')).toBeTruthy();
			expect(wrapper.emitted('input')[0][0]).toBe('test');
		});
	});
});
