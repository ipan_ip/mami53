import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';

import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mockAxios from 'tests-fe/utils/mock-axios';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import debounce from 'tests-fe/utils/mock-debounce';
import VeeValidate, { Validator } from 'vee-validate';
import id from 'vee-validate/dist/locale/id';

import InputPhotosStore from '../__mocks__/InputPhotosStore.js';
import InputPhotos from 'Js/_user/components/forms/InputPhotos.vue';
import vue2Dropzone from 'vue2-dropzone';

describe('InputPhotos.vue', () => {
	const localVue = createLocalVue();
	localVue.use(Vuex);
	mockVLazy(localVue);

	window.validator = new Validator();
	Validator.localize('id', id);

	localVue.directive('validate', jest.fn());
	localVue.use(VeeValidate, {
		locale: 'id'
	});

	global.axios = mockAxios;
	global.debounce = debounce;

	const notify = jest.fn();
	mockWindowProperty('bugsnagClient', { notify });

	describe('render component correctly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = shallowMount(InputPhotos, {
				localVue,
				store: new Vuex.Store(InputPhotosStore),
				propsData: {
					photos: null
				},
				stubs: {
					Dropzone: vue2Dropzone
				}
			});
		});

		it('should render dropzone container properly', () => {
			expect(wrapper.find('.dropzone-custom').exists()).toBeTruthy();
			expect(wrapper.find('.vue-dropzone').exists()).toBeTruthy();
		});

		it('should show "Ganti Photo" button and previously uploaded image when isPhotoExists is true', () => {
			wrapper = shallowMount(InputPhotos, {
				localVue,
				store: new Vuex.Store(InputPhotosStore),
				propsData: {
					photos: {
						medium: 'path/to/image.jpg'
					}
				},
				stubs: {
					Dropzone: vue2Dropzone
				}
			});

			const { isPhotoExists } = wrapper.vm;
			expect(isPhotoExists).toBeTruthy();

			const dropzoneUploadedWrapper = wrapper.find('.dropzone-uploaded');
			if (dropzoneUploadedWrapper.exists()) {
				expect(dropzoneUploadedWrapper.element.style.display).not.toBe('none');
			}

			const resetButtonContainer = wrapper.find('.btn-reset-container');
			if (resetButtonContainer.exists()) {
				expect(resetButtonContainer.element.style.display).not.toBe('none');
			}
		});

		it('should render failed alert when isError is true', async () => {
			await wrapper.setData({ isError: true });
			expect(wrapper.find('.failed-alert').exists()).toBeTruthy();
		});
	});

	describe('call functions correctly', () => {
		let wrapper;

		beforeEach(() => {
			wrapper = shallowMount(InputPhotos, {
				localVue,
				store: new Vuex.Store(InputPhotosStore),
				propsData: {
					photos: null
				},
				stubs: {
					Dropzone: vue2Dropzone
				}
			});
		});

		jest.useFakeTimers();

		it('should call resetPhotos function on "Ganti Photo" button click', async () => {
			wrapper.setMethods({ resetPhotos: jest.fn() });
			const resetButton = wrapper.find('.btn-mamigreen-inverse');
			if (resetButton.exists()) {
				await resetButton.trigger('click');
				expect(wrapper.vm.resetPhotos).toBeCalled();
			}
		});

		it('should return vuex data as photoMedium value', async () => {
			wrapper.vm.$store.state.userPhoto.medium = 'path/to/image.jpg';

			const { photoMedium } = wrapper.vm;
			expect(photoMedium).toBe('path/to/image.jpg');
		});

		it('should toggle "isPhotoExists" value on "Ganti Photo" button click', async () => {
			expect(wrapper.vm.isPhotoExists).toBeFalsy();
			const resetButton = wrapper.find('.btn-mamigreen-inverse');

			if (resetButton.exists()) {
				await resetButton.trigger('click');
				expect(wrapper.vm.isPhotoExists).toBeTruthy();
			}
		});

		it('should toggle "isPhotoExists" value on "Ganti Photo" button click', async () => {
			expect(wrapper.vm.isPhotoExists).toBeFalsy();
			const resetButton = wrapper.find('.btn-mamigreen-inverse');

			if (resetButton.exists()) {
				await resetButton.trigger('click');
				expect(wrapper.vm.isPhotoExists).toBeTruthy();
			}
		});

		it('should execute showSuccess function on "vdropzone-success" emit', async () => {
			await wrapper.setMethods({
				showSuccess: jest.fn().mockResolvedValue({ accepted: true })
			});
			const $dropzone = wrapper.vm.$refs['photo-profile'];
			if ($dropzone) {
				await $dropzone.$emit('vdropzone-success', { media: '' });
				expect(wrapper.vm.showSuccess).toBeCalled();
			}
		});

		it('should call $store.commit function if file was successfully uploaded', () => {
			InputPhotosStore.mutations.setValue = jest.fn();
			wrapper = shallowMount(InputPhotos, {
				localVue,
				store: new Vuex.Store(InputPhotosStore),
				stubs: {
					Dropzone: vue2Dropzone
				}
			});
		});

		it('should save media_id to vuex if file was succesfully uploaded and accepted', async () => {
			const $dropzone = wrapper.vm.$refs['photo-profile'];
			if ($dropzone) {
				await $dropzone.$emit(
					'vdropzone-success',
					{
						accepted: true
					},
					{
						media: {
							id: 1234567890
						}
					}
				);

				expect(InputPhotosStore.mutations.setValue).toBeCalled();
			}
		});

		it('should set data when calling showAdded method', async () => {
			const emitSpy = jest.spyOn(wrapper.vm, '$emit');
			// file is accepted
			await wrapper.vm.showAdded({ accepted: true, size: 1 });
			jest.advanceTimersByTime(6000);
			expect(wrapper.vm.isError).toBeFalsy();
			expect(emitSpy).toBeCalled();

			// file not accepted
			await wrapper.vm.showAdded({ accepted: false, size: 1 });
			jest.advanceTimersByTime(6000);
			expect(wrapper.vm.isError).toBeTruthy();
			expect(emitSpy).toBeCalled();

			emitSpy.mockRestore();
		});

		it('should execute showAdded function on "vdropzone-file-added" emit', async () => {
			await wrapper.setMethods({
				showAdded: jest.fn()
			});
			const $dropzone = wrapper.vm.$refs['photo-profile'];
			if ($dropzone) {
				$dropzone.$emit('vdropzone-file-added', { accepted: true });

				setTimeout(() => {
					expect(wrapper.vm.showAdded).toBeCalled();
				}, 1000);
			}
		});

		it('should set isError value and emit "errorImage" as false on "vdropzone-file-added" emit', async () => {
			const $dropzone = wrapper.vm.$refs['photo-profile'];
			if ($dropzone) {
				$dropzone.$emit('vdropzone-file-added', { accepted: true });

				setTimeout(() => {
					const { isError } = wrapper.vm;
					expect(isError).toBeFalsy();
					expect(wrapper.emitted().errorImage).toBeFalsy();
				}, 1000);
			}
		});

		it('should set isError value and emit "errorImage" as true on "vdropzone-file-added" emit', async () => {
			const $dropzone = wrapper.vm.$refs['photo-profile'];
			if ($dropzone) {
				$dropzone.$emit('vdropzone-file-added', { accepted: false });

				setTimeout(() => {
					const { isError } = wrapper.vm;
					expect(isError).toBeTruthy();
					expect(wrapper.emitted().errorImage).toBeTruthy();
				}, 1000);
			}
		});

		it('should emit updated event on photo_id value change', async () => {
			await wrapper.setData({ photo_id: '1234567890' });

			expect(wrapper.emitted('updated')).toBeTruthy();
			expect(wrapper.emitted().updated[0]).toEqual(['1234567890']);
		});
	});
});
