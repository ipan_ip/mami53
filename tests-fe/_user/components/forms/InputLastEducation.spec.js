import Vuex from 'vuex';

import { createLocalVue, shallowMount, mount } from '@vue/test-utils';

import InputLastEducation from 'Js/_user/components/forms/InputLastEducation.vue';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

describe('InputLastEducation.vue', () => {
	const localVue = createLocalVue();
	localVue.use(Vuex);
	mockVLazy(localVue);

	const notify = jest.fn();
	mockWindowProperty('bugsnagClient', { notify });

	const tracker = jest.fn();
	global.tracker = tracker;

	describe('should render correctly', () => {
		let wrapper;

		const $store = {
			state: {
				userEditProfile: {
					last_education: ''
				}
			},
			commit: jest.fn(() => {})
		};

		beforeEach(() => {
			wrapper = shallowMount(InputLastEducation, {
				localVue,
				mocks: {
					$store
				}
			});
		});

		it('should render last education container correctly', () => {
			expect(
				wrapper.find('.input-control.input-control-custom').exists()
			).toBeTruthy();
		});

		it('should render selection correctly', () => {
			const selectWrapper = wrapper.find('.form-control');
			expect(selectWrapper.exists()).toBeTruthy();
		});

		it('should render selection options correctly', () => {
			const { dataEducation } = wrapper.vm;
			const selectWrapper = wrapper.find('.form-control');
			const selectOptions = selectWrapper.findAll('option');

			expect(selectOptions.length).toBe(dataEducation.length + 1);
		});
	});

	describe('should call functions correctly', () => {
		const $store = {
			state: {
				userEditProfile: {
					last_education: ''
				}
			},
			commit: jest.fn()
		};

		let wrapper;
		beforeEach(() => {
			wrapper = shallowMount(InputLastEducation, {
				localVue,
				mocks: {
					$store
				}
			});
		});

		it('should call the $store.commit function and received "S1" as param when received falsy data from vuex', () => {
			expect($store.commit).toBeCalled();

			const calls = $store.commit.mock.calls || [[]];
			expect(calls[0][1]).toBe('S1');
		});

		it('should call the $store.commit function and received "Diploma" as param when received "D3" from vuex', () => {
			wrapper.vm.$store.state.userEditProfile.last_education = 'D3';
			wrapper.vm.$nextTick(() => {
				expect($store.commit).toBeCalled();

				const calls = $store.commit.mock.calls || [[]];
				expect(calls[calls.length - 2][1]).toBe('Diploma');
			});
		});

		it('should call $store.commit function and received previously sent data as param when valueLastEducation is set', () => {
			wrapper.setData({ valueLastEducation: 'S1' });
			wrapper.vm.$nextTick(() => {
				expect($store.commit).toBeCalled();

				const calls = $store.commit.mock.calls || [[]];
				expect(calls[calls.length - 1][1]).toBe('S1');
			});
		});
	});
});
