import Vuex from 'vuex';

import { createLocalVue, shallowMount, mount } from '@vue/test-utils';

import InputStatus from 'Js/_user/components/forms/InputStatus.vue';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

describe('InputStatus.vue', () => {
	const localVue = createLocalVue();
	localVue.use(Vuex);
	mockVLazy(localVue);

	const notify = jest.fn();
	mockWindowProperty('bugsnagClient', { notify });

	const tracker = jest.fn();
	global.tracker = tracker;

	describe('should render correctly', () => {
		let wrapper;

		const $store = {
			state: {
				userEditProfile: {
					marital_status: ''
				}
			},
			commit: jest.fn(() => {})
		};

		beforeEach(() => {
			wrapper = shallowMount(InputStatus, {
				localVue,
				mocks: {
					$store
				}
			});
		});

		it('should render status container correctly', () => {
			expect(
				wrapper.find('.input-control.input-control-custom').exists()
			).toBeTruthy();
		});

		it('should render selection correctly', () => {
			const selectWrapper = wrapper.find('.form-control');
			expect(selectWrapper.exists()).toBeTruthy();
		});

		it('should render selection options correctly', () => {
			const selectOptions = wrapper.findAll('.form-control option');
			expect(selectOptions.length).toBe(3);
		});

		it('should render failed alert when errorInput is true correctly', async () => {
			await wrapper.setData({ errorInput: true });

			const failedAlert = wrapper.findAll('.failed-alert');
			expect(failedAlert.exists).toBeTruthy();
		});
	});

	describe('should call functions correctly', () => {
		const $store = {
			state: {
				userEditProfile: {
					marital_status: ''
				}
			},
			commit: jest.fn()
		};

		let wrapper;
		beforeEach(() => {
			wrapper = shallowMount(InputStatus, {
				localVue,
				mocks: {
					$store
				}
			});
		});

		it('should call the $store.commit function and received "Belum Kawin" as param when received falsy data from vuex', () => {
			expect($store.commit).toBeCalled();

			const calls = $store.commit.mock.calls || [[]];
			expect(calls[0][0]).toBe('updateMartial');
			expect(calls[0][1]).toBe('Belum Kawin');
		});

		it('should call $store.commit function and received previously sent data as param when valueStatus is set', async () => {
			await wrapper.setData({ valueStatus: 'Kawin' });
			expect($store.commit).toBeCalled();

			const calls = $store.commit.mock.calls || [[]];
			expect(calls[0][0]).toBe('updateMartial');
			expect(calls[calls.length - 1][1]).toBe('Kawin');
		});
	});
});
