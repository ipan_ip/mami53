import Vuex from 'vuex';
import moment from 'moment';

import flatPickr from 'vue-flatpickr-component';

import { createLocalVue, shallowMount, mount } from '@vue/test-utils';

import InputDateBirth from 'Js/_user/components/forms/InputDateBirth.vue';
import mockAxios from 'tests-fe/utils/mock-axios';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

describe('InputDateBirth.vue', () => {
	const localVue = createLocalVue();
	localVue.use(Vuex);
	mockVLazy(localVue);

	global.axios = mockAxios;
	global.moment = moment;

	const notify = jest.fn();
	mockWindowProperty('bugsnagClient', { notify });

	const tracker = jest.fn();
	global.tracker = tracker;

	describe('should render correctly', () => {
		let wrapper;
		const stubs = {
			flatPickr
		};

		const $store = {
			state: {
				userEditProfile: {
					birthday: ''
				}
			},
			commit: jest.fn(() => {
				console.log('the $store.commit function is called');
			})
		};

		beforeEach(() => {
			wrapper = shallowMount(InputDateBirth, {
				localVue,
				stubs,
				mocks: {
					$store
				}
			});
		});

		it('should render input date birth container correctly', () => {
			expect(
				wrapper.find('.input-control.input-control-custom').exists()
			).toBeTruthy();
		});

		it('should render flatPickr correctly', () => {
			const flatPickr = wrapper.find('.flatpickr-input');
			expect(flatPickr.exists()).toBeTruthy();
		});
	});

	describe('should call functions correctly', () => {
		const stubs = {
			flatPickr
		};

		const $store = {
			state: {
				userEditProfile: {
					birthday: ''
				}
			},
			commit: jest.fn()
		};

		let wrapper;
		beforeEach(() => {
			wrapper = shallowMount(InputDateBirth, {
				localVue,
				stubs,
				mocks: {
					$store
				}
			});
		});

		it('should render flatPickr date as 1990-01-01 when it is not provided by vuex', () => {
			const flatPickr = wrapper.find('.flatpickr-input');
			expect(flatPickr.attributes('value')).toBe('1990-01-01');
		});

		it('should set the flatPickr date when the it is provided by vuex', () => {
			const flatPickr = wrapper.find('.flatpickr-input');

			wrapper.vm.$store.state.userEditProfile.birthday = 1318781876;
			wrapper.vm.$nextTick(() => {
				expect(flatPickr.attributes('value')).toBe('1970-01-16');
			});
		});

		it('should reset the flatPickr date to 1990-01-01 when the date provided is within the past 5 years', () => {
			const todayTimestamp = moment().format('x');
			const flatPickr = wrapper.find('.flatpickr-input');

			wrapper.vm.$store.state.userEditProfile.birthday = todayTimestamp;
			wrapper.vm.$nextTick(() => {
				expect(flatPickr.attributes('value')).toBe('1990-01-01');
			});
		});

		it('should call $store.commit() function when the date is set', () => {
			const todayTimestamp = moment().format('x');

			wrapper.setData({ valueBirthday: todayTimestamp });
			wrapper.vm.$nextTick(() => {
				expect($store.commit).toBeCalled();
			});
		});
	});
});
