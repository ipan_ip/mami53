import Vuex from 'vuex';
import { createLocalVue, shallowMount } from '@vue/test-utils';
import { select, option } from 'vue-strap';

import areaCities from '../__mocks__/areaCities.json';
import InputHometown from 'Js/_user/components/forms/InputHometown.vue';

import mockAxios from 'tests-fe/utils/mock-axios';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

describe('InputHometown.vue', () => {
	const localVue = createLocalVue();
	localVue.use(Vuex);
	mockVLazy(localVue);

	mockWindowProperty('bugsnagClient', { notify: jest.fn() });

	global.axios = mockAxios;

	describe('should render correctly', () => {
		let wrapper;
		const stubs = {
			vSelect: select,
			vOption: option
		};
		const $store = {
			state: {
				userEditProfile: {
					city: ''
				},
				allCity: []
			},
			commit: jest.fn(() => {})
		};

		beforeEach(() => {
			wrapper = shallowMount(InputHometown, {
				localVue,
				stubs,
				mocks: {
					$store
				}
			});
		});

		it('should render hometown container correctly', () => {
			expect(
				wrapper.find('.input-control.input-control-custom').exists()
			).toBeTruthy();
		});

		it('should render vSelect correctly', () => {
			const vSelect = wrapper.find('select.secret');
			expect(vSelect.exists()).toBeTruthy();
		});

		it('should render vSelect options correctly', async () => {
			wrapper.vm.$store.state.allCity = areaCities;
			wrapper.vm.$nextTick(() => {
				const vSelectOptions = wrapper.findAll('select.secret option');
				expect(vSelectOptions.length).toBe(areaCities.length);
			});
		});

		it('should render vSelect dropdown menu correctly', async () => {
			wrapper.vm.$store.state.allCity = areaCities;
			wrapper.vm.$nextTick(() => {
				const dropdownMenu = wrapper.find('.dropdown-menu');
				expect(dropdownMenu.exists()).toBeTruthy();

				const dropdownMenuList = wrapper.findAll('.dropdown-menu li ');
				expect(dropdownMenuList.length).toBe(areaCities.length + 1);
			});
		});

		it('should show failed alert if errorInput is true', async () => {
			await wrapper.setData({ errorInput: true });
			expect(wrapper.find('.failed-alert').exists()).toBeTruthy();
		});
	});

	describe('should call functions correctly', () => {
		const stubs = {
			vSelect: select,
			vOption: option
		};
		const $store = {
			state: {
				userEditProfile: {
					city: null
				},
				allCity: []
			},
			commit: jest.fn()
		};

		let wrapper;
		beforeEach(() => {
			wrapper = shallowMount(InputHometown, {
				localVue,
				stubs,
				mocks: {
					$store
				}
			});
		});

		it('should set errorInput as false on mounted', () => {
			expect(wrapper.vm.errorInput).toBe(true);
		});

		it('should call $store.commit function with areaCities as the param after fetching city data correctly', async () => {
			axios.mockResolve({ data: { cities: areaCities, status: 200 } });
			await wrapper.vm.getCity();
			expect($store.commit).toBeCalled();

			const calls = $store.commit.mock.calls || [[]];
			expect(calls[calls.length - 1][0]).toBe('setAllCity');

			const vSelectOptions = calls[calls.length - 1][1];
			expect(vSelectOptions.length).toEqual(areaCities.length);
		});

		it('should set errorInput value as false if valueCity exists', async () => {
			wrapper.vm.$store.state.userEditProfile.city = 'test';
			await wrapper.vm.$nextTick();
			expect(wrapper.vm.errorInput).toBe(false);
		});

		it('should set errorInput value as false if valueCity is null', async () => {
			wrapper.vm.$store.state.userEditProfile.city = 'test';
			await wrapper.vm.$nextTick();
			wrapper.vm.$store.state.userEditProfile.city = null;
			await wrapper.vm.$nextTick();
			expect(wrapper.vm.errorInput).toBe(true);
		});

		it('should call bugsnagClient.notify function when calling getCity function and caught errors', async () => {
			axios.mockReject('error');
			await wrapper.vm.getCity();
			expect(global.bugsnagClient.notify).toBeCalled();
		});
	});
});
