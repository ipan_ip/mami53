import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';

import { mockComponentWithAttrs } from 'tests-fe/utils/mock-component';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

import UserProfile from 'Js/_user/components/UserProfile.vue';

// mock store
const UserProfileStore = {
	state: {
		userProfile: {
			phone: '081234567890',
			name: 'name',
			is_verify_phone_number: true,
			photo: {
				medium: '/path/to/medium/sized/image'
			}
		}
	}
};

// mock stubbed component
const stubs = {
	RouterLink: true
};

describe('UserProfile.vue', () => {
	const localVue = createLocalVue();
	localVue.use(Vuex);

	// mock directives
	mockVLazy(localVue);

	const mount = () => {
		return shallowMount(UserProfile, {
			localVue,
			stubs,
			store: new Vuex.Store(UserProfileStore)
		});
	};

	describe('render component correctly', () => {
		let wrapper;

		beforeEach(() => {
			wrapper = mount();
		});

		it('should render container', () => {
			expect(wrapper.find('.detail-profil-wrap').exists()).toBeTruthy();
		});
	});

	describe('render computed data properly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = mount();
		});

		it('should return photoMedium value from $store', () => {
			expect(wrapper.vm.photoMedium).toBe('/path/to/medium/sized/image');
		});

		it('should return verifyPhoneNumber value from $store', () => {
			expect(wrapper.vm.verifyPhoneNumber).toBeTruthy();
		});

		it('should return userPhone value from $store', () => {
			expect(wrapper.vm.userPhone).toBe('081234567890');
		});

		it('should return userName value from $store', () => {
			expect(wrapper.vm.userName).toBe('name');

			// length > 19
			wrapper.vm.$store.state.userProfile.name =
				'areallylongnamethatshouldexceednineteencharacters test';
			expect(wrapper.vm.userName).toBe('areallylongnamethat...');
		});

		it('should return userPhoto value from $store', () => {
			// get
			expect(wrapper.vm.userPhoto).toEqual({
				src: '/path/to/medium/sized/image',
				loading: '/general/img/pictures/default-user-image.png',
				error: '/general/img/pictures/default-user-image.png'
			});
		});
	});
});
