import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import moment from 'moment';
import _find from 'lodash/find';
import _get from 'lodash/get';

import mockAxios from 'tests-fe/utils/mock-axios';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import { mockComponentWithAttrs } from 'tests-fe/utils/mock-component';

import UserKost from 'Js/_user/components/UserKost.vue';
import mixinUserKost from 'Js/_user/mixins/mixinUserKost';

// mock imports
jest.mock('Js/_user/event-bus/event-bus', () => {
	const EventBus = {
		$emit: jest.fn()
	};

	global.EventBus = EventBus;
	return global.EventBus;
});

// mock store
const UserKostStore = {
	state: {
		userMenuActive: '',
		authData: {
			all: 'all'
		},
		statusRent: {
			status: false,
			checkout_date: '2020-08-10 09:10:11'
		},
		userKost: {
			contract_id: 'contract_id',
			invoice: [
				{
					invoice_number: 'DP_123'
				},
				{
					invoice_number: 'ST_123',
					status: 'paid', // paid_late, paid, verified
					shortlink: 'link/to/settlement/page'
				}
			],
			room_data: {
				name: 'room_name'
			},
			booking_data: {
				booking_code: 'booking_code',
				checkin: '2020-08-10 10:11:12',
				status: 'verified',
				has_checked_in: true,
				rent_count_type: 'monthly'
			}
		}
	},
	mutations: {
		setStatusRent(state, statusRent) {
			state.statusRent = { ...state.statusRent, ...statusRent };
		},
		setMenu(state, userMenuActive) {
			state.userMenuActive = userMenuActive;
		},
		setUserKost(state, userKost) {
			state.userKost = { ...state.userKost, ...userKost };
		}
	}
};

// mock stubbed component
const stubs = {
	MamiLoadingInline: mockComponentWithAttrs({ class: 'mami-loading-inline' }),
	UserKostClaim: mockComponentWithAttrs({ id: 'userKostClaim' }),
	UserKostClaimDetail: mockComponentWithAttrs({ id: 'userKostClaimDetail' }),
	UserKostClaimDetailBill: mockComponentWithAttrs({
		id: 'userKostClaimDetailBill'
	}),
	UserKostContract: mockComponentWithAttrs({ id: 'userKostContract' }),
	UserKostData: mockComponentWithAttrs({ id: 'userKostData' }),
	UserKostEmpty: mockComponentWithAttrs({ id: 'userKostEmpty' }),
	UserKostModalStopRent: mockComponentWithAttrs({
		id: 'userKostModalStopRent'
	}),
	BookingActionModal: mockComponentWithAttrs({ id: 'bookingActionModal' }),
	BookingBasicModal: mockComponentWithAttrs({ id: 'bookingBasicModal' })
};

describe('UserKost.vue', () => {
	const localVue = createLocalVue();
	localVue.use(Vuex);

	// implement mixin
	localVue.mixin([mixinUserKost]);

	// mock global functions
	global.axios = mockAxios;
	global.bugsnagClient = {
		notify: jest.fn()
	};

	// mock window functions
	mockWindowProperty('open', jest.fn());
	mockWindowProperty(
		'document.querySelector',
		jest.fn().mockReturnValue({ click: jest.fn() })
	);

	const mount = () => {
		return shallowMount(UserKost, {
			localVue,
			stubs,
			store: new Vuex.Store(UserKostStore),
			methods: {
				$dayjs: moment,
				swalError: jest.fn(),
				closeSwalLoading: jest.fn()
			},
			mocks: {
				_find,
				_get
			}
		});
	};

	describe('render component correctly', () => {
		let wrapper;

		beforeEach(() => {
			wrapper = mount();
		});

		it('should render container', () => {
			expect(wrapper.find('.row.content').exists()).toBeTruthy();
		});

		it('should render mami inline when isLoading is true', async () => {
			wrapper.setData({ isLoading: true });
			await wrapper.vm.$nextTick();
			expect(wrapper.find('.mami-loading-inline').exists()).toBeTruthy();
		});
	});

	describe('render computed data properly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = mount();
		});

		it('should return authData.all from $store', () => {
			expect(wrapper.vm.authData).toBe('all');
		});

		it('should return rentCountType from $store', () => {
			expect(wrapper.vm.rentCountType).toBe('monthly');
		});

		it('should return userKost from $store', () => {
			expect(wrapper.vm.userKost).toEqual(UserKostStore.state.userKost);
		});

		it('should return room name from $store', () => {
			expect(wrapper.vm.roomName).toBe('room_name');
		});

		it('should return checkin date from $store', () => {
			expect(wrapper.vm.checkinDate).toBe('2020-08-10 10:11:12');
		});

		it('should return isDownPayment from $store', () => {
			expect(wrapper.vm.isDownPayment).toBeTruthy();
		});

		it('should return bookingIdCheckin from $store', () => {
			expect(wrapper.vm.bookingIdCheckin).toBe('booking_code');
		});

		it('should return isSettlement from $store', () => {
			expect(wrapper.vm.isSettlement).toBeTruthy();
		});

		it('should return isSettlementPaid from $store', () => {
			// false condition: payment status is "test"
			wrapper.vm.$store.state.userKost.invoice[1].status = 'test';
			expect(wrapper.vm.isSettlementPaid).toBeFalsy();

			// true condition: payment status is "paid"
			wrapper.vm.$store.state.userKost.invoice[1].status = 'paid';
			expect(wrapper.vm.isSettlementPaid).toBeTruthy();
		});

		it('should return urlInvoiceSettlement from $store', () => {
			expect(wrapper.vm.urlInvoiceSettlement).toBe('link/to/settlement/page');
		});

		it('should return checkoutDateRent value properly', () => {
			// not empty
			expect(wrapper.vm.checkoutDateRent).toBe('Monday, 10 August 2020');

			// empty
			wrapper.vm.$store.state.statusRent.checkout_date = null;
			expect(wrapper.vm.checkoutDateRent).toBe('');
		});

		it('should return contractAlert value properly', () => {
			/* truthy condition:
      isContract && rentCountType !== 'weekly' &&
      !getStatusRent.status &&
      userKost.booking_data.status === 'verified' &&
      numberActiveInvoice !== 0
    */
			wrapper.setData({
				isContract: true,
				numberActiveInvoice: 1
			});
			expect(wrapper.vm.contractAlert).toBeTruthy();
		});
	});

	describe('call functions correctly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = mount();
		});

		it('should return isCheckinTime value properly', () => {
			let isCheckinTime = wrapper.vm.isCheckinTime(new Date());
			expect(isCheckinTime).toBeTruthy();

			isCheckinTime = wrapper.vm.isCheckinTime(moment().add(1));
			expect(isCheckinTime).toBeTruthy();
		});

		it('should return actionCheckin return value properly', async () => {
			// show confirm settlement modal when settlement status is not paid
			wrapper.vm.$store.state.userKost.invoice[1].status = 'test';
			await wrapper.vm.$nextTick();
			await wrapper.vm.actionCheckin();
			expect(wrapper.vm.isModalSettlementConfirm).toBeTruthy();

			// show accept checkin modal when settlement status is paid but checkin date is after current date
			wrapper.vm.$store.state.userKost.booking_data.checkin = new Date();
			wrapper.vm.$store.state.userKost.invoice[1].status = 'paid';
			await wrapper.vm.$nextTick();
			await wrapper.vm.actionCheckin();
			expect(wrapper.vm.isModalAcceptCheckin).toBeTruthy();

			// show 'it is not time for checkin' modal when settlement status is paid, but the checkin date is way after current date
			wrapper.vm.$store.state.userKost.booking_data.checkin =
				'2050-08-10 10:11:12';
			wrapper.vm.$store.state.userKost.invoice[1].status = 'paid';
			await wrapper.vm.$nextTick();
			await wrapper.vm.actionCheckin();
			expect(wrapper.vm.isModalNotTimeCheckin).toBeTruthy();
		});

		it('should hide modal and call window.open when calling goSettlementInvoice method', async () => {
			await wrapper.vm.goSettlementInvoice();
			expect(wrapper.vm.isModalSettlementConfirm).toBeFalsy();
			expect(window.open).toBeCalled();
		});

		it('should call window.open when calling goToMyKost method', async () => {
			await wrapper.vm.goToMyKost();
			expect(window.open).toBeCalled();
		});

		it('should call goCheckinProccess when calling goCheckin method and the booking code is not empty', async () => {
			const goCheckinProcessSpy = jest.spyOn(wrapper.vm, 'goCheckinProccess');

			await wrapper.vm.goCheckin();
			expect(goCheckinProcessSpy).toBeCalled();

			goCheckinProcessSpy.mockRestore();
		});

		it('should return reviews made by user when calling isMyReview method', () => {
			const myReview = wrapper.vm.isMyReview([
				{
					content: 'test',
					is_me: true
				}
			]);
			expect(myReview).toEqual({
				content: 'test',
				is_me: true
			});
		});

		it('should call document.querySelector when calling openChatToOwner method', async () => {
			// falsy condition
			const consoleSpy = jest.spyOn(global.console, 'error');
			mockWindowProperty('document.querySelector', jest.fn());
			await wrapper.vm.openChatToOwner();
			expect(consoleSpy).toBeCalled();
			consoleSpy.mockRestore();

			// truthy condition
			mockWindowProperty(
				'document.querySelector',
				jest.fn().mockReturnValue({ click: jest.fn() })
			);
			await wrapper.vm.openChatToOwner();
			expect(document.querySelector).toBeCalled();
		});

		it('should set isContract  and isClaim data when calling toggleContract method properly', async () => {
			await wrapper.vm.toggleContract();

			expect(wrapper.vm.isContract).toBeTruthy();
			expect(wrapper.vm.isClaim).toBeFalsy();
		});

		it('should set isContract  and isClaim data when calling toggleClaim method properly', async () => {
			await wrapper.vm.toggleClaim();

			expect(wrapper.vm.isContract).toBeFalsy();
			expect(wrapper.vm.isClaim).toBeTruthy();
		});

		it('should set isClaimDetail data when calling closeDetailClaim method properly', async () => {
			await wrapper.vm.closeDetailClaim();

			expect(wrapper.vm.isClaimDetail).toBeFalsy();
		});

		it('should hide checkin modal, and show success checkin modal, if request status is true on successful request when calling goCheckinProccess method', async () => {
			await axios.mockResolve({
				data: {
					status: true
				}
			});
			await wrapper.vm.goCheckinProccess();
			expect(wrapper.vm.isModalAcceptCheckin).toBeFalsy();
			expect(wrapper.vm.isModalSuccessCheckin).toBeTruthy();
			expect(wrapper.vm.isDisabledCheckinButton).toBeFalsy();
		});

		it('should call swalError if request status is false on successful request when calling goCheckinProccess method', async () => {
			const swalErrorSpy = jest.spyOn(wrapper.vm, 'swalError');
			await axios.mockResolve({
				data: {
					status: false,
					meta: {
						message: 'test'
					}
				}
			});
			await wrapper.vm.goCheckinProccess();
			expect(swalErrorSpy).toBeCalled();
			swalErrorSpy.mockRestore();
		});

		it('should set last review & tenant review on successful request when calling getReviewData method, and the review data is not empty', async () => {
			await axios.mockResolve({
				data: {
					status: true,
					review: [
						{
							is_me: true,
							clean: 1,
							safe: 2,
							happy: 3,
							room_facilities: 4,
							public_facilities: 5,
							pricing: 1,
							content: 'test',
							is_anonim: true,
							photo: 'test',
							status: true,
							rating: 4
						}
					]
				}
			});
			await wrapper.vm.getReviewData();
			expect(wrapper.vm.tenantReview).toEqual({
				reviewStatus: true,
				reviewScore: '4.0',
				reviewContent: 'test'
			});
		});

		it('should set claim data & detail, if request status is true on successful request when calling getDetailClaim  method', async () => {
			await axios.mockResolve({
				data: {
					status: true,
					data: 'test'
				}
			});
			await wrapper.vm.getDetailClaim({ data: {}, index: 1 });
			expect(wrapper.vm.claimDataIndex).toBe(1);
			expect(wrapper.vm.claimData).toEqual({});
			expect(wrapper.vm.claimDetail).toBe('test');
			expect(wrapper.vm.isClaimDetail).toBeTruthy();
		});

		it('should call swal error if request status is false on successful request when calling getDetailClaim  method', async () => {
			const swalErrorSpy = jest.spyOn(wrapper.vm, 'swalError');
			await axios.mockResolve({
				data: {
					status: false,
					meta: {
						message: ''
					}
				}
			});
			await wrapper.vm.getDetailClaim({ data: {}, index: 1 });
			expect(swalErrorSpy).toBeCalled();
			swalErrorSpy.mockRestore();
		});

		it('should call bugsnagClient on failed request when calling getDetailClaim  method', async () => {
			await axios.mockReject({
				data: {
					status: false
				}
			});
			await wrapper.vm.getDetailClaim({ data: {} });
			expect(bugsnagClient.notify).toBeCalled();
		});
	});
});
