import { shallowMount, createLocalVue, RouterLinkStub } from '@vue/test-utils';
import HistoryKostList from 'Js/_user/components/history-kost/HistoryKostList.vue';
import mockSwalLoading from 'tests-fe/utils/mock-swal-loading';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import historyKostListData from './__mocks__/history-kost-list.json';

const localVue = createLocalVue();
window.Vue = localVue;
mockSwalLoading();

const axios = {
	get: jest.fn().mockResolvedValue({
		data: {
			data: {
				current_page: 1,
				per_page: 5,
				has_more: false,
				data: [...historyKostListData]
			},
			status: true
		}
	})
};

global.axios = axios;

const bugsnagClient = { notify: jest.fn() };
global.bugsnagClient = bugsnagClient;

describe('HistoryKostList.vue', () => {
	const wrapper = shallowMount(HistoryKostList, {
		localVue,
		stubs: {
			RouterLink: RouterLinkStub
		}
	});

	it('should render history kost card', () => {
		expect(wrapper.find('.history-kost-list').exists()).toBe(true);
	});

	it('should render all history kost', () => {
		expect(wrapper.findAll('history-kost-card-stub').length).toBe(
			historyKostListData.length
		);
	});

	it('should render loading skeleton if is loading', async () => {
		await wrapper.setData({ isLoading: true });
		expect(wrapper.find('history-kost-card-skeleton-stub').exists()).toBe(true);

		await wrapper.setData({ isLoading: false });
		expect(wrapper.find('history-kost-card-skeleton-stub').exists()).toBe(
			false
		);
	});

	it('should render empty state if first page is empty', async () => {
		await wrapper.setData({
			currentPage: 1,
			historyKostList: [],
			isLoading: false
		});
		expect(wrapper.find('history-kost-empty-state-stub').exists()).toBe(true);
	});

	it('should call getHistoryKostList if in bottom of the page and has more history kost', async () => {
		await wrapper.setData({
			currentPage: 1,
			hasMoreHistoryKostList: true,
			isLoading: false
		});

		const spyGetHistoryKostList = jest.spyOn(wrapper.vm, 'getHistoryKostList');

		mockWindowProperty('window.innerHeight', 768);
		mockWindowProperty('window.document.documentElement.scrollTop', 200);
		mockWindowProperty('window.document.documentElement.offsetHeight', 968);

		window.dispatchEvent(new CustomEvent('scroll'));

		expect(spyGetHistoryKostList).toBeCalled();
		spyGetHistoryKostList.mockRestore();
	});

	it('should not call getHistoryKostList if in bottom of the page and has no more history kost', async () => {
		await wrapper.setData({
			currentPage: 1,
			hasMoreHistoryKostList: false,
			isLoading: false
		});

		const spyGetHistoryKostList = jest.spyOn(wrapper.vm, 'getHistoryKostList');

		mockWindowProperty('window.innerHeight', 768);
		mockWindowProperty('window.document.documentElement.scrollTop', 200);
		mockWindowProperty('window.document.documentElement.offsetHeight', 968);

		window.dispatchEvent(new CustomEvent('scroll'));

		expect(spyGetHistoryKostList).not.toBeCalled();
		spyGetHistoryKostList.mockRestore();
	});

	it('should not call getHistoryKostList if in bottom of the page and is loading', async () => {
		await wrapper.setData({
			currentPage: 1,
			hasMoreHistoryKostList: true,
			isLoading: true
		});

		const spyGetHistoryKostList = jest.spyOn(wrapper.vm, 'getHistoryKostList');

		mockWindowProperty('window.innerHeight', 768);
		mockWindowProperty('window.document.documentElement.scrollTop', 200);
		mockWindowProperty('window.document.documentElement.offsetHeight', 968);

		window.dispatchEvent(new CustomEvent('scroll'));

		expect(spyGetHistoryKostList).not.toBeCalled();
		spyGetHistoryKostList.mockRestore();
	});

	it('should show swal error if get history kost list return status false', async () => {
		global.axios.get = jest.fn().mockResolvedValue({
			data: {
				status: false,
				meta: {
					message: 'meta message'
				}
			}
		});
		const spySwalError = jest.spyOn(wrapper.vm, 'swalError');
		await wrapper.vm.getHistoryKostList();

		expect(spySwalError).toBeCalledWith(null, 'meta message');

		global.axios.get = jest.fn().mockResolvedValue({
			data: {
				status: false
			}
		});
		await wrapper.vm.getHistoryKostList();

		expect(spySwalError).toBeCalledWith(
			null,
			'Terjadi galat. Silakan coba lagi.'
		);
		spySwalError.mockRestore();
	});

	it('should call bugsnag notify if get history kost list is failed', async () => {
		global.axios.get = jest.fn().mockRejectedValue(new Error('error'));

		const spyNotify = jest.spyOn(global.bugsnagClient, 'notify');
		await wrapper.vm.getHistoryKostList();

		expect(spyNotify).toBeCalled();
		spyNotify.mockRestore();
	});

	it('should remove scroll event listener before component destoryed', () => {
		const spyRemoveEventListener = jest.spyOn(window, 'removeEventListener');
		wrapper.destroy();

		expect(spyRemoveEventListener).toBeCalledWith(
			'scroll',
			expect.any(Function)
		);

		spyRemoveEventListener.mockRestore();
	});
});
