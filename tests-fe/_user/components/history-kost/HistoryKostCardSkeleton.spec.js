import { shallowMount, createLocalVue } from '@vue/test-utils';
import HistoryKostCardSkeleton from 'Js/_user/components/history-kost/HistoryKostCardSkeleton.vue';

const localVue = createLocalVue();

describe('HistoryKostCardSkeleton', () => {
	const wrapper = shallowMount(HistoryKostCardSkeleton, { localVue });

	it('should render history kost card skeleton', () => {
		expect(wrapper.find('.history-kost-card-skeleton').exists()).toBe(true);
	});

	it('should render all skeletons', () => {
		expect(wrapper.findAll('skeleton-loader-stub').length).toBe(7);
	});
});
