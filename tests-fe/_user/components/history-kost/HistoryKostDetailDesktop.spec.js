import { createLocalVue, shallowMount } from '@vue/test-utils';
import HistoryKostDetailDesktop from 'Js/_user/components/history-kost/HistoryKostDetailDesktop.vue';
import Vuex from 'vuex';
import dayjs from 'dayjs';

const localVue = createLocalVue();
localVue.prototype.$dayjs = dayjs;
localVue.use(Vuex);

const store = {
	state: {
		historyKostDetail: {
			room: {
				id: 1,
				name: 'kost test',
				checker: null,
				slug: 'kost-test'
			},
			duration_string: '2 Bulan',
			is_flash_sale: false,
			flash_sale: null,
			review: {
				rating: '5.0'
			},
			owner: {
				name: 'owner name'
			},
			tenant: {
				name: 'tenant name',
				phone: '08271828282'
			}
		},
		historyDetailCheckin: '20 Februari 2020',
		historyDetailCheckout: '30 April 2020'
	}
};

const $route = {
	params: {
		contractId: 12
	}
};

const BookingDetailCardSpending = {
	template: '<div class="history-kos-detail-wrapper" />'
};

const BookingModalFacilities = {
	template: '<div class="booking-modal-facilities" />'
};

const $router = { push: jest.fn() };

describe('HistoryKostDetailDesktop.vue', () => {
	const wrapper = shallowMount(HistoryKostDetailDesktop, {
		localVue,
		store: new Vuex.Store(store),
		mocks: { $route, $router },
		stubs: { BookingDetailCardSpending, BookingModalFacilities },
		propsData: {
			rentCount: {
				rentType: 'Bulan',
				rentPrice: 700000
			}
		}
	});

	it('should render history detail wrapper', () => {
		expect(wrapper.find('.history-kos-detail-wrapper').exists()).toBe(true);
	});

	it('should navigate to riwayat transaksi with contract id', () => {
		const spyRouterPush = jest.spyOn(wrapper.vm.$router, 'push');
		wrapper
			.findComponent(BookingDetailCardSpending)
			.vm.$emit('view-transaction');

		expect(spyRouterPush).toBeCalledWith('/riwayat-transaksi?contract=12');
		spyRouterPush.mockRestore();
	});

	it('should set is discount applied properly', () => {
		expect(wrapper.vm.isDiscountApplied).toBe(false);
		wrapper.vm.$store.state.historyKostDetail = {
			...wrapper.vm.$store.state.historyKostDetail,
			is_flash_sale: true,
			flash_sale: {
				discount: '10%'
			}
		};

		expect(wrapper.vm.isDiscountApplied).toBe(true);
	});

	it('should close booking modal facilities when component emit close', async () => {
		await wrapper.setData({ isShowFacilities: true });
		wrapper.findComponent(BookingModalFacilities).vm.$emit('close');
		expect(wrapper.vm.isShowFacilities).toBe(false);
	});

	it('should navigate to riwayat kos page if back button clicked', () => {
		const spyRouterPush = jest.spyOn(wrapper.vm.$router, 'push');
		wrapper
			.findComponent(BookingDetailCardSpending)
			.vm.$emit('view-transaction');

		wrapper.find('.detail-nav-back').trigger('click');
		expect(spyRouterPush).toBeCalledWith('/riwayat-kos');
		spyRouterPush.mockRestore();
	});
});
