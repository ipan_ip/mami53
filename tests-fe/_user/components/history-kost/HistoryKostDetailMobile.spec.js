import { createLocalVue, shallowMount } from '@vue/test-utils';
import HistoryKostDetailMobile from 'Js/_user/components/history-kost/HistoryKostDetailMobile.vue';
import Vuex from 'vuex';

const localVue = createLocalVue();
localVue.use(Vuex);

const store = {
	state: {
		historyKostDetail: {
			room: {
				id: 1,
				name: 'kost test',
				checker: null,
				slug: 'kost-test'
			},
			duration_string: '2 Bulan',
			review: {
				rating: '5.0'
			},
			owner: {
				name: 'owner name'
			},
			tenant: {
				name: 'tenant name'
			}
		},
		historyDetailCheckin: '20 Februari 2020',
		historyDetailCheckout: '30 April 2020'
	}
};

const $route = {
	params: {
		contractId: 12
	}
};

const BookingDetailCardSpending = {
	template: '<div class="booking-detail-card-spending" />'
};

const $router = { push: jest.fn() };

describe('HistoryKostDetailMobile.vue', () => {
	const wrapper = shallowMount(HistoryKostDetailMobile, {
		localVue,
		store: new Vuex.Store(store),
		mocks: { $route, $router },
		stubs: { BookingDetailCardSpending },
		propsData: {
			rentCount: {
				rentType: 'Bulan',
				rentPrice: 700000
			}
		}
	});

	it('should render history detail mobile', () => {
		expect(wrapper.find('.history-kost-detail-mobile').exists()).toBe(true);
	});

	it('should navigate to riwayat transaksi with contract id', () => {
		const spyRouterPush = jest.spyOn(wrapper.vm.$router, 'push');
		wrapper
			.findComponent(BookingDetailCardSpending)
			.vm.$emit('view-transaction');

		expect(spyRouterPush).toBeCalledWith('/riwayat-transaksi?contract=12');
		spyRouterPush.mockRestore();
	});
});
