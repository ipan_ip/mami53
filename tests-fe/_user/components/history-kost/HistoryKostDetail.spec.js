import { createLocalVue, shallowMount } from '@vue/test-utils';
import HistoryKostDetail from 'Js/_user/components/history-kost/HistoryKostDetail.vue';
import Vuex from 'vuex';
import dayjs from 'dayjs';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import { flushPromises } from 'tests-fe/utils/promises';

const localVue = createLocalVue();
localVue.prototype.$dayjs = dayjs;
mockVLazy(localVue);
localVue.use(Vuex);

const historyKostDetail = {
	room: {
		id: 1,
		name: 'kost test',
		checker: null,
		slug: 'kost-test',
		price: {
			monthly: 5000
		}
	},
	rent_count_type: 'monthly',
	duration_string: '2 Bulan',
	is_flash_sale: false,
	flash_sale: null,
	review: {
		rating: '5.0'
	},
	owner: {
		name: 'owner name'
	},
	tenant: {
		name: 'tenant name',
		phone: '08271828282'
	}
};

const store = {
	state: {
		historyKostDetail: {},
		historyDetailCheckin: '20 Februari 2020',
		historyDetailCheckout: '30 April 2020'
	},
	mutations: {
		setHistoryKostDetail: (state, payload) => {
			state.historyKostDetail = payload;
		},
		setHistoryDetailCheckin: jest.fn(),
		setHistoryDetailCheckout: jest.fn()
	}
};

const $route = {
	params: {
		contractId: 12
	}
};

const $router = { push: jest.fn() };

const axios = {
	get: jest.fn().mockResolvedValue({
		data: {
			data: historyKostDetail,
			status: true
		}
	})
};

global.axios = axios;

const bugsnagClient = { notify: jest.fn() };
global.bugsnagClient = bugsnagClient;

describe('HistoryKostDetail.vue', () => {
	const wrapper = shallowMount(HistoryKostDetail, {
		localVue,
		store: new Vuex.Store(store),
		mocks: { $route, $router }
	});

	it('should render history kos detail', () => {
		expect(wrapper.find('.history-kos-detail').exists()).toBe(true);
	});

	it('should set price correctly', () => {
		const price = {
			daily: {
				label: 'hari',
				amount: 1000
			},
			weekly: {
				label: 'minggu',
				amount: 2000
			},
			monthly: {
				label: 'bulan',
				amount: 3000
			},
			quarterly: {
				label: '3 bulan',
				amount: 4000
			},
			semiannually: {
				label: '6 bulan',
				prop: 'semiannualy',
				amount: 5000
			},
			yearly: {
				label: 'tahun',
				amount: 6000
			}
		};

		Object.keys(price).forEach(async priceKey => {
			const { label, amount, prop } = price[priceKey];

			wrapper.vm.$store.state.historyKostDetail.room.price[
				prop || priceKey
			] = amount;

			await wrapper.vm.$nextTick();
			wrapper.vm.$store.state.historyKostDetail.rent_count_type = priceKey;

			expect(wrapper.vm.rentCount).toEqual({
				rentType: label,
				rentPrice: amount
			});
		});
	});

	it('should show modal error if get history detail failed', async () => {
		await wrapper.setData({ isModalError: false });
		global.axios.get = jest.fn().mockRejectedValue(new Error('error'));
		await wrapper.vm.getHistoryDetail();
		await flushPromises();
		expect(bugsnagClient.notify).toBeCalled();
		expect(wrapper.vm.isModalError).toBe(true);
	});

	it('should show modal error if get history detail status is false', async () => {
		await wrapper.setData({ isModalError: false });
		global.axios.get = jest
			.fn()
			.mockResolvedValue({ data: { data: {}, meta: { message: 'error' } } });
		await wrapper.vm.getHistoryDetail();
		await flushPromises();
		expect(wrapper.vm.isModalError).toBe(true);
	});

	it('should navigate back to riwayat kos if there is no contract id param', async () => {
		const wrapper = shallowMount(HistoryKostDetail, {
			localVue,
			store: new Vuex.Store(store),
			mocks: { $route: { params: {} }, $router }
		});

		const spyPush = jest.spyOn(wrapper.vm.$router, 'push');
		expect(spyPush).toBeCalledWith('/riwayat-kos');
		spyPush.mockRestore();
	});

	describe('isMobile', () => {
		mockWindowProperty('window.innerWidth', 700);

		const HistoryKostDetailMobile = { template: '<div />' };

		const createWrapper = (stubs = {}) =>
			shallowMount(HistoryKostDetail, {
				localVue,
				store: new Vuex.Store(store),
				mocks: { $route, $router },
				stubs: {
					...stubs,
					HistoryKostDetailMobile
				}
			});

		it('should render mobile component if it is mobile', async () => {
			const wrapper = createWrapper();
			await wrapper.setData({ isLoading: false });
			expect(wrapper.vm.isMobile).toBe(true);
			expect(wrapper.findComponent(HistoryKostDetailMobile).exists()).toBe(
				true
			);
			expect(wrapper.find('.btn-close-filter').exists()).toBe(true);
		});

		it('should navigate to riwayat kos if button close clicked', () => {
			const FilterCloseButton = { template: '<div />' };
			const wrapper = createWrapper({ FilterCloseButton });
			const spyPush = jest.spyOn(wrapper.vm.$router, 'push');
			wrapper.findComponent(FilterCloseButton).vm.$emit('press');
			expect(spyPush).toBeCalledWith('/riwayat-kos');
			spyPush.mockRestore();
		});
	});
});
