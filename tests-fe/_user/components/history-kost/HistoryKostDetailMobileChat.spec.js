import { createLocalVue, shallowMount } from '@vue/test-utils';
import HistoryKostDetailMobileChat from 'Js/_user/components/history-kost/HistoryKostDetailMobileChat.vue';

const localVue = createLocalVue();

describe('HistoryKostDetailMobileChat.vue', () => {
	const wrapper = shallowMount(HistoryKostDetailMobileChat, { localVue });

	it('should render history detail mobile chat', () => {
		expect(wrapper.find('.history-detail-mobile-chat').exists()).toBe(true);
	});

	it('should render icon chat', () => {
		expect(wrapper.find('icon-chat-stub').exists()).toBe(true);
	});
});
