import { createLocalVue, shallowMount } from '@vue/test-utils';
import HistoryKostDetailDesktopSkeleton from 'Js/_user/components/history-kost/HistoryKostDetailDesktopSkeleton.vue';

const localVue = createLocalVue();

describe('HistoryKostDetailDesktopSkeleton.vue', () => {
	const wrapper = shallowMount(HistoryKostDetailDesktopSkeleton, { localVue });

	it('should render skeleton loading', () => {
		expect(wrapper.find('.history-kost-desktop-skeleton').exists()).toBe(true);
	});

	it('should render all skeletons', () => {
		expect(wrapper.findAll('skeleton-loader-stub').length).toBe(10);
	});
});
