import { createLocalVue, shallowMount } from '@vue/test-utils';
import HistoryKostDetailMobileSkeleton from 'Js/_user/components/history-kost/HistoryKostDetailMobileSkeleton.vue';

const localVue = createLocalVue();

describe('HistoryKostDetailMobileSkeleton.vue', () => {
	const wrapper = shallowMount(HistoryKostDetailMobileSkeleton, { localVue });

	it('should render skeleton loading', () => {
		expect(wrapper.find('.history-kost-mobile-skeleton').exists()).toBe(true);
	});

	it('should render all skeletons', () => {
		expect(wrapper.findAll('skeleton-loader-stub').length).toBe(11);
	});
});
