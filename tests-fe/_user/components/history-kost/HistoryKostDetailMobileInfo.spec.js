import { createLocalVue, shallowMount } from '@vue/test-utils';
import HistoryKostDetailMobileInfo from 'Js/_user/components/history-kost/HistoryKostDetailMobileInfo';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

const localVue = createLocalVue();
mockVLazy(localVue);

describe('HistoryKostDetailMobileInfo.vue', () => {
	const bookingDetail = {
		room: {
			gender: 1,
			name: 'kost test',
			number: 12,
			photo: {
				small: ''
			}
		},
		is_flash_sale: false,
		flash_sale: null
	};

	const rentCount = {
		rentType: 'Bulan',
		rentPrice: 700000
	};

	const wrapper = shallowMount(HistoryKostDetailMobileInfo, {
		localVue,
		propsData: { bookingDetail, rentCount }
	});

	it('should render history kost info', () => {
		expect(wrapper.find('.history-kost-info').exists()).toBe(true);
	});

	it('should render correct gender', async () => {
		expect(wrapper.find('.property-gender').text()).toBe('Putra');

		await wrapper.setProps({
			bookingDetail: {
				...bookingDetail,
				room: { ...bookingDetail.room, gender: 2 }
			}
		});
		expect(wrapper.find('.property-gender').text()).toBe('Putri');

		await wrapper.setProps({
			bookingDetail: {
				...bookingDetail,
				room: { ...bookingDetail.room, gender: 0 }
			}
		});
		expect(wrapper.find('.property-gender').text()).toBe('Campur');
	});

	it('should set is discount applied properly', async () => {
		expect(wrapper.vm.isDiscountApplied).toBe(false);

		await wrapper.setProps({
			bookingDetail: {
				...bookingDetail,
				is_flash_sale: true,
				flash_sale: {
					discount: '10%'
				}
			}
		});
		expect(wrapper.vm.isDiscountApplied).toBe(true);
	});
});
