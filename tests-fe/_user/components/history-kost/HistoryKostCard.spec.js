import { shallowMount, createLocalVue, RouterLinkStub } from '@vue/test-utils';
import HistoryKostCard from 'Js/_user/components/history-kost/HistoryKostCard.vue';
import dayjs from 'dayjs';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import historyKostList from './__mocks__/history-kost-list.json';

const localVue = createLocalVue();
mockVLazy(localVue);
localVue.prototype.$dayjs = dayjs;

const historyKost = historyKostList[0];

describe('HistoryKostCard.vue', () => {
	const wrapper = shallowMount(HistoryKostCard, {
		localVue,
		propsData: {
			roomData: historyKost.room,
			contractId: historyKost.id,
			startDate: historyKost.start_date,
			endDate: historyKost.end_date,
			reviewData: historyKost.review
		},
		stubs: {
			RouterLink: RouterLinkStub
		}
	});

	it('should render history kost card', () => {
		expect(wrapper.find('.history-kost-card').exists()).toBe(true);
	});

	it('should return correct formatted room detail', () => {
		expect(wrapper.vm.formattedRoomDetail).toEqual({
			...historyKost.room,
			'room-title': historyKost.room.name,
			checker: !!historyKost.room.checker
		});
	});

	it('should return empty object if review data is null', async () => {
		await wrapper.setProps({ reviewData: null });
		expect(wrapper.vm.reviewItemScore).toEqual({});
	});

	it('should return correct review data', async () => {
		await wrapper.setProps({ reviewData: historyKostList[1].review });
		expect(wrapper.vm.reviewItemScore).not.toEqual({});
	});

	it('should render hypen if date is invalid or empty', async () => {
		await wrapper.setProps({
			startDate: ''
		});
		expect(wrapper.find('.start-date').text()).toBe('-');
	});
});
