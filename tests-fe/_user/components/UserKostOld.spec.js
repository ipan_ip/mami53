import { createLocalVue, shallowMount } from '@vue/test-utils';
import mockAxios from 'tests-fe/utils/mock-axios';
import UserKostOld from 'Js/_user/components/UserKostOld.vue';

describe('UserKostOld.vue', () => {
	const localVue = createLocalVue();

	// mock global functions
	global.axios = mockAxios;
	global.bugsnagClient = {
		notify: jest.fn()
	};

	// mock stubs
	const stubs = {
		RouterLink: true
	};

	const mount = () => {
		return shallowMount(UserKostOld, {
			localVue,
			stubs,
			methods: {
				swalError: jest.fn(),
				openSwalLoading: jest.fn(),
				closeSwalLoading: jest.fn()
			}
		});
	};

	describe('render component correctly', () => {
		let wrapper;

		beforeEach(() => {
			wrapper = mount();
		});

		it('should render container', () => {
			expect(wrapper.find('.row.content').exists()).toBeTruthy();
		});
	});

	describe('call functions correctly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = mount();
		});

		it('should set kostData on successful request when calling getUser method', async () => {
			await axios.mockResolve({
				data: {
					status: true,
					kos: {
						test: 'test'
					}
				}
			});
			await wrapper.vm.getUser();
			expect(wrapper.vm.kostData).toEqual({ test: 'test' });
		});

		it('should call swalError if request status is false on successful request when calling getUser method', async () => {
			const swalErrorSpy = jest.spyOn(wrapper.vm, 'swalError');
			await axios.mockResolve({
				data: {
					status: false,
					meta: {
						message: 'test'
					}
				}
			});
			await wrapper.vm.getUser();
			expect(swalErrorSpy).toBeCalled();
			swalErrorSpy.mockRestore();
		});
	});
});
