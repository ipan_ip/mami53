import { createLocalVue, shallowMount } from '@vue/test-utils';
import LastSeenBookingList from 'Js/_user/components/booking/last-seen/LastSeenBookingList.vue';
import draftData from '../__mocks__/draft-data.json';

const localVue = createLocalVue();

const tracker = jest.fn();
global.tracker = tracker;

const $store = {
	state: {
		authData: {
			all: {
				name: 'Mamikos Unit Test',
				gender: 'male'
			}
		}
	}
};

const DraftBookingCard = {
	template: `<div class="draft-booking-card-stub"><button @click="$emit('delete', 2)" class="delete"></button></div>`
};

describe('LastSeenBookingList.vue', () => {
	const wrapper = shallowMount(LastSeenBookingList, {
		localVue,
		stubs: { DraftBookingCard },
		mocks: { $store },
		propsData: { drafts: [draftData] }
	});

	it('should render draft booking list', () => {
		expect(wrapper.find('.draft-last-seen-list').exists()).toBe(true);
	});

	it('should all draft booking card', () => {
		expect(wrapper.findAll('.draft-booking-card-stub').length).toBe(
			wrapper.vm.drafts.length
		);
	});

	it('should emit delete when button delete clicked from card', async () => {
		jest.clearAllMocks();
		const draftBookingCard = wrapper.findAll('.draft-booking-card-stub').at(0);
		draftBookingCard.find('.delete').trigger('click');
		expect(wrapper.emitted('delete', 2));
	});
});
