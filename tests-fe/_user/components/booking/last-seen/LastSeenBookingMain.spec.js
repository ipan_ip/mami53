import { createLocalVue, shallowMount } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import LastSeenBookingMain from 'Js/_user/components/booking/last-seen/LastSeenBookingMain.vue';

const localVue = createLocalVue();
const $store = {
	commit: jest.fn()
};
const tracker = jest.fn();
global.tracker = tracker;

describe('LastSeenBookingMain.vue', () => {
	const wrapper = shallowMount(LastSeenBookingMain, {
		localVue,
		stubs: {
			DraftBookingWrapper: `<div class="draft-booking-wrapper"><button class="visited" @click="$emit('track-page-visited', { listData: [], authData: {}})"></button><button class="content-loaded" @click="$emit('content-loaded')"></button></div>`,
			LastSeenBookingList: `<div class="last-seen-booking-list"></div>`
		},
		mocks: { $store }
	});

	it('should render draft booking wrapper', () => {
		expect(wrapper.find('.draft-booking-wrapper').exists()).toBe(true);
	});

	it('should set notification to 0 and last seen data after content loaded', () => {
		jest.clearAllMocks();
		wrapper.find('.draft-booking-wrapper > .content-loaded').trigger('click');
		expect($store.commit).toHaveBeenNthCalledWith(
			1,
			'setLastSeenNotification',
			0
		);
	});

	it('should call tracker when page visited event triggered', () => {
		jest.clearAllMocks();
		wrapper.find('.draft-booking-wrapper .visited').trigger('click');
		expect(tracker).toBeCalledWith('moe', [
			'[User] Baru Dilihat Page Visited',
			expect.any(Object)
		]);
	});
});
