import { createLocalVue, shallowMount } from '@vue/test-utils';
import DraftBookingList from 'Js/_user/components/booking/draft/DraftBookingList.vue';
import draftData from '../__mocks__/draft-data.json';

const localVue = createLocalVue();

const tracker = jest.fn();
global.tracker = tracker;

const $store = {
	state: {
		authData: {
			all: {
				name: 'Mamikos Unit Test',
				gender: 'male'
			}
		}
	}
};

const DraftBookingCard = {
	template: `<div class="draft-booking-card-stub"><button class="continue-booking" @click="$emit('track', { type: 'continue-booking', data: {}})"></button><button class="continue-booking-expired" @click="$emit('track', { type: 'continue-booking-expired', data: {}})"></button><button @click="$emit('delete', 2)" class="delete"></button></div>`
};

describe('DraftBookingList.vue', () => {
	const wrapper = shallowMount(DraftBookingList, {
		localVue,
		stubs: { DraftBookingCard },
		mocks: { $store },
		propsData: { drafts: [draftData] }
	});

	it('should render draft booking list', () => {
		expect(wrapper.find('.draft-booking-list').exists()).toBe(true);
	});

	it('should all draft booking card', () => {
		expect(wrapper.findAll('.draft-booking-card-stub').length).toBe(
			wrapper.vm.drafts.length
		);
	});

	it('should call tracker continue booking if track event triggered with continue booking', async () => {
		jest.clearAllMocks();
		const draftBookingCard = wrapper.findAll('.draft-booking-card-stub').at(0);
		draftBookingCard.find('.continue-booking').trigger('click');
		expect(tracker).toBeCalledWith('moe', [
			'[User] "Lanjutkan Booking" clicked',
			expect.any(Object)
		]);
	});

	it('should call tracker continue booking expired if track event triggered with continue booking expired', async () => {
		jest.clearAllMocks();
		const draftBookingCard = wrapper.findAll('.draft-booking-card-stub').at(0);
		draftBookingCard.find('.continue-booking-expired').trigger('click');
		expect(tracker).toBeCalledWith('moe', [
			'[User] "Booking Ulang" Clicked',
			expect.any(Object)
		]);
	});

	it('should emit delete when button delete clicked from card', async () => {
		jest.clearAllMocks();
		const draftBookingCard = wrapper.findAll('.draft-booking-card-stub').at(0);
		draftBookingCard.find('.delete').trigger('click');
		expect(wrapper.emitted('delete', 2));
	});
});
