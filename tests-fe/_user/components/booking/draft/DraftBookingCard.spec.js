import { createLocalVue, shallowMount } from '@vue/test-utils';
import DraftBookingCard from 'Js/_user/components/booking/draft/DraftBookingCard.vue';
import dayjs from 'dayjs';
import 'dayjs/locale/id';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import draftData from '../__mocks__/draft-data.json';

const localVue = createLocalVue();
localVue.prototype.$dayjs = dayjs;
mockVLazy(localVue);
mockWindowProperty('open', jest.fn());

const draft = draftData;

const mountComponent = (options = {}) => {
	return shallowMount(DraftBookingCard, {
		localVue,
		propsData: {
			draftRoom: draft.room,
			draftInfo: draft,
			lastSeen: false,
			redirectionSource: '',
			...options
		}
	});
};

describe('DraftBookingCard.vue', () => {
	it('should emit delete selected draft', () => {
		const wrapper = mountComponent();

		expect(wrapper.find('.btn-default').exists()).toBe(true);
		wrapper.find('.btn-default').trigger('click');
		expect(wrapper.emitted('delete')).toBeTruthy();
	});

	it('should return checkin date is not existed', () => {
		const wrapper = shallowMount(DraftBookingCard, {
			localVue,
			propsData: {
				draftRoom: draft.room,
				draftInfo: { ...draft, checkin: null }
			}
		});

		const checkinLabel = wrapper.findAll('.info-item span').at(3);
		expect(checkinLabel.text()).toBe('Belum terisi');
	});

	it('should open room detail in new tab with redirection source draft booking', () => {
		const wrapper = mountComponent({ redirectionSource: 'draft booking' });
		wrapper.find('.info-detail').trigger('click');
		expect(window.open).toBeCalledWith(
			'/room/kost-jayapura-kost-putri-murah-kost-garden-abepura-?redirection_source=draft%20booking',
			'_blank'
		);
	});

	it('should open room detail in new tab with redirection source baru dilihat', () => {
		const wrapper = mountComponent({ redirectionSource: 'baru dilihat' });
		wrapper.find('.info-detail').trigger('click');
		expect(window.open).toBeCalledWith(
			'/room/kost-jayapura-kost-putri-murah-kost-garden-abepura-?redirection_source=baru%20dilihat',
			'_blank'
		);
	});

	it('should show book again when checkin date is expired', async () => {
		const wrapper = mountComponent({
			draftInfo: { ...draft, checkin: '2016-01-01' }
		});
		const bookingButton = wrapper.find('button span');
		expect(bookingButton.text()).toBe('Booking Ulang');
	});

	it('should show "booking langsung" if it is last seen', async () => {
		const wrapper = mountComponent({ isLastSeen: true });
		const bookingButton = wrapper.find('button span');
		expect(bookingButton.text()).toBe('Booking Langsung');
	});

	it('should book the room again', () => {
		const wrapper = mountComponent();
		wrapper
			.findAll('button')
			.at(1)
			.trigger('click');

		expect(window.open).toBeCalled();
	});

	it('should emit correct tracker type if it is last seen', async () => {
		const wrapper = mountComponent({ isLastSeen: true });
		jest.clearAllMocks();
		wrapper.vm.setPrimaryActionTracker();
		expect(wrapper.emitted('track')[0][0]).toEqual(
			expect.objectContaining({ data: expect.any(Object), type: 'last-seen' })
		);
	});

	it('should emit correct tracker type if it is expired', async () => {
		const wrapper = mountComponent({
			draftInfo: { ...draft, checkin: '2016-01-01' }
		});
		jest.clearAllMocks();
		wrapper.vm.setPrimaryActionTracker();
		expect(wrapper.emitted('track')[0][0]).toEqual(
			expect.objectContaining({
				data: expect.any(Object),
				type: 'continue-booking-expired'
			})
		);
	});

	it('should emit correct tracker type if it is not expired', async () => {
		const wrapper = mountComponent();
		jest.clearAllMocks();
		wrapper.vm.setPrimaryActionTracker();
		expect(wrapper.emitted('track')[0][0]).toEqual(
			expect.objectContaining({
				data: expect.any(Object),
				type: 'continue-booking'
			})
		);
	});
});
