import { createLocalVue, shallowMount } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import DraftBookingWrapper from 'Js/_user/components/booking/draft/DraftBookingWrapper.vue';
import Vuex from 'vuex';

const localVue = createLocalVue();
localVue.use(Vuex);

const draftData = {
	data: {
		status: true,
		data: {
			data: ['room1', 'room2'],
			current_page: 1,
			from: 1,
			last_page: 1,
			next_page_url: null,
			per_page: 5,
			prev_page_url: null,
			to: 5,
			total: 5,
			has_more: false
		}
	}
};

const mockStore = {
	state: {
		authData: {}
	}
};

const propsData = {
	endpoint: '/endpoint',
	sortOptions: [
		{ name: 'Tanggal Masuk Terdekat', val: 'ASC' },
		{ name: 'Tanggal Masuk Terjauh', val: 'DESC' }
	]
};

const bugsnagClient = {
	notify: jest.fn()
};
global.bugsnagClient = bugsnagClient;
global.tracker = jest.fn();

const swalError = jest.fn();
const swalSuccess = jest.fn((a, b, cb) => {
	cb();
});
const openSwalLoading = jest.fn();
const closeSwalLoading = jest.fn();
const $toasted = { show: jest.fn() };

const axios = {
	get: jest.fn().mockResolvedValue({
		data: { data: draftData, status: true }
	})
};

global.axios = axios;

const mountOptions = {
	localVue,
	propsData,
	methods: { swalError, swalSuccess, openSwalLoading, closeSwalLoading },
	mocks: { $toasted },
	stubs: { DetachedPagination: '<div></div>' },
	store: new Vuex.Store(mockStore)
};

const mountComponent = options => {
	return shallowMount(DraftBookingWrapper, { ...mountOptions, ...options });
};

describe('DraftBookingWrapper.vue', () => {
	it('should render draft booking wrapper', () => {
		const wrapper = mountComponent();
		expect(wrapper.find('#draftBookingWrapper').exists()).toBe(true);
	});

	it('should emit content loaded and track when data loaded', async () => {
		const wrapper = mountComponent();
		await wrapper.vm.getDraftList(true);
		await wrapper.vm.$nextTick();
		expect(wrapper.emitted('content-loaded')[0]).toBeTruthy();
		expect(wrapper.emitted('track-page-visited')[0][0]).toEqual(
			expect.any(Object)
		);
	});

	it('should open modal sort', async () => {
		const wrapper = mountComponent();
		await wrapper.vm.getDraftList();
		await wrapper.vm.$nextTick();
		wrapper.find('.draft-booking-sort').trigger('click');
		expect(wrapper.vm.isShowModalSort).toBe(true);
	});

	it('should close modal sort', () => {
		const wrapper = mountComponent();
		wrapper.vm.closeModalSort();
		expect(wrapper.vm.isShowModalSort).toBe(false);
	});

	it('should assign selected sort value', () => {
		const wrapper = mountComponent();
		wrapper.vm.selectSort({
			name: 'Tanggal Masuk Terjauh',
			val: 'DESC'
		});
		expect(wrapper.vm.sortVal.name).toBe('Tanggal Masuk Terjauh');
		expect(wrapper.vm.sortVal.val).toBe('DESC');
	});

	it('should open modal delete confirmation', () => {
		const wrapper = mountComponent();
		wrapper.vm.openModalDelete(123);
		expect(wrapper.vm.isShowModalDelete).toBe(true);
		expect(wrapper.vm.idSelected).toBe(123);
	});

	it('should close modal delete confirmation', () => {
		const wrapper = mountComponent();
		wrapper.vm.closeModalDelete();
		expect(wrapper.vm.isShowModalDelete).toBe(false);
	});

	it('should change the page', () => {
		const getDraftList = jest.fn();
		const wrapper = mountComponent({
			methods: { ...mountOptions.methods, getDraftList }
		});

		wrapper.vm.pageChanged(2);
		expect(wrapper.vm.draftPage).toBe(2);
		expect(getDraftList).toBeCalled();
	});

	it('should show swal error popup when get api return false', async () => {
		const axios = {
			get: jest.fn().mockResolvedValue({
				data: {
					status: false,
					data: { data: [] },
					meta: { message: 'Gagal mengambil data' }
				}
			})
		};
		global.axios = axios;

		const wrapper = mountComponent();
		wrapper.vm.getDraftList();
		await wrapper.vm.$nextTick();
		expect(swalError).toBeCalledWith(null, 'Gagal mengambil data');
	});

	it('should throw error when get api catch error', async () => {
		const axios = {
			get: jest.fn().mockRejectedValue()
		};
		global.axios = axios;

		const wrapper = mountComponent();
		wrapper.vm.getDraftList();
		await wrapper.vm.$nextTick();
		expect(swalError).toBeCalled();
	});

	it('should delete selected draft', async () => {
		const axios = {
			delete: jest.fn().mockResolvedValue({
				data: { status: true, meta: { message: 'Berhasil' } }
			})
		};
		global.axios = axios;

		const wrapper = mountComponent();
		wrapper.vm.idSelected = 123;
		wrapper.vm.deleteDraftBooking();
		await wrapper.vm.$nextTick();
		expect($toasted.show).toBeCalled();
	});

	it('should show swal error popup when delete return status false', async () => {
		const axios = {
			delete: jest
				.fn()
				.mockResolvedValue({ data: { status: false, meta: 'Gagal' } })
		};
		global.axios = axios;

		const wrapper = mountComponent();
		wrapper.vm.idSelected = 123;
		wrapper.vm.deleteDraftBooking();
		await wrapper.vm.$nextTick();
		expect(swalError).toBeCalled();
	});

	it('should throw error when delete catch error', async () => {
		const axios = {
			delete: jest.fn().mockRejectedValue()
		};
		global.axios = axios;

		const wrapper = mountComponent();

		wrapper.vm.idSelected = 123;
		wrapper.vm.deleteDraftBooking();
		await wrapper.vm.$nextTick();
		expect(swalError).toBeCalled();
		expect(bugsnagClient.notify).toBeCalled();
	});
});
