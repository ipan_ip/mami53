import { shallowMount } from '@vue/test-utils';
import DraftBookingModalDelete from 'Js/_user/components/booking/draft/DraftBookingModalDelete.vue';

describe('DraftBookingModalDelete.vue', () => {
	const wrapper = shallowMount(DraftBookingModalDelete, {
		propsData: { showModal: true }
	});

	it('should render delete draft booking modal', () => {
		expect(wrapper.find('#draftBookingModalDelete').exists()).toBe(true);
	});

	it('should render cancel and delete buttons', () => {
		const buttons = wrapper.findAll('button');
		expect(buttons.length).toBe(2);
	});

	it('should cancel delete and close the modal', () => {
		wrapper.find('.btn-success').trigger('click');
		expect(wrapper.emitted('close')).toBeTruthy();
	});

	it('should delete draft booking', () => {
		wrapper.find('.btn-primary').trigger('click');
		expect(wrapper.emitted('delete')).toBeTruthy();
	});
});
