import { createLocalVue, shallowMount } from '@vue/test-utils';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import BookingListMenu from 'Js/_user/components/booking/history/BookingListMenu.vue';

const localVue = createLocalVue();
mockVLazy(localVue);

describe('BookingListMenu', () => {
	it('should mount vue component', () => {
		const wrapper = shallowMount(BookingListMenu, { localVue });

		expect(wrapper.element).toMatchSnapshot();
	});

	it('should match status props', () => {
		const wrapper = shallowMount(BookingListMenu, {
			localVue,
			propsData: { status: 'booked' }
		});

		const unorderedListSelected = wrapper.findAll('ul li');
		expect(unorderedListSelected.length).toBe(3);
	});

	it('should match id number props', () => {
		const wrapper = shallowMount(BookingListMenu, {
			localVue,
			propsData: { id: 123, status: 'verified' }
		});

		const anchorSelected = wrapper.findAll('li a').at(1);
		expect(anchorSelected.element.getAttribute('href')).toBe(
			'/booking-detail/?id=123'
		);
	});

	it('should match link props', () => {
		const wrapper = shallowMount(BookingListMenu, {
			localVue,
			propsData: { status: 'verified', link: '/some-link' }
		});

		const anchorSelected = wrapper.findAll('li a').at(0);
		expect(anchorSelected.element.getAttribute('href')).toBe('/some-link');
	});
});
