import { shallowMount, createLocalVue } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import BookingDetailCardContractInfo from 'Js/_user/components/booking/history/BookingDetailCardContractInfo';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

const mixinNavigator = require('Js/@mixins/MixinNavigatorIsMobile');
const localVue = createLocalVue();
mockVLazy(localVue);
localVue.mixin(mixinNavigator);

describe('BookingDetailCardContractInfo.vue', () => {
	const propsData = {
		bookingDetail: {
			booking_code: 'MB12345678XX',
			duration_string: '2 Bulan'
		},
		rentType: 'Bulan',
		checkinDate: '12 Aug 2020',
		checkoutDate: '12 Sep 2020'
	};

	mockWindowProperty('window.innerWidth', 992);
	const wrapper = shallowMount(BookingDetailCardContractInfo, {
		localVue,
		propsData
	});

	it('should render the component', () => {
		expect(wrapper.find('.detail-booking').exists()).toBe(true);
	});

	it('should open detail booking', () => {
		wrapper.vm.isOpenDetailBooking = false;
		wrapper.vm.navigator.isMobile = false;
		wrapper.vm.toggleDetailBooking();
		expect(wrapper.vm.isOpenDetailBooking).toBe(true);
	});

	it('should render correct booking code', () => {
		const bookingCode = wrapper.findAll('.booking-item span b').at(0);
		expect(bookingCode.text()).toContain('MB12345678XX');
	});

	it('should render correct checkin date and checkout date', () => {
		const checkinDate = wrapper.findAll('.booking-item span b').at(1);
		const checkoutDate = wrapper.findAll('.booking-item span b').at(2);

		expect(checkinDate.text()).toContain('12 Aug 2020');
		expect(checkoutDate.text()).toContain('12 Sep 2020');
	});

	it('should render correct booking duration and rent type', () => {
		const bookingDuration = wrapper.findAll('.booking-item span b').at(3);
		const bookingRentType = wrapper.findAll('.booking-item span b').at(4);

		expect(bookingDuration.text().toLowerCase()).toContain('2 bulan');
		expect(bookingRentType.text().toLowerCase()).toContain('bulanan');
	});
});
