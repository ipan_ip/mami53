import { shallowMount, createLocalVue } from '@vue/test-utils';
import BookingListMain from 'Js/_user/components/booking/history/BookingListMain';
import Vuex from 'vuex';
import dayjs from 'dayjs';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.prototype.$dayjs = dayjs;
mockWindowProperty('open', jest.fn());

jest.mock('Js/_user/event-bus/event-bus', () => {
	const EventBus = {
		$emit: jest.fn(),
		$on: jest.fn()
	};

	global.EventBus = EventBus;

	return global.EventBus;
});

const bugsnagClient = {
	notify: jest.fn()
};
global.bugsnagClient = bugsnagClient;

const axios = {
	get: jest.fn().mockResolvedValue(),
	post: jest.fn().mockResolvedValue()
};
global.axios = axios;

let mockStore = {
	state: {
		userMenuActive: 'user-menu'
	},
	mutations: {
		setMenu(state, payload) {
			state.userMenuActive = payload;
		}
	}
};

describe('BookingListMain', () => {
	let store = new Vuex.Store(mockStore);
	const swalError = jest.fn();
	const closeSwalLoading = jest.fn();

	const generateWrapper = ({ methodsProp = {} } = {}) => {
		const mountProp = {
			localVue,
			store,
			methods: { swalError, closeSwalLoading, ...methodsProp }
		};
		return shallowMount(BookingListMain, mountProp);
	};

	describe('fetch detail room', () => {
		it('should get detail room', async () => {
			const wrapper = generateWrapper();

			axios.get = jest.fn().mockResolvedValue({
				data: { status: true, story: {} }
			});

			wrapper.vm.roomId = 1234;
			await wrapper.vm.getRoomDetail(wrapper.vm.roomId);
			expect(axios.get).toBeCalledWith('/stories/1234');
			expect(wrapper.vm.isAddReview).toBe(true);
		});

		it('should send error report to bugsnag', async () => {
			const wrapper = generateWrapper();

			axios.get = jest.fn().mockRejectedValue();

			wrapper.vm.roomId = 1234;
			await wrapper.vm.getRoomDetail(wrapper.vm.roomId);
			expect(bugsnagClient.notify).toBeCalled();
		});
	});

	describe('fetch review data', () => {
		const reviewData = [
			{
				clean: 4,
				safe: 4,
				happy: 4,
				room_facilities: 4,
				public_facilities: 4,
				pricing: 4,
				content: 'nice kost',
				is_anonim: true,
				photo: [],
				is_me: true
			}
		];

		it('should get review data', async () => {
			const wrapper = generateWrapper();

			axios.get = jest.fn().mockResolvedValue({
				data: { review: reviewData }
			});

			wrapper.vm.roomId = 1234;
			await wrapper.vm.getReviewData(wrapper.vm.roomId);
			expect(axios.get).toBeCalledWith('/stories/review/1234');
			expect(wrapper.vm.isReview).toBe(true);
		});

		it('send error report to bugsnag', async () => {
			const wrapper = generateWrapper();

			axios.get = jest.fn().mockRejectedValue();

			wrapper.vm.roomId = 1234;
			await wrapper.vm.getReviewData(wrapper.vm.roomId);
			expect(bugsnagClient.notify).toBeCalled();
		});
	});

	it('should close add review form', () => {
		const wrapper = generateWrapper();
		wrapper.vm.closeAddReview();
		expect(wrapper.vm.isAddReview).toBe(false);
	});

	it('should go to kost saya page', () => {
		const wrapper = generateWrapper();
		wrapper.vm.goToMyKost();
		expect(window.open).toBeCalled();
	});

	it('should set invoice settlement with invoice url', () => {
		const wrapper = generateWrapper();
		const invoiceUrl = 'https://mamikos.com';
		wrapper.vm.setInvoiceSettlement(invoiceUrl);
		expect(wrapper.vm.settlementInvoceUrl).toBe('https://mamikos.com');
	});

	it('should go to invoice page', () => {
		const wrapper = generateWrapper();
		wrapper.vm.goInvoiceSettlement();
		expect(window.open).toBeCalled();
		expect(wrapper.vm.isModalSettlementConfirm).toBe(false);
	});

	describe('send checkin', () => {
		it('should send checkin process by user', async () => {
			const wrapper = generateWrapper();

			axios.post = jest.fn().mockResolvedValue({
				data: { status: true }
			});

			wrapper.vm.bookingIdCheckin = 1234;
			await wrapper.vm.goCheckin();
			expect(axios.post).toBeCalled();
			expect(wrapper.vm.isModalAcceptCheckin).toBe(false);
		});

		it('should show error popup when status received is false', async () => {
			const wrapper = generateWrapper();

			axios.post = jest.fn().mockResolvedValue({
				data: { status: false, meta: { message: 'error' } }
			});

			wrapper.vm.bookingIdCheckin = 1234;
			await wrapper.vm.goCheckin();
			expect(swalError).toBeCalled();
		});

		it('should send error message to bugsnag', async () => {
			const wrapper = generateWrapper();

			axios.post = jest.fn().mockRejectedValue();
			wrapper.vm.bookingIdCheckin = 1234;
			await wrapper.vm.goCheckin();
			expect(bugsnagClient.notify).toBeCalled();
		});
	});

	describe('fetch booking list', () => {
		it('should get booking list data', async () => {
			const openTenantBookingChat = jest.fn();
			const wrapper = generateWrapper({
				methodsProp: { openTenantBookingChat }
			});

			axios.get = jest.fn().mockResolvedValue({
				data: { status: true, data: { total: 321, per_page: 5, data: [] } }
			});

			await wrapper.vm.getBookingList(true);
			expect(axios.post).toBeCalled();
			expect(openTenantBookingChat).toBeCalled();
		});
	});

	it('should set filter changes', () => {
		const getBookingList = jest.fn();
		const wrapper = generateWrapper({ methodsProp: { getBookingList } });

		wrapper.vm.setFilter();
		expect(wrapper.vm.bookingPagination.page).toBe(1);
		expect(wrapper.vm.isFilterSet).toBe(true);
		expect(getBookingList).toBeCalled();
	});

	it('should set filter status on mobile', () => {
		const setFilter = jest.fn();
		const wrapper = generateWrapper({ methodsProp: { setFilter } });
		wrapper.vm.setFilterMobile('filter set');
		expect(wrapper.vm.statusValue).toBe('filter set');
		expect(wrapper.vm.isModalFilter).toBe(false);
		expect(setFilter).toBeCalled();
	});

	it('should set filter sort on mobile', () => {
		const setFilter = jest.fn();
		const wrapper = generateWrapper({ methodsProp: { setFilter } });
		wrapper.vm.setSortMobile('sort set');
		expect(wrapper.vm.sortValue).toBe('sort set');
		expect(wrapper.vm.isModalSort).toBe(false);
		expect(setFilter).toBeCalled();
	});

	it('should assign new page value', () => {
		const getBookingList = jest.fn();
		const wrapper = generateWrapper({ methodsProp: { getBookingList } });
		wrapper.vm.pageChanged(10);
		expect(wrapper.vm.bookingPagination.page).toBe(10);
		expect(getBookingList).toBeCalled();
	});

	it('should show modal filter and sort', () => {
		const wrapper = generateWrapper();
		wrapper.vm.showModalFilter(true);
		expect(wrapper.vm.isModalFilter).toBe(true);
		wrapper.vm.showModalSort(true);
		expect(wrapper.vm.isModalSort).toBe(true);
	});
});
