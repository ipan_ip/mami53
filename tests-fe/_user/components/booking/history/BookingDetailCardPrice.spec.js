import { shallowMount, createLocalVue } from '@vue/test-utils';
import BookingDetailCardPrice from 'Js/_user/components/booking/history/BookingDetailCardPrice';

const localVue = createLocalVue();

describe('BookingDetailCardPrice.vue', () => {
	const propsData = {
		isDiscountApplied: true,
		bookingDetail: {
			flash_sale: {
				percentage: '15%',
				rent_price: 700000,
				price_after_discount: 500000
			}
		},
		rentCount: {
			rentType: 'Bulan',
			rentPrice: 700000
		},
		isDefaultFormat: false
	};

	const wrapper = shallowMount(BookingDetailCardPrice, { localVue, propsData });

	it('should render the component', () => {
		expect(wrapper.find('.detail-price-wrapper').exists()).toBe(true);
	});

	it.skip('should render correct format for price', () => {
		const priceFormat = wrapper.findAll('.detail-price-wrapper span').at(2);
		const renderedPrice = priceFormat.text().toLowerCase();
		expect(renderedPrice).toContain('per bulan');
		expect(renderedPrice).toContain('rp500,000');
	});
});
