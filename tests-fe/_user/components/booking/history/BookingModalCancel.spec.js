import { shallowMount } from '@vue/test-utils';
import Vue from 'vue';
import mockSwalLoading from 'tests-fe/utils/mock-swal-loading';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import BookingModalCancel from 'Js/_user/components/booking/history/BookingModalCancel.vue';

window.Vue = require('vue');
mockSwalLoading();
const swalError = jest.fn();

const bugsnagClient = {
	notify: jest.fn()
};
global.bugsnagClient = bugsnagClient;

let axios = {
	get: jest.fn().mockResolvedValue({
		data: { data: { data: { room: {} } }, status: true }
	}),
	post: jest.fn().mockResolvedValue({})
};
global.axios = axios;

describe('BookingModalCancel', () => {
	const generateWrapper = ({ methodsProp = {} } = {}) => {
		const shallowMountProp = {
			propsData: {
				showModal: true,
				bookingCode: 'MB127367484'
			},
			methods: {
				...methodsProp
			}
		};
		return shallowMount(BookingModalCancel, shallowMountProp);
	};

	it('should mount vue component', () => {
		const wrapper = generateWrapper();
		expect(wrapper.element).toMatchSnapshot();
	});

	it('should emit close action', () => {
		const wrapper = generateWrapper();

		wrapper.find('.btn-cancel-no').trigger('click');
		expect(wrapper.emitted().close).toBeTruthy();
	});

	it('should send booking cancelation', async () => {
		axios.post = jest.fn().mockResolvedValue({
			data: { status: true }
		});
		global.axios = axios;

		const wrapper = generateWrapper();

		wrapper.vm.cancelReason = 'Tidak jadi ngekos';
		wrapper.find('.btn-cancel-yes').trigger('click');

		await wrapper.vm.sendCancelBooking(
			wrapper.vm.cancelReason,
			wrapper.vm.bookingCode
		);
		expect(wrapper.emitted().success).toBeTruthy();
	});

	it('should show modal error when send booking return false', async done => {
		const wrapper = generateWrapper({ methodsProp: { swalError } });

		axios.post = jest.fn().mockResolvedValue({ data: { status: false } });
		global.axios = axios;

		wrapper.vm.cancelReason = 'Tidak jadi ngekos';
		wrapper.find('.btn-cancel-yes').trigger('click');

		await wrapper.vm.sendCancelBooking(
			wrapper.vm.cancelReason,
			wrapper.vm.bookingCode
		);

		wrapper.vm.$nextTick(() => {
			expect(swalError).toBeCalled();
			done();
		});
	});

	it('should show modal error when get reject reason return false', async done => {
		const wrapper = generateWrapper({ methodsProp: { swalError } });

		axios.get = jest.fn().mockResolvedValue({
			data: {
				status: false,
				meta: { message: 'Gagal mengambil alasan penolakan' }
			}
		});
		global.axios = axios;

		await wrapper.vm.getCancelReasons();

		wrapper.vm.$nextTick(() => {
			expect(swalError).toBeCalled();
			done();
		});
	});

	it('should show modal error when get reject reason failed', async done => {
		const wrapper = generateWrapper({ methodsProp: { swalError } });

		axios.get = jest.fn().mockRejectedValue();
		global.axios = axios;

		await wrapper.vm.getCancelReasons();

		wrapper.vm.$nextTick(() => {
			expect(swalError).toBeCalled();
			done();
		});
	});

	it('should check other reason if too long', done => {
		const wrapper = generateWrapper();

		wrapper.vm.cancelReasonOther =
			'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.';

		wrapper.vm.$nextTick(() => {
			expect(wrapper.vm.isWarningText).toBe(true);
			done();
		});
	});

	it('should toggle auto scroll', async () => {
		const toggleScroll = jest.fn();

		const wrapper = generateWrapper({ methodsProp: { toggleScroll } });

		wrapper.vm.isOtherReason = true;
		await wrapper.vm.$nextTick();
		wrapper.find('textarea').trigger('focus');
		expect(toggleScroll).toBeCalled();
		wrapper.vm.toggleScroll();
	});

	it('should close modal', () => {
		const wrapper = generateWrapper();

		wrapper.vm.closeModal();
		expect(wrapper.emitted().close).toBeTruthy();
	});
});
