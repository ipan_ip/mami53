import { shallowMount } from '@vue/test-utils';
import BookingTime from 'Js/_user/components/booking/history/BookingTime.vue';

describe('BookingTime', () => {
	it('should mount vue component', () => {
		const wrapper = shallowMount(BookingTime, {
			propsData: { checkin: '2020-09-09', duration: '1 Bulan' }
		});

		expect(wrapper.element).toMatchSnapshot();
	});

	it('should show checkin date based from props data', () => {
		const wrapper = shallowMount(BookingTime, {
			propsData: { checkin: '2020-09-09' }
		});

		const checkinDateEl = wrapper.findAll('.title-value').at(0);
		expect(checkinDateEl.text()).toBe('9 Sep 2020');
	});

	it('should show duration based from props data', () => {
		const duration = '1 Bulan';
		const wrapper = shallowMount(BookingTime, {
			propsData: { duration: duration }
		});

		const durationEl = wrapper.findAll('.title-value').at(1);
		expect(durationEl.text()).toBe(duration);
	});
});
