import { shallowMount, createLocalVue } from '@vue/test-utils';
import BookingModalCancelSuccess from 'Js/_user/components/booking/history/BookingModalCancelSuccess';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

const localVue = createLocalVue();
mockVLazy(localVue);
mockWindowProperty('open', jest.fn());

describe('BookingModalCancelSuccess.vue', () => {
	const wrapper = shallowMount(BookingModalCancelSuccess, {
		localVue,
		propsData: { showModal: true }
	});

	it('should render the component', () => {
		expect(wrapper.find('#bookingContractModalCancel'));
	});

	it('should open the booking page when close modal', () => {
		wrapper.vm.closeModal();
		expect(window.open).toBeCalledWith('/user/booking', '_self');
	});
});
