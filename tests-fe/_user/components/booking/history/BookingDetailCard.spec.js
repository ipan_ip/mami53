import { shallowMount, createLocalVue } from '@vue/test-utils';
import BookingDetailCard from 'Js/_user/components/booking/history/BookingDetailCard.vue';
import userBookingList from '../__mocks__/user-booking-list.json';
import bookingStatusLabel from '../__mocks__/booking-status-label.json';
import mockVLazy from '../../../../utils/mock-v-lazy';

jest.mock('Js/_user/event-bus/event-bus', () => {
	const EventBus = {
		$on: jest.fn(),
		$emit: jest.fn()
	};
	global.EventBus = EventBus;
	return global.EventBus;
});

const localVue = createLocalVue();
mockVLazy(localVue);

describe('BookingDetailCard.vue', () => {
	const bookingDetail = userBookingList[0];
	const isCheckinStatus = false;
	const bookingStatus = bookingStatusLabel.waiting_payment;
	const terminatedDetail = {
		contractId: 12345,
		totalPayment: '2000000',
		hasReviewed: true
	};

	const propsData = {
		bookingDetail,
		bookingStatus,
		isCheckinStatus,
		terminatedDetail
	};

	const mixinOpenChat = {};
	const $dayjs = jest.fn(() => {
		return {
			locale: jest.fn(() => {
				return {
					format: jest.fn()
				};
			})
		};
	});

	const wrapper = shallowMount(BookingDetailCard, {
		localVue,
		propsData,
		methods: {
			$dayjs
		},
		mocks: {
			$router: { push: jest.fn() }
		},
		mixins: [mixinOpenChat]
	});

	it('should render booking card detail', () => {
		expect(wrapper.find('#bookingDetailCard').exists()).toBe(true);
	});

	it('should not render cancel reason if booking not failed', () => {
		expect(wrapper.find('.header-reject-reason').exists()).toBe(false);
	});

	// it('should show correct tenant name', () => {
	// 	expect(wrapper.find('.--name').text()).toBe(bookingDetail.name);
	// });

	it('should render cancel reason if booking failed', async () => {
		wrapper.setProps({ bookingStatus: bookingStatusLabel.cancelled });
		await wrapper.vm.$nextTick();
		expect(wrapper.find('.header-reject-reason').exists()).toBe(true);
	});

	it('should render detail checkin if status is paid', async () => {
		wrapper.setProps({ bookingStatus: bookingStatusLabel.paid });
		await wrapper.vm.$nextTick();
		expect(wrapper.find('.detail-checkin').exists()).toBe(true);
	});

	it('should render chat button if status is paid', async () => {
		const footerButton = wrapper
			.findAll('.booking-card-footer button')
			.filter(button => button.text().toLowerCase() === 'chat pemilik');
		expect(footerButton.exists()).toBe(true);
	});

	it('should render kost saya button if status is paid', async () => {
		const footerButton = wrapper
			.findAll('.booking-card-footer button')
			.filter(button => button.text().toLowerCase() === 'kost saya');
		expect(footerButton.exists()).toBe(false);
	});

	it('should render Bayar sekarang button if status is waiting payment', async () => {
		wrapper.setProps({ bookingStatus: bookingStatusLabel.waiting_payment });
		await wrapper.vm.$nextTick();
		const footerButton = wrapper
			.findAll('.booking-card-footer a')
			.filter(button => button.text().toLowerCase() === 'bayar sekarang');
		expect(footerButton.exists()).toBe(true);
	});

	it('should render Cari Kost Lain button if status is termiated or failed', async () => {
		wrapper.setProps({ bookingStatus: bookingStatusLabel.cancelled });
		await wrapper.vm.$nextTick();
		const footerButton = wrapper
			.findAll('.booking-card-footer a')
			.filter(button => button.text().toLowerCase() === 'cari kost lain');
		expect(footerButton.exists()).toBe(true);
	});

	it('should render cancel button if status is waiting confirmation', async () => {
		wrapper.setProps({
			bookingStatus: bookingStatusLabel.waiting_confirmation
		});
		await wrapper.vm.$nextTick();
		expect(wrapper.find('.detail-cancel').exists()).toBe(true);
	});

	it('should render correct cancel reason label', async () => {
		wrapper.setProps({ bookingStatus: bookingStatusLabel.rejected });
		await wrapper.vm.$nextTick();
		expect(wrapper.find('.header-reject-reason label').text()).toEqual(
			expect.stringContaining('Penolakan')
		);

		wrapper.setProps({ bookingStatus: bookingStatusLabel.expired });
		await wrapper.vm.$nextTick();
		expect(wrapper.find('.header-reject-reason label').text()).toEqual(
			expect.stringContaining('Expired')
		);
	});

	it('should toggle collapse if detail collapse button clicked', () => {
		wrapper.find('.detail-collapse').trigger('click');
		expect(wrapper.emitted('toggle')[0]).toBeTruthy();
	});

	describe('methods testing', () => {
		it('should emit give review action', () => {
			wrapper.vm.giveReview();
			expect(EventBus.$emit).toBeCalled();
		});

		it('should push the route to transaction history page', () => {
			wrapper.vm.goHistoryTransaction();
			expect(wrapper.vm.$router.push).toBeCalledWith(
				`/riwayat-transaksi?contract=12345`
			);
		});

		// it('should toggle detail booking', () => {
		// 	wrapper.vm.isOpenDetailBooking = false;
		// 	wrapper.vm.toggleDetailBooking();
		// 	expect(wrapper.vm.isOpenDetailBooking).toBe(true);
		// });

		it('should emit toggle panel', () => {
			wrapper.vm.togglePanel();
			expect(wrapper.emitted('toggle')).toBeTruthy();
		});

		it('should emit open checkin', () => {
			wrapper.vm.openCheckin();
			expect(wrapper.emitted('open-checkin')).toBeTruthy();
		});
	});

	describe('computed properties', () => {
		// it('should show total payment statement', () => {
		// 	expect(
		// 		wrapper.vm.totalPayment
		// 			.toLowerCase()
		// 			.includes('selama 1 bulan menyewa kamu telah')
		// 	).toBe(true);
		// });

		// it('should show total discount statement', () => {
		// 	expect(wrapper.vm.totalDiscount.toLowerCase().includes('hemat rp.')).toBe(
		// 		true
		// 	);
		// });

		it('should show correct star icon', () => {
			expect(wrapper.vm.reviewIcon.src).toBe(
				'/general/img/icons/ic_star-mamipay_s.svg'
			);
		});

		it('should show correct review label', () => {
			expect(wrapper.vm.reviewInstruction.toLowerCase()).toBe('sudah direview');
		});

		it('should show correct payment status', () => {
			expect(wrapper.vm.isFullyPaid).toBe(false);
		});
	});
});
