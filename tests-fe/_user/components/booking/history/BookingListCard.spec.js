import { shallowMount, createLocalVue } from '@vue/test-utils';
import BookingListCard from 'Js/_user/components/booking/history/BookingListCard.vue';
import userBookingList from '../__mocks__/user-booking-list.json';
import bookingStatusLabel from '../__mocks__/booking-status-label.json';
import rentCountType from '../__mocks__/rent-count-type.json';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

jest.mock('Js/_user/event-bus/event-bus', () => {
	const EventBus = {
		$on: jest.fn(),
		$emit: jest.fn()
	};
	global.EventBus = EventBus;
	return global.EventBus;
});

const localVue = createLocalVue();

const bookingDetail = userBookingList[0];
const propsData = { bookingDetail };

const $store = {
	state: {
		authData: {
			all: {
				id: '1'
			}
		}
	}
};

const $dayjs = jest.fn(() => {
	return {
		locale: jest.fn(() => {
			return {
				format: jest.fn()
			};
		}),
		format: jest.fn()
	};
});

const tracker = jest.fn();
const $router = { push: jest.fn() };
const axiosSuccessValue = {
	data: {
		status: true,
		data: {
			booking_data: {
				checkin: '2020-01-01',
				checkout: '2020-02-02',
				expired_date: '2020-01-01',
				schedule_date: '2020-01-01'
			}
		}
	}
};

global.axios = { get: jest.fn().mockResolvedValue(axiosSuccessValue) };
global.bugsnagClient = { notify: jest.fn() };
global.open = jest.fn();
global.tracker = tracker;

const swalError = jest.fn((a, b, cb) => {
	if (typeof cb === 'function') {
		cb();
	}
});

mockWindowProperty(
	'addEventListener',
	jest.fn((_, fn) => {
		fn();
	})
);
mockWindowProperty('open', jest.fn());
mockWindowProperty('removeEventListener', jest.fn());

let wrapper = Function;

const trackMyKostViewed = jest.fn();
const trackPayBooking = jest.fn();

const shallowMountComponent = () => {
	return shallowMount(BookingListCard, {
		localVue,
		propsData,
		methods: { $dayjs, swalError, trackMyKostViewed, trackPayBooking },
		mocks: { $store, $router: { push: jest.fn() } },
		stubs: {
			BookingDetailCard: {
				template: '<div class="booking-detail-card"></div>'
			},
			BookingDetailCardMobile: {
				template: '<div class="booking-detail-card-mobile"></div>'
			}
		}
	});
};

describe('BookingListCard.vue', () => {
	afterEach(() => {
		wrapper.destroy();
	});

	it('should render booking card', () => {
		wrapper = shallowMountComponent();
		expect(wrapper.find('.booking-card').exists()).toBe(true);
	});

	it('should set bookingDetailResponse when open detail card', async () => {
		wrapper = shallowMountComponent();
		wrapper.vm.isCardOpen = true;
		await wrapper.vm.$nextTick();
		expect(wrapper.vm.bookingDetailResponse).toEqual(
			axiosSuccessValue.data.data
		);
	});

	it('should set booking status correctly', () => {
		Object.keys(bookingStatusLabel).forEach(key => {
			expect(wrapper.vm.checkBookingStatus(key)).toEqual(
				bookingStatusLabel[key]
			);
		});
	});

	it('should set correct booking class by booking status', () => {
		const wrapper = shallowMountComponent();
		const bookingStatus = [
			{ status: 'waiting_confirmation', className: '--waiting' },
			{ status: 'waiting_payment', className: '--waiting' },
			{ status: 'paid', className: '--paid' },
			{ status: 'expired', className: '--expired' },
			{ status: 'cancel', className: '--failed' }
		];

		bookingStatus.forEach(({ status, className }) => {
			wrapper.setData({
				displayStatus: bookingStatusLabel[status]
			});
			expect(wrapper.vm.classByStatus).toBe(className);
		});
	});

	it('should set correct gender wording', () => {
		const wrapper = shallowMountComponent();
		const genders = ['Campur', 'Putra', 'Putri'];
		genders.forEach(async (gender, index) => {
			const changedGenderProps = Object.assign({}, bookingDetail);
			changedGenderProps.room.gender = index;
			wrapper.setProps({ bookingDetail: changedGenderProps });
			expect(wrapper.vm.roomGender).toBe(gender);
		});
	});

	it('should render countdown correctly', () => {
		const nowDate = 1586930369;
		const limitDate = nowDate + 1 * 60 * 60 + 5 * 60 + 10;
		bookingDetail.payment_expired_date = limitDate;

		const wrapper = shallowMountComponent();

		wrapper.vm.countdownTimer.nowDate = nowDate;
		wrapper.vm.countdownTimer.limitDate = limitDate;

		const { hoursLeft, minutesLeft, secondsLeft } = wrapper.vm;

		expect(hoursLeft).toBe(1);
		expect(minutesLeft).toBe(5);
		expect(secondsLeft).toBe(10);
	});

	it('should show countdown if it is waiting for payment', () => {
		const nowDate = 1586930369;
		const limitDate = nowDate + 1 * 60 * 60 + 5 * 60 + 10;
		bookingDetail.payment_expired_date = limitDate;

		const wrapper = shallowMountComponent();

		wrapper.setData({
			displayStatus: bookingStatusLabel.waiting_payment
		});
		expect(wrapper.vm.showCountdownTimer).toBe(limitDate);

		wrapper.setData({
			displayStatus: bookingStatusLabel.cancel
		});
		expect(wrapper.vm.showCountdownTimer).toBe(false);
	});

	it('should set countdown to 00:00:00 when time is up', async () => {
		const wrapper = shallowMountComponent();
		await wrapper.vm.$nextTick();
		wrapper.setData({
			countdownTimer: {
				nowDate: 0,
				limitDate: 0
			},
			displayStatus: bookingStatusLabel.waiting_payment,
			isCardOpen: true
		});
		await wrapper.vm.$nextTick();
		expect(wrapper.find('.card-header .timer').text()).toBe('00:00:00');
	});

	it('should calculate time every seconds', () => {
		const wrapper = shallowMountComponent();
		const thisMoment = new Date();
		const addSeconds = function(date, seconds) {
			const copy = new Date(Number(date));
			copy.setSeconds(copy.getSeconds() + seconds);
			return Math.trunc(new Date(copy).getTime() / 1000);
		};
		jest.useFakeTimers();
		wrapper.vm.calculateTimer(addSeconds(thisMoment, 8));
		jest.advanceTimersByTime(1000);
		expect(wrapper.vm.countdownTimer.nowDate).not.toBe(0);
	});

	it('should toggle modal when close button clicked', () => {
		const wrapper = shallowMountComponent();
		wrapper.setData({ isModalOpen: true });
		wrapper.find('.modal--close').trigger('click');
		expect(wrapper.vm.isModalOpen).toBe(false);
	});

	it('should emited proccessCheckinEvent if button Check-in kos clicked', async () => {
		const wrapper = shallowMountComponent();
		wrapper.setData({
			displayStatus: bookingStatusLabel.paid,
			isCardOpen: false
		});
		await wrapper.vm.$nextTick();
		const checkinBtn = wrapper
			.findAll('.footer-buttons button')
			.filter(button => button.text().toLowerCase() === 'bayar sekarang')
			.at(0);
		checkinBtn.trigger('click');
		expect(global.EventBus.$emit).toBeCalledWith(
			'proccessCheckinEvent',
			expect.any(Object)
		);
	});

	it('should hide card and send tracking data to moe if watch more button clicked', async () => {
		const wrapper = shallowMountComponent();
		wrapper.setData({
			isCardOpen: false,
			bookingDetailResponse: axiosSuccessValue.data.data
		});
		await wrapper.vm.$nextTick();
		const trackerSpy = jest.spyOn(global, 'tracker');
		const cardToggle = wrapper.find('.card-toggle');
		cardToggle.trigger('click');

		await wrapper.vm.$nextTick();
		expect(wrapper.vm.isCardOpen).toBe(true);
		expect(trackerSpy).toBeCalledWith('moe', expect.any(Array));
		trackerSpy.mockRestore();
	});

	it('should open detail booking if it is mobile when card-body clicked', async () => {
		mockWindowProperty('window.innerWidth', 600);
		const wrapper = shallowMountComponent();
		wrapper.setMethods({ trackBookingDetailViewed: jest.fn });
		const trackerSpy = jest.spyOn(wrapper.vm, 'trackBookingDetailViewed');
		wrapper.find('.card-body').trigger('click');
		expect(wrapper.vm.isModalOpen).toBe(true);
		await wrapper.vm.$nextTick();
		expect(trackerSpy).toBeCalled();
	});

	it('should open detail booking if isMobile return true when card-body clicked', async () => {
		mockWindowProperty('window.innerWidth', null);
		mockWindowProperty('document.documentElement', { clientWidth: null });
		mockWindowProperty('document.body', { clientWidth: null });
		const wrapper = shallowMountComponent();
		wrapper.setMethods({ trackBookingDetailViewed: jest.fn });
		const trackerSpy = jest.spyOn(wrapper.vm, 'trackBookingDetailViewed');
		wrapper.find('.card-body').trigger('click');
		expect(wrapper.vm.isModalOpen).toBe(true);
		await wrapper.vm.$nextTick();
		expect(trackerSpy).toBeCalled();
	});

	it('should not open detail booking if it is desktop when card-body clicked', async () => {
		mockWindowProperty('window.innerWidth', 0);
		mockWindowProperty('document.documentElement', { clientWidth: 1200 });
		const wrapper = shallowMountComponent();
		wrapper.setMethods({ trackBookingDetailViewed: jest.fn });
		const trackerSpy = jest.spyOn(wrapper.vm, 'trackBookingDetailViewed');
		wrapper.find('.card-body').trigger('click');
		expect(wrapper.vm.isModalOpen).toBe(false);
		expect(trackerSpy).not.toBeCalled();
	});

	it('should not set bookingDetailResponse if request status error', async () => {
		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: { status: false, meta: { message: 'error' } }
			})
		};
		wrapper = shallowMountComponent();
		await wrapper.vm.getBookingDetail();
		expect(open).toBeCalledWith('/user/booking', '_self');
		expect(wrapper.vm.bookingDetailResponse).toBe(null);
	});

	it('should not set bookingDetailResponse if request error', async () => {
		global.axios = { get: jest.fn().mockRejectedValue(new Error('error')) };
		wrapper = shallowMountComponent();
		await wrapper.vm.$nextTick();
		expect(wrapper.vm.bookingDetailResponse).toBe(null);
	});

	it('should return correct rent count type when propertyRentType called', () => {
		wrapper = shallowMountComponent();
		Object.keys(rentCountType).forEach(rentCountKey => {
			const bookingData = {
				booking_data: {
					rent_count_type: rentCountKey
				}
			};
			expect(wrapper.vm.propertyRentType(bookingData.booking_data)).toBe(
				rentCountType[rentCountKey]
			);
		});

		const bookingData = {
			rent_count_type: 'another_rent_count_type'
		};
		expect(wrapper.vm.propertyRentType(bookingData)).toBe(
			rentCountType.monthly
		);
	});

	it('should return correct gender when bookingDataTenantGender called', () => {
		wrapper = shallowMountComponent();
		const genders = { female: 'Perempuan', male: 'Laki-Laki' };
		Object.keys(genders).forEach(genderKey => {
			const bookingData = {
				user_booking: {
					gender: genderKey
				}
			};
			expect(wrapper.vm.bookingDataTenantGender(bookingData)).toBe(
				genders[genderKey]
			);
		});
	});

	it('should return correct redirection source when redirectionSource called', async () => {
		wrapper = shallowMountComponent();
		const redirectionSources = [
			{ from: 'booking', title: 'Booking Success Landing Page' },
			{ from: 'profile', title: 'User Profile' }
		];

		wrapper.setMethods({
			getQueryString: () => {
				return redirectionSources[0].from;
			}
		});
		await wrapper.vm.$nextTick();
		wrapper.vm.redirectionSource();
		expect(wrapper.vm.redirectionSource()).toBe(redirectionSources[0].title);

		wrapper.setMethods({
			getQueryString: () => {
				return redirectionSources[1].from;
			}
		});
		await wrapper.vm.$nextTick();
		wrapper.vm.redirectionSource();
		expect(wrapper.vm.redirectionSource()).toBe(redirectionSources[1].title);
	});

	it('should terminate booking', async () => {
		const resolvedValue = {
			has_reviewed: true,
			total_discount: 0,
			total_payment: 0,
			owner_name: 'sri',
			tenant_name: 'ani',
			contract_id: 123
		};

		wrapper = shallowMountComponent();
		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: {
					data: { resolvedValue },
					status: true
				}
			})
		};

		await wrapper.vm.getTerminateBookingDetail(123);
		expect(axios.get).toBeCalledWith('/user/booking/123', {
			baseURL: '/user/mamipay/request'
		});
	});

	it('should open kost list page', () => {
		wrapper = shallowMountComponent();

		wrapper.vm.openKostList('data');
		expect(wrapper.vm.$router.push).toBeCalledWith({
			path: '/kost-saya',
			query: { from: 'booking_history' }
		});
	});

	it('should open booking invoice page', () => {
		wrapper = shallowMountComponent();
		wrapper.vm.payBooking('booking data');
		expect(trackPayBooking).toBeCalledWith('booking data');
		expect(window.open).toBeCalled();
	});
});
