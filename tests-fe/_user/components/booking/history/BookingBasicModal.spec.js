import { shallowMount, createLocalVue } from '@vue/test-utils';
import BookingBasicModal from 'Js/_user/components/booking/history/BookingBasicModal';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

const localVue = createLocalVue();
mockWindowProperty('open', jest.fn());

describe('BookingBasicModal.vue', () => {
	const wrapper = shallowMount(BookingBasicModal, {
		localVue,
		propsData: { showModal: true }
	});

	it('should update showModal to false when modal closed', () => {
		wrapper.vm.closeModal();
		expect(wrapper.emitted('update:showModal')).toBeTruthy();
	});

	it('should open the redirect url', () => {
		wrapper.setProps({ closeRedirectUrl: 'https://mamikos.com' });
		wrapper.vm.closeModal();
		expect(window.open).toBeCalled();
	});
});
