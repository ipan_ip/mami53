import { shallowMount, createLocalVue } from '@vue/test-utils';
import BookingActionModal from 'Js/_user/components/booking/history/BookingActionModal';
import mockVLazy from '../../../../utils/mock-v-lazy';

const localVue = createLocalVue();
mockVLazy(localVue);

describe('BookingActionModal.vue', () => {
	const wrapper = shallowMount(BookingActionModal, {
		localVue,
		propsData: { showModal: true }
	});

	it('should render booking modal action', () => {
		expect(wrapper.find('#otherCustomActionModal').exists()).toBe(true);
	});

	it('should update showModal to false when modal closed', () => {
		wrapper.vm.closeModal();
		expect(wrapper.emitted('update:showModal')).toBeTruthy();
	});
});
