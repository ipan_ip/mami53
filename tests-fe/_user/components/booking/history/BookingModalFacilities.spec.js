import { shallowMount, createLocalVue } from '@vue/test-utils';
import BookingModalFacilities from 'Js/_user/components/booking/history/BookingModalFacilities.vue';
import roomFacility from '../__mocks__/room-facility.json';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

const localVue = createLocalVue();
mockVLazy(localVue);

describe('BookingModalFacilities.vue', () => {
	const propsData = {
		showModal: true,
		roomFacility
	};

	const wrapper = shallowMount(BookingModalFacilities, { localVue, propsData });

	it('should render booking modal facilities', () => {
		expect(wrapper.find('.booking-modal-facilities').exists()).toBe(true);
	});

	it('should render all facility group', () => {
		expect(wrapper.findAll('.booking-fac-content').length).toBe(
			roomFacility.length
		);
	});

	it('should render all facility icons', () => {
		const iconCounts = roomFacility.reduce((acc, cur) => {
			return acc + cur.value.length;
		}, 0);
		expect(wrapper.findAll('.room-facility').length).toBe(iconCounts);
	});

	it('should set class to modal-fullscreen if isFullscreenModal prop is true', async () => {
		wrapper.setProps({ isFullscreenModal: true });
		await wrapper.vm.$nextTick();
		expect(
			wrapper.find('.booking-modal-facilities').classes('modal-fullscreen')
		).toBe(true);
	});

	it('should render booking fac navbar if prop withNavbar is true', async () => {
		wrapper.setProps({ withNavbar: true });
		await wrapper.vm.$nextTick();
		expect(wrapper.find('.booking-fac-navbar').exists()).toBe(true);
	});

	it('should emit close modal when icon image clicked', async () => {
		wrapper.setProps({ withNavbar: true });
		await wrapper.vm.$nextTick();
		wrapper.find('.--icon').trigger('click');
		expect(wrapper.emitted('close')[0]).toBeTruthy();
	});
});
