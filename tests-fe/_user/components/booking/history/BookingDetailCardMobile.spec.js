import { shallowMount, createLocalVue } from '@vue/test-utils';
import BookingDetailCardMobile from 'Js/_user/components/booking/history/BookingDetailCardMobile.vue';
import userBookingList from '../__mocks__/user-booking-list.json';
import bookingStatusLabel from '../__mocks__/booking-status-label.json';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

jest.mock('Js/_user/event-bus/event-bus', () => {
	const EventBus = {
		$on: jest.fn(),
		$emit: jest.fn()
	};
	global.EventBus = EventBus;
	return global.EventBus;
});

const localVue = createLocalVue();
mockVLazy(localVue);

describe('BookingDetailCardMobile.vue', () => {
	const bookingDetail = userBookingList[0];
	const isCheckinStatus = false;
	const bookingStatus = bookingStatusLabel.waiting_payment;
	const ownerName = 'Suparman';
	const terminatedDetail = {
		contractId: 12345,
		totalPayment: '2000000',
		hasReviewed: true
	};

	const propsData = {
		bookingDetail,
		bookingStatus,
		isCheckinStatus,
		ownerName,
		terminatedDetail
	};

	const mixinOpenChat = {};
	const $dayjs = jest.fn(() => {
		return {
			locale: jest.fn(() => {
				return {
					format: jest.fn()
				};
			})
		};
	});

	const wrapper = shallowMount(BookingDetailCardMobile, {
		localVue,
		propsData,
		directives: {
			countdown: {}
		},
		methods: {
			$dayjs
		},
		mocks: {
			$router: { push: jest.fn() }
		},
		mixins: [mixinOpenChat]
	});

	it('should render booking card detail', () => {
		expect(wrapper.find('.booking-detail-mobile').exists()).toBe(true);
	});

	it('should not render cancel reason if booking not failed', () => {
		expect(wrapper.find('.header-reject-reason').exists()).toBe(false);
	});

	it('should render detail checkin if status is paid', async () => {
		wrapper.setProps({ bookingStatus: bookingStatusLabel.paid });
		await wrapper.vm.$nextTick();
		expect(wrapper.find('.detail-checkin').exists()).toBe(false);
	});

	// it('should render chat button if status is paid', async () => {
	// 	wrapper.setProps({ bookingStatus: bookingStatusLabel.waiting_payment });
	// 	const footerButton = wrapper
	// 		.findAll('.booking-card-footer button')
	// 		.filter(button => button.text().toLowerCase() === 'chat pemilik');
	// 	expect(footerButton.exists()).toBe(false);
	// });

	it('should render kost saya button if status is paid', async () => {
		const footerButton = wrapper
			.findAll('.booking-card-footer button')
			.filter(button => button.text().toLowerCase() === 'kost saya');
		expect(footerButton.exists()).toBe(false);
	});

	it('should render Bayar sekarang button if status is waiting payment', async () => {
		wrapper.setProps({ bookingStatus: bookingStatusLabel.waiting_payment });
		await wrapper.vm.$nextTick();
		const footerButton = wrapper
			.findAll('.booking-card-footer a')
			.filter(button => button.text().toLowerCase() === 'bayar sekarang');
		expect(footerButton.exists()).toBe(true);
	});

	// it('should render Cari Kost Lain button if status is termiated or failed', async () => {
	// 	wrapper.setProps({ bookingStatus: bookingStatusLabel.cancelled });
	// 	await wrapper.vm.$nextTick();
	// 	const footerButton = wrapper
	// 		.findAll('.booking-card-footer a')
	// 		.filter(button => button.text().toLowerCase() === 'cari kost lain');
	// 	expect(footerButton.exists()).toBe(false);
	// });

	// it('should render cancel button if status is waiting confirmation', async () => {
	// 	wrapper.setProps({
	// 		bookingStatus: bookingStatusLabel.waiting_confirmation
	// 	});
	// 	await wrapper.vm.$nextTick();
	// 	const footerButton = wrapper
	// 		.findAll('.booking-card-footer button')
	// 		.filter(button => button.text().toLowerCase() === 'batalkan booking');
	// 	expect(footerButton.exists()).toBe(false);
	// });

	describe('methods test', () => {
		it('should emit give review action with eventbus', () => {
			wrapper.vm.giveReview();
			expect(EventBus.$emit).toBeCalled();
		});

		it('should push the route to transaction histiry', () => {
			wrapper.vm.goHistoryTransaction();
			expect(wrapper.vm.$router.push).toBeCalledWith(
				'/riwayat-transaksi?contract=12345'
			);
		});

		it('should emit open checkin process', () => {
			wrapper.vm.openCheckin();
			expect(wrapper.emitted('open-checkin')).toBeTruthy();
		});
	});

	describe('computed properties test', () => {
		it('should show total payment statement', () => {
			expect(
				wrapper.vm.totalPayment
					.toLowerCase()
					.includes('selama 1 bulan menyewa kamu telah')
			).toBe(true);
		});

		it('should show total discount statement', () => {
			expect(wrapper.vm.totalDiscount.toLowerCase().includes('hemat rp.')).toBe(
				true
			);
		});

		it('should show correct star icon', () => {
			expect(wrapper.vm.reviewIcon.src).toBe(
				'/general/img/icons/ic_star-mamipay_s.svg'
			);
		});

		it('should show correct review label', () => {
			expect(wrapper.vm.reviewInstruction.toLowerCase()).toBe('sudah direview');
		});

		it('should show correct payment status', () => {
			expect(wrapper.vm.isFullyPaid).toBe(false);
		});
	});
});
