import { shallowMount, createLocalVue } from '@vue/test-utils';
import BookingDetailCardMobileDate from 'Js/_user/components/booking/history/BookingDetailCardMobileDate';

const localVue = createLocalVue();

describe('BookingDetailCardMobileDate.vue', () => {
	const propsData = {
		durationString: '2 Bulan',
		checkinDate: 'Jumat, 14 Aug 2020',
		checkoutDate: 'Senin, 14 Sep 2020'
	};

	const wrapper = shallowMount(BookingDetailCardMobileDate, {
		localVue,
		propsData
	});

	it('should render the component', () => {
		expect(wrapper.find('.booking-contract-date').exists()).toBe(true);
	});

	it('shoud render correct booking duration', () => {
		const bookingDuration = wrapper.findAll('.date-checkin span').at(0);
		expect(bookingDuration.text().toLowerCase()).toContain('2 bulan');
	});

	it('should render correct booking checkin and checout date', () => {
		const checkinDate = wrapper.findAll('.date-checkin span').at(1);
		const checkoutDate = wrapper.findAll('.date-checkin span').at(2);

		expect(checkinDate.text().toLowerCase()).toBe('14 aug 2020');
		expect(checkoutDate.text().toLowerCase()).toBe('14 sep 2020');
	});
});
