import { shallowMount, createLocalVue } from '@vue/test-utils';
import BookingModalSort from 'Js/_user/components/booking/history/BookingModalSort.vue';

const localVue = createLocalVue();

describe('BookingModalSort.vue', () => {
	const wrapper = shallowMount(BookingModalSort, {
		localVue,
		propsData: { showModal: true }
	});

	const { vm } = wrapper;

	it('should render booking modal mobile sort', () => {
		expect(wrapper.find('#bookingModalHistorySort').exists()).toBe(true);
	});

	it('should sort as newest first by default', () => {
		expect(wrapper.vm.sortSelected.name).toBe('Booking Terbaru');
		expect(wrapper.vm.sortSelected.value).toBe('default');
	});

	it('should emit close when modal closed', () => {
		wrapper.vm.closeModal();
		expect(wrapper.emitted('close')[0]).toBeTruthy();
	});

	it('should emit selected sort when user pick a sort option', () => {
		const selectedSort = { name: 'Booking Terlama', value: 'first_requested' };

		wrapper.vm.setSort(selectedSort);
		expect(wrapper.vm.sortSelected).toBe(selectedSort);
	});
});
