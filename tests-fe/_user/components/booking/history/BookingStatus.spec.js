import { shallowMount, createLocalVue } from '@vue/test-utils';
import BookingStatus from 'Js/_user/components/booking/history/BookingStatus.vue';

const localVue = createLocalVue();

describe('BookingStatus.vue', () => {
	const axios = {
		get: jest.fn().mockResolvedValue({
			data: {
				status: true,
				data: {
					booking_data: {
						invoice_url: 'mamikos.com/invoice-url'
					}
				}
			}
		})
	};

	const alert = jest.fn();
	const bugsnagClient = { notify: jest.fn() };

	global.axios = axios;
	global.alert = alert;
	global.bugsnagClient = bugsnagClient;

	const propsData = {
		detail: 'Butuh Konfirmasi',
		status: 'confirmed',
		id: 1
	};

	const swalError = jest.fn();

	let wrapper;

	const mountComponent = (changedProps = {}) => {
		return shallowMount(BookingStatus, {
			localVue,
			propsData: { ...propsData, ...changedProps },
			methods: { swalError }
		});
	};

	it('should render booking status component properly', () => {
		wrapper = mountComponent();
		expect(wrapper.find('.booking-status').exists()).toBe(true);
		expect(wrapper.find('.box-status').text()).toBe(propsData.detail);
	});

	it('should set class to cancel if status is rejected', async () => {
		wrapper = mountComponent({
			status: 'rejected',
			detail: 'Ditolak'
		});
		expect(wrapper.find('.box-status').classes('cancel')).toBe(true);
		expect(wrapper.find('.box-status').text()).toBe('Ditolak');
	});

	it('should set class to waiting-payment if status is booked', async () => {
		wrapper = mountComponent({
			status: 'confirmed',
			detail: 'Butuh Konfirmasi'
		});
		expect(wrapper.find('.box-status').classes('waiting-payment')).toBe(true);
		expect(wrapper.find('.box-status').text()).toBe('Butuh Konfirmasi');
	});

	it('should set class to accept if status is paid', async () => {
		wrapper = mountComponent({
			status: 'paid',
			detail: 'Terbayar'
		});
		expect(wrapper.find('.box-status').classes('accept')).toBe(true);
		expect(wrapper.find('.box-status').text()).toBe('Terbayar');
	});

	it('should set Dibatalkan if status is cancel_by_admin', async () => {
		wrapper = mountComponent({ status: 'cancel_by_admin' });
		expect(wrapper.find('.box-status').classes('cancel')).toBe(true);
		expect(wrapper.find('.box-status').text()).toBe('Dibatalkan');
	});

	it('should show Bayar Kost button if status is confirmed', () => {
		wrapper = mountComponent({ status: 'confirmed' });
		expect(wrapper.find('.button-pay').exists()).toBe(true);
	});

	it('should call swalError if response data status is false', async () => {
		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: { status: false, meta: { message: 'Error' } }
			})
		};
		wrapper = mountComponent({ status: 'confirmed' });
		await wrapper.vm.$nextTick();
		expect(swalError).toBeCalledWith(null, 'Error');
	});

	it('should report error if failed requesting to detail booking', async () => {
		global.axios = {
			get: jest.fn().mockRejectedValue(new Error('Error'))
		};
		wrapper = mountComponent({ status: 'confirmed' });

		await wrapper.vm.$nextTick();
		expect(swalError).toBeCalledWith(null, expect.any(String));
		expect(alert).toBeCalledWith(new Error('Error'));
		expect(bugsnagClient.notify).toBeCalledWith(new Error('Error'));
	});
});
