import { shallowMount } from '@vue/test-utils';
import BookingCheckinInstruction from 'Js/_user/components/booking/history/BookingCheckinInstruction';

describe('BookingCheckinInstruction.vue', () => {
	const wrapper = shallowMount(BookingCheckinInstruction, {
		propsData: { coordinateNumber: '12' }
	});

	it('should render the component', () => {
		expect(wrapper.find('#bookingCheckinInstruction').exists()).toBe(true);
	});

	it('should render the correct path for href tag', () => {
		const anchor = wrapper.findAll('.list-text a').at(0);
		expect(anchor.attributes('href')).toBe(
			'https://www.google.com/maps/search/?api=1&query=12'
		);
	});
});
