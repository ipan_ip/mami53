import { shallowMount, createLocalVue } from '@vue/test-utils';
import BookingDetailCardSpending from 'Js/_user/components/booking/history/BookingDetailCardSpending';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

const localVue = createLocalVue();
mockVLazy(localVue);

describe('BookingDetailCardSpending.vue', () => {
	const propsData = {
		totalPayment: 700000,
		totalDiscount: 300000
	};

	const wrapper = shallowMount(BookingDetailCardSpending, {
		localVue,
		propsData
	});

	it('should render the component', () => {
		expect(wrapper.find('.detail-tenant-spending').exists()).toBe(true);
	});

	it('should go to transaction page', () => {
		wrapper.vm.goHistoryTransaction();
		expect(wrapper.emitted('view-transaction')).toBeTruthy();
	});

	it.skip('should rendr correct discount and total payment amount', () => {
		const discountAmount = wrapper.find('.discount');
		const totalPayment = wrapper.find('.rent');

		expect(discountAmount.text().toLowerCase()).toContain('hemat rp300.000');
		expect(totalPayment.text().toLowerCase()).toContain('1 bulan');
	});
});
