import { shallowMount, createLocalVue } from '@vue/test-utils';
import BookingModalFilter from 'Js/_user/components/booking/history/BookingModalFilter.vue';

const localVue = createLocalVue();

describe('BookingModalFilter.vue', () => {
	const wrapper = shallowMount(BookingModalFilter, {
		localVue,
		propsData: { showModal: true }
	});
	const _this = wrapper.vm;

	it('should render booking modal mobile filter', () => {
		expect(wrapper.find('#bookingModalHistoryFilter').exists()).toBe(true);
	});

	it('should has all filters option', () => {
		expect(_this.filterOptions.length).toBe(8);
	});

	it('should not show filtered list on default', () => {
		expect(_this.filterSelected.name).toBe('Tampilkan Semua');
		expect(_this.filterSelected.value).toBe('all');
	});

	it('should emit close when modal closed', () => {
		_this.closeModal();
		expect(wrapper.emitted('close')[0]).toBeTruthy();
	});

	it('should emit selected filter when user pick a filter', () => {
		const selectedFilter = {
			name: 'Tunggu Konfirmasi',
			value: 'booked'
		};

		_this.setFilter(selectedFilter);
		expect(_this.filterSelected).toBe(selectedFilter);
	});
});
