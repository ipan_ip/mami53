import { createLocalVue, shallowMount } from '@vue/test-utils';

import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import mockComponent from 'tests-fe/utils/mock-component';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mockAxios from 'tests-fe/utils/mock-axios';
import dayjs from 'dayjs';
import UserHistoryTransactionDetail from 'Js/_user/components/UserHistoryTransactionDetail.vue';

jest.mock('html2canvas', () => {
	return jest.fn().mockResolvedValue({
		toDataURL: jest.fn().mockResolvedValue('test')
	});
});

// mock stubbed component
const stubs = {
	modal: mockComponent,
	DetailHistoryTransaction: mockComponent,
	MamiLoadingInline: mockComponent
};

const mocks = {
	$dayjs: dayjs,
	$router: {
		go: jest.fn()
	},
	$route: {
		params: {
			invoiceId: '12345'
		}
	}
};

const methods = {
	swalError: jest.fn()
};

// mock window properties
mockWindowProperty(
	'addEventListener',
	jest.fn((event, cb) => cb && cb())
);
mockWindowProperty('removeEventListener', jest.fn());
mockWindowProperty('innerHeight', 10);
mockWindowProperty('document.querySelector', jest.fn());
mockWindowProperty('navigator', {
	share: jest.fn().mockResolvedValue({}),
	canShare: true
});

// mock global properties
global.bugsnagClient = {
	notify: jest.fn()
};
global.fetch = jest
	.fn()
	.mockResolvedValue({ arrayBuffer: jest.fn().mockResolvedValue({}) });
global.html2canvas = jest.fn().mockResolvedValue('test');
global.axios = mockAxios;
global.File = jest.fn().mockReturnValue('test');

describe('UserHistoryTransactionDetail.vue', () => {
	const localVue = createLocalVue();

	// mock directives
	mockVLazy(localVue);

	const mount = () => {
		return shallowMount(UserHistoryTransactionDetail, {
			localVue,
			stubs,
			mocks,
			methods
		});
	};

	describe('render component correctly', () => {
		let wrapper;

		beforeEach(() => {
			wrapper = mount();
		});

		it('should render container properly', () => {
			expect(wrapper.find('#userBookingList').exists()).toBeTruthy();
		});
	});

	describe('render computed data properly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = mount();
		});

		it('should return invoiceId value from $route', () => {
			expect(wrapper.vm.invoiceId).toBe('12345');
		});

		it('should return isLoading value from isProgressFetchData', () => {
			expect(wrapper.vm.isLoading).toBe(wrapper.vm.isProgressFetchData);
		});

		it('should return isCanShare value from navigator.canShare', () => {
			expect(wrapper.vm.isCanShare).toBeTruthy();
		});
	});

	describe('call functions properly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = mount();
		});

		jest.useFakeTimers();

		it('should call functions before destroy properly', () => {
			wrapper.destroy();

			expect(window.removeEventListener).toBeCalled();
		});

		it('should set full mobile height when calling setFullHeightForMobile method', async () => {
			await wrapper.vm.setFullHeightForMobile();
			expect(wrapper.vm.fullMobileHeight).toBe('10px');
		});

		it('should set full mobile height and call window.addEventListener when calling initialMobileConfiguration method', async () => {
			await wrapper.vm.initialMobileConfiguration();
			expect(wrapper.vm.fullMobileHeight).toBe('10px');
			expect(window.addEventListener).toBeCalled();
		});

		it('should call navigator.share when calling shareDetailHistory method', async () => {
			const shareSpy = jest.spyOn(global.navigator, 'share');
			await wrapper.vm.shareDetailHistory();
			jest.advanceTimersByTime(3000);

			expect(shareSpy).toBeCalled();
			shareSpy.mockRestore();
		});

		it('should set detailTransactionData when calling fetchDataDetailTransaction method and received successful response', async () => {
			await axios.mockResolve({
				data: {
					status: true,
					data: {
						flash_sale: true,
						invoice_number: 1,
						payment_date: '2020-08-09 10:11',
						invoice_name: 'invoice_name',
						payment_amount: '1000',
						kost_name: 'kost_name',
						kost_type: 'kost_type',
						room_number: 1,
						rent_type: 'rent_type',
						kost_slug: 'kost_slug',
						kost_image: {
							medium: 'link/to/medium/sized/image'
						},
						owner_name: 'owner_name',
						tenant_name: 'tenant_name',
						invoice_url: 'pay.server.com/invoice/12345',
						payment_status: 'paid',
						payment_description: 'desc',
						detailed_costs: 'detail',
						group_channel_id: 12345
					},
					meta: {
						message: 'test'
					}
				}
			});
			await wrapper.vm.fetchDataDetailTransaction();
			await wrapper.vm.$nextTick();

			expect(wrapper.vm.detailTransaction).toEqual({
				flashSale: true,
				invoiceNumber: 1,
				paidDate: '2020-08-09 10:11',
				invoiceName: 'invoice_name',
				price: '1000',
				kostName: 'kost_name',
				kostType: 'kost_type',
				roomNumber: 1,
				rentType: 'rent_type',
				kostSlug: 'kost_slug',
				kostImage: 'link/to/medium/sized/image',
				ownerName: 'owner_name',
				tenantName: 'tenant_name',
				invoiceUrl: 'pay.server.com/invoice/12345',
				status: 'paid',
				statusDescription: 'desc',
				detailCost: 'detail',
				channelId: 12345
			});
			expect(wrapper.vm.isProgressFetchData).toBeFalsy();
		});

		it('should set detailTransactionData when calling fetchDataDetailTransaction method and received failed response', async () => {
			const swalErrorSpy = jest.spyOn(wrapper.vm, 'swalError');
			await axios.mockResolve({
				data: {
					status: false,
					meta: {
						message: 'test'
					}
				}
			});
			await wrapper.vm.fetchDataDetailTransaction();
			await wrapper.vm.$nextTick();

			expect(swalErrorSpy).toBeCalled();
			swalErrorSpy.mockRestore();
		});
	});
});
