import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';

import mockComponent from 'tests-fe/utils/mock-component';

import UserVerificationAccount from 'Js/_user/components/UserVerificationAccount.vue';

// mock imports
jest.mock('Js/_user/components/verification-account/VerificationEmail', () => {
	return mockComponent;
});
jest.mock('Js/_user/components/verification-account/VerificationPhone', () => {
	return mockComponent;
});
jest.mock(
	'Js/_user/components/verification-account/VerificationFacebook',
	() => {
		return mockComponent;
	}
);
jest.mock('Js/_user/components/verification-account/VerificationGoogle', () => {
	return mockComponent;
});
jest.mock('Js/_user/components/verification-identity/VerificationForm', () => {
	return mockComponent;
});

// mock stubbed component
const stubs = {
	VerificationEmail: mockComponent,
	VerificationPhone: mockComponent,
	VerificationFacebook: mockComponent,
	VerificationGoogle: mockComponent,
	VerificationForm: mockComponent
};

const store = {
	state: {
		menu: ''
	},
	mutations: {
		setMenu(state, menu) {
			state.menu = menu;
		}
	}
};

describe('UserVerificationAccount.vue', () => {
	const localVue = createLocalVue();
	localVue.use(Vuex);

	const mount = () => {
		return shallowMount(UserVerificationAccount, {
			localVue,
			stubs,
			store: new Vuex.Store(store)
		});
	};

	describe('render component correctly', () => {
		let wrapper;

		beforeEach(() => {
			wrapper = mount();
		});

		it('should render container', () => {
			expect(wrapper.find('.row.content').exists()).toBeTruthy();
		});
	});
});
