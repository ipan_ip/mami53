import Vuex from 'vuex';

import { createLocalVue, shallowMount, mount } from '@vue/test-utils';

import userVoucher from 'Js/_user/components/UserVoucher.vue';
import store from 'VoucherDetail';
import mockAxios from 'tests-fe/utils/mock-axios';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import mockComponent from 'tests-fe/utils/mock-component';

describe('UserVoucher.vue', () => {
	const localVue = createLocalVue();
	localVue.use(Vuex);
	mockVLazy(localVue);

	global.axios = mockAxios;

	const notify = jest.fn();
	mockWindowProperty('bugsnagClient', { notify });

	const tracker = jest.fn();
	global.tracker = tracker;

	const authData = {
		all: {
			id: 1725551
		}
	};

	const storeVoucher = new Vuex.Store(store);

	describe('Render voucher list', () => {
		const tabsEventListener = jest.fn();
		const checkUserDevice = jest.fn();
		const moengageTrackEvent = jest.fn();
		let wrapper;

		beforeEach(() => {
			wrapper = shallowMount(userVoucher, {
				localVue,
				storeVoucher,
				mocks: {
					$store: {
						commit: jest.fn(),
						dispatch: jest.fn()
					}
				},
				methods: {
					checkUserDevice,
					tabsEventListener,
					moengageTrackEvent
				}
			});
		});

		it('should called checkUserDevice, moengageTrackEvent, tabsEventListener methods on mount', () => {
			expect(checkUserDevice).toHaveBeenCalled();
			expect(moengageTrackEvent).toHaveBeenCalled();
			expect(tabsEventListener).toHaveBeenCalled();
		});

		it('should render voucherku container', () => {
			expect(wrapper.find('.my-voucher').exists()).toBeTruthy();
		});
	});

	describe('Render voucher list in Mobile device', () => {
		let wrapper;

		beforeEach(() => {
			mockWindowProperty('innerWidth', 700);
			mockWindowProperty('isMobile', true);

			wrapper = shallowMount(userVoucher, {
				localVue,
				storeVoucher,
				mocks: {
					$store: {
						commit: jest.fn(),
						dispatch: jest.fn(),
						state: {
							authData
						}
					}
				},
				attachToDocument: true
			});
		});

		it('should set isMobile as true', () => {
			expect(wrapper.vm.isMobile).toBe(true);
		});

		it('should render Promo Lainnya container and button', () => {
			expect(wrapper.find('.other-promo').exists()).toBeTruthy();
			expect(wrapper.find('button.other-promo__button').exists()).toBeTruthy();
		});

		it('should redirect to Promo Page on click', done => {
			mockWindowProperty('open', 'https://promo.mamikos.com');
			wrapper.find('button.other-promo__button').trigger('click');
			wrapper.vm.$nextTick(() => {
				expect(window.open).toBe('https://promo.mamikos.com');
				done();
			});
		});
	});

	describe('Click on tab', () => {
		const refreshVoucherCount = jest.fn();
		const moengageTrackEvent = jest.fn();

		const wrapper = mount(userVoucher, {
			localVue,
			storeVoucher,
			mocks: {
				$store: {
					commit: jest.fn(),
					dispatch: jest.fn(),
					state: {
						authData
					}
				}
			},
			stubs: {
				VoucherContainer: mockComponent
			},
			methods: {
				refreshVoucherCount,
				moengageTrackEvent
			},
			attachToDocument: true
		});

		it('should render voucher tabs', () => {
			expect(wrapper.find('#voucher-tab li').exists()).toBeTruthy();
		});

		it('Should refresh counter ', async () => {
			await wrapper
				.findAll('#voucher-tab li')
				.at(1)
				.trigger('click');
			expect(refreshVoucherCount).toHaveBeenCalled();
		});

		it('Should set active tab ', async () => {
			const indexExpect = 1;
			await wrapper
				.findAll('#voucher-tab li')
				.at(indexExpect)
				.trigger('click');
			expect(wrapper.vm.indexActiveTabs).toBe(indexExpect);
		});

		it('Should called moengageTrackEvent if indexActiveTabs value is 1 or 2 ', async () => {
			await wrapper
				.findAll('#voucher-tab li')
				.at(1)
				.trigger('click');
			expect(moengageTrackEvent).toHaveBeenCalled();

			await wrapper
				.findAll('#voucher-tab li')
				.at(2)
				.trigger('click');
			expect(moengageTrackEvent).toHaveBeenCalled();
		});
	});
});
