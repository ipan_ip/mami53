import { shallowMount, createLocalVue } from '@vue/test-utils';
import UserBookingTabs from 'Js/_user/components/UserBookingTabs.vue';

const localVue = createLocalVue();

describe('UserBookingTabs.vue', () => {
	const propsData = {
		tabs: [
			{
				label: 'Riwayat',
				path: '/booking'
			},
			{
				label: 'Draft',
				path: '/booking/draft',
				notification: 2
			},
			{
				label: 'Baru Dilihat',
				path: '/booking/last-seen'
			}
		],
		value: '/booking',
		selectedValue: 'path',
		redDot: 'notification',
		tabLabel: 'label'
	};

	const mountComponent = () =>
		shallowMount(UserBookingTabs, { localVue, propsData });

	it('should render component properly', () => {
		const wrapper = mountComponent();
		expect(wrapper.element).toMatchSnapshot();
	});

	it('should set --active class if tab is active', () => {
		const wrapper = mountComponent();
		expect(
			wrapper
				.findAll('li')
				.at(0)
				.classes('--active')
		).toBe(true);
	});

	it('should set --red-dot class if tab has notification', () => {
		const wrapper = mountComponent();
		expect(
			wrapper
				.findAll('li')
				.at(1)
				.classes('--red-dot')
		).toBe(true);
	});

	it('should not set --red-dot class if red dot is not specified', async () => {
		const wrapper = mountComponent();
		wrapper.setProps({ redDot: '' });
		await wrapper.vm.$nextTick();
		expect(
			wrapper
				.findAll('li')
				.at(1)
				.classes('--red-dot')
		).toBe(false);
	});

	it('should emit selected tab value if tab clicked', () => {
		const wrapper = mountComponent();
		wrapper
			.findAll('li a')
			.at(2)
			.trigger('click.prevent');
		expect(wrapper.emitted('change')[0][0]).toBe('/booking/last-seen');
	});

	it('should emit selected tab value if selected value changed', async () => {
		const wrapper = mountComponent();
		wrapper.setProps({ value: '/booking/draft' });
		await wrapper.vm.$nextTick();
		expect(wrapper.emitted('change')[0][0]).toBe('/booking/draft');
	});
});
