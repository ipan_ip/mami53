import { mount, createLocalVue } from '@vue/test-utils';
import HistoryModalDesktopFilter from 'Js/_user/components/history-transaction/HistoryModalDesktopFilter.vue';
import mockComponent from 'tests-fe/utils/mock-component';

const stubs = {
	dropdown: mockComponent,
	HistorySelectFilterWrapper: mockComponent
};

const localVue = createLocalVue();

describe('HistoryModalDesktopFilter', () => {
	let wrapper;

	beforeEach(() => {
		wrapper = mount(HistoryModalDesktopFilter, {
			localVue,
			stubs
		});
	});

	it('should render component properly', () => {
		expect(wrapper.find('.wrapper-desktop-filter').exists()).toBeTruthy();
	});
});
