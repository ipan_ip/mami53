import { mount, createLocalVue } from '@vue/test-utils';
import HistoryModalMobileFilter from 'Js/_user/components/history-transaction/HistoryModalMobileFilter.vue';
import { mockComponentWithAttrs } from 'tests-fe/utils/mock-component';

const stubs = {
	FilterCloseButton: mockComponentWithAttrs({ id: 'closeButton' }),
	modal: mockComponentWithAttrs({ id: 'bookingModalMobileFilter' }),
	HistorySelectFilterWrapper: mockComponentWithAttrs({ id: 'selectWrapper' })
};

const mocks = {
	$emit: jest.fn()
};

const propsData = {
	showModal: true
};

const localVue = createLocalVue();

describe('HistoryModalMobileFilter', () => {
	let wrapper;

	beforeEach(() => {
		wrapper = mount(HistoryModalMobileFilter, {
			localVue,
			mocks,
			stubs,
			propsData
		});
	});

	it('should render component properly', () => {
		expect(wrapper.find('.filter-header').exists()).toBeTruthy();
	});

	it('should call $emit when calling closeModal method', async () => {
		const emitSpy = jest.spyOn(wrapper.vm, '$emit');
		await wrapper.vm.closeModal();
		expect(emitSpy).toBeCalled();
		emitSpy.mockRestore();
	});

	it('should call $emit when calling setFilter method', async () => {
		const emitSpy = jest.spyOn(wrapper.vm, '$emit');
		await wrapper.vm.setFilter();
		expect(emitSpy).toBeCalled();
		emitSpy.mockRestore();
	});
});
