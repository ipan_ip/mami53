import { mount, createLocalVue } from '@vue/test-utils';
import HistorySelectFilterWrapper from 'Js/_user/components/history-transaction/HistorySelectFilterWrapper.vue';

import { mockComponentWithAttrs } from 'tests-fe/utils/mock-component';

const stubs = {
	HistorySelectFilterWrapper: mockComponentWithAttrs({
		class: 'filter-options'
	})
};

const localVue = createLocalVue();

const mountWrapper = (options = {}) => {
	return mount(HistorySelectFilterWrapper, {
		...{
			localVue,
			stubs
		},
		...options
	});
};

describe('HistorySelectFilterWrapper', () => {
	let wrapper;

	describe('should render properly', () => {
		beforeEach(() => {
			wrapper = mountWrapper();
		});

		it('should render component properly', () => {
			expect(wrapper.find('.filter-options').exists()).toBeTruthy();
		});
	});
});
