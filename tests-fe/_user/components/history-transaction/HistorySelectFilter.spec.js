import { mount, createLocalVue } from '@vue/test-utils';
import HistorySelectFilter from 'Js/_user/components/history-transaction/HistorySelectFilter.vue';

const mocks = {
	$emit: jest.fn()
};

const propsData = {
	title: '',
	headerTitle: '',
	activeBottomLine: false,
	isMultipleSelect: false
};

const localVue = createLocalVue();

const mountWrapper = (options = {}) => {
	return mount(HistorySelectFilter, {
		...{
			localVue,
			mocks,
			propsData
		},
		...options
	});
};

describe('HistorySelectFilter', () => {
	let wrapper;

	beforeEach(() => {
		wrapper = mountWrapper();
	});

	it('should render component properly', () => {
		expect(wrapper.find('.filter-options').exists()).toBeTruthy();
	});

	it('should call $emit when clicking on filter options', async () => {
		const filterOptions = wrapper.find('.filter-options');

		if (filterOptions && filterOptions.exists()) {
			const emitSpy = jest.spyOn(wrapper.vm, '$emit');
			await filterOptions.trigger('click');
			expect(emitSpy).toBeCalled();
			emitSpy.mockRestore();
		}
	});

	it('should set options data when clicking on a single select option ', async () => {
		const options = wrapper.findAll('.wrapper-filter > span');
		if (options && options.length) {
			const firstOption = options.at(0);
			firstOption.trigger('click');
			await wrapper.vm.$nextTick();

			expect(wrapper.vm.options).toEqual([
				{
					label: '',
					value: '',
					active: true
				}
			]);
			expect(wrapper.vm.options[0].active).toBeTruthy();
		}
	});

	it('should toggle option active value when clicking on multiple select option', async () => {
		wrapper.setProps({
			isMultipleSelect: true
		});
		wrapper.setData({
			options: [
				{
					label: 'test',
					value: 'test',
					active: true
				}
			]
		});

		await wrapper.vm.$nextTick();
		const options = wrapper.findAll('.wrapper-filter > span');
		if (options && options.length) {
			const firstOption = options.at(0);
			firstOption.trigger('click');
			await wrapper.vm.$nextTick();

			expect(wrapper.vm.options[0].active).toBeFalsy();
		}
	});
});
