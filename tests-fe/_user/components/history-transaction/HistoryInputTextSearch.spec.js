import '@babel/polyfill';
import { mount, createLocalVue } from '@vue/test-utils';
import HistoryInputTextSearch from 'Js/_user/components/history-transaction/HistoryInputTextSearch.vue';

jest.mock('Js/_user/event-bus/event-bus', () => {
	const EventBus = {
		$on: jest.fn(),
		$emit: jest.fn()
	};
	global.EventBus = EventBus;
	return global.EventBus;
});

const localVue = createLocalVue();

describe('HistoryInputTextSearch', () => {
	let wrapper;

	beforeEach(() => {
		wrapper = mount(HistoryInputTextSearch, {
			localVue
		});
	});

	it('Should emit value text search when enter action', async () => {
		const { vm } = wrapper;

		await vm.goSearch();
		expect(wrapper.emitted().textSearch).toBeTruthy();
	});
});
