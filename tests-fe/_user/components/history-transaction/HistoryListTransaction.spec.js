import '@babel/polyfill';
import { mount, createLocalVue } from '@vue/test-utils';
import HistoryListTransaction from 'Js/_user/components/history-transaction/HistoryListTransaction.vue';

jest.mock('Js/_user/event-bus/event-bus', () => {
	const EventBus = {
		$on: jest.fn(),
		$emit: jest.fn()
	};
	global.EventBus = EventBus;
	return global.EventBus;
});

const mocks = {
	$dayjs: jest.fn(() => {
		return {
			format: jest.fn(() => '20 Apr 2020')
		};
	})
};

const localVue = createLocalVue();

describe('HistoryListTransaction', () => {
	let wrapper;

	beforeEach(() => {
		wrapper = mount(HistoryListTransaction, {
			localVue,
			mocks
		});
	});

	it('Should emit value text search when enter action', async () => {});
});
