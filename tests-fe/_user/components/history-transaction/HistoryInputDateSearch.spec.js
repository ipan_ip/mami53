import '@babel/polyfill';
import { mount, createLocalVue } from '@vue/test-utils';
import HistoryInputDateSearch from 'Js/_user/components/history-transaction/HistoryInputDateSearch.vue';

jest.mock('Js/_user/event-bus/event-bus', () => {
	const EventBus = {
		$on: jest.fn(),
		$emit: jest.fn()
	};
	global.EventBus = EventBus;
	return global.EventBus;
});

const mocks = {
	$dayjs: jest.fn(() => {
		return {
			format: jest.fn(() => '20 Apr 2020')
		};
	})
};

const propsData = {
	label: 'Dari'
};

const localVue = createLocalVue();

describe('HistoryInputDateSearch', () => {
	let wrapper;

	beforeEach(() => {
		wrapper = mount(HistoryInputDateSearch, {
			localVue,
			mocks,
			propsData
		});
	});

	it('Should show label and date selected', () => {
		const titleDate = wrapper.find('.title-date-info');
		const dateSelected = wrapper.find('.value-date-info');

		expect(titleDate.text()).toBe('Dari');
		expect(dateSelected.text()).toBe('20 Apr 2020');
	});

	it('Should emit event bus if date insert', async () => {
		const { vm } = wrapper;

		await vm.dateChange('2020-01-01');
		expect(global.EventBus.$emit).toBeCalledWith(
			'dateSearchChange',
			expect.any(Object)
		);
	});
});
