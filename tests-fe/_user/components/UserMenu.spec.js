import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';

import { mockComponentWithAttrs } from 'tests-fe/utils/mock-component';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

import UserMenu from 'Js/_user/components/UserMenu.vue';

// mock store
const UserMenuStore = {
	state: {
		isUserLoggedInViaSocial: true,
		breadcrumb: 'user',
		userProfile: {
			is_verify_email: true,
			is_verify_phone_number: true,
			is_verify_identity_card: true
		},
		voucherCount: 20,
		totalPoin: 20
	},
	getters: {
		bookingNotificationCount() {
			return 10;
		}
	},
	mutations: {
		setBreadcrumb(state, breadcrumb) {
			state.breadcrumb = breadcrumb;
		}
	},
	actions: {
		getVoucherCount() {},
		getTotalPoin() {}
	}
};

// mock stubbed component
const stubs = {
	UserProfile: mockComponentWithAttrs({ id: 'userProfile' }),
	UserMamipayWidget: mockComponentWithAttrs({ id: 'userMamipayWidget' }),
	RouterLink: true
};

describe('UserMenu.vue', () => {
	const localVue = createLocalVue();
	localVue.use(Vuex);

	// mock directives
	mockVLazy(localVue);

	const mount = () => {
		return shallowMount(UserMenu, {
			localVue,
			stubs,
			store: new Vuex.Store(UserMenuStore),
			mocks: {
				$route: {
					path: {
						match: jest.fn().mockReturnValue('routeName')
					}
				}
			}
		});
	};

	describe('render component correctly', () => {
		let wrapper;

		beforeEach(() => {
			wrapper = mount();
		});

		it('should render container', () => {
			expect(wrapper.find('.row').exists()).toBeTruthy();
		});
	});

	describe('render computed data properly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = mount();
		});

		it('should return isVerifyEmail value from $store', () => {
			expect(wrapper.vm.isVerifyEmail).toBeTruthy();
		});

		it('should return isVerifyPhoneNumber value from $store', () => {
			expect(wrapper.vm.isVerifyPhoneNumber).toBeTruthy();
		});

		it('should return isVerifyIdentityCard value from $store', () => {
			expect(wrapper.vm.isVerifyIdentityCard).toBeTruthy();
		});

		it('should return voucherCount value from $store', () => {
			expect(wrapper.vm.voucherCount).toBe(20);
		});

		it('should return bookingNotification value from $store and set the notification count in profileMenu when changed', () => {
			// get
			expect(wrapper.vm.bookingNotification).toBe(10);

			// set
			wrapper.vm.bookingNotification = 6;
			expect(wrapper.vm.profilMenu.booking.notification).toBe(6);
		});
	});

	describe('call functions in the watch block correctly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = mount();
		});

		it('should add 1 to notification count on isVerifyEmail value change', async () => {
			wrapper.setData({
				profilMenu: {
					verifikasiAkun: {
						notification: 1
					}
				}
			});
			wrapper.vm.$store.state.userProfile.is_verify_email = false;
			await wrapper.vm.$nextTick();
			expect(wrapper.vm.profilMenu.verifikasiAkun.notification).toBe(2);
		});

		it('should add 1 to notification count on isVerifyPhoneNumber value change', async () => {
			wrapper.setData({
				profilMenu: {
					verifikasiAkun: {
						notification: 1
					}
				}
			});
			wrapper.vm.$store.state.userProfile.is_verify_phone_number = false;
			await wrapper.vm.$nextTick();
			expect(wrapper.vm.profilMenu.verifikasiAkun.notification).toBe(2);
		});

		it('should add 1 to notification count on isVerifyIdentityCard value change', async () => {
			wrapper.setData({
				profilMenu: {
					verifikasiIdentitas: {
						notification: 1
					}
				}
			});
			wrapper.vm.$store.state.userProfile.is_verify_identity_card = 'No Valid';
			await wrapper.vm.$nextTick();
			expect(wrapper.vm.profilMenu.verifikasiIdentitas.notification).toBe(2);
		});
	});

	describe('call functions correctly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = mount();
		});

		it('should call $store.dispatch when calling refreshVoucherCount method', async () => {
			const dispatchSpy = jest.spyOn(wrapper.vm.$store, 'dispatch');
			await wrapper.vm.refreshVoucherCount();
			expect(dispatchSpy).toBeCalled();
			dispatchSpy.mockRestore();
		});

		it('should hide settings when calling setSettingsMenuVisibility method', async () => {
			await wrapper.vm.setSettingsMenuVisibility();

			expect(wrapper.vm.profilMenu.settings.hidden).toBeTruthy();
		});
	});

	describe.only('sanitizeUrl method', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = mount();
		});

		it('should only contain starting URI', () => {
			const sanitizedUrl = wrapper.vm.sanitizeUrl('/booking/draft');
			expect(sanitizedUrl).toBe('booking');

			const sanitizedUrl2 = wrapper.vm.sanitizeUrl('/booking');
			expect(sanitizedUrl2).toBe('booking');
		});

		it('should clear the query string', () => {
			const urlWithQueryString = wrapper.vm.sanitizeUrl('/booking?q=val&b=');
			expect(urlWithQueryString).toBe('booking');

			const urlWithQueryString2 = wrapper.vm.sanitizeUrl(
				'/booking/draft?q=val&b='
			);
			expect(urlWithQueryString2).toBe('booking');
		});

		it('should not do any transformation, when the parameter is a root URI', () => {
			const rootUrl = wrapper.vm.sanitizeUrl('/');
			expect(rootUrl).toBe('/');
		});
	});
});
