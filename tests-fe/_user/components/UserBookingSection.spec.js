import { shallowMount, createLocalVue } from '@vue/test-utils';
import UserBookingSection from 'Js/_user/components/UserBookingSection.vue';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

const localVue = createLocalVue();

describe('UserBookingSection.vue', () => {
	const RouterView = `<div></div>`;
	const $route = { path: '/booking' };
	const $router = { replace: jest.fn() };
	const $store = {
		state: {
			bookingNotification: {
				draft: 0,
				lastSeen: 0
			}
		},
		commit: jest.fn(),
		getters: {
			bookingNotificationCount: () => 0
		}
	};

	const scrollIntoView = jest.fn();
	mockWindowProperty(
		'document.querySelector',
		jest.fn(() => {
			return {
				scrollIntoView
			};
		})
	);

	const mountComponent = (isMobile = false) => {
		if (isMobile) {
			mockWindowProperty('window.innerWidth', 600);
			mockWindowProperty('navigator.isMobile', true);
		}

		return shallowMount(UserBookingSection, {
			localVue,
			stubs: { RouterView },
			mocks: { $router, $route, $store }
		});
	};

	it('should render user booking section', () => {
		const wrapper = mountComponent();
		expect(wrapper.find('.user-booking-section').exists()).toBe(true);
	});

	it('should autoscroll after component mounted', async () => {
		jest.clearAllMocks();
		const wrapper = mountComponent();
		await wrapper.vm.$nextTick();
		expect(scrollIntoView).toBeCalled();
	});

	it('should set activeTab when router change', async () => {
		const wrapper = mountComponent();
		wrapper.vm.$route.path = '/booking/draft';
		await wrapper.vm.$nextTick();
		expect(wrapper.vm.activeTab).toBe('/booking/draft');
	});

	it('should replace current route path with activeTab value changes', async () => {
		const wrapper = mountComponent();
		wrapper.vm.activeTab = '/booking/last-seen';
		await wrapper.vm.$nextTick();
		expect($router.replace).toBeCalledWith('/booking/last-seen');
	});
});
