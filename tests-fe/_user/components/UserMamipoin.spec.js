import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import moment from 'moment';
import mockAxios from 'tests-fe/utils/mock-axios';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mixinMamipoin from 'Js/_user/mixins/mixinMamipoin.js';
import UserMamipoin from 'Js/_user/components/UserMamipoin.vue';
import MamipoinCard from 'Js/_user/components/mamipoin/MamipoinCard';
import MamipoinSkeleton from 'Js/_user/components/mamipoin/MamipoinSkeleton';
import MamipoinCardTotalPoin from 'Js/_user/components/mamipoin/MamipoinCardTotalPoin';

const mockStore = {
	state: {
		totalPoin: {
			point: 20000,
			near_expired_point: 10000,
			near_expired_date: moment()
		}
	}
};

const stubs = {
	MamipoinCard,
	MamipoinSkeleton,
	MamipoinCardTotalPoin,
	RouterLink: true
};

const mocks = {
	$router: {
		push: jest.fn()
	}
};

describe('UserMamipoin.vue', () => {
	const localVue = createLocalVue();
	localVue.use(Vuex);
	localVue.mixin([mixinMamipoin]);
	mockVLazy(localVue);
	window.innerWidth = 1440;
	global.axios = mockAxios;
	global.tracker = jest.fn();
	const mount = () => {
		return shallowMount(UserMamipoin, {
			localVue,
			stubs,
			mocks,
			moment: moment,
			store: new Vuex.Store(mockStore)
		});
	};

	let wrapper;
	beforeEach(() => {
		wrapper = mount();
	});

	describe('render userMamipoin correctly', () => {
		it('should render title Mamipoin', () => {
			expect(wrapper.find('.title').exists()).toBeTruthy();
		});

		it('should render mamipoin skeleton when loaded', () => {
			expect(wrapper.findComponent(MamipoinSkeleton)).toBeTruthy();
		});
		it('should render card total poin tenant', () => {
			expect(wrapper.findComponent(MamipoinCardTotalPoin)).toBeTruthy();
		});
	});

	describe('should computed value correctly', () => {
		describe('totalPoint', () => {
			it('should return total poin in thousand format', () => {
				expect(wrapper.vm.totalPoint).toBe('20.000');
			});
			it('should return 0  when total poin from store is null', () => {
				wrapper.vm.$store.state.totalPoin.point = null;
				expect(wrapper.vm.totalPoint).toBe('0');
			});
		});

		describe('poinExpired', () => {
			it('should return expired poin in thousand format', () => {
				expect(wrapper.vm.pointExpired).toBe('10.000');
			});
			it('should return 0  when Expired poin from store is null', () => {
				wrapper.vm.$store.state.totalPoin.near_expired_point = null;
				expect(wrapper.vm.pointExpired).toBe('0');
			});
		});

		describe('dateExpired', () => {
			it('should return 0  when date Expired  from store is null', () => {
				wrapper.vm.$store.state.totalPoin.near_expired_date = null;
				expect(wrapper.vm.dateExpired).toBe('0');
				wrapper.vm.$store.state.totalPoin.near_expired_date = moment();
			});
		});

		describe('isMobile', () => {
			it('should return false when inner width of device  more than 768 ', () => {
				expect(wrapper.vm.isMobile).toBeFalsy();
				// set for mobile
				global.innerWidth = 375;
			});

			it('should return true when inner width of device no more than 768 ', () => {
				expect(wrapper.vm.isMobile).toBeTruthy();
			});
		});
	});

	describe('shoulld run method correctly', () => {
		describe('toThousand', () => {
			it('should change value into thousand format ', () => {
				expect(wrapper.vm.toThousand(1500)).toBe('1.500');
			});
		});

		describe('toMonth', () => {
			it('should change input date into day mount year format', () => {
				expect(wrapper.vm.toMonth('2020-10-31 23:59:59')).toBe('31 Okt 2020');
			});
		});

		describe('isTenantBlacklist', () => {
			it('should return false when poin of tenant more than 0', () => {
				wrapper.vm.$store.state.totalPoin.point = 5;
				expect(wrapper.vm.isTenantBlacklist).toBeFalsy();
			});
		});
	});
});
