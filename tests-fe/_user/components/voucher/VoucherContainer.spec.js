import { createLocalVue, shallowMount } from '@vue/test-utils';
import cloneDeep from 'lodash/cloneDeep';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import 'tests-fe/utils/mock-vue';
import mockAxios from 'tests-fe/utils/mock-axios';
import VoucherContainer from 'Js/_user/components/voucher/VoucherContainer.vue';

global.axios = mockAxios;

const localVue = createLocalVue();
mockVLazy(localVue);
window.addEventListener = jest.fn();
const mockGetElementByid = wrapper => {
	document.getElementById = p => {
		const el = wrapper.find(`#${p}`).element;
		el.scrollTop = 0;
		el.style.height = 0;
		return el;
	};
};

const responseApiMocked = {
	data: {
		status: true,
		data: [
			{
				id: 0,
				code: 'response-code',
				end_date: '2020-02-02',
				title: 'response-title',
				image: 'response-img'
			}
		],
		has_next: true
	}
};
const propsData = {
	isAvailable: true,
	isMobile: false,
	label: 'label-test',
	type: 'type-test',
	emptyTitle: 'empty-title-test',
	emptyText: 'empty-text-test'
};

const stubs = {
	VoucherCard: { template: '<div id="voucherCard"></div>' },
	Masonry: {
		template: `<div :id="id" style="height='1px';overflow-y='scroll'" @click="click"><slot></slot></div>`,
		props: ['id'],
		methods: {
			click() {
				console.log('asd');
				this.$emit('scroll');
			}
		}
	}
};

describe('VoucherContainer.vue', () => {
	let wrapper;
	beforeEach(() => {
		mockAxios.mockResolve(responseApiMocked);
		wrapper = shallowMount(VoucherContainer, {
			localVue,
			propsData,
			stubs,
			methods: {
				swalError: jest.fn()
			}
		});
	});
	describe('should render', () => {
		it('event onscroll properly', () => {
			expect(window.addEventListener).toBeCalled();
			expect(window.addEventListener).toBeCalledWith(
				'scroll',
				wrapper.vm.handleScroll
			);
		});
		it('voucher-card properly when api response with data', async done => {
			await mockAxios.get;
			wrapper.vm.$nextTick(() => {
				expect(wrapper.find('#voucherCard').exists()).toBeTruthy();
				done();
			});
			expect(wrapper.vm.isEmpty).toBe(false);
		});

		it('component when api response with no data', async done => {
			const noDataResponse = cloneDeep(responseApiMocked);
			noDataResponse.data.data = [];
			mockAxios.mockResolve(noDataResponse);
			const wrapper = shallowMount(VoucherContainer, {
				localVue,
				propsData,
				stubs
			});

			await mockAxios.get;

			expect(wrapper.vm.isEmpty).toBe(true);
			wrapper.vm.$nextTick(() => {
				expect(wrapper.find('#voucherCard').exists()).toBeFalsy();
				done();
			});
		});

		it('component when api response return error', async done => {
			mockAxios.mockReject('error');
			window.bugsnagClient = { notify: jest.fn() };

			const wrapper = shallowMount(VoucherContainer, {
				localVue,
				propsData,
				stubs,
				methods: {
					swalError: jest.fn()
				}
			});

			await mockAxios.get;
			await Promise.prototype.catch;

			expect(wrapper.vm.isEmpty).toBe(true);
			expect(wrapper.vm.isLoading).toBe(false);
			expect(window.bugsnagClient.notify).toBeCalled();
			wrapper.vm.$nextTick(() => {
				expect(wrapper.find('#voucherCard').exists()).toBeFalsy();
				done();
			});
		});

		it('window which has scroll event', () => {
			wrapper.vm.handleScroll();
			expect(wrapper.vm.isHeaderOnTop).toBe(true);
		});
	});

	it('should fire scroll event properly', async done => {
		await mockAxios.get;
		mockGetElementByid(wrapper);

		wrapper.vm.$nextTick(() => {
			wrapper.vm.nextVoucher = jest.fn();
			wrapper.find('.voucher__list').trigger('scroll');
			expect(wrapper.vm.nextVoucher).toBeCalled();
			done();
		});
	});

	it('should get next voucher', async done => {
		await mockAxios.get;
		mockGetElementByid(wrapper);

		wrapper.vm.$nextTick(async () => {
			wrapper.find('.voucher__list').trigger('scroll');
			await mockAxios.get;
			expect(wrapper.vm.vouchers.length).toBe(2);
			done();
		});
	});

	it('should not get next voucher because of error', async done => {
		await mockAxios.get;
		mockGetElementByid(wrapper);

		wrapper.vm.$nextTick(async () => {
			mockAxios.mockReject('error');
			window.bugsnagClient = { notify: jest.fn() };

			wrapper.find('.voucher__list').trigger('scroll');
			await mockAxios.get;
			expect(wrapper.vm.vouchers.length).toBe(1);
			done();
		});
	});

	it('should update scrollTop', () => {
		document.getElementById = p => wrapper.find(`#${p}`).element;
		wrapper.vm.backToTop();
		expect(wrapper.find('.voucher__list').element.scrollTop).toBe(0);
	});
});
