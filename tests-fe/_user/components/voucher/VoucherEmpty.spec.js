import { createLocalVue, shallowMount } from '@vue/test-utils';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import VoucherEmpty from 'Js/_user/components/voucher/VoucherEmpty.vue';

describe('VoucherEmpty.vue', () => {
	const localVue = createLocalVue();
	mockVLazy(localVue);
	const wrapper = shallowMount(VoucherEmpty, {
		localVue,
		propsData: {
			emptyTitle: 'empty title',
			emptyText: 'empty text'
		}
	});

	it('should render properly', () => {
		expect(wrapper.find('.voucher-empty').exists()).toBe(true);
		expect(wrapper.find('h1').text()).toBe('empty title');
		expect(wrapper.find('p').text()).toBe('empty text');
	});
});
