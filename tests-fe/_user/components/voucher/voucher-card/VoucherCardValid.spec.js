import { createLocalVue, shallowMount } from '@vue/test-utils';
import VoucherCardValid from 'Js/_user/components/voucher/voucher-card/VoucherCardValid';
import Vuex from 'vuex';

describe('VoucherCardValid.vue', () => {
	const localVue = createLocalVue();
	localVue.use(Vuex);
	const props = {
		voucherValidDate: '10 April 2020',
		isAvailable: false,
		label: 'label'
	};
	const store = {
		state: {
			isVoucherDetail: false
		}
	};

	const wrapper = shallowMount(VoucherCardValid, {
		localVue,
		propsData: props,
		store: new Vuex.Store(store)
	});

	it('should render the components properly', () => {
		expect(wrapper.find('.voucher-valid').exists()).toBe(true);
	});

	it('should add class detail when isVoucherDetail', async () => {
		const newStore = {
			state: {
				isVoucherDetail: true
			}
		};
		const wrapper = shallowMount(VoucherCardValid, {
			localVue,
			propsData: props,
			store: new Vuex.Store(newStore)
		});
		await wrapper.vm.$nextTick();

		expect(wrapper.find('.detail').exists()).toBe(true);
	});
});
