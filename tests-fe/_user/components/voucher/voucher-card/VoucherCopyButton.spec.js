import { createLocalVue, shallowMount } from '@vue/test-utils';
import VoucherCopyButton from 'Js/_user/components/voucher/voucher-card/VoucherCopyButton';
import mockVLazy from '../../../../utils/mock-v-lazy';
import Vuex from 'vuex';

jest.useFakeTimers();

describe('VoucherCopyButton.vue', () => {
	const localVue = createLocalVue();
	const props = {
		voucherId: 200,
		code: 'PROMO200',
		isMobile: false,
		isAvailable: true
	};
	const store = {
		state: {
			isVoucherDetail: false,
			authData: {
				all: {
					id: 33333
				}
			}
		}
	};
	const trackerProps = type => {
		return [
			type,
			[
				'[User] Voucher Code Copied',
				{
					user_id: 33333,
					voucher_id: props.voucherId,
					voucher_code: props.code,
					source_page: 'voucher list'
				}
			]
		];
	};
	localVue.use(Vuex);
	mockVLazy(localVue);
	let wrapper;
	const tracker = jest.fn();
	global.tracker = tracker;
	const $toasted = {
		show: jest.fn()
	};
	const tooltipMsg = ['Kode berhasil disalin', 'Gagal salin kode voucher'];

	beforeEach(() => {
		wrapper = shallowMount(VoucherCopyButton, {
			localVue,
			propsData: props,
			store: new Vuex.Store(store),
			mocks: { $toasted }
		});
	});

	it('should render components correctly', () => {
		expect(wrapper.find('.voucher-code__button').exists()).toBe(true);
	});

	it('button should be disable is not available', async () => {
		wrapper.setProps({
			isAvailable: false
		});
		await wrapper.vm.$nextTick();
		expect(wrapper.attributes('disabled')).toBe('disabled');
		expect(wrapper.find('.voucher-code__button.disable').exists()).toBe(true);
		expect(wrapper.find('.button-text.disabled').exists()).toBe(false);
	});

	it('should return correct pageLocation', async () => {
		expect(wrapper.vm.pageLocation).toEqual('voucher list');

		const newStore = {
			state: {
				isVoucherDetail: true
			}
		};
		const wrappers = shallowMount(VoucherCopyButton, {
			localVue,
			propsData: props,
			store: new Vuex.Store(newStore)
		});

		await wrapper.vm.$nextTick();
		expect(wrappers.vm.pageLocation).toEqual('voucher detail');
	});

	it('should copy promo text properly when being clicked', async () => {
		wrapper.vm.promoCodeCopied = jest.fn();
		wrapper.vm.$copyText = () => {
			return Promise.resolve();
		};

		const voucherBtn = wrapper.find('.voucher-code__button');
		voucherBtn.trigger('click');
		await wrapper.vm.$copyText;

		expect(global.tracker).toHaveBeenCalledWith(...trackerProps('moe'));
		expect(wrapper.vm.promoCodeCopied).toHaveBeenCalledWith(tooltipMsg[0]);
	});

	it('should return eror msg when copy promo text doesnt works properly', async () => {
		wrapper.vm.promoCodeCopied = jest.fn();
		wrapper.vm.$copyText = () => {
			return Promise.reject(new Error('Error'));
		};

		const voucherBtn = wrapper.find('.voucher-code__button');
		voucherBtn.trigger('click');
		await wrapper.vm.$copyText;

		expect(wrapper.vm.promoCodeCopied).toHaveBeenCalledWith(tooltipMsg[1]);
	});

	it('should shows tooltip for 1 sec', async () => {
		wrapper.vm.promoCodeCopied(tooltipMsg[0]);
		expect(wrapper.vm.copying).toBe(true);
		expect(wrapper.vm.tooltipMessage).toEqual(tooltipMsg[0]);
		expect(wrapper.vm.showTooltip).toBeTruthy();

		jest.advanceTimersByTime(1000);
		expect(wrapper.vm.showTooltip).toBeFalsy();
	});

	it('tosted should be appears in mobile ', () => {
		wrapper.setProps({ isMobile: true });

		wrapper.vm.promoCodeCopied();

		expect($toasted.show).toBeCalled();
		expect(wrapper.vm.copying).toBeTruthy();

		jest.advanceTimersByTime(1500);
		expect(wrapper.vm.copying).toBeFalsy();
	});

	it('should call copyPromoCode function on click', () => {
		const copyPromoButton = wrapper.find('.voucher-code__button');
		if (copyPromoButton.exists()) {
			wrapper.setMethods({ copyPromoCode: jest.fn() });
			copyPromoButton.trigger('click');
			expect(wrapper.vm.copyPromoCode).toBeCalled();
		}
	});
});
