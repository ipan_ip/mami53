import { shallowMount } from '@vue/test-utils';
import VoucherCardCode from 'Js/_user/components/voucher/voucher-card/VoucherCardCode';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

describe('VoucherCardCode.vue', () => {
	let wrapper;
	const props = {
		voucherId: 200,
		code: 'MAMI200',
		isAvailable: true
	};

	beforeEach(() => {
		wrapper = shallowMount(VoucherCardCode, {
			propsData: props
		});
		mockWindowProperty('innerWidth', 320);
		mockWindowProperty('isMobile', true);
	});

	it('should render the components properly', () => {
		expect(wrapper.find('.voucher-code').exists()).toBe(true);
	});

	it('isMobile should return true when innerWidth < 768', () => {
		expect(wrapper.vm.isMobile).toBe(true);
	});

	it('voucher code should being disable when !isAvailable', () => {
		wrapper.setProps({
			isAvailable: false
		});

		wrapper.vm.$nextTick(() => {
			const voucherCodeDisable = wrapper.find('.voucher-code__code.disable');
			const voucherTextDisable = wrapper.find('.voucher-code__text.disable');
			expect(voucherCodeDisable.exists()).toBe(true);
			expect(voucherTextDisable.exists()).toBe(true);
		});
	});
});
