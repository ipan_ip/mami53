import { createLocalVue, shallowMount } from '@vue/test-utils';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import BackToTopButton from 'Js/_user/components/voucher/BackToTopButton';

describe('BackToTopButton.vue', () => {
	const localVue = createLocalVue();
	mockVLazy(localVue);
	const wrapper = shallowMount(BackToTopButton, {
		localVue,
		propsData: {
			backToTop: () => {
				window.test = 'test';
			}
		}
	});

	it('should render properly', () => {
		expect(wrapper.find('img').element.getAttribute('data-src')).toBe(
			'/general/img/icons/ic_voucher_back_to_top.svg'
		);
	});

	it('should call backToTop prop when button is clicked', () => {
		wrapper.find('.back-to-top').trigger('click');
		expect(window.test).toBe('test');
	});
});
