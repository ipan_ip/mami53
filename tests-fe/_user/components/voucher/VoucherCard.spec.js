import { createLocalVue, shallowMount } from '@vue/test-utils';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import VoucherCard from 'Js/_user/components/voucher/VoucherCard.vue';
import Vuex from 'vuex';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

describe('VoucherCard.vue', () => {
	const localVue = createLocalVue();
	mockVLazy(localVue);
	localVue.use(Vuex);

	const mockTracker = jest.fn();
	const mockStore = {
		state: {
			authData: {
				all: {
					id: 'id'
				}
			}
		}
	};
	const wrapper = shallowMount(VoucherCard, {
		localVue,
		propsData: {
			image: { medium: 'medium-image-url' },
			voucherId: 12,
			code: 'code-test',
			title: 'title-test',
			isAvailable: true,
			label: 'label-test'
		},
		store: new Vuex.Store(mockStore),
		mocks: {
			$router: { push: jest.fn(), path: '/voucherku' },
			$store: { commit: jest.fn() }
		},
		stubs: ['router-link']
	});

	global.tracker = mockTracker;

	it('should render properly', () => {
		expect(wrapper.find('img').element.getAttribute('data-src')).toBe(
			'medium-image-url'
		);
		expect(wrapper.vm.voucherValidDate).toBe('kapan saja');

		wrapper.setProps({
			endDate: '2020-02-02'
		});
		expect(['hingga 2 Feb 2020', 'hingga Feb 2, 2020']).toContain(
			wrapper.vm.voucherValidDate
		);
	});

	it('should call the tracker when voucher-image is clicked', () => {
		const voucherImage = wrapper.find('.voucher-image');
		if (voucherImage.exists()) {
			voucherImage.trigger('click');
			expect(mockTracker).toHaveBeenCalled();
		}
	});

	it('should redirect to voucher detail page on click', () => {
		const voucherLink = wrapper.find('.voucher-card a');
		if (voucherLink.exists()) {
			voucherLink.trigger('click');
			expect(wrapper.$router.path).toBe('voucherku/detail');
		}
	});
});
