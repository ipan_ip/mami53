import { shallowMount, createLocalVue } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import App from 'Js/_user/components/App.vue';
import Vuex from 'vuex';
import VuexStore from './__mocks__/VuexStoreApp';
import {
	MockAxios,
	draft,
	profile,
	socialMedia
} from './__mocks__/mockAxios.js';

const localVue = createLocalVue();
localVue.use(Vuex);

const $router = { push: jest.fn() };
const $route = { path: '/' };
const swalError = jest.fn();
const Moengage = { add_user_attribute: jest.fn() };
const bugsnagClient = { notify: jest.fn() };

global.bugsnagClient = bugsnagClient;
global.Moengage = Moengage;

describe('App.vue', () => {
	const generateWrapper = mockAxiosOptions => {
		VuexStore.state.bookingNotification.draft = 0;
		VuexStore.state.isUserLoggedInViaSocial = false;
		VuexStore.state.userProfile = {};
		global.axios = new MockAxios(mockAxiosOptions);
		return shallowMount(App, {
			localVue,
			mocks: { $router, $route },
			methods: { swalError },
			store: new Vuex.Store(VuexStore),
			stubs: {
				NavbarCustom: '<div class="navbar-custom"</div>',
				RouterView: '<div class="router-view"></div>',
				BreadcrumbTrails: '<div class="breadcrumb-trails"></div>'
			}
		});
	};

	describe('Initial Render', () => {
		it('should render app wrapper', () => {
			const wrapper = generateWrapper();
			expect(wrapper.find('.app-wrapper').exists()).toBe(true);
		});
	});

	describe('Success fetch APIs', () => {
		it('should set draft booking count', async () => {
			jest.clearAllMocks();
			const wrapper = generateWrapper();
			await wrapper.vm.$nextTick();
			expect(wrapper.vm.$store.state.bookingNotification.draft).toBe(
				draft.data.total_of_draft
			);
		});

		it('should set user profile and user edit profile job', async () => {
			jest.clearAllMocks();
			const wrapper = generateWrapper();
			await wrapper.vm.$nextTick();
			expect(wrapper.vm.$store.state.userProfile).toEqual(profile.profile);
			expect(wrapper.vm.$store.state.userEditProfile.jobs).toBe(
				profile.profile.jobs
			);
		});

		it('should set social media status', async () => {
			jest.clearAllMocks();
			const wrapper = generateWrapper();
			wrapper.vm.getUserIsLoggedInViaSocial();
			await wrapper.vm.$nextTick();
			expect(wrapper.vm.$store.state.isUserLoggedInViaSocial).toBe(
				socialMedia.status
			);
		});
	});

	describe('Success fetch APIs with false status', () => {
		it('should not set draft booking count', async () => {
			jest.clearAllMocks();
			const wrapper = generateWrapper({ status: false });
			await wrapper.vm.$nextTick();
			expect(wrapper.vm.$store.state.bookingNotification.draft).toBe(0);
		});

		it('should not set user profile', async () => {
			jest.clearAllMocks();
			const wrapper = generateWrapper({ status: false });
			await wrapper.vm.$nextTick();
			expect(wrapper.vm.$store.state.userProfile).toEqual({});
		});

		it('should not set social media status', async () => {
			jest.clearAllMocks();
			const wrapper = generateWrapper({ status: false });
			await wrapper.vm.getUserIsLoggedInViaSocial();
			expect(wrapper.vm.$store.state.isUserLoggedInViaSocial).toBe(false);
		});
	});

	describe('Failed fetch APIs', () => {
		it('should not set draft booking count', async () => {
			jest.clearAllMocks();
			const wrapper = generateWrapper({ resolved: false });
			await wrapper.vm.$nextTick();
			expect(wrapper.vm.$store.state.bookingNotification.draft).toBe(0);
			expect(bugsnagClient.notify).toBeCalled();
		});

		it('should not set user profile', async () => {
			jest.clearAllMocks();
			const wrapper = generateWrapper({ resolved: false });
			await wrapper.vm.$nextTick();
			expect(wrapper.vm.$store.state.userProfile).toEqual({});
			await wrapper.vm.$nextTick();
			expect(bugsnagClient.notify).toBeCalled();
			expect(swalError).toBeCalled();
		});

		it('should not set social media status', async () => {
			jest.clearAllMocks();
			const wrapper = generateWrapper({ resolved: false });
			await wrapper.vm.getUserIsLoggedInViaSocial();
			expect(wrapper.vm.$store.state.isUserLoggedInViaSocial).toBe(false);
			await wrapper.vm.$nextTick();
			expect(bugsnagClient.notify).toBeCalled();
		});
	});
});
