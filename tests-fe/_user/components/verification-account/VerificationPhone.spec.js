import { createLocalVue, shallowMount } from '@vue/test-utils';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mockAxios from 'tests-fe/utils/mock-axios';
import 'tests-fe/utils/mock-debounce';
import VerificationPhone from 'Js/_user/components/verification-account/VerificationPhone.vue';
import 'tests-fe/utils/mock-vue';
import VeeValidate, { Validator } from 'vee-validate';
import id from 'vee-validate/dist/locale/id';

global.axios = mockAxios;
global.bugsnagClient = { notify: jest.fn() };
const localVue = createLocalVue();
mockVLazy(localVue);
localVue.directive('validate', jest.fn());
window.validator = new Validator();
Validator.localize('id', id);
localVue.use(VeeValidate, {
	locale: 'id'
});

window.location.reload = jest.fn();

describe('VerificationPhone.vue', () => {
	const wrapper = shallowMount(VerificationPhone, {
		localVue,
		mocks: {
			$store: {
				state: {
					userProfile: { phone: '1000', is_verify_phone_number: false }
				}
			},
			errors: { has: jest.fn(), first: jest.fn() }
		}
	});
	wrapper.setMethods({
		openSwalLoading: jest.fn(),
		closeSwalLoading: jest.fn(),
		swalError: jest.fn(),
		swalSuccess: jest.fn((...args) => {
			args[2]();
		})
	});

	it('should render properly when user is not verified yet', () => {
		expect(wrapper.vm.phoneVerification.stepOne).toBe(true);
		expect(wrapper.vm.isVerifyPhone).toBe(false);
		expect(wrapper.vm.userPhone).toBe('1000');
		expect(wrapper.find('.detail-text > p').text()).toBe('Belum verifikasi');
		expect(wrapper.find('.btn-list-with-button.--verification').text()).toBe(
			'Verifikasi'
		);
	});

	it('should show verification view (stepTwo) when user is not verified yet', async () => {
		// click button to show stepTwo
		wrapper.find('.btn-list-with-button.--verification').trigger('click');
		await wrapper.vm.$nextTick;
		const input = wrapper.find('.edit-detail input');

		expect(wrapper.vm.phoneVerification.stepTwo).toBe(true);
		expect(input.exists()).toBe(true);
		expect(wrapper.find('.edit-detail h1').text()).toBe('Nomor Handphone');

		wrapper.find('.btn.btn-success.--verification').trigger('click');
		expect(wrapper.vm.phoneVerification.stepOne).toBe(true);
	});

	it('should handle move to stepThree when user try to verify', async () => {
		wrapper.find('.btn-list-with-button.--verification').trigger('click');
		await wrapper.vm.$nextTick;
		const input = wrapper.find('.edit-detail input');
		// mock API response with status true
		axios.mockResolve({ data: { status: true } });

		input.setValue('08123535235');
		// need call this to wait until debounce function has finished
		await new Promise(resolve => setTimeout(resolve, 0));

		expect(wrapper.vm.userPhoneVerification).toBe('08123535235');
		expect(wrapper.vm.isDisabledButton).toBe(false);
		// click button to show stepThree
		wrapper.find('.btn.--verification').trigger('click');
		await wrapper.vm.$nextTick;
		expect(wrapper.vm.phoneVerification.stepThree).toBe(true);

		// set true to show button
		wrapper.vm.sendAgainOtp = true;
		await wrapper.vm.$nextTick;
		// mock API response with status true
		axios.mockResolve({ data: { status: false } });
		// click button to show stepThree
		wrapper.find('.send-back-otp').trigger('click');

		await mockAxios.post;
		await Promise.prototype.catch;

		expect(wrapper.vm.phoneVerification.stepThree).toBe(true);
		expect(wrapper.vm.swalError).toBeCalled();
		expect(window.bugsnagClient.notify).toBeCalled();
	});

	it('should verify phone number with otp', async done => {
		// API status false
		axios.mockResolve({ data: { status: false } });
		const verifyButton = wrapper.find('.btn-otp-verification');
		verifyButton.trigger('click');
		await mockAxios.post;
		expect(wrapper.vm.swalError).toBeCalled();

		// API status true
		await wrapper.vm.$nextTick;
		axios.mockResolve({ data: { status: true } });
		verifyButton.trigger('click');
		await mockAxios.post;
		expect(wrapper.vm.swalSuccess).toBeCalled();

		// catch error
		verifyButton.element.disabled = false;
		await wrapper.vm.$nextTick;
		axios.mockReject({});
		verifyButton.trigger('click');
		await mockAxios.post;
		await Promise.prototype.catch;
		expect(wrapper.vm.swalSuccess).toBeCalled();
		done();
	});

	it('should start timer correctly', () => {
		window.setInterval = jest.fn();
		wrapper.vm.startTimer(0);
		// run first argument of first call, to simulate reaching 0 second
		window.setInterval.mock.calls[0][0]();
		expect(window.setInterval).toBeCalled();
		expect(wrapper.vm.sendAgainOtp).toBe(true);
	});
});
