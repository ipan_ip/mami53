const verified = {
	state: {
		userProfile: {
			is_verify_email: true,
			email: 'jhon.doe@mail.com'
		}
	},
	mutations: {
		setEmail(state, payload) {
			state.userProfile.email = payload;
		}
	}
};

const notVerified = {
	state: {
		userProfile: {
			is_verify_email: false,
			email: 'jhon.doe@mail.com'
		}
	},
	mutations: {
		setEmail(state, payload) {
			state.userProfile.email = payload;
		}
	}
};

const VerificationEmailStore = { verified, notVerified };

export default VerificationEmailStore;
