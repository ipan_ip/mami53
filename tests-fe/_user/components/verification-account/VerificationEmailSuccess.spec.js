import { createLocalVue, shallowMount } from '@vue/test-utils';
import VerificationEmailSucces from 'Js/_user/components/verification-account/VerificationEmailSuccess';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import Vuex from 'vuex';
import storeCollection from '../__mocks__/VerificationEmailSuccess';

describe('VerificationEmailSuccess', () => {
	let wrapper;
	const localVue = createLocalVue();
	localVue.use(Vuex);
	mockVLazy(localVue);

	beforeEach(() => {
		wrapper = shallowMount(VerificationEmailSucces, {
			localVue,
			store: new Vuex.Store(storeCollection[0]),
			stubs: ['router-link']
		});
	});

	it('should render VerificationEmailSuccess properly', () => {
		expect(wrapper.find('.wrapper-email-success').exists()).toBe(true);
		expect(wrapper.find('p').text()).toBe(
			'Selamat, Email Kamu sudah terverifikasi'
		);
	});

	it('should set url to true if there is authData', () => {
		expect(wrapper.vm.url).toBe(true);
	});

	it('should set url to false if there is no authData', () => {
		wrapper = shallowMount(VerificationEmailSucces, {
			localVue,
			store: new Vuex.Store(storeCollection[1]),
			stubs: ['router-link']
		});
		expect(wrapper.vm.url).toBe(false);
		expect(wrapper.find('a').exists()).toBe(true);
	});

	it('should commit mutations', () => {
		const store = new Vuex.Store(storeCollection[0]);
		store.commit('setBreadcrumb', 'mamipay');
		store.commit('setMenu', 'no-menu');
		expect(store.state.profileBreadcrumb[2].url).toEqual('/isi-saldo');
		expect(store.state.userMenuActive).toBe('no-menu');
	});
});
