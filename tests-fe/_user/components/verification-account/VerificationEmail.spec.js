import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import VerificationEmailStore from 'VerificationEmail';

import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mockAxios from 'tests-fe/utils/mock-axios';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import debounce from 'tests-fe/utils/mock-debounce';
import VeeValidate, { Validator } from 'vee-validate';
import id from 'vee-validate/dist/locale/id';

import VerificationEmail from 'Js/_user/components/verification-account/VerificationEmail';

describe('VerificationEmail.vue', () => {
	const localVue = createLocalVue();
	localVue.use(Vuex);
	mockVLazy(localVue);

	window.validator = new Validator();
	Validator.localize('id', id);

	localVue.directive('validate', jest.fn());
	localVue.use(VeeValidate, {
		locale: 'id'
	});

	global.axios = mockAxios;
	global.debounce = debounce;

	const notify = jest.fn();
	mockWindowProperty('bugsnagClient', { notify });

	describe('Render Verified Email', () => {
		let wrapper;

		beforeEach(() => {
			wrapper = shallowMount(VerificationEmail, {
				localVue,
				store: new Vuex.Store(VerificationEmailStore.verified)
			});
		});

		it('should render verification email properly', () => {
			expect(wrapper.find('button').text()).toBe('Ubah');
			expect(wrapper.find('.list-with-button').exists()).toBeTruthy();
		});

		it('should show verified email', () => {
			const store = new Vuex.Store(VerificationEmailStore.verified);
			expect(wrapper.find('p').text()).toBe(store.state.userProfile.email);
		});

		it('should set changeEmail to true', async () => {
			await wrapper.find('button').trigger('click');
			expect(wrapper.vm.changeEmail).toBe(true);
			expect(wrapper.vm.$validator.errors.any()).toBeFalsy();
		});

		it('should render input field', async () => {
			await wrapper.find('button').trigger('click');
			expect(wrapper.find('.input-email').exists()).toBeTruthy();
		});
	});

	describe('Change email', () => {
		let wrapper;

		beforeEach(() => {
			wrapper = shallowMount(VerificationEmail, {
				localVue,
				store: new Vuex.Store(VerificationEmailStore.verified)
			});

			wrapper.setData({ changeEmail: true });
			wrapper.setMethods({
				openSwalLoading: jest.fn(),
				closeSwalLoading: jest.fn(),
				swalError: jest.fn()
			});
		});

		it('should show error message on wrong format input and empty', async () => {
			expect(wrapper.vm.$validator.errors.any()).toBeFalsy();

			const input = wrapper.find('input');

			// Validate input not email
			input.setValue('thisisnotemailformat');
			await new Promise(resolve => setTimeout(resolve, 0));
			expect(wrapper.vm.userEmail).toBe('thisisnotemailformat');
			expect(wrapper.vm.$validator.errors.any()).toBeTruthy();

			// Validate input empty
			input.setValue('');
			await new Promise(resolve => setTimeout(resolve, 0));
			expect(wrapper.vm.userEmail).toBe('');
			expect(wrapper.vm.$validator.errors.any()).toBeTruthy();
		});

		it('should enabled button', async done => {
			const input = wrapper.find('input');
			const button = wrapper.findAll('button').at(0);

			expect(button.attributes('disabled')).toBe('disabled');
			input.setValue('jhon.snow@mail.com');

			await new Promise(resolve => setTimeout(resolve, 0));
			expect(wrapper.vm.userEmail).toBe('jhon.snow@mail.com');
			expect(wrapper.vm.$validator.errors.any()).toBeFalsy();
			expect(button.attributes('disabled')).not.toBe('disabled');

			done();
		});

		it('should return status false', async done => {
			const store = new Vuex.Store(VerificationEmailStore.verified);
			const button = wrapper.findAll('button').at(0);

			// API status false
			axios.mockResolve({
				data: { status: false, meta: { message: 'gagal' } }
			});
			button.trigger('click');
			expect(wrapper.vm.openSwalLoading).toBeCalled();
			await mockAxios.post;
			expect(wrapper.vm.swalError).toBeCalled();
			expect(wrapper.vm.userEmail).toBe(store.state.userProfile.email);
			expect(wrapper.vm.btnChangeConfirmDisabled).toBe(false);

			done();
		});

		it('should change user email', async done => {
			const button = wrapper.findAll('button').at(0);

			// API status true
			await wrapper.vm.$nextTick;
			axios.mockResolve({ data: { status: true } });
			button.trigger('click');
			expect(wrapper.vm.openSwalLoading).toBeCalled();
			await mockAxios.post;
			expect(wrapper.vm.changeEmail).toBe(false);
			expect(wrapper.vm.notifEmail).toBe(true);
			expect(wrapper.vm.btnChangeConfirmDisabled).toBe(false);
			expect(wrapper.vm.closeSwalLoading).toBeCalled();

			done();
		});

		it('should reject request', async done => {
			const button = wrapper.findAll('button').at(0);

			// API Rejected
			await wrapper.vm.$nextTick;
			axios.mockReject({ error: 'Rejected' });
			button.trigger('click');
			expect(wrapper.vm.openSwalLoading).toBeCalled();
			await mockAxios.post;
			await Promise.prototype.catch;
			expect(wrapper.vm.closeSwalLoading).toBeCalled();
			expect(bugsnagClient.notify).toBeCalled();
			expect(wrapper.vm.swalError).toBeCalled();

			done();
		});

		it('should commit email', () => {
			const store = new Vuex.Store(VerificationEmailStore.verified);
			store.commit('setEmail', 'jhon.snow@mail.com');
			expect(store.state.userProfile.email).toBe('jhon.snow@mail.com');
		});

		it('should close edit email', async () => {
			const store = new Vuex.Store(VerificationEmailStore.verified);
			const button = wrapper.findAll('button').at(1);

			button.trigger('click');
			expect(wrapper.vm.changeEmail).toBe(false);
			expect(wrapper.vm.userEmail).toBe(store.state.userProfile.email);
		});
	});

	describe('Render Not Verified Email', () => {
		let wrapper;

		beforeEach(() => {
			wrapper = shallowMount(VerificationEmail, {
				localVue,
				store: new Vuex.Store(VerificationEmailStore.notVerified)
			});
		});

		it('should render verify button', () => {
			expect(wrapper.find('.--verification').exists()).toBeTruthy();
			expect(wrapper.find('button').text()).toBe('Verifikasi');
		});

		it('should show massages "Belum verifikasi"', () => {
			expect(wrapper.find('p').text()).toBe('Belum verifikasi');
		});

		it('should show email need confirm', async () => {
			wrapper.setData({ changeEmail: false, notifEmail: true });
			await wrapper.vm.$nextTick;
			expect(
				wrapper
					.findAll('p')
					.at(1)
					.text()
			).toBe('Cek email kamu, kami sudah mengirimkan link untuk konfirmasi');
		});
	});
});
