import { shallowMount, createLocalVue } from '@vue/test-utils';
import DetailHistoryPrice from 'Js/_user/components/history-transaction-detail/DetailHistoryPrice.vue';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mockAxios from 'tests-fe/utils/mock-axios';

import mockComponent from 'tests-fe/utils/mock-component';

import _isEmpty from 'lodash/isEmpty';
import dayjs from 'dayjs';

jest.mock('file-saver', () => {
	return { saveAs: jest.fn() };
});

const stubs = {
	transition: {
		template: '<div><slot /></div>'
	},
	DetailHistoryAlert: mockComponent,
	MamiButtonLoading: mockComponent
};

const mocks = {
	$dayjs: dayjs,
	$emit: jest.fn()
};

const propsData = {
	paidDate: '',
	invoiceNumber: 'DP1234567890',
	listCost: [
		{
			name: 'test',
			amount: 1,
			type: 'base_cost'
		},
		{
			name: 'test_2',
			amount: 2,
			type: 'dp_cost'
		},
		{
			name: 'test_3',
			amount: 3,
			type: 'discount'
		},
		{
			name: 'test_4',
			amount: 4,
			type: 'fine'
		},
		{
			name: 'test_5',
			amount: 5,
			type: 'deposit'
		},
		{
			name: 'test_6',
			amount: 6,
			type: 'admin'
		},
		{
			name: 'test_7',
			amount: 7,
			type: 'other'
		},
		{
			name: 'test_8',
			amount: 8,
			type: 'fixed'
		}
	],
	totalCost: 0,
	invoiceUrl: '',
	isFlashSale: {}
};

global._isEmpty = _isEmpty;
global.saveAs = jest.fn();
global.bugsnagClient = {
	notify: jest.fn()
};
global.axios = mockAxios;

const localVue = createLocalVue();
mockVLazy(localVue);

describe('DetailHistoryPrice', () => {
	describe('should render properly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = shallowMount(DetailHistoryPrice, {
				localVue,
				stubs,
				propsData,
				mocks
			});
		});

		it('should render component properly', () => {
			expect(wrapper.find('#userKostClaimDetailBill').exists()).toBeTruthy();
		});

		it('should render listCost props as [] when not received any prop', () => {
			wrapper.destroy();
			wrapper = wrapper = shallowMount(DetailHistoryPrice, {
				localVue,
				stubs,
				mocks
			});

			expect(wrapper.vm.listCost).toEqual([]);
		});
	});

	describe('render computed data properly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = shallowMount(DetailHistoryPrice, {
				localVue,
				stubs,
				propsData,
				mocks
			});
		});

		it('should render isDownPayment value properly', () => {
			wrapper.setProps({
				invoiceNumber: 'DP1234567890'
			});

			expect(wrapper.vm.isDownPayment).toBeTruthy();
		});

		it.skip('should render formattedTotalCost value properly', () => {
			wrapper.setProps({
				totalCost: '1000000'
			});

			expect(wrapper.vm.formattedTotalCost).toBe('Rp1.000.000');
		});

		it('should render paidAtDate properly', () => {
			wrapper.setProps({
				paidDate: '2020-08-09 10:11:12'
			});

			// format YYYYMMDD
			expect(wrapper.vm.paidAtDate).toBe('20200809');
		});

		it('should render detailDate properly', () => {
			wrapper.setProps({
				paidDate: '2020-08-09 10:11:12'
			});

			// format D MMM YYYY
			expect(wrapper.vm.detailDate).toBe('9 Aug 2020');
		});

		it('should render baseCost properly', () => {
			expect(wrapper.vm.baseCost).toEqual({
				name: 'test',
				amount: 1,
				type: 'base_cost'
			});
		});

		it('should render dpCost properly', () => {
			expect(wrapper.vm.dpCost).toEqual({
				name: 'test_2',
				amount: 2,
				type: 'dp_cost'
			});
		});

		it('should render discount properly', () => {
			expect(wrapper.vm.discount).toEqual({
				name: 'test_3',
				amount: 3,
				type: 'discount'
			});
		});

		it('should render fine properly', () => {
			expect(wrapper.vm.fine).toEqual({
				name: 'test_4',
				amount: 4,
				type: 'fine'
			});
		});

		it('should render deposit properly', () => {
			expect(wrapper.vm.deposit).toEqual({
				name: 'test_5',
				amount: 5,
				type: 'deposit'
			});
		});

		it('should render admin properly', () => {
			expect(wrapper.vm.admin).toEqual({
				name: 'test_6',
				amount: 6,
				type: 'admin'
			});
		});

		it('should render otherCost properly', () => {
			expect(wrapper.vm.otherCost).toEqual([
				{
					name: 'test_7',
					amount: 7,
					type: 'other'
				},
				{
					name: 'test_8',
					amount: 8,
					type: 'fixed'
				}
			]);
		});

		it('should render toggleTitle properly', async () => {
			// truthy condition
			await wrapper.setData({
				isShowDetail: true
			});
			expect(wrapper.vm.toggleTitle).toBe('Sembunyikan');

			// falsy condition
			await wrapper.setData({
				isShowDetail: false
			});
			expect(wrapper.vm.toggleTitle).toBe('Lihat Rincian');
		});
	});

	describe('call functions properly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = shallowMount(DetailHistoryPrice, {
				localVue,
				stubs,
				propsData,
				mocks,
				methods: {
					swalError: jest.fn()
				}
			});
		});

		it('should set showAlert based on value received when calling closeAlert method', () => {
			wrapper.vm.closeAlert(true);
			expect(wrapper.vm.showAlert.show).toBeTruthy();
		});

		it('should return shortlink url when calling getShortlinkCode method with given url', () => {
			const url = wrapper.vm.getShortlinkCode(
				'https://pay.server.com/invoice/12345'
			);

			expect(url).toBe('12345');
		});

		it.skip('should return formatted number when calling formatToRupiah method', () => {
			const totalFee = wrapper.vm.formatToRupiah('1000000');

			expect(totalFee).toBe('1.000.000');
		});

		it("should return object's empty state when calling isEmpty method", () => {
			const isObjectEmpty = wrapper.vm.isEmpty({});

			expect(isObjectEmpty).toBeTruthy();
		});

		it('should download pdf file when calling pdfDownloader method and the response was successful', async () => {
			wrapper.setProps({
				invoiceUrl: 'https://pay.server.com/invoice/12345'
			});
			await axios.mockResolve({
				data: ''
			});
			await wrapper.vm.pdfDownloader();
			await wrapper.vm.$nextTick();
			expect(wrapper.vm.showAlert.show).toBeTruthy();
		});

		it('should call bugsnagClient and swalError when calling pdfDownloader method and the response was not successful', async () => {
			const swalErrorSpy = jest.spyOn(wrapper.vm, 'swalError');
			wrapper.setProps({
				invoiceUrl: 'https://pay.server.com/invoice/12345'
			});
			await axios.mockReject('error');
			await wrapper.vm.pdfDownloader();
			await wrapper.vm.$nextTick();

			expect(global.bugsnagClient.notify).toBeCalled();
			expect(swalErrorSpy).toBeCalled();
			swalErrorSpy.mockRestore();
		});
	});
});
