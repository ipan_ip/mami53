import { mount, createLocalVue } from '@vue/test-utils';
import DetailHistoryTransaction from 'Js/_user/components/history-transaction-detail/DetailHistoryTransaction.vue';
import _get from 'lodash/get';
import mockComponent from 'tests-fe/utils/mock-component';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

import dayjs from 'dayjs';

const stubs = {
	DetailHistoryPrice: mockComponent
};

const mocks = {
	_get,
	$dayjs: dayjs
};

const propsData = {
	transaction: {
		flashSale: '',
		invoiceNumber: '',
		paidDate: '2020-08-09 10:11:12',
		invoiceName: '',
		price: '',
		kostName: '',
		kostType: 'mixed',
		roomNumber: '',
		rentType: 'day',
		kostSlug: '',
		kostImage: '',
		ownerName: '',
		tenantName: '',
		invoiceUrl: '',
		status: '',
		statusDescription: '',
		detailCost: '',
		channelId: ''
	}
};

const localVue = createLocalVue();

describe('DetailHistoryTransaction', () => {
	describe('it should render component properly', () => {
		let wrapper;

		beforeEach(() => {
			wrapper = mount(DetailHistoryTransaction, {
				localVue,
				stubs,
				propsData,
				mocks
			});
		});

		it('should render component properly', () => {
			expect(wrapper.find('.detail-history-transaction').exists()).toBeTruthy();
		});
	});

	describe('should render default props properly', () => {
		let wrapper;

		beforeEach(() => {
			wrapper = mount(DetailHistoryTransaction, {
				localVue,
				stubs,
				mocks
			});
		});

		it('should render default transaction prop properly', () => {
			expect(wrapper.vm.transaction).toEqual({
				flashSale: '',
				invoiceNumber: '',
				paidDate: '',
				invoiceName: '',
				price: '',
				kostName: '',
				kostType: '',
				roomNumber: '',
				rentType: '',
				kostSlug: '',
				kostImage: '',
				ownerName: '',
				tenantName: '',
				invoiceUrl: '',
				status: '',
				statusDescription: '',
				detailCost: '',
				channelId: ''
			});
		});
	});

	describe('it should render computed value properly', () => {
		let wrapper;

		beforeEach(() => {
			wrapper = mount(DetailHistoryTransaction, {
				localVue,
				stubs,
				propsData,
				mocks
			});
		});

		it('should render rentTypeLabel properly', () => {
			expect(wrapper.vm.rentTypeLabel).toBe('Per Hari');
		});

		it('should render typeKost properly', () => {
			expect(wrapper.vm.typeKost).toBe('Campur');
		});

		it('should render typeKostStyle properly', () => {
			expect(wrapper.vm.typeKostStyle).toBe('is-all');
		});

		it('should render detailDate properly', () => {
			expect(wrapper.vm.detailDate).toBe('9 Aug 2020, 10:11');
		});

		it('should render price properly', () => {
			expect(wrapper.vm.price).toBe('Rp0');
		});
	});
});
