import { mount, createLocalVue } from '@vue/test-utils';
import DetailHistoryAlert from 'Js/_user/components/history-transaction-detail/DetailHistoryAlert.vue';

import mockWindowProperty from 'tests-fe/utils/mock-window-property';

const stubs = {
	transition: {
		template: '<div> <slot /> </div>'
	}
};

const mocks = {
	$emit: jest.fn()
};

const propsData = {
	show: true
};

const localVue = createLocalVue();

mockWindowProperty('clearTimeout', jest.fn());
mockWindowProperty(
	'setTimeout',
	jest
		.fn(fn => {
			fn && fn();
		})
		.mockReturnValue(4000)
);

describe('DetailHistoryAlert', () => {
	let wrapper;

	beforeEach(() => {
		wrapper = mount(DetailHistoryAlert, {
			localVue,
			stubs,
			propsData,
			mocks
		});
	});

	it('should render component properly', () => {
		expect(wrapper.find('.booking-alert').exists()).toBeTruthy();
	});

	it('should call $emit and window.clearTimeout when clicking on booking alert image', async () => {
		const emitSpy = jest.spyOn(wrapper.vm, '$emit');
		wrapper.setProps({
			show: true
		});
		await wrapper.vm.$nextTick();
		const image = wrapper.find('img');
		if (image && image.exists()) {
			await image.trigger('click');
			expect(window.clearTimeout).toBeCalled();
			expect(emitSpy).toBeCalled();
		}
		emitSpy.mockRestore();
	});

	it('should call setActiveAlertTimeout method on show value change', async () => {
		await wrapper.setProps({
			show: false
		});
		await wrapper.vm.$nextTick();
		await wrapper.setProps({
			show: true,
			duration: 4000
		});

		await wrapper.vm.$nextTick();
		expect(window.clearTimeout).toBeCalled();
		expect(window.setTimeout).toBeCalled();
		expect(wrapper.vm.activeTimeout).toBe(4000);
	});
});
