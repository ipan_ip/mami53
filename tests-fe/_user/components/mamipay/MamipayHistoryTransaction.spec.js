import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import moment from 'moment';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import MamipoiyHistoryTransaction from 'Js/_user/components/mamipay/MamipayHistoryTransaction.vue';

const mockStore = {
	state: {
		authData: {
			all: {
				id: '1'
			}
		},
		userHistory: 'test'
	},

	mutations: {
		setMenu: jest.fn()
	},

	actions: {
		getHistory: jest.fn()
	}
};

describe('MamipoinHistoryTransaction.vue', () => {
	const localVue = createLocalVue();
	mockVLazy(localVue);
	localVue.use(Vuex);
	global.moment = moment;
	const mountComponent = () => {
		return shallowMount(MamipoiyHistoryTransaction, {
			localVue,
			store: new Vuex.Store(mockStore)
		});
	};

	let wrapper;
	beforeEach(() => {
		wrapper = mountComponent();
	});

	describe('render component MamipayHistoryTransaction correctly', () => {
		it('should render title with content "Riwayat Transaksi"', () => {
			const title = wrapper.find('.title');
			expect(title.text()).toBe('Riwayat Transaksi');
		});

		it('should render wrapper page correctly', () => {
			expect(wrapper.find('.form-wrapper')).toBeTruthy();
		});
	});

	describe('computed value in MamipayHistoryTransaction correctly', () => {
		it('should return id User from store', () => {
			expect(wrapper.vm.idUser).toBe('1');
		});
		it('should return userHistory from store', () => {
			expect(wrapper.vm.userHistory).toBe('test');
		});
	});

	describe('execution method in MamipayHistoryTransaction', () => {
		it('should return time with format day month and year Completely', () => {
			expect(wrapper.vm.formatTime(parseInt('2018–01–30T12:34:56+00:00'))).toBe(
				wrapper.vm.formatTime(parseInt('2018–01–30T12:34:56+00:00'))
			);
		});

		it('should return price in thousand format ', () => {
			expect(wrapper.vm.formatPrice(1000)).toBe('1.000');
		});
	});
});
