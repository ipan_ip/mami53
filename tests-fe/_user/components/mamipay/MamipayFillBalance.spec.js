import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import MamipayFillBalance from 'Js/_user/components/mamipay/MamipayFillBalance.vue';

const mockStore = {
	state: {
		virtualAccount: { va_number_bni: '' }
	},
	mutations: {
		setBreadcrumb: jest.fn(),
		setMenu: jest.fn()
	}
};

describe('MamipayFillBalance.vue', () => {
	const localVue = createLocalVue();
	localVue.use(Vuex);
	mockVLazy(localVue);

	const mountComponent = () => {
		return shallowMount(MamipayFillBalance, {
			localVue,
			store: new Vuex.Store(mockStore)
		});
	};

	let wrapper;

	beforeEach(() => {
		wrapper = mountComponent();
	});

	describe('render component MamiPayFillBalance', () => {
		it('should render title with content "Isi Saldo"', () => {
			const title = wrapper.find('.title');
			expect(title.text()).toBe('Isi Saldo');
		});
	});

	describe('computed value in MamipayFillBalance component', () => {
		describe('vaBni', () => {
			it('should return number of virtual acount BNI with "xxxx xxxx xxxx" when store is false', () => {
				expect(wrapper.vm.vaBni).toBe('xxxx xxxx xxxx');
			});
		});
	});
});
