import { createLocalVue, shallowMount } from '@vue/test-utils';
import mockDebounce from 'tests-fe/utils/mock-debounce';
import mockAxios from 'tests-fe/utils/mock-axios';
import Vuex from 'vuex';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import MamipayWithdrawBalance from 'Js/_user/components/mamipay/MamipayWithdrawBalance.vue';

const mockStore = {
	state: {
		userBalance: '4',
		bankList: {
			id: '1',
			name: 'test Bank'
		}
	},
	actions: {
		getBankList: jest.fn()
	},
	mutations: {
		setMenu: jest.fn()
	}
};

const mocks = {
	openSwalLoading: jest.fn(),
	closeSwalLoading: jest.fn(),
	swalError: jest.fn()
};

describe('MamipoinWithdrawBalance.vue', () => {
	const localVue = createLocalVue();
	localVue.use(Vuex);
	mockVLazy(localVue);
	global.debounce = mockDebounce;
	global.axios = mockAxios;
	window.alert = jest.fn();
	window.bugsnagClient = { notify: jest.fn() };

	const mountComponent = () => {
		return shallowMount(MamipayWithdrawBalance, {
			localVue,
			mocks,
			store: new Vuex.Store(mockStore)
		});
	};

	let wrapper;

	beforeEach(() => {
		wrapper = mountComponent();
		global.swal = jest.fn().mockImplementation(() => Promise.resolve());
	});

	describe('render component MamipayWithdrawBalance', () => {
		it('should render title woth content "Tarik Saldo"', () => {
			const title = wrapper.find('.title');
			expect(title.text()).toBe('Tarik Saldo');
		});

		it('should render component inputPin and  WithdrawAddBank when showPinModal is true', () => {
			wrapper.vm.showPinModal = true;
			expect(wrapper.findComponent({ name: 'inputPin' })).toBeTruthy();
		});
	});

	describe('computed value in MamipayWithdrawBalance', () => {
		describe('totalCost', () => {
			it('should return total cost from balance + costAdmin', () => {
				wrapper.vm.balance = 1;
				wrapper.vm.costAdmin = 1;
				expect(wrapper.vm.totalCost).toBe(2);
			});
		});

		describe('getPrimaryBank', () => {
			it('should return sll primary bank', () => {
				expect(wrapper.vm.getPrimaryBank).toEqual({
					id: '1',
					name: 'test Bank'
				});
			});
		});

		describe('balance', () => {
			it('should return balance value when input more than getBalance', () => {
				wrapper.vm.inputBalance = 5;
				expect(wrapper.vm.balance).toBe(5);
			});

			it('should return balance value when input more than getBalance', () => {
				wrapper.vm.inputBalance = '2';
				expect(wrapper.vm.balance).toBe('2');
			});

			it('should return userBalance value from store', () => {
				wrapper.vm.inputBalance = '2';
				expect(wrapper.vm.getBalance).toBe('4');
			});

			it('should return listAccounts value from store', () => {
				// wrapper.vm.listAccounts = '2';
				expect(wrapper.vm.listAccounts).toEqual({ id: '1', name: 'test Bank' });
			});
		});
	});

	describe('execute method in MamipayWithdrawBalance', () => {
		it('should set negation value in showPinModal data', () => {
			wrapper.vm.showPinModal = true;
			expect(wrapper.vm.insertPin()).toBeFalsy();
		});

		it('should show swet alert to confirm before delete account', () => {
			wrapper.vm.showCancelButton = false;
			wrapper.vm.showCloseButton = false;

			wrapper.vm.deleteAccountConfirm();
			wrapper.vm.$nextTick(() => {
				const spySwal = jest.spyOn(global, 'swal');
				expect(spySwal).toBeCalled();
				spySwal.mockRestore();
			});
		});

		it('should show swet alert to inform that no Bank Account for this user', () => {
			wrapper.vm.$store.state.bankList = '';
			wrapper.vm.withdrawBalance();
			wrapper.vm.$nextTick(() => {
				const spySwal = jest.spyOn(global, 'swal');
				expect(spySwal).toBeCalled();
				spySwal.mockRestore();
			});
		});

		it('should show swet alert to inform "Minimal 10.000 untuk tarik saldo" this user', () => {
			wrapper.vm.$store.state.bankList = 1;
			wrapper.vm.inputBalance = '';
			wrapper.vm.$nextTick(() => {
				wrapper.vm.withdrawBalance();
				expect(wrapper.vm.errorInputBalance).toBeTruthy();
			});
		});

		it('should confirm balance to next step withdraw', () => {
			wrapper.vm.$store.state.bankList = 1;
			wrapper.vm.inputBalance = 11000;
			wrapper.vm.$nextTick(() => {
				wrapper.vm.withdrawBalance();
				expect(wrapper.vm.errorInputBalance).toBeTruthy();
			});
		});

		it('should reload page when withdraw successe', () => {
			window.location.reload = jest.fn();
			wrapper.vm.withdrawProcessOk();
			expect(window.location.reload).toBeCalled();
		});

		it('should open swal loader when request are sending to server ', () => {
			wrapper.vm.$store.state.bankList = [
				{
					id: '1',
					name: 'test Bank'
				}
			];
			wrapper.vm.$nextTick(() => {
				wrapper.vm.withdrawProcess();
				expect(wrapper.vm.showPinModal).toBeFalsy();
			});
		});

		it('should show alert to inform that account only can be create is 3', () => {
			wrapper.vm.$store.state.bankList = [
				{
					id: '1',
					name: 'test Bank'
				},
				{
					id: '2',
					name: 'test Bank'
				},
				{
					id: '3',
					name: 'test Bank'
				}
			];

			wrapper.vm.$nextTick(() => {
				wrapper.vm.addNewAccount();
				const spySwal = jest.spyOn(global, 'swal');
				expect(spySwal).toBeCalled();
				spySwal.mockRestore();
			});
		});

		it('should show alert to inform that account only can be create is 3', () => {
			wrapper.vm.$store.state.bankList = [
				{
					id: '1',
					name: 'test Bank'
				},
				{
					id: '2',
					name: 'test Bank'
				}
			];

			wrapper.vm.$nextTick(() => {
				wrapper.vm.addNewAccount();
				expect(wrapper.vm.showPinModal).toBeFalsy();
			});
		});

		it('should render swal to confirm that user will be change account primary', () => {
			wrapper.vm.selectAccount();
			wrapper.vm.$nextTick(() => {
				const spySwal = jest.spyOn(global, 'swal');
				expect(spySwal).toBeCalled();
				spySwal.mockRestore();
			});
		});

		it('should reset value in inputbalance to Be 0', () => {
			wrapper.vm.removeText();
			expect(wrapper.vm.inputBalance).toBe(0);
		});

		it(' should delete account by id that pass in function deleteAccount() and close loader', () => {
			axios.mockResolve({ status: true });
			const closeLoaderSpy = jest.spyOn(wrapper.vm, 'closeSwalLoading');
			wrapper.vm.deleteAccount();
			expect(closeLoaderSpy).toBeCalled();
			closeLoaderSpy.mockRestore();
		});

		it(' should show alert when request deleted account get error', () => {
			axios.mockReject({ status: false });
			const alertSpy = jest.spyOn(window, 'alert');
			wrapper.vm.deleteAccount();
			expect(alertSpy).toBeCalled();
			alertSpy.mockRestore();
		});

		it(' should close swalLoading when request update Account has successed', () => {
			axios.mockResolve({ status: true });
			const closeLoaderSpy = jest.spyOn(wrapper.vm, 'closeSwalLoading');
			wrapper.vm.settingAccount();
			expect(closeLoaderSpy).toBeCalled();
			closeLoaderSpy.mockRestore();
		});

		it(' should show alert when request updated account get error from server', () => {
			axios.mockReject({ status: false });
			const alertSpy = jest.spyOn(window, 'alert');
			wrapper.vm.settingAccount();
			expect(alertSpy).toBeCalled();
			alertSpy.mockRestore();
		});
	});
});
