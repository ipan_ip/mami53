import { createLocalVue, shallowMount } from '@vue/test-utils';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mockAxios from 'tests-fe/utils/mock-axios';
import InputPin from 'Js/_user/components/mamipay/InputPin.vue';

const propsData = {
	show: true
};

const mocks = {
	openSwalLoading: jest.fn(),
	closeSwalLoading: jest.fn(),
	swalError: jest.fn(),
	$emit: jest.fn()
};

describe('InputPin.vue', () => {
	const localVue = createLocalVue();
	mockVLazy(localVue);
	global.axios = mockAxios;
	window.alert = jest.fn();
	const mountComponent = () => {
		return shallowMount(InputPin, {
			localVue,
			mocks,
			propsData
		});
	};

	let wrapper;

	beforeEach(() => {
		wrapper = mountComponent();
	});

	describe('render componen InputPin.vue', () => {
		it('should render component title with content "Konfirmasi PIN"', () => {
			const modalTitle = wrapper.find('.modal-title');
			expect(modalTitle.text()).toBe('Konfirmasi PIN');
		});

		it('should not show modal when props "show" is false', () => {
			wrapper.setProps({ show: false });
			wrapper.vm.$nextTick(() => {
				expect(wrapper.find('wrapper-pin').exists()).toBeFalsy();
			});
		});
	});

	describe('execute method in InputPoin', () => {
		it('should render swall Loader when function are called', async () => {
			const swalLoading = jest.spyOn(wrapper.vm, 'openSwalLoading');
			wrapper.vm.checkValid();
			expect(swalLoading).toBeCalled();
			swalLoading.mockRestore();
		});
	});
	describe('execute method in InputPoin', () => {
		it('should show swall error when response from API is "false"', async () => {
			await axios.mockResolve({
				data: {
					status: false
				}
			});
			await wrapper.vm.$nextTick();
			await wrapper.vm.checkValid();
			const swalError = jest.spyOn(wrapper.vm, 'swalError');
			await expect(swalError).toBeCalled();
			swalError.mockRestore();
		});

		it('should emit function when status from API is "true"', async () => {
			await axios.mockResolve({
				data: {
					status: true
				}
			});
			await wrapper.vm.$nextTick();
			await wrapper.vm.checkValid();
			await expect(wrapper.emitted().action).toBeTruthy();
		});
	});
});
