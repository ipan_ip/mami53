import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import mockAxios from 'tests-fe/utils/mock-axios';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mockDebounce from 'tests-fe/utils/mock-debounce';
import WithdrawAddBank from 'Js/_user/components/mamipay/WithdrawAddBank.vue';
jest.mock('lodash/debounce', () => jest.fn(fn => fn));
jest.useFakeTimers();
const mocks = {
	errors: {
		has: jest.fn(),
		first: jest.fn()
	},
	openSwalLoading: jest.fn(),
	closeSwalLoading: jest.fn(),
	swalError: jest.fn()
};

const propsData = {
	bankList: {
		id: '1',
		name: 'test Bank'
	},
	showModal: false
};

const mockStore = {
	state: {
		userBalance: '4',
		bankList: {
			id: '1',
			name: 'test Bank'
		},
		authData: { all: { id: 1 } }
	},
	actions: {
		getBankList: jest.fn()
	},
	mutations: {
		setMenu: jest.fn()
	}
};

describe('WithdrawAddBank.vue', () => {
	const localVue = createLocalVue();
	localVue.use(Vuex);
	mockVLazy(localVue);
	global.axios = mockAxios;
	global.validator = {
		attach: jest.fn(),
		validate: jest.fn()
	};
	global.debounce = jest.fn(() => {
		return {
			debounce: mockDebounce
		};
	});

	window.alert = jest.fn();
	window.bugsnagClient = { notify: jest.fn() };
	localVue.directive('validate', jest.fn());

	const mountComponent = () => {
		return shallowMount(WithdrawAddBank, {
			localVue,
			mocks,
			propsData,
			store: new Vuex.Store(mockStore)
		});
	};

	let wrapper;
	beforeEach(() => {
		wrapper = mountComponent();
		global.swal = jest.fn().mockImplementation(() => Promise.resolve());
	});

	describe('render withdrawAddBank components correctly', () => {
		it('should render title with text content "Tambah Akun"', () => {
			const title = wrapper.find('.modal-title');
			expect(title.text()).toBe('Tambah Akun');
		});

		it('should render label with text content "Nama Bank"', () => {
			const title = wrapper.find('.label-form');
			expect(title.text()).toBe('Nama Bank');
		});
	});

	describe('execute method in withdrawAddBank component', () => {
		it('should sending create account and showModal is false when status is true', () => {
			axios.mockResolve({ data: { status: true } });
			wrapper.vm.createBankAccount();
			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.showModal).toBeFalsy();
			});
		});

		it('should sending create account and showModal is false when status is true', () => {
			axios.mockResolve({ data: { status: false } });
			const swallErrorSpy = jest.spyOn(wrapper.vm, 'swalError');
			wrapper.vm.createBankAccount();

			wrapper.vm.$nextTick(() => {
				expect(swallErrorSpy).toBeCalled();
				swallErrorSpy.mockRestore();
			});
		});

		it('should sending create account and show alert when status false', () => {
			axios.mockResolve({ data: { status: false } });
			const swallErrorSpy = jest.spyOn(wrapper.vm, 'swalError');
			wrapper.vm.createBankAccount();

			wrapper.vm.$nextTick(() => {
				expect(swallErrorSpy).toBeCalledWith(
					null,
					'Tidak dapat menambahkan akun Bank untuk sementara.'
				);
				swallErrorSpy.mockRestore();
			});
		});

		it('should sending create account and show alert when status false', () => {
			axios.mockReject('error');
			const alertSpy = jest.spyOn(window, 'alert');
			wrapper.vm.createBankAccount();
			window.alert();
			expect(alertSpy).toBeCalled();
			alertSpy.mockRestore();
		});
	});

	describe('watch variable in WithdrawAddBank Components', () => {
		it('should wacth all changed value on variable ownerName and set saveAccountDisabledOne to "true"', () => {
			wrapper.vm.ownerName = 'test name';
			global.validator = {
				attach: jest.fn(),
				validate: jest.fn().mockImplementation(() => Promise.resolve())
			};
			expect(wrapper.vm.saveAccountDisabledOne).toBeTruthy();
		});
		it('should wacth all changed value on variable ownerName and set saveAccountDisabledOne to "false"', () => {
			wrapper.vm.ownerName = 'test2';
			global.validator = {
				attach: jest.fn(),
				validate: jest.fn().mockImplementation(() =>
					Promise.resolve(() => {
						return false;
					})
				)
			};
			wrapper.vm.$nextTick();
			expect(wrapper.vm.saveAccountDisabledOne).toBeTruthy();
		});

		it('should wacth all changed value on variable numberAccount set saveAccountDisabledTwo to "true"', () => {
			wrapper.vm.numberAccount = '1';
			global.validator = {
				attach: jest.fn(),
				validate: jest.fn().mockImplementation(() => Promise.resolve())
			};
			expect(wrapper.vm.saveAccountDisabledTwo).toBeTruthy();
		});

		it('should wacth all changed value on variable numberAccount set saveAccountDisabledTwo to "false"', () => {
			wrapper.vm.numberAccount = '3';
			wrapper.vm.saveAccountDisabledTwo = false;
			global.validator = {
				attach: jest.fn(),
				validate: jest.fn().mockImplementation(() =>
					Promise.resolve(() => {
						return false;
					})
				)
			};
			expect(wrapper.vm.saveAccountDisabledTwo).toBeFalsy();
		});
	});
});
