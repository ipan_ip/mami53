import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';

import mockWindowProperty from 'tests-fe/utils/mock-window-property';

import UserKostReviewImageUpload from 'Js/_user/components/user-kost/UserKostReviewImageUpload.vue';
import vue2Dropzone from 'vue2-dropzone';

global.Vue = require('vue');
const mixinSwalLoading = require('Js/@mixins/MixinSwalLoading');

describe('UserKostReviewImageUpload.vue', () => {
	const localVue = createLocalVue();
	localVue.mixin([mixinSwalLoading]);

	const notify = jest.fn();
	mockWindowProperty('bugsnagClient', { notify });

	const tracker = jest.fn();
	mockWindowProperty('tracker', tracker);

	const swal = jest.fn();
	mockWindowProperty('swal', swal);

	const swalError = jest.fn();

	const UserKostReviewImageUploadStore = {
		state: {
			token: 'token'
		}
	};
	const shallowMountComponent = (mockProps = null) => {
		return shallowMount(UserKostReviewImageUpload, {
			localVue,
			propsData: mockProps || {
				lastImage: []
			},
			methods: { swalError },
			store: new Vuex.Store(UserKostReviewImageUploadStore),
			stubs: {
				Dropzone: vue2Dropzone
			}
		});
	};

	describe('render component correctly', () => {
		let wrapper;

		beforeEach(() => {
			wrapper = shallowMountComponent();
		});

		it('should render container properly', () => {
			expect(wrapper.find('#uploadImageReview').exists()).toBeTruthy();
		});

		it('should render upload-image__area when isShowing is true', async () => {
			await wrapper.setData({ isShowing: true });

			const uploadArea = wrapper.find('.upload-image__area');
			expect(uploadArea.exists()).toBeTruthy();
		});
	});

	describe('call functions correctly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = shallowMountComponent();
		});

		it('should call toggleUpload function on btn-show-upload click', () => {
			const showUploadButton = wrapper.find('.btn-show-upload');

			const { isShowing } = wrapper.vm;
			if (showUploadButton && showUploadButton.exists()) {
				showUploadButton.trigger('click');

				const toggledIsShowing = wrapper.vm.isShowing;
				expect(toggledIsShowing).toBe(!isShowing);
			}
		});

		it('should set lastImage properly', () => {
			const mockProps = {
				lastImage: [
					{
						photo_id: 'id',
						photo_url: {
							large: '/path/to/image.jpg',
							medium: '/path/to/image.jpg',
							small: '/path/to/image.jpg'
						}
					},
					{
						photo_id: 'id',
						photo_url: {
							medium: '/path/to/image.jpg',
							small: ''
						}
					}
				]
			};

			wrapper = shallowMountComponent(mockProps);

			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.isShowing).toBeTruthy();
				expect(wrapper.vm.isAssigned).toBeTruthy();
			});
		});

		it('should set dropzone token when calling assignUploadParams function', () => {
			wrapper.vm.assignUploadParams();

			expect(wrapper.vm.dropzoneOptions.params._token).toBe(wrapper.vm.token);
		});

		it('should render token from vuex', () => {
			const { token } = wrapper.vm;

			expect(token).toBe('token');
		});

		it('should call swalError in uploadSuccess function when dropzone emitted vdropzone-success event on false response status', async () => {
			await wrapper.setData({ isShowing: true });

			const $dropzone = wrapper.vm.$refs.dropzoneArea;
			if ($dropzone && typeof $dropzone === 'object') {
				$dropzone.$emit(
					'vdropzone-success',
					{ upload: { uuid: '' } },
					{
						status: false,
						media: {
							id: ''
						}
					}
				);

				expect(swalError).toBeCalled();

				const swalCalls = swalError.mock.calls || [[]];
				const lastSwalCall = swalCalls[swalCalls.length - 1];
				expect(lastSwalCall[1]).toBe(
					'Upload Gagal! File gagal diupload. Silakan cek kembali tipe file atau koneksi internet Anda.'
				);
			}
		});

		it('should call set data and emit event in uploadSuccess function when dropzone emitted vdropzone-success event on true response status', async () => {
			await wrapper.setData({ isShowing: true });

			const $dropzone = wrapper.vm.$refs.dropzoneArea;
			if ($dropzone && typeof $dropzone === 'object') {
				$dropzone.$emit(
					'vdropzone-success',
					{ upload: { uuid: 'new_image' }, val: 'test' },
					{
						status: true,
						media: {
							id: 'media_id'
						}
					}
				);

				wrapper.vm.$nextTick(() => {
					expect(wrapper.vm.imageUploaded.length).toBe(1);
					expect(wrapper.vm.imageCount).toBe(1);
					expect(wrapper.vm.imageSubmit).toContain('media_id');
					expect(wrapper.emitted().imageUploaded).toBeTruthy();
				});
			}
		});

		it('should call swalError in uploadError function when dropzone emitted vdropzone-error event and file size exceeded 5MB', async () => {
			await wrapper.setData({ isShowing: true });

			const $dropzone = wrapper.vm.$refs.dropzoneArea;
			if ($dropzone && typeof $dropzone === 'object') {
				$dropzone.$emit(
					'vdropzone-error',
					{ size: 5000001 },
					{
						status: true,
						media: {
							id: 'media_id'
						}
					}
				);

				expect(swalError).toBeCalled();

				const swalCalls = swalError.mock.calls || [[]];
				const lastSwalCall = swalCalls[swalCalls.length - 1];
				expect(lastSwalCall[1]).toBe(
					'Upload foto gagal! Tidak dapat mengunggah foto dengan ukuran melebihi 5MB.'
				);
			}
		});

		it('should call swalError in uploadError function when dropzone emitted vdropzone-error event and file size not exceeding 5MB', async () => {
			await wrapper.setData({ isShowing: true });

			const $dropzone = wrapper.vm.$refs.dropzoneArea;
			if ($dropzone && typeof $dropzone === 'object') {
				$dropzone.$emit(
					'vdropzone-error',
					{ size: 4000000 },
					{
						status: true,
						media: {
							id: 'media_id'
						}
					}
				);

				expect(swalError).toBeCalled();

				const swalCalls = swalError.mock.calls || [[]];
				const lastSwalCall = swalCalls[swalCalls.length - 1];
				expect(lastSwalCall[1]).toBe(
					'Upload foto gagal! Silakan cek kembali tipe file, batas jumlah foto maksimal, atau koneksi internet Anda.'
				);
			}
		});

		it('should call removeFile function and set data when dropzone emitted vdropzone-removed-file event and imageUploaded is not empty', async () => {
			await wrapper.setData({ isShowing: true });
			await wrapper.setData({
				imageUploaded: [
					{
						uuid: 'test'
					},
					{
						uuid: 'test_2'
					}
				]
			});

			const $dropzone = wrapper.vm.$refs.dropzoneArea;
			if ($dropzone && typeof $dropzone === 'object') {
				$dropzone.$emit(
					'vdropzone-removed-file',
					{ size: 4000000, upload: { uuid: 'test' } },
					{
						status: true,
						media: {
							id: 'media_id'
						}
					}
				);

				const { imageUploaded } = wrapper.vm;
				expect(imageUploaded.length).toBe(1);
			}
		});

		it('should call removeFile function and set data when dropzone emitted vdropzone-removed-file event and imageUploaded is not empty', async () => {
			await wrapper.setData({ isShowing: true });
			await wrapper.setData({
				imageUploaded: [
					{
						uuid: 'test',
						name: 'test'
					},
					{
						uuid: 'test_2',
						name: 'test_2'
					}
				]
			});

			const $dropzone = wrapper.vm.$refs.dropzoneArea;
			if ($dropzone && typeof $dropzone === 'object') {
				$dropzone.$emit(
					'vdropzone-removed-file',
					{ size: 4000000, name: 'test', upload: { uuid: 'test' } },
					{
						status: true,
						media: {
							id: 'media_id'
						}
					}
				);

				const { imageUploaded } = wrapper.vm;

				expect(imageUploaded.length).toBe(1);
				expect(imageUploaded).not.toContain({ name: 'test' });
			}
		});

		it('should return lastImage as empty array [] when not set ', async () => {
			wrapper = shallowMountComponent({});

			expect(wrapper.vm.lastImage).toEqual([]);
		});
	});
});
