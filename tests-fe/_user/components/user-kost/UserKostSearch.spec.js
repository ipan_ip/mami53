import { createLocalVue, shallowMount } from '@vue/test-utils';

import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import { mockComponentWithAttrs } from 'tests-fe/utils/mock-component';

import UserKostSearch from 'Js/_user/components/UserKostSearch.vue';
import room from 'tests-fe/@components/__mocks__/room.json';

global.Vue = require('vue');
const mixinSwalLoading = require('Js/@mixins/MixinSwalLoading');

describe('UserKostSearch.vue', () => {
	const localVue = createLocalVue();
	localVue.mixin([mixinSwalLoading]);

	global.axios = {
		post: jest.fn().mockResolvedValue({
			data: {
				status: 200,
				rooms: [room, room],
				'has-more': true,
				'next-page': 2,
				limit: 1,
				offset: 0,
				page: 1,
				total: 2,
				totalStr: 'dua'
			},
			meta: {
				message: ''
			}
		})
	};

	const notify = jest.fn();
	mockWindowProperty('bugsnagClient', { notify });

	const tracker = jest.fn();
	mockWindowProperty('tracker', tracker);

	const swal = jest.fn();
	mockWindowProperty('swal', swal);

	const swalError = jest.fn();
	const shallowMountComponent = () => {
		return shallowMount(UserKostSearch, {
			localVue,
			attachToDocument: true,
			methods: { swalError },
			stubs: {
				MiniList: mockComponentWithAttrs({
					class: 'wrapper-list'
				})
			}
		});
	};

	describe('render component correctly', () => {
		let wrapper;

		beforeEach(() => {
			wrapper = shallowMountComponent();
		});

		it('should render container properly', () => {
			expect(wrapper.find('.row.content').exists()).toBeTruthy();
		});
	});

	describe('call functions correctly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = shallowMountComponent();
		});

		it('should call swal function if search data is empty on form submission ', async () => {
			await wrapper.setData({ search: '' });
			const submitButton = wrapper.find("[type='submit']");

			if (submitButton.exists()) {
				await submitButton.trigger('click');

				expect(swal).toBeCalled();
			}
		});

		it('should call axios if search data is not empty on form submission ', async () => {
			await wrapper.setData({ search: 'test' });
			await wrapper.setMethods({
				openSwalLoading: jest.fn(),
				closeSwalLoading: jest.fn()
			});

			const submitButton = wrapper.find("[type='submit']");

			if (submitButton.exists()) {
				await submitButton.trigger('click');

				expect(axios.post).toBeCalled();
				expect(wrapper.vm.openSwalLoading).toBeCalled();
			}
		});

		it('should set data on succesful axios request ', async () => {
			await wrapper.setData({ search: 'test' });
			await wrapper.setMethods({
				openSwalLoading: jest.fn(),
				closeSwalLoading: jest.fn()
			});

			const submitButton = wrapper.find("[type='submit']");
			if (submitButton.exists()) {
				submitButton.trigger('click');

				wrapper.vm.$nextTick(() => {
					const {
						searchText,
						hasMore,
						roomsData,
						limit,
						offset,
						nextPage,
						page,
						total,
						totalStr
					} = wrapper.vm;

					expect(searchText).toBe('test');
					expect(hasMore).toBe(true);
					expect(roomsData).toEqual([room, room]);
					expect(limit).toBe(1);
					expect(offset).toBe(0);
					expect(nextPage).toBe(2);
					expect(page).toBe(1);
					expect(total).toBe(2);
					expect(totalStr).toBe('dua');
				});
			}
		});

		it('should not set data on unsuccesful axios request ', async () => {
			const swals = {
				openSwalLoading: jest.fn(),
				closeSwalLoading: jest.fn(),
				swalError: jest.fn()
			};

			await wrapper.setMethods(swals);
			await wrapper.setData({ search: 'test' });

			axios.post = jest.fn().mockResolvedValue({
				data: {
					status: false,
					meta: {
						message: 'test'
					}
				}
			});

			const submitButton = wrapper.find("[type='submit']");
			if (submitButton.exists()) {
				submitButton.trigger('click');

				wrapper.vm.$nextTick(() => {
					expect(swals.swalError).toBeCalled();
				});
			}
		});

		it('should catch error and not setting the data on failed axios request ', async () => {
			const swals = {
				openSwalLoading: jest.fn(),
				closeSwalLoading: jest.fn(),
				swalError: jest.fn()
			};

			axios.post = jest.fn().mockRejectedValue('typo');

			await wrapper.setMethods(swals);
			await wrapper.setData({ search: 'test' });

			const submitButton = wrapper.find("[type='submit']");
			if (submitButton.exists()) {
				submitButton.trigger('click');

				wrapper.vm.$nextTick(() => {
					expect(wrapper.vm.searchText).toBe('');

					expect(notify).toBeCalled();
					expect(swals.closeSwalLoading).toBeCalled();
					expect(swals.swalError).toBeCalled();
				});
			}
		});
	});
});
