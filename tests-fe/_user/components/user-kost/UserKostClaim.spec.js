import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import moment from 'moment';

import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mockAxios from 'tests-fe/utils/mock-axios';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import { mockComponentWithAttrs } from '../../../utils/mock-component/index.js';

import debounce from 'tests-fe/utils/mock-debounce';
import VeeValidate, { Validator } from 'vee-validate';
import id from 'vee-validate/dist/locale/id';

import paymentHistoryData from '../__mocks__/paymentHistoryData.json';
import mixinUserKost from 'Js/_user/mixins/mixinUserKost';

import UserKostBillingCategory from 'Js/_user/components/user-kost/UserKostBillingCategory.vue';
import UserKostClaim from 'Js/_user/components/user-kost/UserKostClaim.vue';

import UserKostClaimStore from '../__mocks__/UserKostClaimStore.js';

// mock imports
jest.mock('Js/_user/event-bus/event-bus', () => {
	const EventBus = {
		$emit: jest.fn()
	};

	global.EventBus = EventBus;
	return global.EventBus;
});
jest.mock('Js/@components/MamiLoadingInline', () => {
	return mockComponentWithAttrs({ class: 'mami-loading-inline' });
});

describe('UserKostClaim.vue', () => {
	const localVue = createLocalVue();

	// initiate vue libraries
	localVue.use(VeeValidate, {
		locale: 'id'
	});
	localVue.use(Vuex);

	// mock vue functions
	mockVLazy(localVue);
	localVue.directive('validate', jest.fn());
	localVue.mixin([mixinUserKost]);

	// mock global functions
	global.axios = mockAxios;
	global.debounce = debounce;

	// mock window functions
	const notify = jest.fn(() => {});
	const swalError = jest.fn(() => {});
	const windowOpen = jest.fn(() => {});
	mockWindowProperty('bugsnagClient', { notify });
	mockWindowProperty('open', windowOpen);

	// initiate vee validate validator
	window.validator = new Validator();
	Validator.localize('id', id);

	// mock stubbed component
	const stubs = {
		MamiLoadingInline: mockComponentWithAttrs({ class: 'mami-loading-inline' }),
		UserKostBillingCategory
	};

	const mount = (store = null) => {
		return shallowMount(UserKostClaim, {
			localVue,
			stubs,
			store: new Vuex.Store(store || UserKostClaimStore),
			methods: {
				$dayjs: moment,
				swalError
			}
		});
	};

	describe('render component correctly', () => {
		let wrapper;

		beforeEach(() => {
			wrapper = mount();
		});

		it('should render container', done => {
			expect(wrapper.find('.user-kost-claim').exists()).toBeTruthy();
			done();
		});

		it('should render userKost computed data properly', done => {
			const { userKost } = wrapper.vm;
			expect(userKost).toEqual(UserKostClaimStore.state.userKost);
			done();
		});

		it('should render UserBillingCategory properly', done => {
			expect(
				wrapper.find('.container.notif-center-menu').exists()
			).toBeTruthy();

			done();
		});

		it('should render mami-loading-inline when isLoading is true', async done => {
			await wrapper.setData({ isLoading: true });
			expect(wrapper.find('.mami-loading-inline').exists()).toBeTruthy();

			done();
		});

		it('should render empty-wrapper if paymentHistory length == 0 ', () => {
			const cards = wrapper.findAll('.item-card');
			const { paymentHistory } = wrapper.vm;

			if (paymentHistory && paymentHistory.length === 0) {
				expect(cards.length).toBe(0);
				expect(wrapper.find('.empty-wrapper').exists()).toBeTruthy();
			}
		});

		it('should render paid claim card properly', async () => {
			await axios.mockResolve({
				data: {
					status: true,
					schedules: paymentHistoryData
				}
			});
			await wrapper.vm.getClaimList();

			const cards = wrapper.findAll('.item-card');
			if (cards && cards.length) {
				const paidCard = cards.at(0);

				if (paidCard.exists()) {
					expect(paidCard.find('.header-card').classes()).toContain(
						'--success-header'
					);

					const titleCards = paidCard.findAll('.title-card');
					if (titleCards.at(0).exists()) {
						expect(titleCards.at(0).text()).toBe('Dibayar');
					}

					if (titleCards.at(1).exists()) {
						expect(titleCards.at(1).text()).toBe('paid desc');
					}

					const payButton = paidCard.find('.--custom-button-style');
					expect(payButton.exists()).toBeTruthy();
				}
			}
		});

		it('should render unpaid claim card properly', async () => {
			await axios.mockResolve({
				data: {
					status: true,
					schedules: paymentHistoryData
				}
			});
			await wrapper.vm.getClaimList();

			const cards = wrapper.findAll('.item-card');
			if (cards && cards.length) {
				const unpaidCard = cards.at(1);

				if (unpaidCard.exists()) {
					expect(unpaidCard.find('.header-card').classes()).toContain(
						'--error-header'
					);

					const titleCards = unpaidCard.findAll('.title-card');
					if (titleCards.at(0).exists()) {
						expect(titleCards.at(0).text()).toBe('Belum Dibayar');
					}

					if (titleCards.at(1).exists()) {
						expect(titleCards.at(1).text()).toBe('unpaid desc');
					}

					const payButton = unpaidCard.find('.--custom-button-style');
					expect(payButton.exists()).toBeTruthy();
				}
			}
		});

		it('should render ignored unpaid claim card properly', async () => {
			const store = Object.assign(UserKostClaimStore);
			store.state.statusRent.status = true;

			wrapper = mount(store);
			await axios.mockResolve({
				data: {
					status: true,
					schedules: paymentHistoryData
				}
			});
			await wrapper.vm.getClaimList();

			const cards = wrapper.findAll('.item-card');
			if (cards && cards.length) {
				const unpaidCard = cards.at(1);

				if (unpaidCard.exists()) {
					expect(unpaidCard.find('.header-card').classes()).toContain(
						'--ignore-header'
					);

					const payButton = unpaidCard.find('.--custom-button-style');
					expect(payButton.exists()).toBeFalsy();
				}
			}
		});

		it('should render warning unpaid claim card properly if due date is today', async () => {
			const store = Object.assign(UserKostClaimStore);
			store.state.statusRent.status = true;

			wrapper = mount(store);
			await axios.mockResolve({
				data: {
					status: true,
					schedules: [
						{
							...paymentHistoryData[0],
							realDueDate: new Date(),
							payment_status: 'unpaid'
						}
					]
				}
			});
			await wrapper.vm.getClaimList();

			const cards = wrapper.findAll('.item-card');
			if (cards && cards.length) {
				const unpaidCard = cards.at(1);

				if (unpaidCard.exists()) {
					expect(unpaidCard.find('.header-card').classes()).toContain(
						'--warning-header'
					);

					const payButton = unpaidCard.find('.--custom-button-style');
					expect(payButton.exists()).toBeFalsy();
				}
			}
		});
	});

	describe('call functions correctly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = mount();
		});

		it('should call openModalStopRent function on "stop contract" link click', async () => {
			const store = Object.assign(UserKostClaimStore);
			store.state.statusRent.status = false;

			wrapper = mount(store);
			wrapper.setMethods({ openModalStopRent: jest.fn() });

			const stopContractLink = wrapper.find('.link-stop-contract');
			if (stopContractLink && stopContractLink.exists()) {
				stopContractLink.trigger('click');
				expect(wrapper.vm.openModalStopRent).toBeCalled();
			}
		});

		it('should call axios and set paymentHistory data if request status is true on successful request', async () => {
			await axios.mockResolve({
				data: {
					status: true,
					schedules: paymentHistoryData
				}
			});

			await wrapper.vm.getClaimList();
			const { paymentHistory = [], isLoading } = wrapper.vm;
			expect(isLoading).toBeFalsy();
			expect(paymentHistory.length).toBe(paymentHistoryData.length);
		});

		it('should call axios and swalError function  if request status is false on successful request', async () => {
			await axios.mockResolve({
				data: {
					status: false,
					schedules: [],
					meta: {
						message: 'error message'
					}
				}
			});
			await wrapper.vm.getClaimList();

			const { isLoading } = wrapper.vm;
			expect(isLoading).toBeFalsy();
			expect(notify).toBeCalled();
			expect(swalError).toBeCalled();
		});

		it('should call openDetailClaim function on  item-card click ', async () => {
			await axios.mockResolve({
				data: {
					status: true,
					schedules: paymentHistoryData
				}
			});
			await wrapper.vm.getClaimList();

			wrapper.setMethods({ openDetailClaim: jest.fn() });
			const cards = wrapper.findAll('.item-card');
			if (cards && cards.length) {
				const firstCard = cards.at(0);
				firstCard.trigger('click');
				expect(wrapper.vm.openDetailClaim).toBeCalled();
			}
		});

		it('should emitted "open" event on  item-card click ', async () => {
			await axios.mockResolve({
				data: {
					status: true,
					schedules: paymentHistoryData
				}
			});
			await wrapper.vm.getClaimList();

			const cards = wrapper.findAll('.item-card');
			if (cards && cards.length) {
				const firstCard = cards.at(0);
				firstCard.trigger('click');

				expect(wrapper.emitted('open')).toBeTruthy();
			}
		});

		it('should call window.open when calling openInvoice method', async () => {
			await wrapper.vm.openInvoice();

			expect(windowOpen).toBeCalled();
		});

		it('should call EventBus.$emit when calling openModalStopRent method', async () => {
			const EventBusEmitSpy = jest.spyOn(global.EventBus, '$emit');
			await wrapper.vm.openModalStopRent();

			expect(EventBusEmitSpy).toBeCalled();
			EventBusEmitSpy.mockRestore();
		});

		it('should call $emit when calling openDetailClaim method', async () => {
			const emitSpy = jest.spyOn(wrapper.vm, 'openDetailClaim');

			await wrapper.vm.openDetailClaim(0, false);
			expect(emitSpy).toBeCalled();
			emitSpy.mockRestore();
		});
	});
});
