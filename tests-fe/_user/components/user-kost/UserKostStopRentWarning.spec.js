import { createLocalVue, shallowMount } from '@vue/test-utils';
import UserKostStopRentWarning from 'Js/_user/components/user-kost/UserKostStopRentWarning.vue';

const localVue = createLocalVue();

describe('UserKostStopRentWarning.vue', () => {
	const propsData = { showModal: true };
	const wrapper = shallowMount(UserKostStopRentWarning, {
		localVue,
		propsData
	});

	it('should render the component', () => {
		expect(wrapper.find('.stop-rent-warning').exists()).toBe(true);
	});

	it('should close the modal', () => {
		wrapper.vm.closeModal();
		expect(wrapper.emitted('close')).toBeTruthy();
	});

	it('should trigger on confirm button acrion', () => {
		wrapper.vm.onConfirm();
		expect(wrapper.emitted('confirm')).toBeTruthy();
	});
});
