import { createLocalVue, shallowMount } from '@vue/test-utils';
import UserKostClaimDetailBill from 'Js/_user/components/user-kost/UserKostClaimDetailBill';
import Vuex from 'vuex';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

jest.mock('Js/_user/event-bus/event-bus', () => {
	const EventBus = {
		$emit: jest.fn()
	};
	global.EventBus = EventBus;

	return global.EventBus;
});
jest.mock('Js/_user/mixins/mixinUserKost.js', () => ({}));
const getStatusRent = jest.fn();
const open = jest.fn();

describe('UserKostClaimDetailBill', () => {
	let wrapper;

	const store = {
		state: {
			userKost: {
				booking_data: {
					rent_count_type: 'daily'
				}
			}
		}
	};

	const props = {
		claimData: {
			depositPrice: [1000, 10000],
			invoiceNumber: 'DP',
			finePrice: [
				{
					fine_delay: 2
				}
			],
			payment_status: 'unpaid'
		},
		claimDetail: {},
		claimIndex: 123
	};

	const localVue = createLocalVue();
	localVue.use(Vuex);

	beforeEach(() => {
		wrapper = shallowMount(UserKostClaimDetailBill, {
			localVue,
			propsData: props,
			store: new Vuex.Store(store),
			mocks: { getStatusRent }
		});
	});

	it('should render this component properly ', () => {
		expect(wrapper.find('#userKostClaimDetailBill').exists()).toBeTruthy();
	});

	it('should return correct duration text', () => {
		const fineDuration = ['day', 'week'];
		expect(wrapper.vm.fineDurationText(fineDuration[0])).toEqual('Hari');
		expect(wrapper.vm.fineDurationText(fineDuration[1])).toEqual('Minggu');
		expect(wrapper.vm.fineDurationText()).toEqual('Bulan');
	});

	it('should stop contact properly', async () => {
		await wrapper.find('.link-stop-contract').trigger('click');
		expect(global.EventBus.$emit).toBeCalled();
	});

	it('should open window based on the link', async () => {
		mockWindowProperty('open', open);
		wrapper.setProps({
			claimData: {
				invoiceNumber: 'DP',
				payment_status: 'paid',
				otherCost: [],
				depositPrice: [],
				discount: [],
				finePrice: []
			}
		});
		await wrapper.vm.$nextTick();
		wrapper.find('.--is-custom-button').trigger('click');
		expect(open).toBeCalled();
	});
});
