import { createLocalVue, shallowMount } from '@vue/test-utils';
import UserKostProfileSection from 'Js/_user/components/user-kost/UserKostProfileSection.vue';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

const localVue = createLocalVue();
mockVLazy(localVue);

describe('UserKostProfileSection.vue', () => {
	let propsData = {
		roomData: {
			gender: 0,
			area_label: 'Jakarta Selatan',
			name: 'Kos Arum Syariah',
			type_name: 'Mawar 5',
			photo: { small: 'http://mamikos.com' }
		}
	};

	it('should render the component', () => {
		const wrapper = shallowMount(UserKostProfileSection, {
			localVue,
			propsData
		});

		expect(wrapper.find('.profile-section-wrapper').exists()).toBe(true);
	});

	it('should render correct gender if gender is female', () => {
		propsData.roomData.gender = 2;
		const wrapper = shallowMount(UserKostProfileSection, {
			localVue,
			propsData
		});

		expect(wrapper.vm.gender).toBe('Putri');
	});

	it('should render correct gender if gender is male', () => {
		propsData.roomData.gender = 1;
		const wrapper = shallowMount(UserKostProfileSection, {
			localVue,
			propsData
		});

		expect(wrapper.vm.gender).toBe('Putra');
	});
});
