import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import moment from 'moment';

import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mockAxios from 'tests-fe/utils/mock-axios';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

import debounce from 'tests-fe/utils/mock-debounce';
import VeeValidate, { Validator } from 'vee-validate';
import id from 'vee-validate/dist/locale/id';
import 'tests-fe/utils/mock-vue';
import UserKostContractStore from '../__mocks__/UserKostContractStore.js';
import UserKostContract from 'Js/_user/components/user-kost/UserKostContract.vue';

import mixinRentCountLabel from 'Js/@mixins/MixinRentCountLabel';
import mixinUserKost from 'Js/_user/mixins/mixinUserKost';

const mixinNavigator = require('Js/@mixins/MixinNavigatorIsMobile');

jest.mock('Js/_user/event-bus/event-bus', () => {
	const EventBus = {
		$emit: jest.fn()
	};
	global.EventBus = EventBus;

	return global.EventBus;
});

global.tracker = jest.fn();

describe('UserKostContract.vue', () => {
	const localVue = createLocalVue();
	localVue.use(Vuex);
	mockVLazy(localVue);

	window.validator = new Validator();
	Validator.localize('id', id);

	localVue.directive('validate', jest.fn());
	localVue.use(VeeValidate, {
		locale: 'id'
	});
	localVue.mixin([mixinRentCountLabel, mixinUserKost]);

	global.axios = mockAxios;
	global.debounce = debounce;

	const notify = jest.fn();
	mockWindowProperty('bugsnagClient', { notify });

	const shallowMountComponent = (store = null) => {
		return shallowMount(UserKostContract, {
			localVue,
			store: new Vuex.Store(store || UserKostContractStore),
			methods: { $dayjs: moment }
		});
	};

	describe('render component correctly', () => {
		let wrapper;

		beforeEach(() => {
			wrapper = shallowMountComponent();
		});

		it('should render container', () => {
			expect(wrapper.find('.user-kost-contract').exists()).toBeTruthy();
		});

		it('should render checkin and checkout date correctly', () => {
			const todayTimestamp = moment().format();
			const dateFormat = 'DD MMMM YYYY';
			const todayDate = moment().format(dateFormat);

			const store = Object.assign(UserKostContractStore);
			store.state.userKost.booking_data.checkin = todayTimestamp;
			store.state.userKost.booking_data.checkout = todayTimestamp;

			wrapper = shallowMountComponent(store);

			const { checkin, checkout } = wrapper.vm;

			expect(checkin).toBe(todayDate);
			expect(checkout).toBe(todayDate);
		});

		it('should render contract detail for "weekly" frequency properly', () => {
			const store = Object.assign(UserKostContractStore);
			store.state.userKost.booking_data.rent_count_type = 'weekly';

			wrapper = shallowMountComponent(store);

			const { tenantRentCount, tenantRentCountText } = wrapper.vm;
			expect(tenantRentCount).toBe('Mingguan');
			expect(tenantRentCountText).toBe('1 Minggu');
		});

		it('should render contract detail for "monthly" frequency properly', () => {
			const store = Object.assign(UserKostContractStore);
			store.state.userKost.booking_data.rent_count_type = 'monthly';

			wrapper = shallowMountComponent(store);

			const { tenantRentCount, tenantRentCountText } = wrapper.vm;
			expect(tenantRentCount).toBe('Bulanan');
			expect(tenantRentCountText).toBe('1 Bulan');
		});

		it('should render contract detail for "quarterly" frequency properly', () => {
			const store = Object.assign(UserKostContractStore);
			store.state.userKost.booking_data.rent_count_type = 'quarterly';

			wrapper = shallowMountComponent(store);

			const { tenantRentCount, tenantRentCountText } = wrapper.vm;
			expect(tenantRentCount).toBe('3 Bulanan');
			expect(tenantRentCountText).toBe('3 Bulan');
		});

		it('should render contract detail for "semiannually" frequency properly', () => {
			const store = Object.assign(UserKostContractStore);
			store.state.userKost.booking_data.rent_count_type = 'semiannually';

			wrapper = shallowMountComponent(store);

			const { tenantRentCount, tenantRentCountText } = wrapper.vm;
			expect(tenantRentCount).toBe('6 Bulanan');
			expect(tenantRentCountText).toBe('6 Bulan');
		});

		it('should render contract detail for "yearly" frequency properly', () => {
			const store = Object.assign(UserKostContractStore);
			store.state.userKost.booking_data.rent_count_type = 'yearly';

			wrapper = shallowMountComponent(store);

			const { tenantRentCount, tenantRentCountText } = wrapper.vm;
			expect(tenantRentCount).toBe('Tahunan');
			expect(tenantRentCountText).toBe('1 Tahun');
		});

		it('should render dateBillInfo from vuex fine.billing_date properly', () => {
			const todayDate = moment().format('DD MMMM YYYY');
			const store = Object.assign(UserKostContractStore);
			store.state.userKost.prices.fine.billing_date = todayDate;

			wrapper = shallowMountComponent(store);

			const { dateBillInfo } = wrapper.vm;
			expect(dateBillInfo).toBe(todayDate);
		});

		it('should render dateBillInfo from vuex invoice properly', () => {
			const todayDate = moment().format('DD MMMM YYYY');
			const store = Object.assign(UserKostContractStore);
			store.state.userKost.invoice = [
				{
					scheduled_date_second: todayDate
				}
			];

			wrapper = shallowMountComponent(store);

			const { dateBillInfo } = wrapper.vm;
			expect(dateBillInfo).toBe(todayDate);
		});

		it('should render rulesFineInfo for maximum_length == 0 properly', () => {
			const { rulesFineInfo } = wrapper.vm;
			expect(rulesFineInfo).toBe('Tidak Ada');
		});

		it('should render userKost computed data properly', () => {
			const { userKost } = wrapper.vm;
			expect(userKost).toEqual(UserKostContractStore.state.userKost);
		});

		it('should render rulesFineInfo for maximum_length > 0 properly', () => {
			const maximumLengthFine = 1;
			const durationType = 'Bulan';
			const priceTotal = 'Rp. 10.000,00';

			const store = Object.assign(UserKostContractStore);
			store.state.userKost.prices.fine.maximum_length = maximumLengthFine;
			store.state.userKost.prices.fine.duration_type = durationType;
			store.state.userKost.prices.fine.price_total_string = priceTotal;

			wrapper = shallowMountComponent(store);

			const rulesFineInfoText = `Denda diberlakukan jika penyewa membayar tagihan Kos melewati ${maximumLengthFine} ${durationType} dari tanggal tagihan, nominal denda adalah ${priceTotal}`;
			const { rulesFineInfo } = wrapper.vm;

			expect(rulesFineInfo).toBe(rulesFineInfoText);
		});

		it('should render wrapper-link-stop-contract if statusRent.status is false', () => {
			const stopContractWrapper = wrapper.find('.wrapper-link-stop-contract');

			expect(stopContractWrapper.exists()).toBeTruthy();
			expect(
				stopContractWrapper.find('.link-stop-contract').exists()
			).toBeTruthy();
		});
	});

	describe('call functions correctly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = shallowMountComponent();
		});

		it('should call openModalStopRent function on "stop contract" link click', () => {
			wrapper.setMethods({ openModalStopRent: jest.fn() });

			const stopContractLink = wrapper.find('.link-stop-contract');
			stopContractLink.trigger('click');

			expect(wrapper.vm.openModalStopRent).toBeCalled();
		});

		it('should open stop rent modal', () => {
			wrapper.vm.openModalStopRent();
			expect(global.EventBus.$emit).toBeCalledWith('actionModalStopRent', {
				isOpenModal: true
			});
		});

		it('should send tracker to moengage', () => {
			wrapper.vm.trackStopRentClicked();
			expect(tracker).toBeCalled();
		});
	});
});
