import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import moment from 'moment';

import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mockAxios from 'tests-fe/utils/mock-axios';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import {
	mockComponent,
	mockComponentWithAttrs
} from 'tests-fe/utils/mock-component';

import UserKostHeader from 'Js/_user/components/user-kost/UserKostHeader.vue';

import UserKostDataStore from '../__mocks__/UserKostDataStore.js';
import room from 'tests-fe/@components/__mocks__/room.json';

jest.mock('Js/_user/event-bus/event-bus', () => {
	const EventBus = {
		$emit: jest.fn()
	};
	global.EventBus = EventBus;

	return global.EventBus;
});

describe('UserKostHeader.vue', () => {
	const localVue = createLocalVue();
	localVue.use(Vuex);
	mockVLazy(localVue);

	global.axios = mockAxios;

	const windowOpen = jest.fn();
	mockWindowProperty('open', windowOpen);

	const authCheck = {
		all: true,
		user: true
	};
	mockWindowProperty('authCheck', authCheck);

	const notify = jest.fn();
	mockWindowProperty('bugsnagClient', { notify });

	const tracker = jest.fn();
	mockWindowProperty('tracker', tracker);

	global.document = {
		body: {
			style: {
				overflow: ''
			}
		}
	};

	const swalError = jest.fn();
	const shallowMountComponent = (propsData = null, store = null) => {
		return shallowMount(UserKostHeader, {
			localVue,
			propsData: propsData || {
				reviewData: {
					reviewStatus: 'empty'
				},
				tenantReview: {},
				editData: {}
			},
			methods: { $dayjs: moment, swalError },
			store: new Vuex.Store(store || UserKostDataStore),
			stubs: {
				UserKostReviewAdd: mockComponentWithAttrs({
					id: 'addReviewKost'
				}),
				UserReviewCard: mockComponent
			}
		});
	};

	describe('render component correctly', () => {
		let wrapper;

		beforeEach(() => {
			wrapper = shallowMountComponent();
		});

		it('should render container properly', () => {
			expect(wrapper.find('.user-kost-data').exists()).toBeTruthy();
		});

		it('should render userKost data from vuex properly', () => {
			const { userKost } = wrapper.vm;
			expect(userKost).toEqual(UserKostDataStore.state.userKost);
		});
	});

	describe('call functions correctly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = shallowMountComponent();
		});

		it('should not set roomDetail data on axios unsuccessful request', async () => {
			await axios.mockResolve({
				data: {
					status: false,
					story: {}
				}
			});
			await wrapper.vm.getRoomDetail();

			expect(wrapper.vm.roomDetail).toEqual({});
		});

		it('should set roomDetail data on axios successful request', async () => {
			await axios.mockResolve({
				data: {
					status: true,
					story: room
				}
			});
			await wrapper.vm.getRoomDetail();

			expect(wrapper.vm.roomDetail).toEqual(room);
		});

		it('should call bugsnagClient.notify on error request', async () => {
			await axios.mockResolve({
				data: {
					status: false
				}
			});
			await wrapper.vm.getRoomDetail();
			expect(notify).toBeCalled();
		});
	});
});
