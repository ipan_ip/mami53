import { createLocalVue, shallowMount } from '@vue/test-utils';
import UserKostStopRentDate from 'Js/_user/components/user-kost/UserKostStopRentDate.vue';
import moment from 'moment';

const localVue = createLocalVue();

describe('UserKostStopRentDate.vue', () => {
	const propsData = {
		checkoutDate: ''
	};

	const wrapper = shallowMount(UserKostStopRentDate, {
		localVue,
		propsData,
		methods: { $dayjs: moment }
	});

	it('should render the component', () => {
		expect(wrapper.find('.stop-rent-date').exists()).toBe(true);
	});

	it('should format checkout date', () => {
		wrapper.setProps({ checkoutDate: '10-10-2020' });
		expect(wrapper.vm.formattedCheckoutDate.toLowerCase()).toBe(
			'10 october 2020'
		);
	});

	it('should show datepicker', () => {
		wrapper.vm.isShowDatePicker = false;
		wrapper.vm.toggleDatePicker(true);
		expect(wrapper.vm.isShowDatePicker).toBe(true);
	});

	it('should select date choosen', () => {
		wrapper.vm.selectDate('09-10-2020');
		expect(wrapper.emitted('select')[0][0]).toEqual('09-10-2020');
	});
});
