import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';

import { mockComponentWithAttrs } from 'tests-fe/utils/mock-component';

import UserKostAddingPhotoStopRent from 'Js/_user/components/user-kost/UserKostAddingPhotoStopRent.vue';

describe('UserKostAddingPhotoStopRent.vue', () => {
	const localVue = createLocalVue();
	localVue.use(Vuex);

	// mock stubbed component
	const stubs = {
		Dropzone: mockComponentWithAttrs({
			class: 'dropzone'
		})
	};

	const UserKostAddingPhotoStopRentStore = {
		state: {
			token: 'token'
		}
	};

	// mount function
	const mount = () => {
		return shallowMount(UserKostAddingPhotoStopRent, {
			localVue,
			stubs,
			mocks: {
				$emit: jest.fn()
			},
			methods: {
				swalError: jest.fn()
			},
			store: new Vuex.Store(UserKostAddingPhotoStopRentStore)
		});
	};

	describe('render component correctly', () => {
		let wrapper;

		beforeEach(() => {
			wrapper = mount();
		});

		it('should render container properly', () => {
			expect(
				wrapper.find('#userKostAddingPhotoStopRent').exists()
			).toBeTruthy();
		});
	});

	describe('render computed value properly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = mount();
		});

		it('should return token value from $store.state.token properly', () => {
			expect(wrapper.vm.token).toBe('token');
		});
	});

	describe('call functions correctly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = mount();

			// mock refs
			wrapper.vm.$refs.dropzoneArea0 = [
				{
					getAcceptedFiles: jest.fn().mockReturnValue([{}]),
					removeAllFiles: jest.fn()
				}
			];
			wrapper.vm.$refs.dropzoneArea = {
				manuallyAddFile: jest.fn(),
				removeFile: jest.fn()
			};
		});

		it('should call removeAllFiles function when calling deleteFile method', async () => {
			await wrapper.vm.deleteFile(0);

			expect(wrapper.vm.$refs.dropzoneArea0[0].removeAllFiles).toBeCalled();
		});

		it('should set isShowing value based on lastImage length when calling checkLastImage method', () => {
			wrapper.setProps({
				lastImage: [{}]
			});
			wrapper.vm.checkLastImage();
			expect(wrapper.vm.isShowing).toBeTruthy();
		});

		it('should return checking result properly when calling checkingFileUpload method', () => {
			const isValid = wrapper.vm.checkingFileUpload(0);

			expect(isValid).toBeTruthy();
		});

		it('should toggle isShowing value when calling toggleUpload method', () => {
			wrapper.setData({
				isShowing: true
			});
			wrapper.vm.toggleUpload();
			expect(wrapper.vm.isShowing).toBeFalsy();
		});

		it('should set imageSubmit data from imageUploaded data when calling popImageValue method', async () => {
			wrapper.setData({
				imageUploaded: [
					{
						val: 'test'
					}
				]
			});

			await wrapper.vm.popImageValue();
			expect(wrapper.vm.imageSubmit).toEqual(['test']);
		});

		it('should set dropzone token when calling assignUploadParams method', () => {
			wrapper.vm.assignUploadParams();

			expect(wrapper.vm.dropzoneOptionsCover.params._token).toBe('token');
		});

		it('should emit data and push new data to imageSubmit when calling assignLastImage method and the lastImage array is not empty', async () => {
			// has url
			wrapper.setProps({
				lastImage: [
					{ photo_id: 'test', photo_url: { small: 'url/to/small/image' } }
				]
			});
			await wrapper.vm.assignLastImage();
			expect(wrapper.vm.$refs.dropzoneArea.manuallyAddFile).toBeCalled();
			expect(wrapper.vm.imageSubmit).toContain('test');

			// no url
			wrapper.setProps({
				lastImage: [{ photo_id: 'test_2', photo_url: {} }]
			});
			await wrapper.vm.assignLastImage();
			expect(wrapper.vm.$refs.dropzoneArea.manuallyAddFile).toBeCalled();
			expect(wrapper.vm.imageSubmit).toContain('test_2');
		});

		it('should removeFile when calling uploadError method and file size exceeded max byte size', async () => {
			const removeFileSpy = jest.spyOn(
				wrapper.vm.$refs.dropzoneArea,
				'removeFile'
			);
			const swalErrorSpy = jest.spyOn(wrapper.vm, 'swalError');

			await wrapper.vm.uploadError({ size: 5000001 });
			expect(removeFileSpy).toBeCalled();
			expect(swalErrorSpy).toBeCalled();

			removeFileSpy.mockRestore();
			swalErrorSpy.mockRestore();
		});

		it('should call swalError when calling uploadError method and file size not exceeding max byte size', async () => {
			const swalErrorSpy = jest.spyOn(wrapper.vm, 'swalError');

			await wrapper.vm.uploadError({ size: 3000000 });
			expect(swalErrorSpy).toBeCalled();

			swalErrorSpy.mockRestore();
		});

		it('should set imageUploaded, formPhotos and call popImageValue function when calling uploadSuccess method and received truthy status', async () => {
			const popImageValueSpy = jest.spyOn(wrapper.vm, 'popImageValue');

			await wrapper.vm.uploadSuccess(
				{ upload: { uuid: 'test' } },
				{ status: true, media: { id: 'id' } }
			);
			expect(popImageValueSpy).toBeCalled();
			expect(wrapper.vm.imageUploaded).toEqual([
				{
					uuid: 'test',
					val: 'id'
				}
			]);
			expect(wrapper.vm.formPhotos).toBe(2);

			popImageValueSpy.mockRestore();
		});

		it('should call swalError when calling uploadSuccess method and received falsy status', async () => {
			const swalErrorSpy = jest.spyOn(wrapper.vm, 'swalError');

			await wrapper.vm.uploadSuccess({ size: 0 }, { status: false });
			expect(swalErrorSpy).toBeCalled();

			swalErrorSpy.mockRestore();
		});

		it('should set imageUploaded, deleteImage, and call popImageValue function when calling removeFile method and imageUploaded is not empty', async () => {
			const popImageValueSpy = jest.spyOn(wrapper.vm, 'popImageValue');

			wrapper.setData({
				imageUploaded: [{ uuid: 'test' }]
			});
			await wrapper.vm.removeFile({ upload: { uuid: 'test' } });

			expect(popImageValueSpy).toBeCalled();
			expect(wrapper.vm.deleteImage).toBe(0);
			expect(wrapper.vm.imageUploaded).toEqual([]);

			popImageValueSpy.mockRestore();
		});

		it('should set imageSubmit data when calling removeFile method and imageSubmit is not empty', async () => {
			wrapper.setData({
				imageSubmit: ['test']
			});
			await wrapper.vm.removeFile({ name: 'test', upload: { uuid: 'test' } });

			expect(wrapper.vm.imageSubmit).toEqual([]);
		});
	});
});
