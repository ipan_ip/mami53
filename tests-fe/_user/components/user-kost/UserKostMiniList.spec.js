import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import moment from 'moment';

import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mockAxios from 'tests-fe/utils/mock-axios';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

import debounce from 'tests-fe/utils/mock-debounce';
import VeeValidate, { Validator } from 'vee-validate';
import id from 'vee-validate/dist/locale/id';

import UserKostMiniList from 'Js/_user/components/UserKostMiniList.vue';
import room from 'tests-fe/@components/__mocks__/room.json';
import mixinGetQueryString from 'Js/@mixins/MixinGetQueryString';

jest.mock('Js/_user/event-bus/event-bus', () => {
	const EventBus = {
		$emit: jest.fn()
	};
	global.EventBus = EventBus;

	return global.EventBus;
});

describe('UserKostMiniList.vue', () => {
	const localVue = createLocalVue();
	localVue.use(Vuex);
	mockVLazy(localVue);

	window.validator = new Validator();
	Validator.localize('id', id);

	localVue.directive('validate', jest.fn());
	localVue.use(VeeValidate, {
		locale: 'id'
	});
	localVue.mixin([mixinGetQueryString]);

	global.axios = mockAxios;
	global.debounce = debounce;

	const windowOpen = jest.fn();
	mockWindowProperty('open', windowOpen);

	const shallowMountComponent = (propsData = null) => {
		return shallowMount(UserKostMiniList, {
			localVue,
			propsData: propsData || {
				rooms: [room],
				reference: 'test'
			},
			methods: { $dayjs: moment }
		});
	};

	describe('render component correctly', () => {
		let wrapper;

		beforeEach(() => {
			wrapper = shallowMountComponent();
		});

		it('should render container properly', () => {
			expect(wrapper.find('.wrapper-list').exists()).toBeTruthy();
		});

		it('should render room card properly', async () => {
			await wrapper.setProps({ rooms: [room, room], reference: 'test' });

			const roomWrappers = wrapper.findAll(
				'.mini-list-container.material-card-1'
			);
			expect(roomWrappers.length).toBe(2);
		});

		it('should render mini room info properly', async () => {
			const miniRoom = {
				room,
				...{
					photo_url: {
						medium: ''
					}
				}
			};
			const itemWithOneAvailableRoom = {
				...miniRoom,
				...{ available_room: 1 }
			};
			const itemWithMoreThanOneAvailableRoom = {
				...miniRoom,
				...{ available_room: 2 }
			};
			const itemWithUnavailableRoom = { ...miniRoom, ...{ available_room: 0 } };

			await wrapper.setProps({
				rooms: [
					itemWithOneAvailableRoom,
					itemWithMoreThanOneAvailableRoom,
					itemWithUnavailableRoom
				]
			});

			const roomWrappers = wrapper.findAll(
				'.mini-list-container.material-card-1'
			);
			if (roomWrappers && roomWrappers.length === 3) {
				const miniOneRoomLabel = roomWrappers.at(0).find('.mini-status.one');
				const miniMultipleRoomLabel = roomWrappers
					.at(1)
					.find('.mini-status.available');
				const miniUnavailableRoomLabel = roomWrappers
					.at(2)
					.find('.mini-status.full');

				expect(miniOneRoomLabel.exists()).toBeTruthy();
				expect(miniMultipleRoomLabel.exists()).toBeTruthy();
				expect(miniUnavailableRoomLabel.exists()).toBeTruthy();
			}
		});

		it('should render mini-photo-fade class properly', async () => {
			const itemWithUnavailableRoom = {
				...room,
				...{ available_room: 0 }
			};
			await wrapper.setProps({
				rooms: [itemWithUnavailableRoom]
			});

			const roomWrapper = wrapper.find('.mini-photo');
			if (roomWrapper && roomWrapper.exists()) {
				expect(roomWrapper.classes()).toContain('mini-photo-fade');
			}
		});

		it('should render gender info properly', async () => {
			const genderRoom = Object.assign(room);

			const campurRoom = {
				...genderRoom,
				...{
					gender: 0
				}
			};
			const putraRoom = {
				...genderRoom,
				...{
					gender: 1
				}
			};
			const putriRoom = {
				...genderRoom,
				...{
					gender: 2
				}
			};

			await wrapper.setProps({ rooms: [campurRoom, putraRoom, putriRoom] });
			const roomWrappers = wrapper.findAll(
				'.mini-list-container.material-card-1'
			);

			if (roomWrappers && roomWrappers.length === 3) {
				const campurRoomLabel = roomWrappers
					.at(0)
					.find('.mini-tag-gender.mini-tag-gender-mixed');
				const putraRoomLabel = roomWrappers
					.at(1)
					.find('.mini-tag-gender.mini-tag-gender-male');
				const putriRoomLabel = roomWrappers
					.at(2)
					.find('.mini-tag-gender.mini-tag-gender-female');

				expect(campurRoomLabel.exists()).toBeTruthy();
				expect(putraRoomLabel.exists()).toBeTruthy();
				expect(putriRoomLabel.exists()).toBeTruthy();
			}
		});
	});

	describe('call functions correctly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = shallowMountComponent();
		});

		it('should call openRoom function when room card is clicked properly', async () => {
			await wrapper.setProps({ rooms: [room, room], reference: 'test' });
			await wrapper.setMethods({ openRoom: jest.fn() });
			const roomWrappers = wrapper.findAll(
				'.mini-list-container.material-card-1'
			);
			if (roomWrappers && roomWrappers.length) {
				const firstRoom = roomWrappers.at(0);
				firstRoom.trigger('click');
				expect(wrapper.vm.openRoom).toBeCalled();
			}
		});

		it('should call window.open function when room card is clicked properly', async () => {
			await wrapper.setProps({ rooms: [room, room], reference: 'test' });

			const roomWrappers = wrapper.findAll(
				'.mini-list-container.material-card-1'
			);
			if (roomWrappers && roomWrappers.length) {
				const firstRoom = roomWrappers.at(0);
				firstRoom.trigger('click');

				expect(windowOpen).toBeCalled();
				const calls = windowOpen.mock.calls || [];
				const lastCall = calls[calls.length - 1];

				expect(lastCall[0]).not.toContain('ref=edit');
			}
		});

		it('should call window.open with edit param is clicked properly', async () => {
			await wrapper.setProps({ rooms: [room, room], reference: 'edit' });

			const roomWrappers = wrapper.findAll(
				'.mini-list-container.material-card-1'
			);
			if (roomWrappers && roomWrappers.length) {
				const firstRoom = roomWrappers.at(0);
				firstRoom.trigger('click');
				expect(windowOpen).toBeCalled();

				const calls = windowOpen.mock.calls || [];
				const lastCall = calls[calls.length - 1];

				expect(lastCall[0]).toContain('ref=edit');
			}
		});
	});
});
