import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import dayjs from 'dayjs';
import '../__mocks__/mockStoreToken';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import mockComponent, {
	mockComponentWithAttrs
} from 'tests-fe/utils/mock-component';

import UserKostModalStopRent from 'Js/_user/components/user-kost/UserKostModalStopRent.vue';
import store from 'Js/_user/store/index';
import UserKostModalStopRentInitialState from '../__mocks__/UserKostModalStopRentInitialState';

// mock imports
jest.mock('Js/_user/event-bus/event-bus', () => {
	const EventBus = {
		$emit: jest.fn(),
		$on: jest.fn((event, fn) => {
			fn({
				isOpenModal: true
			});
		})
	};
	global.EventBus = EventBus;

	return global.EventBus;
});

const tracker = jest.fn();
global.tracker = tracker;

const postTerminateResponse = {
	data: {
		status: true,
		data: {
			title: 'test'
		}
	}
};

const postReviewAddResponse = {
	data: {
		status: true,
		review: {
			rating: '5.0'
		}
	}
};

const generateAxios = (
	stopRent = postTerminateResponse,
	review = postReviewAddResponse
) => {
	return {
		post: jest.fn(endpoint => {
			if (endpoint === '/contract/request/terminate') {
				return jest.fn().mockResolvedValue(stopRent)();
			} else if (endpoint === '/stories/review/add') {
				return jest.fn().mockResolvedValue(review)();
			}

			return jest.fn().mockRejectedValue('error')();
		}),
		all: jest.fn().mockResolvedValue('result'),
		spread: jest.fn(fn => {
			fn(stopRent, review);
		}),
		get: jest.fn().mockResolvedValue({
			data: {
				status: true,
				data: ['reason 1', 'reason 2']
			}
		})
	};
};

global.axios = generateAxios();

describe('UserKostModalStopRent.vue', () => {
	const localVue = createLocalVue();
	localVue.use(Vuex);
	localVue.prototype.$dayjs = dayjs;

	// mock window
	const notify = jest.fn();
	mockWindowProperty('bugsnagClient', { notify });

	// mock stubbed component
	const stubs = {
		modal: mockComponentWithAttrs({ id: 'userKostModalStopRent' }),
		radio: mockComponent,
		checkbox: mockComponent,
		StarRating: mockComponentWithAttrs({ id: 'StarRating' }),
		UserKostReviewImageUpload: mockComponentWithAttrs({
			id: 'UserKostReviewImageUpload'
		}),
		'v-select': mockComponent
	};

	const mocks = {
		navigator: { isMobile: true },
		$toasted: { show: jest.fn() },
		swalError: jest.fn(),
		openSwalLoading: jest.fn(),
		closeSwalLoading: jest.fn()
	};

	// mount function
	const mount = () => {
		return shallowMount(UserKostModalStopRent, {
			localVue,
			stubs,
			mocks,
			store: new Vuex.Store(store)
		});
	};

	describe('render component correctly', () => {
		let wrapper;

		beforeEach(() => {
			wrapper = mount();
		});

		it('should render container properly', () => {
			expect(wrapper.find('#userKostModalStopRent').exists()).toBeTruthy();
		});

		it('should render UserKostReviewImageUpload when kost is already assigned', () => {
			wrapper.setData({ assigned: true });

			expect(wrapper.find('#UserKostReviewImageUpload').exists()).toBeTruthy();
		});
	});

	describe('render computed value properly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = mount();
		});

		it('should return token value from $store.state.token properly', () => {
			expect(wrapper.vm.token).toBe(global.csrfToken);
		});

		it('should return checkoutDateList value  properly', () => {
			wrapper.vm.$store.state.userKost.booking_data = {
				nearest_checkout_date_multiple: [
					'2020-08-10 08:09:10',
					'2020-09-10 10:00:00'
				]
			};

			// booking_data is available
			expect(wrapper.vm.checkoutDateList).toEqual([
				{
					value: '2020-08-10 08:09:10',
					label: 'Monday, 10 August 2020'
				},
				{
					value: '2020-09-10 10:00:00',
					label: 'Thursday, 10 September 2020'
				}
			]);

			// booking_data is not available
			wrapper.vm.$store.state.userKost = {
				contract_id: 'contract_id',
				room_data: {
					_id: '1'
				},
				booking_data: {
					nearest_checkout_date_multiple: [],
					checkin: '2020-02-02',
					checkout: '2020-12-12'
				}
			};
			expect(wrapper.vm.checkoutDateList).toEqual([]);
		});

		it('should return isButtonSubmitNotValid value properly', async () => {
			await wrapper.setData({
				ratingLists: [1, 1, 2, 3, 4, 5],
				reviewDescription: 'test',
				imageUploaded: 'image',
				dateSelected: 'date',
				reasonRadioValue: 'other',
				otherReason: 'test'
			});
			expect(wrapper.vm.isButtonSubmitNotValid).toBeFalsy();
		});

		it('should return reasonTextCount value properly', () => {
			wrapper.setData({ otherReason: 'test' });
			expect(wrapper.vm.reasonTextCount).toBe(4);
		});

		it('should return userKost value properly', () => {
			expect(wrapper.vm.userKost).toEqual(wrapper.vm.$store.state.userKost);
		});

		it('should get review Data properly', () => {
			wrapper.vm.$store.state.reviewData = {
				lastReview: '2',
				tenantReview: { reviewScore: '4' }
			};

			expect(wrapper.vm.reviewData).toBe('2');
			expect(wrapper.vm.reviewScore).toBe('4');
		});
	});

	describe('call functions correctly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = mount();
		});

		it('should set showModal as false and reset data when calling onModalClosedOrCancel function', async () => {
			await wrapper.vm.onModalClosedOrCancel();
			expect(wrapper.vm.showModal).toBeFalsy();
			expect(wrapper.vm.$data).toEqual(UserKostModalStopRentInitialState);
		});

		it('should set image data when calling handleImageUpload function', async () => {
			await wrapper.vm.handleImageUpload('test');
			expect(wrapper.vm.imageUploaded).toBe('test');
		});

		it('should listen to eventBus and do actions when calling listenEventModal function', async () => {
			await wrapper.vm.listenEventModal();
			await new Promise(resolve => setImmediate(resolve));

			expect(wrapper.vm.showModal).toBeTruthy();
			expect(wrapper.vm.reasonLists).toEqual(['reason 1', 'reason 2']);
		});

		it('should handle get reason list properly', async () => {
			wrapper.setData({ reasonLists: [] });
			global.axios.get = jest.fn().mockRejectedValue('error');

			await wrapper.vm.getReasonList();
			await new Promise(resolve => setImmediate(resolve));

			expect(wrapper.vm.reasonLists).toEqual([]);
			expect(notify).toBeCalledWith('error');

			jest.clearAllMocks();
			global.axios.get = jest.fn().mockResolvedValue({
				data: { status: false, meta: { message: 'message' } }
			});

			await wrapper.vm.getReasonList();
			await new Promise(resolve => setImmediate(resolve));

			expect(wrapper.vm.reasonLists).toEqual([]);
			expect(mocks.swalError).toBeCalledWith(null, 'message');
			expect(notify).not.toBeCalled();
		});

		it('should call assignLastData function on showModal value change', async () => {
			const assignLastDataSpy = jest.spyOn(wrapper.vm, 'assignLastData');
			wrapper.setData({
				showModal: false,
				assigned: false
			});
			await wrapper.vm.$nextTick();
			wrapper.vm.showModal = true;
			await wrapper.vm.$nextTick();

			expect(assignLastDataSpy).toBeCalled();
			assignLastDataSpy.mockRestore();
		});

		it('should return labeled date when calling labelingDateCheckoutList method', () => {
			const date = wrapper.vm.labelingDateCheckoutList(['2020-08-10 08:09:10']);

			expect(date).toEqual([
				{
					value: '2020-08-10 08:09:10',
					label: 'Monday, 10 August 2020'
				}
			]);
		});

		it('should set ratingLists when calling changedRating method', async () => {
			await wrapper.vm.changedRating(1, 0);

			expect(wrapper.vm.ratingLists[0].value).toBe(1);
		});

		it('should return formatted date properly when calling formatDate method', () => {
			const date = wrapper.vm.formatDate('2020-08-10 08:09:10');

			expect(date).toBe('Monday, 10 August 2020');
		});

		it('should handle confirm stop rent post action', async () => {
			const spyOpenGroupChannel = jest.spyOn(wrapper.vm, 'openGroupChannel');
			wrapper.vm.$store.state.userKost = {
				room_data: { group_channel_url: '' }
			};
			await wrapper.setData({ showModal: true });

			wrapper.vm.confirmStopRent();

			expect(spyOpenGroupChannel).not.toBeCalled();
			expect(wrapper.vm.showModal).toBe(false);

			wrapper.vm.$store.state.userKost = {
				room_data: { group_channel_url: 'group_channel_url' }
			};
			await wrapper.setData({
				showModal: true
			});

			wrapper.vm.confirmStopRent();

			expect(spyOpenGroupChannel).toBeCalledWith('group_channel_url');
			expect(wrapper.vm.showModal).toBe(false);
		});

		it('should set ratingList when calling assignLastData method properly', async () => {
			// is anonymous
			wrapper.setProps({
				editData: {
					clean: 0,
					safe: 1,
					happy: 2,
					public_facilities: 3,
					room_facilities: 4,
					pricing: 5,
					share_word: 'test',
					is_anonim: true
				}
			});
			await wrapper.vm.assignLastData();
			expect(wrapper.vm.assigned).toBeTruthy();
			expect(wrapper.vm.ratingLists[0].value).toBe(0);
			expect(wrapper.vm.ratingLists[1].value).toBe(1);
			expect(wrapper.vm.username).toBe(1);

			// not anonymous
			wrapper.setProps({
				editData: {
					share_word: 'test_2',
					is_anonim: false
				}
			});
			await wrapper.vm.assignLastData();
			expect(wrapper.vm.username).toBe(0);
		});

		it('should assign the selected date', () => {
			wrapper.vm.selectDate('08 Oct 2019');
			expect(wrapper.vm.dateSelected.label.toLowerCase()).toBe(
				'tuesday, 8 october 2019'
			);
		});

		it('should send tracker to moengage', () => {
			wrapper.vm.trackSuccessStopRent('ingin pindah');
			expect(tracker).toBeCalled();
		});

		it('should open warning pop up', () => {
			wrapper.vm.isModalWarning = false;
			wrapper.vm.toggleModalWarning(true);
			expect(wrapper.vm.isModalWarning).toBe(true);
		});

		it('should close the warning pop up when group channel doesnt exist', () => {
			wrapper.vm.isModalWarning = true;
			wrapper.vm.confirmStopRent();
			expect(wrapper.vm.isModalWarning).toBe(false);
		});

		it('should handle submit data properly', async () => {
			await wrapper.setData({ successStopRent: false, successReview: false });
			await wrapper.vm.submitAllData();
			await new Promise(resolve => setImmediate(resolve));

			expect(wrapper.vm.successStopRent).toBe(true);
			expect(wrapper.vm.successReview).toBe(true);

			// status: false case
			const failedData = {
				data: {
					status: false,
					meta: {
						message: 'error'
					}
				}
			};
			global.axios = generateAxios(failedData, failedData);

			await wrapper.setData({ successStopRent: false, successReview: false });
			await wrapper.vm.submitAllData();
			await new Promise(resolve => setImmediate(resolve));

			expect(wrapper.vm.successStopRent).toBe(false);
			expect(wrapper.vm.successReview).toBe(false);

			// catch
			global.axios.all = jest.fn().mockRejectedValue('error');

			await wrapper.setData({ successStopRent: false, successReview: false });
			await wrapper.vm.submitAllData();
			await new Promise(resolve => setImmediate(resolve));

			expect(wrapper.vm.successStopRent).toBe(false);
			expect(wrapper.vm.successReview).toBe(false);
			expect(notify).toBeCalledWith('error');
		});
	});
});
