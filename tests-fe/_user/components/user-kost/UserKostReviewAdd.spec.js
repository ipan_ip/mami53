import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';

import mockAxios from 'tests-fe/utils/mock-axios';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import { mockComponentWithAttrs } from 'tests-fe/utils/mock-component';

import UserKostReviewAdd from 'Js/_user/components/user-kost/UserKostReviewAdd.vue';

global.Vue = require('vue');
const mixinSwalLoading = require('Js/@mixins/MixinSwalLoading');
const MixinNavigatorIsMobile = require('Js/@mixins/MixinNavigatorIsMobile');

describe('UserKostReviewAdd.vue', () => {
	const localVue = createLocalVue();
	localVue.use(Vuex);
	localVue.mixin([mixinSwalLoading, MixinNavigatorIsMobile]);

	global.axios = mockAxios;

	const notify = jest.fn();
	mockWindowProperty('bugsnagClient', { notify });

	const tracker = jest.fn();
	mockWindowProperty('tracker', tracker);

	const swal = jest.fn();
	mockWindowProperty('swal', swal);

	const locationReload = jest.fn();
	mockWindowProperty('location', { reload: locationReload });

	const UserKostReviewAddStore = {
		state: {
			token: 'token',
			authData: 'authData'
		}
	};

	const swalError = jest.fn();
	const swalSuccessTimer = jest.fn();

	const shallowMountComponent = (mockProps = null, mockStore = null) => {
		return shallowMount(UserKostReviewAdd, {
			localVue,
			propsData: mockProps || {
				kostId: 0,
				editData: {},
				detail: {}
			},
			store: new Vuex.Store(mockStore || UserKostReviewAddStore),
			methods: { swalError, swalSuccessTimer },
			stubs: {
				UserKostReviewImageUpload: mockComponentWithAttrs({
					id: 'uploadImageReview'
				}),
				StarRating: mockComponentWithAttrs(),
				radio: mockComponentWithAttrs(),
				FilterCloseButton: mockComponentWithAttrs({
					class: 'btn-close-filter'
				})
			}
		});
	};

	describe('render component correctly', () => {
		let wrapper;

		beforeEach(() => {
			wrapper = shallowMountComponent();
		});

		it('should render container properly', () => {
			expect(wrapper.find('#addReviewKost').exists()).toBeTruthy();
		});
	});

	describe('call functions correctly', () => {
		let wrapper;

		beforeEach(() => {
			axios.mockResolve({
				data: {
					status: 200
				}
			});
			wrapper = shallowMountComponent();
		});

		it('should set ratings from editData when available', () => {
			const mockProps = {
				kostId: 0,
				editData: {
					clean: 0,
					safe: 1,
					happy: 2,
					room_facilities: 3,
					public_facilities: 4,
					pricing: 5
				},
				detail: {}
			};

			wrapper = shallowMountComponent(mockProps);

			expect(wrapper.vm.ratings[0].val).toBe(0);
			expect(wrapper.vm.ratings[1].val).toBe(1);
			expect(wrapper.vm.ratings[2].val).toBe(2);
			expect(wrapper.vm.ratings[3].val).toBe(3);
			expect(wrapper.vm.ratings[4].val).toBe(4);
			expect(wrapper.vm.ratings[5].val).toBe(5);
		});

		it('should set username as 0 if is_anonim was false', () => {
			const mockProps = {
				kostId: 0,
				editData: {
					is_anonim: false
				},
				detail: {}
			};

			wrapper = shallowMountComponent(mockProps);

			expect(wrapper.vm.username).toBe(0);
		});

		it('should return editData as {} when not defined', () => {
			const mockProps = {
				kostId: 0,
				detail: {}
			};

			wrapper = shallowMountComponent(mockProps);

			expect(wrapper.vm.editData).toEqual({});
		});

		it('should set username as 1 if is_anonim was true', () => {
			const mockProps = {
				kostId: 0,
				editData: {
					is_anonim: true
				},
				detail: {}
			};

			wrapper = shallowMountComponent(mockProps);

			expect(wrapper.vm.username).toBe(1);
		});

		it('should update ratings when updateReview function is called', async () => {
			await wrapper.vm.updateReview(5, 0);

			expect(wrapper.vm.ratings[0].val).toBe(5);
		});

		it('should set imageUploaded when calling handleImageUpload', async () => {
			await wrapper.vm.handleImageUpload('test');

			expect(wrapper.vm.imageUploaded).toBe('test');
		});

		it('should generate correct label when calling generateLabel function', () => {
			const label = wrapper.vm.generateLabel('MAMI KOST TEST');

			expect(label).toBe('lbl_mAMIKOSTTEST');
		});

		it('should return true when one of the ratings value is 0', () => {
			const hasBlankRating = wrapper.vm.checkBlankRating();

			expect(hasBlankRating).toBeTruthy();
		});

		it('should return false when none of the ratings value is 0', async () => {
			await wrapper.setData({
				ratings: [
					{ name: 'Kebersihan', val: 1 },
					{ name: 'Keamanan', val: 1 },
					{ name: 'Kenyamanan', val: 1 },
					{ name: 'Fasilitas Kamar', val: 1 },
					{ name: 'Fasilitas Bersama', val: 1 },
					{ name: 'Harga', val: 1 }
				]
			});

			const hasBlankRating = wrapper.vm.checkBlankRating();

			expect(hasBlankRating).toBeFalsy();
		});

		it('should set tracker propertyId from kostId data when handleMoEngageVisited is called', () => {
			wrapper.setProps({ kostId: 1, editData: {}, detail: {} });
			wrapper.vm.handleMoEngageVisited();

			expect(tracker).toBeCalled();

			const trackerCalls = tracker.mock.calls || [[]];
			const lastTrackerCall = trackerCalls[trackerCalls.length - 1] || [];
			const trackerSentParam = lastTrackerCall[1][1] || {};

			expect(trackerSentParam.property_id).toBe(1);
		});

		it('should set tracker propertyId from kostId data when handleMoEngageSubmit is called', () => {
			wrapper.setProps({ kostId: 1, editData: {}, detail: {} });
			wrapper.vm.handleMoEngageSubmit();

			expect(tracker).toBeCalled();

			const trackerCalls = tracker.mock.calls || [[]];
			const lastTrackerCall = trackerCalls[trackerCalls.length - 1] || [];
			const trackerSentParam = lastTrackerCall[1][1] || {};

			expect(trackerSentParam.property_id).toBe(1);
		});

		it('should set tracker interface as desktop when navigator isMobile is false', async () => {
			const { navigator = {} } = wrapper.vm;

			await wrapper.vm.handleMoEngageVisited();

			const trackerCalls = tracker.mock.calls || [[]];
			const lastTrackerCall = trackerCalls[trackerCalls.length - 1] || [];
			const trackerSentParam = lastTrackerCall[1][1] || {};

			expect(navigator.isMobile).toBeFalsy();
			expect(trackerSentParam.interface).toBe('desktop');
		});

		it('should call tracker for moengage on created', () => {
			expect(tracker).toBeCalled();

			const trackerCalls = tracker.mock.calls || [[]];
			expect(trackerCalls[0][0]).toBe('moe');
		});

		it('should emit close when clicking filter-close-button', () => {
			const $filterCloseButton = wrapper.find('.btn-close-filter');

			if ($filterCloseButton && $filterCloseButton.exists()) {
				$filterCloseButton.vm.$emit('press');

				expect(wrapper.emitted().close).toBeTruthy();
			}
		});

		it('should return token and authData from vuex properly', () => {
			const { token, authData } = wrapper.vm;

			expect(token).toBe('token');
			expect(authData).toBe('authData');
		});

		it('should call swalError when trying to submit review while some of the ratings value are still 0', async () => {
			await wrapper.setData({
				ratings: [
					{ name: 'Kebersihan', val: 0 },
					{ name: 'Keamanan', val: 0 },
					{ name: 'Kenyamanan', val: 1 },
					{ name: 'Fasilitas Kamar', val: 1 },
					{ name: 'Fasilitas Bersama', val: 1 },
					{ name: 'Harga', val: 1 }
				]
			});

			const submitButton = wrapper.find('.btn.btn-info');
			if (submitButton && submitButton.exists()) {
				submitButton.trigger('click');

				expect(swalError).toBeCalled();
				const swalCalls = swalError.mock.calls || [];
				expect(swalCalls[swalCalls.length - 1][1]).toBe(
					'Maaf, untuk bisa menambahkan review Anda harus mengisi rating terlebih dahulu'
				);
			}
		});

		it('should call swalError when trying to submit review while reviewContent is still empty', async () => {
			await wrapper.setData({
				ratings: [
					{ name: 'Kebersihan', val: 1 },
					{ name: 'Keamanan', val: 1 },
					{ name: 'Kenyamanan', val: 1 },
					{ name: 'Fasilitas Kamar', val: 1 },
					{ name: 'Fasilitas Bersama', val: 1 },
					{ name: 'Harga', val: 1 }
				],
				reviewContent: ''
			});

			const submitButton = wrapper.find('.btn.btn-info');
			if (submitButton && submitButton.exists()) {
				submitButton.trigger('click');

				expect(swalError).toBeCalled();

				const swalCalls = swalError.mock.calls || [];
				expect(swalCalls[swalCalls.length - 1][1]).toBe(
					'Maaf, untuk bisa menambahkan review Anda harus menulis review sedikitnya 20 karakter'
				);
			}
		});

		it('should call swalSuccessTimer on successful axios request when trying to submit review if user has filled reviewContent and ratings', async () => {
			await wrapper.setData({
				ratings: [
					{ name: 'Kebersihan', val: 1 },
					{ name: 'Keamanan', val: 1 },
					{ name: 'Kenyamanan', val: 1 },
					{ name: 'Fasilitas Kamar', val: 1 },
					{ name: 'Fasilitas Bersama', val: 1 },
					{ name: 'Harga', val: 1 }
				],
				reviewContent: '12345678901234567890'
			});

			await wrapper.setMethods({
				openSwalLoading: jest.fn(),
				closeSwalLoading: jest.fn()
			});

			const submitButton = wrapper.find('.btn.btn-info');
			if (submitButton && submitButton.exists()) {
				submitButton.trigger('click');

				wrapper.vm.$nextTick(() => {
					expect(swalSuccessTimer).toBeCalled();
					expect(locationReload).toBeCalled();
					expect(tracker).toBeCalled();
				});
			}
		});

		it('should call swalError on unsuccessful axios request when trying to submit review if user has filled reviewContent and ratings', async () => {
			axios.mockResolve({
				data: {
					status: false,
					meta: {
						message: 'error'
					}
				}
			});

			await wrapper.setData({
				ratings: [
					{ name: 'Kebersihan', val: 1 },
					{ name: 'Keamanan', val: 1 },
					{ name: 'Kenyamanan', val: 1 },
					{ name: 'Fasilitas Kamar', val: 1 },
					{ name: 'Fasilitas Bersama', val: 1 },
					{ name: 'Harga', val: 1 }
				],
				reviewContent: '12345678901234567890'
			});

			const swals = {
				openSwalLoading: jest.fn(),
				closeSwalLoading: jest.fn()
			};
			await wrapper.setMethods(swals);

			const submitButton = wrapper.find('.btn.btn-info');
			if (submitButton && submitButton.exists()) {
				submitButton.trigger('click');

				wrapper.vm.$nextTick(() => {
					expect(swalError).toBeCalled();
					expect(tracker).toBeCalled();

					const swalCalls = swalError.mock.calls || [[]];
					expect(swalCalls[swalCalls.length - 1][1]).toBe('error');
				});
			}
		});
	});

	describe('catch error correctly', () => {
		let wrapper;

		beforeEach(() => {
			axios.mockReject('typo');
			wrapper = shallowMountComponent();
		});

		it('should catch error on rejected axios request when trying to submit review if user has filled reviewContent and ratings', async () => {
			await wrapper.setData({
				ratings: [
					{ name: 'Kebersihan', val: 1 },
					{ name: 'Keamanan', val: 1 },
					{ name: 'Kenyamanan', val: 1 },
					{ name: 'Fasilitas Kamar', val: 1 },
					{ name: 'Fasilitas Bersama', val: 1 },
					{ name: 'Harga', val: 1 }
				],
				reviewContent: '12345678901234567890'
			});

			const swals = {
				openSwalLoading: jest.fn(),
				closeSwalLoading: jest.fn()
			};
			await wrapper.setMethods(swals);

			const submitButton = wrapper.find('.btn.btn-info');
			if (submitButton && submitButton.exists()) {
				submitButton.trigger('click');

				wrapper.vm.$nextTick(() => {
					expect(swalError).toBeCalled();
				});
			}
		});
	});
});
