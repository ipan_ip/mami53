import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import { JSDOM } from 'jsdom';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

import UserKostClaimDetail from 'Js/_user/components/user-kost/UserKostClaimDetail.vue';

const dom = new JSDOM();

describe('UserKostClaimDetail.vue', () => {
	const localVue = createLocalVue();
	localVue.use(Vuex);
	mockVLazy(localVue);

	const document = dom.window.document;
	const execCommand = jest.fn();
	const documentSelection = jest.fn(() => {});
	const createTextRange = jest.fn(() => {
		return {
			moveToElementText: jest.fn(),
			select: jest.fn()
		};
	});
	const createRange = jest.fn(() => {
		return {
			selectNode: jest.fn()
		};
	});
	const getSelection = jest.fn(() => {
		return {
			addRange: jest.fn(),
			removeAllRanges: jest.fn()
		};
	});

	global.document = document;
	global.document.body.createTextRange = createTextRange;

	global.document.createRange = createRange;
	global.document.execCommand = execCommand;
	global.document.selection = documentSelection;

	global.window = dom.window;
	global.window.getSelection = getSelection;

	const shallowMountComponent = (mockProps = null) => {
		return shallowMount(UserKostClaimDetail, {
			localVue,
			propsData: mockProps || {
				claimData: {
					payment_status: 'paid'
				},
				claimDetail: {}
			}
		});
	};

	describe('render component correctly', () => {
		let wrapper;

		beforeEach(() => {
			wrapper = shallowMountComponent();
		});

		it('should render container', () => {
			expect(wrapper.find('.user-kost-claim-detail').exists()).toBeTruthy();
		});

		it('should render statusPaymentText as Dibayar if claimData.payment_status is paid', () => {
			wrapper.setProps({ claimData: { payment_status: 'paid' } });

			expect(wrapper.vm.statusPaymentText).toBe('Dibayar');
		});

		it('should render statusPaymentText as Belum Dibayar if claimData.payment_status is unpaid', () => {
			wrapper.setProps({ claimData: { payment_status: 'unpaid' } });

			expect(wrapper.vm.statusPaymentText).toBe('Belum Dibayar');
		});
	});

	describe('call functions correctly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = shallowMountComponent();
		});

		it('should call backToClaim function on claim-detail-header click', () => {
			const detailHeader = wrapper.find('.claim-detail-header');
			if (detailHeader && detailHeader.exists()) {
				detailHeader.trigger('click');

				expect(wrapper.emitted().back).toBeTruthy();
			}
		});

		it('should call copyText function on copy-text link click', () => {
			const copyLink = wrapper.find('.copy-text');

			if (copyLink && copyLink.exists()) {
				copyLink.trigger('click');

				expect(execCommand).toBeCalled();
			}
		});

		it('should call createTextRange function on copy-text link click if document.selection function is available', () => {
			const copyLink = wrapper.find('.copy-text');

			if (copyLink && copyLink.exists()) {
				copyLink.trigger('click');

				expect(createTextRange).toBeCalled();
			}
		});

		it('should call createRange function on copy-text link click if the document.selection function is unavailable and window.getSelection is available', () => {
			global.document.selection = null;

			const copyLink = wrapper.find('.copy-text');

			if (copyLink && copyLink.exists()) {
				copyLink.trigger('click');

				expect(createRange).toBeCalled();
			}
		});
	});
});
