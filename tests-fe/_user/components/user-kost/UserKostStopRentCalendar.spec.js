import { createLocalVue, shallowMount } from '@vue/test-utils';
import UserKostStopRentCalendar from 'Js/_user/components/user-kost/UserKostStopRentCalendar.vue';

const localVue = createLocalVue();

describe('UserKostStopRentCalendar.vue', () => {
	const propsData = {
		showModal: true
	};

	const closeModalSpy = jest.spyOn(
		UserKostStopRentCalendar.methods,
		'closeModal'
	);

	const wrapper = shallowMount(UserKostStopRentCalendar, {
		localVue,
		propsData,
		methods: {
			closeModalSpy
		}
	});

	it('should render the component', () => {
		expect(wrapper.find('.stop-rent-calendar').exists()).toBe(true);
	});

	it('should close the calendar', () => {
		wrapper.vm.closeModal();
		expect(wrapper.emitted('close')).toBeTruthy();
	});

	it('should save selected date', () => {
		wrapper.vm.selectDate('10-10-2020');
		expect(wrapper.vm.checkoutDate).toBe('10-10-2020');
	});

	it('should emit the selected date and close the modal', () => {
		jest.clearAllMocks();
		wrapper.vm.selectDate('11-10-2020');
		wrapper.vm.chooseDate();
		expect(wrapper.emitted('select')[0][0]).toEqual('11-10-2020');
		expect(closeModalSpy).toBeCalled();
	});
});
