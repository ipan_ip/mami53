import { createLocalVue, shallowMount } from '@vue/test-utils';
import UserKostBillingCategory from 'Js/_user/components/user-kost/UserKostBillingCategory.vue';

global.bugsnagClient = { notify: jest.fn() };
const localVue = createLocalVue();
describe('UserKostBillingCategory.vue', () => {
	const wrapper = shallowMount(UserKostBillingCategory, {
		localVue
	});

	it('should render properly', () => {
		expect(wrapper.findAll('.menu-slide button').length).toBe(2);
	});
	it('should handle click event', () => {
		const buttons = wrapper.findAll('.menu-slide button');
		const comingSoonButton = buttons.at(0);
		comingSoonButton.trigger('click');
		expect(wrapper.vm.selectedCategory).toBe(true);
		expect(wrapper.emitted().actionChangedCategory[0][0]).toBe(true);

		const paidButton = buttons.at(1);
		paidButton.trigger('click');
		expect(wrapper.vm.selectedCategory).toBe(false);
		expect(wrapper.emitted().actionChangedCategory[1][0]).toBe(false);
	});
});
