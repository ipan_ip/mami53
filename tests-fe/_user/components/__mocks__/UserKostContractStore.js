const UserKostContractStore = {
	state: {
		userKost: {
			contract_id: '',
			booking_data: {
				checkin: '',
				checkout: '',
				duration_string: '',
				rent_count_type: 'weekly'
			},
			prices: {
				fine: {
					billing_date: '',
					duration_type: '',
					maximum_length: 0,
					price_total_string: ''
				}
			},
			invoice: [
				{
					scheduled_date_second: '1589189180 '
				}
			]
		},
		bookingDetail: {
			booking_data: {
				rent_count_type: ''
			}
		},
		statusRent: {
			status: false,
			checkout_date: false
		}
	},
	mutations: {
		setStatusRent(state, payload) {
			state.statusRent = payload;
		}
	}
};

export default UserKostContractStore;
