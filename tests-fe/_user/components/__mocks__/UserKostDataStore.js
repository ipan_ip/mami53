const UserKostDataStore = {
	state: {
		userKost: {
			room_data: {
				_id: 'test',
				available_rating: true,
				photo: {
					large: '',
					medium: '',
					small: ''
				}
			},
			booking_data: {
				booking_funnel_from: 'owner'
			}
		}
	},
	mutations: {}
};

export default UserKostDataStore;
