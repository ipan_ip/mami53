import mockWindowProperty from 'tests-fe/utils/mock-window-property';

const csrfToken = 'csrf-token-123';
global.csrfToken = csrfToken;

mockWindowProperty(
	'document.head.querySelector',
	jest.fn().mockImplementation(() => ({ content: csrfToken }))
);
