const UserKostClaimStore = {
	state: {
		userKost: {
			contract_id: '',
			booking_data: {
				checkin: '',
				checkout: '',
				duration_string: '',
				rent_count_type: 'weekly'
			}
		},
		statusRent: {
			status: false,
			checkout_date: false
		}
	},
	mutations: {
		setStatusRent(state, payload) {
			state.statusRent = payload;
		}
	}
};

export default UserKostClaimStore;
