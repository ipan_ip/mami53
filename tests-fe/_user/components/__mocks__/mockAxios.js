const draft = {
	data: {
		total_of_draft: 2
	},
	status: true
};

const profile = {
	profile: {
		name: 'Mamikos',
		jobs: 'kerja'
	},
	status: true
};

const socialMedia = { data: {}, status: true };

function MockAxios({ resolved = true, status = true } = {}) {
	this.resolved = resolved;
	this.mockFunction = `get${this.resolved ? 'Resolved' : 'Rejected'}`;

	this.get = jest.fn(endpoint => {
		return this[this.mockFunction](endpoint).then(r => r);
	});

	this.getResolved = jest.fn(endpoint => {
		let response = null;
		if (endpoint === '/user/profile') {
			response = profile;
		} else if (endpoint === '/user/user-social') {
			response = socialMedia;
		} else if (endpoint === '/user/draft-booking/notification') {
			response = draft;
		}

		return new Promise(resolve => resolve({ data: { ...response, status } }));
	});

	this.getRejected = jest.fn(() => {
		return new Promise((resolve, reject) => reject(new Error('error')));
	});
}

export { draft, profile, socialMedia, MockAxios };
