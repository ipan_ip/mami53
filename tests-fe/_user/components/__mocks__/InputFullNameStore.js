const InputFullNameStore = {
	state: {
		userEditProfile: {
			name: ''
		},
		formState: false
	},
	mutations: {
		updateName(state, payload) {
			state.userEditProfile.name = payload;
		},
		setFormState(state, payload) {
			state.formState = payload;
		}
	}
};

export default InputFullNameStore;
