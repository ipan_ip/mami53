const store = {
	state: {
		authData: {
			all: {
				id: 1725551
			}
		},
		userMenuActive: 'user-menu',
		voucherCount: 0,
		isVoucherDetail: false
	},
	actions: {
		getVoucherCount({ commit, state }) {
			axios.get('/user/voucher/count').then(response => {
				if (response.data.status) {
					commit('setVoucherCount', response.data.count);
				}
			});
		}
	},
	mutations: {
		setAuthData(state, payload) {
			state.authData = payload;
		},
		setVoucherCount(state, payload) {
			state.voucherCount = payload;
		},
		setMenu(state, payload) {
			state.userMenuActive = payload;
		},
		setDetailVoucher(state, payload) {
			state.isVoucherDetail = payload;
		}
	},
	getters: {
		userId: state => {
			return state.authData.all.id;
		}
	}
};

export default store;
