const store = {
	state: {
		userMenuActice: 'user-menu',
		profileBreadCrumb: [],
		authData: {
			all: {
				id: 1930830
			}
		}
	},
	mutations: {
		setBreadcrumb(state, payload) {
			if (payload === 'user') {
				state.profileBreadcrumb = [
					{ name: 'Home', url: '/' },
					{ name: 'User', url: '/user' }
				];
			} else if (payload === 'mamipay') {
				state.profileBreadcrumb = [
					{ name: 'Home', url: '/' },
					{ name: 'User', url: '/user' },
					{ name: 'Mamipay', url: '/isi-saldo' }
				];
			}
		},
		setMenu(state, payload) {
			state.userMenuActive = payload;
		}
	}
};

const newStore = {
	state: {
		userMenuActice: 'user-menu',
		profileBreadCrumb: [],
		authData: {
			all: {
				id: 0
			}
		}
	},
	mutations: {
		setBreadcrumb(state, payload) {
			if (payload === 'user') {
				state.profileBreadcrumb = [
					{ name: 'Home', url: '/' },
					{ name: 'User', url: '/user' }
				];
			} else if (payload === 'mamipay') {
				state.profileBreadcrumb = [
					{ name: 'Home', url: '/' },
					{ name: 'User', url: '/user' },
					{ name: 'Mamipay', url: '/isi-saldo' }
				];
			}
		},
		setMenu(state, payload) {
			state.userMenuActive = payload;
		}
	}
};

const storeCollection = [store, newStore];

export default storeCollection;
