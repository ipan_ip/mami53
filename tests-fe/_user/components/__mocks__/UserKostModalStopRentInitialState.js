const initialState = {
	showModal: false,
	reasonRadioValue: '',
	otherReason: '',
	reasonLists: [],
	ratingLists: [
		{
			label: 'Kebersihan',
			value: 0
		},
		{
			label: 'Keamanan',
			value: 0
		},
		{
			label: 'Kenyamanan',
			value: 0
		},
		{
			label: 'Fasilitas Umum',
			value: 0
		},
		{
			label: 'Fasilitas Kamar',
			value: 0
		},
		{
			label: 'Harga',
			value: 0
		}
	],
	dateSelected: '',
	dateFormat: '',
	isDisabledBtnSubmit: false,
	successStopRent: false,
	successReview: false,
	reviewDescription: '',
	username: 0,
	imageUploaded: [],
	assigned: false,
	isModalWarning: false,
	errorMessage: '',
	reviewRating: 0,
	responseStatus: {
		title: '',
		description: ''
	}
};

export default initialState;
