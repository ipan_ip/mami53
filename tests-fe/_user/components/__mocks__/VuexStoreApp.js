export default {
	state: {
		userMenuActive: 'user-menu',
		userProfile: {},
		userEditProfile: {},
		profileBreadcrumb: [],
		isVoucherDetail: true,
		bookingNotification: {
			draft: 0,
			lastSeen: 0
		},
		isUserLoggedInViaSocial: false
	},
	mutations: {
		setDraftNotification(state, payload) {
			state.bookingNotification.draft = payload;
		},
		setLastSeenNotification(state, payload) {
			state.bookingNotification.lastSeen = payload;
		},
		setProfileUser(state, payload) {
			state.userProfile = payload;
		},
		setJobs(state, payload) {
			state.userEditProfile.jobs = payload;
		},
		setIsUserLoggedInViaSocial(state, payload) {
			state.isUserLoggedInViaSocial = payload;
		}
	},
	getters: {
		bookingNotificationCount: state => {
			return Object.values(state.bookingNotification).reduce((acc, current) => {
				return acc + current;
			}, 0);
		}
	}
};
