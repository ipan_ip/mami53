const InputPhotosStore = {
	state: {
		userPhoto: {
			medium: ''
		}
	},
	mutations: {
		setValue(state, payload) {
			state.userPhoto.medium = payload;
		}
	}
};

export default InputPhotosStore;
