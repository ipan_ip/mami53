const storeData = {
	state: {
		token: 'token',
		userKost: {
			contract_id: 'contract_id',
			booking_data: {
				nearest_checkout_date_multiple: ['2020-08-10 08:09:10'],
				checkin: '2020-02-02',
				checkout: '2020-12-12'
			},
			room_data: {
				_id: '1'
			}
		},
		roomDetail: {
			_id: 986547
		},
		reviewData: {
			lastReview: {},
			tenantReview: {
				reviewScore: '5'
			}
		}
	},
	mutations: {}
};

export default storeData;
