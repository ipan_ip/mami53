const InputPhoneEmergencyStore = {
	state: {
		userEditProfile: {
			phone_number_additional: ''
		}
	},
	mutations: {
		updateEmergency(state, payload) {
			state.userEditProfile.phone_number_additional = payload;
		}
	}
};

export default InputPhoneEmergencyStore;
