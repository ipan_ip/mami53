import { shallowMount, createLocalVue } from '@vue/test-utils';
import DetailVoucherCodeComponent from 'Js/_user/components/voucher-detail/DetailVoucherCode';
import mockVLazy from '../../../utils/mock-v-lazy';

describe('DetailVoucherCode.vue', () => {
	const localVue = createLocalVue();
	const propsData = {
		voucherCode: 'MAMIKOS2020',
		isAvailable: false,
		voucherId: 10001
	};

	const $store = {
		state: {
			authData: ''
		}
	};

	mockVLazy(localVue);

	global.tracker = jest.fn();

	const mount = () => {
		return shallowMount(DetailVoucherCodeComponent, {
			localVue,
			propsData,
			mocks: {
				$store
			}
		});
	};

	const wrapper = mount();

	const { vm } = wrapper;
	const voucerValue = 'LUNAS3BULAN';
	const tooltipMessageValue = [
		'Kode berhasil disalin',
		'Gagal salin kode voucher'
	];
	const ticketIcons = [
		'ic_voucher_detail_active.svg',
		'ic_voucher_detail_disable.svg'
	];
	const classText = ['text-active', 'text-disable'];
	const classButton = ['button-active', 'button-disable'];

	it('should render detail voucher code component properly', () => {
		expect(wrapper.find('.copy-code-card').exists()).toBe(true);
	});

	it('should render props properly', () => {
		wrapper.setProps({ voucherCode: voucerValue, isAvailable: true });
		expect(vm.voucherCode).toBe(voucerValue);
		expect(vm.isAvailable).toBe(true);
	});

	it('should render voucher code from props properly', () => {
		const wrapperVoucherCode = wrapper.find('.copy-code-text__code');
		expect(wrapperVoucherCode.text()).toBe(voucerValue);
	});

	it('should render data properly', () => {
		wrapper.setData({
			showTooltip: true,
			tooltipMessage: tooltipMessageValue[0]
		});
		expect(vm.showTooltip).toBe(true);
		expect(vm.tooltipMessage).toBe(tooltipMessageValue[0]);
	});

	it('should render ticket icons properly', () => {
		const imgIcons = wrapper.find('img[alt="ticket_icon"]');
		expect(vm.ticketIcon).toBe(ticketIcons[0]);
		wrapper.setProps({ isAvailable: false });
		expect(vm.ticketIcon).toBe(ticketIcons[1]);
		expect(imgIcons.exists()).toBe(true);
	});

	it('should render class text properly', () => {
		expect(vm.classText).toBe(classText[1]);
		wrapper.setProps({ isAvailable: true });
		expect(vm.classText).toBe(classText[0]);
	});

	it('should render class button properly', () => {
		expect(vm.classButton).toBe(classButton[0]);
		wrapper.setProps({ isAvailable: false });
		expect(vm.classButton).toBe(classButton[1]);
	});

	it('should show & hide tooltip properly', done => {
		const wrapper = mount();
		wrapper.vm.promoCodeCopied(tooltipMessageValue[0]);
		expect(wrapper.vm.showTooltip).toBe(true);
		expect(wrapper.vm.tooltipMessage).toBe(tooltipMessageValue[0]);

		setTimeout(() => {
			expect(wrapper.vm.showTooltip).toBe(false);
			done();
		}, 1000);
	});

	it('should toggle variable copying with true & false properly', done => {
		const wrapper = mount();

		wrapper.vm.promoCodeCopied(tooltipMessageValue[0]);
		expect(wrapper.vm.copying).toBe(true);

		setTimeout(() => {
			expect(wrapper.vm.copying).toBe(false);
			done();
		}, 1500);
	});

	it('should run copyPromoCode properly', () => {
		const wrapper = mount();
		wrapper.vm.$copyText = () => {
			return Promise.resolve('success').then(() => {
				wrapper.vm.promoCodeCopied(tooltipMessageValue[0]);
			});
		};

		wrapper.setProps({ isAvailable: true });

		wrapper.vm.$nextTick(() => {
			const button = wrapper.find('.copy-code-card__button');
			button.trigger('click');
			expect(button.exists()).toBe(true);
			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.showTooltip).toBe(true);
				expect(wrapper.vm.tooltipMessage).toBe(tooltipMessageValue[0]);
			});
		});
	});

	it('should call promoCodeCopied when running copyPromoCode properly', async () => {
		const wrapper = mount();
		const promoCodeCopiedSpy = jest.spyOn(wrapper.vm, 'promoCodeCopied');
		wrapper.vm.$copyText = jest.fn().mockRejectedValue('error');
		await wrapper.vm.copyPromoCode('event');
		await wrapper.vm.$nextTick();
		expect(promoCodeCopiedSpy).toBeCalled();
		promoCodeCopiedSpy.mockRestore();
	});
});
