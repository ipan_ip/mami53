import { shallowMount } from '@vue/test-utils';
import DetailVoucherCodeMobileComponent from 'Js/_user/components/voucher-detail/DetailVoucherCodeMobile';

describe('DetailVoucherCodeMobile.vue', () => {
	const propsData = {
		voucherCode: 'MAMIKOS2020',
		isAvailable: false,
		voucherId: 10001
	};

	const voucerValue = 'LUNAS3BULAN';
	const buttonIcons = ['ic_voucher_copy_green.svg', 'ic_voucher_copy_grey.svg'];
	const classText = ['text-active', 'text-disable'];

	const wrapper = shallowMount(DetailVoucherCodeMobileComponent, {
		propsData
	});

	const { vm } = wrapper;

	it('should render component properly', () => {
		expect(wrapper.find('.copy-code-card').exists()).toBe(true);
	});

	it('should render props properly', () => {
		wrapper.setProps({ voucherCode: voucerValue, isAvailable: true });
		expect(vm.voucherCode).toBe(voucerValue);
		expect(vm.isAvailable).toBe(true);
	});

	it('should render voucher code from props properly', () => {
		const wrapperVoucherCode = wrapper.find('.copy-code-text__code');
		expect(wrapperVoucherCode.text()).toBe(voucerValue);
	});

	it('should render buttonIcon properly', () => {
		expect(vm.buttonIcon).toBe(buttonIcons[0]);
		wrapper.setProps({ isAvailable: false });
		expect(vm.buttonIcon).toBe(buttonIcons[1]);
	});

	it('should render classText properly', () => {
		expect(vm.classText).toBe(classText[1]);
		wrapper.setProps({ isAvailable: true });
		expect(vm.classText).toBe(classText[0]);
	});
});
