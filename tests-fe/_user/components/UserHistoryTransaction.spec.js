import { createLocalVue, shallowMount } from '@vue/test-utils';
import dayjs from 'dayjs';
import 'dayjs/locale/id';

import mockAxios from 'tests-fe/utils/mock-axios';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import mockComponent from 'tests-fe/utils/mock-component';

import UserHistoryTransaction from 'Js/_user/components/UserHistoryTransaction.vue';

// mock imports
jest.mock('Js/_user/event-bus/event-bus', () => {
	const EventBus = {
		$emit: jest.fn(),
		$on: jest.fn((event, fn) => {
			fn({
				dateType: 'start',
				date: '2020-08-09 10:11:12'
			});
		})
	};
	global.EventBus = EventBus;
	return global.EventBus;
});

// mock components
jest.mock('Js/@components/EmptyState', () => {
	return mockComponent;
});
jest.mock('Js/@components/ButtonMobileFilter', () => {
	return mockComponent;
});
jest.mock('Js/@components/MamiLoadingInline', () => {
	return mockComponent;
});

// set vue
window.Vue = require('vue');
window.Vue.config.silent = true;

describe('UserHistoryTransaction.vue', () => {
	const localVue = createLocalVue();
	mockVLazy(localVue);

	localVue.prototype.$dayjs = dayjs;

	global.axios = mockAxios;

	// mock window
	const notify = jest.fn();
	mockWindowProperty(
		'addEventListener',
		jest.fn((eventName, fn) => {
			fn();
		})
	);
	mockWindowProperty('removeEventListener', jest.fn());
	mockWindowProperty('bugsnagClient', { notify });

	// mock stubbed component
	const stubs = {
		modal: mockComponent,
		paginate: mockComponent,
		HistoryListTransaction: mockComponent,
		HistoryModalMobileFilter: mockComponent,
		HistoryModalDesktopFilter: mockComponent,
		HistoryInputTextSearch: mockComponent,
		HistoryInputDateSearch: mockComponent,
		EmptyState: mockComponent,
		ButtonMobileFilter: mockComponent,
		MamiLoadingInline: mockComponent
	};

	const $route = {
		query: {
			contract: 100,
			page: 0
		}
	};

	const $router = {
		push: jest.fn().mockRejectedValue('error'),
		go: jest.fn()
	};

	// mount function
	const mount = () => {
		return shallowMount(UserHistoryTransaction, {
			localVue,
			stubs,
			mocks: {
				$router,
				$route
			},
			methods: {
				swalError: jest.fn()
			}
		});
	};

	describe('render component correctly', () => {
		let wrapper;

		beforeEach(() => {
			wrapper = mount();
		});

		it('should render container properly', () => {
			expect(wrapper.find('#userBookingList').exists()).toBeTruthy();
		});
	});

	describe('render computed value properly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = mount();
		});

		it('should return isLoading value properly', () => {
			expect(wrapper.vm.isLoading).toBe(wrapper.vm.isProgressFetchData);
		});
	});

	describe('call functions correctly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = mount();
		});

		it('should call window.addEventListener when calling initialMobileConfiguration method', async () => {
			const setFullHeightForMobileSpy = jest.spyOn(
				wrapper.vm,
				'setFullHeightForMobile'
			);
			await wrapper.vm.initialMobileConfiguration();

			expect(setFullHeightForMobileSpy).toBeCalled();
			expect(window.addEventListener).toBeCalled();
			setFullHeightForMobileSpy.mockRestore();
		});

		it('should set modal filter state when calling showModalFilter method', () => {
			wrapper.vm.showModalFilter(true);

			expect(wrapper.vm.isModalFilter).toBeTruthy();
		});

		it('should set isModalOpen state and contract id based on $route.query.contract data availability when calling showHistoryTransactionBooking method', () => {
			wrapper.vm.showHistoryTransactionBooking(true);

			expect(wrapper.vm.isModalOpen).toBeTruthy();
			expect(wrapper.vm.contractId).toBe(100);
		});

		it('should set pagination page count based on provided data when callong calculatePagination method', () => {
			wrapper.setData({
				pagination: {
					limit: 5
				}
			});
			wrapper.vm.calculatePagination(10);
			expect(wrapper.vm.pagination.pageCount).toBe(2);
		});

		it('should return separated data when calling separateData method', () => {
			const separatedData = wrapper.vm.separateData({
				paid_date: '2020-08-10 09:10:11',
				invoice_name: 'name',
				paid_amount: 10,
				kost_name: 'test',
				paid_status: 'paid',
				invoice_id: 'invoiceId'
			});

			expect(separatedData).toEqual({
				paidDate: '2020-08-10 09:10:11',
				type: 'name',
				price: 10,
				kostName: 'test',
				status: 'paid',
				invoiceId: 'invoiceId'
			});
		});

		it('should set pagination data when calling onPageChanged method', () => {
			wrapper.setData({
				pagination: {
					limit: 5
				}
			});

			wrapper.vm.onPageChanged(4);
			expect(wrapper.vm.pagination.offset).toBe(15);
			expect(wrapper.vm.$router.push).toBeCalled();
		});

		it('should set pagination and search data when calling onSearchTextChange method', () => {
			wrapper.vm.onSearchTextChange('kost');
			expect(wrapper.vm.search.kostName).toBe('kost');
			expect(wrapper.vm.pagination.offset).toBe(0);

			expect(wrapper.vm.$router.push).toBeCalled();
		});

		it('should set search date when calling onDateChange method when received start as the dateType from eventBus', async () => {
			await wrapper.vm.onDateChange();
			expect(wrapper.vm.search.startDate).toBe('2020-08-09');
		});

		it('should set search date when calling onDateChange method when received "end" as the dateType from eventBus', () => {
			global.EventBus.$on = jest.fn((event, fn) => {
				fn({
					dateType: 'end',
					date: '2020-08-09 10:11:12'
				});
			});

			wrapper.vm.onDateChange();
			expect(wrapper.vm.search.endDate).toBe('2020-08-09');
		});

		it('should set transaction data and calculate pagination when calling fetchDataTransaction method and the axios request was successful', async () => {
			//  have transaction data
			await axios.mockResolve({
				data: {
					status: true,
					data: {
						total: 10,
						transactions: [{ test: 'test' }]
					}
				}
			});
			const calculatePaginationSpy = jest.spyOn(
				wrapper.vm,
				'calculatePagination'
			);
			await wrapper.vm.fetchDataTransaction();
			expect(wrapper.vm.transactionData).toEqual([{ test: 'test' }]);
			expect(calculatePaginationSpy).toBeCalled();
			calculatePaginationSpy.mockRestore();

			// do not have transaction data
			await axios.mockResolve({
				data: {
					status: true,
					data: {
						total: 0
					}
				}
			});
			await wrapper.vm.fetchDataTransaction();
			expect(wrapper.vm.transactionData).toEqual([]);
		});

		it('should call swalError when calling fetchDataTransaction method and the axios request was not successful', async () => {
			const swalErrorSpy = jest.spyOn(wrapper.vm, 'swalError');
			await axios.mockResolve({
				data: {
					status: false,
					meta: {
						message: 'test'
					}
				}
			});
			await wrapper.vm.fetchDataTransaction();
			expect(swalErrorSpy).toBeCalled();
			swalErrorSpy.mockRestore();
		});
	});

	describe('call functions before destroy', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = mount();
		});

		it('should call window.removeEventListener before destroy', () => {
			wrapper.destroy();

			expect(window.removeEventListener).toBeCalled();
		});
	});
});
