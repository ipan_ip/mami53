import { createLocalVue, shallowMount } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import UserProfileSettings from 'Js/_user/components/UserProfileSettings.vue';
import mockComponent from 'tests-fe/utils/mock-component';
import { alert } from 'vue-strap';

window.Vue = require('vue');
window.Vue.config.silent = true;

const mixinNavigator = require('Js/@mixins/MixinNavigatorIsMobile');
const localVue = createLocalVue();

localVue.mixin(mixinNavigator);

describe('UserProfileSettings.vue', () => {
	let wrapper;

	beforeEach(() => {
		wrapper = shallowMount(UserProfileSettings, {
			localVue,
			stubs: {
				UserSettingsPasswordChange: mockComponent,
				alert
			}
		});
	});

	it('Should exists', () => {
		expect(wrapper.find('.user-profile-settings').exists()).toBe(true);
	});

	it('Should toggle setting content element properly', () => {
		expect(wrapper.vm.settingContentActive).toBe(false);
		wrapper.vm.toggleSettingContent();
		expect(wrapper.vm.settingContentActive).toBe(true);
	});

	it('Should handle finish setting action properly', () => {
		const mockMessage = 'mock message';

		wrapper.vm.finishSetting(mockMessage);
		expect(wrapper.vm.settingContentActive).toBe(false);
		expect(wrapper.vm.showAlert).toBe(true);
		expect(wrapper.vm.alertMessage).toBe(mockMessage);
	});

	it('Should handle close alert action properly', () => {
		wrapper.vm.closeAlert();
		expect(wrapper.vm.showAlert).toBe(false);
		expect(wrapper.vm.alertMessage).toBe(null);
	});
});
