import { shallowMount, createLocalVue } from '@vue/test-utils';
import UserReviewCard from 'Js/_user/components/UserReviewCard.vue';
import reviewData from './__mocks__/review.json';

const localVue = createLocalVue();

const UserKostReviewAdd = { template: '<div class="review-modal" />' };

describe('UserReviewCard.vue', () => {
	const wrapper = shallowMount(UserReviewCard, {
		localVue,
		propsData: {
			roomId: 1,
			rating: '',
			roomData: {}
		},
		stubs: { UserKostReviewAdd }
	});

	it('should render user review card', () => {
		expect(wrapper.find('.user-review-card').exists()).toBe(true);
	});

	it('should render empty card if rating is empty', () => {
		expect(wrapper.find('.user-review-card--empty').exists()).toBe(true);
	});

	it('should show modal review if review card clicked', async () => {
		await wrapper.setData({ isAddReview: false });
		const reviewCard = wrapper.find('.user-review-card--empty');
		await reviewCard.trigger('click');
		expect(wrapper.vm.isAddReview).toBe(true);
	});

	it('should set isAddReview to false if modal review closed', async () => {
		await wrapper.setData({ isAddReview: true });
		const reviewCard = wrapper.findComponent(UserKostReviewAdd);
		reviewCard.vm.$emit('close');
		expect(wrapper.vm.isAddReview).toBe(false);
	});

	it('should render reviewed card if rating is already given', async () => {
		await wrapper.setProps({ rating: '4.5', reviewData });
		expect(wrapper.find('.user-review-card--reviewed').exists()).toBe(true);
	});
});
