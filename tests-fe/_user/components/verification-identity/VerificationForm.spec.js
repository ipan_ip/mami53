import { createLocalVue, shallowMount } from '@vue/test-utils';
import VerificationFormComponent from 'Js/_user/components/verification-identity/VerificationForm';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mockSwalLoading from 'tests-fe/utils/mock-swal-loading';
import mockAxios from 'tests-fe/utils/mock-axios';
import VerificationFormStore from './__mocks__/VerificationForm';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import Vuex from 'vuex';

jest.mock('Js/@mixins/MixinNavigatorIsMobile', () => '');

const notify = jest.fn();
mockWindowProperty('bugsnagClient', {
	notify
});

describe('VerificationForm.vue', () => {
	const localVue = createLocalVue();
	global.axios = mockAxios;
	window.Vue = require('vue');
	mockVLazy(localVue);
	mockSwalLoading();
	localVue.use(Vuex);

	const swalError = jest.fn();

	const store = new Vuex.Store(VerificationFormStore);

	const wrapper = shallowMount(VerificationFormComponent, {
		localVue,
		store,
		mocks: {
			$store: { commit: jest.fn() }
		}
	});

	it('should render currentId get properly', () => {
		wrapper.vm.$store.commit('setCurrentId', 'sim');
		expect(wrapper.vm.currentId).toBe('sim');
	});

	it('should render currentId set properly', () => {
		const wrapper = shallowMount(VerificationFormComponent, {
			localVue,
			store,
			mocks: {
				$store: { commit: jest.fn() }
			}
		});
		wrapper.vm.currentId = 'testing';
	});

	it('should render idImg properly', () => {
		wrapper.vm.$store.commit('setIdImg', { src: 'img/idImg2.jpg' });
		expect(wrapper.vm.idImg.src).toBe('img/idImg2.jpg');
	});

	it('should render selfieImg properly', () => {
		wrapper.vm.$store.commit('setSelfieImg', { src: 'img/selfieImg2.jpg' });
		expect(wrapper.vm.selfieImg.src).toBe('img/selfieImg2.jpg');
	});

	it('should render verification-form properly', () => {
		const wrapper = shallowMount(VerificationFormComponent, {
			localVue,
			store,
			mocks: {
				$store: { commit: jest.fn() }
			}
		});

		wrapper.vm.checkVerifyIdentity();

		wrapper.vm.$nextTick(() => {
			expect(
				wrapper.find('.verification-form > #formIdVerification').exists()
			).toBe(true);
		});
	});

	it('should render succes verification-form properly', () => {
		const wrapper = shallowMount(VerificationFormComponent, {
			localVue,
			store,
			mocks: {
				$store: { commit: jest.fn() }
			}
		});

		wrapper.vm.$store.commit('setProfileUser', {
			is_verify_identity_card: 'verified'
		});

		expect(wrapper.vm.verifyIdentityStatus).toBe('verified');
		wrapper.vm.checkVerifyIdentity();

		wrapper.vm.$nextTick(() => {
			expect(
				wrapper.find('.verification-form > #formIdVerification').exists()
			).toBe(false);
			expect(wrapper.find('.verification-desc > span.--desc-bold').text()).toBe(
				'Verifikasi berhasil di upload'
			);
		});
	});

	it('should run methods setCardId properly', () => {
		wrapper.vm.setCardId('passport');
		expect(wrapper.vm.cardIdVal).toBe('passport');
	});

	it('should run changePhoto properly', () => {
		const wrapper = shallowMount(VerificationFormComponent, {
			localVue,
			store,
			mocks: {
				$store: { commit: jest.fn() }
			}
		});
		const { vm } = wrapper;
		wrapper.setData({ isModalChange: true });
		vm.navigator = { isMobile: false };
		vm.$nextTick(() => {
			expect(wrapper.find('#idVerifModalShowPhoto').exists()).toBe(true);
			const buttonChangePhoto = wrapper
				.findAll('#idVerifModalShowPhoto .button-actions .btn-primary')
				.at(1);
			buttonChangePhoto.trigger('click');
			vm.$nextTick(() => {
				expect(vm.isModalChange).toBe(false);
				expect(vm.isCamera).toBe(true);
			});
		});
	});

	it('should run showPhoto properly', () => {
		const wrapper = shallowMount(VerificationFormComponent, {
			localVue,
			store,
			mocks: {
				$store: { commit: jest.fn() }
			}
		});
		const { vm } = wrapper;
		wrapper.setData({ isModalChange: true });
		vm.$nextTick(() => {
			expect(wrapper.find('#idVerifModalShowPhoto').exists()).toBe(true);
			const buttonShow = wrapper
				.findAll('#idVerifModalShowPhoto .button-actions .btn-primary')
				.at(0);
			buttonShow.trigger('click');
			vm.$nextTick(() => {
				expect(vm.isModalShow).toBe(true);
			});
		});
	});

	it('should run nextStep properly', () => {
		const wrapper = shallowMount(VerificationFormComponent, {
			localVue,
			store,
			mocks: {
				$store: { commit: jest.fn() }
			}
		});
		const { vm } = wrapper;
		wrapper.setData({ isModalWebcam: true });
		vm.$nextTick(() => {
			expect(wrapper.find('#idVerifModalWarning').exists()).toBe(true);
			const buttonNext = wrapper.find(
				'#idVerifModalWarning .booking-webcam-button button'
			);
			buttonNext.trigger('click');
			vm.$nextTick(() => {
				expect(vm.isCamera).toBe(true);
			});
		});
	});

	it('should run openModalDeletePhoto properly', () => {
		const wrapper = shallowMount(VerificationFormComponent, {
			localVue,
			store,
			mocks: {
				$store: { commit: jest.fn() }
			}
		});
		const { vm } = wrapper;
		vm.navigator = { isMobile: false };
		wrapper.setData({ isModalChange: true });
		vm.$nextTick(() => {
			expect(wrapper.find('#idVerifModalShowPhoto').exists()).toBe(true);
			const buttonOpenDeletePhoto = wrapper.find(
				'#idVerifModalShowPhoto .button-actions .btn-info'
			);
			buttonOpenDeletePhoto.trigger('click');
			vm.$nextTick(() => {
				expect(vm.changeId).toBe(false);
				expect(vm.isModalCancelChange).toBe(true);
			});
		});
	});

	it('should run closeCamera properly', () => {
		const wrapper = shallowMount(VerificationFormComponent, {
			localVue,
			store,
			mocks: {
				$store: { commit: jest.fn() }
			}
		});
		const { vm } = wrapper;
		wrapper.setData({ isCamera: true });
		wrapper.vm.closeCamera();
		vm.$nextTick(() => {
			expect(vm.isCamera).toBe(false);
		});
	});

	it('should run closeModalShow properly', () => {
		const wrapper = shallowMount(VerificationFormComponent, {
			localVue,
			store,
			mocks: {
				$store: { commit: jest.fn() }
			}
		});
		const { vm } = wrapper;
		wrapper.setData({ isModalShow: true });
		vm.$nextTick(() => {
			expect(wrapper.find('#idVerifModalShowPhoto').exists()).toBe(true);
			const buttonCloseModalShow = wrapper.find(
				'#idVerifModalShowPhoto .photo-esc a'
			);
			buttonCloseModalShow.trigger('click');
			vm.$nextTick(() => {
				expect(vm.isModalShow).toBe(false);
			});
		});
	});

	it('should run closeModalChange properly', () => {
		const wrapper = shallowMount(VerificationFormComponent, {
			localVue,
			store,
			mocks: {
				$store: { commit: jest.fn() }
			}
		});
		const { vm } = wrapper;
		wrapper.setData({ isModalChange: true });
		vm.navigator = { isMobile: false };
		vm.$nextTick(() => {
			expect(wrapper.find('#idVerifModalShowPhoto').exists()).toBe(true);
			const buttonCloseModalChange = wrapper.find(
				'#idVerifModalShowPhoto .button-cancel button'
			);
			buttonCloseModalChange.trigger('click');
			vm.$nextTick(() => {
				expect(vm.isModalChange).toBe(false);
			});
		});
	});

	it('should run closeModalCancel properly', () => {
		const wrapper = shallowMount(VerificationFormComponent, {
			localVue,
			store,
			mocks: {
				$store: { commit: jest.fn() }
			}
		});
		const { vm } = wrapper;
		wrapper.setData({ isModalCancelChange: true });
		vm.$nextTick(() => {
			expect(wrapper.find('#idVerifModalCancelAction').exists()).toBe(true);

			const buttonCloseModalCancel = wrapper.find(
				'#idVerifModalCancelAction .confirmation-button-group .btn-default'
			);
			buttonCloseModalCancel.trigger('click');
			vm.$nextTick(() => {
				expect(vm.isModalCancelChange).toBe(false);
			});
		});
	});

	it('should run openCamera for photo Card properly', () => {
		const wrapper = shallowMount(VerificationFormComponent, {
			localVue,
			store,
			mocks: {
				$store: { commit: jest.fn() }
			}
		});
		const { vm } = wrapper;
		vm.verificationSent = false;
		vm.$store.commit('setSelfieImg', null);
		vm.$store.commit('setIdImg', null);

		vm.$nextTick(() => {
			const button = wrapper
				.findAll('#formIdVerification .upload-box-container .upload-box')
				.at(0);
			button.trigger('click');
			expect(vm.snapType).toBe('card');
		});
	});

	it('should run openCamera for photo Selfie properly', () => {
		const wrapper = shallowMount(VerificationFormComponent, {
			localVue,
			store,
			mocks: {
				$store: { commit: jest.fn() }
			}
		});
		const { vm } = wrapper;
		vm.verificationSent = false;
		vm.$store.commit('setSelfieImg', null);
		vm.$store.commit('setIdImg', null);

		vm.$nextTick(() => {
			const button = wrapper
				.findAll('#formIdVerification .upload-box-container .upload-box')
				.at(1);
			button.trigger('click');
			expect(vm.snapType).toBe('selfie');
		});
	});

	it('should run openCamera for edit photo Card properly', () => {
		const wrapper = shallowMount(VerificationFormComponent, {
			localVue,
			store,
			mocks: {
				$store: { commit: jest.fn() }
			}
		});
		const { vm } = wrapper;
		vm.verificationSent = false;
		vm.navigator = { isMobile: false };
		vm.$store.commit('setSelfieImg', null);
		vm.$store.commit('setIdImg', { src: 'img/idImg.jpg' });

		vm.$nextTick(() => {
			const button = wrapper.find('#formIdVerification .upload-box.--photo');
			button.trigger('click');
		});
	});

	it('should run openCamera for edit photo Selfie properly', () => {
		const wrapper = shallowMount(VerificationFormComponent, {
			localVue,
			store,
			mocks: {
				$store: { commit: jest.fn() }
			}
		});
		const { vm } = wrapper;
		vm.verificationSent = false;
		vm.navigator = { isMobile: false };
		vm.$store.commit('setSelfieImg', { src: 'img/idImg.jpg' });
		vm.$store.commit('setIdImg', null);

		vm.$nextTick(() => {
			const button = wrapper.find('#formIdVerification .upload-box.--photo');
			button.trigger('click');
		});
	});

	it('should run uploadImages properly', async done => {
		const wrapper = shallowMount(VerificationFormComponent, {
			localVue,
			store,
			mocks: {
				$store: { commit: jest.fn() }
			}
		});
		wrapper.setMethods({
			openSwalLoading: jest.fn(),
			closeSwalLoading: jest.fn(),
			swalError,
			swalSuccess: jest.fn((...args) => {
				args[2]();
			})
		});
		const { vm } = wrapper;
		wrapper.setData({ isCamera: true });

		// Success
		await vm.$nextTick();
		axios.mockResolve({ data: { status: true } });
		vm.$store.commit('setIdImg', {
			src: vm.$store.state.dataUri.src
		});
		vm.$store.commit('setSelfieImg', null);
		expect(vm.idImg).not.toBe(null);
		vm.uploadImage(vm.idImg, 'card');
		await mockAxios.post;
		expect(vm.closeSwalLoading).toBeCalled();
		done();
	});

	describe('deletePhoto Functions', () => {
		it('should run methods deletePhoto properly type card desktop', () => {
			wrapper.vm.navigator = { isMobile: false };

			wrapper.vm.$store.commit('setIdImg', { src: 'img/idImg2.jpg' });
			wrapper.vm.$store.commit('setProfileUser', {
				is_verify_identity_card: ''
			});
			wrapper.vm.$store.commit('setSelfieImg', null);

			wrapper.vm.checkVerifyIdentity();
			wrapper.vm.$nextTick(() => {
				const buttonDelete = wrapper.find('.delete-button');
				buttonDelete.trigger('click');
				expect(wrapper.vm.idImg).toBe(null);
			});
		});

		it('should run methods deletePhoto properly type card mobile', () => {
			wrapper.vm.navigator = { isMobile: true };

			wrapper.vm.$store.commit('setIdImg', { src: 'img/idImg2.jpg' });
			wrapper.vm.$store.commit('setProfileUser', {
				is_verify_identity_card: ''
			});
			wrapper.vm.$store.commit('setSelfieImg', null);

			wrapper.vm.checkVerifyIdentity();
			wrapper.vm.$nextTick(() => {
				const buttonDelete = wrapper.find('.delete-button');
				buttonDelete.trigger('click');
				expect(wrapper.vm.idImg).toBe(null);
			});
		});

		it('should run methods deletePhoto properly type selfie desktop', () => {
			wrapper.vm.navigator = { isMobile: false };
			wrapper.setData({ snapType: 'selfie' });

			wrapper.vm.$store.commit('setIdImg', null);
			wrapper.vm.$store.commit('setProfileUser', {
				is_verify_identity_card: ''
			});
			wrapper.vm.$store.commit('setSelfieImg', { src: 'img/selfieImg.jpg' });

			wrapper.vm.checkVerifyIdentity();
			wrapper.vm.$nextTick(() => {
				const buttonDelete = wrapper.find('.delete-button');
				buttonDelete.trigger('click');
				expect(wrapper.vm.selfieImg).toBe(null);
			});
		});

		it('should run methods deletePhoto properly type selfie mobile', () => {
			wrapper.vm.navigator = { isMobile: true };
			wrapper.setData({ snapType: 'selfie' });

			wrapper.vm.$store.commit('setIdImg', null);
			wrapper.vm.$store.commit('setProfileUser', {
				is_verify_identity_card: ''
			});
			wrapper.vm.$store.commit('setSelfieImg', { src: 'img/selfieImg.jpg' });

			wrapper.vm.checkVerifyIdentity();
			wrapper.vm.$nextTick(() => {
				const buttonDelete = wrapper.find('.delete-button');
				buttonDelete.trigger('click');
				expect(wrapper.vm.selfieImg).toBe(null);
			});
		});

		it('should run methods deleteAllPhoto properly', () => {
			wrapper.vm.$store.commit('setCurrentId', null);

			const buttonDeleteAllPhoto = wrapper.find(
				'#idVerifModalCancelAction button.btn-primary'
			);
			buttonDeleteAllPhoto.trigger('click');

			wrapper.vm.$nextTick(() => {
				expect(wrapper.vm.isModalCancelChange).toBe(false);
				expect(wrapper.vm.cardId).toBe(null);
				expect(wrapper.vm.selfieId).toBe(null);
				expect(wrapper.vm.idImg).toBe(null);
				expect(wrapper.vm.selfieImg).toBe(null);
			});
		});
	});

	describe('Watch Functions', () => {
		let wrapper;

		beforeEach(() => {
			wrapper = shallowMount(VerificationFormComponent, {
				localVue,
				store,
				mocks: {
					$store: { commit: jest.fn() }
				}
			});
		});

		it('watch cardIdVal properly', () => {
			const { vm } = wrapper;
			vm.$store.commit('setCardId', 'e_ktp');
			expect(vm.currentId).not.toBe(vm.cardIdVal);
			wrapper.setData({
				storeCurentId: vm.cardIdVal,
				changeId: true,
				isModalCancelChange: true
			});
			vm.$nextTick(() => {
				expect(vm.storeCurentId).toBe(vm.cardIdVal);
				expect(wrapper.find('#idVerifModalCancelAction').exists()).toBe(true);
			});
		});
		it('watch cardIdVal properly if selfieImg & idImg null', () => {
			const { vm } = wrapper;
			vm.$store.commit('setIdImg', null);
			vm.$store.commit('setSelfieImg', null);
			vm.$store.commit('setCardId', 'sim');
			vm.$nextTick(() => {
				expect(vm.currentId).toBe('sim');
			});
		});
		it('watch isCamera for Selfie properly', () => {
			const { vm } = wrapper;
			wrapper.setData({ isCamera: true });

			vm.$nextTick(() => {
				wrapper.setData({ isCamera: false });
				vm.$nextTick(() => {
					vm.$store.commit('setIdImg', null);
					vm.$store.commit('setSelfieImg', {
						src: vm.$store.state.dataUri.src
					});
					expect(vm.selfieImg).not.toBe(null);
					vm.uploadImage(vm.selfieImg, 'selfie');
				});
			});
		});
		it('watch isCamera for Card properly', () => {
			const { vm } = wrapper;
			wrapper.setData({ isCamera: true });

			vm.$nextTick(() => {
				wrapper.setData({ isCamera: false });
				vm.$nextTick(() => {
					vm.$store.commit('setIdImg', {
						src: vm.$store.state.dataUri.src
					});
					vm.$store.commit('setSelfieImg', null);
					expect(vm.idImg).not.toBe(null);
					vm.uploadImage(vm.idImg, 'card');
				});
			});
		});
		it('watch isCamera not properly', () => {
			const { vm } = wrapper;
			wrapper.setData({ isCamera: true });

			vm.$nextTick(() => {
				wrapper.setData({ isCamera: false });
				expect(vm.isCamera).toBe(false);
			});
		});
	});

	describe('should run saveIdVerif properly', () => {
		let wrapper2, callback, result;
		let results = [{ data: { status: true } }, { data: { status: true } }];
		const mockAxiosSpreadResult = jest.fn();

		mockAxios.all = jest.fn().mockResolvedValue(results);
		mockAxios.spread = jest.fn().mockReturnValue(mockAxiosSpreadResult);

		beforeEach(() => {
			wrapper2 = shallowMount(VerificationFormComponent, {
				localVue,
				store,
				mocks: {
					$store: { commit: jest.fn() }
				}
			});

			wrapper2.setMethods({
				openSwalLoading: jest.fn(),
				closeSwalLoading: jest.fn(),
				swalError
			});

			wrapper2.setData({
				verificationSent: false,
				agreement: true,
				cardId: 123,
				selfieId: 312
			});
		});

		it('should save id verification images', async done => {
			await wrapper2.vm.saveIdVerif();

			expect(mockAxios.spread).toHaveBeenCalledWith(expect.any(Function));
			expect(mockAxiosSpreadResult).toHaveBeenCalledWith(results);

			callback = mockAxios.spread.mock.calls[0][0];
			result = callback({ data: { status: true } }, { data: { status: true } });
			done();
		});

		it('should throw error card id photo cannot be saved', async done => {
			await wrapper2.vm.saveIdVerif();

			callback = mockAxios.spread.mock.calls[0][0];
			result = callback(
				{ data: { status: false, meta: { message: 'upload id card error' } } },
				{ data: { status: true } }
			);

			wrapper2.vm.$nextTick(() => {
				expect(swalError).toBeCalledWith(null, 'upload id card error');
				done();
			});
		});

		it('should throw error card id photo cannot be saved 2', async done => {
			await wrapper2.vm.saveIdVerif();

			callback = mockAxios.spread.mock.calls[0][0];
			result = callback(
				{ data: { status: true } },
				{ data: { status: false, meta: { message: 'upload id card error 2' } } }
			);

			wrapper2.vm.$nextTick(() => {
				expect(swalError).toBeCalledWith(null, 'upload id card error 2');
				done();
			});
		});

		it('should throw error selfie and id photo cannot be saved', async done => {
			await wrapper2.vm.saveIdVerif();

			callback = axios.spread.mock.calls[0][0];
			result = callback(
				{ data: { status: false } },
				{ data: { status: false } }
			);

			wrapper2.vm.$nextTick(() => {
				expect(swalError).toBeCalledWith(
					null,
					'Maaf foto Anda gagal diverifikasi. Silahkan ulangi kembali atau cek konekasi internet Anda.'
				);
				done();
			});
		});

		it('should throw error and call send error to bugsnag', async () => {
			mockAxios.all = jest.fn().mockRejectedValue();

			await wrapper2.vm.saveIdVerif();

			expect(swalError).toBeCalled();
		});
	});
});
