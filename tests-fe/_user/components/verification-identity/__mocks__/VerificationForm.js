export default {
	state: {
		extras: {
			selfieImg: {
				src: 'img/selfieImg.jpg'
			},
			idImg: {
				src: 'img/idImg.jpg'
			},
			cardIdVal: 'e_ktp',
			currentId: 'e_ktp'
		},
		userProfile: {
			is_verify_identity_card: ''
		},
		dataUri: {
			src:
				'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAT4AAAGoCAYAAADfB4luAAAgAElEQVR4XlS9W5MlWXactyPOLat6IJrJ'
		}
	},
	mutations: {
		setCardId(state, payload) {
			state.extras.cardIdVal = payload;
		},
		setIdImg(state, payload) {
			state.extras.idImg = payload;
		},
		setSelfieImg(state, payload) {
			state.extras.selfieImg = payload;
		},
		setCurrentId(state, payload) {
			state.extras.currentId = payload;
		},
		setProfileUser(state, payload) {
			state.userProfile = payload;
		}
	}
};
