import { createLocalVue, shallowMount } from '@vue/test-utils';
import VerificationCamera from 'Js/_user/components/verification-identity/VerificationCamera';
import Vuex from 'vuex';
jest.mock('Js/@components/IdVerifCamera', () => {
	return '';
});

describe('VerificationCamera.vue', () => {
	let wrapper;
	const localVue = createLocalVue();
	localVue.use(Vuex);
	const props = {
		snapType: 'snap'
	};
	const store = {
		state: {
			extras: {
				idImg: null,
				selfieImg: null
			}
		},
		mutations: {
			setIdImg(state, payload) {
				state.extras.idImg = payload;
			},
			setSelfieImg(state, payload) {
				state.extras.selfieImg = payload;
			}
		}
	};

	beforeEach(() => {
		wrapper = shallowMount(VerificationCamera, {
			localVue,
			propsData: props,
			stubs: ['id-verif-camera'],
			store: new Vuex.Store(store)
		});
	});

	it('should render VerificationCamer properly', () => {
		expect(wrapper.find('#verificationCamera').exists()).toBe(true);
		expect(wrapper.find('.camera-container').exists()).toBe(true);
	});

	it('should emmited cancelCapture and saveCanvas properly', () => {
		wrapper.vm.cancelCapture();
		wrapper.vm.saveCanvas();
		expect(wrapper.emitted('close')).toBeTruthy();
	});

	it('should call save selfie and save card properly', () => {
		wrapper.vm.saveSelfie();
		wrapper.vm.saveCard();
	});
});
