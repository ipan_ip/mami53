import { createLocalVue, shallowMount } from '@vue/test-utils';

import ProfileKostList from 'Js/user/components/user-profile/ProfileKostList.vue';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

describe('ProfileKostList.vue', () => {
	const localVue = createLocalVue();

	mockWindowProperty('open', jest.fn());
	mockWindowProperty(
		'matchMedia',
		jest.fn().mockReturnValue({ matches: true })
	);
	mockWindowProperty('location', {
		href: ''
	});

	const propsData = {
		room: {
			is_promoted: true,
			rating: 3,
			review_count: 3,
			min_month: 2,
			is_premium_owner: false,
			gender: 0,
			price_title_time: 'test',
			has_round_photo: false,
			has_video: true,
			available_room: 2,
			photo_url: {
				medium: 'link/to/medium/sized/image'
			}
		}
	};

	const mount = () => {
		return shallowMount(ProfileKostList, {
			localVue,
			propsData
		});
	};

	describe('render component correctly', () => {
		let wrapper;

		beforeEach(() => {
			wrapper = mount();
		});

		it('should render container', () => {
			expect(wrapper.find('.kost-list-container').exists()).toBeTruthy();
		});
	});

	describe('call functions correctly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = mount();
		});

		it('should set window.location.href when calling openRoom method and received truthy window.matchMedia or truthy global navigator values', async () => {
			// window.matchMedia is a function and navigator.standalone returns true
			mockWindowProperty(
				'matchMedia',
				jest.fn().mockReturnValue({ matches: true })
			);
			global.navigator = {
				standalone: true
			};
			await wrapper.vm.openRoom('test');
			expect(global.window.location.href).toBe('test');

			// window.matchMedia is an object and navigator.standalone returns true
			mockWindowProperty('matchMedia', {});
			global.navigator = {
				standalone: true
			};
			await wrapper.vm.openRoom('test');
			expect(global.window.location.href).toBe('test');

			// window.matchMedia is a function and window.standalone returns false
			mockWindowProperty(
				'matchMedia',
				jest.fn().mockReturnValue({ matches: true })
			);
			global.navigator = {
				standalone: false
			};
			await wrapper.vm.openRoom('test');
			expect(global.window.location.href).toBe('test');
		});

		it('should call window.open when calling openRoom method and received falsy window.matchMedia and falsy navigator', async () => {
			// window.matchMedia is a function and window.standalone returns false
			mockWindowProperty(
				'matchMedia',
				jest.fn().mockReturnValue({ matches: false })
			);
			global.navigator = {
				standalone: false
			};
			await wrapper.vm.openRoom('test');
			expect(window.open).toBeCalled();

			// window.matchMedia is not a function and window.standalone is false
			mockWindowProperty('matchMedia', 'test');
			global.navigator = {
				standalone: false
			};
			await wrapper.vm.openRoom('test');
			expect(window.open).toBeCalled();
		});
	});
});
