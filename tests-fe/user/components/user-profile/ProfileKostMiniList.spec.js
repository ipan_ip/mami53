import { createLocalVue, shallowMount } from '@vue/test-utils';

import ProfileKostMiniList from 'Js/user/components/user-profile/ProfileKostMiniList.vue';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

// mock imports
jest.mock('Js/@mixins/MixinGetQueryString', () => {
	return {};
});

describe('ProfileKostMiniList.vue', () => {
	const localVue = createLocalVue();

	mockWindowProperty('open', jest.fn());

	const propsData = {
		rooms: [
			{
				share_url: 'link/to/share/url',
				photo_url: {
					medium: 'link/to/medium/sized/image'
				},
				available_room: 2,
				price_title_time: '200000',
				'room-title': 'room title',
				gender: 0
			}
		],
		reference: 'edit'
	};

	const mount = () => {
		return shallowMount(ProfileKostMiniList, {
			localVue,
			propsData
		});
	};

	describe('render component correctly', () => {
		let wrapper;

		beforeEach(() => {
			wrapper = mount();
		});

		it('should render container', () => {
			expect(wrapper.find('.mini-list-container').exists()).toBeTruthy();
		});
	});

	describe('call functions correctly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = mount();
		});

		it('should call window open function when clicking on the room card', async () => {
			// reference: edit
			const firstCard = wrapper.findAll('.material-card-1').at(0);
			if (firstCard && firstCard.exists()) {
				firstCard.trigger('click');
				expect(global.window.open).toBeCalled();

				const calls = global.window.open.mock.calls || [[]];
				expect(calls[0][0]).toContain('ref=edit');
			}

			// reference: none
			wrapper.setProps({
				reference: ''
			});
			await wrapper.vm.$nextTick();
			if (firstCard && firstCard.exists()) {
				firstCard.trigger('click');
				expect(global.window.open).toBeCalled();

				const calls = global.window.open.mock.calls || [[]];
				expect(calls[1][0]).not.toContain('ref=edit');
			}
		});
	});
});
