import { createLocalVue, shallowMount } from '@vue/test-utils';

import ProfileUpload from 'Js/user/components/user-profile/ProfileUpload.vue';

import vue2Dropzone from 'vue2-dropzone';

describe('ProfileUpload.vue', () => {
	const localVue = createLocalVue();

	// mock stubs
	const stubs = {
		Dropzone: vue2Dropzone
	};

	const propsData = {
		photos: {
			medium: ''
		}
	};

	const mount = () => {
		return shallowMount(ProfileUpload, {
			localVue,
			stubs,
			propsData,
			mocks: {
				$emit: jest.fn()
			}
		});
	};

	describe('render component correctly', () => {
		let wrapper;

		beforeEach(() => {
			wrapper = mount();
		});

		it('should render container', () => {
			expect(wrapper.find('.dropzone-custom').exists()).toBeTruthy();
			expect(wrapper.vm.isPhotoExists).toBeTruthy();
		});
	});

	describe('call functions correctly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = mount();
		});

		it('should set photo_id when calling showSuccess method', async () => {
			await wrapper.vm.showSuccess({}, { media: { id: 1 } });
			expect(wrapper.vm.photo_id).toBe(1);

			await wrapper.vm.showSuccess({}, { media: { id: 0 } });
			expect(wrapper.vm.photo_id).toBe(1);
		});

		it('should emit event on photo_id change', async () => {
			const emitSpy = jest.spyOn(wrapper.vm, '$emit');
			wrapper.setData({
				photo_id: 1
			});
			await wrapper.vm.$nextTick();
			wrapper.setData({
				photo_id: 2
			});

			expect(emitSpy).toBeCalled();
			emitSpy.mockRestore();
		});

		it('should toggle isPhotoExists value when calling resetPhotos method', async () => {
			wrapper.setData({
				isPhotoExists: true
			});
			await wrapper.vm.resetPhotos();

			expect(wrapper.vm.isPhotoExists).toBeFalsy();
		});
	});
});
