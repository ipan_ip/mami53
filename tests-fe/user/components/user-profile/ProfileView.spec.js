import { createLocalVue, shallowMount } from '@vue/test-utils';

import ProfileView from 'Js/user/components/user-profile/ProfileView.vue';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import mockComponent from 'tests-fe/utils/mock-component';
import mockAxios from 'tests-fe/utils/mock-axios';

import mixinGetQueryString from 'Js/@mixins/MixinGetQueryString';
import mixinScrollTop from 'Js/@mixins/MixinScrollTop';

// mock stubbed component
const stubs = {
	MamiLoading: {
		template: '<div id="mamiLoading" />',
		methods: {
			closeModal: jest.fn(),
			openModal: jest.fn()
		}
	},
	SectionTitle: mockComponent,
	RouterLink: true
};

describe('ProfileView.vue', () => {
	const localVue = createLocalVue();

	// mock directive
	localVue.directive('cloak', jest.fn());

	// mock window properties
	mockWindowProperty('matchMedia', jest.fn());
	mockWindowProperty('location', {
		href: 'http://www.mamikos.com?post_checkin=true&post_update=true&forum=true'
	});
	mockWindowProperty('open', jest.fn());
	mockWindowProperty('scrollTo', jest.fn());

	// mock global properties

	global.$ = jest.fn().mockReturnValue({
		modal: jest.fn()
	});
	global.navigator = {
		standalone: true
	};
	global.bugsnagClient = {
		notify: jest.fn()
	};
	global.axios = mockAxios;

	const mount = () => {
		return shallowMount(ProfileView, {
			localVue,
			stubs,
			mixins: [mixinGetQueryString, mixinScrollTop],
			methods: {
				closeSwalLoading: jest.fn()
			}
		});
	};

	describe('render component correctly', () => {
		let wrapper;

		beforeEach(() => {
			wrapper = mount();
		});

		it('should render container', () => {
			expect(wrapper.find('.profile-header').exists()).toBeTruthy();
		});
	});

	describe('render computed value correctly', () => {
		let wrapper;

		beforeEach(() => {
			wrapper = mount();
		});

		it('should render visitedForum value from window.location.href value', () => {
			expect(wrapper.vm.visitedForum).toBeTruthy();
		});
	});

	describe('call functions correctly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = mount();
		});

		it('should set data when calling getUser method and received successful response', async () => {
			const openModalSpy = jest.spyOn(
				wrapper.vm.$refs.mamiLoading,
				'openModal'
			);
			const closeModalSpy = jest.spyOn(
				wrapper.vm.$refs.mamiLoading,
				'closeModal'
			);

			await axios.mockResolve({
				data: {
					status: true,
					profile: { photo: 'test' },
					kos: { kos: 'test' },
					kos_time: '2020-08-09 10:11:12',
					data: { jobData: 'test' }
				}
			});
			await wrapper.vm.getUser();

			expect(wrapper.vm.userData).toEqual({ photo: 'test' });
			expect(wrapper.vm.kostData).toEqual({ kos: 'test' });
			expect(wrapper.vm.kostTime).toEqual('2020-08-09 10:11:12');
			expect(wrapper.vm.jobData).toEqual({ jobData: 'test' });

			expect(openModalSpy).toBeCalled();
			expect(closeModalSpy).toBeCalled();

			openModalSpy.mockRestore();
			closeModalSpy.mockRestore();
		});

		it('should call bugsnag when calling getUser method and received error response', async () => {
			await axios.mockReject('error');
			await wrapper.vm.getUser();
			expect(global.bugsnagClient.notify).toBeCalled();
		});

		it('should set data when calling getUserApplication method and received successful response', async () => {
			const closeSwalLoadingSpy = jest.spyOn(wrapper.vm, 'closeSwalLoading');
			await axios.mockResolve({
				data: {
					status: true,
					total: 1,
					data: [
						{
							name: 'test',
							status: 'onprocess'
						}
					]
				}
			});
			await wrapper.vm.getUserApplication();
			await wrapper.vm.$nextTick();

			expect(wrapper.vm.userApplicationData).toEqual([
				{
					name: 'test',
					status: 'onprocess'
				}
			]);
			expect(wrapper.vm.userApplicationCount).toBe(1);

			expect(closeSwalLoadingSpy).toBeCalled();
			closeSwalLoadingSpy.mockRestore();
		});

		it('should call bugsnag when calling getUserApplication method and received error response', async () => {
			await axios.mockReject('error');
			await wrapper.vm.getUser();
			expect(global.bugsnagClient.notify).toBeCalled();
		});

		it("should show modal when href contains 'post_checkin' and received 'closed' event from mamiLoading", async () => {
			const mamiLoading = wrapper.find('#mamiLoading');
			if (mamiLoading && mamiLoading.exists()) {
				mamiLoading.vm.$emit('closed');
				await wrapper.vm.$nextTick();
				expect(global.$).toBeCalled();
			}
		});

		it('should set window.location.href when calling openRoom method and received truthy window.matchMedia or truthy global navigator values', async () => {
			// window.matchMedia is function
			mockWindowProperty(
				'matchMedia',
				jest.fn().mockReturnValue({ matches: true })
			);
			global.navigator = {
				standalone: true
			};
			await wrapper.vm.openRoom('test');
			expect(global.window.location.href).toBe('test');

			// window.matchMedia is object
			mockWindowProperty('matchMedia', {});
			global.navigator = {
				standalone: true
			};
			await wrapper.vm.openRoom('test');
			expect(global.window.location.href).toBe('test');

			// window.matchMedia is function and window.standalone is false
			mockWindowProperty(
				'matchMedia',
				jest.fn().mockReturnValue({ matches: true })
			);
			global.navigator = {
				standalone: false
			};
			await wrapper.vm.openRoom('test');
			expect(global.window.location.href).toBe('test');
		});

		it('should call window.open when calling openRoom method and received falsy window.matchMedia and falsy navigator', async () => {
			mockWindowProperty(
				'matchMedia',
				jest.fn().mockReturnValue({ matches: false })
			);
			global.navigator = {
				standalone: false
			};

			await wrapper.vm.openRoom('test');
			expect(window.open).toBeCalled();
		});
	});
});
