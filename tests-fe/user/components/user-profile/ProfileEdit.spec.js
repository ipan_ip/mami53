import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';

import ProfileEdit from 'Js/user/components/user-profile/ProfileEdit.vue';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import mockComponent from 'tests-fe/utils/mock-component';
import mockAxios from 'tests-fe/utils/mock-axios';

import mixinGetQueryString from 'Js/@mixins/MixinGetQueryString';
import mixinAlertError from 'Js/@mixins/MixinAlertError';

// mock stubbed component
const stubs = {
	MamiLoading: {
		template: '<div id="mamiLoading" />',
		methods: {
			closeModal: jest.fn(),
			openModal: jest.fn()
		}
	},
	BirthdayPicker: {
		template: '<div />',
		data() {
			return {
				startTime: {
					time: ''
				}
			};
		}
	},
	UploadPhoto: mockComponent,
	SectionTitle: mockComponent,
	RouterLink: true
};

describe('ProfileEdit.vue', () => {
	const localVue = createLocalVue();
	localVue.use(Vuex);

	// mock window properties
	mockWindowProperty('location', {
		href: 'http://www.mamikos.com?ref=detail'
	});
	mockWindowProperty('alert', jest.fn());
	mockWindowProperty('open', jest.fn());

	mockWindowProperty('sessionStorage', {
		userData: JSON.stringify({
			name: 'name',
			email: 'email',
			city: 'city',
			education: 'education',
			birthday: 'birthday',
			gender: 'gender',
			photo_id: 'photo_id',
			phone: 'phone',
			jobs: 'kerja', // kerja, kuliah, lainnya
			position: 'position',
			work_place: 'work_place',
			semester: '2',
			description: 'description'
		}),
		setItem: jest.fn()
	});

	// mock global properties
	global.$ = jest.fn().mockReturnValue({
		modal: jest.fn()
	});
	global.bugsnagClient = {
		notify: jest.fn()
	};
	global.swal = jest.fn().mockResolvedValue(
		jest.fn(fn => {
			fn && fn();
		}),
		jest.fn(fn => {
			fn && fn();
		})
	);
	global.axios = mockAxios;

	// mock store
	const store = {
		state: {
			token: 'token',
			userData: {},
			birthdayData: {}
		}
	};

	const mount = () => {
		return shallowMount(ProfileEdit, {
			localVue,
			stubs,
			mixins: [mixinGetQueryString, mixinAlertError],
			mocks: {
				showError: jest.fn(),
				$router: {
					push: jest.fn()
				}
			},
			methods: {
				closeSwalLoading: jest.fn()
			},
			store: new Vuex.Store(store)
		});
	};

	describe('render component correctly', () => {
		let wrapper;

		beforeEach(() => {
			wrapper = mount();
		});

		it('should render container', () => {
			expect(wrapper.find('.container').exists()).toBeTruthy();
		});

		it("should set major, university, semester, when received jobs value as 'kuliah' ", () => {
			mockWindowProperty('sessionStorage', {
				userData: JSON.stringify({
					jobs: 'kuliah', // kerja, kuliah, lainnya
					position: 'position',
					work_place: 'work_place',
					semester: '2',
					description: 'description'
				}),
				setItem: jest.fn()
			});

			wrapper.destroy();
			wrapper = mount();

			expect(wrapper.vm.jobs).toBe('kuliah');
			expect(wrapper.vm.major).toBe('position');
			expect(wrapper.vm.university).toBe('work_place');
			expect(wrapper.vm.semester).toBe('2');
		});

		it("should set major, university, semester, when received jobs value as 'kuliah' ", () => {
			mockWindowProperty('sessionStorage', {
				userData: JSON.stringify({
					jobs: 'lainnya', // kerja, kuliah, lainnya
					description: 'description'
				}),
				setItem: jest.fn()
			});

			wrapper.destroy();
			wrapper = mount();

			expect(wrapper.vm.jobs).toBe('lainnya');
			expect(wrapper.vm.description).toBe('description');
		});

		it("should call getUser method on mounted, when ref is not 'detail'", () => {
			const getUserSpy = jest.spyOn(ProfileEdit.methods, 'getUser');
			mockWindowProperty('location', {
				href: 'http://www.mamikos.com?ref=edit'
			});

			wrapper.destroy();
			wrapper = mount();

			expect(getUserSpy).toBeCalled();
			getUserSpy.mockRestore();
		});
	});

	describe('render computed data correctly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = mount();
		});

		it('should return userData value from $store', () => {
			expect(wrapper.vm.userData).toBe(wrapper.vm.$store.state.userData);
		});

		it('should return birthdayData value from $store', () => {
			expect(wrapper.vm.birthdayData).toBe(
				wrapper.vm.$store.state.birthdayData
			);
		});
	});

	describe('call functions correctly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = mount();
		});

		it('should set data when calling getUser method on successful api request', async () => {
			// jobs: kerja, gender: female
			await axios.mockResolve({
				data: {
					status: true,
					data: {
						name: 'name',
						email: 'email',
						work: 'work',
						work_place: 'work_place',
						birthday: '09',
						phone: '081234567890',
						gender: 'Perempuan',
						jobs: 'kerja',
						last_education: 'last_education',
						city: 'city',
						description: 'description',
						photo_id: 'photo_id',
						photo_url: 'photo_url'
					}
				}
			});
			await wrapper.vm.getUser();
			await wrapper.vm.$nextTick();
			expect(wrapper.vm.name).toBe('name');
			expect(wrapper.vm.birthday).toBe('09');
			expect(wrapper.vm.gender).toBe('female');
			expect(wrapper.vm.jobs).toBe('kerja');
			expect(wrapper.vm.position).toBe('work');
			expect(wrapper.vm.work_place).toBe('work_place');
		});

		it('should call bugsnag and close modal when calling getUser method on successful api request', async () => {
			// jobs: kerja, gender: female
			const closeModalSpy = jest.spyOn(
				wrapper.vm.$refs.mamiLoading,
				'closeModal'
			);
			await axios.mockResolve({
				data: {
					status: false
				}
			});
			await wrapper.vm.getUser();
			expect(closeModalSpy).toBeCalled();
			closeModalSpy.mockRestore();
		});

		it('should set data when calling getUser method on successful api request', async () => {
			// jobs: kuliah, gender: male
			await axios.mockResolve({
				data: {
					status: true,
					data: {
						name: 'name',
						email: 'email',
						work: 'work',
						work_place: 'work_place',
						birthday: '09',
						phone: '081234567890',
						gender: 'test',
						jobs: 'kuliah',
						last_education: 'last_education',
						city: 'city',
						description: 'description',
						photo_id: 'photo_id',
						photo_url: 'photo_url'
					}
				}
			});
			await wrapper.vm.getUser();
			await wrapper.vm.$nextTick();
			expect(wrapper.vm.gender).toBe('male');
			expect(wrapper.vm.jobs).toBe('kuliah');
		});

		it('should set data when calling getUser method on successful api request', async () => {
			// jobs: lainnya, gender: male
			await axios.mockResolve({
				data: {
					status: true,
					data: {
						name: 'name',
						email: 'email',
						work: 'work',
						work_place: 'work_place',
						birthday: '09',
						phone: '081234567890',
						gender: 'test',
						jobs: 'lainnya',
						last_education: 'last_education',
						city: 'city',
						description: 'description',
						photo_id: 'photo_id',
						photo_url: 'photo_url'
					}
				}
			});
			await wrapper.vm.getUser();
			await wrapper.vm.$nextTick();
			expect(wrapper.vm.jobs).toBe('lainnya');
		});

		it('should call close modal when calling getUser method on successful api request, but received falsy status', async () => {
			// jobs: kerja, gender: female
			const closeModalSpy = jest.spyOn(
				wrapper.vm.$refs.mamiLoading,
				'closeModal'
			);
			await axios.mockResolve({
				data: {
					status: false
				}
			});
			await wrapper.vm.getUser();
			expect(closeModalSpy).toBeCalled();
			closeModalSpy.mockRestore();
		});

		it('should call close modal and bugsnag when calling getUser method on failed api request', async () => {
			// jobs: kerja, gender: female
			const closeModalSpy = jest.spyOn(
				wrapper.vm.$refs.mamiLoading,
				'closeModal'
			);
			await axios.mockReject('error');
			await wrapper.vm.getUser();
			await wrapper.vm.$nextTick();
			expect(global.bugsnagClient.notify).toBeCalled();
			expect(closeModalSpy).toBeCalled();
			closeModalSpy.mockRestore();
		});

		it('should set birthday when calling onDateUpdated method', () => {
			wrapper.vm.onDateUpdated('2020-08-10 11:12:12');
			expect(wrapper.vm.birthday).toBe('2020-08-10 11:12:12');
		});

		it('should set photo_id when calling onPhotoUploaded method', () => {
			wrapper.vm.onPhotoUploaded('1234567890');
			expect(wrapper.vm.photo_id).toBe('1234567890');
		});

		it('should call sessionStorage.setItem and window.open when calling goToSearch method', async () => {
			await wrapper.vm.goToSearch();

			expect(global.sessionStorage.setItem).toBeCalled();
			expect(window.open).toBeCalled();
		});

		it('should set data when calling submitUpdate method', async () => {
			await wrapper.setData({
				jobs: 'kerja',
				position: 'position',
				work_place: 'work_place'
			});
			await wrapper.vm.$nextTick();
			await wrapper.vm.submitUpdate();
			expect(wrapper.vm.position_container).toBe('position');
			expect(wrapper.vm.work_place_container).toBe('work_place');
			expect(wrapper.vm.semester_container).toBe('');
		});

		it('should set data when calling submitUpdate method', async () => {
			await wrapper.setData({
				jobs: 'kuliah',
				major: 'position',
				university: 'work_place',
				semester: '2'
			});
			await wrapper.vm.$nextTick();
			await wrapper.vm.submitUpdate();
			expect(wrapper.vm.position_container).toBe('position');
			expect(wrapper.vm.work_place_container).toBe('work_place');
			expect(wrapper.vm.semester_container).toBe('2');
		});

		it('should set data when calling submitUpdate method', async () => {
			await wrapper.setData({
				jobs: 'lainnya',
				description: 'description'
			});
			await wrapper.vm.$nextTick();
			await wrapper.vm.submitUpdate();
			expect(wrapper.vm.position_container).toBe('');
			expect(wrapper.vm.work_place_container).toBe('');
			expect(wrapper.vm.semester_container).toBe('');
			expect(wrapper.vm.description).toBe('description');
		});

		it('should call axios when calling submitUpdate method and the required data are not empty', async () => {
			const openModalSpy = jest.spyOn(
				wrapper.vm.$refs.mamiLoading,
				'openModal'
			);
			const closeModalSpy = jest.spyOn(
				wrapper.vm.$refs.mamiLoading,
				'closeModal'
			);
			await wrapper.setData({
				jobs: 'kerja',
				name: 'name',
				birthday: 'birthday',
				gender: 'gender',
				email: 'email',
				phone: 'phone',
				job: 'job'
			});
			await axios.mockResolve({
				data: {
					status: true
				}
			});
			await wrapper.vm.submitUpdate();

			expect(openModalSpy).toBeCalled();
			expect(closeModalSpy).toBeCalled();
			expect(global.swal).toBeCalled();

			openModalSpy.mockRestore();
			closeModalSpy.mockRestore();
		});

		it('should call axios when calling submitUpdate method and the required data are not empty', async () => {
			const closeModalSpy = jest.spyOn(
				wrapper.vm.$refs.mamiLoading,
				'closeModal'
			);
			const showErrorSpy = jest.spyOn(wrapper.vm, 'showError');
			await wrapper.setData({
				jobs: 'kerja',
				name: 'name',
				birthday: 'birthday',
				gender: 'gender',
				email: 'email',
				phone: 'phone',
				job: 'job'
			});
			await axios.mockResolve({
				data: {
					status: false
				}
			});
			await wrapper.vm.submitUpdate();

			expect(closeModalSpy).toBeCalled();
			expect(showErrorSpy).toBeCalled();

			showErrorSpy.mockRestore();
			closeModalSpy.mockRestore();
		});

		it('should call closeModal, alert, and bugsnagClient, when calling submitUpdate method, but received error api request', async () => {
			await wrapper.setData({
				jobs: 'kerja',
				name: 'name',
				birthday: 'birthday',
				gender: 'gender',
				email: 'email',
				phone: 'phone',
				job: 'job'
			});
			await axios.mockReject('error');
			await wrapper.vm.submitUpdate();

			expect(global.alert).toBeCalled();
			expect(global.bugsnagClient.notify).toBeCalled();
		});
	});
});
