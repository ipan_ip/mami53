import { createLocalVue, shallowMount } from '@vue/test-utils';
import jquery from 'jquery';

import ContainerUserProfile from 'Js/user/components/user-profile/ContainerUserProfile.vue';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import mockComponent from 'tests-fe/utils/mock-component';

// mock imports
jest.mock('Js/@components/NavbarCustom', () => {
	return mockComponent;
});

// mock stubbed component
const stubs = {
	NavbarCustom: mockComponent,
	BreadcrumbTrails: mockComponent,
	RouterView: true
};

describe('ContainerUserProfile.vue', () => {
	const localVue = createLocalVue();

	mockWindowProperty('Cookies', {
		get: jest.fn().mockReturnValue(false),
		set: jest.fn()
	});

	global.$ = jquery;

	const mount = () => {
		return shallowMount(ContainerUserProfile, {
			localVue,
			stubs
		});
	};

	describe('render component correctly', () => {
		let wrapper;

		beforeEach(() => {
			wrapper = mount();
		});

		it('should render container', () => {
			expect(wrapper.find('.app-wrapper').exists()).toBeTruthy();
		});
	});

	describe('call functions correctly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = mount();
		});

		it('should call Cookies.set when banner is hidden ', async () => {
			global.Cookies.get = jest.fn().mockReturnValue(true);
			const jquerySpy = jest.spyOn(global, '$');
			await wrapper.destroy();

			wrapper = mount();

			expect(global.Cookies.set).toBeCalled();
			expect(jquerySpy).toBeCalled();
			jquerySpy.mockRestore();
		});
	});
});
