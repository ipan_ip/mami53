import { createLocalVue, shallowMount } from '@vue/test-utils';

import ProfileSearch from 'Js/user/components/user-profile/ProfileSearch.vue';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import mockComponent from 'tests-fe/utils/mock-component';
import mockAxios from 'tests-fe/utils/mock-axios';

import mixinGetQueryString from 'Js/@mixins/MixinGetQueryString';

// mock stubbed component
const stubs = {
	MamiLoading: {
		template: '<div id="mamiLoading" />',
		methods: {
			closeModal: jest.fn(),
			openModal: jest.fn()
		}
	},
	MiniList: mockComponent,
	SectionTitle: mockComponent,
	RouterLink: true
};

describe('ProfileSearch.vue', () => {
	const localVue = createLocalVue();

	// mock window properties
	mockWindowProperty('location', {
		href: 'http://www.mamikos.com?ref=edit'
	});
	mockWindowProperty('alert', jest.fn());

	// mock global properties
	global.$ = jest.fn().mockReturnValue({
		modal: jest.fn()
	});
	global.navigator = {
		standalone: true
	};
	global.bugsnagClient = {
		notify: jest.fn()
	};
	global.swal = jest.fn();
	global.axios = mockAxios;

	const mount = () => {
		return shallowMount(ProfileSearch, {
			localVue,
			stubs,
			mixins: [mixinGetQueryString],
			methods: {
				closeSwalLoading: jest.fn()
			}
		});
	};

	describe('render component correctly', () => {
		let wrapper;

		beforeEach(() => {
			wrapper = mount();
		});

		it('should render container', () => {
			expect(wrapper.find('.search-container').exists()).toBeTruthy();
		});
	});

	describe('call functions correctly', () => {
		let wrapper;
		beforeEach(() => {
			wrapper = mount();
		});

		it("should set ref as 'edit' when the query ref is equal to edit", () => {
			mockWindowProperty('location', {
				href: 'http://www.mamikos.com?ref=edit'
			});
			wrapper.destroy();
			wrapper = mount();
			expect(wrapper.vm.ref).toBe('edit');
		});

		it('should not set ref when the query ref is not provided', () => {
			mockWindowProperty('location', {
				href: 'http://www.mamikos.com'
			});
			wrapper.destroy();
			wrapper = mount();
			expect(wrapper.vm.ref).toBe('');
		});

		it('should call swal when the search keyword length is less or equal to 0 when calling searchRooms method', async () => {
			wrapper.setData({
				search: ''
			});
			await wrapper.vm.searchRooms();
			expect(global.swal).toBeCalled();
		});

		it('should set data when the search keyword length is more than 0 when calling searchRooms on successful request', async () => {
			wrapper.setData({
				search: 'test'
			});
			axios.mockResolve({
				data: {
					status: true,
					rooms: [],
					'has-more': false,
					limit: 10,
					'next-page': 2,
					offset: 10,
					page: 1,
					total: 10,
					totalStr: 12
				}
			});
			const closeModalSpy = jest.spyOn(
				wrapper.vm.$refs.mamiLoading,
				'closeModal'
			);
			await wrapper.vm.searchRooms();

			expect(wrapper.vm.page).toBe(1);
			expect(global.$).toBeCalled();
			expect(closeModalSpy).toBeCalled();
			closeModalSpy.mockRestore();
		});

		it('should call closeModal when the search keyword length is more than 0 when calling searchRooms on failed request', async () => {
			wrapper.setData({
				search: 'test'
			});
			axios.mockResolve({
				data: {
					status: false
				}
			});
			const closeModalSpy = jest.spyOn(
				wrapper.vm.$refs.mamiLoading,
				'closeModal'
			);
			await wrapper.vm.searchRooms();
			expect(closeModalSpy).toBeCalled();
			closeModalSpy.mockRestore();
		});

		it('should call bugsnag when the search keyword length is more than 0 when calling searchRooms on error request', async () => {
			wrapper.setData({
				search: 'test'
			});
			axios.mockReject('error');
			await wrapper.vm.searchRooms();
			expect(global.bugsnagClient.notify).toBeCalled();
		});
	});
});
