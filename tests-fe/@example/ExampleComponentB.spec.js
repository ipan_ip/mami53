//please use 'shallowMount' by default unless you really need 'mount'
import { createLocalVue, shallowMount } from '@vue/test-utils';
import expect from 'expect';
import Vuex from 'vuex';
import ExampleComponentB from 'Js/@example/components/ExampleComponentB.vue';
//line space
const localVue = createLocalVue();
localVue.use(Vuex);
//line space
describe('ExampleComponentB.vue', () => {
	//always use English and simple clear message/intention about the test
	it('barTotal data should have correct calculation result', () => {
		const store = new Vuex.Store({
			state: {
				foo: {
					bar: [10, 20]
				}
			}
		});
		//line space
		const wrapper = shallowMount(ExampleComponentB, {
			localVue,
			store
		});
		const sumBarMinMax = Number(wrapper.vm.barMin + wrapper.vm.barMax);
		//line space
		wrapper.vm.calculateBarTotal();
		//line space
		expect(wrapper.vm.barTotal !== null).toBe(true);
		expect(wrapper.vm.barTotal).toBe(sumBarMinMax);
	});
});
