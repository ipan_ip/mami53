//please use 'shallowMount' by default unless you really need 'mount'
import { createLocalVue, shallowMount } from '@vue/test-utils';
import expect from 'expect';
import ExampleComponentA from 'Js/@example/components/ExampleComponentA.vue';
//line space
describe('ExampleComponentA.vue', () => {
	it('update name data after calling exampleMethod', () => {
		const localVue = createLocalVue();
		const wrapper = shallowMount(ExampleComponentA, {
			localVue
		});
		//line space
		wrapper.vm.exampleMethod('giga');
		//line space
		expect(wrapper.vm.name).toBe('giga');
	});
});
