import { shallowMount } from '@vue/test-utils';
import DashboardPage from 'Consultant/pages/dashboard/DashboardPage';
import { flushPromises } from 'tests-fe/utils/promises';
import EventBus from 'Consultant/event-bus';
import { isEmpty } from 'lodash';

const bugsnagNotify = jest.fn();
global.bugsnagClient = { notify: bugsnagNotify };

describe('DashboardPage.vue', () => {
	let mountComponent;

	beforeEach(() => {
		bugsnagNotify.mockReset();
		global.axios = {
			get: jest.fn().mockResolvedValue({})
		};

		mountComponent = (options = {}) => {
			return shallowMount(DashboardPage, options);
		};
	});

	it('Should initialize event bus to refresh dashboard correctly', async () => {
		const axiosGet = jest.fn().mockResolvedValue({});
		global.axios = {
			get: axiosGet
		};

		const wrapper = mountComponent();

		await flushPromises();

		expect(axiosGet).toHaveBeenCalledTimes(1);

		EventBus.$emit('REFRESH_DASHBOARD');

		await flushPromises();

		expect(axiosGet).toHaveBeenCalledTimes(2);

		wrapper.destroy();
	});

	it('Should uninitialize event bus to refresh dashboard', async () => {
		const axiosGet = jest.fn().mockResolvedValue({});
		global.axios = {
			get: axiosGet
		};

		const wrapper = mountComponent();

		await flushPromises();

		expect(axiosGet).toHaveBeenCalledTimes(1);

		wrapper.destroy();

		EventBus.$emit('REFRESH_DASHBOARD');

		await flushPromises();

		expect(axiosGet).toHaveBeenCalledTimes(1);
	});

	it('Should set amount by key correctly and match snapshot', async () => {
		const dashboardData = {
			hangingBooking: 0,
			hangingPayment: 100,
			recurringTenant: 20,
			unpaidContract: 1,
			has_new_sales_motion: true
		};

		const axiosGet = jest.fn().mockResolvedValue({
			data: {
				status: true,
				data: dashboardData
			}
		});

		global.axios = {
			get: axiosGet
		};

		const wrapper = mountComponent();

		await flushPromises();

		expect(wrapper.vm.dashboardLists).toEqual(
			expect.arrayContaining([
				expect.objectContaining({
					key: 'recurring',
					amount: dashboardData.recurringTenant
				}),
				expect.objectContaining({
					key: 'booking',
					amount: dashboardData.hangingBooking
				}),
				expect.objectContaining({
					key: 'payment',
					amount: dashboardData.hangingPayment
				})
			])
		);

		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should hit Event Bus with parems correctly', async () => {
		const dashboardData = {
			hangingBooking: 'test hanging booking',
			hangingPayment: 'test hanging payment',
			recurringTenant: 'test recurring tenant',
			unpaidContract: 'test unpaid contract',
			has_new_sales_motion: 'test sales motion'
		};

		const axiosGet = jest.fn().mockResolvedValue({
			data: {
				status: true,
				data: dashboardData
			}
		});

		global.axios = {
			get: axiosGet
		};

		const notificationSidebar = jest.fn();
		EventBus.$on('SET_NOTIFICATION_ON_SIDEBAR', notificationSidebar);

		mountComponent();

		await flushPromises();

		expect(notificationSidebar).toHaveBeenCalledWith({
			booking: dashboardData.hangingBooking,
			payment: dashboardData.hangingPayment,
			recurring: dashboardData.recurringTenant,
			contract: dashboardData.unpaidContract,
			sales_motion: dashboardData.has_new_sales_motion
		});

		EventBus.$off('SET_NOTIFICATION_ON_SIDEBAR', notificationSidebar);
	});

	it('Should move route when dashboard card was clicked', async () => {
		const push = jest.fn();
		const wrapper = mountComponent({
			mocks: {
				$router: { push },
				$isEmpty: isEmpty
			}
		});

		await flushPromises();

		const dashboardCards = wrapper.findAll('.dashboard-card');
		dashboardCards.at(0).trigger('click');

		expect(push).toHaveBeenCalledWith(wrapper.vm.dashboardLists[0].route);

		push.mockReset();

		dashboardCards.at(1).trigger('click');

		expect(push).not.toHaveBeenCalled();

		push.mockReset();

		dashboardCards.at(2).trigger('click');

		expect(push).not.toHaveBeenCalled();
	});

	it('Should call bugsnag notify', async () => {
		global.axios = {
			get: jest.fn().mockRejectedValue(new Error())
		};

		mountComponent();

		await flushPromises();

		expect(bugsnagNotify).toHaveBeenCalled();
	});
});
