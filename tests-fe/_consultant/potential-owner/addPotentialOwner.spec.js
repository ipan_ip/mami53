import { createLocalVue, shallowMount } from '@vue/test-utils';
import { flushPromises } from 'tests-fe/utils/promises';

// import x_mock_json_component_name_x from 'x_url_for_your_mock_json_x';
import addPotentialOwner from 'Consultant/pages/potential-owner/Add';

// const axios = {
// 	get: jest.fn().mockResolvedValue({
// 		data: x_mock_json_component_name_x
// 	})
// };

const bugsnagClient = {
	notify: jest.fn()
};

const mocks = {
	$router: { push: jest.fn() },
	$route: {
		params: {
			stage_id: 100,
			task_id: 50
		}
	}
};

describe('Consultant/pages/potential-owner/Add', () => {
	test('should render correctly', async () => {
		const localVue = createLocalVue();

		const tree = shallowMount(addPotentialOwner, {
			localVue,
			mocks
		});

		await flushPromises();

		expect(tree.element).toMatchSnapshot();
	});

	// ----------------------------------------------------------------

	let mountComponent = Function;

	beforeEach(() => {
		const localVue = createLocalVue();

		console.error = jest.fn();
		global.alert = jest.fn();

		// global.axios = axios;
		global.bugsnagClient = bugsnagClient;

		mountComponent = (options = {}) => {
			return shallowMount(addPotentialOwner, {
				localVue,
				mocks,
				...options
			});
		};
	});

	// test('x_test_name_x', async () => {
	// 	const wrapper = mountComponent();
	// 	await flushPromises();

	// 	// expect
	// });

});
