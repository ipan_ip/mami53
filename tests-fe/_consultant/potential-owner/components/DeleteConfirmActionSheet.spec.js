import { shallowMount } from '@vue/test-utils';
import DeleteConfirmActionSheet from 'Consultant/pages/potential-owner/components/DeleteConfirmActionSheet';

import { flushPromises } from 'tests-fe/utils/promises';
import EventBus from 'Consultant/event-bus';

describe('DeleteConfirmActionSheet', () => {
	let mountComponent;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return shallowMount(DeleteConfirmActionSheet, {
				stubs: {
					ActionSheet: {
						template: `<div><slot></slot></div>`
					}
				},
				...options
			});
		};
	});

	it('Should render component and match snapshot', () => {
		const wrapper = mountComponent();

		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should open action sheet', () => {
		const wrapper = mountComponent();
		wrapper.vm.openActionSheet();
		expect(wrapper.vm.isActionSheetOpen).toBe(true);
	});

	it('Should close action sheet', async () => {
		const wrapper = mountComponent({
			stubs: {
				CtButton: {
					template: `<button @click="$emit('click')"></button>`
				}
			}
		});
		wrapper.vm.openActionSheet();
		expect(wrapper.vm.isActionSheetOpen).toBe(true);

		wrapper
			.findAll('button')
			.at(1)
			.trigger('click');

		expect(wrapper.vm.isActionSheetOpen).toBe(false);
	});

	it('Should execute click back handler when delete has succeed', async () => {
		global.axios = {
			delete: jest.fn().mockResolvedValue({
				data: {
					status: true
				}
			})
		};

		const onClickBack = jest.fn();
		const wrapper = mountComponent({
			mocks: {
				$route: {
					params: {
						id: 'test id'
					},
					meta: {
						navbarOptions: {
							onClickBack
						}
					}
				}
			},
			stubs: {
				CtButton: {
					template: `<button @click="$emit('click')"></button>`
				}
			}
		});

		await wrapper.vm.$nextTick();

		wrapper
			.findAll('button')
			.at(0)
			.trigger('click');

		await flushPromises();

		expect(onClickBack).toHaveBeenCalled();
	});

	it('Should call "openAlert" with message from server', async () => {
		const message = 'Test message from server';
		global.axios = {
			delete: jest.fn().mockResolvedValue({
				data: {
					status: false,
					meta: {
						message
					}
				}
			})
		};

		const wrapper = mountComponent({
			mocks: {
				$route: {
					params: {
						id: 'test id'
					}
				}
			},
			stubs: {
				CtButton: {
					template: `<button @click="$emit('click')"></button>`
				}
			}
		});

		const openAlert = jest.fn();
		EventBus.$on('openAlert', openAlert);

		await wrapper.vm.$nextTick();

		wrapper
			.findAll('button')
			.at(0)
			.trigger('click');

		await flushPromises();

		expect(openAlert).toHaveBeenCalledWith(message);
	});

	it('Should call bugsnag and open alert', async () => {
		global.axios = {
			delete: jest.fn().mockRejectedValue(new Error())
		};

		const bugsnagNotify = jest.fn();
		global.bugsnagClient = { notify: bugsnagNotify };

		const openAlert = jest.fn();
		EventBus.$on('openAlert', openAlert);

		const wrapper = mountComponent({
			mocks: {
				$route: {
					params: {
						id: 'test id'
					}
				}
			},
			stubs: {
				CtButton: {
					template: `<button @click="$emit('click')"></button>`
				}
			}
		});

		await wrapper.vm.$nextTick();

		wrapper
			.findAll('button')
			.at(0)
			.trigger('click');

		await flushPromises();

		expect(openAlert).toHaveBeenCalledWith(
			expect.stringContaining('Terjadi galat, silahkan coba lagi')
		);
		expect(bugsnagNotify).toHaveBeenCalled();
	});
});
