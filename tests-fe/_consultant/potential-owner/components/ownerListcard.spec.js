import { mount, RouterLinkStub } from '@vue/test-utils';
import OwnerListCard from 'Consultant/pages/potential-owner/components/OwnerListCard';
import OwnerItem from '../data/OwnerItem.json';

describe('Consultant/pages/potential-owner/OwnerListCard', () => {
	let mountComponent;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return mount(OwnerListCard, {
				mocks: {
					$dayjs: jest.fn(d => ({ format: jest.fn().mockReturnValue(d) }))
				},
				stubs: {
					'router-link': RouterLinkStub
				},
				...options
			});
		};
	});

	it('Should render component and match snapshot', async () => {
		const wrapper = mountComponent({
			propsData: {
				item: {
					...OwnerItem,
					bbkStatus: {
						color: 'success',
						text: 'BBK'
					},
					followupStatus: {
						color: 'success',
						text: 'Done'
					}
				}
			}
		});

		expect(wrapper.html()).toMatchSnapshot();
	});
});
