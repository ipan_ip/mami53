import { mount } from '@vue/test-utils';
import PotentialOwnerListAction from 'Consultant/pages/potential-owner/components/PotentialOwnerListAction';

describe('PotentialOwnerListAction.vue', () => {
	let mountComponent;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return mount(PotentialOwnerListAction, {
				mocks: {
					$route: { query: {} }
				},
				...options
			});
		};
	});

	it('Should render component with "sort" type', async () => {
		const wrapper = mountComponent();

		wrapper.vm.open('sort');

		await wrapper.vm.$nextTick();

		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should render component with "filter" type', async () => {
		const wrapper = mountComponent();

		wrapper.vm.open('filter');

		await wrapper.vm.$nextTick();

		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should change route with sort selected', async () => {
		const push = jest.fn();
		const wrapper = mountComponent({
			stubs: {
				'ct-button': {
					template: `<button class="actionBtn" @click="$emit('click')"></button>`,

				}
			},
			mocks: {
				$route: { name: 'test', query: {} },
				$router: { push }
			}
		});

		const indexSortSelected = 1;
		const sortItems = wrapper.vm.sortItems;
		const sortSelected = sortItems[indexSortSelected];
		const selectedSortValue = sortSelected.value;

		wrapper.vm.open('sort');

		wrapper.setData({ selectedSort: selectedSortValue });

		wrapper.vm.$forceUpdate();

		wrapper
			.findAll('.actionBtn')
			.at(1)
			.trigger('click');

		expect(push).toHaveBeenCalledWith(
			expect.objectContaining({
				query: expect.objectContaining({
					page: 1,
					sort_by: sortSelected.sort,
					order_direction: sortSelected.order
				})
			})
		);
	});

	it('Should change route with filter selected', async () => {
		const push = jest.fn();
		const wrapper = mountComponent({
			stubs: {
				'ct-button': {
					template: `<button class="actionBtn" @click="$emit('click')"></button>`
				}
			},
			mocks: {
				$route: { name: 'test', query: {} },
				$router: { push }
			}
		});

		wrapper.vm.open('filter');

		await wrapper.vm.$nextTick();

		const indexFilterSelected = 1;
		const filterSelected = wrapper.vm.filterItems[indexFilterSelected];

		wrapper
			.findAll('.ct-checkbox__action')
			.at(indexFilterSelected)
			.trigger('click');

		await wrapper.vm.$nextTick();

		wrapper
			.findAll('.actionBtn')
			.at(1)
			.trigger('click');

		expect(push).toHaveBeenCalledWith(
			expect.objectContaining({
				query: expect.objectContaining({
					page: 1,
					filters: filterSelected.value
				})
			})
		);
	});

	it('Should change route with filter date selected', async () => {
		const push = jest.fn();
		const wrapper = mountComponent({
			stubs: {
				'ct-button': {
					template: `<button class="actionBtn" @click="$emit('click')"></button>`
				}
			},
			mocks: {
				$route: { name: 'test', query: {} },
				$router: { push }
			}
		});

		wrapper.vm.open('filter');

		const dateStart = 'test start date';
		const dateEnd = 'test end date';
		wrapper.setData({
			dateStart,
			dateEnd
		});

		await wrapper.vm.$nextTick();

		wrapper
			.findAll('.actionBtn')
			.at(1)
			.trigger('click');

		expect(push).toHaveBeenCalledWith(
			expect.objectContaining({
				query: expect.objectContaining({
					page: 1,
					date_visit_from: dateStart,
					date_visit_to: dateEnd
				})
			})
		);
	});

	it('Should set selected sort with value from query', async () => {
		const wrapper = mountComponent({
			mocks: {
				$route: {
					query: {
						sort_by: 'date_to_visit',
						order_direction: 'desc'
					}
				}
			}
		});

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.selectedSort).toBe('newest_visit_date');
	});

	it('Should set selected filter with value from query', async () => {
		const expectedFilter = ['consultant'];
		const expectedDateStart = '2020-01-01';
		const expectedDateEnd = '2020-12-12';

		const wrapper = mountComponent({
			mocks: {
				$route: {
					query: {
						filters: expectedFilter.join(','),
						date_visit_from: expectedDateStart,
						date_visit_to: expectedDateEnd
					}
				}
			}
		});

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.selectedFilter).toEqual(expectedFilter);
		expect(wrapper.vm.dateStart).toEqual(expectedDateStart);
		expect(wrapper.vm.dateEnd).toEqual(expectedDateEnd);
	});
});
