import { mount } from '@vue/test-utils';
import mixinStatus from 'Consultant/pages/potential-owner/mixins/status';


describe('Test mixin status', () => {
  let mountComponent;

	beforeEach(() => {
		mountComponent = () => {
			const Component = {
				mixins: [mixinStatus],
				render: h => h('div')
			};

			return mount(Component);
		};
  });
  
  it('Should return BBK Status object correctly', () => {
    const wrapper = mountComponent();
    const expected = {
      color: 'info',
      text: 'Waiting'
    };
    
    expect(wrapper.vm.getDetailBbkStatus('waiting')).toEqual(expected);
  });

  it('Should return default BBK Status object', () => {
    const wrapper = mountComponent();
    const expected = {
      color: 'default',
      text: 'Unknown'
    };
    
    expect(wrapper.vm.getDetailBbkStatus('undefined')).toEqual(expected);
  });

  it('Should return Follow Up Status object correctly', () => {
    const wrapper = mountComponent();
    const expected = {
      color: 'info',
      text: 'On-going'
    };
    
    expect(wrapper.vm.getFollowupStatus('ongoing')).toEqual(expected);
  });

  it('Should return default Follow Up Status object', () => {
    const wrapper = mountComponent();
    const expected = {
      color: 'default',
      text: 'Unknown'
    };
    
    expect(wrapper.vm.getFollowupStatus('undefined')).toEqual(expected);
  });
});