import { shallowMount, RouterLinkStub, createLocalVue } from '@vue/test-utils';
import Detail from 'Consultant/pages/potential-owner/Detail';
import MamiLoadingInline from 'Js/@components/MamiLoadingInline';

import { flushPromises } from 'tests-fe/utils/promises';
import ownerDetail from './data/ownerDetail.json';

import EventBus from 'Consultant/event-bus';
import routeModule from 'Consultant/store/modules/route';
import Vuex from 'vuex';

const axiosMock = {
	get: jest.fn().mockResolvedValue({
		data: {
			status: true,
			owner: ownerDetail
		}
	})
};

const bugsnagClient = {
	notify: jest.fn()
};

const mocks = {
	$router: { push: jest.fn() },
	$route: {
		params: {
			id: 1
		},
		meta: {
			navbarOptions: {
				onClickBack: null
			}
		}
	},
	$dayjs: jest.fn(d => ({ format: () => d }))
};

describe('potential-owner/Detail.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		global.axios = axiosMock;
		global.bugsnagClient = bugsnagClient;

		mountComponent = (options = {}) => {
			return shallowMount(Detail, {
				mocks,
				stubs: {
					RouterLink: RouterLinkStub
				},
				...options
			});
		};
	});

	afterEach(() => {
		EventBus.$off('NAVBAR_MENU_OWNER');
	});

	it('Should render component and match snapshot', async () => {
		const wrapper = mountComponent();

		expect(wrapper.findComponent(MamiLoadingInline).exists()).toBeTruthy();

		await flushPromises();

		expect(wrapper.findComponent(MamiLoadingInline).exists()).toBeFalsy();
		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should show alert and displays nothing', async () => {
		global.axios = {
			get: jest.fn().mockRejectedValue(new Error())
		};

		const wrapper = mountComponent();
		const openAlert = jest.fn();

		EventBus.$on('openAlert', openAlert);

		await flushPromises();

		expect(openAlert).toHaveBeenCalled();
		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should show visit date as "-"', async () => {
		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: {
					status: true,
					owner: { ...ownerDetail, date_to_visit: '0000-00-00' }
				}
			})
		};

		const wrapper = mountComponent();

		await flushPromises();

		expect(wrapper.vm.owner.visitDate).toBe('-');
	});

	it('Should open confirm delete', async () => {
		const openActionSheet = jest.fn();
		const DeleteConfirmAction = {
			template: '<div></div>',
			methods: {
				openActionSheet
			}
		};

		mountComponent({
			stubs: {
				RouterLink: RouterLinkStub,
				DeleteConfirmAction
			}
		});

		await flushPromises();

		EventBus.$emit('NAVBAR_MENU_OWNER', { type: 'delete' });

		expect(openActionSheet).toHaveBeenCalled();
	});

	it('Should move to edit', async () => {
		const ownerId = 'test-owner-id';
		const push = jest.fn();
		mountComponent({
			mocks: {
				...mocks,
				$route: {
					...mocks.$route,
					params: { id: ownerId }
				},
				$router: { push }
			}
		});

		await flushPromises();

		EventBus.$emit('NAVBAR_MENU_OWNER', { type: 'edit' });

		expect(push).toHaveBeenCalledWith({
			name: 'po.edit',
			params: { id: ownerId }
		});
	});

	it('Should move potential owner list when click back button', async () => {
		const localVue = createLocalVue();
		localVue.use(Vuex);

		const push = jest.fn();
		const prevQuery = {
			page: 1,
			test: 'test query'
		};

		const wrapper = mountComponent({
			localVue,
			store: new Vuex.Store({
				modules: {
					route: {
						...routeModule,
						state: {
							previousQuery: {
								'potential-owner': prevQuery
							}
						}
					}
				}
			}),
			mocks: {
				...mocks,
				$router: { push }
			}
		});

		await flushPromises();

		wrapper.vm.$route.meta.navbarOptions.onClickBack();

		expect(push).toHaveBeenCalledWith({
			name: 'potential-owner',
			query: prevQuery
		});
	});

	it('Should clear stored previous query before leave page', async () => {
		const localVue = createLocalVue();
		localVue.use(Vuex);

		const prevQuery = {
			page: 1,
			test: 'test query'
		};

		const wrapper = mountComponent({
			localVue,
			store: new Vuex.Store({
				modules: {
					route: {
						...routeModule,
						state: {
							previousQuery: {
								'potential-owner': prevQuery
							}
						}
					}
				}
			})
		});

		await flushPromises();

		expect(wrapper.vm.$store.state.route.previousQuery).toEqual({
			'potential-owner': prevQuery
		});

		const next = jest.fn();
		const to = {
			name: 'potential-owner'
		};

		Detail.beforeRouteLeave.call(wrapper.vm, to, undefined, next);

		expect(wrapper.vm.$store.state.route.previousQuery).toEqual({});
	});
});
