import { shallowMount, createLocalVue } from '@vue/test-utils';
import Lists from 'Consultant/pages/potential-owner/Lists';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import { flushPromises } from 'tests-fe/utils/promises';
import EventBus from 'Consultant/event-bus';

import EmptyState from 'Consultant/components/ConsultantEmptyState';

const localVue = createLocalVue();
mockVLazy(localVue);

global.axios = {
	get: jest
		.fn()
		.mockResolvedValue({ data: { status: true, data: { data: [] } } })
};
global.tracker = jest.fn();

describe('Consultant/pages/sales-motion/Lists', () => {
	let mountComponent;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return shallowMount(Lists, {
				localVue,
				mocks: {
					$route: {
						query: {}
					},
					$store: {
						state: {
							authData: { id: 'tes' }
						}
					}
				},
				...options
			});
		};
	});

	it('Should render component correctly', async () => {
		const wrapper = mountComponent();

		await flushPromises();

		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should show empty state when have not owner data', async () => {
		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: { data: { data: [] } },
				total: 0,
				status: true
			})
		};

		const wrapper = mountComponent();

		await flushPromises();

		expect(wrapper.findComponent(EmptyState).exists()).toBe(true);
	});

	it('Should set active sort from bus event', async () => {
		const wrapper = mountComponent();

		const data = { type: 'sort', value: 'test_sort' };

		EventBus.$emit('SET_OWNER_ACTION_LABEL', data);

		expect(wrapper.vm.activeSort).toBe(data.value);
	});

	it('Should set active filter length from bus event', async () => {
		const wrapper = mountComponent();

		const data = { type: 'filter', value: 10 };

		EventBus.$emit('SET_OWNER_ACTION_LABEL', data);

		expect(wrapper.vm.activeFilterLength).toBe(data.value);
	});
});
