import { shallowMount } from '@vue/test-utils';
import Add from 'Consultant/pages/potential-owner/Add';
import Button from 'Consultant/components/ConsultantButton.vue';

import { flushPromises } from 'tests-fe/utils/promises';
import ownerDetail from './data/ownerDetail.json';

const axiosMock = {
	get: jest.fn().mockResolvedValue({
		data: {
			status: true,
			owner: ownerDetail
		}
	})
};

const mocks = {
	$dayjs: jest.fn(d => ({ format: () => d })),
	$route: {
		params: {}
	}
};

global.alert = jest.fn();

const mountComponent = (options = {}) => {
	return shallowMount(Add, {
		mocks,
		...options
	});
};

describe('potential-owner/Add.vue', () => {
	it('Should render component and match snapshot', async () => {
		const wrapper = mountComponent();

		await wrapper.vm.$nextTick();

		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should format date correctly', async () => {
		const mockDate = '2020-12-22';
		const dayJsMock = d => ({ format: f => `${d} will be format with ${f}` });
		const wrapper = mountComponent({
			mocks: {
				...mocks,
				$dayjs: dayJsMock
			},
			stubs: {
				Datepicker: {
					template: `<input id="datepicker" @change="$emit('selected', inputDate)" />`,
					data() {
						return {
							inputDate: mockDate
						};
					}
				}
			}
		});

		await wrapper.vm.$nextTick();

		wrapper.find('#datepicker').trigger('change');

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.input.date_to_visit).toBe(
			dayJsMock(mockDate).format('YYYY-MM-DD')
		);
	});

	it('Should submit data as create page and move to detail owner', async () => {
		const id = 'test';
		const axiosPost = jest.fn().mockResolvedValue({
			data: {
				status: true,
				owner: { id }
			}
		});

		global.axios = {
			post: axiosPost
		};

		const push = jest.fn();
		const wrapper = mountComponent({
			mocks: {
				...mocks,
				$router: { push }
			},
			stubs: {
				Button
			}
		});

		await wrapper.vm.$nextTick();

		const inputData = {
			name: 'Test',
			phone_number: '086347474',
			email: 'test@mail.com',
			date_to_visit: '',
			remark: ''
		};

		wrapper.setData({
			input: inputData
		});

		await wrapper.vm.$nextTick();

		const buttonSubmit = wrapper.find('.consultant-btn button');
		buttonSubmit.trigger('click');

		await flushPromises();

		expect(axiosPost).toHaveBeenCalledWith(
			'/api/potential-owner/store',
			inputData
		);

		expect(push).toHaveBeenCalledWith({
			name: 'po.detail',
			params: { id }
		});
	});

	it('Should submit data with "null" email', async () => {
		const axiosPost = jest.fn().mockResolvedValue();

		global.axios = {
			post: axiosPost
		};

		const wrapper = mountComponent({
			mocks: {
				...mocks,
				$router: { push: jest.fn() }
			},
			stubs: {
				Button
			}
		});

		await wrapper.vm.$nextTick();

		wrapper.setData({
			input: {
				name: 'Test',
				phone_number: '086347474',
				email: '',
				date_to_visit: '',
				remark: ''
			}
		});

		await wrapper.vm.$nextTick();

		const buttonSubmit = wrapper.find('.consultant-btn button');
		buttonSubmit.trigger('click');

		await flushPromises();

		expect(axiosPost).toHaveBeenCalledWith(
			expect.any(String),
			expect.objectContaining({ email: null })
		);
	});

	it('Should disable submit button and cannot submit data', async () => {
		global.axios = { post: jest.fn() };
		const wrapper = mountComponent({
			stubs: {
				Button
			}
		});

		const submitData = jest.spyOn(wrapper.vm, 'addUpdatePotentialOwner');

		await wrapper.vm.$nextTick();

		// fill all inputs
		wrapper.setData({
			input: {
				name: 'Test',
				phone_number: '086347474',
				email: 'test@mail.com',
				date_to_visit: '2020-12-22',
				remark: 'test'
			}
		});

		await wrapper.vm.$nextTick();

		const buttonSubmit = wrapper.find('.consultant-btn button');
		buttonSubmit.trigger('click');

		expect(submitData).toHaveBeenCalled();

		submitData.mockReset();
		wrapper.vm.$data.input.name = '';
		await wrapper.vm.$nextTick();
		buttonSubmit.trigger('click');
		expect(submitData).not.toHaveBeenCalled();

		submitData.mockReset();
		wrapper.vm.$data.input.name = 'Test';
		await wrapper.vm.$nextTick();
		buttonSubmit.trigger('click');
		expect(submitData).toHaveBeenCalled();

		submitData.mockReset();
		wrapper.vm.$data.input.email = 'test@mail'; // invalid email
		await wrapper.vm.$nextTick();
		buttonSubmit.trigger('click');
		expect(submitData).not.toHaveBeenCalled();
	});

	describe('Test component as edit page', () => {
		it('Should render component as edit page and match snapshot', async () => {
			global.axios = axiosMock;

			const wrapper = mountComponent({
				mocks: {
					...mocks,
					$route: {
						params: {
							id: 'test'
						}
					}
				}
			});

			await flushPromises();

			expect(wrapper.html()).toMatchSnapshot();
		});

		it('Showing alert when failed to get detail of potential owner', async () => {
			const mockAlert = jest.fn();
			global.axios = {
				get: jest.fn().mockRejectedValue(new Error('Failed to fetch owner'))
			};
			global.alert = mockAlert;

			mountComponent({
				mocks: {
					...mocks,
					$route: {
						params: {
							id: 'test'
						}
					}
				}
			});

			await flushPromises();

			expect(mockAlert).toHaveBeenCalledWith(expect.any(Error));
		});

		it('Should mapping input data correctly', async () => {
			global.axios = {
				get: jest.fn().mockResolvedValue({
					data: {
						status: true,
						owner: {
							...ownerDetail,
							date_to_visit: '0000-00-00',
							email: '-'
						}
					}
				})
			};
			const wrapper = mountComponent({
				mocks: {
					...mocks,
					$route: {
						params: {
							id: 'test'
						}
					}
				}
			});

			await flushPromises();

			expect(wrapper.vm.input).toEqual(
				expect.objectContaining({
					date_to_visit: '',
					email: null
				})
			);
		});

		it('Should submit data as edit page and move to detail owner', async () => {
			const id = 'test';
			const axiosPut = jest.fn().mockResolvedValue({
				data: {
					status: true,
					owner: { id }
				}
			});

			global.axios = {
				...axiosMock,
				put: axiosPut
			};

			const push = jest.fn();
			const wrapper = mountComponent({
				mocks: {
					...mocks,
					$route: {
						...mocks.$route,
						params: { id }
					},
					$router: { push }
				},
				stubs: {
					Button
				}
			});

			await flushPromises();

			const expectedSubmitData = {
				name: ownerDetail.name,
				phone_number: `0${ownerDetail.phone_number}`,
				email: ownerDetail.email === '-' ? null : ownerDetail.email,
				date_to_visit: expect.any(String),
				remark: ownerDetail.notes
			};

			const buttonSubmit = wrapper.find('.consultant-btn button');
			buttonSubmit.trigger('click');

			await flushPromises();

			expect(axiosPut).toHaveBeenCalledWith(
				'/api/potential-owner/update/' + id,
				expectedSubmitData
			);

			expect(push).toHaveBeenCalledWith({
				name: 'po.detail',
				params: { id }
			});
		});
	});
});
