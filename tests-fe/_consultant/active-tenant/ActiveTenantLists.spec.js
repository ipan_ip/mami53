import { shallowMount, createLocalVue } from '@vue/test-utils';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import ActiveTenantLists from 'Consultant/pages/active-tenant/ActiveTenantLists.vue';
import VueRouter from 'vue-router';
import { flushPromises } from 'tests-fe/utils/promises';

const localVue = createLocalVue();
mockVLazy(localVue);

const bugsnagClient = {
	notify: jest.fn()
};

global.bugsnagClient = bugsnagClient;
global.tracker = jest.fn();

localVue.use(VueRouter);

const router = new VueRouter({
	routes: [
		{
			path: 'test',
			meta: {
				navbarOptions: {
					title: ''
				}
			}
		}
	]
});

jest.mock('Consultant/mixins/mixinPagination.js', () => ({
	data() {
		return {
			isPaginationOnly: false,
			customPagination: {},
			pagination: {
				totalData: 0,
				offset: 0,
				page: 1,
				sort_by: '',
				limit: 10,
				search: '',
				filters: []
			}
		};
	}
}));

describe('ActiveTenantLists.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return shallowMount(ActiveTenantLists, {
				localVue,
				router,
				mocks: {
					$dayjs: jest.fn(() => ({ format: jest.fn() })),
					$store: {
						state: {
							authData: { id: 'tes' }
						}
					}
				},
				...options
			});
		};
	});

	it('Should render component', async () => {
		const wrapper = mountComponent();

		await wrapper.vm.$nextTick();

		expect(wrapper.classes('active-tenant-list-page')).toBe(true);
	});

	it('Should set title on beforeRouteEnter hook to "Penyewa Aktif"', () => {
		const wrapper = mountComponent();
		const next = jest.fn();
		const to = {
			query: {},
			meta: { navbarOptions: { title: '' } }
		};

		ActiveTenantLists.beforeRouteEnter.call(wrapper.vm, to, undefined, next);

		expect(to.meta.navbarOptions.title).toBe('Penyewa Aktif');
		expect(next).toHaveBeenCalled();
	});

	it('Should set title on beforeRouteEnter hook to "Ingatkan Pembayaran"', () => {
		const wrapper = mountComponent();
		const next = jest.fn();
		const to = {
			query: {
				'recurring-payment': true
			},
			meta: { navbarOptions: { title: '' } }
		};

		ActiveTenantLists.beforeRouteEnter.call(wrapper.vm, to, undefined, next);

		expect(to.meta.navbarOptions.title).toBe('Ingatkan Pembayaran');
		expect(next).toHaveBeenCalled();
	});

	it('Should set pagination sortBy to be created_at', async () => {
		const wrapper = mountComponent();

		wrapper.vm.defaultPagination();

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.pagination.sort_by).toBe('created_at');
	});

	it('Should set data list and change totalData of pagination', async () => {
		const data = ['test'];
		const totalData = 1;
		const changeQuery = jest.fn();
		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: {
					status: true,
					total: totalData,
					data
				}
			})
		};

		const wrapper = mountComponent({
			methods: {
				changeQueryPaginate: changeQuery
			}
		});

		wrapper.vm.triggerGetData({});

		await flushPromises();

		expect(changeQuery).toHaveBeenCalledWith({ totalData });
		expect(wrapper.vm.activeTenantLists).toEqual(expect.arrayContaining(data));
	});

	it('when response get list with status false', async () => {
		const changeQuery = jest.fn();
		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: {
					status: false
				}
			})
		};

		const wrapper = mountComponent({
			methods: {
				changeQueryPaginate: changeQuery
			}
		});

		wrapper.vm.triggerGetData({});

		await flushPromises();

		expect(changeQuery).not.toHaveBeenCalled();
	});

	it('should execute bugnag and dont call changeQueryPaginate', async () => {
		const changeQuery = jest.fn();
		global.axios = {
			get: jest.fn().mockRejectedValue(new Error('error'))
		};

		const wrapper = mountComponent({
			methods: {
				changeQueryPaginate: changeQuery
			}
		});

		wrapper.vm.triggerGetData({});

		await flushPromises();

		expect(changeQuery).not.toHaveBeenCalled();
		expect(bugsnagClient.notify).toHaveBeenCalled();
	});

	it('should change active of sort item when value is match', async () => {
		const wrapper = mountComponent();

		wrapper.setData({ sortItems: [{ value: 'test', active: false }] });
		wrapper.vm.triggerRouteChanged({
			query: { sort_by: 'test' }
		});

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.sortItems).toEqual(
			expect.arrayContaining([
				expect.objectContaining({ value: 'test', active: true })
			])
		);
	});

	it('should set custom pagination when have query "recurring-payment"', async () => {
		const wrapper = mountComponent();

		wrapper.vm.triggerRouteChanged({
			query: { 'recurring-payment': true }
		});

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.isPaginationOnly).toBe(true);
		expect(wrapper.vm.customPagination).toEqual(
			expect.objectContaining({ 'recurring-payment': true })
		);
	});

	it('should set pagination and change route when search', async () => {
		const changeRoute = jest.fn();
		const wrapper = mountComponent({
			methods: {
				handleChangeRoute: changeRoute
			},
			stubs: {
				'search-box-and-filter': {
					template: `<input id="search"/>`,
					methods: {
						onSearch(data) {
							this.$emit('onSearch', data);
						}
					}
				}
			}
		});

		wrapper.setData({ activeTenantLists: ['test'] });

		await wrapper.vm.$nextTick();
		const search = 'test search';
		wrapper.find('#search').vm.onSearch(search);

		expect(wrapper.vm.pagination.search).toBe(search);
		expect(changeRoute).toHaveBeenCalledWith({
			page: 1,
			sort_by: 'created_at',
			search
		});
	});

	it('should change route with sort_by', async () => {
		const changeRoute = jest.fn();
		const wrapper = mountComponent({
			methods: {
				handleChangeRoute: changeRoute
			},
			stubs: {
				'search-box-and-filter': {
					template: `
						<div>
							<slot name="filter"></slot>
						</div>
					`
				}
			}
		});

		const mockSortBy = 'test';
		wrapper.setData({ activeTenantLists: ['test'] });
		wrapper.setData({ sortItems: [{ value: mockSortBy, active: false }] });

		await wrapper.vm.$nextTick();

		wrapper
			.findAll('.sort__item')
			.at(0)
			.trigger('click');

		expect(changeRoute).toHaveBeenCalledWith({
			page: 1,
			sort_by: mockSortBy,
			search: ''
		});
	});
});
