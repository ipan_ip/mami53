import { shallowMount } from '@vue/test-utils';
import ActiveTenantDetail from 'Consultant/pages/active-tenant/ActiveTenantDetail.vue';

import { flushPromises } from 'tests-fe/utils/promises';

const bugsnagClient = {
	notify: jest.fn()
};

global.bugsnagClient = bugsnagClient;

jest.mock('Consultant/mixins/mixinProcessingDataList.js', () => ({}));

describe('ActiveTenantDetail.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return shallowMount(ActiveTenantDetail, {
				mocks: {
					$route: {
						params: {
							id: 'id'
						}
					}
				},
				...options
			});
		};
	});

	it('Should render component', async () => {
		const wrapper = mountComponent();

		await wrapper.vm.$nextTick();

		expect(wrapper.classes('active-tenant-detail-page')).toBe(true);
	});

	it('Should set data activeTenant', async () => {
		const data = 'test';
		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: {
					status: true,
					data
				}
			})
		};

		const wrapper = mountComponent({
			methods: {
				updateDetailActiveTenantValue: jest.fn()
			}
		});

		await flushPromises();

		expect(wrapper.vm.activeTenant).toBe(data);
	});

	it('Should not set data activeTenant', async () => {
		const updateDetailActiveTenantValue = jest.fn();
		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: {
					status: false
				}
			})
		};

		const wrapper = mountComponent({
			methods: {
				updateDetailActiveTenantValue: jest.fn()
			}
		});

		await flushPromises();

		expect(Object.keys(wrapper.vm.activeTenant).length).toBe(0);
		expect(updateDetailActiveTenantValue).not.toHaveBeenCalled();
	});

	it("Should execute bugsnag and don't call updateDetailActiveTenantValue", async () => {
		const updateDetailActiveTenantValue = jest.fn();
		global.axios = {
			get: jest.fn().mockRejectedValue(new Error('error'))
		};

		mountComponent({
			methods: {
				updateDetailActiveTenantValue
			}
		});

		await flushPromises();

		expect(updateDetailActiveTenantValue).not.toHaveBeenCalled();
		expect(bugsnagClient.notify).toHaveBeenCalled();
	});

	describe('Test updateDetailActiveTenantValue method', () => {
		it('should set url when type is image', () => {
			const url = 'test url';
			const detailLists = [
				{
					type: 'images',
					value: [
						{
							key: 'photo_identifier',
							url: ''
						}
					]
				}
			];
			const tenantData = {
				photo_identifier: url
			};

			const result = ActiveTenantDetail.methods.updateDetailActiveTenantValue(
				tenantData,
				detailLists
			);

			expect(result[0].value).toEqual(
				expect.arrayContaining([
					expect.objectContaining({
						url
					})
				])
			);
		});

		it('should execute processingPriceUnit', () => {
			const processingPriceUnit = jest.fn().mockReturnValue({});

			const wrapper = mountComponent({
				methods: {
					processingPriceUnit
				}
			});

			wrapper.vm.updateDetailActiveTenantValue({}, [
				{ key: 'price_unit', type: '' }
			]);

			expect(processingPriceUnit).toHaveBeenCalled();
		});

		it('should execute createDurationValue', () => {
			const createDurationValue = jest.fn().mockReturnValue({});

			const wrapper = mountComponent({
				methods: {
					createDurationValue
				}
			});

			wrapper.vm.updateDetailActiveTenantValue({}, [
				{ key: 'duration', value: '', type: '' }
			]);

			expect(createDurationValue).toHaveBeenCalled();
		});

		it('should execute mixinChangeDataValueByType', () => {
			const mixinChangeDataValueByType = jest.fn().mockReturnValue({});

			const wrapper = mountComponent({
				methods: {
					mixinChangeDataValueByType
				}
			});

			wrapper.vm.updateDetailActiveTenantValue({ test: 'test' }, [
				{ key: 'test', value: '', type: '' }
			]);

			expect(mixinChangeDataValueByType).toHaveBeenCalled();
		});

		it('should add link when value is email', () => {
			const wrapper = mountComponent({
				methods: {
					mixinChangeDataValueByType: jest.fn()
				}
			});

			const email = 'test@mamiteam.com';

			const result = wrapper.vm.updateDetailActiveTenantValue({ email }, [
				{ key: 'email', type: 'email' }
			]);

			expect(result[0].link).toBe(`mailto:${email}`);
		});

		it('should add link when value is phone', () => {
			const wrapper = mountComponent({
				methods: {
					mixinChangeDataValueByType: jest.fn()
				}
			});

			const phone = '08123456789';

			const result = wrapper.vm.updateDetailActiveTenantValue({ phone }, [
				{ key: 'phone', type: 'phone' }
			]);

			expect(result[0].link).toBe(`tel:${phone}`);
		});
	});
});
