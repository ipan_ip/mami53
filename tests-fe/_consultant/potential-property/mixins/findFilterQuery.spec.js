import { mount } from '@vue/test-utils';
import findFilterQuery from 'Consultant/pages/potential-property/mixins/findFilterQuery';

describe('Test findFilterQuery mixin', () => {
	let mountComponent;

	beforeEach(() => {
		mountComponent = () => {
			const Component = {
				mixins: [findFilterQuery],
				render: h => h('div')
			};

			return mount(Component);
		};
	});

	it(`Should return created_by's possible value`, () => {
		const wrapper = mountComponent();

		const query = ['whatever', 'consultant', 'low'];
		const expected = ['consultant'];

		expect(wrapper.vm.mixFindFilterQuery(query, 'creator')).toEqual(expected);
	});

	it(`Should return priority's possible value`, () => {
		const wrapper = mountComponent();

		const query = ['high', 'whatever', 'consultant', 'low'];
		const expected = ['high', 'low'];

		expect(wrapper.vm.mixFindFilterQuery(query, 'priority')).toEqual(expected);
	});

	it(`Should return empty array when no match anything`, () => {
		const wrapper = mountComponent();

		const query = ['whatever', 'random', 'test'];
		const expected = [];

		expect(wrapper.vm.mixFindFilterQuery(query, 'priority')).toEqual(expected);
	});
});
