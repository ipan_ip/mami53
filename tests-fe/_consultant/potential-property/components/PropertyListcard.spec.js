import { shallowMount } from '@vue/test-utils';

import PropertyListCard from 'Consultant/pages/potential-property/components/PropertyListCard';

import expectExport from 'expect';

const bugsnagClient = {
	notify: jest.fn()
};

const mocks = {
	$router: { push: jest.fn() },
	$dayjs: jest.fn(() => {
		return {
			format: jest.fn()
		};
	})
};

const propsData = {
	item: {
		id: 1228,
		owner_id: 1077,
		name: 'Kos Ranprem1',
		area_city: 'Halmahera Utara',
		province: 'Maluku Utara',
		created_by: 'Ranmium satu',
		priority: 'high',
		bbk_status: 'bbk',
		followup_status: 'rejected',
		product_offered: ['gp2'],
		created_at: '2021-03-29 15:03:19'
	}
};

describe('Consultant/pages/potential-property/components/PropertyListCard', () => {
	let mountComponent = Function;

	beforeEach(() => {
		console.error = jest.fn();

		global.bugsnagClient = bugsnagClient;

		mountComponent = (options = {}) => {
			return shallowMount(PropertyListCard, {
				mocks,
				propsData,
				...options
			});
		};
	});

	test('should render correctly', async () => {
		const tree = shallowMount(PropertyListCard, {
			mocks,
			propsData
		});

		expectExport(tree.element).toMatchSnapshot();
	});

	test('show dash string if area city is empty string', async () => {
		const wrapper = mountComponent({
			propsData: {
				item: {
					...propsData.item,
					area_city: ''
				}
			}
		});

		await wrapper.vm.$nextTick();

		expect(
			wrapper
				.findAll('.detail-icon')
				.at(0)
				.text()
		).toBe('-');
	});

	test('show dash string if can not find priority on priorty labels list', async () => {
		const wrapper = mountComponent({
			propsData: {
				item: {
					...propsData.item,
					priority: ''
				}
			}
		});

		expect(wrapper.vm.priority).toBe('-');
	});

	test('change chip color to success value if no value found chip colors list or value is empty', async () => {
		const wrapper = mountComponent();

		expect(wrapper.vm.chipColorValue('hoihoihoi')).toBe('success');
		expect(wrapper.vm.chipColorValue('')).toBe('success');
	});
});
