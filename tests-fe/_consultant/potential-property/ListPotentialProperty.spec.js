import { shallowMount, createLocalVue } from '@vue/test-utils';
import { flushPromises } from 'tests-fe/utils/promises';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

import MockData from './data/property-list.json';
import ListPotentialProperty from 'Consultant/pages/potential-property/ListPotentialProperty.vue';
import expectExport from 'expect';

global.tracker = jest.fn();

const localVue = createLocalVue();
mockVLazy(localVue);

const axios = {
	get: jest.fn().mockResolvedValue({
		data: MockData
	})
};

const bugsnagClient = {
	notify: jest.fn()
};

const mocks = {
	$router: { push: jest.fn() },
	$route: {
		query: {
			page: 1
		},
		params: {
			id: 1
		},
		meta: {
			navbarOptions: {}
		}
	},
	$store: {
		state: {
			authData: { id: 'test' }
		}
	}
};

describe('Consultant/pages/property/potential-property/ListPotentialProperty.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		console.error = jest.fn();

		global.axios = axios;
		global.bugsnagClient = bugsnagClient;

		mountComponent = (options = {}) => {
			return shallowMount(ListPotentialProperty, {
				mocks,
				...options
			});
		};
	});

	test('should render correctly', async () => {
		const tree = mountComponent();

		await flushPromises();

		expectExport(tree.element).toMatchSnapshot();
	});

	test.skip('go to detail potential property page', async () => {
		const wrapper = mountComponent();
		await flushPromises();

		wrapper.vm.routeToPropertyDetail(1);

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.$router.push).toBeCalledWith({
			name: 'potential-property.detail',
			params: {
				id: 1
			}
		});
	});

	test.skip('go to add potential property page', async () => {
		const wrapper = mountComponent();
		await flushPromises();

		wrapper.vm.clickAddProperty();

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.$router.push).toBeCalledWith({
			name: 'potential-property.add'
		});
	});

	test.skip('display alert if get bad API response', async () => {
		const alert = jest.spyOn(window, 'alert').mockImplementation(() => {});

		global.axios = {
			get: jest.fn().mockRejectedValue(new Error('test error'))
		};

		mountComponent();

		await flushPromises();

		expect(alert).toBeCalled();
		alert.mockRestore();
	});
});
