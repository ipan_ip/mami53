import { shallowMount, createLocalVue } from '@vue/test-utils';
import AddPotentialProperty from 'Consultant/pages/potential-property/AddPotentialProperty';
import MockDataProperty from './data/potential-property-detail.json';
import EventBus from 'Consultant/event-bus';
import VeeValidate from 'Consultant/config/veeValidate';
import { flushPromises } from 'tests-fe/utils/promises';

const localVue = createLocalVue();
localVue.use(VeeValidate);

const tempConsole = { ...console };

describe('AddPotentialProperty.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return shallowMount(AddPotentialProperty, {
				localVue,
				mocks: {
					$route: {
						params: {},
						meta: {
							navbarOptions: {
								onClickBack: jest.fn()
							}
						}
					}
				},
				...options
			});
		};
	});

	beforeAll(() => {
		global.console = {
			error: jest.fn()
		};
	});

	afterAll(() => {
		global.console = tempConsole;
	});

	it('Should render page and match snapshot', async () => {
		const wrapper = mountComponent();

		await wrapper.vm.$nextTick();

		expect(wrapper.html()).toMatchSnapshot();
	});

	it.skip("Should error when form hasn't filled", async () => {
		const wrapper = mountComponent({
			stubs: {
				ctButton: {
					template: `<button @click="$emit('click')"></button>`
				}
			}
		});

		wrapper
			.findAll('button')
			.at(0)
			.trigger('click');

		await flushPromises();

		expect(wrapper.vm.$validator.errors.any()).toBe(true);
	});

	it.skip(`Should openAlert with message "Foto Properti harus diisi" when property image hasn't been uploaded yet`, async () => {
		const wrapper = mountComponent({
			stubs: {
				ctButton: {
					template: `<button @click="$emit('click')"></button>`
				}
			}
		});

		const message = 'Foto Properti harus diisi';
		const openAlert = jest.fn();
		EventBus.$on('openAlert', openAlert);

		wrapper.setData({
			property: {
				property_id: null,
				name: 'test',
				address: 'test',
				area_city: 'test',
				province: 'test',
				total_room: 'test',
				product_offered: ['gp'],
				media: null,
				priority: 'high'
			}
		});

		wrapper.find('button').trigger('click');

		await flushPromises();

		expect(openAlert).toHaveBeenCalledWith(message);
	});

	it.skip(`Should move page to potential property list when submit was succeed`, async () => {
		global.axios = {
			post: jest.fn().mockResolvedValue({ data: { status: true } })
		};

		const push = jest.fn();
		const wrapper = mountComponent({
			stubs: {
				ctButton: {
					template: `<button @click="$emit('click')"></button>`
				}
			},
			mocks: {
				$router: {
					push
				},
				$route: {
					params: {},
					meta: {
						navbarOptions: {
							onClickBack: jest.fn()
						}
					}
				}
			}
		});

		const openAlert = jest.fn();
		EventBus.$on('openAlert', openAlert);

		wrapper.setData({
			property: {
				property_id: null,
				name: 'test',
				address: 'test',
				area_city: 'test',
				province: 'test',
				total_room: 'test',
				product_offered: ['gp'],
				media: 1,
				priority: 'high'
			}
		});

		wrapper.find('button').trigger('click');

		await flushPromises();

		expect(openAlert).toHaveBeenCalledWith(
			expect.any(String),
			'dark',
			expect.objectContaining({ placement: 'bottom' })
		);
		expect(push).toHaveBeenCalledWith({ name: 'potential-property' });
	});

	it.skip(`Should openAlert with message "Terjadi galat, silahkan coba lagi" when submit has been failed`, async () => {
		global.axios = {
			post: jest.fn().mockRejectedValue(new Error())
		};

		const wrapper = mountComponent({
			stubs: {
				ctButton: {
					template: `<button @click="$emit('click')"></button>`
				}
			}
		});

		const message = 'Terjadi galat, silahkan coba lagi';
		const openAlert = jest.fn();
		EventBus.$on('openAlert', openAlert);

		wrapper.setData({
			property: {
				property_id: null,
				name: 'test',
				address: 'test',
				area_city: 'test',
				province: 'test',
				total_room: 'test',
				product_offered: ['gp'],
				photo: 1,
				priority: 'high'
			}
		});

		wrapper.find('button').trigger('click');

		await flushPromises();

		expect(openAlert).toHaveBeenCalledWith([message]);
	});

	it(`Should show alert when upload is failed`, () => {
		const message = 'test message';
		const CtFormUploadImage = {
			template: `<input class="upload" @change="$emit('updated', upload)" />`,
			data() {
				return {
					upload: {
						response: {
							data: {
								status: false
							},
							meta: { message }
						}
					}
				};
			}
		};

		const wrapper = mountComponent({
			stubs: {
				CtFormUploadImage
			}
		});

		const openAlert = jest.fn();
		EventBus.$on('openAlert', openAlert);

		wrapper.find('.upload').trigger('change');

		expect(openAlert).toHaveBeenCalledWith(message);
	});

	it(`Should set media with id from image uploaded`, async () => {
		const imageId = 'test';
		const CtFormUploadImage = {
			template: `<input class="upload" @change="$emit('updated', upload)" />`,
			data() {
				return {
					upload: {
						response: {
							status: true,
							id: imageId
						}
					}
				};
			}
		};

		const wrapper = mountComponent({
			stubs: {
				CtFormUploadImage
			}
		});

		wrapper.find('.upload').trigger('change');
		await wrapper.vm.$nextTick();
		expect(wrapper.vm.property.photo).toBe(imageId);
	});

	describe('Test component in edit condition', () => {
		it(`Should init data property when page is edit`, async () => {
			const getProperty = jest.fn().mockResolvedValue({});

			global.axios = {
				get: getProperty
			};

			mountComponent({
				mocks: {
					$route: {
						params: {
							ownerId: 1,
							id: 1
						},
						meta: {
							navbarOptions: {
								onClickBack: jest.fn()
							}
						}
					}
				}
			});

			await flushPromises();

			expect(getProperty).toHaveBeenCalled();
		});

		it.skip(`Should update property local state with property from server`, async () => {
			const getProperty = jest.fn().mockResolvedValue({
				data: MockDataProperty
			});

			global.axios = {
				get: getProperty
			};

			const wrapper = mountComponent({
				mocks: {
					$route: {
						params: {
							ownerId: 1,
							id: 1
						},
						meta: {
							navbarOptions: {
								onClickBack: jest.fn()
							}
						}
					}
				}
			});

			await flushPromises();

			expect(wrapper.vm.property).toEqual(
				expect.objectContaining({
					name: MockDataProperty.data.name
				})
			);
		});
	});
});
