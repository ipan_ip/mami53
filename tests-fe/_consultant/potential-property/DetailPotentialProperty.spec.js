import { createLocalVue, shallowMount } from '@vue/test-utils';
import { flushPromises } from 'tests-fe/utils/promises';
import EventBus from 'Consultant/event-bus';
import routeModule from 'Consultant/store/modules/route';
import Vuex from 'vuex';

import MockData from './data/potential-property-detail.json';
import DetailPotentialProperty from 'Consultant/pages/potential-property/DetailPotentialProperty.vue';
import expectExport from 'expect';

const axios = {
	get: jest.fn().mockResolvedValue({
		data: MockData
	})
};

const bugsnagClient = {
	notify: jest.fn()
};

const mocks = {
	$router: { push: jest.fn() },
	$route: {
		meta: {
			navbarOptions: {
				title: 'test',
				onClickBack: null
			}
		},
		params: {
			ownerId: 1,
			id: 2
		},
		query: {
			redirect_to: 'list_property_owner'
		}
	},
	$dayjs: jest.fn(() => {
		return {
			format: jest.fn()
		};
	})
};

const localVue = createLocalVue();

describe('Consultant/pages/property/potential-property/DetailPotentialProperty.vue', () => {
	let mountComponent = Function;
	localVue.use(Vuex);

	beforeEach(() => {
		console.error = jest.fn();

		global.axios = axios;
		global.bugsnagClient = bugsnagClient;

		mountComponent = (options = {}) => {
			return shallowMount(DetailPotentialProperty, {
				mocks,
				...options
			});
		};
	});

	afterEach(() => {
		EventBus.$off('NAVBAR_MENU_PROPERTY');
	});

	test('should render correctly', async () => {
		const tree = shallowMount(DetailPotentialProperty, {
			mocks
		});

		await flushPromises();

		expectExport(tree.element).toMatchSnapshot();
	});

	test('display alert if get bad API response', async () => {
		const alert = jest.spyOn(window, 'alert').mockImplementation(() => {});

		global.axios = {
			get: jest.fn().mockRejectedValue(new Error('test error'))
		};

		mountComponent();

		await flushPromises();

		expect(alert).toBeCalled();
		alert.mockRestore();
	});

	test('redirect to owner potential property list when click back button', async () => {
		const prevQuery = {
			owner_id: 1,
			page: 1,
			test: 'test query'
		};

		const wrapper = mountComponent({
			localVue,
			store: new Vuex.Store({
				modules: {
					route: {
						...routeModule,
						state: {
							previousQuery: {
								'potential-property': prevQuery
							}
						}
					}
				}
			})
		});

		await flushPromises();

		wrapper.vm.$route.meta.navbarOptions.onClickBack();

		expect(wrapper.vm.$router.push).toHaveBeenCalledWith({
			name: 'potential-property',
			query: prevQuery
		});
	});

	test('redirect to potential property list when click back button', async () => {
		const prevQuery = {
			page: 1,
			test: 'test query'
		};

		const wrapper = mountComponent({
			localVue,
			store: new Vuex.Store({
				modules: {
					route: {
						...routeModule,
						state: {
							previousQuery: {
								'potential-property': prevQuery
							}
						}
					}
				}
			}),
			mocks: {
				...mocks,
				$route: {
					...mocks.$route,
					query: {
						redirect_to: 'something2'
					}
				}
			}
		});
		await flushPromises();

		wrapper.vm.$route.meta.navbarOptions.onClickBack();

		expect(wrapper.vm.$router.push).toHaveBeenCalledWith({
			name: 'potential-property',
			query: prevQuery
		});
	});

	test('change chip color to success value if no value found chip colors list', async () => {
		const wrapper = mountComponent();
		await flushPromises();

		expect(wrapper.vm.chipColorValue('hoihoihoi')).toBe('success');
	});

	test('go to edit potential property page', async () => {
		const wrapper = mountComponent();
		await flushPromises();

		wrapper.vm.menuHandler({ type: 'edit' });

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.$router.push).toBeCalledWith({
			name: 'potential-property.edit',
			query: {
				from: 'owner_detail'
			},
			params: {
				ownerId: 1,
				id: 2
			}
		});
	});

	test('show delete potential property confirmation', async () => {
		const wrapper = mountComponent();
		await flushPromises();

		wrapper.vm.menuHandler({ type: 'delete' });

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.openSnackbar).toBe(true);
	});

	test('delete potential property', async () => {
		global.axios = {
			delete: jest.fn().mockResolvedValue({
				data: {
					status: true,
					message: 'lorem ipsum jare'
				}
			})
		};

		const openAlert = jest.fn();
		const wrapper = mountComponent();
		EventBus.$on('openAlert', openAlert);
		await flushPromises();

		wrapper.vm.triggerDelete();
		await flushPromises();

		expect(wrapper.vm.deletingProperty).toBe(true);
		expect(openAlert).toHaveBeenCalledWith(
			'Data properti potensial berhasil dihapus',
			'dark',
			{
				placement: 'bottom'
			}
		);
	});

	test('fail delete potential property', async () => {
		global.axios = {
			delete: jest.fn().mockRejectedValue()
		};

		const wrapper = mountComponent();
		await flushPromises();

		wrapper.vm.triggerDelete();
		await flushPromises();

		expect(wrapper.vm.deletingProperty).toBe(false);
	});

	test('clear page state when go to other page', async () => {
		const beforeDestroyedSpy = jest.spyOn(
			DetailPotentialProperty,
			'beforeDestroy'
		);
		const wrapper = mountComponent();
		await flushPromises();

		wrapper.destroy();

		await wrapper.vm.$nextTick();

		expect(beforeDestroyedSpy).toHaveBeenCalled();
	});
});
