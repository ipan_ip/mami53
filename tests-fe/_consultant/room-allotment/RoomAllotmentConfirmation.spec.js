import { shallowMount, createLocalVue } from '@vue/test-utils';
import RoomAllotmentConfirmation from 'Consultant/pages/room-allotment/components/RoomAllotmentConfirmation';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import Button from 'Consultant/components/ConsultantButton.vue';

const localVue = createLocalVue();
mockVLazy(localVue);

describe('RoomAllotmentConfirmation', () => {
	let mountComponent;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return shallowMount(RoomAllotmentConfirmation, {
				localVue,
				...options
			});
		};
	});

	it('Should open modal and match snapshot', async () => {
		const wrapper = mountComponent();

		wrapper.vm.confirmation(true);

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.isOpen).toBe(true);
		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should attach an emit close when modal closed', async () => {
		const wrapper = mountComponent({
			stubs: {
				Button
			}
		});

		const emitClose = jest.fn();
		wrapper.vm.$on('close', emitClose);

		wrapper.vm.confirmation(true);

		await wrapper.vm.$nextTick();

		wrapper.find('.consultant-btn button').trigger('click');

		await wrapper.vm.$nextTick();

		expect(emitClose).toHaveBeenCalled();
	});

	it('Should render "add" content and emit type "addRoom"', async () => {
		const wrapper = mountComponent({
			stubs: {
				Button
			}
		});

		const addRoom = jest.fn();
		wrapper.vm.$on('addRoom', addRoom);

		wrapper.vm.confirmation(true, 'add');

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.content).toEqual({
			title: 'Yakin ingin tambah kamar baru?',
			subtitle: 'Pastikan jumlah kamar sesuai dengan yang dimiliki kos.',
			buttonText: 'Tambah Kamar'
		});

		wrapper
			.findAll('.consultant-btn button')
			.at(1)
			.trigger('click');

		expect(addRoom).toHaveBeenCalled();
	});

	it('Should render "delete" content and emit type "deleteRoom"', async () => {
		const wrapper = mountComponent({
			stubs: {
				Button
			}
		});

		const deleteRoom = jest.fn();
		wrapper.vm.$on('deleteRoom', deleteRoom);

		wrapper.vm.confirmation(true, 'delete');

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.content).toEqual({
			title: 'Yakin ingin hapus kamar ini?',
			subtitle: 'Kamu akan menghapus kamar dari kos.',
			buttonText: 'Hapus'
		});

		wrapper
			.findAll('.consultant-btn button')
			.at(1)
			.trigger('click');

		expect(deleteRoom).toHaveBeenCalled();
	});

	it('Should render "back" content and emit type "onBack"', async () => {
		const wrapper = mountComponent({
			stubs: {
				Button
			}
		});

		const onBack = jest.fn();
		wrapper.vm.$on('onBack', onBack);

		wrapper.vm.confirmation(true, 'back');

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.content).toEqual({
			title: 'Mohon Perhatiannya Sebentar',
			subtitle:
				'Data yang telah dimasukkan pada halaman ini belum disimpan. Yakin ingin pindah halaman?',
			buttonText: 'Ya, Pindahkan'
		});

		wrapper
			.findAll('.consultant-btn button')
			.at(1)
			.trigger('click');

		expect(onBack).toHaveBeenCalled();
	});

	it('Should render "activateSwitch" content and emit type "onToggleSwitch"', async () => {
		const wrapper = mountComponent({
			stubs: {
				Button
			}
		});

		const onToggleSwitch = jest.fn();
		wrapper.vm.$on('onToggleSwitch', onToggleSwitch);

		wrapper.vm.confirmation(true, 'activateSwitch');

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.content).toEqual({
			title: 'Yakin ingin menandai kamar ini “Sudah Berpenghuni”?',
			subtitle: 'Kamar akan ditandai terisi/sudah berpenghuni.',
			buttonText: 'Ya',
			cancelText: 'Tidak'
		});

		wrapper
			.findAll('.consultant-btn button')
			.at(1)
			.trigger('click');

		expect(onToggleSwitch).toHaveBeenCalled();
	});

	it('Should render "deactivateSwitch" content and emit type "onToggleSwitch"', async () => {
		const wrapper = mountComponent({
			stubs: {
				Button
			}
		});

		const onToggleSwitch = jest.fn();
		wrapper.vm.$on('onToggleSwitch', onToggleSwitch);

		wrapper.vm.confirmation(true, 'deactivateSwitch');

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.content).toEqual({
			title: 'Yakin ingin menghapus tanda “Sudah Berpenghuni”?',
			subtitle: 'Kamar akan ditandai kosong/tidak berpenghuni.',
			buttonText: 'Ya',
			cancelText: 'Tidak'
		});

		wrapper
			.findAll('.consultant-btn button')
			.at(1)
			.trigger('click');

		expect(onToggleSwitch).toHaveBeenCalled();
	});
});
