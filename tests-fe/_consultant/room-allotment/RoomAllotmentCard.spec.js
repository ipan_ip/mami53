import { shallowMount, createLocalVue } from '@vue/test-utils';
import { flushPromises } from 'tests-fe/utils/promises';
import VeeValidate from 'Consultant/config/veeValidate';

import RoomAllotmentCard from 'Consultant/pages/room-allotment/components/Card.vue';

jest.mock('Consultant/styles/_tooltip.scss', () => jest.fn());

const localVue = createLocalVue();
localVue.use(VeeValidate);
localVue.mixin({
	computed: {
		navigator() {
			return {
				isMobile: true
			};
		}
	}
});

const bugsnagClient = {
	notify: jest.fn()
};

describe('Consultant/pages/room-allotment/components/Card.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		console.error = jest.fn();

		global.bugsnagClient = bugsnagClient;

		mountComponent = (options = {}) => {
			return shallowMount(RoomAllotmentCard, {
				localVue,
				propsData: {
					uniqueId: 'test',
					listRoom: [{ unique_id: test, name: 'test name' }]
				},
				...options
			});
		};
	});

	test('should render correctly', async () => {
		const tree = mountComponent();

		await flushPromises();

		expect(tree.element).toMatchSnapshot();
	});

	test('should render tooltip', async () => {
		const tree = mountComponent({
			propsData: {
				numbering: 1,
				roomNameOrNumber: 'Test',
				roomFloor: 1,
				isDisabled: true,
				occupied: true,
				uniqueId: 'unique-id',
				listRoom: []
			}
		});

		await tree.vm.$nextTick();

		expect(tree.find('.text-occupied')).toMatchSnapshot();
	});

	test('should attach an event delete when click delete icon', async () => {
		const tree = mountComponent();

		await tree.vm.$nextTick();

		const deleteEvent = jest.fn();

		tree.vm.$on('clickDelete', deleteEvent);

		tree.find('.delete-button').trigger('click');

		expect(deleteEvent).toHaveBeenCalled();
	});

	test('should not attach an event delete when click delete icon if room has a contract', async () => {
		const tree = mountComponent({
			propsData: {
				isDisabled: true,
				uniqueId: 'test',
				listRoom: []
			}
		});

		await tree.vm.$nextTick();

		const deleteEvent = jest.fn();

		tree.vm.$on('clickDelete', deleteEvent);

		tree.find('.delete-button').trigger('click');

		expect(deleteEvent).not.toHaveBeenCalled();
	});
});
