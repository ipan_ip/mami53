import { shallowMount } from '@vue/test-utils';
import RoomAllotmentList from 'Consultant/pages/room-allotment/List.vue';
import Card from 'Consultant/pages/room-allotment/components/Card';
import Pagination from 'Consultant/components/ConsultantPagination.vue';
import mockRoomAllotmentList from './data/room-allotment.json';
import mockRoomAllotmentList11Data from './data/room-allotment-11-data.json';
import { flushPromises } from 'tests-fe/utils/promises';

jest.mock('Consultant/event-bus', () => jest.fn());
jest.mock('Consultant/styles/_tooltip.scss', () => jest.fn());

const axios = {
	get: jest.fn().mockResolvedValue({
		data: mockRoomAllotmentList
	})
};

const bugsnagClient = {
	notify: jest.fn()
};

const mocks = {
	$router: { push: jest.fn(), go: jest.fn() },
	$route: {
		params: {
			song_id: 1
		},
		meta: {
			navbarOptions: {}
		}
	}
};

describe('Consultant/pages/room-allotment/List.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		console.error = jest.fn();

		global.axios = axios;
		global.bugsnagClient = bugsnagClient;

		mountComponent = (options = {}) => {
			return shallowMount(RoomAllotmentList, {
				mocks,
				...options
			});
		};
	});

	test('should render correctly', async () => {
		const tree = mountComponent();

		expect(tree.find('.room-allotment-list').exists()).toBe(true);
	});

	test('should render room list correctly', async () => {
		const tree = mountComponent();

		await flushPromises();

		const cards = tree.findAll(Card);

		expect(cards.length).toBe(mockRoomAllotmentList.data.length);
	});

	test('should have pagination when total data grether than 10', async () => {
		global.axios = {
			get: jest.fn().mockResolvedValue({ data: mockRoomAllotmentList11Data })
		};

		const tree = mountComponent();

		await flushPromises();

		const pagination = tree.find(Pagination);

		expect(pagination.exists()).toBe(true);
	});
});
