import * as Helper from 'Consultant/pages/room-allotment/helpers';

describe('Test helper on room allotment', () => {
	describe('Test fuction handleFindRoomBySearch', () => {
		test('should keep list when matched with name', () => {
			const list = [{ name: 'test' }];
			const listFiltered = list.filter(
				Helper.handleFindRoomBySearch.bind({ src: 'test' })
			);

			expect(listFiltered).toEqual(list);
		});

		test('should remove list when not matched with name', () => {
			const list = [{ name: 'test' }];
			const listFiltered = list.filter(
				Helper.handleFindRoomBySearch.bind({ src: 'test-1' })
			);

			expect(listFiltered).toEqual([]);
		});
	});
});
