import { mount } from '@vue/test-utils';
import RoomFilter from 'Consultant/pages/room-allotment/components/RoomFilter';

describe('RoomFilter', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return mount(RoomFilter, {
				propsData: {
					value: 'all'
				},
				...options
			});
		};
	});

	test('should render correctly', async () => {
		const wrapper = mountComponent();

		await wrapper.vm.$nextTick();

		expect(wrapper.element).toMatchSnapshot();
	});

	test('should set active on the filter was matched with prop value', async () => {
		const wrapper = mountComponent();
		wrapper.setData({
			roomFilters: [
				{
					id: 'id',
					active: false,
					key: 'test'
				}
			]
		});

		await wrapper.vm.$nextTick();

		wrapper.setProps({
			value: 'test'
		});

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.roomFilters).toEqual(
			expect.arrayContaining([
				expect.objectContaining({ key: 'test', active: true })
			])
		);
	});

	test('should attach an event input with filter selected', async () => {
		const wrapper = mountComponent();
		wrapper.setData({
			roomFilters: [
				{
					id: 'id',
					active: false,
					key: 'test'
				}
			]
		});

		const emitInput = jest.fn();
		wrapper.vm.$on('input', emitInput);

		await wrapper.vm.$nextTick();

		wrapper.find('.chip').trigger('click');

		expect(emitInput).toHaveBeenCalledWith('test');
	});
});
