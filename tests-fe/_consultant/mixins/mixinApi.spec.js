import { mount } from '@vue/test-utils';
import mixinApi from 'Consultant/mixins/mixinApi';
import { flushPromises } from 'tests-fe/utils/promises';

describe('Test mixinApi mixin', () => {
	let mountComponent;

	beforeEach(() => {
		mountComponent = () => {
			const Component = {
				mixins: [mixinApi],
				render: h => h('div')
			};

			return mount(Component);
		};
	});

	it(`Should hit API with URL correctly`, async () => {
		const url = 'test-url';
		const axiosGet = jest
			.fn()
			.mockResolvedValue({ data: { status: false, data: '' } });
		global.axios = {
			get: axiosGet
		};
		const wrapper = mountComponent();

		await wrapper.vm.mixApiGet({ url });

		expect(axiosGet).toHaveBeenCalledWith(url);
	});

	it(`Should return data correctly`, async () => {
		const response = {
			status: true,
			data: {
				test: ['test']
			}
		};
		const axiosGet = jest.fn().mockResolvedValue({ data: response });
		global.axios = {
			get: axiosGet
		};
		const wrapper = mountComponent();

		const result = await wrapper.vm.mixApiGet({
			url: 'test',
			field: 'data.test'
		});

		await flushPromises();

		expect(result).toEqual({
			success: true,
			data: response.data.test,
			message: expect.any(String),
			response
		});
	});

	it(`Should return false with message from API response`, async () => {
		const message = 'test message';
		const response = {
			status: false,
			message
		};
		const axiosGet = jest.fn().mockResolvedValue({ data: response });
		global.axios = {
			get: axiosGet
		};
		const wrapper = mountComponent();

		const result = await wrapper.vm.mixApiGet({
			url: 'test'
		});

		await flushPromises();

		expect(result).toEqual(
			expect.objectContaining({
				success: false,
				message
			})
		);
	});

	it(`Should return error and execute bugsnag`, async () => {
		const bugsnag = jest.fn();
		const axiosGet = jest.fn().mockRejectedValue(new Error());
		global.axios = {
			get: axiosGet
		};
		global.bugsnagClient = { notify: bugsnag };
		const wrapper = mountComponent();

		const result = await wrapper.vm.mixApiGet({
			url: 'test'
		});

		await flushPromises();

		expect(bugsnag).toHaveBeenCalled();
		expect(result).toEqual(
			expect.objectContaining({
				success: false,
				message: expect.stringContaining('Terjadi galat')
			})
		);
	});
});
