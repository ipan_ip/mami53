import { shallowMount, createLocalVue } from '@vue/test-utils';
import EditAmount from 'Consultant/pages/contract/EditAmount.vue';
import invoiceModule from 'Consultant/store/modules/invoice';
import ConsultantButton from 'Consultant/components/ConsultantButton.vue';

import { flushPromises } from 'tests-fe/utils/promises';
import EventBus from 'Consultant/event-bus';

import Vuex from 'vuex';

const localVue = createLocalVue();
localVue.use(Vuex);

const mocks = {
	$route: {
		query: {}
	},
	$router: {
		go: jest.fn(),
		push: jest.fn()
	}
};

describe('EditAmount.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return shallowMount(EditAmount, {
				localVue,
				store: new Vuex.Store({
					modules: {
						invoice: invoiceModule
					}
				}),
				mocks,
				...options
			});
		};
	});

	test('Should render component correctly', async () => {
		const wrapper = mountComponent();

		expect(wrapper.find('.edit-amount').exists()).toBeTruthy();
	});

	test('Should set local amount state', async () => {
		const wrapper = mountComponent();
		const amount = 'test';
		wrapper.vm.$store.commit('invoice/setAmount', amount);

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.updatedAmount).toBe(amount);
	});

	test('Should go to invoice page with params from current query', async () => {
		global.axios = {
			post: jest.fn().mockResolvedValue({
				data: { status: true, data: {} }
			})
		};

		const push = jest.fn();
		const tenantId = 'test tenant id';
		const booking = 'test booking';
		const contractStatus = 'test contract status';
		const wrapper = mountComponent({
			stubs: { ConsultantButton },
			mocks: {
				$route: {
					params: {
						invoiceId: '1'
					},
					query: {
						tenant_id: tenantId,
						from_booking: booking,
						contract_status: contractStatus
					}
				},
				$router: {
					push
				}
			}
		});

		wrapper.setData({ updatedAmount: 'test' });

		await wrapper.vm.$nextTick();

		wrapper.find('.consultant-btn button').trigger('click');

		await flushPromises();

		expect(push).toHaveBeenCalledWith({
			name: 'invoice',
			params: {
				id: tenantId,
				booking: booking,
				contract_status: contractStatus
			}
		});
	});

	test('Should go to previous page after submit succeed', async () => {
		global.axios = {
			post: jest.fn().mockResolvedValue({
				data: { status: true, data: {} }
			})
		};

		const go = jest.fn();
		const wrapper = mountComponent({
			stubs: { ConsultantButton },
			mocks: {
				$route: {
					params: {
						invoiceId: '1'
					},
					query: {
						tenant_id: 'test'
					}
				},
				$router: {
					go
				}
			}
		});

		wrapper.setData({ updatedAmount: 'test' });

		await wrapper.vm.$nextTick();

		wrapper.find('.consultant-btn button').trigger('click');

		await flushPromises();

		expect(go).toHaveBeenCalledWith(-1);
	});

	test('Should show alert and execute bugsnag', async () => {
		const openAlert = jest.fn();
		const bugsnagNotify = jest.fn();

		global.bugsnagClient = { notify: bugsnagNotify };

		global.axios = {
			post: jest.fn().mockRejectedValue(new Error())
		};

		const wrapper = mountComponent({
			stubs: { ConsultantButton },
			mocks: {
				$route: {
					params: {
						invoiceId: '1'
					},
					query: {}
				},
				$router: {
					go: jest.fn()
				}
			}
		});

		EventBus.$on('openAlert', openAlert);

		wrapper.setData({ updatedAmount: 'test' });

		await wrapper.vm.$nextTick();

		const submitButton = wrapper.find('.consultant-btn button');
		submitButton.trigger('click');
		await flushPromises();

		expect(bugsnagNotify).toHaveBeenCalledWith(expect.any(Error));
		expect(openAlert).toHaveBeenCalledWith('Terjadi galat. Silakan coba lagi.');
	});
});
