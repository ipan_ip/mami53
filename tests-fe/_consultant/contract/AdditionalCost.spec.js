import { shallowMount, createLocalVue } from '@vue/test-utils';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import AdditionalCost from 'Consultant/pages/contract/AdditionalCost.vue';

const localVue = createLocalVue();
mockVLazy(localVue);

jest.useFakeTimers();

jest.mock('Consultant/event-bus/index', () => {
	const EventBus = {
		$on: jest.fn()
	};
	global.EventBus = EventBus;
	return global.EventBus;
});

const propsData = {
	value: ['test value'],
	label: 'test label'
};

describe('Consultant/pages/contract/AdditionalCost.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return shallowMount(AdditionalCost, {
				localVue,
				propsData,
				...options
			});
		};
	});

	test('Should render correctly', async () => {
		const tree = shallowMount(AdditionalCost, {
			localVue,
			propsData
		});
		expect(tree.element).toMatchSnapshot();
	});

	test('send value to parent component', async () => {
		const wrapper = mountComponent();

		wrapper.vm.sendValueToParent();
		await wrapper.vm.$nextTick();
		expect(wrapper.emitted('input')).toBeTruthy();
	});

	test('toggling add cost form component', async () => {
		const wrapper = mountComponent();

		wrapper.vm.toggleOpenAddCostForm();
		await wrapper.vm.$nextTick();

		expect(wrapper.vm.isAddCostActive).toBe(true);
		expect(wrapper.find('.other-costs__form').classes('is--open')).toBe(true);
	});

	test('edit existing additional cost', async () => {
		const action = {
			key: 'edit'
		};

		const test = {
			test: 'test'
		};

		const wrapper = mountComponent();

		wrapper.vm.handleSelectMenu(action, test, 0);
		await wrapper.vm.$nextTick();

		expect(wrapper.find('.other-costs__form').classes('is--open')).toBe(true);

		expect(wrapper.vm.cost).toEqual(test);
	});

	test('delete existing additional cost', async () => {
		const action = {
			key: 'delete'
		};

		const wrapper = mountComponent();

		wrapper.setData({
			costs: [0, 1, 2, 3, 4]
		});
		await wrapper.vm.$nextTick();

		wrapper.vm.handleSelectMenu(action, {}, 0);
		await wrapper.vm.$nextTick();

		expect(wrapper.emitted('delete')).toBeTruthy();

		expect(wrapper.vm.costs.length).toBe(4);
	});

	test('save existing additional price value', async () => {
		const editCost = {
			test: 'test'
		};

		const wrapper = mountComponent();

		wrapper.setData({
			cost: editCost,
			costs: [{ test: 'a' }, { test: 'b' }],
			isCostUpdate: true,
			indexCostActive: 0
		});
		await wrapper.vm.$nextTick();

		wrapper.vm.handleSaveAdditionalPrice();
		await wrapper.vm.$nextTick();

		expect(wrapper.vm.costs).toEqual(
			expect.arrayContaining([expect.objectContaining(editCost)])
		);
	});

	test('save new additional price value', async () => {
		const editCost = {
			cost_title: 'test title',
			cost_value: 'test value'
		};

		const wrapper = mountComponent();

		wrapper.setData({
			cost: editCost,
			isCostUpdate: false,
			indexCostActive: 0
		});

		wrapper.vm.handleSaveAdditionalPrice();
		await wrapper.vm.$nextTick();

		expect(wrapper.vm.costs).toEqual(
			expect.arrayContaining([expect.objectContaining(editCost)])
		);
	});

	test('reset value after save additional price', () => {
		const mockReset = jest.fn();

		const wrapper = mountComponent({
			methods: {
				resetCost: mockReset
			}
		});

		wrapper.vm.handleSaveAdditionalPrice();

		jest.advanceTimersByTime(301);
		expect(mockReset).toBeCalled();
	});
});
