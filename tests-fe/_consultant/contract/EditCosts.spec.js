import { shallowMount, createLocalVue } from '@vue/test-utils';
import EditCosts from 'Consultant/pages/contract/EditCosts.vue';
import invoiceModule from 'Consultant/store/modules/invoice';
import ConsultantButton from 'Consultant/components/ConsultantButton.vue';

import EventBus from 'Consultant/event-bus';
import { flushPromises } from 'tests-fe/utils/promises';
import { cloneDeep } from 'lodash';

import Vuex from 'vuex';

// mock mixin
jest.mock('Consultant/pages/contract/mixins/handleUniqueQuery.js', () => ({}));

const mountComponent = (options = {}) => {
	const localVue = createLocalVue();
	localVue.use(Vuex);

	return shallowMount(EditCosts, {
		localVue,
		store: new Vuex.Store(
			cloneDeep({
				modules: {
					invoice: invoiceModule
				}
			})
		),
		mocks: {
			$route: {
				params: { invoiceId: 'test' }
			}
		},
		...options
	});
};

describe('EditCosts.vue', () => {
	test('Should render component and match snapshot', async () => {
		const wrapper = mountComponent();

		expect(wrapper.html()).toMatchSnapshot();
	});

	test('Should mapping existing costs correctly', async () => {
		const wrapper = mountComponent();
		const otherCost = {
			id: '1',
			invoice_id: 'test other cost invoice id',
			cost_title: 'test other cost title',
			cost_value: 'test other cost value',
			cost_type: 'other'
		};
		const fixCost = {
			id: '2',
			invoice_id: 'test fixed cost invoice id',
			cost_title: 'test fixed cost title',
			cost_value: 'test fixed cost value',
			cost_type: 'fixed'
		};
		const existingCosts = [otherCost, fixCost];

		wrapper.vm.$store.commit('invoice/setAdditionalCost', existingCosts);

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.fineCost).toEqual([
			{
				additional_cost_id: fixCost.id,
				invoice_id: fixCost.invoice_id,
				cost_title: fixCost.cost_title,
				cost_value: fixCost.cost_value,
				cost_type: fixCost.cost_type,
				action: 'UPDATE'
			}
		]);
		expect(wrapper.vm.otherCost).toEqual([
			{
				additional_cost_id: otherCost.id,
				invoice_id: otherCost.invoice_id,
				cost_title: otherCost.cost_title,
				cost_value: otherCost.cost_value,
				cost_type: otherCost.cost_type,
				action: 'UPDATE'
			}
		]);
	});

	test('Should store deleted cost to costsDeleted state', async () => {
		const cost = {
			additional_cost_id: '1',
			invoice_id: 'test invoice id',
			cost_title: 'test title',
			cost_value: 'test value',
			cost_type: 'test'
		};

		const AddOtherCost = {
			template: `
				<div>
					<button class="delete-cost" @click="$emit('delete', cost)"></button>
				</div>
			`,
			data() {
				return { cost };
			}
		};

		const wrapper = mountComponent({
			stubs: {
				AddOtherCost
			}
		});

		const deleteButtons = wrapper.findAll('.delete-cost');
		deleteButtons.at(0).trigger('click');

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.costsDeleted).toEqual([
			{ ...cost, action: 'DELETE', cost_type: 'fixed' }
		]);

		deleteButtons.at(1).trigger('click');

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.costsDeleted).toEqual([
			{ ...cost, action: 'DELETE', cost_type: 'fixed' },
			{ ...cost, action: 'DELETE', cost_type: 'other' }
		]);
	});

	test('Should submit cost data correctly ', async () => {
		const axiosPost = jest.fn().mockResolvedValue({
			data: {
				status: true,
				data: {}
			}
		});

		global.axios = {
			post: axiosPost
		};

		const invoiceId = 'test invoice id';
		const wrapper = mountComponent({
			stubs: { ConsultantButton },
			mocks: {
				$route: {
					params: {
						invoiceId
					},
					query: {}
				},
				$router: {
					go: jest.fn()
				}
			}
		});

		const costsDeleted = { type: 'DELETED' };
		const otherCost = {
			id: '1',
			invoice_id: 'test other cost invoice id',
			cost_title: 'test other cost title',
			cost_value: 'test other cost value',
			cost_type: 'other'
		};
		const fixCost = {
			id: '2',
			invoice_id: 'test fixed cost invoice id',
			cost_title: 'test fixed cost title',
			cost_value: 'test fixed cost value',
			cost_type: 'fixed'
		};
		const existingCosts = [otherCost, fixCost];
		wrapper.vm.$store.commit('invoice/setAdditionalCost', existingCosts);
		wrapper.setData({ costsDeleted: [costsDeleted] });
		const expectedSubmitData = [
			{
				additional_cost_id: fixCost.id,
				invoice_id: invoiceId,
				cost_title: fixCost.cost_title,
				cost_value: fixCost.cost_value,
				cost_type: fixCost.cost_type,
				action: 'UPDATE'
			},
			{
				additional_cost_id: otherCost.id,
				invoice_id: invoiceId,
				cost_title: otherCost.cost_title,
				cost_value: otherCost.cost_value,
				cost_type: otherCost.cost_type,
				action: 'UPDATE'
			},
			costsDeleted
		];

		await wrapper.vm.$nextTick();

		wrapper.find('.consultant-btn button').trigger('click');

		await flushPromises();

		expect(axiosPost).toHaveBeenCalledWith(
			'/api/contract/invoice/update/additional-cost',
			{
				additional_costs: expectedSubmitData
			}
		);
	});

	test('Should go to invoice page with params from current query after submit succeed', async () => {
		const axiosPost = jest.fn().mockResolvedValue({
			data: {
				status: true,
				data: {}
			}
		});

		global.axios = {
			post: axiosPost
		};

		const booking = 'true';
		const tenantId = 'test tenant id';
		const contractStatus = 'test contract status';
		const push = jest.fn();
		const wrapper = mountComponent({
			stubs: { ConsultantButton },
			mocks: {
				$route: {
					params: {
						invoiceId: '1'
					},
					query: {
						from_booking: booking,
						tenant_id: tenantId,
						contract_status: contractStatus
					}
				},
				$router: {
					push
				}
			}
		});

		await wrapper.vm.$nextTick();

		wrapper.find('.consultant-btn button').trigger('click');

		await flushPromises();

		expect(push).toHaveBeenCalledWith({
			name: 'invoice',
			params: {
				id: tenantId,
				booking: booking,
				contract_status: contractStatus
			}
		});
	});

	test('Should go to previous page after submit succeed', async () => {
		const axiosPost = jest.fn().mockResolvedValue({
			data: {
				status: true,
				data: {}
			}
		});

		global.axios = {
			post: axiosPost
		};

		const go = jest.fn();
		const wrapper = mountComponent({
			stubs: { ConsultantButton },
			mocks: {
				$route: {
					params: {
						invoiceId: '1'
					},
					query: {}
				},
				$router: {
					go
				}
			}
		});

		await wrapper.vm.$nextTick();

		wrapper.find('.consultant-btn button').trigger('click');

		await flushPromises();

		expect(go).toHaveBeenCalledWith(-1);
	});

	test('Should show alert and execute bugsnag', async () => {
		const openAlert = jest.fn();
		const bugsnagNotify = jest.fn();

		global.bugsnagClient = { notify: bugsnagNotify };

		global.axios = {
			post: jest.fn().mockRejectedValue(new Error())
		};

		const wrapper = mountComponent({
			stubs: { ConsultantButton },
			mocks: {
				$route: {
					params: {
						invoiceId: '1'
					},
					query: {}
				}
			}
		});

		EventBus.$on('openAlert', openAlert);

		await wrapper.vm.$nextTick();

		const submitButton = wrapper.find('.consultant-btn button');
		submitButton.trigger('click');

		await flushPromises();

		expect(bugsnagNotify).toHaveBeenCalledWith(expect.any(Error));
		expect(openAlert).toHaveBeenCalledWith('Terjadi galat. Silakan coba lagi.');
	});
});
