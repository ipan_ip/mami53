import { createLocalVue, shallowMount } from '@vue/test-utils';
import ContractFilter from 'Consultant/pages/contract/ContractFilter.vue';
import Vuex from 'vuex';
import contractStoreModule from 'Consultant/store/modules/contract';

const localVue = createLocalVue();
localVue.use(Vuex);

const store = new Vuex.Store({
	modules: {
		contract: contractStoreModule
	}
});

describe('Consultant/pages/contract/ContractFilter.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return shallowMount(ContractFilter, {
				localVue,
				store,
				mocks: {
					$route: {
						query: {}
					}
				},
				...options
			});
		};
	});

	test('Should render correctly', async () => {
		const tree = shallowMount(ContractFilter, {
			localVue,
			store,
			mocks: {
				$route: {
					query: {}
				}
			}
		});
		expect(tree.element).toMatchSnapshot();
	});

	test('selecting a from date', async () => {
		const mocks = {
			$dayjs: jest.fn(() => {
				return {
					format: jest.fn(() => '2020-01-01')
				};
			}),
			$route: {
				query: {}
			},
			showCalendar: jest.fn()
		};

		const stubs = {
			openUntilDatepicker: {
				render: () => {},
				methods: {
					showCalendar: jest.fn()
				}
			}
		};

		const wrapper = mountComponent({
			mocks,
			stubs
		});

		wrapper.vm.selectDate('01 Januari 2020', 'sejak');

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.selectedDateFrom.label).toBe('01 Januari 2020');
		expect(wrapper.vm.selectedDateFrom.value).toBe('2020-01-01');
	});

	test('selecting a to date', async () => {
		const wrapper = mountComponent({
			mocks: {
				$dayjs: jest.fn(() => {
					return {
						format: jest.fn(() => '2020-12-30')
					};
				}),
				$route: {
					query: {}
				}
			}
		});

		wrapper.vm.selectDate('31 December 2020', 'hingga');

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.selectedDateTo.label).toBe('31 December 2020');
		expect(wrapper.vm.selectedDateTo.value).toBe('2020-12-30');
	});

	test('should assign a value to xCheckedFilter when checkbox has been checked', async () => {
		const wrapper = mountComponent();
		const filterValue = 'whatever';
		wrapper.setData({
			filterList: [
				{
					id: 1,
					groupName: 'test',
					groupValue: 'test',
					selectedItems: [],
					items: [
						{
							id: '1',
							filter: 'Test',
							value: filterValue
						}
					]
				}
			]
		});

		await wrapper.vm.$nextTick();

		const checkbox = wrapper.find(`input[type=checkbox]`);
		checkbox.trigger('click');

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.filterList[0].selectedItems).toEqual(
			expect.arrayContaining([filterValue])
		);
	});
});
