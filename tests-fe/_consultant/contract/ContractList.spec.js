import { shallowMount, createLocalVue } from '@vue/test-utils';
import ContractList from 'Consultant/pages/contract/ContractList.vue';
import ConsultantButton from 'Consultant/components/ConsultantButton.vue';

import contractModule from 'Consultant/store/modules/contract';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import { flushPromises } from 'tests-fe/utils/promises';
import { cloneDeep } from 'lodash';
import contractListData from './data/contractList.json';

import Vuex from 'vuex';

const mockLogger = jest.fn();

jest.mock('Consultant/helpers/tracker', () => ({
	__esModule: true,
	default: jest.fn(() => ({ logger: mockLogger })),
	TRACKERS: {}
}));

global.bugsnagClient = { notify: jest.fn() };

const mockStoreObject = {
	modules: {
		contract: contractModule
	},
	state: {
		authData: {}
	},
	getters: {
		isAdminConsultant() {
			return false;
		}
	}
};

const mockVueRouter = {
	$route: {
		query: {}
	}
};

const $dayjs = jest.fn(d => ({
	locale: jest.fn(() => ({ format: jest.fn(() => d) })),
	format: jest.fn(() => (Array.isArray(d) ? d.join('-') : d))
}));

const installVuePlugin = () => {
	const localVue = createLocalVue();
	mockVLazy(localVue);
	localVue.use(Vuex);
	return localVue;
};

const mountComponent = (options = {}) => {
	return shallowMount(ContractList, {
		localVue: installVuePlugin(),
		store: new Vuex.Store(cloneDeep(mockStoreObject)),
		mocks: {
			...mockVueRouter,
			$dayjs
		},
		...options
	});
};

describe('ContractList.vue', () => {
	const RealDate = Date;

	beforeEach(() => {
		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: {
					status: true,
					data: contractListData
				}
			})
		};
	});

	afterEach(() => {
		global.Date = RealDate;
		mockLogger.mockClear();
	});

	it('Should render component and match snapshot', async () => {
		const wrapper = mountComponent();

		await flushPromises();

		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should render "Tambah Kontrak Baru" button and move to create contract page', async () => {
		const push = jest.fn();
		const wrapper = mountComponent({
			store: new Vuex.Store(
				cloneDeep({
					...mockStoreObject,
					getters: {
						isAdminConsultant() {
							return true;
						}
					}
				})
			),
			mocks: {
				...mockVueRouter,
				$dayjs,
				$router: {
					push
				}
			},
			stubs: {
				ConsultantButton
			}
		});

		await flushPromises();

		expect(wrapper.find('.add-contract-button').exists()).toBe(true);

		wrapper.find('.consultant-btn button').trigger('click');

		expect(push).toHaveBeenCalledWith({ name: 'contract.create' });
	});

	it('Should execute tracker with correct data', async () => {
		const consultantId = 'test id';
		mountComponent({
			localVue: installVuePlugin(),
			store: new Vuex.Store(
				cloneDeep({
					...mockStoreObject,
					state: {
						authData: {
							consultant: { id: consultantId }
						}
					}
				})
			)
		});

		await flushPromises();

		expect(mockLogger).toHaveBeenCalledWith([
			'Homepage Kelola Kontrak',
			{
				consultant_id: consultantId
			}
		]);
	});

	it('Should hit API with correct query', async () => {
		const axisGet = jest.fn().mockResolvedValue({
			data: {
				status: true,
				data: []
			}
		});

		global.axios = {
			get: axisGet
		};

		mountComponent({
			mocks: {
				$dayjs,
				$route: {
					query: {
						page: 2,
						contract_status: 'active,cancelled',
						paid_status: 'paid',
						created_by: 'consultant,tenant',
						search: 'test search',
						promotion_type: 'booking'
					}
				}
			}
		});

		await flushPromises();

		expect(axisGet).toHaveBeenCalledWith(
			expect.stringMatching(
				/\/api\/contract\?offset=10&limit=10&sort_by=updated_at&date_from=(\w|\W)+&date_to=(\w|\W)+&contract_status\[\]=active&contract_status\[\]=cancelled&paid_status\[\]=paid&created_by\[\]=consultant&created_by\[\]=tenant&search=test search&is_booking=true/
			)
		);
	});

	it('Should hit API with current date correctly and set date to vuex', async () => {
		const year = 2020;
		const month = 11;
		const expectedFirstDay = `${year}-${month}-1`;
		const expectedLastDay = `${year}-${month + 2}-0`;
		const axisGet = jest.fn().mockResolvedValue({
			data: {
				status: true,
				data: []
			}
		});

		global.Date = jest.fn().mockImplementation((...args) => {
			if (args.length) {
				return args;
			} else {
				return {
					getFullYear: jest.fn().mockReturnValue(year),
					getMonth: jest.fn().mockReturnValue(month)
				};
			}
		});

		global.axios = {
			get: axisGet
		};

		const wrapper = mountComponent({
			mocks: {
				$dayjs,
				$route: {
					query: {}
				}
			}
		});

		await flushPromises();

		expect(wrapper.vm.$store.state.contract.selectedDateFrom).toEqual(
			expect.objectContaining({ value: expectedFirstDay })
		);
		expect(wrapper.vm.$store.state.contract.selectedDateTo).toEqual(
			expect.objectContaining({ value: expectedLastDay })
		);
		expect(axisGet).toHaveBeenCalledWith(
			expect.stringContaining(
				`date_from=${expectedFirstDay}&date_to=${expectedLastDay}`
			)
		);
	});

	it('Should hit API with date from current query and set date to vuex', async () => {
		const dateFrom = 'test query date from';
		const dateTo = 'test query date to';

		const axisGet = jest.fn().mockResolvedValue({
			data: {
				status: true,
				data: []
			}
		});

		global.Date = jest.fn().mockImplementation(date => {
			return [date];
		});

		global.axios = {
			get: axisGet
		};

		const wrapper = mountComponent({
			mocks: {
				$dayjs,
				$route: {
					query: {
						date_from: dateFrom,
						date_to: dateTo
					}
				}
			}
		});

		await flushPromises();

		expect(wrapper.vm.$store.state.contract.selectedDateFrom).toEqual(
			expect.objectContaining({ value: dateFrom })
		);
		expect(wrapper.vm.$store.state.contract.selectedDateTo).toEqual(
			expect.objectContaining({ value: dateTo })
		);
		expect(axisGet).toHaveBeenCalledWith(
			expect.stringContaining(`date_from=${dateFrom}&date_to=${dateTo}`)
		);
	});

	it('Should disable search and set total data on the pagination state', async () => {
		const totalData = 0;
		const axisGet = jest.fn().mockResolvedValue({
			data: {
				status: true,
				data: [],
				total: totalData
			}
		});

		global.axios = {
			get: axisGet
		};

		const wrapper = mountComponent();

		await flushPromises();

		expect(wrapper.vm.disableSearch).toBe(true);
		expect(wrapper.vm.pagination.totalData).toBe(totalData);
	});

	it('Should execute bugsnag', async () => {
		const axisGet = jest.fn().mockRejectedValue(new Error());
		const bugsnagClient = jest.fn();

		global.bugsnagClient = { notify: bugsnagClient };
		global.axios = {
			get: axisGet
		};

		mountComponent();

		await flushPromises();

		expect(bugsnagClient).toHaveBeenCalled();
	});

	it('should move to invoice detail with correct params', async () => {
		const push = jest.fn();
		const contractId = 'test contract id';
		const fromBooking = true;
		const contractStatus = 'test contract status';

		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: {
					status: true,
					data: [
						{
							...contractListData[0],
							id: contractId,
							from_booking: fromBooking,
							contract_status: contractStatus
						}
					]
				}
			})
		};

		const wrapper = mountComponent({
			mocks: {
				...mockVueRouter,
				$dayjs,
				$router: {
					push
				}
			}
		});

		await flushPromises();

		wrapper
			.findAll('.contract-card-container')
			.at(0)
			.trigger('click');

		expect(push).toHaveBeenCalledWith({
			name: 'invoice',
			params: {
				id: contractId,
				booking: fromBooking,
				contract_status: contractStatus
			}
		});
	});

	describe('Test pagination', () => {
		it('should move to page 2', async () => {
			const currentRouteName = 'current route name';
			const push = jest.fn(() => ({ catch: jest.fn() }));
			const page = 2;

			const wrapper = mountComponent({
				mocks: {
					$dayjs,
					$route: {
						name: currentRouteName,
						query: {}
					},
					$router: {
						push
					}
				}
			});

			await flushPromises();

			wrapper.vm.changePagination(page);

			expect(push).toHaveBeenCalledWith({
				name: currentRouteName,
				query: expect.objectContaining({ page })
			});
		});

		it('should change route with search query', async () => {
			const currentRouteName = 'current route name';
			const push = jest.fn(() => ({ catch: jest.fn() }));
			const searchText = 'test search text';

			const wrapper = mountComponent({
				mocks: {
					$dayjs,
					$route: {
						name: currentRouteName,
						query: {}
					},
					$router: {
						push
					}
				}
			});

			await flushPromises();

			wrapper.vm.handleSearch(searchText);

			await wrapper.vm.$nextTick();

			expect(wrapper.vm.pagination.search).toBe(searchText);
			expect(push).toHaveBeenCalledWith({
				name: currentRouteName,
				query: expect.objectContaining({ page: 1, search: searchText })
			});
		});
	});

	describe('Test terminate and cancel contract', () => {
		beforeEach(() => {
			global.axios = {
				get: jest.fn().mockResolvedValue({
					data: {
						status: true,
						data: [
							{
								...contractListData[0],
								from_booking: 1
							}
						]
					}
				})
			};
		});

		it('should render cancel and terminate button and match snapshot', async () => {
			const wrapper = mountComponent({
				localVue: installVuePlugin(),
				store: new Vuex.Store(
					cloneDeep({
						...mockStoreObject,
						getters: {
							isAdminConsultant() {
								return true;
							}
						}
					})
				)
			});

			await flushPromises();

			expect(wrapper.find('#cancel-terminate').html()).toMatchSnapshot();
		});

		it('should show cancel confirmation and set selected contract id', async () => {
			const id = 'test cancel contract id';
			global.axios = {
				get: jest.fn().mockResolvedValue({
					data: {
						status: true,
						data: [
							{
								...contractListData[0],
								id,
								from_booking: 1
							}
						]
					}
				})
			};

			const wrapper = mountComponent({
				localVue: installVuePlugin(),
				store: new Vuex.Store(
					cloneDeep({
						...mockStoreObject,
						getters: {
							isAdminConsultant() {
								return true;
							}
						}
					})
				),
				stubs: {
					ConsultantButton
				}
			});

			await flushPromises();

			const buttons = wrapper.findAll('.consultant-btn button');
			buttons.at(1).trigger('click');

			await wrapper.vm.$nextTick();

			expect(wrapper.vm.snackbarCancel).toBe(true);
			expect(wrapper.vm.selectedContract).toBe(id);
		});

		it('should show terminate confirmation and set selected contract id', async () => {
			const id = 'test terminate contract id';
			global.axios = {
				get: jest.fn().mockResolvedValue({
					data: {
						status: true,
						data: [
							{
								...contractListData[0],
								id
							}
						]
					}
				})
			};

			const wrapper = mountComponent({
				localVue: installVuePlugin(),
				store: new Vuex.Store(
					cloneDeep({
						...mockStoreObject,
						getters: {
							isAdminConsultant() {
								return true;
							}
						}
					})
				),
				stubs: {
					ConsultantButton
				}
			});

			await flushPromises();

			const buttons = wrapper.findAll('.consultant-btn button');
			buttons.at(2).trigger('click');

			await wrapper.vm.$nextTick();

			expect(wrapper.vm.snackbarTerminate).toBe(true);
			expect(wrapper.vm.selectedContract).toBe(id);
		});

		it('Cancel contract with correct id and move to current url', async () => {
			const id = 'test cancel contract id';
			const axiosPost = jest.fn().mockResolvedValue();
			const currentRouteName = 'current route name';
			const push = jest.fn(() => ({ catch: jest.fn() }));

			global.axios = {
				get: jest.fn().mockResolvedValue({
					data: {
						status: true,
						data: [
							{
								...contractListData[0],
								id,
								from_booking: 1
							}
						]
					}
				}),
				post: axiosPost
			};

			const wrapper = mountComponent({
				localVue: installVuePlugin(),
				store: new Vuex.Store(
					cloneDeep({
						...mockStoreObject,
						getters: {
							isAdminConsultant() {
								return true;
							}
						}
					})
				),
				mocks: {
					$dayjs,
					$route: {
						name: currentRouteName,
						query: {}
					},
					$router: {
						go: jest.fn(),
						push
					}
				},
				stubs: {
					ConsultantButton
				}
			});

			await flushPromises();

			const buttons = wrapper.findAll('.consultant-btn button');
			buttons.at(1).trigger('click');

			await wrapper.vm.$nextTick();

			buttons.at(4).trigger('click');

			expect(axiosPost).toHaveBeenCalledWith(`/api/contract/cancel/${id}`);

			await flushPromises();

			expect(push).toHaveBeenCalledWith({ name: currentRouteName, query: {} });
		});

		it('Terminate contract with correct id and move to current url', async () => {
			const id = 'test terminate contract id';
			const axiosPost = jest.fn().mockResolvedValue();
			const currentRouteName = 'test current route name';
			const push = jest.fn(() => ({ catch: jest.fn() }));

			global.axios = {
				get: jest.fn().mockResolvedValue({
					data: {
						status: true,
						data: [
							{
								...contractListData[0],
								id
							}
						]
					}
				}),
				post: axiosPost
			};

			const wrapper = mountComponent({
				localVue: installVuePlugin(),
				store: new Vuex.Store(
					cloneDeep({
						...mockStoreObject,
						getters: {
							isAdminConsultant() {
								return true;
							}
						}
					})
				),
				mocks: {
					$dayjs,
					$route: {
						name: currentRouteName,
						query: {}
					},
					$router: {
						go: jest.fn(),
						push
					}
				},
				stubs: {
					ConsultantButton
				}
			});

			await flushPromises();

			const buttons = wrapper.findAll('.consultant-btn button');
			buttons.at(2).trigger('click');

			await wrapper.vm.$nextTick();

			buttons.at(6).trigger('click');

			expect(axiosPost).toHaveBeenCalledWith(`/api/contract/terminate/${id}`);

			await flushPromises();

			expect(push).toHaveBeenCalledWith({ name: currentRouteName, query: {} });
		});
	});
});
