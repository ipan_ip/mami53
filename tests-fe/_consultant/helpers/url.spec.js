import {
	makeQueryParam,
	createMultipleParam,
	mergeParams,
	transformToArray
} from 'Consultant/helpers/url';

describe('Test URL helper', () => {
	describe('Test makeQueryParam', () => {
		it('should return query param correctly', () => {
			const obj = { param1: 'test1', param2: 'test2' };
			const expected = 'param1=test1&param2=test2';

			expect(makeQueryParam(obj)).toBe(expected);
		});

		it('should return empty string when using argument non object', () => {
			const obj = [];
			const expected = '';

			expect(makeQueryParam(obj)).toBe(expected);
		});

		it('should return multiple query', () => {
			const obj = { param: ['test1', 'test2'] };
			const expected = 'param[]=test1&param[]=test2';

			expect(makeQueryParam(obj)).toBe(expected);
		});
	});

	describe('Test createMultipleParam', () => {
		it('should return multiple param correctly', () => {
			const paramValue = ['test', 'test2'];
			const paramKey = 'filter';
			const expected = 'filter[]=test&filter[]=test2';

			expect(createMultipleParam(paramValue, paramKey)).toBe(expected);
		});
	});

	describe('Test mergeParams', () => {
		it('should merge query correctly', () => {
			const param1 = 'test=query1&test2=whatever';
			const param2 = 'limit=10&offset=20';
			const param3 = '';
			const expected = param1 + '&' + param2;

			expect(mergeParams(param1, param2, param3)).toBe(expected);
		});
	});

	describe('Test transformToArray', () => {
		it('should transform string to array correctly', () => {
			const str = 'test1,test2,test3';
			const expected = ['test1', 'test2', 'test3'];

			expect(transformToArray(str)).toEqual(expected);
		});

		it('should return empty array when there is no string', () => {
			const str = '';
			const expected = [];

			expect(transformToArray(str)).toEqual(expected);
		});
	});
});
