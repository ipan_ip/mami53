import deleteOptionalField from 'Consultant/helpers/deleteOptionalField.js';

describe('Test Helper deleteOptionalField()', () => {
	describe('No optional keys', () => {
		it('Should get object as the original', () => {
			const obj = { foo: 'hello world', bar: 123 };
			expect(JSON.stringify(deleteOptionalField(obj))).toBe(
				JSON.stringify(obj)
			);
		});

		it('should get all fields even if the value is null or an empty string', () => {
			const obj = { foo: '', bar: null };
			const updatedObj = deleteOptionalField(obj);
			expect(updatedObj.foo).toBe(obj.foo);
			expect(updatedObj.bar).toBe(obj.bar);
		});
	});

	describe('With optional keys', () => {
		it('Should get optional field if it has filled', () => {
			const obj = { foo: 'hello world', bar: 123 };
			expect(JSON.stringify(deleteOptionalField(obj, ['foo']))).toBe(
				JSON.stringify(obj)
			);
		});

		it('should get fileds except optional if it empty or null', () => {
			const obj = { foo: 'bar', bar: null };
			const expected = { foo: 'bar' };
			const updatedObj = deleteOptionalField(obj, ['bar']);
			expect(JSON.stringify(updatedObj)).toBe(JSON.stringify(expected));
		});
	});
});
