import Roman from 'Consultant/helpers/numberToRomanNumeral';

describe('Helper numberToRomanNumeral', () => {
	it('Should transform number correctly', () => {
		const templateData = {
			XXIX: 29,
			XLI: 41,
			LXVII: 67,
			XCIX: 99,
			CCXIX: 219,
			DCLXVI: 666,
			MMMMCXCIX: 4199
		};

		for (const [roman, num] of Object.entries(templateData)) {
			expect(Roman(num)).toBe(roman);
		}
	});
});
