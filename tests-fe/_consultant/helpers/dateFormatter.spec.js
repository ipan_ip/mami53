import dateFormatter from 'Consultant/helpers/dateFormatter';

describe('Test Helper: dateFormatter', () => {
	it('Should format date correctly', () => {
		const plainDate = '2020-11-17 14:13:22';
		const expected = 'Nov 17, 2020';

		expect(dateFormatter(plainDate)).toBe(expected);
	});
});
