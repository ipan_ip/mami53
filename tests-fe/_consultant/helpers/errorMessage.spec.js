import { getErrorMessage } from 'Consultant/helpers/errorMessage';

describe('Helper errorMessage', () => {
	it('Should get message and collect it into array', () => {
		const emailErrorMessages = ['test error message on email field'];
		const whateverErrorMessages = ['test error', 'whatever error message'];
		const response = {
			status: false,
			message: {
				email: emailErrorMessages,
				whatever: whateverErrorMessages
			},
			meta: {}
		};
		const expected = [emailErrorMessages[0], whateverErrorMessages[0]];

		expect(getErrorMessage(response)).toEqual(expected);
	});

	it('Should return message with length based on second param', () => {
		const minLength = 2;
		const response = {
			status: false,
			message: {
				email: ['test'],
				username: ['test'],
				whatever: ['test']
			},
			meta: {}
		};

		expect(getErrorMessage(response, minLength)).toHaveLength(minLength);
	});

	it('Should return message from meta', () => {
		const metaMessage = 'test meta message';
		const response = {
			status: false,
			meta: {
				message: metaMessage
			}
		};

		expect(getErrorMessage(response)).toEqual([metaMessage]);
	});

	it('Should return default message', () => {
		const defaultMessage = 'Terjadi galat, silahkan coba lagi';
		const response = {
			status: false,
			meta: {}
		};

		expect(getErrorMessage(response)).toEqual([defaultMessage]);
	});
});
