import CreateTracker, { TRACKERS } from 'Consultant/helpers/tracker';

window.tracker = function() {};

describe('Test class tracker', () => {
	it('Should instance class properly', () => {
		const tracker = new CreateTracker(TRACKERS.MOE, { name: 'test' });

		expect('moe' in tracker).toBe(true);
		expect('restoreGlobalAuth' in tracker).toBe(true);
	});

	it('Should set global auth variables with user passed', () => {
		window.authCheck = false;
		window.authData = {};
		const user = { id: 1, name: 'test' };
		// eslint-disable-next-line no-unused-vars
		const tracker = new CreateTracker(TRACKERS.MOE, user);

		expect(window.authData).toEqual({ all: user });
	});

	it('Should restore global auth variables when call restoreGlobalAuth', () => {
		const oldAuthData = { name: 'old name' };
		window.authCheck = false;
		window.authData = oldAuthData;
		const user = { id: 1, name: 'test' };
		const tracker = new CreateTracker(TRACKERS.MOE, user);

		expect(window.authData).toEqual({ all: user });

		tracker.restoreGlobalAuth();

		expect(window.authData).toEqual(authData);
	});

	it('Should install tracker type correctly', () => {
		const loggerTracker = jest.fn();
		const tracker = jest.fn().mockImplementation((type, param, callback) => {
			const t = {
				logger: function() {
					loggerTracker(param, callback);
				}
			};
			t[type]();
		});
		window.tracker = tracker;

		const user = { id: 1, name: 'test' };
		const trackerInstance = new CreateTracker(TRACKERS.LOGGER, user);
		const params = { test: 'param' };
		trackerInstance.logger(params, () => {});

		expect(tracker).toHaveBeenCalledWith(
			'logger',
			params,
			expect.any(Function)
		);
		expect(loggerTracker).toHaveBeenCalledWith(params, expect.any(Function));
	});
});
