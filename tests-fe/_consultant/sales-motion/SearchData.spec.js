import { shallowMount } from '@vue/test-utils';
import SearchData from 'Consultant/pages/sales-motion/SearchData';

describe('Consultant/pages/sales-motion/SearchData', () => {
	let mountComponent;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return shallowMount(SearchData, {
				stubs: {
					SearchWrapper: {
						template: `<div><slot></slot></div>`
					}
				},
				mocks: {
					$route: { query: {} }
				},
				...options
			});
		};
	});

	it('Should render component correctly', () => {
		const wrapper = mountComponent();

		expect(wrapper.find('.sales-motion-search-data').exists()).toBe(true);
	});
});
