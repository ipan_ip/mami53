import { shallowMount } from '@vue/test-utils';
import DetailReport from 'Consultant/pages/sales-motion/DetailReport';

describe('Consultant/pages/sales-motion/DetailReport', () => {
	let mountComponent;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return shallowMount(DetailReport, {
				...options
			});
		};
	});

	it('Should render component correctly', () => {
		const wrapper = mountComponent();

		expect(wrapper.classes('sales-motion-report-detail')).toBe(true);
	});
});
