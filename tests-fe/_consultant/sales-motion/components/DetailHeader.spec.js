import { createLocalVue, shallowMount } from '@vue/test-utils';
import { flushPromises } from 'tests-fe/utils/promises';

import MockData from '../data/sales-motion-detail.json';
import DetailHeader from 'Consultant/pages/sales-motion/components/DetailHeader';

const axios = {
	get: jest.fn().mockResolvedValue({
		data: MockData
	})
};

const bugsnagClient = {
	notify: jest.fn()
};

const mocks = {
	$dayjs: jest.fn(() => {
		return {
			format: jest.fn()
		};
	}),
	$router: { push: jest.fn() },
	$route: {
		params: {
			id: 1
		}
	}
};

const propsData = {
	data: MockData.sales_motion
};

const stubs = {
	Button: {
		template: `<button class="add-report" @click="$emit('click')"></button>`
	}
};

describe('Consultant/pages/sales-motion/components/DetailHeader', () => {
	let mountComponent = Function;

	beforeEach(() => {
		const localVue = createLocalVue();

		console.error = jest.fn();
		global.alert = jest.fn();

		global.axios = axios;
		global.bugsnagClient = bugsnagClient;

		mountComponent = (options = {}) => {
			return shallowMount(DetailHeader, {
				localVue,
				propsData,
				mocks,
				stubs,
				...options
			});
		};
	});

	test('should render correctly', async () => {
		const tree = shallowMount(DetailHeader, {
			propsData,
			mocks
		});

		expect(tree.element).toMatchSnapshot();
	});

	test('go to add report page', async () => {
		const wrapper = mountComponent();
		await flushPromises();

		wrapper.find('.add-report').trigger('click');

		expect(wrapper.emitted('addReport')).toBeTruthy();
	});

});
