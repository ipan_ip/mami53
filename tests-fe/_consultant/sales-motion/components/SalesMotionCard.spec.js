import { mount } from '@vue/test-utils';
import SalesMotionCard from 'Consultant/pages/sales-motion/components/SalesMotionCard';
import salesMotionItem from '../data/salesMotionListItem.json';
import Chip from 'Consultant/components/Chip';

describe('Consultant/pages/sales-motion/SalesMotionCard', () => {
	let mountComponent;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return mount(SalesMotionCard, {
				mocks: {
					$dayjs: jest.fn(d => ({ format: jest.fn().mockReturnValue(d) }))
				},
				stubs: {
					'router-link': {
						template: `<a><slot></slot></a>`
					}
				},
				...options
			});
		};
	});

	it('Should render component and match snapshot', async () => {
		const wrapper = mountComponent({
			propsData: {
				item: salesMotionItem
			}
		});

		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should render chip correctly status based', async () => {
		const wrapper = mountComponent({
			propsData: {
				item: { ...salesMotionItem, is_active: false }
			}
		});

		const status = wrapper.findComponent(Chip);

		expect(status.html()).toMatchSnapshot();
	});
});
