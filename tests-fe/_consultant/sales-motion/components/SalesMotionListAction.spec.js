import { mount } from '@vue/test-utils';
import SalesMotionListAction from 'Consultant/pages/sales-motion/components/SalesMotionListAction';

describe('SalesMotionListAction.vue', () => {
	let mountComponent;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return mount(SalesMotionListAction, {
				mocks: {
					$route: { query: {} }
				},
				...options
			});
		};
	});

	it('Should render component with "sort" type', async () => {
		const wrapper = mountComponent();

		wrapper.vm.open('sort');

		await wrapper.vm.$nextTick();

		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should render component with "filter" type', async () => {
		const wrapper = mountComponent();

		wrapper.vm.open('filter');

		await wrapper.vm.$nextTick();

		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should change route with sort selected', async () => {
		const push = jest.fn();
		const wrapper = mountComponent({
			stubs: {
				'ct-button': {
					template: `<button class="actionBtn" @click="$emit('click')"></button>`
				}
			},
			mocks: {
				$route: { name: 'test', query: {} },
				$router: { push }
			}
		});

		const indexSortSelected = 1;
		const sortItems = wrapper.vm.sortItems;
		const selectedSortValue = sortItems[indexSortSelected].value;

		wrapper.vm.open('sort');

		wrapper.setData({ selectedSort: selectedSortValue });

		await wrapper.vm.$nextTick();

		wrapper
			.findAll('.actionBtn')
			.at(1)
			.trigger('click');

		expect(push).toHaveBeenCalledWith(
			expect.objectContaining({
				query: expect.objectContaining({
					page: 1,
					sort_by: selectedSortValue
				})
			})
		);
	});

	it('Should change route with filter selected', async () => {
		const push = jest.fn();
		const wrapper = mountComponent({
			stubs: {
				'ct-button': {
					template: `<button class="actionBtn" @click="$emit('click')"></button>`
				}
			},
			mocks: {
				$route: { name: 'test', query: {} },
				$router: { push }
			}
		});

		wrapper.vm.open('filter');

		await wrapper.vm.$nextTick();

		const indexFilterSelected = 1;
		const filterSelected = wrapper.vm.filterItems[indexFilterSelected];

		wrapper
			.findAll('.ct-checkbox__action')
			.at(indexFilterSelected)
			.trigger('click');

		await wrapper.vm.$nextTick();

		wrapper
			.findAll('.actionBtn')
			.at(1)
			.trigger('click');

		expect(push).toHaveBeenCalledWith(
			expect.objectContaining({
				query: expect.objectContaining({
					page: 1,
					filters: filterSelected.value
				})
			})
		);
	});

	it('Should set selected sort with value from query', async () => {
		const sort = {
			label: 'Tanggal Berakhir',
			value: 'end_date'
		};

		const wrapper = mountComponent({
			mocks: {
				$route: {
					query: {
						sort_by: sort.value
					}
				}
			}
		});

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.selectedSort).toBe(sort.value);
	});

	it('Should set selected filter with value from query', async () => {
		const expectedFilter = ['campaign', 'product'];

		const wrapper = mountComponent({
			mocks: {
				$route: {
					query: {
						filters: expectedFilter.join(',')
					}
				}
			}
		});

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.selectedFilter).toEqual(expectedFilter);
	});
});
