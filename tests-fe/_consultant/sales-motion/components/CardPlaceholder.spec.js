import { shallowMount } from '@vue/test-utils';
import CardPlaceholder from 'Consultant/pages/sales-motion/components/CardPlaceholder';

describe('CardPlaceholder.vue', () => {
	it('Should render component and match snapshot', async () => {
		const wrapper = shallowMount(CardPlaceholder);

		expect(wrapper.html()).toMatchSnapshot();
	});
});
