import { shallowMount } from '@vue/test-utils';
import AddReport from 'Consultant/pages/sales-motion/AddReport';

describe('Consultant/pages/sales-motion/AddReport', () => {
	let mountComponent;

	const mocks = {
		$dayjs: jest.fn(() => {
			return {
				format: jest.fn()
			};
		}),
		$route: {
			params: {
				id: 1
			}
		}
	};

	global.alert = jest.fn();

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return shallowMount(AddReport, {
				mocks,
				...options
			});
		};
	});

	it('Should render component correctly', () => {
		const wrapper = mountComponent();

		expect(wrapper.classes('sales-motion-add-report')).toBe(true);
	});
});
