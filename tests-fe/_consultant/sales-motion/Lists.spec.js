import { shallowMount, createLocalVue } from '@vue/test-utils';
import Lists from 'Consultant/pages/sales-motion/Lists';
import mockList from './data/salesMotionList.json';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import { flushPromises } from 'tests-fe/utils/promises';
import EventBus from 'Consultant/event-bus';

import CtTabs from 'Consultant/components/CtTabs';
import EmptyState from 'Consultant/components/ConsultantEmptyState';

const localVue = createLocalVue();
mockVLazy(localVue);

global.axios = {
	get: jest.fn().mockResolvedValue({ data: mockList })
};

global.tracker = jest.fn();

describe('Consultant/pages/sales-motion/Lists', () => {
	let mountComponent;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return shallowMount(Lists, {
				localVue,
				mocks: {
					$route: {
						query: {}
					},
					$store: {
						state: {
							authData: { id: 'test' }
						}
					}
				},
				...options
			});
		};
	});

	it('Should render component correctly', async () => {
		const wrapper = mountComponent();

		await flushPromises();

		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should push to new query with active tab', async () => {
		const push = jest.fn();
		const wrapper = mountComponent({
			stubs: {
				CtTabs
			},
			mocks: {
				$route: {
					name: 'test',
					query: {}
				},
				$router: {
					push
				},
				$store: {
					state: {
						authData: { id: 'test' }
					}
				}
			}
		});

		wrapper
			.findAll('.ct-tab')
			.at(1)
			.trigger('click');

		expect(push).toHaveBeenCalledWith(
			expect.objectContaining({
				query: expect.objectContaining({
					page: 1,
					active: 0
				})
			})
		);
	});

	it('Should hit API with deactive query', async () => {
		const axiosSpy = jest.spyOn(global.axios, 'get');

		mountComponent({
			mocks: {
				$route: {
					name: 'test',
					query: {
						active: false
					}
				},
				$store: {
					state: {
						authData: { id: 'test' }
					}
				}
			}
		});

		expect(axiosSpy).toHaveBeenCalledWith(
			expect.stringContaining('is_active=false')
		);
	});

	it('Should hit API with type query', async () => {
		const axiosSpy = jest.spyOn(global.axios, 'get');

		mountComponent({
			mocks: {
				$route: {
					name: 'test',
					query: {
						filters: 'test1,test2'
					}
				},
				$store: {
					state: {
						authData: { id: 'test' }
					}
				}
			}
		});

		expect(axiosSpy).toHaveBeenCalledWith(
			expect.stringContaining('type[]=test1&type[]=test2')
		);
	});

	it('Should show empty state when have not sales motion data', async () => {
		global.axios = {
			get: jest
				.fn()
				.mockResolvedValue({ data: { data: [] }, total: 0, status: true })
		};

		const wrapper = mountComponent();

		await flushPromises();

		expect(wrapper.findComponent(EmptyState).exists()).toBe(true);
	});

	it('Should set active sort from bus event', async () => {
		const wrapper = mountComponent();

		const data = { type: 'sort', value: 'test_sort' };

		EventBus.$emit('SET_ACTION_LABEL', data);

		expect(wrapper.vm.activeSort).toBe(data.value);
	});

	it('Should set active filter length from bus event', async () => {
		const wrapper = mountComponent();

		const data = { type: 'filter', value: 10 };

		EventBus.$emit('SET_ACTION_LABEL', data);

		expect(wrapper.vm.activeFilterLength).toBe(data.value);
	});
});
