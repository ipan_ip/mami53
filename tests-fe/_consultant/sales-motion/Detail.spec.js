import { createLocalVue, shallowMount } from '@vue/test-utils';
import { flushPromises } from 'tests-fe/utils/promises';

import MockData from './data/sales-motion-detail.json';
import Detail from 'Consultant/pages/sales-motion/Detail';
import DetailHeader from 'Consultant/pages/sales-motion/components/DetailHeader';

const axios = {
	get: jest.fn().mockResolvedValue({
		data: MockData
	})
};

const bugsnagClient = {
	notify: jest.fn()
};

const mocks = {
	$router: { push: jest.fn() },
	$route: {
		params: {
			id: 1
		}
	}
};

describe('Consultant/pages/sales-motion/Detail', () => {
	let mountComponent = Function;

	test('should render correctly', async () => {
		const localVue = createLocalVue();

		const tree = shallowMount(Detail, {
			mocks,
			axios,
			localVue
		});

		await flushPromises();

		expect(tree.element).toMatchSnapshot();
	});

	beforeEach(() => {
		const localVue = createLocalVue();

		console.error = jest.fn();
		global.alert = jest.fn();

		global.axios = axios;
		global.bugsnagClient = bugsnagClient;

		mountComponent = (options = {}) => {
			return shallowMount(Detail, {
				localVue,
				mocks,
				axios,
				...options
			});
		};
	});

	test('go to add report page', async () => {
		const wrapper = mountComponent();
		await flushPromises();

		wrapper.findComponent(DetailHeader).vm.$emit('addReport');

		expect(wrapper.vm.$router.push).toBeCalledWith({
			name: 'sm.report.add',
			params: { id: 1 }
		});
	});

	test('show the correct content when clicking on change tab', async () => {
		const wrapper = mountComponent();

		wrapper.vm.changeTabContent({ label: 'Laporan' });

		await wrapper.vm.$nextTick();

		expect(wrapper.findAll('.tab-content').at(1).isVisible()).toBeTruthy();
	});

	test('get the correct response from API', async () => {
		const wrapper = mountComponent();
		await flushPromises();

		expect(wrapper.vm.salesMotionData.id).toBe(mocks.$route.params.id);
	});

	test('display alert if get bad API response', async () => {
		const alert = jest.spyOn(window, 'alert').mockImplementation(() => {});

		global.axios = {
			get: jest.fn().mockRejectedValue(new Error('test error'))
		};

		mountComponent();

		await flushPromises();

		expect(alert).toBeCalled();
		alert.mockRestore();
	});
});
