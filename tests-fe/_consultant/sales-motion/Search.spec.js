import { shallowMount } from '@vue/test-utils';
import Search from 'Consultant/pages/sales-motion/Search';

import mockList from './data/salesMotionList.json';

global.axios = {
	get: jest.fn().mockResolvedValue({ data: mockList })
};

describe('Consultant/pages/sales-motion/Search', () => {
	let mountComponent;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return shallowMount(Search, {
				stubs: {
					SearchWrapper: {
						template: `<div><slot></slot></div>`
					}
				},
				mocks: {
					$route: {
						query: {}
					}
				},
				...options
			});
		};
	});

	it('Should render component correctly', () => {
		const wrapper = mountComponent();

		expect(wrapper.find('.sales-motion-search').exists()).toBe(true);
	});
});
