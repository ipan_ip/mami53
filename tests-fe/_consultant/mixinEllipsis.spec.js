import mixinEllipsis from 'Js/_consultant/mixins/mixinEllipsis';

describe('Test mixin ellipsis', () => {
	let str = '';
	describe('with argument "hello world"', () => {
		beforeAll(() => {
			str = 'hello world';
		});
		it('when max length 5, so would generated be ellipsis', () => {
			const expected = 'hello...';
			expect(mixinEllipsis.methods.ellipsis(str, 5)).toBe(expected);
		});

		it('when max length 15, so would return normally', () => {
			const expected = 'hello world';
			expect(mixinEllipsis.methods.ellipsis(str, 15)).toBe(expected);
		});
	});

	describe('with argument "Kos Putra Gejayan Indah Sleman Yogyakarta"', () => {
		beforeAll(() => {
			str = 'Kos Putra Gejayan Indah Sleman Yogyakarta';
		});
		it('when no argument passed, got a charecter with length 20 and ellipsis', () => {
			const expected = 'Kos Putra Gejayan In...';
			expect(mixinEllipsis.methods.ellipsis(str)).toBe(expected);
		});
	});
});
