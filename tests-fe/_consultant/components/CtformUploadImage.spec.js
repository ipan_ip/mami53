import { mount } from '@vue/test-utils';
import CtFormUploadImage from 'Consultant/components/CtFormUploadImage.vue';
jest.unmock('vue2-dropzone');

global.Cookies = { set: jest.fn(), get: jest.fn() };

describe('CtFormUploadImage.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		const mocks = {
			$store: {
				state: {
					token: 'abcde'
				}
			}
		};

		mountComponent = (options = {}) => {
			return mount(CtFormUploadImage, {
				mocks,
				...options
			});
		};
	});

	it('Should render component and match snapshot', () => {
		const wrapper = mountComponent({
			propsData: {
				url: 'url'
			}
		});

		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should load url in dropzone option', async () => {
		const url = 'url test';
		const wrapper = mountComponent({
			propsData: {
				url
			}
		});

		await wrapper.vm.$nextTick();
		expect(wrapper.vm.dropzoneOptions.url).toBe(url);
	});

	it.skip('Should load image when using currentImage prop', async () => {
		const currentImage = 'test current image';
		const wrapper = mountComponent({
			propsData: {
				url: 'test'
			}
		});

		wrapper.setProps({ currentImage });

		await wrapper.vm.$nextTick();

		const dzPreviewImage = wrapper.find('.dz-image img');
		expect(dzPreviewImage.attributes('src')).toBe(currentImage);
	});

	it('Should override default dropzone option', async () => {
		const options = { url: 'url', test: 'test' };
		const wrapper = mountComponent({
			propsData: {
				url: 'test',
				options
			}
		});

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.dropzoneOptions).toEqual(options);
	});

	it("Should throw error when using options prop haven't url", () => {
		// disable console only this test.
		// for testing Error
		const consoleError = console.error;
		global.console.error = jest.fn();
		try {
			mountComponent({
				propsData: {
					url: 'test',
					options: { test: 'test' }
				}
			});
		} catch (error) {
			expect(error).toBeInstanceOf(Error);
		}
		global.console.error = consoleError;
	});

	it('Should emit an event uploaded and remove "ct-upload--hide-info" class', () => {
		const successResponse = { file: 'test file', response: 'test response' };
		const wrapper = mountComponent({
			propsData: {
				url: 'test'
			},
			stubs: {
				Dropzone: {
					template: `<input class="dropzone" />`,
					methods: {
						success() {
							this.$emit(
								'vdropzone-success',
								successResponse.file,
								successResponse.response
							);
						}
					}
				}
			}
		});

		const uploaded = jest.fn();
		wrapper.vm.$on('updated', uploaded);

		wrapper.findComponent({ ref: 'uploadImage' }).vm.success();
		expect(uploaded).toHaveBeenCalledWith(successResponse);
		expect(wrapper.classes('ct-upload--hide-info')).toBe(false);
	});

	it('Should remove error message and emit an event remove', async () => {
		const wrapper = mountComponent({
			propsData: {
				url: 'test'
			}
		});

		const remove = jest.fn();
		wrapper.vm.$on('remove', remove);

		wrapper.setProps({ currentImage: 'test' });

		await wrapper.vm.$nextTick();

		expect(wrapper.find('.dz-image').exists()).toBe(true);

		wrapper.find('.dz-remove').trigger('click');

		await wrapper.vm.$nextTick();

		expect(wrapper.find('.dz-image').exists()).toBe(false);
		expect(remove).toHaveBeenCalledTimes(1);
	});

	it('Should show error correctly', async () => {
		const message = 'test message';

		const wrapper = mountComponent({
			propsData: {
				url: 'test'
			},
			stubs: {
				Dropzone: {
					template: `
					<div>
						<input class="dropzone" />
						<div class="dz-error-message">
							<div data-dz-errormessage></div>
						</div>
					</div>
					`,
					methods: {
						error() {
							this.$emit('vdropzone-error', 'test', { message }, 'test');
						}
					}
				}
			}
		});

		const error = jest.fn();
		wrapper.vm.$on('error', error);

		wrapper.findComponent({ ref: 'uploadImage' }).vm.error();

		await wrapper.vm.$nextTick();

		expect(
			wrapper.find('.dz-error-message [data-dz-errormessage]').text()
		).toBe(message);
		expect(error).toHaveBeenCalled();
	});

	it('Should show error with default message', async () => {
		const defaultMessage = 'Gagal upload gambar';

		const wrapper = mountComponent({
			propsData: {
				url: 'test'
			},
			stubs: {
				Dropzone: {
					template: `
					<div>
						<input class="dropzone" />
						<div class="dz-error-message">
							<div data-dz-errormessage></div>
						</div>
					</div>
					`,
					methods: {
						error() {
							this.$emit('vdropzone-error', 'test', {}, 'test');
						}
					}
				}
			}
		});

		wrapper.findComponent({ ref: 'uploadImage' }).vm.error();

		await wrapper.vm.$nextTick();

		expect(
			wrapper.find('.dz-error-message [data-dz-errormessage]').text()
		).toBe(defaultMessage);
	});
});
