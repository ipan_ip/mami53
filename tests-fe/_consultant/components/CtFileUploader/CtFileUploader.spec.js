import { shallowMount, createLocalVue } from '@vue/test-utils';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import CtFileUploader from 'Consultant/components/CtFileUploader/CtFileUploader.vue';

import { flushPromises } from 'tests-fe/utils/promises';

const localVue = createLocalVue();
mockVLazy(localVue);

const mockAxios = {
	CancelToken: {
		source: jest.fn(() => ({ token: 'test-token', cancel: jest.fn() }))
	},
	post: jest.fn().mockImplementation((url, file, options) => {
		if (typeof options.onUploadProgress === 'function') {
			options.onUploadProgress({ loaded: 100, total: 100 });
		}

		return Promise.resolve({ data: { status: true } });
	})
};

describe('CtFileUploader.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		global.axios = mockAxios;

		mountComponent = (options = {}) => {
			return shallowMount(CtFileUploader, {
				localVue,
				propsData: {
					url: 'test'
				},
				...options
			});
		};
	});

	afterEach(() => {
		global.axios = {};
	});

	it('Should render component and match snapshot', () => {
		const wrapper = mountComponent();

		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should emit an event success', async () => {
		const wrapper = mountComponent();

		const event = {
			target: {
				files: [
					{
						name: 'test.png',
						size: 20000,
						type: 'image/png'
					}
				]
			}
		};

		const success = jest.fn();
		wrapper.vm.$on('success', success);

		await wrapper.vm.$nextTick();

		wrapper.vm.handleFileChange(event);

		await flushPromises();

		expect(success).toHaveBeenCalled();
	});

	it('Should show loading element with percentage of width', async () => {
		const totalFileSize = 100;
		const uploadedSize = 10;
		const expectedWidth = (uploadedSize * 100) / totalFileSize;

		const event = {
			target: {
				files: [
					{
						name: 'test.png',
						size: totalFileSize,
						type: 'image/png'
					}
				]
			}
		};

		global.axios = {
			CancelToken: {
				source: jest.fn(() => ({ token: 'test-token' }))
			},
			post: jest.fn().mockImplementation((url, file, options) => {
				if (typeof options.onUploadProgress === 'function') {
					options.onUploadProgress({
						loaded: uploadedSize,
						total: totalFileSize
					});
				}

				return Promise.resolve({ data: { status: true } });
			})
		};

		const wrapper = mountComponent();

		await wrapper.vm.$nextTick();

		wrapper.vm.handleFileChange(event);

		await wrapper.vm.$nextTick();

		const loadingEl = wrapper.find('.preview__loading');

		expect(loadingEl.attributes('style')).toEqual(
			expect.stringContaining(`width: ${expectedWidth}%`)
		);

		await flushPromises();
	});

	it('Should reset upload and cancel request when click cancel button', async () => {
		const event = {
			target: {
				files: [
					{
						name: 'test.png',
						size: 10000,
						type: 'image/png'
					}
				]
			}
		};

		const cancel = jest.fn();
		const confirmPopUp = jest.fn(() => true);
		global.confirm = confirmPopUp;

		global.axios = {
			CancelToken: {
				source: jest.fn(() => ({ token: 'test-token', cancel }))
			},
			post: jest.fn().mockImplementation((url, file, options) => {
				if (typeof options.onUploadProgress === 'function') {
					options.onUploadProgress({
						loaded: 10,
						total: 10000
					});
				}

				return Promise.resolve({ data: { status: true } });
			})
		};

		const wrapper = mountComponent();

		await wrapper.vm.$nextTick();

		wrapper.vm.handleFileChange(event);

		expect(wrapper.vm.file).toBe(event.target.files[0]);

		await wrapper.vm.$nextTick();

		wrapper.find('.preview__action button').trigger('click');

		expect(confirmPopUp).toHaveBeenCalled();

		await flushPromises();

		expect(cancel).toHaveBeenCalled();
		expect(wrapper.vm.file).toBeNull();
	});

	it('Should show error file too big', async () => {
		const event = {
			target: {
				files: [
					{
						name: 'test.png',
						size: 2 * 1024 * 1024, // 2MB
						type: 'image/png'
					}
				]
			}
		};

		const wrapper = mountComponent({
			propsData: {
				url: 'test',
				maxFilesize: 1 // 1MB
			}
		});

		const expectedErrorMessage = 'File terlalu besar';
		const error = jest.fn();
		wrapper.vm.$on('error', error);

		await wrapper.vm.$nextTick();

		wrapper.vm.handleFileChange(event);

		expect(wrapper.vm.errorMessage).toBe(expectedErrorMessage);
		expect(error).toHaveBeenCalled();
	});

	it('Should show error "file ini tidak diizinkan"', async () => {
		const event = {
			target: {
				files: [
					{
						name: 'test.png',
						size: 1000,
						type: 'image/png'
					}
				]
			}
		};

		const wrapper = mountComponent({
			propsData: {
				url: 'test',
				acceptedFiles: ['jpg']
			}
		});

		const expectedErrorMessage = 'file ini tidak diizinkan';
		const error = jest.fn();
		wrapper.vm.$on('error', error);

		await wrapper.vm.$nextTick();

		wrapper.vm.handleFileChange(event);

		expect(wrapper.vm.errorMessage).toBe(expectedErrorMessage);
		expect(error).toHaveBeenCalled();
	});

	it('Should reset file when click delete', async () => {
		const event = {
			target: {
				files: [
					{
						name: 'test.png',
						size: 1000,
						type: 'image/png'
					}
				]
			}
		};

		const wrapper = mountComponent();

		await wrapper.vm.$nextTick();

		wrapper.vm.handleFileChange(event);

		await flushPromises();

		expect(wrapper.vm.file).toBe(event.target.files[0]);

		wrapper.find('.preview__action button').trigger('click');

		expect(wrapper.vm.file).toBeNull();
	});

	it('Should emit an event delete when have delete listener and upload was error on hit request', async () => {
		const event = {
			target: {
				files: [
					{
						name: 'test.png',
						size: 1000,
						type: 'image/png'
					}
				]
			}
		};

		global.axios = {
			CancelToken: {
				source: jest.fn(() => ({ token: 'test-token', cancel: jest.fn() }))
			},
			post: jest.fn().mockRejectedValue(new Error({}))
		};

		const wrapper = mountComponent({
			listeners: {
				delete: jest.fn()
			}
		});

		const deleteEvent = jest.fn();
		wrapper.vm.$on('delete', deleteEvent);
		wrapper.vm.canRequestDelete = true;

		await wrapper.vm.$nextTick();

		wrapper.vm.handleFileChange(event);

		await flushPromises();

		wrapper.find('.preview__action button').trigger('click');

		expect(deleteEvent).toHaveBeenCalled();
	});
});
