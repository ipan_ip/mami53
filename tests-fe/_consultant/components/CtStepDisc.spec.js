import { shallowMount, createLocalVue } from '@vue/test-utils';
import CtStepDisc from 'Consultant/components/CtStepDisc.vue';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

const localVue = createLocalVue();
mockVLazy(localVue);

describe('CtStepDisc.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return shallowMount(CtStepDisc, {
				localVue,
				...options
			});
		};
	});

	it('Should render component and match snapshot', () => {
		const wrapper = mountComponent();

		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should render icon and match snapshot', () => {
		const wrapper = mountComponent({
			propsData: {
				showIcon: true,
				activated: true
			}
		});

		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should has class gradient', () => {
		const wrapper = mountComponent({
			propsData: {
				gradient: 'green'
			}
		});

		expect(wrapper.classes('disc--green')).toBe(true);
	});

	it('Should has class activated', () => {
		const wrapper = mountComponent({
			propsData: {
				activated: true
			}
		});

		expect(wrapper.classes('disc--activated')).toBe(true);
	});
});
