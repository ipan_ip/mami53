import { mount } from '@vue/test-utils';
import CtActivityItem from 'Consultant/components/CtActivityItem.vue';

describe('CtActivityItem.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}, additionalProps = {}) => {
			return mount(CtActivityItem, {
				propsData: {
					progress: 10,
					text: 'Test',
					subtext: 'sub text',
					...additionalProps
				},
				...options
			});
		};
	});

	it('Should render component', () => {
		const wrapper = mountComponent();

		expect(wrapper.classes('ct-activity-item')).toBe(true);
	});

	it('Should render icon when using type icon', async () => {
		const wrapper = mountComponent(
			{},
			{
				itemType: 'icon',
				icon: 'icon'
			}
		);

		await wrapper.vm.$nextTick();
		expect(wrapper.find('.mamikos-icon').exists()).toBe(true);
	});

	it('Should render checkbox when using type checkbox', async () => {
		const wrapper = mountComponent(
			{
				stubs: {
					CtCheckbox: {
						template: '<div class="checkbox-test"></div>'
					}
				}
			},
			{
				itemType: 'checkbox'
			}
		);

		await wrapper.vm.$nextTick();
		expect(wrapper.find('.checkbox-test').exists()).toBe(true);
	});

	it('Should render slot subtext', async () => {
		const wrapper = mountComponent(
			{
				slots: {
					subtext: 'test 2'
				}
			},
			{
				subtext: 'test'
			}
		);

		await wrapper.vm.$nextTick();
		expect(wrapper.find('.task-label').text()).toBe('test 2');
	});

	it('Should emit change:checkbox', async () => {
		const wrapper = mountComponent(
			{
				stubs: {
					CtCheckbox: {
						template: `<input id="checkbox" @change="$emit('change')" />`
					}
				}
			},
			{
				itemType: 'checkbox'
			}
		);

		const change = jest.fn();
		wrapper.vm.$on('change:checkbox', change);

		await wrapper.vm.$nextTick();
		wrapper.find('#checkbox').trigger('change');
		expect(change).toHaveBeenCalled();
	});

	it('Should not emit change:checkbox when type is not checkbox', () => {
		const wrapper = mountComponent(
			{},
			{
				itemType: ''
			}
		);

		const change = jest.fn();
		wrapper.vm.$on('change:checkbox', change);

		wrapper.vm.handleCheck(true);
		expect(change).not.toHaveBeenCalled();
	});

	it('Should set local state when parent state changed', async () => {
		const wrapper = mountComponent();

		wrapper.setProps({ checked: true });

		await wrapper.vm.$nextTick();
		expect(wrapper.vm.isChecked).toBe(true);
	});

	it('Should attach an event onContentClick', async () => {
		const wrapper = mountComponent({
			stubs: {
				CtListItem: {
					template: `
					<div id="ct-list-item" @click="$emit('onContentClick')">
						<slot></slot>
					</div>
					`
				}
			}
		});

		const onContentClick = jest.fn();
		wrapper.vm.$on('onContentClick', onContentClick);

		wrapper.find('#ct-list-item').trigger('click');

		expect(onContentClick).toHaveBeenCalled();
	});
});
