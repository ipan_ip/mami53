import { shallowMount } from '@vue/test-utils';
import Chip from 'Consultant/components/Chip.vue';

describe('Chip.vue', () => {
	let createWrapper = Function;

	beforeEach(() => {
		createWrapper = (options = {}) => {
			return shallowMount(Chip, options);
		};
	});

	it('Should render chip and match snapshot', () => {
		const wrapper = createWrapper();

		expect(wrapper.element).toMatchSnapshot();
	});

	it('Should has class according to "type" prop', done => {
		const wrapper = createWrapper();
		const type = 'error';
		wrapper.setProps({ type });
		wrapper.vm.$nextTick(() => {
			expect(wrapper.classes()).toContain(`chip--${type}`);
			done();
		});
	});

	it('Should render default slot and match snapshot', () => {
		const slotText = 'Should render this text';
		const wrapper = createWrapper({
			slots: {
				default: slotText
			}
		});

		expect(wrapper.find('.chip__content').text()).toBe(slotText);
		expect(wrapper.element).toMatchSnapshot();
	});

	it('Should render chip as outlined', () => {
		const wrapper = createWrapper({
			propsData: {
				outlined: true
			}
		});

		expect(wrapper.classes()).toMatchSnapshot();
	});

	it('Should render chip as medium size', () => {
		const wrapper = createWrapper({
			propsData: {
				medium: true
			}
		});

		expect(wrapper.classes()).toMatchSnapshot();
	});

	it('Should render chip as large size', () => {
		const wrapper = createWrapper({
			propsData: {
				large: true
			}
		});

		expect(wrapper.classes()).toMatchSnapshot();
	});

	it('Should render chip as pill', () => {
		const wrapper = createWrapper({
			propsData: {
				pill: true
			}
		});

		expect(wrapper.classes()).toMatchSnapshot();
	});

	it('Should emit an event click', async () => {
		const wrapper = createWrapper({
			listeners: {
				click: () => {}
			}
		});

		await wrapper.vm.$nextTick();

		wrapper.trigger('click');

		expect(wrapper.emitted('click')).toBeTruthy();
	});
});
