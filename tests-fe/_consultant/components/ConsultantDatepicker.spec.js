import { shallowMount } from '@vue/test-utils';
import ConsultantDatepicker from 'Consultant/components/ConsultantDatepicker.vue';

describe('Consultant/components/ConsultantDatepicker.vue', () => {
	const mocks = {
		showCalendar: jest.fn()
	};

	let mountComponent = Function;

	const refDatepickerStub = {
		render: () => {},
		methods: {
			showCalendar: jest.fn()
		}
	};

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return shallowMount(ConsultantDatepicker, {
				mocks,
				...options
			});
		};
	});

	test('Should render correctly', async () => {
		const tree = shallowMount(ConsultantDatepicker, {
			mocks
		});

		expect(tree.element).toMatchSnapshot();
	});

	test('selecting a date', async () => {
		const wrapper = mountComponent({
			options: {
				stubs: {
					refDatepicker: refDatepickerStub
				}
			}
		});
		wrapper.vm.selectedDate();
		wrapper.vm.showCalendar();
		await wrapper.vm.$nextTick();
		expect(wrapper.emitted('selected')).toBeTruthy();
	});

	test('have full width class if the full width props is true', async () => {
		const wrapper = mountComponent();
		wrapper.setProps({
			fullWidth: true
		});

		await wrapper.vm.$nextTick();

		expect(wrapper.classes('datepicker--full-width')).toBe(true);
	});
});
