import { mount } from '@vue/test-utils';
import CtCheckbox from 'Consultant/components/CtCheckbox.vue';

describe('CtCheckbox.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return mount(CtCheckbox, options);
		};
	});

	it('Should render checkbox component and match snapshot', () => {
		const wrapper = mountComponent({
			propsData: {
				label: 'label'
			}
		});
		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should render label and match snapshot', () => {
		const wrapper = mountComponent({
			propsData: {
				label: 'Test'
			}
		});
		expect(wrapper.find('.ct-checkbox__label').exists()).toBe(true);
	});

	it('Should slot and replace label prop', () => {
		const label = 'label slot';
		const wrapper = mountComponent({
			propsData: {
				label: 'Test'
			},
			slots: {
				default: label
			}
		});
		expect(wrapper.find('.ct-checkbox__label').text()).toBe(label);
	});

	it('Should render checkbox with display block', () => {
		const wrapper = mountComponent({
			attrs: {
				block: true
			}
		});

		expect(wrapper.classes('ct-checkbox--block')).toBe(true);
	});

	it('Should render input with disabled atribute when using disabled prop', () => {
		const wrapper = mountComponent({
			attrs: {
				disabled: true
			}
		});

		expect(wrapper.classes('ct-checkbox--disabled')).toBe(true);
		expect(wrapper.find('input').attributes('disabled')).toMatchSnapshot();
	});

	it('Should render input with disabled atribute when using readonly prop', () => {
		const wrapper = mountComponent({
			attrs: {
				readonly: true
			}
		});

		expect(wrapper.classes('ct-checkbox--readonly')).toBe(true);
		expect(wrapper.find('input').attributes('disabled')).toMatchSnapshot();
	});

	describe('When Check', () => {
		it('Should has active class', async () => {
			const wrapper = mountComponent();

			wrapper.find('.ct-checkbox__action').trigger('click');

			await wrapper.vm.$nextTick();

			expect(wrapper.classes('ct-checkbox--active')).toBe(true);
		});

		it('Should not attach an event click when disabled', async () => {
			const wrapper = mountComponent({
				attrs: {
					disabled: true
				}
			});

			wrapper.find('.ct-checkbox__action').trigger('click');

			await wrapper.vm.$nextTick();

			expect(wrapper.classes('ct-checkbox--active')).toBe(false);
		});

		it('Should not attach an event click when readonly', async () => {
			const wrapper = mountComponent({
				attrs: {
					readonly: true
				}
			});

			wrapper.find('.ct-checkbox__action').trigger('click');

			await wrapper.vm.$nextTick();

			expect(wrapper.classes('ct-checkbox--active')).toBe(false);
		});

		it("Should emit an event input with boolean value when hasn't value prop", async () => {
			const wrapper = mountComponent();
			const inputEvent = jest.fn();

			wrapper.vm.$on('input', inputEvent);

			wrapper.find('.ct-checkbox__action').trigger('click');

			expect(inputEvent).toHaveBeenCalledWith(expect.any(Boolean));
		});

		it('Should emit an event input with string type', async () => {
			const value = 'test';
			const wrapper = mountComponent({
				propsData: {
					value
				}
			});
			const inputEvent = jest.fn();

			wrapper.vm.$on('input', inputEvent);

			wrapper.find('.ct-checkbox__action').trigger('click');

			expect(inputEvent).toHaveBeenCalledWith(value);
		});

		it('Should emit an event input with object type', async () => {
			const value = { test: 'test' };
			const wrapper = mountComponent({
				propsData: {
					value
				}
			});
			const inputEvent = jest.fn();

			wrapper.vm.$on('input', inputEvent);

			wrapper.find('.ct-checkbox__action').trigger('click');

			expect(inputEvent).toHaveBeenCalledWith(value);
		});

		it('Should append value to input value when it type is array', async () => {
			const value = 'test';
			const wrapper = mountComponent({
				propsData: {
					value,
					inputValue: []
				}
			});
			const inputEvent = jest.fn();

			wrapper.vm.$on('input', inputEvent);

			wrapper.find('.ct-checkbox__action').trigger('click');

			expect(inputEvent).toHaveBeenCalledWith(expect.arrayContaining([value]));
		});
	});

	describe('When Uncheck', () => {
		it("Should emit an event input with false value when hasn't value prop", async () => {
			const wrapper = mountComponent({
				propsData: {
					inputValue: true
				}
			});
			const inputEvent = jest.fn();

			wrapper.vm.$on('input', inputEvent);

			wrapper.find('.ct-checkbox__action').trigger('click');

			expect(inputEvent).toHaveBeenCalledWith(false);
		});

		it('Should emit an event input with null', async () => {
			const value = 'test';
			const wrapper = mountComponent({
				propsData: {
					inputValue: value,
					value
				}
			});
			const inputEvent = jest.fn();

			wrapper.vm.$on('input', inputEvent);

			wrapper.find('.ct-checkbox__action').trigger('click');

			expect(inputEvent).toHaveBeenCalledWith(null);
		});

		it('Should reduce inputValue when using array type', async () => {
			const value = 'test';
			const wrapper = mountComponent({
				propsData: {
					inputValue: [value],
					value
				}
			});
			const inputEvent = jest.fn();

			wrapper.vm.$on('input', inputEvent);

			wrapper.find('.ct-checkbox__action').trigger('click');

			expect(inputEvent).toHaveBeenCalledWith(
				expect.not.arrayContaining([value])
			);
		});
	});
});
