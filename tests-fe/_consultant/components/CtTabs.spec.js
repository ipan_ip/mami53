import { shallowMount } from '@vue/test-utils';
import CtTabs from 'Consultant/components/CtTabs.vue';

describe('CtTabs.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return shallowMount(CtTabs, options);
		};
	});

	it('Should render component and match snapshot', async () => {
		const wrapper = mountComponent({
			propsData: {
				items: [{ label: 'test' }]
			}
		});

		await wrapper.vm.$nextTick();

		expect(wrapper.html()).toMatchSnapshot();
	});

	it.skip('Should render slider and match snapshot', async () => {
		const wrapper = mountComponent({
			propsData: {
				value: 0,
				items: [{ label: 'test' }]
			}
		});

		await wrapper.vm.$nextTick();

		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should attach an event change when tab has clicked', async () => {
		const items = [{ label: 'test' }, { label: 'test 2' }];
		const indexSelect = 1;
		const wrapper = mountComponent({
			propsData: {
				items: items
			}
		});

		const change = jest.fn();
		wrapper.vm.$on('change', change);

		await wrapper.vm.$nextTick();

		wrapper
			.findAll('.ct-tab')
			.at(indexSelect)
			.trigger('click');

		expect(change).toHaveBeenCalledWith(items[indexSelect], indexSelect);
	});

	it('Should not attach an event change when item disabled', async () => {
		const items = [{ label: 'test' }, { label: 'test 2', disabled: true }];
		const indexSelect = 1;
		const wrapper = mountComponent({
			propsData: {
				items: items
			}
		});

		const change = jest.fn();
		wrapper.vm.$on('change', change);

		await wrapper.vm.$nextTick();

		wrapper
			.findAll('.ct-tab')
			.at(indexSelect)
			.trigger('click');

		expect(change).not.toHaveBeenCalled();
	});
});
