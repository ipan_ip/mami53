import { shallowMount } from '@vue/test-utils';
import InputCurrency from 'Consultant/components/InputCurrency.vue';

describe('InputCurrency.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return shallowMount(InputCurrency, options);
		};
	});

	it('Should render input component and match snapshot', () => {
		const wrapper = mountComponent();
		expect(wrapper.element).toMatchSnapshot();
	});

	it('Should render label and match snapshot', () => {
		const wrapper = mountComponent({
			propsData: {
				label: 'Input Label'
			}
		});
		expect(wrapper.find('.input__label')).toMatchSnapshot();
	});

	it('Should has mandatory sign', () => {
		const wrapper = mountComponent({
			propsData: {
				mandatorySign: true
			}
		});

		expect(wrapper.classes('has-mandatory-sign')).toBe(true);
	});

	it('should has outlined class when use outlined prop', () => {
		const wrapper = mountComponent({
			propsData: {
				outlined: true
			}
		});

		expect(wrapper.classes('input--outlined')).toBe(true);
	});

	it('should change local value when parent value has changed', async () => {
		const valueChanges = 'Value from parent';
		const wrapper = mountComponent();

		wrapper.setProps({ value: valueChanges });
		await wrapper.vm.$nextTick();
		expect(wrapper.vm.currency).toBe(valueChanges);
	});

	it('should emit an event input', async () => {
		const inputEmit = jest.fn();
		const wrapper = mountComponent({
			propsData: {
				label: 'input'
			},
			stubs: {
				'vue-numeric': `<input @input="$emit('input')" />`
			}
		});

		wrapper.vm.$on('input', inputEmit);
		await wrapper.vm.$nextTick();

		wrapper.find('#input').trigger('input');
		expect(inputEmit).toHaveBeenCalled();
	});

	it('should set focus when input child is focus', async () => {
		const wrapper = mountComponent({
			propsData: {
				label: 'input'
			},
			stubs: {
				VueNumeric: `<input @focus="$emit('focus')" />`
			}
		});

		await wrapper.vm.$nextTick();

		wrapper.find('#input').trigger('focus');
		expect(wrapper.vm.isFocus).toBe(true);
	});

	it('should remove focus when input child is blur', async () => {
		const wrapper = mountComponent({
			propsData: {
				label: 'input'
			},
			stubs: {
				VueNumeric: `<input @blur="$emit('blur')" />`
			}
		});

		await wrapper.vm.$nextTick();

		wrapper.find('#input').trigger('blur');
		expect(wrapper.vm.isFocus).toBe(false);
	});
});
