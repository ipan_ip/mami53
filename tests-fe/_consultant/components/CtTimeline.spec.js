import { shallowMount } from '@vue/test-utils';
import CtTimeline from 'Consultant/components/CtTimeline.vue';

describe('CtTimeline.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}, additionalProps = {}) => {
			return shallowMount(CtTimeline, {
				propsData: {
					items: [
						{
							label: 'test 1',
							date: 'date 1'
						},
						{
							label: 'test 2',
							date: 'date 2'
						}
					],
					...additionalProps
				},
				...options
			});
		};
	});

	it('Should render component and match snapshot', () => {
		const wrapper = mountComponent();

		expect(wrapper.classes('ct-timelie')).toBe(true);
		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should render slot content correctly', () => {
		const wrapper = mountComponent({
			slots: {
				content: '<div class="content"></div>'
			}
		});

		const contents = wrapper.findAll('.content');

		expect(contents.length).toBe(2);
	});

	it('Should render item reversely when using reverse prop', () => {
		const wrapper = mountComponent({}, { reverse: true });

		const item = wrapper.findAll('.label-text');

		expect(item.at(0).text()).toBe('test 2');
	});
});
