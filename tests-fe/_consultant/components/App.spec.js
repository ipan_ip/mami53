import { shallowMount } from '@vue/test-utils';
import App from 'Consultant/components/App.vue';
import { toKebebCase } from 'Consultant/helpers/stringManipulation';

describe('App.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return shallowMount(App, {
				stubs: ['router-view'],
				...options
			});
		};
	});

	it('Should render background app based on background passed on route', async () => {
		const backgroundColor = 'test';

		const wrapper = mountComponent({
			mocks: {
				$route: {
					meta: {
						theme: {
							background: backgroundColor
						}
					}
				}
			}
		});

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.background).toEqual(backgroundColor);
	});

	it('Should render background app with default background', async () => {
		const wrapper = mountComponent({
			mocks: {
				$route: {
					meta: {}
				}
			}
		});

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.background).toEqual('#f4f4f4');
	});

	it('Should have style according to style passed by route', async () => {
		const styles = { backgroundColor: 'black', marginTop: '20px' };

		const wrapper = mountComponent({
			mocks: {
				$route: {
					meta: {
						contentOptions: { style: styles }
					}
				}
			}
		});

		await wrapper.vm.$nextTick();

		for (const [prop, value] of Object.entries(styles)) {
			const style = `${toKebebCase(prop)}: ${value};`;
			expect(wrapper.vm.contentStyles).toEqual(expect.stringContaining(style));
		}
	});

	it('Should set "hasNavbar" state to be false', async () => {
		const wrapper = mountComponent({
			mocks: {
				$route: {
					meta: {
						navbar: false
					}
				}
			}
		});

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.hasNavbar).toBe(false);
	});
});
