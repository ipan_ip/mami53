import { shallowMount, createLocalVue } from '@vue/test-utils';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import CardListImage from 'Consultant/components/detail-card/CardListImage.vue';

const localVue = createLocalVue();
mockVLazy(localVue);

describe('CardListImage.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return shallowMount(CardListImage, {
				localVue,
				propsData: {
					src: {
						small: 'url-image-small',
						large: 'url-image-large'
					},
					title: 'image title'
				},
				...options
			});
		};
	});

	it('Should render component and match snapshot', () => {
		const wrapper = mountComponent();

		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should render image preview when image content clicked', async () => {
		const wrapper = mountComponent();

		wrapper.find({ ref: 'image' }).trigger('click');
		await wrapper.vm.$nextTick();
		expect(wrapper.vm.isImagePreviweLoaded).toBe(true);
		expect(wrapper.find('.image__preview').exists()).toBe(true);
	});

	it('Should loaded image preview after 100ms', async () => {
		jest.useFakeTimers();
		const wrapper = mountComponent();

		wrapper.setData({ isImagePreviweLoaded: true });
		await wrapper.vm.$nextTick();

		expect(wrapper.vm.imageHasOpened).toBe(false);
		jest.advanceTimersByTime(100);
		expect(wrapper.vm.imageHasOpened).toBe(true);
	});

	it('Should remove is-image--open class immediately', async () => {
		jest.useFakeTimers();
		const wrapper = mountComponent();

		wrapper.setData({ isImagePreviweLoaded: true });
		await wrapper.vm.$nextTick();
		jest.advanceTimersByTime(100);

		wrapper.setData({ isImagePreviweLoaded: false });
		await wrapper.vm.$nextTick();
		jest.advanceTimersByTime(0);
		expect(wrapper.vm.imageHasOpened).toBe(false);
	});
});
