import { shallowMount } from '@vue/test-utils';
import CardList from 'Consultant/components/detail-card/CardList.vue';

describe('CardList.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}, additionalProps = {}) => {
			return shallowMount(CardList, {
				propsData: {
					label: 'label',
					content: 'content',
					type: 'text',
					...additionalProps
				},
				...options
			});
		};
	});

	it('Should render component and match snapshot', () => {
		const wrapper = mountComponent();

		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should have is-value-as-label when using value_as_label type', () => {
		const wrapper = mountComponent(
			{},
			{
				type: 'value_as_label'
			}
		);

		expect(wrapper.classes('is-value-as-label')).toBe(true);
	});

	it('Should have is-value-as-label and additional class', () => {
		const wrapper = mountComponent(
			{},
			{
				type: 'value_as_label',
				classes: 'additional-class'
			}
		);

		expect(wrapper.classes()).toEqual(
			expect.arrayContaining(['is-value-as-label', 'additional-class'])
		);
	});

	it('Emit an event when label has been clicked', () => {
		const wrapper = mountComponent();

		const event = jest.fn();
		wrapper.vm.$on('label-clicked', event);

		wrapper.find('.card-list__label').trigger('click');
		expect(event).toHaveBeenCalled();
	});
});
