import { shallowMount } from '@vue/test-utils';
import DetailCard from 'Consultant/components/detail-card/DetailCard.vue';

describe('DetailCard.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return shallowMount(DetailCard, options);
		};
	});

	it('Should render default slot and match snapshot', () => {
		const wrapper = mountComponent({
			slots: {
				default: '<div></div>'
			}
		});

		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should render header slot and match snapshot', () => {
		const headerText = 'test header slot';
		const wrapper = mountComponent({
			slots: {
				header: headerText
			}
		});

		expect(wrapper.find('.detail-card-header').text()).toBe(headerText);
		expect(wrapper.html()).toMatchSnapshot();
	});
});
