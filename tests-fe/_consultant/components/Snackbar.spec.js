import { shallowMount, createLocalVue } from '@vue/test-utils';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import Snackbar from 'Consultant/components/Snackbar.vue';
import Vue from 'vue';

const localVue = createLocalVue();
mockVLazy(localVue);

describe('Snackbar.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return shallowMount(Snackbar, {
				localVue,
				propsData: {
					value: true
				},
				...options
			});
		};
	});

	it('Should render default slot and match snapshot', () => {
		const text = 'Default Slot';
		const wrapper = mountComponent({
			slots: {
				default: text
			}
		});

		const defaultSlot = wrapper.find('.consultant-snackbar__content');
		expect(defaultSlot.text()).toMatchSnapshot();
		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should render default header slot when have show-button-close attribute', async () => {
		const wrapper = mountComponent({
			attrs: {
				'show-button-close': true
			}
		});

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.hasCloseButton).toBe(true);
	});

	it("Should not render default header slot when haven't show-button-close attribute", () => {
		const wrapper = mountComponent();

		expect(wrapper.find('.consultant-snackbar__close').exists()).toBe(false);
	});

	it('Should render header slot and match snapshot', () => {
		const wrapper = mountComponent({
			slots: {
				'snackbar-header': '<div class="header-slot"></div>'
			}
		});

		expect(wrapper.find('.header-slot').exists()).toBe(true);
		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Emit an event input when button close has been clicked', () => {
		const wrapper = mountComponent({
			attrs: {
				'show-button-close': true
			}
		});

		const input = jest.fn();
		wrapper.vm.$on('input', input);

		const btnCloseContainer = wrapper.find('.consultant-snackbar__close');
		btnCloseContainer.find('img').trigger('click');
		expect(input).toHaveBeenCalledWith(false);
	});

	it('Should close snackbar when not using show-button-close attribute', () => {
		const wrapper = mountComponent();

		const input = jest.fn();
		wrapper.vm.$on('input', input);

		const background = wrapper.find('.consultant-snackbar__bg');
		background.trigger('click');
		expect(input).toHaveBeenCalledWith(false);
	});

	it('Should not close snackbar when using show-button-close attribute', () => {
		const wrapper = mountComponent({
			attrs: {
				'show-button-close': true
			}
		});

		const input = jest.fn();
		wrapper.vm.$on('input', input);

		const background = wrapper.find('.consultant-snackbar__bg');
		background.trigger('click');
		expect(input).not.toHaveBeenCalled();
	});

	it('Should have overflowY: hidden when snackbar opened', async () => {
		const wrapper = mountComponent({
			propsData: {
				value: false
			}
		});

		wrapper.setProps({ value: true });

		await wrapper.vm.$nextTick();

		expect(document.body.getAttribute('style')).toMatch(/overflow-y: hidden/);
		expect(document.body.getAttribute('style')).toMatchSnapshot();
	});

	it('Should remove overflowY: hidden when snackbar closed', async () => {
		const wrapper = mountComponent({
			propsData: {
				value: true
			}
		});

		wrapper.setProps({ value: false });

		await wrapper.vm.$nextTick();
		const style = document.body.getAttribute('style');
		expect(style).not.toMatch(/overflow-y: hidden/);
		expect(style).toMatchSnapshot();
	});

	it('Should emit an event input when reset from parent component', () => {
		const ParentComponent = Vue.component('parent');

		const wrapper = mountComponent({
			parentComponent: ParentComponent
		});

		const input = jest.fn();
		wrapper.vm.$on('input', input);

		const parent = wrapper.vm.$parent;

		parent.$children[0].handleClickResetFilter();

		expect(input).toHaveBeenCalled();
	});

	it('should remove overflow in body when component has been destroyed', async () => {
		const wrapper = mountComponent({
			propsData: {
				value: false
			}
		});

		wrapper.setProps({ value: true });

		await wrapper.vm.$nextTick();

		expect(document.body.getAttribute('style')).toEqual(
			expect.stringContaining('overflow-y: hidden')
		);

		wrapper.destroy();

		expect(document.body.getAttribute('style')).toEqual(
			expect.not.stringContaining('overflow-y: hidden')
		);
	});
});
