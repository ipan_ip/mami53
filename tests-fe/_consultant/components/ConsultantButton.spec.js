import { shallowMount } from '@vue/test-utils';
import Button from 'Consultant/components/ConsultantButton.vue';

describe('ConsultantButton.vue', () => {
	let wrapper;

	beforeEach(() => {
		wrapper = shallowMount(Button);
	});

	it('Should render button component', () => {
		expect(wrapper.find('.consultant-btn').exists()).toBeTruthy();
	});

	it('Should render button with submit type', async () => {
		wrapper.setProps({ submit: true });
		await wrapper.vm.$nextTick();
		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should have "-medium" class', done => {
		wrapper.setProps({ medium: true });
		wrapper.vm.$nextTick(() => {
			expect(wrapper.classes()).toContain('-medium');
			done();
		});
	});

	it('Should have "-large" class', done => {
		wrapper.setProps({ large: true });
		wrapper.vm.$nextTick(() => {
			expect(wrapper.classes()).toContain('-large');
			done();
		});
	});

	it('Should have "-outline" class', done => {
		wrapper.setProps({ outline: true });
		wrapper.vm.$nextTick(() => {
			expect(wrapper.classes()).toContain('-outline');
			done();
		});
	});

	it('Should have "-block" class', done => {
		wrapper.setProps({ block: true });
		wrapper.vm.$nextTick(() => {
			expect(wrapper.classes()).toContain('-block');
			done();
		});
	});

	it('Should have "--loading" class', done => {
		wrapper.setProps({ loading: true });
		wrapper.vm.$nextTick(() => {
			expect(wrapper.classes()).toContain('--loading');
			done();
		});
	});

	it('Should have "--disabled" class', done => {
		wrapper.setProps({ disabled: true });
		wrapper.vm.$nextTick(() => {
			expect(wrapper.classes()).toContain('--disabled');
			done();
		});
	});

	it('Should have "-flat" class', done => {
		wrapper.setProps({ flat: true });
		wrapper.vm.$nextTick(() => {
			expect(wrapper.classes().includes('-flat')).toBeTruthy();
			done();
		});
	});

	describe('Test button when loading', () => {
		beforeAll(() => {
			wrapper.setProps({ loading: false });
		});

		it('should render text loading', done => {
			const loadingText = 'Loading...';
			wrapper.setProps({ loading: true, loadingText });
			wrapper.vm.$nextTick(() => {
				expect(wrapper.find('button').text()).toBe(loadingText);
				done();
			});
		});

		it('should render slot loading text', done => {
			const loadingText = 'Loading...';
			const wrapperWithSlot = shallowMount(Button, {
				slots: {
					loading: loadingText
				}
			});
			wrapperWithSlot.setProps({ loading: true });
			wrapperWithSlot.vm.$nextTick(() => {
				expect(wrapperWithSlot.find('button').text()).toBe(loadingText);
				done();
			});
		});
	});

	describe('Test Button on Click', () => {
		let button = null;

		beforeEach(() => {
			wrapper = shallowMount(Button, {
				propsData: {
					loading: false,
					disabled: false
				},
				mocks: { $router: { push: jest.fn() } }
			});
			button = wrapper.find('button');
		});

		it('Should send emiting event to parent', () => {
			const clickEvent = jest.fn();
			wrapper.vm.$on('click', clickEvent);
			button.trigger('click');
			expect(clickEvent).toHaveBeenCalled();
		});

		it('Should attach and event push when using "to" prop', async () => {
			const to = 'test';
			wrapper.setProps({
				to
			});

			await wrapper.vm.$nextTick();

			button.trigger('click');

			expect(wrapper.vm.$router.push).toHaveBeenCalledWith(to);
		});

		it('Cant click when loading', done => {
			wrapper.setProps({ loading: true });
			wrapper.vm.$nextTick(() => {
				button.trigger('click');
				expect(wrapper.emitted().click).toBeFalsy();
				done();
			});
		});

		it('Cant click when disabled', done => {
			wrapper.setProps({ disabled: true });
			wrapper.vm.$nextTick(() => {
				button.trigger('click');
				expect(wrapper.emitted().click).toBeFalsy();
				done();
			});
		});
	});
});
