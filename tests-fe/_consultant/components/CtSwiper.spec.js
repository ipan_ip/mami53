import { shallowMount } from '@vue/test-utils';
import CtSwiper from 'Consultant/components/CtSwiper.vue';

describe('CtSwiper.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return shallowMount(CtSwiper, options);
		};
	});

	it('Should render component', () => {
		const wrapper = mountComponent();

		expect(wrapper.classes('ct-swiper')).toBe(true);
	});

	it('Should render slot and match snapshot', () => {
		const wrapper = mountComponent({
			propsData: {
				items: [{}]
			},
			slots: {
				items: '<div></div>'
			}
		});

		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should attach max-width style when using maxWidth prop', () => {
		const wrapper = mountComponent({
			propsData: {
				maxWidth: '80%'
			}
		});

		expect(wrapper.attributes('style')).toMatchSnapshot();
	});
});
