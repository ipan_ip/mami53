import { shallowMount, createLocalVue } from '@vue/test-utils';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import ConsultantModal from 'Consultant/components/ConsultantModal.vue';

const localVue = createLocalVue();
mockVLazy(localVue);

describe('ConsultantModal.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return shallowMount(ConsultantModal, {
				localVue,
				...options
			});
		};
	});

	it('Should render modal and match snapshot', () => {
		const wrapper = mountComponent({
			propsData: {
				open: false
			}
		});

		expect(wrapper.element).toMatchSnapshot();
	});

	it('should hide close button and match snapshot', () => {
		const wrapper = mountComponent({
			propsData: {
				open: false,
				hideCloseButton: true
			}
		});

		expect(wrapper.element).toMatchSnapshot();
	});

	it('should render default slot and match snapshot', () => {
		const slotText = 'This is a default slot';
		const wrapper = mountComponent({
			propsData: {
				open: false
			},
			slots: {
				default: slotText
			}
		});

		expect(wrapper.find('.modal__content').text()).toBe(slotText);
		expect(wrapper.element).toMatchSnapshot();
	});

	it('should close modal when click close button', done => {
		const closeModal = jest.fn();
		const wrapper = mountComponent({
			propsData: {
				open: true
			}
		});

		wrapper.vm.$on('update:open', closeModal);
		expect(wrapper.vm.open).toBe(true);
		wrapper.find('.modal__close-btn').trigger('click');
		wrapper.vm.$nextTick(() => {
			expect(closeModal).toHaveBeenCalled();
			done();
		});
	});

	it('should close modal from child', async () => {
		const closeModal = jest.fn();
		const wrapper = mountComponent({
			propsData: {
				open: true,
				closeOnOutsideClick: true
			},
			stubs: {
				modal: `<button id="btnClose" @click="$emit('closed')"></button>`
			}
		});

		wrapper.vm.$on('close', closeModal);
		wrapper.find('#btnClose').trigger('click');
		await wrapper.vm.$nextTick();
		expect(closeModal).toHaveBeenCalled();
	});

	it('should not close modal if closeOnOutsideClick is false', () => {
		const closeModal = jest.fn();
		const wrapper = mountComponent({
			propsData: {
				open: true,
				closeOnOutsideClick: false
			}
		});

		wrapper.vm.$on('close', closeModal);
		expect(wrapper.vm.open).toBe(true);
		wrapper.vm.handleCloseModal();
		expect(closeModal).not.toHaveBeenCalled();
	});

	it('should set backdropClick after open modal', async () => {
		jest.useFakeTimers();
		const wrapper = mountComponent({
			propsData: {
				open: false,
				closeOnOutsideClick: true
			}
		});

		wrapper.setProps({ open: true });
		await wrapper.vm.$nextTick();
		expect(wrapper.vm.backdropClick).toBe(false);
		jest.runAllTimers();
		expect(wrapper.vm.backdropClick).toBe(true);
	});
});
