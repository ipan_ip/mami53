import { shallowMount } from '@vue/test-utils';
import CtInputText from 'Consultant/components/CtInputText.vue';

describe('CtInputText.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return shallowMount(CtInputText, options);
		};
	});

	it('Should render input component and match snapshot', () => {
		const wrapper = mountComponent();
		expect(wrapper.element).toMatchSnapshot();
	});

	it('Should render label', () => {
		const label = 'Input label';
		const wrapper = mountComponent({
			propsData: {
				label: label
			}
		});

		expect(wrapper.find('.ct-input__label').text()).toBe(label);
	});

	it('Should has mandatory sign', () => {
		const wrapper = mountComponent({
			propsData: {
				mandatorySign: true,
				label: 'test'
			}
		});

		const label = wrapper.find('.ct-input__label');

		expect(label.classes('has-mandatory-sign')).toBe(true);
	});

	it('should implement attribute to input', () => {
		const wrapper = mountComponent({
			attrs: {
				required: 'required'
			}
		});

		const input = wrapper.find('input');
		expect(input.element.getAttribute('required')).toBeTruthy();
	});

	it('should have border-radius according to radius prop passed', () => {
		const wrapper = mountComponent({
			propsData: {
				radius: 10
			}
		});

		const inputBody = wrapper.find('.ct-input__body');
		expect(inputBody.attributes('style')).toMatchSnapshot();
	});

	it('Should have background-color according to prop passed', () => {
		const wrapper = mountComponent({
			propsData: {
				backgroundColor: '#fff'
			}
		});

		const inputBody = wrapper.find('.ct-input__body');
		expect(inputBody.attributes('style')).toMatchSnapshot();
	});

	it('Should have class ct-input--outlined', () => {
		const wrapper = mountComponent({
			attrs: {
				outlined: true
			}
		});

		expect(wrapper.classes('ct-input--outlined')).toBe(true);
	});

	it('Should have class ct-input--flat', async () => {
		const wrapper = mountComponent({
			attrs: {
				flat: true
			}
		});

		expect(wrapper.classes('ct-input--flat')).toBe(true);
	});

	it('Should set input invalid when using invalidMessage prop', async () => {
		const invalidText = 'test';
		const wrapper = mountComponent({
			propsData: {
				invalidMessage: invalidText
			}
		});

		await wrapper.vm.$nextTick();

		expect(wrapper.classes('ct-input--invalid')).toBe(true);
		expect(wrapper.find('.message-text').text()).toBe(invalidText);
	});

	it('should render prepend icon', () => {
		const wrapper = mountComponent({
			propsData: {
				prependIcon: 'test'
			}
		});

		const prependIcon = wrapper.find('.ct-input__prepend-icon');
		expect(prependIcon).toMatchSnapshot();
	});

	it('should render append icon', () => {
		const wrapper = mountComponent({
			propsData: {
				appendIcon: 'test'
			}
		});

		const appendIcon = wrapper.find('.ct-input__append-icon');
		expect(appendIcon).toMatchSnapshot();
	});

	it('should render clearable icon', () => {
		const wrapper = mountComponent({
			propsData: {
				clearable: true
			}
		});

		const clearableIcon = wrapper.find('.clearable-icon');
		expect(clearableIcon).toMatchSnapshot();
	});

	it('should emit an event input', async () => {
		const wrapper = mountComponent();
		const value = 'test';
		const inputEvent = jest.fn();
		const input = wrapper.findComponent({ ref: 'input' });

		wrapper.vm.$on('input', inputEvent);
		input.element.value = value;

		input.trigger('input');

		await wrapper.vm.$nextTick();
		expect(wrapper.vm.localValue).toBe(value);
		expect(inputEvent).toHaveBeenCalledWith(value);
	});

	it('should not emit an event input when disabled', async () => {
		const wrapper = mountComponent({
			propsData: {
				disabled: true
			}
		});
		const inputEvent = jest.fn();

		wrapper.vm.$on('input', inputEvent);

		wrapper.findComponent({ ref: 'input' }).trigger('input');

		expect(inputEvent).not.toHaveBeenCalled();
	});

	it('should not emit an event input when readonly', async () => {
		const wrapper = mountComponent({
			propsData: {
				readonly: true
			}
		});
		const inputEvent = jest.fn();

		wrapper.vm.$on('input', inputEvent);

		wrapper.findComponent({ ref: 'input' }).trigger('input');

		expect(inputEvent).not.toHaveBeenCalled();
	});

	it('should set focus and emit focus event', async () => {
		const wrapper = mountComponent();
		const focusEvent = jest.fn();

		wrapper.vm.$on('focus', focusEvent);

		wrapper.find('input').trigger('focus');

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.isFocused).toBe(true);
		expect(focusEvent).toHaveBeenCalled();
	});

	it('should emit an event blur', async () => {
		const wrapper = mountComponent();
		const blurEvent = jest.fn();

		wrapper.vm.$on('blur', blurEvent);

		wrapper.find('input').trigger('blur');

		await wrapper.vm.$nextTick();

		expect(blurEvent).toHaveBeenCalled();
	});

	it('should fire a method onfocus', async () => {
		const wrapper = mountComponent();

		wrapper.vm.focus();

		expect(wrapper.vm.isFocused).toBe(true);
	});

	it('should fire a method onblur', async () => {
		const wrapper = mountComponent();
		const blur = jest.fn();
		wrapper.vm.$on('blur', blur);

		wrapper.findComponent({ ref: 'input' }).element.focus();

		expect(wrapper.vm.isFocused).toBe(true);

		wrapper.vm.blur();

		expect(wrapper.vm.isFocused).toBe(false);
		expect(blur).toHaveBeenCalled();
	});

	it('should set input focus when body has clicked', async () => {
		const wrapper = mountComponent();

		wrapper.find('.ct-input__body').trigger('click');

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.isFocused).toBe(true);
	});

	it('should clear input value', async () => {
		const wrapper = mountComponent({
			propsData: {
				clearable: true
			}
		});

		wrapper.setData({ localValue: 'test' });

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.localValue).toBe('test');

		wrapper.find('.clearable-icon .mamikos-icon').trigger('click');

		expect(wrapper.vm.localValue).toBe('');
	});

	it('should emit an event input with only number when using onlyNumber prop', () => {
		const wrapper = mountComponent({
			propsData: {
				onlyNumbers: true
			}
		});

		const inputEmit = jest.fn();
		wrapper.vm.$on('input', inputEmit);

		const input = wrapper.findComponent({ ref: 'input' });

		input.element.value = '123qwerty';
		input.trigger('input');

		expect(inputEmit).toHaveBeenCalledWith('123');
	});

	it('should attach event enter and blur when press enter key', () => {
		const wrapper = mountComponent();

		const enter = jest.fn();
		wrapper.vm.$on('enter', enter);

		const blur = jest.fn();
		wrapper.vm.$on('blur', blur);

		const input = wrapper.findComponent({ ref: 'input' });
		input.element.focus();
		input.trigger('keydown.enter');

		expect(enter).toHaveBeenCalled();
		expect(blur).toHaveBeenCalled();
	});

	it('should attach an event click:append', async () => {
		const wrapper = mountComponent({
			propsData: {
				appendIcon: 'test'
			}
		});

		const append = jest.fn();
		wrapper.vm.$on('click:append', append);

		await wrapper.vm.$nextTick();

		wrapper.find('.ct-input__append-icon.inner-icon i').trigger('click');

		expect(append).toHaveBeenCalled();
	});
});
