import '@babel/polyfill';
import { shallowMount } from '@vue/test-utils';
import ConsultantMenu from 'Consultant/components/ConsultantMenu.vue';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

describe('ConsultantMenu.vue', () => {
	let mountComponent = Function;
	const menuOptions = [
		{
			title: 'Menu 1'
		},
		{
			title: 'Menu 2'
		}
	];

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return shallowMount(ConsultantMenu, options);
		};
	});

	it('Should render menu with activator slot', () => {
		const activatorSlot = 'activator text';
		const wrapper = mountComponent({
			propsData: {
				options: menuOptions
			},
			slots: {
				activator: activatorSlot
			}
		});
		expect(wrapper.find({ ref: 'activator' }).text()).toBe(activatorSlot);
		expect(wrapper.element).toMatchSnapshot();
	});

	it("Should throw error when options hasn't title", () => {
		try {
			mountComponent({
				propsData: {
					options: [{}]
				}
			});
		} catch (error) {
			expect(error).toBeInstanceOf(Error);
		}
	});

	it('Should has active class after activator clicked', async done => {
		const wrapper = mountComponent();

		wrapper.find({ ref: 'activator' }).trigger('click');
		await wrapper.vm.$nextTick();
		setTimeout(() => {
			expect(wrapper.classes('menu--is-active')).toBe(true);
			done();
		}, 100);
	});

	it('Should set style on menu content and match snapshot', done => {
		const wrapper = mountComponent({
			propsData: {
				options: menuOptions
			},
			slots: {
				activator: '<button></button>'
			}
		});

		const getBoundingClientReact = () => ({
			x: 402.5,
			y: 633,
			width: 80,
			height: 88,
			top: 633,
			right: 482.5,
			bottom: 721,
			left: 402.5
		});

		wrapper.find({
			ref: 'activator'
		}).element.getBoundingClientRect = getBoundingClientReact;

		wrapper.find({
			ref: 'menuContent'
		}).element.getBoundingClientRect = getBoundingClientReact;

		const content = wrapper.find({
			ref: 'menuContent'
		});

		setTimeout(() => {
			wrapper.vm.handleActivatorClick();
			setTimeout(() => {
				expect(content.attributes('style')).toMatchSnapshot();
				done();
			}, 100);
		}, 100);
	});

	it('Should render content on the left of activator', async done => {
		const wrapper = mountComponent({
			propsData: {
				options: menuOptions,
				offsetLeft: true
			},
			slots: {
				activator: '<button></button>'
			}
		});

		const getBoundingClientReactActivator = () => ({
			x: 402.5,
			y: 633,
			width: 80,
			height: 88,
			top: 633,
			right: 482.5,
			bottom: 721,
			left: 402.5
		});

		const getBoundingClientReactContent = () => ({
			x: 402.5,
			y: 633,
			width: 100,
			height: 150,
			top: 633,
			right: 482.5,
			bottom: 721,
			left: 402.5
		});

		wrapper.find({
			ref: 'activator'
		}).element.getBoundingClientRect = getBoundingClientReactActivator;

		wrapper.find({
			ref: 'menuContent'
		}).element.getBoundingClientRect = getBoundingClientReactContent;

		const content = wrapper.find({
			ref: 'menuContent'
		});

		setTimeout(() => {
			wrapper.vm.handleActivatorClick();
			setTimeout(() => {
				expect(content.attributes('style')).toMatchSnapshot();
				done();
			}, 100);
		}, 100);
	});

	it('should not attach event to activator when disabled', async done => {
		const wrapper = mountComponent({
			propsData: {
				disabled: true
			}
		});

		wrapper.find({ ref: 'activator' }).trigger('click');

		await wrapper.vm.$nextTick();
		setTimeout(() => {
			expect(wrapper.classes('menu--is-active')).toBe(false);
			done();
		}, 100);
	});

	it('should render class on menu item', () => {
		const options = [
			{
				title: 'Menu 1'
			},
			{
				title: 'Menu 2',
				class: 'additional-class additional-class-2'
			}
		];
		const wrapper = mountComponent({
			propsData: {
				options
			}
		});

		const menuItem = wrapper.findAll('.list__tile').at(1);
		expect(menuItem.classes()).toMatchSnapshot();
	});

	it('should not attach event to menu item when disabled', () => {
		const options = [
			{
				title: 'Menu 1',
				class: 'disabled-menu-item',
				disabled: true
			}
		];
		const wrapper = mountComponent({
			propsData: {
				options
			}
		});

		const menuItem = wrapper.find('.disabled-menu-item');
		menuItem.trigger('click');
		expect(wrapper.emitted('select')).toBeFalsy();
	});

	it('should emit select event when menu item clicked', () => {
		const options = [
			{
				title: 'Menu 1',
				disabled: false
			}
		];
		const wrapper = mountComponent({
			propsData: {
				options
			}
		});

		const menuItem = wrapper.find('.list__tile');
		menuItem.trigger('click');
		expect(wrapper.emitted('select')).toBeTruthy();
	});

	it('should render no data is there are no options', () => {
		const options = [];
		const wrapper = mountComponent({
			propsData: {
				options
			}
		});

		const menuItem = wrapper.find('.list--no-data');
		expect(menuItem).toBeTruthy();
		expect(wrapper.find({ ref: 'menuContent' })).toMatchSnapshot();
	});

	it('Should remove all class of menu on activator click', () => {
		const removeClass = jest.fn();
		mockWindowProperty('document.querySelectorAll', () => {
			return [
				{
					classList: { remove: removeClass }
				}
			];
		});

		const wrapper = mountComponent();

		wrapper.find({ ref: 'activator' }).trigger('click');

		expect(removeClass).toHaveBeenCalled();
	});

	it.skip('Should set display none when menu is close', () => {
		jest.useFakeTimers();
		const wrapper = mountComponent();

		wrapper.vm.handleActivatorClick();

		jest.advanceTimersByTime(100);

		wrapper.vm.deactivateMenu();

		jest.advanceTimersByTime(300);

		expect(
			wrapper.find({ ref: 'menuContent' }).attributes('style')
		).toMatchSnapshot();
	});

	it('Should fire a method handleActivatorClick', async () => {
		const activatorClick = jest.fn();
		const wrapper = mountComponent({
			methods: {
				handleActivatorClick: activatorClick
			}
		});

		expect(activatorClick).not.toHaveBeenCalled();
		wrapper.setProps({ value: true });
		await wrapper.vm.$nextTick();
		expect(activatorClick).toHaveBeenCalled();
	});

	it('Should fire a method deactivateMenu', async () => {
		const deactivateMenu = jest.fn();
		const wrapper = mountComponent({
			propsData: {
				value: true
			},
			methods: {
				deactivateMenu
			}
		});

		wrapper.setProps({ value: false });
		await wrapper.vm.$nextTick();
		expect(deactivateMenu).toHaveBeenCalled();
	});
});
