import { shallowMount } from '@vue/test-utils';
import InputTextarea from 'Consultant/components/InputTextarea.vue';

describe('InputTextarea.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return shallowMount(InputTextarea, options);
		};
	});

	it('Should render input text and match snapshot', () => {
		const wrapper = mountComponent();
		expect(wrapper.element).toMatchSnapshot();
	});

	it('Should render label and match snapshot', () => {
		const wrapper = mountComponent({
			propsData: {
				label: 'Textarea Label'
			}
		});

		const label = wrapper.find('.textarea__label');
		expect(label.exists()).toBe(true);
		expect(label).toMatchSnapshot();
	});

	it('Should render placeholder of textarea', () => {
		const wrapper = mountComponent({
			propsData: {
				placeholder: 'Textarea placeholder'
			}
		});

		const textarea = wrapper.find({ ref: 'textarea' });
		expect(textarea.attributes('placeholder')).toBeTruthy();
	});

	it('Should render maxLength of textarea', () => {
		const wrapper = mountComponent({
			propsData: {
				maxLength: 200
			}
		});

		const textarea = wrapper.find({ ref: 'textarea' });
		expect(textarea.attributes('maxlength')).toBeTruthy();
	});

	it('Should render footer and have a counter element', () => {
		const wrapper = mountComponent({
			propsData: {
				counter: true
			}
		});

		const footer = wrapper.find('.textarea__footer');
		expect(footer).toMatchSnapshot();
		expect(footer.find('.textarea__counter').exists()).toBeTruthy();
	});

	it('Should emit an event input and count length of local value', () => {
		const wrapper = mountComponent();

		const textarea = wrapper.find({ ref: 'textarea' });

		expect(wrapper.vm.countCharacter).toBe(0);

		textarea.element.value = '1';
		textarea.trigger('input');

		expect(wrapper.vm.countCharacter).toBe(1);
		expect(wrapper.emitted('input')).toBeTruthy();
	});

	it('Should set height automatically of textarea when autoGrow is active', () => {
		const wrapper = mountComponent({
			propsData: {
				autoGrow: true
			}
		});

		const textarea = wrapper.find({ ref: 'textarea' });

		expect(textarea.attributes('style')).toBeFalsy();

		textarea.trigger('input');

		expect(textarea.attributes('style')).toBeTruthy();
		expect(textarea.attributes('style')).toMatchSnapshot();
	});

	it('Should auto set height textarea when value changed from parent', async () => {
		const newValue = 'value from parent';
		const wrapper = mountComponent({
			propsData: {
				autoGrow: true
			}
		});

		const textarea = wrapper.find({ ref: 'textarea' });

		wrapper.setProps({ value: newValue });

		await wrapper.vm.$nextTick();
		expect(textarea.attributes('style')).toBeTruthy();
		expect(wrapper.vm.localValue).toBe(newValue);
	});

	it('Should set empty string when value from parent is null', async () => {
		const wrapper = mountComponent();

		wrapper.setData({ localValue: 'current value' });

		wrapper.setProps({ value: null });
		await wrapper.vm.$nextTick();
		expect(wrapper.vm.localValue).toBe('');
	});

	it('Emit an event focus and set "focused" state', () => {
		const wrapper = mountComponent();

		const textarea = wrapper.find({ ref: 'textarea' });

		textarea.trigger('focus');

		expect(wrapper.vm.focused).toBe(true);
		expect(wrapper.emitted('focus')).toBeTruthy();
	});

	it('Emit an event blur and set "focused" state to false', () => {
		const wrapper = mountComponent();
		wrapper.setData({ focused: true });

		const textarea = wrapper.find({ ref: 'textarea' });

		textarea.trigger('blur');

		expect(wrapper.vm.focused).toBe(false);
		expect(wrapper.emitted('blur')).toBeTruthy();
	});
});
