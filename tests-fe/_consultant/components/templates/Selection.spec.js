import { shallowMount } from '@vue/test-utils';
import Selection from 'Consultant/components/templates/Selection.vue';

describe('Selection.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return shallowMount(Selection, {
				propsData: {
					items: []
				},
				...options
			});
		};
	});

	it('Should render component and match snapshot', () => {
		const wrapper = mountComponent({
			propsData: {
				items: ['test']
			},
			slots: {
				item: 'item slot'
			}
		});

		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should set selected value with item clicked', async () => {
		const items = [{ label: 'test' }, { label: 'test 2' }];
		const indexSelected = 1;

		const wrapper = mountComponent({
			propsData: {
				items
			},
			slots: {
				item: 'item slot'
			}
		});

		const input = jest.fn();
		wrapper.vm.$on('input', input);

		wrapper
			.findAll('.selection__item')
			.at(indexSelected)
			.trigger('click');

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.valueSelected).toBe(items[indexSelected]);
		expect(input).toHaveBeenCalledWith(items[indexSelected]);
	});

	it('Should not set selected value with item clicked when disabled', async () => {
		const items = [{ label: 'test' }, { label: 'test 2', disabled: true }];
		const indexSelected = 1;

		const wrapper = mountComponent({
			propsData: {
				items
			},
			slots: {
				item: 'item slot'
			}
		});

		const input = jest.fn();
		wrapper.vm.$on('input', input);

		wrapper
			.findAll('.selection__item')
			.at(indexSelected)
			.trigger('click');

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.valueSelected).not.toBe(items[indexSelected]);
		expect(input).not.toHaveBeenCalledWith(items[indexSelected]);
	});

	it('Should not set selected value with array when using multiple props', async () => {
		const items = [{ label: 'test' }, { label: 'test 2' }];

		const wrapper = mountComponent({
			propsData: {
				items,
				multiple: true
			},
			slots: {
				item: 'item slot'
			}
		});

		wrapper
			.findAll('.selection__item')
			.at(0)
			.trigger('click');

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.valueSelected).toEqual([items[0]]);

		wrapper
			.findAll('.selection__item')
			.at(1)
			.trigger('click');

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.valueSelected).toEqual(items);
	});

	it('Should can toggle select when using multiple select', async () => {
		const items = [{ label: 'test' }, { label: 'test 2' }];
		const indexSelected = 0;

		const wrapper = mountComponent({
			propsData: {
				items,
				multiple: true
			},
			slots: {
				item: 'item slot'
			}
		});

		wrapper
			.findAll('.selection__item')
			.at(indexSelected)
			.trigger('click');

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.valueSelected).toEqual([items[0]]);

		wrapper
			.findAll('.selection__item')
			.at(indexSelected)
			.trigger('click');

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.valueSelected).toEqual([]);
	});

	it("Should can't toggle select when not using multiple select", async () => {
		const items = [{ label: 'test' }, { label: 'test 2' }];
		const indexSelected = 0;

		const wrapper = mountComponent({
			propsData: {
				items
			},
			slots: {
				item: 'item slot'
			}
		});

		wrapper
			.findAll('.selection__item')
			.at(indexSelected)
			.trigger('click');

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.valueSelected).toEqual(items[0]);

		wrapper
			.findAll('.selection__item')
			.at(indexSelected)
			.trigger('click');

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.valueSelected).toEqual(items[0]);
	});

	it("Should can't set selected value when using silent prop", async () => {
		const items = [{ label: 'test' }, { label: 'test 2' }];
		const indexSelected = 0;

		const wrapper = mountComponent({
			propsData: {
				items,
				silent: true
			},
			slots: {
				item: 'item slot'
			}
		});

		wrapper
			.findAll('.selection__item')
			.at(indexSelected)
			.trigger('click');

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.valueSelected).not.toBe(items[0]);
	});

	it('Should set selected value when value prop not empty', async () => {
		const valueParent = { label: 'test' };
		const items = [valueParent];

		const wrapper = mountComponent({
			propsData: {
				items,
				value: valueParent
			}
		});

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.valueSelected).toBe(valueParent);
	});
});
