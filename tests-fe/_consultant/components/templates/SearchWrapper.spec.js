import { mount, createLocalVue } from '@vue/test-utils';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import SearchWrapper from 'Consultant/components/templates/SearchWrapper.vue';

const localVue = createLocalVue();
mockVLazy(localVue);

describe('SearchWrapper.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return mount(SearchWrapper, {
				localVue,
				mocks: {
					$route: {
						query: {}
					}
				},
				...options
			});
		};
	});

	it('Should render component and match snapshot', () => {
		const wrapper = mountComponent({
			slots: {
				default: '<div></div>'
			}
		});

		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should auto focus when using autofocus prop and has not text of search before', async () => {
		const wrapper = mountComponent({
			propsData: {
				autofocus: true
			}
		});

		await wrapper.vm.$nextTick();

		const input = wrapper.find('.ct-input-text');
		expect(input.classes('ct-input--focus')).toBe(true);
	});

	it('Should not auto focus when using autofocus prop and has text of search before', async () => {
		const wrapper = mountComponent({
			propsData: {
				autofocus: true
			},
			mocks: {
				$route: {
					query: {
						search: 'test'
					}
				}
			}
		});

		await wrapper.vm.$nextTick();

		const input = wrapper.find('.ct-input-text');
		expect(input.classes('ct-input--focus')).toBe(false);
	});

	it('Should attach an event search when input search was pressed enter', async () => {
		const wrapper = mountComponent();

		const searchEvent = jest.fn();
		wrapper.vm.$on('search', searchEvent);

		await wrapper.vm.$nextTick();

		wrapper.find('.ct-input-text input').trigger('keydown.enter');

		expect(searchEvent).toHaveBeenCalled();
	});

	it('Should go back when has not back handler', () => {
		const go = jest.fn();
		const wrapper = mountComponent({
			mocks: {
				$route: {
					query: {}
				},
				$router: {
					go
				}
			}
		});

		wrapper.find('.arrow-btn').trigger('click');
		expect(go).toHaveBeenCalled();
	});

	it('Should emit onBack when has back handler', () => {
		const wrapper = mountComponent({
			listeners: {
				onBack: jest.fn()
			}
		});

		const onBack = jest.fn();
		wrapper.vm.$on('onBack', onBack);

		wrapper.find('.arrow-btn').trigger('click');
		expect(onBack).toHaveBeenCalled();
	});
});
