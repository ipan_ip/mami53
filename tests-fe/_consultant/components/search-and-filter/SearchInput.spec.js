import { shallowMount, createLocalVue } from '@vue/test-utils';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import SearchInput from 'Consultant/components/search-and-filter/SearchInput.vue';

const localVue = createLocalVue();
mockVLazy(localVue);

describe('SearchInput.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}, additionalProp = {}) => {
			return shallowMount(SearchInput, {
				localVue,
				propsData: {
					value: '',
					...additionalProp
				},
				...options
			});
		};
	});

	it('Should render search input and match snapshot', () => {
		const wrapper = mountComponent();

		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should render placeholder on input', () => {
		const placeholder = 'Input Placeholder';
		const wrapper = mountComponent(
			{},
			{
				placeholder
			}
		);

		const input = wrapper.find({ ref: 'input' }).element;
		expect(input.getAttribute('placeholder')).toBe(placeholder);
	});

	it('Should has class is--dirty', async () => {
		const wrapper = mountComponent();

		wrapper.setData({ searchText: 'test' });

		await wrapper.vm.$nextTick();

		expect(wrapper.classes('is--dirty')).toBe(true);
	});

	it('Should has class is--focused', async () => {
		const wrapper = mountComponent();

		wrapper.find({ ref: 'input' }).trigger('focus');

		await wrapper.vm.$nextTick();

		expect(wrapper.classes('is--focused')).toBe(true);
	});

	it('Should has search-input__custom', async () => {
		const wrapper = mountComponent({
			attrs: {
				'is-custom': true
			}
		});

		await wrapper.vm.$nextTick();

		expect(wrapper.classes('search-input__custom')).toBe(true);
	});

	it('Should change local state when its parent changed', async () => {
		const wrapper = mountComponent();
		const localValue = 'change';

		wrapper.setProps({ value: localValue });

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.isSearched).toBe(true);
		expect(wrapper.vm.searchText).toBe(localValue);
	});

	it('Should auto focus input', () => {
		const wrapper = mountComponent();
		const onFocus = jest.spyOn(wrapper.vm.$refs.input, 'focus');

		wrapper.vm.focus();

		expect(onFocus).toHaveBeenCalled();
	});

	it('Should emit an event onSearch and set blur when pressed enter', async () => {
		const wrapper = mountComponent();

		const input = wrapper.find({ ref: 'input' });
		const onBlur = jest.spyOn(input.element, 'blur');
		const onSearch = jest.fn();
		wrapper.vm.$on('onSearch', onSearch);

		input.element.value = 'test';
		input.trigger('input');
		await wrapper.vm.$nextTick();

		input.trigger('keypress.enter');

		expect(onBlur).toHaveBeenCalled();
		expect(onSearch).toHaveBeenCalled();
	});

	it('Should not emit an event onSearch when input have not value', async () => {
		const wrapper = mountComponent();

		const input = wrapper.find({ ref: 'input' });
		const onSearch = jest.fn();
		wrapper.vm.$on('onSearch', onSearch);

		await wrapper.vm.$nextTick();

		input.trigger('keypress.enter');

		expect(onSearch).not.toHaveBeenCalled();
	});

	describe('Simulate click on button back', () => {
		it('Emit an event onSearchCancel when using custom', async () => {
			const wrapper = mountComponent({
				attrs: {
					'is-custom': true
				}
			});

			const onSearchCancel = jest.fn();
			wrapper.vm.$on('onSearchCancel', onSearchCancel);

			wrapper.find('.search-input__close img').trigger('click');

			expect(onSearchCancel).toHaveBeenCalled();
		});

		it('Should blur input when focused in custom mode', async () => {
			const wrapper = mountComponent({
				attrs: {
					'is-custom': true
				}
			});

			const onBlur = jest.spyOn(wrapper.vm.$refs.input, 'blur');

			wrapper.vm.focus();

			await wrapper.vm.$nextTick();

			wrapper.find('.search-input__close img').trigger('click');

			expect(onBlur).toHaveBeenCalled();
		});

		it('Emit an event close and reset local state', async () => {
			const wrapper = mountComponent();

			const onClose = jest.fn();
			wrapper.vm.$on('close', onClose);
			wrapper.setData({ searchText: 'test' });

			await wrapper.vm.$nextTick();

			wrapper.find('.search-input__close img').trigger('click');

			expect(wrapper.vm.searchText).toBe('');
			expect(onClose).toHaveBeenCalled();
		});

		it('Emit an event onSearch when has value from parent', async () => {
			const wrapper = mountComponent();

			const onSearch = jest.fn();
			wrapper.vm.$on('onSearch', onSearch);
			wrapper.setProps({ value: 'test' });

			await wrapper.vm.$nextTick();

			wrapper.find('.search-input__close img').trigger('click');

			expect(onSearch).toHaveBeenCalled();
		});

		it('Should blur input', async () => {
			const wrapper = mountComponent();

			const onBlur = jest.spyOn(wrapper.vm.$refs.input, 'blur');
			wrapper.setProps({ value: 'test' });
			wrapper.vm.focus();

			await wrapper.vm.$nextTick();

			wrapper.find('.search-input__close img').trigger('click');

			expect(onBlur).toHaveBeenCalled();
		});

		it('Should set local state same with parent state when empty', async () => {
			const parentState = 'parent state';
			const wrapper = mountComponent(
				{},
				{
					value: parentState
				}
			);

			wrapper.setData({ searchText: '' });
			wrapper.vm.focus();

			await wrapper.vm.$nextTick();

			wrapper.find('.search-input__close img').trigger('click');

			expect(wrapper.vm.searchText).toBe(parentState);
		});
	});
});
