import { shallowMount, createLocalVue } from '@vue/test-utils';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import SearchBoxAndFilter from 'Consultant/components/search-and-filter/SearchBoxAndFilter.vue';

const localVue = createLocalVue();
mockVLazy(localVue);

describe('SearchBoxAndFilter.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return shallowMount(SearchBoxAndFilter, {
				localVue,
				mocks: {
					$route: {
						query: {}
					}
				},
				...options
			});
		};
	});

	it('Should render component and match snapshot', () => {
		const wrapper = mountComponent();

		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should render snackbar-header slot', () => {
		const snackbarHeaderSlotText = 'test';
		const wrapper = mountComponent({
			slots: {
				'snackbar-header': snackbarHeaderSlotText
			},
			stubs: {
				snackbar: {
					template: `
					<div class="snackbar-stub">
						<slot name="snackbar-header"></slot>
					</div>
					`
				}
			}
		});

		expect(wrapper.find('.snackbar-stub').text()).toBe(snackbarHeaderSlotText);
	});

	it('Should render filter slot', () => {
		const filterSlotText = 'test';
		const wrapper = mountComponent({
			slots: {
				filter: filterSlotText
			},
			stubs: {
				snackbar: {
					template: `
					<div class="snackbar-stub">
						<slot></slot>
					</div>
					`
				}
			}
		});

		expect(wrapper.find('.snackbar-stub').text()).toBe(filterSlotText);
	});

	it('Should set searchText when have search on route', async () => {
		const searchQuery = 'test';
		const wrapper = mountComponent({
			mocks: {
				$route: {
					query: {
						search: searchQuery
					}
				}
			}
		});

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.searchText).toBe(searchQuery);
	});

	it('Should have is-search--active class and set child to focus', async () => {
		const onFocus = jest.fn();
		const wrapper = mountComponent({
			stubs: {
				'search-input': {
					template: '<input />',
					methods: {
						focus: onFocus
					}
				}
			}
		});

		wrapper.find('.input__indicator').trigger('click');
		await wrapper.vm.$nextTick();
		expect(wrapper.vm.isSearchFocused).toBe(true);
		expect(onFocus).toHaveBeenCalled();
	});

	it('Should emit an event onSearch', async () => {
		const onSearch = jest.fn();
		const onSearchText = 'test';

		const wrapper = mountComponent({
			stubs: {
				'search-input': {
					template: '<input  />',
					methods: {
						emitOnSearch() {
							this.$emit('onSearch', onSearchText);
						}
					}
				}
			}
		});

		wrapper.vm.$on('onSearch', onSearch);
		wrapper.find('input').vm.emitOnSearch();

		expect(wrapper.vm.searchText).toBe(onSearchText);
		expect(onSearch).toHaveBeenCalled();
	});
});
