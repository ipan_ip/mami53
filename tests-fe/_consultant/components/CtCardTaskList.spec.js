import { mount } from '@vue/test-utils';
import CtCardTaskList from 'Consultant/components/CtCardTaskList.vue';

describe('CtCardTaskList.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}, additionalProps = {}) => {
			return mount(CtCardTaskList, {
				propsData: {
					lists: [],
					totalList: 0,
					dataType: 'tipe data',
					...additionalProps
				},
				...options
			});
		};
	});

	it('Should render component and match snapshot', () => {
		const wrapper = mountComponent();

		expect(wrapper.element).toMatchSnapshot();
	});

	it('Should render task type correctly', () => {
		const text = 'test';
		const wrapper = mountComponent(
			{},
			{
				taskType: text
			}
		);

		expect(wrapper.find('.task-type').text()).toBe(text);
	});

	it('Should render task title correctly', () => {
		const text = 'test';
		const wrapper = mountComponent(
			{},
			{
				taskTitle: text
			}
		);

		expect(wrapper.find('.task-title').text()).toBe(text);
	});

	it('Should render task sub title correctly', () => {
		const text = 'test';
		const wrapper = mountComponent(
			{},
			{
				taskSubtitle: text
			}
		);

		expect(wrapper.find('.task-subtitle').text()).toBe(text);
	});

	it('Should render counter text correctly and mact snapshot', async () => {
		const wrapper = mountComponent(
			{},
			{
				lists: [{}],
				totalList: 1
			}
		);

		await wrapper.vm.$nextTick();

		expect(wrapper.find('.counter-text').text()).toMatchSnapshot();
	});

	it('Should render counter text when list grether than 10 and mact snapshot', async () => {
		const wrapper = mountComponent(
			{},
			{
				lists: Array.from({ length: 11 }),
				totalList: 11
			}
		);

		await wrapper.vm.$nextTick();

		expect(wrapper.find('.counter-text').text()).toMatchSnapshot();
	});

	it('Should render counter text if no list', async () => {
		const wrapper = mountComponent();

		await wrapper.vm.$nextTick();

		expect(wrapper.find('.counter-text').text()).toMatchSnapshot();
	});

	it('Should render total list according to data passed', async () => {
		const lists = [{}, {}, {}];
		const wrapper = mountComponent(
			{
				slots: {
					lists: `<div class="render-list"></div>`
				}
			},
			{
				lists: lists
			}
		);

		await wrapper.vm.$nextTick();

		const listElms = wrapper.findAll('.render-list');

		expect(listElms.length).toBe(lists.length);
	});

	it('Should render slot no-list', async () => {
		const wrapper = mountComponent({
			slots: {
				'no-list': `<div class="no-list"></div>`
			}
		});

		await wrapper.vm.$nextTick();

		expect(wrapper.find('.no-list').exists()).toBe(true);
	});

	it('Should remove footer when no list', async () => {
		const wrapper = mountComponent({
			stubs: {
				CtCardFooter: {
					template: '<div class="footer-slot"></div>'
				}
			}
		});

		await wrapper.vm.$nextTick();

		expect(wrapper.find('.footer-slot').exists()).toBe(false);
	});

	it('Should emit an event click:button', async () => {
		const wrapper = mountComponent(
			{
				stubs: {
					CtBtn: {
						template: `<button id="see-more" @click="$emit('click')"></button>`
					}
				}
			},
			{
				lists: [{}]
			}
		);

		const clickButton = jest.fn();
		wrapper.vm.$on('click:button', clickButton);

		await wrapper.vm.$nextTick();

		wrapper.find('#see-more').trigger('click');
		expect(clickButton).toHaveBeenCalled();
	});
});
