import { shallowMount, createLocalVue } from '@vue/test-utils';
import CtAvatar from 'Consultant/components/CtAvatar.vue';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

const localVue = createLocalVue();
mockVLazy(localVue);

describe('CtAvatar.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return shallowMount(CtAvatar, {
				localVue,
				...options
			});
		};
	});

	it('Should render avatar correctly with src prop', () => {
		const wrapper = mountComponent({
			propsData: {
				src: 'Test src prop'
			}
		});

		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should render avatar with default src', () => {
		const wrapper = mountComponent({
			propsData: {
				src: null
			}
		});

		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should render avatar with size from prop', () => {
		const wrapper = mountComponent({
			propsData: {
				size: '100px'
			}
		});

		expect(wrapper.html()).toMatchSnapshot();
	});
});
