import '@babel/polyfill';
import { shallowMount, createLocalVue, mount } from '@vue/test-utils';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import ConsultantPagination from 'Consultant/components/ConsultantPagination.vue';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

const localVue = createLocalVue();
mockVLazy(localVue);

describe('ConsultantPagination.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return shallowMount(ConsultantPagination, {
				localVue,
				...options
			});
		};
	});

	it('Should render pagination and match snapshot', () => {
		const wrapper = mountComponent({
			propsData: {
				totalData: 100,
				pageActive: 1
			}
		});

		expect(wrapper.element).toMatchSnapshot();
	});

	it('Should emit change when pagination has been changed', () => {
		const changePagination = jest.fn();
		const pageChanges = 2;
		const VuePaginate = {
			template: `
			<div id="vue-paginate" @click="clickHandler(${pageChanges})">
				<slot name="breakViewContent"></slot>
			</div>
			`,
			props: [
				'value',
				'pageCount',
				'pageRange',
				'prevText',
				'nextText',
				'clickHandler'
			]
		};
		const wrapper = mountComponent({
			propsData: {
				totalData: 100,
				pageActive: 1
			},
			stubs: {
				paginate: VuePaginate
			}
		});

		wrapper.vm.$on('change', changePagination);

		wrapper.find('#vue-paginate').trigger('click');
		expect(changePagination).toHaveBeenCalledWith(pageChanges);
	});

	it('Should change activePagination when pageActive is change', async () => {
		const wrapper = mountComponent({
			propsData: {
				totalData: 100,
				pageActive: 1
			}
		});

		expect(wrapper.vm.activePagination).toBe(1);
		wrapper.setProps({ pageActive: 2 });
		await wrapper.vm.$nextTick();
		expect(wrapper.vm.activePagination).toBe(2);
	});

	it('pageRange should be 2', () => {
		const wrapper = mountComponent({
			propsData: {
				totalData: 100,
				pageActive: 1
			}
		});

		expect(wrapper.vm.pageRange).toBe(2);
	});
});
