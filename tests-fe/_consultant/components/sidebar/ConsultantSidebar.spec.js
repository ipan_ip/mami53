import '@babel/polyfill';
import { shallowMount, createLocalVue } from '@vue/test-utils';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import ConsultantSidebar from 'Consultant/components/sidebar/ConsultantSidebar.vue';
import MenuItem from 'Consultant/components/sidebar/MenuItem';

import { flushPromises } from 'tests-fe/utils/promises';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import EventBus from 'Consultant/event-bus';

const localVue = createLocalVue();
mockVLazy(localVue);

jest.useFakeTimers();

describe('ConsultantSidebar.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return shallowMount(ConsultantSidebar, {
				localVue,
				propsData: {
					value: false
				},
				mocks: {
					$store: {
						getters: {
							isAdminConsultant: true
						}
					}
				},
				...options
			});
		};
	});

	it('Should render sidebar menu and match snapshot', () => {
		const wrapper = mountComponent({
			stubs: {
				MenuItem
			}
		});
		expect(wrapper.element).toMatchSnapshot();
	});

	it('Should open snackbar', async () => {
		const wrapper = mountComponent();

		await wrapper.vm.$nextTick();

		wrapper.setProps({ value: true });

		jest.runAllTimers();

		await wrapper.vm.$nextTick();

		expect(wrapper.classes('--is-opened')).toBeTruthy();
	});

	it('Should close sidebar when background has clicked', async () => {
		const wrapper = mountComponent();

		await wrapper.vm.$nextTick();

		wrapper.setProps({ value: true });

		jest.runAllTimers();

		await wrapper.vm.$nextTick();

		expect(wrapper.classes('--is-opened')).toBeTruthy();
		wrapper.find('.consultant-sidebar__bg').trigger('click');

		expect(wrapper.emitted('input')).toBeTruthy();
	});

	it('Should close sidebar when button has clicked', async () => {
		const wrapper = mountComponent();

		await wrapper.vm.$nextTick();

		wrapper.setProps({ value: true });

		jest.runAllTimers();

		await wrapper.vm.$nextTick();

		expect(wrapper.classes('--is-opened')).toBeTruthy();
		wrapper.find('.close-button img').trigger('click');

		expect(wrapper.emitted('input')).toBeTruthy();
	});

	it('should hide menu non consultant user', async () => {
		const wrapper = mountComponent();

		wrapper.vm.$store.getters.isAdminConsultant = false;

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.sidebarMenus).toEqual(
			expect.arrayContaining([
				expect.objectContaining({ id: 'activity', hide: true })
			])
		);
	});

	describe('Test Dot Notification', () => {
		const ErrorHandler = jest.fn();

		beforeAll(() => {
			global.bugsnagClient = {
				notify: ErrorHandler
			};
		});

		it('should throw error when notification data not object', () => {
			const wrapper = mountComponent();
			wrapper.vm.handleSetNotificationOnMenu(null);
			const error = new TypeError(
				'type of bus event "SET_NOTIFICATION_ON_SIDEBAR" must object'
			);
			expect(ErrorHandler).toHaveBeenCalledWith(error);
		});

		it('should set hasNotify to menu parent that has match key', () => {
			const dataMenus = [
				{
					key: 'match-key',
					hasNotify: false
				}
			];
			const wrapper = mountComponent();
			wrapper.setData({ sidebarMenus: dataMenus });

			wrapper.vm.handleSetNotificationOnMenu({ 'match-key': 1 });
			expect(wrapper.vm.sidebarMenus).toEqual(
				expect.arrayContaining([
					{
						key: 'match-key',
						hasNotify: true
					}
				])
			);
		});

		it('should set hasNotify to menu child and its parent that has match key', () => {
			const dataMenus = [
				{
					hasNotify: false,
					child: [
						{
							key: 'match-key',
							hasNotify: false
						}
					]
				}
			];
			const wrapper = mountComponent();
			wrapper.setData({ sidebarMenus: dataMenus });

			wrapper.vm.handleSetNotificationOnMenu({ 'match-key': 1 });
			expect(wrapper.vm.sidebarMenus).toEqual(
				expect.arrayContaining([
					{
						hasNotify: true,
						child: expect.arrayContaining([
							{
								key: 'match-key',
								hasNotify: true
							}
						])
					}
				])
			);
		});
	});

	describe('When Menu has been clicked', () => {
		it.skip('should emit an event bus REFRESH_DASHBOARD', async () => {
			const pathName = 'destination-name';
			const menuItemData = { pathName: pathName };
			const wrapper = mountComponent({
				stubs: {
					'menu-item': {
						template: `
							<li @click="$emit('click', menu)"></li>
						`,
						data: () => ({
							menu: menuItemData
						})
					}
				},
				mocks: {
					$route: {
						name: pathName
					},
					$store: {
						getters: {
							isAdminConsultant: true
						}
					}
				}
			});

			await wrapper.vm.$nextTick();

			wrapper.find('.sidebar__menu li').trigger('click');
			// eslint-disable-next-line no-undef
			expect(EventBus.$emit).toHaveBeenCalledWith('REFRESH_DASHBOARD');
		});

		it('should go to page according to data passed', async () => {
			const pathName = 'destination-name';
			const menuItemData = { pathName: pathName };
			const push = jest.fn().mockReturnValue({ catch: jest.fn() });
			const wrapper = mountComponent({
				stubs: {
					'menu-item': {
						template: `
							<li @click="$emit('click', menu)"></li>
						`,
						data: () => ({
							menu: menuItemData
						})
					}
				},
				mocks: {
					$route: {
						name: 'current-name'
					},
					$router: {
						push
					},
					$store: {
						getters: {
							isAdminConsultant: true
						}
					}
				}
			});

			await wrapper.vm.$nextTick();

			wrapper.find('.sidebar__menu li').trigger('click');
			expect(push).toHaveBeenCalledWith({ name: pathName });
		});

		it('should open snackbar when action type is logout', async () => {
			// eslint-disable-next-line standard/no-callback-literal
			const menuItemData = { action: cb => cb('logout'), pathName: null };
			const wrapper = mountComponent({
				stubs: {
					'menu-item': {
						template: `
							<li @click="$emit('click', menu)"></li>
						`,
						data: () => ({
							menu: menuItemData
						})
					}
				}
			});

			await wrapper.vm.$nextTick();

			expect(wrapper.vm.snackbarOpened).toBe(false);
			wrapper.find('.sidebar__menu li').trigger('click');
			expect(wrapper.vm.snackbarOpened).toBe(true);
		});
	});

	describe('When logout', () => {
		let wrapper = Function;
		const axios = {
			getSuccess: jest.fn(() => {
				return Promise.resolve({
					data: {
						status: 'success'
					}
				});
			})
		};
		const removeCookie = jest.fn();
		const openAlert = jest.fn();

		global.Cookies = { remove: removeCookie };

		beforeEach(() => {
			EventBus.$on('openAlert', openAlert);

			global.axios = {
				get: axios.getSuccess
			};

			wrapper = mountComponent({
				propsData: {
					value: true
				},
				stubs: {
					ConsultantButton: {
						template: `<button class="logout" @click="$emit('click')"></button>`
					}
				},
				mocks: {
					$store: {
						commit: jest.fn(),
						getters: {
							isAdminConsultant: true
						}
					}
				}
			});
			wrapper.setData({ snackbarOpened: true });
		});

		it('should redirect when logout success', async () => {
			const replace = jest.fn();
			mockWindowProperty('window.location', {
				origin: 'test',
				replace
			});
			await wrapper.vm.$nextTick();
			const button = wrapper.findAll('.logout').at(1);
			button.trigger('click');

			await flushPromises();

			expect(replace).toHaveBeenCalledWith('test/consultant-tools/');
		});

		it("Should show error message when failed to logout", async () => {
			global.axios = { get: jest.fn().mockRejectedValue({
				response: { status: 500 }
			})};

			await wrapper.vm.$nextTick();
			const button = wrapper.findAll('.logout').at(1);
			button.trigger('click');

			await flushPromises();

			expect(openAlert).toHaveBeenCalledWith(
				'Logout gagal silahkan coba lagi atau refresh halaman anda.'
			);
		});
	});
});
