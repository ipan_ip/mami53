import '@babel/polyfill';
import { shallowMount, createLocalVue } from '@vue/test-utils';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import MenuItem from 'Consultant/components/sidebar/MenuItem.vue';

const localVue = createLocalVue();
mockVLazy(localVue);

describe('MenuItem.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return shallowMount(MenuItem, options);
		};
	});

	it('Should render Menu item and match snapshot', () => {
		const wrapper = mountComponent({
			propsData: {
				item: {
					title: 'Kelola Properti',
					icon: 'ic_home-consultant_green.svg',
					hasNotify: false,
					pathName: 'property',
					action: null
				}
			}
		});
		expect(wrapper.element).toMatchSnapshot();
	});

	it('Should render child menu and match snapshot', () => {
		const wrapper = mountComponent({
			propsData: {
				item: {
					title: 'Kelola Tenant',
					icon: 'ic_user-consultant_green.svg',
					hasNotify: false,
					action: null,
					expanded: false,
					child: [
						{
							title: 'Penyewa Aktif',
							hasNotify: false,
							pathName: 'active-tenant',
							key: 'recurring'
						}
					]
				}
			}
		});
		expect(wrapper.element).toMatchSnapshot();
	});

	it('emit an event click when menu clicked', async () => {
		const item = {
			title: 'Kelola Properti',
			icon: 'ic_home-consultant_green.svg',
			hasNotify: false,
			pathName: 'property',
			action: null
		};
		const eventClick = jest.fn();
		const wrapper = mountComponent({
			propsData: {
				item
			}
		});

		await wrapper.vm.$nextTick();

		wrapper.vm.$on('click', eventClick);
		wrapper.find('.menu-item > .menu__content').trigger('click');
		expect(eventClick).toHaveBeenCalledWith(item);
	});

	it('should not emit an event click when disabled', async () => {
		const item = {
			title: 'Kelola Properti',
			icon: 'ic_home-consultant_green.svg',
			hasNotify: false,
			pathName: 'property',
			disabled: true,
			action: null
		};
		const eventClick = jest.fn();
		const wrapper = mountComponent({
			propsData: {
				item
			}
		});

		await wrapper.vm.$nextTick();

		wrapper.vm.$on('click', eventClick);
		wrapper.find('.menu-item > .menu__content').trigger('click');
		expect(eventClick).not.toHaveBeenCalled();
	});

	it('Should expand child when menu has childs', async () => {
		const eventClick = jest.fn();
		const wrapper = mountComponent({
			propsData: {
				item: {
					title: 'Kelola Tenant',
					icon: 'ic_user-consultant_green.svg',
					hasNotify: false,
					action: null,
					expanded: false,
					child: [
						{
							title: 'Penyewa Aktif',
							hasNotify: false,
							pathName: 'active-tenant',
							key: 'recurring'
						}
					]
				}
			}
		});

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.menu.expanded).toBe(false);
		wrapper.find('.menu-item > .menu__content').trigger('click');
		expect(eventClick).not.toHaveBeenCalled();
		expect(wrapper.vm.menu.expanded).toBe(true);
	});

	it('emit an event click when child has been clicked', async () => {
		const childItem = {
			title: 'Penyewa Aktif',
			hasNotify: false,
			pathName: 'active-tenant',
			key: 'recurring'
		};
		const eventClick = jest.fn();
		const wrapper = mountComponent({
			propsData: {
				item: {
					title: 'Kelola Tenant',
					icon: 'ic_user-consultant_green.svg',
					hasNotify: false,
					action: null,
					expanded: false,
					child: [childItem]
				}
			}
		});

		wrapper.vm.$on('click', eventClick);

		await wrapper.vm.$nextTick();

		wrapper.find('.menu-item > .menu__content').trigger('click');

		await wrapper.vm.$nextTick();

		wrapper.find('.menu__child-item .menu__content').trigger('click');
		expect(eventClick).toHaveBeenCalledWith(childItem);
	});

	it('should not emit an event click when child disabled', async () => {
		const childItem = {
			title: 'Penyewa Aktif',
			hasNotify: false,
			pathName: 'active-tenant',
			key: 'recurring',
			disabled: true
		};
		const eventClick = jest.fn();
		const wrapper = mountComponent({
			propsData: {
				item: {
					title: 'Kelola Tenant',
					icon: 'ic_user-consultant_green.svg',
					hasNotify: false,
					action: null,
					expanded: false,
					child: [childItem]
				}
			}
		});

		wrapper.vm.$on('click', eventClick);

		await wrapper.vm.$nextTick();

		wrapper.find('.menu-item > .menu__content').trigger('click');

		await wrapper.vm.$nextTick();

		wrapper.find('.menu__child-item .menu__content').trigger('click');
		expect(eventClick).not.toHaveBeenCalled();
	});
});
