import { shallowMount } from '@vue/test-utils';
import CtCardFunnel from 'Consultant/components/CtCardFunnel.vue';

describe('Consultant/components/CtCardFunnel.vue', () => {
	const mockPropTo = { name: 'route-name' };

	const mocks = {
		$router: { push: jest.fn() }
	};

	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return shallowMount(CtCardFunnel, {
				mocks,
				...options
			});
		};
	});

	test('Should render correctly', async () => {
		const tree = shallowMount(CtCardFunnel, {
			propsData: {
				title: 'test title',
				subtitle: 'test subtitle'
			}
		});

		expect(tree.element).toMatchSnapshot();
	});

	test('No backgroud gradient if disabled', async () => {
		const wrapper = mountComponent({
			propsData: {
				disabled: true
			}
		});

		expect(wrapper.find('.outer').classes('gradient-blue')).toBe(false);
	});

	test('Click emit parent method if there is no "to" prop', async () => {
		const wrapper = mountComponent();

		wrapper.vm.handleClick();

		expect(wrapper.emitted('click')).toBeTruthy();
	});

	test('Click emit parent method if there is no "to" prop', async () => {
		const wrapper = mountComponent({
			propsData: {
				to: mockPropTo
			}
		});

		wrapper.vm.handleClick();

		expect(wrapper.vm.$router.push).toBeCalledWith(mockPropTo);
	});
});
