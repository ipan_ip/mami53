import { mount } from '@vue/test-utils';
import CtRadioGroupIcon from 'Consultant/components/CtRadioGroupIcon.vue';

describe('CtRadioGroupIcon.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return mount(CtRadioGroupIcon, {
				...options
			});
		};
	});

	it('Should render component and match snapshot', () => {
		const wrapper = mountComponent({
			propsData: {
				items: [{ label: 'test' }],
				label: 'label'
			}
		});

		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should emit an event input when item has been clicked', async () => {
		const value = 'test value';
		const items = [{ label: 'test', value }, { label: 'test 2' }];

		const wrapper = mountComponent({
			propsData: {
				items
			}
		});

		const input = jest.fn();
		wrapper.vm.$on('input', input);

		wrapper
			.findAll('.selection__item')
			.at(0)
			.trigger('click');

		await wrapper.vm.$nextTick();

		expect(input).toHaveBeenCalledWith(value);
	});

	it('Should not emit an event input when item has disabled', async () => {
		const items = [{ label: 'test', disabled: true }];

		const wrapper = mountComponent({
			propsData: {
				items
			}
		});

		const input = jest.fn();
		wrapper.vm.$on('input', input);

		wrapper
			.findAll('.selection__item')
			.at(0)
			.trigger('click');

		await wrapper.vm.$nextTick();

		expect(input).not.toHaveBeenCalled();
	});

	it('Should emit with object when using return object', async () => {
		const value = { label: 'test' };
		const items = [value];

		const wrapper = mountComponent({
			propsData: {
				items,
				returnObject: true
			}
		});

		const input = jest.fn();
		wrapper.vm.$on('input', input);

		wrapper
			.findAll('.selection__item')
			.at(0)
			.trigger('click');

		await wrapper.vm.$nextTick();

		expect(input).toHaveBeenCalledWith(value);
	});

	it('Should set local valueSelected with object', async () => {
		const valueLocal = { label: 'test', value: 1 };
		const items = [valueLocal];

		const wrapper = mountComponent({
			propsData: {
				items,
				value: 1
			}
		});

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.valueSelected).toBe(valueLocal);
	});
});
