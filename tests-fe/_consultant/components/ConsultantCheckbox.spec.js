import { mount } from '@vue/test-utils';
import Checkbox from 'Consultant/components/ConsultantCheckbox.vue';

describe('ConsultantCheckbox.vue', () => {
	let createWrapper = Function;

	beforeEach(() => {
		createWrapper = (options = {}) => {
			return mount(Checkbox, options);
		};
	});

	it('Should render checkbox component and match snapshot', () => {
		const wrapper = createWrapper({
			propsData: {
				value: false
			}
		});
		expect(wrapper.element).toMatchSnapshot();
	});

	it('Should render label from prop and match snapshot', () => {
		const Label = 'Checkbox Label';
		const wrapper = createWrapper({
			propsData: {
				value: false,
				label: Label
			}
		});
		expect(wrapper.find('label.checkbox').text()).toBe(Label);
		expect(wrapper.element).toMatchSnapshot();
	});

	it('Should render slot label event though it has label prop', () => {
		const labelProp = 'Checkbox label prop';
		const labelSlot = 'Checkbox label slot';
		const wrapper = createWrapper({
			propsData: {
				value: false,
				label: labelProp
			},
			slots: {
				label: labelSlot
			}
		});
		expect(wrapper.find('label.checkbox').text()).toBe(labelSlot);
		expect(wrapper.element).toMatchSnapshot();
	});

	it('Emit events when checkbox changed', () => {
		const changeEmit = jest.fn();
		const inputEmit = jest.fn();

		const wrapper = createWrapper({
			propsData: {
				value: false
			},
			stubs: {
				checkbox: `<input id="checkbox" @input="$emit('input')" />`
			}
		});

		wrapper.vm.$on('input', inputEmit);
		wrapper.vm.$on('change', changeEmit);

		wrapper.find('#checkbox').trigger('input');

		expect(changeEmit).toHaveBeenCalled();
		expect(inputEmit).toHaveBeenCalled();
	});

	it('Should change local value when its parent has changed', async () => {
		const wrapper = createWrapper({
			propsData: {
				value: false
			}
		});

		expect(wrapper.vm.checked).toBe(false);
		wrapper.setProps({ value: true });
		await wrapper.vm.$nextTick();
		expect(wrapper.vm.checked).toBe(true);
	});
});
