import { shallowMount } from '@vue/test-utils';
import CtIcon from 'Consultant/components/CtIcon.vue';

describe('CtIcon.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return shallowMount(CtIcon, options);
		};
	});

	it('Should match snapshot when using default slot', () => {
		const wrapper = mountComponent({
			slots: {
				default: 'default-slot-name'
			}
		});

		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should match snapshot when using name prop', () => {
		const wrapper = mountComponent({
			propsData: {
				name: 'slot-name'
			}
		});

		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should match snapshot when using size prop', () => {
		const wrapper = mountComponent({
			propsData: {
				size: '10px'
			}
		});

		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should match snapshot when using color prop', () => {
		const wrapper = mountComponent({
			propsData: {
				color: 'green'
			}
		});

		expect(wrapper.html()).toMatchSnapshot();
	});
});
