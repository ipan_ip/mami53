import { mount } from '@vue/test-utils';
import ActiveTenantDetailPlaceholder from 'Consultant/components/placeholders/ActiveTenantDetailPlaceholder.vue';

describe('ActiveTenantDetailPlaceholder.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return mount(ActiveTenantDetailPlaceholder, options);
		};
	});

	it('Should render component and match snapshot', () => {
		const wrapper = mountComponent();

		expect(wrapper.html()).toMatchSnapshot();
	});
});
