import { shallowMount } from '@vue/test-utils';
import ShimmerPlaceholder from 'Consultant/components/placeholders/ShimmerPlaceholder.vue';

describe('ShimmerPlaceholder.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return shallowMount(ShimmerPlaceholder, options);
		};
	});

	it('Should render component with default props', () => {
		const wrapper = mountComponent();

		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should render style with height', () => {
		const wrapper = mountComponent({
			propsData: {
				height: '100px'
			}
		});

		expect(wrapper.attributes('style')).toMatchSnapshot();
	});

	it('Should render style with width', () => {
		const wrapper = mountComponent({
			propsData: {
				width: '100px'
			}
		});

		expect(wrapper.attributes('style')).toMatchSnapshot();
	});
});
