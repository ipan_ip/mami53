import { mount } from '@vue/test-utils';
import PotentialTenantDetailPlaceholder from 'Consultant/components/placeholders/PotentialTenantDetailPlaceholder.vue';

describe('PotentialTenantDetailPlaceholder.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return mount(PotentialTenantDetailPlaceholder, options);
		};
	});

	it('Should render component and match snapshot', () => {
		const wrapper = mountComponent();

		expect(wrapper.html()).toMatchSnapshot();
	});
});
