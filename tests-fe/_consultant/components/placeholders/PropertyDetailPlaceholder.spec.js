import { mount } from '@vue/test-utils';
import PropertyDetailPlaceholder from 'Consultant/components/placeholders/PropertyDetailPlaceholder.vue';

describe('PropertyDetailPlaceholder.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return mount(PropertyDetailPlaceholder, options);
		};
	});

	it('Should render component and match snapshot', () => {
		const wrapper = mountComponent();

		expect(wrapper.html()).toMatchSnapshot();
	});
});
