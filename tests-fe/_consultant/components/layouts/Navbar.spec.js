import { shallowMount, createLocalVue } from '@vue/test-utils';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import Navbar from 'Consultant/components/layouts/Navbar.vue';
import { isEmpty } from 'lodash';
import VueRouter from 'vue-router';

const localVue = createLocalVue();
mockVLazy(localVue);

jest.mock('Consultant/event-bus/index', () => {
	const EventBus = {
		$on: jest.fn(),
		$off: jest.fn()
	};
	global.EventBus = EventBus;
	return global.EventBus;
});

describe('Navbar.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			const additionalMocks = options.mocks || {};
			return shallowMount(Navbar, {
				localVue,
				stubs: {
					ConsultantSidebar: {
						template: '<div class="sidebar"></div>'
					}
				},
				mocks: {
					$isEmpty: isEmpty,
					...additionalMocks
				}
			});
		};
	});

	it('Should render navbar and match snapshot', () => {
		const wrapper = mountComponent();

		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should show burger icon when burger on route is true', async () => {
		const wrapper = mountComponent({
			mocks: {
				$route: {
					meta: {
						navbarOptions: {
							actionIcons: {
								burger: true
							}
						},
						sidebar: true
					}
				}
			}
		});

		await wrapper.vm.$nextTick();

		expect(wrapper.find('.right-action')).toMatchSnapshot();
	});

	it('Should show icon turn back when navbar is secondary', async () => {
		const wrapper = mountComponent({
			mocks: {
				$route: {
					meta: {
						navbarOptions: {
							title: 'Navbar title'
						}
					}
				}
			}
		});

		await wrapper.vm.$nextTick();

		expect(wrapper.findAll('.left-action').at(0)).toMatchSnapshot();
	});

	it('Should have class consultant-navbar--primary', async () => {
		const wrapper = mountComponent({
			mocks: {
				$route: {
					meta: {
						navbarOptions: {
							primary: true
						}
					}
				}
			}
		});

		await wrapper.vm.$nextTick();

		expect(wrapper.classes('consultant-navbar--primary')).toBe(true);
	});

	it('Should have class consultant-navbar--secondary', async () => {
		const wrapper = mountComponent({
			mocks: {
				$route: {
					meta: {
						navbarOptions: {
							title: 'Navbar title'
						}
					}
				}
			}
		});

		await wrapper.vm.$nextTick();

		expect(wrapper.classes('consultant-navbar--secondary')).toBe(true);
	});

	it('Should have navbar extended', async () => {
		const wrapper = mountComponent({
			mocks: {
				$route: {
					meta: {
						navbarExtended: true
					}
				}
			}
		});

		await wrapper.vm.$nextTick();

		expect(wrapper.classes('has-extend')).toBe(true);
		expect(wrapper.find('.consultant-navbar__extended').exists()).toBe(true);
	});

	it('Should open side bar when burger clicked', async () => {
		const wrapper = mountComponent({
			mocks: {
				$route: {
					meta: {
						navbarOptions: {
							actionIcons: {
								burger: true
							}
						},
						sidebar: true
					}
				}
			}
		});

		wrapper.find('.drawer-activator').trigger('click');
		await wrapper.vm.$nextTick();
		expect(wrapper.vm.sidebarOpen).toBe(true);
	});

	it('Should execute onClickBack on navbar options', async () => {
		const onClickBack = jest.fn();
		const wrapper = mountComponent({
			mocks: {
				$route: {
					meta: {
						navbarOptions: {
							onClickBack
						}
					}
				}
			}
		});

		wrapper.find('.arrow-right').trigger('click');

		expect(onClickBack).toHaveBeenCalled();
	});

	it('Should push to prevPathName', async () => {
		const pathName = 'previous-path';
		const push = jest.fn();
		const wrapper = mountComponent({
			mocks: {
				$route: {
					meta: {
						navbarOptions: {
							prevPathName: pathName
						}
					}
				},
				$router: {
					push
				}
			}
		});

		wrapper.find('.arrow-right').trigger('click');

		expect(push).toHaveBeenCalledWith({ name: pathName });
	});

	it('Should back to previous path', async () => {
		const go = jest.fn();
		const wrapper = mountComponent({
			mocks: {
				$route: {
					meta: {
						navbarOptions: {
							prevPathName: null
						}
					}
				},
				$router: {
					go
				}
			}
		});

		wrapper.find('.arrow-right').trigger('click');

		expect(go).toHaveBeenCalledWith(-1);
	});

	it('Should close sidebar when route changed', async () => {
		const Local = createLocalVue();
		Local.use(VueRouter);
		const router = new VueRouter({
			routes: [{ path: '/new-path' }]
		});

		const wrapper = shallowMount(Navbar, {
			localVue: Local,
			router,
			stubs: {
				ConsultantSidebar: {
					template: '<div class="sidebar"></div>'
				}
			},
			mocks: {
				$isEmpty: isEmpty
			}
		});

		wrapper.setData({ sidebarOpen: true });

		await wrapper.vm.$nextTick();
		expect(wrapper.vm.sidebarOpen).toBe(true);

		router.push({ path: '/new-path' });

		await wrapper.vm.$nextTick();
		expect(wrapper.vm.sidebarOpen).toBe(false);
	});
});
