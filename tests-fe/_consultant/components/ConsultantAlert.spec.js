import { shallowMount, createLocalVue } from '@vue/test-utils';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import Alert from 'Consultant/components/ConsultantAlert.vue';
import EventBus from 'Consultant/event-bus';

jest.useFakeTimers();

const localVue = createLocalVue();
mockVLazy(localVue);

describe('ConsultantAlert.vue', () => {
	let wrapper;

	beforeEach(() => {
		wrapper = shallowMount(Alert, { localVue });
	});

	it('Should render alert and match snapshot', () => {
		expect(wrapper.find('.consultant-alert').exists()).toBeTruthy();
		expect(wrapper.html()).toMatchSnapshot();
	});

	it('must reset all state', () => {
		wrapper.setData({
			currentType: 'message',
			messages: ['message'],
			closeTimeout: 1000,
			autoClose: false
		});
		wrapper.vm.resetAlertState();
		const { currentType, messages, closeTimeout, autoClose } = wrapper.vm.$data;
		expect(currentType).toBe('error');
		expect(messages).toHaveLength(0);
		expect(closeTimeout).toBe(6000);
		expect(autoClose).toBe(true);
	});

	describe('Test opening alert', () => {
		it('should open alert by event bus', () => {
			expect(wrapper.vm.isOpen).toBe(false);

			EventBus.$emit('openAlert', 'test');

			jest.runAllTimers();

			expect(wrapper.vm.isOpen).toBe(true);
		});

		it('should not open alert when has not message', () => {
			expect(wrapper.vm.isOpen).toBe(false);

			EventBus.$emit('openAlert');

			jest.runAllTimers();

			expect(wrapper.vm.isOpen).toBe(false);
		});

		it('should render message and match snapshot', async () => {
			const message = 'test message';

			EventBus.$emit('openAlert', message);

			jest.runAllTimers();
			await wrapper.vm.$nextTick();
			expect(wrapper.vm.messages).toEqual([message]);
			expect(wrapper.find('.message__container').html()).toMatchSnapshot();
		});

		it('should render message with array type', async () => {
			const messages = ['test message 1', 'test message 2'];

			EventBus.$emit('openAlert', messages);

			jest.runAllTimers();

			expect(wrapper.vm.messages).toEqual(messages);
		});

		it('should render message with array type', async () => {
			const messages = ['test message 1', 'test message 2'];

			EventBus.$emit('openAlert', messages);

			jest.runAllTimers();

			expect(wrapper.vm.messages).toEqual(messages);
		});

		it('should render alert with default type', async () => {
			EventBus.$emit('openAlert', 'test', 'test');

			jest.runAllTimers();

			await wrapper.vm.$nextTick();
			const alertWrapper = wrapper.find('.alert__wrapper');
			expect(alertWrapper.classes(`alert--error`)).toBe(true);
			expect(wrapper.vm.alertClassName).toEqual({
				'alert--error': true
			});
		});

		it('should render alert success when using type success', async () => {
			const type = 'success';

			EventBus.$emit('openAlert', 'test', type);

			jest.runAllTimers();

			expect(wrapper.vm.currentType).toBe(type);

			await wrapper.vm.$nextTick();
			const alertWrapper = wrapper.find('.alert__wrapper');
			expect(alertWrapper.classes(`alert--${type}`)).toBe(true);
		});

		it('should render alert in bottom when using bottom placement', async () => {
			const placement = 'bottom';

			EventBus.$emit('openAlert', 'test', 'error', { placement });

			jest.runAllTimers();

			expect(wrapper.vm.options.placement).toBe(placement);
		});

		it('should replace closeTimeout with option passed', async () => {
			const timeout = '1000';

			EventBus.$emit('openAlert', 'test', 'error', { timeout });

			jest.runAllTimers();

			expect(wrapper.vm.closeTimeout).toBe(timeout);
		});

		it('should clear timeout when open repeatly', async () => {
			wrapper.vm.handleAutoCloseAlert(true);
			jest.advanceTimersByTime(1000);
			wrapper.vm.handleAutoCloseAlert(true);
			jest.advanceTimersByTime(1000);
			expect(clearTimeout).toHaveBeenCalled();
		});
	});

	describe('Test closing alert', () => {
		it('should auto close alert', async () => {
			EventBus.$emit('openAlert', 'test');

			jest.advanceTimersByTime(300);

			await wrapper.vm.$nextTick();

			expect(wrapper.vm.isOpen).toBe(true);

			jest.advanceTimersByTime(wrapper.vm.closeTimeout);

			expect(wrapper.vm.isOpen).toBe(false);
		});

		it('should not auto close alert', async () => {
			EventBus.$emit('openAlert', 'test', 'error', { autoClose: false });

			jest.advanceTimersByTime(300);

			await wrapper.vm.$nextTick();

			expect(wrapper.vm.isOpen).toBe(true);

			jest.advanceTimersByTime(wrapper.vm.closeTimeout);

			expect(wrapper.vm.isOpen).toBe(true);
		});
	});

	it('should remove bus openAlert before detroy component', () => {
		const busOff = jest.spyOn(EventBus, '$off');
		expect(busOff).not.toHaveBeenCalled();

		wrapper.destroy();

		expect(busOff).toHaveBeenCalled();
	});
});
