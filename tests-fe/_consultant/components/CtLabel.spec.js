import { shallowMount } from '@vue/test-utils';
import CtLabel from 'Consultant/components/CtLabel.vue';

describe('CtLabel.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return shallowMount(CtLabel, options);
		};
	});

	it('Should render component and match snapshot', () => {
		const wrapper = mountComponent({
			slots: {
				default: 'Test Label'
			}
		});

		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should render label with mandatory sign', () => {
		const wrapper = mountComponent({
			propsData: {
				mandatory: true
			},
			slots: {
				default: 'Test Label'
			}
		});

		expect(wrapper.html()).toMatchSnapshot();
	});
});
