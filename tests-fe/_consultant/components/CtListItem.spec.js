import { shallowMount } from '@vue/test-utils';
import CtListItem from 'Consultant/components/CtListItem.vue';

describe('CtListItem.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return shallowMount(CtListItem, options);
		};
	});

	it('Should render component', () => {
		const wrapper = mountComponent();

		expect(wrapper.classes('ct-list-item')).toBe(true);
	});

	it('Should action and default slot', () => {
		const wrapper = mountComponent({
			slots: {
				default: '<div>default slot</div>',
				action: '<div>action slot</div>'
			}
		});

		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should have divider by default', () => {
		const wrapper = mountComponent();

		expect(wrapper.classes('item--has-divider')).toBe(true);
	});

	it('Should have fulldivider', () => {
		const wrapper = mountComponent({
			propsData: {
				fullDivider: true
			}
		});

		expect(wrapper.classes('item--full-divider')).toBe(true);
	});

	it('Should have aria disabled', () => {
		const wrapper = mountComponent({
			propsData: {
				disabled: true
			}
		});

		expect(wrapper.attributes('aria-disabled')).toBeTruthy();
	});

	it('Should emit an event click', () => {
		const wrapper = mountComponent({
			listeners: {
				click: jest.fn()
			}
		});
		const click = jest.fn();

		wrapper.vm.$on('click', click);

		wrapper.trigger('click');

		expect(click).toHaveBeenCalled();
	});

	it('Should not emit an event click', () => {
		const wrapper = mountComponent();
		const click = jest.fn();

		wrapper.vm.$on('click', click);

		wrapper.trigger('click');

		expect(click).not.toHaveBeenCalled();
	});

	it('Should not emit an event click when disabled', () => {
		const wrapper = mountComponent({
			propsData: {
				disabled: true
			},
			listeners: {
				click: jest.fn()
			}
		});

		const click = jest.fn();

		wrapper.vm.$on('click', click);

		wrapper.trigger('click');

		expect(click).not.toHaveBeenCalled();
	});

	it('Should emit an event onContentClick', () => {
		const wrapper = mountComponent();

		const onContentClick = jest.fn();

		wrapper.vm.$on('onContentClick', onContentClick);

		wrapper.find('.ct-list__content').trigger('click');

		expect(onContentClick).toHaveBeenCalled();
	});
});
