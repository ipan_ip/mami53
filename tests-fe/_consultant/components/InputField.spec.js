import { shallowMount, createLocalVue } from '@vue/test-utils';
import InputField from 'Consultant/components/InputField.vue';
import VeeValidate from 'Consultant/config/veeValidate';

const localVue = createLocalVue();

localVue.use(VeeValidate, {
	locale: 'id'
});

jest.mock('Consultant/event-bus/index', () => {
	const EventBus = {
		$on: jest.fn()
	};
	global.EventBus = EventBus;
	return global.EventBus;
});

describe('InputField.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return shallowMount(InputField, {
				localVue,
				...options
			});
		};
	});

	it('Should render input component and match snapshot', () => {
		const wrapper = mountComponent();
		expect(wrapper.element).toMatchSnapshot();
	});

	it('Should render legend and match snapshot', () => {
		const legend = 'Input Legend';
		const wrapper = mountComponent({
			propsData: {
				legend: legend
			}
		});

		expect(wrapper.find('.consultant-input__legend').text()).toBe(legend);
		expect(wrapper.element).toMatchSnapshot();
	});

	it('Should has mandatory sign', () => {
		const wrapper = mountComponent({
			propsData: {
				mandatorySign: true
			}
		});

		expect(wrapper.classes('has-mandatory-sign')).toBe(true);
	});

	it('should implement attribute to input', () => {
		const wrapper = mountComponent({
			attrs: {
				required: 'required'
			}
		});

		const input = wrapper.find('input');
		expect(input.element.getAttribute('required')).toBeTruthy();
	});

	it('should have rounded according to rounded prop passed', () => {
		const wrapper = mountComponent({
			propsData: {
				rounded: 10
			}
		});

		const input = wrapper.find('input');
		expect(input.attributes('style')).toMatchSnapshot();
	});

	it('Should have radius if rounded prop has value as string', () => {
		const wrapper = mountComponent({
			propsData: {
				rounded: '20px'
			}
		});

		expect(wrapper.find('input').attributes('style')).toMatchSnapshot();
	});

	it('Should have radius 3px as default rounded', () => {
		const wrapper = mountComponent({
			propsData: {
				rounded: 'unknown'
			}
		});

		expect(wrapper.vm.roundedConversion).toBe('3px');
		expect(wrapper.find('input').attributes('style')).toMatchSnapshot();
	});

	it('Should have class is--solo', async () => {
		const wrapper = mountComponent({
			attrs: {
				solo: true
			}
		});

		await wrapper.vm.$nextTick();
		expect(wrapper.classes('is--solo')).toBe(true);
	});

	it('Should have class is--regular', async () => {
		const wrapper = mountComponent({
			attrs: {
				regular: true
			}
		});

		await wrapper.vm.$nextTick();
		expect(wrapper.classes('is--regular')).toBe(true);
	});

	it('Should have class is--regular-placeholder', async () => {
		const wrapper = mountComponent({
			attrs: {
				regular: true,
				placeholder: 'placeholder'
			}
		});

		await wrapper.vm.$nextTick();
		expect(wrapper.classes('is--regular-placeholder')).toBe(true);
	});

	it('should render append slot', async () => {
		const wrapper = mountComponent({
			slots: {
				append: '<div></div>'
			}
		});

		const append = wrapper.find('.consultant-input__append');
		expect(append).toBeTruthy();
		expect(append).toMatchSnapshot();
	});

	it('should render error message', async () => {
		const wrapper = mountComponent({
			propsData: {
				rules: 'required',
				name: 'field-name'
			}
		});

		await wrapper.vm.$validator.validate();
		expect(wrapper.vm.$validator.errors.any()).toBe(true);
		expect(wrapper.vm.isErrorMessageAppear).toBe(true);
		expect(wrapper.find('.error__message')).toMatchSnapshot();
	});

	it('should reset error', async () => {
		const wrapper = mountComponent({
			propsData: {
				rules: 'required',
				name: 'field-name'
			}
		});

		await wrapper.vm.$validator.validate();
		expect(wrapper.vm.$validator.errors.any()).toBe(true);

		wrapper.vm.resetError();
		await wrapper.vm.$nextTick();
		expect(wrapper.vm.$validator.errors.any()).toBe(false);
	});

	it('should set focus false if input has been blur', async () => {
		const wrapper = mountComponent();
		wrapper.setData({ isFocus: true });

		wrapper.find('input').trigger('blur');
		expect(wrapper.vm.isFocus).toBe(false);
	});

	it('should set focus and emit focus event', async () => {
		const focusEvent = jest.fn();
		const wrapper = mountComponent();

		wrapper.vm.$on('focus', focusEvent);

		wrapper.find('input').trigger('focus');

		expect(wrapper.vm.isFocus).toBe(true);
		expect(focusEvent).toHaveBeenCalled();
	});

	describe('Test Input With Fake Timer', () => {
		beforeEach(() => {
			jest.useFakeTimers();
		});

		it("should emit input directly if hasn't rules", async () => {
			const emitImput = jest.fn();
			const inputText = 'new data inputted';
			const wrapper = mountComponent();

			wrapper.vm.$on('input', emitImput);

			wrapper.setData({ inputData: inputText });

			await wrapper.vm.$nextTick();

			jest.advanceTimersByTime(500);

			expect(emitImput).toHaveBeenCalledWith(inputText);
		});

		it('should emit input when passed validation', async done => {
			const emitImput = jest.fn();
			const wrapper = mountComponent({
				propsData: {
					rules: 'min:3',
					name: 'username'
				}
			});

			wrapper.vm.$on('input', emitImput);

			wrapper.setData({ inputData: '123' });

			await wrapper.vm.$nextTick();

			jest.advanceTimersByTime(500);

			jest.useRealTimers();
			setTimeout(() => {
				expect(wrapper.vm.$validator.errors.any()).toBe(false);
				expect(emitImput).toHaveBeenCalledWith('123');
				done();
			}, 0);
		});

		it('should error and emit with empty string', async done => {
			const emitImput = jest.fn();
			const wrapper = mountComponent({
				propsData: {
					rules: 'min:3',
					name: 'username'
				}
			});

			wrapper.vm.$on('input', emitImput);

			wrapper.setData({ inputData: '12' });

			await wrapper.vm.$nextTick();

			jest.advanceTimersByTime(500);

			jest.useRealTimers();
			setTimeout(() => {
				expect(wrapper.vm.$validator.errors.any()).toBe(true);
				expect(emitImput).toHaveBeenCalledWith('');
				done();
			}, 0);
		});

		it('should send bugsnag when validator was rejected', async done => {
			const bugsnag = jest.fn();
			global.bugsnagClient = { notify: bugsnag };
			const wrapper = mountComponent({
				propsData: {
					rules: 'min:3',
					name: 'username'
				}
			});

			wrapper.vm.$validator.validate = jest.fn().mockRejectedValue();

			wrapper.setData({ inputData: '123' });

			await wrapper.vm.$nextTick();

			jest.advanceTimersByTime(500);

			jest.useRealTimers();
			setTimeout(() => {
				expect(bugsnag).toHaveBeenCalled();
				done();
			}, 0);
		});

		it('should clearTimeout When input repeatly', async () => {
			const clearTimeoutSpy = jest.spyOn(window, 'clearTimeout');
			const wrapper = mountComponent();

			wrapper.setData({ inputData: 'input 1' });

			await wrapper.vm.$nextTick();

			jest.advanceTimersByTime(100);

			expect(clearTimeoutSpy).not.toBeCalled();

			wrapper.setData({ inputData: 'input 2' });

			await wrapper.vm.$nextTick();

			expect(clearTimeoutSpy).toBeCalled();
		});
	});
});
