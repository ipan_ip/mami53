import { mount, createLocalVue } from '@vue/test-utils';
import CtStageStep from 'Consultant/components/CtStageStep.vue';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

const localVue = createLocalVue();
mockVLazy(localVue);

describe('CtStageStep.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}, additionalProps = {}) => {
			return mount(CtStageStep, {
				localVue,
				propsData: {
					steps: [],
					...additionalProps
				},
				...options
			});
		};
	});

	it('Should render component and match snapshot', () => {
		const wrapper = mountComponent(
			{},
			{
				steps: [{}]
			}
		);

		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should render disc according to steps passed', async () => {
		const steps = [{}, {}, {}];
		const wrapper = mountComponent(
			{},
			{
				steps
			}
		);

		await wrapper.vm.$nextTick();
		const discs = wrapper.findAll('.ct-step-disc');
		expect(discs.length).toBe(steps.length);
	});

	it('Should emit an event click:detail', async () => {
		const handleClickDetail = jest.fn();
		const wrapper = mountComponent({
			listeners: {
				'click:detail': handleClickDetail
			}
		});

		await wrapper.vm.$nextTick();
		wrapper.find('.stage-action').trigger('click');
		expect(handleClickDetail).toHaveBeenCalled();
	});
});
