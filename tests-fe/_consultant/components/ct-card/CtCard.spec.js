import { shallowMount } from '@vue/test-utils';
import CtCard from 'Consultant/components/ct-card';

describe('CtCard.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return shallowMount(CtCard, {
				...options
			});
		};
	});

	it('Should render component and match snapshot', async () => {
		const wrapper = mountComponent();

		await wrapper.vm.$nextTick();
		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should have color style when using prop string', async () => {
		const wrapper = mountComponent({
			propsData: {
				color: 'red'
			}
		});

		await wrapper.vm.$nextTick();
		expect(wrapper.attributes('style')).toMatchSnapshot();
	});

	it('Should have color style when using prop object', async () => {
		const wrapper = mountComponent({
			propsData: {
				color: {
					gradient: {
						from: 'red',
						to: 'green'
					}
				}
			}
		});

		await wrapper.vm.$nextTick();
		expect(wrapper.attributes('style')).toMatchSnapshot();
	});
});
