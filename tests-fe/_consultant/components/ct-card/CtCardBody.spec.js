import { shallowMount } from '@vue/test-utils';
import { CtCardBody } from 'Consultant/components/ct-card';

describe('CtCardBody.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return shallowMount(CtCardBody, {
				slots: {
					default: '<div></div>'
				},
				...options
			});
		};
	});

	it('Should render component and match snapshot', async () => {
		const wrapper = mountComponent();

		await wrapper.vm.$nextTick();
		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should have min height', async () => {
		const wrapper = mountComponent({
			propsData: {
				minHeight: '20px'
			}
		});

		await wrapper.vm.$nextTick();
		expect(wrapper.attributes('style')).toMatchSnapshot();
	});

	it('Should render card body that has scroll', async () => {
		const wrapper = mountComponent({
			propsData: {
				scrollable: true
			}
		});

		await wrapper.vm.$nextTick();
		expect(wrapper.classes('body--scrollable')).toBe(true);
	});
});
