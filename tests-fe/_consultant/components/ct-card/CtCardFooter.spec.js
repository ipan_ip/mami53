import { shallowMount } from '@vue/test-utils';
import { CtCardFooter } from 'Consultant/components/ct-card';

describe('CtCardFooter.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return shallowMount(CtCardFooter, {
				slots: {
					default: '<div></div>'
				},
				...options
			});
		};
	});

	it('Should render component and match snapshot', async () => {
		const wrapper = mountComponent();

		await wrapper.vm.$nextTick();
		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should have color style when using prop string', async () => {
		const wrapper = mountComponent({
			propsData: {
				color: 'red'
			}
		});

		await wrapper.vm.$nextTick();
		expect(wrapper.attributes('style')).toMatchSnapshot();
	});

	it('Should have color style when using prop object', async () => {
		const wrapper = mountComponent({
			propsData: {
				color: {
					gradient: {
						from: 'red',
						to: 'green'
					}
				}
			}
		});

		await wrapper.vm.$nextTick();
		expect(wrapper.attributes('style')).toMatchSnapshot();
	});

	it('Should have divider', async () => {
		const wrapper = mountComponent({
			propsData: {
				divider: 'line'
			}
		});

		await wrapper.vm.$nextTick();
		expect(wrapper.classes('divider--line')).toBe(true);
	});

	it('Should have min height', async () => {
		const wrapper = mountComponent({
			propsData: {
				minHeight: '20px'
			}
		});

		await wrapper.vm.$nextTick();
		expect(wrapper.attributes('style')).toMatchSnapshot();
	});

	it('Should render footer in center', async () => {
		const wrapper = mountComponent({
			propsData: {
				center: true
			}
		});

		await wrapper.vm.$nextTick();
		expect(wrapper.classes('footer--center')).toBe(true);
	});
});
