import {
	generateBackgroundStyle,
	createStyle
} from 'Consultant/components/ct-card/utils';

describe('Function: generateBackgroundStyle', () => {
	it('Should get transparent by default value returned', () => {
		const arg = null;
		const expected = 'background-color: transparent';
		expect(generateBackgroundStyle(arg)).toBe(expected);
	});

	it('Should return data passed when it type is string', () => {
		const arg = 'red';
		const expected = 'background-color: ' + arg;
		expect(generateBackgroundStyle(arg)).toBe(expected);
	});

	it('Should get transparent when argument not string or object', () => {
		const arg = [];
		const expected = 'background-color: transparent';
		expect(generateBackgroundStyle(arg)).toBe(expected);
	});

	it("Should get transparent when argument is object and hasn't from or to properties", () => {
		const arg = {};
		const expected = 'background-color: transparent';
		expect(generateBackgroundStyle(arg)).toBe(expected);
	});

	it('Should get gradient style', () => {
		const arg = {
			gradient: {
				from: 'red',
				to: 'white'
			}
		};
		expect(generateBackgroundStyle(arg)).toMatchSnapshot();
	});
});

describe('Fuction: createStyle', () => {
	it('Should return default value when no value', () => {
		const defaultValue = 'test';
		expect(createStyle('prop', null, defaultValue)).toBe(defaultValue);
	});

	it('Should get empty string', () => {
		expect(createStyle('prop', null)).toBe('');
	});

	it('Should get data style', () => {
		const prop = 'prop';
		const value = 'value';
		expect(createStyle(prop, value)).toBe(`${prop}: ${value};`);
	});
});
