import { shallowMount } from '@vue/test-utils';
import RadioGroup from 'Consultant/components/ConsultantRadioGroup.vue';

describe('ConsultantRadioGroup.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return shallowMount(RadioGroup, options);
		};
	});

	it('Should render radio component and match snapshot', () => {
		const wrapper = mountComponent({
			propsData: {
				options: []
			}
		});
		expect(wrapper.element).toMatchSnapshot();
	});

	it('Should render legend and match snapshot', () => {
		const wrapper = mountComponent({
			propsData: {
				options: [],
				legend: 'Radio Legend'
			}
		});
		expect(wrapper.find('.radio-group__legend').element).toMatchSnapshot();
	});

	it('Should render radio as inline', () => {
		const wrapper = mountComponent({
			propsData: {
				options: [],
				legend: 'Radio Legend',
				inline: true
			}
		});
		expect(wrapper.element).toMatchSnapshot();
	});

	it('Should render legend slot', () => {
		const legendText = 'test';

		const wrapper = mountComponent({
			propsData: {
				options: [],
				legend: 'Radio Legend'
			},
			slots: {
				legend: legendText
			}
		});
		expect(wrapper.find('.radio-group__legend').text()).toBe(legendText);
	});

	it('Should render label slot', () => {
		const label = 'label';

		const wrapper = mountComponent({
			propsData: {
				options: [{}]
			},
			slots: {
				label: label
			},
			stubs: {
				radio: {
					template: `<div class="radio-label"><slot></slot></div>`
				}
			}
		});

		const labelElm = wrapper.findAll('.radio-label').at(0);
		expect(labelElm.text()).toBe(label);
	});

	it("Should render radio's subtitle", async () => {
		const wrapper = mountComponent({
			propsData: {
				options: [{ subtitle: true, label: 'test' }]
			}
		});

		const radioContent = wrapper.find('.radio-group__content');
		expect(radioContent.html()).toMatchSnapshot();
	});

	it('Should set local value when parent was changed', () => {
		const valueFromParent = 'This is value from parent';
		const wrapper = mountComponent({
			propsData: {
				options: []
			}
		});
		expect(wrapper.vm.valueSelected).toBeFalsy();
		wrapper.vm.handleParentValueChanged(valueFromParent);
		expect(wrapper.vm.valueSelected).toBe(valueFromParent);
	});

	it('emit an event input when radio is selected', () => {
		const sendEmit = jest.fn();
		const wrapper = mountComponent({
			propsData: {
				options: [
					{
						value: 'Option 1'
					},
					{
						value: 'Option 2'
					}
				]
			},
			stubs: {
				radio: {
					template: `<input @input="$emit('input', selectedValue)" type="radio" :value="selectedValue" />`,
					props: ['selectedValue', 'value']
				}
			}
		});

		wrapper.vm.$on('input', sendEmit);
		wrapper
			.findAll('input')
			.at(1)
			.trigger('input');
		expect(sendEmit).toHaveBeenCalledWith('Option 2');
	});

	it('should render option normally when using labelValue', async () => {
		const options = ['option 1', 'option 2'];
		const wrapper = mountComponent({
			propsData: {
				options,
				labelValue: true
			}
		});

		await wrapper.vm.$nextTick();

		expect(options).toEqual(options);
	});
});
