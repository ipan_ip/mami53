import { shallowMount, createLocalVue } from '@vue/test-utils';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import ConsultantEmptyState from 'Consultant/components/ConsultantEmptyState.vue';

const localVue = createLocalVue();
mockVLazy(localVue);

describe('ConsultantEmptyState.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return shallowMount(ConsultantEmptyState, {
				localVue,
				...options
			});
		};
	});

	it('Should render title and match snapshot', () => {
		const wrapper = mountComponent({
			propsData: {
				title: 'Empty State title'
			}
		});

		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should render subtitle and match snapshot', () => {
		const wrapper = mountComponent({
			propsData: {
				subtitle: 'Empty State subtitle'
			}
		});

		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should have "empty-state--relative" class when using relative prop', () => {
		const wrapper = mountComponent({
			propsData: {
				relative: true
			}
		});

		expect(wrapper.classes('empty-state--relative')).toBe(true);
	});
});
