import { shallowMount } from '@vue/test-utils';
import ConsultantSelect from 'Consultant/components/ConsultantSelect.vue';

describe('ConsultantSelect.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return shallowMount(ConsultantSelect, options);
		};
	});

	it('Should render select component and match snapshot', () => {
		const wrapper = mountComponent({
			propsData: {
				value: '',
				options: []
			}
		});
		expect(wrapper.element).toMatchSnapshot();
	});

	it('Should render legend and match snapshot', () => {
		const wrapper = mountComponent({
			propsData: {
				value: '',
				options: [],
				legend: 'Select Legend'
			}
		});
		expect(wrapper.element).toMatchSnapshot();
	});

	it('Should render legend with mandatory sign and match snapshot', () => {
		const wrapper = mountComponent({
			propsData: {
				value: '',
				options: [],
				legend: 'Select Legend',
				mandatorySign: true
			}
		});
		expect(wrapper.element).toMatchSnapshot();
	});

	it('Should render select as regular style', () => {
		const wrapper = mountComponent({
			propsData: {
				value: '',
				options: [],
				legend: 'Select Legend',
				regular: true
			}
		});
		expect(wrapper.element).toMatchSnapshot();
	});

	it('Should render dirty class', done => {
		const wrapper = mountComponent({
			propsData: {
				value: '',
				options: []
			}
		});

		wrapper.setData({ valueSelected: 'not empty' });
		wrapper.vm.$nextTick(() => {
			expect(wrapper.vm.isDirty).toBe(true);
			expect(wrapper.classes()).toMatchSnapshot();
			done();
		});
	});

	it('Should not attach event click to select wrapper', () => {
		const handleClick = jest.fn();
		const wrapper = mountComponent({
			propsData: {
				value: '',
				options: []
			}
		});

		wrapper.vm.$on('click', handleClick);
		wrapper.find('.select__content').trigger('click');
		expect(handleClick).not.toHaveBeenCalled();
	});

	it('Should attach event click to select wrapper when readonly mode', () => {
		const handleClick = jest.fn();
		const wrapper = mountComponent({
			propsData: {
				value: '',
				options: [],
				readonly: true
			}
		});

		wrapper.vm.$on('click', handleClick);
		wrapper.find('.select__content').trigger('click');
		expect(handleClick).toHaveBeenCalled();
	});

	it('Should change local value when parent value changed and match one of options', done => {
		const valueChanges = 'value_1';
		const wrapper = mountComponent({
			propsData: {
				value: null,
				options: [
					{
						value: valueChanges
					}
				]
			}
		});
		expect(wrapper.vm.valueSelected).toBeFalsy();
		wrapper.setProps({ value: valueChanges });
		wrapper.vm.$nextTick(() => {
			expect(wrapper.vm.valueSelected).toBe(valueChanges);
			done();
		});
	});

	it('Nothing changes on local state if parent value no match with options', done => {
		const wrapper = mountComponent({
			propsData: {
				value: null,
				options: [
					{
						value: 'value_1'
					}
				]
			}
		});
		expect(wrapper.vm.valueSelected).toBeFalsy();
		wrapper.setProps({ value: 'value_2' });
		wrapper.vm.$nextTick(() => {
			expect(wrapper.vm.valueSelected).toBeFalsy();
			done();
		});
	});

	it('Nothing changes on local state if sensitive mode', done => {
		const wrapper = mountComponent({
			propsData: {
				value: null,
				insensitive: false,
				options: [
					{
						value: 'value_1'
					}
				]
			}
		});
		expect(wrapper.vm.valueSelected).toBeFalsy();
		wrapper.setProps({ value: 'Value_1' });
		wrapper.vm.$nextTick(() => {
			expect(wrapper.vm.valueSelected).toBeFalsy();
			done();
		});
	});

	it('emit an event input when select is changed', () => {
		const handleInput = jest.fn();
		const handleChange = jest.fn();
		const wrapper = mountComponent({
			propsData: {
				value: '',
				options: []
			},
			stubs: {
				'v-select': {
					template: `
						<select id="select" @change="$emit('change')">
							<option></option>
						</select>
					`
				}
			}
		});

		wrapper.vm.$on('input', handleInput);
		wrapper.vm.$on('change', handleChange);

		wrapper.find('#select').trigger('change');
		expect(handleChange).toHaveBeenCalled();
		expect(handleInput).toHaveBeenCalled();
	});

	describe('Testing method part', () => {
		describe('Testing Method isStringSame', () => {
			let data1 = '';
			let data2 = '';
			beforeEach(() => {
				data1 = 'data';
				data2 = 'DaTa';
			});
			it('return false in sensitive mode', () => {
				expect(
					ConsultantSelect.methods.isStringSame(data1, data2, false)
				).toBeFalsy();
			});

			it('return true in insensitive mode', () => {
				expect(
					ConsultantSelect.methods.isStringSame(data1, data2, true)
				).toBeTruthy();
			});
		});
	});
});
