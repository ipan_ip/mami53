import { mount, createLocalVue } from '@vue/test-utils';
import CtFormInputHour from 'Consultant/components/CtFormInputHour.vue';
import VeeValidate from 'Consultant/config/veeValidate';

const localVue = createLocalVue();
localVue.use(VeeValidate);

describe('CtFormInputHour.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return mount(CtFormInputHour, {
				localVue,
				...options
			});
		};
	});

	it('Should render component and match snapshot', () => {
		const wrapper = mountComponent({
			propsData: {
				label: 'label',
				mandatorySign: true
			}
		});

		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should emit an event input', () => {
		const wrapper = mountComponent({
			stubs: {
				CtInputText: {
					template: `<input @input="$emit('input')" />`
				}
			}
		});

		const eventInput = jest.fn();
		wrapper.vm.$on('input', eventInput);

		wrapper.find('input').trigger('input');

		expect(eventInput).toHaveBeenCalled();
	});

	it('Should set local value', async () => {
		const wrapper = mountComponent();
		const text = 'test';

		wrapper.setProps({ value: text });

		await wrapper.vm.$nextTick();
		expect(wrapper.vm.inputValue).toBe(text);
	});
});
