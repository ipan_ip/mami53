import { shallowMount, createLocalVue } from '@vue/test-utils';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import FreeTextInputPage from 'Consultant/components/FreeTextInputPage.vue';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

const localVue = createLocalVue();
mockVLazy(localVue);

describe('FreeTextInputPage.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return shallowMount(FreeTextInputPage, {
				localVue,
				...options
			});
		};
	});

	it('Should render component and match snapshot', () => {
		const wrapper = mountComponent();

		expect(wrapper.element).toMatchSnapshot();
	});

	it('should render default header slot', () => {
		const wrapper = mountComponent();

		expect(wrapper.find('.free-text__header')).toMatchSnapshot();
	});

	it('should render header slot', () => {
		const HeaderText = 'text of header slot';
		const wrapper = mountComponent({
			slots: {
				header: HeaderText
			}
		});

		expect(wrapper.find('.free-text__header').text()).toBe(HeaderText);
		expect(wrapper.find('.free-text__header')).toMatchSnapshot();
	});

	it('should have class free-text--loading when prop loading is true', async () => {
		const wrapper = mountComponent({
			propsData: {
				loading: true
			}
		});

		await wrapper.vm.$nextTick();
		expect(wrapper.classes('free-text--loading')).toBe(true);
		expect(wrapper.find('.button__save')).toMatchSnapshot();
	});

	it('Emit close and update open on button__close click', async () => {
		const updateOpen = jest.fn();
		const close = jest.fn();
		const wrapper = mountComponent({
			propsData: {
				open: true
			}
		});

		wrapper.vm.$on('update:open', updateOpen);
		wrapper.vm.$on('close', close);

		await wrapper.vm.$nextTick();

		wrapper.find('.button__close').trigger('click');

		expect(updateOpen).toHaveBeenCalled();
		expect(close).toHaveBeenCalled();
	});

	it('should emit an event input when button__save click', async () => {
		const inputEmit = jest.fn();
		const wrapper = mountComponent({
			propsData: {
				open: true
			}
		});

		wrapper.vm.$on('input', inputEmit);

		await wrapper.vm.$nextTick();

		wrapper.find('.button__save').trigger('click');

		expect(inputEmit).toHaveBeenCalled();
	});

	it('should emit an event save only and prevent emit input', async () => {
		const inputEmit = jest.fn();
		const saveEmit = jest.fn();
		const wrapper = mountComponent({
			propsData: {
				open: true,
				preventSave: true
			}
		});

		wrapper.vm.$on('input', inputEmit);
		wrapper.vm.$on('save', saveEmit);

		await wrapper.vm.$nextTick();

		wrapper.find('.button__save').trigger('click');

		expect(inputEmit).not.toHaveBeenCalled();
		expect(saveEmit).toHaveBeenCalled();
	});

	it('Emit event when input from input-textarea on syncValue mode', async () => {
		const inputEmit = jest.fn();
		const wrapper = mountComponent({
			propsData: {
				open: true,
				syncValue: true
			},
			stubs: {
				InputTextarea: `
        <input id="input" @input="$emit('input')" />
        `
			}
		});

		wrapper.vm.$on('input', inputEmit);

		await wrapper.vm.$nextTick();

		wrapper.find('#input').trigger('input');

		expect(inputEmit).toHaveBeenCalled();
	});

	it('Not should event input from input-textarea when syncValue is false', async () => {
		const inputEmit = jest.fn();
		const wrapper = mountComponent({
			propsData: {
				open: true,
				syncValue: false
			},
			stubs: {
				InputTextarea: `
        <input id="input" @input="$emit('input')" />
        `
			}
		});

		wrapper.vm.$on('input', inputEmit);

		await wrapper.vm.$nextTick();

		wrapper.find('#input').trigger('input');

		expect(inputEmit).not.toHaveBeenCalled();
	});

	describe('Testing when open FreeTextInputPage', () => {
		it('should open free-text-input', async () => {
			const wrapper = mountComponent({
				propsData: {
					open: true
				}
			});

			await wrapper.vm.$nextTick();

			expect(wrapper.classes('free-text--active')).toBe(true);
		});

		it('should set local value when open', async () => {
			const valueFromParent = 'This value came from parent';
			const wrapper = mountComponent({
				propsData: {
					value: valueFromParent,
					open: false
				}
			});

			await wrapper.vm.$nextTick();

			expect(wrapper.vm.localValue).toBe('');

			wrapper.setProps({ open: true });

			await wrapper.vm.$nextTick();

			expect(wrapper.vm.localValue).toBe(valueFromParent);
		});

		it('should reset local value when close', async () => {
			const localValue = 'This Local Value that was setted';
			const wrapper = mountComponent({
				propsData: {
					open: true
				}
			});

			wrapper.setData({ localValue: localValue });

			await wrapper.vm.$nextTick();

			expect(wrapper.vm.localValue).toBe(localValue);

			wrapper.setProps({ open: false });

			await wrapper.vm.$nextTick();

			expect(wrapper.vm.localValue).toBe('');
		});

		it('should add class overflow-hidden on body when open', async () => {
			const addClass = jest.fn();
			mockWindowProperty('document.body', {
				classList: {
					add: addClass
				}
			});
			const wrapper = mountComponent();

			wrapper.setProps({ open: true });
			await wrapper.vm.$nextTick();
			expect(addClass).toHaveBeenCalledWith('overflow-hidden');
		});

		it('should remove class overflow-hidden on body when close', async () => {
			const removeClass = jest.fn();
			mockWindowProperty('document.body', {
				classList: {
					remove: removeClass
				}
			});
			const wrapper = mountComponent({
				propsData: {
					open: true
				}
			});

			wrapper.setProps({ open: false });
			await wrapper.vm.$nextTick();
			expect(removeClass).toHaveBeenCalledWith('overflow-hidden');
		});
	});
});
