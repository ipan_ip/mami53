import { mount } from '@vue/test-utils';
import CtSelect from 'Consultant/components/CtSelect.vue';

describe('CtSelect.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return mount(CtSelect, options);
		};
	});

	it('Should render select component and match snapshot', () => {
		const wrapper = mountComponent({
			propsData: {
				label: 'label',
				placeholder: 'placeholder',
				options: ['test'],
				backgroundColor: '#fff'
			}
		});
		expect(wrapper.classes('ct-select')).toBe(true);
		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should render label and match snapshot', () => {
		const wrapper = mountComponent({
			propsData: {
				label: 'Test'
			}
		});
		expect(wrapper.element).toMatchSnapshot();
	});

	it('Should render legend with mandatory sign', () => {
		const wrapper = mountComponent({
			propsData: {
				label: 'label',
				mandatorySign: true
			}
		});
		expect(wrapper.element).toMatchSnapshot();
	});

	it('Should set background color', () => {
		const wrapper = mountComponent({
			propsData: {
				backgroundColor: 'white'
			}
		});
		expect(
			wrapper.find('.ct-input__body').attributes('style')
		).toMatchSnapshot();
	});

	it('Should have subtext and set select as invalid', () => {
		const invalidText = 'test';
		const wrapper = mountComponent({
			propsData: {
				invalidMessage: invalidText
			}
		});

		expect(wrapper.classes('.ct-select--invalid'));
		expect(wrapper.find('.message-text').text()).toBe(invalidText);
	});

	it('Should have clearable icon', () => {
		const wrapper = mountComponent({
			attrs: {
				clearable: true
			}
		});

		expect(wrapper.find('.clearable-icon')).toBeTruthy();
	});

	it('Should reset value and emit to parent when clear button clicked', async () => {
		const wrapper = mountComponent({
			propsData: {
				options: ['test']
			},
			attrs: {
				clearable: true
			}
		});

		const emit = jest.fn();
		wrapper.vm.$on('input', emit);

		wrapper.setProps({ value: 'test' });

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.inputValue).toBe('test');

		wrapper.find('.clearable-icon i').trigger('click');

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.inputValue).toBe('');
		expect(emit).toHaveBeenCalledWith('');
	});

	it('Should reset value and emit an event with empty object', async () => {
		const label = 'test label';
		const option = { value: 'test', label };
		const wrapper = mountComponent({
			propsData: {
				options: [option],
				returnObject: true
			},
			attrs: {
				clearable: true
			}
		});

		const emit = jest.fn();
		wrapper.vm.$on('input', emit);

		wrapper.setProps({ value: option });

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.inputValue).toBe(label);

		wrapper.find('.clearable-icon i').trigger('click');

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.inputValue).toBe('');
		expect(emit).toHaveBeenCalledWith({});
	});

	describe('Test option of select', () => {
		it('Should wrapping each option to object with title property', () => {
			const wrapper = mountComponent({
				propsData: {
					options: ['test']
				}
			});

			expect(wrapper.vm.optionItems).toEqual(
				expect.arrayContaining([expect.objectContaining({ title: 'test' })])
			);
		});

		it('Should add title and active property when using option with object type', () => {
			const wrapper = mountComponent({
				propsData: {
					options: [{ value: 'test', label: 'test' }]
				}
			});

			expect(wrapper.vm.optionItems).toEqual(
				expect.arrayContaining([
					expect.objectContaining({
						title: 'test',
						active: expect.any(Boolean)
					})
				])
			);
		});

		it('Should set the title of option to be empty string', () => {
			const wrapper = mountComponent({
				propsData: {
					options: [{ value: 'test' }]
				}
			});

			expect(wrapper.vm.optionItems).toEqual(
				expect.arrayContaining([
					expect.objectContaining({
						title: ''
					})
				])
			);
		});

		it('Should set active for option when current value has filled and object type', () => {
			const label = 'test label';
			const option = { value: 'test', label };
			const wrapper = mountComponent({
				propsData: {
					options: [option],
					value: option
				}
			});

			expect(wrapper.vm.optionItems).toEqual(
				expect.arrayContaining([
					expect.objectContaining({
						title: label,
						active: true
					})
				])
			);
		});
	});

	describe('Test emitting event', () => {
		it('should emit an event with a string', () => {
			const option = 'test option';

			const wrapper = mountComponent({
				propsData: {
					options: [option]
				}
			});

			const emit = jest.fn();
			wrapper.vm.$on('input', emit);

			wrapper.find('.list__tile').trigger('click');

			expect(emit).toHaveBeenCalledWith(option);
		});

		it('should emit an event with a value of option', () => {
			const value = 'test value';

			const wrapper = mountComponent({
				propsData: {
					options: [{ value }]
				}
			});

			const emit = jest.fn();
			wrapper.vm.$on('input', emit);

			wrapper.find('.list__tile').trigger('click');

			expect(emit).toHaveBeenCalledWith(value);
		});

		it('should emit an event with an object when using returnObject prop', () => {
			const option = { value: 'test', label: 'label' };

			const wrapper = mountComponent({
				propsData: {
					options: [option],
					returnObject: true
				}
			});

			const emit = jest.fn();
			wrapper.vm.$on('input', emit);

			wrapper.find('.list__tile').trigger('click');

			expect(emit).toHaveBeenCalledWith(option);
		});

		it('Should set input value with label from option selected', async () => {
			const label = 'test label';
			const option = { value: 'test', label };

			const wrapper = mountComponent({
				propsData: {
					value: 'test',
					options: [option]
				}
			});

			await wrapper.vm.$nextTick();

			expect(wrapper.vm.inputValue).toBe(label);
		});
	});
});
