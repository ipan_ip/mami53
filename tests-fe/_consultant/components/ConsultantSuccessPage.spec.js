import { shallowMount, createLocalVue } from '@vue/test-utils';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import ConsultantSuccessPage from 'Consultant/components/ConsultantSuccessPage.vue';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

const localVue = createLocalVue();
mockVLazy(localVue);

describe('ConsultantSuccessPage.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return shallowMount(ConsultantSuccessPage, {
				localVue,
				...options
			});
		};
	});

	it("Shouldn't render if page is not open", () => {
		const wrapper = mountComponent();

		expect(wrapper.html()).toBeFalsy();
	});

	it('Should render component if page is it open', async () => {
		const wrapper = mountComponent({
			propsData: {
				open: true
			}
		});
		await wrapper.vm.$nextTick();
		expect(wrapper.html()).toMatchSnapshot();
	});

	it("Shouldn't render default slot if page hasn't it", async () => {
		const wrapper = mountComponent({
			propsData: {
				open: true
			}
		});
		await wrapper.vm.$nextTick();
		expect(wrapper.find('.body__caption').exists()).toBe(false);
	});

	it('Should render default slot if page has it', async () => {
		const wrapper = mountComponent({
			propsData: {
				open: true
			},
			slots: {
				default: '<div></div>'
			}
		});
		await wrapper.vm.$nextTick();
		expect(wrapper.find('.body__caption')).toMatchSnapshot();
	});

	it('should render slot button', async () => {
		const wrapper = mountComponent({
			propsData: {
				open: true
			},
			slots: {
				button: '<button></button>'
			}
		});
		await wrapper.vm.$nextTick();
		expect(wrapper.find('.body__button')).toMatchSnapshot();
	});

	it('should render default slot button', async () => {
		const wrapper = mountComponent({
			propsData: {
				open: true
			}
		});
		await wrapper.vm.$nextTick();
		expect(wrapper.find('.body__button')).toMatchSnapshot();
	});

	it('Emit events when button close has been clicked', async () => {
		const close = jest.fn();
		const updateOpen = jest.fn();
		const wrapper = mountComponent({
			propsData: {
				open: true
			},
			stubs: {
				ConsultantButton: `<button id="button-close" @click="$emit('click')"></button>`
			}
		});

		wrapper.vm.$on('close', close);
		wrapper.vm.$on('update:open', updateOpen);

		await wrapper.vm.$nextTick();

		wrapper.find('#button-close').trigger('click');
		expect(close).toHaveBeenCalled();
		expect(updateOpen).toHaveBeenCalled();
	});

	describe('Testing DOM, to add or remove class', () => {
		const addClass = jest.fn();
		const removeClass = jest.fn();
		jest.useFakeTimers();
		beforeAll(() => {
			mockWindowProperty('document.body.classList', {
				add: addClass,
				remove: removeClass
			});
		});

		it('Should add class "success-page-open" in body when open', async () => {
			const wrapper = mountComponent();

			expect(addClass).not.toHaveBeenCalled();
			wrapper.setProps({ open: true });
			await wrapper.vm.$nextTick();
			expect(addClass).toHaveBeenCalledWith('success-page-open');
		});

		it('Should remove class "success-page-open" in body when close', async () => {
			const wrapper = mountComponent({
				propsData: {
					open: true
				}
			});

			wrapper.setProps({ open: false });

			jest.runAllTimers();

			expect(removeClass).toHaveBeenCalledWith('success-page-open');
		});
	});
});
