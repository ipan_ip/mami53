import { shallowMount, createLocalVue } from '@vue/test-utils';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import CardList from 'Consultant/components/CardList.vue';

const localVue = createLocalVue();
mockVLazy(localVue);

describe('CardList.vue', () => {
	const mockPropTo = { name: 'route-name' };
	const wrapper = shallowMount(CardList, {
		localVue,
		mocks: {
			$router: { push: jest.fn() }
		}
	});

	it('Should render card-list component', () => {
		expect(wrapper.find('.consultant-card-list').exists()).toBeTruthy();
	});

	it('By default card-list have "has-arrow" class', () => {
		expect(wrapper.classes()).toContain('has-arrow');
	});

	it('should remove "has-arrow" class when "noArrow" prop is true', done => {
		wrapper.setProps({ noArrow: true });
		wrapper.vm.$nextTick(() => {
			expect(wrapper.props('noArrow')).toBe(true);
			expect(wrapper.classes().includes('has-arrow')).toBeFalsy();
			done();
		});
	});

	it('should execute "click" $emit when "to" prop is null', () => {
		const cardBody = wrapper.find('.card-list__body');
		cardBody.trigger('click');
		expect(wrapper.emitted().click).toBeTruthy();
	});

	it('should execute $router when "to" prop not null', done => {
		wrapper.setProps({ to: mockPropTo });
		wrapper.vm.$nextTick(() => {
			const cardBody = wrapper.find('.card-list__body');
			cardBody.trigger('click');
			expect(wrapper.vm.$router.push).toBeCalledWith(mockPropTo);
			done();
		});
	});
});
