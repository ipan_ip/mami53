import { shallowMount } from '@vue/test-utils';
import InputNote from 'Consultant/components/input-note/index.vue';

describe('InputNote', () => {
	let createWrapper = Function;

	beforeEach(() => {
		createWrapper = (options = {}) => {
			return shallowMount(InputNote, options);
		};
	});

	it('Should render component', () => {
		const wrapper = createWrapper();

		expect(wrapper.classes('input-note')).toBe(true);
	});

	it('Should emit an event update:remarksCategory', () => {
		const mockValueData = 'remark category note';
		const wrapper = createWrapper({
			stubs: {
				SelectNoteDialog: {
					template: `<input @input="$emit('input', mockValue)" />`,
					props: ['value'],
					data: () => ({
						mockValue: mockValueData
					})
				}
			}
		});

		const updateEmit = jest.fn();
		wrapper.vm.$on('update:remarksCategory', updateEmit);

		wrapper.find('input').trigger('input');

		expect(updateEmit).toHaveBeenCalledWith(mockValueData);
	});

	it('Should emit an event update:remarksNote', async () => {
		const mockValueData = 'remark note';
		const wrapper = createWrapper();

		const updateEmit = jest.fn();
		wrapper.vm.$on('update:remarksNote', updateEmit);

		wrapper.setData({ additionalNote: mockValueData });

		await wrapper.vm.$nextTick();

		expect(updateEmit).toHaveBeenCalledWith(mockValueData);
	});

	it('Should set additionalNote when remarksNote has changed from parent', async () => {
		const valueFromParent = 'remark note';
		const wrapper = createWrapper();

		wrapper.setProps({ remarksNote: valueFromParent });

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.additionalNote).toBe(valueFromParent);
	});

	it('Should return label', async () => {
		const mockNoteOption = {
			checked: false,
			label: 'Ada di data Booking',
			value: 'remark-has_booking_data'
		};

		const label = InputNote.methods.getLabelNote(mockNoteOption.value);

		expect(label).toBe(mockNoteOption.label);
	});

	it('Should return value when value no match', async () => {
		const randomValue = 'random value';

		const label = InputNote.methods.getLabelNote(randomValue);

		expect(label).toBe(randomValue);
	});
});
