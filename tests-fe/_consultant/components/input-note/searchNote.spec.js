import searchNote from 'Consultant/components/input-note/searchNote.js';

describe('Test searchNote function', () => {
	it('should return label when key is match', () => {
		const mockOption = {
			label: 'Memiliki kontrak aktif',
			value: 'remark-has_active_contract'
		};

		const result = searchNote(mockOption.value);

		expect(result).toBe(mockOption.label);
	});

	it('should return default value when no match', () => {
		const defaultValue = 'this is default label';
		const result = searchNote('random label', defaultValue);

		expect(result).toBe(defaultValue);
	});
});
