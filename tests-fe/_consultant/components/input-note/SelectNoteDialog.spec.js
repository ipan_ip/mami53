import { mount } from '@vue/test-utils';
import SelectNoteDialog from 'Consultant/components/input-note/SelectNoteDialog.vue';
import expectExport from 'expect';

describe('SelectNoteDialog', () => {
	let createWrapper = Function;

	beforeEach(() => {
		createWrapper = (options = {}) => {
			return mount(SelectNoteDialog, {
				propsData: {
					open: true
				},
				...options
			});
		};
	});

	it('Emit an event input when option has been selected', async () => {
		const wrapper = createWrapper();
		const input = jest.fn();
		const value = 'input value';
		const radio = wrapper.find('.consultant-radio-group');

		wrapper.vm.$on('input', input);

		radio.vm.$emit('input', value);

		await wrapper.vm.$nextTick();

		expectExport(input).toHaveBeenCalledWith(value);
	});

	it('Emit an event update:open when option has been selected', async () => {
		const wrapper = createWrapper();
		const updateOpen = jest.fn();
		const radio = wrapper.find('.consultant-radio-group');

		wrapper.vm.$on('update:open', updateOpen);

		radio.vm.$emit('input', 'value');

		await wrapper.vm.$nextTick();

		expectExport(updateOpen).toHaveBeenCalled();
	});

	it('should change local value when parent state changed', async () => {
		const wrapper = createWrapper();
		const value = 'value from parent';
		wrapper.setProps({ value });

		await wrapper.vm.$nextTick();

		expectExport(wrapper.vm.noteSelected).toBe(value);
	});

	it('should not change value when same from parent', async () => {
		const value = 'same value';
		const wrapper = createWrapper();
		wrapper.setData({ noteSelected: value });

		await wrapper.vm.$nextTick();

		wrapper.setProps({ value });

		await wrapper.vm.$nextTick();

		expectExport(wrapper.vm.noteSelected).toBe(value);
	});
});
