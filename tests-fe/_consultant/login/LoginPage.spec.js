import { shallowMount, createLocalVue } from '@vue/test-utils';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import LoginPage from 'Consultant/pages/login/LoginPage.vue';
import { flushPromises } from 'tests-fe/utils/promises';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

jest.useFakeTimers();

jest.mock('Consultant/event-bus/index', () => {
	const EventBus = {
		$on: jest.fn(),
		$off: jest.fn()
	};
	global.EventBus = EventBus;
	return global.EventBus;
});

const localVue = createLocalVue();
mockVLazy(localVue);

const bugsnagClient = {
	notify: jest.fn()
};
global.bugsnagClient = bugsnagClient;
// eslint-disable-next-line standard/no-callback-literal
global.tracker = (type, param, callback) => callback({ status: 200 });

describe('LoginPage.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return shallowMount(LoginPage, {
				localVue,
				...options
			});
		};
	});

	it('Should render login page and match snapshot', async () => {
		const wrapper = mountComponent();

		await wrapper.vm.$nextTick();

		expect(wrapper.classes('login-page')).toBe(true);
		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should go to dashboard and save cookie when login succeed', async () => {
		const oauthData = {
			access_token: 'login_token',
			refresh_token: 'refresh_token'
		};
		const replace = jest.fn();
		const setCookie = jest.fn();
		mockWindowProperty('window.location', {
			origin: 'test',
			replace
		});

		global.axios = {
			post: jest.fn().mockResolvedValue({
				data: {
					status: 'success',
					message: 'test',
					oauth: oauthData,
					consultant: 'test'
				}
			})
		};

		global.Cookies = { set: setCookie };

		const wrapper = mountComponent({
			stubs: {
				ConsultantButton: {
					template: `<button @click="$emit('click')"></button>`
				}
			},
			mocks: {
				$store: { commit: jest.fn() },
				$router: {
					replace
				}
			}
		});

		wrapper
			.findAll('button')
			.at(1)
			.trigger('click');

		await flushPromises();

		expect(setCookie).toHaveBeenCalledWith(
			'access_token',
			oauthData.access_token,
			expect.any(Object)
		);
		expect(setCookie).toHaveBeenCalledWith(
			'refresh_token',
			oauthData.refresh_token,
			expect.any(Object)
		);

		jest.runAllTimers();
		await flushPromises();
		expect(replace).toHaveBeenCalledWith('test/consultant-tools/dashboard');
	});

	it('Should get data success false', async () => {
		global.axios = {
			post: jest.fn().mockResolvedValue({
				data: {
					status: 'failed',
					message: 'failed message'
				}
			})
		};
		const wrapper = mountComponent();

		const result = await wrapper.vm.checkCredentialUser({});

		expect(result).toEqual(
			expect.objectContaining({
				success: false,
				message: 'failed message'
			})
		);
	});

	it('Should open snackbar when login was failed', async () => {
		global.axios = {
			post: jest.fn().mockRejectedValue(new Error('test error'))
		};

		const wrapper = mountComponent({
			stubs: {
				ConsultantButton: {
					template: `<button @click="$emit('click')"></button>`
				}
			}
		});

		wrapper
			.findAll('button')
			.at(1)
			.trigger('click');

		await flushPromises();

		expect(wrapper.vm.openSnackbar).toBe(true);
	});
});
