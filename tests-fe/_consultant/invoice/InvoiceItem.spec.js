import { createLocalVue, shallowMount } from '@vue/test-utils';
import InvoiceItem from 'Consultant/pages/invoice/components/InvoiceItem.vue';
import Vuex from 'vuex';

jest.useFakeTimers();
describe('Consultant/pages/invoice/components/InvoiceItem.vue', () => {
	const localVue = createLocalVue();
	localVue.use(Vuex);

	const mocks = {
		$dayjs: jest.fn(() => {
			return {
				locale: jest.fn(() => {
					return {
						format: jest.fn()
					};
				})
			};
		}),
		$router: { push: jest.fn() },
		$route: { params: jest.fn() },
		uniqueId: jest.fn()
	};

	const propsData = {
		item: {
			id: 2127,
			scheduled_date: '2020-03-26',
			status: 'unpaid',
			transfer_reference: null,
			total_cost: 2000000,
			amount: 1000000,
			note:
				'Nam sed mi id ex varius gravida. Proin lorem tortor, sodales et nisi quis, lobortis condimentum nunc.',
			link: 'https://padang2.mamikos.com/invoice/C8oak',
			consultant_note: null,
			additional_costs: [
				{
					id: 1760,
					invoice_id: 2127,
					cost_title: 'wifii 1',
					cost_value: 500000,
					cost_type: 'fixed',
					sort_order: 0,
					created_at: '2020-03-26 15:38:48',
					updated_at: '2020-03-26 15:38:48',
					deleted_at: null
				},
				{
					id: 1761,
					invoice_id: 2127,
					cost_title: 'wifii 2',
					cost_value: 500000,
					cost_type: 'other',
					sort_order: 0,
					created_at: '2020-03-26 15:38:48',
					updated_at: '2020-03-26 15:38:48',
					deleted_at: null
				}
			]
		},
		disableReminder: true
	};

	const store = new Vuex.Store({
		modules: {
			invoice: {
				namespaced: true,
				state: {
					additionalCosts: [],
					amount: '',
					uniqueId: ''
				},
				getters: {
					hasUniqueIdMatch(state) {
						return id => state.uniqueId === id;
					}
				},
				mutations: {
					setAdditionalCost(state, payload) {
						state.additionalCosts = payload || [];
					},
					setAmount(state, payload) {
						state.amount = payload;
					},
					setUniqueId(state, payload) {
						state.uniqueId = payload;
					},
					resetInvoiceState(state) {
						state.additionalCosts = [];
						state.amount = '';
						state.uniqueId = '';
					}
				}
			}
		},
		state: {
			authData: {
				consultant: {
					id: 2,
					name: 'abcde',
					email: 'abcde@mamikos.com',
					user_id: 111222,
					chat_id: null,
					user_key: null,
					device_key: null,
					created_at: '2020-02-28 12:00:00',
					updated_at: '2020-02-28 12:00:00',
					deleted_at: null
				}
			}
		},
		getters: {
			isAdminConsultant: jest.fn()
		},
		mutations: {
			setUniqueId(state, payload) {
				state.uniqueId = payload;
			},
			setAdditionalCost(state, payload) {
				state.additionalCosts = payload || [];
			},
			setAmount(state, payload) {
				state.amount = payload;
			}
		}
	});

	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return shallowMount(InvoiceItem, {
				localVue,
				mocks,
				propsData,
				store,
				...options
			});
		};
	});

	// test('Should render correctly', async () => {
	// 	const tree = shallowMount(InvoiceItem, {
	// 		localVue,
	// 		mocks,
	// 		propsData,
	// 		store
	// 	});

	// 	expect(tree.element).toMatchSnapshot();
	// });

	// test('copy text to clipboard and trim "http://" string', async () => {
	// 	const wrapper = mountComponent();

	// 	navigator.clipboard = {
	// 		writeText: jest.fn()
	// 	};

	// 	wrapper.find('.copy-invoice .icon-copy').trigger('click');
	// 	await wrapper.vm.$nextTick();

	// 	expect(wrapper.find('.copy-invoice span').classes('active')).toBe(true);

	// 	expect(navigator.clipboard.writeText).toHaveBeenCalled();
	// 	expect(wrapper.vm.noHttpText).toBe('padang2.mamikos.com/invoice/C8oak');

	// 	await wrapper.vm.$nextTick(() => {
	// 		jest.advanceTimersByTime(2001);
	// 		expect(wrapper.find('.copy-invoice span').classes('active')).toBe(false);
	// 	});

	// 	wrapper.setProps({
	// 		item: {
	// 			...wrapper.vm.item,
	// 			link: 'padang2.mamikos.com/invoice/C8oak'
	// 		}
	// 	});
	// 	await wrapper.vm.$nextTick();
	// 	expect(wrapper.vm.noHttpText).toBe('padang2.mamikos.com/invoice/C8oak');
	// });

	// test('copy text to clipboard fail', async () => {
	// 	const wrapper = mountComponent();
	// 	const spy = jest.spyOn(global.console, 'error').mockImplementation();

	// 	navigator.clipboard.writeText = jest.fn(() => {
	// 		throw new Error('Error');
	// 	});

	// 	wrapper.find('.copy-invoice .icon-copy').trigger('click');

	// 	expect(spy).toBeCalled();
	// 	spy.mockRestore();
	// });

	test('open note component on parent component', async () => {
		const wrapper = mountComponent();
		wrapper.vm.childClickOpenNotes();
		await wrapper.vm.$nextTick();
		expect(wrapper.emitted('childClickOpenNotes')).toBeTruthy();
	});

	test('reminder user on parent component', async () => {
		const wrapper = mountComponent();
		wrapper.vm.childClickRemindTenant();
		await wrapper.vm.$nextTick();
		expect(wrapper.emitted('childClickRemindTenant')).toBeTruthy();
	});

	test('show note if note not empty', async () => {
		const wrapper = mountComponent();
		expect(wrapper.find('.note-content').isVisible()).toBe(true);
	});

	test('shorten expand note text', async () => {
		const wrapper = mountComponent();
		expect(wrapper.vm.noteLong.length <= 80).toBe(false);
		expect(wrapper.vm.noteShort.length <= wrapper.vm.noteLong.length).toBe(
			true
		);
		expect(wrapper.vm.noteDisplay).toBe(wrapper.vm.noteShort);

		wrapper.find('.note-content a').trigger('click');
		await wrapper.vm.$nextTick();

		expect(wrapper.vm.noteDisplay).toBe(wrapper.vm.noteLong);
	});

	test('note text does not need shorten', async () => {
		propsData.item.note = 'short message is short';
		const wrapper = mountComponent();
		expect(wrapper.vm.noteLong.length <= 80).toBe(true);
		expect(wrapper.vm.noteDisplay).toBe(propsData.item.note);
	});

	test('check if random number length is 14 and a string', async () => {
		const wrapper = mountComponent();
		expect(typeof wrapper.vm.uniqueId()).toBe('string');
		expect(wrapper.vm.uniqueId().length).toBe(14);
	});

	test('change rent price', async () => {
		const wrapper = mountComponent();
		jest.spyOn(wrapper.vm, 'uniqueId').mockReturnValue('1234567890');

		wrapper.vm.handleChangeAdditionalCost();
		wrapper.vm.$store.commit('setUniqueId', 111);
		wrapper.vm.$store.commit(
			'setAdditionalCost',
			propsData.item.additional_costs
		);

		jest.advanceTimersByTime(1);
		expect(wrapper.vm.$router.push).toBeCalledWith({
			name: 'invoice.edit-costs',
			params: {
				invoiceId: 2127
			},
			query: {
				unique_id: `invoice-${wrapper.vm.uniqueId()}`,
				tenant_id: wrapper.vm.$route.params.id,
				from_booking: wrapper.vm.$route.params.booking
			}
		});
	});

	test('change other price', async () => {
		const wrapper = mountComponent();
		jest.spyOn(wrapper.vm, 'uniqueId').mockReturnValue('1234567890');

		wrapper.vm.handleChangeAmount();
		wrapper.vm.$store.commit('setUniqueId', 111);
		wrapper.vm.$store.commit('setAmount', propsData.item.amount);

		jest.advanceTimersByTime(1);
		expect(wrapper.vm.$router.push).toBeCalledWith({
			name: 'invoice.edit-amount',
			params: {
				invoiceId: 2127
			},
			query: {
				unique_id: `invoice-${wrapper.vm.uniqueId()}`,
				tenant_id: wrapper.vm.$route.params.id,
				from_booking: wrapper.vm.$route.params.booking
			}
		});
	});
});
