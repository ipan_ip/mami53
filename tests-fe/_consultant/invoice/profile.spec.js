import { shallowMount } from '@vue/test-utils';

import Profile from 'Consultant/pages/invoice/components/Profile.vue';

describe('Consultant/pages/invoice/components/Profile.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return shallowMount(Profile, options);
		};
	});

	test('render correctly', async () => {
		const wrapper = mountComponent();

		const tree = shallowMount(Profile, {
			propsData: {
				disableProfileDetail: true,
				avatarImage: '',
				profileName: 'Test Profile Name'
			}
		});

		await wrapper.vm.$nextTick();

		expect(tree.element).toMatchSnapshot();
	});

	test('click profile call parent method', () => {
		const wrapper = mountComponent();
		wrapper.vm.childProfileClick();
		expect(wrapper.emitted('childProfileClick')).toBeTruthy();
	});

	test('disable click if not potential tenant', async () => {
		const wrapper = mountComponent();

		wrapper.setProps({ disableProfileDetail: true });

		await wrapper.vm.$nextTick();

		expect(wrapper.find('.profile-container').classes('disabled')).toBe(true);
	});

	test('show name on profile', async () => {
		const wrapper = mountComponent();

		const mockName = 'test name';
		wrapper.setProps({ profileName: mockName });

		await wrapper.vm.$nextTick();

		expect(wrapper.find('.information .name').text()).toBe(mockName);
	});
});
