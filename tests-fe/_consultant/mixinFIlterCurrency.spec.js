import mixinFilterCurrency from 'Js/_consultant/mixins/mixinFilterCurrency';

describe('Js/_consultant/mixins/mixinFilterCurrency', () => {
	let value = null;
	beforeAll(() => {
		value = 15000000;
	});

	test.skip('add currency symbol', () => {
		const expected = 'Rp 15,000,000';
		expect(mixinFilterCurrency.filters.currencyID(value)).toBe(expected);
	});

	test.skip('show empty string if not valid value', () => {
		value = Infinity;
		expect(mixinFilterCurrency.filters.currencyID(value)).toBe('');
	});
});
