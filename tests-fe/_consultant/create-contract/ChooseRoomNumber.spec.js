import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import ChooseRoomNumber from 'Consultant/pages/create-contract/ChooseRoomNumber';
import CtButton from 'Consultant/components/ConsultantButton';

import createContract from 'Consultant/store/modules/createContract';
import availableRooms from './data/availableRooms.json';
import { flushPromises } from 'tests-fe/utils/promises';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import EventBus from 'Consultant/event-bus';

jest.mock('Consultant/mixins/mixinUniqueId', () => {
	return {};
});

jest.useFakeTimers();

const localVue = createLocalVue();
mockVLazy(localVue);
localVue.use(Vuex);

const store = new Vuex.Store({
	modules: {
		createContract
	}
});

describe('ChooseRoomNumber.vue', () => {
	let mountComponent = Function;
	let $router;

	beforeEach(() => {
		global.axios = {
			get: jest.fn().mockResolvedValue({ data: availableRooms })
		};

		$router = { push: jest.fn() };

		mountComponent = (options = {}) => {
			return shallowMount(ChooseRoomNumber, {
				localVue,
				store,
				mocks: {
					$router,
					$route: {
						params: {
							propertyId: 1
						},
						query: {
							unique_id: 'random-string'
						},
						meta: {
							navbarOptions: {}
						}
					}
				},
				stubs: ['router-link'],
				...options
			});
		};
	});

	it('Should render component and match snapshot', async () => {
		const wrapper = mountComponent();

		await flushPromises();

		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should add property label, value and disabled in every list', async () => {
		const wrapper = mountComponent();

		await flushPromises();

		expect(wrapper.vm.roomNumberLists).toEqual(
			expect.arrayContaining([
				expect.objectContaining({
					label: expect.any(String),
					value: expect.anything(),
					disabled: expect.any(Boolean)
				})
			])
		);
	});

	it('Should add subtitle on the list of room units', async () => {
		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: {
					status: true,
					room_units: [
						{
							id: 1,
							has_active_contract: true,
							name: 'Kost 1',
							floor: 1,
							occupied: true
						},
						{
							id: 2,
							has_active_contract: false,
							name: 'Kost 2',
							floor: 1,
							occupied: false
						}
					]
				}
			})
		};
		const wrapper = mountComponent();

		await flushPromises();

		expect(wrapper.vm.roomNumberProcessed).toEqual(
			expect.arrayContaining([
				expect.objectContaining({ subtitle: true, label: expect.any(String) }),
				expect.objectContaining({ disabled: false, name: 'Kost 2' }),
				expect.objectContaining({ subtitle: true, label: expect.any(String) }),
				expect.objectContaining({ disabled: true, name: 'Kost 1' })
			])
		);
	});

	it('Should set search state with debounce 500 ms', async () => {
		const src = 'test';
		const ct = jest.spyOn(global, 'clearTimeout');
		const wrapper = mountComponent({
			stubs: {
				'router-link': { template: '<a></a>' },
				CtInputText: {
					template: `<input id="search" @input="$emit('input', '${src}')" />`
				}
			}
		});

		await wrapper.vm.$nextTick();

		wrapper.find('#search').trigger('input');

		expect(wrapper.vm.search).toBe('');

		wrapper.find('#search').trigger('input');

		expect(ct).toHaveBeenCalled();

		jest.advanceTimersByTime(500);

		expect(wrapper.vm.search).toBe(src);
	});

	it('Should get list base on search', async () => {
		const mockRooms = [
			{
				id: 1,
				has_active_contract: false,
				name: 'test 1',
				floor: 1,
				occupied: false
			},
			{
				id: 2,
				has_active_contract: false,
				name: 'test 2',
				floor: 1,
				occupied: false
			}
		];

		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: {
					status: true,
					room_units: mockRooms
				}
			})
		};
		const wrapper = mountComponent({
			stubs: {
				'router-link': { template: '<a></a>' },
				CtInputText: {
					template: `<input id="search" @input="$emit('input', '2')" />`
				}
			}
		});

		await flushPromises();

		expect(wrapper.vm.roomNumberProcessed).toMatchSnapshot();

		wrapper.find('#search').trigger('input');

		jest.advanceTimersByTime(500);

		expect(wrapper.vm.roomNumberProcessed).toMatchSnapshot();
	});

	it('Should set selected room by with data from vuex', async () => {
		const wrapper = mountComponent();

		const selectedRoom = { id: 'test' };
		expect(wrapper.vm.selectedRoom).toBe('');

		wrapper.vm.setRoomNumber(selectedRoom);

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.selectedRoom).toBe(selectedRoom.id);
	});

	it('Should set vuex roomNumber and move to create contract when submitted', async () => {
		const mockRooms = [
			{
				id: 1,
				has_active_contract: false,
				name: 'test 1',
				floor: 1,
				occupied: false
			}
		];

		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: {
					status: true,
					room_units: mockRooms
				}
			})
		};

		const RadioGroup = {
			template: `<input type="radio" @input="$emit('input', options[1].id)" />`,
			props: ['options']
		};

		const wrapper = mountComponent({
			stubs: {
				'router-link': { template: '<a></a>' },
				CtButton: CtButton,
				CtRadioGroup: RadioGroup
			}
		});

		const setRoom = jest.spyOn(wrapper.vm, 'setRoomNumber');

		await flushPromises();

		wrapper.findComponent(RadioGroup).trigger('input');

		await wrapper.vm.$nextTick();

		wrapper.find('button').trigger('click');

		expect(setRoom).toHaveBeenCalledWith(expect.objectContaining(mockRooms[0]));
		expect($router.push).toHaveBeenCalledWith(
			expect.objectContaining({ name: expect.stringContaining('contract') })
		);
	});

	it('Should show alert when fetch room was rejected', async () => {
		global.axios = {
			get: jest.fn().mockRejectedValue(new Error())
		};
		const bugsnag = jest.fn();
		global.bugsnagClient = { notify: bugsnag };

		const openAlert = jest.fn();
		EventBus.$on('openAlert', openAlert);

		mountComponent();

		await flushPromises();

		expect(bugsnag).toHaveBeenCalledWith(expect.any(Error));
		expect(openAlert).toHaveBeenCalledWith(expect.any(String));
	});

	it('Should show alert when fetch room was failed', async () => {
		const message = 'error message';
		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: {
					status: false,
					meta: {
						message
					}
				}
			})
		};

		const openAlert = jest.fn();
		EventBus.$on('openAlert', openAlert);

		mountComponent();

		await flushPromises();

		expect(openAlert).toHaveBeenCalledWith(message);
	});

	it('Should request axios with property id ', async () => {
		const axiosGet = jest.fn();
		const propertyId = 'test';
		global.axios = {
			get: axiosGet
		};

		mountComponent({
			mocks: {
				$route: {
					params: {
						propertyId
					},
					query: {
						unique_id: 'random-string'
					},
					meta: {
						navbarOptions: {}
					}
				}
			}
		});

		expect(axiosGet).toHaveBeenCalledWith(expect.stringContaining(propertyId));
	});
});
