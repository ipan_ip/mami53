export const propertyFormData = {
	rent_count: 'monthly',
	rent_duration: '10',
	room_number: 'test',
	price: 20000,
	note: 'test note',
	date_in: 'test date in',
	other_costs: [
		{
			name: 'test other cost',
			price: 20000
		}
	],
	fix_costs: [
		{
			name: 'test fix cost',
			price: 50000
		}
	],
	pinalty_cost: {
		isActive: true,
		cost: 10000,
		duration_time: 10,
		duration_unit: 'month'
	}
};

export const property = {
	value: 'test property selected'
};

export const tenant = {
	name: 'test name',
	phone_number: '08623723624',
	gender_value: 'male',
	email: 'test@mail.com',
	occupation: 'Karyawan',
	marital_status: 'Belum Kawin',
	parent_name: 'Rahasia',
	parent_phone_number: '083627242'
};

export const photos = {
	identity: { id: 'identity id' },
	document: { id: 'document id' }
};

export const roomNumber = {
	id: 'room number id',
	name: 'room number name'
};
