import { shallowMount } from '@vue/test-utils';
import AddPenaltyCost from 'Consultant/pages/create-contract/partials/addPenaltyCost';

jest.useFakeTimers();

describe('AddPenaltyCost.vue', () => {
	let mountComponent;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return shallowMount(AddPenaltyCost, {
				propsData: {
					value: {}
				},
				...options
			});
		};
	});

	it('Should render component and match snapshot', () => {
		const wrapper = mountComponent();

		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should add class "overflow-visible"', async () => {
		const wrapper = mountComponent({
			stubs: {
				ToggleButton: {
					template: `<input class="switch" v-model="value" type="checkbox" @change="$emit('change', { value })" />`,
					data() {
						return { value: false };
					}
				}
			}
		});

		wrapper.find('.switch').setChecked();

		jest.runAllTimers();

		const formEl = wrapper.findComponent({ ref: 'pinaltyCostForm' });
		expect(formEl.classes('overflow-visible')).toBe(true);
	});

	it('Should remove class "overflow-visible"', async () => {
		const wrapper = mountComponent({
			stubs: {
				ToggleButton: {
					template: `<input class="switch" type="checkbox" @change="$emit('change', { value: false })" />`
				}
			}
		});

		wrapper.vm.handleTogglePinaltyCost({ value: true });

		jest.runAllTimers();

		const formEl = wrapper.findComponent({ ref: 'pinaltyCostForm' });

		expect(formEl.classes('overflow-visible')).toBe(true);

		wrapper.find('.switch').trigger('change');

		jest.runAllTimers();

		expect(formEl.classes('overflow-visible')).toBe(false);
	});

	it('Set duration time with data inputed when select duration unit and duration time has error', async () => {
		const wrapper = mountComponent({
			stubs: {
				InputField: {
					template: `<input v-model="inputData" class="duration-time" />`,
					data() {
						return {
							inputData: '',
							hasError: true
						};
					}
				},
				ConsultantSelect: {
					template: `<select @change="$emit('change')"></select>`
				}
			}
		});

		const duration = 'test';
		const input = wrapper.find('.duration-time');
		input.element.value = duration;
		input.trigger('input');

		wrapper.find('select').trigger('change');

		expect(wrapper.vm.pinaltyCost.duration_time).toBe(duration);
	});

	it('Should set pinaltyCost state when using updatedValue prop', async () => {
		const updatedValue = {
			isActive: true,
			cost: 'test cost',
			duration_time: 'test time',
			duration_unit: 'test unit'
		};
		const wrapper = mountComponent({
			propsData: {
				value: {},
				updatedValue
			}
		});

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.pinaltyCost).toEqual(updatedValue);
	});
});
