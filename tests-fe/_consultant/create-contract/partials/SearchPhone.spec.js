import { shallowMount, createLocalVue } from '@vue/test-utils';
import SearchPhone from 'Consultant/pages/create-contract/partials/SearchPhone';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

import createContract from 'Consultant/store/modules/createContract';
import { flushPromises } from 'tests-fe/utils/promises';
import EventBus from 'Consultant/event-bus';
import { contract } from 'Consultant/event-bus/eventNames';

import Vuex from 'vuex';

const localVue = createLocalVue();
mockVLazy(localVue);
localVue.use(Vuex);

describe('SearchPhone.vue', () => {
	let mountComponent = Function;
	const $router = { push: jest.fn() };

	beforeEach(() => {
		mountComponent = (state = {}) => {
			const store = new Vuex.Store({
				modules: {
					createContract: {
						...createContract,
						state: {
							...createContract.state,
							property: {
								label: 'Selected Property',
								value: 1
							},
							...state
						}
					}
				}
			});

			return shallowMount(SearchPhone, {
				localVue,
				store,
				stubs: {
					ConsultantButton: {
						template: `<button @click="$emit('click')"></button>`
					},
					InputField: {
						template: `<input v-model="value" @input="$emit('input', value)" />`,
						data() {
							return { value: '' };
						}
					}
				},
				mocks: {
					$isEmpty: data => !Object.keys(data).length,
					$router
				}
			});
		};
	});

	it('Should render component and match snapshot', async () => {
		const wrapper = mountComponent();

		await wrapper.vm.$nextTick();

		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should move next section directly after "Lanjutkan" button has been clicked', async () => {
		const phone = '086737374';

		const wrapper = mountComponent({
			section: 2,
			tenant: {
				phone_number: phone
			}
		});

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.section).toBe(2);

		const input = wrapper.find('input');
		input.element.value = phone;
		input.trigger('input');

		wrapper.find('button').trigger('click');

		expect(wrapper.vm.section).toBe(3);
	});

	it('Should set tenant and next section when "Lanjutkan" button has been clicked', async () => {
		const tenantData = {
			phone_number: 'test'
		};

		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: {
					status: true,
					data: {
						tenant: tenantData
					}
				}
			})
		};

		const wrapper = mountComponent({
			section: 2
		});

		await wrapper.vm.$nextTick();

		const input = wrapper.find('input');
		input.element.value = 'test';
		input.trigger('input');

		await wrapper.vm.$nextTick();

		wrapper.find('button').trigger('click');

		await flushPromises();

		expect(wrapper.vm.tenant).toEqual(tenantData);
		expect(wrapper.vm.section).toBe(3);
	});

	it('Should show error message and open the snackbar', async () => {
		const message = {
			title: 'test title',
			message: 'test message'
		};

		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: {
					status: false,
					message
				}
			})
		};

		const wrapper = mountComponent({
			section: 2
		});

		await wrapper.vm.$nextTick();

		const input = wrapper.find('input');
		input.element.value = 'test';
		input.trigger('input');

		await wrapper.vm.$nextTick();

		wrapper.find('button').trigger('click');

		await flushPromises();

		expect(wrapper.vm.errorMessage).toEqual(message);
		expect(wrapper.vm.isSnackbarOpen).toBe(true);
	});

	it('Should show error message and open the snackbar', async () => {
		const error = new Error('test error');
		const bugsnagNotify = jest.fn();

		global.bugsnagClient = { notify: bugsnagNotify };

		global.axios = {
			get: jest.fn().mockRejectedValue(error)
		};

		const wrapper = mountComponent({
			section: 2
		});

		await wrapper.vm.$nextTick();

		const input = wrapper.find('input');
		input.element.value = 'test';
		input.trigger('input');

		await wrapper.vm.$nextTick();

		wrapper.find('button').trigger('click');

		await flushPromises();

		expect(bugsnagNotify).toHaveBeenCalledWith(error);
	});

	it('Should go to contract list when button on snackbar has been clicked', async () => {
		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: {
					status: false,
					message: null
				}
			})
		};

		const wrapper = mountComponent({
			section: 2
		});

		await wrapper.vm.$nextTick();

		const input = wrapper.find('input');
		input.element.value = 'test';
		input.trigger('input');

		await wrapper.vm.$nextTick();

		wrapper.find('button').trigger('click');

		await flushPromises();

		wrapper
			.findAll('button')
			.at(1)
			.trigger('click');

		expect($router.push).toHaveBeenCalledWith({ name: 'contract' });
	});

	it('Should reset search by event bus', async () => {
		const search = 'test search';
		const wrapper = mountComponent({
			section: 2
		});

		await wrapper.vm.$nextTick();

		const input = wrapper.find('input');
		input.element.value = search;
		input.trigger('input');

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.searchText).toBe(search);

		EventBus.$emit(contract.resetPhoneState);

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.searchText).toBe('');
	});
});
