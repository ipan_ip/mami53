import { mount, createLocalVue } from '@vue/test-utils';
import CreateContractSuccessPage from 'Consultant/pages/create-contract/partials/CreateContractSuccessPage';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

import createContract from 'Consultant/store/modules/createContract';
import EventBus from 'Consultant/event-bus';
import { contract } from 'Consultant/event-bus/eventNames';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

import Vuex from 'vuex';

const localVue = createLocalVue();
mockVLazy(localVue);
localVue.use(Vuex);

describe('CreateContractSuccessPage.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}, state = {}) => {
			const store = new Vuex.Store({
				modules: {
					createContract: {
						...createContract,
						state: {
							...createContract.state,
							section: 4,
							...state
						}
					}
				}
			});

			return mount(CreateContractSuccessPage, {
				localVue,
				store,
				propsData: {
					open: true
				},
				stubs: {
					ConsultantButton: {
						template: `<button @click="$emit('click')"></button>`
					}
				},
				mocks: {
					$route: {
						query: {}
					}
				},
				...options
			});
		};
	});

	it('Should render component and match snapshot', async () => {
		const wrapper = mountComponent();

		await wrapper.vm.$nextTick();

		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should render component differently when entered from potential tenant', async () => {
		const wrapper = mountComponent({
			mocks: {
				$route: {
					query: {
						prev_page: 'potential-tenant',
						tenant_phone: 'test',
						property_id: 'test'
					}
				}
			}
		});

		await wrapper.vm.$nextTick();

		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should back to first section and emit some event', async () => {
		const push = jest.fn();

		const wrapper = mountComponent({
			mocks: {
				$router: { push },
				$route: {
					query: {}
				}
			}
		});

		const resetProperty = jest.fn();
		const resetPhone = jest.fn();
		const resetImage = jest.fn();
		EventBus.$on(contract.resetPropertyState, resetProperty);
		EventBus.$on(contract.resetPhoneState, resetPhone);
		EventBus.$on(contract.resetImageUpload, resetImage);

		await wrapper.vm.$nextTick();

		wrapper.find('button').trigger('click');

		expect(push).toHaveBeenCalledWith(
			expect.objectContaining({ name: 'contract.create' })
		);
		expect(resetProperty).toHaveBeenCalled();
		expect(resetPhone).toHaveBeenCalled();
		expect(resetImage).toHaveBeenCalled();
	});

	it('Should hide success page and go to contact list', async () => {
		const push = jest.fn();

		const wrapper = mountComponent({
			mocks: {
				$router: { push },
				$route: {
					query: {}
				}
			}
		});

		const updateOpen = jest.fn();
		wrapper.vm.$on('update:open', updateOpen);

		await wrapper.vm.$nextTick();

		wrapper
			.findAll('button')
			.at(1)
			.trigger('click');

		expect(push).toHaveBeenCalledWith({ name: 'contract' });
		expect(updateOpen).toHaveBeenCalledWith(false);
	});

	it('Should back to first section and emit some event', async () => {
		const push = jest.fn();

		const wrapper = mountComponent({
			mocks: {
				$router: { push },
				$route: {
					query: {
						prev_page: 'potential-tenant',
						tenant_phone: 'test',
						property_id: 'test'
					}
				}
			}
		});

		await wrapper.vm.$nextTick();

		wrapper.find('button').trigger('click');

		expect(push).toHaveBeenCalledWith(
			expect.objectContaining({ name: 'potential-tenant' })
		);
	});

	it('Should remove class "success-page-open"', async () => {
		const remove = jest.fn();
		mockWindowProperty('document.body.classList', {
			remove
		});

		const wrapper = mountComponent({
			stubs: {
				ConsultantSuccessPage: {
					template: `<div><slot></slot></div>`
				}
			}
		});

		wrapper.destroy();

		expect(remove).toHaveBeenCalledWith('success-page-open');
	});
});
