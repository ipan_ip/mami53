import { shallowMount, createLocalVue } from '@vue/test-utils';
import SearchProperty from 'Consultant/pages/create-contract/partials/SearchProperty';

import createContract from 'Consultant/store/modules/createContract';
import VeeValidate from 'Consultant/config/veeValidate';
import EventBus from 'Consultant/event-bus';
import { contract } from 'Consultant/event-bus/eventNames';
import { flushPromises } from 'tests-fe/utils/promises';

import Vuex from 'vuex';

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.use(VeeValidate);

const store = new Vuex.Store({
	modules: {
		createContract
	}
});

jest.useFakeTimers();

describe('SearchProperty.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return shallowMount(SearchProperty, {
				localVue,
				store,
				mocks: {
					$route: {
						query: {},
						meta: {
							navbarOptions: {}
						}
					}
				},
				...options
			});
		};
	});

	it('Should render component and match snapshot', async () => {
		const wrapper = mountComponent();

		await wrapper.vm.$nextTick();

		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should define bus listener correctly', async () => {
		const wrapper = mountComponent();

		wrapper.setData({ searchText: 'test' });

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.searchText).toBe('test');

		EventBus.$emit(contract.resetPropertyState);

		expect(wrapper.vm.searchText).toBe('');
	});

	it('Should cannot search when text inputted less than 3', async () => {
		const wrapper = mountComponent({
			stubs: {
				ConsultantButton: {
					template: `<button @click="$emit('click')"></button>`
				}
			}
		});

		wrapper.setData({ searchText: '12' });

		await wrapper.vm.$nextTick();

		wrapper.find('button').trigger('click');

		await flushPromises();

		expect(wrapper.vm.$validator.errors.any()).toBe(true);
	});

	it('Should hit API search and show search result after 500ms', async () => {
		const searchText = 'test';
		const requestAPI = jest
			.fn()
			.mockResolvedValue({ data: { success: true, data: [] } });

		global.axios = {
			get: requestAPI
		};

		const wrapper = mountComponent({
			stubs: {
				PropertySearchResult: {
					template: `<div class="search-result"></div>`
				},
				ConsultantButton: {
					template: `<button @click="$emit('click')"></button>`
				}
			}
		});

		wrapper.setData({ searchText });

		await wrapper.vm.$nextTick();

		wrapper.find('button').trigger('click');

		await flushPromises();

		expect(requestAPI).toHaveBeenCalledWith(
			expect.stringContaining(`/api/contract/search-property/${searchText}`)
		);

		expect(wrapper.find('.search-result').exists()).toBe(false);

		jest.advanceTimersByTime(500);

		await wrapper.vm.$nextTick();

		expect(wrapper.find('.search-result').exists()).toBe(true);
	});

	describe('When button back on navbar was clicked', () => {
		let mountWithMockRouter;

		beforeEach(() => {
			mountWithMockRouter = (mocks = {}) =>
				mountComponent({
					mocks: {
						$route: {
							query: {},
							meta: {
								navbarOptions: {}
							}
						},
						...mocks
					},
					stubs: {
						PropertySearchResult: {
							template: `<div class="search-result"></div>`
						}
					}
				});
		});

		it('Should hide search result component', async () => {
			const wrapper = mountWithMockRouter();
			wrapper.setData({ showSearchResultPage: true });

			await wrapper.vm.$nextTick();

			const resultElm = wrapper.find('.search-result');

			expect(resultElm.exists()).toBe(true);

			wrapper.vm.handleClickBackAction();

			await wrapper.vm.$nextTick();

			expect(resultElm.exists()).toBe(false);
		});

		it('Should go to previous page according to query passed', async () => {
			const prevPageName = 'test';
			const push = jest.fn();
			const wrapper = mountWithMockRouter({
				$route: {
					query: {
						prev_page: prevPageName
					},
					meta: {
						navbarOptions: {}
					}
				},
				$router: {
					push,
					options: {
						routes: [
							{
								name: prevPageName
							}
						]
					}
				}
			});

			wrapper.vm.handleClickBackAction();

			expect(push).toHaveBeenCalledWith({ name: prevPageName });
		});

		it('Should go to previous page', async () => {
			const prevPathName = 'test';
			const push = jest.fn();
			const wrapper = mountWithMockRouter({
				$route: {
					query: {},
					meta: {
						navbarOptions: {
							prevPathName
						}
					}
				},
				$router: {
					push
				}
			});

			wrapper.vm.handleClickBackAction();

			expect(push).toHaveBeenCalledWith({ name: prevPathName });
		});
	});
});
