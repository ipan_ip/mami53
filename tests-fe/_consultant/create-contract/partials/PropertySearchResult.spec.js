import { mount, createLocalVue } from '@vue/test-utils';
import PropertySearchResult from 'Consultant/pages/create-contract/partials/PropertySearchResult';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

import createContract from 'Consultant/store/modules/createContract';

import Vuex from 'vuex';

const localVue = createLocalVue();
mockVLazy(localVue);
localVue.use(Vuex);

const nextSection = jest.fn();
createContract.mutations.nextSection = nextSection;

const store = new Vuex.Store({
	modules: {
		createContract
	}
});

describe('PropertySearchResult.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return mount(PropertySearchResult, {
				localVue,
				store,
				...options
			});
		};
	});

	it('Should render component with loading interface', async () => {
		const wrapper = mountComponent({
			propsData: {
				loadingSearch: true,
				propertyLists: []
			}
		});

		await wrapper.vm.$nextTick();

		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should render component with list of property', async () => {
		const wrapper = mountComponent({
			propsData: {
				loadingSearch: false,
				propertyLists: [
					{
						label: 'property 1',
						value: 12324
					}
				]
			}
		});

		await wrapper.vm.$nextTick();

		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should render empty state when has not result', async () => {
		const wrapper = mountComponent({
			propsData: {
				loadingSearch: false,
				propertyLists: []
			},
			stubs: {
				ConsultantEmptyState: {
					template: `<div class="empty-state-stub"></div>`
				}
			}
		});

		await wrapper.vm.$nextTick();

		expect(wrapper.find('.empty-state-stub').exists()).toBe(true);
	});

	it('Should emit an event "hide-result-page"', async () => {
		const wrapper = mountComponent({
			propsData: {
				loadingSearch: false,
				propertyLists: []
			},
			stubs: {
				ConsultantButton: {
					template: `<button class="hide" @click="$emit('click')"></button>`
				}
			}
		});

		const hideResultPage = jest.fn();
		wrapper.vm.$on('hide-result-page', hideResultPage);

		await wrapper.vm.$nextTick();

		wrapper.find('.hide').trigger('click');

		expect(hideResultPage).toHaveBeenCalled();
	});

	it('Should set vuex with selected property and move next section', async () => {
		const properties = [
			{
				label: 'property 1',
				value: 1
			},
			{
				label: 'property 2',
				value: 2
			}
		];

		const wrapper = mountComponent({
			propsData: {
				loadingSearch: false,
				propertyLists: properties
			},
			stubs: {
				ConsultantButton: {
					template: `<button class="next" @click="$emit('click')"></button>`
				}
			}
		});

		wrapper.setData({ propertyIdSelected: properties[1].value });

		await wrapper.vm.$nextTick();

		wrapper.find('.next').trigger('click');

		expect(wrapper.vm.$store.state.createContract.property).toEqual(
			properties[1]
		);
		expect(nextSection).toHaveBeenCalled();
	});
});
