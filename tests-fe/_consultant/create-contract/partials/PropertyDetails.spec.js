import { shallowMount, createLocalVue } from '@vue/test-utils';
import PropertyDetails from 'Consultant/pages/create-contract/partials/PropertyDetails';
import createContract from 'Consultant/store/modules/createContract';
import EventBus from 'Consultant/event-bus';
import propertyDetail from '../data/propertyDetail.json';
import { flushPromises } from 'tests-fe/utils/promises';

import * as submitData from '../data/mockSubmitData';

import Vuex from 'vuex';

const localVue = createLocalVue();
// mockVLazy(localVue);
localVue.use(Vuex);

global.axios = {
	get: jest.fn().mockResolvedValue({
		data: propertyDetail
	})
};

describe('PropertyDetails.vue', () => {
	let mountComponent = Function;
	const $dayjs = date => ({ format: () => date });

	beforeEach(() => {
		mountComponent = (options = {}, state = {}) => {
			const store = new Vuex.Store({
				state: {
					uniqueId: ''
				},
				modules: {
					createContract: {
						...createContract,
						state: {
							...createContract.state,
							section: 4,
							...state
						}
					}
				},
				mutations: {
					setUniqueId(state, payload) {
						state.uniqueId = payload;
					}
				}
			});

			return shallowMount(PropertyDetails, {
				localVue,
				store,
				propsData: {
					open: true
				},
				stubs: {
					ConsultantButton: {
						template: `<button @click="$emit('click')"></button>`
					}
				},
				mocks: {
					$route: {
						query: {}
					},
					$dayjs
				},
				...options
			});
		};
	});

	it('Should render component and match snapshot', async () => {
		const wrapper = mountComponent();

		await flushPromises();

		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should set propertyDetail state by property from API', async () => {
		const propertyId = 'test property id';
		const axiosGet = jest.fn().mockResolvedValue({
			data: propertyDetail
		});

		global.axios = { get: axiosGet };

		const expected = propertyDetail.data[0];

		const wrapper = mountComponent(
			{},
			{
				property: {
					value: propertyId
				}
			}
		);

		expect(axiosGet).toHaveBeenCalledWith(`/api/property/${propertyId}`);

		await flushPromises();

		expect(wrapper.vm.propertyDetail).toEqual(expected);
	});

	it('Should price of propertyFormData state with "price_monthly" from API', async () => {
		const expected = propertyDetail.data[0].price_monthly;

		const wrapper = mountComponent();

		await flushPromises();

		expect(wrapper.vm.propertyFormData.price).toEqual(expected);
	});

	it("Should filter rent options by price of property's data have", async () => {
		const axiosGet = jest.fn().mockResolvedValue({
			data: {
				status: true,
				data: [
					{
						price_monthly: 6570000,
						price_weekly: 4500000
					}
				]
			}
		});

		global.axios = { get: axiosGet };

		const wrapper = mountComponent();

		await flushPromises();

		expect(wrapper.vm.rentCountOptions).toEqual([
			expect.objectContaining({ value: 'monthly' }),
			expect.objectContaining({ value: 'weekly' })
		]);
	});

	it('Should not hit property API if from room number page', async () => {
		const axiosGet = jest.fn().mockResolvedValue({
			data: propertyDetail
		});

		global.axios = { get: axiosGet };

		mountComponent({
			mocks: {
				$route: {
					query: {
						previous_page: 'room_number_page'
					}
				},
				$dayjs
			}
		});

		expect(axiosGet).not.toHaveBeenCalled();
	});

	it('Should set some state with data from vuex that stored before', async () => {
		const propertyFormData = {
			test: 'propertyFormData',
			pinalty_cost: 'test',
			rent_count: 'monthly'
		};
		const detail = { test: 'propertyDetail' };
		const rentCountOptions = [{ value: 'rent_montly' }];
		const roomNumber = { name: 'test room number' };

		const wrapper = mountComponent(
			{
				stubs: {
					AddPenaltyCost: {
						template: `<div></div>`
					}
				}
			},
			{
				keep: true,
				roomNumber,
				tempPropertyDetail: {
					propertyFormData,
					detail,
					rentCountOptions
				}
			}
		);

		expect(wrapper.vm.propertyFormData).toEqual({
			...propertyFormData,
			room_number: roomNumber.name
		});
		expect(wrapper.vm.propertyDetail).toEqual(detail);
		expect(wrapper.vm.pinaltyCostUpdated).toEqual(
			propertyFormData.pinalty_cost
		);
		expect(wrapper.vm.rentCountOptions).toEqual(rentCountOptions);
	});

	it('Should go to choose room number page and store current data', async () => {
		const propertyId = 'property id';
		const push = jest.fn();
		const wrapper = mountComponent(
			{
				mocks: {
					$route: {
						query: {}
					},
					$router: {
						push
					},
					$dayjs
				},
				stubs: {
					InputField: {
						template: `<input />`,
						props: ['legend']
					}
				}
			},
			{
				property: {
					value: propertyId
				}
			}
		);

		await flushPromises();

		expect(wrapper.vm.tempPropertyDetail).toEqual({});

		wrapper.find('input').trigger('click');

		expect(wrapper.vm.tempPropertyDetail).toEqual({
			detail: expect.any(Object),
			propertyFormData: expect.any(Object),
			rentDurationOptions: expect.any(Array),
			rentCountOptions: expect.any(Array)
		});
		expect(wrapper.vm.$store.state.createContract.keep).toBe(true);
		expect(wrapper.vm.$store.state.uniqueId).not.toBe('');
		expect(push).toHaveBeenCalledWith({
			name: 'contract.create.room-number',
			params: { propertyId: propertyId },
			query: {
				unique_id: expect.any(String),
				from: 'create_contract'
			}
		});
	});

	it('Should submit contract with data correctly', async () => {
		const axiosPost = jest.fn().mockResolvedValue({
			data: { status: true }
		});

		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: propertyDetail
			}),
			post: axiosPost
		};

		const {
			propertyFormData,
			property,
			tenant,
			photos,
			roomNumber
		} = submitData;

		const expectSubmitedData = {
			room_id: property.value,
			name: tenant.name,
			phone_number: tenant.phone_number,
			room_number: roomNumber.name,
			designer_room_id: roomNumber.id,
			gender: tenant.gender_value,
			email: tenant.email,
			occupation: tenant.occupation,
			marital_status: tenant.marital_status,
			parent_name: tenant.parent_name,
			parent_phone_number: tenant.parent_phone_number,
			photo_identifier_id: photos.identity.id,
			photo_document_id: photos.document.id,
			start_date: propertyFormData.date_in,
			rent_type: 'month',
			amount: propertyFormData.price,
			duration: +propertyFormData.rent_duration,
			note: propertyFormData.note,
			fine_amount: propertyFormData.pinalty_cost.cost,
			fine_maximum_length: propertyFormData.pinalty_cost.duration_time,
			fine_duration_type: propertyFormData.pinalty_cost.duration_unit,
			additional_costs: [
				{
					field_title: 'test other cost',
					field_value: 20000,
					cost_type: 'other'
				},
				{
					field_title: 'test fix cost',
					field_value: 50000,
					cost_type: 'fixed'
				}
			]
		};

		const wrapper = mountComponent(
			{
				stubs: {
					ConsultantButton: {
						template: `<button @click="$emit('click')"></button>`
					}
				}
			},
			{
				property,
				tenant,
				roomNumber,
				photos
			}
		);

		wrapper.setData({ propertyFormData, loadingGetProperty: false });

		await wrapper.vm.$nextTick();

		wrapper.find('button').trigger('click');

		await flushPromises();

		expect(axiosPost).toHaveBeenCalledWith(
			'/api/tenant/contract',
			expectSubmitedData
		);
	});

	it('Should move to section 3 and hit event bus with message', async () => {
		const message = 'test message';
		const axiosPost = jest.fn().mockResolvedValue({
			data: {
				status: false,
				messages: {
					phone_number: [message]
				}
			}
		});

		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: propertyDetail
			}),
			post: axiosPost
		};

		const openAlert = jest.fn();
		EventBus.$on('openAlert', openAlert);

		const {
			propertyFormData,
			property,
			tenant,
			photos,
			roomNumber
		} = submitData;

		const wrapper = mountComponent(
			{
				stubs: {
					ConsultantButton: {
						template: `<button @click="$emit('click')"></button>`
					}
				}
			},
			{
				property,
				tenant,
				roomNumber,
				photos
			}
		);

		wrapper.setData({ propertyFormData, loadingGetProperty: false });

		await wrapper.vm.$nextTick();

		wrapper.find('button').trigger('click');

		await flushPromises();

		expect(openAlert).toHaveBeenCalledWith([message]);
		expect(wrapper.vm.section).toBe(3);
	});
});
