import { shallowMount, createLocalVue } from '@vue/test-utils';
import TenantInformations from 'Consultant/pages/create-contract/partials/TenantInformations';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

import createContract from 'Consultant/store/modules/createContract';
import { flushPromises } from 'tests-fe/utils/promises';
import { isEmpty } from 'lodash';

import Vuex from 'vuex';

const localVue = createLocalVue();
mockVLazy(localVue);
localVue.use(Vuex);

describe('TenantInformations.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (mocks = {}, state = {}) => {
			const store = new Vuex.Store({
				modules: {
					createContract: {
						...createContract,
						state: {
							...createContract.state,
							section: 3,
							property: {
								label: 'Selected Property',
								value: 1
							},
							...state
						}
					}
				}
			});

			return shallowMount(TenantInformations, {
				localVue,
				store,
				stubs: {
					ConsultantButton: {
						template: `<button @click="$emit('click')"></button>`
					},
					InputField: {
						template: `<input v-model="value" @input="$emit('input', value)" />`,
						data() {
							return { value: '' };
						}
					}
				},
				mocks: {
					$isEmpty: data => isEmpty(data),
					$route: {
						query: {}
					},
					...mocks
				}
			});
		};
	});

	it('Should render component and match snapshot', async () => {
		const wrapper = mountComponent();

		await wrapper.vm.$nextTick();

		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should update all state when tenant not empty before', async () => {
		const state = {
			tenant: {
				phone_number: '0873273',
				photo_document: 'test photo_document',
				photo_identifier: 'test photo_identifier',
				photo_identifier_id: 'test photo_identifier_id',
				photo_document_id: 'photo_document_id'
			}
		};

		const wrapper = mountComponent({}, state);

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.tenantData).toEqual(
			expect.objectContaining({
				phone_number: state.tenant.phone_number
			})
		);
	});

	it('Should push to previous page', async () => {
		const prevPathName = 'test';
		const push = jest.fn();
		const $router = {
			push,
			options: {
				routes: [
					{
						name: prevPathName
					}
				]
			}
		};

		const wrapper = mountComponent({
			$router,
			$route: {
				query: {
					prev_page: prevPathName
				}
			}
		});

		await wrapper.vm.$nextTick();

		wrapper.vm.handleClickBackAction();

		expect(push).toHaveBeenCalledWith({ name: prevPathName });
	});

	it('Should move to previous section', async () => {
		const $router = {
			options: {
				routes: [
					{
						name: 'test'
					}
				]
			}
		};

		const wrapper = mountComponent({
			$router,
			$route: {
				query: {}
			}
		});

		await wrapper.vm.$nextTick();

		wrapper.vm.handleClickBackAction();

		expect(wrapper.vm.section).toBe(2);
	});

	it('Should hit Tenant API directly when have "tenant_phone" and "property_id" query', async () => {
		const uniqueId = 'test_unique_id';
		const phone = '08172326424';

		const tenantData = {
			phone_number: 'test'
		};

		const axiosGet = jest.fn().mockResolvedValue({
			data: {
				status: true,
				data: {
					tenant: tenantData
				}
			}
		});

		global.axios = { get: axiosGet };

		const wrapper = mountComponent(
			{
				$route: {
					query: {
						prev_page: 'test',
						tenant_phone: phone,
						property_id: 'test',
						unique_id: uniqueId
					}
				}
			},
			{ uniqueId }
		);

		await wrapper.vm.$nextTick();

		expect(axiosGet).toHaveBeenCalledWith(
			`/api/tenant/search?phone_number=${phone}`
		);

		await flushPromises();

		expect(wrapper.vm.tenant).toEqual(tenantData);
	});

	it('Should push to "potential tenant" when vuex have not unique_id', async () => {
		const push = jest.fn();

		const wrapper = mountComponent(
			{
				$route: {
					query: {
						prev_page: 'test',
						tenant_phone: '08132435',
						property_id: 'test',
						unique_id: 'test unique id'
					}
				},
				$router: {
					push
				}
			},
			{ uniqueId: '' }
		);

		await wrapper.vm.$nextTick();

		expect(push).toHaveBeenCalledWith({ name: 'potential-tenant' });
	});

	it(`Should move to section 1 when doesn't have either "tenant_phone" and "property_id"`, async () => {
		const wrapper = mountComponent({
			$route: {
				query: {
					prev_page: 'test',
					tenant_phone: '08132435'
				}
			}
		});

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.section).toBe(1);
	});

	it(`Should move to section 1 when phone number is weird`, async () => {
		const wrapper = mountComponent({
			$route: {
				query: {
					prev_page: 'test',
					tenant_phone: 'the weird phone number'
				}
			}
		});

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.section).toBe(1);
	});

	it(`Should disabled button when "Mahasiswa" was selected`, async () => {
		const wrapper = mountComponent();

		wrapper.setData({
			photos: {
				identity: {
					id: 'test'
				},
				document: {
					id: 'test'
				}
			},
			tenantData: {
				name: 'test',
				phone_number: 'test',
				email: 'test',
				occupation: 'test',
				gender_value: 'test',
				marital_status: 'test',
				parent_name: '',
				parent_phone_number: ''
			}
		});

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.isButtonNextDisabled).toBe(false);

		wrapper.vm.$data.tenantData.occupation = 'Mahasiswa';

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.isButtonNextDisabled).toBe(true);
	});

	it(`Should set tenant and photo data and move to next section`, async () => {
		const wrapper = mountComponent();
		const photos = {
			identity: {
				id: 'test'
			},
			document: {
				id: 'test'
			}
		};

		const tenantData = {
			name: 'test',
			phone_number: 'test',
			email: 'test',
			occupation: 'test',
			gender_value: 'test',
			marital_status: 'test',
			parent_name: '',
			parent_phone_number: ''
		};

		wrapper.setData({
			photos,
			tenantData
		});

		await wrapper.vm.$nextTick();

		wrapper.find('button').trigger('click');

		expect(wrapper.vm.tenant).toEqual(tenantData);
		expect(wrapper.vm.$store.state.createContract.photos).toEqual(photos);
		expect(wrapper.vm.section).toBe(4);
	});
});
