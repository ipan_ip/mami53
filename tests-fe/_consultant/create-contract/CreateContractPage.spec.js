import { shallowMount, createLocalVue } from '@vue/test-utils';
import CreateContractPage from 'Consultant/pages/create-contract/CreateContractPage';
import createContract from 'Consultant/store/modules/createContract';

import Vuex from 'vuex';

const localVue = createLocalVue();
localVue.use(Vuex);

const store = new Vuex.Store({
	modules: {
		createContract
	}
});

const veeValidateError = {
	computed: {
		errors() {
			return {
				first: jest.fn()
			};
		}
	}
};

global.console = { error: jest.fn() };

describe('CreateContractPage.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return shallowMount(CreateContractPage, {
				localVue,
				store,
				mocks: {
					$route: {
						query: {},
						meta: {
							navbarOptions: {}
						}
					}
				},
				stubs: ['keep-alive'],
				...options
			});
		};
	});

	it('Should change section if the route have skip_sec_to query', async () => {
		const activeSection = 2;
		const wrapper = mountComponent({
			mocks: {
				$route: {
					query: {
						skip_sec_to: activeSection
					},
					meta: {
						navbarOptions: {}
					}
				}
			},
			stubs: {
				keepAlive: {
					template: '<div></div>'
				}
			}
		});

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.$store.state.createContract.section).toBe(activeSection);
		expect(wrapper.vm.currentComponent).toBe('SearchPhone');
	});

	it('Should move previous section when click back and has not back action handler', async () => {
		const activeSection = 2;
		const SearchPhone = {
			template: `
        <div>
          <button id="back" @click="back">back</button>
        </div>
      `,
			methods: {
				back() {
					this.$route.meta.navbarOptions.onClickBack();
				}
			},
			...veeValidateError
		};
		const wrapper = mountComponent({
			mocks: {
				$route: {
					query: {
						skip_sec_to: activeSection
					},
					meta: {
						navbarOptions: {}
					}
				}
			},
			stubs: {
				keepAlive: {
					template: '<div><slot></slot></div>'
				},
				SearchPhone,
				SearchProperty: {
					template: '<div></div>'
				}
			}
		});

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.$store.state.createContract.section).toBe(activeSection);

		wrapper.find('#back').trigger('click');

		expect(wrapper.vm.$store.state.createContract.section).toBe(
			activeSection - 1
		);
	});

	it('Should execute back action handler on active child component when click back', async () => {
		const handleClickBackAction = jest.fn();
		const TenantInformations = {
			template: `
        <div>
          <button id="back" @click="back">back</button>
        </div>
      `,
			methods: {
				back() {
					this.$route.meta.navbarOptions.onClickBack();
				},
				handleClickBackAction
			}
		};
		const wrapper = mountComponent({
			mocks: {
				$route: {
					query: {
						skip_sec_to: 3
					},
					meta: {
						navbarOptions: {}
					}
				}
			},
			stubs: {
				keepAlive: {
					template: '<div><slot></slot></div>'
				},
				TenantInformations
			}
		});

		await wrapper.vm.$nextTick();

		wrapper.find('#back').trigger('click');

		expect(handleClickBackAction).toHaveBeenCalled();
	});

	it('Should reset all state before destroying component', async () => {
		const wrapper = mountComponent({
			mocks: {
				$route: {
					query: {
						skip_sec_to: 4
					},
					meta: {
						navbarOptions: {}
					}
				}
			},
			stubs: {
				keepAlive: {
					template: '<div></div>'
				},
				SearchProperty: {
					template: '<div></div>'
				},
				PropertyDetails: {
					template: '<div></div>'
				}
			}
		});

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.$store.state.createContract.section).toBe(4);

		wrapper.destroy();

		expect(wrapper.vm.$store.state.createContract.section).toBe(1);
	});
});
