import { createLocalVue, shallowMount } from '@vue/test-utils';
import PropertyLists from 'Consultant/pages/property/PropertyLists';
import { flushPromises } from 'tests-fe/utils/promises';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

import property from './data/mockProperty.json';

const localVue = createLocalVue();
mockVLazy(localVue);

global.axios = {
	get: jest.fn().mockResolvedValue({
		data: {
			status: true,
			total: 1,
			data: [property]
		}
	})
};

global.tracker = jest.fn();

describe('PropertyLists.vue', () => {
	let mountComponent;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return shallowMount(PropertyLists, {
				localVue,
				mocks: {
					$route: {
						query: {}
					},
					$dayjs: jest.fn(d => ({ format: () => d })),
					$store: {
						state: {
							authData: { id: 'test' }
						}
					}
				},
				...options
			});
		};
	});

	it('should render component correctly and match snapshot', async () => {
		const tree = mountComponent();

		await flushPromises();

		expect(tree.element).toMatchSnapshot();
	});

	it('should add property status detail according to its status', async () => {
		const tree = mountComponent();

		await flushPromises();

		expect(tree.vm.propertyLists).toEqual([
			expect.objectContaining({
				statusDetail: expect.objectContaining({ label: 'Verifikasi Berhasil' })
			})
		]);
	});
});
