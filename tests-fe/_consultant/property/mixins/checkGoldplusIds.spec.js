import { mount } from '@vue/test-utils';
import checkGoldplusIds from 'Consultant/pages/property/mixins/checkGoldplusIds';

describe('checkGoldplusIds mixin', () => {
  let mountComponent;

	beforeEach(() => {
		mountComponent = () => {
			const Component = {
				mixins: [checkGoldplusIds],
				render: h => h('div')
			};

			return mount(Component);
		};
  });
  
  it('should change string to number type', () => {
    const wrapper = mountComponent();
    const ids = ['1', '2', '3'];
    const expected = [1, 2, 3];

    expect(wrapper.vm.checkGoldplusIds(ids)).toEqual(expected);
  });

  it('should filter number only', () => {
    const wrapper = mountComponent();
    const ids = ['1', 'string', 'test123'];
    const expected = [1];

    expect(wrapper.vm.checkGoldplusIds(ids)).toEqual(expected);
  });

})