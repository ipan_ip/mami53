export const propertyDetailSucceed = {
	status: true,
	meta: {
		response_code: 200,
		code: 200,
		severity: 'OK',
		message:
			'The request has succeeded. An entity corresponding to the requested resource is sent in the response.'
	},
	data: [
		{
			id: 88737652,
			name: 'Kost Estingte Di Jambu',
			link:
				'http://localhost:8000/room/kost-gunung-kidul-kost-campur-eksklusif-kost-estingte-di-jambu-1',
			is_premium: 'false',
			is_booking: 1,
			total_room: 10,
			status: 'verified',
			used_room: 6,
			available_room: 4,
			owner_name: 'Fariz Kiiaa',
			owner_phone: '087822178994',
			manager_name: '',
			manager_phone: '',
			area: 'Gunung Kidul',
			address:
				'Unnamed Road, Gamsungi, Tobelo, Kabupaten Halmahera Utara, Maluku Utara, Indonesia',
			price_monthly: 100000,
			price_weekly: 100000,
			price_yearly: 100000,
			updated_at: '2020-02-19 14:56:46',
			price_daily: 0,
			price_3_month: 100000,
			price_6_month: 100000
		}
	]
};

export const propertyDetailFailed = {
	status: false,
	meta: {
		response_code: 200,
		code: 200,
		severity: 'OK',
		message:
			'The request has succeeded. An entity corresponding to the requested resource is sent in the response.'
	},
	data: []
};
