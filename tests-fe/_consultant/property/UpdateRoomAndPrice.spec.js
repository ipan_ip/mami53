import '@babel/polyfill';
import { createLocalVue, shallowMount } from '@vue/test-utils';
import UpdateRoomAndPrice from 'Consultant/pages/property/UpdateRoomAndPrice.vue';
import {
	propertyDetailSucceed,
	propertyDetailFailed
} from './__mocks__/propertyDetail.js';

jest.mock('Js/@global/event-bus.js', () => {});

const localVue = createLocalVue();
const axios = {
	getSuccess: jest.fn(() => {
		return Promise.resolve({
			data: propertyDetailSucceed
		});
	}),
	getFailed: jest.fn(() => {
		return Promise.resolve({
			data: propertyDetailFailed
		});
	}),
	// eslint-disable-next-line prefer-promise-reject-errors
	getError: () => Promise.reject('error')
};

global.bugsnagClient = {
	notify: jest.fn()
};

describe('UpdateRoomAndPrice.vue', () => {
	let wrapper;

	beforeEach(() => {
		wrapper = shallowMount(UpdateRoomAndPrice, {
			localVue,
			mocks: {
				$route: { params: { id: '123' } },
				$router: { go: jest.fn() }
			},
			stubs: {
				InputField: true,
				ConsultantButton: true,
				Snackbar: true,
				InputCurrency: true
			}
		});

		global.axios = { get: axios.getSuccess };
	});

	describe('test request API', () => {
		it('should got success and filled data when request API has been succeed', async () => {
			const response = await wrapper.vm.getProperty('123');
			const expectedData = propertyDetailSucceed.data[0];
			expect(response.success).toBeTruthy();
			expect(response.data).toEqual(expect.objectContaining(expectedData));
		});

		it('should got failed and empty object when request API has been failed', async () => {
			global.axios.get = axios.getError;
			const response = await wrapper.vm.getProperty('123');
			const expectedData = {};
			expect(response.success).toBeFalsy();
			expect(response.data).toEqual(expect.objectContaining(expectedData));
		});

		it('should go to previous page if no user found or failed to get data', () => {
			global.axios.get = axios.getFailed;
			wrapper.vm.initDataPropertyForUpdate();
			expect(wrapper.vm.$router.go).toBeCalledWith(-1);
		});
	});

	describe('test method changeZeroValueToEmptyString()', () => {
		let sourceData = {};
		beforeAll(() => {
			sourceData = { price_daily: 0, price_weekly: 1000 };
		});

		it('should changed 0 value to empty string', () => {
			const expected = { price_daily: '', price_weekly: 1000 };
			expect(wrapper.vm.changeZeroValueToEmptyString(sourceData)).toEqual(
				expect.objectContaining(expected)
			);
		});

		it('should keeping 0 when the field has been ignored', () => {
			const IGNORED_FIELDS = ['price_daily'];
			const expected = { price_daily: 0, price_weekly: 1000 };
			expect(
				wrapper.vm.changeZeroValueToEmptyString(sourceData, IGNORED_FIELDS)
			).toEqual(expect.objectContaining(expected));
		});
	});

	describe('test method sortAndGetUpdatingData()', () => {
		it('should return data as expected', () => {
			const fieldExpected = [
				'name',
				'price_daily',
				'price_weekly',
				'price_monthly',
				'price_yearly',
				'price_3_month',
				'price_6_month'
			];

			const sortedData = wrapper.vm.sortAndGetUpdatingData(
				propertyDetailSucceed.data[0]
			);

			expect(Object.keys(sortedData).length).toBe(fieldExpected.length);

			expect(Object.keys(sortedData)).toEqual(
				expect.arrayContaining(fieldExpected)
			);
		});
	});
});
