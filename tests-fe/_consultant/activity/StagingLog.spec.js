import { mount } from '@vue/test-utils';
import StagingLog from 'Consultant/pages/activity/StagingLog';
import { flushPromises } from 'tests-fe/utils/promises';
import stagingLogResponse from './data/stagingLogResponse.json';
import * as storage from 'Js/@utils/storage';

import EventBus from 'Consultant/event-bus';

global.axios = {
	get: jest.fn().mockResolvedValue({
		data: stagingLogResponse
	})
};

jest.mock('Consultant/helpers/dateFormatter.js', () =>
	jest.fn(() => 'formatted')
);

describe('StagingLog.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return mount(StagingLog, {
				mocks: {
					$route: {
						params: {
							taskId: 1
						}
					}
				},
				...options
			});
		};
	});

	it('should render component correctly', async () => {
		const wrapper = mountComponent();

		await flushPromises();
		await wrapper.vm.$nextTick();
		expect(wrapper.classes('activity-staging-log')).toBe(true);
		expect(wrapper.html()).toMatchSnapshot();
	});

	it('should render log correctly', async () => {
		const logData = [
			{
				status_name: 'test',
				date: 'test'
			}
		];
		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: {
					status: true,
					data: logData
				}
			})
		};
		const wrapper = mountComponent();

		await flushPromises();

		await wrapper.vm.$nextTick();
		const logs = wrapper.findAll('.timeline-item');
		expect(logs.length).toBe(logData.length);
	});

	it('should show alert with message from API', async () => {
		const openAlert = jest.fn();
		const message = 'test message';
		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: {
					status: false,
					message
				}
			})
		};
		mountComponent();
		EventBus.$on('openAlert', openAlert);

		await flushPromises();

		expect(openAlert).toHaveBeenCalledWith(message);
	});

	it('should open alert with message "Terjadi galat, silahkan coba lagi"', async () => {
		const openAlert = jest.fn();
		const message = 'Terjadi galat, silahkan coba lagi';
		global.axios = {
			get: jest.fn().mockRejectedValue(new Error())
		};
		mountComponent();
		EventBus.$on('openAlert', openAlert);

		await flushPromises();

		expect(openAlert).toHaveBeenCalledWith(message);
	});

	it('should set stage state when has local storage', async () => {
		const mockStageData = {
			staging_name: 'test',
			name: 'test'
		};

		storage.local.setItem('staging_log', JSON.stringify(mockStageData));

		const wrapper = mountComponent();

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.stage).toEqual(expect.objectContaining(mockStageData));
	});

	it('should not set stage state when has not local storage', async () => {
		storage.local.setItem('staging_log', '');
		const wrapper = mountComponent();

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.stage).toEqual({});
	});

	it('should update response of get stage log', async () => {
		const logData = [
			{
				status_name: 'test',
				date: 'test'
			}
		];
		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: {
					status: true,
					data: logData
				}
			})
		};
		const wrapper = mountComponent({
			mocks: {
				$route: {
					params: {
						taskId: 1
					}
				}
			}
		});

		await flushPromises();

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.logs).toEqual(
			expect.arrayContaining([
				expect.objectContaining({
					label: 'test',
					date: 'formatted'
				})
			])
		);
	});
});
