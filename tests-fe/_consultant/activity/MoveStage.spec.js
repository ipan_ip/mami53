import { shallowMount } from '@vue/test-utils';
import MoveStage from 'Consultant/pages/activity/components/MoveStage';

import { flushPromises } from 'tests-fe/utils/promises';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

import stageListResponse from './data/stageListResponse.json';

jest.mock('vue-fragment', () => ({
	Fragment: {
		template: `<div><slot></slot></div>`
	}
}));

describe('MoveStage.vue', () => {
	let mountComponent = Function;
	const axiosGet = jest.fn().mockResolvedValue({ data: stageListResponse });
	global.axios = {
		get: axiosGet
	};
	beforeEach(() => {
		delete window.location;
		window.location = { reload: jest.fn() };
		mountComponent = (options = {}) => {
			return shallowMount(MoveStage, {
				propsData: {
					funnelId: 1,
					taskId: 2,
					progressId: 3
				},
				mocks: {
					$router: {
						push: jest.fn()
					},
					$route: {
						query: {}
					},
					$store: {
						getters: {
							isAdminConsultant: true
						}
					}
				},
				...options
			});
		};
	});

	it('should render component correctly', () => {
		const wrapper = mountComponent();

		expect(wrapper.html()).toMatchSnapshot();
	});

	it('should fetch API and updating data when tasks is empty', async () => {
		const wrapper = mountComponent();

		wrapper.vm.open();

		await flushPromises();

		expect(axiosGet).toHaveBeenCalled();
	});

	it('should set active stage a greater than current stage', async () => {
		const tasks = [
			{
				name: 'test',
				is_current_stage: true
			},
			{
				name: 'test 2',
				is_current_stage: false
			}
		];

		global.axios = {
			get: jest
				.fn()
				.mockResolvedValue({ data: { status: true, stages: tasks } })
		};

		const wrapper = mountComponent();

		wrapper.vm.open();

		await flushPromises();

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.activeTask).toBe(1);
	});

	it('should show confirm when active task greater than currentStage + 1', async () => {
		const RadioGroup = {
			props: ['value', 'options'],
			template: `
			<div class="radio-group">
				<input type="radio" v-for="opt in options" class="radio" @change="$emit('input', opt.value)" />
			</div>
			`
		};

		const tasks = [
			{
				name: 'test',
				is_current_stage: false
			},
			{
				name: 'test 2',
				is_current_stage: true
			},
			{
				name: 'test 3',
				is_current_stage: false
			},
			{
				name: 'test 3',
				is_current_stage: false
			}
		];

		global.axios = {
			get: jest
				.fn()
				.mockResolvedValue({ data: { status: true, stages: tasks } })
		};

		const wrapper = mountComponent({
			propsData: {
				funnelId: 1,
				taskId: 2,
				progressId: 3
			},
			stubs: {
				'ct-radio-group': RadioGroup,
				'ct-button': {
					template: `<button @click="$emit('click')"></button>`
				}
			}
		});

		wrapper.vm.open();

		await flushPromises();

		await wrapper.vm.$nextTick();

		const radios = wrapper.findAll('.radio-group input');

		radios.at(3).trigger('change');

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.activeTask).toBe(3);

		wrapper.find('button').trigger('click');

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.isConfirmOpen).toBe(true);
	});

	it('should show confirm when active task less than current stage', async () => {
		const tasks = [
			{
				name: 'test',
				is_current_stage: false
			},
			{
				name: 'test 2',
				is_current_stage: false
			},
			{
				name: 'test 3',
				is_current_stage: true
			},
			{
				name: 'test 3',
				is_current_stage: false
			}
		];

		global.axios = {
			get: jest
				.fn()
				.mockResolvedValue({ data: { status: true, stages: tasks } })
		};

		const RadioGroup = {
			props: ['value', 'options'],
			template: `
			<div class="radio-group">
				<input type="radio" v-for="opt in options" class="radio" @change="$emit('input', opt.value)" />
			</div>
			`
		};
		const wrapper = mountComponent({
			propsData: {
				funnelId: 1,
				taskId: 2,
				progressId: 3
			},
			stubs: {
				'ct-radio-group': RadioGroup,
				'ct-button': {
					template: `<button @click="$emit('click')"></button>`
				}
			}
		});

		wrapper.vm.open();

		await flushPromises();

		await wrapper.vm.$nextTick();

		const radios = wrapper.findAll('.radio-group input');

		radios.at(1).trigger('change');

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.activeTask).toBe(1);

		wrapper.find('button').trigger('click');

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.isConfirmOpen).toBe(true);
	});

	it('should set next stage be actived when drawer was opened', async () => {
		const tasks = [
			{
				name: 'test',
				is_current_stage: false
			},
			{
				name: 'test 2',
				is_current_stage: true
			},
			{
				name: 'test 3',
				is_current_stage: false
			},
			{
				name: 'test 3',
				is_current_stage: false
			}
		];

		global.axios = {
			get: jest
				.fn()
				.mockResolvedValue({ data: { status: true, stages: tasks } })
		};

		const wrapper = mountComponent();

		wrapper.vm.open();

		await flushPromises();

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.activeTask).toBe(2);

		wrapper.setData({ activeTask: 3, isOpen: false });

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.activeTask).toBe(3);

		wrapper.vm.open();

		await flushPromises();

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.activeTask).toBe(2);
	});

	it('should move to staging form with stage id selected', async () => {
		const idSelected = 'id selected';
		const tasks = [
			{
				name: 'test',
				is_current_stage: false
			},
			{
				name: 'test 2',
				is_current_stage: false
			},
			{
				name: 'test 3',
				is_current_stage: true
			},
			{
				id: idSelected,
				name: 'test 3',
				is_current_stage: false
			}
		];

		global.axios = {
			get: jest
				.fn()
				.mockResolvedValue({ data: { status: true, stages: tasks } })
		};

		const wrapper = mountComponent({
			propsData: {
				funnelId: 1,
				taskId: 2,
				progressId: 3
			},
			stubs: {
				'ct-button': {
					template: `<button @click="$emit('click')"></button>`
				}
			}
		});

		wrapper.vm.open();

		await flushPromises();

		await wrapper.vm.$nextTick();

		wrapper.find('button').trigger('click');

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.$router.push).toHaveBeenCalledWith({
			name: 'activity.staging-form',
			params: expect.objectContaining({ stage_id: idSelected })
		});
	});

	it('should move to staging form after show confirmation drawer and click "pindahkan"', async () => {
		const RadioGroup = {
			props: ['value', 'options'],
			template: `
			<div class="radio-group">
				<input type="radio" v-for="opt in options" class="radio" @change="$emit('input', opt.value)" />
			</div>
			`
		};

		const idSelected = 'id selected';
		const tasks = [
			{
				name: 'test',
				is_current_stage: false
			},
			{
				name: 'test 2',
				is_current_stage: true
			},
			{
				name: 'test 3',
				is_current_stage: false
			},
			{
				id: idSelected,
				name: 'test 3',
				is_current_stage: false
			}
		];

		global.axios = {
			get: jest
				.fn()
				.mockResolvedValue({ data: { status: true, stages: tasks } })
		};

		const wrapper = mountComponent({
			propsData: {
				funnelId: 1,
				taskId: 2,
				progressId: 3
			},
			stubs: {
				'ct-radio-group': RadioGroup,
				'ct-button': {
					template: `<button @click="$emit('click')"></button>`
				}
			}
		});

		wrapper.vm.open();

		await flushPromises();

		await wrapper.vm.$nextTick();

		const radios = wrapper.findAll('.radio-group input');

		radios.at(3).trigger('change');

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.activeTask).toBe(3);

		wrapper.find('button').trigger('click');

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.isConfirmOpen).toBe(true);

		wrapper
			.findAll('button')
			.at(1)
			.trigger('click');

		expect(wrapper.vm.$router.push).toHaveBeenCalledWith({
			name: 'activity.staging-form',
			params: expect.objectContaining({ stage_id: idSelected })
		});
	});

	it(`should call handleSelectBacklogAndTodo when id's selected task is null and don't need confirmation before`, async () => {
		const tasks = [
			{
				id: null,
				name: 'test',
				is_current_stage: true
			},
			{
				id: null,
				name: 'test 2',
				is_current_stage: false
			}
		];
		const activate = jest.fn().mockResolvedValue({ data: { status: true } });
		const button = {
			template: `<button @click="$emit('click')"></button>`
		};
		mockWindowProperty('window.location', {
			replace: jest.fn()
		});

		global.axios = {
			get: jest
				.fn()
				.mockResolvedValue({ data: { status: true, stages: tasks } }),
			post: activate
		};

		const wrapper = mountComponent({
			propsData: {
				funnelId: 1,
				taskId: 2,
				progressId: 3
			},
			stubs: {
				'ct-button': button
			}
		});

		const handleSelectBacklogAndTodo = jest.spyOn(
			wrapper.vm,
			'handleSelectBacklogAndTodo'
		);

		wrapper.vm.open();

		await flushPromises();

		wrapper.find('button').trigger('click');

		expect(handleSelectBacklogAndTodo).toHaveBeenCalled();
	});

	it('should move to backlog', async () => {
		const tasks = [
			{
				id: null,
				name: 'test',
				is_current_stage: false
			},
			{
				id: 1,
				name: 'test 2',
				is_current_stage: false
			},
			{
				id: 2,
				name: 'test 3',
				is_current_stage: false
			},
			{
				id: 3,
				name: 'test 4',
				is_current_stage: true
			}
		];
		const activate = jest.fn().mockResolvedValue({ data: { status: true } });
		const button = {
			template: `<button @click="$emit('click')"></button>`
		};
		mockWindowProperty('window.location', {
			replace: jest.fn()
		});

		global.axios = {
			get: jest
				.fn()
				.mockResolvedValue({ data: { status: true, stages: tasks } }),
			post: activate
		};

		const wrapper = mountComponent({
			propsData: {
				funnelId: 1,
				taskId: 2,
				progressId: 3
			},
			stubs: {
				'ct-button': button
			}
		});

		wrapper.vm.open();

		await flushPromises();

		wrapper.setData({ activeTask: 0 });

		await wrapper.vm.$nextTick();

		wrapper
			.findAll('button')
			.at(1)
			.trigger('click');

		expect(activate).toHaveBeenCalledWith(
			'/api/activity-management/task/backlog',
			expect.objectContaining({ task_id: 2 })
		);
	});

	it('should replace url with progress 0 and task id 0 after move to backlog', async () => {
		const tasks = [
			{
				id: null,
				name: 'test',
				is_current_stage: false
			},
			{
				id: 1,
				name: 'test 2',
				is_current_stage: false
			},
			{
				id: 2,
				name: 'test 3',
				is_current_stage: false
			},
			{
				id: 3,
				name: 'test 4',
				is_current_stage: true
			}
		];
		const button = {
			template: `<button @click="$emit('click')"></button>`
		};
		const replace = jest.fn();
		mockWindowProperty('window.location', {
			replace,
			pathname: 'test'
		});

		global.axios = {
			get: jest
				.fn()
				.mockResolvedValue({ data: { status: true, stages: tasks } }),
			post: jest.fn().mockResolvedValue({ data: { status: true } })
		};

		const wrapper = mountComponent({
			propsData: {
				funnelId: 1,
				taskId: 2,
				progressId: 3
			},
			stubs: {
				'ct-button': button
			}
		});

		wrapper.vm.open();

		await flushPromises();

		wrapper.setData({ activeTask: 0 });

		await wrapper.vm.$nextTick();

		wrapper
			.findAll('button')
			.at(1)
			.trigger('click');

		await flushPromises();
		expect(replace).toHaveBeenCalledWith(
			expect.stringContaining('progress=0&task_id=0')
		);
	});

	it('should move to todo ("tugas Aktif")', async () => {
		const tasks = [
			{
				id: null,
				name: 'test',
				is_current_stage: false
			},
			{
				id: null,
				name: 'Tugas Aktif',
				is_current_stage: false
			},
			{
				id: 2,
				name: 'test 3',
				is_current_stage: false
			},
			{
				id: 3,
				name: 'test 4',
				is_current_stage: true
			}
		];
		const activate = jest.fn().mockResolvedValue({ data: { status: true } });
		const button = {
			template: `<button @click="$emit('click')"></button>`
		};
		mockWindowProperty('window.location', {
			reload: jest.fn()
		});

		global.axios = {
			get: jest
				.fn()
				.mockResolvedValue({ data: { status: true, stages: tasks } }),
			post: activate
		};

		const wrapper = mountComponent({
			propsData: {
				funnelId: 1,
				taskId: 2,
				progressId: 3
			},
			stubs: {
				'ct-button': button
			}
		});

		wrapper.vm.open();

		await flushPromises();

		wrapper.setData({ activeTask: 1 });

		await wrapper.vm.$nextTick();

		wrapper
			.findAll('button')
			.at(1)
			.trigger('click');

		expect(activate).toHaveBeenCalledWith(
			'/api/activity-management/task/todo',
			expect.objectContaining({ task_id: 2 })
		);
	});

	it('should reload url when move to todo was succeed', async () => {
		const tasks = [
			{
				id: null,
				name: 'test',
				is_current_stage: false
			},
			{
				id: null,
				name: 'tugas aktif',
				is_current_stage: false
			},
			{
				id: 2,
				name: 'test 3',
				is_current_stage: false
			},
			{
				id: 3,
				name: 'test 4',
				is_current_stage: true
			}
		];
		const button = {
			template: `<button @click="$emit('click')"></button>`
		};
		const reload = jest.fn();
		mockWindowProperty('window.location', {
			reload
		});

		global.axios = {
			get: jest
				.fn()
				.mockResolvedValue({ data: { status: true, stages: tasks } }),
			post: jest.fn().mockResolvedValue({ data: { status: true } })
		};

		const wrapper = mountComponent({
			propsData: {
				funnelId: 1,
				taskId: 2,
				progressId: 3
			},
			stubs: {
				'ct-button': button
			}
		});

		wrapper.vm.open();

		await flushPromises();

		wrapper.setData({ activeTask: 1 });

		await wrapper.vm.$nextTick();

		wrapper
			.findAll('button')
			.at(1)
			.trigger('click');

		await flushPromises();
		expect(reload).toHaveBeenCalled();
	});
});
