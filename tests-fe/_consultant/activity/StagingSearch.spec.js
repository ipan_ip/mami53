import { createLocalVue, shallowMount } from '@vue/test-utils';
import { flushPromises } from 'tests-fe/utils/promises';
import EventBus from 'Consultant/event-bus';

import MockData from './data/staging-search.json';
import ActivityStagingSearch from 'Consultant/pages/activity/StagingSearch';

const axios = {
	get: jest.fn().mockResolvedValue({
		data: MockData
	})
};

const bugsnagClient = {
	notify: jest.fn()
};

const mocks = {
	$router: { push: jest.fn() },
	$route: {
		params: {
			funnel_id: 1,
			stage: 9,
			stage_name: 'Telah Dihubungi',
			funnel_name: 'Data Penyewa Potential',
			data_type: 'Data Penyewa'
		},
		meta: {
			navbarOptions: {
				onClickBack: jest.fn()
			}
		},
		query: {}
	}
};

describe('Consultant/pages/activity/StagingSearch', () => {
	test('should render correctly', async () => {
		const localVue = createLocalVue();

		const tree = shallowMount(ActivityStagingSearch, {
			localVue,
			mocks
		});

		await flushPromises();

		expect(tree.element).toMatchSnapshot();
	});

	// ----------------------------------------------------------------

	let mountComponent = Function;

	beforeEach(() => {
		const localVue = createLocalVue();

		console.error = jest.fn();
		global.alert = jest.fn();

		global.axios = axios;
		global.bugsnagClient = bugsnagClient;

		mountComponent = (options = {}) => {
			return shallowMount(ActivityStagingSearch, {
				localVue,
				mocks,
				...options
			});
		};
	});

	test('get the index value of checked id', async () => {
		const wrapper = mountComponent();
		await flushPromises();

		wrapper.setData({ checked: [1, 2, 3, 5, 7] });

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.checkExistence(3)).toBe(2);
	});

	test('add checked item into checked state', async () => {
		const wrapper = mountComponent();
		await flushPromises();

		wrapper.setData({ checked: [1, 2, 3, 5, 7] });

		await wrapper.vm.$nextTick();

		wrapper.vm.handleCheck(11, true);

		expect(wrapper.vm.checked).toEqual([1, 2, 3, 5, 7, 11]);
	});

	test('remove unchecked checked item from checked state', async () => {
		const wrapper = mountComponent();
		await flushPromises();

		wrapper.setData({ checked: [1, 2, 3, 5, 7] });

		await wrapper.vm.$nextTick();

		wrapper.vm.handleCheck(2, false);

		expect(wrapper.vm.checked).toEqual([1, 3, 5, 7]);
	});

	test('go to staging page', async () => {
		const wrapper = mountComponent();
		await flushPromises();

		wrapper.vm.goToStaging();

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.$router.push).toBeCalledWith({
			name: 'activity.staging',
			params: {
				funnel_id: wrapper.vm.funnelId,
				funnel_name: wrapper.vm.funnelName,
				funnel_type: wrapper.vm.dataType
			}
		});
	});

	test('go to detail of a staging page', async () => {
		const wrapper = mountComponent();
		await flushPromises();

		wrapper.vm.handleClickItem({
			progressable_id: 1,
			task_id: null,
			progress: 0
		});

		expect(wrapper.vm.$router.push).toBeCalledWith({
			name: 'activity.detail',
			query: {
				funnel_id: 1,
				progress_id: 1,
				task_id: 0,
				progress: 0,
				funnel_name: 'Data Penyewa Potential'
			}
		});
	});

	test('display alert if get bad API response', async () => {
		const alert = jest.spyOn(window, 'alert').mockImplementation(() => {});

		global.axios = {
			get: jest.fn().mockRejectedValue(new Error('test error'))
		};

		mountComponent();

		await flushPromises();

		expect(alert).toBeCalled();
		alert.mockRestore();
	});

	test('set funnel name as title on the header', async () => {
		const wrapper = mountComponent();
		const next = jest.fn();
		const to = {
			params: {
				funnel_name: 'Funnel Name',
				stage_name: 'Stage Name'
			},
			meta: { navbarOptions: { title: '' } }
		};

		ActivityStagingSearch.beforeRouteEnter.call(
			wrapper.vm,
			to,
			undefined,
			next
		);

		expect(to.meta.navbarOptions.title).toBe('Funnel Name Funnel: Stage Name');
		expect(next).toHaveBeenCalled();
	});

	test('go to search page', async () => {
		const changeRoute = jest.fn();
		const wrapper = mountComponent({
			methods: {
				handleChangeRoute: changeRoute
			}
		});
		await flushPromises();

		wrapper.setData({ search: 'test search' });
		wrapper.vm.handleSearch();

		expect(wrapper.vm.pagination.search).toBe('test search');
		expect(changeRoute).toHaveBeenCalledWith({
			page: 1,
			search: 'test search'
		});
	});

	test('submit checked contracts to activate them', async () => {
		global.axios = {
			post: jest.fn().mockResolvedValue({
				data: {
					status: true,
					task_ids: [1, 2, 3]
				}
			})
		};

		const wrapper = mountComponent();

		const openAlert = jest.fn();
		EventBus.$on('openAlert', openAlert);

		wrapper.setData({ checked: [1, 2, 3] });

		wrapper.vm.handleSubmitChecked();

		await flushPromises();

		expect(openAlert).toHaveBeenCalledWith(
			expect.any(String),
			'dark',
			expect.objectContaining({ placement: 'bottom' })
		);
	});

	test('display alert if get bad API response on posting checked contracts', async () => {
		const alert = jest.spyOn(window, 'alert').mockImplementation(() => {});

		global.axios = {
			post: jest.fn().mockRejectedValue(new Error('test error'))
		};

		const wrapper = mountComponent();

		wrapper.vm.handleSubmitChecked();

		await flushPromises();

		expect(alert).toBeCalled();
		alert.mockRestore();
	});

	test('show input checkbox after user click pilih button', async () => {
		const wrapper = mountComponent();

		await flushPromises();

		wrapper.vm.toggleCheckbox();

		await wrapper.vm.$nextTick();

		expect(wrapper.find('item-stub').props('itemType')).toBe('checkbox');
		expect(wrapper.find('bottom-container-stub')).toBeTruthy();
	});
});
