import { shallowMount } from '@vue/test-utils';
import TaskTypeInformation from 'Consultant/pages/activity/components/TaskTypeInformation';

const mockDetailProp = {
	name: 'Keeley Dicki',
	gender: 'male',
	is_booking: 1,
	progressable_id: 2232323,
	type: 'contract',
	contract_status: 'rejected',
	phone_number: '(509) 773-0380 x47075',
	property_name: 'Kos Exclusive Rosalinda35',
	property_owner_name: 'Abigail Satterfield',
	property_owner_phone_number: '0886248773'
};

describe('TaskTypeInformation.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return shallowMount(TaskTypeInformation, {
				stubs: {
					Fragment: {
						template: '<div><slot></slot></div>'
					}
				},
				...options
			});
		};
	});

	it('should render component with type is contract', async () => {
		const wrapper = mountComponent({
			propsData: {
				detail: mockDetailProp
			}
		});

		await wrapper.vm.$nextTick();

		expect(wrapper.html()).toMatchSnapshot();
	});

	it('should render component with type is property', async () => {
		const wrapper = mountComponent({
			propsData: {
				detail: { ...mockDetailProp, type: 'property' }
			}
		});

		await wrapper.vm.$nextTick();

		expect(wrapper.html()).toMatchSnapshot();
	});

	it('should render component with type is potential tenant', async () => {
		const wrapper = mountComponent({
			propsData: {
				detail: { ...mockDetailProp, type: 'potential_tenant' }
			}
		});

		await wrapper.vm.$nextTick();

		expect(wrapper.html()).toMatchSnapshot();
	});

	it('should add label "Bisa Booking"', async () => {
		const wrapper = mountComponent({
			propsData: {
				detail: mockDetailProp
			}
		});

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.detailTask).toEqual(
			expect.objectContaining({
				chipLabel: 'Bisa Booking'
			})
		);
	});

	it('should add label "Free Listing"', async () => {
		const wrapper = mountComponent({
			propsData: {
				detail: { ...mockDetailProp, is_booking: 0 }
			}
		});

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.detailTask).toEqual(
			expect.objectContaining({
				chipLabel: 'Free Listing'
			})
		);
	});

	it('should add link with type is "contract"', async () => {
		const wrapper = mountComponent({
			propsData: {
				detail: mockDetailProp
			}
		});

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.detailTask).toEqual(
			expect.objectContaining({
				link: expect.stringMatching(/\/consultant-tools\/invoice\//)
			})
		);
	});

	it('should add link with type is "property"', async () => {
		const wrapper = mountComponent({
			propsData: {
				detail: { ...mockDetailProp, type: 'property' }
			}
		});

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.detailTask).toEqual(
			expect.objectContaining({
				link: expect.stringMatching(/\/consultant-tools\/property\//)
			})
		);
	});
});
