import { mount } from '@vue/test-utils';
import updateActivityData from 'Consultant/pages/activity/mixins/updateActivityData';

describe('updateActivityData.js', () => {
	let wrapper;

	beforeEach(() => {
		wrapper = mount({
			mixins: [updateActivityData],
			render: h => h('div')
		});
	});

	it('Should have activity type and match snapshot', async () => {
		expect(wrapper.vm.activityType).toMatchSnapshot();
	});

	it('should add icon and label DBET', () => {
		const activities = [
			{
				data_type: 'DBET',
				current_stage: 0,
				stage_name: 'stage name'
			}
		];

		const updatedActivities = wrapper.vm.mixUpdateDataActivity(activities);
		expect(updatedActivities).toMatchSnapshot();
	});

	it('should add icon and label Property when its type is "Properti Potensial"', () => {
		const activities = [
			{
				data_type: 'Properti Potensial',
				current_stage: 0,
				stage_name: 'stage name'
			}
		];

		const updatedActivities = wrapper.vm.mixUpdateDataActivity(activities);
		expect(updatedActivities).toEqual(
			expect.arrayContaining([
				expect.objectContaining({
					icon: 'icon-home-circle',
					label: expect.stringContaining('Properti')
				})
			])
		);
	});

	it('should add icon and label owner when its type is "Owner Potensial"', () => {
		const activities = [
			{
				data_type: 'Owner Potensial',
				current_stage: 0,
				stage_name: 'stage name'
			}
		];

		const updatedActivities = wrapper.vm.mixUpdateDataActivity(activities);
		expect(updatedActivities).toEqual(
			expect.arrayContaining([
				expect.objectContaining({
					icon: 'icon-user-circle-outline',
					label: expect.stringContaining('Owner Potensial')
				})
			])
		);
	});

	it('should add icon and label with empty string when unkwown type', () => {
		const activities = [
			{
				data_type: 'Other Type',
				current_stage: 0,
				stage_name: 'stage name'
			}
		];

		const updatedActivities = wrapper.vm.mixUpdateDataActivity(activities);
		expect(updatedActivities).toMatchSnapshot();
	});
});
