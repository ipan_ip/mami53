import { shallowMount } from '@vue/test-utils';
import FilterActivityList from 'Consultant/pages/activity/components/FilterActivityList.vue';
import Snackbar from 'Consultant/components/Snackbar';
import dataTypeResponse from './data/dataTypeResponse.json';
import { flushPromises } from 'tests-fe/utils/promises';

describe('FilterActivityList.vue', () => {
	let mountComponent = Function;
	const tempConsole = { ...console };
	const Button = {
		template: `<button class="action-btn" @click="$emit('click')"></button>`
	};

	beforeEach(() => {
		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: dataTypeResponse
			})
		};

		global.console = tempConsole;

		mountComponent = (
			options = {},
			additionalMocks = {},
			additionalStubs = {}
		) => {
			return shallowMount(FilterActivityList, {
				mocks: {
					$route: { query: {} },
					$router: { push: jest.fn() },
					...additionalMocks
				},
				stubs: {
					Snackbar,
					...additionalStubs
				},
				...options
			});
		};
	});

	it('should render component and match snapshot', () => {
		const wrapper = mountComponent();

		expect(wrapper.html()).toMatchSnapshot();
	});

	it('should open snackbar', async () => {
		const wrapper = mountComponent();

		wrapper.vm.open();

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.isOpen).toBe(true);
	});

	it('should fetch filter when open', async () => {
		const wrapper = mountComponent();

		wrapper.vm.open();

		await flushPromises();

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.filterItems.length > 0).toBe(true);
	});

	it('should set activityFilter keys and set filter active', async () => {
		const setFilterActive = jest.fn();
		const wrapper = mountComponent(
			{
				methods: {
					setFilterActive
				}
			},
			{
				$route: {
					query: {
						filters: 'test'
					}
				}
			}
		);

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.currentActiveFilterKeys).toEqual(
			expect.arrayContaining(['test'])
		);

		expect(setFilterActive).toHaveBeenCalled();
	});

	it('should set active when checkbox was clicked', async () => {
		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: {
					status: true,
					data: [
						{
							related_table: 'designer',
							data_type: 'Data Desiner'
						}
					]
				}
			})
		};

		const wrapper = mountComponent();
		wrapper.vm.open();

		await flushPromises();
		const key = 'designer';
		const potentialFilter = wrapper.vm.filterItems.find(
			filter => filter.key === key
		);

		await wrapper.vm.$nextTick();

		expect(potentialFilter.active).toBe(false);

		wrapper.find(`input#${key}`).trigger('click');

		await wrapper.vm.$nextTick();

		expect(potentialFilter.active).toBe(true);
	});

	it('should move to route with filter actived', async () => {
		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: {
					status: true,
					data: [
						{
							related_table: 'designer',
							data_type: 'Data Desiner'
						}
					]
				}
			})
		};

		const push = jest.fn();
		const expectedQuery = {
			filters: 'designer',
			page: 1
		};
		const wrapper = mountComponent(
			{},
			{
				$router: {
					push
				}
			},
			{
				ConsultantButton: Button
			}
		);
		wrapper.setData({ isOpen: true });

		await flushPromises();
		await wrapper.vm.$nextTick();

		wrapper.find('input#designer').trigger('click');

		await wrapper.vm.$nextTick();

		wrapper
			.findAll('.action-btn')
			.at(1)
			.trigger('click');

		expect(push).toHaveBeenCalledWith({
			name: 'activity',
			query: expectedQuery
		});
	});

	it('should reset active filter when click button cancel', async () => {
		const wrapper = mountComponent(
			{},
			{},
			{
				ConsultantButton: Button
			}
		);

		wrapper.setData({
			filterItems: [
				{
					label: 'Data Penyewa Potensial',
					key: 'potential_tenant',
					active: true,
					disabled: false
				},
				{
					label: 'Data Kontrak Penyewa Potential',
					key: 'mamipay_contract',
					active: false,
					disabled: false
				}
			],
			currentActiveFilterKeys: ['potential_tenant']
		});

		const findFilter = (wp, key) => {
			return wp.vm.filterItems.find(filter => filter.key === key);
		};
		const potentialKey = 'potential_tenant';
		const contractKey = 'mamipay_contract';

		await wrapper.vm.$nextTick();

		expect(findFilter(wrapper, potentialKey).active).toBe(true);

		wrapper.find(`input#${contractKey}`).trigger('click');

		await wrapper.vm.$nextTick();

		expect(findFilter(wrapper, contractKey).active).toBe(true);

		wrapper
			.findAll('.action-btn')
			.at(0)
			.trigger('click');

		await wrapper.vm.$nextTick();

		expect(findFilter(wrapper, contractKey).active).toBe(false);
		expect(findFilter(wrapper, potentialKey).active).toBe(true);
	});

	it('should get error when fetch filter has failed', async () => {
		global.axios = {
			get: jest.fn().mockRejectedValue()
		};

		const error = jest.fn();
		global.console = { error };

		const wrapper = mountComponent();
		wrapper.setData({ isOpen: true });

		await flushPromises();

		expect(error).toHaveBeenCalled();
	});
});
