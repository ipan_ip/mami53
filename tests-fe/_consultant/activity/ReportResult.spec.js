import { shallowMount } from '@vue/test-utils';
import ReportResult from 'Consultant/pages/activity/components/ReportResult';

describe('ReportResult.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return shallowMount(ReportResult, {
				propsData: {
					reports: [{ label: 'test', value: 'test' }]
				},
				...options
			});
		};
	});

	it('should render component and match snapshot', () => {
		const wrapper = mountComponent();

		expect(wrapper.classes('report-result')).toBe(true);
		expect(wrapper.html()).toMatchSnapshot();
	});

	it('should render list correctly', () => {
		const wrapper = mountComponent({
			propsData: {
				reports: [
					{
						label: 'test',
						value: 'test',
						type: 'text'
					},
					{
						label: 'test',
						value: 'test',
						type: 'text'
					}
				]
			}
		});

		const lists = wrapper.findAll('.list__item');
		expect(lists.length).toBe(2);
	});

	it('should ignore report data when type has matched', async () => {
		const wrapper = mountComponent({
			propsData: { reports: [] }
		});

		wrapper.setData({ ignoredTypes: ['ignore-type'] });

		wrapper.setProps({
			reports: [
				{
					label: 'test',
					value: 'test',
					type: 'ignore-type'
				},
				{
					label: 'test',
					value: 'test',
					type: 'test'
				}
			]
		});

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.reportDataProcessed.length).toBe(1);
	});

	it('should formating date', async () => {
		const wrapper = mountComponent({
			propsData: { reports: [] },
			mocks: {
				$dayjs: jest.fn(() => ({
					format: jest.fn().mockReturnValue('formatted')
				}))
			}
		});

		wrapper.setProps({
			reports: [
				{
					label: 'test',
					value: 'not formatted yet',
					type: 'date'
				}
			]
		});

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.reportDataProcessed).toEqual(
			expect.arrayContaining([
				expect.objectContaining({
					label: expect.any(String),
					type: expect.any(String),
					value: 'formatted'
				})
			])
		);
	});
});
