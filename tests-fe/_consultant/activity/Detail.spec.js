import { shallowMount } from '@vue/test-utils';
import ActivityDetail from 'Consultant/pages/activity/Detail';

import { flushPromises } from 'tests-fe/utils/promises';
import * as storage from 'Js/@utils/storage';

import stageResponse from './data/stageResponse.json';
import detailResponse from './data/detailResponse.json';
import taskActivatedResponse from './data/taskActivatedResponse.json';
import reportResponse from './data/reportResponse.json';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

import EventBus from 'Consultant/event-bus';

jest.mock('Consultant/pages/activity/components/MoveStage', () =>
	jest.fn(() => ({
		template: '<div></div>'
	}))
);

jest.mock('Consultant/helpers/spyingElement', () =>
	jest.fn(() => ({
		spy: cl => cl({ percentage: 100 })
	}))
);

global.axios = {
	get: jest.fn().mockImplementation(url => {
		if (/\/activity-management\/funnel\//.test(url)) {
			return Promise.resolve({ data: detailResponse });
		} else if (/\/activity-management\/task\//.test(url)) {
			return Promise.resolve({ data: stageResponse });
		} else if (/\/activity-management\/progress\/detail\//.test(url)) {
			return Promise.resolve({ data: reportResponse });
		}
	}),
	post: jest.fn().mockResolvedValue({ data: taskActivatedResponse })
};

describe('ActivityDetail.vue', () => {
	let mountComponent = Function;
	const mockStore = {
		getters: {
			getConsultantFirstName: 'name',
			getConsultantRoles: 'role',
			isAdminConsultant: true
		}
	};

	const mockQuery = {
		funnel_id: 111,
		progress_id: 222,
		task_id: 333,
		progress: 0
	};

	beforeEach(() => {
		mountComponent = (options = {}, additionalMock = {}) => {
			return shallowMount(ActivityDetail, {
				mocks: {
					$route: {
						query: mockQuery,
						meta: {
							navbarOptions: {}
						}
					},
					$store: mockStore,
					...additionalMock
				},
				...options
			});
		};
	});

	it('should render component correctly', () => {
		const wrapper = mountComponent();

		expect(wrapper.classes('activity-detail')).toBe(true);
	});

	it('should go to staging log page and set local storage', async () => {
		const push = jest.fn();
		const wrapper = mountComponent({
			stubs: {
				CtStageStep: {
					template: `<div id="stage" @click="$emit('click:detail')"></div>`
				}
			},
			mocks: {
				$router: {
					push
				},
				$route: {
					query: mockQuery,
					meta: {
						navbarOptions: {}
					}
				},
				$store: mockStore
			}
		});

		const spyStorageLocalSetItem = jest.spyOn(storage.local, 'setItem');

		await flushPromises();

		wrapper.find('#stage').trigger('click');

		expect(spyStorageLocalSetItem).toHaveBeenCalled();
		expect(push).toHaveBeenCalled();
	});

	it('should go to dashboard when query is incomplete', () => {
		const push = jest.fn();
		mountComponent(
			{},
			{
				$router: { push },
				$route: {
					query: {},
					meta: {
						navbarOptions: {}
					}
				}
			}
		);

		expect(push).toHaveBeenCalled();
	});

	it('should update title on navbar option', async () => {
		const push = jest.fn();
		const wrapper = mountComponent(
			{},
			{
				$router: { push },
				$route: {
					query: {},
					meta: {
						navbarOptions: {}
					}
				}
			}
		);

		await flushPromises();

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.$route.meta.navbarOptions).toEqual(
			expect.objectContaining({
				title: expect.any(String)
			})
		);
	});

	it('should only render edit and next button when task_id is not 0', async () => {
		const wrapper = mountComponent();

		await flushPromises();

		await wrapper.vm.$nextTick();

		expect(wrapper.find('.section-float')).toMatchSnapshot();
	});

	it('should only render activate button when task_id is 0', async () => {
		const wrapper = mountComponent(
			{},
			{
				$route: {
					query: {
						...mockQuery,
						task_id: 0
					},
					meta: {
						navbarOptions: {}
					}
				}
			}
		);

		await flushPromises();

		await wrapper.vm.$nextTick();

		expect(wrapper.find('.section-float')).toMatchSnapshot();
	});

	it('should showing alert and replacing url when task activate was successed', async () => {
		const openAlert = jest.fn();
		const replace = jest.fn();
		EventBus.$on('openAlert', openAlert);
		mockWindowProperty('window.location', {
			pathname: '/test',
			replace
		});

		const wrapper = mountComponent(
			{
				stubs: {
					'ct-button': {
						template: `
						<button class="active-btn" @click="$emit('click')">
							<slot></slot>
						</button>`
					}
				}
			},
			{
				$route: {
					query: {
						...mockQuery,
						task_id: 0
					},
					meta: {
						navbarOptions: {}
					}
				}
			}
		);

		await flushPromises();
		await wrapper.vm.$nextTick();

		const activateBtn = wrapper.find('.active-btn');
		activateBtn.trigger('click');

		await flushPromises();

		expect(openAlert).toHaveBeenCalled();
		expect(replace).toHaveBeenCalled();
	});

	it('should get error when request API was failed', async () => {
		global.axios = {
			get: jest.fn().mockRejectedValue(new Error())
		};

		global.console = {
			error: jest.fn()
		};

		const wrapper = mountComponent(
			{},
			{
				$route: {
					query: {
						funnel_id: 1,
						progress_id: 2,
						task_id: 3,
						progress: 4
					},
					meta: {
						navbarOptions: {}
					}
				}
			}
		);

		const result = await wrapper.vm.fetchReport(1);

		await flushPromises();

		expect(result.error).toBeInstanceOf(Error);
	});
});
