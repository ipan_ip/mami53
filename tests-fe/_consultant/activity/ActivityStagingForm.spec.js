import { shallowMount } from '@vue/test-utils';
import { flushPromises } from 'tests-fe/utils/promises';

import ActivityStagingFormMock from './data/activity-staging-form.json';
import ActivityStagingForm from 'Consultant/pages/activity/StagingForm';
import expectExport from 'expect';

const axios = {
	get: jest.fn().mockResolvedValue({
		data: ActivityStagingFormMock
	})
};

const bugsnagClient = {
	notify: jest.fn()
};

const mocks = {
	$router: { push: jest.fn() },
	$route: {
		params: {
			stage_id: 100,
			task_id: 50
		}
	}
};

describe('Consultant/pages/activity/StagingForm', () => {
	/* eslint-disable-next-line no-unused-vars */
	let mountComponent = Function;

	beforeEach(() => {
		console.error = jest.fn();

		global.axios = axios;
		global.bugsnagClient = bugsnagClient;

		mountComponent = (options = {}) => {
			return shallowMount(ActivityStagingForm, {
				mocks,
				...options
			});
		};
	});

	test('should render correctly', async () => {
		const tree = shallowMount(ActivityStagingForm, {
			mocks
		});

		await flushPromises();

		expectExport(tree.element).toMatchSnapshot();
	});
});
