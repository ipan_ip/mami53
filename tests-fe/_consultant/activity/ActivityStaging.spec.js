import { shallowMount } from '@vue/test-utils';
import { flushPromises } from 'tests-fe/utils/promises';
import { swiper, swiperSlide } from 'vue-awesome-swiper';

import ActivityStagingMock from './data/tasks.json';

import ActivityStaging from 'Consultant/pages/activity/Staging.vue';

const axios = {
	get: jest.fn(() => {
		return Promise.resolve({
			data: ActivityStagingMock
		});
	})
};
global.axios = axios;

const bugsnagClient = {
	notify: jest.fn()
};
global.bugsnagClient = bugsnagClient;

const mocks = {
	$router: { push: jest.fn() },
	$route: { params: jest.fn() }
};

const stubsComponent = {
	swiper,
	swiperSlide
};

const alert = jest.spyOn(window, 'alert');

describe('Consultant/pages/activity/Staging.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return shallowMount(ActivityStaging, {
				stubs: stubsComponent,
				mocks,
				...options
			});
		};
	});

	test('should render correctly', async () => {
		const tree = shallowMount(ActivityStaging, {
			stubs: stubsComponent,
			mocks
		});

		expect(tree.element).toMatchSnapshot();
	});

	test('click to go to staging search', async () => {
		const wrapper = mountComponent();

		wrapper.vm.handleClickSeeAll(0, 0);
		await wrapper.vm.$nextTick();

		expect(wrapper.vm.$router.push).toBeCalledWith({
			name: 'activity.staging-search',
			params: {
				stage: 'backlog'
			}
		});

		wrapper.vm.handleClickSeeAll(1, 1);
		await wrapper.vm.$nextTick();

		expect(wrapper.vm.$router.push).toBeCalledWith({
			name: 'activity.staging-search',
			params: {
				stage: 'todo'
			}
		});

		wrapper.vm.handleClickSeeAll(2, 2);
		await wrapper.vm.$nextTick();

		expect(wrapper.vm.$router.push).toBeCalledWith({
			name: 'activity.staging-search',
			params: {
				stage: 2
			}
		});
	});

	// test('click to go to tenant detail', async () => {
	// 	const wrapper = mountComponent();

	// 	wrapper.vm.handleClickToDetail();
	// 	await wrapper.vm.$nextTick();

	// 	expect(wrapper.vm.$router.push).toBeCalledWith({
	// 		name: 'activity.detail'
	// 	});
	// });

	// test('fail on getting tasks list', async () => {
	// 	global.axios = {
	// 		get: jest.fn().mockRejectedValue(new Error('test error'))
	// 	};

	// 	mountComponent();

	// 	await flushPromises();

	// 	expect(alert).toBeCalled();
	// 	alert.mockRestore();
	// });
});
