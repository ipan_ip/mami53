import { mount } from '@vue/test-utils';
import ActivitySearch from 'Consultant/pages/activity/Search';
import searchResponse from './data/searchResponse.json';
import { flushPromises } from 'tests-fe/utils/promises';
import EventBus from 'Consultant/event-bus';

describe('ActivitySearch.vue', () => {
	let mountComponent = Function;
	let push;
	const tempConsole = { ...console };

	beforeEach(() => {
		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: searchResponse
			})
		};
		global.console = tempConsole;
		push = jest.fn();

		mountComponent = (options = {}) => {
			return mount(ActivitySearch, {
				mocks: {
					$route: {
						query: {}
					},
					$router: {
						push
					}
				},
				...options
			});
		};
	});

	it('should render component correctly', () => {
		const wrapper = mountComponent({
			mocks: {
				$route: {
					query: {}
				}
			}
		});

		expect(wrapper.classes('activity-search')).toBe(true);
	});

	it('should close search page', () => {
		const wrapper = mountComponent();

		wrapper.find('.search-action button').trigger('click');

		expect(push).toHaveBeenCalledWith({ name: 'activity' });
	});

	it('should render filter correctly', async () => {
		const dataTypes = [{ test: 'test' }];

		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: {
					status: true,
					data: dataTypes
				}
			})
		};

		const wrapper = mountComponent({
			stubs: {
				Chip: `<div class="filter-chip"></div>`
			}
		});

		await flushPromises();

		await wrapper.vm.$nextTick();

		const filters = wrapper.findAll('.filter-chip');

		expect(filters.length).toBe(1);
	});

	it('should get data type filter and concat with current data type', async () => {
		const dataType = { test: 'test' };

		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: {
					status: true,
					data: [dataType]
				}
			})
		};

		const wrapper = mountComponent();

		await flushPromises();

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.filterItems).toEqual(
			expect.arrayContaining([expect.objectContaining(dataType)])
		);
	});

	it('should auto activate filter items that match with query', async () => {
		const dataType = { related_table: 'test' };

		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: {
					status: true,
					data: [dataType]
				}
			})
		};

		const wrapper = mountComponent({
			mocks: {
				$route: {
					query: {
						data_type: 'test'
					}
				}
			},
			methods: {
				searchActivity: jest
					.fn()
					.mockResolvedValue({ success: true, total: 0, data: [] }),
				changeQueryPaginate: jest.fn(),
				updateDataSearch: jest.fn().mockReturnValue([])
			}
		});

		await flushPromises();

		// simulate get API search
		wrapper.vm.initDataActivity({});

		await flushPromises();

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.activeFilter).toBe('test');

		expect(wrapper.vm.filterItems).toEqual(
			expect.arrayContaining([{ ...dataType, active: true }])
		);
	});

	it('should change route with query of filter selected when search before', async () => {
		const dataType = { related_table: 'test' };
		const handleChangeRoute = jest.fn();
		const searchText = 'test search';

		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: {
					status: true,
					data: [dataType]
				}
			})
		};

		const wrapper = mountComponent({
			stubs: {
				Chip: `<button class="filter-chip" @click="$emit('click')"></button>`
			},
			methods: {
				handleChangeRoute
			}
		});

		await flushPromises();

		wrapper.setData({ search: searchText });

		await wrapper.vm.$nextTick();

		wrapper
			.findAll('.filter-chip')
			.at(0)
			.trigger('click');

		expect(handleChangeRoute).toHaveBeenCalled();
	});

	it('should update filter when search less then 5 and filter clicked', async () => {
		const dataType = { related_table: 'test' };
		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: {
					status: true,
					data: [dataType]
				}
			})
		};
		const wrapper = mountComponent({
			stubs: {
				Chip: `<button class="filter-chip" @click="$emit('click')"></button>`
			}
		});
		await flushPromises();
		wrapper.setData({ search: '12' });
		await wrapper.vm.$nextTick();
		wrapper
			.findAll('.filter-chip')
			.at(0)
			.trigger('click');
		expect(wrapper.vm.filterItems).toEqual(
			expect.arrayContaining([
				expect.objectContaining({
					related_table: 'test',
					active: true
				})
			])
		);
	});

	it('should not attach an event click on filter when not search before', async () => {
		const handleChangeRoute = jest.fn();

		const wrapper = mountComponent({
			stubs: {
				Chip: `<button class="filter-chip" @click="$emit('click')"></button>`
			},
			methods: {
				handleChangeRoute
			}
		});

		await flushPromises();

		await wrapper.vm.$nextTick();

		wrapper.find('.filter-chip').trigger('click');

		expect(handleChangeRoute).not.toHaveBeenCalledWith();
	});

	it("should reset all state when hasn't search query", async done => {
		const wrapper = mountComponent();

		await wrapper.vm.$nextTick();

		wrapper.setData({
			searched: true,
			searchResults: ['test'],
			filterItems: [{ active: true }]
		});
		await wrapper.vm.$nextTick();
		setTimeout(() => {
			expect(wrapper.vm.filterItems).toEqual(
				expect.arrayContaining([expect.objectContaining({ active: false })])
			);
			done();
		});
	});

	it('should render empty state when no result', async () => {
		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: {
					status: true,
					data: []
				}
			})
		};

		const wrapper = mountComponent({
			mocks: {
				$route: {
					query: {
						search: 'test search'
					}
				}
			}
		});

		await flushPromises();

		await wrapper.vm.$nextTick();

		expect(wrapper.find('.no-result').exists()).toBe(true);
	});

	it('should can not search when character is less than 5', () => {
		const wrapper = mountComponent();
		const openAlert = jest.fn();
		EventBus.$on('openAlert', openAlert);

		wrapper.setData({ search: '12' });

		wrapper.find({ ref: 'inputSearch' }).vm.onEnter({});

		expect(openAlert).toHaveBeenCalled();
	});

	it('should showing alert when no data type selected', async () => {
		const wrapper = mountComponent();

		const openAlert = jest.fn();
		EventBus.$on('openAlert', openAlert);

		wrapper.setData({ search: '12345' });

		wrapper.find({ ref: 'inputSearch' }).vm.onEnter({});

		expect(openAlert).toHaveBeenCalled();
	});

	it('should change route and set pagination search', async () => {
		const search = '12345';
		const handleChangeRoute = jest.fn();
		const wrapper = mountComponent({
			methods: {
				handleChangeRoute
			}
		});

		wrapper.setData({ search, filterItems: [{ active: true }] });

		wrapper.find({ ref: 'inputSearch' }).vm.onEnter({});

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.pagination.search).toBe(search);
		expect(handleChangeRoute).toHaveBeenCalled();
	});

	it('should call bus to open alert when search is fail', async () => {
		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: {
					status: false,
					data: []
				}
			})
		};
		mountComponent({
			mocks: {
				$route: {
					query: {
						search: 'test search'
					}
				}
			}
		});

		const openAlert = jest.fn();
		EventBus.$on('openAlert', openAlert);
		const error = jest.fn();
		// hide console error
		global.console = { error };

		await flushPromises();

		expect(openAlert).toHaveBeenCalled();
		expect(error).toHaveBeenCalled();
	});

	it('should get console error when search has rejected', async () => {
		global.axios = {
			get: jest.fn().mockRejectedValue()
		};

		const error = jest.fn();
		global.console = { error };

		mountComponent({
			mocks: {
				$route: {
					query: {
						search: 'test search'
					}
				}
			},
			methods: {
				initDataFilter: jest.fn()
			}
		});

		await flushPromises();

		expect(error).toHaveBeenCalled();
	});
});
