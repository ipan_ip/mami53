import { shallowMount } from '@vue/test-utils';
import ActivityListPage from 'Consultant/pages/activity/ActivityListPage.vue';
import activityListMock from './data/response.json';
import { flushPromises } from 'tests-fe/utils/promises';
import CtSwiper from 'Consultant/components/CtSwiper';

describe('ActivityListPage.vue', () => {
	let mountComponent = Function;
	const tempConsole = { ...console };

	beforeEach(() => {
		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: activityListMock
			})
		};

		global.console = tempConsole;
		global.tracker = jest.fn();

		mountComponent = (options = {}) => {
			return shallowMount(ActivityListPage, {
				mocks: {
					$router: {
						push: jest.fn()
					},
					$route: {
						query: {}
					},
					$store: {
						state: {
							authData: { id: 'tes' }
						}
					}
				},
				...options
			});
		};
	});

	it('should render correctly', () => {
		const wrapper = mountComponent();

		expect(wrapper.classes('activity-list')).toBe(true);
	});

	it('should go activity search', async () => {
		const wrapper = mountComponent({
			stubs: {
				CtInputText: `<input class="search" @click="$emit('click')" />`
			}
		});

		wrapper
			.findAll('.search')
			.at(0)
			.trigger('click');

		expect(wrapper.vm.$router.push).toHaveBeenCalledWith({
			name: 'activity.search'
		});
	});

	it('Should open filter', async () => {
		const openFilter = jest.fn();
		const wrapper = mountComponent({
			stubs: {
				Chip: `<button class="chip" @click="$emit('click')">chip</button>`,
				CtSwiper,
				FilterActivityList: {
					template: `<div></div>`,
					methods: {
						open: openFilter
					}
				}
			}
		});

		await flushPromises();
		await wrapper.vm.$nextTick();

		wrapper.find('.chip').trigger('click');
		expect(openFilter).toHaveBeenCalled();
	});

	it('Should open filter when no activity data', async () => {
		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: { status: false }
			})
		};

		// hide console error
		global.console = { error: jest.fn() };

		const openFilter = jest.fn();
		const wrapper = mountComponent({
			stubs: {
				Chip: `<button class="chip" @click="$emit('click')">chip</button>`,
				CtSwiper,
				FilterActivityList: {
					template: `<div></div>`,
					methods: {
						open: openFilter
					}
				}
			}
		});

		await flushPromises();
		await wrapper.vm.$nextTick();

		wrapper.find('.chip').trigger('click');
		expect(openFilter).toHaveBeenCalled();
	});

	it('Should console error ', async () => {
		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: { status: false }
			})
		};

		const error = jest.fn();

		global.console = { error };

		mountComponent();

		await flushPromises();

		expect(error).toHaveBeenCalled();
	});

	it('Should console error when something wrong in process', async () => {
		const errorResponse = 'error';
		global.axios = {
			get: jest.fn().mockRejectedValue(errorResponse)
		};

		const error = jest.fn();

		global.console = { error };

		mountComponent();

		await flushPromises();

		expect(error).toHaveBeenCalled();
	});

	it('Should go to funnel list', async () => {
		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: {
					status: true,
					data: [],
					total: 0
				}
			})
		};

		const push = jest.fn();

		const wrapper = mountComponent({
			stubs: {
				ConsultantButton: `<button class="activate" @click="$emit('click')">chip</button>`
			},
			mocks: {
				$router: {
					push
				},
				$route: {
					query: {}
				},
				$store: {
					state: {
						authData: { id: 'tes' }
					}
				}
			}
		});

		await flushPromises();

		wrapper.find('.activate').trigger('click');

		expect(push).toHaveBeenCalledWith({ name: 'activity.funnels' });
	});
});
