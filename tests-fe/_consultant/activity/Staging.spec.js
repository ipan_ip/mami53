import { createLocalVue, shallowMount } from '@vue/test-utils';
import { flushPromises } from 'tests-fe/utils/promises';

import MockData from './data/staging.json';
import ActivityStaging from 'Consultant/pages/activity/Staging';

const axios = {
	get: jest.fn().mockResolvedValue({
		data: MockData
	})
};

const bugsnagClient = {
	notify: jest.fn()
};

const mocks = {
	$router: { push: jest.fn() },
	$route: {
		params: {
			funnel_id: 1,
			funnel_name: 'Data Penyewa Potential',
			funnel_type: 'Data Penyewa'
		}
	}
};

describe('Consultant/pages/activity/Staging', () => {
	test('should render correctly', async () => {
		const localVue = createLocalVue();

		const tree = shallowMount(ActivityStaging, {
			localVue,
			mocks
		});

		await flushPromises();

		expect(tree.element).toMatchSnapshot();
	});

	// ----------------------------------------------------------------

	let mountComponent = Function;

	beforeEach(() => {
		const localVue = createLocalVue();

		console.error = jest.fn();
		global.alert = jest.fn();

		global.axios = axios;
		global.bugsnagClient = bugsnagClient;

		mountComponent = (options = {}) => {
			return shallowMount(ActivityStaging, {
				localVue,
				mocks,
				...options
			});
		};
	});

	test('go to detail of a staging page', async () => {
		const wrapper = mountComponent();
		await flushPromises();

		wrapper.vm.handleClickToDetail({
			progressable_id: 1,
			task_id: null,
			progress: 0
		});

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.$router.push).toBeCalledWith({
			name: 'activity.detail',
			query: {
				funnel_id: 1,
				progress_id: 1,
				task_id: 0,
				progress: 0,
				funnel_name: 'Data Penyewa Potential'
			}
		});
	});

	test('go to search activity page', async () => {
		const wrapper = mountComponent();
		await flushPromises();

		wrapper.vm.handleClickSeeAll(0, null, 'Daftar Tugas');

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.$router.push).toBeCalledWith({
			name: 'activity.staging-search',
			params: {
				funnel_id: 1,
				stage: 'backlog',
				stage_name: 'Daftar Tugas',
				funnel_name: 'Data Penyewa Potential',
				data_type: 'Data Penyewa'
			}
		});
	});

	test('display alert if get bad API response', async () => {
		const alert = jest.spyOn(window, 'alert').mockImplementation(() => {});

		global.axios = {
			get: jest.fn().mockRejectedValue(new Error('test error'))
		};

		mountComponent();

		await flushPromises();

		expect(alert).toBeCalled();
		alert.mockRestore();
	});

	test('set funnel name as title on the header', async () => {
		const wrapper = mountComponent();
		const next = jest.fn();
		const to = {
			params: {
				funnel_name: 'Data Penyewa Potential'
			},
			meta: { navbarOptions: { title: '' } }
		};

		ActivityStaging.beforeRouteEnter.call(wrapper.vm, to, undefined, next);

		expect(to.meta.navbarOptions.title).toBe('Data Penyewa Potential');
		expect(next).toHaveBeenCalled();
	});
});
