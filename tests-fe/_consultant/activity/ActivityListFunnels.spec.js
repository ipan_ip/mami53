import { createLocalVue, shallowMount } from '@vue/test-utils';
import { flushPromises } from 'tests-fe/utils/promises';
import ActivityListFunnels from 'Consultant/pages/activity/components/ActivityListFunnels.vue';
import Vuex from 'vuex';

const localVue = createLocalVue();
localVue.use(Vuex);

const axios = {
	get: jest.fn(() => {
		return Promise.resolve({
			data: {
				status: true,
				data: [
					{ name: 'oCaAmgmVLn', data_type: 'Data Kontrak' },
					{ name: 'wme4jKscFo', data_type: 'Data Kontrak' },
					{ name: 'testing', data_type: 'Data Penyewa' }
				]
			}
		});
	})
};
global.axios = axios;

const bugsnagClient = {
	notify: jest.fn()
};
global.bugsnagClient = bugsnagClient;

const store = new Vuex.Store({
	getters: {
		getConsultantFirstName: jest.fn(),
		getConsultantRoles: jest.fn()
	}
});


describe('Consultant/pages/activity/components/ActivityListFunnels.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = (options = {}) => {
			return shallowMount(ActivityListFunnels, {
				stubs: ['router-link'],
				localVue,
				store,
				...options
			});
		};
	});

	test('should render correctly', async () => {
		const tree = shallowMount(ActivityListFunnels, {
			stubs: ['router-link'],
			localVue,
			store
		});

		expect(tree.element).toMatchSnapshot();
	});

	// axios fail fire alert
	test('fail on get funnels list', async () => {
		const alert = jest.spyOn(window, 'alert').mockImplementation(() => {});

		global.axios = {
			get: jest.fn().mockRejectedValue(new Error('test error'))
		};

		mountComponent();

		await flushPromises();

		expect(alert).toBeCalled();
		alert.mockRestore();
	});

	// axios get data status false fire alert with meta message
	test('status false on get funnels list', async () => {
		const alert = jest.spyOn(window, 'alert').mockImplementation(() => {});

		global.axios = {
			get: jest.fn(() => {
				return Promise.resolve({
					data: {
						status: false,
						meta: {
							message: 'Error message'
						}
					}
				});
			})
		};

		mountComponent();
		await flushPromises();

		expect(alert).toBeCalled();
		alert.mockRestore();
	});
});
