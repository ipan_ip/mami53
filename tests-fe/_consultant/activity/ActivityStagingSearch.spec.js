import { shallowMount } from '@vue/test-utils';
import { flushPromises } from 'tests-fe/utils/promises';

// import EventBus from 'Consultant/event-bus';

import 'tests-fe/utils/mock-vue';
import ActivityStagingSearchMock from './data/backlog.json';
import ActivityStagingSearch from 'Consultant/pages/activity/StagingSearch';

const axios = {
	get: jest.fn().mockResolvedValue({
		data: ActivityStagingSearchMock
	})
};

const bugsnagClient = {
	notify: jest.fn()
};

const mocks = {
	$router: { push: jest.fn() },
	$route: {
		params: {
			funnel_id: 0,
			stage: 'backlog'
		},
		query: {
			page: 1
		},
		meta: {
			navbarOptions: {}
		}
	}
};

// const stubsComponent = {
// 	handleGetQueriesFromRoute
// };

console.error = jest.fn();

describe('Consultant/pages/activity/StagingSearch', () => {
	let mountComponent = Function;

	beforeEach(() => {
		global.axios = axios;
		global.bugsnagClient = bugsnagClient;

		mountComponent = (options = {}) => {
			return shallowMount(ActivityStagingSearch, {
				mocks,
				...options
			});
		};
	});

	test('should render correctly', async () => {
		const tree = shallowMount(ActivityStagingSearch, {
			mocks
		});

		await flushPromises();

		expect(tree.element).toMatchSnapshot();
	});

	test('fail on getting tasks list', async () => {
		const alert = jest.spyOn(window, 'alert');

		global.axios = {
			get: jest.fn().mockRejectedValue(new Error('test error'))
		};

		mountComponent();

		await flushPromises();

		expect(alert).toBeCalled();
		alert.mockRestore();
	});

	test('success calling API but wrong value to API', async () => {
		const alert = jest.spyOn(window, 'alert');

		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: {
					status: false,
					meta: {
						message: 'No, it aint magic'
					}
				}
			})
		};

		mountComponent();

		await flushPromises();

		expect(alert).toBeCalled();

		// check the alert message
		// expect(alert.contain('test')).toBe(true);
		alert.mockRestore();
	});

	// test('should change route and set pagination search', async () => {
	// 	const handleChangeRoute = jest.fn();
	// 	const wrapper = mountComponent({
	// 		methods: {
	// 			handleChangeRoute
	// 		}
	// 	});

	// 	wrapper.setData({ search: 'test' });

	// 	// wrapper.find({ ref: 'inputSearch' }).vm.onEnter({});
	// 	// wrapper.find('ct-input-text-stub').vm.enter;

	// 	await wrapper.vm.$nextTick();

	// 	// expect(handleChangeRoute).toHaveBeenCalled();
	// });

	test('toggle checkbox to show the checkbox and bottom buttons', async () => {
		const wrapper = mountComponent();

		await flushPromises();

		wrapper.find('.search-header a').trigger('click');

		await wrapper.vm.$nextTick();

		expect(wrapper.contains('bottom-container-stub')).toBe(true);
	});


	// test('go to clicked task detail page', async() => {
	// 	const wrapper = mountComponent();

	// 	await flushPromises();

	// 	wrapper.find('.search-item').trigger('click');

	// 	await wrapper.vm.$nextTick();

	// 	console.log(wrapper.html());

	// 	expect(wrapper.vm.$router.push).toBeCalledWith({
	// 		name: 'activity.detail'
	// 	});
	// });

	// test('activated checked values', async () => {
	// 	const wrapper = mountComponent();

	// 	wrapper.setData({ showCheckbox: true, checked: [1, 2, 3, 4, 5] });

	// 	await wrapper.vm.$nextTick();
	// 	await flushPromises();

	// 	wrapper.find({ ref: 'submit-checked' }).trigger('click');

	// 	await wrapper.vm.$nextTick();

	// 	expect(wrapper.vm.showLoading).toBe(true);
	// 	expect(wrapper.vm.showCheckBox).toBe(false);

	// 	console.log(wrapper.html());
	// });
});
