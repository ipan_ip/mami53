import { createLocalVue, shallowMount } from '@vue/test-utils';
import { flushPromises } from 'tests-fe/utils/promises';

// import x_mock_json_component_name_x from 'x_url_for_your_mock_json_x';
import BookingManagementDetail from 'Consultant/pages/booking-management/Detail';

const axios = {
	get: jest.fn().mockResolvedValue({
		// data: x_mock_json_component_name_x
	})
};

const bugsnagClient = {
	notify: jest.fn()
};

const mocks = {
	$router: { push: jest.fn() },
	$route: {
		params: {
			id: 1
		},
		meta: {
			navbarOptions: {
				onClickBack: jest.fn()
			}
		}
	},
	$store: {
		state: {
			authData: { id: 'tes' }
		}
	}
};

describe('Consultant/pages/booking-management/Detail', () => {
	test.skip('should render correctly', async () => {
		const localVue = createLocalVue();

		const tree = shallowMount(BookingManagementDetail, {
			localVue,
			mocks
		});

		await flushPromises();

		expect(tree.element).toMatchSnapshot();
	});

	// ----------------------------------------------------------------

	let mountComponent = Function;

	beforeEach(() => {
		const localVue = createLocalVue();

		console.error = jest.fn();
		global.alert = jest.fn();

		global.axios = axios;
		global.bugsnagClient = bugsnagClient;

		mountComponent = (options = {}) => {
			return shallowMount(BookingManagementDetail, {
				localVue,
				mocks,
				...options
			});
		};
	});

	/* test('x_test_name_x', async () => {
		const wrapper = mountComponent();
		await flushPromises();

		// expect
	}); */

	/*
	test('display alert if get bad API response', async () => {
		const alert = jest.spyOn(window, 'alert').mockImplementation(() => {});

		global.axios = {
			get: jest.fn().mockRejectedValue(new Error('test error'))
		};

		mountComponent();

		await flushPromises();

		expect(alert).toBeCalled();
		alert.mockRestore();
	});
	*/

});
