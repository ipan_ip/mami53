import { shallowMount, RouterLinkStub, createLocalVue } from '@vue/test-utils';
import BookingListCard from 'Consultant/pages/booking-management/components/BookingListCard';
import CtIcon from 'Consultant/components/CtIcon';
import bookingModule from 'Consultant/store/modules/booking';
import Vuex from 'vuex';
import bookingList from '../data/bookingList.json';
import kostLevel from '../data/kostLevel.json';

const localVue = createLocalVue();
localVue.use(Vuex);

describe('BookingListCard.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		const store = new Vuex.Store({
			modules: {
				booking: {
					...bookingModule,
					state: {
						...bookingModule.state,
						status: kostLevel.data.status
					}
				}
			}
		});

		mountComponent = (options = {}) => {
			return shallowMount(BookingListCard, {
				localVue,
				store,
				propsData: {
					item: bookingList.data[0]
				},
				mocks: {
					$route: {
						query: {}
					},
					$dayjs: jest.fn(d => ({ format: jest.fn(() => d) }))
				},
				stubs: {
					RouterLink: RouterLinkStub,
					CtIcon: CtIcon
				},
				...options
			});
		};
	});

	test('should render correctly and match snapshot', async () => {
		const wrapper = mountComponent();

		await wrapper.vm.$nextTick();

		expect(wrapper.element).toMatchSnapshot();
	});
});
