import { shallowMount } from '@vue/test-utils';
import FilterActionSheet from 'Consultant/pages/booking-management/components/FilterActionSheet';
import CtIcon from 'Consultant/components/CtIcon';

import kostLevel from '../data/kostLevel.json';

describe('FilterActionSheet.vue', () => {
	let mountComponent;

	beforeEach(() => {
		mountComponent = options => {
			return shallowMount(FilterActionSheet, {
				propsData: {
					kostLevels: kostLevel.data.level
				},
				stubs: {
					CtIcon,
					ActionSheet: {
						template: `<div><slot></slot></div>`
					}
				},
				mocks: {
					$route: {
						query: {}
					}
				},
				...options
			});
		};
	});

	it('Should render component and match snapshot', () => {
		const wrapper = mountComponent();

		expect(wrapper.html()).toMatchSnapshot();
	});

	it('Should separate goldplus and other filter', async () => {
		const goldplus = [
			{
				id: 1,
				name: 'GP 1'
			}
		];

		const others = [
			{
				id: 2,
				name: 'Other Filter'
			}
		];
		const wrapper = mountComponent();

		wrapper.setProps({ kostLevels: [...goldplus, ...others] });

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.goldPlusList).toEqual(goldplus);
		expect(wrapper.vm.otherList).toEqual(others);
	});

	it('Should separate goldplus and other filter which were selected', async () => {
		const goldplus = [
			{
				id: 1,
				name: 'GP 1'
			}
		];

		const others = [
			{
				id: 2,
				name: 'Other Filter'
			}
		];
		const wrapper = mountComponent({
			mocks: {
				$route: {
					query: {
						filters: '1,2'
					}
				}
			}
		});

		wrapper.setProps({ kostLevels: [...goldplus, ...others] });

		wrapper.vm.open();

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.selectedGoldPlus).toEqual([1]);
		expect(wrapper.vm.selectedOther).toEqual([2]);
	});

	it('Should set selected instant booking filter', async () => {
		const wrapper = mountComponent({
			mocks: {
				$route: {
					query: {
						instant_booking: 'active,inactive'
					}
				}
			}
		});

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.selectedInstantBooking).toEqual(['active', 'inactive']);
	});
});
