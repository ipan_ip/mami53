import { shallowMount, createLocalVue } from '@vue/test-utils';
import BookingListActions from 'Consultant/pages/booking-management/components/BookingListActions';
import CtSwiper from 'Consultant/components/CtSwiper';

import { flushPromises } from 'tests-fe/utils/promises';
import kostLevel from '../data/kostLevel.json';

import Vuex from 'vuex';
import bookingModule from 'Consultant/store/modules/booking';

const localVue = createLocalVue();
localVue.use(Vuex);

describe('BookingListActions.vue', () => {
	let mountComponent = Function;

	global.axios = {
		get: jest.fn().mockResolvedValue({ data: kostLevel })
	};

	beforeEach(() => {
		const store = new Vuex.Store({
			modules: {
				booking: bookingModule
			}
		});

		mountComponent = (options = {}) => {
			return shallowMount(BookingListActions, {
				localVue,
				store,
				mocks: {
					$route: {
						query: {}
					}
				},
				...options
			});
		};
	});

	test('should render correctly and match snapshot', async () => {
		const wrapper = mountComponent();

		await flushPromises();

		expect(wrapper.element).toMatchSnapshot();
	});

	test('should mapping data status', async () => {
		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: {
					status: true,
					data: {
						status: [{ key: 'test', label: 'Test' }],
						level: []
					}
				}
			})
		};

		const wrapper = mountComponent();

		await flushPromises();

		expect(wrapper.vm.statusList).toEqual([
			expect.objectContaining({ value: 'all', label: 'Semua Status' }),
			expect.objectContaining({ value: 'test', label: 'Test' })
		]);
	});

	test('should set "Semua Status" to be active', async () => {
		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: {
					status: true,
					data: {
						status: [{ key: 'test', label: 'Test' }],
						level: []
					}
				}
			})
		};

		const wrapper = mountComponent();

		await flushPromises();

		expect(wrapper.vm.statusList).toEqual(
			expect.arrayContaining([
				{ value: 'all', label: 'Semua Status', active: true }
			])
		);
	});

	test('should set status to be active according to "status" query', async () => {
		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: {
					status: true,
					data: {
						status: [{ key: 'test-status', label: 'Test Status' }],
						level: []
					}
				}
			})
		};

		const wrapper = mountComponent({
			mocks: {
				$route: {
					query: {
						status: 'test-status'
					}
				}
			}
		});

		await flushPromises();

		expect(wrapper.vm.statusList).toEqual([
			{ value: 'all', label: 'Semua Status', active: false },
			{ value: 'test-status', label: 'Test Status', active: true }
		]);
	});

	test('should change to new query from status was clicked', async () => {
		const status = { key: 'test-status-click', label: 'Test Status' };
		global.axios = {
			get: jest.fn().mockResolvedValue({
				data: {
					status: true,
					data: {
						status: [status],
						level: []
					}
				}
			})
		};

		const push = jest.fn();

		const wrapper = mountComponent({
			stubs: {
				CtSwiper,
				Chip: {
					template: `<button class="status" @click="$emit('click')"></button>`
				}
			},
			mocks: {
				$route: { query: {}, name: 'test-booking' },
				$router: { push }
			}
		});

		await flushPromises();

		const chipStatus = wrapper.findAll('.status').at(1);
		chipStatus.trigger('click');

		expect(wrapper.vm.statusList).toEqual(
			expect.arrayContaining([
				{
					value: status.key,
					label: status.label,
					active: true
				}
			])
		);

		expect(push).toHaveBeenCalledWith({
			name: 'test-booking',
			query: expect.objectContaining({ page: 1, status: status.key })
		});
	});

	test('should change to "search" query when input search was enter', async () => {
		const push = jest.fn();

		const wrapper = mountComponent({
			stubs: {
				CtInputText: {
					template: `<input @keydown.enter="$emit('enter')" />`
				}
			},
			mocks: {
				$route: { query: {}, name: 'test-booking' },
				$router: { push }
			}
		});

		const search = 'Test Search';
		wrapper.setData({ search });

		await flushPromises();

		const input = wrapper.find('input');
		input.trigger('keydown.enter');

		expect(push).toHaveBeenCalledWith(
			expect.objectContaining({
				query: {
					page: 1,
					search
				}
			})
		);
	});
});
