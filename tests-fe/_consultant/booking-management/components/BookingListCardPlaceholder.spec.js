import { shallowMount } from '@vue/test-utils';

import BookingListCardPlaceholder from 'Consultant/pages/booking-management/components/BookingListCardPlaceholder';

describe('BookingListCardPlaceholder.vue', () => {
	let mountComponent = Function;

	beforeEach(() => {
		mountComponent = () => {
			return shallowMount(BookingListCardPlaceholder);
		};
	});

	test('should render correctly and match snapshot', async () => {
		const wrapper = mountComponent();

		await wrapper.vm.$nextTick();

		expect(wrapper.element).toMatchSnapshot();
	});
});
