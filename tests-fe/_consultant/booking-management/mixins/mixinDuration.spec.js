import { mount } from '@vue/test-utils';
import mixinDuration from 'Consultant/pages/booking-management/mixins/mixinDuration';

describe('mixinDuration.js', () => {
	let wrapper;

	beforeEach(() => {
		wrapper = mount({
			mixins: [mixinDuration],
			render: h => h('div')
		});
	});

	it('Should return "2 Minggu"', async () => {
		const expected = '2 Minggu';
		const duration = wrapper.vm.getBookingDuration(2, 'weekly');

		expect(duration).toBe(expected);
	});

	it('Should return "3 Bulan"', async () => {
		const expected = '3 Bulan';
		const duration = wrapper.vm.getBookingDuration(1, 'quarterly');

		expect(duration).toBe(expected);
	});

	it('Should return "2 x 6 Bulan"', async () => {
		const expected = '2 x 6 Bulan';
		const duration = wrapper.vm.getBookingDuration(2, 'semiannually');

		expect(duration).toBe(expected);
	});

	it('Should return "-"', async () => {
		const expected = '-';
		const duration = wrapper.vm.getBookingDuration(1);

		expect(duration).toBe(expected);
	});
});
