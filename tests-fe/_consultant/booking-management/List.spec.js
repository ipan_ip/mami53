import { createLocalVue, shallowMount } from '@vue/test-utils';
import BookingManagementList from 'Consultant/pages/booking-management/List';
import { flushPromises } from 'tests-fe/utils/promises';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import bookingList from './data/bookingList.json';

const mocks = {
	$route: {
		query: {}
	},
	$store: {
		state: {
			authData: { id: 'test' }
		}
	}
};

global.axios = {
	get: jest.fn().mockResolvedValue({ data: bookingList })
};

global.tracker = jest.fn();

describe('Consultant/pages/booking-management/List', () => {
	let mountComponent = Function;

	beforeEach(() => {
		const localVue = createLocalVue();
		mockVLazy(localVue);

		mountComponent = (options = {}) => {
			return shallowMount(BookingManagementList, {
				localVue,
				mocks,
				...options
			});
		};
	});

	test('should render correctly and match snapshot', async () => {
		const tree = mountComponent();

		await flushPromises();

		expect(tree.element).toMatchSnapshot();
	});

	test('should hit API with "is_instant_booking" query', async () => {
		const apiGet = jest.fn().mockResolvedValue({ data: bookingList });
		global.axios = { get: apiGet };

		mountComponent({
			mocks: {
				$route: {
					query: {
						instant_booking: 'active,inactive'
					}
				},
				$store: {
					state: {
						authData: { id: 'test' }
					}
				}
			}
		});

		await flushPromises();

		expect(apiGet).toHaveBeenCalledWith(
			expect.stringContaining(
				'is_instant_booking[]=true&is_instant_booking[]=false'
			)
		);
	});

	test('should hit API with "status" query', async () => {
		const apiGet = jest.fn().mockResolvedValue({ data: bookingList });
		const status = 'test-status-query';
		global.axios = { get: apiGet };

		mountComponent({
			mocks: {
				$route: {
					query: {
						status
					}
				},
				$store: {
					state: {
						authData: { id: 'test' }
					}
				}
			}
		});

		await flushPromises();

		expect(apiGet).toHaveBeenCalledWith(
			expect.stringContaining(`status=${status}`)
		);
	});

	test('should hit API with "kost_level" query', async () => {
		const apiGet = jest.fn().mockResolvedValue({ data: bookingList });
		global.axios = { get: apiGet };

		mountComponent({
			mocks: {
				$route: {
					query: {
						filters: '1,2,3'
					}
				},
				$store: {
					state: {
						authData: { id: 'test' }
					}
				}
			}
		});

		await flushPromises();

		expect(apiGet).toHaveBeenCalledWith(
			expect.stringContaining(`kost_level[]=1&kost_level[]=2&kost_level[]=3`)
		);
	});

	test('should hit API with "offset" query', async () => {
		const apiGet = jest.fn().mockResolvedValue({ data: bookingList });
		global.axios = { get: apiGet };

		mountComponent({
			mocks: {
				$route: {
					query: {
						page: 3
					}
				},
				$store: {
					state: {
						authData: { id: 'test' }
					}
				}
			}
		});

		await flushPromises();

		expect(apiGet).toHaveBeenCalledWith(expect.stringContaining(`offset=20`));
	});
});
