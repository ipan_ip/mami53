import { createLocalVue, shallowMount } from '@vue/test-utils';
import { flushPromises } from 'tests-fe/utils/promises';

// import x_mock_json_component_name_x from 'x_url_for_your_mock_json_x';
import BookingManagementAccept from 'Consultant/pages/booking-management/Accept';

const axios = {
	get: jest.fn().mockResolvedValue({
		// data: x_mock_json_component_name_x
	})
};

const bugsnagClient = {
	notify: jest.fn()
};

const mocks = {
	$dayjs: jest.fn(() => {
		return {
			locale: jest.fn(() => {
				return {
					format: jest.fn()
				};
			})
		};
	}),
	$router: { push: jest.fn() },
	$route: {
		params: {
			id: 1
		},
		meta: {
			navbarOptions: {
				onClickBack: () => {}
			}
		}
	}
};

describe('Consultant/pages/booking-management/Accept', () => {
	test.skip('should render correctly', async () => {
		const localVue = createLocalVue();

		const tree = shallowMount(BookingManagementAccept, {
			localVue,
			mocks
		});

		await flushPromises();

		expect(tree.element).toMatchSnapshot();
	});

	// ----------------------------------------------------------------

	let mountComponent = Function;

	beforeEach(() => {
		const localVue = createLocalVue();

		console.error = jest.fn();
		global.alert = jest.fn();

		global.axios = axios;
		global.bugsnagClient = bugsnagClient;

		mountComponent = (options = {}) => {
			return shallowMount(BookingManagementAccept, {
				localVue,
				mocks,
				...options
			});
		};
	});

	/* test('x_test_name_x', async () => {
		const wrapper = mountComponent();
		await flushPromises();

		// expect
	}); */

	/*
	test('display alert if get bad API response', async () => {
		const alert = jest.spyOn(window, 'alert').mockImplementation(() => {});

		global.axios = {
			get: jest.fn().mockRejectedValue(new Error('test error'))
		};

		mountComponent();

		await flushPromises();

		expect(alert).toBeCalled();
		alert.mockRestore();
	});
	*/

});
