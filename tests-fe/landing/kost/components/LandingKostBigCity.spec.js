import { shallowMount } from '@vue/test-utils';
import LandingKostBigCity from 'Js/landing/kost/components/LandingKostBigCity';

describe('LandingKostBigCity.vue', () => {
	const wrapper = shallowMount(LandingKostBigCity);

	it('Component should match the snapshot', () => {
		expect(wrapper).toMatchSnapshot();
	});
});
