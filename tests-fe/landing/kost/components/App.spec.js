import { shallowMount } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import App from 'Js/landing/kost/components/App';

describe('App.vue', () => {
	const wrapper = shallowMount(App);

	it('Component should match the snapshot', () => {
		expect(wrapper).toMatchSnapshot();
	});
});
