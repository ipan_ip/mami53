import Vuex from 'vuex';
import { createLocalVue, shallowMount } from '@vue/test-utils';
import LandingKostArticlePromo from 'Js/landing/kost/components/LandingKostArticlePromo';
import LandingStore from './__mocks__/landingKostStore';

global.slug = 'kost-jogja-murah';
window.bugsnagClient = { notify: jest.fn() };

const localVue = createLocalVue();
localVue.use(Vuex);
const store = new Vuex.Store(LandingStore);

describe('LandingKostArticlePromo.vue', () => {
	const wrapper = shallowMount(LandingKostArticlePromo, {
		localVue,
		store
	});

	it('Component should match the snapshot', () => {
		expect(wrapper).toMatchSnapshot();
	});

	it('Computed description should return from the store', () => {
		expect(wrapper.vm.description).toEqual(
			wrapper.vm.$store.state.extras.description
		);
	});

	it('Computed breadcrumb should return from the store', () => {
		expect(wrapper.vm.breadcrumb).toEqual(
			wrapper.vm.$store.state.extras.breadcrumb
		);
	});
});
