import { createLocalVue, shallowMount } from '@vue/test-utils';
import LandingKostCardBooking from 'Js/landing/kost/components/LandingKostCardBooking';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

const localVue = createLocalVue();
mockVLazy(localVue);

const prevSlideSpy = jest.spyOn(LandingKostCardBooking.methods, 'prevSlide');
const nextSlideSpy = jest.spyOn(LandingKostCardBooking.methods, 'nextSlide');

jest.useFakeTimers();

describe('LandingKostCardBooking.vue', () => {
	let wrapper = shallowMount(LandingKostCardBooking, {
		localVue
	});

	it('Computed isPwa should return correctly', () => {
		expect(wrapper.vm.isPwa).toBe(false);

		global.navigator.standalone = true;

		wrapper = shallowMount(LandingKostCardBooking, {
			localVue
		});

		expect(wrapper.vm.isPwa).toBe(true);
	});

	it('Computed enterClass should return correctly based on the slider status', async () => {
		expect(wrapper.vm.enterClass).toBe('slide-in-left');
		expect(wrapper.vm.leaveClass).toBe('slide-out-right');

		await wrapper.setData({ nextSlider: true });

		expect(wrapper.vm.enterClass).toBe('slide-in-right');
		expect(wrapper.vm.leaveClass).toBe('slide-out-left');
	});

	it('Should call method prevSlide on clicking the image', async () => {
		const testCase = [
			{
				noExpect: true
			},
			{
				noExpect: false,
				slideActive: 2,
				expect: 1
			},
			{
				noExpect: false,
				slideActive: 1,
				expect: 3
			}
		];

		const prevElement = wrapper.findAll('img.slider-control').at(0);

		expect(prevElement.exists()).toBe(true);

		for (const test of testCase) {
			if (!test.noExpect) {
				wrapper.setData({ slideActive: test.slideActive, onSlide: false });
				prevElement.trigger('click');

				await wrapper.vm.$nextTick();

				expect(prevSlideSpy).toBeCalled();

				expect(wrapper.vm.slideActive).toBe(test.expect);

				jest.runAllTimers();

				expect(wrapper.vm.onSlide).toBe(false);
			} else {
				await wrapper.setData({ onSlide: true });
				prevElement.trigger('click');
			}
		}
	});

	it('Should call method nextSlide on clicking the image', async () => {
		const testCase = [
			{
				noExpect: true
			},
			{
				noExpect: false,
				slideActive: 2,
				expect: 3
			},
			{
				noExpect: false,
				slideActive: 3,
				expect: 1
			}
		];

		const nextElement = wrapper.findAll('img.slider-control').at(1);

		expect(nextElement.exists()).toBe(true);

		for (const test of testCase) {
			if (!test.noExpect) {
				wrapper.setData({ slideActive: test.slideActive, onSlide: false });
				nextElement.trigger('click');

				await wrapper.vm.$nextTick();

				expect(nextSlideSpy).toBeCalled();

				expect(wrapper.vm.slideActive).toBe(test.expect);

				jest.runAllTimers();

				expect(wrapper.vm.onSlide).toBe(false);
			} else {
				await wrapper.setData({ onSlide: true });
				nextElement.trigger('click');
			}
		}
	});
});
