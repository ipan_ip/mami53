import Vuex from 'vuex';
import { createLocalVue, shallowMount } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import ContainerLandingKost from 'Js/landing/kost/components/ContainerLandingKost';
import { makeAPICall } from 'Js/@utils/makeAPICall.js';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import LandingStore from './__mocks__/landingKostStore';
import EventBus from 'Js/@global/event-bus';

jest.mock('Js/@utils/makeAPICall.js');
jest.mock('lodash/debounce', () => jest.fn(fn => fn));
jest.useFakeTimers();

const localVue = createLocalVue();
const store = new Vuex.Store(LandingStore);
mockVLazy(localVue);

global.bugsnagClient = {
	notify: jest.fn()
};
global.scrollTo = jest.fn();
global.tracker = jest.fn();

const toggleModalFacilitiesSpy = jest.spyOn(
	ContainerLandingKost.methods,
	'toggleModalFacilities'
);
const closeSearchSpy = jest.spyOn(ContainerLandingKost.methods, 'closeSearch');
const closeModalLoginSpy = jest.spyOn(
	ContainerLandingKost.methods,
	'closeModalLogin'
);
const listenEventBusSpy = jest.spyOn(
	ContainerLandingKost.methods,
	'listenEventBus'
);
const changeBoundsSpy = jest.spyOn(
	ContainerLandingKost.methods,
	'changeBounds'
);
const openClusterSpy = jest.spyOn(ContainerLandingKost.methods, 'openCluster');
const radiusChangeHandlerSpy = jest.spyOn(
	ContainerLandingKost.methods,
	'radiusChangeHandler'
);
const addTrackEventChangeRadiusSpy = jest.spyOn(
	ContainerLandingKost.methods,
	'addTrackEventChangeRadius'
);
const handleScrollSpy = jest.spyOn(
	ContainerLandingKost.methods,
	'handleScroll'
);
const resetStickyFilterPositionSpy = jest.spyOn(
	ContainerLandingKost.methods,
	'resetStickyFilterPosition'
);

const loginUser = {
	template: `<button class="close-login" @click="$emit('close')" />`
};
const leafletMap = {
	template: `<div ref="landingMap"><button @click="$emit('bounds-changed', [])" class="bounds-change" /><button @click="$emit('select-cluster', {
    latitude: -6.123, longitude: 7.12313})" class="select-cluster" /></div>`,
	methods: {
		showClusters: jest.fn()
	}
};
const modalFacilities = {
	template: `<button class="toggle-facilities" @click="$emit('facilities-modal-handler', false)"/>`
};
const globalNavbar = {
	template: `<button class="close-search" @click="$emit('closeModalWithProps')"/>`
};
const filterMobile = {
	template: `<button class="reset-sticky" @click="$emit('reset-sticky-filter-handler')"/>`
};

const stubs = {
	loginUser,
	modalFacilities,
	globalNavbar,
	leafletMap,
	filterMobile
};

const mocks = {
	$ls: {
		set: jest.fn()
	}
};

describe('ContainerLandingKost.vue', () => {
	global.innerWidth = 1366;
	makeAPICall.mockImplementation(payload => {
		if (payload.url === '/stories/filters') {
			return {
				fac_room: [],
				fac_share: [],
				kos_rule: [],
				rent_type: []
			};
		}
	});

	let wrapper = shallowMount(ContainerLandingKost, {
		localVue,
		store,
		stubs,
		mocks
	});

	it('Should call listenEventBus method on mounted', async () => {
		expect(listenEventBusSpy).toBeCalled();

		const eventBusMock = jest.fn();
		EventBus.$on('openModalLoginFav', eventBusMock);
		EventBus.$on('toTop', eventBusMock);
		EventBus.$on('openCLusterNextPage', eventBusMock);
		EventBus.$on('searchToggle', eventBusMock);

		await EventBus.$emit('openModalLoginFav');
		await EventBus.$emit('toTop');
		jest.runAllTimers();
		await EventBus.$emit('openCLusterNextPage');
		await EventBus.$emit('searchToggle');

		expect(eventBusMock).toBeCalledTimes(4);
	});

	it('Computed isPwa should return correctly', () => {
		expect(wrapper.vm.isPwa).toBe(false);
		EventBus.$emit = jest.fn();

		global.navigator.standalone = true;

		wrapper = shallowMount(ContainerLandingKost, {
			localVue,
			store,
			stubs,
			mocks
		});

		expect(wrapper.vm.isPwa).toBe(true);
	});

	it('Computed centerLatitude should return from store', () => {
		expect(wrapper.vm.centerLatitude).toBe(
			wrapper.vm.$store.state.extras.centerLatitude
		);
	});

	it('Computed landingBreadcrumb should return from store', () => {
		expect(wrapper.vm.landingBreadcrumb).toEqual(
			wrapper.vm.$store.state.extras.breadcrumb
		);
	});

	it('Computed centerLongitude should return from store', () => {
		expect(wrapper.vm.centerLongitude).toBe(
			wrapper.vm.$store.state.extras.centerLongitude
		);
	});

	it('Computed landingBreadcrumbTitle should return from store', () => {
		expect(wrapper.vm.landingBreadcrumbTitle).toBe('Kost Jakarta');
	});

	it('Computed isLogin should return correctly', () => {
		expect(wrapper.vm.isLogin).toBe(false);
	});

	it('Computed landingParams should return from store', () => {
		expect(wrapper.vm.landingParams).toEqual(wrapper.vm.$store.state.landing);
	});

	it('Computed cluster should return from store', () => {
		expect(wrapper.vm.cluster).toEqual(wrapper.vm.$store.state.extras.cluster);
	});

	it('Computed corner should return from store', () => {
		expect(wrapper.vm.corner).toEqual(wrapper.vm.$store.state.extras.cornerMap);
	});

	it('Computed center should return from store', () => {
		expect(wrapper.vm.center).toEqual({
			lat: -6.1241,
			lng: 7.2882
		});
	});

	it('Watch radius and getClusterRooms should called correctly', async () => {
		wrapper.vm.$store.commit('setFacLoading', false);
		wrapper.vm.$refs.landingMap.showClusters = jest.fn();
		wrapper.vm.$ls.set = jest.fn();
		wrapper.vm.$store.commit('setKostCluster', {
			rooms: [{ name: 'kost test' }]
		});
		wrapper.vm.$store.commit('setIsLoading', false);

		expect(wrapper.vm.setUrlFilter).toBe(false);

		await wrapper.setData({
			radius: 5
		});

		expect(radiusChangeHandlerSpy).toBeCalled();
		expect(addTrackEventChangeRadiusSpy).toBeCalled();
		expect(wrapper.vm.setUrlFilter).toBe(true);

		await wrapper.setData({
			radius: 3
		});

		expect(EventBus.$emit).toBeCalledWith('getList');
	});

	it('Should call toggleModalFacilities method correctly', async () => {
		const toggleModalElement = wrapper.find('button.toggle-facilities');

		expect(toggleModalElement.exists()).toBe(true);

		await toggleModalElement.trigger('click');

		expect(toggleModalFacilitiesSpy).toBeCalledWith(false);
	});

	it('Should call closeSearch method correctly on click', async () => {
		const closeSearchElement = wrapper.find('button.close-search');

		expect(closeSearchElement.exists()).toBe(true);

		await closeSearchElement.trigger('click');

		expect(closeSearchSpy).toBeCalled();
		expect(EventBus.$emit).toBeCalledWith('searchToggle');
	});

	it('Should call changeBounds method correctly on change map', async () => {
		const boundsElement = wrapper.find('button.bounds-change');

		await wrapper.setData({ setUrlFilter: false });
		expect(boundsElement.exists()).toBe(true);

		await boundsElement.trigger('click');

		expect(changeBoundsSpy).toBeCalled();
		expect(wrapper.vm.setUrlFilter).toBe(true);

		await boundsElement.trigger('click');

		expect(EventBus.$emit).toBeCalledWith('getList');
	});

	it('Should call openCluster method correctly on change map', async () => {
		const selectClusterElement = wrapper.find('button.select-cluster');

		expect(selectClusterElement.exists()).toBe(true);

		await selectClusterElement.trigger('click');

		expect(openClusterSpy).toBeCalled();
		expect(EventBus.$emit).toBeCalledWith('resetPaginationInnervalue');
		expect(EventBus.$emit).toBeCalledWith('toTop');
	});

	it('Should call closeModalLogin method on click', async () => {
		const closeModalLogin = wrapper.find('button.close-login');

		expect(closeModalLogin.exists()).toBe(true);

		await closeModalLogin.trigger('click');

		expect(closeModalLoginSpy).toBeCalled();
	});

	it('Should call resetStickyFilterPositionSpy method correctly', async () => {
		wrapper = shallowMount(ContainerLandingKost, {
			localVue,
			store,
			stubs,
			mocks,
			computed: {
				isMobile() {
					return true;
				}
			}
		});

		const resetElement = wrapper.find('button.reset-sticky');

		expect(resetElement.exists()).toBe(true);

		await resetElement.trigger('click');

		expect(resetStickyFilterPositionSpy).toBeCalled();
	});

	it('Should call handleScroll on scroll correctly', async () => {
		const testCase = [
			{
				isMobile: true,
				innerWidth: 400,
				pageYOffset: 1000,
				lastScrollPosition: 50,
				ref: true,
				authCheck: {
					all: true
				},
				topValue: 20
			},
			{
				isMobile: true,
				innerWidth: 400,
				pageYOffset: 1000,
				lastScrollPosition: 50,
				ref: true,
				authCheck: {
					all: true
				},
				topValue: 60
			},
			{
				isMobile: true,
				innerWidth: 1200,
				pageYOffset: 1000,
				lastScrollPosition: 1200,
				ref: true,
				authCheck: {
					all: false
				},
				topValue: 60
			},
			{
				isMobile: true,
				innerWidth: 1200,
				pageYOffset: 1000,
				lastScrollPosition: 1200,
				ref: false,
				authCheck: {
					all: true
				},
				topValue: 120
			},
			{
				isMobile: false,
				innerWidth: 1366,
				pageYOffset: 1000,
				lastScrollPosition: 1200,
				ref: false,
				authCheck: {
					all: true
				},
				topValue: 80
			}
		];

		const mockMapEventListener = {};
		window.addEventListener = jest.fn((event, cb) => {
			mockMapEventListener[event] = cb;
		});

		for (const test of testCase) {
			global.pageYOffset = test.pageYOffset;
			global.innerWidth = test.innerWidth;

			wrapper = shallowMount(ContainerLandingKost, {
				localVue,
				store,
				stubs,
				mocks,
				computed: {
					isMobile() {
						return test.isMobile;
					}
				}
			});

			wrapper.vm.$store.commit('setAuthCheck', test.authCheck);
			wrapper.vm.$refs.contentWrapper.getBoundingClientRect = jest.fn(() => ({
				top: test.topValue
			}));

			await wrapper.setData({ lastScrollPosition: test.lastScrollPosition });
			await mockMapEventListener.scroll({ scrollTo: test.lastScrollPosition });

			expect(handleScrollSpy).toBeCalled();
		}
	});

	it('Should call removeEventListener on destroy', async () => {
		global.addEventListener = jest.fn();
		global.removeEventListener = jest.fn();

		wrapper.destroy();

		expect(window.removeEventListener).toBeCalled();
	});
});
