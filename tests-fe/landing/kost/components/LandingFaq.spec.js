import Vuex from 'vuex';
import { createLocalVue, shallowMount } from '@vue/test-utils';
import LandingFaq from 'Js/landing/kost/components/LandingFaq';

global.slug = 'kost-jogja-murah';
window.bugsnagClient = { notify: jest.fn() };

const storeData = {
	faqs: {
		original: {
			data: {
				location: 'purworejo',
				faqs: {
					cheapest_daily: {
						song_id: 70685712,
						area_city: 'Purworejo',
						price_daily: 150000
					},
					cheapest_monthly: {
						song_id: 62603720,
						area_city: 'Purworejo',
						price_monthly: 250000
					},
					cheapest_weekly: null,
					cheapest_yearly: {
						song_id: 56893823,
						area_city: 'Purworejo',
						price_yearly: 4800000
					},
					available_bisaBooking: null,
					cheapest_putri: {
						song_id: 62603720,
						area_city: 'Purworejo',
						price_monthly: 250000
					},
					cheapest_putra: null,
					cheapest_campur: {
						song_id: 73600186,
						area_city: 'Purworejo',
						price_monthly: 450000
					},
					cheapest_bebas24Jam: {
						song_id: 95309686,
						area_city: 'Purworejo',
						price_monthly: 500000
					},
					cheapest_pasutri: {
						song_id: 73600186,
						area_city: 'Purworejo',
						price_monthly: 450000
					},
					cheapest_kamarMandiDalam: {
						song_id: 73600186,
						area_city: 'Purworejo',
						price_monthly: 450000
					},
					cheapest_wifi: {
						song_id: 62603720,
						area_city: 'Purworejo',
						price_monthly: 250000
					}
				}
			}
		}
	}
};

const localVue = createLocalVue();
localVue.use(Vuex);
const storeFaq = new Vuex.Store(storeData);

describe('LandingFaq.vue', () => {
	const wrapper = shallowMount(LandingFaq, {
		localVue,
		storeFaq,
		mocks: {
			$store: { state: storeData }
		}
	});

	it('should render faq with response', () => {
		expect(wrapper.find('.faq').exists()).toBeTruthy();
		expect(wrapper.find('.faq__container').exists()).toBeTruthy();
	});
});
