import Vuex from 'vuex';
import { createLocalVue, shallowMount } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import LandingKostList from 'Js/landing/kost/components/LandingKostList';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import EventBus from 'Js/@global/event-bus';
import LandingStore from './__mocks__/landingKostStore';
const mixinNavigator = require('Js/@mixins/MixinNavigatorIsMobile');

const localVue = createLocalVue();
const store = new Vuex.Store(LandingStore);
mockVLazy(localVue);
localVue.mixin([mixinNavigator]);

global.bugsnagClient = {
	notify: jest.fn()
};
global.scrollTo = jest.fn();
global.tracker = jest.fn();

const openSearchBarSpy = jest.spyOn(LandingKostList.methods, 'openSearchBar');
const toggleLoginModalSpy = jest.spyOn(
	LandingKostList.methods,
	'toggleLoginModal'
);
const getRoomListSpy = jest.spyOn(LandingKostList.methods, 'getRoomList');
const getFilteredListSpy = jest.spyOn(
	LandingKostList.methods,
	'getFilteredList'
);
const pageChangedSpy = jest.spyOn(LandingKostList.methods, 'pageChanged');
const goTopSpy = jest.spyOn(LandingKostList.methods, 'goTop');

const mocks = {
	$ls: {
		set: jest.fn(),
		get: jest.fn()
	}
};

const relatedList = {
	template: '<div />'
};
const roomList = {
	template: `<button class="toggle-login" @click="$emit('toggleLoginModal')" />`
};
const paginate = {
	template: `<button class="page-change" @click="clickHandler(2)" />`,
	props: ['clickHandler']
};
const stubs = {
	relatedList,
	roomList,
	paginate
};

describe('LandingKostList.vue', () => {
	const wrapper = shallowMount(LandingKostList, {
		localVue,
		store,
		mocks,
		stubs
	});

	it('Computed filter should return from the store', () => {
		expect(wrapper.vm.filter).toEqual(wrapper.vm.$store.state.landing);
	});

	it('Computed totalPage should return correctly', () => {
		wrapper.vm.$store.commit('setKostList', {
			total: 10,
			limit: 5,
			rooms: []
		});

		expect(wrapper.vm.totalPage).toBe(2);

		wrapper.vm.$store.commit('setKostList', {
			total: 30,
			limit: 2,
			rooms: []
		});

		expect(wrapper.vm.totalPage).toBe(10);
	});

	it('Computed totalPageAll should return correctly', () => {
		wrapper.vm.$store.commit('setKostList', {
			total: 10,
			limit: 5,
			rooms: []
		});

		expect(wrapper.vm.totalPageAll).toBe(2);
	});

	it('Computed titleCondition should return correctly', () => {
		expect(wrapper.vm.isLoading).toBe(true);

		wrapper.vm.$store.commit('setIsLoading', false);
		wrapper.vm.$store.commit('setKostList', {
			total: 10,
			limit: 5,
			rooms: [
				{
					name: 'kost 1'
				}
			]
		});

		expect(wrapper.vm.isLoading).toBe(false);
	});

	it('Computed list should return from the store', () => {
		expect(wrapper.vm.list).toEqual(wrapper.vm.$store.state.extras.list);
	});

	it('Computed breadcrumb should return from the store', () => {
		expect(wrapper.vm.breadcrumb).toEqual(
			wrapper.vm.$store.state.extras.breadcrumb
		);
	});

	it('Computed landingTitle should return correctly', () => {
		expect(wrapper.vm.landingTitle).toBe('Kost Jakarta');
	});

	it('Computed page should return from the store', () => {
		expect(wrapper.vm.page).toBe(wrapper.vm.$store.state.landing.page);
	});

	it('Computed roomsCount should return correctly', () => {
		expect(wrapper.vm.roomsCount).toBe(1);
	});

	it('Computed roomsLoaded should return correctly', () => {
		expect(wrapper.vm.roomsLoaded).toBe(1);
	});

	it('Computed place should return from the store', () => {
		expect(wrapper.vm.place).toBe(wrapper.vm.$store.state.extras.place);
	});

	it('Computed tagIds should return from the store', () => {
		expect(wrapper.vm.tagIds).toEqual(
			wrapper.vm.$store.state.landing.filters.tag_ids
		);
	});

	it('Computed levelInfo should return from the store', () => {
		expect(wrapper.vm.levelInfo).toEqual(
			wrapper.vm.$store.state.landing.filters.level_info
		);
	});

	it('Computed isClusterOpen should return from the store', () => {
		expect(wrapper.vm.isClusterOpen).toEqual(
			wrapper.vm.$store.state.extras.isClusterOpen
		);
	});

	it('Computed title should return correctly', () => {
		expect(wrapper.vm.title).toBe(
			'1 - 1 kost dari 10 kos-kosan di sekitar Kost Jakarta'
		);

		wrapper.vm.$store.commit('setKostPageList', 3);
		wrapper.vm.$store.commit('setKostList', {
			total: 30,
			limit: 5,
			rooms: [
				{
					name: 'kost 1'
				},
				{
					name: 'kost 2'
				},
				{
					name: 'kost 3'
				},
				{
					name: 'kost 3'
				},
				{
					name: 'kost 3'
				}
			]
		});

		expect(wrapper.vm.title).toBe(
			'11 - 15 kost dari 30 kos-kosan di sekitar Kost Jakarta'
		);

		wrapper.vm.$store.commit('setIsLoading', true);

		expect(wrapper.vm.title).toBe('Sedang mencari...');
	});

	it('Computed filterChange should return correctly', () => {
		expect(wrapper.vm.filterChange).toBe(false);
	});

	it('Computed emptyStateData should return correctly', () => {
		expect(wrapper.vm.emptyStateData).toEqual({
			buttonText: 'Cari area lain',
			needButton: true,
			subTitle: 'Cari di Area lain untuk meningkatkan hasil pencarian Kos',
			title: 'Belum Ada Kos di Area Ini'
		});
	});

	it('Should call getFilteredList, setLoadingState, resetPaginationInnervalue method on mounted', async () => {
		expect(getFilteredListSpy).toBeCalled();

		const eventBusMock = jest.fn();
		EventBus.$on('getList', eventBusMock);
		EventBus.$on('setLoading', eventBusMock);
		EventBus.$on('resetPaginationInnervalue', eventBusMock);
		await EventBus.$emit('getList');
		await EventBus.$emit('setLoading');
		await EventBus.$emit('resetPaginationInnervalue');

		expect(eventBusMock).toBeCalledTimes(3);
		expect(getRoomListSpy).toBeCalled();
		expect(goTopSpy).toBeCalled();
	});

	it('Should call openSearchBar method correctly', async () => {
		EventBus.$emit = jest.fn();

		wrapper.vm.$store.commit('setKostList', {
			total: 0,
			limit: 20,
			rooms: []
		});
		wrapper.vm.$store.commit('setIsLoading', false);
		await wrapper.vm.$nextTick();
		const openSearchElement = wrapper.find('button.empty-button');

		expect(openSearchElement.exists()).toBe(true);

		await openSearchElement.trigger('click');

		expect(openSearchBarSpy).toBeCalled();
		expect(EventBus.$emit).toBeCalledWith('searchToggle');
	});

	it('Should call toggleLoginModal method correctly', async () => {
		wrapper.vm.$store.commit('setKostList', {
			total: 1,
			limit: 20,
			rooms: [
				{
					name: 'kost 1'
				}
			]
		});
		wrapper.vm.$store.commit('setIsLoading', false);
		await wrapper.vm.$nextTick();
		const toggleLoginElement = wrapper.find('button.toggle-login');

		expect(toggleLoginElement.exists()).toBe(true);

		await toggleLoginElement.trigger('click');

		expect(toggleLoginModalSpy).toBeCalled();
		expect(EventBus.$emit).toBeCalledWith('openModalLoginFav');
	});

	it('Should call pageChanged method correctly', async () => {
		const paginateElement = wrapper.find('button.page-change');

		expect(paginateElement.exists()).toBe(true);

		await paginateElement.trigger('click');

		expect(pageChangedSpy).toBeCalled();
	});
});
