import Vuex from 'vuex';
import { createLocalVue, shallowMount } from '@vue/test-utils';
import LandingKostListTags from 'Js/landing/kost/components/LandingKostListTags';
import LandingStore from './__mocks__/landingKostStore';

window.bugsnagClient = { notify: jest.fn() };

const localVue = createLocalVue();
localVue.use(Vuex);
const store = new Vuex.Store(LandingStore);

describe('LandingKostListTags.vue', () => {
	const wrapper = shallowMount(LandingKostListTags, {
		localVue,
		store
	});

	it('Component should match the snapshot', () => {
		expect(wrapper).toMatchSnapshot();
	});

	it('Computed landingRelatedArea should return from the store', () => {
		expect(wrapper.vm.landingRelatedArea).toEqual(
			wrapper.vm.$store.state.extras.landing_related_area
		);
	});

	it('Computed landingRelatedCampus should return from the store', () => {
		expect(wrapper.vm.landingRelatedCampus).toEqual(
			wrapper.vm.$store.state.extras.landing_related_campus
		);
	});
});
