import { shallowMount } from '@vue/test-utils';
import LandingKostCardForum from 'Js/landing/kost/components/LandingKostCardForum';

const prevSlideSpy = jest.spyOn(LandingKostCardForum.methods, 'prevSlide');
const nextSlideSpy = jest.spyOn(LandingKostCardForum.methods, 'nextSlide');

jest.useFakeTimers();

describe('LandingKostCardForum.vue', () => {
	const wrapper = shallowMount(LandingKostCardForum);

	it('Should call method prevSlide on clicking the button', async () => {
		const testCase = [
			{
				slideActive: 2,
				expect: 1
			},
			{
				slideActive: 1,
				expect: 4
			}
		];

		const prevElement = wrapper.findAll('i.slider-control').at(0);

		expect(prevElement.exists()).toBe(true);

		for (const test of testCase) {
			wrapper.setData({ slideActive: test.slideActive });
			prevElement.trigger('click');

			await wrapper.vm.$nextTick();

			expect(prevSlideSpy).toBeCalled();

			expect(wrapper.vm.slideActive).toBe(test.expect);
		}
	});

	it('Should call method nextSlide on mounted', () => {
		jest.advanceTimersByTime(4000);

		expect(nextSlideSpy).toBeCalled();
	});

	it('Should call method nextSlide on clicking the button', async () => {
		const testCase = [
			{
				slideActive: 2,
				expect: 3
			},
			{
				slideActive: 4,
				expect: 1
			}
		];

		const nextElement = wrapper.findAll('i.slider-control').at(1);

		expect(nextElement.exists()).toBe(true);

		for (const test of testCase) {
			wrapper.setData({ slideActive: test.slideActive });
			nextElement.trigger('click');

			await wrapper.vm.$nextTick();

			expect(nextSlideSpy).toBeCalled();

			expect(wrapper.vm.slideActive).toBe(test.expect);
		}
	});
});
