export default {
	state: {
		token: '1231323213',
		authCheck: {},
		authData: {},
		project: {
			photos: [],
			facilities: [
				{
					name: 'dummy facility 1',
					photo_url: 'img/dummy-1'
				},
				{
					name: 'dummy facility 2',
					photo_url: 'img/dummy-2'
				}
			],
			name: 'Project 1',
			city: 'Jakarta',
			developer: 'dummy developer',
			floors_count: 3,
			unit_count: 20,
			phones: {
				building: '',
				marketing: '',
				other: ''
			},
			towers: [
				{
					name: 'Avres',
					status: true
				}
			],
			types: [
				{
					bedroom_count: 1,
					bathroom_count: 3,
					size_string: 'xl'
				},
				{
					bedroom_count: 2,
					bathroom_count: 2,
					size_string: 'm'
				}
			],
			location: {
				latitude: -6.1512,
				longitude: 6.1235
			},
      breadcrumb: [
        {
          url: '/breadcrumb_url_1'
        },
        {
          url: '/breadcrumb_url_2'
        }
      ]
		},
		suggestions: {
      suggestion: {
        share_url: ''
      }
    },
		filters: {
			unitType: '',
			furnished: 'all',
			priceRange: [0, 50000000],
			tagIds: [],
			rentType: 2
		},
		sorting: {
			fields: 'price',
			direction: '-'
		},
		extras: {
			landingType: 'project',
			windowWidth: 1366,
			random_seeds: Math.floor(Math.random() * (1000 - 0 + 1)) + 0,
			photosFallback: [],
			descriptionFallback: 'Deskripsi fallback untuk'
		}
	},
	mutations: {
		setAuthCheck(state, payload) {
			state.authCheck = payload;
		},
		setAuthData(state, payload) {
			state.authData = payload;
		},
		changeSection(state, payload) {
			state.section = payload;
		},
		setProjectData(state, payload) {
			state.project = payload;
		},
		updateWindowWidth(state, payload) {
			state.extras.windowWidth = payload;
		},
		setSuggestionsData(state, payload) {
			state.suggestions = payload;
		},
		setFilterUnitType(state, payload) {
			state.filters.unitType = payload;
		},
		setFilterFurnished(state, payload) {
			state.filters.furnished = payload;
		},
		setFilterUnitPriceRange(state, payload) {
			state.filters.priceRange = payload;
		},
		setFilterTagIds(state, payload) {
			state.filters.tagIds = payload;
		},
		setFilterRentType(state, payload) {
			state.filters.rentType = payload;
		},
		setSortingDirection(state, payload) {
			state.sorting.direction = payload;
		}
	}
};
