export default {
	state: {
		token: 'A4ASD412551',
		authCheck: {},
		authdata: {},
		area: {
			description_1: '',
			description_2: '',
			description_3: ''
		},
		extras: {
			landingType: 'area',
			apartmentType: 'unit',
			windowWidth: 1366,
			random_seeds: Math.floor(Math.random() * (1000 - 0 + 1)) + 0,
			tags: {
				unit_facilities: [],
				project_facilities: []
			},
			tag_ids: [],
			tag_ids_project: [],
			descriptionFallback: 'description fallback'
		}
	},
	mutations: {
		setAuthCheck(state, payload) {
			state.authCheck = payload;
		},
		setAuthData(state, payload) {
			state.authData = payload;
		},
		changeSection(state, payload) {
			state.section = payload;
		},
		setAreaData(state, payload) {
			state.area = payload;
		},
		setMapCenter(state, payload) {
			state.center_latitude = payload.lat;
			state.center_longitude = payload.lng;
		},
		changeApartmentType(state, payload) {
			state.extras.apartmentType = payload;
		},
		updateWindowWidth(state, payload) {
			state.extras.windowWidth = payload;
		},
		setUnitTags(state, payload) {
			state.extras.tags.unit_facilities = payload;
		},
		setProjectTags(state, payload) {
			state.extras.tags.project_facilities = payload;
		},
		setFilterFacilities(state, payload) {
			state.extras.tag_ids = payload;
		},
		setFilterFacilitiesProject(state, payload) {
			state.extras.tag_ids_project = payload;
		}
	}
};
