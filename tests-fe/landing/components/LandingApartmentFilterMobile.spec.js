import { shallowMount } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import LandingApartmentFilterMobile from 'Js/landing/components/LandingApartmentFilterMobile';

const hideFilterOverlaySpy = jest.spyOn(
	LandingApartmentFilterMobile.methods,
	'hideFilterOverlay'
);
const filterCloseButton = {
	template: `<button class="hide-filter-exit" @click="$emit('press', 'exit')"/>`
};

describe('LandingApartmentFilterMobile.vue', () => {
	const wrapper = shallowMount(LandingApartmentFilterMobile, {
		stubs: {
			filterCloseButton
		},
		propsData: {
			tags: {
				data: []
			}
		}
	});

	it('Component should match the snapshot', () => {
		expect(wrapper).toMatchSnapshot();
	});

	it('Should call hideFilterOverlay method correctly', async () => {
		const hideElementExit = wrapper.find('button.hide-filter-exit');
		const hideElementElse = wrapper.find('button.btn-set-filter');

		expect(hideElementExit.exists()).toBe(true);
		expect(hideElementElse.exists()).toBe(true);

		await hideElementExit.trigger('click');

		expect(hideFilterOverlaySpy).toBeCalled();
		expect(wrapper.emitted('close')).toBeTruthy();

		await hideElementElse.trigger('click');

		expect(hideFilterOverlaySpy).toBeCalled();
		expect(wrapper.emitted('set-filter')).toBeTruthy();
	});
});
