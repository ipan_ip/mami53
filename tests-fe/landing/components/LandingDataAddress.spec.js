import Vuex from 'vuex';
import { createLocalVue, shallowMount } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import LandingDataAddress from 'Js/landing/components/LandingDataAddress';
import landingUnitStore from './__mocks__/landingUnitStore';

const localVue = createLocalVue();
localVue.use(Vuex);
const store = new Vuex.Store(landingUnitStore);

describe('LandingDataAddress.vue', () => {
	const wrapper = shallowMount(LandingDataAddress, {
		localVue,
		store,
		propsData: {
			grid: 'col-lg-12'
		}
	});

	it('Component should match the snapshot', () => {
		expect(wrapper).toMatchSnapshot();
	});

	it('Computed address should return from the store', () => {
		expect(wrapper.vm.address).toBe(wrapper.vm.$store.state.project.address);
	});
});
