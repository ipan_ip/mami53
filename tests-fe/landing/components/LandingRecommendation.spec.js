import LandingRecommendation from 'Js/landing/components/LandingRecommendation';
import landingUnitStore from './__mocks__/landingUnitStore';

import { createLocalVue, shallowMount } from '@vue/test-utils';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import Vuex from 'vuex';

const localVue = createLocalVue();
localVue.use(Vuex);
const store = new Vuex.Store(landingUnitStore);

describe('LandingRecommendation.vue', () => {
  const wrapper = shallowMount(LandingRecommendation, {
    localVue,
    store,
    propsData: {
      suggestions: []
    }
  });

  global.open = jest.fn();

  mockWindowProperty('open', open);

  it('should mount component properly', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('should has apartment suggestions url according to breadcrumb index', () => {
    expect(wrapper.vm.apartmentSuggestions).toBe('/breadcrumb_url_1');
  });

  it('should not call window.open when device is standalone or matchMedia is null', () => {
		mockWindowProperty('open', open);
    mockWindowProperty('location', { href: store.state.suggestions.suggestion.share_url });
    
    wrapper.vm.openDetailApartment();
    mockWindowProperty('matchMedia', null);
    expect(global.open).toBeCalled();
  });

  it('should open more apartment suggestions', () => {
    wrapper.vm.openMoreSuggestions();
    wrapper.vm.$nextTick();

    expect(open).toBeCalled();
  });
});