import Vuex from 'vuex';
import { createLocalVue, shallowMount } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import mockComponent from 'tests-fe/utils/mock-component';
import LandingDataType from 'Js/landing/components/LandingDataType';
import landingUnitStore from './__mocks__/landingUnitStore';

const localVue = createLocalVue();
localVue.use(Vuex);
const store = new Vuex.Store(landingUnitStore);

const stubs = {
	landingChildTitle: mockComponent
};

describe('LandingDataType.vue', () => {
	const wrapper = shallowMount(LandingDataType, {
		localVue,
		store,
		stubs,
		propsData: {
			grid: 'col-lg-12'
		}
	});

	it('Component should match the snapshots', () => {
		expect(wrapper).toMatchSnapshot();
	});

	it('Computed types should return correctly from store', () => {
		expect(wrapper.vm.types).toBe(wrapper.vm.$store.state.project.types);

		const mockData = {
			types: [
				{
					bedroom_count: 1,
					bathroom_count: 3,
					size_string: 'xl'
				}
			]
		};

		wrapper.vm.$store.commit('setProjectData', mockData);

		expect(wrapper.vm.types).toBe(mockData.types);
	});

	it('Computed isMediumDevice should return correctly from store', () => {
		expect(wrapper.vm.isMediumDevice).toBe(false);

		wrapper.vm.$store.commit('updateWindowWidth', 400);

		expect(wrapper.vm.isMediumDevice).toBe(true);
	});
});
