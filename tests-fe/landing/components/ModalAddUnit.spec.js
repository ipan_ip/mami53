import ModalAddUnit from 'Js/landing/components/ModalAddUnit.vue';
import { shallowMount, createLocalVue } from '@vue/test-utils';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import mockAxios from 'tests-fe/utils/mock-axios';

const localVue = createLocalVue();

global.axios = mockAxios;
global.bugsnagClient = {
  notify: jest.fn()
};

const mocksData = {
  data: {
    room_title: 'room_title',
		_id: 0,
		photo_url: {
			medium: '/path/to/medium/sized/image'
		}
  }
};

const mountData = {
  localVue,
  mocksData
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData };

  return shallowMount(ModalAddUnit, finalData);
};

describe('ModalAddUnit.vue', () => {
  let wrapper;

  delete window.location;
  window.location = { reload: jest.fn() };

  mockWindowProperty(
    '$',
    jest.fn().mockReturnValue({
      modal: jest.fn(),
      on: jest.fn((eventName, cb) => cb && cb())
    })
  );

  beforeEach(() => {
    wrapper = mount();
  });

  it('should mount properly', () => {
    expect(wrapper.find('#modalAddUnit').exists()).toBeTruthy();
  });

  it('should call $() when calling openModal method', () => {
    wrapper.vm.openModal();

    expect(global.$).toBeCalled();
  });

  it('should hide modalAddUnit when submitting add unit', async () => {
    await axios.mockResolve({
      data: {
        status: true
      }
    });
    await wrapper.vm.submitAddUnit();

    expect(global.$).toBeCalled();
  });
});
