import Vuex from 'vuex';
import { createLocalVue, shallowMount } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import LandingArea from 'Js/landing/components/LandingArea';
import LandingAreaStore from './__mocks__/landingAreaStore';
import mockComponent from 'tests-fe/utils/mock-component';
import { makeAPICall } from 'Js/@utils/makeAPICall.js';
import { translateWords } from 'Js/@utils/langTranslator.js';
import EventBus from 'Js/landing/event-bus';

jest.mock('Js/@utils/makeAPICall.js');
jest.mock('Js/@utils/langTranslator.js');

global.axios = {
	get: jest.fn().mockResolvedValue({}),
	post: jest.fn().mockResolvedValue({})
};
makeAPICall.mockImplementation(() => {
	return {};
});
global.bugsnagClient = {
	notify: jest.fn()
};
global.swal = jest.fn();
global.translateWords = jest.fn();
global.alert = jest.fn();
global.document.getElementById = jest.fn(() => {
	return {
		scrollIntoView: jest.fn()
	};
});
global.scrollTo = jest.fn();
global.tracker = jest.fn();
global.$ = jest.fn(() => {
	return {
		width: jest.fn().mockReturnValue(400)
	};
});
global.document = {
	getElementById: jest.fn()
};

const toggleFilterMobileSpy = jest.spyOn(
	LandingArea.methods,
	'toggleFilterMobile'
);
const setFilterUnitSpy = jest.spyOn(LandingArea.methods, 'setFilterUnit');
const goToProjectSpy = jest.spyOn(LandingArea.methods, 'goToProject');
const removeHashSpy = jest.spyOn(LandingArea.methods, 'removeHash');
const pageChangedSpy = jest.spyOn(LandingArea.methods, 'pageChanged');
const registerUnitSpy = jest.spyOn(LandingArea.methods, 'registerUnit');
const boundsChangedSpy = jest.spyOn(LandingArea.methods, 'boundsChanged');
const clusterUnitsSpy = jest.spyOn(LandingArea.methods, 'clusterUnits');
const getClusterDataSpy = jest.spyOn(LandingArea.methods, 'getClusterData');
const toggleApartmentTypeSpy = jest.spyOn(
	LandingArea.methods,
	'toggleApartmentType'
);
const getUnitDataSpy = jest.spyOn(LandingArea.methods, 'getUnitData');

const filterMobileButton = {
	template: `<button class="toggle-filter" @click="$emit('open')"/>`
};
const landingApartmentFilterMobile = {
	template: `<button class="set-filter" @click="$emit('set-filter')"/>`
};
const landingAreaUnit = {
	template: `<button class="page-change" @click="$emit('pageChanged', 2)"/>`
};
const showClustersSpy = jest.fn();
const leafletMap = {
	template: `<div ref="mamiMap"><button class="bounds-changed" @click="$emit('bounds-changed')"/><button class="select-cluster" @click="$emit('select-cluster', {latitude: -6.123, longitude: 6.123})"/></div>`,
	methods: {
		showClusters: showClustersSpy
	}
};

const stubs = {
	leafletMap,
	landingProjectList: mockComponent,
	listPlaceholder: mockComponent,
	mamiLoadingList: mockComponent,
	filterMobileButton,
	landingApartmentFilterMobile,
	landingAreaUnit
};

const localVue = createLocalVue();
localVue.use(Vuex);
const store = new Vuex.Store(LandingAreaStore);

describe('LandingArea.vue', () => {
	let wrapper = shallowMount(LandingArea, {
		localVue,
		store,
		stubs,
		propsData: {
			grid: 'col-md-12'
		}
	});

	it('Computed authCheck should return from store', () => {
		expect(wrapper.vm.authCheck).toEqual(wrapper.vm.$store.state.authCheck);

		const mockData = {
			owner: false,
			user: true,
			admin: false,
			all: true
		};

		wrapper.vm.$store.commit('setAuthCheck', mockData);

		expect(wrapper.vm.authCheck).toEqual(mockData);
	});

	it('Computed area should return from store', () => {
		expect(wrapper.vm.area).toEqual(wrapper.vm.$store.state.area);

		const mockData = {
			keyword: 'Jakarta',
			slug: 'kost/area/jakarta',
			center_latitude: -6.123123,
			center_longitude: 6.512515
		};

		wrapper.vm.$store.commit('setAreaData', mockData);

		expect(wrapper.vm.area).toEqual(mockData);
	});

	it('Computed landingTitle should return keyword from the store', () => {
		expect(wrapper.vm.landingTitle).toBe(wrapper.vm.$store.state.area.keyword);
	});

	it('Computed landingSlug should return slug from the store', () => {
		expect(wrapper.vm.landingSlug).toBe(wrapper.vm.$store.state.area.slug);
	});

	it('Computed randomSeeds should return random_seeds from the store', () => {
		expect(wrapper.vm.randomSeeds).toBe(
			wrapper.vm.$store.state.extras.random_seeds
		);
	});

	it('Computed center should return latitude longitude from the store', () => {
		expect(wrapper.vm.center).toEqual({
			lat: wrapper.vm.$store.state.area.center_latitude,
			lng: wrapper.vm.$store.state.area.center_longitude
		});
	});

	it('Computed hasMap should return correctly', () => {
		expect(wrapper.vm.hasMap).toBe(false);
	});

	it('Computed landingConnector should return correctly', () => {
		expect(wrapper.vm.landingConnector).toBe(
			wrapper.vm.$store.state.area.landing_connector
		);
	});

	it('Computed totalPageProject should return correctly', () => {
		expect(wrapper.vm.totalPageProject).toBe(0);

		wrapper.setData({
			project: {
				apartment_projects: [],
				total: 30,
				limit: 2,
				offset: 0
			}
		});

		expect(wrapper.vm.totalPageProject).toBe(10);
	});

	it('Watch area should handled correctly', async () => {
		global.axios = {
			get: jest.fn().mockResolvedValue({}),
			post: jest.fn().mockResolvedValue({})
		};

		wrapper.setData({
			params: {
				filters: {
					rent_type: 2,
					unit_type: 'kost',
					furnished: false
				}
			}
		});

		expect(wrapper.vm.params.filters.rent_type).toBe(2);
		expect(wrapper.vm.params.filters.unit_type).toBe('kost');
		expect(wrapper.vm.params.filters.furnished).toBe(false);

		await wrapper.vm.$store.commit('setAreaData', {
			keyword: 'Jakarta',
			slug: 'kost/area/jakarta',
			center_latitude: -6.123123,
			center_longitude: 6.512515,
			rent_type: 0,
			unit_type: 'apartment',
			is_furnished: true
		});

		expect(wrapper.vm.params.filters.rent_type).toBe(0);
		expect(wrapper.vm.params.filters.unit_type).toBe('apartment');
		expect(wrapper.vm.params.filters.furnished).toBe(true);
	});

	it('Watch apartmentType should handled based on type correctly', async () => {
		await wrapper.setData({ firstLoad: false, apartmentType: 'project' });

		expect(getUnitDataSpy).toBeCalled();

		await wrapper.setData({ apartmentType: 'unit' });

		expect(wrapper.vm.params.page).toBe(1);
	});

	it('Should call method toggleFilterMobile correctly on clicking the button', () => {
		const toggleElement = wrapper.find('button.toggle-filter');

		expect(toggleElement.exists()).toBe(true);

		toggleElement.trigger('click');

		expect(toggleFilterMobileSpy).toBeCalled();
	});

	it('Should call method setFilterUnit correctly on clicking the button', () => {
		const setFilterElement = wrapper.find('button.set-filter');

		expect(setFilterElement.exists()).toBe(true);

		setFilterElement.trigger('click');

		expect(setFilterUnitSpy).toBeCalled();
	});

	it('Should call method goToProject correctly', () => {
		wrapper.vm.goToProject();

		expect(goToProjectSpy).toBeCalled();
		expect(wrapper.vm.apartmentType).toBe('unit');
	});

	it('Should call method toggleApartmentType correctly', () => {
		const toggleUnitElement = wrapper.find('span.track-cari-unit-apt');
		const toggleProjectElement = wrapper.find('span.track-cari-project-apt');

		expect(toggleUnitElement.exists()).toBe(true);
		expect(toggleProjectElement.exists()).toBe(true);

		toggleUnitElement.trigger('click');

		expect(toggleApartmentTypeSpy).toBeCalledWith('unit');
		expect(removeHashSpy).toBeCalledTimes(1);

		toggleProjectElement.trigger('click');

		expect(toggleApartmentTypeSpy).toBeCalledWith('unit');
		expect(removeHashSpy).toBeCalledTimes(1);
	});

	it('Should call pageChanged correctly', async () => {
		await wrapper.setData({ apartmentType: 'unit' });

		const pageChangeElement = wrapper.find('button.page-change');

		expect(pageChangeElement.exists()).toBe(true);

		pageChangeElement.trigger('click');

		expect(pageChangedSpy).toBeCalled();
	});

	it('Should call registerUnit method correctly', async () => {
		delete window.location;
		window.location = { href: jest.fn() };
		const registerElement = wrapper.find('button.track-tambah-unit');

		expect(registerElement.exists()).toBe(true);

		registerElement.trigger('click');

		expect(registerUnitSpy).toBeCalled();
	});

	it('Should call boundsChanged method correctly', async () => {
		const boundsElement = wrapper.find('button.bounds-changed');

		expect(boundsElement.exists()).toBe(true);

		boundsElement.trigger('click');

		expect(boundsChangedSpy).toBeCalled();
	});

	it('Should call clusterUnits method correctly', async () => {
		document.getElementById.scrollIntoView = jest.fn();
		const selectClusterElement = wrapper.find('button.select-cluster');

		expect(selectClusterElement.exists()).toBe(true);

		selectClusterElement.trigger('click');

		expect(clusterUnitsSpy).toBeCalled();
	});

	it('Should call EventBus method correctly', () => {
		const mockEventBus = jest.fn();

		wrapper.setData({
			params: {
				sorting: {
					direction: 'asc'
				},
				landing_source_apartment: ''
			}
		});

		EventBus.$on('addFilterTags', mockEventBus);
		EventBus.$on('addFilterTagsProject', mockEventBus);
		EventBus.$on('sortApartmentList', mockEventBus);
		EventBus.$on('changeFilterPriceMin', mockEventBus);
		EventBus.$on('changeFilterPriceMax', mockEventBus);
		EventBus.$on('changeFilterPrice', mockEventBus);

		EventBus.$emit('addFilterTags');
		EventBus.$emit('addFilterTagsProject');
		EventBus.$emit('sortApartmentList');
		EventBus.$emit('changeFilterPriceMin');
		EventBus.$emit('changeFilterPriceMax');
		EventBus.$emit('changeFilterPrice', {
			min: 10000,
			max: 100000
		});

		expect(mockEventBus).toBeCalledTimes(6);

		wrapper.vm.$store.commit('setAreaData', {
			tag_ids: [1, 2, 3, 5]
		});
	});

	it('Should cover all API function correctly', async () => {
		global.axios = {
			get: jest.fn().mockImplementation(url => {
				if (url === '/stories/input/tags/apartment') {
					return Promise.resolve({
						data: {
							data: {
								unit_type: 'unit mock',
								unit_facilities: [
									{
										name: 'unit facilities 1'
									}
								],
								room_facilities: [
									{
										name: 'unit facilities 1'
									}
								]
							}
						}
					});
				} else if (url === '/apartment/project-tags') {
					return Promise.resolve({
						data: {
							tags: [
								{
									name: 'tags 1'
								}
							]
						}
					});
				}
			}),
			post: jest.fn().mockImplementation(() => {
				return Promise.resolve({
					data: {
						apartment_projects: [],
						rooms: []
					}
				});
			})
		};
		global.$ = jest.fn(() => {
			return {
				width: jest.fn().mockReturnValue(1366)
			};
		});
		translateWords.mockImplementation(() => {
			return [
				{
					name: 'kost 1'
				}
			];
		});
		makeAPICall.mockImplementation(() => {
			return {
				rooms: 'asdadasdas1241',
				apartment_projects: []
			};
		});

		wrapper = shallowMount(LandingArea, {
			localVue,
			store,
			stubs,
			propsData: {
				grid: 'col-md-12'
			}
		});

		await wrapper.setData({
			tags: {
				unit_facilities: [
					{
						tag_id: 1
					}
				]
			}
		});

		wrapper.vm.getUnitTags();

		await wrapper.setData({ apartmentType: 'project' });
		wrapper.vm.getUnitData();
		wrapper.vm.getClusterData();

		expect(showClustersSpy).toBeCalled();
	});

	it('Should cover all API function correctly', async () => {
		global.axios = {
			get: jest.fn().mockResolvedValue({}),
			post: jest.fn().mockImplementation(() => {
				return Promise.reject(new Error('error'));
			})
		};

		wrapper = shallowMount(LandingArea, {
			localVue,
			store,
			stubs,
			propsData: {
				grid: 'col-md-12'
			}
		});
		const selectClusterElement = wrapper.find('button.select-cluster');

		expect(selectClusterElement.exists()).toBe(true);

		selectClusterElement.trigger('click');

		expect(clusterUnitsSpy).toBeCalled();
		expect(bugsnagClient.notify).toBeCalled();

		await wrapper.vm.$store.commit('setAreaData', {
			keyword: 'mock jakarta',
			slug: 'kost/area/jakarta',
			center_latitude: -6.123123,
			center_longitude: 6.512515,
			rent_type: 2,
			unit_type: 'apartment',
			is_furnished: true
		});

		expect(getClusterDataSpy).toBeCalled();
		expect(bugsnagClient.notify).toBeCalled();
	});
});
