import Vuex from 'vuex';
import { createLocalVue, shallowMount } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import mockComponent from 'tests-fe/utils/mock-component';
import LandingDataShare from 'Js/landing/components/LandingDataShare';
import landingUnitStore from './__mocks__/landingUnitStore';

const localVue = createLocalVue();
localVue.use(Vuex);
const store = new Vuex.Store(landingUnitStore);

const stubs = {
	landingChildTitle: mockComponent,
	mamiShare: mockComponent
};

describe('LandingDataShare.vue', () => {
	const wrapper = shallowMount(LandingDataShare, {
		localVue,
		store,
		stubs,
		propsData: {
			grid: 'col-lg-12'
		}
	});

	it('Component should match the snapshots', () => {
		expect(wrapper).toMatchSnapshot();
	});

	it('Computed isMediumDevice should return correctly from store', () => {
		expect(wrapper.vm.isMediumDevice).toBe(false);

		wrapper.vm.$store.commit('updateWindowWidth', 400);

		expect(wrapper.vm.isMediumDevice).toBe(true);
	});
});
