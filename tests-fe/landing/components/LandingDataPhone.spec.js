import Vuex from 'vuex';
import { createLocalVue, shallowMount } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import LandingDataPhone from 'Js/landing/components/LandingDataPhone';
import landingUnitStore from './__mocks__/landingUnitStore';

const localVue = createLocalVue();
localVue.use(Vuex);
const store = new Vuex.Store(landingUnitStore);

const mockData = {
	phones: {
		building: '0817894657',
		marketing: '0817894612',
		other: '0817894655'
	}
};

describe('LandingDataPhone.vue', () => {
	const wrapper = shallowMount(LandingDataPhone, {
		localVue,
		store,
		propsData: {
			grid: 'col-lg-12'
		}
	});

	it('Component should match the snapshots', () => {
		expect(wrapper).toMatchSnapshot();
	});

	it('Computed noPhones should return correctly from store', () => {
		expect(wrapper.vm.noPhones).toBe(true);

		wrapper.vm.$store.commit('setProjectData', mockData);

		expect(wrapper.vm.noPhones).toBe(false);
	});

	it('Computed phones should return correctly from store', () => {
		wrapper.vm.$store.commit('setProjectData', {
			phones: {
				building: '',
				marketing: '',
				other: ''
			}
		});

		expect(wrapper.vm.phones).toBe(wrapper.vm.$store.state.project.phones);

		wrapper.vm.$store.commit('setProjectData', mockData);

		expect(wrapper.vm.$store.state.project.phones).toBe(mockData.phones);
		expect(wrapper.vm.phones).toBe(mockData.phones);
	});
});
