import LandingTitle from 'Js/landing/components/LandingTitle';
import { mount, createLocalVue } from '@vue/test-utils';

const propsData = {
  title: 'Test Page'
};

describe('LandingTitle.vue', () => {
  const localVue = createLocalVue();
  let wrapper = mount(LandingTitle, {
    localVue,
    propsData
  });

  it('should render Landing title properly', () => {
    expect(wrapper.find('.landing-title').exists()).toBeTruthy();
  });
})
