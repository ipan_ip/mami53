import LandingProjectList from 'Js/landing/components/LandingProjectList';
import landingUnitStore from './__mocks__/landingUnitStore';

import 'tests-fe/utils/mock-vue';
import mockComponent from 'tests-fe/utils/mock-component';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';

const localVue = createLocalVue();
localVue.use(Vuex);
const store = new Vuex.Store(landingUnitStore);

const stubs = {
	popover: mockComponent
};

describe('LandingProjectList.vue', () => {
	const wrapper = shallowMount(LandingProjectList, {
		localVue,
		store,
		stubs,
		propsData: {
			projects: [
				{
					name: 'Project 1',
					share_url: 'test',
					photo: [],
					subdisctrict: 'test',
					city: 'Jakarta',
					unit_types: []
				}
			]
		}
	});

	it('should mount component properly', () => {
		expect(wrapper).toMatchSnapshot();
	});

	// getUnitType test functionality
	describe('getUnitType()', () => {
		it('should get unit type list data when mouseover or clicked at project item', async () => {
			const testCases = ['test-1', 'test-2', 'test-3'];
			const expected = '<b>test-1<br>test-2<br>test-3<br></b>';

			let unitTypeList = '';

			await wrapper.setProps({
				projects: [
					{
						name: 'Project 1',
						share_url: 'test',
						photo: [],
						subdisctrict: 'test',
						city: 'Jakarta',
						unit_types: testCases
					}
				]
			});

			expect(wrapper.find('.unit-list-on-desktop').exists()).toBeTruthy();
			const project = wrapper.find('.unit-list-on-desktop');

			project.trigger('mouseover' || 'click');

			unitTypeList = unitTypeList + '<b>';
			testCases.forEach(el => {
				unitTypeList = unitTypeList + el + '<br>';
			});
			unitTypeList = unitTypeList + '</b>';

			expect(unitTypeList).toBe(expected);
		});
	});

	// openDetail test functionality
	describe('openDetail()', () => {
		beforeEach(() => {
			wrapper.setProps({
				projects: [
					{
						name: 'Project 1',
						share_url: 'test',
						photo: [],
						subdisctrict: 'test',
						city: 'Jakarta',
						unit_types: ['test-1', 'test-2']
					}
				]
			});

			mockWindowProperty('location', { href: wrapper.vm.projects.share_url });
		});

		global.open = jest.fn();

		it('should passing parameter link to location.href', () => {
			mockWindowProperty(
				'matchMedia',
				jest.fn(() => {
					return { matches: true };
				})
			);

			wrapper.find('.project-list-container').trigger('click');

			if (matchMedia.matches) {
				location.href = link;
				expect(location.href).toBe(link);
			}
		});

		it('should open page detail in new page', () => {
			mockWindowProperty(
				'matchMedia',
				jest.fn(() => {
					return { matches: null };
				})
			);

			wrapper.find('.project-list-container').trigger('click');

			if (matchMedia.matches == null) {
				expect(global.open).toBeCalled();
			}
		});
	});
});
