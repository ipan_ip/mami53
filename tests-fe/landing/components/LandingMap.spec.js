import Vuex from 'vuex';
import { createLocalVue, shallowMount } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import mockComponent from 'tests-fe/utils/mock-component';
import LandingMap from 'Js/landing/components/LandingMap';
import landingUnitStore from './__mocks__/landingUnitStore';

const localVue = createLocalVue();
localVue.use(Vuex);
const store = new Vuex.Store(landingUnitStore);

const stubs = {
	locationOsm: mockComponent
};

const toggleShowMapSpy = jest.spyOn(LandingMap.methods, 'toggleShowMap');

global.tracker = jest.fn();

describe('LandingMap.vue', () => {
	const wrapper = shallowMount(LandingMap, {
		localVue,
		store,
		stubs
	});

	it('Component should match the snapshots', () => {
		expect(wrapper).toMatchSnapshot();
	});

	it('Component latlng should return correctly from the store', () => {
		expect(wrapper.vm.latlng).toEqual({
			lat: wrapper.vm.$store.state.project.location.latitude,
			lng: wrapper.vm.$store.state.project.location.longitude
		});

		const mockData = {
			...wrapper.vm.$store.state.project,
			location: {
				latitude: -1,
				longitude: 1
			}
		};

		wrapper.vm.$store.commit('setProjectData', mockData);

		expect(wrapper.vm.latlng).toEqual({
			lat: -1,
			lng: 1
		});
	});

	it('Computed projectName should return correctly from store', () => {
		expect(wrapper.vm.projectName).toBe(wrapper.vm.$store.state.project.name);

		const mockData = {
			...wrapper.vm.$store.state.project,
			name: 'mocking name'
		};

		wrapper.vm.$store.commit('setProjectData', mockData);

		expect(wrapper.vm.projectName).toBe(mockData.name);
	});

	it('Computed mapTitle should return correctly', () => {
		expect(wrapper.vm.projectName).toBe(wrapper.vm.$store.state.project.name);
		expect(wrapper.vm.projectName).toBe('mocking name');
		expect(wrapper.vm.mapTitle).toBe(`Peta Lokasi mocking name`);
	});

	it('Should call toggleShowMap method correctly', () => {
		const toggleMapElement = wrapper.find('button.btn-mamigreen');

		expect(toggleMapElement.exists()).toBe(true);

		toggleMapElement.trigger('click');

		expect(toggleShowMapSpy).toBeCalledWith(true);
	});
});
