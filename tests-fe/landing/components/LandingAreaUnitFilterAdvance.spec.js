import Vuex from 'vuex';
import { createLocalVue, shallowMount } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import LandingAreaUnitFilterAdvance from 'Js/landing/components/LandingAreaUnitFilterAdvance';
import LandingAreaStore from './__mocks__/landingAreaStore';
import EventBus from 'Js/landing/event-bus';

const localVue = createLocalVue();
localVue.use(Vuex);
const store = new Vuex.Store(LandingAreaStore);

const checkDeviceTypeSpy = jest.spyOn(
	LandingAreaUnitFilterAdvance.methods,
	'checkDeviceType'
);
const toggleFilterAdvancedSpy = jest.spyOn(
	LandingAreaUnitFilterAdvance.methods,
	'toggleFilterAdvanced'
);

describe('LandingAreaUnitFilterAdvance.vue', () => {
	const wrapper = shallowMount(LandingAreaUnitFilterAdvance, {
		localVue,
		store
	});

	it('Computed tags should return from store', () => {
		expect(wrapper.vm.tags).toEqual(wrapper.vm.$store.state.extras.tags);
	});

	it('Computed tagIds should return correctly', async () => {
		EventBus.$emit = jest.fn();

		expect(wrapper.vm.tagIds).toEqual(
			wrapper.vm.$store.state.extras.tag_ids_project
		);

		await wrapper.setData({ tagIds: [1, 2, 3] });

		expect(EventBus.$emit).toBeCalled();
	});

	it('Watch tagIds should return correctly', async () => {
		await wrapper.setData({ tagIds: [1, 2, 3] });

		expect(wrapper.vm.filterAdvanced).toBe(true);

		await wrapper.setData({ filterAdvanced: false });
	});

	it('Should call checkDeviceType method correctly', async () => {
		expect(checkDeviceTypeSpy).toBeCalled();
		expect(wrapper.vm.filterAdvanced).toBe(false);

		await wrapper.setProps({ deviceType: 'mobile' });
		wrapper.vm.checkDeviceType();

		expect(wrapper.vm.filterAdvanced).toBe(true);
	});

	it('Should call toggleFilterAdvanced method on clicking the button', async () => {
		const toggleElement = wrapper.find('button.btn-advanced-filter');

		expect(toggleElement.exists()).toBe(true);

		await toggleElement.trigger('click');

		expect(toggleFilterAdvancedSpy).not.toBeCalled();

		await wrapper.vm.$store.commit('setUnitTags', [1, 2, 3]);
		await toggleElement.trigger('click');

		expect(toggleFilterAdvancedSpy).toBeCalled();
	});
});
