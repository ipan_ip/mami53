import 'tests-fe/utils/mock-vue';
import LandingUnit from 'Js/landing/components/LandingUnit';
import LandingUnitStore from './__mocks__/landingUnitStore';

import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import mockAxios from 'tests-fe/utils/mock-axios';
import EventBus from 'Js/_detail/event-bus'
import mockComponent from 'tests-fe/utils/mock-component';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

// mocks component
const paginate = {
	template: `<button class="page-change" @click="clickHandler(2)"/>`,
	props: ['clickHandler']
};
const FilterMobileButton = {
  import: jest.fn(),
  template: '<button class="btn btn-mamigreen btn-filter" @click="openFilterMobile(true)"> FILTER & URUTKAN </button>',
};

global.bugsnagClient = {
  notify: jest.fn()
};
global.swal = jest.fn();

global.axios = mockAxios;



window.Vue = require('vue');
const localVue = createLocalVue();
localVue.use(Vuex);

const propsData = {
    rooms: [
      {
        "id": 3583,
        "name": "1",
        "floor": "1",
        "occupied": false,
        "disable": false
      },
      {
        "id": 3584,
        "name": "2",
        "floor": "1",
        "occupied": false,
        "disable": false
      } 
    ]
  };

const mountData = {
  localVue,
  mocksData: {
    tags: {
      unit_type: [],
      unit_facilities: []
    }
  },
  stubs: {
    paginate,
    FilterMobileButton,
    mamiLoadingList: mockComponent,
    listPlaceholder: mockComponent
  },
  propsData,
  store: new Vuex.Store(LandingUnitStore)
};

const mount = (adtMountData = {}) => {
  const finalData = { ...mountData, ...adtMountData};

  return shallowMount(LandingUnit, finalData);
};
  
describe('LandingUnit.vue', () => {
  let wrapper;
  jest.useFakeTimers();

  delete window.location;
  window.location = { reload: jest.fn() };
  window.alert = jest.fn();
  mockWindowProperty('scrollTo', jest.fn())

  mockAxios.mockResolve({
      data: {
      rooms: [],
      loadingUnit: true,
      unit: [],
      showPagination: jest.fn()
    }
  });
  mockAxios.mockReject({
    bugsnagClient
  });

  beforeEach(() => {
    wrapper = mount();
  });

  it('should mount properly', () => {
    expect(wrapper.find('.landing-section').exists()).toBeTruthy();
  });
  
  it('should get value of authCheck from store', () => {
    expect(wrapper.vm.authCheck).toBeTruthy();
  })

  it('should return total unit of page', () => {
    wrapper.setData({unit: { rooms: [], total: 50, limit: 3, offset: 0 }})
    expect(wrapper.vm.totalPageUnit).toBe(10);
  });
  
  it('should handle filter mobile', () => {
    wrapper.vm.toggleFilterMobile();
  });

  it('should set filter unit', () => {
    const getUnitData = jest.spyOn(LandingUnit.methods, 'getUnitData');
    wrapper.vm.setFilterUnit();

    expect(wrapper.vm.isFilterMobile).toBeFalsy();
  });

  it('should go to top when page changed', () => {
    wrapper.vm.gotoTop();

    jest.runAllTimers();
    expect(window.scrollTo).toBeCalledWith(0, top = 0);
  });

  it('should emit open modal login Fav when toggle login modal clicked', () => {
    wrapper.vm.toggleLoginModal();
    EventBus.$emit = jest.fn();

    expect(EventBus.$emit).not.toBeCalled();
  });

  it('should run change page function', () => {
    
    wrapper.vm.pageChanged();

    expect(wrapper.vm.getUnitData).toBeTruthy();
    expect(wrapper.vm.gotoTop).toBeTruthy();
    expect(wrapper.vm.getUnitCount).toBeTruthy();
    expect(wrapper.vm.getUnitLoaded).toBeTruthy();
  });

  it('should handle unit registration redirect or alert', () => {
    wrapper.vm.registerUnit();

    if (wrapper.vm.$store.state.authCheck.user) {
      expect(global.swal).toBeCalled();
    } else if (wrapper.vm.$store.state.authCheck.owner) {
      expect(window.location.href).toBe('/ownerpage/add')
      w
    }
  });

  it('should run axios properly', () => {
    mockAxios.mockResolve({
        data: {
        rooms: [],
        loadingUnit: true,
        unit: [],
        showPagination: jest.fn()
      }
    });
  });

  it('should call bugsnag when failed get/post axios process', async () => {
    mockAxios.mockReject('error');

    expect(bugsnagClient.notify).toBeCalled();
    expect(global.alert).toBeCalled();
  });

  it('should change filter page', () => {
    wrapper.vm.changeFilters();
    EventBus.$on('changeFilter', payload => {
      if (window.innerWidth > 767) {
        expect(wrapper.vm.pageChanged(1)).toBeCalled();
      }
    });
  });
});
