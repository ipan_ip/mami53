import Vuex from 'vuex';
import { createLocalVue, shallowMount } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import mockComponent from 'tests-fe/utils/mock-component';
import LandingDataTower from 'Js/landing/components/LandingDataTower';
import landingUnitStore from './__mocks__/landingUnitStore';

const localVue = createLocalVue();
localVue.use(Vuex);
const store = new Vuex.Store(landingUnitStore);

const stubs = {
	landingChildTitle: mockComponent
};

describe('LandingDataTower.vue', () => {
	const wrapper = shallowMount(LandingDataTower, {
		localVue,
		store,
		stubs,
		propsData: {
			grid: 'col-lg-12'
		}
	});

	it('Component should match the snapshots', () => {
		expect(wrapper).toMatchSnapshot();
	});

	it('Computed towers should return correctly from store', () => {
		expect(wrapper.vm.towers).toEqual(wrapper.vm.$store.state.project.towers);

		const mockData = {
			towers: [
				{
					name: 'tower 1',
					status: true
				},
				{
					name: 'tower 2',
					status: false
				}
			]
		};
		wrapper.vm.$store.commit('setProjectData', mockData);

		expect(wrapper.vm.towers).toEqual(mockData.towers);
	});
});
