import LandingSwitch from 'Js/landing/components/LandingSwitch';

import { shallowMount, createLocalVue } from '@vue/test-utils';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

const localVue = createLocalVue();

const mocksData = {
  landingSelector: 'landing_apartment',
  isAnimating: true,
};

const mountData = {
  localVue,
  mocksData,
};

describe('LandingSwitch.vue', () => {
  const wrapper = shallowMount(LandingSwitch, {
    propsData: {
      landingConnector: [],
      keywordSelector: 'landing-apartment'
    },
    mountData
  });

  it('should mount component properly', () => {
    expect(wrapper.find('.track-switch-category').exists()).toBeTruthy();
  });

  it('should update newData to landingSelector', () => {
		mockWindowProperty('window.location', jest.fn());
    wrapper.setData({ landingSelector: 'landing_goldplus' });
    wrapper.vm.$nextTick();

    expect(wrapper.vm.landingSelector).toBe('landing_goldplus');
  });
});