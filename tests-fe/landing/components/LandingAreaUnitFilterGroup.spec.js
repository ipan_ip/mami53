import Vuex from 'vuex';
import { createLocalVue, shallowMount } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import LandingAreaUnitFilterGroup from 'Js/landing/components/LandingAreaUnitFilterGroup';
import LandingAreaStore from './__mocks__/landingAreaStore';
import EventBus from 'Js/landing/event-bus';

const localVue = createLocalVue();
localVue.use(Vuex);
const store = new Vuex.Store(LandingAreaStore);

const filterPriceRangeSpy = jest.spyOn(
	LandingAreaUnitFilterGroup.methods,
	'filterPriceRange'
);
const changePriceMinSpy = jest.spyOn(
	LandingAreaUnitFilterGroup.methods,
	'changePriceMin'
);
const changePriceMaxSpy = jest.spyOn(
	LandingAreaUnitFilterGroup.methods,
	'changePriceMax'
);

const filterPrice = {
	template: `<div><button @click="$emit('filterPrice', 500)" class="filter-price"/><button @click="$emit('filterPriceMin', 500)" class="filter-price-min"/><button @click="$emit('filterPriceMax', 1000)" class="filter-price-max"/></div>`
};

describe('LandingAreaUnitFilterGroup.vue', () => {
	const wrapper = shallowMount(LandingAreaUnitFilterGroup, {
		localVue,
		store,
		propsData: {
			tags: {
				unit_type: []
			}
		},
		stubs: {
			filterPrice
		}
	});

	it('Computed area should return from store', () => {
		expect(wrapper.vm.area).toEqual(wrapper.vm.$store.state.area);
	});

	it('Watch sorting direction should handled correctly', async () => {
		EventBus.$emit = jest.fn();
		await wrapper.setData({
			sorting: {
				fields: 'price',
				direction: 'asc'
			}
		});

		expect(EventBus.$emit).toBeCalledWith('sortApartmentList', 'asc');
	});

	it('Should call filterPriceRange method when changing the price correctly', async () => {
		const filterPriceElement = wrapper.find('button.filter-price');

		expect(filterPriceElement.exists()).toBe(true);

		await filterPriceElement.trigger('click');

		expect(filterPriceRangeSpy).toBeCalled();
		expect(EventBus.$emit).toBeCalledWith('changeFilterPrice', 500);
	});

	it('Should call filterPriceMin method when changing the price correctly', async () => {
		const filterPriceMinElement = wrapper
			.findAll('button.filter-price-min')
			.at(1);

		expect(filterPriceMinElement.exists()).toBe(true);

		await filterPriceMinElement.trigger('click');

		expect(changePriceMinSpy).toBeCalled();
		expect(EventBus.$emit).toBeCalledWith('changeFilterPriceMin', 500);
	});

	it('Should call filterPriceMax method when changing the price correctly', async () => {
		const filterPriceMaxElement = wrapper
			.findAll('button.filter-price-max')
			.at(1);

		expect(filterPriceMaxElement.exists()).toBe(true);

		await filterPriceMaxElement.trigger('click');

		expect(changePriceMaxSpy).toBeCalled();
		expect(EventBus.$emit).toBeCalledWith('changeFilterPriceMax', 1000);
	});
});
