import Vuex from 'vuex';
import { createLocalVue, shallowMount } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import LandingChildTitle from 'Js/landing/components/LandingChildTitle';
import LandingAreaStore from './__mocks__/landingAreaStore';

const handleResizeSpy = jest.spyOn(LandingChildTitle.methods, 'handleResize');
const toggleCollapseSpy = jest.spyOn(
	LandingChildTitle.methods,
	'toggleCollapse'
);

const localVue = createLocalVue();
localVue.use(Vuex);
const store = new Vuex.Store(LandingAreaStore);

describe('LandingChildTitle.vue', () => {
	const wrapper = shallowMount(LandingChildTitle, {
		propsData: {
			grid: 'col-md-12',
			title: 'Kost Jakarta',
			collapseId: '1'
		},
		localVue,
		store
	});

	it('Component should match the snapshot', () => {
		expect(wrapper).toMatchSnapshot();
	});

	it('Should call handleResize method correctly', async () => {
		wrapper.vm.handleResize();

		expect(handleResizeSpy).toBeCalled();
		expect(wrapper.vm.isCollapsed).toBe(true);
	});

	it('Should call toggleCollapse method correctly', async () => {
		const toggleElement = wrapper.findAll('div.child-title').at(1);

		expect(toggleElement.exists()).toBe(true);

		await toggleElement.trigger('click');

		expect(toggleCollapseSpy).toBeCalled();
	});
});
