import Vuex from 'vuex';
import { createLocalVue, shallowMount } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import LandingAreaUnit from 'Js/landing/components/LandingAreaUnit';
import LandingAreaStore from './__mocks__/landingAreaStore';
import EventBus from 'Js/@global/event-bus';

const localVue = createLocalVue();
localVue.use(Vuex);
const store = new Vuex.Store(LandingAreaStore);

const pageChangedSpy = jest.spyOn(LandingAreaUnit.methods, 'pageChanged');
const toggleLoginModalSpy = jest.spyOn(
	LandingAreaUnit.methods,
	'toggleLoginModal'
);

const paginate = {
	template: `<button class="page-change" @click="clickHandler(2)"/>`,
	props: ['clickHandler']
};
const landingUnitList = {
	template: `<button class="toggle-login" @click="$emit('toggleLoginModal')"/>`
};

describe('LandingAreaUnit.vue', () => {
	const wrapper = shallowMount(LandingAreaUnit, {
		localVue,
		store,
		stubs: {
			paginate,
			landingUnitList
		},
		propsData: {
			unit: {
				total: 100,
				limit: 20,
				rooms: []
			},
			params: {
				page: 5
			},
			loadingUnit: false,
			unitCount: 100,
			unitLoaded: 20,
			tags: {
				unit_facilities: [],
				project_facilities: []
			}
		}
	});

	it('Computed landingTitle should return from store', () => {
		expect(wrapper.vm.landingTitle).toEqual(
			wrapper.vm.$store.state.area.keyword
		);
	});

	it('Computed totalPage should calculate unit total divide the unit limit with max 10 page', async () => {
		expect(wrapper.vm.totalPage).toBe(5);

		await wrapper.setProps({
			unit: {
				total: 200,
				limit: 10,
				rooms: [
					{
						name: 'kost 1'
					}
				]
			}
		});

		expect(wrapper.vm.totalPage).toBe(10);
	});

	it('Computed totalPageAll should calculate unit total divide the unit limit', () => {
		expect(wrapper.vm.totalPageAll).toBe(20);
	});

	it('Should call pageChanged method on clicking the button', async () => {
		const pageChangeElement = wrapper.find('button.page-change');

		expect(pageChangeElement.exists()).toBe(true);

		await pageChangeElement.trigger('click');

		expect(pageChangedSpy).toBeCalledWith(2);
		expect(wrapper.emitted('pageChanged')).toBeTruthy();
	});

	it('Should call toggleLoginModal method on clicking the button', async () => {
		EventBus.$emit = jest.fn();

		const toggleLoginElement = wrapper.find('button.toggle-login');

		expect(toggleLoginElement.exists()).toBe(true);

		await toggleLoginElement.trigger('click');

		expect(toggleLoginModalSpy).toBeCalled();
		expect(EventBus.$emit).toBeCalled();
	});
});
