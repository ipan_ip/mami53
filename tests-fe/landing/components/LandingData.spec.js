import Vuex from 'vuex';
import { createLocalVue, shallowMount } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import mockComponent from 'tests-fe/utils/mock-component';
import LandingData from 'Js/landing/components/LandingData';
import landingUnitStore from './__mocks__/landingUnitStore';

const localVue = createLocalVue();
localVue.use(Vuex);
const store = new Vuex.Store(landingUnitStore);

const stubs = {
	landingPhoto: mockComponent,
	landingAvailable: mockComponent,
	landingDataAddress: mockComponent,
	landingDataPhone: mockComponent,
	landingDataFacility: mockComponent,
	landingDataInfo: mockComponent,
	landingDataType: mockComponent,
	landingDataTower: mockComponent,
	landingDataShare: mockComponent
};

describe('LandingData.vue', () => {
	const wrapper = shallowMount(LandingData, {
		localVue,
		store,
		stubs
	});

	it('Component should match the snapshot', () => {
		expect(wrapper).toMatchSnapshot();
	});

	it('Computed landingPhotos should return correctly based on photo length', async () => {
		expect(wrapper.vm.landingPhotos).toEqual(
			wrapper.vm.$store.state.extras.photosFallback
		);

		wrapper.vm.$store.commit('setProjectData', {
			photos: [
				{
					large: 'test.jpg'
				}
			]
		});

		expect(wrapper.vm.landingPhotos).toEqual(
			wrapper.vm.$store.state.project.photos
		);
	});

	it('Computed landingAvailableUnit should return from the store', async () => {
		expect(wrapper.vm.landingAvailableUnit).toEqual(
			wrapper.vm.$store.state.project.unit_available
		);
	});

	it('Computed landingDescription should return from the store', async () => {
		expect(wrapper.vm.landingDescription).toEqual(
			wrapper.vm.$store.state.project.description
		);
	});

	it('Computed landingTotalUnit should return from the store', async () => {
		expect(wrapper.vm.landingDescription).toEqual(
			wrapper.vm.$store.state.project.unit_total
		);
	});
});
