import Vuex from 'vuex';
import { createLocalVue, shallowMount } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import mockComponent from 'tests-fe/utils/mock-component';
import LandingDataInfo from 'Js/landing/components/LandingDataInfo';
import landingUnitStore from './__mocks__/landingUnitStore';

const localVue = createLocalVue();
localVue.use(Vuex);
const store = new Vuex.Store(landingUnitStore);

const stubs = {
	landingChildTitle: mockComponent
};

describe('LandingDataInfo.vue', () => {
	const wrapper = shallowMount(LandingDataInfo, {
		localVue,
		store,
		stubs,
		propsData: {
			grid: 'col-lg-12'
		}
	});

	it('Component should match the snapshots', () => {
		expect(wrapper).toMatchSnapshot();
	});

	it('Computed developer should return correctly from store', () => {
		expect(wrapper.vm.developer).toBe(
			wrapper.vm.$store.state.project.developer
		);

		const mockData = {
			developer: 'mock developer'
		};
		wrapper.vm.$store.commit('setProjectData', mockData);

		expect(wrapper.vm.$store.state.project.developer).toBe(mockData.developer);
		expect(wrapper.vm.developer).toBe(mockData.developer);
	});

	it('Computed sizeString should return correctly from store', () => {
		expect(wrapper.vm.sizeString).toBe(
			wrapper.vm.$store.state.project.size_string
		);

		const mockData = {
			size_string: 'mock size string'
		};
		wrapper.vm.$store.commit('setProjectData', mockData);

		expect(wrapper.vm.$store.state.project.size_string).toBe(
			mockData.size_string
		);
		expect(wrapper.vm.sizeString).toBe(mockData.size_string);
	});

	it('Computed unitCount should return correctly from store', () => {
		expect(wrapper.vm.unitCount).toBe(
			wrapper.vm.$store.state.project.unit_count
		);

		const mockData = {
			unit_count: 6
		};
		wrapper.vm.$store.commit('setProjectData', mockData);

		expect(wrapper.vm.$store.state.project.unit_count).toBe(
			mockData.unit_count
		);
		expect(wrapper.vm.unitCount).toBe(mockData.unit_count);
	});

	it('Computed floorsCount should return correctly from store', () => {
		expect(wrapper.vm.floorsCount).toBe(
			wrapper.vm.$store.state.project.floors_count
		);

		const mockData = {
			floors_count: 2
		};
		wrapper.vm.$store.commit('setProjectData', mockData);

		expect(wrapper.vm.$store.state.project.floors_count).toBe(
			mockData.floors_count
		);
		expect(wrapper.vm.floorsCount).toBe(mockData.floors_count);
	});
});
