import Vuex from 'vuex';
import { createLocalVue, shallowMount } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import mockComponent from 'tests-fe/utils/mock-component';
import LandingDataFacility from 'Js/landing/components/LandingDataFacility';
import landingUnitStore from './__mocks__/landingUnitStore';

const localVue = createLocalVue();
localVue.use(Vuex);
const store = new Vuex.Store(landingUnitStore);

const stubs = {
	landingChildTitle: mockComponent
};

describe('LandingDataFacility.vue', () => {
	const wrapper = shallowMount(LandingDataFacility, {
		localVue,
		store,
		stubs,
		propsData: {
			grid: 'col-lg-12'
		}
	});

	it('Component should match the snapshots', () => {
		expect(wrapper).toMatchSnapshot();
	});

	it('Computed facilities should return correctly from store', () => {
		expect(wrapper.vm.facilities).toEqual(
			wrapper.vm.$store.state.project.facilities
		);

		const mockData = {
			facilities: [
				{
					photo_url: 'img/mock-1',
					name: 'mock-1'
				}
			]
		};
		wrapper.vm.$store.commit('setProjectData', mockData);

		expect(wrapper.vm.$store.state.project.facilities).toEqual(
			mockData.facilities
		);
		expect(wrapper.vm.facilities).toEqual(mockData.facilities);
	});
});
