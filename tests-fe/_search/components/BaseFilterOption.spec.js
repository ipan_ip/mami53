import { createLocalVue, shallowMount } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import BaseFilterOption from 'Js/@components/BaseFilterOption.vue';
import Vuex from 'vuex';
import SearchStore from '../__mocks__/searchStore';

const localVue = createLocalVue();
localVue.use(Vuex);
const store = new Vuex.Store(SearchStore);

describe('BaseFilterOption.vue', () => {
	const wrapper = shallowMount(BaseFilterOption, {
		localVue,
		store,
		propsData: {
			optionData: [
				{
					id: 1,
					value: 1,
					name: 'Mingguan',
					fac_event: 'filter_period_mingguan'
				},
				{
					id: 2,
					value: 2,
					name: 'Bulanan',
					fac_event: 'filter_period_bulanan'
				},
				{
					id: 4,
					value: 4,
					name: '3 Bulan',
					fac_event: 'filter_period_3_bulan'
				},
				{
					id: 5,
					value: 5,
					name: 'Tahunan',
					fac_event: 'filter_period_6_bulan'
				},
				{
					id: 3,
					value: 3,
					name: 'Tahunan',
					fac_event: 'filter_period_tahunan'
				}
			],
			optionValue: 2
		}
	});

	it('Computed valueBinding should return correctly', async () => {
		expect(wrapper.vm.valueBinding).toEqual(2);

		const inputElement = wrapper.findAll('input');

		expect(inputElement.at(1).element.checked).toBeTruthy();
		expect(inputElement.length).toBe(5);
	});

	it('Should not list available option if optionData is null', () => {
		const wrapper = shallowMount(BaseFilterOption, {
			localVue,
			store,
			propsData: {
				optionValue: 2
			}
		});

		expect(wrapper.vm.optionData).toEqual([]);

		const inputElement = wrapper.findAll('input');

		expect(inputElement.length).toBe(0);
	});
});
