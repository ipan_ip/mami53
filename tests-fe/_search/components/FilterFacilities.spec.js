import { createLocalVue, shallowMount } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import FilterFacilities from 'Js/_search/components/FilterFacilities.vue';
import Vuex from 'vuex';
import SearchStore from '../__mocks__/searchStore';

const localVue = createLocalVue();
localVue.use(Vuex);
const store = new Vuex.Store(SearchStore);

describe('FilterFacilities.vue', () => {
	const wrapper = shallowMount(FilterFacilities, {
		localVue,
		store
	});

	it('Computed valueBinding should return correctly', async () => {
		await wrapper.setProps({
			facilitiesData: {
				title: 'Fasilitas',
				data: [
					{
						fac_id: 13,
						value: 13,
						name: 'AC',
						fac_icon_url:
							'https://static3.mamikos.com/uploads/tags/GrTE2miP.png',
						fac_event: 'fac_13'
					},
					{
						fac_id: 12,
						value: 12,
						name: 'TV',
						fac_icon_url:
							'https://static3.mamikos.com/uploads/tags/GrTE2miP.png',
						fac_event: 'fac_12'
					}
				],
				getValue: [13],
				active: true
			}
		});
		expect(wrapper.vm.valueBinding).toEqual([13]);
	});
});
