import { createLocalVue, shallowMount } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import MobileMap from 'Js/_search/components/MobileMap.vue';
import Vuex from 'vuex';
import SearchStore from '../__mocks__/searchStore';

const localVue = createLocalVue();
localVue.use(Vuex);
const store = new Vuex.Store(SearchStore);

const handlePassContentTypeSpy = jest.spyOn(
	MobileMap.methods,
	'handlePassContentType'
);

const SearchMap = {
	template: `<button class="pass-content" @click="$emit('handle-pass-content-type', 'map')" />`
};

const stubs = {
	SearchMap
};

const propsData = {
	contentType: 'map'
};

global.innerWidth = 400;
global.document.querySelector = jest.fn(() => {
	return {
		style: {
			height: 'auto'
		}
	};
});

describe('MobileMap.vue', () => {
	const wrapper = shallowMount(MobileMap, {
		localVue,
		store,
		stubs,
		propsData
	});

	it('Should handle method handlePassContentType correctly when user change the content type in mobile', async () => {
		const changeContentElement = wrapper.find('button.pass-content');

		expect(changeContentElement.exists()).toBe(true);

		changeContentElement.trigger('click');

		expect(handlePassContentTypeSpy).toBeCalledWith('map');
		expect(wrapper.emitted('handle-change-content-type')).toBeTruthy();
	});
});
