import { createLocalVue, shallowMount } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import ModalSortingMobile from 'Js/_search/components/ModalSortingMobile.vue';
import MixinFilterHelper from 'Js/_search/mixins/MixinFilterHelper';
import PickABooSelect from 'Js/@components/PickABooSelectModal';
import Vuex from 'vuex';
import SearchStore from '../__mocks__/searchStore';
const localVue = createLocalVue();

mockWindowProperty('tracker', jest.fn());
mockWindowProperty('window', {
	scrollTo: jest.fn()
});

localVue.mixin([MixinFilterHelper]);
localVue.use(Vuex);
const store = new Vuex.Store(SearchStore);

const handleApplySortingSpy = jest.spyOn(
	ModalSortingMobile.methods,
	'handleApplySorting'
);

const handleSetSortingSpy = jest.spyOn(
	ModalSortingMobile.methods,
	'handleSetSorting'
);

const handleResetSortingSpy = jest.spyOn(
	ModalSortingMobile.methods,
	'handleResetSorting'
);

const baseFilterOptionStub = {
	template: `<button class="update-sort" @click="$emit('set-value', ['price','desc'])"/>`
};

describe('ModalSortingMobile.vue', () => {
	const wrapper = shallowMount(ModalSortingMobile, {
		localVue,
		store,
		stubs: {
			'base-filter-option': baseFilterOptionStub,
			'modal-sorting-mobile': PickABooSelect
		}
	});

	it('Watch isShow should return correctly', async () => {
		const payload = ['price', 'asc'];
		wrapper.vm.$store.commit('setSort', payload);
		await wrapper.setProps({ isShow: true });

		expect(wrapper.vm.sorting).toEqual(payload);

		await wrapper.setProps({ isShow: false });

		expect(wrapper.vm.sorting).toEqual(payload);
	});

	it('Should call method handleSetSorting correctly ', async () => {
		await wrapper.setProps({ isShow: true });

		const sortingElement = wrapper.find('button.update-sort');

		expect(sortingElement.exists()).toBe(true);

		await sortingElement.trigger('click');

		expect(handleSetSortingSpy).toBeCalledWith(['price', 'desc']);
	});

	it('Should call method handleResetSorting correctly', async () => {
		const resetElement = wrapper.find('button.search-btn-reset');

		expect(resetElement.exists()).toBe(true);

		await resetElement.trigger('click');

		expect(handleResetSortingSpy).toBeCalled();
	});

	it('Should call method handleApplySorting correctly', async () => {
		const saveElement = wrapper.find('button.search-btn-save');

		expect(saveElement.exists()).toBe(true);

		await saveElement.trigger('click');

		expect(handleApplySortingSpy).toBeCalled();
		expect(wrapper.emitted('toggle-modal-handler')).toBeTruthy();
	});
});
