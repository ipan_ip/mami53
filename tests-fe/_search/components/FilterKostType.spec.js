import { createLocalVue, shallowMount } from '@vue/test-utils';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import 'tests-fe/utils/mock-vue';
import FilterKostType from 'Js/_search/components/FilterKostType.vue';
import Vuex from 'vuex';
import SearchStore from '../__mocks__/searchStore';
const mixinFilterHelper = require('Js/_search/mixins/MixinFilterHelper');
const localVue = createLocalVue();
localVue.mixin([mixinFilterHelper]);
localVue.use(Vuex);
const store = new Vuex.Store(SearchStore);

mockWindowProperty('axios', {
	get: jest.fn(() => {
		return Promise.resolve({
			data: {
				datas: [
					{
						is_running: true,
						is_finished: false
					}
				],
				total_running: 1
			}
		});
	})
});
mockWindowProperty('bugsnagClient', {
	notify: jest.fn()
});
mockWindowProperty('tracker', jest.fn());

const checkRunningFlashsaleSpy = jest.spyOn(
	FilterKostType.methods,
	'checkRunningFlashsale'
);

const checkFtueSpy = jest.spyOn(FilterKostType.methods, 'checkFtue');

const flashSaleTrackerSpy = jest.spyOn(
	FilterKostType.methods,
	'flashSaleTracker'
);

const bookingLangsungCallbackSpy = jest.spyOn(
	FilterKostType.methods,
	'bookingLangsungCallback'
);

const flashSaleCallbackSpy = jest.spyOn(
	FilterKostType.methods,
	'flashSaleCallback'
);

const setFilterKostTypeSpy = jest.spyOn(
	FilterKostType.methods,
	'setFilterKostType'
);

const setSortingSpy = jest.spyOn(FilterKostType.methods, 'setSorting');

const BaseFtueStub = {
	template: `
  <div>
    <button class="set-value-flashsale" @click="$emit('set-value', true, 'flash_sale')" />
     <button class="set-value-others" @click="$emit('set-value', true, 'booking')" />
    <button class="callback-flashsale" @click="$emit('callback-on-understand')" />
    <button class="callback-hide" @click="$emit('callback-on-hide')" />
  </div>`
};

const BaseMainFilterStub = {
	template: `<button class="set-sorting" @click="$emit('set-value', ['price','desc'])" />`
};

const stubs = {
	'base-ftue': BaseFtueStub,
	'base-main-filter': BaseMainFilterStub
};

describe('FilterKostType.vue', () => {
	let wrapper = shallowMount(FilterKostType, {
		localVue,
		store,
		stubs
	});

	it('Computed flashsale should return correctly', () => {
		expect(wrapper.vm.flashsale).toEqual({
			title: 'Promo Ngebut',
			getValue: false,
			active: false,
			link: '/promo-ngebut'
		});
	});

	it('Computed bookingFtueCondition should return correctly', () => {
		expect(wrapper.vm.bookingFtueCondition).toBe(false);

		wrapper.vm.$store.commit('setEmptyList', false);
		wrapper.vm.$store.commit('setRooms', {
			status: true,
			rooms: [1],
			total: 1
		});

		expect(wrapper.vm.bookingFtueCondition).toBe(false);
	});

	it('Computed flashSaleId should return correctly', async () => {
		expect(wrapper.vm.flashSaleId).toEqual(0);

		await wrapper.setData({
			haveFlashSale: {
				id: 1
			}
		});

		expect(wrapper.vm.flashSaleId).toEqual(1);
	});

	it('Computed flashSaleFtueCondition should return correctly', async () => {
		expect(wrapper.vm.flashSaleFtueCondition).toBe(false);

		await wrapper.setData({ currentFtue: 'flashSale' });

		expect(wrapper.vm.flashSaleFtueCondition).toBe(true);
	});

	it('Should call checkRunningFlashsale method correctly', () => {
		expect(checkRunningFlashsaleSpy).toBeCalled();
		expect(wrapper.vm.haveFlashSale).toEqual({
			id: 1,
			is_running: true,
			is_finished: false
		});
		expect(checkFtueSpy).toBeCalled();
	});

	it('Should call checkRunningFlashsale with no running data correctly', () => {
		mockWindowProperty('axios', {
			get: jest.fn(() => {
				return Promise.resolve({
					data: {
						total_running: 0
					}
				});
			})
		});

		wrapper = shallowMount(FilterKostType, {
			localVue,
			store,
			stubs
		});

		expect(checkRunningFlashsaleSpy).toBeCalled();
		expect(wrapper.vm.haveFlashSale).toEqual(null);
		expect(checkFtueSpy).toBeCalled();
	});

	it('Should call checkRunningFlashsale with false response', async () => {
		mockWindowProperty('axios', {
			get: jest.fn(() => {
				return Promise.reject(new Error('error response'));
			})
		});

		wrapper = shallowMount(FilterKostType, {
			localVue,
			store,
			stubs
		});

		expect(checkRunningFlashsaleSpy).toBeCalled();
		expect(wrapper.vm.haveFlashSale).toEqual(null);
		expect(checkFtueSpy).toBeCalled();
	});

	it('Should call checkFtue method correctly', async () => {
		wrapper = shallowMount(FilterKostType, {
			localVue,
			store,
			stubs,
			data() {
				return {
					haveFlashSale: {
						id: 2
					}
				};
			}
		});

		expect(checkRunningFlashsaleSpy).toBeCalled();
		expect(checkFtueSpy).toBeCalled();
	});

	it('Should call flashSaleTracker method correctly', async () => {
		const testCase = [
			{
				isMobile: true
			},
			{
				isMobile: false
			}
		];

		for (const test in testCase) {
			wrapper = shallowMount(FilterKostType, {
				localVue,
				store,
				stubs,
				computed: {
					isMobile() {
						return test.isMobile;
					}
				}
			});

			await wrapper.setData({ haveFlashSale: true });
			const callbackUnderstandElement = wrapper.find(
				'button.callback-flashsale'
			);

			expect(callbackUnderstandElement.exists()).toBe(true);

			await callbackUnderstandElement.trigger('click');

			expect(flashSaleTrackerSpy).toBeCalled();
		}
	});

	it('Should call flashSaleCallback method correctly', async () => {
		await wrapper.setData({ haveFlashSale: true });
		const callbackHideElement = wrapper.find('button.callback-hide');

		expect(callbackHideElement.exists()).toBe(true);

		await callbackHideElement.trigger('click');

		expect(flashSaleCallbackSpy).toBeCalled();
	});

	it('Should call bookingLangsungCallback method correctly', async () => {
		await wrapper.setData({ haveFlashSale: null });
		const callbackHideElement = wrapper.find('button.callback-hide');

		expect(callbackHideElement.exists()).toBe(true);

		await callbackHideElement.trigger('click');

		expect(bookingLangsungCallbackSpy).toBeCalled();
	});

	it('Should call setFilterKostType method correctly', async () => {
		await wrapper.setData({ haveFlashSale: true });
		const setValueFlashsaleElement = wrapper.find('button.set-value-flashsale');
		const setValueOthersElement = wrapper.find('button.set-value-others');

		expect(setValueFlashsaleElement.exists()).toBe(true);
		expect(setValueOthersElement.exists()).toBe(true);

		await setValueFlashsaleElement.trigger('click');

		expect(setFilterKostTypeSpy).toBeCalledWith(true, 'flash_sale');

		await setValueOthersElement.trigger('click');

		expect(setFilterKostTypeSpy).toBeCalledWith(true, 'booking');
	});

	it('Should call setSorting method correctly', async () => {
		wrapper = shallowMount(FilterKostType, {
			localVue,
			store,
			stubs,
			computed: {
				isMobile() {
					return false;
				}
			}
		});
		const setSortingElement = wrapper.findAll('button.set-sorting').at(3);
		expect(setSortingElement.exists()).toBe(true);

		await setSortingElement.trigger('click');

		expect(setSortingSpy).toBeCalled();
	});

	it('Should call setFilterKostType method with goldplus correctly', async () => {
		const testCases = [
			{
				status: true
			},
			{
				status: false
			}
		];

		for (const test of testCases) {
			const BaseMainFilterStub = {
				template: `<button class="set-filter-goldplus" @click="$emit('set-value', ${test.status}, 'goldplus')" />`
			};

			const stubs = {
				'base-ftue': BaseFtueStub,
				'base-main-filter': BaseMainFilterStub
			};

			wrapper = shallowMount(FilterKostType, {
				localVue,
				store,
				stubs
			});

			const filterGoldPlusElement = wrapper.find('button.set-filter-goldplus');

			expect(filterGoldPlusElement.exists()).toBe(true);

			await filterGoldPlusElement.trigger('click');

			expect(setFilterKostTypeSpy).toBeCalledWith(true, 'goldplus');
		}
	});
});
