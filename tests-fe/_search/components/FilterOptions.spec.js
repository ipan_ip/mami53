import { createLocalVue, shallowMount } from '@vue/test-utils';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import 'tests-fe/utils/mock-vue';
import FilterOptions from 'Js/_search/components/FilterOptions.vue';
import Vuex from 'vuex';
import SearchStore from '../__mocks__/searchStore';
const mixinFilterHelper = require('Js/_search/mixins/MixinFilterHelper');
const localVue = createLocalVue();
localVue.mixin(mixinFilterHelper);
localVue.use(Vuex);
const store = new Vuex.Store(SearchStore);

mockWindowProperty('tracker', jest.fn());

const applyFilterSpy = jest.spyOn(FilterOptions.methods, 'applyFilter');

const baseMainFilterStub = {
	template: `<button class="base-main-filter" @click="$emit('save-action')" />`
};

describe('FilterOptions.vue', () => {
	const wrapper = shallowMount(FilterOptions, {
		localVue,
		store,
		stubs: {
			'base-main-filter': baseMainFilterStub
		}
	});

	it('Should call method applyFilter correctly', async () => {
		const filterElement = wrapper.find('button.base-main-filter');

		expect(filterElement.exists()).toBe(true);

		await filterElement.trigger('click');

		expect(applyFilterSpy).toBeCalled();
	});
});
