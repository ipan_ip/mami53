import { createLocalVue, shallowMount } from '@vue/test-utils';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import 'tests-fe/utils/mock-vue';
import ModalFilterMobile from 'Js/_search/components/ModalFilterMobile.vue';
import mockAxios from 'tests-fe/utils/mock-axios';
import Vuex from 'vuex';
import SearchStore from '../__mocks__/searchStore';
jest.mock('Js/@utils/makeAPICall.js');
const mixinFilterHelper = require('Js/_search/mixins/MixinFilterHelper');
const localVue = createLocalVue();
localVue.mixin(mixinFilterHelper);
localVue.use(Vuex);
const store = new Vuex.Store(SearchStore);

mockWindowProperty('tracker', jest.fn());
mockWindowProperty('axios', mockAxios);
mockWindowProperty('bugsnagClient', {
	notify: jest.fn()
});

const handleApplyFilterSpy = jest.spyOn(
	ModalFilterMobile.methods,
	'handleApplyFilter'
);

const handleSetFilterSpy = jest.spyOn(
	ModalFilterMobile.methods,
	'handleSetFilter'
);

const handleSetPriceSpy = jest.spyOn(
	ModalFilterMobile.methods,
	'handleSetPrice'
);

const handleSetTagIdsSpy = jest.spyOn(
	ModalFilterMobile.methods,
	'handleSetTagIds'
);

const handleResetFilterSpy = jest.spyOn(
	ModalFilterMobile.methods,
	'handleResetFilter'
);

const fetchCountRoomsSpy = jest.spyOn(
	ModalFilterMobile.methods,
	'fetchCountRooms'
);

const baseFilterPriceStub = {
	template: `<button class="update-price" @click="$emit('set-value', [10000, 200000])" />`,
	methods: {
		reCalculatePrice: jest.fn(),
		resetAction: jest.fn()
	}
};

const baseFilterOptionStub = {
	template: `<button class="update-value" @click="$emit('set-value', [1], 'gender')" />`
};

const filterFacilitiesStub = {
	template: `<button class="update-tag" @click="$emit('set-value', [12,35])" />`
};

describe('ModalFilterMobile.vue', () => {
	let wrapper = shallowMount(ModalFilterMobile, {
		localVue,
		store,
		computed: {
			getIsFacilitiesExist() {
				return true;
			}
		},
		stubs: {
			'base-filter-price': baseFilterPriceStub,
			'base-filter-option': baseFilterOptionStub,
			'filter-facilities': filterFacilitiesStub
		}
	});

	it('Computed applyButtonText should return correctly', async () => {
		const testCase = [
			{
				totalFilteredKos: 0,
				expect: 'Tampilkan 0 Kos'
			},
			{
				totalFilteredKos: 200,
				expect: 'Tampilkan 200 Kos'
			},
			{
				totalFilteredKos: 1350,
				expect: 'Tampilkan 900+ Kos'
			}
		];

		for (const test of testCase) {
			await wrapper.setData({ totalFilteredKos: test.totalFilteredKos });

			expect(wrapper.vm.applyButtonText).toEqual(test.expect);
		}
	});

	it('Computed facilitiesDataMobile should return correctly', async () => {
		expect(wrapper.vm.facilitiesDataMobile).toEqual({
			title: 'Fasilitas',
			facilityCategories: [
				{ title: 'Fasilitas Kamar', data: [] },
				{ title: 'Fasilitas Bersama', data: [] }
			],
			getValue: [],
			count: 0,
			active: false,
			allFacTag: []
		});

		await wrapper.setData({
			filters: {
				...wrapper.vm.filters,
				tag_ids: [12]
			}
		});

		expect(wrapper.vm.facilitiesDataMobile.getValue).toEqual([12]);
	});

	it('Watch isShow should return correctly', async () => {
		const payload = {
			...wrapper.vm.getKostFilters,
			tag_ids: [1, 2],
			gender: [1, 0],
			rent_type: 3,
			booking: true,
			flash_sale: true,
			mamirooms: true,
			mamichecker: true
		};
		await wrapper.setProps({ isShow: false });

		wrapper.vm.$store.commit('setFilter', payload);

		expect(wrapper.vm.filters).not.toEqual(payload);

		await wrapper.setProps({ isShow: true });

		expect(wrapper.vm.filters).toEqual(payload);

		await wrapper.setProps({ isShow: false });

		expect(wrapper.vm.filters).toEqual(payload);
	});

	it('Should call method handleApplyFilter correctly', async () => {
		await wrapper.setProps({ isShow: true });
		const closeFilterElement = wrapper.find('span.modal-filter-close');

		expect(closeFilterElement.exists()).toBe(true);

		await closeFilterElement.trigger('click');

		expect(handleApplyFilterSpy).toBeCalledWith(false);
		expect(wrapper.emitted('toggle-modal-handler')).toBeTruthy();
	});

	it('Should call method handleSetFilter correctly', async () => {
		const updateFilterElement = wrapper.find('button.update-value');

		expect(updateFilterElement.exists()).toBe(true);

		await updateFilterElement.trigger('click');

		expect(handleSetFilterSpy).toBeCalledWith([1], 'gender');
		expect(fetchCountRoomsSpy).toBeCalled();
	});

	it('Should call method handleSetTagIds correctly', async () => {
		const updatedTagElement = wrapper.find('button.update-tag');

		expect(updatedTagElement.exists()).toBe(true);

		await updatedTagElement.trigger('click');

		expect(handleSetTagIdsSpy).toBeCalledWith([12, 35]);
		expect(fetchCountRoomsSpy).toBeCalled();
	});

	it('Should call method handleResetFilter correctly', async () => {
		const resetFilterElement = wrapper.find('button.search-btn-reset');

		expect(resetFilterElement.exists()).toBe(true);

		await resetFilterElement.trigger('click');

		expect(handleResetFilterSpy).toBeCalled();
		expect(wrapper.vm.filters).toEqual({
			...wrapper.vm.filters,
			gender: [],
			price_range: [10000, 20000000],
			tag_ids: [],
			level_info: [],
			rent_type: 2,
			property_type: 'all',
			booking: false,
			flash_sale: false,
			mamichecker: false,
			mamirooms: false
		});
		expect(fetchCountRoomsSpy).toBeCalled();
	});

	it('Should call method handleSetPrice correctly', async () => {
		const updatedPriceElement = wrapper.find('button.update-price');

		expect(updatedPriceElement.exists()).toBe(true);

		await updatedPriceElement.trigger('click');

		expect(handleSetPriceSpy).toBeCalledWith([10000, 200000]);
		expect(fetchCountRoomsSpy).toBeCalled();
	});

	it('Should call method handleApplyFilter with fetch correctly', async () => {
		wrapper = shallowMount(ModalFilterMobile, {
			localVue,
			store,
			stubs: {
				'base-filter-price': baseFilterPriceStub,
				'base-filter-option': baseFilterOptionStub,
				'filter-facilities': filterFacilitiesStub
			}
		});
		await wrapper.setProps({ isShow: true });
		await wrapper.setData({ isCountRoomsLoading: false });
		const saveElement = wrapper.find('button.search-btn-save');

		expect(saveElement.exists()).toBe(true);

		await saveElement.trigger('click');

		expect(handleApplyFilterSpy).toBeCalled();
	});
});
