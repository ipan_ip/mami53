import { createLocalVue, shallowMount } from '@vue/test-utils';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import 'tests-fe/utils/mock-vue';
import BaseFilterPrice from 'Js/@components/BaseFilterPrice.vue';
import Vuex from 'vuex';
import SearchStore from '../__mocks__/searchStore';

const mixinNavigator = require('Js/@mixins/MixinNavigatorIsMobile');
const localVue = createLocalVue();
localVue.mixin(mixinNavigator);
localVue.use(Vuex);
const store = new Vuex.Store(SearchStore);

// mockWindowProperty('tracker', jest.fn());

const sanitizeInputSpy = jest.spyOn(BaseFilterPrice.methods, 'sanitizeInput');

const handleOnInputSpy = jest.spyOn(BaseFilterPrice.methods, 'handleOnInput');

const resetActionSpy = jest.spyOn(BaseFilterPrice.methods, 'resetAction');

const reCalculatePriceSpy = jest.spyOn(
	BaseFilterPrice.methods,
	'reCalculatePrice'
);

const handleStatusDropdownChangeSpy = jest.spyOn(
	BaseFilterPrice.methods,
	'handleStatusDropdownChange'
);

const addNecessaryListenerSpy = jest.spyOn(
	BaseFilterPrice.methods,
	'addNecessaryListener'
);

const reRouteTrackSpy = jest.spyOn(BaseFilterPrice.methods, 'reRouteTrack');

const removeNecessaryListenerSpy = jest.spyOn(
	BaseFilterPrice.methods,
	'removeNecessaryListener'
);

const checkInvalidConditionSpy = jest.spyOn(
	BaseFilterPrice.methods,
	'checkInvalidCondition'
);

const checkPriceThumbSpy = jest.spyOn(
	BaseFilterPrice.methods,
	'checkPriceThumb'
);

const setThumbOnInputSpy = jest.spyOn(
	BaseFilterPrice.methods,
	'setThumbOnInput'
);

const handleCheckPriceSpy = jest.spyOn(
	BaseFilterPrice.methods,
	'handleCheckPrice'
);

const handleInputBlurSpy = jest.spyOn(
	BaseFilterPrice.methods,
	'handleInputBlur'
);

const handleHoldingThumbSpy = jest.spyOn(
	BaseFilterPrice.methods,
	'handleHoldingThumb'
);

const handleReleaseThumbSpy = jest.spyOn(
	BaseFilterPrice.methods,
	'handleReleaseThumb'
);

const handleThumbMoveSpy = jest.spyOn(
	BaseFilterPrice.methods,
	'handleThumbMove'
);

const handleEmitPriceSpy = jest.spyOn(
	BaseFilterPrice.methods,
	'handleEmitPrice'
);

// const baseMainFilterStub = {
//   template: `<button class="base-main-filter" @click="$emit('save-action')" />`
// };

describe('BaseFilterPrice.vue', () => {
	let wrapper = shallowMount(BaseFilterPrice, {
		localVue,
		store
	});

	it('Should call method handleOnInput on input minPrice correctly', async () => {
		const testCase = [
			{
				inputValue: 20000,
				dropdown: true
			},
			{
				inputValue: 0,
				dropdown: false
			},
			{
				inputValue: '2000asd',
				dropdown: true
			}
		];

		for (const test of testCase) {
			await wrapper.setData({ minPrice: test.inputValue });

			const inputElement = wrapper.findAll('input.price-input').at(0);

			expect(inputElement.exists()).toBe(true);

			await inputElement.trigger('input');

			expect(handleOnInputSpy).toBeCalled();
		}
	});

	it('Should call method handleOnInput on input maxPrice correctly', async () => {
		const testCase = [
			{
				inputValue: 20000,
				dropdown: true
			},
			{
				inputValue: 0,
				dropdown: false
			},
			{
				inputValue: '2000asd',
				dropdown: true
			}
		];

		for (const test of testCase) {
			await wrapper.setData({ minPrice: test.inputValue });

			const inputElement = wrapper.findAll('input.price-input').at(1);

			expect(inputElement.exists()).toBe(true);

			await inputElement.trigger('input');

			expect(handleOnInputSpy).toBeCalled();
			expect(sanitizeInputSpy).toBeCalled();
			expect(setThumbOnInputSpy).toBeCalled();
			expect(reRouteTrackSpy).toBeCalled();

			wrapper.vm.handleStatusDropdownChange(test.dropdown);

			expect(handleStatusDropdownChangeSpy).toBeCalled();

			if (test.dropdown) {
				await wrapper.vm.$nextTick();

				expect(reCalculatePriceSpy).toBeCalled();
			}

			expect(setThumbOnInputSpy).toBeCalled();
		}

		wrapper.vm.resetAction();

		expect(resetActionSpy).toBeCalled();
		expect(wrapper.vm.minPrice).toEqual(10000);
		expect(wrapper.vm.maxPrice).toEqual(20000000);
	});

	it('Should call method handleInputBlur on input minPrice correctly', async () => {
		const testCase = [
			{
				inputValue: 20000,
				dropdown: true,
				otherValue: 0
			},
			{
				inputValue: 0,
				dropdown: false,
				otherValue: 5000000
			},
			{
				inputValue: '2000asd',
				dropdown: true,
				otherValue: 5000000
			},
			{
				inputValue: 9999999999,
				dropdown: true,
				otherValue: 9999999999
			}
		];

		await wrapper.setProps({ applyOnChange: true });

		for (const test of testCase) {
			await wrapper.setData({ minPrice: test.inputValue });
			await wrapper.setData({ maxPrice: test.otherValue });

			const inputElement = wrapper.findAll('input.price-input').at(0);

			expect(inputElement.exists()).toBe(true);

			await inputElement.trigger('blur');

			expect(handleInputBlurSpy).toBeCalled();
			expect(handleCheckPriceSpy).toBeCalled();
			expect(checkInvalidConditionSpy).toBeCalled();
			expect(handleEmitPriceSpy).toBeCalled();
		}
	});

	it('Should call method handleInputBlur on input maxPrice correctly', async () => {
		const testCase = [
			{
				inputValue: 0,
				dropdown: true,
				otherValue: 20000
			},
			{
				inputValue: 5000000,
				dropdown: false,
				otherValue: 0
			},
			{
				inputValue: 5000000,
				dropdown: true,
				otherValue: '2000asd'
			},
			{
				inputValue: 9999999999,
				dropdown: true,
				otherValue: 9999999999
			}
		];

		await wrapper.setProps({ applyOnChange: true });

		for (const test of testCase) {
			await wrapper.setData({ maxPrice: test.inputValue });
			await wrapper.setData({ minPrice: test.otherValue });

			const inputElement = wrapper.findAll('input.price-input').at(1);

			expect(inputElement.exists()).toBe(true);

			await inputElement.trigger('blur');

			expect(handleInputBlurSpy).toBeCalled();
			expect(handleCheckPriceSpy).toBeCalled();
		}
	});

	it('Should call handleHoldingThumb on mousedown correctly', async () => {
		const thumbElement = wrapper.findAll('span.track-thumb').at(0);

		expect(thumbElement.exists()).toBe(true);

		await thumbElement.trigger('mousedown');

		expect(handleHoldingThumbSpy).toBeCalled();
		expect(addNecessaryListenerSpy).toBeCalled();
	});

	it('Should call all thumb handler correctly', async () => {
		const testCase = [
			{
				thumbAt: 0
			},
			{
				thumbAt: 1
			}
		];

		for (const test of testCase) {
			const mockMapEventListener = {};
			window.addEventListener = jest.fn((event, cb) => {
				mockMapEventListener[event] = cb;
			});

			wrapper = shallowMount(BaseFilterPrice, {
				localVue,
				store
			});

			const thumbElement = wrapper.findAll('span.track-thumb').at(test.thumbAt);

			expect(thumbElement.exists()).toBe(true);

			await thumbElement.trigger('mousedown');

			expect(handleHoldingThumbSpy).toBeCalled();
			expect(addNecessaryListenerSpy).toBeCalled();

			await mockMapEventListener.mousemove({ clientX: 50 });

			expect(handleThumbMoveSpy).toBeCalled();

			await mockMapEventListener.mouseup();

			expect(handleReleaseThumbSpy).toBeCalled();
		}
	});

	it('Should call removeNecessaryListener on wrapper destroy', () => {
		wrapper.destroy();

		expect(removeNecessaryListenerSpy).toBeCalled();
	});
});
