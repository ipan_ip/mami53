import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import SearchStore from '../__mocks__/searchStore';
import mockAxios from 'tests-fe/utils/mock-axios';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import 'tests-fe/utils/mock-vue';
import ListContainer from 'Js/_search/components/ListContainer.vue';

const MixinIsMobile = require('Js/_search/mixins/MixinIsMobile');
const localVue = createLocalVue();
localVue.mixin(MixinIsMobile);
localVue.use(Vuex);
const store = new Vuex.Store(SearchStore);

mockWindowProperty('axios', mockAxios);
mockWindowProperty('tracker', jest.fn());
mockWindowProperty('scrollTo', jest.fn());
mockWindowProperty('Cookies', {
	set: jest.fn()
});
mockWindowProperty('bugsnagClient', {
	notify: jest.fn()
});
mockWindowProperty('mamiCriteria', {
	gender: null,
	keywords: '',
	latitude: -6.2839313,
	longitude: 106.7320383,
	price_max: 10000000,
	price_min: 0,
	rent_type: 2,
	zoom: 14,
	_id: null
});

const toggleSuggestionSpy = jest.spyOn(
	ListContainer.methods,
	'toggleSuggestion'
);

const getLandingSuggestionSpy = jest.spyOn(
	ListContainer.methods,
	'getLandingSuggestion'
);

const trackOpenLandingSpy = jest.spyOn(
	ListContainer.methods,
	'trackOpenLanding'
);

const toggleLoginModalSpy = jest.spyOn(
	ListContainer.methods,
	'toggleLoginModal'
);

const closeModalLoginSpy = jest.spyOn(ListContainer.methods, 'closeModalLogin');

const handleScrollSpy = jest.spyOn(ListContainer.methods, 'handleScroll');

const goTopSpy = jest.spyOn(ListContainer.methods, 'goTop');

const fetchMoreListSpy = jest.spyOn(ListContainer.methods, 'fetchMoreList');

const stubs = {
	'login-user': {
		template: `<div><button class="closeLogin" @click="$emit('close')">close</button></div>`
	},
	'room-list': {
		template: `<button class="openLogin" @click="$emit('toggleLoginModal')">open login</button>`
	}
};

const propsData = {
	contentType: 'list'
};

describe('ListContainer.vue', () => {
	let wrapper = shallowMount(ListContainer, {
		localVue,
		store,
		propsData,
		stubs
	});

	wrapper.vm.$store.commit('setEmptyList', false);

	it('Should call method getLandingSuggestion on mounted and call click event', async () => {
		axios.mockResolve({
			data: {
				data: {
					landing_rooms: [{ alias: 'Parangtritis', slug: '/parangtritis' }],
					name: 'UGM'
				},
				status: true
			}
		});

		wrapper = shallowMount(ListContainer, {
			localVue,
			store,
			propsData,
			stubs,
			computed: {
				center() {
					return {
						lat: -6.2839313,
						lng: 106.7320383
					};
				}
			}
		});

		expect(getLandingSuggestionSpy).toBeCalled();

		wrapper.vm.$store.commit('setLoading', false);
		await wrapper.vm.$nextTick();

		expect(tracker).toBeCalled();

		const element = wrapper.findAll('a.list__content-suggestion-area').at(0);

		expect(element.exists()).toBe(true);

		await element.trigger('click');

		expect(trackOpenLandingSpy).toBeCalled();
	});

	it('Should call method getLandingSuggestionSpy on mounted with no areas available', async () => {
		axios.mockResolve({
			data: {
				data: {
					landing_rooms: [],
					name: 'UGM'
				},
				status: true
			}
		});

		wrapper = shallowMount(ListContainer, {
			localVue,
			store,
			propsData,
			stubs,
			computed: {
				center() {
					return {
						lat: -6.2839313,
						lng: 106.7320383
					};
				}
			}
		});

		expect(getLandingSuggestionSpy).toBeCalled();

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.areas.length).toEqual(0);
	});

	it('Should call method getLandingSuggestionSpy on mounted with false status', async () => {
		axios.mockResolve({
			data: {
				status: false
			}
		});

		wrapper = shallowMount(ListContainer, {
			localVue,
			store,
			propsData,
			stubs,
			computed: {
				center() {
					return {
						lat: -6.2839313,
						lng: 106.7320383
					};
				}
			}
		});

		expect(getLandingSuggestionSpy).toBeCalled();

		await wrapper.vm.$nextTick();

		expect(wrapper.vm.areas.length).toEqual(0);
	});

	it('Should call method getLandingSuggestionSpy on mounted with promise reject', async () => {
		axios.mockReject('error');

		wrapper = shallowMount(ListContainer, {
			localVue,
			store,
			propsData,
			stubs,
			computed: {
				center() {
					return {
						lat: -6.2839313,
						lng: 106.7320383
					};
				}
			}
		});

		expect(getLandingSuggestionSpy).toBeCalled();

		await wrapper.vm.$nextTick();

		expect(bugsnagClient.notify).toBeCalled();
	});

	it('Should call method fetchMoreList on click', async () => {
		wrapper.vm.$store.commit('setLoading', false);
		wrapper.vm.$store.commit('setMoreLoading', false);
		wrapper.vm.$store.commit('setHasMore', true);

		await wrapper.vm.$nextTick();

		const element = wrapper.find('div.list__content-load-action');

		expect(element.exists()).toBe(true);

		await element.trigger('click');

		expect(fetchMoreListSpy).toBeCalled();
	});

	it('Should not call method getLandingSuggestionSpy on mounted when there is no center', async () => {
		wrapper.vm.$store.commit('setEmptyList', false);
		await wrapper.vm.$nextTick();

		expect(wrapper.vm.isAreaSuggestion).toBe(false);
	});

	it('Computed title should return correctly', async () => {
		wrapper.vm.$store.commit('setLoading', true);
		await wrapper.vm.$nextTick();

		expect(wrapper.vm.title).toEqual('Sedang mencari...');

		wrapper.vm.$store.commit('setLoading', false);
		wrapper.vm.$store.commit('setEmptyList', false);
		const payload = {
			status: true,
			isRequestMore: false,
			rooms: ['room1', 'room2'],
			total: 2
		};
		wrapper.vm.$store.commit('setRooms', payload);

		const testCase = [
			{
				keywords: '',
				geolocActive: false,
				output: 'Ditemukan 2 kos-kosan di sekitar Jabodetabek'
			},
			{
				keywords: 'sekitar',
				geolocActive: false,
				output: 'Ditemukan 2 Kos-kosan'
			},
			{
				keywords: 'UGM',
				geolocActive: false,
				output: 'Ditemukan 2 kos-kosan di sekitar UGM'
			},
			{
				keywords: '',
				geolocActive: true,
				output: 'Ditemukan 2 kos-kosan di area sekitar saat ini'
			}
		];

		for (const test of testCase) {
			wrapper.vm.$store.commit('setCriteria', { keywords: test.keywords });
			wrapper.vm.$store.commit('setExtrasGeolocActive', test.geolocActive);
			await wrapper.vm.$nextTick();

			expect(wrapper.vm.title).toEqual(test.output);
		}
	});

	it('Computed suggestionLabelButton should return correctly', async () => {
		await wrapper.setData({ isAreaSuggestion: true, isOpenSuggestion: false });
		const element = wrapper.find('button.list__content-suggestion-button');

		expect(element.exists()).toBe(true);

		await element.trigger('click');

		expect(toggleSuggestionSpy).toBeCalled();
		expect(wrapper.vm.isOpenSuggestion).toBe(true);

		await element.trigger('click');

		expect(toggleSuggestionSpy).toBeCalled();
		expect(wrapper.vm.isOpenSuggestion).toBe(false);
	});

	it('Watcher getEmptyList should call tracker', () => {
		const testCase = [
			{
				keywords: 'UGM'
			},
			{
				keywords: ''
			}
		];

		for (const test of testCase) {
			wrapper.vm.$store.commit('setEmptyList', true);
			wrapper.vm.$store.commit('setCriteria', { keywords: test.keywords });

			expect(tracker).toBeCalled();
		}
	});

	it('Should handle login condition correctly', async () => {
		wrapper.vm.$store.commit('setEmptyList', false);
		wrapper.vm.$store.commit('setLoading', false);

		await wrapper.vm.$nextTick();

		const openElement = wrapper.find('button.openLogin');
		const closeElement = wrapper.find('button.closeLogin');

		expect(openElement.exists()).toBe(true);
		expect(closeElement.exists()).toBe(true);

		await openElement.trigger('click');

		expect(toggleLoginModalSpy).toBeCalled();
		expect(wrapper.vm.isModalLogin).toBe(true);

		await closeElement.trigger('click');

		expect(closeModalLoginSpy).toBeCalled();
		expect(wrapper.vm.isModalLogin).toBe(false);
	});

	it('Should call goTop method on click', async () => {
		mockWindowProperty('pageYOffset', 1000);
		mockWindowProperty(
			'document.querySelector',
			jest.fn(() => {
				return {
					scrollIntoView: jest.fn()
				};
			})
		);

		const mockMethodEventListener = {};
		window.addEventListener = jest.fn((event, cb) => {
			mockMethodEventListener[event] = cb;
		});
		wrapper = shallowMount(ListContainer, {
			localVue,
			store,
			propsData,
			stubs,
			computed: {
				isMobile() {
					return false;
				}
			}
		});
		mockMethodEventListener.scroll({ scrollTo: 1000 });
		wrapper.vm.$store.commit('setLoading', false);
		wrapper.vm.$store.commit('setEmptyList', false);
		await wrapper.vm.$nextTick();

		const goTopElement = wrapper.find('div.list__go-top');

		expect(goTopElement.exists()).toBe(true);

		await goTopElement.trigger('click');

		expect(goTopSpy).toBeCalled();
	});

	it('Should call handleScroll method on scroll', async () => {
		const testCase = [
			{
				offset: 1000,
				lastPosition: 0,
				topValue: 62,
				width: 400,
				noRef: false
			},
			{
				offset: 1000,
				lastPosition: 0,
				topValue: 200,
				width: 400,
				noRef: false
			},
			{
				offset: 500,
				lastPosition: 1000,
				topValue: 200,
				width: 400,
				noRef: false
			},
			{
				offset: 1000,
				lastPosition: 0,
				topValue: 200,
				width: 1366,
				noRef: false
			},
			{
				offset: 500,
				lastPosition: 1000,
				topValue: 50,
				width: 500,
				noRef: false
			},
			{
				offset: 500,
				lastPosition: 1000,
				topValue: 50,
				width: 500,
				noRef: true
			}
		];

		for (const test of testCase) {
			mockWindowProperty('pageYOffset', test.offset);
			mockWindowProperty('innerWidth', test.width);

			const mockMethodEventListener = {};
			window.addEventListener = jest.fn((event, cb) => {
				mockMethodEventListener[event] = cb;
			});

			wrapper = shallowMount(ListContainer, {
				localVue,
				store,
				propsData,
				stubs,
				computed: {
					isMobile() {
						return false;
					}
				}
			});

			if (test.noRef) {
				wrapper.vm.$refs.goTopRef = null;
			}
			wrapper.setData({ lastScrollPosition: test.lastPosition });
			mockMethodEventListener.scroll({ scrollTo: test.offset });
			await wrapper.vm.$nextTick();

			expect(handleScrollSpy).toBeCalled();
		}
	});

	it('Computed emptyStateData should return correctly', async () => {
		wrapper = shallowMount(ListContainer, {
			localVue,
			store,
			propsData,
			stubs
		});

		wrapper.vm.$store.commit('setFilter', {
			tag_ids: [12],
			level_info: ['superkost'],
			rent_type: 1
		});
		await wrapper.vm.$nextTick();

		expect(wrapper.vm.emptyStateData).toEqual({
			title: 'Kos Tidak Ditemukan',
			subTitle: 'Silahkan ubah filter untuk meningkatkan hasil pencarian kos.',
			needButton: false,
			buttonText: 'Ubah Filter'
		});

		wrapper.destroy();
	});
});
