import { createLocalVue, shallowMount } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import FilterPrice from 'Js/@components/BaseMainPrice.vue';
import Vuex from 'vuex';
import SearchStore from '../__mocks__/searchStore';
import { dropdown } from 'vue-strap';
const localVue = createLocalVue();
localVue.use(Vuex);
const store = new Vuex.Store(SearchStore);

const handleCloseDropdownSpy = jest.spyOn(
	FilterPrice.methods,
	'handleCloseDropdown'
);

const setFilterPriceSpy = jest.spyOn(FilterPrice.methods, 'setFilterPrice');

const resetPriceSpy = jest.spyOn(FilterPrice.methods, 'resetPrice');

const closeActionSpy = jest.spyOn(FilterPrice.methods, 'closeAction');

const baseFilterPriceStub = {
	template: `<div>Stub Component</div>`,
	methods: {
		handleStatusDropdownChange: jest.fn(),
		resetAction: jest.fn()
	}
};

describe('FilterPrice.vue', () => {
	let wrapper = shallowMount(FilterPrice, {
		localVue,
		store,
		stubs: {
			dropdown,
			'base-filter-price': baseFilterPriceStub
		}
	});

	it('Should call method handleCloseDropdown correctly', async () => {
		const toggleElement = wrapper.find('button.base-filter-button');

		expect(toggleElement.exists()).toBe(true);

		await toggleElement.trigger('click');

		expect(handleCloseDropdownSpy).toBeCalled();
	});

	it('Should call method resetPrice correctly', async () => {
		const resetElement = wrapper.find('button.base-reset');

		expect(resetElement.exists()).toBe(true);

		await resetElement.trigger('click');

		expect(resetPriceSpy).toBeCalled();
	});

	it('Should call method closeAction correctly', async () => {
		const closeElement = wrapper.find('button.base-save');

		expect(closeElement.exists()).toBe(true);

		await closeElement.trigger('click');

		expect(closeActionSpy).toBeCalled();
	});

	it('Should call method setFilterPrice correctly', async () => {
		const baseFilterPriceSetValueStub = {
			template: `<button class="set-value" @click="$emit('set-value')" />`
		};

		wrapper = shallowMount(FilterPrice, {
			localVue,
			store,
			stubs: {
				dropdown,
				'base-filter-price': baseFilterPriceSetValueStub
			}
		});

		const setPriceElement = wrapper.find('button.set-value');

		expect(setPriceElement.exists()).toBe(true);

		await setPriceElement.trigger('click');

		expect(setFilterPriceSpy).toBeCalled();
	});
});
