import { createLocalVue, shallowMount } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import ContainerSearchPage from 'Js/_search/components/ContainerSearchPage.vue';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import mockAxios from 'tests-fe/utils/mock-axios';
import Vuex from 'vuex';
import SearchStore from '../__mocks__/searchStore';

jest.mock('Js/@utils/makeAPICall.js');
jest.mock('lodash/debounce', () => jest.fn(fn => fn));

const mixinFilterHelper = require('Js/_search/mixins/MixinFilterHelper');
const localVue = createLocalVue();
localVue.mixin(mixinFilterHelper);
localVue.use(Vuex);
const store = new Vuex.Store(SearchStore);

mockWindowProperty('Cookies', {
	get: jest.fn(() => {
		return '';
	}),
	remove: jest.fn()
});
mockWindowProperty('axios', mockAxios);
mockWindowProperty('tracker', jest.fn());
mockWindowProperty('bugsnagClient', {
	notify: jest.fn()
});

const handleScrollSpy = jest.spyOn(ContainerSearchPage.methods, 'handleScroll');

const resetStickyFilterPositionSpy = jest.spyOn(
	ContainerSearchPage.methods,
	'resetStickyFilterPosition'
);

const toggleModalFacilitiesSpy = jest.spyOn(
	ContainerSearchPage.methods,
	'toggleModalFacilities'
);

const handleRevertTagSpy = jest.spyOn(
	ContainerSearchPage.methods,
	'handleRevertTag'
);

const handleRevertCriteriaSpy = jest.spyOn(
	ContainerSearchPage.methods,
	'handleRevertCriteria'
);

const handleChangeContentTypeSpy = jest.spyOn(
	ContainerSearchPage.methods,
	'handleChangeContentType'
);

const handleBrowserScrollSpy = jest.spyOn(
	ContainerSearchPage.methods,
	'handleBrowserScroll'
);

const FilterOptions = {
	template: `<button class="toggle-modal" @click="$emit('facilities-modal-handler')" />`
};

const ModalFacilities = {
	template: `<button class="save-modal" @click="$emit('save-facilities-handler')" />`
};

const FilterMobile = {
	template: `<div><button class="reset-sticky" @click="$emit('reset-sticky-filter-handler')" /><button class="content-map" @click="$emit('handle-change-content-type', 'map')" /><button class="content-list" @click="$emit('handle-change-content-type', 'list')" /><button class="content-falsy" @click="$emit('handle-change-content-type', 'falsy')" /></div>`
};

const GlobalNavbar = {
	template: `<button class="revert-criteria" @click="$emit('handle-show-modal-callback')"/>`
};

const stubs = { FilterOptions, ModalFacilities, GlobalNavbar, FilterMobile };

global.scrollTo = jest.fn();

describe('ContainerSearchPage.vue', () => {
	let wrapper = shallowMount(ContainerSearchPage, {
		localVue,
		store,
		stubs
	});

	it('Computed isLogin should return correctly', () => {
		expect(wrapper.vm.isLogin).toEqual(false);

		wrapper.vm.$store.commit('setAuthCheck', {
			all: true
		});

		expect(wrapper.vm.isLogin).toEqual(true);
	});

	it('Computed center should return correctly', () => {
		wrapper.vm.$store.commit('setCriteria', {
			gender: null,
			keywords: '',
			latitude: -6.2839313,
			longitude: 106.7320383,
			price_max: 10000000,
			price_min: 0,
			rent_type: 2,
			zoom: 14,
			_id: null
		});

		expect(wrapper.vm.center).toEqual({
			lat: -6.2839313,
			lng: 106.7320383
		});
	});

	it('Computed generatedCriteria should return correctly', async () => {
		const testCase = [
			{
				rentType: 1,
				search_keyword: undefined,
				keywords: 'UGM',
				output: 'UGM'
			},
			{
				rentType: 2,
				search_keyword: 'UGM',
				keywords: '',
				output: ''
			},
			{
				rentType: 3,
				search_keyword: 'UGM',
				keywords: 'sekitar',
				output: ''
			},
			{
				rentType: 0,
				search_keyword: 'UGM',
				keywords: 'UGM, Yogyakarta',
				output: 'UGM'
			}
		];

		const rentWord = ['harian', 'mingguan', 'bulanan', 'tahunan'];

		testCase.forEach(selection => {
			mockWindowProperty('Cookies', {
				get: jest.fn(() => {
					return selection.search_keyword;
				}),
				remove: jest.fn()
			});

			wrapper = shallowMount(ContainerSearchPage, {
				localVue,
				store,
				stubs
			});
			wrapper.vm.$store.commit('setFilter', {
				rent_type: selection.rentType
			});
			wrapper.vm.$store.commit('setCriteria', {
				keywords: selection.keywords
			});

			expect(wrapper.vm.generatedCriteria).toEqual({
				keywords: selection.output,
				gender: 'all',
				rentType: rentWord[selection.rentType],
				price: [10000, 20000000]
			});
		});
	});

	it('Should call toggleModalFacilities method correctly', async () => {
		const toggleElement = wrapper.find('button.toggle-modal');

		expect(toggleElement.exists()).toBe(true);

		await toggleElement.trigger('click');

		expect(toggleModalFacilitiesSpy).toBeCalled();
	});

	it('Should call resetStickyFilterPosition method correctly', async () => {
		mockWindowProperty('innerWidth', 400);

		wrapper = shallowMount(ContainerSearchPage, {
			localVue,
			store,
			stubs
		});
		const resetElement = wrapper.find('button.reset-sticky');

		expect(resetElement.exists()).toBe(true);

		await resetElement.trigger('click');

		expect(resetStickyFilterPositionSpy).toBeCalled();
	});

	it('Should call handleScroll on scroll correctly', async () => {
		const testCase = [
			{
				isMobile: true,
				innerWidth: 400,
				pageYOffset: 1000,
				lastScrollPosition: 50,
				ref: true,
				authCheck: {
					all: true
				},
				topValue: 20
			},
			{
				isMobile: true,
				innerWidth: 400,
				pageYOffset: 1000,
				lastScrollPosition: 50,
				ref: true,
				authCheck: {
					all: true
				},
				topValue: 60
			},
			{
				isMobile: true,
				innerWidth: 1200,
				pageYOffset: 1000,
				lastScrollPosition: 1200,
				ref: true,
				authCheck: {
					all: false
				},
				topValue: 60
			},
			{
				isMobile: false,
				innerWidth: 1366,
				pageYOffset: 1000,
				lastScrollPosition: 1200,
				ref: false,
				authCheck: {
					all: true
				},
				topValue: 80,
				criteria: {
					...wrapper.vm.criteria,
					isShowNominatim: true
				}
			},
			{
				isMobile: true,
				innerWidth: 1200,
				pageYOffset: 1000,
				lastScrollPosition: 1200,
				ref: false,
				authCheck: {
					all: true
				},
				topValue: 120
			}
		];

		const mockMapEventListener = {};
		window.addEventListener = jest.fn((event, cb) => {
			mockMapEventListener[event] = cb;
		});

		for (const test of testCase) {
			mockWindowProperty('pageYOffset', test.pageYOffset);
			mockWindowProperty('innerWidth', test.innerWidth);

			wrapper = shallowMount(ContainerSearchPage, {
				localVue,
				store,
				stubs,
				computed: {
					isMobile() {
						return test.isMobile;
					}
				}
			});

			test.criteria &&
				!test.isMobile &&
				wrapper.vm.$store.commit('setCriteria', test.criteria);
			wrapper.vm.$store.commit('setAuthCheck', test.authCheck);
			wrapper.vm.$refs.contentWrapper.getBoundingClientRect = jest.fn(() => ({
				top: test.topValue
			}));

			wrapper.setData({ lastScrollPosition: test.lastScrollPosition });

			mockMapEventListener.scroll({ scrollTo: test.lastScrollPosition });

			await wrapper.vm.$nextTick();

			expect(handleScrollSpy).toBeCalled();
		}
	});

	it('Should call watch correctly, when isLoading state is change and the list should scroll to the top', async () => {
		await wrapper.vm.$store.commit('setLoading', false);

		expect(window.scrollTo).toBeCalledWith(0, 0);
		expect(wrapper.vm.lastListPosition).toBe(0);

		wrapper.setData({ lastListPosition: 10 });
		wrapper.vm.$store.commit('setLoading', true);
		await wrapper.vm.$nextTick();

		expect(wrapper.vm.lastListPosition).toBe(10);
	});

	it('Should handle revert criteria when user show the search modal', async () => {
		const testCase = [
			{
				criteria: {
					...wrapper.vm.criteria,
					isShowNominatim: false,
					tag: null
				},
				onlyCalled: true,
				revertedTag: null
			},
			{
				criteria: {
					...wrapper.vm.criteria,
					isShowNominatim: true,
					tag: 'Jakarta'
				},
				onlyCalled: false,
				revertedTag: 'Jakarta'
			}
		];

		const revertElement = wrapper.find('button.revert-criteria');

		expect(revertElement.exists()).toBe(true);

		for (const test of testCase) {
			wrapper.vm.$store.commit('setCriteria', test.criteria);
			revertElement.trigger('click');

			await wrapper.vm.$nextTick();

			expect(handleRevertCriteriaSpy).toBeCalled();
			expect(wrapper.vm.criteria.tag).toBe(null);
			expect(wrapper.vm.revertedTag).toBe(test.revertedTag);
		}
	});

	it('Computed kostTitle should return correctly based on criteria', () => {
		const testCase = [
			{
				criteria: {
					...wrapper.vm.criteria,
					propertyName: 'Jatiasih Tipe A',
					tag: null,
					keywords: '',
					isShowNominatim: false
				},
				output: {
					text: 'Kos di sekitar Jatiasih Tipe A',
					isReadTag: false,
					show: true
				}
			},
			{
				criteria: {
					...wrapper.vm.criteria,
					propertyName: '',
					tag: 'Jakarta',
					keywords: '',
					isShowNominatim: true
				},
				output: {
					text: 'Kos di jakarta',
					isReadTag: true,
					show: true
				}
			},
			{
				criteria: {
					...wrapper.vm.criteria,
					propertyName: '',
					tag: null,
					keywords: 'Bogor',
					isShowNominatim: true
				},
				output: {
					text: 'Kos di Bogor',
					isReadTag: true,
					show: true
				}
			},
			{
				criteria: {
					...wrapper.vm.criteria,
					propertyName: '',
					tag: null,
					keywords: '',
					isShowNominatim: false
				},
				output: {
					text: null,
					isReadTag: false,
					show: false
				}
			}
		];

		for (const test of testCase) {
			wrapper.vm.$store.commit('setCriteria', test.criteria);

			expect(wrapper.vm.kostTitle).toEqual(test.output);
		}
	});

	it('Should handle change of content type correctly when user toggle it on mobile', async () => {
		const listElement = wrapper.find('button.content-list');
		const mapElement = wrapper.find('button.content-map');
		const falsyElement = wrapper.find('button.content-falsy');

		expect(listElement.exists()).toBe(true);
		expect(mapElement.exists()).toBe(true);
		expect(falsyElement.exists()).toBe(true);

		await listElement.trigger('click');

		expect(handleChangeContentTypeSpy).toBeCalledWith('list');
		expect(handleBrowserScrollSpy).toBeCalledTimes(1);

		await mapElement.trigger('click');

		expect(handleChangeContentTypeSpy).toBeCalledWith('map');
		expect(handleBrowserScrollSpy).toBeCalledTimes(2);

		await falsyElement.trigger('click');

		expect(handleChangeContentTypeSpy).toBeCalledWith('falsy');
		expect(handleBrowserScrollSpy).toBeCalledTimes(2);
	});

	it('Should call method handleRevertTag when closing the global navbar component', async () => {
		const GlobalNavbarRevert = {
			template: `<button class="revert-tag" @click="$emit('handle-close-modal-callback')"/>`
		};

		wrapper = shallowMount(ContainerSearchPage, {
			localVue,
			store,
			stubs: {
				...stubs,
				GlobalNavbar: GlobalNavbarRevert
			}
		});

		const testCase = [
			{
				criteria: {
					...wrapper.vm.criteria,
					isShowNominatim: false,
					revertedTag: null
				}
			},
			{
				criteria: {
					...wrapper.vm.criteria,
					isShowNominatim: true,
					revertedTag: 'Jakarta'
				}
			}
		];

		const revertTagElement = wrapper.find('button.revert-tag');

		expect(revertTagElement.exists()).toBe(true);

		for (const test of testCase) {
			wrapper.setData({ revertedTag: test.criteria.revertedTag });
			wrapper.vm.$store.commit('setCriteria', test.criteria);
			revertTagElement.trigger('click');

			expect(handleRevertTagSpy).toBeCalled();
			expect(wrapper.vm.criteria.tag).toBe(test.criteria.revertedTag);
		}
	});

	it('Should call removeEventListener on destroy', async () => {
		mockWindowProperty('window', {
			addEventListener: jest.fn(),
			removeEventListener: jest.fn()
		});

		wrapper.destroy();

		expect(window.removeEventListener).toBeCalled();
	});
});
