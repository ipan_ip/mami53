import { createLocalVue, shallowMount } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import BaseFtue from 'Js/_search/components/BaseFtue.vue';
import PopperWidget from 'Js/@components/PopperWidget';
import Vuex from 'vuex';
import SearchStore from '../__mocks__/searchStore';

const localVue = createLocalVue();
localVue.use(Vuex);
const store = new Vuex.Store(SearchStore);

mockWindowProperty('window', {
	scrollTo: jest.fn()
});

const setFilterKostTypeSpy = jest.spyOn(BaseFtue.methods, 'setFilterKostType');
const hidePopperLearnMoreSpy = jest.spyOn(
	BaseFtue.methods,
	'hidePopperLearnMore'
);
const hidePopperUnderstandSpy = jest.spyOn(
	BaseFtue.methods,
	'hidePopperUnderstand'
);
const hidePopperSpy = jest.spyOn(BaseFtue.methods, 'hidePopper');
const showPopperSpy = jest.spyOn(BaseFtue.methods, 'showPopper');
const togglePopperSpy = jest.spyOn(BaseFtue.methods, 'togglePopper');

const baseMainFilterStub = {
	template: `<button class="base-main-filter" @click="$emit('set-value',true, 'rent-type')" />`
};

const propsData = {
	contentType: 'list'
};

describe('BaseFtue.vue', () => {
	let wrapper = shallowMount(BaseFtue, {
		localVue,
		store,
		propsData,
		stubs: {
			'popper-widget': PopperWidget,
			'base-main-filter': baseMainFilterStub
		}
	});

	it('Watch showFtue should handled correctly', async () => {
		expect(wrapper.vm.showFtue).toBe(false);

		await wrapper.setProps({ showFtue: true });

		expect(wrapper.vm.showFtue).toBe(true);
		expect(showPopperSpy).toBeCalled();
		expect(wrapper.vm.isShow).toBe(true);
	});

	it('Should call method setFilterKostType correctly', async () => {
		const filterElement = wrapper.find('button.base-main-filter');

		expect(filterElement.exists()).toBe(true);

		await filterElement.trigger('click');

		expect(setFilterKostTypeSpy).toBeCalledWith(true, 'rent-type');
		expect(wrapper.emitted('set-value')).toBeTruthy();
	});

	it('Should call method togglePopper correctly', async () => {
		const baseMainFilterCallbackStub = {
			template: `<button class="base-main-filter" @click="$emit('callback-action')" />`
		};

		wrapper = shallowMount(BaseFtue, {
			localVue,
			store,
			propsData,
			stubs: {
				'popper-widget': PopperWidget,
				'base-main-filter': baseMainFilterCallbackStub
			}
		});

		const filterElement = wrapper.find('button.base-main-filter');

		expect(filterElement.exists()).toBe(true);

		await filterElement.trigger('click');

		expect(togglePopperSpy).toBeCalled();

		await wrapper.setProps({ showFtue: true });

		expect(wrapper.vm.showFtue).toBe(true);
		expect(showPopperSpy).toBeCalled();
		expect(wrapper.vm.isShow).toBe(true);

		await filterElement.trigger('click');

		expect(hidePopperSpy).toBeCalled();
	});

	it('Should call method hidePopperLearnMore correctly', async () => {
		wrapper = shallowMount(BaseFtue, {
			localVue,
			store,
			propsData,
			stubs: {
				'popper-widget': PopperWidget
			}
		});
		await wrapper.setProps({ showFtue: true });

		expect(wrapper.vm.showFtue).toBe(true);
		expect(wrapper.vm.isShow).toBe(true);

		const linkElement = wrapper.find('a.popper-ftue__content-link');

		expect(linkElement.exists()).toBe(true);

		await linkElement.trigger('click');

		expect(hidePopperLearnMoreSpy).toBeCalled();
		expect(wrapper.emitted('callback-on-learn-more')).toBeTruthy();
		expect(hidePopperSpy).toBeCalled();
		expect(wrapper.emitted('callback-on-hide')).toBeTruthy();
		expect(wrapper.vm.isShow).toBe(false);
	});

	it('Should call method hidePopperUnderstand correctly', async () => {
		wrapper = shallowMount(BaseFtue, {
			localVue,
			store,
			propsData,
			stubs: {
				'popper-widget': PopperWidget
			}
		});
		await wrapper.setProps({ showFtue: true });

		expect(wrapper.vm.showFtue).toBe(true);
		expect(wrapper.vm.isShow).toBe(true);

		const buttonElement = wrapper.find('button.popper-ftue__content-button');

		expect(buttonElement.exists()).toBe(true);

		await buttonElement.trigger('click');

		expect(hidePopperUnderstandSpy).toBeCalled();
		expect(wrapper.emitted('callback-on-understand')).toBeTruthy();
		expect(hidePopperSpy).toBeCalled();
		expect(wrapper.emitted('callback-on-hide')).toBeTruthy();
		expect(wrapper.vm.isShow).toBe(false);
	});
});
