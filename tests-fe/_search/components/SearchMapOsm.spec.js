import { createLocalVue, shallowMount } from '@vue/test-utils';
import '@babel/polyfill';
import Vuex from 'vuex';
import SearchStore from '../__mocks__/searchStore';
import mockAxios from 'tests-fe/utils/mock-axios';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import 'tests-fe/utils/mock-vue';
import SearchMapOsm from 'Js/_search/components/SearchMapOsm.vue';
const mixinNavigator = require('Js/@mixins/MixinNavigatorIsMobile');
const localVue = createLocalVue();
localVue.mixin(mixinNavigator);
localVue.use(Vuex);
const store = new Vuex.Store(SearchStore);
mockWindowProperty('axios', mockAxios);
mockWindowProperty('bugsnagClient', {
	notify: jest.fn()
});

const initLocationCheckSpy = jest.spyOn(
	SearchMapOsm.methods,
	'initLocationCheck'
);
const getCurrentPositionSpy = jest.spyOn(
	SearchMapOsm.methods,
	'getCurrentPosition'
);
const getRadiusCalculationSpy = jest.spyOn(
	SearchMapOsm.methods,
	'getRadiusCalculation'
);
const boundsChangeSpy = jest.spyOn(SearchMapOsm.methods, 'boundsChanged');
const fetchRoomsSpy = jest.spyOn(SearchMapOsm.methods, 'fetchRooms');
const clusterKostSpy = jest.spyOn(SearchMapOsm.methods, 'clusterKost');
const fetchClustersSpy = jest.spyOn(SearchMapOsm.methods, 'fetchClusters');
const handleResetFilterSpy = jest.spyOn(
	SearchMapOsm.methods,
	'handleResetFilter'
);
const handleCheckUserPositionSpy = jest.spyOn(
	SearchMapOsm.methods,
	'handleCheckUserPosition'
);
const applyLocSpy = jest.fn();
const showClustersSpy = jest.fn();

const stubs = {
	'leaflet-map': {
		template: `<div>Leaflet Map<button class="bound-changed" @click="$emit('bounds-changed', [1,2])">Bounds Change</button><button class="select-cluster" @click="$emit('select-cluster', [1,2])">Select Cluster</button><button class="reset-filter" @click="$emit('handle-reset-filter')" /></div>`,
		methods: {
			applyLoc: applyLocSpy,
			showClusters: showClustersSpy
		}
	}
};

const propsData = {
	totalActive: {
		filter: 10
	}
};

describe('SearchMapOsm.vue', () => {
	let wrapper = shallowMount(SearchMapOsm, {
		localVue,
		store,
		stubs,
		propsData
	});

	wrapper.vm.$store.commit('setEmptyList', false);
	wrapper.vm.$store.commit('setAuthCheck', {
		all: true,
		tenant: true,
		owner: false,
		admin: false
	});

	it('Computed corner and zoom should return correctly', async () => {
		const testCase = [
			{
				location: [
					[106.69793128967285, -6.296188782911225],
					[106.76608085632326, -6.271703381316111]
				],
				criteria: {
					gender: null,
					keywords: '',
					latitude: -6.2839313,
					longitude: 106.7320383,
					price_max: 10000000,
					price_min: 0,
					rent_type: 2,
					zoom: 10,
					_id: null
				},
				outputCorner: 2,
				outputZoom: 10
			},
			{
				location: [],
				criteria: {
					gender: null,
					keywords: '',
					latitude: -6.2839313,
					longitude: 106.7320383,
					price_max: 10000000,
					price_min: 0,
					rent_type: 2,
					zoom: null,
					_id: null
				},
				outputCorner: 0,
				outputZoom: null
			}
		];

		for (const test of testCase) {
			wrapper.vm.$store.commit('setCriteria', test.criteria);
			wrapper.vm.$store.commit('setLocation', test.location);
			await wrapper.vm.$nextTick();

			expect(wrapper.vm.corner.length).toEqual(test.outputCorner);
			expect(wrapper.vm.zoom).toEqual(test.outputZoom);
		}
	});

	it('Computed isLogin should return correctly', () => {
		expect(wrapper.vm.isLogin).toBe(true);
	});

	it('Computed boundaries should return correctly', () => {
		expect(wrapper.vm.boundaries).toEqual([]);
	});

	it('Computed isUsingZoom should return correctly', () => {
		expect(wrapper.vm.isUsingZoom).toBe(true);
	});

	it('Computed isShowNominatim should return correctly', () => {
		expect(wrapper.vm.isShowNominatim).toBe(false);
	});

	it('Computed isMapLoading should return correctly', () => {
		expect(wrapper.vm.isMapLoading).toBe(true);
	});

	it('Computed mapClass should return correctly', () => {
		expect(wrapper.vm.mapClass).toBe('map-mobile');

		global.innerWidth = 1366;

		wrapper = shallowMount(SearchMapOsm, {
			localVue,
			store,
			stubs,
			propsData
		});

		expect(wrapper.vm.mapClass).toBe('container-fluid map-container ');

		global.innerWidth = 400;
	});

	it('Should call method initLocationCheck on created', async () => {
		const testCase = [
			{
				criteria: {
					gender: null,
					keywords: '',
					latitude: -6.2839313,
					longitude: 106.7320383,
					price_max: 10000000,
					price_min: 0,
					rent_type: 2,
					zoom: 10,
					_id: null
				},
				isMobile: false,
				calledMethod: 'getCurrentPosition'
			},
			{
				criteria: {
					gender: null,
					keywords: 'UGM',
					latitude: -6.2839313,
					longitude: 106.7320383,
					price_max: 10000000,
					price_min: 0,
					rent_type: 2,
					zoom: 10,
					_id: null
				},
				isMobile: true,
				calledMethod: 'getRadiusCalculation'
			},
			{
				criteria: {
					gender: null,
					keywords: 'UGM',
					latitude: -6.2839313,
					longitude: 106.7320383,
					price_max: 10000000,
					price_min: 0,
					rent_type: 2,
					zoom: 10,
					_id: null
				},
				isMobile: true,
				calledMethod: 'setCenter'
			}
		];

		for (const test of testCase) {
			wrapper = shallowMount(SearchMapOsm, {
				localVue,
				store,
				stubs,
				propsData,
				computed: {
					isMobile() {
						return test.isMobile;
					},
					criteria() {
						return test.criteria;
					}
				}
			});

			await wrapper.vm.$store.commit('setFacLoading', false);

			expect(initLocationCheckSpy).toBeCalled();
			if (test.calledMethod === 'getCurrentPosition') {
				expect(getCurrentPositionSpy).toBeCalled();
			} else if (test.calledMethod === 'getRadiusCalculation') {
				expect(getRadiusCalculationSpy).toBeCalled();
			} else {
				expect(wrapper.vm.centerReady).toBe(true);
			}
		}
	});

	it('Should call boundsChanged method with has map condition', async () => {
		const testCase = [true, false];

		for (const test in testCase) {
			mockWindowProperty('innerWidth', 1366);
			mockWindowProperty('mamiCriteria', {
				gender: null,
				keywords: 'UGM',
				latitude: -6.2839313,
				longitude: 106.7320383,
				price_max: 10000000,
				price_min: 0,
				rent_type: 2,
				zoom: 14,
				_id: null
			});

			wrapper = shallowMount(SearchMapOsm, {
				localVue,
				store,
				stubs,
				propsData
			});

			wrapper.setData({ centerReady: true });
			await wrapper.vm.$store.commit('setFacLoading', false);

			const boundsChangedElement = wrapper.find('button.bound-changed');

			expect(boundsChangedElement.exists()).toBe(true);

			boundsChangedElement.trigger('click');
			await wrapper.vm.$store.commit('setClusterStatus', testCase[test]);

			expect(boundsChangeSpy).toBeCalled();

			wrapper.vm.boundsChanged([1, 2]);

			expect(fetchRoomsSpy).toBeCalled();
		}
	});

	it('Should call clusterKost method with has map condition', async () => {
		mockWindowProperty('innerWidth', 1366);
		mockWindowProperty('mamiCriteria', {
			gender: null,
			keywords: 'UGM',
			latitude: -6.2839313,
			longitude: 106.7320383,
			price_max: 10000000,
			price_min: 0,
			rent_type: 2,
			zoom: 14,
			_id: null
		});

		wrapper = shallowMount(SearchMapOsm, {
			localVue,
			store,
			stubs,
			propsData
		});

		await wrapper.setData({ centerReady: true });

		const clusterKostElement = wrapper.find('button.select-cluster');

		expect(clusterKostElement.exists()).toBe(true);

		await clusterKostElement.trigger('click');

		expect(clusterKostSpy).toBeCalled();
		expect(fetchClustersSpy).toBeCalled();

		global.innerWidth = 400;

		wrapper = shallowMount(SearchMapOsm, {
			localVue,
			store,
			stubs,
			propsData
		});

		await clusterKostElement.trigger('click');

		expect(clusterKostSpy).toBeCalled();
		expect(fetchClustersSpy).toBeCalled();
	});

	it('Should call handleResetFilter on reset filter', async () => {
		await wrapper.setData({ centerReady: true });

		const resetFilterElement = wrapper.find('button.reset-filter');

		expect(resetFilterElement.exists()).toBe(true);

		await resetFilterElement.trigger('click');

		expect(handleResetFilterSpy).toBeCalled();
	});

	it('Watch getClusterRooms should return correctly', async () => {
		wrapper.vm.$store.commit('setClusterKost', [1]);

		await wrapper.vm.$nextTick();

		expect(showClustersSpy).toBeCalled();
	});

	it('Should call getCurrentPosition method with navigator geolocation correctly', async () => {
		const testCase = [
			{
				checkUser: true,
				status: true
			},
			{
				checkUser: false,
				status: false
			}
		];

		for (const test of testCase) {
			if (test.status) {
				mockWindowProperty('navigator', {
					geolocation: {
						getCurrentPosition: jest.fn().mockImplementationOnce(success =>
							Promise.resolve(
								success({
									coords: {
										latitude: 51.1,
										longitude: 45.3
									}
								})
							)
						)
					}
				});
			} else {
				mockWindowProperty('navigator', {
					geolocation: {
						getCurrentPosition: jest
							.fn()
							.mockImplementationOnce((success, error) => {
								error(new Error('some error'));
							})
					}
				});
			}

			wrapper = shallowMount(SearchMapOsm, {
				localVue,
				store,
				propsData,
				stubs
			});

			wrapper.vm.$store.commit('setCriteria', {
				gender: null,
				keywords: '',
				latitude: -6.2839313,
				longitude: 106.7320383,
				price_max: 10000000,
				price_min: 0,
				rent_type: 2,
				zoom: 14,
				_id: null
			});

			expect(initLocationCheckSpy).toBeCalled();
			expect(getCurrentPositionSpy).toBeCalled();

			if (test.checkUser) {
				expect(handleCheckUserPositionSpy).toBeCalled();
			}
		}
	});
});
