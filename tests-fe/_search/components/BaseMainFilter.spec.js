import { createLocalVue, shallowMount } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import BaseMainFilter from 'Js/@components/BaseMainFilter';
import Vuex from 'vuex';
import SearchStore from '../__mocks__/searchStore';

const localVue = createLocalVue();
localVue.use(Vuex);
const store = new Vuex.Store(SearchStore);

const closeActionSpy = jest.spyOn(BaseMainFilter.methods, 'closeAction');
const handleCloseDropdownSpy = jest.spyOn(
	BaseMainFilter.methods,
	'handleCloseDropdown'
);

const dropdownTrue = {
	template: `<button class="dropdown" @click="$emit('input', true)" />`
};

const dropdownFalse = {
	template: `<button class="dropdown" @click="$emit('input', false)" />`
};

describe('BaseMainFilter.vue', () => {
	let wrapper = shallowMount(BaseMainFilter, {
		localVue,
		store,
		propsData: {
			filterType: 'rent_type',
			needReset: true,
			needSave: true
		}
	});

	it('Computed valueBinding should return correctly', async () => {
		await wrapper.setProps({
			dropdownData: {
				title: 'Bulanan',
				data: [
					{
						id: 1,
						value: 1,
						name: 'Mingguan',
						fac_event: 'filter_period_mingguan'
					},
					{
						id: 2,
						value: 2,
						name: 'Bulanan',
						fac_event: 'filter_period_bulanan'
					},
					{
						id: 4,
						value: 4,
						name: '3 Bulan',
						fac_event: 'filter_period_3_bulan'
					},
					{
						id: 5,
						value: 5,
						name: 'Tahunan',
						fac_event: 'filter_period_6_bulan'
					},
					{
						id: 3,
						value: 3,
						name: 'Tahunan',
						fac_event: 'filter_period_tahunan'
					}
				],
				getValue: 2,
				active: true
			}
		});
		expect(wrapper.vm.valueBinding).toEqual(2);
	});

	it('Should call method closeAction correctly', async () => {
		const saveElement = wrapper.find('button.base-save');

		expect(saveElement.exists()).toBe(true);

		await saveElement.trigger('click');

		expect(closeActionSpy).toBeCalled();
	});

	it('Should call method handleCloseDropdown with true status correctly', async () => {
		wrapper = shallowMount(BaseMainFilter, {
			localVue,
			store,
			stubs: {
				dropdown: dropdownTrue
			}
		});
		const dropdownElement = wrapper.find('button.dropdown');
		await wrapper.setProps({ isMobileProps: true });

		expect(dropdownElement.exists()).toBe(true);

		await dropdownElement.trigger('click');

		expect(handleCloseDropdownSpy).toBeCalledWith(true);
		expect(wrapper.emitted('set-value')).toBeTruthy();
		expect(wrapper.emitted('callback-action')).toBeTruthy();
		expect(closeActionSpy).toBeCalled();
	});

	it('Should call method handleCloseDropdown with false status correctly', async () => {
		wrapper = shallowMount(BaseMainFilter, {
			localVue,
			store,
			stubs: {
				dropdown: dropdownFalse
			}
		});
		await wrapper.setProps({
			saveOnCloseDropdown: true,
			isMobileProps: true,
			dropdownData: { getValue: 2 }
		});
		const dropdownElement = wrapper.find('button.dropdown');

		expect(dropdownElement.exists()).toBe(true);

		await dropdownElement.trigger('click');

		expect(handleCloseDropdownSpy).toBeCalledWith(false);
		expect(wrapper.emitted('save-action')).toBeTruthy();
	});
});
