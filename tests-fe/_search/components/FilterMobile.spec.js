import { createLocalVue, shallowMount } from '@vue/test-utils';
import 'tests-fe/utils/mock-vue';
import FilterMobile from 'Js/_search/components/FilterMobile.vue';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import Vuex from 'vuex';
import SearchStore from '../__mocks__/searchStore';

const localVue = createLocalVue();
localVue.use(Vuex);
const store = new Vuex.Store(SearchStore);

const addEvent = jest
	.spyOn(global, 'addEventListener')
	.mockImplementation(() => {});

const removeEvent = jest
	.spyOn(global, 'removeEventListener')
	.mockImplementation(() => {});

const handleCloseSortingSpy = jest.spyOn(
	FilterMobile.methods,
	'handleCloseSorting'
);

const handleToggleMenuSpy = jest.spyOn(
	FilterMobile.methods,
	'handleToggleMenu'
);

const handleScrollSpy = jest.spyOn(FilterMobile.methods, 'handleScroll');

const modalSortingMobileStub = {
	template: `<button class="close-sorting" @click="$emit('toggle-modal-handler')" />`
};

const modalFilterMobileStub = {
	template: `<button class="close-filter" @click="$emit('toggle-modal-handler', 'Filter')" />`
};

global.scrollTo = jest.fn();

describe('FilterMobile.vue', () => {
	let wrapper = shallowMount(FilterMobile, {
		localVue,
		store,
		stubs: {
			'modal-sorting-mobile': modalSortingMobileStub,
			'modal-filter-mobile': modalFilterMobileStub
		}
	});

	it('Should call method handleToggleModal to open sorting modal correctly', async () => {
		const openSortingElement = wrapper
			.findAll('button.filter-mobile__button')
			.at(1);

		await openSortingElement.trigger('click');

		expect(handleToggleMenuSpy).toBeCalledWith('Urutkan');
		expect(wrapper.vm.isShowSortingModal).toBe(true);
	});

	it('Should call method handleCloseSorting correctly', async () => {
		await wrapper.setData({ isShowSortingModal: true });
		const closeSortingElement = wrapper.find('button.close-sorting');

		expect(closeSortingElement.exists()).toBe(true);

		await closeSortingElement.trigger('click');

		expect(handleCloseSortingSpy).toBeCalled();
		expect(wrapper.vm.isShowSortingModal).toBe(false);
	});

	it('Should call method handleToggleModal to close filter modal correctly', async () => {
		await wrapper.setData({ isShowFilterModal: true });
		const closeFilterElement = wrapper.find('button.close-filter');

		expect(closeFilterElement.exists()).toBe(true);

		await closeFilterElement.trigger('click');

		expect(handleToggleMenuSpy).toBeCalledWith('Filter');
		expect(wrapper.vm.isShowSortingModal).toBe(false);
	});

	it('Should call handleScroll on scroll correctly', async () => {
		const testCase = [
			{
				pageYOffset: 1000,
				lastScrollPosition: 50,
				ref: true
			},
			{
				pageYOffset: 1000,
				lastScrollPosition: 1200,
				ref: true
			},
			{
				pageYOffset: 1000,
				lastScrollPosition: 1200,
				ref: false
			}
		];

		const mockMapEventListener = {};
		window.addEventListener = jest.fn((event, cb) => {
			mockMapEventListener[event] = cb;
		});

		for (const test of testCase) {
			mockWindowProperty('pageYOffset', test.pageYOffset);

			wrapper = shallowMount(FilterMobile, {
				localVue,
				store
			});

			if (!test.ref) wrapper.vm.$refs.filterButton = null;

			await wrapper.setData({ lastScrollPosition: test.lastScrollPosition });
			await mockMapEventListener.scroll({ scrollTo: test.lastScrollPosition });

			expect(handleScrollSpy).toBeCalled();
		}
	});

	it('Should remove listener on destroy', () => {
		expect(addEvent).toHaveBeenCalled();

		wrapper.destroy();

		expect(removeEvent).toHaveBeenCalled();
	});
});
