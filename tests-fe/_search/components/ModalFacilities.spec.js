import { createLocalVue, shallowMount } from '@vue/test-utils';
import { modal } from 'vue-strap';
import 'tests-fe/utils/mock-vue';
import ModalFacilities from 'Js/_search/components/ModalFacilities.vue';
import Vuex from 'vuex';
import SearchStore from '../__mocks__/searchStore';
const localVue = createLocalVue();
localVue.use(Vuex);
const store = new Vuex.Store(SearchStore);

const resetHandlerSpy = jest.spyOn(ModalFacilities.methods, 'resetHandler');

describe('ModalFacilities.vue', () => {
	const wrapper = shallowMount(ModalFacilities, {
		localVue,
		store,
		stubs: {
			modal
		}
	});

	it('Should watch isShowModal correctly', async () => {
		await wrapper.setProps({
			facilitiesData: {
				title: 'Fasilitas Bersama',
				facilityCategories: [
					{
						data: [
							{
								value: 1,
								name: 'Fasilitas 1'
							},
							{
								value: 2,
								name: 'Fasilitas 2'
							},
							{
								value: 3,
								name: 'Fasilitas 3'
							}
						]
					}
				],
				getValue: 2,
				count: 0,
				active: false,
				allFacTag: []
			},
			isShowModal: true
		});

		expect(wrapper.vm.facTagData).toEqual(2);
	});

	it('Should call resetHandler method correctly', async () => {
		const resetElement = wrapper.find('button.search-btn-reset');

		expect(resetElement.exists()).toBe(true);

		resetElement.trigger('click');

		expect(resetHandlerSpy).toBeCalled();
	});
});
