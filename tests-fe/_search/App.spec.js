import { createLocalVue, shallowMount } from '@vue/test-utils';
import '@babel/polyfill';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import mockComponent from 'tests-fe/utils/mock-component';
import App from 'Js/_search/App.vue';
import Vuex from 'vuex';
import SearchStore from './__mocks__/searchStore';
/* eslint-disable-next-line */
window.Vue = require('vue');
// eslint-disable-next-line
mockWindowProperty('tracker', jest.fn());
const mixinNavigator = require('Js/@mixins/MixinNavigatorIsMobile');
const localVue = createLocalVue();
localVue.mixin(mixinNavigator);
localVue.use(Vuex);
const store = new Vuex.Store(SearchStore);

describe('App.vue', () => {
	const moengageEventTrackSpy = jest.spyOn(App.methods, 'moengageEventTrack');
	mockWindowProperty('innerWidth', 1366);
	shallowMount(App, {
		localVue,
		stubs: {
			'router-view': mockComponent
		},
		store
	});

	it('Should call moengageEventTrack on mounted with mobile device', () => {
		expect(moengageEventTrackSpy).toHaveBeenCalled();
	});

	it('Should call moengageEventTrack on mounted with desktop device', () => {
		mockWindowProperty('innerWidth', 400);
		shallowMount(App, {
			localVue,
			stubs: {
				'router-view': mockComponent
			},
			store
		});
		expect(moengageEventTrackSpy).toHaveBeenCalled();
	});
});
