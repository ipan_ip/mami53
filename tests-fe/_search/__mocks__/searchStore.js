import { translateWords } from 'Js/@utils/langTranslator.js';
import { makeAPICall } from 'Js/@utils/makeAPICall.js';
import { getParams } from 'Js/@utils/queryString';
import {
	reservedKeys,
	reservedValues,
	sanitizeFilterTag
} from 'Js/@utils/kostFilter';

export default {
	state: {
		authCheck: {},
		authData: {},
		criteria: {},
		searchModalStatus: false,
		kos: {
			params: {
				filters: {
					gender: [],
					price_range: [10000, 20000000],
					tag_ids: [],
					level_info: [],
					rent_type: 2,
					property_type: 'all',
					random_seeds: Math.floor(Math.random() * (1000 - 0 + 1)) + 0,
					booking: false,
					flash_sale: false,
					mamichecker: false,
					mamirooms: false,
					goldplus: []
				},
				sorting: {
					field: 'price',
					direction: '-'
				},
				location: [],
				point: {},
				include_promoted: false,
				limit: 20,
				offset: 0
			},
			isGoldPlus: false,
			kosList: [],
			clusterList: [],
			isEmptyList: false,
			totalRooms: 0,
			responseData: null,
			hasMore: false,
			tagRooms: [],
			tagShare: [],
			tagRules: [],
			rentTypeList: []
		},
		extras: {
			hasMap: window.innerWidth > 991,
			geolocActive: false,
			clusterOpen: false,
			changeFilters: false
		},
		isMoreLoading: false,
		isLoading: true,
		isFacLoading: true,
		historyPoint: -1
	},
	getters: {
		getAllParams: state => {
			return state.kos.params;
		},
		getCriteria: state => {
			return state.criteria;
		},
		getLocation: state => {
			return state.kos.params.location;
		},
		getOffsetParams: state => {
			return state.kos.params.offset;
		},
		getAuthCheck: state => {
			return state.authCheck;
		},
		getGeolocActive: state => {
			return state.extras.geolocActive;
		},
		getAuthData: state => {
			return state.authData;
		},
		getKostData: state => {
			return state.kos;
		},
		getKostSorting: state => {
			return [
				state.kos.params.sorting.field,
				state.kos.params.sorting.direction
			];
		},
		getKostFilters: state => {
			return state.kos.params.filters;
		},
		getSearchStatus: state => {
			return state.searchModalStatus;
		},
		getIsLoading: state => {
			return state.isLoading;
		},
		getFacLoading: state => {
			return state.isFacLoading;
		},
		getIsMoreLoading: state => {
			return state.isMoreLoading;
		},
		getEmptyList: state => {
			return state.kos.isEmptyList;
		},
		getRooms: state => {
			return state.kos.kosList;
		},
		getClusterRooms: state => {
			return state.kos.clusterList;
		},
		getTotalRooms: state => {
			return state.kos.totalRooms;
		},
		getHasMore: state => {
			return state.kos.hasMore;
		},
		getHasMap: state => {
			return state.extras.hasMap;
		},
		getTagRooms: state => {
			return state.kos.tagRooms;
		},
		getTagShare: state => {
			return state.kos.tagShare;
		},
		getTagRules: state => {
			return state.kos.tagRules;
		},
		getRentTypeList: state => {
			return state.kos.rentTypeList;
		},
		getClusterStatus: state => {
			return state.extras.clusterOpen;
		},
		getResponseData: state => {
			return state.kos.responseData;
		},
		getIsFacilitiesExist: state => {
			return state.kos.tagRooms.length && state.kos.tagShare.length;
		}
	},
	mutations: {
		setFlashSaleAvailability(state, payload) {
			state.isFlashSaleAvailable = payload;
		},
		setAuthCheck(state, payload) {
			state.authCheck = payload;
		},
		setIsGoldPlus(state, payload) {
			state.isGoldPlus = payload;
		},
		setAuthData(state, payload) {
			state.authData = payload;
		},
		setCriteria(state, payload) {
			state.criteria = payload;
		},
		setSearchStatus(state, payload) {
			state.searchModalStatus = payload;
		},
		setClusterStatus(state, payload) {
			state.extras.clusterOpen = payload;
		},
		setFilter(state, payload) {
			state.kos.params.filters = {
				...state.kos.params.filters,
				...payload
			};
			if (state.extras.clusterOpen) state.extras.clusterOpen = false;
		},
		setSort(state, payload) {
			state.kos.params.sorting.field = payload[0];
			state.kos.params.sorting.direction = payload[1];
			if (state.extras.clusterOpen) state.extras.clusterOpen = false;
		},
		setLoading(state, payload) {
			state.isLoading = payload;
		},
		setFacLoading(state, payload) {
			state.isFacLoading = payload;
		},
		setMoreLoading(state, payload) {
			state.isMoreLoading = payload;
		},
		setEmptyList(state, payload) {
			state.kos.isEmptyList = payload;
		},
		setHasMore(state, payload) {
			state.kos.hasMore = payload;
		},
		setRooms(state, payload) {
			if (payload.status) {
				if (payload.isRequestMore) {
					state.kos.kosList = [...state.kos.kosList, ...payload.rooms];
				} else {
					state.kos.kosList = payload.rooms;
				}
				state.kos.totalRooms = payload.total;
			} else {
				state.kos.kosList = [];
				state.kos.totalRooms = 0;
			}
		},
		setOffset(state, payload) {
			state.kos.params.offset = payload;
		},
		setTagRooms(state, payload) {
			const tagRoomsList = sanitizeFilterTag(payload);
			state.kos.tagRooms = tagRoomsList;
		},
		setTagShare(state, payload) {
			const tagShareList = sanitizeFilterTag(payload);
			state.kos.tagShare = tagShareList;
		},
		setTagRules(state, payload) {
			const tagRulesList = sanitizeFilterTag(payload);
			state.kos.tagRules = tagRulesList;
		},
		setRentTypeList(state, payload) {
			const rentList = payload.map(rent => {
				return {
					...rent,
					value: rent.id,
					name: rent.rent_name
				};
			});
			state.kos.rentTypeList = rentList;
		},
		setClusterKost(state, payload) {
			state.kos.clusterList = payload;
		},
		setCriteriaLngLat(state, payload) {
			state.criteria.latitude = payload.lat;
			state.criteria.longitude = payload.lng;
		},
		setMapCenter(state, payload) {
			state.criteria.latitude = payload.lat;
			state.criteria.longitude = payload.lng;
		},
		setLocation(state, payload) {
			state.kos.params.location = payload;
		},
		setPoint(state, payload) {
			state.kos.params.point = payload;
		},
		setExtrasGeolocActive(state, payload) {
			state.extras.geolocActive = payload;
		},
		setResponseData(state, payload) {
			state.kos.responseData = payload;
		},
		setHistoryPoint(state) {
			state.historyPoint = state.historyPoint - 1;
		}
	},
	actions: {
		settingFilter({ commit, dispatch }, payload) {
			commit('setFilter', payload);
			dispatch('fetchRooms');
		},
		settingSort({ commit, dispatch }, payload) {
			commit('setSort', payload);
			dispatch('fetchRooms');
		},
		settingTagIds({ commit, dispatch }, payload) {
			commit('setFilter', payload.content);
			if (payload.search) {
				dispatch('fetchRooms');
			}
		},
		settingAllFilter({ commit, state, dispatch }, payload) {
			commit('setSort', payload.sortingPayload);
			const filters = {
				...payload.filterPayload,
				tag_ids: state.mainData[state.activeType].params.filters.tag_ids
			};
			commit('setFilter', filters);
			dispatch('fetchRooms');
		},
		async fetchFacilitiesList({ commit }) {
			commit('setFacLoading', true);
			const response = await makeAPICall({
				method: 'get',
				url: '/stories/facility'
			});

			if (response) {
				commit('setTagList', response.facs);
				commit('setTagListOthers', response.other_facs);
				commit('setFacLoading', false);
			} else {
				commit('setFacLoading', false);
			}
		},
		async fetchRooms({ commit, state, dispatch }, loadingMore = false) {
			if (state.extras.clusterOpen) {
				dispatch('fetchClusters', loadingMore);
			} else {
				if (loadingMore) {
					commit('setMoreLoading', true);
					const offset =
						state.mainData[state.activeType][`${state.activeType}List`].length;
					commit('setOffset', offset);
				} else {
					commit('setLoading', true);
					commit('setOffset', 0);
				}
				if (state.mainData[state.activeType].params.location.length === 0) {
					const payload = [
						[106.69793128967285, -6.296188782911225],
						[106.76608085632326, -6.271703381316111]
					];
					commit('setLocation', payload);
				}
				const params = state.mainData[state.activeType].params;
				const response = await makeAPICall({
					method: 'post',
					url: '/stories/list',
					data: params
				});

				if (response) {
					const rooms = translateWords(response.rooms) || [];
					if (rooms.length > 0) {
						const payload = {
							status: true,
							rooms,
							total: response.total,
							isRequestMore: loadingMore
						};
						commit('setEmptyList', false);
						commit('setRooms', payload);
					} else {
						commit('setEmptyList', true);
						commit('setRooms', {
							status: false
						});
					}
					commit('setHasMore', response.data['has-more']);
					commit('setResponseData', response.data);
					loadingMore
						? commit('setMoreLoading', false)
						: commit('setLoading', false);
				} else {
					commit('setEmptyList', true);
					commit('setRooms', {
						status: false
					});
					commit('setHasMore', false);
					loadingMore
						? commit('setMoreLoading', false)
						: commit('setLoading', false);
				}

				if (state.extras.hasMap) {
					const resRoomCluster = await makeAPICall({
						method: 'post',
						url: '/stories/cluster',
						data: params
					});
					if (resRoomCluster) {
						commit('setClusterKost', resRoomCluster.rooms);
					}
				}
			}
		},
		async fetchClusters({ commit, state }, loadingMore = false) {
			if (loadingMore) {
				commit('setMoreLoading', true);
				const offset =
					state.mainData[state.activeType][`${state.activeType}List`].length;
				commit('setOffset', offset);
			} else {
				commit('setLoading', true);
				commit('setOffset', 0);
			}
			if (state.mainData[state.activeType].params.location.length === 0) {
				const payload = [
					[106.69793128967285, -6.296188782911225],
					[106.76608085632326, -6.271703381316111]
				];
				commit('setLocation', payload);
			}
			if (!state.extras.clusterOpen) commit('setClusterStatus', true);
			const params = state.mainData[state.activeType].params;
			const response = await makeAPICall({
				method: 'post',
				url: '/room/list/cluster',
				data: params
			});
			if (response) {
				const rooms = response.rooms;
				if (rooms.length > 0) {
					const payload = {
						status: true,
						rooms,
						total: response.total,
						isRequestMore: loadingMore
					};
					commit('setEmptyList', false);
					commit('setRooms', payload);
				} else {
					commit('setEmptyList', true);
					commit('setRooms', {
						status: false
					});
				}
				commit('setHasMore', response['has-more']);
				commit('setResponseData', response);
				loadingMore
					? commit('setMoreLoading', false)
					: commit('setLoading', false);
			} else {
				commit('setEmptyList', true);
				commit('setRooms', {
					status: false
				});
				commit('setHasMore', false);
				loadingMore
					? commit('setMoreLoading', false)
					: commit('setLoading', false);
			}
		}
	}
};
