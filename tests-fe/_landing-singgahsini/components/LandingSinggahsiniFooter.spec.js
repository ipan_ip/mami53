import { shallowMount, createLocalVue } from '@vue/test-utils';
import LandingSinggahsiniFooter from 'Js/_landing-singgahsini/components/LandingSinggahsiniFooter.vue';

const localVue = createLocalVue();

describe('LandingSinggahsiniFooter.vue', () => {
	const wrapper = shallowMount(LandingSinggahsiniFooter, { localVue });

	it('should render the component', () => {
		expect(wrapper.find('.landing-singgahsini-footer').exists()).toBe(true);
	});
});
