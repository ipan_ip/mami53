import { shallowMount, createLocalVue } from '@vue/test-utils';
import LandingSinggahsiniTestimoni from 'Js/_landing-singgahsini/components/LandingSinggahsiniTestimoni.vue';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

const localVue = createLocalVue();
mockVLazy(localVue);

describe('LandingSinggahsiniTestimoni.vue', () => {
	it('should render the component', () => {
		const wrapper = shallowMount(LandingSinggahsiniTestimoni, { localVue });
		expect(wrapper.find('.home-testimoni').exists()).toBe(true);
	});

	it('should return pagination for mobile device', () => {
		mockWindowProperty('innerWidth', 700);
		const wrapper = shallowMount(LandingSinggahsiniTestimoni, { localVue });
		expect(wrapper.vm.swiperOption.spaceBetween).toBe(8);
	});

	it('should return pagination for desktop device', () => {
		mockWindowProperty('innerWidth', 1199);
		const wrapper = shallowMount(LandingSinggahsiniTestimoni, { localVue });
		expect(wrapper.vm.swiperOption.spaceBetween).toBe(24);
	});
});
