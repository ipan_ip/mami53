import { shallowMount, createLocalVue } from '@vue/test-utils';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import LandingSinggahsiniFaq from 'Js/_landing-singgahsini/components/LandingSinggahsiniFaq.vue';

const localVue = createLocalVue();
mockVLazy(localVue);

describe('LandingSinggahsiniFaq.vue', () => {
	const wrapper = shallowMount(LandingSinggahsiniFaq, {
		localVue
	});

	it('should load the component', () => {
		expect(wrapper.find('.singgahsini-faqs').exists()).toBe(true);
	});

	it('should toggle accordion class', () => {
		expect(wrapper.vm.isActive('')).toBe('--active');
		expect(wrapper.vm.isActive('open')).toBe('--closed');
	});

	it('should assign activeAccordion value', () => {
		wrapper.vm.faqClicked('open');
		expect(wrapper.vm.activeAccordion).toBe('open');
		wrapper.vm.activeAccordion = 'closed';
		wrapper.vm.faqClicked('closed');
		expect(wrapper.vm.activeAccordion).toBe('closed');
	});
});
