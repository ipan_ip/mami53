import { shallowMount, createLocalVue } from '@vue/test-utils';
import App from 'Js/_landing-singgahsini/components/App.vue';

const localVue = createLocalVue();

describe('App.vue', () => {
	const wrapper = shallowMount(App, { localVue });

	it('should load the component', () => {
		expect(wrapper.find('.landing-singgahsini-warp').exists()).toBe(true);
	});
});
