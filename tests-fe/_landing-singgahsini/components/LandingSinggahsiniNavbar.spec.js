import { shallowMount, createLocalVue } from '@vue/test-utils';
import LandingSinggahsiniNavbar from 'Js/_landing-singgahsini/components/LandingSinggahsiniNavbar.vue';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

const localVue = createLocalVue();
mockWindowProperty('document.getElementById', () => ({
	offsetHeight: 400
}));

describe('LandingSinggahsiniNavbar.vue', () => {
	const wrapper = shallowMount(LandingSinggahsiniNavbar, { localVue });

	it('should load the component', () => {
		expect(wrapper.find('.landing-singgahsini-navbar').exists()).toBe(true);
	});

	it('should assign mobile menu value', () => {
		wrapper.vm.toggleMenu(true);
		expect(wrapper.vm.showMobileMenu).toBe(true);
	});
});
