import { shallowMount, createLocalVue } from '@vue/test-utils';
import LandingSinggahsiniJoinStep from 'Js/_landing-singgahsini/components/LandingSinggahsiniJoinStep.vue';

const localVue = createLocalVue();

describe('LandingSinggahsiniJoinStep.vue', () => {
	const wrapper = shallowMount(LandingSinggahsiniJoinStep, { localVue });

	it('should render the component', () => {
		expect(wrapper.find('.landing-singgahsini-join-step').exists()).toBe(true);
	});
});
