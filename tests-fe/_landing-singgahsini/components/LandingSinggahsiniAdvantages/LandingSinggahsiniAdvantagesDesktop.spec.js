import { shallowMount, createLocalVue } from '@vue/test-utils';
import LandingSinggahsiniAdvantagesDesktop from 'Js/_landing-singgahsini/components/LandingSinggahsiniAdvantages/LandingSinggahsiniAdvantagesDesktop.vue';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

const localVue = createLocalVue();
mockVLazy(localVue);

describe('LandingSinggahsiniAdvantagesDesktop.vue', () => {
	const propsData = {
		startIndex: 0,
		advantages: [
			{
				title: 'Kerja Sama Yang Profesional',
				description:
					'Berkoordinasi transparan dengan pemilik properti untuk pengelolaan dan kebijakan kos.',
				image: {
					src:
						'/general/img/pictures/landing-singgahsini/advantages/svg/landing-singgahsini-kerjasama.svg'
				},
				hasVideo: false
			},
			{
				title: 'Standardisasi Properti',
				description:
					'Konsultasi perawatan aset dan perbaikan fasilitas dari tim Singgahsini.',
				image: {
					src:
						'/general/img/pictures/landing-singgahsini/advantages/svg/landing-singgahsini-standardisasi.svg'
				},
				hasVideo: false
			}
		]
	};
	const wrapper = shallowMount(LandingSinggahsiniAdvantagesDesktop, {
		localVue,
		propsData
	});

	it('should load the component', () => {
		expect(wrapper.find('.advantage-desktop').exists()).toBe(true);
	});

	it('should move to the next slide', () => {
		wrapper.vm.currentIndex = 0;
		wrapper.vm.nextSlide();
		expect(wrapper.vm.currentIndex).toBe(1);
	});

	it('should move to the prev slide', () => {
		wrapper.vm.currentIndex = 4;
		wrapper.vm.prevSlide();
		expect(wrapper.vm.currentIndex).toBe(3);
	});

	it('should go to the nth slide', () => {
		wrapper.vm.goToSlide(3);
		expect(wrapper.vm.currentIndex).toBe(3);
	});

	it('should auto slide to start index', () => {
		wrapper.vm.currentIndex = 5;
		wrapper.vm.autoSlide();
		wrapper.vm.currentIndex = 0;
	});
});
