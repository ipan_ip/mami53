import { shallowMount, createLocalVue } from '@vue/test-utils';
import SliderButton from 'Js/_landing-singgahsini/components/LandingSinggahsiniAdvantages/components/SliderButton.vue';

const localVue = createLocalVue();

describe('SliderButton.vue', () => {
	const propsData = { totalAdvantage: 5, currentIndex: 0 };
	const wrapper = shallowMount(SliderButton, { localVue, propsData });

	it('should load the component', () => {
		expect(wrapper.find('.slider-button').exists()).toBe(true);
	});
});
