import { shallowMount, createLocalVue } from '@vue/test-utils';
import SliderList from 'Js/_landing-singgahsini/components/LandingSinggahsiniAdvantages/components/SliderList.vue';

const localVue = createLocalVue();
const mockedSliderListDOM = {
	scrollWidth: 375,
	clientWidth: 375,
	scrollLeft: 0
};
global.document.getElementById = () => mockedSliderListDOM;

describe('SliderList.vue', () => {
	const propsData = {
		advantages: [
			{
				title: 'Kerja Sama Yang Profesional',
				description:
					'Berkoordinasi transparan dengan pemilik properti untuk pengelolaan dan kebijakan kos.',
				image: {
					src:
						'/general/img/pictures/landing-singgahsini/advantages/svg/landing-singgahsini-kerjasama.svg'
				},
				hasVideo: false
			},
			{
				title: 'Standardisasi Properti',
				description:
					'Konsultasi perawatan aset dan perbaikan fasilitas dari tim Singgahsini.',
				image: {
					src:
						'/general/img/pictures/landing-singgahsini/advantages/svg/landing-singgahsini-standardisasi.svg'
				},
				hasVideo: false
			}
		],
		currentIndex: 0
	};
	const wrapper = shallowMount(SliderList, { localVue, propsData });

	it('should load the component', () => {
		expect(wrapper.find('.advantage-list').exists()).toBe(true);
	});
});
