import { shallowMount, createLocalVue } from '@vue/test-utils';
import index from 'Js/_landing-singgahsini/components/LandingSinggahsiniAdvantages/index.vue';

const localVue = createLocalVue();

describe('index.vue', () => {
	const wrapper = shallowMount(index, { localVue });

	it('should load the component', () => {
		expect(wrapper.find('.advantages').exists()).toBe(true);
	});
});
