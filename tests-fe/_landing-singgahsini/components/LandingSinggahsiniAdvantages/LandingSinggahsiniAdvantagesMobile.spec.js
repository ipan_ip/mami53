import { shallowMount, createLocalVue } from '@vue/test-utils';
import LandingSinggahsiniAdvantagesMobile from 'Js/_landing-singgahsini/components/LandingSinggahsiniAdvantages/LandingSinggahsiniAdvantagesMobile.vue';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

const localVue = createLocalVue();
mockVLazy(localVue);

describe('LandingSinggahsiniAdvantagesMobile.vue', () => {
	const propsData = {
		advantages: [
			{
				title: 'Kerja Sama Yang Profesional',
				description:
					'Berkoordinasi transparan dengan pemilik properti untuk pengelolaan dan kebijakan kos.',
				image: {
					src:
						'/general/img/pictures/landing-singgahsini/advantages/svg/landing-singgahsini-kerjasama.svg'
				},
				hasVideo: false
			},
			{
				title: 'Standardisasi Properti',
				description:
					'Konsultasi perawatan aset dan perbaikan fasilitas dari tim Singgahsini.',
				image: {
					src:
						'/general/img/pictures/landing-singgahsini/advantages/svg/landing-singgahsini-standardisasi.svg'
				},
				hasVideo: false
			}
		]
	};
	const wrapper = shallowMount(LandingSinggahsiniAdvantagesMobile, {
		localVue,
		propsData
	});

	it('should load the component', () => {
		expect(wrapper.find('.advantage-mobile').exists()).toBe(true);
	});
});
