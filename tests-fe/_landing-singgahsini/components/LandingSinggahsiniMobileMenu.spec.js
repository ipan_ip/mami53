import { shallowMount, createLocalVue } from '@vue/test-utils';
import LandingSinggahsiniMobileMenu from 'Js/_landing-singgahsini/components/LandingSinggahsiniMobileMenu.vue';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

const localVue = createLocalVue();

describe('LandingSinggahsiniMobileMenu.vue', () => {
	const propsData = { show: true, menus: [{ link: '/about', label: 'about' }] };
	const wrapper = shallowMount(LandingSinggahsiniMobileMenu, {
		localVue,
		propsData
	});

	it('should render the component', () => {
		expect(wrapper.find('.mobile-navbar-menu').exists()).toBe(true);
	});

	it('should emit close action', () => {
		wrapper.vm.closeMenu();
		expect(wrapper.emitted('toggleMenu')).toBeTruthy();
	});

	it('should return singgahsini/daftar registration url', () => {
		mockWindowProperty('window.location', {
			pathname: '/singgahsini/'
		});
		expect(wrapper.vm.registerUrl).toBe('singgahsini/daftar');
	});
});
