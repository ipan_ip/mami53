import { shallowMount, createLocalVue } from '@vue/test-utils';
import LandingSinggahsiniGallery from 'Js/_landing-singgahsini/components/LandingSinggahsiniGallery.vue';
import Vuex from 'vuex';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';

const localVue = createLocalVue();
localVue.use(Vuex);
mockVLazy(localVue);

describe('LandingSinggahsiniGallery.vue', () => {
	const mockStore = {
		state: {
			roomsData: [
				{
					area_name: 'Condongcatur',
					city: 'Sleman',
					price: 9000000,
					price_title_format: 'Rp9000.000',
					photo_list: 'mamikos.com',
					photo_url: 'mamikos.com',
					room_title: 'Singgahsini'
				}
			]
		},
		getters: {
			getRooms: state => {
				return state.roomsData;
			}
		}
	};
	const wrapper = shallowMount(LandingSinggahsiniGallery, {
		localVue,
		store: new Vuex.Store(mockStore)
	});

	it('should render the component', () => {
		expect(wrapper.find('.landing-singgahsini-gallery').exists()).toBe(true);
	});
});
