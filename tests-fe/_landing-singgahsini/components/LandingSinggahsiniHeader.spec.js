import { shallowMount, createLocalVue } from '@vue/test-utils';
import LandingSinggahsiniHeader from 'Js/_landing-singgahsini/components/LandingSinggahsiniHeader.vue';
import mockVLazy from 'tests-fe/utils/mock-v-lazy';
import mockWindowProperty from 'tests-fe/utils/mock-window-property';

const localVue = createLocalVue();
mockVLazy(localVue);

describe('LandingSinggahsiniHeader.vue', () => {
	it('should render the component', () => {
		const wrapper = shallowMount(LandingSinggahsiniHeader, {
			localVue
		});
		expect(wrapper.find('.landing-singgahsini-header').exists()).toBe(true);
	});

	it('should return singgahsini logo all white', () => {
		mockWindowProperty('innerWidth', 700);
		const wrapper = shallowMount(LandingSinggahsiniHeader, {
			localVue
		});
		expect(wrapper.vm.logoImage).toBe(
			'general/img/pictures/landing-singgahsini/singgahsini-logo-all-white.png'
		);
	});

	it('should return singgahsini logo reguler', () => {
		mockWindowProperty('innerWidth', 1199);
		const wrapper = shallowMount(LandingSinggahsiniHeader, {
			localVue
		});
		expect(wrapper.vm.logoImage).toBe(
			'general/img/pictures/landing-singgahsini/singgahsini-logo-white.png'
		);
	});
});
