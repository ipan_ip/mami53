const storeManageBooking = {
	state: {
		listBooking: {},
		listBookingSelected: '',
		cancelTokenBookingList: [],
		acceptBookingProfile: '',
		isLoadingBookingList: false,
		searchBooking: {
			limit: '',
			offset: '',
			perPage: 3,
			totalPage: 0,
			sort: 'desc',
			order: 'created_at',
			group: '&group[]=0',
			selectedNumberPagination: 1
		},
		extras: {
			rejectBookingFrom: ''
		},
		selectedDetailBooking: null,
		ownerTermsAgreement: false
	},
	mutations: {
		setSearchBookingSort(state, payload) {
			state.searchBooking.sort = payload;
		},
		setBookingSelectedNumberPagination(state, payload) {
			state.searchBooking.selectedNumberPagination = payload;
		},
		setSearchBookingGroup(state, payload) {
			state.searchBooking.group = payload;
		},
		setSearchBookingOrder(state, payload) {
			state.searchBooking.order = payload;
		},
		setSearchBookingLimit(state, payload) {
			state.searchBooking.limit = payload;
		},
		setSearchBookingOffset(state, payload) {
			state.searchBooking.offset = payload;
		},
		setListBooking(state, payload) {
			state.listBooking = payload;
		},
		setListBookingSelected(state, payload) {
			state.listBookingSelected = payload;
		},
		setSearchBookingPerPage(state, payload) {
			state.searchBooking.perPage = payload;
		},
		setSearchBookingTotalPage(state, payload) {
			state.searchBooking.totalPage = payload;
		},
		setAcceptBookingProfile(state, payload) {
			state.acceptBookingProfile = payload;
		},
		setRejectBookingFrom(state, payload) {
			state.extras.rejectBookingFrom = payload;
		},
		setBookingListLoading(state, payload) {
			state.isLoadingBookingList = payload;
		},
		setSelectedDetailBooking(state, payload) {
			state.selectedDetailBooking = payload;
		},
		setOwnerTermsAgreement(state, payload) {
			state.ownerTermsAgreement = payload;
		},
		addCancelTokenBookingList(state, payload) {
			state.cancelTokenBookingList.push(payload);
		},
		clearCancelTokenBookingList(state) {
			state.cancelTokenBookingList = [];
		}
	},
	actions: {
		cancelPendingBookingListRequest({ state, commit }) {
			state.cancelTokenBookingList.forEach(request => {
				request && request.cancel && request.cancel();
			});

			commit('clearCancelTokenBookingList');
		}
	}
};

export default storeManageBooking;
