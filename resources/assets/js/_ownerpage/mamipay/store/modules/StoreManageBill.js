const currentTime = new Date();

const storeManageBill = {
	state: {
		listBill: {},
		listBillReport: {},
		searchBill: {
			month: currentTime.getMonth() + 1,
			year: currentTime.getFullYear(),
			criteria: '',
			limit: '',
			offset: '',
			perPage: 3,
			totalPage: 0,
			sort: 'desc',
			order: 'name',
			billDatePeriode: '',
			selectedNumberPagination: 1
		},
		billReport: {
			month: currentTime.getMonth() + 1,
			year: currentTime.getFullYear()
		}
	},
	mutations: {
		setListBill(state, payload) {
			state.listBill = payload;
		},
		setListBillReport(state, payload) {
			state.listBillReport = payload;
		},
		setSearchBillMonth(state, payload) {
			state.searchBill.month = payload;
		},
		setSearchBillYear(state, payload) {
			state.searchBill.year = payload;
		},
		setSearchBillCriteria(state, payload) {
			state.searchBill.criteria = payload;
		},
		setSearchBillLimit(state, payload) {
			state.searchBill.limit = payload;
		},
		setSearchBillOffset(state, payload) {
			state.searchBill.offset = payload;
		},
		setSearchBillTotalPage(state, payload) {
			state.searchBill.totalPage = payload;
		},
		setSearchBillOrder(state, payload) {
			state.searchBill.order = payload;
		},
		setSearchBillSort(state, payload) {
			state.searchBill.sort = payload;
		},
		setSearchBillPerPage(state, payload) {
			state.searchBill.perPage = payload;
		},
		setBillDatePeriode(state, payload) {
			state.searchBill.billDatePeriode = payload;
		},
		setBillSelectedNumberPagination(state, payload) {
			state.searchBill.selectedNumberPagination = payload;
		},
		setBillReportYear(state, payload) {
			state.billReport.year = payload;
		},
		setBillReportMonth(state, payload) {
			state.billReport.month = payload;
		}
	}
};

export default storeManageBill;
