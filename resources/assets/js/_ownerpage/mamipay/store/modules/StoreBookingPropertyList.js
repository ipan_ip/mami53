const storeBookingPropertyList = {
	namespaced: true,
	state: {
		propertyList: [],
		isLoadingProperties: false,
		isLoadingMore: false,
		perScreenLimit: 9,
		hasMoreThanCurrentOffsetLimit: false,
		currentOffset: 0,
		selectedFilter: 'all',
		isSelectAll: false,
		numberOfSelectedProperties: 0,
		selectedProperties: []
	},
	getters: {
		selectableProperties: state => {
			const mappedItems = state.propertyList.map(e => {
				return e.bbk_status.key === 'not_active' ? e.id : null;
			});

			return mappedItems.filter(e => e !== null);
		},
		isSelectionControlShown: state => {
			return (
				!state.isLoading &&
				!state.propertyList.every(
					e => ['approve', 'waiting'].indexOf(e.bbk_status.key) >= 0
				)
			);
		},
		selectedPropertiesInDetail: state => {
			return state.propertyList.filter(
				e => state.selectedProperties.indexOf(e.id) !== -1
			);
		}
	},
	mutations: {
		setPropertyList(state, payload) {
			if (payload.length > 0) {
				const newList = state.propertyList.concat(payload);
				state.propertyList = newList;
			} else state.propertyList = payload;
		},
		setIsLoadingProperties(state, payload) {
			state.isLoadingProperties = payload;
		},
		setIsLoadingMore(state, payload) {
			state.isLoadingMore = payload;
		},
		setHasMoreThanCurrentOffsetLimit(state, payload) {
			state.hasMoreThanCurrentOffsetLimit = payload;
		},
		setCurrentOffset(state, payload) {
			state.currentOffset = payload;
		},
		setSelectedFilter(state, payload) {
			state.selectedFilter = payload;
		},
		setIsSelectAll(state, payload) {
			state.isSelectAll = payload;
		},
		setNumberOfSelectedProperties(state, payload) {
			state.numberOfSelectedProperties = payload;
		},
		setSelectedProperties(state, payload) {
			state.selectedProperties = payload;
		}
	},
	actions: {
		fetchProperties({ commit, state }) {
			return new Promise((resolve, reject) => {
				axios
					.get('rooms', {
						params: {
							booking_status: state.selectedFilter,
							limit: state.perScreenLimit,
							offset: state.currentOffset,
							booking: false,
							access: 'mamikos'
						}
					})
					.then(response => {
						if (response.data.status) {
							commit('setPropertyList', response.data.data);
							commit(
								'setHasMoreThanCurrentOffsetLimit',
								response.data.has_more
							);
						} else {
							bugsnagClient.notify(JSON.stringify(response));
							alert(
								'Galat tidak diketahui.\nSilakan cek koneksi internet Anda dan coba lagi.'
							);
						}

						resolve();
					})
					.catch(error => {
						reject(error);
					});
			});
		}
	}
};

export default storeBookingPropertyList;
