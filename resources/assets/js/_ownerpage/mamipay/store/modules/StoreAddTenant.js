const initParams = () => {
	return {
		name: '',
		gender: '',
		parent_name: '',
		parent_phone_number: '',
		marital_status: '',
		occupation: '',
		email: '',
		room_id: 0,
		room_number: '',
		phone_number: '',
		photo_document_id: 0,
		photo_identifier_id: 0,
		photo_id: 0,
		billing_date: 0,
		fine_duration_type: 'day',
		fine_maximum_length: 0,
		fine_amount: 0,
		amount: 0,
		use_dp: false,
		dp_amount: 0,
		dp_date: '',
		dp_settlement_date: '',
		duration: 0,
		existing_tenant: '',
		fixed_billing: false,
		designer_room_id: '',
		start_date: '',
		additional_costs: [{ field_title: '', field_value: '' }],
		save_cost_group: false,
		owner_accept: true,
		questions: [],
		rent_type: ''
	};
};

export default {
	namespaced: true,
	state: {
		depositAmount: 0,
		ownerKostList: [],
		ownerKostListParams: {
			offset: 0,
			limit: 10,
			hasMore: false
		},
		selectedKost: null,
		selectedTenantRoom: '',
		rentDurationList: [],
		rentCountInfoType: '',
		isLoadedAdditionalCostGroup: false,
		isSelectedKostFromQuery: false,
		isMatchedQueryKost: false,
		selectedKostFromQuery: null,
		isAllBbk: false,
		queryData: {
			kost: '',
			room: ''
		},
		kostFullModal: {
			show: false,
			id: 0
		},
		webviewInterface: '',
		rentCountDictionary: {
			weekly: {
				label: 'Minggu',
				type: 'weekly',
				value: 'week'
			},
			monthly: {
				label: 'Bulan',
				type: 'monthly',
				value: 'month'
			},
			quarterly: {
				label: '3 Bulan',
				type: 'quarterly',
				value: '3_month'
			},
			semiannually: {
				label: '6 Bulan',
				type: 'semiannualy',
				value: '6_month'
			},
			yearly: {
				label: 'Tahun',
				type: 'yearly',
				value: 'year'
			}
		},
		params: initParams(),
		journey: [],
		disclaimerText: '',
		isDepositShown: true,
		redirectionSource: {
			selectKos: '',
			checkPhone: ''
		}
	},
	mutations: {
		setOwnerKostList(state, payload) {
			state.ownerKostList = payload;
		},
		setWebviewValue(state, payload) {
			state.webviewInterface = payload;
		},
		setSelectedKost(state, payload) {
			state.params = initParams();
			state.rentCountInfoType = '';
			state.selectedKost = payload;
			state.params.room_id = payload.id;
			state.isLoadedAdditionalCostGroup = false;
		},
		setRentDurationList(state, payload) {
			state.rentDurationList = payload || [];
		},
		setRentPrice(state, payload) {
			state.params.amount = payload;
		},
		setRentType(state, payload) {
			state.params.rent_type = payload;
		},
		setRentCountInfoType(state, payload) {
			state.rentCountInfoType = payload;
		},
		setRentDuration(state, payload) {
			state.params.duration = payload;
		},
		setDueDate(state, payload) {
			state.params.start_date = payload;
		},
		setTenantName(state, payload) {
			state.params.name = payload;
		},
		setTenantPhone(state, payload) {
			state.params.phone_number = payload;
		},
		setTenantGender(state, payload) {
			state.params.gender = payload;
		},
		setDesignerRoomId(state, payload) {
			state.params.designer_room_id = payload;
		},
		setDepositAmount(state, payload) {
			state.depositAmount = payload;
		},
		setFinePayment(state, payload) {
			state.params.fine_amount = payload;
		},
		setCostLabel(state, payload) {
			state.params.additional_costs[payload.index].field_title = payload.value;
		},
		setCostPrice(state, payload) {
			state.params.additional_costs[payload.index].field_value = payload.value;
		},
		setDeleteAddCostItem(state, payload) {
			state.params.additional_costs.splice(payload, 1);
		},
		setAdditionalCost(state, payload) {
			state.params.additional_costs = payload;
		},
		setSelectedTenantRoom(state, payload) {
			state.selectedTenantRoom = payload;
		},
		setFineMaxLength(state, payload) {
			state.params.fine_maximum_length = payload;
		},
		setFineDurationType(state, payload) {
			state.params.fine_duration_type = payload;
		},
		setSaveCostGroup(state, payload) {
			state.params.save_cost_group = payload;
		},
		setLoadedAdditionalCostGroup(state, payload) {
			state.isLoadedAdditionalCostGroup = payload;
		},
		setTenantInitialProfile(state, payload) {
			state.params.name = payload.tenant_name;
			state.params.gender = payload.gender_value;
			state.params.parent_name = payload.parent_name;
			state.params.parent_phone_number = payload.parent_phone_number;
			state.params.marital_status = payload.marital_status;
			state.params.occupation = payload.occupation;
			state.params.email = payload.email;
			state.params.room_number = payload.room_number;
			state.params.phone_number = payload.phone_number;
			state.params.photo_document_id = payload.tenant_photo_document_id;
			state.params.photo_identifier_id = payload.tenant_photo_identifier_id;
			state.params.photo_id = payload.tenant_photo_id;
		},
		setDisclaimerText(state, payload) {
			state.disclaimerText = payload;
		},
		setIsDepositShown(state, payload) {
			state.isDepositShown = payload;
		},
		setIsSelectedKostFromQuery(state, payload) {
			state.isSelectedKostFromQuery = !!payload;
		},
		setKostListParams(state, payload) {
			state.ownerKostListParams = { ...state.ownerKostListParams, ...payload };
		},
		setIsMatchedQueryKost(state, payload) {
			state.isMatchedQueryKost = payload;
		},
		setSelectedKostFromQuery(state, payload) {
			state.selectedKostFromQuery = payload;
		},
		setQueryData(state, payload) {
			state.queryData = { ...state.queryData, ...payload };
		},
		setKostFullModal(state, payload) {
			state.kostFullModal = payload;
		},
		setIsAllBbk(state, payload) {
			state.isAllBbk = !!payload;
		},
		setRedirectionSourceSelectKos(state, payload) {
			state.redirectionSource.selectKos = payload;
		},
		setRedirectionSourceCheckPhone(state, payload) {
			state.redirectionSource.checkPhone = payload;
		}
	},
	getters: {
		rentCountOptions({ rentCountDictionary }, { kostPriceObject }) {
			const rentCountOptions = [];
			if (kostPriceObject) {
				Object.keys(rentCountDictionary).forEach(rentCount => {
					const price = rentCountDictionary[rentCount];
					const priceLabel = `price_${price.type}`;
					const priceValue = kostPriceObject[priceLabel];
					if (priceValue) {
						rentCountOptions.push({
							type: rentCount,
							label: `Per ${price.label}`,
							price: priceValue,
							value: price.value
						});
					}
				});
			}
			return rentCountOptions;
		},
		kostPriceObject({ selectedKost }) {
			return selectedKost ? selectedKost.original_price || selectedKost : null;
		},
		selectedRoomId({ selectedKost }) {
			return selectedKost ? selectedKost.id : 0;
		},
		mainPrices({
			rentCountInfoType,
			params,
			rentCountDictionary,
			isDepositShown,
			depositAmount
		}) {
			if (!rentCountInfoType) {
				return [];
			}

			const rentCountLabel = rentCountDictionary[rentCountInfoType].label;
			return isDepositShown
				? [
						{
							label: `Harga Sewa Per ${rentCountLabel}`,
							amount: params.amount
						},
						{
							label: 'Deposit',
							amount: depositAmount
						}
				  ]
				: [
						{
							label: `Harga Sewa Per ${rentCountLabel}`,
							amount: params.amount
						}
				  ];
		},
		otherPrices({ params }) {
			const filteredAdditionalPrices = [];
			params.additional_costs.forEach(cost => {
				if (!!cost.field_title && !!cost.field_value) {
					filteredAdditionalPrices.push(cost);
				}
			});
			return filteredAdditionalPrices;
		},
		userInterface({ webviewInterface }) {
			if (webviewInterface === 'ios') {
				return 'mobile-ios';
			} else if (webviewInterface === 'android') {
				return 'mobile-android';
			}

			return navigator.isMobile ? 'mobile' : 'desktop';
		}
	},
	actions: {
		setKostListResponse({ commit }, payload) {
			commit('setOwnerKostList', payload);
		},
		resetRentCount({ commit }) {
			commit('setRentType', '');
			commit('setRentCountInfoType', '');
			commit('setRentPrice', 0);
			commit('setRentDuration', 0);
			commit('setRentDurationList', []);
		},
		applyRentCount({ commit, state, dispatch }, payload) {
			const rentCount = state.rentCountDictionary[payload];
			if (rentCount) {
				const priceObject =
					state.selectedKost.original_price || state.selectedKost;
				commit('setRentDuration', 0);
				commit('setRentPrice', priceObject[`price_${rentCount.type}`]);
				commit('setRentType', rentCount.value);
				commit('setRentCountInfoType', payload);
			} else {
				dispatch('resetRentCount');
			}
		},
		resetTenantData({ state, dispatch, commit }) {
			state.params = initParams();
			state.isLoadedAdditionalCostGroup = false;
			state.selectedTenantRoom = '';
			dispatch('resetRoomAllotment');
			commit('setLoadedAdditionalCostGroup', false);
			dispatch('resetRentCount');
		},
		setSelectedKost({ commit, dispatch }, payload) {
			dispatch('resetRoomAllotment');
			commit('setSelectedKost', payload);
		},
		resetRoomAllotment({ state, commit }) {
			state.selectedTenantRoom = '';
			state.params.room_number = '';
			commit(
				'setRoomAllotment',
				{ id: null, name: '', floor: '' },
				{ root: true }
			);
		}
	}
};
