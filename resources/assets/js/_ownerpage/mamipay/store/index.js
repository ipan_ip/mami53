import StoreTenantProfile from './modules/StoreProfileTenant';
import StoreManageBooking from './modules/StoreManageBooking';
import StoreBookingPropertyList from './modules/StoreBookingPropertyList';
import StoreAddTenant from './modules/StoreAddTenant';
import StoreManageBill from './modules/StoreManageBill';

const defaultStore = {
	modules: {
		tenantProfile: StoreTenantProfile,
		manageBooking: StoreManageBooking,
		bookingPropertyList: StoreBookingPropertyList,
		addTenant: StoreAddTenant,
		manageBill: StoreManageBill
	},
	state: {
		token: document.head.querySelector('meta[name="csrf-token"]').content,
		authCheck: {},
		authData: {},
		tenantStatus: {},
		isMamipayUser: false,
		userRooms: [],
		userRoom: [],
		userRoomStatus: {},
		allDetailRoom: {},
		totalIncome: '0',
		totalTransfer: '0',
		rejectReasons: [],
		ownerDetail: {},
		bankList: [],
		bulkProcessBookingActivation: false,
		isFromGoldPlusFlow: false,
		profile: {},
		isChatOpen: false
	},
	getters: {
		userHasNoProperty(state) {
			return (
				((state.profile &&
					state.profile.user &&
					state.profile.user.kost_total) ||
					0) <= 0
			);
		}
	},
	mutations: {
		setAuthCheck(state, payload) {
			state.authCheck = payload;
		},
		setAuthData(state, payload) {
			state.authData = payload;
		},
		setProfile(state, payload) {
			state.profile = payload;
		},
		setTenantStatus(state, payload) {
			state.tenantStatus = payload;
		},
		setIsMamipayUser(state, payload) {
			state.isMamipayUser = payload;
		},
		setUserRooms(state, payload) {
			state.userRooms = payload;
		},
		setUserRoom(state, payload) {
			state.userRoom = payload;
		},
		setUserRoomStatus(state, payload) {
			state.userRoomStatus = payload;
		},
		setTotalIncome(state, payload) {
			state.totalIncome = payload;
		},
		setTotalTransfer(state, payload) {
			state.totalTransfer = payload;
		},
		setAllDetailRoom(state, payload) {
			state.allDetailRoom = payload;
		},
		setRoomAvailability(state, payload) {
			state.allDetailRoom.room_available = payload;
		},
		setRejectReasons(state, payload) {
			state.rejectReasons = payload;
		},
		setOwnerDetail(state, payload) {
			state.ownerDetail = payload;
		},
		setBankList(state, payload) {
			state.bankList = payload;
		},
		setBulkProcessBookingActivation(state, payload) {
			state.bulkProcessBookingActivation = payload;
		},
		setIsFromGoldPlusFlow(state, payload) {
			state.isFromGoldPlusFlow = payload;
		},
		setIsChatOpen(state, payload) {
			state.isChatOpen = payload;
		}
	},
	actions: {
		fetchBankList({ commit }) {
			return new Promise((resolve, reject) => {
				axios
					.get('/bank/list')
					.then(response => {
						if (response.data.status) {
							commit('setBankList', response.data.data);
						} else {
							bugsnagClient.notify(response);
							alert(
								'Galat tidak diketahui.\nSilakan cek koneksi internet Anda dan coba lagi.'
							);
						}

						resolve();
					})
					.catch(error => {
						reject(error);
					});
			});
		}
	}
};

export default defaultStore;
