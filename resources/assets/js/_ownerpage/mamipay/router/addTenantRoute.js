import addTenantStore from '../store/modules/StoreAddTenant';
window.debounce = require('lodash/debounce');

const AddTenantContainer = require('../components/add-tenant/AddTenantContainer')
	.default;
const AddTenantSelectKost = require('../components/add-tenant/select-kost/AddTenantSelectKost')
	.default;
const AddTenantCheckPhone = require('../components/add-tenant/AddTenantCheckPhone')
	.default;
const AddTenantPaymentInfo = require('Js/_ownerpage/mamipay/components/add-tenant/payment-info/AddTenantPaymentInfo.vue')
	.default;
const AddTenantBiodata = require('../components/add-tenant/biodata/AddTenantBiodata')
	.default;
const AddTenantPaymentDetail = require('Js/_ownerpage/mamipay/components/add-tenant/payment-detail/AddTenantPaymentDetail.vue')
	.default;
const AddTenantPaymentOthers = require('../components/add-tenant/payment-others/AddTenantPaymentOthers')
	.default;
const AddTenantAdditionalCost = require('../components/add-tenant/payment-others/AddTenantAdditionalCost')
	.default;

export const addTenantSteps = [
	{
		path: 'select-kost',
		name: 'addTenantSelectKost',
		component: AddTenantSelectKost,
		meta: {
			formTitle: 'Pilih kos untuk penyewa',
			stepTitle: 'Pilih kos untuk penyewa'
		}
	},
	{
		path: 'check-phone',
		name: 'addTenantCheckPhone',
		component: AddTenantCheckPhone,
		meta: {
			formTitle: 'Masukkan Informasi Penyewa',
			stepTitle: 'Tambah Penyewa'
		}
	},
	{
		path: 'tenant-biodata',
		name: 'addTenantBiodata',
		component: AddTenantBiodata,
		meta: {
			formTitle: 'Masukkan Informasi Penyewa',
			stepTitle: 'Data Penyewa',
			stepIndex: 1
		}
	},
	{
		path: 'payment-info',
		name: 'addTenantPaymentInfo',
		component: AddTenantPaymentInfo,
		meta: {
			formTitle: 'Masukkan informasi pembayaran',
			stepTitle: 'Atur Sewa',
			stepIndex: 2
		}
	},
	{
		path: 'payment-others',
		name: 'addTenantPaymentOthers',
		component: AddTenantPaymentOthers,
		meta: {
			formTitle: 'Masukkan informasi biaya lain',
			stepTitle: 'Atur Biaya',
			stepIndex: 3
		}
	},
	{
		path: 'payment-others-settings',
		name: 'addTenantAdditionalCost',
		component: AddTenantAdditionalCost,
		meta: {
			formTitle: 'Atur biaya tambahan',
			stepTitle: 'Atur biaya tambahan'
		}
	},
	{
		path: 'payment-detail',
		name: 'addTenantPaymentDetail',
		component: AddTenantPaymentDetail,
		meta: {
			formTitle: 'Rincian Pembayaran',
			stepTitle: 'Rincian Pembayaran',
			stepIndex: 4
		}
	}
];

export const addTenantRoute = {
	path: '/add-tenant',
	name: 'mainAddTenant',
	component: AddTenantContainer,
	redirect: '/add-tenant/select-kost',
	children: addTenantSteps.map(step => {
		return {
			...step,
			meta: {
				...step.meta,
				isResponsive: true
			},
			beforeEnter: (to, from, next) => {
				if (to.name === 'addTenantSelectKost') {
					if (window.document.referrer) {
						addTenantStore.state.journey.push(window.document.referrer);
					} else {
						addTenantStore.state.journey.push(`router-path-${from.path}`);
					}

					next();
				} else if (!addTenantStore.state.selectedKost) {
					next({ name: 'addTenantSelectKost' });
				} else {
					next();
				}
			}
		};
	})
};
