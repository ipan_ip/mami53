import VueRouter from 'vue-router';

import { addTenantRoute } from './addTenantRoute';

window.debounce = require('lodash/debounce');
Vue.use(VueRouter);
const ManageActiveMamipay = require('../components/page-active-mamipay-booking/ManageActiveMamipay')
	.default;

const ManageMain = require('../components/ManageMain').default;
const ManageOwnerBooking = require('../components/page-manage-booking/ManageOwnerBooking')
	.default;
const ManageRoomPrice = require('../components/page-manage-booking/ManageRoomPrice')
	.default;
const ManageOwnerBill = require('../components/page-manage-bill/ManageOwnerBill')
	.default;

const ManageProfileMain = require('../components/page-tenant-profile/ManageProfileMain')
	.default;
const ManageProfileContentBiodata = require('../components/page-tenant-profile/ManageProfileContentBiodata')
	.default;
const ManageProfileContentBilling = require('../components/page-tenant-profile/ManageProfileContentBilling')
	.default;
const ManageProfileContentPayment = require('../components/page-tenant-profile/ManageProfileContentPayment')
	.default;

const ManageBookingPropertyList = require('../components/page-manage-booking-property-list/ManageBookingPropertyList')
	.default;

const routes = [
	{
		path: '/',
		redirect: '/all/booking'
	},
	{
		path: '/activate',
		name: 'mainActivate',
		component: ManageActiveMamipay,
		meta: {
			isResponsive: true
		}
	},
	{
		path: '/:seq',
		name: 'mainManage',
		component: ManageMain,
		children: [
			{
				path: 'booking',
				name: 'manageBooking',
				component: ManageOwnerBooking
			},
			{
				path: 'bill',
				name: 'manageBill',
				component: ManageOwnerBill
			},
			{
				path: 'room-price',
				name: 'manageRoomPrice',
				component: ManageRoomPrice
			},
			{
				path: 'property-list',
				name: 'manageBookingPropertyList',
				component: ManageBookingPropertyList
			}
		]
	},
	{ ...addTenantRoute },
	{
		path: '/profile-tenant/:tenantId?',
		name: 'maintenantProfile',
		component: ManageProfileMain,
		redirect: '/profile-tenant/:tenantId?/biodata-tenant',
		children: [
			{
				path: 'biodata-tenant/:typeProfile?',
				name: 'biodataTenant',
				component: ManageProfileContentBiodata
			},
			{
				path: 'billing-tenant/:typeProfile?',
				name: 'tenantBill',
				component: ManageProfileContentBilling
			},
			{
				path: 'payment-tenant/',
				name: 'paymentTenant',
				component: ManageProfileContentPayment
			}
		]
	}
];

const router = new VueRouter({
	mode: 'history',
	base: '/ownerpage/manage',
	routes
});

router.beforeEach((to, _, next) => {
	const viewport = document.head.querySelector('meta[name="viewport"]');

	if (to.meta && to.meta.isResponsive && viewport) {
		viewport.setAttribute('content', 'width=device-width, initial-scale=1');
	} else if (viewport) {
		viewport.setAttribute(
			'content',
			'width=1200, initial-scale=0, shrink-to-fit=no'
		);
	}

	next();
});

export default router;
