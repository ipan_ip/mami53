import Dayjs from 'vue-dayjs';
import 'dayjs/locale/id';
import VTooltip from 'v-tooltip';
import VeeValidateLanguage from 'vee-validate/dist/locale/id';
import VeeValidate, { Validator } from 'vee-validate';
import Vuex from 'vuex';
import vueScrollTo from 'vue-scroll-to';
import VueClipboard from 'vue-clipboard2';

import defaultStore from './store/index';
import router from './router/index';
import { addTenantSteps } from './router/addTenantRoute';

import 'Js/@mixins/MixinNavigatorIsMobile';
import 'Js/@mixins/MixinSwalLoading';
import 'Js/@mixins/MixinSwalSuccessError';
import App from './components/App.vue';
import mixinAuth from 'Js/@mixins/MixinAuth';

window.axios.defaults.baseURL = '/ownerpage/mamipay/request/';
window.axios.defaults.headers = {};

Vue.use(VueClipboard);

Vue.use(Dayjs, {
	lang: 'id'
});

Vue.use(VTooltip);

const optionsVueScroll = {
	speed: 500,
	padding: 0,
	movingFrequency: 15
};
Vue.use(vueScrollTo, optionsVueScroll);

window.validator = new Validator();
Validator.localize('id', VeeValidateLanguage);
Vue.use(VeeValidate, {
	locale: 'id',
	fieldsBagName: 'vvFields'
});

Vue.use(Vuex);

Vue.component('app', App);

const store = new Vuex.Store(defaultStore);

// eslint-disable-next-line no-unused-vars
const app = new Vue({
	el: '#app',
	mixins: [mixinAuth],
	router,
	store,
	created() {
		this.$dayjs.locale('id');
	},
	mounted() {
		try {
			document.getElementById('preloading').remove();
		} catch (e) {
			document.getElementById('preloading').style.display = 'none';
		}

		if (this.getUseDesktopSite()) this.requestDesktopSite();
	},
	methods: {
		getUseDesktopSite() {
			let useDesktopSite = this.$route.name !== 'mainActivate';

			addTenantSteps.forEach(addTenantRoute => {
				if (this.$route.name === addTenantRoute.name) {
					useDesktopSite = false;
				}
			});

			return useDesktopSite;
		},
		requestDesktopSite() {
			const viewport = document.head.querySelector('meta[name="viewport"]');

			if (this.navigator.isMobile) {
				viewport.setAttribute(
					'content',
					'width=1200, initial-scale=0, shrink-to-fit=no'
				);
			}
		}
	}
});
