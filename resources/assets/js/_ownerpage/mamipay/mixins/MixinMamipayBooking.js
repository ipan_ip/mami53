import _get from 'lodash/get';
import EventBus from '../event-bus/event-bus';

const mixinMamipayBooking = {
	computed: {
		authData() {
			return this.$store.state.authData.all;
		},
		searchBookingGroup: {
			set(val) {
				this.$store.commit('setSearchBookingGroup', val);
			},
			get() {
				return this.$store.state.manageBooking.searchBooking.group;
			}
		},
		searchBookingOrder: {
			set(val) {
				this.$store.commit('setSearchBookingOrder', val);
			},
			get() {
				return this.$store.state.manageBooking.searchBooking.order;
			}
		},
		searchBookingSort: {
			set(val) {
				this.$store.commit('setSearchBookingSort', val);
			},
			get() {
				return this.$store.state.manageBooking.searchBooking.sort;
			}
		},
		searchBookingLimit: {
			set(val) {
				this.$store.commit('setSearchBookingLimit', val);
			},
			get() {
				return this.$store.state.manageBooking.searchBooking.limit;
			}
		},
		searchBookingOffset: {
			set(val) {
				this.$store.commit('setSearchBookingOffset', val);
			},
			get() {
				return this.$store.state.manageBooking.searchBooking.offset;
			}
		},
		perPage: {
			set(val) {
				this.$store.commit('setSearchBookingPerPage', val);
			},
			get() {
				return this.$store.state.manageBooking.searchBooking.perPage;
			}
		},
		bookingSelectedNumberPagination: {
			set(val) {
				this.$store.commit('setBookingSelectedNumberPagination', val);
			},
			get() {
				return this.$store.state.manageBooking.searchBooking
					.selectedNumberPagination;
			}
		},
		userRoom: {
			get() {
				return this.$store.state.userRoom;
			},
			set(val) {
				this.$store.commit('setUserRoom', val);
			}
		},
		listBooking() {
			return this.$store.state.manageBooking.listBooking.data;
		},
		bookingSelected: {
			get() {
				return this.$store.state.manageBooking.listBookingSelected;
			},
			set(val) {
				this.$store.commit('setListBookingSelected', val);
			}
		},
		rejectFrom() {
			return this.$store.state.manageBooking.extras.rejectBookingFrom;
		}
	},

	created() {
		this.getDateNow();
		this.perPage = 8;
		this.searchBookingLimit = 5;
		this.searchBookingOffset = 0;
		this.searchBookingGroup = '&group[]=0';
		this.bookingSelectedNumberPagination = 1;
	},

	methods: {
		getDateNow() {
			const year = this.$dayjs().format('YYYY');
			const month = this.$dayjs().format('M');

			this.searchBillYear = year;
			this.searchBillMonth = month;

			this.billReportYear = year;
			this.billReportMonth = month;
		},

		getRoomDetail(idRoom) {
			axios
				.get(`/room/detail/${idRoom}`)
				.then(response => {
					const dataStatus = response.data || false;

					if (dataStatus.status) {
						this.$store.commit('setUserRoomStatus', dataStatus);
					}
				})
				.catch(error => {
					bugsnagClient.notify(error);
					EventBus.$emit('showErrorModal');
				});
		},

		getAllDetailRoom(idRoom, pushToBookingSelected = null) {
			this.openSwalLoading();
			axios
				.get(`/owner/data/update/${idRoom}`, { baseURL: '/garuda' })
				.then(response => {
					const dataResponse = response.data;
					if (dataResponse.status) {
						this.getRoomDetail(idRoom);
						if (!pushToBookingSelected) {
							if (Object.keys(dataResponse.data).length !== 0) {
								this.$store.commit('setAllDetailRoom', dataResponse.data);
							}
						} else {
							this.bookingSelected.address = dataResponse.data.address;
							this.bookingSelected.gender = dataResponse.data.gender;
						}
						this.closeSwalLoading();
					}
				})
				.catch(error => {
					bugsnagClient.notify(error);
					this.swalError(
						null,
						'Terjadi galat mengambil detail kost. Silakan coba lagi.'
					);
				});
		},

		getUserRooms(seq = null) {
			this.$store.commit('setBookingListLoading', true);

			axios
				.get('/room/list')
				.then(response => {
					const dataResponse = response.data;
					const callback = () => {
						if (dataResponse.status && dataResponse.data.length > 0) {
							const listRoom = dataResponse.data;
							this.$store.commit('setUserRooms', listRoom);
							if (seq !== 'all') {
								const selectedRoom = listRoom.filter(room => {
									return seq === room.id;
								});
								if (!selectedRoom.length) {
									this.$store.commit('setUserRoom', listRoom[0]);
									this.$store.commit('setRoomId', listRoom[0].id);
									this.getAllDetailRoom(listRoom[0].id);
									this.getIncomeAndTransfer(listRoom[0].id);
								} else {
									this.$store.commit('setUserRoom', selectedRoom[0]);
									this.getAllDetailRoom(selectedRoom[0].id);
									this.getIncomeAndTransfer(selectedRoom[0].id);
								}
							} else {
								this.$store.commit('setUserRoom', listRoom[0]);
								this.getAllDetailRoom(listRoom[0].id);
								this.getIncomeAndTransfer(listRoom[0].id);
							}
							this.getBookingList({
								seq: this.userRoom.id
							});
						}

						this.$store.commit('setBookingListLoading', false);
					};

					// add new data on first dataResponse when query param kos_name exists
					if (this.$route.query.kos_name) {
						axios
							.get(`room/list?keyword=${this.$route.query.kos_name}`)
							.then(res => {
								dataResponse.data.unshift(...res.data.data);
								callback();
							});
					} else {
						callback();
					}
				})
				.catch(error => {
					bugsnagClient.notify(error);
					this.swalError(
						null,
						'Terjadi galat mengambil list kost. Silakan coba lagi.'
					);
				});
		},

		getIncomeAndTransfer(roomId, month = null, year = null) {
			const valueMonth = month == null ? this.searchBillMonth : month;
			const valueYear = year == null ? this.searchBillYear : year;

			this.openSwalLoading();
			axios
				.get(
					`/payment-schedules/amount-report/${roomId}/${valueYear}/${valueMonth}`
				)
				.then(response => {
					if (response.data.status) {
						this.$store.commit(
							'setTotalIncome',
							response.data.total_paid_amount
						);
						this.$store.commit(
							'setTotalTransfer',
							response.data.total_transferred_amount
						);
						this.closeSwalLoading();
					}
				})
				.catch(error => {
					bugsnagClient.notify(error);
					this.swalError(
						null,
						'Terjadi galat mengambil total transfer. Silakan coba lagi.'
					);
				});
		},

		dataBooking(data) {
			const allDataBooking = {
				data: [],
				hasMore: data.has_more,
				total: data.total
			};

			let i = 1;

			data.data.forEach(item => {
				allDataBooking.data.push({
					id: i,
					photo: item.photo,
					name: item.name,
					status: item.status,
					managed_by_label: item.managed_by_label,
					checkin: item.checkin_date,
					rentDuration: item.duration,
					action: item
				});
				i++;
			});

			return allDataBooking;
		},

		getBookingList(
			{
				limit = null,
				offset = null,
				seq = null,
				sort = null,
				order = null,
				group = null,
				callback = null,
				callbackParams = null
			},
			isCancelPreviousRequest = false
		) {
			const valueSeq = !seq ? this.userRoom.id : seq;

			if (!valueSeq) return;

			isCancelPreviousRequest &&
				this.$store.dispatch('cancelPendingBookingListRequest');

			const axiosCancelTokenSource = axios.CancelToken.source();

			this.$store.commit('addCancelTokenBookingList', axiosCancelTokenSource);

			const valueLimit = !limit ? this.searchBookingLimit : limit;
			const valueOffset = offset === null ? this.searchBookingOffset : offset;
			const valueSort = !sort ? this.searchBookingSort : sort;
			const valueGroup = !group ? this.searchBookingGroup : group;

			this.openSwalLoading();
			this.$store.commit('setBookingListLoading', true);

			axios
				.get(
					`/owner/booking?song_id=${valueSeq}&limit=${valueLimit}&offset=${valueOffset}&order=${valueSort}${valueGroup}`,
					{ cancelToken: axiosCancelTokenSource.token }
				)
				.then(response => {
					const dataResponse = response && response.data;

					if (
						dataResponse &&
						dataResponse.status &&
						dataResponse.data != null
					) {
						const dataBooking = this.dataBooking(dataResponse);
						this.$store.commit('setListBooking', dataBooking);

						const totalPage = Math.ceil(dataResponse.total / this.perPage);
						this.$store.commit('setSearchBookingTotalPage', totalPage);
					}

					if (callback) {
						callback(callbackParams);
					}

					this.$store.commit('setBookingListLoading', false);
					this.closeSwalLoading();
				})
				.catch(error => {
					const isCanceled = typeof error === 'object' && error.__CANCEL__;

					if (!isCanceled) {
						bugsnagClient.notify(error);
						this.swalError(
							null,
							'Terjadi galat mengambil daftar permintaan booking. Silakan coba lagi.'
						);
						this.$store.commit('setBookingListLoading', false);
					}
				});
		},

		rejectBookingRequest(reasonId, reasonDescription, callback) {
			const params = {
				reason_id: reasonId,
				reason: reasonDescription
			};

			const idBooking = this.bookingSelected.id;

			this.openSwalLoading();
			axios
				.post(`/owner/booking/reject/${idBooking}`, params)
				.then(response => {
					const dataResponse = response.data;
					this.closeSwalLoading();
					if (dataResponse.status) {
						this.$toasted.show('Berhasil menolak request', {
							type: 'info',
							className: 'toasted-default',
							theme: 'bubble',
							position: 'bottom-center',
							duration: 2000
						});
						callback();
						this.addRejectBookingTrackEvent(params.reason);
					} else {
						const errorMessage =
							dataResponse.meta && dataResponse.meta.message
								? dataResponse.meta.message
								: 'Terjadi galat. Silakan coba lagi.';

						throw new Error(errorMessage);
					}
				})
				.catch(error => {
					bugsnagClient.notify(error);
					this.closeSwalLoading();
					this.swalError(null, 'Terjadi galat. Silakan coba lagi.');
				});
		},

		addRejectBookingTrackEvent(rejectReason) {
			tracker('moe', [
				'[Owner] Reject Booking',
				{
					rejected_reason: rejectReason.toString(),
					property_type: 'kost',
					booking_start_time: new Date(this.bookingSelected.checkin_date),
					booking_periode: _get(this.bookingSelected, 'duration_count'),
					tenant_job: _get(this.bookingSelected, 'job'),
					tenant_name: _get(this.bookingSelected, 'name'),
					tenant_phone_number: _get(this.bookingSelected, 'phone_number'),
					user_email: _get(this.bookingSelected, 'email'),
					tenant_gender:
						_get(this.bookingSelected, 'gender') === 2
							? 'perempuan'
							: 'laki-laki',
					property_id: _get(this.bookingSelected, 'room_id'),
					owner_name: _get(this.authData, 'name'),
					owner_phone_number: _get(this.authData, 'phone_number'),
					owner_email: _get(this.authData, 'email'),
					booking_id: _get(this.bookingSelected, 'booking_id'),
					confirm_from: this.rejectFrom.toLowerCase(),
					tenant_marital_status: _get(this.bookingSelected, 'marital_status'),
					request_date: new Date(this.bookingSelected.request_date)
				}
			]);
		},

		fillToProfilePage(data) {
			this.$store.commit('setRoomNumber', data.room_number);
			this.$store.commit('setRoomId', data.room_id);
			this.$store.commit('setFullName', data.name);
			this.$store.commit('setPhoneNumber', data.phone_number);
			this.$store.commit('setGender', data.gender);
			this.$store.commit('setEmail', data.email);
			this.$store.commit('setWork', data.occupation);
			this.$store.commit('setWork', data.occupation);

			this.$store.commit('setParentName', data.parent_name);
			this.$store.commit('setParentNumber', data.parent_phone_number);
			this.$store.commit('setStatus', data.user_status);

			this.$store.commit('setPicture', data.tenant_photo_id);
			this.$store.commit('setCardIdentity', data.tenant_photo_identifier_id);
			this.$store.commit('setDocument', data.tenant_photo_document_id);

			this.$store.commit('setCheckInDate', data.start_date);
			this.$store.commit('setRentCount', data.rent_type);
			this.$store.commit('setRentDuration', data.duration);
			this.$store.commit('setRentPrice', data.amount);
		},

		goBooking() {
			this.$router.push('/all/booking');
		},

		getProfileBooking(seq, callback) {
			const tenantId = this.bookingSelected.id || seq;
			this.openSwalLoading();
			axios
				.get(`/owner/booking/accept/${tenantId}`)
				.then(response => {
					const dataResponse = response.data;
					if (dataResponse.status) {
						callback(
							dataResponse.data.room_id,
							dataResponse.data.rent_type,
							dataResponse.data.room_additional_price,
							dataResponse.data.room_amount
						);
						this.$store.commit('setAcceptBookingProfile', dataResponse.data);
						this.$store.commit(
							'setOwnerTermsAgreement',
							dataResponse.data.owner_agreement
						);
						this.fillToProfilePage(dataResponse.data);
						this.closeSwalLoading();
					} else {
						this.swalError(
							null,
							'Terjadi galat. Silakan coba lagi.',
							this.goBack()
						);
					}
				})
				.catch(error => {
					bugsnagClient.notify(error);
					this.swalError(
						null,
						'Terjadi galat mengambil profile tenant. Silakan coba lagi.',
						this.goBooking
					);
					setTimeout(() => {
						this.goBooking();
					}, 3000);
				});
		},

		goBack() {
			this.$router.go(-1);
		}
	}
};

export default mixinMamipayBooking;
