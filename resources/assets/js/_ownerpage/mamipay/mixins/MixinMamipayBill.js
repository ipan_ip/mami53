const mixinMamipayBill = {
	data() {
		return {
			year: '',
			month: ''
		};
	},

	computed: {
		userRoomId() {
			return this.$store.state.userRoom.id;
		},
		searchBill: {
			set(val) {
				this.$store.commit('setSearchBillCriteria', val);
			},
			get() {
				return this.$store.state.manageBill.searchBill.criteria;
			}
		},
		searchBillMonth: {
			set(val) {
				this.$store.commit('setSearchBillMonth', val);
			},
			get() {
				return this.$store.state.manageBill.searchBill.month;
			}
		},
		searchBillYear: {
			set(val) {
				this.$store.commit('setSearchBillYear', val);
			},
			get() {
				return this.$store.state.manageBill.searchBill.year;
			}
		},
		searchBillLimit: {
			set(val) {
				this.$store.commit('setSearchBillLimit', val);
			},
			get() {
				return this.$store.state.manageBill.searchBill.limit;
			}
		},
		searchBillOffset: {
			set(val) {
				this.$store.commit('setSearchBillOffset', val);
			},
			get() {
				return this.$store.state.manageBill.searchBill.offset;
			}
		},
		searchBillOrder: {
			set(val) {
				this.$store.commit('setSearchBillOrder', val);
			},
			get() {
				return this.$store.state.manageBill.searchBill.order;
			}
		},
		searchBillSort: {
			set(val) {
				this.$store.commit('setSearchBillSort', val);
			},
			get() {
				return this.$store.state.manageBill.searchBill.sort;
			}
		},
		billReportYear: {
			set(val) {
				this.$store.commit('setBillReportYear', val);
			},
			get() {
				return this.$store.state.manageBill.billReport.year;
			}
		},
		billReportMonth: {
			set(val) {
				this.$store.commit('setBillReportMonth', val);
			},
			get() {
				return this.$store.state.manageBill.billReport.month;
			}
		},
		perPage: {
			set(val) {
				this.$store.commit('setSearchBillPerPage', val);
			},
			get() {
				return this.$store.state.manageBill.searchBill.perPage;
			}
		},
		listBillReport() {
			return this.$store.state.manageBill.listBillReport;
		},
		dataBillCount() {
			return this.$store.state.manageBill.listBill.data.length;
		}
	},

	created() {
		this.perPage = 6;
		this.searchBillLimit = 6;
		this.searchBillOffset = 0;
	},

	methods: {
		deleteTenant(tenantId, toBill = null) {
			this.openSwalLoading();
			axios
				.delete(`contract/delete/${tenantId}`)
				.then(response => {
					if (response.data.status) {
						this.isModalDelete = false;
						this.$toasted.show('Berhasil menghapus tenant.', {
							type: 'info',
							className: 'toasted-default',
							theme: 'bubble',
							position: 'bottom-center',
							duration: 2000
						});
						this.redirectAfterDeleteTenantBill(toBill);
						this.closeSwalLoading();
					}
				})
				.catch(error => {
					bugsnagClient.notify(error);
					this.swalError(
						null,
						'Terjadi galat menghapus kontrak. Silakan coba lagi.'
					);
				});
		},

		redirectAfterDeleteTenantBill(toBill) {
			const DATA_OFFSET = !this.dataBillCount ? '0' : this.searchBillOffset;
			this.getBill(
				this.$store.state.userRoom.id,
				null,
				this.searchBillMonth,
				this.searchBillYear,
				this.searchBillYear,
				DATA_OFFSET
			);
			if (toBill) {
				this.$router.push('/all/bill');
			}
		},

		getReportBill(roomId, month = null, year = null) {
			const valueMonth = month == null ? this.billReportMonth : month;
			const valueYear = year == null ? this.billReportYear : year;

			this.billReportMonth = valueMonth;
			this.billReportYear = valueYear;

			if (valueYear && valueMonth) {
				this.openSwalLoading();
				axios
					.get(
						`payment-schedules/report/${roomId}/${valueYear}/${valueMonth}/name/asc`
					)
					.then(response => {
						if (response.data.status) {
							const dataBillReport = this.dataBillReport(response.data);
							this.$store.commit('setListBillReport', dataBillReport);
							this.closeSwalLoading();
						}
					})
					.catch(error => {
						bugsnagClient.notify(error);
						this.swalError(
							null,
							'Terjadi galat mengambil laporan tagihan. Silakan coba lagi.'
						);
					});
			}
		},

		getBill(
			roomId,
			criteria = null,
			month = null,
			year = null,
			limit = null,
			offset = null,
			sort = null,
			order = null
		) {
			const valueMonth = !month ? this.searchBillMonth : month;
			const valueYear = !year ? this.searchBillYear : year;
			const valueLimit = !limit ? this.searchBillLimit : limit;
			let valueOffset = !offset ? this.searchBillOffset : offset;
			const valueSort = !sort ? this.searchBillSort : sort;
			const valueOrder = !order ? this.searchBillOrder : order;

			this.searchBill = criteria || '';

			if (this.listBillReport.length <= this.searchBilgetBilllLimit) {
				this.searchBillOffset = 0;
				valueOffset = 0;
			}

			this.searchBillMonth = valueMonth;
			this.searchBillYear = valueYear;

			const url = `payment-schedules/${roomId}/${valueYear}/${valueMonth}/${valueOrder}/${valueSort}?limit=${valueLimit}&offset=${valueOffset}&criteria=${this.searchBill}`;

			this.openSwalLoading();
			axios
				.get(url)
				.then(response => {
					if (response.data.status) {
						const dataBill = this.dataBill(response.data);
						this.$store.commit('setListBill', dataBill);

						const totalPage = Math.ceil(response.data.total / this.perPage);
						this.$store.commit('setSearchBillTotalPage', totalPage);

						this.closeSwalLoading();
						this.getIncomeAndTransfer(roomId, valueMonth, valueYear);
					}
				})
				.catch(error => {
					bugsnagClient.notify(error);
					this.swalError(
						null,
						'Terjadi galat mengambil data tagihan. Silakan coba lagi.'
					);
				});
		},

		getIncomeAndTransfer(roomId, month = null, year = null) {
			const valueMonth = !month ? this.searchBillMonth : month;
			const valueYear = !year ? this.searchBillYear : year;

			this.openSwalLoading();
			axios
				.get(
					`payment-schedules/amount-report/${roomId}/${valueYear}/${valueMonth}`
				)
				.then(response => {
					if (response.data.status) {
						this.$store.commit(
							'setTotalIncome',
							response.data.total_paid_amount
						);
						this.$store.commit(
							'setTotalTransfer',
							response.data.total_transferred_amount
						);
						this.closeSwalLoading();
					}
				})
				.catch(error => {
					bugsnagClient.notify(error);
					this.swalError(
						null,
						'Terjadi galat mengambil total transfer. Silakan coba lagi.'
					);
				});
		},

		dataBillReport(data) {
			const allDataBillReport = [];
			let fixedCost = '';
			let otherCost = '';
			let i = 1;

			data.data.forEach(item => {
				if (item.total_components.fixed_cost) {
					fixedCost = item.total_components.fixed_cost;
				} else {
					fixedCost = '-';
				}

				if (item.total_components.other_cost) {
					otherCost = item.total_components.other_cost;
				} else {
					otherCost = '-';
				}

				allDataBillReport.push({
					id: i,
					name: item.tenant_name,
					status: item.payment_status,
					dueDate: item.scheduled_date,
					rentCost: item.amount,
					fixedCost: fixedCost,
					additionalCost: otherCost
				});
				i++;
			});

			return allDataBillReport;
		},

		getDistanceDay(outDateParams) {
			const milliseconds = 1000;
			const limitDay = 14;
			const todayDate = this.$dayjs();
			const outDate = this.$dayjs(new Date(outDateParams * milliseconds));
			return outDate.diff(todayDate, 'day') < limitDay;
		},

		dataBill(data) {
			const allDataBill = {
				data: [],
				hasMore: data.has_more,
				total: data.status
			};

			let i = 1;

			let statusRenewButton = false;

			data.data.forEach(item => {
				statusRenewButton = this.getDistanceDay(
					item.contract_end_date_timestamp
				);

				allDataBill.data.push({
					id: i,
					tenantName: item.tenant_name,
					roomNumber: item.room_number,
					paymentStatusCode: item.payment_status_code,
					paymentSchedule: item.payment_schedule,
					contractEndDate: item.contract_end_date,
					amount: item.amount,
					menu: item.contract_id,
					tenantId: item.tenant_id,
					contractId: item.contract_id,
					photo: item.tenant_photo,
					renewButton: statusRenewButton,
					terminateInfo: item.termination_info || {
						status: 'noCheckout'
					},
					hasCheckIn: item.has_checked_in,
					checkoutDate:
						this.labellingCheckoutList(item.nearest_checkout_date_multiple) ||
						[]
				});
				i++;
			});

			return allDataBill;
		},

		labellingCheckoutList(checkoutDateList) {
			return checkoutDateList.map(value => {
				return {
					value,
					label: this.$dayjs(value).format('dddd, D MMMM YYYY')
				};
			});
		},

		getAllDataBill() {
			this.getBill(
				this.userRoom.id,
				null,
				this.$dayjs().format('M'),
				this.$dayjs().format('YYYY')
			);
			this.getReportBill(this.userRoom.id);
			this.getIncomeAndTransfer(this.userRoom.id);
		},

		getUserRooms(seq = null) {
			this.openSwalLoading();
			axios
				.get('room/list')
				.then(response => {
					const dataResponse = response.data;
					if (dataResponse.status && dataResponse.data.length > 0) {
						const listRoom = dataResponse.data;
						this.$store.commit('setUserRooms', listRoom);
						if (seq != 'all') {
							const selectedRoom = listRoom.filter(room => {
								return seq == room.id;
							});
							if (!selectedRoom.length) {
								this.$store.commit('setUserRoom', listRoom[0]);
								this.getAllDetailRoom(listRoom[0].id);
								this.getIncomeAndTransfer(listRoom[0].id);
							} else {
								this.$store.commit('setUserRoom', selectedRoom[0]);
								this.getAllDetailRoom(selectedRoom[0].id);
								this.getIncomeAndTransfer(selectedRoom[0].id);
							}
						} else {
							this.$store.commit('setUserRoom', listRoom[0]);
							this.getAllDetailRoom(listRoom[0].id);
							this.getIncomeAndTransfer(listRoom[0].id);
						}
						this.getAllDataBill();
					}
					this.closeSwalLoading();
				})
				.catch(error => {
					bugsnagClient.notify(error);
					this.swalError(
						null,
						'Terjadi galat mengambil list kost. Silakan coba lagi.'
					);
				});
		},

		getRoomDetail(idRoom) {
			axios
				.get(`/room/detail/${idRoom}`)
				.then(response => {
					const dataStatus = response.data || false;

					if (dataStatus.status) {
						this.$store.commit('setUserRoomStatus', dataStatus);
					}
				})
				.catch(error => {
					bugsnagClient.notify(error);
					this.swalError(
						null,
						'Terjadi galat mengambil detail room. Silakan coba lagi.'
					);
				});
		},

		getAllDetailRoom(idRoom) {
			this.openSwalLoading();
			axios
				.get(`/owner/data/update/${idRoom}`, { baseURL: '/garuda' })
				.then(response => {
					const dataResponse = response.data;
					if (dataResponse.status) {
						this.getRoomDetail(idRoom);
						this.$store.commit('setAllDetailRoom', dataResponse.data);
						this.closeSwalLoading();
					}
				})
				.catch(error => {
					bugsnagClient.notify(error);
					this.swalError(
						null,
						'Terjadi galat mengambil detail kost. Silakan coba lagi.'
					);
				});
		}
	}
};

export default mixinMamipayBill;
