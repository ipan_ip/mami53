const mixinManagePanelArrow = {
	mounted() {
		this.addClickEventTriggerForPanelArrow();
	},
	methods: {
		addClickEventTriggerForPanelArrow() {
			document
				.querySelectorAll('.panel-group .accordion-toggle .header-custom')
				.forEach(header => {
					header.addEventListener('click', () => {
						const accordionIcon = header.querySelector('.accordion-icon');
						if (accordionIcon !== null) {
							accordionIcon.classList.toggle('--is-closed');
						}
					});
				});
		}
	}
};

export default mixinManagePanelArrow;
