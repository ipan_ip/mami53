import _get from 'lodash/get';

const mixinTrackBookingConfirmationClick = {
	methods: {
		trackBookingConfirmationClick(
			bookingData,
			confirmationLocation,
			confirmBookingStatus
		) {
			const user = this.$store.state.authData.all;
			tracker('moe', [
				'[Owner] Click Confirm / Reject Booking Button',
				{
					booking_id: _get(bookingData, 'booking_id'),
					booking_periode: _get(bookingData, 'duration_count'),
					booking_start_time: new Date(bookingData.checkin_date),
					booking_status: _get(bookingData, 'status_label'),
					confirm_booking_status: confirmBookingStatus,
					confirm_from: confirmationLocation,
					owner_email: _get(user, 'email', ''),
					owner_name: _get(user, 'name'),
					owner_phone_number: _get(user, 'phone_number'),
					property_id: _get(bookingData, 'room_id'),
					property_type: 'kost',
					requestDate: new Date(bookingData.request_date),
					tenant_gender: _get(bookingData, 'gender'),
					tenant_job: _get(bookingData, 'job'),
					tenant_marital_status: _get(bookingData, 'marital_status', null),
					tenant_name: _get(bookingData, 'name'),
					tenant_phone_number: _get(bookingData, 'phone_number'),
					user_email: _get(bookingData, 'email')
				}
			]);
		},
		trackClickAcceptBooking(bookingData, confirmationLocation) {
			this.trackBookingConfirmationClick(
				bookingData,
				confirmationLocation,
				'Accepted'
			);
		},
		trackClickRejectBooking(bookingData, confirmationLocation) {
			this.trackBookingConfirmationClick(
				bookingData,
				confirmationLocation,
				'Rejected'
			);
		}
	}
};

export default mixinTrackBookingConfirmationClick;
