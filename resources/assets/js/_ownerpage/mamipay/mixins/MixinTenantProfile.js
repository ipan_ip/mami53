import _get from 'lodash/get';
import { validateEmail } from 'Js/@utils/validation';
import EventBus from '../event-bus/event-bus';

const mixinTenantProfile = {
	data() {
		return {
			otherCostList: { facility: [], price: [] },
			initRentCount: true
		};
	},

	computed: {
		authData() {
			return this.$store.state.authData.all;
		},
		globalRentDurationList: {
			get() {
				return this.$store.state.tenantProfile.roomSelectedDetail
					.rentDurationList;
			},
			set(val) {
				this.$store.commit('setRentDurationList', val);
			}
		},
		tenantProfile() {
			return this.$store.state.tenantProfile.tenantProfile;
		},
		tenantProfileId() {
			return this.$store.state.tenantProfile.tenantProfile.id;
		},
		roomId() {
			return this.$store.state.tenantProfile.tenantProfile.roomId;
		},
		fullName() {
			return this.$store.state.tenantProfile.tenantProfile.fullName;
		},
		phoneNumber() {
			return this.$store.state.tenantProfile.tenantProfile.phoneNumber;
		},
		roomNumber() {
			return this.$store.state.tenantProfile.tenantProfile.roomNumber;
		},
		designerRoomId() {
			return this.$store.state.tenantProfile.tenantProfile.designerRoomId;
		},
		gender() {
			return this.$store.state.tenantProfile.tenantProfile.gender;
		},
		email() {
			return this.$store.state.tenantProfile.tenantProfile.email;
		},
		occupation() {
			return this.$store.state.tenantProfile.tenantProfile.work;
		},
		maritalStatus() {
			return this.$store.state.tenantProfile.tenantProfile.status;
		},
		questions() {
			return this.$store.state.tenantProfile.tenantProfile.question;
		},
		startDate() {
			return this.$store.state.tenantProfile.tenantBill.checkInDate;
		},
		rentType() {
			return this.$store.state.tenantProfile.tenantBill.rentCount;
		},
		amount() {
			return this.$store.state.tenantProfile.tenantBill.rentPrice;
		},
		discountAmount() {
			return this.$store.state.tenantProfile.tenantBill.discountRentPrice;
		},
		duration() {
			return this.$store.state.tenantProfile.tenantBill.rentDuration;
		},
		photoId() {
			return this.$store.state.tenantProfile.tenantProfile.picture;
		},
		parentName() {
			return this.$store.state.tenantProfile.tenantProfile.parentName;
		},
		parentPhoneNumber() {
			return this.$store.state.tenantProfile.tenantProfile.parentNumber;
		},
		photoIdentifierId() {
			return this.$store.state.tenantProfile.tenantProfile.cardIdentity;
		},
		documentId() {
			return this.$store.state.tenantProfile.tenantProfile.document;
		},
		fixedBilling() {
			return this.$store.state.tenantProfile.tenantBill.fixedBilling;
		},
		billingDate() {
			return this.$store.state.tenantProfile.tenantBill.dateBill;
		},
		firstAmount() {
			return this.$store.state.tenantProfile.tenantBill.prorated || 0;
		},
		depositAmount() {
			return this.$store.state.tenantProfile.tenantBill.costDeposit;
		},
		fineAmount() {
			return this.$store.state.tenantProfile.tenantBill.costFine;
		},
		fineMaximumLength() {
			return this.$store.state.tenantProfile.tenantBill.durationDay;
		},
		fineDurationType() {
			return this.$store.state.tenantProfile.tenantBill.dayType;
		},
		additionalCosts() {
			return this.$store.state.tenantProfile.tenantBill.otherCost;
		},
		toggleRentDurationNew() {
			return this.$store.state.tenantProfile.tenantBill.toggleRentDurationNew;
		},
		newRentDuration() {
			return this.$store.state.tenantProfile.tenantBill.newRentDuration;
		},
		saveCostGroup() {
			return this.$store.state.tenantProfile.tenantBill.forOtherTenant;
		},
		useDp() {
			return this.$store.state.tenantProfile.tenantBill.downPayment;
		},
		dpAmount() {
			return this.$store.state.tenantProfile.tenantBill.costDownPayment;
		},
		dpDate() {
			return this.$store.state.tenantProfile.tenantBill.downPaymentDate;
		},
		dpSettlementDate() {
			return this.$store.state.tenantProfile.tenantBill
				.downPaymentSettlementDate;
		},
		totalSimulatedCalculation() {
			return this.$store.state.tenantProfile.tenantBill
				.totalSimulatedCalculation;
		},
		bookingId() {
			return (
				this.$store.state.manageBooking.listBookingSelected.id ||
				this.$route.params.tenantId
			);
		},
		contractId() {
			return this.$store.state.tenantProfile.widgetInfo.contractId;
		},
		roomtenantIdProfile: {
			get() {
				return this.$store.state.tenantProfile.tenantProfile.roomId;
			},
			set(val) {
				this.$store.commit('setRoomId', val);
			}
		},
		unavailableRent: {
			get() {
				return this.$store.state.tenantProfile.unavailableRent;
			},
			set(val) {
				this.$store.commit('setunavailableRent', val);
			}
		},
		billingProfile() {
			return this.$store.state.tenantProfile.billingProfile;
		},
		statusPage() {
			return this.$store.state.tenantProfile.statusPage;
		},
		setSelectedPaymentYear() {
			return this.$store.state.tenantProfile.selectedPaymentYear;
		},
		costData: {
			get() {
				return this.$store.state.tenantProfile.tenantBill.otherCost;
			},
			set(val) {
				this.$store.commit('setOtherCost', val);
			}
		},
		userRoom() {
			return this.$store.state.userRoom;
		},
		userRoomId() {
			return this.$store.state.userRoom.id;
		},
		allDetailRoom() {
			return this.$store.state.allDetailRoom;
		},
		bookingCode() {
			return this.$store.state.manageBooking.acceptBookingProfile.code;
		},
		confirmParams() {
			return this.$store.state.tenantProfile.confirmParams;
		},
		otherBill() {
			return this.$store.state.tenantProfile.otherBill;
		},
		isValidRoomNumber() {
			const { roomNumber, designerRoomId } = this.tenantProfile;
			return designerRoomId !== null || roomNumber;
		}
	},

	mounted() {
		this.assignOtherCost();
	},

	methods: {
		filterTypeRentCount(val) {
			if (val === 'price_monthly') {
				return { label: 'Per-Bulan', value: 'month' };
			} else if (val === 'price_weekly') {
				return { label: 'Per-Minggu', value: 'week' };
			} else if (val === 'price_yearly') {
				return { label: 'Per-Tahun', value: 'year' };
			} else if (val === 'price_quarterly') {
				return { label: 'Per-3 Bulan', value: '3_month' };
			} else if (val === 'price_semiannualy') {
				return { label: 'Per-6 Bulan', value: '6_month' };
			}
		},

		filterRentCount(rentList) {
			this.unavailableRent = [];
			const dataList = [];
			Object.keys(rentList).forEach(rentItem => {
				if (rentItem !== 'price_daily') {
					const itemList = this.filterTypeRentCount(rentItem);
					if (!rentList[rentItem]) {
						this.unavailableRent.push(itemList.value);
					}
					dataList.push(itemList);
				}
			});
			this.$store.commit('setRentCountList', dataList);
		},

		getUserRoomById(idRoom, bookingId = null, isSwalLoading = true) {
			const selectRoom =
				idRoom == null ? this.$store.state.userRoom.id : idRoom;
			const urlGetRoomDetail =
				this.statusPage === 'acceptBooking'
					? `/room/detail/${selectRoom}?booking_id=${bookingId}`
					: `/room/detail/${selectRoom}`;
			!!isSwalLoading && this.openSwalLoading();
			axios
				.get(urlGetRoomDetail)
				.then(response => {
					const dataResponse = response.data;
					if (dataResponse.status && dataResponse !== 'waiting') {
						this.$store.commit('setUserRoomStatus', dataResponse);
						this.$store.commit('setUserRoom', dataResponse.room);
						const price = {
							price_daily: dataResponse.room.price_daily,
							price_monthly: dataResponse.room.price_monthly,
							price_quarterly: dataResponse.room.price_quarterly,
							price_semiannualy: dataResponse.room.price_semiannualy,
							price_weekly: dataResponse.room.price_weekly,
							price_yearly: dataResponse.room.price_yearly
						};
						this.filterRentCount(price);
						this.closeSwalLoading();
					}
				})
				.catch(error => {
					bugsnagClient.notify(error);
					this.swalError(null, 'Terjadi galat. Silakan coba lagi.');
				});
		},

		getAllDetailRoom(idRoom) {
			this.openSwalLoading();
			axios
				.get(`/owner/data/update/${idRoom}`, { baseURL: '/garuda' })
				.then(response => {
					const dataResponse = response.data;
					if (dataResponse.status) {
						this.$store.commit('setAllDetailRoom', dataResponse.data);
						this.closeSwalLoading();
					}
				})
				.catch(error => {
					bugsnagClient.notify(error);
					this.swalError(
						null,
						'Terjadi galat mengambil detail kost. Silakan coba lagi.'
					);
				});
		},

		getUserRooms() {
			this.openSwalLoading();
			axios
				.get('/room/list')
				.then(response => {
					const dataResponse = response.data;
					if (dataResponse.status) {
						this.$store.commit('setUserRooms', dataResponse.data);
						this.$store.commit('setUserRoom', dataResponse.data);
						this.$store.commit('setRoomId', dataResponse.data.id);
						this.getUserRoomById(dataResponse.data.id);
						this.getAllDetailRoom(dataResponse.data.id);
						this.closeSwalLoading();
					}
				})
				.catch(error => {
					bugsnagClient.notify(error);
					this.swalError(null, 'Terjadi galat. Silakan coba lagi.');
				});
		},

		getRentDuration(duration) {
			if (this.$route.name !== 'tenantBill' || !this.initRentCount) {
				this.openSwalLoading();
			} else {
				this.$store.commit('setLoadingState', {
					type: 'rentCount',
					value: true
				});
				if (this.roomId) {
					this.initRentCount = false;
				}
			}
			axios
				.get(`/user/booking/rent-count/${duration}/room/${this.roomId}`, {
					baseURL: '/garuda'
				})
				.then(response => {
					if (response.data.status) {
						this.globalRentDurationList = response.data.data;
						this.closeSwalLoading();
					}
					this.$store.commit('setLoadingState', {
						type: 'rentCount',
						value: false
					});
				})
				.catch(error => {
					this.$store.commit('setLoadingState', {
						type: 'rentCount',
						value: false
					});
					bugsnagClient.notify(error);
					EventBus.$emit('showErrorModal');
				});
		},

		setRoomAllotmentData(roomAllotment) {
			const roomAllotmentDefault = { id: null, name: '', floor: '' };
			// handle select room at location
			if (this.statusPage === 'member' && !roomAllotment) {
				this.$store.commit('setRoomAllotment', {
					...roomAllotmentDefault,
					id: ''
				});
			} else if (!roomAllotment) {
				this.$store.commit('setRoomAllotment', roomAllotmentDefault);
			} else {
				this.$store.commit('setRoomAllotment', roomAllotment);
				this.$store.commit('setSelectedRoomAlloment', roomAllotment);
			}
		},

		setToProfile(data) {
			this.$store.commit('setTenantId', data.tenant_id);
			this.$store.commit('setRoomNumber', data.room_number);
			this.$store.commit('setFullName', data.tenant_name);
			this.$store.commit('setGender', data.gender_value);
			this.$store.commit('setStatus', data.marital_status || 'Belum Kawin');
			this.$store.commit('setWork', data.occupation);
			this.$store.commit('setWorkplace', data.tenant_work_place);
			this.$store.commit('setTenantBirthday', data.tenant_birthday);
			this.$store.commit('setPhoneNumber', data.phone_number);
			this.$store.commit('setEmail', data.email);
			this.$store.commit('setParentNumber', data.parent_phone_number);
			this.$store.commit('setParentName', data.parent_name);
			this.$store.commit('setPicture', data.tenant_photo_id);
			this.$store.commit('setCardIdentity', data.tenant_photo_identifier_id);
			this.$store.commit('setLastPictureImage', data.tenant_photo);
			this.$store.commit(
				'setLastIdentifierImage',
				data.tenant_photo_identifier
			);
			this.$store.commit('setCountDuration', data.duration);
			this.$store.commit('setQuestion', data.additional_info || []);
			this.setRoomAllotmentData(data.room_allotment);
		},

		getUserDetail(tenantId) {
			this.openSwalLoading();
			axios
				.get(`/tenant/show/${tenantId}`)
				.then(response => {
					if (response.data.status) {
						this.setToProfile(response.data.tenant);
						this.closeSwalLoading();
					}
					this.closeSwalLoading();
				})
				.catch(error => {
					bugsnagClient.notify(error);
					this.swalError(null, 'Terjadi galat. Silakan coba lagi.');
				});
		},

		cekTenantPhone(tenantPhone, callBack) {
			axios
				.get(`tenant/phone-number/${tenantPhone}`)
				.then(response => {
					if (response.data.status) {
						this.$store.commit('setUserSelectedProfile', response.data.tenant);
						callBack(1);
					} else {
						this.$store.commit('setUserSelectedProfile', {});
						callBack(0);
					}
				})
				.catch(error => {
					callBack(2);
					bugsnagClient.notify(error);
					this.swalError(
						null,
						'Terjadi galat cek tenant phone. Silakan coba lagi.'
					);
				});
		},

		dateFormatToGlobal(str) {
			const dateMatch = str.match(/\w+/g);

			let newMonth = '';

			if (dateMatch[1] === 'Oktober') {
				newMonth = 'October';
			} else if (dateMatch[1] === 'Januari') {
				newMonth = 'January';
			} else if (dateMatch[1] === 'Februari') {
				newMonth = 'February';
			} else if (dateMatch[1] === 'Maret') {
				newMonth = 'March';
			} else if (dateMatch[1] === 'April') {
				newMonth = 'April';
			} else if (dateMatch[1] === 'Mei') {
				newMonth = 'May';
			} else if (dateMatch[1] === 'Juni') {
				newMonth = 'June';
			} else if (dateMatch[1] === 'Juli') {
				newMonth = 'July';
			} else if (dateMatch[1] === 'Agustus') {
				newMonth = 'August';
			} else if (dateMatch[1] === 'September') {
				newMonth = 'September';
			} else if (dateMatch[1] === 'November') {
				newMonth = 'November';
			} else if (dateMatch[1] === 'Desember') {
				newMonth = 'December';
			}

			return `${dateMatch[0]} ${newMonth} ${dateMatch[2]}`;
		},

		getDurationList(value) {
			let durationType = '';
			if (value === 'year') {
				durationType = 'yearly';
			} else if (value === 'month') {
				durationType = 'monthly';
			} else if (value === '3_month') {
				durationType = 'quarterly';
			} else if (value === '6_month') {
				durationType = 'semiannually';
			} else if (value === 'week') {
				durationType = 'weekly';
			}

			if (durationType) {
				this.getRentDuration(durationType);
			}
		},

		fillToProfile(data) {
			const realStartDate = this.$dayjs(
				this.dateFormatToGlobal(data.contract_profile.start_date)
			).format('YYYY-MM-DD');
			const getRentPrice = data.contract_profile.duration;
			const realFixedBilling = !!data.billing_date;

			this.getDurationList(data.contract_type_value);

			this.$store.commit('setCheckInDate', realStartDate);
			this.$store.commit('setRentCount', data.contract_type_value);
			this.$store.commit('setRentDuration', getRentPrice);
			this.$store.commit('setRentPrice', data.billing_rule.base_amount);
			this.$store.commit('setRentPeriode', data.contract_duration);

			this.$store.commit('setFixedBilling', realFixedBilling);
			this.$store.commit('setDateBill', data.billing_date);

			this.$store.commit('setCostDeposit', data.billing_rule.deposit_amount);
			this.$store.commit('setCostFine', data.billing_rule.fine_amount);
			this.$store.commit(
				'setDurationDay',
				data.billing_rule.fine_maximum_length
			);
			this.$store.commit('setDayType', data.billing_rule.fine_duration_type);
		},

		countTotalTransfered() {
			let total = 0;
			this.billingProfile.forEach(profileItem => {
				if (
					profileItem.status === 'not_in_mamipay' ||
					profileItem.status === 'paid'
				) {
					total = total + profileItem.billingAmount;
				}
			});
			this.$store.commit('setTotalTransferredAmount', total);
		},

		getContractBill(contractId, year, isSwalLoading = true) {
			this.$store.commit('setLoadingState', {
				type: 'contractBill',
				value: true
			});
			!!isSwalLoading && this.openSwalLoading();
			axios
				.get(`/contract/${contractId}/payment-schedules/${year}`)
				.then(response => {
					const dataBill = [];
					const data = response.data;
					if (data.status) {
						Object.keys(data.schedules).forEach(schedulesItem => {
							dataBill.push({
								dateBill: data.schedules[schedulesItem].scheduled_date_raw,
								status: data.schedules[schedulesItem].status,
								billingAmount: data.schedules[schedulesItem].amount,
								information:
									data.schedules[schedulesItem].payment_status_formatted,
								paymentDescription:
									data.schedules[schedulesItem].payment_description,
								amount: data.schedules[schedulesItem].amount,
								invoiceLink: data.schedules[schedulesItem].invoice_link,
								reminder: data.schedules[schedulesItem].manual_reminder,
								menu: data.schedules[schedulesItem].id,
								invoiceNumber: data.schedules[schedulesItem].invoice_number,
								notInMamipay:
									data.schedules[schedulesItem].payment_status ===
									'not_in_mamipay'
							});
						});
					}
					this.$store.commit('setBillingProfile', dataBill);
					this.countTotalTransfered();
					this.closeSwalLoading();
					this.$store.commit('setLoadingState', {
						type: 'contractBill',
						value: false
					});
				})
				.catch(error => {
					this.$store.commit('setLoadingState', {
						type: 'contractBill',
						value: false
					});
					bugsnagClient.notify(error);
					this.swalError(null, 'Terjadi galat. Silakan coba lagi.');
				});
		},

		goBack() {
			this.$router.go(-1);
		},

		goToBilling(seq = 'all') {
			this.$router.push(`/${seq}/bill`);
			window.location.reload(true);
		},

		goToBooking() {
			this.$router.push('/all/booking');
		},

		toastedBillingWarning(message) {
			this.$toasted.show(message, {
				type: 'info',
				className: 'toasted-default',
				theme: 'bubble',
				position: 'bottom-center',
				duration: 2000
			});
		},

		checkingEmptyFieldValue() {
			let countEmptyField = 0;
			this.additionalCosts.forEach(additionalCost => {
				if (additionalCost.field_title && additionalCost.field_value === 0) {
					countEmptyField++;
				}
			});
			return countEmptyField;
		},

		isInvalidBiodataForm() {
			// eslint-disable-next-line no-useless-escape
			const REGEX_CHECK_NUM_SYMBOL = /[^a-zA-Z\s\']/;

			return (
				!this.tenantProfile.roomId ||
				!this.tenantProfile.fullName ||
				REGEX_CHECK_NUM_SYMBOL.test(this.tenantProfile.fullName) ||
				!this.isValidRoomNumber ||
				!this.tenantProfile.gender ||
				!this.tenantProfile.status ||
				!this.tenantProfile.work ||
				!this.tenantProfile.phoneNumber ||
				!this.tenantProfile.email ||
				!validateEmail(this.tenantProfile.email) ||
				!this.tenantProfile.picture ||
				!this.tenantProfile.cardIdentity ||
				(this.tenantProfile.work === 'Mahasiswa' &&
					(!this.tenantProfile.parentName || !this.tenantProfile.parentNumber))
			);
		},

		isValidBillingForm() {
			return (
				!this.startDate ||
				!this.rentType ||
				(!this.duration && this.statusPage !== 'member') ||
				!this.amount ||
				this.checkingEmptyFieldValue() !== 0 ||
				(this.useDp &&
					(this.dpAmount === 0 ||
						this.dpAmount >= this.discountAmount ||
						this.dpAmount >= this.totalSimulatedCalculation)) ||
				(this.fineAmount > 0 &&
					(this.fineMaximumLength <= 0 || !this.fineMaximumLength)) ||
				(this.fixedBilling && !this.billingDate) ||
				(this.toggleRentDurationNew &&
					!this.newRentDuration &&
					this.statusPage === 'member')
			);
		},

		validationBillingForm() {
			if (this.isValidBillingForm()) {
				if (!this.amount && this.statusPage === 'member') {
					this.swalError(null, 'Hitungan sewa Pilihan anda Belum tersedia');
				} else {
					this.toastedBillingWarning('Ada beberapa isian yang belum diisi');
				}
			} else {
				if (this.statusPage === 'acceptBooking') {
					const isValidAdditionalCosts = this.validateAdditionalCosts();
					if (isValidAdditionalCosts) {
						this.acceptBookingRequest();
					} else {
						this.$toasted.show(
							'Tolong lengkapi keterangan biaya tambahan Anda',
							{
								type: 'info',
								className: 'toasted-default',
								theme: 'bubble',
								position: 'bottom-center',
								duration: 2000
							}
						);
					}
				} else {
					this.updateContract();
				}
			}
		},

		filterAdditionalCostValue() {
			if (this.additionalCosts.length <= 0) {
				return [];
			} else {
				const newAdditionalField = [];
				this.additionalCosts.forEach(additionalCostItem => {
					if (additionalCostItem.id && this.saveCostGroup) {
						this.deleteAdditionalCostField(additionalCostItem.id, false);
						newAdditionalField.push({
							field_title: additionalCostItem.field_title,
							field_value: additionalCostItem.field_value
						});
					} else {
						if (additionalCostItem.field_title) {
							newAdditionalField.push({
								field_title: additionalCostItem.field_title,
								field_value: additionalCostItem.field_value
							});
						}
					}
				});
				if (newAdditionalField.length === 0) {
					return [];
				} else {
					return newAdditionalField;
				}
			}
		},

		deleteAdditionalCostField(additionalFieldId) {
			this.openSwalLoading();
			axios
				.delete(`/additional-cost-field/delete/${additionalFieldId}`)
				.then(response => {
					const DATA_RESPONSE = response.data;
					if (DATA_RESPONSE.status) {
						return DATA_RESPONSE.status;
					}
					this.closeSwalLoading();
				})
				.catch(error => {
					bugsnagClient.notify(error);
					this.swalError(null, 'Terjadi galat. Silakan coba lagi.');
				});
		},

		getInterfaceType() {
			return navigator.isMobile || window.innerWidth < 768
				? 'mobile'
				: 'desktop';
		},

		updateBiodata() {
			this.openSwalLoading();

			const params = {
				room_id: this.roomId,
				gender: this.gender,
				marital_status: this.maritalStatus,
				name: this.fullName,
				occupation: this.occupation,
				parent_name: this.parentName,
				parent_phone_number: this.parentPhoneNumber,
				phone_number: this.phoneNumber,
				photo_document_id: this.documentId,
				photo_id: this.photoId,
				photo_identifier_id: this.photoIdentifierId,
				questions: this.questions,
				email: this.email,
				designer_room_id: this.designerRoomId,
				contract_id: parseInt(this.contractId)
			};

			axios
				.put(`/tenant/edit/${this.tenantProfileId}`, params)
				.then(response => {
					const responseData = response.data;

					if (responseData.status) {
						this.swalSuccess(null, 'Berhasil update biodata.');
					} else {
						this.swalError(null, responseData.meta.message);
					}
				})
				.catch(error => {
					bugsnagClient.notify(error);
					this.swalError(null, 'Terjadi galat. Silakan coba lagi.');
				});
		},

		updateContract() {
			this.openSwalLoading();

			const params = {
				room_id: this.roomId,
				name: this.fullName,
				phone_number: this.phoneNumber,
				room_number: this.roomNumber,
				gender: this.gender,
				email: this.email,
				occupation: this.occupation,
				marital_status: this.maritalStatus,
				questions: this.questions,
				start_date: this.startDate,
				rent_type: this.rentType,
				amount: this.discountAmount,
				duration: this.duration,
				photo_id: this.photoId,
				parent_name: this.parentName,
				parent_phone_number: this.parentPhoneNumber,
				photo_identifier_id: this.photoIdentifierId,
				photo_document_id: this.documentId,
				fixed_billing: this.fixedBilling,
				billing_date: this.billingDate,
				first_amount: this.firstAmount,
				deposit_amount: this.depositAmount,
				fine_amount: this.fineAmount,
				fine_maximum_length: this.fineMaximumLength,
				fine_duration_type: this.fineDurationType,
				existing_tenant: '',
				owner_accept: true,
				additional_costs: this.additionalCosts,
				save_cost_group: this.saveCostGroup,
				additional_duration: this.newRentDuration,
				designer_room_id: this.designerRoomId
			};

			axios
				.put(`/contract/update/${this.contractId}`, params)
				.then(response => {
					if (response.data.status) {
						this.swalSuccess(
							null,
							'Berhasil update contract.',
							this.goToBilling(this.userRoomId)
						);
					} else {
						this.swalError(null, response.data.meta.message);
					}
				})
				.catch(error => {
					bugsnagClient.notify(error);
					this.swalError(null, 'Terjadi galat. Silakan coba lagi.');
				});
		},

		validateAdditionalCosts() {
			for (let i = 0; i < this.additionalCosts.length; i++) {
				/* eslint-disable camelcase */
				const { field_title, field_value } = this.additionalCosts[i];
				if (i === this.additionalCosts.length - 1) {
					const onlyTitle = field_title && !Number(field_value);
					const onlyValue = !field_title && Number(field_value);
					if (onlyValue || onlyTitle) {
						return false;
					}
				} else {
					if (!field_title || !Number(field_value)) {
						return false;
					}
				}
				/* eslint-enable */
			}

			return true;
		},

		acceptBookingRequest() {
			this.openSwalLoading();

			const params = {
				room_id: this.roomId,
				name: this.fullName,
				phone_number: this.phoneNumber,
				room_number: this.roomNumber,
				gender: this.gender,
				email: this.email,
				occupation: this.occupation,
				marital_status: this.maritalStatus,
				questions: this.questions,
				start_date: this.startDate,
				rent_type: this.rentType,
				amount: this.discountAmount,
				duration: this.duration,
				photo_id: this.photoId,
				parent_name: this.parentName,
				parent_phone_number: this.parentPhoneNumber,
				photo_identifier_id: this.photoIdentifierId,
				photo_document_id: this.documentId,
				fixed_billing: this.fixedBilling,
				billing_date: this.billingDate,
				first_amount: this.firstAmount,
				deposit_amount: this.depositAmount,
				fine_amount: this.fineAmount,
				fine_maximum_length: this.fineMaximumLength,
				fine_duration_type: this.fineDurationType,
				existing_tenant: '',
				owner_accept: true,
				additional_costs: this.filterAdditionalCostValue(),
				save_cost_group: this.saveCostGroup,
				use_dp: this.useDp,
				dp_amount: this.dpAmount,
				dp_date: this.dpDate,
				dp_settlement_date: this.dpSettlementDate,
				designer_room_id: this.designerRoomId
			};

			axios
				.post(`/owner/booking/accept/${this.bookingId}`, params)
				.then(response => {
					if (response.data.status) {
						this.addConfirmBookingTrackEvent(params);
						this.swalSuccess(
							null,
							'Berhasil menerima penghuni baru.',
							this.goToBooking
						);
					} else {
						this.swalError(null, response.data.meta.message);
					}
				})
				.catch(error => {
					bugsnagClient.notify(error);
					this.swalError(null, 'Terjadi galat. Silakan coba lagi.');
				});
		},

		addConfirmBookingTrackEvent(confirmParams) {
			const otherPrice = [];
			const otherPriceName = [];
			const questions = [];
			const acceptBookingProfile = this.$store.state.manageBooking
				.acceptBookingProfile;

			confirmParams.additional_costs.forEach(cost => {
				if (cost.field_value > 0 && !!cost.field_title) {
					const otherPriceNameStr = `${cost.field_title}: ${cost.field_value}`;
					otherPrice.push(cost.field_value);
					otherPriceName.push(otherPriceNameStr);
				}
			});

			confirmParams.questions.forEach(question => {
				if (question && question.field_title) {
					questions.push(question.field_title);
				}
			});

			tracker('moe', [
				'[Owner] Confirm Booking',
				{
					booking_id: this.bookingCode,
					property_id: _get(confirmParams, 'room_id', null),
					property_type: 'kost',
					property_name: _get(this.userRoom, 'room_title'),
					property_city: _get(this.userRoom, 'area_formatted'),
					tenant_name: _get(confirmParams, 'name'),
					tenant_phone_number: _get(confirmParams, 'phone_number'),
					tenant_id_photo: _get(
						acceptBookingProfile,
						'tenant_photo_identifier.medium',
						''
					),
					tenant_gender:
						_get(confirmParams, 'gender') === 'female'
							? 'perempuan'
							: 'laki-laki',
					tenant_marital_status: _get(confirmParams, 'marital_status', ''),
					tenant_job: _get(confirmParams, 'occupation', ''),
					user_email: _get(confirmParams, 'email', null),
					tenant_guardians_name: _get(confirmParams, 'parent_name', null),
					tenant_guardians_phone_number: _get(
						confirmParams,
						'parent_phone_number',
						null
					),
					room_number: _get(confirmParams, 'room_number'),
					list_of_question: questions,
					booking_start_time: new Date(confirmParams.start_date),
					property_rent_type: _get(confirmParams, 'rent_type'),
					rent_price: this.amount ? this.amount : 0,
					booking_periode: _get(confirmParams, 'duration', 0),
					schedule_date: this.billingDate ? this.billingDate : '',
					prorate_price: this.firstAmount ? this.firstAmount : 0,
					deposit_price: _get(confirmParams, 'deposit_amount'),
					fine_price: _get(confirmParams, 'fine_amount'),
					owner_phone_number: _get(this.authData, 'phone_number', 0),
					owner_email: _get(this.authData, 'email', ''),
					requestDate: new Date(this.confirmParams.request_date),
					confirm_from: this.confirmParams.from + ' booking',
					fine_duration: _get(confirmParams, 'fine_maximum_length'),
					other_price_name: otherPriceName,
					other_price: otherPrice,
					dp_price: _get(confirmParams, 'dp_amount'),
					total_price: this.totalSimulatedCalculation
						? this.totalSimulatedCalculation
						: 0,
					goldplus_status: _get(this.userRoom, 'level_info.name', null)
				}
			]);
		},

		assignOtherCost() {
			this.costData.forEach(item => {
				this.otherCostList.facility.push(item.field_title);
				this.otherCostList.price.push(item.field_value);
			});
		},

		sendNotificationToTenant(invoiceId) {
			this.openSwalLoading();
			const params = {};
			axios
				.post(`/payment-reminder/${invoiceId}`, params)
				.then(response => {
					if (response.data.status) {
						this.toastedBillingWarning('Berhasil Kirim Pengingat.');
						this.closeSwalLoading();

						const updatedBillingProfile = [
							...this.$store.state.tenantProfile.billingProfile
						].map(data => {
							if (data.menu === invoiceId) {
								return {
									...data,
									reminder: false
								};
							}

							return data;
						});
						this.$store.commit('setBillingProfile', [...updatedBillingProfile]);
					} else {
						this.swalError(null, response.data.meta.message);
					}
					EventBus.$emit('isToggleModalRememberTenant', false);
				})
				.catch(error => {
					EventBus.$emit('isToggleModalRememberTenant', false);
					bugsnagClient.notify(error);
					this.swalError(
						null,
						'Terjadi galat kirim notifikasi. Silakan coba lagi.'
					);
				});
		},

		otherBillFilter(value) {
			const otherCost = [];
			if (value.fixed_cost) {
				value.fixed_cost.forEach(fixedCostItem => {
					otherCost.push({
						field_title: fixedCostItem.name,
						field_value: fixedCostItem.amount,
						old: true,
						field_id: fixedCostItem.id,
						new_field: false,
						type: 'fixed'
					});
				});
			}

			if (value.other_cost) {
				value.other_cost.forEach(otherCostItem => {
					if (
						otherCostItem.original_type !== 'deposit' &&
						otherCostItem.name !== 'Denda'
					) {
						otherCost.push({
							field_title: otherCostItem.name,
							field_value: otherCostItem.amount,
							old: true,
							field_id: otherCostItem.id,
							new_field: false,
							type: 'other',
							is_deleted: false
						});
					}
				});
			}

			this.$store.commit('setOtherBill', otherCost);
		}
	}
};

export default mixinTenantProfile;
