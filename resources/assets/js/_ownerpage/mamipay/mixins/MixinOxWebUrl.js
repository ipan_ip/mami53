const mixinOxWebUrl = {
	computed: {
		ownerDashboardUrl() {
			return oxWebUrl.substr(-1) === '/' ? oxWebUrl.slice(0, -1) : oxWebUrl;
		}
	}
};

export default mixinOxWebUrl;
