const mixinMaskingPhoneNumber = {
	computed: {
		statusPage() {
			return this.$store.state.tenantProfile.statusPage;
		}
	},
	methods: {
		maskingPhoneNumber(phoneNumber) {
			phoneNumber = !phoneNumber ? '-' : phoneNumber.toString();
			return this.statusPage != 'acceptBooking'
				? phoneNumber
				: phoneNumber.replace(/\d{3}$/g, 'XXX');
		}
	}
};

export default mixinMaskingPhoneNumber;
