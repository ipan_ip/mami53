import _find from 'lodash/find';
import _get from 'lodash/get';

const mixinAdditionalCost = {
	methods: {
		getFormattedAdditionalCost(additionalCost, multiplier = 1) {
			const formattedAdditionalCost = {};
			additionalCost.forEach(cost => {
				let data = null;
				if (cost.type === 'additional') {
					data = this.getFormattedOtherCost(cost.data || [], multiplier);
				} else if (cost.type === 'fine') {
					data = this.getSingleCostValue(cost.data, {
						price: ['price', 0],
						duration: ['meta.fine_maximum_length', 0],
						durationType: ['meta.fine_duration_type', 'day']
					});
				} else if (cost.type === 'dp') {
					data = this.getSingleCostValue(cost.data, {
						percentage: ['meta.percentage', 0]
					});
				} else if (cost.type === 'deposit') {
					data = this.getSingleCostValue(cost.data, {
						price: ['price', 0]
					});
				}
				formattedAdditionalCost[cost.type] = {
					data,
					type: cost.type,
					active: cost.active || false
				};
			});
			return formattedAdditionalCost;
		},

		getFormattedOtherCost(costs = [], multiplier = 1) {
			return costs.map(cost => {
				return {
					field_title: cost.name || '',
					field_value: (parseInt(cost.price) || 0) * (multiplier || 1)
				};
			});
		},

		getSingleCostValue(costs, formattedObj) {
			const result = {};
			const costData = costs[0] || {};
			Object.keys(formattedObj).forEach(key => {
				const [refKey, defaultValue] = formattedObj[key];
				if (/^meta\..+/g.test(refKey)) {
					const metaKey = refKey.replace('meta.', '');
					result[key] = this.getMetaValue(costData.meta, metaKey, defaultValue);
				} else {
					result[key] = _get(costData, refKey, defaultValue);
				}
			});
			return result;
		},

		getMetaValue(meta, key, defaultVal) {
			const metaData = _find(meta || {}, ['key', key]);
			return metaData ? metaData.value : defaultVal;
		},

		getDiscountedAmount(percentage = 0, amount = 0) {
			return Math.round((Number(percentage) / 100) * Number(amount));
		}
	}
};

export default mixinAdditionalCost;
