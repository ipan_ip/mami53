const mixinClipboard = {
	methods: {
		clipboardOnCopy() {
			this.showToast('Berhasil disalin!');
		},
		clipboardOnError() {
			this.showToast('Gagal menyalin!');
		},
		showToast(text) {
			this.$toasted.show(text, {
				theme: 'bubble',
				position: 'bottom-center',
				duration: 2000
			});
		}
	}
};

export default mixinClipboard;
