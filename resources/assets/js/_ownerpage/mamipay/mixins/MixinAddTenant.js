import _get from 'lodash/get';
import _find from 'lodash/find';
import _findIndex from 'lodash/findIndex';
import EventBus from '../event-bus/event-bus';
import { mapState, mapMutations, mapActions, mapGetters } from 'vuex';

import MixinQueryString from 'Js/@mixins/MixinGetQueryString';

const mixinAddTenant = {
	mixins: [MixinQueryString],

	data() {
		return {
			isLoadingKostList: false,
			isLoadingKostDetail: false
		};
	},

	computed: {
		...mapState('addTenant', [
			'ownerKostList',
			'selectedKost',
			'ownerKostListParams',
			'isSelectedKostFromQuery',
			'isMatchedQueryKost',
			'selectedKostFromQuery',
			'queryData',
			'kostFullModal',
			'isAllBbk'
		]),
		...mapGetters('addTenant', ['userInterface']),
		userData() {
			return this.$store.state.authData.all;
		},
		isShowFullLoading() {
			return (
				(this.kostListParams.offset === 0 && this.isLoadingKostList) ||
				this.isLoadingKostDetail
			);
		},
		kostList: {
			get() {
				return this.ownerKostList;
			},
			set(kostList) {
				this.setKostListResponse(kostList);
			}
		},
		kostListParams: {
			get() {
				return this.ownerKostListParams;
			},
			set(params) {
				this.setKostListParams(params);
			}
		},
		queryKostId: {
			get() {
				return this.queryData.kost;
			},
			set(value) {
				this.setQueryData({ kost: value });
			}
		},
		queryRoomUnitId: {
			get() {
				return this.queryData.room;
			},
			set(value) {
				this.setQueryData({ room: value });
			}
		},
		redirectionSourceSelectKos() {
			return this.$store.state.addTenant.redirectionSource.selectKos;
		},
		redirectionSourceCheckPhone() {
			return this.$store.state.addTenant.redirectionSource.checkPhone;
		}
	},

	methods: {
		...mapMutations('addTenant', [
			'setIsSelectedKostFromQuery',
			'setKostListParams',
			'setIsMatchedQueryKost',
			'setSelectedKostFromQuery',
			'setQueryData',
			'setKostFullModal',
			'setIsAllBbk',
			'setDisclaimerText',
			'setIsDepositShown',
			'setRedirectionSourceSelectKos',
			'setRedirectionSourceCheckPhone'
		]),
		...mapActions('addTenant', ['setSelectedKost', 'setKostListResponse']),

		updateSourceSelectKos() {
			if (this.getQueryString('from') === 'tenant-list') {
				this.setRedirectionSourceSelectKos('tenant-list');
			} else if (this.getQueryString('from') === 'dashboard') {
				this.setRedirectionSourceSelectKos('dashboard_tambah_kontrak');
			} else if (this.getQueryString('from') === 'add-contract') {
				this.setRedirectionSourceSelectKos('pilih_metode_tambah_kontrak');
			} else if (this.getQueryString('k') || this.getQueryString('r')) {
				this.setRedirectionSourceSelectKos('notifikasi');
			}
		},
		updateSourceCheckPhone() {
			if (this.getQueryString('from') === 'update-kamar') {
				this.setRedirectionSourceCheckPhone('update_kamar_dan_harga');
			} else if (this.$route.params.source === 'add-more') {
				this.setRedirectionSourceCheckPhone('popup_success_tambah_penyewa');
			} else if (this.getQueryString('k') || this.getQueryString('r')) {
				this.setRedirectionSourceCheckPhone('deeplink');
			}
		},
		checkPageSource() {
			if (this.getQueryString('interface') === 'ios') {
				this.$store.commit('addTenant/setWebviewValue', 'ios');
			} else if (this.getQueryString('interface') === 'android') {
				this.$store.commit('addTenant/setWebviewValue', 'android');
			} else {
				this.$store.commit('addTenant/setWebviewValue', '');
			}
		},
		setSelectedFromQuery() {
			this.queryKostId = this.getQueryString('k') || '';
			this.queryRoomUnitId = this.getQueryString('r') || '';
		},
		getKostListOffset() {
			if (this.selectedKostFromQuery && !this.isMatchedQueryKost) {
				return this.kostList.length - 1;
			}

			return this.kostList.length;
		},
		async getOwnerKostList() {
			this.isLoadingKostList = true;

			try {
				const response = await axios.get('/room/list', {
					params: {
						offset: this.getKostListOffset(),
						limit: this.kostListParams.limit,
						booking: true
					}
				});

				const responseStatus = _get(response, 'data.status', false);
				if (responseStatus) {
					const kostList = _get(response, 'data.data', []);
					this.setIsAllBbk(_get(response, 'data.is_all_bbk', false));

					const disclaimerText = _get(
						response,
						'data.security_disclaimer_label',
						''
					);
					const isDepositShown = _get(
						response,
						'data.add_tenant_using_deposit',
						false
					);

					this.setDisclaimerText(disclaimerText);
					this.setIsDepositShown(isDepositShown);

					this.handleKostListFetched(kostList);
					this.kostListParams = {
						offset: this.getKostListOffset(),
						hasMore: _get(response, 'data.has_more', true)
					};
					if (
						this.queryKostId &&
						!this.isSelectedKostFromQuery &&
						!this.isMatchedQueryKost
					) {
						this.checkSelectedKostFromQueryString();
					}
				} else {
					this.kostList = [];
					const errorMessage = _get(response, 'data.meta.message', '');
					this.showErrorMessage(errorMessage);
				}
				this.isLoadingKostList = false;
			} catch (error) {
				this.kostList = [];
				this.isLoadingKostList = false;
				this.showErrorMessage();
				bugsnagClient.notify(error);
			}
		},
		showKostFullModal(kostId) {
			this.setKostFullModal({ show: true, id: kostId });
		},
		closeKostFullModal() {
			this.setKostFullModal({ show: false, id: 0 });
		},
		async getOwnerDetailKost(id) {
			this.isLoadingKostDetail = true;
			try {
				const response = await axios.get(`/room/detail/${id}`);
				const responseStatus = _get(response, 'data.status', false);
				if (responseStatus) {
					const kost = _get(response, 'data.room', null);
					const isBooking = _get(response, 'data.booking_active', false);
					if (kost && isBooking) {
						this.insertKostToList(kost);
						if (!this.checkIsKostAvailable(kost)) {
							this.showKostFullModal(kost.id);
						} else {
							this.selectKost(kost, true);
						}
					}
				}
				this.setIsSelectedKostFromQuery(true);
				this.isLoadingKostDetail = false;
			} catch (error) {
				this.isLoadingKostDetail = false;
				bugsnagClient.notify(error);
			}
		},
		selectKost(kost, isFromQueryString = false) {
			this.setSelectedKost(kost);
			this.$store.commit('setUserRoom', kost);

			if (!this.isSelectedKostFromQuery && isFromQueryString) {
				this.$router.push({ name: 'addTenantCheckPhone' });
				this.setIsSelectedKostFromQuery(true);
			} else if (!isFromQueryString) {
				this.$router.push({ name: 'addTenantCheckPhone' });
			}
		},
		showErrorMessage(message = '') {
			if (message) {
				this.swalError(null, message);
			} else {
				EventBus.$emit('showModalError');
			}
		},
		checkSelectedKostFromQueryString() {
			if (!this.queryKostId) return;

			let selectedKost = null;
			if (
				this.selectedKost &&
				String(this.queryKostId) === String(this.selectedKost.id)
			) {
				selectedKost = this.selectedKost;
			} else {
				selectedKost = _find(
					this.kostList,
					kost => String(this.queryKostId) === String(kost.id)
				);
			}

			if (selectedKost && this.checkIsKostAvailable(selectedKost)) {
				this.setIsMatchedQueryKost(true);
				this.selectKost(selectedKost, false);
			} else if (selectedKost && !this.checkIsKostAvailable(selectedKost)) {
				this.setIsMatchedQueryKost(true);
				this.showKostFullModal(selectedKost.id);
			} else if (this.queryKostId && !this.isSelectedKostFromQuery) {
				this.getOwnerDetailKost(this.queryKostId);
			}
		},
		checkIsKostAvailable(kost) {
			return kost && kost.room_available && parseInt(kost.room_available) >= 1;
		},
		insertKostToList(kost) {
			this.setSelectedKostFromQuery(kost);
			const matchedKostList = _find(
				this.kostList,
				kostListItem => kostListItem.id === kost.id
			);
			if (!matchedKostList) {
				this.kostList.unshift(kost);
			} else {
				this.setIsMatchedQueryKost(true);
			}
		},
		handleKostListFetched(kostList) {
			if (this.selectedKostFromQuery) {
				const assignedKostList = [...kostList];
				const matchedKostQuery = _findIndex(
					assignedKostList,
					kostListItem =>
						String(kostListItem.id) === String(this.selectedKostFromQuery.id)
				);
				if (matchedKostQuery >= 0) {
					this.setIsMatchedQueryKost(true);
					assignedKostList.splice(matchedKostQuery, 1);
				}
				this.kostList.push(...assignedKostList);
			} else {
				this.kostList.push(...kostList);
			}
		}
	}
};

export default mixinAddTenant;
