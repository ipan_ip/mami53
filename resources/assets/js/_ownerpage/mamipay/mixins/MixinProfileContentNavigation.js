const mixinProfileContentNavigation = {
	methods: {
		goBack() {
			if (this.$attrs && this.$attrs['previous-route']) {
				const { path, matched } = this.$attrs['previous-route'];
				this.goToPreviousPage(path, matched);
			} else {
				document.referrer === ''
					? this.goToDashboard()
					: (window.location = document.referrer);
			}
		},
		goToPreviousPage(path, matched) {
			if (
				!path ||
				path === '/' ||
				path === this.$route.path ||
				matched[0].path === this.$route.matched[0].path
			) {
				document.referrer === ''
					? this.goToDashboard()
					: (window.location = document.referrer);
			} else {
				this.$router.push({ path });
			}
		},
		goToDashboard() {
			window.location = oxWebUrl;
		}
	}
};

export default mixinProfileContentNavigation;
