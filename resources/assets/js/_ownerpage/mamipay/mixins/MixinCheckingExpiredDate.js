const mixinCheckingExpiredDate = {
	methods: {
		checkingConfirmExpiredDate(expiredDate) {
			const countDownDate = this.$dayjs(expiredDate).valueOf();
			const now = new Date().getTime();
			const distanceDate = countDownDate - now;
			return distanceDate;
		}
	}
};

export default mixinCheckingExpiredDate;
