import EventBus from '../event-bus/event-bus';
import * as storage from 'Js/@utils/storage';

const mixinCheckMamipayBooking = {
	computed: {
		isActivateMamipay() {
			return this.$store.state.isMamipayUser;
		}
	},

	methods: {
		checkingActiveMamipay() {
			axios
				.get('owner/detail')
				.then(response => {
					const dataStatus = response.data || false;

					if (dataStatus && dataStatus.is_mamipay_user) {
						EventBus.$emit('isLoadingMain', false);
						this.$store.commit('setIsMamipayUser', dataStatus.is_mamipay_user);
						this.$store.commit('setOwnerDetail', dataStatus.profile);

						if (storage.local.getItem('activate_booking_redirection_source')) {
							storage.local.removeItem('activate_booking_redirection_source');
						}
					} else {
						window.location.href = `${oxWebUrl}/kos/booking/register`;
					}
				})
				.catch(error => {
					bugsnagClient.notify(error);
					this.swalError(
						null,
						'Terjadi galat dalam mengecek mamipay status. Silakan coba lagi.'
					);
				});
		},

		checkingActiveBooking() {
			axios
				.get('owner/booking/available')
				.then(response => {
					const dataStatus = response.data.data || false;
					if (dataStatus) {
						EventBus.$emit('isLoadingMain', false);
						this.$store.commit('setTenantStatus', dataStatus);
					}
				})
				.catch(error => {
					bugsnagClient.notify(error);
					this.swalError(null, 'Terjadi galat. Silakan coba lagi.');
				});
		}
	}
};

export default mixinCheckMamipayBooking;
