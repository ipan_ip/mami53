const mixinBookingRoom = {
	computed: {
		roomAvailable: {
			get() {
				return this.$store.state.allDetailRoom.room_available || 0;
			},
			set(roomAvailable) {
				this.$store.commit('setRoomAvailability', roomAvailable);
			}
		},
		isKostFull() {
			return this.roomAvailable <= 0;
		},
		roomAvailableStr() {
			return this.isKostFull
				? 'Kamar penuh'
				: `Sisa ${this.roomAvailable} kamar`;
		}
	},
	methods: {
		sanitizeStr(str, removedStr) {
			if (!str) return '';
			const removedStrIndex = str.toLowerCase().indexOf(removedStr);
			if (removedStrIndex >= 0) {
				str = str.slice(removedStrIndex + removedStr.length, str.length);
				return str.trim();
			}
			return str;
		}
	}
};

export default mixinBookingRoom;
