import VueRouter from 'vue-router';
Vue.use(VueRouter);

const BillingManagementMain = require('../components/profile-tenant/BillingManagementMain')
	.default;
const ContentPayment = require('../components/profile-tenant/ContentPayment')
	.default;

const routes = [
	{
		path: '/profile-tenant/:tenantId',
		name: 'BillingManagementMain',
		component: BillingManagementMain,
		redirect: '/profile-tenant/:tenantId/payment-tenant',
		children: [
			{
				path: 'payment-tenant/',
				name: 'paymentTenant',
				component: ContentPayment
			}
		]
	}
];

const router = new VueRouter({
	mode: 'history',
	base: '/ownerpage/billing-management',
	routes
});

export default router;
