import StoreTenantProfile from './modules/StoreProfileTenant';
import StoreManageBooking from './modules/StoreManageBooking';

const currentTime = new Date();

const defaultStore = {
	modules: {
		tenantProfile: StoreTenantProfile,
		manageBooking: StoreManageBooking
	},
	state: {
		token: document.head.querySelector('meta[name="csrf-token"]').content,
		authCheck: {},
		authData: {},
		tenantStatus: {},
		isMamipayUser: false,
		userRooms: [],
		userRoom: [],
		userRoomStatus: {},
		allDetailRoom: {},
		listBill: {},
		listBillReport: {},
		listBooking: {},
		totalIncome: '0',
		totalTransfer: '0',
		searchBill: {
			month: currentTime.getMonth() + 1,
			year: currentTime.getFullYear(),
			criteria: '',
			limit: '',
			offset: '',
			perPage: 3,
			totalPage: 0,
			sort: 'desc',
			order: 'name',
			billDatePeriode: '',
			selectedNumberPagination: 1
		},
		billReport: {
			month: currentTime.getMonth() + 1,
			year: currentTime.getFullYear()
		},
		showHighlight: false,
		rejectReasons: [],
		ownerDetail: {},
		bankList: [],
		bulkProcessBookingActivation: false
	},
	mutations: {
		setAuthCheck(state, payload) {
			state.authCheck = payload;
		},
		setAuthData(state, payload) {
			state.authData = payload;
		},
		setTenantStatus(state, payload) {
			state.tenantStatus = payload;
		},
		setIsMamipayUser(state, payload) {
			state.isMamipayUser = payload;
		},
		setUserRooms(state, payload) {
			state.userRooms = payload;
		},
		setUserRoom(state, payload) {
			state.userRoom = payload;
		},
		setUserRoomStatus(state, payload) {
			state.userRoomStatus = payload;
		},
		setListBill(state, payload) {
			state.listBill = payload;
		},
		setListBillReport(state, payload) {
			state.listBillReport = payload;
		},
		setListBooking(state, payload) {
			state.listBooking = payload;
		},
		setTotalIncome(state, payload) {
			state.totalIncome = payload;
		},
		setTotalTransfer(state, payload) {
			state.totalTransfer = payload;
		},
		setSearchBillMonth(state, payload) {
			state.searchBill.month = payload;
		},
		setSearchBillYear(state, payload) {
			state.searchBill.year = payload;
		},
		setSearchBillCriteria(state, payload) {
			state.searchBill.criteria = payload;
		},
		setSearchBillLimit(state, payload) {
			state.searchBill.limit = payload;
		},
		setSearchBillOffset(state, payload) {
			state.searchBill.offset = payload;
		},
		setSearchBillTotalPage(state, payload) {
			state.searchBill.totalPage = payload;
		},
		setSearchBillOrder(state, payload) {
			state.searchBill.order = payload;
		},
		setSearchBillSort(state, payload) {
			state.searchBill.sort = payload;
		},
		setSearchBillPerPage(state, payload) {
			state.searchBill.perPage = payload;
		},
		setBillDatePeriode(state, payload) {
			state.searchBill.billDatePeriode = payload;
		},
		setBillSelectedNumberPagination(state, payload) {
			state.searchBill.selectedNumberPagination = payload;
		},
		setBillReportYear(state, payload) {
			state.billReport.year = payload;
		},
		setBillReportMonth(state, payload) {
			state.billReport.month = payload;
		},
		setPriceDaily(state, payload) {
			state.userRoom.price_daily = payload;
		},
		setPriceMonthly(state, payload) {
			state.userRoom.price_monthly = payload;
		},
		setPriceWeekly(state, payload) {
			state.userRoom.price_weekly = payload;
		},
		setPriceQuarterly(state, payload) {
			state.userRoom.price_quarterly = payload;
		},
		setPriceSemiannualy(state, payload) {
			state.userRoom.price_semiannualy = payload;
		},
		setPriceYearly(state, payload) {
			state.userRoom.price_yearly = payload;
		},
		setAllDetailRoom(state, payload) {
			state.allDetailRoom = payload;
		},
		setHighlight(state, payload) {
			state.showHighlight = payload;
		},
		setRoomAvailability(state, payload) {
			state.allDetailRoom.room_available = payload;
		},
		setRejectReasons(state, payload) {
			state.rejectReasons = payload;
		},
		setOwnerDetail(state, payload) {
			state.ownerDetail = payload;
		},
		setBankList(state, payload) {
			state.bankList = payload;
		},
		setBulkProcessBookingActivation(state, payload) {
			state.bulkProcessBookingActivation = payload;
		}
	},
	actions: {
		fetchBankList({ commit }) {
			return new Promise((resolve, reject) => {
				axios
					.get('/bank/list')
					.then(response => {
						if (response.data.status) {
							commit('setBankList', response.data.data);
						} else {
							bugsnagClient.notify(response);
							alert(
								'Galat tidak diketahui.\nSilakan cek koneksi internet Anda dan coba lagi.'
							);
						}

						resolve();
					})
					.catch(error => {
						reject(error);
					});
			});
		}
	}
};

export default defaultStore;
