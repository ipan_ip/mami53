const storeManageBooking = {
	state: {
		listBooking: {
			total: 0
		},
		listBookingSelected: '',
		acceptBookingProfile: '',
		isLoadingBookingList: true,
		searchBooking: {
			limit: '',
			offset: '',
			perPage: 3,
			totalPage: 0,
			sort: 'desc',
			order: 'created_at',
			group: '&group[]=0',
			selectedNumberPagination: 1
		},
		extras: {
			rejectBookingFrom: ''
		}
	},
	mutations: {
		setSearchBookingSort(state, payload) {
			state.searchBooking.sort = payload;
		},
		setBookingSelectedNumberPagination(state, payload) {
			state.searchBooking.selectedNumberPagination = payload;
		},
		setSearchBookingGroup(state, payload) {
			state.searchBooking.group = payload;
		},
		setSearchBookingOrder(state, payload) {
			state.searchBooking.order = payload;
		},
		setSearchBookingLimit(state, payload) {
			state.searchBooking.limit = payload;
		},
		setSearchBookingOffset(state, payload) {
			state.searchBooking.offset = payload;
		},
		setListBooking(state, payload) {
			state.listBooking = payload;
		},
		setListBookingSelected(state, payload) {
			state.listBookingSelected = payload;
		},
		setSearchBookingPerPage(state, payload) {
			state.searchBooking.perPage = payload;
		},
		setSearchBookingTotalPage(state, payload) {
			state.searchBooking.totalPage = payload;
		},
		setAcceptBookingProfile(state, payload) {
			state.acceptBookingProfile = payload;
		},
		setRejectBookingFrom(state, payload) {
			state.extras.rejectBookingFrom = payload;
		},
		setBookingListLoading(state, payload) {
			state.isLoadingBookingList = payload;
		}
	},
	actions: {}
};

export default storeManageBooking;
