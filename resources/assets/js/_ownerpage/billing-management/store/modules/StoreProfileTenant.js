const getDefaulttenantProfile = () => {
	return {
		id: '',
		roomId: '',
		roomNumber: '',
		fullName: '',
		gender: '',
		status: null,
		work: null,
		cardIdentity: '',
		picture: '',
		document: '',
		phoneNumber: '',
		email: '',
		parentName: '',
		parentNumber: '',
		question: [{ field_title: '', field_value: '' }],
		lastPictureImage: '',
		lastIdentifierImage: '',
		roomAllotment: {
			id: null,
			name: '',
			floor: ''
		},
		designerRoomId: null,
		selectedRoomAllotment: null
	};
};

const getDefaulttenantBill = () => {
	return {
		checkInDate: '',
		rentCount: '',
		rentDuration: '',
		toggleRentDurationNew: false,
		newRentDuration: 0,
		rentPrice: '',
		discountRentPrice: '',
		fixedBilling: false,
		dateBill: 0,
		prorated: 0,
		realProrated: 0,
		proratedDate: '',
		costDeposit: 0,
		costFine: 0,
		durationDay: '0',
		dayType: 'day',
		otherCost: [{ field_title: '', field_value: '', id: 0 }],
		forOtherTenant: false,
		downPayment: false,
		costDownPayment: 0,
		downPaymentDate: '',
		downPaymentSettlementDate: '',
		totalSimulatedCalculation: 0
	};
};

const getDefaultWidgetInfo = () => {
	return {
		contractId: '',
		profilePicture: '',
		tenantName: '',
		roomNumber: '',
		phoneNumber: '',
		countDuration: '',
		billingDate: '',
		contractType: '',
		totalTransferredAmount: '',
		totalContractAmount: '',
		settlementAmount: ''
	};
};

const storeTenantProfile = {
	state: {
		userSelectedProfile: {},
		roomSelectedDetail: {
			rentCountList: '',
			rentDurationList: ''
		},
		widgetInfo: getDefaultWidgetInfo(),
		billingProfile: [],
		statusPage: 'member', // Avaible options: 'member', 'newMember', 'acceptBooking',
		tenantProfile: getDefaulttenantProfile(),
		tenantBill: getDefaulttenantBill(),
		otherBill: [
			{
				field_title: '',
				field_value: '',
				old: false,
				field_id: '',
				new_field: true
			}
		],
		newTenantNumberPhone: '',
		unavailableRent: [],
		selectedPaymentYear: '',
		confirmParams: {
			request_date: '',
			from: ''
		},
		selectedRoomNumber: {},
		loadingState: {
			contract: false,
			contractBill: false,
			rentCount: false
		}
	},
	getters: {
		isLoadingContractForm(state) {
			return Object.keys(state.loadingState).some(
				key => !!state.loadingState[key]
			);
		}
	},
	mutations: {
		setStatusPage(state, payload) {
			state.statusPage = payload;
		},
		setRentDurationList(state, payload) {
			state.roomSelectedDetail.rentDurationList = payload;
		},
		setRentCountList(state, payload) {
			state.roomSelectedDetail.rentCountList = payload;
		},
		setUserSelectedProfile(state, payload) {
			state.userSelectedProfile = payload;
		},
		setStatusSelectedProfile(state, payload) {
			state.userSelectedProfile.status = payload;
		},
		setRoomId(state, payload) {
			state.tenantProfile.roomId = payload;
		},
		setRoomNumber(state, payload) {
			state.tenantProfile.roomNumber = payload;
		},
		setRoomAllotment(state, payload) {
			state.tenantProfile.roomAllotment = payload;
			state.tenantProfile.designerRoomId = payload.id;
		},
		setSelectedRoomAlloment(state, payload) {
			state.tenantProfile.selectedRoomAllotment = payload;
		},
		setTenantId(state, payload) {
			state.tenantProfile.id = payload;
		},
		setFullName(state, payload) {
			state.tenantProfile.fullName = payload;
		},
		setGender(state, payload) {
			state.tenantProfile.gender = payload;
		},
		setStatus(state, payload) {
			state.tenantProfile.status = payload;
		},
		setCountDuration(state, payload) {
			state.widgetInfo.countDuration = payload;
		},
		setWork(state, payload) {
			state.tenantProfile.work = payload;
		},
		setPicture(state, payload) {
			state.tenantProfile.picture = payload;
		},
		setCardIdentity(state, payload) {
			state.tenantProfile.cardIdentity = payload;
		},
		setDocument(state, payload) {
			state.tenantProfile.document = payload;
		},
		setPhoneNumber(state, payload) {
			state.tenantProfile.phoneNumber = payload;
		},
		setEmail(state, payload) {
			state.tenantProfile.email = payload;
		},
		setParentName(state, payload) {
			state.tenantProfile.parentName = payload;
		},
		setParentNumber(state, payload) {
			state.tenantProfile.parentNumber = payload;
		},
		setQuestion(state, payload) {
			state.tenantProfile.question = payload;
		},
		setLastPictureImage(state, payload) {
			state.tenantProfile.lastPictureImage = payload;
		},
		setLastIdentifierImage(state, payload) {
			state.tenantProfile.lastIdentifierImage = payload;
		},
		setOtherCost(state, payload) {
			state.tenantBill.otherCost = payload;
		},
		setForOtherTenant(state, payload) {
			state.tenantBill.forOtherTenant = payload;
		},
		setCheckInDate(state, payload) {
			state.tenantBill.checkInDate = payload;
		},
		setRentCount(state, payload) {
			state.tenantBill.rentCount = payload;
		},
		setRentDuration(state, payload) {
			state.tenantBill.rentDuration = payload;
		},
		setToggleRentDurationNew(state, payload) {
			state.tenantBill.toggleRentDurationNew = payload;
		},
		setNewRentDuration(state, payload) {
			state.tenantBill.newRentDuration = payload;
		},
		setRentPrice(state, payload) {
			state.tenantBill.rentPrice = payload;
		},
		setDiscountRentPrice(state, payload) {
			state.tenantBill.discountRentPrice = payload;
		},
		setDateBill(state, payload) {
			state.tenantBill.dateBill = payload;
		},
		setProrated(state, payload) {
			state.tenantBill.prorated = payload;
		},
		setRealProrated(state, payload) {
			state.tenantBill.realProrated = payload;
		},
		setProratedDate(state, payload) {
			state.tenantBill.proratedDate = payload;
		},
		setFixedBilling(state, payload) {
			state.tenantBill.fixedBilling = payload;
		},
		setCostDeposit(state, payload) {
			state.tenantBill.costDeposit = payload;
		},
		setCostFine(state, payload) {
			state.tenantBill.costFine = payload;
		},
		setDurationDay(state, payload) {
			state.tenantBill.durationDay = payload;
		},
		setDayType(state, payload) {
			state.tenantBill.dayType = payload;
		},
		setDownPayment(state, payload) {
			state.tenantBill.downPayment = payload;
		},
		setCostDownPayment(state, payload) {
			state.tenantBill.costDownPayment = payload;
		},
		setDownPaymentDate(state, payload) {
			state.tenantBill.downPaymentDate = payload;
		},
		setDownPaymentSettlementDate(state, payload) {
			state.tenantBill.downPaymentSettlementDate = payload;
		},
		setTotalSimulatedCalculation(state, payload) {
			state.tenantBill.totalSimulatedCalculation = payload;
		},
		setWidgetInfo(state, payload) {
			state.widgetInfo = payload;
		},
		setTotalTransferredAmount(state, payload) {
			state.widgetInfo.totalTransferredAmount = payload;
		},
		setBillingProfile(state, payload) {
			state.billingProfile = payload;
		},
		setOtherBill(state, payload) {
			state.otherBill = payload;
		},
		setNewTenantNumberPhone(state, payload) {
			state.newTenantNumberPhone = payload;
		},
		setunavailableRent(state, payload) {
			state.unavailableRent = payload;
		},
		setSelectedPaymentYear(state, payload) {
			state.selectedPaymentYear = payload;
		},
		setConfirmParamsDate(state, payload) {
			state.confirmParams.request_date = payload;
		},
		setConfirmParamsFrom(state, payload) {
			state.confirmParams.from = payload;
		},
		setLoadingState(state, { type, value }) {
			state.loadingState[type] = value;
		},
		resetStateTenantProfile(state) {
			Object.assign(state.tenantProfile, getDefaulttenantProfile());
		},
		resetStatetenantBill(state) {
			Object.assign(state.tenantBill, getDefaulttenantBill());
		},
		resetStateWidgetInfo(state) {
			Object.assign(state.widgetInfo, getDefaultWidgetInfo());
		}
	},
	actions: {
		resetTenantProfile({ commit }) {
			commit('resetStateTenantProfile');
		},
		resetTenantBill({ commit }) {
			commit('resetStatetenantBill');
		},
		resetWidgetInfo({ commit }) {
			commit('resetStateWidgetInfo');
		},
		resetDataAcceptBooking({ commit }) {
			commit('resetDataAcceptBooking');
		}
	}
};

export default storeTenantProfile;
