import Dayjs from 'vue-dayjs';
import 'dayjs/locale/id';
import Vuex from 'vuex';
import vueScrollTo from 'vue-scroll-to';
import VTooltip from 'v-tooltip';
import VueClipboard from 'vue-clipboard2';

import VeeValidateLanguage from 'vee-validate/dist/locale/id';
import VeeValidate, { Validator } from 'vee-validate';

import defaultStore from './store/index';
import router from './router/index';

import 'Js/@mixins/MixinNavigatorIsMobile';
import 'Js/@mixins/MixinSwalLoading';
import 'Js/@mixins/MixinSwalSuccessError';
import App from './components/App.vue';

import mixinAuth from 'Js/@mixins/MixinAuth';

window.axios.defaults.baseURL = '/ownerpage/mamipay/request/';
window.axios.defaults.headers = {};

Vue.use(Dayjs, {
	lang: 'id'
});

const optionsVueScroll = {
	speed: 500,
	padding: 0,
	movingFrequency: 15
};
Vue.use(vueScrollTo, optionsVueScroll);

Vue.use(VueClipboard);

Vue.use(VTooltip);

Vue.use(Vuex);

window.validator = new Validator();
Validator.localize('id', VeeValidateLanguage);
Vue.use(VeeValidate, {
	locale: 'id',
	fieldsBagName: 'vvFields'
});

Vue.component('app', App);

const store = new Vuex.Store(defaultStore);

const app = new Vue({
	el: '#app',
	mixins: [mixinAuth],
	router,
	store,
	created() {
		this.$dayjs.locale('id');
	},
	mounted() {
		try {
			document.getElementById('preloading').remove();
		} catch (e) {
			document.getElementById('preloading').style.display = 'none';
		}
	}
});
