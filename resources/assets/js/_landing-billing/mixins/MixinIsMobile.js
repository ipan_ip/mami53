export default {
	computed: {
		isMobile() {
			return navigator.isMobile || window.innerWidth < 768;
		}
	}
};
