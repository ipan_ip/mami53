const mixinEllipsis = {
	methods: {
		ellipsis(str, maxLength = 20) {
			if (typeof str === 'string') {
				if (str.length > maxLength) {
					return str.slice(0, maxLength) + '...';
				}
			}
			return str;
		}
	}
};

export default mixinEllipsis;
