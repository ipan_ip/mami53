export const genderLabelInID = {
	male: 'Laki-laki',
	female: 'Perempuan'
};

export const unitLabelInID = {
	day: 'Hari',
	week: 'Minggu',
	month: 'Bulan',
	year: 'Tahun'
};

export const rentCountInID = {
	day: 'Perhari',
	week: 'Perminggu',
	month: 'Perbulan',
	'3_month': 'Per 3 Bulan',
	'6_month': 'Per 6 Bulan',
	year: 'Pertahun'
};

const mixinProcessingDataList = {
	methods: {
		mixinChangeDataValueByType(type, value, option = {}) {
			switch (type) {
				case 'nominal':
					return Number(value).toLocaleString('id');

				case 'price':
					return value ? `Rp ${Number(value).toLocaleString('id')}` : '-';

				case 'date':
					if (!value) return '-';
					if (option.format) {
						return this.$dayjs(value).format(option.format);
					} else {
						return `${this.$dayjs(value).format('DD MMM YYYY, HH:mm')} WIB`;
					}

				case 'gender':
					return genderLabelInID[value] || '-';

				default:
					return value || '-';
			}
		},

		processingPriceUnit(tenantList, tenantData) {
			const durationRegex = /^(\d+)_(\w+)$/;
			let durationUnit = tenantData.duration_unit;
			let duration = '';
			const durationUnitBreaked = durationUnit.match(durationRegex);
			if (durationUnitBreaked && durationUnitBreaked.length) {
				durationUnit = unitLabelInID[durationUnitBreaked[2]];
				duration = `${durationUnitBreaked[1]} `;
			} else {
				durationUnit = unitLabelInID[durationUnit];
			}

			return {
				...tenantList,
				label: `Harga ${duration}${durationUnit}an`,
				value: tenantData.amount
					? `Rp. ${Number(tenantData.amount).toLocaleString('id')}`
					: '-'
			};
		},

		rentCountToId(value) {
			return rentCountInID[value] || '-';
		},

		createDurationValue(durationUnit, duration) {
			const durationRegex = /^(\d+)_(\w+)$/;
			const durationUnitBreaked = durationUnit.match(durationRegex);
			if (durationUnitBreaked && durationUnitBreaked.length) {
				return `${Number(duration) * Number(durationUnitBreaked[1])} ${
					unitLabelInID[durationUnitBreaked[2]]
				}`;
			} else {
				return `${duration} ${unitLabelInID[durationUnit]}`;
			}
		},

		getDataLink(type, value) {
			if (type === 'email' && value) {
				return `mailto:${value}`;
			} else if (type === 'phone' && value) {
				return `tel:${value}`;
			} else {
				return '';
			}
		}
	}
};

export default mixinProcessingDataList;
