const mixinUniqueId = {
	computed: {
		mixHasUniqueId() {
			return id => this.$store.getters.isUniqueIdMatch(id);
		}
	},

	watch: {
		$route: {
			immediate: true,
			handler(value) {
				const uniqueId = value.query.unique_id;
				if (!uniqueId || !this.mixHasUniqueId(uniqueId)) {
					if (typeof this.throwBackRoute === 'function') {
						this.throwBackRoute();
					} else {
						this.$router.go(-1);
					}
				}
			}
		}
	},

	beforeDestroy() {
		this.$store.commit('setUniqueId', '');
	}
};

export default mixinUniqueId;
