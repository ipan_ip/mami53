const mixinFilterCurrency = {
	filters: {
		currencyID: value => {
			if (value && isFinite(value)) {
				return 'Rp ' + Number(value).toLocaleString('id');
			} else {
				return '';
			}
		}
	}
};

export default mixinFilterCurrency;
