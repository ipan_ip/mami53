const mixinHelper = {
	methods: {
		mixSubtitleIcon(icon) {
			switch (icon) {
				case 'Data Kontrak':
					return 'file';
				case 'Data Penyewa':
					return 'user-circle-outline';
				case 'Data Properti':
					return 'home-circle';
				case 'Data Properti Potensial':
					return 'home-circle';
				default:
					console.log('Missing icon?');
			}
		}
	}
};

export default mixinHelper;
