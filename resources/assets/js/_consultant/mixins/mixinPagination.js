import ConsultantPagination from 'Consultant/components/ConsultantPagination.vue';

const mixinPagination = {
	components: {
		ConsultantPagination
	},
	data() {
		return {
			isPaginationOnly: false,
			scrollTop: true,
			customPagination: {},
			pagination: {
				totalData: 0,
				offset: 0,
				page: 1,
				sort_by: '',
				limit: 10,
				search: '',
				filters: []
			}
		};
	},
	watch: {
		$route: {
			immediate: true,
			handler(routeData) {
				this.setDefaultPagination(routeData);
				this.watchRouteChanges(routeData);
				this.handleGetQueriesFromRoute(routeData);
			}
		}
	},
	methods: {
		setDefaultPagination(route) {
			if (typeof this.defaultPagination === 'function') {
				this.defaultPagination(route);
			} else {
				this.pagination.sort_by = 'updated_at';
			}
		},

		changeQueryPaginate(dataChanges) {
			this.pagination = { ...this.pagination, ...dataChanges };
		},

		paginationChanged(pageNumber) {
			this.pagination.page = pageNumber;
			this.pagination.offset =
				pageNumber * this.pagination.limit - this.pagination.limit;

			this.handleChangeRoute(
				this.pagination,
				this.isPaginateOnly,
				this.customPagination
			);
		},

		handleChangeRoute(
			paginationQueries,
			isPaginateOnly = false,
			customPagination = {}
		) {
			let queries = {
				page: paginationQueries.page
			};

			if (!isPaginateOnly) {
				queries = {
					...queries,
					sort_by: paginationQueries.sort_by,
					search: paginationQueries.search
				};

				if (paginationQueries.filters) {
					queries = {
						...queries,
						filters: paginationQueries.filters.join(',')
					};
				}
			}

			if (Object.keys(customPagination).length) {
				queries = {
					...queries,
					...customPagination
				};
			}

			this.$router.push({
				name: this.$route.name,
				query: queries
			});

			if (this.scrollTop) {
				window.scrollTo({
					top: 0,
					behavior: 'smooth'
				});
			}
		},

		handleGetQueriesFromRoute(routeData) {
			const queryKeys = Object.keys(routeData.query);

			if (queryKeys.length) {
				// eslint-disable-next-line camelcase
				const { page, sort_by, search } = routeData.query;
				const currentPage = Number(page || this.pagination.page);
				this.pagination.page = currentPage > 100 ? 100 : currentPage;
				// eslint-disable-next-line camelcase
				this.pagination.sort_by = sort_by || this.pagination.sort_by;
				this.pagination.search = search || this.pagination.search;
				this.pagination.offset =
					this.pagination.page * this.pagination.limit - this.pagination.limit;
				this.pagination.filters = this.getDataFilterFromQuery(routeData);
			}
			this.getDataByPagination();
		},

		getDataFilterFromQuery(routeData) {
			if (routeData.query.filters) {
				return routeData.query.filters.split(',');
			}
			return [];
		},

		getDataByPagination() {
			if (typeof this.triggerGetData === 'function') {
				this.triggerGetData(this.pagination);
			}
		},

		watchRouteChanges(routeData) {
			if (typeof this.triggerRouteChanged === 'function') {
				this.triggerRouteChanged(routeData);
			}
		},

		createPaginationQueryUrl(queries, isPaginationOnly = false) {
			let queryFilters = '';
			if (queries.filters && queries.filters.length) {
				queryFilters += queries.filters.reduce((filter, eachFilter) => {
					return `${filter}&filter[]=${eachFilter}`;
				}, '');
			}
			if (queries.search) {
				queryFilters += `&search=${queries.search}`;
			}

			return (
				`limit=${queries.limit}` +
				`&offset=${queries.offset}` +
				(!isPaginationOnly
					? `&sort_by=${queries.sort_by || ''}${queryFilters}`
					: '')
			);
		},

		resetFilterAndGoTo(routeData) {
			return () => {
				this.pagination.filters = [];
				this.$router.push(routeData);
			};
		}
	}
};

export default mixinPagination;
