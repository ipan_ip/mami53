const mixinConverter = {
	methods: {
		mixRupiahConverter(value) {
			return (+value).toLocaleString('id-ID', {
				style: 'currency',
				currency: 'IDR',
				minimumFractionDigits: 0
			});
		},

		mixDateConverter(dateString, short = false) {
			const temp = short ? 'DD MMM YYYY' : 'DD MMMM YYYY';
			return this.$dayjs(dateString)
				.locale('id')
				.format(temp);
		}
	},

	filters: {
		mixFilterRupiah: value => {
			return (+value).toLocaleString('id-ID', {
				style: 'currency',
				currency: 'IDR',
				minimumFractionDigits: 0
			});
		}
	}
};

export default mixinConverter;
