import { get as _get } from 'lodash';

export default {
	methods: {
		/**
		 *
		 * @param {Object} options
		 * @property {String} url - the endpoint of Request GET (required)
		 * @property {String} field - the field of data in the response (default: data)
		 *
		 * @returns {Promise<Object>}
		 * @property {Boolean} success
		 * @property {Any} data
		 * @property {String} message
		 * @property {Response} response
		 */
		async mixApiGet(options) {
			const field = options.field || 'data';
			const result = {
				success: false,
				data: null,
				message: '',
				response: null
			};
			const defaultMessage = 'Terjadi galat, silahkan coba lagi';
			try {
				const res = await axios.get(options.url);

				if (res.data.status) {
					result.success = true;
					result.data = _get(res.data, field);
					result.message = 'Succeed to get data';
				} else {
					result.message =
						res.data.message || /* istanbul ignore next */ defaultMessage;
				}
				result.response = res.data;
			} catch (error) {
				bugsnagClient.notify(error);
				result.message = defaultMessage;
			}

			return result;
		}
	}
};
