import VueRouter from 'vue-router';
import routePath from './paths';
import { isLoggedIn, getCredential } from 'Consultant/helpers/credentials';

// eslint-disable-next-line no-undef
Vue.use(VueRouter);

const router = new VueRouter({
	mode: 'history',
	base: '/consultant-tools',
	routes: routePath,
	scrollBehavior(to, from, savedPosition) {
		if (savedPosition) {
			return savedPosition;
		} else {
			return { x: 0, y: 0 };
		}
	}
});

router.beforeEach((to, from, next) => {
	if (window && window.location.pathname === '/') {
		window.location.replace(window.location.origin + '/consultant-tools/');
		return;
	}
	const {
		meta: { auth = false, consultant = false }
	} = to;
	const authData = getCredential();

	if (auth) {
		if (isLoggedIn()) {
			if (consultant && !authData.consultant) next({ name: 'dashboard' });
			else next();
		} else {
			next({ name: 'login' });
		}
	} else {
		if (isLoggedIn()) next({ name: from.name || 'dashboard' });
		else next();
	}
});

export default router;
