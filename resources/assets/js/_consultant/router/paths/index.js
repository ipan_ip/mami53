import DashboardPage from 'Consultant/pages/dashboard/DashboardPage.vue';
import LoginPage from 'Consultant/pages/login/LoginPage.vue';
import PropertyListPage from 'Consultant/pages/property/PropertyLists.vue';
import PropertyDetailPage from 'Consultant/pages/property/PropertyDetail.vue';
import UpdateRoomAndPricePage from 'Consultant/pages/property/UpdateRoomAndPrice.vue';
import ActiveTenantListPage from 'Consultant/pages/active-tenant/ActiveTenantLists.vue';
import ActiveTenantDetailPage from 'Consultant/pages/active-tenant/ActiveTenantDetail.vue';
import PotentialTenantListPage from 'Consultant/pages/potential-tenant/PotentialTenantLists.vue';
import PotentialTenantDetailPage from 'Consultant/pages/potential-tenant/PotentialTenantDetail.vue';
import EditNotePotentialTenant from 'Consultant/pages/potential-tenant/EditNotePage.vue';
import AddPotentialTenant from 'Consultant/pages/potential-tenant/AddPotentialTenant.vue';
import ContractList from 'Consultant/pages/contract/ContractList.vue';
import CreateContractPage from 'Consultant/pages/create-contract/CreateContractPage.vue';
import InvoiceList from 'Consultant/pages/invoice/InvoiceList.vue';
import EditInvoiceBase from 'Consultant/pages/contract/EditInvoiceBase.vue';
import EditCosts from 'Consultant/pages/contract/EditCosts.vue';
import EditAmount from 'Consultant/pages/contract/EditAmount.vue';
import RoomAllotmentList from 'Consultant/pages/room-allotment/List.vue';

import activityListPath from './activityList';
import potentialPropertyPath from './potentialProperty';
import salesMotionPath from './salesMotion';
import potentialOwner from './potentialOwner';
import bookingManagement from './bookingManagement';

const routePath = [
	{
		path: '/',
		name: 'login',
		component: LoginPage,
		meta: {
			theme: {
				background: '#f4f4f4'
			},
			navbarOptions: {
				primary: true,
				actionIcons: {
					back: false
				}
			}
		}
	},
	{
		path: '/dashboard',
		name: 'dashboard',
		component: DashboardPage,
		meta: {
			auth: true,
			navbarExtended: true,
			theme: {
				background: '#f4f4f4'
			},
			navbarOptions: {
				primary: true,
				actionIcons: {
					back: false,
					burger: true
				}
			}
		}
	},
	{
		path: '/property',
		name: 'property',
		component: PropertyListPage,
		meta: {
			auth: true,
			navbarOptions: {
				title: 'Kelola Properti',
				prevPathName: 'dashboard'
			},
			theme: {
				background: '#ffffff'
			}
		}
	},
	{
		path: '/property/:id',
		name: 'property.detail',
		component: PropertyDetailPage,
		meta: {
			auth: true,
			navbarOptions: {
				title: 'Detail Properti'
			},
			theme: {
				background: '#ffffff'
			}
		}
	},
	{
		path: '/property/:id/edit/room-and-price',
		name: 'property.edit.room-price',
		component: UpdateRoomAndPricePage,
		meta: {
			auth: true,
			consultant: true,
			navbarOptions: {
				title: 'Update Kamar dan Harga'
			},
			theme: {
				background: '#ffffff'
			}
		}
	},
	{
		path: '/active-tenant',
		name: 'active-tenant',
		component: ActiveTenantListPage,
		meta: {
			auth: true,
			navbarOptions: {
				title: 'Penyewa Aktif',
				prevPathName: 'dashboard'
			},
			theme: {
				background: '#ffffff'
			}
		}
	},
	{
		path: '/active-tenant/:id',
		name: 'active-tenant.detail',
		component: ActiveTenantDetailPage,
		meta: {
			auth: true,
			navbarOptions: {
				title: 'Detail Penyewa'
			},
			theme: {
				background: '#ffffff'
			}
		}
	},
	{
		path: '/potential-tenant',
		name: 'potential-tenant',
		component: PotentialTenantListPage,
		meta: {
			auth: true,
			navbarOptions: {
				title: 'Penyewa Potensial',
				prevPathName: 'dashboard'
			},
			theme: {
				background: '#ffffff'
			}
		}
	},
	{
		path: '/potential-tenant/create',
		name: 'add.potential-tenant',
		component: AddPotentialTenant,
		meta: {
			auth: true,
			consultant: true,
			navbarOptions: {
				title: 'Potensial Penyewa'
			},
			theme: {
				background: '#ffffff'
			}
		}
	},
	{
		path: '/potential-tenant/:id',
		name: 'potential-tenant.detail',
		component: PotentialTenantDetailPage,
		meta: {
			auth: true,
			navbarOptions: {
				title: 'Detail Penyewa'
			},
			theme: {
				background: '#ffffff'
			}
		}
	},
	{
		path: '/potential-tenant/:tenantId/edit',
		name: 'edit.potential-tenant',
		component: AddPotentialTenant,
		meta: {
			auth: true,
			consultant: true,
			navbarOptions: {
				title: 'Potensial Penyewa'
			},
			theme: {
				background: '#ffffff'
			}
		}
	},
	{
		path: '/potential-tenant/:id/edit-note',
		name: 'edit.note.potential-tenant',
		component: EditNotePotentialTenant,
		meta: {
			auth: true,
			navbarOptions: {
				title: 'Ubah Catatan'
			},
			theme: {
				background: '#ffffff'
			}
		}
	},
	{
		path: '/contract',
		name: 'contract',
		component: ContractList,
		meta: {
			auth: true,
			navbarOptions: {
				title: 'Kelola Kontrak',
				prevPathName: 'dashboard'
			},
			theme: {
				background: '#ffffff'
			}
		}
	},
	{
		path: '/contract/create',
		name: 'contract.create',
		component: CreateContractPage,
		meta: {
			auth: true,
			navbarOptions: {
				title: 'Kontrak Baru',
				prevPathName: 'dashboard'
			},
			theme: {
				background: '#ffffff'
			}
		}
	},
	{
		path: '/contract/create/room-number/:propertyId',
		name: 'contract.create.room-number',
		component: () =>
			import('Consultant/pages/create-contract/ChooseRoomNumber.vue'),
		meta: {
			auth: true,
			navbarOptions: {
				title: 'Nomor Kamar'
			},
			theme: {
				background: '#ffffff'
			}
		}
	},
	{
		path: '/invoice/:id/:booking/:contract_status',
		name: 'invoice',
		component: InvoiceList,
		meta: {
			auth: true,
			navbarOptions: {
				title: 'Tagihan'
			},
			theme: {
				background: '#ffffff'
			}
		}
	},
	{
		path: '/contract/invoice/:invoiceId/',
		component: EditInvoiceBase,
		children: [
			{
				path: 'edit-costs',
				name: 'invoice.edit-costs',
				component: EditCosts,
				meta: {
					auth: true,
					navbarOptions: {
						title: 'Ubah Invoice'
					},
					theme: {
						background: '#ffffff'
					}
				}
			},
			{
				path: 'edit-amount',
				name: 'invoice.edit-amount',
				component: EditAmount,
				meta: {
					auth: true,
					navbarOptions: {
						title: 'Ubah Invoice'
					},
					theme: {
						background: '#ffffff'
					}
				}
			}
		]
	},

	...activityListPath,

	...potentialPropertyPath,

	...salesMotionPath,

	...potentialOwner,

	...bookingManagement,

	{
		path: '/room-allotment/:song_id',
		name: 'room-allotment',
		component: RoomAllotmentList,
		meta: {
			auth: true,
			navbarOptions: {
				title: 'Update Ketersediaan Kamar',
				style: {
					borderBottom: '1px solid #e8e8e8',
					marginBottom: 'unset'
				}
			},
			theme: {
				background: '#ffffff'
			}
		}
	}
];

export default routePath;
