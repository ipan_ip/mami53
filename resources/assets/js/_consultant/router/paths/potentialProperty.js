export default [
	{
		path: '/potential-property',
		name: 'potential-property',
		component: () =>
			import('Consultant/pages/potential-property/ListPotentialProperty'),
		meta: {
			auth: true,
			navbarOptions: {
				title: 'Properti Potensial',
				actionIcons: {
					burger: true
				}
			},
			theme: {
				background: '#ffffff'
			}
		}
	},
	{
		path: '/potential-property/:ownerId/add',
		name: 'potential-property.add',
		component: () =>
			import('Consultant/pages/potential-property/AddPotentialProperty'),
		meta: {
			auth: true,
			navbarOptions: {
				title: 'Tambah Properti'
			},
			theme: {
				background: '#ffffff'
			},
			contentOptions: {
				style: {
					paddingBottom: '80px'
				}
			}
		}
	},
	{
		path: '/potential-property/:ownerId/edit/:id',
		name: 'potential-property.edit',
		component: () =>
			import('Consultant/pages/potential-property/AddPotentialProperty'),
		meta: {
			auth: true,
			navbarOptions: {
				title: 'Edit Properti'
			},
			theme: {
				background: '#ffffff'
			},
			contentOptions: {
				style: {
					paddingBottom: '80px'
				}
			}
		}
	},
	{
		path: '/potential-property/:ownerId/:id',
		name: 'potential-property.detail',
		component: () =>
			import('Consultant/pages/potential-property/DetailPotentialProperty'),
		meta: {
			auth: true,
			navbarOptions: {
				title: 'Detail Potensial',
				prevPathName: 'potential-property',
				style: {
					borderBottom: '1px solid #e8e8e8'
				},
				actionIcons: {
					menu: true
				},
				menuOptions: {
					prefix: 'PROPERTY',
					options: [
						{
							icon: 'edit',
							title: 'Edit',
							type: 'edit'
						},
						{
							icon: 'bin',
							title: 'Hapus',
							type: 'delete'
						}
					]
				}
			},
			theme: {
				background: '#ffffff'
			}
		}
	}
];
