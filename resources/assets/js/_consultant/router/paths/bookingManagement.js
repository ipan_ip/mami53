export default [
	{
		path: '/booking-management',
		name: 'booking-management',
		component: () => import('Consultant/pages/booking-management/List'),
		meta: {
			auth: true,
			navbarOptions: {
				title: 'Kelola Booking',
				prevPathName: 'dashboard',
				style: {
					borderBottom: '1px solid #e8e8e8',
					marginBottom: 'unset'
				},
				actionIcons: {
					burger: true
				}
			},
			contentOptions: {
				style: {
					padding: 'unset'
				}
			},
			theme: {
				background: '#f6f6f6'
			}
		}
	},
	{
		path: '/booking-management/:id',
		name: 'booking-management.detail',
		component: () => import('Consultant/pages/booking-management/Detail'),
		meta: {
			auth: true,
			navbarOptions: {
				title: 'Detail Booking',
				subtitle: ' Loading...',
				prevPathName: 'dashboard',
				style: {
					borderBottom: '1px solid #e8e8e8'
				}
			},
			theme: {
				background: '#ffffff'
			}
		}
	},
	{
		path: '/booking-management/:id/accept',
		name: 'booking-management.accept',
		component: () => import('Consultant/pages/booking-management/Accept'),
		meta: {
			auth: true,
			navbarOptions: {
				title: 'Terima Booking',
				style: {
					borderBottom: '1px solid #e8e8e8'
				}
			},
			theme: {
				background: '#ffffff'
			}
		}
	},
	{
		path: '/booking-management/:id/reject',
		name: 'booking-management.reject',
		component: () => import('Consultant/pages/booking-management/Reject'),
		meta: {
			auth: true,
			navbarOptions: {
				title: 'Tolak Booking',
				style: {
					borderBottom: '1px solid #e8e8e8'
				}
			},
			theme: {
				background: '#ffffff'
			}
		}
	}
];
