export default [
	{
		path: '/potential-owner',
		name: 'potential-owner',
		component: () => import('Consultant/pages/potential-owner/Lists'),
		meta: {
			auth: true,
			navbarOptions: {
				title: 'Pemilik Potensial',
				prevPathName: 'dashboard',
				style: {
					borderBottom: '1px solid #e8e8e8',
					marginBottom: 'unset'
				},
				actionIcons: {
					burger: true
				}
			},
			theme: {
				background: '#F6F6F6'
			},
			contentOptions: {
				style: {
					padding: 'unset'
				}
			}
		}
	},
	{
		path: '/potential-owner/add',
		name: 'po.add',
		component: () => import('Consultant/pages/potential-owner/Add'),
		meta: {
			auth: true,
			navbarOptions: {
				title: 'Tambah Pemilik Potensial',
				prevPathName: 'potential-owner',
				style: {
					borderBottom: '1px solid #e8e8e8'
				}
			},
			theme: {
				background: '#ffffff'
			},
			contentOptions: {
				style: {
					paddingBottom: '80px'
				}
			}
		}
	},
	{
		path: '/potential-owner/edit/:id',
		name: 'po.edit',
		component: () => import('Consultant/pages/potential-owner/Add'),
		meta: {
			auth: true,
			navbarOptions: {
				title: 'Ubah Data Pemilik Potential',
				style: {
					borderBottom: '1px solid #e8e8e8'
				}
			},
			theme: {
				background: '#ffffff'
			},
			contentOptions: {
				style: {
					paddingBottom: '80px'
				}
			}
		}
	},
	{
		path: '/potential-owner/:id',
		name: 'po.detail',
		component: () => import('Consultant/pages/potential-owner/Detail'),
		meta: {
			auth: true,
			navbarOptions: {
				title: 'Detail Pemilik Potensial',
				style: {
					borderBottom: '1px solid #e8e8e8',
					marginBottom: 'unset'
				},
				actionIcons: {
					menu: true
				},
				menuOptions: {
					prefix: 'OWNER',
					options: [
						{
							icon: 'edit',
							title: 'Edit',
							type: 'edit'
						},
						{
							icon: 'bin',
							title: 'Hapus',
							type: 'delete'
						}
					]
				}
			},
			theme: {
				background: '#F6F6F6'
			},
			contentOptions: {
				style: {
					padding: 'unset'
				}
			}
		}
	}
];
