export default [
	{
		path: '/sales-motion',
		name: 'sales-motion',
		component: () => import('Consultant/pages/sales-motion/Lists.vue'),
		meta: {
			auth: true,
			navbarOptions: {
				title: 'Sales Motion',
				prevPathName: 'dashboard',
				style: {
					borderBottom: '1px solid #e8e8e8',
					marginBottom: 'unset'
				},
				actionIcons: {
					burger: true
				}
			},
			theme: {
				background: '#ffffff'
			},
			contentOptions: {
				style: {
					padding: 'unset'
				}
			}
		}
	},
	{
		path: '/sales-motion/search',
		name: 'sm.search',
		component: () => import('Consultant/pages/sales-motion/Search'),
		meta: {
			auth: true,
			navbar: false,
			theme: {
				background: '#ffffff'
			},
			contentOptions: {
				style: {
					padding: 'unset'
				}
			}
		}
	},
	{
		path: '/sales-motion/:id',
		name: 'sm.detail',
		component: () => import('Consultant/pages/sales-motion/Detail.vue'),
		meta: {
			auth: true,
			navbarOptions: {
				title: 'Detail Sales Motion',
				prevPathName: 'sales-motion',
				style: {
					borderBottom: '1px solid #e8e8e8'
				}
			},
			theme: {
				background: '#ffffff'
			}
		}
	},
	{
		path: '/sales-motion/:id/report/add',
		name: 'sm.report.add',
		component: () => import('Consultant/pages/sales-motion/AddReport.vue'),
		meta: {
			auth: true,
			navbarOptions: {
				title: 'Tambah Laporan',
				style: {
					borderBottom: '1px solid #e8e8e8'
				}
			},
			theme: {
				background: '#ffffff'
			}
		}
	},
	{
		path: '/sales-motion/:id/report/search-data',
		name: 'sm.report.search-data',
		component: () => import('Consultant/pages/sales-motion/SearchData.vue'),
		meta: {
			auth: true,
			navbar: false,
			theme: {
				background: '#ffffff'
			},
			contentOptions: {
				style: {
					padding: 'unset'
				}
			}
		}
	},
	{
		path: '/sales-motion/:id/report/:reportId',
		name: 'sm.report.detail',
		component: () => import('Consultant/pages/sales-motion/DetailReport.vue'),
		meta: {
			auth: true,
			navbarOptions: {
				title: 'Detail Laporan',
				prevPathName: 'dashboard',
				style: {
					borderBottom: '1px solid #e8e8e8',
					marginBottom: 'unset'
				}
			},
			theme: {
				background: '#ffffff'
			},
			contentOptions: {
				style: {
					padding: 'unset'
				}
			}
		}
	}
];
