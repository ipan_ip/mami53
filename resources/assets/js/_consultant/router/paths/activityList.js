export default [
	{
		path: '/activity',
		name: 'activity',
		component: () => import('Consultant/pages/activity/ActivityListPage'),
		meta: {
			auth: true,
			consultant: true,
			navbarOptions: {
				title: 'Kelola Tugas',
				prevPathName: 'dashboard',
				style: {
					borderBottom: '1px solid #e8e8e8',
					marginBottom: 'unset'
				}
			},
			theme: {
				background: '#f8f8f8'
			},
			contentOptions: {
				style: {
					padding: 'unset'
				}
			}
		}
	},
	{
		path: '/activity/search',
		name: 'activity.search',
		component: () => import('Consultant/pages/activity/Search'),
		meta: {
			auth: true,
			consultant: true,
			navbar: false,
			contentOptions: {
				style: {
					padding: 'unset'
				}
			}
		}
	},
	{
		path: '/activity/funnels',
		name: 'activity.funnels',
		component: () => import('Consultant/pages/activity/Funnels'),
		meta: {
			auth: true,
			consultant: true,
			navbarOptions: {
				title: 'Funnel',
				style: {
					borderBottom: '1px solid #e8e8e8',
					marginBottom: 'unset'
				}
			},
			theme: {
				background: '#fff'
			},
			contentOptions: {
				style: {
					padding: 'unset'
				}
			}
		}
	},
	{
		path: '/activity/staging/:funnel_id/:funnel_name/:funnel_type',
		name: 'activity.staging',
		component: () => import('Consultant/pages/activity/Staging'),
		meta: {
			auth: true,
			consultant: true,
			navbarOptions: {
				title: 'DBET Funnel',
				prevPathName: 'activity',
				style: {
					borderBottom: '1px solid #e8e8e8',
					marginBottom: 'unset'
				}
			}
		}
	},
	{
		path:
			'/activity/staging/search/:funnel_id/:stage/:funnel_name/:stage_name/:data_type',
		name: 'activity.staging-search',
		component: () => import('Consultant/pages/activity/StagingSearch'),
		meta: {
			auth: true,
			consultant: true,
			navbarOptions: {
				title: '',
				style: {
					borderBottom: '1px solid #e8e8e8',
					marginBottom: 'unset'
				}
			},
			theme: {
				background: '#fff'
			}
		}
	},
	{
		path: '/activity/detail',
		name: 'activity.detail',
		component: () => import('Consultant/pages/activity/Detail'),
		meta: {
			auth: true,
			consultant: true,
			navbarOptions: {
				title: 'Data Penyewa Potensial',
				style: {
					borderBottom: '1px solid #e8e8e8',
					marginBottom: 'unset'
				}
			},
			theme: {
				background: '#f8f8f8'
			},
			contentOptions: {
				style: {
					padding: 'unset'
				}
			}
		}
	},
	{
		path: '/activity/staging/log/:taskId',
		name: 'activity.staging-log',
		component: () => import('Consultant/pages/activity/StagingLog'),
		meta: {
			auth: true,
			consultant: true,
			navbarOptions: {
				title: 'Riwayat Data',
				style: {
					borderBottom: '1px solid #e8e8e8',
					marginBottom: 'unset'
				}
			},
			theme: {
				background: '#f8f8f8'
			},
			contentOptions: {
				style: {
					padding: 'unset'
				}
			}
		}
	},
	{
		path: '/activity/staging/form/:funnel_id/:progress_id/:stage_id/:task_id',
		name: 'activity.staging-form',
		component: () => import('Consultant/pages/activity/StagingForm'),
		meta: {
			auth: true,
			consultant: true,
			navbarOptions: {
				title: 'Nama Orang',
				style: {
					borderBottom: '1px solid #e8e8e8',
					marginBottom: 'unset'
				}
			},
			theme: {
				background: '#fff'
			}
		}
	},
	{
		path:
			'/activity/staging/form/:edit/:funnel_id/:progress_id/:stage_id/:task_id',
		name: 'activity.staging-form.edit',
		component: () => import('Consultant/pages/activity/StagingForm'),
		meta: {
			auth: true,
			consultant: true,
			navbarOptions: {
				title: 'Nama Orang',
				style: {
					borderBottom: '1px solid #e8e8e8',
					marginBottom: 'unset'
				}
			},
			theme: {
				background: '#fff'
			}
		}
	}
];
