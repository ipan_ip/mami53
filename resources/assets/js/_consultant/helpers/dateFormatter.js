export default function dateFormatter(date) {
	const months = [
		'Jan',
		'Feb',
		'Mar',
		'Apr',
		'May',
		'Jun',
		'Jul',
		'Aug',
		'Sep',
		'Oct',
		'Nov',
		'Dec'
	];

	const current = date.split(' ')[0].split('-');

	const formatted = `${months[current[1] - 1]} ${current[2]}, ${current[0]}`;

	return formatted;
}
