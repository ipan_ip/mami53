export function subtitleIcon(icon) {
	switch (icon) {
		case 'Data Kontrak':
			return 'file';
		case 'Data Penyewa':
			return 'user-circle-outline';
		case 'Data Property':
			return 'home-circle';
		default:
			console.log('Missing icon?');
	}
}
