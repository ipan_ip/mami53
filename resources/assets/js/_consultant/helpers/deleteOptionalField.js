export default function(obj, optionalKeys = []) {
	if (obj && typeof obj === 'object') {
		const keys = Object.keys(obj);
		const finalObj = {};
		if (optionalKeys.length) {
			keys.forEach(function(key) {
				const isOptional = optionalKeys.indexOf(key) !== -1;
				const isEmptyValue = !!(obj[key] === null || obj[key] === '');
				if (!isOptional || (isOptional && !isEmptyValue)) {
					finalObj[key] = obj[key];
				}
			});
		} else {
			keys.forEach(function(key) {
				if (obj[key] !== null || obj[key] !== '') {
					finalObj[key] = obj[key];
				}
			});
		}
		return finalObj;
	} else {
		return obj;
	}
}
