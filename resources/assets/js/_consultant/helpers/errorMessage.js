/**
 *
 * @param {Object} response - Mamikos BE's response API
 * @param {Number} minLenghReturned - minimal message which returned
 * @return {Array} array of messages
 */
export function getErrorMessage(response, minLenghReturned = null) {
	const { message = {}, meta } = response;
	const messages = [];
	if (Object.keys(message).length) {
		// eslint-disable-next-line no-unused-vars
		for (const [_, value] of Object.entries(message)) {
			messages.push(value[0]);
		}
	} else if (meta.message) {
		messages.push(meta.message);
	} else {
		messages.push('Terjadi galat, silahkan coba lagi');
	}

	return minLenghReturned ? messages.splice(0, minLenghReturned) : messages;
}
