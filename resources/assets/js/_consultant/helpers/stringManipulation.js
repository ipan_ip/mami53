export function toKebebCase(str, originalCase = 'camel') {
	switch (originalCase) {
		default:
			return str.replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase();
	}
}

export function capitalize(str) {
	const splited = str.toLowerCase().split(' ');
	const processed = splited.map(word => {
		return word.charAt(0).toUpperCase() + word.slice(1);
	});
	return processed.join(' ');
}
