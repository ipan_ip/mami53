export const TRACKERS = {
	MOE: 'moe',
	GA: 'ga',
	LOGGER: 'logger'
};

const CreateTracker = (() => {
	/**
	 * @private
	 */
	const globalAuthCheck = window.authCheck;
	/**
	 * @private
	 */
	const globalAuthData = window.authData;

	/**
	 * checking property of authData and authCheck on window object
	 * @returns {boolean}
	 * @private
	 */
	function isGlobalAuthInstalledBefore() {
		return Boolean(globalAuthCheck && globalAuthData);
	}

	/**
	 * stop code and throw error
	 * @param {string} message
	 * @return {never}
	 * @private
	 */
	function stop(message) {
		throw new Error(message);
	}

	/**
	 *
	 * @param {TRACKERS} trackerTypes - type of tracker that will installed
	 * @param {object} userLoggedIn
	 * @returns {void}
	 * @private
	 */
	function initTracker(trackerTypes, userLoggedIn) {
		if (userLoggedIn && userLoggedIn.constructor === Object) {
			/*
			 * assign all auth global variables to used in the Logger function
			 * logger function on tracking-logger-api.blade.php
			 */
			if (!isGlobalAuthInstalledBefore()) {
				window.authCheck = null;
				window.authData = null;
			}

			window.authCheck = { all: true };
			window.authData = { all: userLoggedIn };

			for (const type of trackerTypes) {
				if (Object.values(TRACKERS).includes(type)) {
					this[type] = function(params, callback) {
						this.tracker.call(null, type, params, callback);
					};
				}
			}
		}
	}

	class CreateTrackerClass {
		moe() {}
		ga() {}
		logger() {}

		constructor(...options) {
			if (typeof tracker === 'function') {
				this.tracker = tracker;
				const user = options[options.length - 1];
				const types = options.slice(0, -1);

				initTracker.bind(this)(types, user);
			} else {
				stop('Tracker does not installed yet');
			}
		}

		restoreGlobalAuth() {
			window.authCheck = globalAuthCheck;
			window.authData = globalAuthData;
		}
	}

	return CreateTrackerClass;
})();

export default CreateTracker;
