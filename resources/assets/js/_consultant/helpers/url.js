export function makeQueryParam(obj) {
	const query = [];
	if (obj.constructor === Object) {
		const keys = Object.keys(obj);
		for (let i = 0; i < keys.length; i++) {
			if (Array.isArray(obj[keys[i]])) {
				/* istanbul ignore else */
				if (obj[keys[i]].length) {
					query.push(createMultipleParam(obj[keys[i]], keys[i]));
				}
			} else {
				query.push(`${keys[i]}=${obj[keys[i]]}`);
			}
		}
	}

	return query.join('&');
}

export function createMultipleParam(paramValues, paramKey) {
	const params = paramValues.map(value => `${paramKey}[]=${value}`);
	return params.join('&');
}

/**
 *
 * @param  {String} items - a query string
 * @returns {String}
 */
export function mergeParams(...items) {
	let result = '';

	for (const item of items) {
		result += item ? `&${item}` : '';
	}

	return result.replace('&', '');
}

export function transformToArray(str, divider = ',') {
	if (str) {
		return str.split(divider);
	}
	return [];
}
