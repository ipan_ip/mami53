const uniqueId = (prefix = '') => {
	return (
		(prefix ? prefix + '_' : '') +
		Math.random()
			.toString(36)
			.substr(2, 5) +
		'_' +
		Math.random()
			.toString(36)
			.substr(2, 9) +
		'_' +
		Math.floor(Date.now()).toString(36)
	);
};

export default uniqueId;
