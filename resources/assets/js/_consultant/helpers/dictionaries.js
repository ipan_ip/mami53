import durationDict from './data/durationDicts.json';

export function durationInId(en) {
	return durationDict[en];
}
