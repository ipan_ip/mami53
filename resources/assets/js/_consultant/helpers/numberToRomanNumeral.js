// https://codepen.io/jkulton/pen/QjOZQB
export default function numberToRomanNumeral(number) {
	let ourNumber = number;
	let roman = '';

	while (ourNumber >= 1000) {
		roman += 'M';
		ourNumber = ourNumber - 1000;
	}

	while (ourNumber >= 500) {
		roman += 'D';
		ourNumber = ourNumber - 500;
	}

	while (ourNumber >= 100) {
		roman += 'C';
		ourNumber = ourNumber - 100;
	}

	if (ourNumber < 100 && ourNumber > 89) {
		roman += 'XC';
		ourNumber = ourNumber - 90;
	}

	while (ourNumber >= 50) {
		roman += 'L';
		ourNumber = ourNumber - 50;
	}

	if (ourNumber < 50 && ourNumber > 39) {
		roman += 'XL';
		ourNumber = ourNumber - 40;
	}

	while (ourNumber > 9) {
		roman += 'X';
		ourNumber = ourNumber - 10;
	}

	// special case for 9's since they don't follow normal convention i.e. "IX"
	if (ourNumber === 9) {
		roman += 'IX';
		ourNumber = ourNumber - 9;
	}

	while (ourNumber > 4) {
		roman += 'V';
		ourNumber = ourNumber - 5;
	}

	if (ourNumber === 4) {
		roman += 'IV';
		ourNumber = ourNumber - 4;
	}

	while (ourNumber > 0) {
		roman += 'I';
		ourNumber = ourNumber - 1;
	}

	// returns string with characters added as program progressed
	return roman;
}