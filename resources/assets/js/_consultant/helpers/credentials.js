import { getAccessToken, getRefreshToken } from './cookiesToken';
import * as storage from 'Js/@utils/storage';

/**
 * check user was logged in or not
 * @returns { Boolean }
 */
export function isLoggedIn() {
	return !!getAccessToken() || !!getRefreshToken();
}

export function saveCredential(data) {
	const credential = JSON.stringify(data);
	storage.local.setItem('CONSULTANT_USER', credential);
}

export function getCredential(defaultReturn = null) {
	const credential = storage.local.getItem('CONSULTANT_USER');
	return credential ? JSON.parse(credential) : defaultReturn;
}

export function removeCredential() {
	storage.local.removeItem('CONSULTANT_USER');
}
