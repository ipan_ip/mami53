export function getOffsetLeft(targetEl, parentEl = null) {
	let parentLeft = 0;
	if (parentEl) {
		parentLeft = parentEl.getBoundingClientRect().left;
	}

	return targetEl.getBoundingClientRect().left - parentLeft;
}
