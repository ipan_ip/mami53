const getElementY = (element, scrollWindow) => {
	return (
		(scrollWindow ? scrollWindow.scrollTop : window.pageYOffset) +
		element.getBoundingClientRect().top
	);
};
const easing = t => {
	return t < 0.5 ? 4 * t * t * t : (t - 1) * (2 * t - 2) * (2 * t - 2) + 1;
};

export const windowScroll = (targetElement, duration) => {
	const startingY = window.pageYOffset;
	const elementY = getElementY(targetElement);
	const targetY =
		document.body.scrollHeight - elementY < window.innerHeight
			? document.body.scrollHeight - window.innerHeight
			: elementY;
	const diff = targetY - startingY - 100;
	let start;

	if (!diff) return;
	window.requestAnimationFrame(function step(timestamp) {
		if (!start) start = timestamp;

		const time = timestamp - start;
		let percent = Math.min(time / duration, 1);
		percent = easing(percent);

		window.scrollTo(0, startingY + diff * percent);

		if (time < duration) {
			window.requestAnimationFrame(step);
		}
	});
};
