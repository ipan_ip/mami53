const DEFAULT_THRESHOLD = [...Array(101).keys()].map(x => x / 100);

class SpyingElement {
	constructor(element, options = {}) {
		this.__options = {
			threshold: DEFAULT_THRESHOLD,
			...options
		};
		this.element = element;
	}

	/**
	 * @private
	 * @param {IntersectionObserverEntry} entries
	 */
	__handlingObserver(entries) {
		entries.forEach(entry => {
			const ratio = parseFloat(entry.intersectionRatio.toFixed(2));
			const percentage = Math.floor(ratio * 100);
			const position = entry.boundingClientRect.y > 0 ? 'bottom' : 'top';
			this.$callback({ position, percentage }, entry);
		});
	}

	/**
	 * @public
	 * @param {Object} options
	 */
	setOptions(options) {
		this.__options = { ...this.__options, ...options };
	}

	/**
	 * @public
	 * @param {Function} callback
	 */
	spy(callback) {
		const $this = { $callback: callback };
		const observer = new IntersectionObserver(
			this.__handlingObserver.bind($this),
			this.__options
		);

		observer.observe(this.element);
	}
}

export default SpyingElement;
