import noteOptions from './noteOptions.json';

export default function searchNote(key, defaultValue = '-') {
	const noteMatch = noteOptions.find(option => option.value === key);
	return noteMatch ? noteMatch.label : defaultValue;
}
