export function generateBackgroundStyle(rowData) {
	if (rowData && rowData.constructor === Object) {
		if (rowData.gradient && rowData.gradient.from && rowData.gradient.to) {
			const { from, to } = rowData.gradient;
			return `
        background-color: ${from};
        background-image: -moz-linear-gradient(90deg, ${from} 0%, ${to} 100%);
        background-image: -webkit-linear-gradient(90deg, ${from} 0%, ${to} 100%);
        background-image: linear-gradient(90deg, ${from} 0%, ${to} 100%);
        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="${from}",endColorstr="${to}",GradientType=1)
      `;
		}
	} else if (typeof rowData === 'string') {
		return `background-color: ${rowData}`;
	}

	return 'background-color: transparent';
}

export function createStyle(prop, value, defaultReturn = '') {
	if (value) {
		return `${prop}: ${value};`;
	}

	return defaultReturn;
}
