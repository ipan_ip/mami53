import CtCard from './CtCard.vue';
import CtCardHeader from './CtCardHeader.vue';
import CtCardBody from './CtCardBody.vue';
import CtCardFooter from './CtCardFooter.vue';

export default CtCard;

export { CtCard, CtCardHeader, CtCardBody, CtCardFooter };
