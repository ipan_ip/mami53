const defaultOption = {
	maxFilesize: 5, // MB
	acceptedFiles: [],
	paramName: 'file',
	url: ''
};

export default class Uploader {
	constructor(options = {}) {
		this.Axios = axios;
		this.options = {
			...defaultOption,
			...options
		};

		this._requestCancelSource = this.Axios.CancelToken.source();
		this.__onProgress = this.__onError = this.__onSuccess = /* istanbul ignore next */ function() {};
	}

	/**
	 * validating file and uploading file
	 *
	 * @public
	 *
	 * @param {file} file - the file that will be processed
	 */
	async process(file) {
		const isValid = await this.__validateFile(file);
		if (isValid) {
			this.__uploading(file);
		}
	}

	/**
	 * @public
	 *
	 * @param {String} eventType - type of event handler
	 * @param {Function} handler
	 */
	on(eventType, handler) {
		switch (eventType) {
			case 'progress':
				this.__onProgress = handler;
				break;

			case 'error':
				this.__onError = handler;
				break;

			case 'success':
				this.__onSuccess = handler;
				break;
		}
	}

	/**
	 * cancel request of upload
	 * @public
	 */
	cancel() {
		this._requestCancelSource.cancel();
		this._requestCancelSource = this.Axios.CancelToken.source();
	}

	/**
	 * file validator
	 * @private
	 *
	 * @param {File} file
	 * @returns {Promise<Boolean>}
	 */
	__validateFile(file) {
		const size = file.size;
		let isValid = false;

		if (this.__isFileTooBig(size)) {
			this.__onError('File terlalu besar', null, file);
		} else if (this.__isFileNotAllowed(file)) {
			this.__onError('file ini tidak diizinkan', null, file);
		} else {
			isValid = true;
		}

		return Promise.resolve(isValid);
	}

	/**
	 * size validator
	 *
	 * @private
	 *
	 * @param {Number} size - size of file
	 * @returns {Boolean}
	 */
	__isFileTooBig(size) {
		const maxSizeValidation = this.options.maxFilesize * 1024 * 1024;
		return size > maxSizeValidation;
	}

	/**
	 * extention validator
	 *
	 * @private
	 *
	 * @param {File}
	 * @returns {Boolean}
	 */
	__isFileNotAllowed(file) {
		const name = file.name;
		let ext = null;
		const lastIndexOfDot = name.lastIndexOf('.');

		if (lastIndexOfDot !== -1) {
			ext = name.substring(lastIndexOfDot + 1);
		}

		if (ext) {
			if (this.options.acceptedFiles.length) {
				return !this.options.acceptedFiles.includes(ext);
			} else {
				return false;
			}
		} else {
			return true;
		}
	}

	async __uploading(file) {
		const data = new FormData();
		data.append(
			this.options.paramName,
			new Blob([file], { type: file.type }),
			file.name
		);
		let response = null;

		try {
			const result = await this.Axios.post(this.options.url, data, {
				cancelToken: this._requestCancelSource.token,
				onUploadProgress: event => {
					const percentage = Math.round((event.loaded * 100) / event.total);
					this.__onProgress(percentage, file);
				}
			});

			if (result.data.status) {
				response = result.data;
			} else {
				this.__onError(result.data.message, null, file);
				return;
			}
		} catch (error) {
			this.__onError('Terjadi galat silahkan coba lagi', error, file);
			return;
		}

		this.__onSuccess(response, file);
	}
}
