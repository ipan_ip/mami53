const data = [
	{
		id: 'dashboard',
		title: 'Dashboard',
		icon: 'ic_dashboard-consultant_green.svg',
		hasNotify: false,
		pathName: 'dashboard',
		action: null
	},
	{
		id: 'property',
		title: 'Kelola Properti',
		icon: 'ic_home-consultant_green.svg',
		hasNotify: false,
		pathName: 'property',
		action: null
	},
	{
		id: 'data-leads',
		title: 'Data Leads',
		icon: 'ic_data-leads-consultant.svg',
		hasNotify: false,
		action: null,
		expanded: false,
		child: [
			{
				id: 'potential-owner',
				title: 'Pemilik Potensial',
				hasNotify: false,
				pathName: 'potential-owner'
			},
			{
				id: 'potential-property',
				title: 'Properti Potensial',
				hasNotify: false,
				pathName: 'potential-property'
			},
			{
				id: 'tenant-potential',
				title: 'Penyewa Potensial',
				hasNotify: false,
				pathName: 'potential-tenant',
				disabled: false
			}
		]
	},
	{
		id: 'booking',
		title: 'Kelola Booking',
		icon: 'ic_list-consultant_green.svg',
		hasNotify: false,
		pathName: 'booking-management',
		action: null,
		key: 'booking'
	},
	{
		id: 'tenant',
		title: 'Kelola Tenant',
		icon: 'ic_user-consultant_green.svg',
		hasNotify: false,
		pathName: 'active-tenant',
		action: null,
		expanded: false
	},
	{
		id: 'contract',
		title: 'Kelola Kontrak',
		icon: 'ic_file-list_green.svg',
		hasNotify: false,
		pathName: 'contract',
		action: null,
		key: 'contract'
	},
	{
		id: 'activity',
		title: 'Kelola Tugas',
		icon: 'ic_todo_list_green.svg',
		hasNotify: false,
		pathName: 'activity',
		action: null
	},
	{
		id: 'sales-motion',
		title: 'Sales Motion',
		icon: 'ic_promote_green.svg',
		hasNotify: false,
		pathName: 'sales-motion',
		action: null,
		key: 'sales_motion'
	},
	{
		id: 'logout',
		title: 'Logout',
		icon: 'ic_logout-consultant_red.svg',
		iconColor: 'red',
		hasNotify: false,
		pathName: null,
		// eslint-disable-next-line standard/no-callback-literal
		action: cb => cb('logout')
	}
];

export default data;
