import Vuex from 'vuex';
import createContract from './modules/createContract';
import tenant from './modules/tenant';
import invoice from './modules/invoice';
import contract from './modules/contract';
import route from './modules/route';
import booking from './modules/booking';

Vue.use(Vuex);

const store = new Vuex.Store({
	modules: {
		createContract,
		tenant,
		invoice,
		contract,
		route,
		booking
	},
	state: {
		userRoles: [],
		userName: '',
		authData: {},
		uniqueId: {}
	},
	getters: {
		isAdminConsultant(state) {
			return !!state.authData.consultant;
		},
		isUniqueIdMatch(state) {
			return id => state.uniqueId === id;
		},
		getConsultantFirstName(state) {
			return state.userName.split(' ')[0];
		},
		getConsultantRoles(state) {
			const rolesString = state.userRoles.map(function(k) {
				return k.role;
			});
			return rolesString.join(', ');
		}
	},
	mutations: {
		setAuthData(state, payload) {
			if (payload) {
				const { consultant = {} } = payload;
				state.authData = payload;
				state.userName = (consultant && consultant.name) || '';
				state.userRoles = (consultant && consultant.roles) || [];
			}
		},
		setUniqueId(state, payload) {
			state.uniqueId = payload;
		},
		resetState(state) {
			state.authData = {};
			state.userName = '';
			state.userRoles = [];
		}
	}
});

export default store;
