const invoiceModule = {
	namespaced: true,
	state: {
		additionalCosts: [],
		amount: '',
		uniqueId: ''
	},
	getters: {
		hasUniqueIdMatch(state) {
			return id => state.uniqueId === id;
		}
	},
	mutations: {
		setAdditionalCost(state, payload) {
			state.additionalCosts = payload || [];
		},
		setAmount(state, payload) {
			state.amount = payload;
		},
		setUniqueId(state, payload) {
			state.uniqueId = payload;
		},
		resetInvoiceState(state) {
			state.additionalCosts = [];
			state.amount = '';
			state.uniqueId = '';
		}
	}
};

export default invoiceModule;
