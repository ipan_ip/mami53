import { find as _find } from 'lodash';

const booking = {
	namespaced: true,
	state: {
		status: []
	},
	getters: {
		getStatus(state) {
			return statusKey => {
				const selectedStatus = _find(state.status, { key: statusKey });
				return selectedStatus || {};
			};
		}
	},
	mutations: {
		setStatus(state, payload) {
			state.status = [...payload];
		}
	}
};

export default booking;
