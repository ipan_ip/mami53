const tenantModule = {
	namespaced: true,
	state: {
		prevQuery: {},
		selectedDateFrom: {
			label: null,
			value: ''
		},
		selectedDateTo: {
			label: null,
			value: ''
		}
	},
	mutations: {
		setPrevQuery(state, payload) {
			state.prevQuery = payload;
		},
		setSelectedDateFrom(state, payload) {
			state.selectedDateFrom.label = payload.label;
			state.selectedDateFrom.value = payload.value;
		},
		setSelectedDateTo(state, payload) {
			state.selectedDateTo.label = payload.label;
			state.selectedDateTo.value = payload.value;
		}
	}
};

export default tenantModule;
