const tenantModule = {
	namespaced: true,
	state: {
		editableNotes: {}
	},
	mutations: {
		setNoteForEdit(state, payload) {
			state.editableNotes = payload;
		},
		resetNote(state) {
			state.editableNotes = {};
		}
	}
};

export default tenantModule;
