const salesMotion = {
	namespaced: true,
	state: {
		tempReport: {},
		tempImage: {},
		tempFiles: [],
		associatedData: {}
	},
	mutations: {
		setReport(state, payload) {
			state.tempReport = payload;
		},
		setImage(state, payload) {
			state.tempImage = payload;
		},
		setFiles(state, payload) {
			state.tempFiles = payload;
		},
		setAssociatedData(state, payload) {
			state.associatedData = payload;
		},
		resetAllState(state) {
			state.tempReport = {};
			state.tempImage = {};
			state.tempFiles = [];
			state.associatedData = {};
		}
	}
};

export default salesMotion;
