const route = {
	namespaced: true,
	state: {
		previousQuery: {}
	},
	getters: {
		getPrevQuery(state) {
			return key => state.previousQuery[key] || {};
		}
	},
	mutations: {
		setPrevQuery(state, payload) {
			if (payload.type && payload.query) {
				state.previousQuery[payload.type] = payload.query;
			} else {
				throw new Error('Payload must contain type and query');
			}
		},
		clearPrevQuery(state, payload) {
			delete state.previousQuery[payload];
		}
	}
};

export default route;
