const componentSectionOrder = [
	'SearchProperty',
	'SearchPhone',
	'TenantInformations',
	'PropertyDetails'
];

const contractModule = {
	namespaced: true,
	state: {
		section: 1,
		property: {},
		tenant: {},
		totalSection: componentSectionOrder.length,
		photos: {
			document: null,
			identity: null
		},
		uniqueId: '',
		tempPropertyDetail: {},
		roomNumber: {},
		keep: false
	},
	getters: {
		activeComponent: ({ section }) => {
			return componentSectionOrder[section - 1] || componentSectionOrder[0];
		},
		hasUniqueId(state) {
			return id => state.uniqueId === id;
		},
		tempPhotos(state) {
			return state.photos;
		}
	},
	mutations: {
		changeSection(state, payload) {
			if (!state.keep) {
				state.section = payload;
			}
		},
		nextSection(state) {
			state.section++;
		},
		prevSection(state) {
			state.section--;
		},
		setPropertySelected(state, payload) {
			state.property = { ...payload };
		},
		setTenantData(state, payload) {
			state.tenant = { ...state.tenant, ...payload };
		},
		setPhotos(state, payload) {
			state.photos = { ...payload };
		},
		setUniqueId(state, payload) {
			state.uniqueId = payload;
		},
		setPropertyDetail(state, payload) {
			state.tempPropertyDetail = { ...state.tempPropertyDetail, ...payload };
		},
		keepDataContract(state, payload = true) {
			state.keep = payload;
		},
		setRoomNumber(state, payload) {
			state.roomNumber = { ...payload };
		},
		resetAllState(state) {
			if (!state.keep) {
				state.property = {};
				state.tenant = {};
				state.photos = {
					document: null,
					identity: null
				};
				state.tempPropertyDetail = {};
				state.roomNumber = {};
				state.uniqueId = '';
			}
		}
	}
};

export default contractModule;
