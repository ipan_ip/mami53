export const input = {
	resetErrors: 'RESET_ERROR_INPUT_FIELD'
};

export const contract = {
	resetPropertyState: 'RESET_SEARCH_PROPERTY_STATE',
	resetPhoneState: 'RESET_PHONE_STATE',
	resetImageUpload: 'RESET_IMAGE_UPLOAD'
};
