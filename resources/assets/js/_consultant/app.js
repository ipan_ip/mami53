import Dayjs from 'vue-dayjs';
import { isEmpty } from 'lodash';

import router from './router';
import store from './store';
import VeeValidate from './config/veeValidate';
import { getCredential } from './helpers/credentials';

import CtIcon from 'Consultant/components/CtIcon';

import 'dayjs/locale/id';
import './config/axios';
import './styles/main.scss';

import 'Js/@mixins/MixinNavigatorIsMobile';
import 'Js/@mixins/MixinSwalLoading';
import 'Js/@mixins/MixinSwalSuccessError';
import App from './components/App.vue';

Vue.component('app', App);
Vue.component('ct-icon', CtIcon);

Vue.use(VeeValidate, { locale: 'id' });
Vue.use(Dayjs, { lang: 'id' });

Vue.prototype.$isEmpty = isEmpty;

// store user logged in to vuex
if (getCredential()) {
	store.commit('setAuthData', getCredential());
}

// eslint-disable-next-line no-unused-vars
const app = new Vue({
	el: '#app',
	router,
	store,
	created() {
		this.$dayjs.locale('id');
	},
	mounted() {
		try {
			document.getElementById('preloading').remove();
		} catch (e) {
			document.getElementById('preloading').style.display = 'none';
		}
	}
});
