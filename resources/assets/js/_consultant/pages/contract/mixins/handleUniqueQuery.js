import { mapGetters } from 'vuex';

export default {
	computed: {
		...mapGetters('invoice', ['hasUniqueIdMatch'])
	},

	watch: {
		$route: {
			immediate: true,
			handler(value) {
				const uniqueId = value.query.unique_id;
				const query = this.$route.query;
				if (!uniqueId || !this.hasUniqueIdMatch(uniqueId)) {
					if (query.from_booking && query.tenant_id && query.contract_status) {
						this.$router.push({
							name: 'invoice',
							params: {
								id: query.tenant_id,
								booking: query.from_booking,
								contract_status: query.contract_status
							}
						});
					} else {
						this.$router.go(-1);
					}
				}
			}
		}
	}
};
