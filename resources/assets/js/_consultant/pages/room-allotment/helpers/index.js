/**
 * this function used on javascript filter
 * to find room by search
 * should have binding object containing "src"
 *
 * @param {Object} room - each room allotment, should contain "name"
 * @returns {Boolean}
 */
export function handleFindRoomBySearch(room) {
	const rgx = new RegExp(this.src, 'i');
	return rgx.test(room.name);
}
