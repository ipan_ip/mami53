import { statusInId, statusColor } from '../helpers/status';

const mixinBookingLabel = {
	computed: {
		mixStatusText() {
			return statusInId(this.paymentStatus);
		},

		mixStatusColor() {
			return statusColor(this.paymentStatus);
		}
	}
};

export default mixinBookingLabel;
