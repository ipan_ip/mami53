const STATUS_COLOR = {
	success: ['Terbayar', 'Pembayaran Diterima'],
	warning: ['Butuh Konfirmasi', 'Tunggu Pembayaran'],
	disabled: [
		'Sewa Berakhir',
		'Ditolak Pemilik',
		'Dibatalkan',
		'Kadaluwarsa oleh Penyewa',
		'Kadaluwarsa oleh Pemilik',
		'Ditolak Admin'
	]
};

export default {
	computed: {
		mixGetStatusColor() {
			return status => {
				let finalStatusColor = 'disabled';
				for (const [color, statusList] of Object.entries(STATUS_COLOR)) {
					const indexOfMatchStatus = statusList.findIndex(
						_status => _status.toLowerCase() === status.toLowerCase()
					);

					if (indexOfMatchStatus !== -1) {
						finalStatusColor = color;
						break;
					}
				}

				return finalStatusColor;
			};
		}
	}
};
