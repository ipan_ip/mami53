import { durationInId } from 'Consultant/helpers/dictionaries';

export default {
	methods: {
		getBookingDuration(duration, durationUnit) {
			if (!duration || !durationUnit) {
				return '-';
			}

			const addTimesAtUnits = ['quarterly', 'semiannually'];
			let durationStay = duration + ' ';

			if (addTimesAtUnits.includes(durationUnit)) {
				if (+duration > 1) {
					durationStay += 'x ';
				} else {
					durationStay = '';
				}
			}

			return durationStay + durationInId(durationUnit);
		}
	}
};
