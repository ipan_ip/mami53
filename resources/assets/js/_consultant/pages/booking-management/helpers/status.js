const statusDicts = [
	{
		text: 'Butuh Konfirmasi',
		color: 'warning',
		status: ['confirmation', 'booked']
	},
	{
		text: 'Tunggu Pembayaran',
		color: 'warning',
		status: ['wait', 'confirmed']
	},
	{
		text: 'Terbayar',
		color: 'success',
		status: ['paid']
	},
	{
		text: 'Pembayaran Diterima',
		color: 'success',
		status: ['received', 'verified']
	},
	{
		text: 'Sewa Berakhir',
		color: 'disabled',
		status: ['ended', 'terminated']
	},
	{
		text: 'Ditolak Pemilik',
		color: 'disabled',
		status: ['rejected']
	},
	{
		text: 'Dibatalkan',
		color: 'disabled',
		status: ['cancelled']
	},
	{
		text: 'Kadaluarsa oleh Penyewa',
		color: 'disabled',
		status: ['expired', 'expired-tenant']
	},
	{
		text: 'Kadaluarsa oleh Pemilik',
		color: 'disabled',
		status: ['expired_by_owner']
	},
	{
		text: 'Ditolak Admin',
		color: 'disabled',
		status: ['cancel_by_admin', 'rejected-admin']
	}
];

function findStatus(status) {
	let selectedStatus = null;
	for (const item of statusDicts) {
		if (item.status.includes(status)) {
			selectedStatus = item;
			break;
		}
	}

	return selectedStatus;
}

export function statusInId(status) {
	const selectedStatus = findStatus(status);
	if (selectedStatus) {
		return selectedStatus.text;
	} else {
		return 'Unknown Status';
	}
}

export function statusColor(status) {
	const selectedStatus = findStatus(status);
	if (selectedStatus) {
		return selectedStatus.color;
	} else {
		return 'disabled';
	}
}
