const BBK_STATUS = {
  bbk: {
    text: 'BBK',
    color: 'success'
  },
  waiting: {
    text: 'Waiting',
    color: 'info'
  },
  'non-bbk': {
    text: 'Non-BBK',
    color: 'default'
  },
  default: {
    text: 'Unknown',
    color: 'default'
  }
};

const FOLLOWUP_STATUS = {
  done: {
    text: 'Done',
    color: 'success'
  },
  ongoing: {
    text: 'On-going',
    color: 'info'
  },
  new: {
    text: 'New',
    color: 'default'
  },
  default: {
    text: 'Unknown',
    color: 'default'
  }
};

export default {
  methods: {
    getDetailBbkStatus(status) {
      return BBK_STATUS[status] || BBK_STATUS['default'];
    },

    getFollowupStatus(status) {
      return FOLLOWUP_STATUS[status] || FOLLOWUP_STATUS['default'];
    }
  }
}