const labelInID = {
	week: 'Minggu',
	month: 'Bulan',
	year: 'Tahun'
};

function createOption(valueRange, label) {
	if (Array.isArray(valueRange)) {
		return valueRange.map(range => ({
			label: `${range} ${labelInID[label]}`,
			value: String(range)
		}));
	} else {
		return new Array(valueRange).fill(0).map((_, index) => ({
			label: `${index + 1} ${labelInID[label]}`,
			value: String(index + 1)
		}));
	}
}

export const rentDurations = {
	monthly: createOption(12, 'month'),
	weekly: createOption(12, 'week'),
	rent_3_month: createOption([3, 6, 9], 'month'),
	rent_6_month: createOption([6, 12], 'month'),
	yearly: createOption(3, 'year')
};

export const rentCountOptions = [
	{
		label: 'Perbulan',
		value: 'monthly'
	},
	{
		label: 'Perminggu',
		value: 'weekly'
	},
	{
		label: 'Per 3 bulan',
		value: 'rent_3_month'
	},
	{
		label: 'Per 6 bulan',
		value: 'rent_6_month'
	},
	{
		label: 'Pertahun',
		value: 'yearly'
	}
];
