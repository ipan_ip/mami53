/* eslint-disable standard/no-callback-literal */
const fields = [
	['room_id'],
	[],
	[
		'name',
		'phone_number',
		'gender',
		'email',
		'occupation',
		'marital_status',
		'parent_name',
		'parent_phone_number',
		'photo_identifier_id',
		'photo_document_id'
	],
	[
		'room_number',
		'start_date',
		'rent_type',
		'amount',
		'duration',
		'note',
		'fine_amount',
		'fine_maximum_length',
		'fine_duration_type',
		'additional_costs',
		'field_title',
		'field_value',
		'cost_type'
	]
];

export default function(error, callback) {
	if (error) {
		const errorData = {};

		for (const [field, message] of Object.entries(error)) {
			const indexField = fields.findIndex(function(eachField) {
				return eachField.indexOf(field) !== -1;
			});

			if (indexField !== -1) {
				if (
					Object.prototype.hasOwnProperty.call(errorData, String(indexField))
				) {
					errorData[String(indexField)].push(message[0]);
				} else {
					errorData[String(indexField)] = [message[0]];
				}
			}
		}

		const sections = Object.keys(errorData);
		if (sections.length) {
			const smallestSection = Math.min(...sections);
			callback(smallestSection + 1, errorData[String(smallestSection)]);
		} else {
			callback(4, ['Terjadi galat. Silakan coba lagi.']);
		}
	} else {
		callback(4, ['Terjadi galat. Silakan coba lagi.']);
	}
}
