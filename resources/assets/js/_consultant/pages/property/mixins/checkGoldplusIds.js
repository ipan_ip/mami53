export default {
  methods: {
    checkGoldplusIds(goldplusIds) {
      return goldplusIds
              .map(id => +id)
						  .filter(id => !isNaN(id));
    }
  }
}