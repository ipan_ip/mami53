/* eslint-disable camelcase */
const mixin = {
	data() {
		return {
			activityType: {
				dbet: {
					icon: 'icon-user-circle-outline',
					label: 'DBET'
				},
				properti: {
					icon: 'icon-home-circle',
					label: 'Properti'
				},
				kontrak: {
					icon: 'icon-file',
					label: 'Kontrak'
				},
				owner: {
					icon: 'icon-user-circle-outline',
					label: 'Owner Potensial'
				}
			}
		};
	},

	methods: {
		/**
		 * @public
		 *
		 * @param {Array} activities - activity data
		 * @returns {Array}
		 */
		mixUpdateDataActivity(activities) {
			return activities.map(activity => {
				const icon = this.__getActivityIcon(activity.data_type);
				const label = this.__getActivityLabel(activity);

				return {
					...activity,
					icon,
					label
				};
			});
		},

		/**
		 * @private
		 * get icon of activity base on its type
		 *
		 * @param {String} dataType - type of activity
		 * @returns {String}
		 */
		__getActivityIcon(dataType) {
			if (dataType === 'Properti Potensial') {
				dataType = 'properti';
			} else if (/owner/i.test(dataType)) {
				dataType = 'owner';
			}

			const type = this.activityType[dataType.toLowerCase()];

			return type ? type.icon : '';
		},

		/**
		 * @private
		 * get label of activity
		 *
		 * @param {Object} - activity object
		 * @returns {String}
		 */
		__getActivityLabel({ data_type, current_stage, stage_name }) {
			let label = '';
			if (data_type === 'Properti Potensial') {
				label = 'Properti Potensial';
			} else if (/owner/i.test(data_type)) {
				label = 'Owner Potensial';
			} else {
				const type = this.activityType[data_type.toLowerCase()];
				label = type ? type.label : '';
			}
			return `Data ${label} • Tahap ${current_stage + 1} ${stage_name}`;
		}
	}
};

export default mixin;
