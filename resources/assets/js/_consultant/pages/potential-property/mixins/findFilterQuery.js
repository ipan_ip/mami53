export default {
	data() {
		return {
			possibleCreatedBy: ['consultant', 'owner'],
			possiblePriority: ['high', 'low']
		};
	},

	methods: {
		/**
		 *
		 * @param {Array} filters - the value of filter query
		 * @param {String<creator|priority>} filterType - the type of filter query
		 *
		 * @returns {Array} - the possible value that match with query type
		 */
		mixFindFilterQuery(filters, filterType) {
			let possibleValue = [];

			switch (filterType) {
				case 'creator':
					possibleValue = this.possibleCreatedBy;
					break;

				case 'priority':
					possibleValue = this.possiblePriority;
					break;
			}

			return possibleValue.filter(value => filters.includes(value));
		}
	}
};
