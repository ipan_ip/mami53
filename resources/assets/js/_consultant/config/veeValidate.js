import id from 'vee-validate/dist/locale/id';
import VeeValidate, { Validator } from 'vee-validate';
window.validator = new Validator();
Validator.localize('id', id);

Validator.extend('phone', {
	getMessage(field) {
		return field + ' Mohon diawali dengan 08 atau +62';
	},
	validate(value) {
		return /^((08|\+62)\d+)$/.test(value);
	}
});

Validator.extend('timeFormat', {
	getMessage(field) {
		return 'Format ' + field + ' harus HH:mm. contoh 21:30';
	},
	validate(value) {
		return /^(1[0-9]|2[0-3]):([0-5][0-9])$/.test(value);
	}
});

Validator.extend('unique_value', {
	getMessage(field) {
		return `${field} sudah dipakai silahkan cari ${field} yang lain`;
	},
	validate(value, args) {
		return args.indexOf(value) === -1;
	}
});

export default VeeValidate;
