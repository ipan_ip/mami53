import {
	getAccessToken,
	getRefreshToken,
	setToken,
	clearToken
} from 'Consultant/helpers/cookiesToken';
import { removeCredential } from 'Consultant/helpers/credentials';
import Store from 'Consultant/store';
import Router from 'Consultant/router';
import secretParams from './secretParams';

import EventBus from 'Consultant/event-bus';

function errorPromised(error) {
	return new Promise((resolve, reject) => reject(error));
}

export function responseHandler(config) {
	return config;
}

export function responseErrorHandler(error) {
	const originalRequest = { ...error.config };

	if (error.response) {
		if (error.response.status === 401) {
			const refreshToken = getRefreshToken();

			// when access_token can't be used anymore (expired), refresh that token
			return this.$oldAxios
				.post('/oauth/token', {
					...secretParams,
					refresh_token: refreshToken
				})
				.then(
					// when refresh token has succeed
					res => {
						if (res.status === 200) {
							setToken(res.data);
							originalRequest.headers.Authorization = `Bearer ${getAccessToken()}`;
							originalRequest.baseURL = '';
							return this.$axios(originalRequest);
						}
					},
					// when refresh token has failed
					() => {
						/*
						 * remove access_token and refresh_token
						 * remove authData in the local storage and vuex
						 * redirect to login page
						 */
						clearToken();
						removeCredential();
						Store.commit('resetState');
						this.$axios.interceptors.response.eject(window.responseInterceptor);
						EventBus.$emit(
							'openAlert',
							'Sesi anda telah habis, silahkan login kembali'
						);
						Router.push({ name: 'login' });
						bugsnagClient.notify(new Error('Refresh token failed'));
						return errorPromised(error);
					}
				);
		} else {
			bugsnagClient.notify(error);
		}
	}

	return errorPromised(error);
}
