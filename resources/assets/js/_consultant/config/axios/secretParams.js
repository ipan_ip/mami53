export default {
	grant_type: 'refresh_token',
	client_id: process.env.MIX_OAUTH_CLIENT_ID,
	client_secret: process.env.MIX_OAUTH_CLIENT_SECRET
};
