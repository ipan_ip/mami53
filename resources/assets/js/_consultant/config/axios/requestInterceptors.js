import { getAccessToken } from 'Consultant/helpers/cookiesToken';

export function requestHandler(config) {
	const token = getAccessToken();
	const headers = {
		Accept: 'application/json',
		'Content-Type': 'application/json',
		'X-GIT-Time': '1406090202'
	};

	if (token) {
		headers.Authorization = 'Bearer ' + token;
	}

	config.headers = headers;
	return config;
}

export function requestErrorHandler(error) {
	return new Promise((resolve, reject) => reject(error));
}
