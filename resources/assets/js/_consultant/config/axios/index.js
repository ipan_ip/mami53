import axios from 'axios';
import { requestHandler, requestErrorHandler } from './requestInterceptors';
import { responseHandler, responseErrorHandler } from './responseInterceptors';

/**
 * checking axios whether was installed on window object
 * @returns { Boolean }
 */
function doesOldAxiosExist() {
	return typeof window.axios !== 'undefined';
}

let oldAxios = null;
if (doesOldAxiosExist()) {
	oldAxios = window.axios;
	oldAxios.defaults.baseURL = '/';
	delete window.axios;
}

const axiosInstance = axios;
axiosInstance.defaults.baseURL = '/consultant-tools';

axiosInstance.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

axiosInstance.interceptors.request.use(requestHandler, requestErrorHandler);
window.responseInterceptor = axiosInstance.interceptors.response.use(
	responseHandler,
	responseErrorHandler.bind({
		$axios: axiosInstance,
		$oldAxios: oldAxios
	})
);

window.axios = axiosInstance;
