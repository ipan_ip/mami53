require('../starter');

window.debounce = require('lodash/debounce');

window.Vue = require('vue').default;

import Vuex from 'vuex';
Vue.use(Vuex);

import id from 'vee-validate/dist/locale/id';
import VeeValidate, { Validator } from 'vee-validate';

window.validator = new Validator();
Validator.localize('id', id);
Vue.use(VeeValidate, {
	locale: 'id'
});

import mixinAuth from 'Js/@mixins/MixinAuth';

require('Js/@mixins/MixinCenterModal');

const ContainerAgentCheck = require('./components/agent-check/ContainerAgentCheck.vue')
	.default;

// Container Component
Vue.component('container-agent-check', ContainerAgentCheck);

const store = new Vuex.Store({
	state: {
		authCheck: {},
		authData: {}
	},
	mutations: {
		setAuthCheck(state, payload) {
			state.authCheck = payload;
		},
		setAuthData(state, payload) {
			state.authData = payload;
		}
	}
});

Vue.config.productionTip = false;

const app = new Vue({
	el: '#app',
	mixins: [mixinAuth],
	store
});
