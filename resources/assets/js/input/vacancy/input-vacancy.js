require('../../starter');

window.debounce = require('lodash/debounce');

window.Vue = require('vue').default;

import VueLazyload from 'vue-lazyload';
Vue.use(VueLazyload);

import Vuex from 'vuex';
Vue.use(Vuex);

import id from 'vee-validate/dist/locale/id';
import VeeValidate, { Validator } from 'vee-validate';

window.validator = new Validator();
Validator.localize('id', id);
Vue.use(VeeValidate, {
	locale: 'id'
});

require('Js/@mixins/MixinSwalLoading');

const ContainerInputVacancy = require('../components/input-vacancy/ContainerInputVacancy.vue')
	.default;

//  Component
Vue.component('container-input-vacancy', ContainerInputVacancy);

// Global Components
Vue.component('mami-loading', () => import('Js/@components/MamiLoading.vue'));
Vue.component('breadcrumb-pills', () =>
	import('Js/@components/BreadcrumbPills.vue')
);

// Local Components
Vue.component(
	'section-title',
	require('../components/SectionTitle.vue').default
);
Vue.component(
	'section-alert',
	require('../components/SectionAlert.vue').default
);

Vue.component(
	'vacancy-location',
	require('../components/input-vacancy/VacancyLocation.vue').default
);
Vue.component(
	'field-place-name',
	require('../components/input-vacancy/FieldPlaceName.vue').default
);
Vue.component('field-map', require('../components/FieldMapOsm.vue').default);
Vue.component(
	'field-workplace',
	require('../components/input-vacancy/FieldWorkplace.vue').default
);
Vue.component(
	'field-placement',
	require('../components/input-vacancy/FieldPlacement.vue').default
);
Vue.component(
	'field-company-logo',
	require('../components/input-vacancy/FieldCompanyLogo.vue').default
);
Vue.component(
	'modal-warning-logo',
	require('../components/input-vacancy/ModalWarningLogo.vue').default
);

Vue.component(
	'data-vacancy',
	require('../components/input-vacancy/DataVacancy.vue').default
);
Vue.component(
	'field-jobs-name',
	require('../components/input-vacancy/‪FieldJobsName.vue').default
);
Vue.component(
	'field-jobs-type',
	require('../components/input-vacancy/FieldJobsType.vue').default
);
Vue.component(
	'field-last-education',
	require('../components/input-vacancy/FieldLastEducation.vue').default
);
Vue.component(
	'field-description',
	require('../components/input-vacancy/FieldDescription.vue').default
);
Vue.component(
	'field-salary',
	require('../components/input-vacancy/FieldSalary.vue').default
);
Vue.component(
	'field-salary-max',
	require('../components/input-vacancy/FieldSalaryMax.vue').default
);
Vue.component(
	'field-salary-time',
	require('../components/input-vacancy/FieldSalaryTime.vue').default
);
Vue.component(
	'field-show-salary',
	require('../components/input-vacancy/FieldShowSalary.vue').default
);
Vue.component(
	'field-specialization',
	require('../components/input-vacancy/FieldSpecialization.vue').default
);

Vue.component(
	'data-owner',
	require('../components/input-vacancy/DataOwner.vue').default
);
Vue.component(
	'field-owner-name',
	require('../components/input-vacancy/FieldOwnerName.vue').default
);
Vue.component(
	'field-phone-number',
	require('../components/input-vacancy/FieldPhoneNumber.vue').default
);
Vue.component(
	'field-email',
	require('../components/input-vacancy/FieldEmail.vue').default
);
Vue.component(
	'field-expired',
	require('../components/input-vacancy/FieldExpired.vue').default
);

Vue.component(
	'modal-input-success',
	require('../components/ModalInputSuccess.vue').default
);
Vue.component(
	'modal-input-failed',
	require('../components/ModalInputFailed.vue').default
);

const store = new Vuex.Store({
	state: {
		token: document.head.querySelector('meta[name="csrf-token"]').content,
		seq: parseInt(document.head.querySelector('meta[name="seq"]').content),
		authCheck: authCheck,
		authData: authData,
		section: 1,
		params: {
			_token: document.head.querySelector('meta[name="csrf-token"]').content,
			place_name: '',
			latitude: -7.7510466,
			longitude: 110.4057421,
			city: '',
			subdistrict: '',
			address: '',
			workplace: '',
			workplace_status: 0,
			jobs_name: '',
			jobs_type: 'full-time',
			jobs_description: '',
			jobs_salary: '',
			jobs_max_salary: '',
			show_salary: 1,
			jobs_salary_time: 'bulanan',
			insert_username: '',
			insert_phone: '',
			insert_email: '',
			last_education: 'all',
			spesialisasi: '',
			photo_id: null,
			expired_date: '',
			with_expired_date: 1
		},
		extras: {
			map_address: '',
			is_edit: false,
			tags: {
				jobs_type: [],
				jobs_salary_time: [],
				education_options: [],
				spesialisasi_options: []
			},
			photo_small: ''
		}
	},
	mutations: {
		changeSection(state, payload) {
			state.section = payload;
			window.scroll(0, 0);
		},
		setParamsPlaceName(state, payload) {
			state.params.place_name = payload;
		},
		setParamsWorkplace(state, payload) {
			state.params.workplace = payload;
		},
		setParamsWorkplaceStatus(state, payload) {
			state.params.workplace_status = payload;
		},
		setParamsJobsName(state, payload) {
			state.params.jobs_name = payload;
		},
		setParamsJobsType(state, payload) {
			state.params.jobs_type = payload;
		},
		setParamsJobsDescription(state, payload) {
			state.params.jobs_description = payload;
		},
		setParamsJobsSalary(state, payload) {
			state.params.jobs_salary = payload;
		},
		setParamsJobsSalaryMax(state, payload) {
			state.params.jobs_max_salary = payload;
		},
		setParamsShowSalary(state, payload) {
			state.params.show_salary = payload;
		},
		setParamsJobsSalaryTime(state, payload) {
			state.params.jobs_salary_time = payload;
		},
		setParamsInsertUsername(state, payload) {
			state.params.insert_username = payload;
		},
		setParamsInsertPhone(state, payload) {
			state.params.insert_phone = payload;
		},
		setParamsInsertEmail(state, payload) {
			state.params.insert_email = payload;
		},
		setParamsLastEducation(state, payload) {
			state.params.last_education = payload;
		},
		setParamsSpesialisasi(state, payload) {
			state.params.spesialisasi = payload;
		},
		setParamsPhotoId(state, payload) {
			state.params.photo_id = payload;
		},
		setParamsExpiredDate(state, payload) {
			state.params.expired_date = payload;
		},
		setParamsLocationMap(state, payload) {
			state.params.latitude = payload.latitude;
			state.params.longitude = payload.longitude;
			state.params.city = payload.city;
			state.params.subdistrict = payload.subdistrict;
			state.extras.map_address = payload.map_address;
			state.params.address = payload.map_address;
		},
		setEditStatus(state, payload) {
			state.extras.is_edit = payload;
		},
		setExtrasEducationOptions(state, payload) {
			state.extras.tags.education_options = payload;
		},
		setExtrasSpesialisasiOptions(state, payload) {
			state.extras.tags.spesialisasi_options = payload;
		},
		setExtrasPhotoSmall(state, payload) {
			state.extras.photo_small = payload;
		}
	}
});

Vue.config.productionTip = false;

const app = new Vue({
	el: '#app',
	store,
	created() {
		this.$store.commit('setExtrasEducationOptions', educationOptions);
		this.$store.commit('setExtrasSpesialisasiOptions', spesialisasiOptions);
	}
});
