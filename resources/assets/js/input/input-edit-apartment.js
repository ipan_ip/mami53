require('../starter');

window.debounce = require('lodash/debounce');

window.Vue = require('vue').default;

import VueLazyload from 'vue-lazyload';
Vue.use(VueLazyload);

import Vuex from 'vuex';
Vue.use(Vuex);

import id from 'vee-validate/dist/locale/id';
import VeeValidate, { Validator } from 'vee-validate';
window.validator = new Validator();
Validator.localize('id', id);
Vue.use(VeeValidate, {
	locale: 'id'
});

import mixinAuth from 'Js/@mixins/MixinAuth';

require('Js/@mixins/MixinCenterModal');

const App = require('./components/input-edit-apartment/App.vue').default;

// Container Component
Vue.component('app', App);

// Global Components
Vue.component('mami-loading', () => import('Js/@components/MamiLoading.vue'));
Vue.component('mami-loading-inline', () =>
	import('Js/@components/MamiLoadingInline.vue')
);
Vue.component('breadcrumb-pills', () =>
	import('Js/@components/BreadcrumbPills.vue')
);

// Local Components
Vue.component(
	'section-title',
	require('./components/SectionTitle.vue').default
);
Vue.component(
	'section-alert',
	require('./components/SectionAlert.vue').default
);

Vue.component(
	'step-first',
	require('./components/input-properti-apartment/StepFirst.vue').default
);
Vue.component(
	'field-name',
	require('./components/input-properti-apartment/FieldName.vue').default
);
Vue.component(
	'field-unit-name',
	require('./components/input-properti-apartment/FieldUnitName.vue').default
);
Vue.component(
	'field-unit-number',
	require('./components/input-properti-apartment/FieldUnitNumber.vue').default
);
Vue.component(
	'field-type',
	require('./components/input-properti-apartment/FieldType.vue').default
);
Vue.component(
	'field-floor',
	require('./components/input-properti-apartment/FieldFloor.vue').default
);
Vue.component(
	'field-size',
	require('./components/input-properti-apartment/FieldSize.vue').default
);
Vue.component(
	'field-price',
	require('./components/input-properti-apartment/FieldPrice.vue').default
);
Vue.component(
	'field-price-apartment',
	require('./components/input-properti-apartment/FieldPriceApartment.vue')
		.default
);
Vue.component(
	'field-price-extra',
	require('./components/input-properti-apartment/FieldPriceExtra.vue').default
);
Vue.component(
	'field-facility-unit',
	require('./components/input-properti-apartment/FieldFacilityUnit.vue').default
);
Vue.component(
	'field-facility-room',
	require('./components/input-properti-apartment/FieldFacilityRoom.vue').default
);
Vue.component(
	'field-description',
	require('./components/input-properti-apartment/FieldDescriptionApartment.vue')
		.default
);

Vue.component(
	'field-photos',
	require('./components/input-edit/FieldPhotos.vue').default
);
Vue.component(
	'field-photos-upload',
	require('./components/input-edit/FieldPhotosUpload.vue').default
);

Vue.component(
	'modal-input-success',
	require('./components/ModalInputSuccess.vue').default
);
Vue.component(
	'modal-input-failed',
	require('./components/ModalInputFailed.vue').default
);

const store = new Vuex.Store({
	state: {
		token: document.head.querySelector('meta[name="csrf-token"]').content,
		property_id: document.head.querySelector('meta[name="property_index"]')
			.content,
		section: 1,
		authCheck: {},
		authData: {},
		params: {
			_token: document.head.querySelector('meta[name="csrf-token"]').content,
			description: '',
			unit_name: '',
			unit_number: '',
			unit_type: '',
			unit_type_name: '',
			floor: null,
			unit_size: null,
			price_type: 'idr',
			price_monthly: '',
			price_daily: '',
			price_weekly: '',
			price_yearly: '',
			min_payment: null,
			maintenance_price: '',
			parking_price: '',
			fac_unit: [],
			is_furnished: 0,
			fac_room: [],
			photos: {
				cover: [],
				main: [],
				bedroom: [],
				bath: [],
				other: []
			}
		},
		extras: {
			is_edit: true,
			property_type: document.head.querySelector('meta[name="property_type"]')
				.content,
			fac_tags: {
				unit_type: [],
				min_payment: [],
				unit: [],
				furnished: [],
				semi_furnished: [],
				photo_example: ''
			},
			photosBefore: {
				cover: [],
				main: [],
				bedroom: [],
				bath: [],
				other: []
			},
			photosAdded: {
				cover: false,
				main: false,
				bedroom: false,
				bath: false,
				other: false
			},
			photosUrl: {
				cover: [],
				main: [],
				bedroom: [],
				bath: [],
				other: []
			},
			price_daily_checked: false,
			price_weekly_checked: false,
			price_monthly_checked: false,
			price_yearly_checked: false,
			errorMessage: {
				photoOther: false,
				photoCover: false,
				photoMain: false,
				photoBedroom: false,
				photoBath: false
			}
		}
	},
	mutations: {
		setExtrasErrorImage(state, payload) {
			state.extras.errorMessage = payload;
		},
		changeSection(state, payload) {
			state.section = payload;
			window.scroll(0, 0);
		},
		setAuthCheck(state, payload) {
			state.authCheck = payload;
		},
		setAuthData(state, payload) {
			state.authData = payload;
		},
		setExtrasFacTags(state, payload) {
			state.extras.fac_tags = {
				unit_type: payload.unitType,
				min_payment: payload.minPayment,
				unit: payload.unit,
				furnished: payload.furnished,
				semi_furnished: payload.semiFurnished,
				photo_example: payload.photoExample
			};
		},
		setParamsDescription(state, payload) {
			state.params.description = payload;
		},
		setParamsUnitName(state, payload) {
			state.params.unit_name = payload;
		},
		setParamsUnitNumber(state, payload) {
			state.params.unit_number = payload;
		},
		setParamsUnitType(state, payload) {
			state.params.unit_type = payload;
		},
		setParamsUnitTypeName(state, payload) {
			state.params.unit_type_name = payload;
		},
		setParamsFloor(state, payload) {
			state.params.floor = payload;
		},
		setParamsUnitSize(state, payload) {
			state.params.unit_size = payload;
		},
		setParamsPriceMonthly(state, payload) {
			state.params.price_monthly = payload;
		},
		setParamsPriceDaily(state, payload) {
			state.params.price_daily = payload;
		},
		setParamsPriceWeekly(state, payload) {
			state.params.price_weekly = payload;
		},
		setParamsPriceYearly(state, payload) {
			state.params.price_yearly = payload;
		},
		setParamsMinPayment(state, payload) {
			state.params.min_payment = payload;
		},
		setParamsMaintenancePrice(state, payload) {
			state.params.maintenance_price = payload;
		},
		setParamsParkingPrice(state, payload) {
			state.params.parking_price = payload;
		},
		setParamsFacUnit(state, payload) {
			state.params.fac_unit = payload;
		},
		setParamsIsFurnished(state, payload) {
			state.params.is_furnished = payload;
		},
		setParamsFacRoom(state, payload) {
			state.params.fac_room = payload;
		},
		setExtrasPhotosBefore(state, payload) {
			state.extras.photosBefore = {
				cover: payload.cover,
				main: payload.bangunan,
				bedroom: payload.kamar,
				bath: payload['kamar-mandi'],
				other: payload.lainnya
			};
		},
		removeExtrasPhotosBeforeMain(state, payload) {
			state.extras.photosBefore.main.length = payload;
		},
		removeExtrasPhotosBeforeCover(state, payload) {
			state.extras.photosBefore.cover.length = payload;
		},
		removeExtrasPhotosBeforeBedroom(state, payload) {
			state.extras.photosBefore.bedroom.length = payload;
		},
		removeExtrasPhotosBeforeBath(state, payload) {
			state.extras.photosBefore.bath.length = payload;
		},
		removeExtrasPhotosBeforeOther(state, payload) {
			state.extras.photosBefore.other.length = payload;
		},
		setParamsPhotos(state, payload) {
			state.params.photos = {
				cover: payload.cover,
				main: payload.main,
				bedroom: payload.bedroom,
				bath: payload.bath,
				other: payload.other
			};
		},
		setExtrasPhotosUrl(state, payload) {
			state.extras.photosUrl = {
				cover: payload.cover,
				main: payload.main,
				bedroom: payload.bedroom,
				bath: payload.bath,
				other: payload.other
			};
		},
		setExtrasPhotosAdded(state, payload) {
			state.extras.photosAdded = {
				cover: payload.cover,
				main: payload.main,
				bedroom: payload.bedroom,
				bath: payload.bath,
				other: payload.other
			};
		},
		setParamsRateType(state, payload) {
			state.params.price_type = payload;
		},
		setExtrasPriceDaily(state, payload) {
			state.extras.price_daily_checked = payload;
		},
		setExtrasPriceWeekly(state, payload) {
			state.extras.price_weekly_checked = payload;
		},
		setExtrasPriceMonthly(state, payload) {
			state.extras.price_monthly_checked = payload;
		},
		setExtrasPriceYearly(state, payload) {
			state.extras.price_yearly_checked = payload;
		}
	}
});

Vue.config.productionTip = false;

const app = new Vue({
	el: '#app',
	mixins: [mixinAuth],
	store
});
