require('../starter');

window.debounce = require('lodash/debounce');

window.Vue = require('vue').default;

import VueLazyload from 'vue-lazyload';
Vue.use(VueLazyload);

import Vuex from 'vuex';
Vue.use(Vuex);

import id from 'vee-validate/dist/locale/id';
import VeeValidate, { Validator } from 'vee-validate';
window.validator = new Validator();
Validator.localize('id', id);
Vue.use(VeeValidate, {
	locale: 'id'
});

import mixinAuth from '../@mixins/MixinAuth';

require('../@mixins/MixinCenterModal');

const ContainerInputKost = require('./components/input-kost/ContainerInputKost.vue')
	.default;

// Container Component
Vue.component('container-input-kost', ContainerInputKost);

// Global Components
Vue.component(
	'mami-loading',
	require('../@components/MamiLoading.vue').default
);
Vue.component(
	'mami-loading-inline',
	require('../@components/MamiLoadingInline.vue').default
);
Vue.component(
	'breadcrumb-pills',
	require('../@components/BreadcrumbPills.vue').default
);

// Local Components
Vue.component(
	'section-title',
	require('./components/SectionTitle.vue').default
);
Vue.component(
	'section-alert',
	require('./components/SectionAlert.vue').default
);

Vue.component(
	'step-first',
	require('./components/input-kost/StepFirst.vue').default
);
Vue.component(
	'field-owner-phone',
	require('./components/FieldOwnerPhone.vue').default
);
Vue.component('field-map', require('./components/FieldMapOsm.vue').default);
Vue.component(
	'field-address',
	require('./components/FieldAddress.vue').default
);

Vue.component(
	'step-second',
	require('./components/input-kost/StepSecond.vue').default
);
Vue.component('field-name', require('./components/FieldName.vue').default);
Vue.component(
	'field-owner-name',
	require('./components/FieldOwnerName.vue').default
);
Vue.component('field-gender', require('./components/FieldGender.vue').default);
Vue.component(
	'field-room-size',
	require('./components/FieldRoomSize.vue').default
);
Vue.component('field-price', require('./components/FieldPrice.vue').default);

Vue.component(
	'step-third',
	require('./components/input-kost/StepThird.vue').default
);
Vue.component(
	'field-facility-property',
	require('./components/FieldFacilityProperty.vue').default
);
Vue.component(
	'field-facility-room',
	require('./components/FieldFacilityRoom.vue').default
);
Vue.component(
	'field-facility-bath',
	require('./components/FieldFacilityBath.vue').default
);
Vue.component('field-photos', require('./components/FieldPhotos.vue').default);
Vue.component(
	'field-photos-upload',
	require('./components/FieldPhotosUpload.vue').default
);

Vue.component(
	'modal-input-success',
	require('./components/ModalInputSuccess.vue').default
);
Vue.component(
	'modal-input-failed',
	require('./components/ModalInputFailed.vue').default
);
Vue.component(
	'modal-input-promo',
	require('./components/ModalInputPromo.vue').default
);

const store = new Vuex.Store({
	state: {
		token: document.head.querySelector('meta[name="csrf-token"]').content,
		section: 1,
		authCheck: {},
		authData: {},
		params: {
			_token: document.head.querySelector('meta[name="csrf-token"]').content,
			input_as: 'Anak Kos',
			latitude: -7.7864525,
			longitude: 110.3659488,
			city: '',
			subdistrict: '',
			address: '',
			name: '',
			gender: 2,
			room_size: '3x3',
			room_count: 1,
			room_available: 1,
			price_monthly: '',
			price_daily: '',
			price_weekly: '',
			price_yearly: '',
			fac_property: [],
			fac_room: [],
			fac_bath: [],
			photos: {
				cover: [],
				bedroom: [],
				bath: []
			},
			owner_name: '',
			owner_phone: ''
		},
		extras: {
			is_edit: false,
			property_type: document.head.querySelector('meta[name="property_type"]')
				.content,
			map_address: '',
			slug: '',
			fac_tags: {
				property: [],
				room: [],
				bath: [],
				photo_example: ''
			},
			fac_room_toggle: 'empty',
			photosUrl: {
				cover: {},
				bedroom: {},
				bath: {}
			}
		}
	},
	mutations: {
		changeSection(state, payload) {
			state.section = payload;
			window.scroll(0, 0);
		},
		setAuthCheck(state, payload) {
			state.authCheck = payload;
		},
		setAuthData(state, payload) {
			state.authData = payload;
		},
		setParamsOwnerPhone(state, payload) {
			state.params.owner_phone = payload;
		},
		setParamsLocationMap(state, payload) {
			state.params.latitude = payload.latitude;
			state.params.longitude = payload.longitude;
			state.params.city = payload.city;
			state.params.subdistrict = payload.subdistrict;
			state.extras.map_address = payload.map_address;
		},
		setParamsAddress(state, payload) {
			state.params.address = payload;
		},
		setParamsName(state, payload) {
			state.params.name = payload;
		},
		setExtrasSlug(state, payload) {
			state.extras.slug = payload;
		},
		setParamsOwnerName(state, payload) {
			state.params.owner_name = payload;
		},
		setParamsGender(state, payload) {
			state.params.gender = payload;
		},
		setParamsRoomSize(state, payload) {
			state.params.room_size = payload;
		},
		setParamsRoomAvailable(state, payload) {
			state.params.room_available = payload;
		},
		setParamsRoomCount(state, payload) {
			state.params.room_count = payload;
		},
		setParamsPriceMonthly(state, payload) {
			state.params.price_monthly = payload;
		},
		setParamsPriceDaily(state, payload) {
			state.params.price_daily = payload;
		},
		setParamsPriceWeekly(state, payload) {
			state.params.price_weekly = payload;
		},
		setParamsPriceYearly(state, payload) {
			state.params.price_yearly = payload;
		},
		setExtrasFacTags(state, payload) {
			state.extras.fac_tags = {
				property: payload.property,
				room: payload.room,
				bath: payload.bath,
				photo_example: payload.photoExample
			};
		},
		setParamsFacProperty(state, payload) {
			state.params.fac_property = payload;
		},
		changeExtrasFacRoomToggle(state, payload) {
			state.extras.fac_room_toggle = payload;
		},
		setParamsFacRoom(state, payload) {
			state.params.fac_room = payload;
		},
		setParamsFacBath(state, payload) {
			state.params.fac_bath.length = 0;
			state.params.fac_bath.push(payload);
		},
		setParamsPhotos(state, payload) {
			state.params.photos = {
				cover: payload.cover,
				bedroom: payload.bedroom,
				bath: payload.bath
			};
		},
		setExtrasPhotosUrl(state, payload) {
			state.extras.photosUrl = {
				cover: payload.cover,
				bedroom: payload.bedroom,
				bath: payload.bath
			};
		}
	}
});

Vue.config.productionTip = false;

const app = new Vue({
	el: '#app',
	mixins: [mixinAuth],
	store
});
