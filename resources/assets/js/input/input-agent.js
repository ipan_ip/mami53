require('../starter');

window.debounce = require('lodash/debounce');

window.Vue = require('vue').default;

import VueLazyload from 'vue-lazyload';
Vue.use(VueLazyload);

import Vuex from 'vuex';
Vue.use(Vuex);

import id from 'vee-validate/dist/locale/id';
import VeeValidate, { Validator } from 'vee-validate';
window.validator = new Validator();
Validator.localize('id', id);
Vue.use(VeeValidate, {
	locale: 'id'
});

import Vue2Filters from 'vue2-filters';
Vue.use(Vue2Filters);

import mixinAuth from 'Js/@mixins/MixinAuth';

require('Js/@mixins/MixinCenterModal');

const ContainerInputKost = require('./components/input-kost/ContainerInputKost.vue')
	.default;

// Container Component
Vue.component('container-input-kost', ContainerInputKost);

// Global Components
Vue.component('mami-loading', () => import('Js/@components/MamiLoading.vue'));
Vue.component('mami-loading-inline', () =>
	import('Js/@components/MamiLoadingInline.vue')
);
Vue.component('breadcrumb-pills', () =>
	import('Js/@components/BreadcrumbPills.vue')
);

// Local Components
Vue.component(
	'widget-form-bookmark',
	require('./components/WidgetFormBookmark.vue').default
);

Vue.component(
	'section-title',
	require('./components/SectionTitle.vue').default
);
Vue.component(
	'section-alert',
	require('./components/SectionAlert.vue').default
);

Vue.component(
	'step-first',
	require('./components/input-kost/StepFirst.vue').default
);
Vue.component(
	'field-agent-name',
	require('./components/FieldAgentName.vue').default
);
Vue.component(
	'field-agent-email',
	require('./components/FieldAgentEmail.vue').default
);
Vue.component(
	'field-agent-phone',
	require('./components/FieldAgentPhone.vue').default
);
Vue.component(
	'field-owner2-phone',
	require('./components/FieldOwner2Phone.vue').default
);
Vue.component('field-map', require('./components/FieldMapOsm.vue').default);
Vue.component(
	'field-address',
	require('./components/FieldAddress.vue').default
);

Vue.component(
	'step-second',
	require('./components/input-kost/StepSecond.vue').default
);
Vue.component('field-name', require('./components/FieldName.vue').default);
Vue.component(
	'field-owner-name',
	require('./components/FieldOwnerName.vue').default
);
Vue.component(
	'field-manager-name',
	require('./components/FieldManagerName.vue').default
);
Vue.component(
	'field-manager-phone',
	require('./components/FieldManagerPhone.vue').default
);
Vue.component('field-gender', require('./components/FieldGender.vue').default);
Vue.component(
	'field-room-size',
	require('./components/FieldRoomSize.vue').default
);
Vue.component(
	'field-room-available',
	require('./components/FieldRoomAvailable.vue').default
);
Vue.component(
	'field-room-count',
	require('./components/FieldRoomCount.vue').default
);
Vue.component('field-price', require('./components/FieldPrice.vue').default);
Vue.component(
	'field-price-remark',
	require('./components/FieldPriceRemark.vue').default
);
Vue.component(
	'field-description',
	require('./components/FieldDescription.vue').default
);
Vue.component(
	'field-remarks',
	require('./components/FieldRemarks.vue').default
);

Vue.component(
	'step-third',
	require('./components/input-edit/DataFacility.vue').default
);
Vue.component(
	'field-min-payment',
	require('./components/input-edit/FieldMinPayment.vue').default
);
Vue.component(
	'field-facility-bath',
	require('./components/input-edit/FieldFacilityBath.vue').default
);
Vue.component(
	'field-facility-near',
	require('./components/input-edit/FieldFacilityNear.vue').default
);
Vue.component(
	'field-facility-parking',
	require('./components/input-edit/FieldFacilityParking.vue').default
);
Vue.component(
	'field-facility-share',
	require('./components/input-edit/FieldFacilityShare.vue').default
);
Vue.component(
	'field-facility-room',
	require('./components/input-edit/FieldFacilityRoom.vue').default
);
Vue.component(
	'field-photos',
	require('./components/input-edit/FieldPhotos.vue').default
);
Vue.component(
	'field-photos-upload',
	require('./components/input-edit/FieldPhotosUpload.vue').default
);

Vue.component(
	'modal-input-success',
	require('./components/ModalInputSuccess.vue').default
);
Vue.component(
	'modal-input-failed',
	require('./components/ModalInputFailed.vue').default
);
Vue.component(
	'modal-input-promo',
	require('./components/ModalInputPromo.vue').default
);

const store = new Vuex.Store({
	state: {
		token: document.head.querySelector('meta[name="csrf-token"]').content,
		section: 1,
		authCheck: {},
		authData: {},
		params: {
			_token: document.head.querySelector('meta[name="csrf-token"]').content,
			input_as: 'Agen',
			latitude: -7.7864525,
			longitude: 110.3659488,
			city: '',
			subdistrict: '',
			address: '',
			name: '',
			gender: 2,
			room_size: '3x3',
			room_count: 1,
			room_available: null,
			price_monthly: '',
			price_daily: '',
			price_weekly: '',
			price_yearly: '',
			price_remark: '',
			description: '',
			remarks: '',
			fac_property: [],
			fac_room: [],
			fac_bath: [],
			concern_ids: [],
			photos: {
				cover: [],
				main: [],
				bedroom: [],
				bath: [],
				other: []
			},
			owner_name: '',
			owner_phone: '',
			agent_name: '',
			agent_email: '',
			agent_phone: '',
			manager_name: '',
			manager_phone: ''
		},
		extras: {
			is_edit: false,
			property_type: document.head.querySelector('meta[name="property_type"]')
				.content,
			map_address: '',
			slug: '',
			fac_parking: [],
			fac_share: [],
			fac_near: [],
			fac_tags: {
				room: [],
				bath: [],
				parking: [],
				share: [],
				near: [],
				min_payment: [],
				photo_example: ''
			},
			fac_room_toggle: 'empty',
			photosBefore: {
				cover: [],
				main: [],
				bedroom: [],
				bath: [],
				other: []
			},
			photosAdded: {
				cover: false,
				main: false,
				bedroom: false,
				bath: false,
				other: false
			},
			photosUrl: {
				cover: [],
				main: [],
				bedroom: [],
				bath: [],
				other: []
			}
		}
	},
	mutations: {
		changeSection(state, payload) {
			state.section = payload;
			window.scroll(0, 0);
		},
		setAuthCheck(state, payload) {
			state.authCheck = payload;
		},
		setAuthData(state, payload) {
			state.authData = payload;
		},
		setParamsAgentName(state, payload) {
			state.params.agent_name = payload;
		},
		setParamsAgentEmail(state, payload) {
			state.params.agent_email = payload;
		},
		setParamsAgentPhone(state, payload) {
			state.params.agent_phone = payload;
		},
		setParamsOwnerPhone(state, payload) {
			state.params.owner_phone = payload;
		},
		setParamsLocationMap(state, payload) {
			state.params.latitude = payload.latitude;
			state.params.longitude = payload.longitude;
			state.params.city = payload.city;
			state.params.subdistrict = payload.subdistrict;
			state.extras.map_address = payload.map_address;
		},
		setParamsAddress(state, payload) {
			state.params.address = payload;
		},
		setParamsName(state, payload) {
			state.params.name = payload;
		},
		setExtrasSlug(state, payload) {
			state.extras.slug = payload;
		},
		setParamsOwnerName(state, payload) {
			state.params.owner_name = payload;
		},
		setParamsManagerName(state, payload) {
			state.params.manager_name = payload;
		},
		setParamsManagerPhone(state, payload) {
			state.params.manager_phone = payload;
		},
		setParamsGender(state, payload) {
			state.params.gender = payload;
		},
		setParamsRoomSize(state, payload) {
			state.params.room_size = payload;
		},
		setParamsRoomAvailable(state, payload) {
			state.params.room_available = payload;
		},
		setParamsRoomCount(state, payload) {
			state.params.room_count = payload;
		},
		setParamsPriceMonthly(state, payload) {
			state.params.price_monthly = payload;
		},
		setParamsPriceDaily(state, payload) {
			state.params.price_daily = payload;
		},
		setParamsPriceWeekly(state, payload) {
			state.params.price_weekly = payload;
		},
		setParamsPriceYearly(state, payload) {
			state.params.price_yearly = payload;
		},
		setParamsPriceRemark(state, payload) {
			state.params.price_remark = payload;
		},
		setParamsDescription(state, payload) {
			state.params.description = payload;
		},
		setParamsRemarks(state, payload) {
			state.params.remarks = payload;
		},
		setExtrasFacTags(state, payload) {
			state.extras.fac_tags = {
				min_payment: payload.minPayment,
				bath: payload.bath,
				near: payload.near,
				parking: payload.parking,
				room: payload.room,
				share: payload.share,
				photo_example: payload.photoExample
			};
		},
		setParamsConcernIds(state, payload) {
			state.params.concern_ids = payload;
		},
		changeExtrasFacRoomToggle(state, payload) {
			state.extras.fac_room_toggle = payload;
		},
		setParamsFacRoom(state, payload) {
			state.params.fac_room = payload;
		},
		setParamsFacBath(state, payload) {
			state.params.fac_bath = payload;
		},
		setExtrasFacParking(state, payload) {
			state.extras.fac_parking = payload;
		},
		setExtrasFacShare(state, payload) {
			state.extras.fac_share = payload;
		},
		setExtrasFacNear(state, payload) {
			state.extras.fac_near = payload;
		},
		setParamsFacProperty(state, payload) {
			if (payload == true) {
				state.params.fac_property.length = 0;
				state.extras.fac_parking.forEach((item, index) => {
					state.params.fac_property.push(item);
				});
				state.extras.fac_share.forEach((item, index) => {
					state.params.fac_property.push(item);
				});
				state.extras.fac_near.forEach((item, index) => {
					state.params.fac_property.push(item);
				});
			}
		},
		setExtrasPhotosBefore(state, payload) {
			state.extras.photosBefore = {
				cover: payload.cover,
				main: payload.bangunan,
				bedroom: payload.kamar,
				bath: payload['kamar-mandi'],
				other: payload.lainnya
			};
		},
		setExtrasPhotosBeforeEdit(state, payload) {
			state.extras.photosBefore = {
				cover: payload.cover,
				main: payload.main,
				bedroom: payload.bedroom,
				bath: payload.bath,
				other: payload.other
			};
		},
		removeExtrasPhotosBeforeMain(state, payload) {
			state.extras.photosBefore.main.length = payload;
		},
		removeExtrasPhotosBeforeCover(state, payload) {
			state.extras.photosBefore.cover.length = payload;
		},
		removeExtrasPhotosBeforeBedroom(state, payload) {
			state.extras.photosBefore.bedroom.length = payload;
		},
		removeExtrasPhotosBeforeBath(state, payload) {
			state.extras.photosBefore.bath.length = payload;
		},
		removeExtrasPhotosBeforeOther(state, payload) {
			state.extras.photosBefore.other.length = payload;
		},
		setParamsPhotos(state, payload) {
			state.params.photos = {
				cover: payload.cover,
				main: payload.main,
				bedroom: payload.bedroom,
				bath: payload.bath,
				other: payload.other
			};
		},
		setExtrasPhotosUrl(state, payload) {
			state.extras.photosUrl = {
				cover: payload.cover,
				main: payload.main,
				bedroom: payload.bedroom,
				bath: payload.bath,
				other: payload.other
			};
		},
		setExtrasPhotosAdded(state, payload) {
			state.extras.photosAdded = {
				cover: payload.cover,
				main: payload.main,
				bedroom: payload.bedroom,
				bath: payload.bath,
				other: payload.other
			};
		},
		setStateBookmark(state, payload) {
			state.params = payload.params;
			state.params._token = state.token;
			state.extras = payload.extras;
		}
	}
});

Vue.config.productionTip = false;

const app = new Vue({
	el: '#app',
	mixins: [mixinAuth],
	store
});
