require('../starter');

window.debounce = require('lodash/debounce');

window.Vue = require('vue').default;

import VueLazyload from 'vue-lazyload';
Vue.use(VueLazyload);

import Vuex from 'vuex';
Vue.use(Vuex);

import id from 'vee-validate/dist/locale/id';
import VeeValidate, { Validator } from 'vee-validate';
window.validator = new Validator();
Validator.localize('id', id);
Vue.use(VeeValidate, {
	locale: 'id'
});

const ContainerInputPropertyKost = require('./components/input-properti/ContainerInputPropertyKost.vue')
	.default;

// Container Component
Vue.component('container-input-property-kost', ContainerInputPropertyKost);

// Global Components
Vue.component('mami-loading', () => import('Js/@components/MamiLoading.vue'));
Vue.component('mami-loading-inline', () =>
	import('Js/@components/MamiLoadingInline.vue')
);
Vue.component('breadcrumb-pills', () =>
	import('Js/@components/BreadcrumbPills.vue')
);

// Local Components
Vue.component(
	'widget-help-button',
	require('./components/WidgetHelpButton.vue').default
);

Vue.component(
	'section-title',
	require('./components/SectionTitle.vue').default
);
Vue.component(
	'section-alert',
	require('./components/SectionAlert.vue').default
);

Vue.component(
	'data-claim',
	require('./components/input-properti/DataClaim.vue').default
);
Vue.component('field-claim', require('./components/FieldClaim.vue').default);

Vue.component(
	'data-location',
	require('./components/input-properti/DataLocation.vue').default
);
Vue.component('field-map', require('./components/FieldMapOsm.vue').default);
Vue.component(
	'field-address',
	require('./components/FieldAddress.vue').default
);

Vue.component(
	'data-kost',
	require('./components/input-properti/DataKost.vue').default
);
Vue.component('field-name', require('./components/FieldName.vue').default);
Vue.component('field-gender', require('./components/FieldGender.vue').default);
Vue.component(
	'field-room-size',
	require('./components/FieldRoomSize.vue').default
);
Vue.component(
	'field-year-building',
	require('./components/FieldYearBuilding.vue').default
);
Vue.component(
	'field-room-available',
	require('./components/FieldRoomAvailable.vue').default
);
Vue.component(
	'field-room-count',
	require('./components/FieldRoomCount.vue').default
);
Vue.component('field-price', require('./components/FieldPrice.vue').default);

Vue.component(
	'data-facility',
	require('./components/input-properti/DataFacility.vue').default
);
Vue.component(
	'field-facility-property',
	require('./components/FieldFacilityProperty.vue').default
);
Vue.component(
	'field-facility-room',
	require('./components/FieldFacilityRoom.vue').default
);
Vue.component(
	'field-facility-bath',
	require('./components/FieldFacilityBath.vue').default
);
Vue.component('field-photos', require('./components/FieldPhotos.vue').default);
Vue.component(
	'field-photos-upload',
	require('./components/FieldPhotosUpload.vue').default
);

Vue.component(
	'data-owner',
	require('./components/input-properti/DataOwner.vue').default
);
Vue.component(
	'field-owner-name',
	require('./components/FieldOwnerName.vue').default
);
Vue.component(
	'field-owner-email',
	require('./components/FieldOwnerEmail.vue').default
);
Vue.component(
	'field-password',
	require('./components/FieldPassword.vue').default
);

Vue.component(
	'data-room-allotment',
	require('./components/input-properti/DataRoomAllotment.vue').default
);
Vue.component(
	'field-room-allotment',
	require('./components/FieldRoomAllotment.vue').default
);
Vue.component(
	'filter-room-allotment',
	require('./components/FillterRoomAllotment.vue').default
);

Vue.component(
	'modal-input-success',
	require('./components/ModalInputSuccess.vue').default
);
Vue.component(
	'modal-input-failed',
	require('./components/ModalInputFailed.vue').default
);

const store = new Vuex.Store({
	state: {
		token: document.head.querySelector('meta[name="csrf-token"]').content,
		authCheck: authCheck,
		authData: authData,
		section: null,
		membership: {},
		params: {
			_token: document.head.querySelector('meta[name="csrf-token"]').content,
			input_as: document.head.querySelector('meta[name="input_as"]').content,
			latitude: -7.7864525,
			longitude: 110.3659488,
			city: '',
			subdistrict: '',
			address: '',
			name: '',
			gender: 2,
			room_size: '3x3',
			building_year: '',
			room_count: 1,
			room_available: null,
			price_monthly: '',
			price_daily: '',
			price_weekly: '',
			price_yearly: '',
			'3_month': '',
			'6_month': '',
			fac_property: [],
			fac_room: [],
			fac_bath: [],
			photos: {
				cover: null,
				bedroom: null,
				bath: null
			},
			owner_name: document.head.querySelector('meta[name="user_name"]').content,
			owner_phone: document.head.querySelector('meta[name="phone_number"]')
				.content,
			owner_email: document.head.querySelector('meta[name="user_email"]')
				.content,
			password: '',
			autobbk: 0
		},
		id_kos: '',
		room_unit: [],
		extras: {
			is_edit: false,
			property_type: document.head.querySelector('meta[name="property_type"]')
				.content,
			claim: document.head.querySelector('meta[name="claim"]').content,
			chatData: chatData,
			map_address: '',
			slug: '',
			fac_tags: {
				property: [],
				room: [],
				bath: [],
				photo_example: ''
			},
			fac_room_toggle: 'empty',
			photosUrl: {
				cover: {},
				bedroom: {},
				bath: {}
			},
			user_key: document.head.querySelector('meta[name="user_key"]').content,
			namePrevious: ''
		},
		is_price_flash_sale: {
			monthly: false,
			daily: false,
			weekly: false,
			quarterly: false,
			semiannually: false,
			annually: false
		}
	},
	mutations: {
		changeNamePrevious(state, payload) {
			state.extras.namePrevious = payload;
		},
		changeSection(state, payload) {
			state.section = payload;
			window.scroll(0, 0);
		},
		setParamsLocationMap(state, payload) {
			state.params.latitude = payload.latitude;
			state.params.longitude = payload.longitude;
			state.params.city = payload.city;
			state.params.subdistrict = payload.subdistrict;
			state.extras.map_address = payload.map_address;
		},
		setParamsAddress(state, payload) {
			state.params.address = payload;
		},
		setParamsName(state, payload) {
			state.params.name = payload;
		},
		setExtrasSlug(state, payload) {
			state.extras.slug = payload;
		},
		setParamsGender(state, payload) {
			state.params.gender = payload;
		},
		setParamsRoomSize(state, payload) {
			state.params.room_size = payload;
		},
		setParamsBuildingYear(state, payload) {
			state.params.building_year = payload;
		},
		setParamsRoomAvailable(state, payload) {
			state.params.room_available = payload;
		},
		setParamsRoomCount(state, payload) {
			state.params.room_count = payload;
		},
		setParamsPriceMonthly(state, payload) {
			state.params.price_monthly = payload;
		},
		setParamsPriceDaily(state, payload) {
			state.params.price_daily = payload;
		},
		setParamsPriceWeekly(state, payload) {
			state.params.price_weekly = payload;
		},
		setParamsPriceYearly(state, payload) {
			state.params.price_yearly = payload;
		},
		setParamsPriceThreeMonthly(state, payload) {
			state.params['3_month'] = payload;
		},
		setParamsPriceSixMonthly(state, payload) {
			state.params['6_month'] = payload;
		},
		setExtrasFacTags(state, payload) {
			state.extras.fac_tags = {
				property: payload.property,
				room: payload.room,
				bath: payload.bath,
				photo_example: payload.photoExample
			};
		},
		setParamsFacProperty(state, payload) {
			state.params.fac_property = payload;
		},
		changeExtrasFacRoomToggle(state, payload) {
			state.extras.fac_room_toggle = payload;
		},
		setParamsFacRoom(state, payload) {
			state.params.fac_room = payload;
		},
		setParamsFacBath(state, payload) {
			state.params.fac_bath.length = 0;
			state.params.fac_bath.push(payload);
		},
		setParamsPhotos(state, payload) {
			state.params.photos = {
				cover: payload.cover,
				bedroom: payload.bedroom,
				bath: payload.bath,
				main: payload.main
			};
		},
		setExtrasPhotosUrl(state, payload) {
			state.extras.photosUrl = {
				cover: payload.cover,
				bedroom: payload.bedroom,
				bath: payload.bath
			};
		},
		setParamsOwnerName(state, payload) {
			state.params.owner_name = payload;
		},
		setParamsOwnerEmail(state, payload) {
			state.params.owner_email = payload;
		},
		setParamsPassword(state, payload) {
			state.params.password = payload;
		},
		setParamsAutoBBK(state, payload) {
			state.params.autobbk = payload;
		},
		setParamsRoomAllotment(state, payload) {
			state.room_unit = payload;
		},
		setIdKos(state, payload) {
			state.id_kos = payload;
		},
		setMembershipData(state, payload) {
			state.membership = payload;
		}
	}
});

Vue.config.productionTip = false;

const app = new Vue({
	el: '#app',
	store
});
