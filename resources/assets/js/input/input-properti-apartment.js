require('../starter');

window.debounce = require('lodash/debounce');

window.Vue = require('vue').default;

import VueLazyload from 'vue-lazyload';
Vue.use(VueLazyload);

import Vuex from 'vuex';
Vue.use(Vuex);

import id from 'vee-validate/dist/locale/id';
import VeeValidate, { Validator } from 'vee-validate';
window.validator = new Validator();
Validator.localize('id', id);
Vue.use(VeeValidate, {
	locale: 'id'
});

const ContainerInputPropertyApartment = require('./components/input-properti-apartment/ContainerInputPropertyApartment.vue')
	.default;

// Container Component
Vue.component(
	'container-input-property-apartment',
	ContainerInputPropertyApartment
);

// Global Components
Vue.component('mami-loading', () => import('Js/@components/MamiLoading.vue'));
Vue.component('mami-loading-inline', () =>
	import('Js/@components/MamiLoadingInline.vue')
);
Vue.component('breadcrumb-pills', () =>
	import('Js/@components/BreadcrumbPills.vue')
);

// Local Components
Vue.component(
	'section-title',
	require('./components/SectionTitle.vue').default
);
Vue.component(
	'section-alert',
	require('./components/SectionAlert.vue').default
);

Vue.component(
	'step-first',
	require('./components/input-properti-apartment/StepFirst.vue').default
);
Vue.component(
	'field-name',
	require('./components/input-properti-apartment/FieldName.vue').default
);
Vue.component(
	'field-unit-name',
	require('./components/input-properti-apartment/FieldUnitName.vue').default
);
Vue.component(
	'field-unit-number',
	require('./components/input-properti-apartment/FieldUnitNumber.vue').default
);
Vue.component(
	'field-type',
	require('./components/input-properti-apartment/FieldType.vue').default
);
Vue.component(
	'field-floor',
	require('./components/input-properti-apartment/FieldFloor.vue').default
);
Vue.component(
	'field-size',
	require('./components/input-properti-apartment/FieldSize.vue').default
);
Vue.component(
	'field-price',
	require('./components/input-properti-apartment/FieldPrice.vue').default
);
Vue.component(
	'field-price-apartment',
	require('./components/input-properti-apartment/FieldPriceApartment.vue')
		.default
);
Vue.component(
	'field-price-extra',
	require('./components/input-properti-apartment/FieldPriceExtra.vue').default
);
Vue.component(
	'field-facility-unit',
	require('./components/input-properti-apartment/FieldFacilityUnit.vue').default
);
Vue.component(
	'field-facility-room',
	require('./components/input-properti-apartment/FieldFacilityRoom.vue').default
);
Vue.component(
	'field-description',
	require('./components/input-properti-apartment/FieldDescriptionApartment.vue')
		.default
);

Vue.component(
	'field-photos',
	require('./components/input-properti-apartment/FieldPhotosApartment.vue')
		.default
);
Vue.component(
	'field-photos-upload',
	require('./components/input-properti-apartment/FieldPhotosUploadApartment.vue')
		.default
);

Vue.component(
	'data-owner',
	require('./components/input-properti/DataOwner.vue').default
);
Vue.component(
	'field-owner-name',
	require('./components/FieldOwnerName.vue').default
);
Vue.component(
	'field-owner-email',
	require('./components/FieldOwnerEmail.vue').default
);
Vue.component(
	'field-password',
	require('./components/FieldPassword.vue').default
);

Vue.component(
	'modal-input-success',
	require('./components/ModalInputSuccess.vue').default
);
Vue.component(
	'modal-input-failed',
	require('./components/ModalInputFailed.vue').default
);

const store = new Vuex.Store({
	state: {
		token: document.head.querySelector('meta[name="csrf-token"]').content,
		section: 1,
		authCheck: {},
		authData: {},
		params: {
			_token: document.head.querySelector('meta[name="csrf-token"]').content,
			project_id: null,
			project_name: '',
			unit_name: '',
			unit_number: '',
			unit_type: '1-Room Studio',
			unit_type_name: '',
			floor: null,
			unit_size: null,
			description: '',
			price_type: 'idr',
			price_monthly: 0,
			price_daily: 0,
			price_weekly: 0,
			price_yearly: 0,
			min_payment: null,
			maintenance_price: '',
			parking_price: '',
			fac_unit: [],
			is_furnished: 0,
			fac_room: [],
			photos: {
				cover: 0,
				bedroom: [],
				bath: [],
				other: []
			},
			positionForm: 0,
			input_as: document.head.querySelector('meta[name="input_as"]').content,
			owner_name: document.head.querySelector('meta[name="user_name"]').content,
			owner_phone: document.head.querySelector('meta[name="phone_number"]')
				.content,
			owner_email: document.head.querySelector('meta[name="user_email"]')
				.content,
			password: ''
		},
		extras: {
			is_edit: false,
			property_type: document.head.querySelector('meta[name="property_type"]')
				.content,
			claim: document.head.querySelector('meta[name="claim"]').content,
			fac_tags: {
				unit_type: [],
				min_payment: [],
				unit: [],
				furnished: [],
				semi_furnished: [],
				photo_example: ''
			},
			photosUrl: {
				cover: [],
				bedroom: [],
				bath: [],
				other: []
			},
			user_key: document.head.querySelector('meta[name="user_key"]').content,
			price_daily_checked: false,
			price_weekly_checked: false,
			price_monthly_checked: false,
			price_yearly_checked: false
		}
	},
	mutations: {
		setAuthCheck(state, payload) {
			state.authCheck = payload;
		},
		setAuthData(state, payload) {
			state.authData = payload;
		},
		changeSection(state, payload) {
			state.section = payload;
			window.scroll(0, 0);
		},
		setExtrasFacTags(state, payload) {
			state.extras.fac_tags = {
				unit_type: payload.unitType,
				min_payment: payload.minPayment,
				unit: payload.unit,
				furnished: payload.furnished,
				semi_furnished: payload.semiFurnished,
				photo_example: payload.photoExample
			};
		},
		setParamsProjectId(state, payload) {
			state.params.project_id = payload;
		},
		setParamsProjectName(state, payload) {
			state.params.project_name = payload;
		},
		setParamsUnitName(state, payload) {
			state.params.unit_name = payload;
		},
		setParamsUnitNumber(state, payload) {
			state.params.unit_number = payload;
		},
		setParamsUnitType(state, payload) {
			state.params.unit_type = payload;
		},
		setParamsUnitTypeName(state, payload) {
			state.params.unit_type_name = payload;
		},
		setParamsFloor(state, payload) {
			state.params.floor = payload;
		},
		setParamsUnitSize(state, payload) {
			state.params.unit_size = payload;
		},
		setParamsPriceMonthly(state, payload) {
			state.params.price_monthly = payload;
		},
		setParamsPriceDaily(state, payload) {
			state.params.price_daily = payload;
		},
		setParamsPriceWeekly(state, payload) {
			state.params.price_weekly = payload;
		},
		setParamsPriceYearly(state, payload) {
			state.params.price_yearly = payload;
		},
		setParamsPriceShown(state, payload) {
			state.params.price_shown = payload;
		},
		setParamsMinPayment(state, payload) {
			state.params.min_payment = payload;
		},
		setParamsMaintenancePrice(state, payload) {
			state.params.maintenance_price = payload;
		},
		setParamsParkingPrice(state, payload) {
			state.params.parking_price = payload;
		},
		setParamsFacUnit(state, payload) {
			state.params.fac_unit = payload;
		},
		setParamsIsFurnished(state, payload) {
			state.params.is_furnished = payload;
		},
		setParamsFacRoom(state, payload) {
			state.params.fac_room = payload;
		},
		setParamsPhotos(state, payload) {
			state.params.photos = {
				cover: payload.cover,
				bedroom: payload.bedroom,
				bath: payload.bath,
				other: payload.other
			};
		},
		setExtrasPhotosUrl(state, payload) {
			state.extras.photosUrl = {
				cover: payload.cover,
				bedroom: payload.bedroom,
				bath: payload.bath,
				other: payload.other
			};
		},
		setParamsOwnerName(state, payload) {
			state.params.owner_name = payload;
		},
		setParamsOwnerEmail(state, payload) {
			state.params.owner_email = payload;
		},
		setParamsPassword(state, payload) {
			state.params.password = payload;
		},
		setParamsRateType(state, payload) {
			state.params.price_type = payload;
		},
		setExtrasPriceDaily(state, payload) {
			state.extras.price_daily_checked = payload;
		},
		setExtrasPriceWeekly(state, payload) {
			state.extras.price_weekly_checked = payload;
		},
		setExtrasPriceMonthly(state, payload) {
			state.extras.price_monthly_checked = payload;
		},
		setExtrasPriceYearly(state, payload) {
			state.extras.price_yearly_checked = payload;
		},
		setParamsDescription(state, payload) {
			state.params.description = payload;
		},
		setPositionForm(state, payload) {
			state.params.positionForm = payload;
		}
	}
});

Vue.config.productionTip = false;

const app = new Vue({
	el: '#app',
	store,
	created() {
		this.$store.commit('setAuthCheck', authCheck);
		this.$store.commit('setAuthData', authData);
	}
});
