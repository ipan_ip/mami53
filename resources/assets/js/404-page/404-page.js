require('../starter2');

window.Vue = require('vue').default;

const Container404 = require('./components/Container404.vue').default;

// Container Component
Vue.component('container-404', Container404);

const app = new Vue({
	el: '#app'
});
