/* eslint-disable no-unused-vars */
import 'Js/@mixins/MixinNavigatorIsMobile';
import 'Js/@mixins/MixinSwalLoading';
import 'Js/@mixins/MixinSwalSuccessError';
import mixinAuth from 'Js/@mixins/MixinAuth';
import App from './components/App.vue';
import Vuex from 'vuex';
import StoreData from './store';

import 'bangul-vue/dist/bangul.css';

const store = new Vuex.Store(StoreData);

Vue.component('app', App);

const app = new Vue({
	el: '#app',
	mixins: [mixinAuth],
	store,
	data() {
		return {
			state: {
				token: document.head.querySelector('meta[name="csrf-token"]').content,
				authCheck: {},
				authData: {}
			}
		};
	},
	mounted() {
		try {
			document.getElementById('preloading').remove();
		} catch (e) {
			document.getElementById('preloading').style.display = 'none';
		}
	},

	created() {
		// eslint-disable-next-line no-undef
		this.$store.dispatch('setRooms', rooms);
	}
});
