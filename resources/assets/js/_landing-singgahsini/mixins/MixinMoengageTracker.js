export default {
	methods: {
		handleClickRegister(redirection) {
			const path = /\/singgahsini/g;

			const registerLocation = 'daftar';

			this.moengageTracker(redirection);
			if (path.test(window.location.pathname)) {
				window.location.href = `/singgahsini/${registerLocation}`;
			} else {
				window.location.href = `/${registerLocation}`;
			}
		},
		moengageTracker(redirection) {
			const trackInterface = this.interfaceCheck();

			tracker('moe', [
				`Singgahsini - Registration Form Clicked`,
				{
					interface: trackInterface,
					is_owner: window.authCheck.owner,
					redirection_source: redirection
				}
			]);
		},
		interfaceCheck() {
			const userAgent = navigator.userAgent.toLocaleLowerCase();
			let userDevice = 'desktop';
			const operatingSystemList = {
				android: {
					regex: /(android)/g,
					os: 'mobile-android'
				},
				ios: {
					regex: /(iPod|iPad|iPhone)/g,
					os: 'mobile-ios'
				},
				mobile: {
					regex: /(windows phone 8|windows phone os 7|blackberry|playbookwebos)/g,
					os: 'mobile'
				}
			};

			const found = false;
			Object.keys(operatingSystemList).forEach(key => {
				if (!found && operatingSystemList[key].regex.test(userAgent)) {
					userDevice = operatingSystemList[key].os;
				}
			});

			return userDevice;
		}
	}
};
