export default {
	state: {
		token: document.head.querySelector('meta[name="csrf-token"]').textContent,
		authCheck: {},
		authData: {},
		roomsData: {}
	},
	mutations: {
		setAuthCheck(state, payload) {
			state.authCheck = payload;
		},
		setAuthData(state, payload) {
			state.authData = payload;
		},
		setRoomData(state, rooms) {
			state.roomsData = rooms;
		}
	},
	actions: {
		setRooms({ commit }, rooms) {
			commit('setRoomData', rooms);
		}
	},
	getters: {
		getRooms: state => {
			return state.roomsData;
		}
	}
};
