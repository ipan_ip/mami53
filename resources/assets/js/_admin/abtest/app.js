import Vue from 'vue';
import VeeValidate from 'vee-validate';
import 'bangul-vue/dist/bangul.css';
import './utils/vee-validate-rules';

import router from './router/index';
import App from './components/App';

window.axios = require('axios');

Vue.use(VeeValidate);

// eslint-disable-next-line no-new
new Vue({
	el: '#app',
	components: {
		App
	},
	router,
	data() {
		return {
			state: {
				// token: document.head.querySelector('meta[name="csrf-token"]').content,
				token: '',
				authCheck: {},
				authData: {}
			}
		};
	}
});
