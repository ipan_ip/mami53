import { Validator } from 'vee-validate';

Validator.extend('validExperimentName', {
	getMessage: () =>
		'Must be following the format: [Product_Name]-Experiment_Name',
	validate: value => /\[.+\]-(\w|\/)+$/.test(value)
});
