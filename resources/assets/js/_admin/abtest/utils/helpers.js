export const getStatus = (isActive = false, startDate = null) => {
	/**
	 * 1. Not Yet -> status false yang start datenya kosong
	 * 2. Running -> status true
	 * 3. Done -> status false yang start datenya isi
	 */

	if (isActive) {
		return 'Running';
	} else {
		if (startDate) {
			return 'Done';
		} else {
			return 'Not Yet';
		}
	}
};

export const conversionRatesGrowth = (variantRate, controlRate) => {
	if (controlRate > 0) {
		return (((variantRate - controlRate) / controlRate) * 100).toFixed(2);
	}

	return 0;
};

export const growthColor = value => {
	if (value > 0) {
		return '#00FF00';
	}

	return '#FF0000';
};
