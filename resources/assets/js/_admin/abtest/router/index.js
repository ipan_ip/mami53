import Vue from 'vue';
import VueRouter from 'vue-router';

import ListPage from '../pages/ListPage';
import DetailPage from '../pages/DetailPage';

Vue.use(VueRouter);

const routes = [
	{
		path: '/',
		name: 'List',
		component: ListPage
	},
	{
		path: '/detail/:id',
		name: 'Detail',
		component: DetailPage
	}
];

const router = new VueRouter({
	base: '/admin/abtest',
	mode: 'history',
	routes
});

export default router;
