export const EP_LIST_EXPERIMENTS = '/admin/abtest?mode=list_experiment';
export const EP_START_EXPERIMENT = '/admin/abtest?mode=start_experiment';
export const EP_STOP_EXPERIMENT = '/admin/abtest?mode=stop_experiment';
export const EP_DELETE_EXPERIMENT = '/admin/abtest?mode=delete_experiment';
export const EP_ADD_EXPERIMENT = '/admin/abtest?mode=create_experiment';
export const EP_UPDATE_EXPERIMENT = '/admin/abtest?mode=update_experiment';
export const EP_DETAIL_EXPERIMENT = '/admin/abtest?mode=goals_experiment';
