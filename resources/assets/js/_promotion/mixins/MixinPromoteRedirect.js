const mixinPromoteRedirect = {
	methods: {
		checkingPromoteRedirect() {
			if (Cookies.get('promote-redirect') == '1') {
				Cookies.remove('promote-redirect', {
					path: '/promosi-kost'
				});
				window.location.href = '/login?page=login';
			}
		},

		confirmationRedirect() {
			Cookies.set('promote-redirect', '1', {
				path: '/promosi-kost'
			});
			window.location.href = '/auth/logout';
		},

		setRedirectCookie() {
			swal({
				text:
					'Maaf, untuk menggunakan fitur ini Anda harus login sebagai Pemilik Iklan terlebih dahulu',
				type: 'info',
				allowOutsideClick: false,
				allowEscapeKey: false,
				showCancelButton: true,
				cancelButtonClass: 'btn-swal-cancel-custom',
				confirmButtonColor: '#1BAA56',
				confirmButtonText: 'Oke',
				cancelButtonText: 'Nanti saja'
			})
				.then(result => {
					if (result) {
						this.confirmationRedirect();
					}
				})
				.catch(swal.noop);
		}
	}
};

export default mixinPromoteRedirect;
