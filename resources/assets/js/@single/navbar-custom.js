Vue.component('navbar-custom', () => import('../@components/NavbarCustom.vue'));

const modal = new Vue({
	el: '#modalLogin'
});

const app = new Vue({
	el: '#navbarCustom',

	data() {
		return {
			state: {},
			detailNavigations: [],
			navigations: [
				{ name: 'Cari kost', url: '/cari' },
				{ name: 'Cari apartemen', url: '/apartemen' },
				{ name: 'Cari lowongan kerja', url: '/loker' }
			]
		};
	},

	created() {
		this.state.authCheck = authCheck;
		this.state.authData = authData;

		if (typeof latitude !== 'undefined') {
			this.state.lat = latitude;
		}
		if (typeof longitude !== 'undefined') {
			this.state.long = longitude;
		}
		this.detailNavigations = [
			{
				name: 'Cari di sekitar sini',
				url:
					'/cari/sekitar|' +
					this.state.lat +
					'|' +
					this.state.long +
					'/all/bulanan/0-15000000'
			},
			{ name: 'Cari kost', url: '/cari' },
			{ name: 'Cari apartemen', url: '/apartemen' },
			{ name: 'Cari lowongan kerja', url: '/loker' }
		];
	}
});
