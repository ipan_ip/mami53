import Vuex from 'vuex';

Vue.use(Vuex);

const store = new Vuex.Store({
	state: {
		isChatOpen: false,
		authCheck: {},
		authData: {}
	},
	mutations: {
		setIsChatOpen(state, payload) {
			state.isChatOpen = payload;
		},
		setAuthCheck(state, payload) {
			state.authCheck = payload;
		},
		setAuthData(state, payload) {
			state.authData = payload;
		}
	}
});

const app = new Vue({
	el: '#navbarOwner',
	components: {
		'navbar-owner': require('../@components/NavbarOwner.vue').default
	},
	store,
	data() {
		return {
			state: {}
		};
	},
	created() {
		this.state.authCheck = authCheck;
		this.state.authData = authData;
		this.$store.commit('setAuthCheck', authCheck);
		this.$store.commit('setAuthData', authData);
	}
});

const ownerMenu = new Vue({
	el: '#ownerMenu',
	components: {
		'owner-menu': require('../owner/components/OwnerMenu.vue').default
	},
	data() {
		return {
			state: {}
		};
	},
	created() {
		this.state.authCheck = authCheck;
		this.state.authData = authData;
	}
});
