/* eslint-disable no-undef */
import 'Js/@mixins/MixinNavigatorIsMobile';
import 'Js/@mixins/MixinSwalLoading';
import 'Js/@mixins/MixinSwalSuccessError';
import 'bangul-vue/dist/bangul.css';

import mixinAuth from 'Js/@mixins/MixinAuth';

import App from './components/DetailWithBooking.vue';

import store from './stores/index';
import router from './router';

import './mixins/MixinDetailType';

import Toasted from 'vue-toasted';
import Dayjs from 'vue-dayjs';
import Fragment from 'vue-fragment';
import 'dayjs/locale/id';
import VTooltip from 'v-tooltip';

Vue.use(VTooltip);
Vue.use(Toasted);
Vue.use(Fragment.Plugin);
Vue.use(Dayjs, {
	lang: 'id'
});

Vue.component('app', App);

const app = new Vue({
	el: '#app',
	mixins: [mixinAuth],
	router,
	store,
	created() {
		this.$store.commit('setDetail', detail);
		this.$store.commit(
			'setIsCheckinDateFirst',
			detail.is_booking_with_calendar || false
		);
		if (detail.type !== 'apartment') {
			this.$store.commit('setPriceCardInfo', price_card_info);
		}

		this.$store.commit('setDetailTypeName', detailTypeName);

		this.$store.commit(
			'setDisabledChat',
			!(
				detail.seq !== 46823270 &&
				detail.seq !== 99076892 &&
				detail.seq !== 46997623 &&
				detail.seq !== 35923806 &&
				detail.seq !== 95536057 &&
				detail.seq !== 46823270
			)
		);
	},
	mounted() {
		try {
			document.getElementById('preloading').remove();
		} catch (e) {
			document.getElementById('preloading').style.display = 'none';
		}
	}
});
