import Vuex from 'vuex';
import BookingRequestStore from './BookingRequestStore';
import DetailStore from './DetailStore';

const vuexStores = DetailStore;

// eslint-disable-next-line no-undef
if (detail.is_booking) {
	['state', 'mutations', 'getters', 'actions'].forEach(vuexProp => {
		vuexStores[vuexProp] = {
			...vuexStores[vuexProp],
			...BookingRequestStore[vuexProp]
		};
	});
}

export default new Vuex.Store(vuexStores);
