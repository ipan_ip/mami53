/* eslint-disable camelcase */

import {
	formatDate,
	getEstimateCheckout,
	getMoeSessionId,
	getDurationLabel
} from 'Js/_booking/request/utils/bookingRequestUtils';

export default {
	state: {
		token: document.head.querySelector('meta[name="csrf-token"]').content,
		authCheck: {},
		authData: {},
		params: {
			checkin: '',
			checkout: '',
			duration: 1,
			rent_count_type: '',
			contact_gender: 'male',
			contact_identity: '',
			contact_name: '',
			contact_phone: '',
			contact_job: '',
			contact_work_place: '',
			contact_introduction: '',
			total_renter: 1,
			is_married: false
		},
		flashSale: {
			isRunning: false,
			start: null,
			end: null
		},
		detail: {},
		extras: {
			selfieImg: null,
			idImg: null,
			idVerifStatus: null,
			emailVerifStatus: null,
			selfieId: null,
			cardId: null,
			isIntroAssigned: false,
			cardType: 'e_ktp',
			currentId: 'e_ktp'
		},
		showBookingSuccess: false,
		formError: {
			name: false,
			phone_number: false
		},
		showModalCancelBooking: false,
		submittingBooking: false,
		chatData: '',
		formAgreement: false,
		bookingRoom: {},
		bookingRoomLoaded: false,
		showBookingAlert: {
			show: false,
			message: ''
		},
		forcedValidation: '',
		isShowModalTerms: false,
		otherWorkPlace: '',
		submitBookingData: {},
		savedIds: false,
		isShowFlashSaleTerminatedModal: false
	},
	getters: {
		uncompleteTenantInfo: state => {
			const {
				formError: { name, phone_number },
				params: { contact_name, contact_phone }
			} = state;
			return !contact_name || !contact_phone || name || phone_number;
		},
		uncompleteRentInfo: state => {
			const {
				params: { checkin, duration }
			} = state;
			return !duration || !checkin;
		},
		uncompleteBookingForm: (state, getters) => {
			if (getters.uncompleteRentInfo) {
				let message = 'Mohon lengkapi Data Sewa Anda';
				if (!state.params.checkin) {
					message = 'Mohon isi tanggal masuk sewa';
				} else if (!state.params.duration) {
					message = 'Mohon isi durasi sewa';
				}
				return {
					error: true,
					message,
					path: '/date'
				};
			} else if (getters.uncompleteTenantInfo) {
				return {
					error: true,
					message: 'Mohon lengkapi Data Penyewa dulu',
					path: '/tenant-info'
				};
			}

			return {
				error: false,
				message: ''
			};
		},
		disabledSubmitBooking: (state, getters) => {
			return !state.formAgreement || getters.uncompleteBookingForm.error;
		},
		checkoutDate: ({ params }) => {
			return getEstimateCheckout(
				params.checkin,
				params.duration,
				params.rent_count_type
			);
		},
		bookingParams: (
			{ params, otherWorkPlace, flashSale },
			{ checkoutDate }
		) => {
			const contactWorkPlace = params.contact_work_place;
			return {
				...params,
				session_id: getMoeSessionId(),
				is_flash_sale: flashSale.isRunning,
				checkout: formatDate(checkoutDate, 'YYYY-MM-DD'),
				contact_work_place:
					contactWorkPlace === 'Lainnya' ? otherWorkPlace : contactWorkPlace
			};
		},
		durationLabel: ({ params }) => {
			return getDurationLabel(params.duration, params.rent_count_type);
		}
	},
	mutations: {
		setSavedIds(state, payload) {
			state.savedIds = payload;
		},
		setOtherWorkPlace(state, payload) {
			state.otherWorkPlace = payload;
		},
		setShowModalTerms(state, payload) {
			state.isShowModalTerms = payload;
		},
		setForcedValidation(state, payload) {
			state.forcedValidation = payload;
		},
		setShowModalCancelBooking(state, payload) {
			state.showModalCancelBooking = payload;
		},
		setUserDataName(state, payload) {
			state.params.contact_name = payload;
		},
		setUserDataEmail(state, payload) {
			state.params.contact_email = payload;
		},
		setUserDataPhone(state, payload) {
			state.params.contact_phone = payload;
		},
		setUserDataGender(state, payload) {
			state.params.contact_gender = payload;
		},
		setUserDataJob(state, payload) {
			state.params.contact_job = payload;
		},
		setUserDataWorkPlace(state, payload) {
			state.params.contact_work_place = payload;
		},
		setUserDataTotalRenter(state, payload) {
			state.params.total_renter = payload;
		},
		setUserDataIsMarriedStatus(state, payload) {
			state.params.is_married = payload;
		},
		setTenantDesc(state, payload) {
			if (payload == null) {
				state.params.contact_introduction = '';
			} else {
				state.params.contact_introduction = payload;
			}
		},
		setCheckin(state, payload) {
			state.params.checkin = payload;
		},
		setRentDuration(state, payload) {
			state.params.duration = payload;
		},
		setRentCountType(state, payload) {
			state.params.rent_count_type = payload;
		},
		setSelfieImg(state, payload) {
			state.extras.selfieImg = payload;
		},
		setIdImg(state, payload) {
			state.extras.idImg = payload;
		},
		setIdVerifStatus(state, payload) {
			state.extras.idVerifStatus = payload;
		},
		setEmailVerifStatus(state, payload) {
			state.extras.emailVerifStatus = payload;
		},
		setSelfieId(state, payload) {
			state.extras.selfieId = payload;
		},
		setCardId(state, payload) {
			state.extras.cardId = payload;
		},
		setIsIntroAssigned(state, payload) {
			state.extras.isIntroAssigned = payload;
		},
		setCardType(state, payload) {
			state.extras.cardType = payload;
		},
		setCurrentId(state, payload) {
			state.extras.currentId = payload;
		},
		setShowBookingSuccess(state, payload) {
			state.showBookingSuccess = payload;
		},
		setErrorName(state, payload) {
			state.formError.name = payload;
		},
		setErrorPhone(state, payload) {
			state.formError.phone_number = payload;
		},
		setSubmittingBooking(state, payload) {
			state.submittingBooking = payload;
		},
		setChatData(state, payload) {
			state.chatData = payload;
		},
		setFormAgreement(state, payload) {
			state.formAgreement = payload;
		},
		setBookingRoom(state, payload) {
			state.bookingRoomLoaded = true;
			state.bookingRoom = payload;
		},
		setShowBookingAlert(state, payload) {
			state.showBookingAlert = payload;
		},
		setSubmitBookingData(state, payload) {
			state.submitBookingData = payload;
		},
		setFlashSale(state, payload) {
			state.flashSale = payload;
		},
		setFlashSaleTerminatedModal(state, payload) {
			state.isShowFlashSaleTerminatedModal = payload;
		}
	},
	actions: {
		stopFlashSale({ commit }) {
			commit('setFlashSale', { isRunning: false, end: null, start: null });
		}
	}
};
