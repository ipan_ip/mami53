import _get from 'lodash/get';
export default {
	state: {
		token: document.head.querySelector('meta[name="csrf-token"]').content,
		authCheck: {},
		authData: {},
		detail: {},
		priceCardInfo: {},
		detailTypeName: null,
		isChatReplied: false,
		isCheckinDateFirst: false,
		extras: {
			isLimitedContent: false, // only valid for AB testing purpose, remove after that. idk when tho
			disabledChat: false
		},
		showBookingRequestProcess: false,
		bookingBenefit: {
			isShowModal: false,
			isModalCloseable: false,
			steps: [],
			cover: null,
			highlightedTarget: null,
			redirect: null
		},
		flashSaleData: {
			isShowModal: false,
			steps: [],
			redirect: null
		},
		priceDetailState: '',
		priceDetailData: null,
		rentTypes: {
			daily: 'Per Hari',
			weekly: 'Per Minggu',
			monthly: 'Per Bulan',
			quarterly: 'Per 3 Bulan',
			semiannually: 'Per 6 Bulan',
			yearly: 'Per Tahun'
		},
		isOverrideDraftCheckin: false,
		isOverrideDraftRentCountType: false,
		params: {
			rent_count_type: '',
			checkin: ''
		},
		isUserAbTest: false,
		isAbTestChecked: false
	},
	getters: {
		availablePrices: state => {
			const availablePrices = {};
			Object.keys(state.rentTypes).forEach(type => {
				const priceKey = `price_${type}`;
				const priceObject = _get(
					state.priceCardInfo,
					`price.${priceKey}`,
					null
				);
				const showedPrice = priceObject
					? priceObject.discount_price || priceObject.price
					: 0;
				if (priceObject && showedPrice) {
					availablePrices[type] = {
						currencySymbol: priceObject.currency_symbol,
						priceFormatted: showedPrice,
						price: parseInt(showedPrice.toString().replace(/\./g, '')),
						normalPrice: priceObject.price,
						discount: priceObject.discount,
						discountPrice: priceObject.discount_price
							? parseInt(showedPrice.toString().replace(/\./g, ''))
							: 0,
						unit: priceObject.rent_type_unit,
						unitLabel: state.rentTypes[type],
						type,
						priceKey
					};

					if (
						_get(state, `priceDetailData.${type}`, null) &&
						type !== 'daily'
					) {
						availablePrices[type].detail = state.priceDetailData[type];
					}
				}
			});
			return availablePrices;
		},
		priceKost: (state, getters) => {
			return _get(getters.availablePrices, state.params.rent_count_type) || {};
		},
		defaultRentType: (state, getters) => {
			if (getters.availablePrices.monthly) {
				return 'monthly';
			} else {
				// if there is no monthly rent type
				const orderedRentType = [
					'weekly',
					'quarterly',
					'semiannually',
					'yearly',
					'daily'
				];

				for (let i = 0; i < orderedRentType.length; i++) {
					const rentType = getters.availablePrices[orderedRentType[i]];
					if (rentType) {
						return orderedRentType[i];
					}
				}

				return '';
			}
		},
		defaultRentTypeLabel: (state, getters) => {
			return _get(
				getters.availablePrices,
				`${getters.defaultRentType}.unitLabel`,
				''
			);
		}
	},
	mutations: {
		setUserAbTest(state, payload) {
			state.isUserAbTest = payload;
		},
		setAbTestCheckedStatus(state, payload) {
			state.isAbTestChecked = payload;
		},
		setAuthCheck(state, payload) {
			state.authCheck = payload;
		},
		setAuthData(state, payload) {
			state.authData = payload;
		},
		setDetail(state, payload) {
			state.detail = payload;
			if (payload.is_booking && state.authData.all) {
				state.params.contact_identity = state.authData.all.id;
			}
		},
		setPriceCardInfo(state, payload) {
			state.priceCardInfo = payload;
		},
		setDetailTypeName(state, payload) {
			state.detailTypeName = payload;
		},
		setIsChatReplied(state, payload) {
			state.isChatReplied = payload;
		},
		setDisabledChat(state, payload) {
			state.extras.disabledChat = payload;
		},
		setLimitedContent(state, payload) {
			state.extras.isLimitedContent = payload;
		},
		setBookingRequestProcess(state, payload) {
			state.showBookingRequestProcess = payload;
		},
		setFTUEPayload(state, payload) {
			state[payload.tag] = { ...state.bookingBenefit, ...payload.data };
		},
		setIsCheckinDateFirst(state, payload) {
			state.isCheckinDateFirst = payload;
		},
		setPriceDetailState(state, payload) {
			state.priceDetailState = payload;
		},
		setPriceDetailData(state, payload) {
			state.priceDetailData = payload;
		},
		setRentCountType(state, payload) {
			state.params.rent_count_type = payload;
		},
		setIsOverrideDraftCheckin(state, payload) {
			state.isOverrideDraftCheckin = payload;
		},
		setIsOverrideDraftRentCountType(state, payload) {
			state.isOverrideDraftRentCountType = payload;
		},
		setCheckin(state, payload) {
			state.params.checkin = payload;
		}
	},
	actions: {
		setFormattedPriceDetail({ commit }, data) {
			if (data && data.length) {
				const priceDetail = {};
				data.forEach(price => {
					priceDetail[price.type] = price;
				});
				commit('setPriceDetailData', priceDetail);
			} else {
				commit('setPriceDetailData', null);
			}
		},
		setRentCountType({ commit }, data) {
			commit('setRentCountType', data);
			commit('setIsOverrideDraftRentCountType', true);
		}
	}
};
