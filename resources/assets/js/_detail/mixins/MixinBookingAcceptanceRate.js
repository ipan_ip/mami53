import _get from 'lodash/get';

export default {
	computed: {
		bookingAcceptanceRate() {
			const detailBAR = _get(this.$store.state, 'detail.booking', null);
			const ownerRates = [];
			const chatResponseTime = _get(
				this.$store.state,
				'detail.reply_average_time',
				null
			);

			if (
				!detailBAR ||
				(!detailBAR.active_from &&
					detailBAR.average_time === null &&
					!detailBAR.accepted_rate &&
					!chatResponseTime)
			)
				return null;

			const activeFrom = detailBAR.active_from
				? this.$dayjs(detailBAR.active_from)
						.locale('id')
						.format('MMM YYYY')
				: null;

			const fullDateTime = detailBAR.active_from || null;

			const acceptanceRate = detailBAR.accepted_rate
				? `${detailBAR.accepted_rate}%`
				: null;

			if (chatResponseTime || chatResponseTime === 0) {
				let chatResponse = null;
				if (chatResponseTime < 24 && chatResponseTime > 0) {
					chatResponse = `± ${chatResponseTime} jam`;
				} else if (chatResponseTime <= 0) {
					chatResponse = `± 1 jam`;
				} else {
					chatResponse = `± 24 jam`;
				}
				if (chatResponse) {
					ownerRates.push({
						icon: 'fast-respond-chat',
						iconTitle: 'rate chat dibalas',
						title: 'Chat Dibalas',
						label: chatResponse,
						description:
							'Waktu rata-rata pemilik kos ini membalas chat yang kamu kirim.'
					});
				}
			}

			if (detailBAR.average_time || detailBAR.average_time === 0) {
				let averageTime = null;
				const totalHour = detailBAR.average_time;
				if (totalHour < 24 && totalHour > 0) {
					averageTime = `± ${totalHour} jam`;
				} else if (totalHour <= 0) {
					averageTime = `± 1 jam`;
				} else {
					const days = Math.floor(totalHour / 24);
					const totalDiff = totalHour % 24;
					const totalDays = days + Math.round(totalDiff / 24);
					averageTime = `± ${totalDays} hari`;
				}
				if (averageTime) {
					ownerRates.push({
						icon: 'timer',
						iconTitle: 'rate booking diproses',
						title: 'Booking Diproses',
						label: averageTime,
						description:
							'Waktu rata-rata pemilik kos ini memproses booking. Lebih dari 3 hari, booking otomatis dibatalkan.'
					});
				}
			}

			if (acceptanceRate) {
				ownerRates.push({
					icon: 'renter-kos',
					iconTitle: 'rate peluang booking',
					title: 'Peluang Booking',
					label: acceptanceRate,
					description:
						'Tingkat kemungkinan booking yang kamu ajukan disetujui oleh pemilik kos ini, sebelum tahap pembayaran.'
				});
			}

			return {
				activeFrom,
				ownerRates,
				fullDateTime
			};
		}
	}
};
