/* eslint-disable no-undef */
import mixinGetQueryString from 'Js/@mixins/MixinGetQueryString';
import _get from 'lodash/get';
import _find from 'lodash/find';
import { makeAPICall } from 'Js/@utils/makeAPICall.js';

const mixinDetailChat = {
	mixins: [mixinGetQueryString],

	data() {
		return {
			userData: {},
			selectedQuestion: {},
			selectedQuestionId: null,
			adminId: null,
			adminList: [],
			callId: null,
			userGroup: [],
			groupChannel: null
		};
	},

	computed: {
		token() {
			return _get(this.$store, 'state.token');
		},
		authData() {
			return _get(this.$store, 'state.authData');
		},
		detail() {
			return _get(this.$store, 'state.detail');
		},
		questions() {
			return _get(this.$store, 'state.detail.questions');
		},
		seq() {
			return _get(this.$store, 'state.detail._id', '');
		},
		roomTitle() {
			return _get(this.$store, 'state.detail.room_title');
		},
		isPromoted() {
			return _get(this.$store, 'state.detail.is_promoted', false);
		},
		payFor() {
			return _get(this.$store, 'state.detail.pay_for', null);
		},
		isPremiumOwner() {
			return _get(this.$store, 'state.detail.is_premium_owner', false);
		},
		enableChat() {
			return _get(this.$store, 'state.detail.enable_chat');
		},
		ownerId() {
			return _get(this.$store, 'state.detail.owner_id', null);
		},
		ownerChatName() {
			return _get(this.$store, 'state.detail.owner_chat_name', '');
		},
		cards() {
			return _get(this.$store, 'state.detail.cards');
		},
		chatCallPostUrl() {
			return this.detailType === 'kost' || this.detailType === 'apartment'
				? `/stories/${this.seq}/call`
				: `/stories/house_property/${this.seq}/call`;
		},
		chatReplyPostUrl() {
			if (this.detailType === 'kost' || this.detailType === 'apartment') {
				return this.isPromoted && this.payFor === 'chat'
					? `/stories/${this.seq}/chat/reply?from_ads=true`
					: `/stories/${this.seq}/chat/reply`;
			} else {
				return `/stories/house_property/${this.seq}/chat/reply`;
			}
		},
		chatUtm() {
			return location.search !== '' ? this.getQueryString('utm') : '';
		},
		priceCardInfo() {
			return _get(this.$store, 'state.priceCardInfo');
		}
	},

	watch: {
		selectedQuestionId: {
			immediate: true,
			handler(val) {
				this.questions.forEach((question, i) => {
					if (question.id === val) {
						this.selectedQuestion = question;
					}
				});
			}
		}
	},

	created() {
		this.assignUserData();

		this.initEscKeyup();
	},

	methods: {
		initEscKeyup() {
			window.addEventListener('keyup', this.closeKeyModal);
		},
		closeKeyModal(e) {
			if (e.key === 'Escape') {
				this.closeModal();
			}
		},
		closeModal() {
			this.showModal = false;
		},
		assignSelectedQuestion(set) {
			if (set === null) {
				this.questions.forEach((question, i) => {
					if (i === 0) {
						this.selectedQuestion = question;
						this.selectedQuestionId = question.id;
					}
				});
			} else {
				this.selectedQuestion = set;
				this.selectedQuestionId = set.id;
			}
		},
		assignUserData() {
			Object.assign(this.userData, this.authData.all);
		},
		sendChat(source) {
			if (source === 'modal') {
				window.currentChatSource = 'pretext';
				this.closeModal();
			}

			this.openSwalLoading();

			axios
				.post(this.chatCallPostUrl, {
					_token: this.token,
					id_question: this.selectedQuestion.id,
					note: this.selectedQuestion.question,
					phone: this.userData.phone_number,
					email: this.userData.email,
					name: this.userData.name,
					group_chat_id: this.roomTitle,
					utm: this.chatUtm
				})
				.then(response => {
					if (response.data.status) {
						this.adminId = response.data.admin_id;
						this.adminList = response.data.admin_list;
						this.callId = response.data.call_id;

						if (response.data.group_channel_url) {
							this.getGroupChatByChannelUrl(response.data.group_channel_url);
						} else {
							const groupName = `${this.userData.name} : ${this.roomTitle}`;
							this.getGroupChatByName(groupName);
						}
					} else {
						this.swalError(
							null,
							_get(
								response,
								'data.meta.message',
								'Terjadi galat. Silakan coba lagi.'
							)
						);
					}

					if (this.detail.type === 'kost') {
						this.moengageEventTrack(response);
					}
				})
				.catch(error => {
					this.closeSwalLoading();

					bugsnagClient.notify(error);

					this.swalError(null, 'Terjadi galat. Silakan coba lagi.');
				});
		},
		getGroupChatByChannelUrl(channelUrl) {
			const channelListQuery = sb.GroupChannel.createMyGroupChannelListQuery();
			channelListQuery.includeEmpty = true;
			channelListQuery.channelUrlsFilter = [channelUrl];

			channelListQuery.next((groupChannels, error) => {
				if (error) {
					return;
				}

				if (groupChannels.length) {
					this.groupChannel = groupChannels[0];
					this.sendChatToGroupChannel();
				} else {
					this.swalError(
						null,
						'Maaf, terjadi kesalahan pada sistem chat. Silakan coba lagi.'
					);
				}
			});
		},
		getGroupChatByName(groupName) {
			const channelListQuery = sb.GroupChannel.createMyGroupChannelListQuery();
			channelListQuery.includeEmpty = true;
			channelListQuery.channelNameContainsFilter = groupName;

			channelListQuery.next((groupChannels, error) => {
				if (error) {
					return;
				}

				const groups = groupChannels;

				for (const group of groups) {
					const isOwnerAMember = Boolean(
						_find(group.members, member => member.userId === this.ownerId)
					);
					if (
						group.name.trim().toLowerCase() ===
							groupName.trim().toLowerCase() &&
						isOwnerAMember
					) {
						this.groupChannel = group;

						for (const member of group.members) {
							if (this.adminList.includes(parseInt(member.userId))) {
								this.adminId = member.userId;
							}
						}

						break;
					}
				}

				this.checkGroupChannelExistance();
			});
		},
		checkGroupChannelExistance() {
			if (this.groupChannel === null) {
				this.createChatGroupChannel();
			} else {
				this.sendChatToGroupChannel();
			}
		},
		createChatGroupChannel() {
			this.userGroup = [this.userData.id, this.adminId];

			const dataParams = {
				room_id: this.seq
			};

			if (this.enableChat && this.ownerId !== null) {
				this.userGroup.push(this.ownerId);
			}

			const groupName = `${this.userData.name} : ${this.roomTitle}`;
			const groupImage =
				this.cards[0].photo_url.small ||
				'/assets/default_chat_channel_cover.png';

			sb.GroupChannel.createChannelWithUserIds(
				this.userGroup,
				false,
				groupName,
				groupImage,
				JSON.stringify(dataParams),
				(groupChannel, error) => {
					if (error) {
						return;
					}

					this.groupChannel = groupChannel;

					const params = new sb.UserMessageParams();
					params.message = this.selectedQuestion.question;
					params.data = JSON.stringify(dataParams);

					this.groupChannel.sendUserMessage(params, (message, error) => {
						if (error) {
							return;
						}

						this.insertChannelMetadata();

						if (this.selectedQuestion.id !== 0) {
							this.sendAutoChatReply();
						} else {
							sbWidget.showChannel(this.groupChannel.url);
							this.closeSwalLoading();
						}
					});
				}
			);
		},
		sendChatToGroupChannel() {
			const dataParams = {
				room_id: this.seq
			};

			const params = new sb.UserMessageParams();
			params.message = this.selectedQuestion.question;
			params.data = JSON.stringify(dataParams);

			this.groupChannel.sendUserMessage(params, (message, error) => {
				if (error) {
					return;
				}
				this.insertChannelMetadata();
				if (this.selectedQuestion.id !== 0) {
					this.sendAutoChatReply();
				} else {
					sbWidget.showChannel(this.groupChannel.url);
					this.closeSwalLoading();
				}
			});
		},
		insertChannelMetadata() {
			const upsertIfNotExist = true;
			const metadata = {
				room: this.seq.toString(),
				chat_support: false
			};

			if (this.detailType === 'apartment') {
				metadata.apartment_id = this.seq.toString();
			}

			if (this.detailType === 'house') {
				metadata.house_id = this.seq.toString();
			}

			this.groupChannel.updateMetaData(metadata, upsertIfNotExist);
		},
		async sendAutoChatReply() {
			const reqData = {
				_token: this.token,
				id_question: this.selectedQuestion.id,
				note: this.selectedQuestion.question,
				admin_id: this.adminId,
				call_id: this.callId,
				group_id: this.groupChannel.url,
				group_channel_url: this.groupChannel.url
			};

			const response = await makeAPICall({
				method: 'post',
				url: this.chatReplyPostUrl,
				data: reqData
			});

			if (response) {
				sbWidget.showChannel(this.groupChannel.url);
				this.closeSwalLoading();
				if (Cookies.get('mami-reply-chat') !== 1) {
					this.setReplyCookie();
				}
			} else {
				this.closeSwalLoading();
				this.swalError(
					null,
					'Maaf, terjadi kesalahan pada sistem chat. Silakan coba lagi.'
				);
			}
		},
		setReplyCookie() {
			Cookies.set('mami-reply-chat', 1, { expires: 30 });
			this.$store.commit('setIsChatReplied', true);
		},
		moengageEventTrack(response) {
			tracker('moe', [
				'[User] Send Message',
				{
					success_status: _get(response, 'data.status', false),
					property_type: _get(this.detail, 'type', null),
					property_id: +this.seq || null,
					property_name: _get(this.detail, 'room_title', null),
					property_location_city: _get(this.detail, 'area_city', null),
					property_location_subdistrict: _get(
						this.detail,
						'area_subdistrict',
						null
					),
					property_updated_at: _get(this.detail, 'updated_at', null),
					daily_price: _get(this.detail, 'daily_price', null),
					weekly_price: _get(this.detail, 'weekly_price', null),
					monthly_price: _get(this.detail, 'monthly_price', null),
					yearly_price: _get(this.detail, 'yearly_price', null),
					is_premium: _get(this.detail, 'is_premium_owner', false),
					is_promoted: _get(this.detail, 'is_promoted', false),
					is_promo_ngebut: _get(this.detail, 'is_flash_sale', false),
					is_booking: _get(this.detail, 'is_booking', false),
					goldplus_status: _get(this.detail, 'goldplus_status', null),
					room_available: _get(this.detail, 'available_room', null),
					owner_id: +this.detail.owner_id || null,
					owner_name: _get(this.detail, 'owner_name', null),
					owner_phone_number: _get(this.detail, 'phone', null),
					user_phone_number: _get(this.userData, 'phone_number', null)
				}
			]);
		}
	}
};

export default mixinDetailChat;
