Vue.mixin({
	computed: {
		detailType() {
			return this.$store.state.detail.type;
		},
		detailTypeName() {
			return this.$store.state.detailTypeName;
		}
	}
});
