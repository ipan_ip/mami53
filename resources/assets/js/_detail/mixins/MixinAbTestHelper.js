import { mapState } from 'vuex';

export default {
	data() {
		return {
			/**
			 * the attribute below is need to check if section viewed
			 * before or after the ab test checking is done
			 * because we want to maximize the performance by call
			 * api only one time and only when user scrolled to the section
			 */
			isViewedBeforeCheckingAbTest: false
		};
	},
	computed: {
		...mapState(['isUserAbTest', 'isAbTestChecked'])
	}
};
