const mixinDetailAds = {
	computed: {
		token() {
			return this.$store.state.token;
		},
		seq() {
			return this.$store.state.detail._id;
		}
	},

	mounted() {
		this.$nextTick(() => {
			this.handleAds();
		});
	},

	methods: {
		handleAds() {
			let logView = fromads => {
				// if it's more than 2 hours from load time, let assume this is loaded from cache
				let pageLoadTime = moment(
					document.querySelector('meta[name="load-time"]').content
				);

				if (pageLoadTime.add(30, 'minutes') < moment()) {
					return;
				}

				// if using back button, then no need to execute view function
				if (!!window.performance && window.performance.navigation.type === 2) {
					return;
				}

				axios
					.post(`/stories/${this.seq}/view`, {
						_token: this.token,
						from_ads: fromads
					})
					.then(response => {})
					.catch(error => {
						bugsnagClient.notify(error);
					});
			};

			let adscookies = Cookies.getJSON('adsids');
			let cookiesfound;

			if (adscookies !== undefined) {
				let currentid = this.seq;

				cookiesfound = false;
				for (let idx in adscookies) {
					let currentTime = new Date();

					if (
						adscookies[idx].id == currentid &&
						adscookies[idx].expire > currentTime.getTime()
					) {
						cookiesfound = true;
						logView(true);
					}
				}

				if (!cookiesfound) {
					logView(false);
				}
			} else {
				logView(false);
			}
		}
	}
};

export default mixinDetailAds;
