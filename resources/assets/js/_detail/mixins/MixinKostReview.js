export default {
	filters: {
		ratingText(value) {
			return typeof value === 'number' && !isNaN(value)
				? value.toFixed(1)
				: value;
		}
	},
	methods: {
		handleCalculateReviewDate(days) {
			if (days < 7) {
				// handle days
				if (days > 2) {
					return `${days} hari yang lalu`;
				} else {
					return days === 1 ? 'Hari ini' : 'Kemarin';
				}
			} else if (days < 30) {
				// handle weeks
				const week = days / 7;
				return `${Math.floor(week)} minggu yang lalu`;
			} else if (days < 365) {
				// handle months
				const month = days / 30;
				return `${Math.floor(month)} bulan yang lalu`;
			} else {
				// handle years
				const year = days / 365;
				return `${Math.floor(year)} tahun yang lalu`;
			}
		}
	}
};
