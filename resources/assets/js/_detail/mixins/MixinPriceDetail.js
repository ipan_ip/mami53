import _get from 'lodash/get';

const mixinPriceDetail = {
	computed: {
		priceDetailState: {
			get() {
				return _get(this.$store.state, 'priceDetailState');
			},
			set(value) {
				this.$store.commit('setPriceDetailState', value);
			}
		},
		rentTypeParams: {
			get() {
				return _get(this.$store.state, 'params.rent_count_type', '');
			},
			set(rentType) {
				this.$store.commit('setRentCountType', rentType);
			}
		},
		isLoadedPriceEstimation() {
			return this.priceDetailState === 'loaded';
		},
		isLoadingPriceEstimation() {
			return this.priceDetailState === 'loading';
		},
		availablePrices() {
			return _get(this.$store.getters, 'availablePrices', {});
		},
		availablePriceKeys() {
			return Object.keys(this.availablePrices);
		},
		defaultRentType() {
			return _get(this.$store.getters, 'defaultRentType', '');
		},
		defaultRentTypeLabel() {
			return _get(this.$store.getters, 'defaultRentTypeLabel', '');
		},
		selectedRentPrice() {
			return _get(this.availablePrices, this.rentTypeParams, null);
		},
		songId() {
			return _get(this.$store.state, 'detail._id', 0);
		},
		isKostFlashSale() {
			return _get(this.$store.state, 'detail.is_flash_sale', false);
		}
	},
	methods: {
		async getBookingPriceEstimation() {
			if (this.priceDetailState === 'loading') return;

			this.priceDetailState = 'loading';
			try {
				const res = await axios.get(
					`/user/booking/draft-price/${this.songId}`,
					{ params: { is_flash_sale: this.isKostFlashSale } }
				);
				const responseStatus = _get(res, 'data.status', false);
				const responseData = _get(res, 'data.data', null);
				if (responseStatus) {
					this.$store.dispatch('setFormattedPriceDetail', responseData);
					this.priceDetailState = 'loaded';
				} else {
					this.priceDetailState = 'failed';
				}
			} catch (error) {
				this.priceDetailState = 'failed';
				bugsnagClient.notify(error);
			}
		}
	}
};

export default mixinPriceDetail;
