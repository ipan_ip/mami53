/* eslint-disable no-undef */
import mixinGetQueryString from '../../@mixins/MixinGetQueryString';
import EventBus from '../event-bus';
import _get from 'lodash/get';
import { makeAPICall } from 'Js/@utils/makeAPICall.js';

const mixinContainerKost = {
	mixins: [mixinGetQueryString],

	data() {
		return {
			isCheckIn: false,
			paramsRelated: {},
			isModalPromo: false,
			isModalLoginVisible: false,
			isModalClaim: false,
			isModalCheckin: false,
			isModalMobilePrice: false,
			isModalBooking: false,
			isModalGenderMismatch: false,
			isModalGenderNull: false,
			isShowPriceEstimationModal: false,
			priceInfoModal: {
				show: false,
				info: '',
				items: []
			},
			modalLoginState: 'default'
		};
	},

	computed: {
		token() {
			return this.$store.state.token;
		},
		authCheck() {
			return this.$store.state.authCheck;
		},
		authData() {
			return this.$store.state.authData;
		},
		detail() {
			return this.$store.state.detail;
		},
		detailTypeName() {
			return this.$store.state.detailTypeName;
		},
		breadcrumbs() {
			return this.$store.state.detail.breadcrumbs || [];
		},
		seq() {
			return this.$store.state.detail.seq || null;
		},
		location() {
			return this.$store.state.detail.location || [null, null];
		},
		latitude() {
			return this.$store.state.detail.latitude || null;
		},
		longitude() {
			return this.$store.state.detail.longitude || null;
		},
		urllanding() {
			return this.$store.state.detail.urllanding || '/';
		},
		alreadyBooked() {
			return this.$store.state.detail.already_booked || false;
		},
		checker() {
			return this.$store.state.detail.checker || null;
		},
		gender() {
			return this.$store.state.detail.gender || 0;
		},
		priceCardInfo() {
			return this.$store.state.priceCardInfo;
		},
		priceDaily() {
			if (this.priceCardInfo.price.price_daily) {
				return this.priceCardInfo.price.price_daily.discount_price
					? this.priceCardInfo.price.price_daily.discount_price
					: this.priceCardInfo.price.price_daily.price;
			} else {
				return '0';
			}
		},
		priceWeekly() {
			if (this.priceCardInfo.price.price_weekly) {
				return this.priceCardInfo.price.price_weekly.discount_price
					? this.priceCardInfo.price.price_weekly.discount_price
					: this.priceCardInfo.price.price_weekly.price;
			} else {
				return '0';
			}
		},
		priceMonthly() {
			if (this.priceCardInfo.price.price_monthly) {
				return this.priceCardInfo.price.price_monthly.discount_price
					? this.priceCardInfo.price.price_monthly.discount_price
					: this.priceCardInfo.price.price_monthly.price;
			} else {
				return '0';
			}
		},
		priceQuarterly() {
			if (this.priceCardInfo.price.price_quarterly) {
				return this.priceCardInfo.price.price_quarterly.discount_price
					? this.priceCardInfo.price.price_quarterly.discount_price
					: this.priceCardInfo.price.price_quarterly.price;
			} else {
				return '0';
			}
		},
		priceSemiannually() {
			if (this.priceCardInfo.price.price_semiannually) {
				return this.priceCardInfo.price.price_semiannually.discount_price
					? this.priceCardInfo.price.price_semiannually.discount_price
					: this.priceCardInfo.price.price_semiannually.price;
			} else {
				return '0';
			}
		},
		priceYearly() {
			if (this.priceCardInfo.price.price_yearly) {
				return this.priceCardInfo.price.price_yearly.discount_price
					? this.priceCardInfo.price.price_yearly.discount_price
					: this.priceCardInfo.price.price_yearly.price;
			} else {
				return '0';
			}
		},
		isPremiumOwner() {
			return this.$store.state.detail.is_premium_owner || false;
		},
		isSuperKost() {
			return _get(this.$store.state, 'detail.level_info.id') === superKostId;
		},
		isOkeKost() {
			return _get(this.$store.state, 'detail.level_info.id') === okeKostId;
		},
		isMoneyBackGuarantee() {
			return (
				_get(this.$store.state, 'detail.fac_room', []).filter(facility =>
					facility.includes('Garansi Uang Kembali')
				).length > 0
			);
		},
		isMamirooms() {
			return _get(this.$store.state, 'detail.is_mamirooms');
		},
		isVerified() {
			return Boolean(_get(this.$store.state, 'detail.checker', null));
		},
		activeBadges() {
			const badgesNames = [];

			if (this.detailTypeName.toLowerCase() === 'kost') {
				this.isMoneyBackGuarantee && badgesNames.push('moneyBackGuarantee');
				this.isSuperKost && badgesNames.push('superKost');
				this.isOkeKost && badgesNames.push('okeKost');
				this.isVerified && badgesNames.push('verified');
			} else {
				// Apartment only has premium badges
				this.isPremiumOwner && badgesNames.push('premiumApartment');
			}

			return badgesNames;
		},
		deviceType() {
			return _get(this.navigator, 'isMobile', false) ? 'mobile' : 'desktop';
		},
		kostLevelInfoName() {
			return _get(this.$store.state, 'detail.level_info.name', null);
		}
	},

	created() {
		this.checkIfCheckIn();
		this.assignRelatedLocation();
		if (this.isListenEventBus) {
			this.listenEventBus();
		}
	},

	mounted() {
		if (this.detail.is_booking) {
			if (
				this.getQueryString('booking') === 'true' &&
				this.getQueryString('redirection_source') === 'booking form'
			) {
				this.openBookingKost({ isAvailable: true });
			} else if (this.getQueryString('booking') === 'true') {
				this.openBookingKost({
					isAvailable: true,
					redirectionSource: 'chat'
				});
			} else if (
				this.getQueryString('draft') === 'true' ||
				this.getQueryString('booking_form') === 'true'
			) {
				const options = { isAvailable: true };
				const redirectionSource = this.getRedirectionSourceQueryParams();
				if (redirectionSource) {
					options.redirectionSource = redirectionSource;
				}
				this.openBookingKost(options);
			}
		}
	},

	methods: {
		goToLanding(event) {
			process.env.MIX_MAMIROOMS_LANDING_URL
				? window.open(process.env.MIX_MAMIROOMS_LANDING_URL, '_blank')
				: event.preventDefault();
		},
		getRedirectionSourceQueryParams() {
			const { referrer } = document;
			if (referrer) {
				const referrerRedirectionSource = this.getQueryString(
					'redirection_source',
					referrer
				);
				const redirectionSource = this.getQueryString('redirection_source');
				const isSameUrl =
					this.removeUrlHashAndProtocol(referrer) ===
					this.removeUrlHashAndProtocol(window.location.href);
				if (!isSameUrl && redirectionSource !== referrerRedirectionSource) {
					return redirectionSource;
				}
			}
			return '';
		},
		removeUrlHashAndProtocol(url) {
			return url.replace(/^https?:\/\//, '').split('#')[0];
		},
		checkIfCheckIn() {
			if (this.getQueryString('checkin') === 'true') {
				this.isCheckIn = true;
			} else {
				this.isCheckIn = false;
			}
		},
		assignRelatedLocation() {
			this.paramsRelated.location = this.location;
		},
		listenEventBus() {
			EventBus.$on('openModalPromo', payload => {
				this.isModalPromo = true;
			});

			EventBus.$on('openModalChat', payload => {
				if (!this.authCheck.all) {
					this.openModalLogin();
				} else if (this.authCheck.all) {
					if (this.authCheck.user) {
						this.checkOpenedChatSection();

						if (this.$refs.detailModalChat) {
							this.$refs.detailModalChat.showModal = true;
						} else {
							return;
						}

						if (typeof payload !== 'undefined') {
							this.$refs.detailModalChat.assignSelectedQuestion(payload);
						} else {
							this.$refs.detailModalChat.assignSelectedQuestion(null);
						}
					} else {
						this.swalError(
							null,
							'Silakan logout akun Pemilik dan login akun Pencari terlebih dahulu'
						);
					}
				}
			});

			EventBus.$on('sendPretextChat', async payload => {
				if (!this.authCheck.all) {
					this.openModalLogin();
				} else if (this.authCheck.all) {
					if (this.authCheck.user) {
						this.openSwalLoading();

						const response = await makeAPICall({
							method: 'post',
							url: '/chat-pretext',
							data: payload
						});

						if (response) {
							this.closeSwalLoading();
							this.checkOpenedChatSection();

							this.$refs.detailModalChat.assignSelectedQuestion(
								_get(response, 'chat-pretext', null)
							);
							this.$refs.detailModalChat.sendChat();
						} else {
							this.closeSwalLoading();
							this.swalError(null, 'Terjadi galat. Silakan coba lagi.');
						}
					} else {
						this.swalError(
							null,
							'Silakan logout akun Pemilik dan login akun Pencari terlebih dahulu'
						);
					}
				}
			});

			EventBus.$on('openModalLogin', payload => {
				this.openModalLogin();
			});

			EventBus.$on('openModalClaim', payload => {
				this.isModalClaim = true;
			});

			EventBus.$on('openModalCheckin', payload => {
				this.openModalCheckin();
			});

			EventBus.$on('sendClaimKost', payload => {
				this.sendClaimKost(payload);
			});

			EventBus.$on('sendCheckinKost', payload => {
				this.sendCheckinKost(payload);
			});

			EventBus.$on('openBookingKost', payload => {
				this.openBookingKost(payload);
			});
			EventBus.$on('openModalMobilePrice', payload => {
				this.openModalMobilePrice();
			});
			EventBus.$on('closeModalMobilePrice', payload => {
				this.closeModalMobilePrice();
			});
			EventBus.$on('trackClickBooking', payload => {
				this.trackBookingClick(payload || {});
			});
			EventBus.$on('validateOpenBookingKost', ({ isAvailable, callback }) => {
				const isValidUser = this.validateOpenBookingKost(isAvailable);
				if (isValidUser && typeof callback === 'function') {
					callback();
				}
			});

			// price modal
			EventBus.$on(
				'setPriceInfo',
				({ show = false, info = '', items = [] }) => {
					this.priceInfoModal = { show, info, items };
				}
			);
			EventBus.$on('showPriceEstimationModal', payload => {
				this.isShowPriceEstimationModal = !!payload;
			});
		},
		checkOpenedChatSection() {
			const chatSectionChildCount = document.querySelector('.chat-section')
				.childElementCount;

			if (chatSectionChildCount > 0) {
				const closeChatSections = document.querySelectorAll(
					'#sb_widget .ic-close'
				);

				for (const closeChatSection of closeChatSections) {
					closeChatSection.click();
				}
			}
		},
		sendClaimKost(claimer) {
			this.openSwalLoading();

			axios
				.post('/owner/claim', {
					_id: this.seq,
					status: claimer,
					_token: this.token
				})
				.then(response => {
					this.closeSwalLoading();
					if (response.data.status) {
						this.swalSuccess('', 'Kost berhasil diklaim', onConfirm => {
							window.open('/ownerpage', '_self');
						});
					} else {
						this.swalError(null, response.data.meta.message);
					}
				})
				.catch(error => {
					this.closeSwalLoading();
					bugsnagClient.notify(error);
					this.swalError(null, 'Terjadi galat. Silakan coba lagi.');
				});
		},
		sendCheckinKost(date) {
			this.closeModalCheckin();
			this.openSwalLoading();

			axios
				.post('/user/checkin', {
					id: this.seq,
					kost_time: date,
					_token: this.token
				})
				.then(response => {
					this.closeSwalLoading();
					if (response.data.status) {
						this.swalSuccess('', 'Berhasil checkin!', onConfirm => {
							window.open('/user/kost-saya', '_self');
						});
					} else {
						this.swalError(null, response.data.meta.message);
					}
				})
				.catch(error => {
					this.closeSwalLoading();
					bugsnagClient.notify(error);
					this.swalError(
						null,
						'Terjadi galat. Silakan coba lagi. (toggleLove, sendClaimKost)'
					);
				});
		},
		validateOpenBookingKost(isAvailable) {
			const kostGender = _get(this.$store.state, 'detail.gender', 0);

			if (this.authCheck.user) {
				if (this.alreadyBooked) {
					this.isModalBooking = true;
				} else {
					if (isAvailable) {
						if (
							this.authData.all.gender !== 'female' &&
							this.authData.all.gender !== 'male'
						) {
							this.isModalGenderNull = true;
						} else {
							if (
								(this.authData.all.gender === 'female' && kostGender === 1) ||
								(this.authData.all.gender === 'male' && kostGender === 2)
							) {
								this.isModalGenderMismatch = true;

								this.trackGenderMismatch();
							} else {
								return true;
							}
						}
					} else {
						this.swalError(
							null,
							'Mohon maaf, booking belum tersedia untuk harian. Kembali lain waktu atau cari kos lain.'
						);
					}
				}
			} else {
				this.isModalLoginVisible = true;
			}

			return false;
		},
		openBookingKost({
			isAvailable = true,
			redirectionSource = '',
			isSendTracker = true
		}) {
			const trackerOptions = {};
			if (redirectionSource) {
				trackerOptions.redirectionSource = redirectionSource;
			}

			if (this.validateOpenBookingKost(isAvailable)) {
				!!isSendTracker && this.trackBookingClick(trackerOptions);
				this.trackBookingFormVisited(redirectionSource);
				this.$store.commit('setBookingRequestProcess', true);
				this.$router.push({ name: 'booking-date' });
			} else {
				this.trackBookingClick(trackerOptions);
			}
		},
		openModalLogin() {
			this.isModalLoginVisible = true;
			this.modalLoginState = 'default';
		},
		openModalCheckin() {
			this.isModalCheckin = true;
		},
		openModalMobilePrice() {
			this.isModalMobilePrice = true;
		},

		// CLOSE MODAL
		closeModalPromo() {
			this.isModalPromo = false;
		},
		closeModalLogin() {
			this.isModalLoginVisible = false;
			this.modalLoginState = 'default';
		},
		closeModalClaim() {
			this.isModalClaim = false;
		},
		closeModalCheckin() {
			this.isModalCheckin = false;
		},
		closeModalMobilePrice() {
			this.isModalMobilePrice = false;
		},
		closeModalBooking() {
			this.isModalBooking = false;
		},
		closeModalGenderMismatch() {
			this.isModalGenderMismatch = false;
		},
		closeModalGenderNull() {
			this.isModalGenderNull = false;
		},
		closePriceInfoModal() {
			this.priceInfoModal = {
				show: false,
				info: '',
				items: []
			};
		},
		trackBookingClick({ redirectionSource = 'property detail page' }) {
			const redirectionSourceArr = [
				'riwayat kos',
				'baru dilihat',
				'riwayat booking',
				'booking form'
			];

			if (
				~redirectionSourceArr.indexOf(this.getQueryString('redirection_source'))
			) {
				redirectionSource = this.getQueryString('redirection_source');
			}

			tracker('moe', [
				'[User] Booking - Click Booking',
				{
					property_type: this.detailTypeName || 'kost',
					property_id: +this.seq,
					property_city: _get(this.detail, 'area_city', null),
					property_subdistrict: _get(this.detail, 'area_subdistrict', null),
					property_name: _get(this.detail, 'room_title', null),
					property_monthly_price: _get(this.detail, 'price_monthly', null)
						? parseInt(this.detail.price_monthly)
						: null,
					tenant_name: _get(this.authData, 'all.name', null),
					source_page: redirectionSource,
					redirection_source: redirectionSource,
					is_mamirooms: this.isMamirooms,
					is_premium: this.isPremiumOwner,
					goldplus_status: _get(this.detail, 'goldplus_status', null),
					is_money_back_guarantee: this.isMoneyBackGuarantee,
					property_checker: this.isVerified,
					is_promoted: _get(this.detail, 'is_promoted', false),
					is_promo_ngebut: _get(this.detail, 'is_flash_sale', false),
					owner_id: _get(this.detail, 'owner_id', false),
					interface: this.deviceType,
					is_owner: false,
					is_user_testing: _get(this.$store, 'state.isCheckinDateFirst', false)
				}
			]);
		},
		trackBookingFormVisited(redirectionSource) {
			let redirectionSourceTracker =
				window.innerWidth < 992
					? 'price property detail page'
					: 'property detail page';

			if (redirectionSource) {
				redirectionSourceTracker = redirectionSource;
			}

			tracker('moe', [
				'[User] Booking Form Visited',
				{
					interface: this.deviceType,
					user_id: _get(this.authData, 'all.id', null),
					property_type: 'kost',
					property_name: _get(this.detail, 'room_title', null),
					property_updated_at: new Date(this.detail.updated_at),
					tenant_gender:
						_get(this.authData, 'all.gender') === 'female'
							? 'Perempuan'
							: 'Laki-Laki',
					property_id: this.seq,
					property_city: _get(this.detail, 'area_city', null),
					property_subdistrict: _get(this.detail, 'area_subdistrict', null),
					redirection_source: redirectionSourceTracker,
					goldplus_status: this.kostLevelInfoName,
					is_user_testing: _get(this.$store, 'state.isCheckinDateFirst', false)
				}
			]);
		},
		trackGenderMismatch() {
			tracker('moe', [
				'[User] Internal In-App Shown',
				{
					property_id: _get(this.detail, 'seq', null),
					tenant_gender:
						_get(this.authData, 'all.gender') === 'female'
							? 'Perempuan'
							: 'Laki-Laki',
					property_gender:
						_get(this.detail, 'gender') === 1 ? 'Kost Pria' : 'Kost Wanita',
					property_name: _get(this.detail, 'room_title', null),
					interface: this.deviceType,
					user_id: _get(this.authData, 'all.id', null)
				}
			]);
		},
		goToGoldPlusLanding() {
			const goldPlusLandingUrl = process.env.MIX_GOLDPLUS_LANDING_URL;

			if (goldPlusLandingUrl) {
				window.open(goldPlusLandingUrl);
			}
		}
	}
};

export default mixinContainerKost;
