const bookingInputDate = require('../../_booking/request/components/booking-date/BookingInputDate.vue')
	.default;
const bookingTenantData = () =>
	import(
		/* webpackChunkName: "detail_BookingTenantData" */
		'../../_booking/request/components/booking-tenant-info/BookingTenantData.vue'
	);
const bookingSummary = () =>
	import(
		/* webpackChunkName: "detail_BookingSummary" */
		'../../_booking/request/components/booking-summary/BookingSummary.vue'
	);
const bookingInputId = () =>
	import(
		/* webpackChunkName: "detail_BookingInputId" */
		'../../_booking/request/components/booking-id-verification/BookingInputId.vue'
	);
const bookingInputCamera = () =>
	import(
		/* webpackChunkName: "detail_BookingInputCamera" */
		'../../_booking/request/components/booking-id-verification/BookingInputCamera.vue'
	);

let routes = [
	{
		path: '/date',
		name: 'booking-date',
		component: bookingInputDate
	},
	{
		path: '/tenant-info',
		name: 'tenant-info',
		component: bookingTenantData
	},
	{
		path: '/booking-summary',
		name: 'booking-summary',
		component: bookingSummary
	},
	{
		path: '/id-verification',
		name: 'id-verification',
		component: bookingInputId,
		meta: {
			child: true
		}
	},
	{
		path: '/snap',
		name: 'snap',
		component: bookingInputCamera,
		meta: {
			child: true
		}
	}
];

routes = routes.map(route => {
	return {
		...route,
		meta: {
			...route.meta,
			booking_request: true
		}
	};
});

export default routes;
