/* eslint-disable no-undef */
import VueRouter from 'vue-router';
import bookingRequestRoutes from './bookingRequestRoutes';
import store from '../stores/index';

Vue.use(VueRouter);

const routerOptions = {
	base: `/room/${detail.slug}`
};

if (detail.is_booking) {
	routerOptions.routes = bookingRequestRoutes;
}

const router = new VueRouter(routerOptions);

if (detail.is_booking) {
	router.beforeEach((to, from, next) => {
		if (!store.state.showBookingRequestProcess && to.path !== '/') {
			next({ path: '/' });
		} else if (to.path === '/' && store.state.showBookingRequestProcess) {
			store.commit('setShowModalCancelBooking', true);
			next(false);
		} else {
			next();
		}
	});
}

export default router;
