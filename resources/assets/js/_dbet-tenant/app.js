import store from './store';
import router from './router';
import App from './components/App.vue';

import 'Js/@mixins/MixinNavigatorIsMobile';
import 'Js/@mixins/MixinSwalLoading';
import 'Js/@mixins/MixinSwalSuccessError';
import mixinAuth from 'Js/@mixins/MixinAuth';

import 'bangul-vue/dist/bangul.css';

Vue.component('app', App);

// eslint-disable-next-line no-unused-vars
const app = new Vue({
	el: '#app',
	mixins: [mixinAuth],
	store,
	router,
	mounted() {
		try {
			document.getElementById('preloading').remove();
		} catch (e) {
			document.getElementById('preloading').style.display = 'none';
		}
	}
});
