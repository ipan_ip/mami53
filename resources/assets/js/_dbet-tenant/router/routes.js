const DbetTenantOnboarding = () => import('../components/DbetTenantOnboarding');
const DbetTenantForm = () =>
	import('../components/DbetTenantForm/DbetTenantForm');
const DbetTenantFormBiodata = () =>
	import('../components/DbetTenantForm/DbetTenantFormBiodata');
const DbetTenantSuccess = () => import('../components/DbetTenantSuccess');

const routes = [
	{
		path: '/',
		name: 'main',
		redirect: { name: 'onboarding' },
		meta: {
			title: 'Data penyewa - Yuk isi data kamu!'
		}
	},
	{
		path: '/success',
		name: 'success',
		component: DbetTenantSuccess,
		meta: {
			title: 'Data penyewa - Terima kasih sudah mengisi data!',
			noBack: true,
			isAuthRequired: true
		}
	},
	{
		path: '/onboarding',
		name: 'onboarding',
		component: DbetTenantOnboarding,
		meta: {
			title: 'Data penyewa - Pemilik kos butuh bantuan kamu isi data!',
			noBack: true
		}
	},
	{
		path: '/form',
		name: 'form',
		component: DbetTenantForm,
		meta: {
			title: 'Data penyewa - Yuk isi data kamu',
			isAuthRequired: true,
			width: 'md'
		}
	},
	{
		path: '/biodata',
		name: 'biodata',
		component: DbetTenantFormBiodata,
		meta: {
			title: 'Data penyewa - Yuk isi data kamu!',
			isAuthRequired: true,
			isCloseTypeNav: true,
			isFormChildren: true,
			width: 'sm'
		}
	},
	{
		path: '*',
		name: 'other',
		redirect: { name: 'main' }
	}
];

export default routes;
