/* eslint-disable no-undef */
import VueRouter from 'vue-router';
import routes from './routes';
import store from '../store';

const ownerDashboard = oxWebUrl;

Vue.use(VueRouter);

const router = new VueRouter({
	mode: 'history',
	base: `/ob/dbet-${dbetCode}`,
	routes,
	scrollBehavior() {
		return { x: 0, y: 0 };
	}
});

router.beforeResolve((to, _, next) => {
	document.title = to.meta.title || 'Data penyewa - Yuk isi data kamu!';
	const authCheck = store.state.authCheck;

	if (to.meta.isAuthRequired) {
		if (authCheck && authCheck.owner) {
			window.location.replace(ownerDashboard);
		} else if (!authCheck || !authCheck.user) {
			window.location.replace(
				`/ob/${store.state.dbet.dbetCodeFormatted}${to.fullPath}`
			);
		} else if (to.meta.isFormChildren && !store.state.isFormVisited) {
			next({ name: 'form', replace: true });
		} else {
			next();
		}
	} else {
		next();
	}
});

export default router;
