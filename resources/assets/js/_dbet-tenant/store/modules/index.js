import room from './room';
import user from './user';
import saved from './saved';
import dbet from './dbet';

export default {
	room,
	user,
	saved,
	dbet
};
