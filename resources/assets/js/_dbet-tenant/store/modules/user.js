export default {
	namespaced: true,
	state: () => ({
		id: 0,
		name: '',
		phone: '',
		gender: '',
		job: '',
		work_place: '',
		other_work_place: '',
		identity_id: 0,
		identity: null
	}),
	getters: {
		workPlace: state => {
			if (state.work_place === 'Lainnya' || state.other_work_place) {
				return state.other_work_place;
			}
			return state.work_place;
		},
		userJobAlias: ({ job }) => {
			const jobMap = {
				kuliah: 'Mahasiswa',
				kerja: 'Karyawan',
				lainnya: 'Lainnya'
			};

			return jobMap[(job || '').toLowerCase()] || '';
		},
		generatedData: (state, { workPlace }) => {
			return {
				fullname: (state.name || '').trim(),
				gender: state.gender,
				phone: state.phone,
				job: state.job,
				work_place: workPlace,
				identity_id: state.identity_id || null
			};
		},
		identityIdUrl: state => {
			const availableUrl =
				state.identity &&
				(state.identity.medium || state.identity.large || state.identity.small);

			return (state.identity && availableUrl) || '';
		}
	},
	mutations: {
		setUserState: (state, payload) => {
			Object.keys(payload).forEach(key => {
				state[key] = payload[key];
			});
		}
	}
};
