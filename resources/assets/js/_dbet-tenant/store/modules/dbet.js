export default {
	namespaced: true,
	state: () => ({
		id: 0,
		dbetCode: '',
		dbetCodeFormatted: '',
		isRequiredIdentity: false,
		isRequiredDueDate: false,
		dueDate: 0,
		designerRoomId: 0,
		designerRoomName: '',
		designerRoomFloor: '',
		selectedAdditionalIds: [],
		promoContent: '',
		privacyLink: '',
		isLoadedPromo: false,
		price: 0,
		rentType: '',
		rentTypeCustom: ''
	}),
	getters: {
		selectedRentType: ({ rentType, rentTypeCustom }) => {
			return rentType === 'custom' ? rentTypeCustom : rentType;
		},
		generatedData: (
			{ id, dueDate, price, designerRoomId },
			{ selectedRentType }
		) => {
			return {
				dbet_link_id: id,
				due_date: dueDate,
				rent_count_type: selectedRentType,
				designer_room_id: designerRoomId,
				price
			};
		},
		selectedRoomName: ({ designerRoomName, designerRoomFloor }) => {
			let roomName = designerRoomName || '';
			if (designerRoomFloor) {
				roomName += ` ${designerRoomFloor}`;
			}
			return (roomName || '').trim();
		}
	},
	mutations: {
		setSelectedAdditionalIds: (state, ids) => {
			state.selectedAdditionalIds = [...ids];
		},
		setSelectedRoomId: (state, { id, name, floor }) => {
			state.designerRoomId = id;
			state.designerRoomName = name;
			state.designerRoomFloor = floor;
		},
		setDbetCodeFormatted: (state, codeFormatted) => {
			state.dbetCodeFormatted = codeFormatted;
		},
		setPromo: (state, promo) => {
			state.promoContent = promo.content;
			state.privacyLink = promo.privacyLink;
		},
		setLoadedPromo: (state, isLoaded) => {
			state.isLoadedPromo = isLoaded;
		},
		setRentData: (state, rent) => {
			state.rentType = rent.rentType;
			state.rentTypeCustom = rent.rentTypeCustom;
			state.price = rent.price;
		},
		setDueDate: (state, dueDate) => {
			state.dueDate = dueDate;
		},
		setDbetState: (state, payload) => {
			Object.keys(payload).forEach(key => {
				state[key] = payload[key];
			});
		}
	}
};
