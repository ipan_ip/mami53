export default {
	namespaced: true,
	state: () => ({
		designerId: 0,
		isLoadedRoomUnits: false,
		isMamirooms: false,
		levelInfo: null,
		roomName: '',
		roomCity: '',
		roomProvince: '',
		roomDistrict: '',
		roomGender: 0,
		roomPrices: [],
		roomUnits: [],
		roomType: 'kost',
		roomLevel: '',
		roomPhoto: {}
	}),
	getters: {
		rentPricesObj: ({ roomPrices }) => {
			const rentPrices = {};
			roomPrices.forEach(price => {
				rentPrices[price.type] = { ...price };
			});
			return rentPrices;
		},
		roomPhotoSmall: ({ roomPhoto }) => {
			return (roomPhoto && roomPhoto.small) || '';
		},
		roomGenderDisabled: ({ roomGender }) => {
			return roomGender === 1 ? 'female' : roomGender === 2 ? 'male' : '';
		}
	},
	mutations: {
		setRoomState: (state, payload) => {
			Object.keys(payload).forEach(key => {
				state[key] = payload[key];
			});
		},
		setRoomUnits: (state, units) => {
			state.roomUnits = units;
		},
		setLoadedRoomUnits: (state, isLoaded) => {
			state.isLoadedRoomUnits = isLoaded;
		}
	}
};
