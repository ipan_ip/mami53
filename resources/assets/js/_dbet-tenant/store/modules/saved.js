export default {
	namespaced: true,
	state: () => ({
		savedUser: false,
		savedRoomNumber: false,
		savedDueDate: false,
		savedRentPrice: false,
		savedAdditionalPrice: false
	}),
	mutations: {
		setSavedState: (state, payload) => {
			Object.keys(payload).forEach(key => {
				state[key] = payload[key];
			});
		}
	}
};
