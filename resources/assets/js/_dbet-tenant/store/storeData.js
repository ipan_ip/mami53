import _get from 'lodash/get';
import _filter from 'lodash/filter';
import _map from 'lodash/map';
import modules from './modules/index';

export default {
	modules,
	state: () => ({
		authCheck: {},
		authData: {},
		isFormVisited: false,
		isOnboardingFinished: false
	}),
	getters: {
		selectedAdditionals: ({ dbet }, { additionalPrices }) => {
			return _filter(additionalPrices, price => {
				return dbet.selectedAdditionalIds.includes(price.id);
			});
		},
		bookingRequestParams: (_, getters) => {
			const additionalPrice = _map(getters.selectedAdditionals, price => {
				return {
					name: price.name,
					price: price.price
				};
			});

			return {
				...getters['user/generatedData'],
				...getters['dbet/generatedData'],
				additional_price: additionalPrice
			};
		},
		isTenant: state => {
			return state.authCheck && state.authCheck.user;
		},
		isOwner: state => {
			return state.authCheck && state.authCheck.owner;
		},
		additionalPrices: (_, getters) => {
			const rentPrices = getters['room/rentPricesObj'];
			const rentType = getters['dbet/selectedRentType'];

			const defaultRentType = rentPrices.month
				? 'month'
				: Object.keys(rentPrices)[0];

			return rentType && rentPrices[rentType]
				? rentPrices[rentType].additionals || []
				: defaultRentType && rentPrices[defaultRentType]
				? rentPrices[defaultRentType].additionals || []
				: [];
		}
	},
	mutations: {
		setAuthCheck: (state, payload) => {
			state.authCheck = payload;
		},
		setAuthData: (state, payload) => {
			state.authData = payload;
		},
		setFormVisitedState: (state, payload) => {
			state.isFormVisited = payload;
		},
		setOnboardingFinishedState: (state, isFinished) => {
			state.isOnboardingFinished = isFinished;
		}
	},
	actions: {
		initData: ({ commit }, payload) => {
			const getData = (key, defaultVal = '') => {
				return _get(payload, key, defaultVal) || defaultVal;
			};

			commit('room/setRoomState', {
				designerId: getData('designer_id', 0),
				isMamirooms: getData('is_mamirooms', false),
				roomName: getData('room_name'),
				roomPhoto: getData('room_photo', null),
				roomGender: getData('room_gender'),
				roomCity: getData('room_city'),
				roomProvince: getData('room_province'),
				roomDistrict: getData('room_district'),
				roomPrices: getData('room_price', []),
				roomLevel: getData('level_info', {}),
				roomType: getData('property_type', 'kost')
			});

			commit('dbet/setDbetState', {
				id: getData('id'),
				dbetCode: getData('code'),
				isRequiredIdentity: !!getData('is_required_identity', false),
				isRequiredDueDate: !!getData('is_required_due_date', false),
				dueDate: getData('due_date', 0)
			});

			commit('user/setUserState', getData('user', {}));
		}
	}
};
