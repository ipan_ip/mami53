import Vuex from 'vuex';
import storeData from './storeData';

Vue.use(Vuex);

export default new Vuex.Store(storeData);
