/**
 * A Function to round to a Certain Number of Decimal Places in JavaScript.
 *
 * @param {Number} number The number that needs to be rounded
 * @param {Number} decimalPlaces Define how many digit that should be exist in decimal
 *
 * @return {Number} Result
 *
 * References:
 * - https://medium.com/swlh/how-to-round-to-a-certain-number-of-decimal-places-in-javascript-ed74c471c1b8
 * - https://www.jacklmoore.com/notes/rounding-in-javascript/
 */
export const round = (number, decimalPlaces = 0) => {
	return Number(
		Math.round(number + 'e' + decimalPlaces) + 'e-' + decimalPlaces
	);
};
