export const getParams = str => {
	let queryString = str || window.location.search || '';
	let keyValPairs = [];
	const params = {};
	queryString = decodeURIComponent(queryString);
	if (queryString.indexOf('?') === -1) return params;

	queryString = queryString.replace(/.*?\?/, '');
	if (queryString.length) {
		keyValPairs = queryString.split('&');
		for (const pairNum in keyValPairs) {
			const key = keyValPairs[pairNum].split('=')[0];
			if (!key.length) continue;
			if (typeof params[key] === 'undefined') params[key] = [];
			params[key].push(keyValPairs[pairNum].split('=')[1]);
		}
	}
	return params;
};

export const appendParams = (newUrl, valuePairs, changeOnTheFly = false) => {
	let currentParams = getParams(newUrl);
	newUrl = newUrl.split('?')[0];
	currentParams = {
		...currentParams,
		...valuePairs
	};
	let paramString = '';
	const arrValPairs = Object.entries(currentParams);
	for (const [index, value] of arrValPairs.entries()) {
		const key = value[0];
		const keyValue = value[1];
		if (
			(key === 'tag' || key === 'gender' || key === 'goldplus') &&
			!keyValue
		) {
			paramString = paramString.substring(0, +paramString.length - 1);
			if (index < arrValPairs.length - 1 && index > 0) paramString += '&';
			continue;
		}
		paramString += `${key}=${keyValue}`;
		if (index < arrValPairs.length - 1) paramString += '&';
	}
	paramString = `?${paramString}`;
	const generatedUrl = `${newUrl}${paramString}`;
	return changeOnTheFly
		? window.history.pushState(null, null, generatedUrl)
		: generatedUrl;
};
