export const validateEmail = email => {
	const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return regex.test(String(email).toLowerCase());
};

export const isNumberOnly = value => {
	const regex = /^[0-9]*$/;
	return regex.test(value);
};

export const isContainingSpecialCharacters = value => {
	const regex = /[!@#$%^&*(),.?":{}|<>]/g;
	return regex.test(value);
};

export const isContainingEmoji = value => {
	const regex = /(\u00a9|\u00ae|[\u2000-\u3300]|\ud83c[\ud000-\udfff]|\ud83d[\ud000-\udfff]|\ud83e[\ud000-\udfff])/g;
	return regex.test(value);
};
