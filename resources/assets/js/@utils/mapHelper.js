const toRad = n => {
	return (n * Math.PI) / 180;
};

const toDeg = n => {
	return (n * 180) / Math.PI;
};

export const expandCorner = (lat1, lon1, brng, dist) => {
	const a = 6378137;
	const b = 6356752.3142;
	const f = 1 / 298.257223563; // WGS-84 ellipsiod
	const s = dist;
	const alpha1 = toRad(brng);
	const sinAlpha1 = Math.sin(alpha1);
	const cosAlpha1 = Math.cos(alpha1);
	const tanU1 = (1 - f) * Math.tan(toRad(lat1));
	const cosU1 = 1 / Math.sqrt(1 + tanU1 * tanU1);
	const sinU1 = tanU1 * cosU1;
	const sigma1 = Math.atan2(tanU1, cosAlpha1);
	const sinAlpha = cosU1 * sinAlpha1;
	const cosSqAlpha = 1 - sinAlpha * sinAlpha;
	const uSq = (cosSqAlpha * (a * a - b * b)) / (b * b);
	const A = 1 + (uSq / 16384) * (4096 + uSq * (-768 + uSq * (320 - 175 * uSq)));
	const B = (uSq / 1024) * (256 + uSq * (-128 + uSq * (74 - 47 * uSq)));
	let sigma = s / (b * A);
	let sigmaP = 2 * Math.PI;
	while (Math.abs(sigma - sigmaP) > 1e-12) {
		var cos2SigmaM = Math.cos(2 * sigma1 + sigma);
		var sinSigma = Math.sin(sigma);
		var cosSigma = Math.cos(sigma);
		const deltaSigma =
			B *
			sinSigma *
			(cos2SigmaM +
				(B / 4) *
					(cosSigma * (-1 + 2 * cos2SigmaM * cos2SigmaM) -
						(B / 6) *
							cos2SigmaM *
							(-3 + 4 * sinSigma * sinSigma) *
							(-3 + 4 * cos2SigmaM * cos2SigmaM)));
		sigmaP = sigma;
		sigma = s / (b * A) + deltaSigma;
	}
	const tmp = sinU1 * sinSigma - cosU1 * cosSigma * cosAlpha1;
	const lat2 = Math.atan2(
		sinU1 * cosSigma + cosU1 * sinSigma * cosAlpha1,
		(1 - f) * Math.sqrt(sinAlpha * sinAlpha + tmp * tmp)
	);
	const lambda = Math.atan2(
		sinSigma * sinAlpha1,
		cosU1 * cosSigma - sinU1 * sinSigma * cosAlpha1
	);
	const C = (f / 16) * cosSqAlpha * (4 + f * (4 - 3 * cosSqAlpha));
	const L =
		lambda -
		(1 - C) *
			f *
			sinAlpha *
			(sigma +
				C *
					sinSigma *
					(cos2SigmaM + C * cosSigma * (-1 + 2 * cos2SigmaM * cos2SigmaM)));
	/* eslint-disable-next-line */
	const revAz = Math.atan2(sinAlpha, -tmp); // final bearing
	return [lon1 + toDeg(L), toDeg(lat2)];
};
