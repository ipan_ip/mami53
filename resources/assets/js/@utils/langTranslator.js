import { AES } from 'crypto-es/lib/aes.js';
import { Utf8 } from 'crypto-es/lib/core.js';

const lang = 'MzljODUyZDBkMGJjNDJlZjgzZjdkM2Q3MDhmNDIzNjg=';
const phrase = 'NWRmNWExMGViYjAzNTA5Nw==';

export const translateWords = input => {
	if (!input) return;

	const output = AES.decrypt(input, Utf8.parse(atob(lang)), {
		iv: Utf8.parse(atob(phrase))
	});

	const words = JSON.parse(output.toString(Utf8));

	return words;
};
