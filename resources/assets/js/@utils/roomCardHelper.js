export const renderTagRibbonProps = (
	apartemenId,
	isGoldplus,
	availableRoom
) => {
	const isKost = apartemenId === 0;
	const isKosAndalan = isGoldplus;
	const kosAndalanTitle = process.env.MIX_GOLDPLUS_TITLE || 'Kos Andalan';

	if (
		(isKosAndalan && isKost && availableRoom > 0) ||
		(isKosAndalan && isKost && availableRoom === 0)
	) {
		return {
			text: kosAndalanTitle,
			variant: 'black'
		};
	} else if (
		!isKosAndalan &&
		isKost &&
		availableRoom > 0 &&
		availableRoom <= 3
	) {
		return {
			text: `Sisa ${availableRoom} Kamar`,
			'text-only': true,
			variant: 'red'
		};
	} else if (!isKost && availableRoom > 0) {
		return {
			text: 'Apartemen',
			variant: 'black'
		};
	} else if (!isKost && availableRoom === 0) {
		return {
			text: 'Apartemen Penuh',
			variant: 'black'
		};
	} else {
		return {};
	}
};

export const generateRoomData = (rooms, horizontalCard, isMobile) => {
	if (rooms.length > 0) {
		return rooms.map(room => {
			let genderProperties = {};
			let topFacilities = '';
			const havePromo = Boolean(room.promo_title);
			const haveDiscount = Boolean(room.price_title_format.discount);
			const haveOtherDiscount =
				room.other_discounts && room.other_discounts.length > 0;
			const haveFlashSale = Boolean(room.is_flash_sale);

			if (room.apartment_project_id === 0) {
				const genderLabels = ['Campur', 'Putra', 'Putri'];
				const text = genderLabels[room.gender];
				genderProperties = {
					variant: 'black',
					text
				};
			}

			if (room.top_facility && room.top_facility.length > 0) {
				topFacilities = room.top_facility.join(' · ');
			}

			const favoriteCondition =
				!horizontalCard || (horizontalCard && !isMobile);

			const promotionCondition =
				!horizontalCard || (horizontalCard && !isMobile);

			const promotionName = haveFlashSale
				? 'Promo'
				: haveDiscount || haveOtherDiscount
				? 'Diskon'
				: havePromo
				? 'Promo'
				: '';

			const tagRibbonProperties = renderTagRibbonProps(
				room.apartment_project_id,
				Boolean(room.goldplus),
				room.available_room
			);

			const priceLabel = `${room.price_title_format.currency_symbol} ${
				room.price_title_format.discount_price
					? room.price_title_format.discount_price
					: room.price_title_format.price
			} / ${room.price_title_format.rent_type_unit}`;

			const otherDiscountLabel =
				room.other_discounts && room.other_discounts.length > 0
					? `Diskon ${room.other_discounts[0].discount} untuk sewa ${room.other_discounts[0].rent_type_unit}an`
					: '';

			const goldplusStatus =
				room.goldplus && room.apartment_project_id === 0
					? Boolean(room.goldplus)
					: false;
			const goldplusName = process.env.MIX_GOLDPLUS_TITLE || 'Kos Andalan';
			const priceLabelOnly = `${room.price_title_format.currency_symbol} ${
				room.price_title_format.discount_price
					? room.price_title_format.discount_price
					: room.price_title_format.price
			}`;
			const rentTypeUnit = `/ ${room.price_title_format.rent_type_unit}`;

			return {
				...room,
				floor: parseInt(room.floor),
				rating: room.rating ? parseFloat(room.rating).toFixed(1) : '',
				top_facilitiy: topFacilities,
				isGoldplus: goldplusStatus,
				goldplusName: goldplusName,
				subdistrict: room.subdistrict,
				isKost: room.apartment_project_id === 0,
				roomType: room.apartment_project_id === 0 ? 'kost' : 'apartemen',
				otherDiscountLabel,
				priceLabel,
				genderProperties,
				favoriteCondition,
				promotionCondition,
				rentTypeUnit,
				priceLabelOnly,
				promotionName,
				tagRibbonProperties
			};
		});
	} else {
		return [];
	}
};

export const createSetOfData = (rooms, groupingNumber, isMobile) => {
	const mainData = [];
	let newSetData = [];
	rooms.map((data, index) => {
		newSetData.push(data);
		if (
			newSetData.length === groupingNumber ||
			index + 1 === rooms.length ||
			isMobile
		) {
			mainData.push({
				index: mainData.length + 1,
				data: newSetData
			});
			newSetData = [];
		}
	});
	return mainData;
};
