import * as storage from 'Js/@utils/storage';

export const isExistInStorage = flashSaleId => {
	const flashSaleStorage = JSON.parse(storage.local.getItem('flashsale-store'));
	return flashSaleId
		? flashSaleStorage
			? flashSaleStorage.ids.find(id => id === flashSaleId)
			: false
		: true;
};

export const setFTUEStorage = flashSaleId => {
	const flashSaleStorage = JSON.parse(storage.local.getItem('flashsale-store'));
	const newStorageData = {
		ids: flashSaleStorage
			? [...flashSaleStorage.ids, flashSaleId]
			: [flashSaleId]
	};
	storage.local.setItem('flashsale-store', JSON.stringify(newStorageData));
};
