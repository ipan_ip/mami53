export const makeAPICall = request => {
	return axios(request)
		.then(response => {
			if (response && response.status) {
				if (response.status === 200 && response.data) {
					return response.data;
				} else {
					bugsnagClient.notify(`Error status: ${response.status}`);
				}
			}

			return null;
		})
		.catch(error => {
			bugsnagClient.notify(error);

			return null;
		});
};
