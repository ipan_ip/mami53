export function formatCurrency(
	val,
	separator = '.',
	hideSymbol = false,
	symbol = 'Rp'
) {
	let prefix = hideSymbol ? '' : symbol;
	if (val < 0) {
		prefix = `-${prefix}`;
	}
	const formatted = (val / 1).toFixed(0).replace('.', '');
	return `${prefix}${formatted
		.toString()
		.replace(/\B(?=(\d{3})+(?!\d))/g, separator)}`;
}
