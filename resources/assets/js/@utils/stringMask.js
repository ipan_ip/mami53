/**
 * This method can be used to mask a string value using custom character.
 * (eg. 0812345678 -> ********)
 *
 * @param {String} value The string value that will be masked.
 * @param {Number} shownValueCount The amount of characters of given string
 * value that will be left shown (descending).
 * @param {RegExp} pattern Regular Expression pattern for custom masking
 * pattern.
 * @param {String} maskChar The character that will be used for masking.
 *
 * @return {String} Masked string value.
 */
export const stringMask = (
	value,
	shownValueCount = 0,
	pattern,
	maskChar = '*'
) => {
	if (pattern) return value.replace(pattern, maskChar);
	else {
		const amount = value.length - shownValueCount;
		const repeatedMaskChar = maskChar.repeat(amount);
		const maskedIndexes = value.slice(0, amount);
		const maskedValue = maskedIndexes.replace(maskedIndexes, repeatedMaskChar);
		const shownValue =
			shownValueCount > 0 ? value.slice(-Math.abs(shownValueCount)) : '';

		return maskedValue + shownValue;
	}
};
