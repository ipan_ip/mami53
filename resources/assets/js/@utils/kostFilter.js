export const rentTypes = ['Harian', 'Mingguan', 'Bulanan', 'Tahunan'];

export const genderLabels = ['Campur', 'Putra', 'Putri'];

export const genderTypes = {
	'0': 'Campur',
	'1': 'Khusus Putra',
	'2': 'Khusus Putri',
	'0,1': 'Putra dan Campur',
	'0,1,2': 'Semua',
	'0,2': 'Putri dan Campur',
	'1,2': 'Putri atau Putra'
};

export const filterSorts = {
	'price,-': 'Rekomendasi',
	'price,asc': 'Harga Termurah',
	'price,desc': 'Harga Tertinggi',
	'availability,desc': 'Kosong ke Penuh',
	'last_update,desc': 'Update Terbaru'
};

export const propertyRentTypes = {
	daily: 'Harian',
	weekly: 'Mingguan',
	monthly: 'Bulanan',
	quarterly: '3 Bulanan',
	semiannually: '6 Bulanan',
	yearly: 'Tahunan'
};

export const genderOptions = [
	{
		value: 1,
		name: 'Putra'
	},
	{
		value: 2,
		name: 'Putri'
	},
	{
		value: 0,
		name: 'Campur'
	}
];

export const sortingOptions = [
	{
		value: ['price', '-'],
		name: 'Rekomendasi'
	},
	{
		value: ['price', 'asc'],
		name: 'Harga termurah'
	},
	{
		value: ['price', 'desc'],
		name: 'Harga termahal'
	}
];

export const reservedKeys = [
	'gender',
	'rent',
	'sort',
	'price',
	'booking',
	'mamichecker',
	'singgahsini',
	'tag',
	'goldplus'
];

export const reservedValues = {
	sort: ['price,-', 'price,asc', 'price,desc'],
	booking: ['0', '1'],
	mamichecker: ['0', '1'],
	singgahsini: ['0', '1']
};

export const sanitizeFilterTag = filterData => {
	return filterData.map(filter => {
		return {
			...filter,
			value: filter.fac_id,
			name: filter.fac_name
		};
	});
};

export const reviewSortingOptions = [
	{
		name: 'Review terbaru',
		value: 'new'
	},
	{
		name: 'Review terlama',
		value: 'last'
	},
	{
		name: 'Rating tertinggi',
		value: 'best'
	},
	{
		name: 'Rating terendah',
		value: 'bad'
	}
];
