/**
 * This method can be used for converting an image Data URI to a server-friendly
 * formatted file data.
 *
 * @param {base64} imageData base64 image Data URI usually obtained from an
 * Image object's 'src' property.
 *
 * @return {binary} Image file data.
 */
export const convertToFile = imageData => {
	const binary = atob(imageData.split(',')[1]);
	const array = [];

	for (let i = 0; i < binary.length; i++) {
		array.push(binary.charCodeAt(i));
	}

	const blobData = new Blob([new Uint8Array(array)], {
		type: 'image/png'
	});

	return new File([blobData], 'image.png', {
		type: 'image/png'
	});
};
