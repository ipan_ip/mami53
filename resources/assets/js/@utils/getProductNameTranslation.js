export const getProductNameTranslation = productType => {
	if (productType === 'kos') {
		return 'Kos';
	} else if (productType === 'apartment') {
		return 'Apartmen';
	} else if (productType === 'vacancy') {
		return 'Pekerjaan';
	} else {
		return 'Kos';
	}
};
