// import LoginUser from './components/LoginUser.vue';
const LoginUserJquery = () => import('./components/LoginUserJquery.vue');

Vue.config.productionTip = false;

new Vue({
	el: '#loginUserJquery',
	components: { LoginUserJquery }
});
