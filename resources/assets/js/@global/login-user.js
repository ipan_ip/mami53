const LoginUser = () =>
	import(
		/* webpackChunkName: "detail_LoginUser" */ './components/LoginUser.vue'
	);

Vue.config.productionTip = false;

new Vue({
	el: '#loginUser',
	components: { LoginUser }
});
