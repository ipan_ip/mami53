/**
 * type:
 * - internal: reachable with vue-router
 * - external: unreachable with vue-route
 * - pms: to pms (owner.mamikos.com) page
 * - action: call javascript function
 */
export default [
	{
		name: 'Home',
		type: 'pms',
		data: '/',
		urlNames: ['pms'],
		icon: 'home'
	},
	{
		name: 'Properti Saya',
		id: 'kosProperty',
		icon: 'kos-marketing',
		children: [
			{
				name: 'Kos',
				type: 'action',
				urlNames: ['kos'],
				data(ctx) {
					ctx.sendSidebarODTracker('[Owner] OD Properti Saya - Kos Clicked');
					if (ctx.hasSameBaseWithCurrent('/ownerpage')) {
						ctx.$router.push({ path: '/kos' });
					} else {
						window.location.href = '/ownerpage/kos';
					}
				}
			},
			{
				name: 'Apartemen',
				type: 'action',
				urlNames: ['apartment', 'apartment_update', 'apartment_promo'],
				data(ctx) {
					ctx.sendSidebarODTracker(
						'[Owner] OD Properti Saya - Apartemen Clicked'
					);
					if (ctx.hasSameBaseWithCurrent('/ownerpage')) {
						ctx.$router.push({ path: '/apartment' });
					} else {
						window.location.href = '/ownerpage/apartment';
					}
				}
			},
			{
				name: 'Pekerjaan',
				type: 'action',
				urlNames: [
					'vacancy',
					'vacancy_detail',
					'vacancy_detail_report',
					'vacancy_detail_report_number'
				],
				data(ctx) {
					ctx.sendSidebarODTracker(
						'[Owner] OD Properti Saya - Pekerjaan Clicked'
					);
					if (ctx.hasSameBaseWithCurrent('/ownerpage')) {
						ctx.$router.push({ path: '/vacancy' });
					} else {
						window.location.href = '/ownerpage/vacancy';
					}
				}
			}
		]
	},
	{
		name: 'Pemasaran Kos',
		id: 'kosMarketing',
		icon: 'promote',
		children: [
			{
				name: 'Premium',
				type: 'pms',
				data: '/premium-dashboard',
				urlNames: ['pms']
			}
		]
	},
	{
		name: 'Manajemen Kos',
		id: 'kosManagement',
		icon: 'kos-management',
		isHiddenFn(context) {
			return context.isNewOwner;
		},
		children: [
			{
				name: 'Pengajuan Booking',
				type: 'action',
				urlNames: ['manageBooking'],
				base: '/ownerpage/manage',
				data(context) {
					context.openBookingOwner();
				}
			},
			{
				name: 'Laporan Keuangan',
				urlNames: ['finance-report'],
				type: 'action',
				data(ctx) {
					ctx.sendSidebarODTracker('[Owner] OD - Laporan Keuangan Clicked', {
						redirection_source: 'tab_manajemen_kos'
					});

					const destination = ctx.isBbk
						? `${ctx.ownerDashboardUrl}/financial-report`
						: `${ctx.ownerDashboardUrl}/kos/booking/register`;

					setTimeout(() => {
						window.location.href = destination;
					}, 1000);
				}
			},
			{
				name: 'Kelola Tagihan',
				type: 'action',
				urlNames: ['billing-management'],
				data(ctx) {
					ctx.sendSidebarODTracker('[Owner] OD - Kelola Tagihan Clicked', {
						redirection_source: 'tab_manajemen_kos'
					});

					setTimeout(() => {
						window.location.href = `${ctx.ownerDashboardUrl}/billing-management`;
					}, 1000);
				}
			},
			{
				name: 'Penyewa',
				type: 'action',
				urlNames: ['tenant-list'],
				data(ctx) {
					ctx.sendSidebarODTracker('[Owner] OD - Penyewa Clicked', {
						redirection_source: 'tab_manajemen_kos'
					});

					const destination = ctx.isBbk
						? `${ctx.ownerDashboardUrl}/tenant-list`
						: `${ctx.ownerDashboardUrl}/kos/booking/register`;

					setTimeout(() => {
						window.location.href = destination;
					}, 1000);
				}
			}
		]
	},
	{
		name: 'Statistik',
		type: 'pms',
		data: '/statistic',
		urlNames: ['statistic-menu'],
		icon: 'chart-kpi'
	},
	{
		name: 'Akun',
		type: 'internal',
		data: '/settings',
		urlNames: ['settings'],
		icon: 'account'
	}
];
