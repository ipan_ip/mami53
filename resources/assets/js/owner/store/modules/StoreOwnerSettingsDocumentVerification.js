const storeOwnerSettingsDocumentVerification = {
	namespaced: true,
	state: {
		availableMethods: [
			{
				key: 'upload',
				description: 'Upload file foto'
			},
			{
				key: 'capture',
				description: 'Ambil foto dari kamera'
			}
		],
		selectedMethod: 'upload',
		currentState: 'intercept',
		documentTypeList: [],
		isLoadingFetchDocumentTypeList: false,
		selectedDocument: {
			key: null,
			label: null
		},
		currentPhotoStep: 1,
		photoValue: {
			cardOnly: null,
			selfie: null
		},
		isSwitchCamera: false,
		isCapturePhoto: false,
		timerCount: 0,
		isPreviewPhoto: false
	},
	mutations: {
		setSelectedMethod(state, payload) {
			state.selectedMethod = payload;
		},
		setCurrentState(state, payload) {
			state.currentState = payload;
		},
		setDocumentTypeList(state, payload) {
			state.documentTypeList = payload;
		},
		setIsLoadingFetchDocumentTypeList(state, payload) {
			state.isLoadingFetchDocumentTypeList = payload;
		},
		setSelectedDocument(state, payload) {
			state.selectedDocument = payload;
		},
		setCurrentPhotoStep(state, payload) {
			state.currentPhotoStep = payload;
		},
		setPhotoValue(state, payload) {
			state.photoValue[payload.type] = payload.value;
		},
		setIsSwitchCamera(state, payload) {
			state.isSwitchCamera = payload;
		},
		setIsCapturePhoto(state, payload) {
			state.isCapturePhoto = payload;
		},
		setTimerCount(state, payload) {
			state.timerCount = payload;
		},
		setIsPreviewPhoto(state, payload) {
			state.isPreviewPhoto = payload;
		}
	},
	getters: {
		availableStates: state => {
			return [
				{
					name: 'intercept',
					component: 'OwnerSettingsDocumentVerificationIntercept'
				},
				{
					name: 'input',
					component: 'OwnerSettingsDocumentVerificationInput'
				},
				{
					name: state.selectedMethod,
					component:
						state.selectedMethod === 'upload'
							? 'OwnerSettingsDocumentVerificationUpload'
							: 'OwnerSettingsDocumentVerificationCapture'
				},
				{
					name: 'review',
					component: 'OwnerSettingsDocumentVerificationReview'
				},
				{
					name: 'finish',
					component: 'OwnerSettingsDocumentVerificationFinish'
				}
			];
		},
		currentStateComponent: (state, getters) => {
			return (
				getters.availableStates.find(
					availableState => availableState.name === state.currentState
				).component || null
			);
		},
		lastCapturedPhotoType: state => {
			const types = Object.keys(state.photoValue);

			for (let i = types.length - 1; i >= 0; i--) {
				if (state.photoValue[types[i]]) return types[i];
			}

			return null;
		},
		completedSteps: state => {
			const completed = [];

			Object.keys(state.photoValue).forEach((key, i) => {
				if (state.photoValue[key]) completed.push(i + 1);
			});

			return completed;
		},
		isLastPhotoStep: state => {
			return Object.keys(state.photoValue).length === state.currentPhotoStep;
		},
		documentLabel: state => (key, isOnlyAccronym = false) => {
			const documentType = state.documentTypeList.find(
				type => type.key === key
			);
			const title = documentType.title;
			const subtitle = documentType.subtitle;

			if (isOnlyAccronym) return subtitle || title;
			else return subtitle ? `${title} (${subtitle})` : title;
		},
		isShowGuideButton: state => {
			return state.currentState === 'capture' && !state.timerCount;
		}
	},
	actions: {
		fetchDocumentTypeList({ commit }) {
			return new Promise((resolve, reject) => {
				axios
					.get('user/verification/identity-card/options')
					.then(response => {
						if (response.data.status) {
							commit('setDocumentTypeList', response.data.data);
							resolve(response);
						} else {
							bugsnagClient.notify(JSON.stringify(response));
							alert(
								'Galat tidak diketahui.\nSilakan cek koneksi internet Anda dan coba lagi.'
							);
						}

						resolve();
					})
					.catch(error => {
						reject(error);
					});
			});
		},
		initSelectedDocumentType({ getters, commit }) {
			const documentPayload = {
				key: 'e_ktp',
				label: getters.documentLabel('e_ktp', true)
			};

			commit('setSelectedDocument', documentPayload);
		},
		shiftPhotoStep({ state, commit }, isForward) {
			const nextStep = isForward
				? state.currentPhotoStep + 1
				: state.currentPhotoStep - 1;

			commit('setCurrentPhotoStep', nextStep);
		},
		shiftState({ state, getters, commit, dispatch }, isGoToNext) {
			const targetState =
				getters.availableStates[
					getters.availableStates.findIndex(
						availableState => availableState.name === state.currentState
					) + (isGoToNext ? 1 : -1)
				].name;

			if (
				state.currentState === 'input' &&
				!isGoToNext &&
				state.currentPhotoStep !== 1
			) {
				/**
				 * Set photo step to the previous one and set current state to 'review'
				 * when going back from 'input' state while currently not on the first
				 * step (card only photo).
				 */
				dispatch('shiftPhotoStep', false);
				commit('setCurrentState', 'review');
			} else {
				if (state.currentState === 'review' && !isGoToNext) {
					// Reset last photo value when going back from 'review' state.
					dispatch('handlePhotoValue', null);
				}

				commit('setCurrentState', targetState);
			}
		},
		switchCamera({ state, commit }) {
			commit('setIsSwitchCamera', !state.isSwitchCamera);
		},
		capturePhoto({ state, commit }) {
			commit('setTimerCount', 3000);

			const captureInterval = setInterval(() => {
				commit('setTimerCount', state.timerCount - 1000);

				if (state.timerCount === 0) {
					commit('setIsCapturePhoto', true);
					clearInterval(captureInterval);
				}
			}, 1000);
		},
		handlePhotoValue({ state, commit, getters }, payload) {
			/**
			 * 'null' payloads represent removing the last captured photo type value,
			 * while truthy payloads (image data) will be used as the value for the
			 * current photo type.
			 */
			const type =
				payload === null
					? getters.lastCapturedPhotoType
					: Object.keys(state.photoValue)[state.currentPhotoStep - 1];
			const photoValue = {
				type,
				value: payload
			};

			commit('setPhotoValue', photoValue);
		}
	}
};

export default storeOwnerSettingsDocumentVerification;
