import StoreOwnerSettingsDocumentVerification from './store/modules/StoreOwnerSettingsDocumentVerification';

import VueRouter from 'vue-router';

import Vuex from 'vuex';

import ToggleButton from 'vue-js-toggle-button';

import Dayjs from 'vue-dayjs';

import VeeValidateLanguage from 'vee-validate/dist/locale/id';
import VeeValidate, { Validator } from 'vee-validate';

import VueClipboard from 'vue-clipboard2';

// Global Mixin
import mixinAuth from 'Js/@mixins/MixinAuth';

import VueLazyload from 'vue-lazyload';

import 'bangul-vue/dist/bangul.css';

// quick fix because there is a break change on https://devel.wejoin.us:3000/ali/mami53/-/merge_requests/11810
document.body.style.overflow = 'unset';

require('Js/starter');

window.debounce = require('lodash/debounce');

window.Vue = require('vue').default;
Vue.use(VueRouter);
Vue.use(Vuex);
Vue.use(ToggleButton);
Vue.use(Dayjs, {
	lang: 'id'
});
window.validator = new Validator();
Validator.localize('id', VeeValidateLanguage);
Vue.use(VeeValidate, {
	locale: 'id'
});

require('vue2-animate/dist/vue2-animate.min.css');
Vue.use(VueClipboard);

require('Js/@mixins/MixinSwalLoading');
require('Js/@mixins/MixinSwalSuccessError');
require('Js/@mixins/MixinInitBootstrapJs');

// local Mixin
require('./mixins/OwnerMixinReloadProfile');
Vue.use(VueLazyload);

const App = require('./components/App.vue').default;
const ContainerOwnerPage = require('./components/ContainerOwnerPage.vue')
	.default;

// Container Component
Vue.component('app', App);
Vue.component('container-owner-page', ContainerOwnerPage);

// Global Components

Vue.component('mami-loading', () => import('Js/@components/MamiLoading.vue'));
Vue.component('mami-loading-inline', () =>
	import('Js/@components/MamiLoadingInline.vue')
);
Vue.component('breadcrumb-trails', () =>
	import('Js/@components/BreadcrumbTrails.vue')
);

Vue.component('room-list', () => import('Js/@components/RoomList.vue'));

// LOCAL COMPONENT

Vue.component('owner-tips-loading', () =>
	import('./components/OwnerTipsLoading.vue')
);

Vue.component('owner-header', () => import('./components/OwnerHeader.vue'));
Vue.component('owner-menu', () => import('./components/OwnerMenu.vue'));
Vue.component('owner-body', () => import('./components/OwnerBody.vue'));

Vue.component('owner-list-null', () =>
	import('./components/OwnerListNull.vue')
);
Vue.component('owner-list-active', () =>
	import('./components/OwnerListActive.vue')
);
Vue.component('owner-list-property', () =>
	import('./components/OwnerListProperty.vue')
);
Vue.component('owner-kos-list-card', () =>
	import('./components/OwnerKosListCard.vue')
);
Vue.component('owner-list-vacancy', () =>
	import('./components/OwnerListVacancy.vue')
);

Vue.component('owner-detail-back', () =>
	import('./components/OwnerDetailBack.vue')
);

Vue.component('owner-modal-need-password', () =>
	import('./components/OwnerModalNeedPassword.vue')
);
Vue.component('owner-modal-phone-verify', () =>
	import('./components/OwnerModalPhoneVerify.vue')
);
Vue.component('owner-modal-add', () =>
	import('./components/OwnerModalAdd.vue')
);
Vue.component('owner-modal-promote', () =>
	import('./components/OwnerModalPromote.vue')
);
Vue.component('owner-modal-special-promo', () =>
	import('./components/OwnerModalSpecialPromo.vue')
);
Vue.component('owner-modal-edit', () =>
	import('./components/OwnerModalEdit.vue')
);
Vue.component('owner-modal-edit-vacancy', () =>
	import('./components/OwnerModalEditVacancy.vue')
);
Vue.component('owner-modal-survey-room', () =>
	import('./components/OwnerModalSurveyRoom.vue')
);
Vue.component('owner-modal-survey-premium', () =>
	import('./components/OwnerModalSurveyPremium.vue')
);

Vue.component('owner-property-update-list', () =>
	import('./components/OwnerPropertyUpdateList.vue')
);
Vue.component('owner-modal-trial', () =>
	import('./components/OwnerModalTrial.vue')
);
Vue.component('payment-info', () =>
	import('./components/payment/PaymentInfo.vue')
);
Vue.component('payment-methods', () =>
	import('./components/payment/PaymentMethods.vue')
);

const ownerDefault = () => import('./components/OwnerDefault.vue');

const ownerSettings = () => import('./components/OwnerSettings.vue');
const ownerSettingsDocumentVerification = () =>
	import('./components/settings/OwnerSettingsDocumentVerification');

const ownerAdd = () => import('./components/OwnerAdd.vue');
const ownerNotif = () => import('./components/OwnerNotif.vue');
const ownerActive = () => import('./components/OwnerActive.vue');

const ownerKos = () => import('./components/OwnerKos.vue');
const ownerApartment = () => import('./components/OwnerApartment.vue');

const ownerPropertyUpdate = () =>
	import('./components/OwnerPropertyUpdate.vue');
const ownerPropertyPromo = () => import('./components/OwnerPropertyPromo.vue');

const ownerVacancy = () => import('./components/OwnerVacancy.vue');
const ownerVacancyDetail = () => import('./components/OwnerVacancyDetail.vue');
const ownerVacancyDetailReport = () =>
	import('./components/OwnerVacancyDetailReport.vue');
const ownerVacancyDetailReportProfile = () =>
	import('./components/OwnerVacancyDetailReportProfile.vue');

const ownerPremiumConfirmation = () =>
	import('./components/premium/OwnerPremiumConfirmation.vue');
const ownerPurchaseDetail = () =>
	import('./components/premium/OwnerPurchaseDetail.vue');

const containerPayment = () =>
	import('./components/payment/ContainerPayment.vue');

const routes = [
	{
		path: '/',
		name: 'default',
		component: ownerDefault
	},
	{
		path: '/settings',
		name: 'settings',
		component: ownerSettings
	},
	{
		path: '/settings/document-verification',
		name: 'document_verification_setting',
		component: ownerSettingsDocumentVerification
	},
	{
		path: '/add',
		name: 'add',
		component: ownerAdd,
		props: true
	},
	{
		path: '/notification',
		name: 'notification',
		component: ownerNotif
	},
	{
		path: '/active',
		name: 'active',
		component: ownerActive
	},
	{
		path: '/kos',
		name: 'kos',
		component: ownerKos
	},
	{
		path: '/kos/update',
		name: 'kos_update',
		component: ownerPropertyUpdate,
		props: { propertyType: 'kos' }
	},
	{
		path: '/kos/:seq',
		name: 'kos_promo',
		component: ownerPropertyPromo,
		props: { propertyType: 'kos' }
	},
	{
		path: '/apartment',
		name: 'apartment',
		component: ownerApartment,
		props: { propertyType: 'apartment' }
	},
	{
		path: '/apartment/update',
		name: 'apartment_update',
		component: ownerPropertyUpdate,
		props: { propertyType: 'apartment' }
	},
	{
		path: '/apartment/:seq',
		name: 'apartment_promo',
		component: ownerPropertyPromo,
		props: { propertyType: 'apartment' }
	},
	{
		path: '/vacancy',
		name: 'vacancy',
		component: ownerVacancy
	},
	{
		path: '/vacancy/:slug',
		name: 'vacancy_detail',
		component: ownerVacancyDetail
	},
	{
		path: '/vacancy/:slug/:seq',
		name: 'vacancy_detail_report',
		component: ownerVacancyDetailReport
	},
	{
		path: '/vacancy/:slug/:seq/:number',
		name: 'vacancy_detail_report_number',
		component: ownerVacancyDetailReportProfile
	},
	{
		path: '/premium/purchase/confirmation',
		name: 'premium_confirmation',
		component: ownerPurchaseDetail,
		props: { confirmationType: 'premium' }
	},
	{
		path: '/premium/balance/confirmation',
		name: 'balance_confirmation',
		component: ownerPurchaseDetail,
		props: { confirmationType: 'balance' }
	},
	{
		path: '/invoice',
		name: 'premium_verification',
		props: { confirmationType: 'premiumInvoice' },
		component: ownerPremiumConfirmation
	},
	{
		path: '/payment',
		name: 'payment',
		component: containerPayment,
		props: { paymentType: 'methods' },
		beforeEnter: (to, from, next) => {
			if (store.state.extras.membershipOnConfirmation) {
				next();
			} else {
				next({ name: 'default' });
			}
		}
	},
	{
		path: '/payment/confirmation',
		name: 'payment_confirmation',
		component: containerPayment,
		props: { paymentType: 'info' }
	}
];

const router = new VueRouter({
	mode: 'history',
	base: '/ownerpage',
	routes
});

const store = new Vuex.Store({
	modules: {
		settingsDocumentVerification: StoreOwnerSettingsDocumentVerification
	},
	state: {
		token: document.head.querySelector('meta[name="csrf-token"]').content,
		authCheck: {},
		authData: {},
		profile: {},
		isChatOpen: false,
		mamipayDetail: {},
		isBookingAllRoom: false,
		bbkKosStatus: {},
		extras: {
			documentTitle: document.title,
			notifCount: null,
			textToDetail: Math.random() >= 0.5 ? 'A' : 'B',
			selectedRoom: 0,
			confirmationData: null,
			membershipOnConfirmation: false,
			balanceOnConfirmation: false
		},
		abTestResult: {},
		invoice: {}
	},
	getters: {
		invoiceUrl(state) {
			return state.invoice.invoice.invoice_url;
		},
		isOnConfirmation(state) {
			return (
				state.extras.membershipOnConfirmation ||
				state.extras.balanceOnConfirmation
			);
		},
		userHasNoProperty(state) {
			return (
				((state.profile &&
					state.profile.user &&
					state.profile.user.kost_total) ||
					0) <= 0
			);
		}
	},
	mutations: {
		setAuthCheck(state, payload) {
			state.authCheck = payload;
		},
		setAuthData(state, payload) {
			state.authData = payload;
		},
		setProfile(state, payload) {
			state.profile = payload;
		},
		setMamipayDetail(state, payload) {
			state.mamipayDetail = payload;
		},
		setIsBookingAllRoom(state, payload) {
			state.isBookingAllRoom = payload;
		},
		setMembershipStatus(state, payload) {
			payload === 'Konfirmasi Pembayaran'
				? (state.extras.membershipOnConfirmation = true)
				: (state.extras.membershipOnConfirmation = false);
		},
		setBalanceStatus(state, payload) {
			payload !== 'verif'
				? (state.extras.balanceOnConfirmation = true)
				: (state.extras.balanceOnConfirmation = false);
		},
		setNotifCount(state, payload) {
			state.extras.notifCount = payload;

			const documentTitle = store.state.extras.documentTitle;
			if (payload > 0 && payload < 10) {
				document.title = '(' + payload + ') ' + documentTitle;
			} else if (payload > 10) {
				document.title = '(9+) ' + documentTitle;
			} else if (payload === 0) {
				document.title = documentTitle;
			}
		},
		setSelectedRoom(state, payload) {
			state.extras.selectedRoom = payload;
		},
		setConfirmationData(state, payload) {
			state.extras.confirmationData = payload;
		},
		setBbkKosStatus(state, payload) {
			state.bbkKosStatus = payload;
		},
		setAbTestResult(state, payload) {
			state.abTestResult = payload;
		},
		setInvoice(state, payload) {
			state.invoice = payload;
		},
		setIsChatOpen(state, payload) {
			state.isChatOpen = payload;
		}
	}
});

Vue.config.productionTip = false;

/* eslint-disable-next-line no-unused-vars */
const app = new Vue({
	el: '#app',
	mixins: [mixinAuth],
	router,
	store
});
