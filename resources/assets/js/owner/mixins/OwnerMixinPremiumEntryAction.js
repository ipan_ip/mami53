import { mapState } from 'vuex';

export default {
	data() {
		return {
			isNoPropertyModalActive: false,
			isConfirmModalActive: false,
			ownerDashboardUrl: oxWebUrl
		};
	},
	computed: {
		...mapState(['profile']),
		membership() {
			return this.profile.membership;
		},
		user() {
			return this.profile.user;
		},
		isPremium() {
			return !this.membership.expired && !!this.membership.status === 'Premium';
		},
		activeKos() {
			return this.user.kost_total_active;
		},
		activeApartment() {
			return this.user.apartment_total_active;
		},
		isBbk() {
			return (
				this.membership.is_mamipay_user && this.profile.is_booking_all_room
			);
		},
		hasActiveListing() {
			return this.activeKos > 0 || this.activeApartment > 0;
		},
		hangingTrx() {
			return this.$store.state.invoice.is_hanging_purchase;
		},
		isMobile() {
			return navigator.isMobile;
		}
	},
	methods: {
		handlePremiumEntryAction() {
			this.trackPremiumEntryPointClick();

			switch (this.membership.status) {
				case 'Coba Trial':
					if (this.hasActiveListing) {
						window.location.href = `/promosi-kost/premium-package`;
					} else {
						this.toggleAddPropertyModal();
					}
					break;
				case 'Upgrade ke Premium':
					if (this.hasActiveListing) {
						window.location.href = `/promosi-kost/premium-package`;
					} else {
						this.toggleAddPropertyModal();
					}
					break;
				case 'Premium':
					if (this.hasActiveListing) {
						window.location.href = `${this.ownerDashboardUrl}/premium-dashboard`;
					} else {
						this.toggleAddPropertyModal();
					}
					break;
				case 'Konfirmasi Pembayaran':
					if (this.hangingTrx || this.membership.expired) {
						this.togglePaymentConfirmationModal();
					} else {
						window.location.href = `${this.ownerDashboardUrl}/premium-dashboard`;
					}
			}
		},
		toggleAddPropertyModal() {
			this.isNoPropertyModalActive = !this.isNoPropertyModalActive;
		},
		togglePaymentConfirmationModal() {
			this.isConfirmModalActive = !this.isConfirmModalActive;
		},
		trackPremiumEntryPointClick() {
			tracker('moe', [
				'[Owner] OD Pemasaran Kos - Premium Clicked',
				{
					owner_id: this.user.user_id,
					owner_name: this.user.name,
					owner_phone_number: this.user.phone_number,
					owner_email: this.user.email || '',
					is_premium: this.isPremium,
					saldo_amount: this.membership.views,
					is_promoted: null,
					is_mamirooms: !!this.user.is_mamirooms || false,
					redirection_source: 'sidebar',
					is_booking: this.isBbk,
					interface: this.isMobile ? 'mobile' : 'desktop'
				}
			]);
		}
	}
};
