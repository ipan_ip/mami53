import * as moment from 'moment';

const ownerMixinPremiumConfirmCountdown = {
	methods: {
		premiumConfirmCountdown(countdown) {
			let countdownDuration = moment.duration(countdown * 1000);
			let realSeconds = countdown;
			let interval = 1000;

			let countdownInterval = setInterval(() => {
				countdownDuration = moment.duration(countdownDuration - interval);
				realSeconds -= 1;
				$('.premium-confirm-countdown').text(
					parseInt(countdownDuration.asHours()) +
						':' +
						countdownDuration.minutes() +
						':' +
						countdownDuration.seconds()
				);
				if (realSeconds <= 0) {
					clearInterval(countdownInterval);
					this.reloadOwnerProfile();
				}
			}, interval);
		}
	}
};

export default ownerMixinPremiumConfirmCountdown;
