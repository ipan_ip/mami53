Vue.mixin({
  methods: {
    reloadOwnerProfile() {
      this.openSwalLoading('Memperbarui data...', null);
      axios
        .get('/owner/data/profile')
        .then(response => {
          this.closeSwalLoading();

          if (response.data.status) {
            this.$store.commit('setProfile', response.data);
            this.$store.commit(
              'setNotifCount',
              response.data.user.notification_count
            );
          } else {
            alert(
              'Galat tidak diketahui.\nSilakan cek koneksi internet Anda dan coba lagi.'
            );
          }
        })
        .catch(error => {
          bugsnagClient.notify(error);
          alert(
            'Galat tidak diketahui.\nSilakan cek koneksi internet Anda dan coba lagi.'
          );
        });
    }
  }
});
