import Toasted from 'vue-toasted';
Vue.use(Toasted);

export default {
	methods: {
		updateAllProperties() {
			// ajax call
			return axios
				.post('owner/data/update-room')
				.then(({ data }) => {
					if (data.status) {
						this.trackOwner();
						this.notifyOwner();
					}
				})
				.catch(error => {
					window.bugsnagClient && window.bugsnagClient.notify(error);
					alert(
						'Galat tidak diketahui.\nSilakan cek koneksi internet Anda dan coba lagi.'
					);
				});
		},
		trackOwner() {
			tracker('moe', [
				'[Owner] Dashboard - Kamar - Update Semua Kos Button Clicked',
				{
					user_id: this.$store.state.authData.all.id.toString(),
					interface: navigator.isMobile ? 'mobile' : 'desktop',
					is_booking: this.$store.state.profile.membership.is_mamipay_user,
					is_premium: this.$store.state.profile.membership.is_mamipay_user
				}
			]);
		},
		notifyOwner() {
			this.$toasted.show('Sukses mengupdate property', {
				type: 'info',
				className: 'toasted-default',
				theme: 'bubble',
				position: 'bottom-center',
				duration: 5000
			});
		}
	}
};
