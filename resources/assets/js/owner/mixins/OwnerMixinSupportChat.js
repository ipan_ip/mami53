const ownerMixinSupportChat = {
	data() {
		return {
			groupChannel: null,
			userGroup: []
		};
	},

	methods: {
		startSupportChat() {
			if (this.profile !== undefined) {
				this.openSwalLoading('Memulai chat...', null);

				const groupName = `${this.profile.user.chat_name} : CS Mamikos`;

				const channelListQuery = sb.GroupChannel.createMyGroupChannelListQuery();
				channelListQuery.includeEmpty = true;

				channelListQuery.next((groupChannels, error) => {
					if (error) {
						return;
					}

					const groups = groupChannels;

					for (let group of groups) {
						if (
							group.name.trim().toLowerCase() === groupName.trim().toLowerCase()
						) {
							this.groupChannel = group;

							break;
						}
					}

					if (this.groupChannel == null) {
						this.userGroup = [
							this.profile.user.user_id,
							this.profile.membership.cs_id
						];

						const groupImage = '/assets/default_chat_channel_cover.png';
						const isDistinct = true;

						sb.GroupChannel.createChannelWithUserIds(
							this.userGroup,
							isDistinct,
							groupName,
							groupImage,
							'',
							(groupChannel, error) => {
								if (error) {
									return;
								}

								this.groupChannel = groupChannel;

								this.insertMetadata();

								sbWidget.showChannel(this.groupChannel.url);

								this.closeSwalLoading();
							}
						);
					} else {
						sbWidget.showChannel(this.groupChannel.url);

						this.closeSwalLoading();
					}
				});
			}
		},
		insertMetadata() {
			const metadata = {
				chat_support: true
			};
			const upsertIfNotExist = true;

			this.groupChannel.updateMetaData(
				metadata,
				upsertIfNotExist,
				(response, error) => {
					if (error) {
						return;
					}
				}
			);
		},

		openZendeskChat() {
			zE('webWidget', 'open');

			zE('webWidget', 'prefill', {
				name: {
					value: this.$store.state.authData.all.name
				},
				email: {
					value: this.$store.state.authData.all.email
				}
			});
		},

		closeZendeskChat() {
			zE('webWidget', 'open');
		}
	}
};

export default ownerMixinSupportChat;
