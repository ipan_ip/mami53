import swal from 'sweetalert2';

const mixinBuyBalanceEligibility = {
	computed: {
		profile() {
			return this.$store.state.profile;
		},

		isVerify() {
			return this.$store.state.profile.user.is_verify;
		},

		isOnTrial() {
			return this.profile.membership.package_category === 'trial';
		},

		isKostActive() {
			return this.$store.state.profile.user.kost_total_active;
		},

		isApartmentActive() {
			return this.$store.state.profile.user.apartment_total_active;
		},

		hasProperty() {
			return this.isApartmentActive > 0 || this.isKostActive > 0;
		},

		buyBalanceEligibility() {
			const eligibility = ['has-property', 'no-property', 'on-trial'];
			if (this.isVerify && !this.isOnTrial) {
				return this.hasProperty > 0 ? eligibility[0] : eligibility[1];
			} else {
				return eligibility[2];
			}
		}
	},

	methods: {
		alertForBalanceEligibility() {
			swal('Perhatian!', `Silakan update ke premium.`, 'warning');
		}
	}
};

export default mixinBuyBalanceEligibility;
