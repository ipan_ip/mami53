import { mapState } from 'vuex';

export default {
	computed: {
		...mapState(['profile', 'mamipayDetail']),
		isMobile() {
			return navigator.isMobile;
		}
	},
	methods: {
		trackUpdatePasswordSucceeded() {
			tracker('moe', [
				'[Owner] Update Password Success',
				{
					request_by: 'phone_number',
					interface: this.isMobile ? 'mobile' : 'desktop'
				}
			]);
		},
		trackDocumentVerificationClicked() {
			tracker('moe', [
				'[Owner] Verification ID - Button Verifikasi Clicked',
				{
					interface: this.isMobile ? 'mobile' : 'desktop',
					owner_name: this.profile.user.name,
					owner_phone_number: this.profile.user.phone_number,
					verification_status: this.mamipayDetail.verification_data
						.identity_card_status
				}
			]);
		},
		trackSettingsPageVisited() {
			tracker('moe', [
				'[Owner] Edit Informasi Pribadi page Visited',
				{
					interface: this.isMobile ? 'mobile' : 'desktop',
					owner_name: this.profile.user.name,
					owner_phone_number: this.profile.user.phone_number
				}
			]);
		},
		trackOTPFormOpened() {
			tracker('moe', [
				'[Owner] Visit Input OTP Form',
				{
					request_from: 'setting',
					interface: this.isMobile
				}
			]);
		},
		trackOTPResent() {
			tracker('moe', [
				'[Owner] Click Resend OTP',
				{
					request_from: 'setting',
					interface: this.isMobile
				}
			]);
		},
		trackOTPSubmitted(message = null) {
			tracker('moe', [
				'[Owner] Verifikasi Phone Number',
				{
					interface: this.isMobile,
					request_from: 'setting',
					success_status: !message,
					fail_reason: message,
					owner_name: this.profile.user.name,
					owner_phone_number: this.profile.user.phone_number,
					owner_email: this.profile.user.email
				}
			]);
		}
	}
};
