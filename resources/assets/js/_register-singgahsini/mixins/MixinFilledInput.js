export default {
	methods: {
		filledInput(data, optional = '') {
			if (optional !== '') {
				data = Object.keys(data).reduce((object, key) => {
					if (key !== optional) {
						object[key] = data[key];
					}
					return object;
				}, {});
			}

			const inputData = Object.values(data);
			let count = 0;
			for (let i = 0; i < inputData.length; ++i) {
				if (inputData[i] !== '' && typeof inputData[i] === 'string') {
					count++;
				}

				if (inputData[i] !== '' && typeof inputData[i] === 'number') {
					count++;
				}

				if (typeof inputData[i] === 'object' && inputData[i].length > 0) {
					count++;
				}
			}

			return count;
		}
	}
};
