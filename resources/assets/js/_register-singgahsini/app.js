/* eslint-disable no-unused-vars */
import 'Js/@mixins/MixinNavigatorIsMobile';
import 'Js/@mixins/MixinSwalLoading';
import 'Js/@mixins/MixinSwalSuccessError';
import mixinAuth from 'Js/@mixins/MixinAuth';
import App from './components/App.vue';
import Vuex from 'vuex';

import defaultStore from './store/index';
import router from './router/index';
import id from 'vee-validate/dist/locale/id';
import VeeValidate, { Validator } from 'vee-validate';

import 'bangul-vue/dist/bangul.css';

Vue.use(Vuex);
Validator.localize('id', id);
Vue.use(VeeValidate, { locale: 'id' });

Vue.component('app', App);

const store = new Vuex.Store(defaultStore);

const app = new Vue({
	el: '#app',
	router,
	store,
	mixins: [mixinAuth],
	mounted() {
		try {
			document.getElementById('preloading').remove();
		} catch (e) {
			document.getElementById('preloading').style.display = 'none';
		}
	}
});
