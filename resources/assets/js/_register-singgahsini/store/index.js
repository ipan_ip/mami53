const defaultStore = {
	state: {
		token: document.head.querySelector('meta[name="csrf-token"]').content,
		registrationData: {
			ownerData: {
				owner_name: '',
				phone_number: '',
				city: '',
				address: '',
				kost_name: ''
			}
		},
		allCity: []
	},
	mutations: {
		setOwnerDataName(state, payload) {
			state.registrationData.ownerData.owner_name = payload;
		},
		setOwnerDataPhoneNumber(state, payload) {
			state.registrationData.ownerData.phone_number = payload;
		},
		setOwnerDataCity(state, payload) {
			state.registrationData.ownerData.city = payload;
		},
		setOwnerDataAddressNote(state, payload) {
			state.registrationData.ownerData.address = payload;
		},
		setOwnerDataKosName(state, payload) {
			state.registrationData.ownerData.kost_name = payload;
		},
		setAllCity(state, payload) {
			state.allCity = payload;
		}
	}
};

export default defaultStore;
