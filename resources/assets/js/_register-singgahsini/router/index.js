import VueRouter from 'vue-router';

window.debounce = require('lodash/debounce');
Vue.use(VueRouter);

const RegisterOwnerData = require('../components/RegisterOwnerData').default;
const routes = [{ path: '', component: RegisterOwnerData }];

const path = /\/singgahsini\/daftar/g;
const baseUrl = path.test(window.location.pathname)
	? 'singgahsini/daftar'
	: '/daftar';

const router = new VueRouter({
	mode: 'history',
	base: baseUrl,
	linkActiveClass: 'active',
	routes
});

router.afterEach(to => {
	// After routing to new page
	tracker('ga', ['set', 'page', `${baseUrl}${to.path}`]);
	tracker('ga', ['send', 'pageview']);
});

export default router;
