var bookingCode = {
	content: ''
};

var getBookingCode = function() {
	bookingCode.content = $('meta[name=booking-code]').attr('content');
};

var getBookingDetail = function() {
	modalLoadingOn();
	$.ajax({
		type: 'GET',
		url: '/garuda/booking/detail/' + bookingCode.content,
		headers: {
			'Content-Type': 'application/json',
			'X-GIT-Time': '1406090202',
			Authorization: 'GIT WEB:WEB'
		},
		crossDomain: true,
		dataType: 'json',
		success: function(response) {
			modalLoadingOff();
			if (response.status) {
				finalBooking.data = response.data;

				//replace summary data object from root module using vue instance name
				booking.bookingType = finalBooking.data.room_data.type;
				booking.thisId = finalBooking.data.room_data._id;
				booking.selectItems.room = finalBooking.data.room_data;
				booking.selectItems.price = finalBooking.data.booking_data.prices;
				booking.selectItems.totalGuest = finalBooking.data.booking_data.guest_total;
				booking.checkItems.startDate = finalBooking.data.booking_data.checkin_formatted;
				booking.checkItems.endDate = finalBooking.data.booking_data.checkout_formatted;
				booking.checkItems.totalRoom = finalBooking.data.booking_data.room_total;
				booking.checkItems.duration = finalBooking.data.booking_data.duration;
				booking.guestsData = finalBooking.data.guests;

				// page.section = 'receipt';

				if (finalBooking.data.booking_data.status == 'verified') {
					page.section = 'receipt';
				} else {
					page.section = 'payment';
					if (finalBooking.data.booking_data.status == 'booked') {
						initPaymentCountdown(finalBooking.data.booking_data);
					} else if (finalBooking.data.booking_data.status == 'confirmed') {
						//cut date & time string into only time string
						finalBooking.data.booking_data.expired_date = finalBooking.data.booking_data.expired_date.substring(
							11,
							16
						);
						initPaymentCountdown(finalBooking.data.booking_data);
						paymentConfirm.destination = finalBooking.data.bank_accounts[0].id; //set default select model value
						setTimeout(function() {
							initPaymentDate();
						}, 500);
					}
				}
			} else {
				showError(response.messages);
			}
		},
		statusCode: statusCodeReload
	});
};

var finalBooking = {
	data: {}
};

var paymentConfirmDate = {
	rawDate: {}
};

var initPaymentDate = function() {
	$('#paymentDate').datetimepicker({
		format: 'Y-MM-DD',
		minDate: moment()
	});
	$('#paymentDate').on('dp.change', function(e) {
		paymentConfirmDate.rawDate = e.date;
	});
};

var countdownData = 0;
var countdownDuration = 0;
var countdownTime = {
	hours: 0,
	minutes: 0,
	seconds: 0
};
var realSeconds = 0;
var interval = 0;

var initPaymentCountdown = function(data) {
	if (data.admin_waiting_time_seconds > 0) {
		countdownData = data.admin_waiting_time_seconds;
	} else {
		countdownData = data.expired_date_seconds;
	}

	countdownDuration = moment.duration(countdownData * 1000);
	realSeconds = countdownData;
	interval = 1000;

	var countdownInterval = setInterval(function() {
		countdownDuration = moment.duration(countdownDuration - interval);
		realSeconds -= 1;

		countdownTime.hours = parseInt(countdownDuration.asHours());
		countdownTime.minutes = countdownDuration.minutes();
		countdownTime.seconds = countdownDuration.seconds();

		if (realSeconds <= 0) {
			clearInterval(countdownInterval);
			getBookingDetail(); //get booking detail data again
		}
	}, interval);
};

var paymentConfirm = {
	source: '',
	name: '',
	number: '',
	destination: 0,
	total: '',
	date: ''
};

var submitPaymentConfirm = function() {
	paymentConfirm.date = moment(paymentConfirmDate.rawDate, 'YYYY-MM-DD').format('YYYY-MM-DD');
	$.ajax({
		type: 'POST',
		url: '/garuda/booking/payment/confirmation/' + bookingCode.content,
		headers: {
			'Content-Type': 'application/json',
			'X-GIT-Time': '1406090202',
			Authorization: 'GIT WEB:WEB'
		},
		data: JSON.stringify({
			_token: csrfToken,
			bank_destination: paymentConfirm.destination,
			bank_source: paymentConfirm.source,
			account_name: paymentConfirm.name,
			account_number: paymentConfirm.number,
			total_payment: paymentConfirm.total,
			payment_date: paymentConfirm.date
		}),
		crossDomain: true,
		dataType: 'json',
		success: function(response) {
			modalLoadingOff();
			if (response.status) {
				swal({
					title: '',
					text: 'Terima kasih. Pembayaran booking Anda segera kami proses.',
					type: 'success',
					timer: 2000
				}).then(
					function() {
						window.location.replace('/booking/history');
					},
					function(dismiss) {
						if (dismiss === 'timer') {
							window.location.replace('/booking/history');
						}
					}
				);
			} else {
				showError(response.messages);
			}
		},
		statusCode: statusCodeNoReload
	});
};