Vue.config.productionTip = false;

var booking = new Vue({
	el: '#booking',
	data: {
		//Global Module Data
		csrfToken,
		thisId,
		thisTitle,
		bookingType,
		page,
		statusCodeReload,
		statusCodeNoReload,
		loadingStatus,

		//Select Module Data
		firstRawDate,
		lastRawDate,
		firstDate,
		lastDate,
		startRawDate,
		endRawDate,
		durationDays,
		checkItems,
		rooms,
		passMinimumStay,
		selectItems,

		//Detail Module Data
		contactData,
		guestsData,
		guestBirthday,
		similarPerson,

		//Payment Module Data
		bookingCode,
		finalBooking,
		paymentConfirmDate,
		paymentConfirm,
		countdownData,
		countdownDuration,
		countdownTime,
		realSeconds,
		interval,

		//Receipt Module Data
		cancelReason,

		//History Module Data
		bookingHistory
	},
	watch: {
		//Global Module Watchers
		'page.section': function(val) {
			if (val == 'select') {
				initRangeDate();
				readySchedule();
			} else if (val == 'detail') {
				setTimeout(function() {
					initBirthday();
				}, 500);
			} else if (page.section == 'history') {
				getBookingHistory();
			}
		},

		//Detail Module Watchers
		'similarPerson.bool': function(val) {
			if (val == true) {
				guestsData[0].name = contactData.name;
				guestsData[0].email = contactData.email;
				guestsData[0].phone_number = contactData.phone_number;
			} else {
				guestsData[0].name = '';
				guestsData[0].email = '';
				guestsData[0].phone_number = '';
			}
		},

		//Payment Module Watchers
		'bookingCode.content': function(val) {
			if (val !== undefined) {
				getBookingDetail();
			}
		}
	},
	mounted() {
		//Mounted from External Module
		centerModal();

		//Mounted from Global Module
		basePath();

		//Mounted from Payment Module
		getBookingCode();
	},
	methods: {
		//Select Module Methods
		toggleEndDate,
		checkBooking,
		selectRoom,

		//Detail Module Methods
		toDetailBooking,
		submitPerson,
		reselectRoom,

		//Review Module Methods
		submitRoomBooking,
		rewriteGuest,

		//Payment Module Methods
		submitPaymentConfirm,

		//Receipt Module Methods
		initBookingCancel,
		submitBookingCancel,
		initClaimGuarantee,
		submitClaimGuarantee
	}
});
