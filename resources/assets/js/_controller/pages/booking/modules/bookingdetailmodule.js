var contactData = {
	name: '',
	email: '',
	phone_number: ''
};

var guestsData = [];

var toDetailBooking = function(room, price) {
	for (var i = 0; i < parseInt(selectItems.totalGuest); i++) {
		guestsData.push({
			name: '',
			birthday: '',
			email: '',
			phone_number: ''
		});
	}
	similarPerson.bool = false;
	selectItems.room = room;
	selectItems.price = price;

	page.section = 'detail';
};

var guestBirthday = {};

var initBirthday = function() {
	$('.guest-birthday').datetimepicker({
		format: 'Y-MM-DD'
	});
	for (i = 0; i < guestsData.length; i++) {
		$('#guestBirthday' + i).on('dp.change', function(e) {
			guestBirthday['rawDate' + $(this).data('key')] = e.date;
		});
	}
};

var similarPerson = {
	bool: false
};

var submitPerson = function() {
	for (i = 0; i < guestsData.length; i++) {
		guestsData[i].birthday = moment(guestBirthday['rawDate' + i], 'YYYY-MM-DD').format(
			'YYYY-MM-DD'
		);
	}
	page.section = 'review';
};

var reselectRoom = function() {
	page.section = 'select';
	guestsData.length = 0;
};