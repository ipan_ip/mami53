var firstRawDate = {};
var lastRawDate = {};
var firstDate = '';
var lastDate = '';

var initRangeDate = function() {
	firstRawDate = moment();
	lastRawDate = moment().add(12, 'months');

	firstDate = moment(firstRawDate, 'YYYY-MM-DD').format('YYYY-MM-DD');
	lastDate = moment(lastRawDate, 'YYYY-MM-DD').format('YYYY-MM-DD');
};

var readySchedule = function() {
	modalLoadingOn();
	$.ajax({
		type: 'POST',
		url: '/garuda/stories/booking/schedule',
		headers: {
			'Content-Type': 'application/json',
			'X-GIT-Time': '1406090202',
			Authorization: 'GIT WEB:WEB'
		},
		data: JSON.stringify({
			_token: csrfToken,
			room_id: thisId,
			type: bookingType,
			start_date: firstDate,
			end_date: lastDate
		}),
		crossDomain: true,
		dataType: 'json',
		success: function(response) {
			modalLoadingOff();
			if (response.status) {
				initTooltip();
				checkBookTooltipOn();
				setTimeout(function() {
					checkBookTooltipOff();
				}, 3000);

				initCheckDate(response.data.schedule);
			} else {
				showError(response.messages);
			}
		},
		statusCode: statusCodeReload
	});
};

var checkBookTooltipOn = function() {
	$('.check-submit').tooltip('show');
};
var checkBookTooltipOff = function() {
	$('.check-submit').tooltip('destroy');
};

var startRawDate = {};
var endRawDate = {};
var durationDays = {
	int: 0,
	str: ''
};

var initCheckDate = function(schedule) {
	var minDate = moment().add(1, 'days');
	schedule.push(moment(minDate, 'YYYY-MM-DD').format('YYYY-MM-DD'));

	$('#checkStartDate').datetimepicker({
		format: 'Y-MM-DD',
		minDate: minDate,
		disabledDates: schedule
	});
	$('#checkStartDate').val('');

	if (bookingType == 'bulanan') {
		$('#checkStartDate').on('dp.change', function(e) {
			startRawDate = e.date;

			startRawDate.seconds(0);
			startRawDate.minutes(0);
			startRawDate.hours(0);
		});
	} else if (bookingType == 'harian') {
		$('#checkEndDate').datetimepicker({
			format: 'Y-MM-DD',
			useCurrent: false,
			disabledDates: schedule
		});
		$('#checkStartDate').on('dp.change', function(e) {
			$('#checkEndDate')
				.data('DateTimePicker')
				.minDate(e.date);
			startRawDate = e.date;

			if (startRawDate.isSameOrAfter(endRawDate)) {
				$('#checkEndDate')
					.data('DateTimePicker')
					.date(e.date);
			}

			startRawDate.seconds(0);
			startRawDate.minutes(0);
			startRawDate.hours(0);
		});
		$('#checkEndDate').on('dp.change', function(e) {
			endRawDate = e.date;

			endRawDate.seconds(0);
			endRawDate.minutes(0);
			endRawDate.hours(0);

			durationDays.int = Math.ceil(endRawDate.diff(startRawDate, 'days', true));
			durationDays.str = durationDays.int.toString();
		});
	}
};

var toggleEndDate = function() {
	if (startRawDate) {
		$('#checkEndDate').removeAttr('disabled');
	} else {
		$('#checkEndDate').attr('disabled', 'disabled');
	}
};

var checkItems = {
	startDate: '',
	endDate: '',
	duration: null,
	totalRoom: null
};

var rooms = {
	available: [],
	lowQty: [],
	notes: ''
};

var passMinimumStay = {
	monthly: false,
	daily: false
};

var checkBookingResponse = function(data) {
	rooms.available = data.available_rooms;
	rooms.lowQty = data.low_qty_rooms;

	rooms.notes = data.notes;

	rooms.available.forEach(function(room, index) {
		if (room.type == 'bulanan') {
			if (room.minimum_stay > parseInt(checkItems.duration)) {
				passMinimumStay.monthly = false;
			} else {
				passMinimumStay.monthly = true;
			}
		} else if (room.type == 'harian') {
			if (room.minimum_stay > durationDays.int) {
				passMinimumStay.daily = false;
			} else {
				passMinimumStay.daily = true;
			}
		}
	});
};

var checkBooking = function() {
	modalLoadingOn();
	checkItems.startDate = moment(startRawDate, 'YYYY-MM-DD').format('YYYY-MM-DD');

	if (bookingType == 'bulanan') {
		endRawDate = startRawDate.add(+checkItems.duration, 'months');
		endRawDate.seconds(0);
		endRawDate.minutes(0);
		endRawDate.hours(0);
		checkItems.endDate = moment(endRawDate, 'YYYY-MM-DD').format('YYYY-MM-DD');
		$.ajax({
			type: 'POST',
			url: '/garuda/stories/booking/search',
			headers: {
				'Content-Type': 'application/json',
				'X-GIT-Time': '1406090202',
				Authorization: 'GIT WEB:WEB'
			},
			data: JSON.stringify({
				_token: csrfToken,
				room_id: thisId,
				type: bookingType,
				start_date: checkItems.startDate,
				duration: +checkItems.duration,
				total_room: +checkItems.totalRoom
			}),
			crossDomain: true,
			dataType: 'json',
			success: function(response) {
				modalLoadingOff();
				if (response.status) {
					checkBookingResponse(response.data);
				} else {
					showError(response.messages);
				}
			},
			statusCode: statusCodeNoReload
		});
	} else if (bookingType == 'harian') {
		checkItems.endDate = moment(endRawDate, 'YYYY-MM-DD').format('YYYY-MM-DD');
		checkItems.duration = durationDays.int;
		$.ajax({
			type: 'POST',
			url: '/garuda/stories/booking/search',
			headers: {
				'Content-Type': 'application/json',
				'X-GIT-Time': '1406090202',
				Authorization: 'GIT WEB:WEB'
			},
			data: JSON.stringify({
				_token: csrfToken,
				room_id: thisId,
				type: bookingType,
				start_date: checkItems.startDate,
				end_date: checkItems.endDate,
				total_room: +checkItems.totalRoom
			}),
			crossDomain: true,
			dataType: 'json',
			success: function(response) {
				modalLoadingOff();
				if (response.status) {
					checkBookingResponse(response.data);
				} else {
					showError(response.messages);
				}
			},
			statusCode: statusCodeNoReload
		});
	}
};

var selectItems = {
	totalGuest: 1,
	room: {},
	price: {}
};

var selectRoom = function(room) {
	if (passMinimumStay.monthly || passMinimumStay.daily) {
		modalLoadingOn();
		$.ajax({
			type: 'POST',
			url: '/garuda/stories/booking/price/calculate/' + thisId + '/' + room.id,
			headers: {
				'Content-Type': 'application/json',
				'X-GIT-Time': '1406090202',
				Authorization: 'GIT WEB:WEB'
			},
			data: JSON.stringify({
				_token: csrfToken,
				type: bookingType,
				checkin: checkItems.startDate,
				checkout: checkItems.endDate,
				duration: +checkItems.duration,
				room_total: +checkItems.totalRoom,
				guest_total: selectItems.totalGuest
			}),
			crossDomain: true,
			dataType: 'json',
			success: function(response) {
				modalLoadingOff();
				if (response.status) {
					toDetailBooking(room, response.data);
				} else {
					showError(response.messages);
				}
			},
			statusCode: statusCodeNoReload
		});
	}
};