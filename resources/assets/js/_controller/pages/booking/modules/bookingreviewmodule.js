var submitRoomBooking = function() {
	var confirmation = confirm('Lanjut ke pembayaran booking sekarang?');
	if (confirmation == true) {
		modalLoadingOn();
		$.ajax({
			type: 'POST',
			url: '/garuda/stories/booking/book/' + thisId + '/' + selectItems.room.id,
			headers: {
				'Content-Type': 'application/json',
				'X-GIT-Time': '1406090202',
				Authorization: 'GIT WEB:WEB'
			},
			data: JSON.stringify({
				_token: csrfToken,
				room_id: thisId,
				type: bookingType,
				checkin: checkItems.startDate,
				checkout: checkItems.endDate,
				duration: +checkItems.duration,
				room_total: +checkItems.totalRoom,
				guest_total: +selectItems.totalGuest,
				contact_name: contactData.name,
				contact_phone: contactData.phone_number,
				contact_email: contactData.email,
				guest: guestsData
			}),
			crossDomain: true,
			dataType: 'json',
			success: function(response) {
				if (response.status) {
					//open booking detail (payment and receipt)
					window.open('/booking/history/' + response.data.booking_data.booking_code, '_self');
				} else {
					modalLoadingOff();
					showError(response.messages);
				}
			},
			statusCode: statusCodeNoReload
		});
	}
};

var rewriteGuest = function() {
	page.section = 'detail';
};