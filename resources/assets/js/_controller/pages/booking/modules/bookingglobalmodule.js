var csrfToken = $('meta[name=csrf-token]').attr('content');

var thisId = parseInt($('meta[name=this-id]').attr('content'));

var thisTitle = $('meta[name=room-title]').attr('content');

var bookingType = $('meta[name=booking-type]').attr('content');

var page = {
	section: '',
	path: ''
};

var basePath = function() {
	page.path = window.location.pathname;
	if (page.path == '/booking') {
		page.section = 'select';
	} else if (page.path == '/booking/history') {
		page.section = 'history';
	}
};

var statusCodeReload = {
	408: function() {
		var err = confirm(
			'Koneksi internet bermasalah.\nPastikan perangkat terhubung dengan internet dan coba lagi.'
		);
		if (err) {
			location.reload();
		}
	},
	500: function() {
		var err = confirm('Galat tidak diketahui.\nSilakan coba lagi.');
		if (err) {
			location.reload();
		}
	}
};

var statusCodeNoReload = {
	408: function() {
		alert(
			'Koneksi internet bermasalah.\nPastikan perangkat terhubung dengan internet dan coba lagi.'
		);
		modalLoadingOff();
	},
	500: function() {
		alert('Galat tidak diketahui.\nSilakan coba lagi.');
		modalLoadingOff();
	}
};

var loadingStatus = false;

var modalLoadingOn = function() {
	$('#modalLoading').modal({
		backdrop: 'static',
		keyboard: false
	});
	booking.loadingStatus = true;
};

var modalLoadingOff = function() {
	$('#modalLoading').modal('hide');
	booking.loadingStatus = false;
};

var showError = function(errors) {
	var errorTxt = '<ul>';
	for (i = 0; i < errors.length; i++) {
		errorTxt += '<li>' + errors[i] + '</li>';
	}
	errorTxt += '</ul>';
	swal({
		title: '',
		type: 'error',
		html: errorTxt
	});
};

var initTooltip = function() {
	$('[data-toggle="tooltip"]').tooltip();
};