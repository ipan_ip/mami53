var bookingHistory = {
	data: {}
};

var getBookingHistory = function() {
	modalLoadingOn();
	$.ajax({
		type: 'GET',
		url: '/garuda/booking/history',
		headers: {
			'Content-Type': 'application/json',
			'X-GIT-Time': '1406090202',
			Authorization: 'GIT WEB:WEB'
		},
		crossDomain: true,
		dataType: 'json',
		success: function(response) {
			modalLoadingOff();
			if (response.status) {
				bookingHistory.data = response.data;
				bookingHistory.data.forEach(function(history, index) {
					if (
						history.payment_status == 'Tunggu Konfirmasi Admin' ||
						history.payment_status == 'Belum Dibayar' ||
						history.payment_status == 'Cancel' ||
						history.payment_status == 'Tunggu Verifikasi' ||
						history.payment_status == 'Expired' ||
						history.payment_status == 'Request Refund' ||
						history.payment_status == 'Di-refund'
					) {
						history.status = 0;
					} else {
						history.status = 1;
					}
				});
			} else {
				showError(response.messages);
			}
		},
		statusCode: statusCodeReload
	});
};