var cancelReason = {
	content: ''
};

var submitCancelAlert = function() {
	swal({
		title: '',
		text: 'Terima kasih. Pembatalan booking Anda segera kami proses.',
		type: 'info',
		timer: 2000
	}).then(
		function() {
			window.location.replace('/booking/history');
		},
		function(dismiss) {
			if (dismiss === 'timer') {
				window.location.replace('/booking/history');
			}
		}
	);
};

var initBookingCancel = function() {
	centerModal();
	$('#cancelBookingModal').modal({
		backdrop: 'static',
		keyboard: false
	});
};

var submitBookingCancel = function() {
	$('#cancelBookingModal').modal('hide');
	modalLoadingOn();
	$.ajax({
		type: 'POST',
		url: '/garuda/booking/cancel/' + bookingCode.content,
		headers: {
			'Content-Type': 'application/json',
			'X-GIT-Time': '1406090202',
			Authorization: 'GIT WEB:WEB'
		},
		data: JSON.stringify({
			_token: csrfToken,
			cancel_reason: cancelReason.content
		}),
		crossDomain: true,
		dataType: 'json',
		success: function(response) {
			modalLoadingOff();
			if (response.status) {
				if (response.data.cancel_status == true) {
					submitCancelAlert();
				}
			} else {
				showError(response.messages);
			}
		},
		statusCode: statusCodeNoReload
	});
};

var initClaimGuarantee = function() {
	centerModal();
	$('#claimGuaranteeModal').modal({
		backdrop: 'static',
		keyboard: false
	});
};

var submitClaimGuarantee = function() {
	$('#cancelBookingModal').modal('hide');
	modalLoadingOn();
	$.ajax({
		type: 'POST',
		url: '/garuda/booking/guarantee/claim/' + bookingCode.content,
		headers: {
			'Content-Type': 'application/json',
			'X-GIT-Time': '1406090202',
			Authorization: 'GIT WEB:WEB'
		},
		data: JSON.stringify({
			_token: csrfToken,
			cancel_reason: cancelReason.content
		}),
		crossDomain: true,
		dataType: 'json',
		success: function(response) {
			modalLoadingOff();
			if (response.status) {
				if (response.data.cancel_status == true) {
					submitCancelAlert();
				}
			} else {
				showError(response.messages);
			}
		},
		statusCode: statusCodeNoReload
	});
};