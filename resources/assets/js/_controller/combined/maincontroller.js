/* ANGULAR SET UP */

/* Bug Snag */
angular
	.module('exceptionOverride', [])
	.factory('$exceptionHandler', function() {
		return function(exception, cause) {
			bugsnagClient.notify(exception);
		};
	});

angular
	.module('myApp', [
		'ngMap',
		'ngResource',
		'ngCookies',
		'ngFileUpload',
		'ui.bootstrap',
		'angular-preload-image'
	])

	/**
    Symbol {{ }} in angular is not compatible with {{ }} in blade laravel.
    So, symbol {{ }} in angular is changed to <% %>.

    Simbol {{ }} di angular akan bentrok dengan {{ }} di blade.
    Untuk itu, simbol {{ }} untuk binding pada angular diganti dengan <% %>
    https://scotch.io/tutorials/quick-tip-using-laravel-blade-with-angularjs

  **/
	.config([
		'$interpolateProvider',
		function($interpolateProvider) {
			$interpolateProvider.startSymbol('<%');
			$interpolateProvider.endSymbol('%>');
		}
	])

	// ==================================================
	// Factories
	// Untuk panggil API
	// For Calling API
	// ==================================================
	.factory('Room', function($resource) {
		return $resource(
			'/garuda/:api/:data',
			{
				api: 'stories',
				data: 'list'
			},
			{
				search: {
					method: 'POST',
					headers: {
						Authorization: 'GIT WEB:WEB',
						'X-GIT-Time': '1406090202',
						'Content-Type': 'application/json'
					}
				} //Headernya
			}
		);
	})
	//Url nya apa
	//Masukin parameter url nya/Adding url parameter
	/** "search is for action method//"search" hanya untuk nama action **/

	/**
        Room.search is for calling API stories/list (output: list rooms)
        Room.search untuk panggil API stories/list (untuk panggil list2 room pada homepage, landing, carikos, dll kalau diperlukan)
      **/

	.factory('callMe', function($resource) {
		return $resource(
			'/garuda/:api/:id/:call',
			{
				api: 'stories',
				id: '@id',
				call: 'call'
			},
			{
				call: {
					method: 'POST',
					headers: {
						Authorization: 'GIT WEB:WEB',
						'X-GIT-Time': '1406090202',
						'Content-Type': 'application/json'
					}
				}
			}
		);
	})

	/**
        callMe.call is for calling api "Subscribe". Save user name, email, hp
        callMe.call untuk panggil API subscribe, simpan nama, email, hp, dll user
      **/

	.factory('downloadApp', function($resource) {
		return $resource(
			'/garuda/:api/',
			{
				api: 'subscribe'
			},
			{
				subs: {
					method: 'POST',
					headers: {
						Authorization: 'GIT WEB:WEB',
						'X-GIT-Time': '1406090202',
						'Content-Type': 'application/json'
					}
				}
			}
		);
	})

	/**
        downloadApp.subs is for calling api "Subscribe". Save user name, email, hp
        downloadApp.subs untuk panggil API subscribe, simpan nama, email, hp, dll user
      **/

	.factory('showLocation', function($resource) {
		return $resource(
			'/garuda/:api/:loc/:id',
			{
				api: 'stories',
				loc: 'location',
				id: '@id'
			},
			{
				get: {
					method: 'GET',
					headers: {
						Authorization: 'GIT WEB:WEB',
						'X-GIT-Time': '1406090202',
						'Content-Type': 'application/json'
					}
				}
			}
		);
	})

	/**
        showLocation.get is for getting room's location, using room's location_id as parameter (@id)
        showLocation.get untuk mengetahui lokasi room (latitude, longitude), dengan @id adalah location_id dari room tersebut
      **/

	.factory('addRoom', function($resource) {
		return $resource(
			'/garuda/:api/:data',
			{
				api: 'stories',
				data: 'save_json_id'
			},
			{
				search: {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json',
						Authorization: 'GIT WEB:WEB',
						'X-GIT-Time': '1406090202'
					}
				}
			}
		);
	})

	/**
        addRoom.search is for saving data room from Form Mamikos
        addRoom.search untuk simpan data room (dipakai pada form, form agen, dan form agen2)
      **/

	.factory('editRoom', function($resource) {
		return $resource(
			'/garuda/:api/:path/:id',
			{
				api: 'owner',
				path: 'edit_room',
				id: '@id'
			},
			{
				search: {
					method: 'GET',
					headers: {
						'Content-Type': 'application/json',
						Authorization: 'GIT WEB:WEB',
						'X-GIT-Time': '1406090202'
					}
				}
			}
		);
	})

	.factory('updateRoom', function($resource) {
		return $resource(
			'/garuda/:api/:path/:id',
			{
				api: 'owner',
				path: 'update_room',
				id: '@id'
			},
			{
				search: {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json',
						Authorization: 'GIT WEB:WEB',
						'X-GIT-Time': '1406090202'
					}
				}
			}
		);
	})

	.factory('draftRoom', function($resource) {
		return $resource(
			'/garuda/:api/:path/:draft/:update/:id',
			{
				api: 'owner',
				path: 'update_room',
				draft: 'draft',
				update: 'update',
				id: '@id'
			},
			{
				search: {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json',
						Authorization: 'GIT WEB:WEB',
						'X-GIT-Time': '1406090202'
					}
				}
			}
		);
	})

	.factory('addRoomByOwner', function($resource) {
		return $resource(
			'/garuda/:api/:path/:action/:id',
			{
				api: 'owner',
				path: 'room',
				action: 'save',
				id: '@id'
			},
			{
				search: {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json',
						Authorization: 'GIT WEB:WEB',
						'X-GIT-Time': '1406090202'
					}
				}
			}
		);
	})

	.factory('addRoomDraft', function($resource) {
		return $resource(
			'/garuda/:api/:path/:action/:id',
			{
				api: 'owner',
				path: 'room',
				action: 'draft',
				id: '@id'
			},
			{
				search: {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json',
						Authorization: 'GIT WEB:WEB',
						'X-GIT-Time': '1406090202'
					}
				}
			}
		);
	})

	.factory('checkName', function($resource) {
		return $resource(
			'/garuda/:api/:data',
			{
				api: 'kost',
				data: 'name',
				name: '@name'
			},
			{
				getname: {
					method: 'GET',
					headers: {
						'Content-Type': 'application/json',
						Authorization: 'GIT WEB:WEB',
						'X-GIT-Time': '1406090202'
					}
				}
			}
		);
	})

	/**
        checkName.getname is for checking available room's name for new data
        checkName.getname untuk check nama room/kost pada form apakah sudah pernah digunakan atau belum. keluarannya is_used true/false
      **/

	.factory('Cluster', function($resource) {
		return $resource(
			'/garuda/:api/:data',
			{
				api: 'stories',
				data: 'cluster'
			},
			{
				search: {
					method: 'POST',
					headers: {
						Authorization: 'GIT WEB:WEB',
						'X-GIT-Time': '1406090202',
						'Content-Type': 'application/json'
					}
				}
			}
		);
	})
	/**
        Cluster.search is for calling api cluster (like stories/list) with cluster as results
        Cluster.search untuk panggil api cluster berdasarkan parameter (mirip dengan stories/list (Room.search)), untuk di /carikos.
      **/

	.factory('ClusterList', function($resource) {
		return $resource(
			'/garuda/:api/:data/:content',
			{
				api: 'room',
				data: 'list',
				content: 'cluster'
			},
			{
				list: {
					method: 'POST',
					headers: {
						Authorization: 'GIT WEB:WEB',
						'X-GIT-Time': '1406090202',
						'Content-Type': 'application/json'
					}
				}
			}
		);
	})
	/**
        ClusterList.list is for calling list rooms from one cluster
        ClusterList.list untuk panggil api list dari cluster yang dipilih
      **/
	.factory('Subscribe', function($resource) {
		return $resource(
			'/garuda/:api',
			{
				api: 'subscribe'
			},
			{
				subs: {
					method: 'POST',
					headers: {
						Authorization: 'GIT WEB:WEB',
						'X-GIT-Time': '1406090202',
						'Content-Type': 'application/json'
					}
				}
			}
		);
	})
	/**
        Subscribe.subs is for calling api "Subscribe". Save user name, email, hp
        Subscribe.subs untuk panggil API subscribe, simpan nama, email, hp, dll user
      **/
	.factory('SubscribeFilter', function($resource) {
		return $resource(
			'/garuda/:api/:data',
			{
				api: 'stories',
				data: 'search_alarm'
			},
			{
				subs: {
					method: 'POST',
					headers: {
						Authorization: 'GIT WEB:WEB',
						'X-GIT-Time': '1406090202',
						'Content-Type': 'application/json'
					}
				}
			}
		);
	})
	.factory('SubscribeSuggestion', function($resource) {
		return $resource(
			'/garuda/:api/:data',
			{
				api: 'stories',
				data: 'subscribe_suggestion'
			},
			{
				subs: {
					method: 'POST',
					headers: {
						Authorization: 'GIT WEB:WEB',
						'X-GIT-Time': '1406090202',
						'Content-Type': 'application/json'
					}
				}
			}
		);
	})
	/**
        SubscribeFilter.subs is for saving user's filter for recommendation room
        SubscribeFilter.subs untuk simpan data filter apa aja untuk rekomendasi user
      **/
	.factory('Area', function($resource) {
		return $resource(
			'/json/:file',
			{
				file: 'home-area.json'
			},
			{
				get: {
					method: 'GET'
				}
			}
		);
	})

	.factory('Favorite', function($resource) {
		return $resource(
			'/garuda/:api/:data/',
			{
				api: 'stories',
				data: 'love',
				devel_access_token:
					'858927937bd4a94bd06f5a2bb87f95f62241390ec4158b9b01becbee3d1d31d6'
			},
			{
				get: {
					method: 'get',
					headers: {
						Authorization: 'GIT devel:',
						'X-GIT-Time': '1406090202',
						'Content-Type': 'application/json'
					}
				}
			}
		);
	})

	.factory('Recommendation', function($resource) {
		return $resource(
			'/garuda/:api',
			{
				api: 'recommendation',
				devel_access_token:
					'858927937bd4a94bd06f5a2bb87f95f62241390ec4158b9b01becbee3d1d31d6'
			},
			{
				get: {
					method: 'get',
					headers: {
						Authorization: 'GIT WEB:WEB',
						'X-GIT-Time': '1406090202',
						'Content-Type': 'application/json'
					}
				}
			}
		);
	})
	// ==================================================
	// Directives
	// ==================================================

	.directive('formTemplate', function() {
		return {
			restrict: 'E',
			scope: {
				var: '=', //sama dengan var:'=var'
				model: '=',
				notes: '=notes',
				gender: '=gender',
				priceFrom: '=price1', //priceFrom nanti yang akan dipanggil di template (template/carikankost2.html)
				//lalu disamakan dengan price1 yang ada di dalam tag <form-template price1="..">
				price2: '=price2',
				dates: '=dates',
				duration: '=duration',
				city: '=city',
				facility: '=facility'
			},
			template: '<div ng-include="var"></div>',
			//ng-include bisa menambahkan isi sebuah template (include template)
			//var adalah $scope.var, yang ketika diganti2 isinya dengan url template
			//maka include nya pun berubah sesuai dengan url nya
			controller: 'carikankostController' //controller yang dipakai
		};
	})

	/**
        formTemplate is used in carikan saya kost page,for changing template form
        formTemplate ini dipakai pada halaman carikan saya kost, untuk ganti2 template pada form
        template yang ada di public/templates.
      **/

	.filter('startFrom', function() {
		return function(input, start) {
			if (input) {
				start = +start; //parse to int
				return input.slice(start);
			}
			return [];
		};
	});
