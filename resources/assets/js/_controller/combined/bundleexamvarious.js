/* ANGULAR SET UP */

/* Bug Snag */
angular
	.module('exceptionOverride', [])
	.factory('$exceptionHandler', function() {
		return function(exception, cause) {
			bugsnagClient.notify(exception);
		};
	});

angular
	.module('myApp', [
		'ngMap',
		'ngResource',
		'ngCookies',
		'ngFileUpload',
		'ui.bootstrap',
		'angular-preload-image'
	])

	/**
    Symbol {{ }} in angular is not compatible with {{ }} in blade laravel.
    So, symbol {{ }} in angular is changed to <% %>.

    Simbol {{ }} di angular akan bentrok dengan {{ }} di blade.
    Untuk itu, simbol {{ }} untuk binding pada angular diganti dengan <% %>
    https://scotch.io/tutorials/quick-tip-using-laravel-blade-with-angularjs

  **/
	.config([
		'$interpolateProvider',
		function($interpolateProvider) {
			$interpolateProvider.startSymbol('<%');
			$interpolateProvider.endSymbol('%>');
		}
	])

	// ==================================================
	// Factories
	// Untuk panggil API
	// For Calling API
	// ==================================================
	.factory('Room', function($resource) {
		return $resource(
			'/garuda/:api/:data',
			{
				api: 'stories',
				data: 'list'
			},
			{
				search: {
					method: 'POST',
					headers: {
						Authorization: 'GIT WEB:WEB',
						'X-GIT-Time': '1406090202',
						'Content-Type': 'application/json'
					}
				} //Headernya
			}
		);
	})
	//Url nya apa
	//Masukin parameter url nya/Adding url parameter
	/** "search is for action method//"search" hanya untuk nama action **/

	/**
        Room.search is for calling API stories/list (output: list rooms)
        Room.search untuk panggil API stories/list (untuk panggil list2 room pada homepage, landing, carikos, dll kalau diperlukan)
      **/

	.factory('callMe', function($resource) {
		return $resource(
			'/garuda/:api/:id/:call',
			{
				api: 'stories',
				id: '@id',
				call: 'call'
			},
			{
				call: {
					method: 'POST',
					headers: {
						Authorization: 'GIT WEB:WEB',
						'X-GIT-Time': '1406090202',
						'Content-Type': 'application/json'
					}
				}
			}
		);
	})

	/**
        callMe.call is for calling api "Subscribe". Save user name, email, hp
        callMe.call untuk panggil API subscribe, simpan nama, email, hp, dll user
      **/

	.factory('downloadApp', function($resource) {
		return $resource(
			'/garuda/:api/',
			{
				api: 'subscribe'
			},
			{
				subs: {
					method: 'POST',
					headers: {
						Authorization: 'GIT WEB:WEB',
						'X-GIT-Time': '1406090202',
						'Content-Type': 'application/json'
					}
				}
			}
		);
	})

	/**
        downloadApp.subs is for calling api "Subscribe". Save user name, email, hp
        downloadApp.subs untuk panggil API subscribe, simpan nama, email, hp, dll user
      **/

	.factory('showLocation', function($resource) {
		return $resource(
			'/garuda/:api/:loc/:id',
			{
				api: 'stories',
				loc: 'location',
				id: '@id'
			},
			{
				get: {
					method: 'GET',
					headers: {
						Authorization: 'GIT WEB:WEB',
						'X-GIT-Time': '1406090202',
						'Content-Type': 'application/json'
					}
				}
			}
		);
	})

	/**
        showLocation.get is for getting room's location, using room's location_id as parameter (@id)
        showLocation.get untuk mengetahui lokasi room (latitude, longitude), dengan @id adalah location_id dari room tersebut
      **/

	.factory('addRoom', function($resource) {
		return $resource(
			'/garuda/:api/:data',
			{
				api: 'stories',
				data: 'save_json_id'
			},
			{
				search: {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json',
						Authorization: 'GIT WEB:WEB',
						'X-GIT-Time': '1406090202'
					}
				}
			}
		);
	})

	/**
        addRoom.search is for saving data room from Form Mamikos
        addRoom.search untuk simpan data room (dipakai pada form, form agen, dan form agen2)
      **/

	.factory('editRoom', function($resource) {
		return $resource(
			'/garuda/:api/:path/:id',
			{
				api: 'owner',
				path: 'edit_room',
				id: '@id'
			},
			{
				search: {
					method: 'GET',
					headers: {
						'Content-Type': 'application/json',
						Authorization: 'GIT WEB:WEB',
						'X-GIT-Time': '1406090202'
					}
				}
			}
		);
	})

	.factory('updateRoom', function($resource) {
		return $resource(
			'/garuda/:api/:path/:id',
			{
				api: 'owner',
				path: 'update_room',
				id: '@id'
			},
			{
				search: {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json',
						Authorization: 'GIT WEB:WEB',
						'X-GIT-Time': '1406090202'
					}
				}
			}
		);
	})

	.factory('draftRoom', function($resource) {
		return $resource(
			'/garuda/:api/:path/:draft/:update/:id',
			{
				api: 'owner',
				path: 'update_room',
				draft: 'draft',
				update: 'update',
				id: '@id'
			},
			{
				search: {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json',
						Authorization: 'GIT WEB:WEB',
						'X-GIT-Time': '1406090202'
					}
				}
			}
		);
	})

	.factory('addRoomByOwner', function($resource) {
		return $resource(
			'/garuda/:api/:path/:action/:id',
			{
				api: 'owner',
				path: 'room',
				action: 'save',
				id: '@id'
			},
			{
				search: {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json',
						Authorization: 'GIT WEB:WEB',
						'X-GIT-Time': '1406090202'
					}
				}
			}
		);
	})

	.factory('addRoomDraft', function($resource) {
		return $resource(
			'/garuda/:api/:path/:action/:id',
			{
				api: 'owner',
				path: 'room',
				action: 'draft',
				id: '@id'
			},
			{
				search: {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json',
						Authorization: 'GIT WEB:WEB',
						'X-GIT-Time': '1406090202'
					}
				}
			}
		);
	})

	.factory('checkName', function($resource) {
		return $resource(
			'/garuda/:api/:data',
			{
				api: 'kost',
				data: 'name',
				name: '@name'
			},
			{
				getname: {
					method: 'GET',
					headers: {
						'Content-Type': 'application/json',
						Authorization: 'GIT WEB:WEB',
						'X-GIT-Time': '1406090202'
					}
				}
			}
		);
	})

	/**
        checkName.getname is for checking available room's name for new data
        checkName.getname untuk check nama room/kost pada form apakah sudah pernah digunakan atau belum. keluarannya is_used true/false
      **/

	.factory('Cluster', function($resource) {
		return $resource(
			'/garuda/:api/:data',
			{
				api: 'stories',
				data: 'cluster'
			},
			{
				search: {
					method: 'POST',
					headers: {
						Authorization: 'GIT WEB:WEB',
						'X-GIT-Time': '1406090202',
						'Content-Type': 'application/json'
					}
				}
			}
		);
	})
	/**
        Cluster.search is for calling api cluster (like stories/list) with cluster as results
        Cluster.search untuk panggil api cluster berdasarkan parameter (mirip dengan stories/list (Room.search)), untuk di /carikos.
      **/

	.factory('ClusterList', function($resource) {
		return $resource(
			'/garuda/:api/:data/:content',
			{
				api: 'room',
				data: 'list',
				content: 'cluster'
			},
			{
				list: {
					method: 'POST',
					headers: {
						Authorization: 'GIT WEB:WEB',
						'X-GIT-Time': '1406090202',
						'Content-Type': 'application/json'
					}
				}
			}
		);
	})
	/**
        ClusterList.list is for calling list rooms from one cluster
        ClusterList.list untuk panggil api list dari cluster yang dipilih
      **/
	.factory('Subscribe', function($resource) {
		return $resource(
			'/garuda/:api',
			{
				api: 'subscribe'
			},
			{
				subs: {
					method: 'POST',
					headers: {
						Authorization: 'GIT WEB:WEB',
						'X-GIT-Time': '1406090202',
						'Content-Type': 'application/json'
					}
				}
			}
		);
	})
	/**
        Subscribe.subs is for calling api "Subscribe". Save user name, email, hp
        Subscribe.subs untuk panggil API subscribe, simpan nama, email, hp, dll user
      **/
	.factory('SubscribeFilter', function($resource) {
		return $resource(
			'/garuda/:api/:data',
			{
				api: 'stories',
				data: 'search_alarm'
			},
			{
				subs: {
					method: 'POST',
					headers: {
						Authorization: 'GIT WEB:WEB',
						'X-GIT-Time': '1406090202',
						'Content-Type': 'application/json'
					}
				}
			}
		);
	})
	.factory('SubscribeSuggestion', function($resource) {
		return $resource(
			'/garuda/:api/:data',
			{
				api: 'stories',
				data: 'subscribe_suggestion'
			},
			{
				subs: {
					method: 'POST',
					headers: {
						Authorization: 'GIT WEB:WEB',
						'X-GIT-Time': '1406090202',
						'Content-Type': 'application/json'
					}
				}
			}
		);
	})
	/**
        SubscribeFilter.subs is for saving user's filter for recommendation room
        SubscribeFilter.subs untuk simpan data filter apa aja untuk rekomendasi user
      **/
	.factory('Area', function($resource) {
		return $resource(
			'/json/:file',
			{
				file: 'home-area.json'
			},
			{
				get: {
					method: 'GET'
				}
			}
		);
	})

	.factory('Favorite', function($resource) {
		return $resource(
			'/garuda/:api/:data/',
			{
				api: 'stories',
				data: 'love',
				devel_access_token:
					'858927937bd4a94bd06f5a2bb87f95f62241390ec4158b9b01becbee3d1d31d6'
			},
			{
				get: {
					method: 'get',
					headers: {
						Authorization: 'GIT devel:',
						'X-GIT-Time': '1406090202',
						'Content-Type': 'application/json'
					}
				}
			}
		);
	})

	.factory('Recommendation', function($resource) {
		return $resource(
			'/garuda/:api',
			{
				api: 'recommendation',
				devel_access_token:
					'858927937bd4a94bd06f5a2bb87f95f62241390ec4158b9b01becbee3d1d31d6'
			},
			{
				get: {
					method: 'get',
					headers: {
						Authorization: 'GIT WEB:WEB',
						'X-GIT-Time': '1406090202',
						'Content-Type': 'application/json'
					}
				}
			}
		);
	})
	// ==================================================
	// Directives
	// ==================================================

	.directive('formTemplate', function() {
		return {
			restrict: 'E',
			scope: {
				var: '=', //sama dengan var:'=var'
				model: '=',
				notes: '=notes',
				gender: '=gender',
				priceFrom: '=price1', //priceFrom nanti yang akan dipanggil di template (template/carikankost2.html)
				//lalu disamakan dengan price1 yang ada di dalam tag <form-template price1="..">
				price2: '=price2',
				dates: '=dates',
				duration: '=duration',
				city: '=city',
				facility: '=facility'
			},
			template: '<div ng-include="var"></div>',
			//ng-include bisa menambahkan isi sebuah template (include template)
			//var adalah $scope.var, yang ketika diganti2 isinya dengan url template
			//maka include nya pun berubah sesuai dengan url nya
			controller: 'carikankostController' //controller yang dipakai
		};
	})

	/**
        formTemplate is used in carikan saya kost page,for changing template form
        formTemplate ini dipakai pada halaman carikan saya kost, untuk ganti2 template pada form
        template yang ada di public/templates.
      **/

	.filter('startFrom', function() {
		return function(input, start) {
			if (input) {
				start = +start; //parse to int
				return input.slice(start);
			}
			return [];
		};
	})
	.controller('examControllerVarious', [
		'$scope',
		'$window',
		function($scope, $window) {
			$scope._token = $('meta[name="csrf-token"]').attr('content');

			//cek apakah user sudah login
			$scope.authCheck = authCheck;

			$scope.savedStudent = Cookies.getJSON('examDownloadData');
			$scope.userdataExam = Cookies.getJSON('userdataExamDownload');

			if (isSubscribed !== '') {
				$scope.showContent = true;
			} else {
				$scope.showContent = false;
			}

			$scope.student = {
				name: '',
				email: '',
				phone: '',
				filters: {},
				location: [],
				user_detail: {}
			};

			$scope.city = '';
			$scope.university = '';
			$scope.destination = '';

			$scope.gender = '0,1';

			$scope.filters = {
				gender: [],
				price_range: [100000, 2000000]
			};

			$scope.location = [
				[106.800156, -6.618229],
				[106.933365, -6.113246]
			];

			$scope.selectedLocation = {
				latitude: '',
				longitude: ''
			};

			if ($scope.authCheck.all) {
				$scope.expiredDownloadToken = md5(expiredDownloadToken);

				if ($scope.userdataExam) {
					if ($scope.userdataExam.user_id == null) {
						$.ajax({
							type: 'POST',
							url: '/garuda/stories/subscribe_download/update',
							headers: {
								'Content-Type': 'application/json',
								'X-GIT-Time': '1406090202',
								Authorization: 'GIT WEB:WEB'
							},
							data: JSON.stringify({
								_seq: $scope.userdataExam.exam_id,
								_token: $scope._token
							}),
							crossDomain: true,
							dataType: 'json',
							success: function(response) {
								if (response.status == true) {
									Cookies.remove('examDownloadData', {
										path: '/download-soal/' + slug
									});
									Cookies.remove('userdataExamDownload', {
										path: '/download-soal/' + slug
									});
									Cookies.set(
										'expiredExamDownload',
										$scope.expiredDownloadToken,
										{
											expires: 7,
											path: '/download-soal/' + slug
										}
									);

									swal('', 'Data berhasil dikirim.', 'success');

									$scope.showContent = true;
									setTimeout(function() {
										// $scope.showContent = true;
										location.reload();
									}, 500);
								} else {
									swal('', response.meta.message, 'error');
								}
							}
						});
					}
				} else if ($scope.savedStudent) {
					$.ajax({
						type: 'POST',
						url: '/garuda/stories/subscribe_download',
						headers: {
							'Content-Type': 'application/json',
							'X-GIT-Time': '1406090202',
							Authorization: 'GIT WEB:WEB'
						},
						data: JSON.stringify({
							name: $scope.savedStudent.name,
							email: $scope.savedStudent.email,
							phone: $scope.savedStudent.phone,
							filters: $scope.savedStudent.filters,
							location: $scope.savedStudent.location,
							form_detail: $scope.savedStudent.user_detail,
							_token: $scope._token
						}),
						crossDomain: true,
						dataType: 'json',
						success: function(response) {
							if (response.status == true) {
								Cookies.remove('examDownloadData', {
									path: '/download-soal/' + slug
								});
								Cookies.set(
									'expiredExamDownload',
									$scope.expiredDownloadToken,
									{
										expires: 7,
										path: '/download-soal/' + slug
									}
								);

								swal('', 'Login sukses. Silakan download file.', 'success');

								$scope.showContent = true;
								setTimeout(function() {
									// $scope.showContent = true;
									location.reload();
								}, 500);
							} else {
								swal(
									'',
									'Terjadi kesalahan. Reload halaman dan coba lagi.',
									'error'
								);
							}

							tracker('moe', [
								'[User] Download Soal',
								{
									success_status: response.status,
									username: $scope.savedStudent.name
										? $scope.savedStudent.name
										: null,
									user_mail: $scope.savedStudent.email
										? $scope.savedStudent.email
										: null,
									user_phone_number: $scope.savedStudent.phone
										? $scope.savedStudent.phone
										: null,
									university_name: $scope.savedStudent.user_detail.university
										? $scope.savedStudent.user_detail.university
										: null,
									location: $scope.savedStudent.location
										? $scope.savedStudent.location
										: null,
									gender: $scope.savedStudent.user_detail.gender
										? $scope.savedStudent.user_detail.gender
										: null,
									destination: $scope.savedStudent.user_detail.destination
										? $scope.savedStudent.user_detail.destination
										: null
								}
							]);
						}
					});
				}

				$scope.expiredExamDownload = Cookies.get('expiredExamDownload');
				if ($scope.expiredDownloadToken == $scope.expiredExamDownload) {
					// $('#downloadSoal').modal({
					// 	backdrop: 'static',
					// 	keyboard: false
					// });
					$scope.showContent = true;
				}
			} else {
				$scope.showContent = false;
				Cookies.remove('examDownloadData', { path: '/download-soal/' + slug });
			}

			$scope.placeChanged = function() {
				$scope.place = this.getPlace();
				if ($scope.place.geometry === undefined) {
				} else {
					$scope.selectedLocation.latitude = $scope.place.geometry.location.lat();
					$scope.selectedLocation.longitude = $scope.place.geometry.location.lng();

					$scope.centerz = [
						$scope.selectedLocation.latitude,
						$scope.selectedLocation.longitude
					];

					//Earth’s radius, sphere
					earthRadius = 6378137;

					//offsets in meters
					moveRange = 5000;

					//Coordinate offsets in radians
					dLat = moveRange / earthRadius;
					dLon =
						moveRange /
						(earthRadius * Math.cos((Math.PI * $scope.centerz[0]) / 180));

					//OffsetPosition, decimal degrees
					latTopRight = $scope.centerz[0] + (dLat * 180) / Math.PI;
					lonTopRight = $scope.centerz[1] + (dLon * 180) / Math.PI;

					latBottomLeft = $scope.centerz[0] - (dLat * 180) / Math.PI;
					lonBottomLeft = $scope.centerz[1] - (dLon * 180) / Math.PI;

					//arary pertama -- (batas kiri bawah), array kedua harus ++(batas kanan atas)
					$scope.location = [
						[lonBottomLeft, latBottomLeft],
						[lonTopRight, latTopRight]
					];
				}
			};

			$scope.submitData = function() {
				if (
					$scope.student.name.trim() !== '' &&
					$scope.student.email.trim() !== '' &&
					$scope.student.phone.trim() !== '' &&
					$scope.city.trim() !== '' &&
					$scope.destination.trim() !== ''
				) {
					$scope.allowSubmit = true;
				} else {
				}

				if ($scope.allowSubmit) {
					if ($scope.gender == '0,1') {
						$scope.filters.gender = [0, 1];
					} else {
						$scope.filters.gender = [0, 2];
					}

					$scope.user_detail = {
						exam_id: _seq,
						name: $scope.student.name,
						email: $scope.student.email,
						phone_number: $scope.student.phone,
						current_location: $scope.city,
						university: $scope.university,
						destination: $scope.destination,
						gender: $scope.gender
					};

					$scope.student.filters = $scope.filters;
					$scope.student.location = $scope.location;
					$scope.student.user_detail = $scope.user_detail;

					if ($scope.authCheck.all) {
						$.ajax({
							type: 'POST',
							url: '/garuda/stories/subscribe_download',
							headers: {
								'Content-Type': 'application/json',
								'X-GIT-Time': '1406090202',
								Authorization: 'GIT WEB:WEB'
							},
							data: JSON.stringify({
								name: $scope.student.name,
								email: $scope.student.email,
								phone: $scope.student.phone,
								filters: $scope.student.filters,
								location: $scope.student.location,
								form_detail: $scope.student.user_detail,
								_token: $scope._token
							}),
							crossDomain: true,
							dataType: 'json',
							success: function(response) {
								tracker('moe', [
									'[User] Download Soal',
									{
										success_status: response.status,
										username: $scope.student.name ? $scope.student.name : null,
										user_mail: $scope.student.email
											? $scope.student.email
											: null,
										user_phone_number: $scope.student.phone
											? $scope.student.phone
											: null,
										university_name: $scope.student.user_detail.university
											? $scope.student.user_detail.university
											: null,
										location: $scope.student.location
											? $scope.student.location
											: null,
										gender: $scope.student.user_detail.gender
											? $scope.student.user_detail.gender
											: null,
										destination: $scope.student.user_detail.destination
											? $scope.student.user_detail.destination
											: null
									}
								]);

								Cookies.set(
									'expiredExamDownload',
									$scope.expiredDownloadToken,
									{
										expires: 7,
										path: '/download-soal/' + slug
									}
								);
								swal(
									'',
									'Submit data berhasil. Silakan download file.',
									'success'
								);

								$scope.showContent = true;
								location.reload();
							}
						});
					} else {
						$.ajax({
							type: 'POST',
							url: '/garuda/stories/subscribe_download',
							headers: {
								'Content-Type': 'application/json',
								'X-GIT-Time': '1406090202',
								Authorization: 'GIT WEB:WEB'
							},
							data: JSON.stringify({
								name: $scope.student.name,
								email: $scope.student.email,
								phone: $scope.student.phone,
								filters: $scope.student.filters,
								location: $scope.student.location,
								form_detail: $scope.student.user_detail,
								_token: $scope._token
							}),
							crossDomain: true,
							dataType: 'json',
							success: function(response) {
								tracker('moe', [
									'[User] Download Soal',
									{
										success_status: response.status,
										username: $scope.student.name ? $scope.student.name : null,
										user_mail: $scope.student.email
											? $scope.student.email
											: null,
										user_phone_number: $scope.student.phone
											? $scope.student.phone
											: null,
										university_name: $scope.student.user_detail.university
											? $scope.student.user_detail.university
											: null,
										location: $scope.student.location
											? $scope.student.location
											: null,
										gender: $scope.student.user_detail.gender
											? $scope.student.user_detail.gender
											: null,
										destination: $scope.student.user_detail.destination
											? $scope.student.user_detail.destination
											: null
									}
								]);

								Cookies.set(
									'userdataExamDownload',
									{
										user_id: response.input_result.user_id,
										exam_id: response.input_result.id
									},
									{
										expires: 1,
										path: '/download-soal/' + slug
									}
								);
							}
						});

						$('#login').modal('show');
						$('.user-login-email').val($scope.student.email);
						$('.user-login-phone').val($scope.student.phone);
						//save subscribe data to cookies
						var in30Minutes = 1 / 48;
						Cookies.set('examDownloadData', $scope.student, {
							expires: in30Minutes,
							path: '/download-soal/' + slug
						});
					}
				}
			};

			$scope.jobSuggestion = [];
			$scope.jobUrl = {};

			$scope.getSuggestion = function() {
				var suggestionFilter = {
					ids: '',
					moreUrl: '',
					moreText: ''
				};
				var suggestion = document.querySelector('vacancy-suggestion');
				if (suggestion != null) {
					suggestionFilter.ids = suggestion.getAttribute('ids').split(',');
					suggestionFilter.moreUrl = suggestion.getAttribute('moreurl');
					suggestionFilter.moreText = suggestion.getAttribute('moretext');
				}

				if (suggestionFilter.moreUrl != null) {
					$scope.jobUrl.url = suggestionFilter.moreUrl;
					if (suggestionFilter.moreText != null) {
						$scope.jobUrl.text = suggestionFilter.moreText;
					} else {
						$scope.jobUrl.text = 'Lihat lowongan pekerjaaan lainnya';
					}
				} else {
					$scope.jobUrl.url = '/loker';
					$scope.jobUrl.text = 'Lihat lowongan pekerjaaan lainnya';
				}

				if (suggestion != null) {
					$.ajax({
						type: 'POST',
						url: '/garuda/stories/jobs/list',
						headers: {
							'Content-Type': 'application/json',
							'X-GIT-Time': '1406090202',
							Authorization: 'GIT WEB:WEB'
						},
						data: JSON.stringify({
							filters: {
								ids: suggestionFilter.ids
							},
							referer: slug,
							limit: 5,
							offset: 0,
							_token: $scope._token
						}),
						crossDomain: true,
						dataType: 'json',
						success: function(response) {
							if (response.total > 0) {
								$scope.jobSuggestion = $scope.jobSuggestion.concat(
									response.vacancy
								);
								$scope.$apply();
							}
						}
					});
				}
			};

			if ($scope.authCheck.all && isSubscribed !== '') {
				$scope.getSuggestion();
			}

			$scope.openDetailVacancy = function(slug) {
				window.open('/lowongan/' + slug, '_blank');
			};
		}
	]);
