import 'Js/@mixins/MixinNavigatorIsMobile';
import mixinAuth from 'Js/@mixins/MixinAuth';
import id from 'vee-validate/dist/locale/id';
import VeeValidate, { Validator } from 'vee-validate';
Validator.localize('id', id);
Vue.use(VeeValidate, {
	locale: 'id'
});

Vue.component('app', () => import('./components/App.vue'));

const app = new Vue({
	el: '#app',
	mixins: [mixinAuth],
	data() {
		return {
			state: {
				token: document.head.querySelector('meta[name="csrf-token"]').content,
				authCheck: {},
				authData: {}
			}
		};
	},
	mounted() {
		try {
			document.getElementById('preloading').remove();
		} catch (e) {
			document.getElementById('preloading').style.display = 'none';
		}
	}
});
