require('Js/starter2');

window.debounce = require('lodash/debounce');

window.Vue = require('vue').default;

import Vuex from 'vuex';
Vue.use(Vuex);

import VueRouter from 'vue-router';
Vue.use(VueRouter);

import mixinAuth from 'Js/@mixins/MixinAuth';

require('Js/@mixins/MixinCenterModal');
require('Js/@mixins/MixinScrollTop');

const App = require('./components/company-landing/App.vue').default;
const ContainerCompanyLanding = require('./components/company-landing/ContainerCompanyLanding.vue')
	.default;

// Container Component
Vue.component('app', App);
Vue.component('container-company-landing', ContainerCompanyLanding);

// Global Components
Vue.component(
	'breadcrumb-trails',
	require('Js/@components/BreadcrumbTrails.vue').default
);
Vue.component(
	'mami-loading-inline',
	require('Js/@components/MamiLoadingInline.vue').default
);
Vue.component(
	'list-placeholder',
	require('Js/@components/ListPlaceholder.vue').default
);
// List Components
Vue.component(
	'company-list',
	require('./components/company-related/CompanyList.vue').default
);

// Local Components
Vue.component(
	'company-landing-title',
	require('./components/company-landing/CompanyLandingTitle.vue').default
);
Vue.component(
	'company-landing-filter',
	require('./components/company-landing/CompanyLandingFilter.vue').default
);

const landingListPagination = require('./components/company-landing/LandingListPagination.vue')
	.default;

const store = new Vuex.Store({
	state: {
		authCheck: {},
		authData: {},
		industryOptions: {},
		article: ''
	},
	mutations: {
		setAuthCheck(state, payload) {
			state.authCheck = payload;
		},
		setAuthData(state, payload) {
			state.authData = payload;
		},
		setIndustryOptions(state, payload) {
			state.industryOptions = payload;
		},
		setArticle(state, payload) {
			state.article = payload;
		}
	}
});

// Router
const routes = [{ path: '/perusahaan', component: landingListPagination }];

const router = new VueRouter({
	mode: 'history',
	routes
});

Vue.config.productionTip = false;

const app = new Vue({
	el: '#app',
	mixins: [mixinAuth],
	router,
	store,
	created() {
		this.$store.commit('setIndustryOptions', industryOptions);
		this.$store.commit('setArticle', article);
	}
});
