require('Js/starter2');

window.debounce = require('lodash/debounce');

window.Vue = require('vue').default;

import Vuex from 'vuex';
Vue.use(Vuex);

import id from 'vee-validate/dist/locale/id';
import VeeValidate, { Validator } from 'vee-validate';
window.validator = new Validator();
Validator.localize('id', id);
Vue.use(VeeValidate, {
	locale: 'id'
});

import mixinAuth from 'Js/@mixins/MixinAuth';

require('Js/@mixins/MixinScrollTop');
require('Js/@mixins/MixinSwalLoading');
require('Js/@mixins/MixinSwalSuccessError');

const App = require('./components/detail/App.vue').default;

// Container Component
Vue.component('app', App);

const store = new Vuex.Store({
	state: {
		token: document.head.querySelector('meta[name="csrf-token"]').content,
		authCheck: {},
		authData: {},
		slug: document.head.querySelector('meta[name="slug"]').content,
		verificationStatus: document.head.querySelector('meta[name="verification"]')
			.content,
		verificationFor: document.head.querySelector(
			'meta[name="verification-for"]'
		).content,
		section: 1,
		detail: {},
		params: {
			_token: document.head.querySelector('meta[name="csrf-token"]').content,
			owner_name: '',
			owner_email: '',
			insert_phone: '',
			address: '',
			education: '',
			skill: '',
			job_experience: '',
			last_salary: '',
			jobs_salary: '',
			workplace_status: 0,
			cv: ''
		},
		extras: {}
	},
	mutations: {
		setAuthCheck(state, payload) {
			state.authCheck = payload;
		},
		setAuthData(state, payload) {
			state.authData = payload;
		},
		changeSection(state, payload) {
			state.section = payload;
		},
		setDetail(state, payload) {
			state.detail = payload;
		},
		setParamsOwnerName(state, payload) {
			state.params.owner_name = payload;
		},
		setParamsOwnerEmail(state, payload) {
			state.params.owner_email = payload;
		},
		setParamsInsertPhone(state, payload) {
			state.params.insert_phone = payload;
		},
		setParamsAddress(state, payload) {
			state.params.address = payload;
		},
		setParamsEducation(state, payload) {
			state.params.education = payload;
		},
		setParamsSkill(state, payload) {
			state.params.skill = payload;
		},
		setParamsJobExperience(state, payload) {
			state.params.job_experience = payload;
		},
		setParamsLastSalary(state, payload) {
			state.params.last_salary = payload;
		},
		setParamsJobsSalary(state, payload) {
			state.params.jobs_salary = payload;
		},
		setParamsWorkplaceStatus(state, payload) {
			state.params.workplace_status = payload;
		},
		setParamsCv(state, payload) {
			state.params.cv = payload;
		}
	}
});

Vue.config.productionTip = false;

const app = new Vue({
	el: '#app',
	mixins: [mixinAuth],
	store,
	created() {
		this.$store.commit('setDetail', mamiData);
	},
	mounted() {
		try {
			document.getElementById('prerender').remove();
		} catch (e) {
			document.getElementById('prerender').style.display = 'none';
		}
	}
});
