require('Js/starter2');

window.debounce = require('lodash/debounce');

window.Vue = require('vue').default;

import Vuex from 'vuex';
Vue.use(Vuex);

import mixinAuth from 'Js/@mixins/MixinAuth';

require('Js/@mixins/MixinScrollTop');

const App = require('./components/company-profile/App.vue').default;
const ContainerCompanyProfile = require('./components/company-profile/ContainerCompanyProfile.vue')
	.default;

// Container Component
Vue.component('app', App);
Vue.component('container-company-profile', ContainerCompanyProfile);

// Global Components
Vue.component(
	'breadcrumb-trails',
	require('Js/@components/BreadcrumbTrails.vue').default
);
Vue.component(
	'section-title',
	require('Js/input/components/SectionTitle.vue').default
);
Vue.component(
	'section-alert',
	require('Js/input/components/SectionAlert.vue').default
);

// Local Components
Vue.component(
	'company-header',
	require('./components/company-profile/CompanyHeader.vue').default
);
Vue.component(
	'company-profile-content',
	require('./components/company-profile/CompanyProfileContent.vue').default
);

Vue.component(
	'company-culture',
	require('./components/company-profile/CompanyCulture.vue').default
);
Vue.component(
	'social-media',
	require('./components/company-profile/SocialMedia.vue').default
);
Vue.component(
	'company-description',
	require('./components/company-profile/CompanyDescription.vue').default
);
Vue.component(
	'company-list-other',
	require('./components/company-profile/CompanyListOther.vue').default
);
Vue.component(
	'company-vacancy',
	require('./components/company-profile/CompanyVacancy.vue').default
);
Vue.component(
	'company-related',
	require('./components/company-profile/CompanyRelated.vue').default
);
Vue.component(
	'company-map',
	require('./components/company-profile/CompanyMap.vue').default
);

const store = new Vuex.Store({
	state: {
		token: document.head.querySelector('meta[name="csrf-token"]').content,
		slug: document.head.querySelector('meta[name="slug"]').content,
		section: null,
		authCheck: {},
		authData: {},
		detail: {},
		params: {},
		extras: {}
	},
	mutations: {
		setAuthCheck(state, payload) {
			state.authCheck = payload;
		},
		setAuthData(state, payload) {
			state.authData = payload;
		},
		setDetail(state, payload) {
			state.detail = payload;
		}
	}
});

Vue.config.productionTip = false;

const app = new Vue({
	el: '#app',
	mixins: [mixinAuth],
	store,
	created() {}
});
