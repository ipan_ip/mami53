require('Js/starter2');

window.Vue = require('vue').default;

import VueRouter from 'vue-router';
Vue.use(VueRouter);

const infiniteScroll = require('vue-infinite-scroll');
Vue.use(infiniteScroll);

require('Js/@mixins/MixinScrollTop');

const App = require('./components/company-vacancy/App.vue').default;
const ContainerCompanyVacancy = require('./components/company-vacancy/ContainerCompanyVacancy.vue')
	.default;

// Container Component
Vue.component('app', App);
Vue.component('container-company-vacancy', ContainerCompanyVacancy);

// Global Components
Vue.component(
	'breadcrumb-trails',
	require('Js/@components/BreadcrumbTrails.vue').default
);
Vue.component(
	'mami-loading-inline',
	require('Js/@components/MamiLoadingInline.vue').default
);
Vue.component(
	'list-placeholder',
	require('Js/@components/ListPlaceholder.vue').default
);
// List Components
Vue.component(
	'vacancy-list',
	require('Js/promo/all-job/components/VacancyList.vue').default
);

// Local Components
Vue.component(
	'company-vacancy-header',
	require('./components/company-vacancy/CompanyVacancyHeader.vue').default
);
Vue.component(
	'company-vacancy-title',
	require('./components/company-vacancy/CompanyVacancyTitle.vue').default
);
Vue.component(
	'company-vacancy-filter',
	require('./components/company-vacancy/CompanyVacancyFilter.vue').default
);

const companyVacancyList = require('./components/company-vacancy/CompanyVacancyList.vue')
	.default;

// Router
const routes = [{ path: '/company-vacancy', component: companyVacancyList }];

const router = new VueRouter({
	mode: 'history',
	routes
});

Vue.config.productionTip = false;

const app = new Vue({
	el: '#app',
	router
});
