require('Js/starter2');

window.Vue = require('vue').default;

import Vuex from 'vuex';
Vue.use(Vuex);

import mixinAuth from 'Js/@mixins/MixinAuth';

require('Js/@mixins/MixinScrollTop');

const App = require('./components/landing/App.vue').default;

// Container Component
Vue.component('app', App);

const store = new Vuex.Store({
	state: {
		token: document.head.querySelector('meta[name="csrf-token"]').content,
		authCheck: {},
		authData: {},
		slug: document.head.querySelector('meta[name="slug"]').content,
		section: null,
		area: {},
		breadcrumb: {},
		centerpoint: {},
		educationOptions: {},
		spesialisasiOptions: {},
		landingConnector: {},
		jobTypeFilter: jobTypeOptions,
		extras: {}
	},
	mutations: {
		setAuthCheck(state, payload) {
			state.authCheck = payload;
		},
		setAuthData(state, payload) {
			state.authData = payload;
		},
		changeSection(state, payload) {
			state.section = payload;
		},
		setArea(state, payload) {
			state.area = payload;
		},
		setBreadcrumb(state, payload) {
			state.breadcrumb = payload;
		},
		setMapCenter(state, payload) {
			state.centerpoint = payload;
		},
		setCenterpoint(state, payload) {
			state.centerpoint = payload;
		},
		setLandingConnector(state, payload) {
			state.landingConnector = payload;
		},
		setEducationOptions(state, payload) {
			state.educationOptions = payload;
		},
		setSpesialisasiOptions(state, payload) {
			state.spesialisasiOptions = payload;
		}
	}
});

Vue.config.productionTip = false;

const app = new Vue({
	el: '#app',
	mixins: [mixinAuth],
	store,
	created() {
		this.$store.commit('setArea', mamiLanding);
		this.$store.commit('setBreadcrumb', mamiBreadcrumb);
		this.$store.commit('setCenterpoint', mamiCenterpoint);
		this.$store.commit('setLandingConnector', mamiLandingConnector);
		this.$store.commit('setEducationOptions', educationOptions);
		this.$store.commit('setSpesialisasiOptions', spesialisasiOptions);
	},
	mounted() {
		try {
			document.getElementById('prerender').remove();
		} catch (e) {
			document.getElementById('prerender').style.display = 'none';
		}
	}
});
