import mixinAuth from 'Js/@mixins/MixinAuth';
import App from './components/App.vue';

Vue.component('app', App);

const app = new Vue({
	el: '#app',
	mixins: [mixinAuth],
	data() {
		return {
			state: {
				token: document.head.querySelector('meta[name="csrf-token"]').content,
				authCheck: {},
				authData: {}
			}
		};
	},
	mounted() {
		try {
			document.getElementById('preloading').remove();
		} catch (e) {
			document.getElementById('preloading').style.display = 'none';
		}
	}
});
