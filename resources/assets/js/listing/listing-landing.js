require('Js/starter');

window.Vue = require('vue').default;

import VueRouter from 'vue-router';
Vue.use(VueRouter);

import Vuex from 'vuex';
Vue.use(Vuex);

import mixinAuth from 'Js/@mixins/MixinAuth';

require('Js/@mixins/MixinCenterModal');
require('Js/@mixins/MixinScrollTop');

const ContainerListing = require('./components/ContainerListing.vue').default;

// Container Component
Vue.component('container-listing', ContainerListing);

// Global Components
Vue.component('breadcrumb-trails', () =>
	import('Js/@components/BreadcrumbTrails')
);
Vue.component('mami-loading-inline', () =>
	import('Js/@components/MamiLoadingInline')
);
Vue.component('list-placeholder', () =>
	import('Js/@components/ListPlaceholder')
);

const vacancyListingList = require('./components/vacancy/VacancyListingListPagination.vue')
	.default;
const kostListingList = require('./components/kost/KostListingListPagination.vue')
	.default;
const apartmentListingList = require('./components/apartment/ApartmentListingListPagination.vue')
	.default;
// Router
const routes = [
	{
		path: '/loker/list/:slug',
		component: vacancyListingList
	},
	{
		path: '/loker/1/list/:slug',
		component: vacancyListingList
	},
	{
		path: '/kost/infokost/:slug',
		component: kostListingList
	},
	{
		path: '/apartemen/disewakan/:slug',
		component: apartmentListingList
	}
];

const router = new VueRouter({
	mode: 'history',
	routes
});

const store = new Vuex.Store({
	state: {
		authCheck: {},
		authData: {},
		listingData: {},
		specialityVacancy: []
	},
	mutations: {
		setAuthCheck(state, payload) {
			state.authCheck = payload;
		},
		setAuthData(state, payload) {
			state.authData = payload;
		},
		setListingData(state, payload) {
			state.listingData = payload;
		},
		setSpecialityVacancy(state, payload) {
			state.specialityVacancy = payload;
		}
	}
});

Vue.config.productionTip = false;

const app = new Vue({
	el: '#app',
	mixins: [mixinAuth],
	router,
	store,
	created() {
		this.$store.commit('setListingData', listingData);
		this.$store.commit('setSpecialityVacancy', spesialisasiOptions);
	},
	mounted() {
		try {
			document.getElementById('prerender').remove();
		} catch (e) {
			document.getElementById('prerender').style.display = 'none';
		}
	}
});
