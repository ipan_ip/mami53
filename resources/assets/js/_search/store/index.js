import _get from 'lodash/get';
import { translateWords } from 'Js/@utils/langTranslator.js';
import { makeAPICall } from 'Js/@utils/makeAPICall.js';
import { sanitizeFilterTag } from 'Js/@utils/kostFilter';
import { round } from 'Js/@utils/math';

export default {
	state: {
		token: document.head.querySelector('meta[name="csrf-token"]').content,
		authCheck: {},
		authData: {},
		criteria: {},
		searchModalStatus: false,
		kos: {
			params: {
				filters: {
					gender: [],
					price_range: [10000, 20000000],
					tag_ids: [],
					level_info: [],
					rent_type: 2,
					property_type: 'all',
					random_seeds: Math.floor(Math.random() * (1000 - 0 + 1)) + 0,
					booking: false,
					flash_sale: false,
					mamichecker: false,
					mamirooms: false,
					goldplus: []
				},
				sorting: {
					field: 'price',
					direction: '-'
				},
				geocode_id: null,
				location: [],
				point: {},
				include_promoted: false,
				limit: 20,
				offset: 0
			},
			kosList: [],
			clusterList: [],
			isEmptyList: false,
			totalRooms: 0,
			responseData: null,
			hasMore: false,
			tagRooms: [],
			tagShare: [],
			tagRules: [],
			rentTypeList: []
		},
		extras: {
			geolocActive: false,
			clusterOpen: false,
			changeFilters: false
		},
		isGoldPlus: false,
		isMoreLoading: false,
		isLoading: true,
		isFacLoading: true,
		isCountRoomsLoading: false,
		isFlashSaleAvailable: false,
		isMapRadius: false,
		isMapDisabled: false,
		historyPoint: -1
	},
	getters: {
		getAllParams: state => {
			return state.kos.params;
		},
		getLocation: state => {
			return state.kos.params.location;
		},
		getGeolocActive: state => {
			return state.extras.geolocActive;
		},
		getKostSorting: state => {
			return [
				state.kos.params.sorting.field,
				state.kos.params.sorting.direction
			];
		},
		getKostFilters: state => {
			return state.kos.params.filters;
		},
		getFacLoading: state => {
			return state.isFacLoading;
		},
		getEmptyList: state => {
			return state.kos.isEmptyList;
		},
		getRooms: state => {
			return state.kos.kosList;
		},
		getClusterRooms: state => {
			return state.kos.clusterList;
		},
		getTotalRooms: state => {
			return state.kos.totalRooms;
		},
		getHasMore: state => {
			return state.kos.hasMore;
		},
		getTagRooms: state => {
			return state.kos.tagRooms;
		},
		getTagShare: state => {
			return state.kos.tagShare;
		},
		getTagRules: state => {
			return state.kos.tagRules;
		},
		getRentTypeList: state => {
			return state.kos.rentTypeList;
		},
		getClusterStatus: state => {
			return state.extras.clusterOpen;
		},
		getResponseData: state => {
			return state.kos.responseData;
		},
		getIsFacilitiesExist: state => {
			return state.kos.tagRooms.length && state.kos.tagShare.length;
		}
	},
	mutations: {
		setMapDisabled(state, payload) {
			state.isMapDisabled = payload;
		},
		setFlashSaleAvailability(state, payload) {
			state.isFlashSaleAvailable = payload;
		},
		setAuthCheck(state, payload) {
			state.authCheck = payload;
		},
		setMapRadius(state, payload) {
			state.isMapRadius = payload;
		},
		setIsGoldPlus(state, payload) {
			state.isGoldPlus = payload;
		},
		setAuthData(state, payload) {
			state.authData = payload;
		},
		setCriteria(state, payload) {
			state.criteria = payload;
		},
		setSearchStatus(state, payload) {
			state.searchModalStatus = payload;
		},
		setClusterStatus(state, payload) {
			state.extras.clusterOpen = payload;
		},
		setFilter(state, payload) {
			state.kos.params.filters = {
				...state.kos.params.filters,
				...payload
			};
			if (state.extras.clusterOpen) state.extras.clusterOpen = false;
		},
		setSort(state, payload) {
			state.kos.params.sorting.field = payload[0];
			state.kos.params.sorting.direction = payload[1];
		},
		setLoading(state, payload) {
			state.isLoading = payload;
		},
		setFacLoading(state, payload) {
			state.isFacLoading = payload;
		},
		setMoreLoading(state, payload) {
			state.isMoreLoading = payload;
		},
		setCountRoomsLoading(state, payload) {
			state.isCountRoomsLoading = payload;
		},
		setEmptyList(state, payload) {
			state.kos.isEmptyList = payload;
		},
		setHasMore(state, payload) {
			state.kos.hasMore = payload;
		},
		setRooms(state, payload) {
			if (payload.status) {
				if (payload.isRequestMore) {
					state.kos.kosList = [...state.kos.kosList, ...payload.rooms];
				} else {
					state.kos.kosList = payload.rooms;
				}
				state.kos.totalRooms = payload.total;
			} else {
				state.kos.kosList = [];
				state.kos.totalRooms = 0;
			}
		},
		setOffset(state, payload) {
			state.kos.params.offset = payload;
		},
		setTagRooms(state, payload) {
			const tagRoomsList = sanitizeFilterTag(payload);
			state.kos.tagRooms = tagRoomsList;
		},
		setTagShare(state, payload) {
			const tagShareList = sanitizeFilterTag(payload);
			state.kos.tagShare = tagShareList;
		},
		setTagRules(state, payload) {
			const tagRulesList = sanitizeFilterTag(payload);
			state.kos.tagRules = tagRulesList;
		},
		setRentTypeList(state, payload) {
			const rentList = payload.map(rent => {
				return {
					...rent,
					value: rent.id,
					name: rent.rent_name
				};
			});
			state.kos.rentTypeList = rentList;
		},
		setClusterKost(state, payload) {
			state.kos.clusterList = payload;
		},
		setCriteriaLngLat(state, payload) {
			state.criteria.latitude = payload.lat;
			state.criteria.longitude = payload.lng;
		},
		setMapCenter(state, payload) {
			state.criteria.latitude = payload.lat;
			state.criteria.longitude = payload.lng;
		},

		/**
		 * Make sure you pass this format for the payload:
		 * [ [long, lat], [long, lat] ]
		 */
		setLocation(state, payload) {
			// truncate the long and lang decimal digit. See: CORE-1567
			payload = payload.map(item => [
				round(_get(item, '[0]', 0), 6),
				round(_get(item, '[1]', 0), 6)
			]);

			state.kos.params.location = payload;
		},
		setPoint(state, payload) {
			state.kos.params.point = payload;
		},
		setExtrasGeolocActive(state, payload) {
			state.extras.geolocActive = payload;
		},
		setResponseData(state, payload) {
			state.kos.responseData = payload;
		},
		resetParams(state, payload) {
			state.kos.params = {
				...state.kos.params,
				...payload
			};
		},
		setGeocodeId(state, payload) {
			state.kos.params.geocode_id = payload;
		},
		setHistoryPoint(state) {
			state.historyPoint = state.historyPoint - 1;
		}
	},
	actions: {
		settingFilter({ commit }, payload) {
			commit('setFilter', payload);
		},
		settingSort({ commit, dispatch }, payload) {
			commit('setSort', payload);
			dispatch('fetchRooms', { loadingMore: false, isNominatim: false });
		},
		settingTagIds({ commit, dispatch }, payload) {
			commit('setFilter', payload.content);
			if (payload.search) {
				dispatch('fetchRooms', { loadingMore: false, isNominatim: false });
			}
		},
		async fetchFacilitiesList({ commit }) {
			commit('setFacLoading', true);
			try {
				const response = await makeAPICall({
					method: 'get',
					url: '/stories/filters'
				});
				if (response) {
					commit('setTagRooms', response.fac_room);
					commit('setTagShare', response.fac_share);
					commit('setTagRules', response.kos_rule);
					commit('setRentTypeList', response.rent_type);
				}
			} catch (error) {
				bugsnagClient.notify(error);
			} finally {
				commit('setFacLoading', false);
			}
		},
		async fetchRooms(
			{ commit, state, dispatch },
			{ loadingMore, isNominatim }
		) {
			if (state.extras.clusterOpen) {
				dispatch('fetchClusters', { loadingMore, isNominatim });
			} else {
				if (loadingMore) {
					commit('setMoreLoading', true);
					const offset = state.kos.kosList.length;
					commit('setOffset', offset);
				} else {
					commit('setLoading', true);
					commit('setOffset', 0);
				}
				if (state.kos.params.location.length === 0) {
					const payload = [
						[106.69793128967285, -6.296188782911225],
						[106.76608085632326, -6.271703381316111]
					];
					commit('setLocation', payload);
				}
				const params = state.kos.params;
				const genderFilter = {
					gender: params.filters.gender.length
						? params.filters.gender
						: [0, 1, 2]
				};
				const response = await makeAPICall({
					method: 'post',
					url: '/stories/list',
					data: {
						...params,
						filters: {
							...params.filters,
							...genderFilter
						}
					}
				});
				if (response) {
					const rooms = translateWords(response.rooms) || [];
					if (rooms.length > 0) {
						const payload = {
							status: true,
							rooms,
							total: response.total,
							isRequestMore: loadingMore
						};
						commit('setEmptyList', false);
						commit('setRooms', payload);
					} else {
						commit('setEmptyList', true);
						commit('setRooms', {
							status: false
						});
					}
					commit('setHasMore', response['has-more']);
					commit('setResponseData', response);
					loadingMore
						? commit('setMoreLoading', false)
						: commit('setLoading', false);
				} else {
					commit('setEmptyList', true);
					commit('setRooms', {
						status: false
					});
					commit('setHasMore', false);
					loadingMore
						? commit('setMoreLoading', false)
						: commit('setLoading', false);
				}

				dispatch('fetchClusterList');
			}
		},
		async fetchClusterList({ commit, state }) {
			const params = state.kos.params;
			const genderFilter = {
				gender: params.filters.gender.length ? params.filters.gender : [0, 1, 2]
			};

			const resRoomCluster = await makeAPICall({
				method: 'post',
				url: '/stories/cluster',
				data: {
					...params,
					filters: {
						...params.filters,
						...genderFilter
					}
				}
			});

			if (resRoomCluster) {
				commit('setClusterKost', resRoomCluster.rooms);
			}
		},
		async fetchClusters({ commit, state }, { loadingMore, isNominatim }) {
			if (loadingMore) {
				commit('setMoreLoading', true);
				const offset = state.kos.kosList.length;
				commit('setOffset', offset);
			} else {
				commit('setLoading', true);
				commit('setOffset', 0);
			}
			if (state.kos.params.location.length === 0) {
				const payload = [
					[106.69793128967285, -6.296188782911225],
					[106.76608085632326, -6.271703381316111]
				];
				commit('setLocation', payload);
			}
			if (!state.extras.clusterOpen) commit('setClusterStatus', true);
			const params = state.kos.params;
			const genderFilter = {
				gender: params.filters.gender.length ? params.filters.gender : [0, 1, 2]
			};

			const response = await makeAPICall({
				method: 'post',
				url: '/room/list/cluster',
				data: {
					...params,
					filters: {
						...params.filters,
						...genderFilter
					},
					geocode_id: isNominatim ? null : params.geocode_id
				}
			});

			if (response) {
				const rooms = response.rooms;
				if (rooms.length > 0) {
					const payload = {
						status: true,
						rooms,
						total: response.total,
						isRequestMore: loadingMore
					};
					commit('setEmptyList', false);
					commit('setRooms', payload);
				} else {
					commit('setEmptyList', true);
					commit('setRooms', {
						status: false
					});
					commit('setClusterKost', []);
				}
				commit('setHasMore', response['has-more']);
				commit('setResponseData', response);
				loadingMore
					? commit('setMoreLoading', false)
					: commit('setLoading', false);
			} else {
				commit('setEmptyList', true);
				commit('setRooms', {
					status: false
				});
				commit('setHasMore', false);
				loadingMore
					? commit('setMoreLoading', false)
					: commit('setLoading', false);
			}
		}
	}
};
