import containerSearchPage from '../components/ContainerSearchPage.vue';

export default [
	{
		path: '/cari',
		name: 'search',
		component: containerSearchPage
	},
	{
		path: '/cari/:keywords/:gender/:rentType/:price',
		name: 'search_slug',
		component: containerSearchPage
	}
];
