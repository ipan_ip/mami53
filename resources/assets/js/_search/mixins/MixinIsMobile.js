export default {
	computed: {
		isMobile() {
			return window.innerWidth <= 1280;
		}
	}
};
