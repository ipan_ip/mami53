import _get from 'lodash/get';
import _isEqual from 'lodash/isEqual';
import { mapActions, mapGetters, mapState, mapMutations } from 'vuex';
import {
	reservedKeys,
	reservedValues,
	genderOptions,
	sortingOptions
} from 'Js/@utils/kostFilter';
import { getParams, appendParams } from 'Js/@utils/queryString';
import MixinIsMobile from './MixinIsMobile';

export default {
	mixins: [MixinIsMobile],
	computed: {
		...mapState(['authCheck', 'isGoldPlus']),
		...mapGetters([
			'getKostFilters',
			'getKostSorting',
			'getTagRooms',
			'getTagShare',
			'getTagRules',
			'getRentTypeList',
			'getIsFacilitiesExist'
		]),
		/* eslint-disable */
		totalActive() {
			let totalFilterActive = 0;
			let totalSortingActive = 0;

			const {
				gender,
				price_range,
				tag_ids,
				rent_type,
				booking,
				flash_sale,
				mamirooms,
				mamichecker
			} = this.getKostFilters;

			if (tag_ids.length) totalFilterActive += tag_ids.length;

			if (price_range[0] !== 10000 || price_range[1] !== 20000000)
				totalFilterActive++;

			if (gender.length) totalFilterActive += gender.length;
			rent_type !== 2 && totalFilterActive++;
			booking && totalFilterActive++;
			flash_sale && totalFilterActive++;
			mamirooms && totalFilterActive++;
			mamichecker && totalFilterActive++;
			this.isGoldPlus && totalFilterActive++;

			if (this.getKostSorting[1] !== '-' || this.getKostSorting[0] !== 'price')
				totalSortingActive++;

			return {
				filter: totalFilterActive,
				sort: totalSortingActive
			};
		},
		/* eslint-enable */
		genderData() {
			const genderFilter = this.getKostFilters.gender;
			let genderText = 'Semua Tipe Kos';

			if (genderFilter.length && genderFilter.length < 3) {
				genderText = genderOptions
					.filter(gender => ~genderFilter.indexOf(gender.value))
					.map(gender => gender.name)
					.join(', ');
			}

			return {
				title: genderText,
				icon: 'gender-icon',
				data: genderOptions,
				getValue: genderFilter,
				active: Boolean(genderFilter.length)
			};
		},
		rulesData() {
			const tagFilter = this.getKostFilters.tag_ids;
			const countActiveRules = tagFilter.filter(tag => {
				if (this.getTagRules.find(rule => +rule.value === +tag)) return tag;
			});

			return {
				title: 'Aturan Kos',
				data: this.getTagRules,
				getValue: tagFilter,
				count: countActiveRules.length,
				active: Boolean(countActiveRules.length)
			};
		},
		rentData() {
			const rentFilter = this.getKostFilters.rent_type;
			const rentData = this.getRentTypeList.find(
				rent => +rent.value === +rentFilter
			);
			const rentText = rentData ? rentData.name : 'Bulanan';

			return {
				title: rentText,
				data: this.getRentTypeList,
				getValue: rentFilter,
				active: true
			};
		},
		facilitiesData() {
			const facilitiesFilter = this.getKostFilters.tag_ids;
			const tagFacilities = [...this.getTagRooms, ...this.getTagShare];
			const countActiveFacilities = facilitiesFilter.filter(tag => {
				if (tagFacilities.find(facility => +facility.value === +tag))
					return tag;
			});

			return {
				title: 'Fasilitas',
				facilityCategories: [
					{
						title: 'Fasilitas Kamar',
						data: this.getTagRooms
					},
					{
						title: 'Fasilitas Bersama',
						data: this.getTagShare
					}
				],
				getValue: facilitiesFilter,
				count: countActiveFacilities.length,
				active: Boolean(countActiveFacilities.length),
				allFacTag: tagFacilities
			};
		},
		sortData() {
			const sortText = sortingOptions.find(
				sort => sort.value.join() === this.getKostSorting.join()
			);

			return {
				title: sortText ? sortText.name : 'Rekomendasi',
				data: sortingOptions,
				icon: 'sorting-icon',
				getValue: this.getKostSorting,
				active: true
			};
		},
		priceData() {
			const isDefaultPrice =
				+this.getKostFilters.price_range[0] === 10000 &&
				+this.getKostFilters.price_range[1] === 20000000;
			const minPriceText = this.generatePriceText(
				this.getKostFilters.price_range[0]
			);
			const maxPriceText = this.generatePriceText(
				this.getKostFilters.price_range[1]
			);
			const title = isDefaultPrice
				? 'Harga'
				: `${minPriceText} - ${maxPriceText}`;

			return {
				title,
				active: !isDefaultPrice
			};
		},
		isOwner() {
			const isLogin = _get(this.authCheck, 'all', false);
			return isLogin ? _get(this.authCheck, 'owner', false) : null;
		},
		interface() {
			return this.isMobile ? 'mobile' : 'desktop';
		}
	},
	methods: {
		...mapActions([
			'settingFilter',
			'settingSort',
			'settingTagIds',
			'fetchRooms'
		]),
		...mapMutations(['setIsGoldPlus', 'setHistoryPoint']),
		checkPriceUrl(price) {
			const currentPrice = price.toString().length > 9 ? 999990000 : price;
			return currentPrice < 10000 ? 10000 : +currentPrice;
		},
		settingFilterUrl() {
			const filterParams = getParams(null);
			const arrValPairs = Object.entries(filterParams);
			const currentFilters = {};
			let currentSorting = ['price', '-'];
			if (arrValPairs && arrValPairs.length) {
				for (const valuePair of arrValPairs) {
					const key = valuePair[0];
					const keyValue = valuePair[1][0];
					const isRightKey = Boolean(~reservedKeys.indexOf(key));
					const isRightValue = reservedValues[key]
						? Boolean(~reservedValues[key].indexOf(keyValue))
						: true;

					if (!keyValue || !isRightKey || !isRightValue) continue;

					if (key === 'rent') {
						const isRentTypeExist = this.getRentTypeList.find(
							rent => +rent.value === +keyValue
						);
						if (isRentTypeExist) currentFilters.rent_type = +keyValue;
					} else if (key === 'gender') {
						let genderArrUrl = keyValue.split(',');
						genderArrUrl = [...new Set(genderArrUrl)];
						currentFilters.gender = genderArrUrl
							.filter(gender => [0, 1, 2].indexOf(+gender) !== -1)
							.map(gender => +gender);
					} else if (key === 'price') {
						let priceData = keyValue.split('-');
						if (priceData.length === 2) {
							priceData = priceData.map(price => price.replace(/\D/g, ''));
							let minPrice = Math.floor(Math.min(...priceData) / 10000) * 10000;
							let maxPrice = Math.ceil(Math.max(...priceData) / 10000) * 10000;

							minPrice = this.checkPriceUrl(minPrice);
							maxPrice = this.checkPriceUrl(maxPrice);

							currentFilters.price_range = [+minPrice, +maxPrice];
						}
					} else if (key === 'goldplus') {
						let goldPlusKey = keyValue.split(',');
						goldPlusKey = [...new Set(goldPlusKey)];
						const goldplusValue = goldPlusKey
							.filter(goldplus => [0, 1, 2, 3, 4].indexOf(+goldplus) !== -1)
							.map(goldplus => +goldplus);
						const isValid = _isEqual(goldplusValue.sort(), [1, 2, 3, 4]);
						if (goldplusValue.indexOf(0) !== -1 || isValid) {
							currentFilters.goldplus = [0, 1, 2, 3, 4];
							this.setIsGoldPlus(true);
						}
					} else if (key === 'sort') {
						currentSorting = keyValue.split(',');
					} else if (key === 'tag') {
						let tagArrUrl = keyValue.split(',');
						tagArrUrl = [...new Set(tagArrUrl)];
						const allTags = [
							...this.getTagRooms,
							...this.getTagShare,
							...this.getTagRules
						];
						const idTags = allTags.map(value => value.fac_id);
						if (tagArrUrl && tagArrUrl.length) {
							currentFilters.tag_ids = tagArrUrl
								.filter(id => idTags.indexOf(+id) !== -1)
								.map(id => +id);
						}
					} else if (key === 'booking') {
						currentFilters.booking = Boolean(+keyValue);
					} else if (key === 'mamichecker') {
						currentFilters.mamichecker = Boolean(+keyValue);
					} else if (key === 'singgahsini') {
						currentFilters.mamirooms = Boolean(+keyValue);
					}
				}
			}
			if (!currentFilters.gender && typeof genderType === 'string') {
				/* eslint-disable */
				currentFilters.gender = genderType.split(',').map(gender => +gender);
				/* eslint-enable */
			}
			this.settingFilter(currentFilters);
			this.settingSort(currentSorting);
		},
		appendFilterUrl() {
			const payload = {
				gender: this.getKostFilters.gender.join(),
				rent: this.getKostFilters.rent_type,
				sort: `${this.getKostSorting[0]},${this.getKostSorting[1]}`,
				price: `${this.getKostFilters.price_range[0]}-${this.getKostFilters.price_range[1]}`,
				booking: this.getKostFilters.booking ? 1 : 0,
				mamichecker: this.getKostFilters.mamichecker ? 1 : 0,
				singgahsini: this.getKostFilters.mamirooms ? 1 : 0,
				tag: this.getKostFilters.tag_ids.join(),
				goldplus: this.getKostFilters.goldplus.join()
			};
			appendParams(window.location.href, payload, true);
		},
		handleOpenFilterMoEngage() {
			const moEngageData = {
				interface: this.interface,
				is_owner: this.isOwner
			};

			tracker('moe', ['Filter Page Visited', moEngageData]);
		},
		handleSaveFilterMoEngage() {
			const sortText = sortingOptions.find(
				sort => sort.value.join() === this.getKostSorting.join()
			);
			const tagFacilities = _get(this.facilitiesData, 'allFacTag', []);
			const allTags = [...tagFacilities, ...this.getTagRules];

			const moEngageData = {
				interface: this.interface,
				is_owner: this.isOwner,
				filter_type_putra: Boolean(~this.getKostFilters.gender.indexOf(1)),
				filter_type_putri: Boolean(~this.getKostFilters.gender.indexOf(2)),
				filter_type_campur: Boolean(~this.getKostFilters.gender.indexOf(0)),
				filter_price_min: +this.getKostFilters.price_range[0],
				filter_price_max: +this.getKostFilters.price_range[1],
				filter_period_mingguan: this.getKostFilters.rent_type === 1,
				filter_period_bulanan: this.getKostFilters.rent_type === 2,
				filter_period_3_bulan: this.getKostFilters.rent_type === 4,
				filter_period_6_bulan: this.getKostFilters.rent_type === 5,
				filter_period_tahunan: this.getKostFilters.rent_type === 3,
				filter_bisa_booking: this.getKostFilters.booking,
				filter_mamichecker: this.getKostFilters.mamichecker,
				filter_mamirooms: this.getKostFilters.mamirooms,
				filter_sort: sortText.name,
				filter_goldplus: this.isGoldPlus
			};

			allTags.map(tag => {
				moEngageData[`fac_${tag.value}`] = Boolean(
					~this.getKostFilters.tag_ids.indexOf(tag.value)
				);
			});

			tracker('moe', ['Save Filter Clicked', moEngageData]);
			this.appendFilterUrl();
		},
		setMainFilter(value, name) {
			const payload = {
				[name]: value
			};
			this.settingFilter(payload);
		},
		setTagIds(tag) {
			const payload = {
				content: {
					tag_ids: tag
				},
				search: false
			};
			this.settingTagIds(payload);
		},
		setFilterPrice(priceRange) {
			const payload = {
				price_range: priceRange
			};
			this.settingFilter(payload);
			this.trackEventPrice(payload);
		},
		resetDesktopFilter(filterType) {
			if (filterType === 'gender') {
				const payload = {
					gender: []
				};
				this.settingFilter(payload);
			} else {
				const arrayData =
					filterType === 'rules'
						? this.getTagRules
						: this.facilitiesData.allFacTag;

				const tagFilter = this.getKostFilters.tag_ids.filter(tag => {
					if (!arrayData.find(rule => +rule.value === +tag)) return tag;
				});
				this.setTagIds(tagFilter);
			}
		},
		generatePriceText(price) {
			const priceLength = price.toString().length;
			const priceCurrency = price
				.toString()
				.replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
			const priceSplit = priceCurrency.split(',');
			const firstPrice = priceSplit[0];
			const secondPrice =
				firstPrice.length < 3
					? priceSplit[1].substring(0, 2)
					: priceSplit[1].substring(0, 1);

			return priceLength > 6
				? `Rp ${firstPrice},${secondPrice} juta`
				: `Rp ${firstPrice} ribu`;
		},
		trackEventPrice(payload) {
			tracker('ga', [
				'send',
				'event',
				{
					eventCategory: 'Price Filter',
					eventAction: 'all',
					eventLabel: `Min: ${payload.price_range[0]} & Max: ${payload.price_range[1]}`
				}
			]);

			tracker('ga', [
				'send',
				'event',
				{
					eventCategory: 'Price Filter',
					eventAction: 'min',
					eventLabel: payload.price_range[0]
				}
			]);

			tracker('ga', [
				'send',
				'event',
				{
					eventCategory: 'Price Filter',
					eventAction: 'max',
					eventLabel: payload.price_range[1]
				}
			]);
		},
		saveFacilitiesTag(payload) {
			const tagRules = this.getKostFilters.tag_ids.filter(tag => {
				if (this.getTagRules.find(rule => +rule.value === +tag)) return tag;
			});

			let currentTags = [...payload, ...tagRules];
			currentTags = [...new Set(currentTags)];

			const filterTags = this.getKostFilters.tag_ids;

			currentTags.sort();
			filterTags.sort();

			const isDataNotChange = _isEqual(currentTags, filterTags);

			if (!isDataNotChange) {
				this.setTagIds(currentTags);
				this.setHistoryPoint();
				this.handleSaveFilterMoEngage();
				this.fetchRooms({ loadingMore: false, isNominatim: false });
			}

			this.toggleModalFacilities(false);
		}
	}
};
