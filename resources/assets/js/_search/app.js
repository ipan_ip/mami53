import 'bangul-vue/dist/bangul.css';
import 'Js/@mixins/MixinNavigatorIsMobile';
import 'Js/@mixins/MixinSwalLoading';
import mixinAuth from 'Js/@mixins/MixinAuth';
import { getParams } from 'Js/@utils/queryString';
import _get from 'lodash/get';
import StoreData from './store';
import routes from './routes';
import Vuex from 'vuex';
import VueRouter from 'vue-router';
import App from './App.vue';

/* eslint-disable */
const router = new VueRouter({
	mode: 'history',
	base: '/',
	routes
});
const store = new Vuex.Store(StoreData);

const app = new Vue({
	el: '#app',
	components: { App },
	mixins: [mixinAuth],
	router,
	store,
	created() {
		const queryParams = getParams(null);
		const location = queryParams.location
			? decodeURIComponent(JSON.parse(queryParams.location[0]))
			: null;
		const latitude = queryParams.lat ? parseFloat(queryParams.lat[0]) : 0;
		const longitude = queryParams.long ? parseFloat(queryParams.long[0]) : 0;
		const propertyName = queryParams.title
			? decodeURIComponent(queryParams.title[0])
			: null;
		const isLatLongValid = Boolean(latitude) && Boolean(longitude);
		let locationArray = location ? location.toString().split(',') : [];

		if (locationArray.length === 4 && isLatLongValid) {
			locationArray = locationArray.map(value => parseFloat(value));
			mamiCriteria = {
				...mamiCriteria,
				isShowNominatim: false,
				latitude: isLatLongValid ? latitude : mamiCriteria.latitude,
				longitude: isLatLongValid ? longitude : mamiCriteria.longitude,
				boundsLocation: [
					[locationArray[1], locationArray[0]],
					[locationArray[3], locationArray[2]]
				],
				propertyName,
				keywords: propertyName ? 'rekomendasi' : mamiCriteria.keywords
			};
			this.$store.commit('setMapRadius', true);
		}

		const geocodeId = queryParams.geocode ? queryParams.geocode[0] : null;
		const geocodeType = _get(mamiCriteria, 'geodata.geocode_type');

		if (geocodeId && geocodeType === 'area') {
			this.$store.commit('setGeocodeId', geocodeId);

			let geoDataLat = _get(mamiCriteria, 'geodata.lat');
			let geoDataLong = _get(mamiCriteria, 'geodata.long');
			geoDataLat = geoDataLat || mamiCriteria.latitude;
			geoDataLong = geoDataLong || mamiCriteria.longitude;

			const geoBoundaries = _get(mamiCriteria, 'geodata.boundary', []);
			const geoType = _get(mamiCriteria, 'geodata.type', '');
			let geoBoundingBox = _get(mamiCriteria, 'geodata.boundingbox', []);
			const isUsingZoom = _get(mamiCriteria, 'geodata.is_using_zoom', true);

			if (geoBoundingBox.length === 2) {
				geoBoundingBox = [
					[geoBoundingBox[0][1], geoBoundingBox[0][0]],
					[geoBoundingBox[1][1], geoBoundingBox[1][0]]
				];
			}

			const isGeodataValid =
				!!geoDataLat &&
				!!geoDataLong &&
				!!geoBoundaries.length &&
				!!geoType &&
				geoBoundingBox.length === 2;

			if (isGeodataValid) {
				mamiCriteria = {
					...mamiCriteria,
					geocodeType,
					isShowNominatim: true,
					geocode: geocodeId,
					latitude: geoDataLat,
					longitude: geoDataLong,
					boundaries: geoBoundaries,
					type: geoType,
					boundsLocation: geoBoundingBox,
					isUsingZoom
				};
			}
		}

		this.$store.commit('setCriteria', mamiCriteria);
	},
	mounted() {
		try {
			document.getElementById('preloading').remove();
		} catch (e) {
			document.getElementById('preloading').style.display = 'none';
		}
	}
});
