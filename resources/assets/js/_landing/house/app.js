import Vuex from 'vuex';
import '../../@mixins/MixinNavigatorIsMobile';
import '../../@mixins/MixinSwalLoading';
import '../../@mixins/MixinSwalSuccessError';
import mixinAuth from '../../@mixins/MixinAuth';
import App from './components/App.vue';

Vue.use(Vuex);

Vue.component('app', App);

const store = new Vuex.Store({
	state: {
		token: document.head.querySelector('meta[name="csrf-token"]').content,
		authCheck: {},
		authData: {},
		centerpoint: {},
		landing: {
			filters: {
				rent_type: 2,
				price_range: [0, 100000000],
				property_type: 'all',
				capacity: null,
				bedroom: null,
				tag_ids: [],
				random_seeds: Math.floor(Math.random() * (1000 - 0 + 1)) + 0
			},
			include_promoted: false,
			landing_source: '',
			limit: 20,
			location: [],
			offset: 0,
			page: 1,
			referer: 'landing',
			sorting: {
				direction: '-',
				fields: 'price'
			},
			radius: 1
		},
		extras: {
			landing_connector: [],
			breadcrumb: [],
			place: null,
			landing_related_area: [],
			landing_related_campus: [],
			description: {
				description_1: '',
				description_2: '',
				description_3: ''
			},
			centerLatitude: null,
			centerLongitude: null,
			cornerMap: [],
			cluster: {},
			list: {}
		}
	},
	mutations: {
		setAuthCheck(state, payload) {
			state.authCheck = payload;
		},
		setAuthData(state, payload) {
			state.authData = payload;
		},
		setMapCenter(state, payload) {
			state.centerpoint = payload;
		},
		setHouseFilterRentType(state, payload) {
			state.landing.filters.rent_type = parseInt(payload);
		},
		setHouseFilterPriceRange(state, payload) {
			state.landing.filters.price_range = payload;
		},
		setHouseFilterPropertyType(state, payload) {
			state.landing.filters.property_type = payload;
		},
		setHouseFilterTagIds(state, payload) {
			state.landing.filters.tag_ids = payload;
		},
		setHouserFilterCapacity(state, payload) {
			state.landing.filters.capacity = payload;
		},
		setHouseFilterRoomTotal(state, payload) {
			state.landing.filters.bedroom = payload;
		},
		setHouseSorting(state, payload) {
			state.landing.sorting.fields = payload[0];
			state.landing.sorting.direction = payload[1];
		},
		setHouseLandingSource(state, payload) {
			state.landing.landing_source = payload;
		},
		setHouseLocation(state, payload) {
			state.landing.location = payload;
		},
		setHousePageList(state, payload) {
			state.landing.page = payload;
		},
		setHouseOffset(state, payload) {
			state.landing.offset = payload;
		},
		setHouseRadius(state, payload) {
			state.landing.radius = payload;
		},
		setHouseLandingConnector(state, payload) {
			state.extras.landing_connector = payload;
		},
		setHouseBreadcrumb(state, payload) {
			state.extras.breadcrumb = payload;
		},
		setHousePlace(state, payload) {
			state.extras.place = payload;
		},
		setHouseLandingRelatedArea(state, payload) {
			state.extras.landing_related_area = payload;
		},
		setHouseLandingRelatedCampus(state, payload) {
			state.extras.landing_related_campus = payload;
		},
		setHouseLandingDescription(state, payload) {
			state.extras.description.description_1 = payload.description_1;
			state.extras.description.description_2 = payload.description_2;
			state.extras.description.description_3 = payload.description_3;
		},
		setHouseLatitude(state, payload) {
			state.extras.centerLatitude = payload;
		},
		setHouseLongitude(state, payload) {
			state.extras.centerLongitude = payload;
		},
		setHouseCornerMap(state, payload) {
			state.extras.cornerMap = payload;
		},
		setHouseCluster(state, payload) {
			state.extras.cluster = payload;
		},
		setHouseList(state, payload) {
			state.extras.list = payload;
		}
	}
});

const app = new Vue({
	el: '#app',
	store,
	mixins: [mixinAuth],
	created() {
		this.$store.commit('setHouseFilterRentType', rentType);
		this.$store.commit('setHouseFilterPriceRange', priceRange);
		this.$store.commit('setHouseFilterTagIds', tagIds);
		this.$store.commit('setHouseLandingSource', slug);
		this.$store.commit('setHouseLocation', locPage);
		this.$store.commit('setHouseLandingConnector', landing_connector);
		this.$store.commit('setHouseBreadcrumb', breadcrumb);
		this.$store.commit('setHousePlace', place);
		this.$store.commit('setHouseLandingRelatedArea', landingRelatedArea);
		this.$store.commit('setHouseLandingRelatedCampus', landingRelatedCampus);
		this.$store.commit('setHouseLandingDescription', description);
		this.$store.commit('setHouseLatitude', centerLatitude);
		this.$store.commit('setHouseLongitude', centerLongitude);
		this.$store.commit('setHouseCornerMap', cornerMap);
		this.$store.commit('setHouseRadius', radius);
		this.$store.commit('setHouseFilterPropertyType', landingType);
	},
	mounted() {
		try {
			document.getElementById('preloading').remove();
		} catch (e) {
			document.getElementById('preloading').style.display = 'none';
		}
	}
});
