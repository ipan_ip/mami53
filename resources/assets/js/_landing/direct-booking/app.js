import Vuex from 'vuex';
import VeeValidate, { Validator } from 'vee-validate';
import id from 'vee-validate/dist/locale/id';
import defaultStore from './store';
import mixinAuth from 'Js/@mixins/MixinAuth';
import 'Js/@mixins/MixinNavigatorIsMobile';
import 'Js/@mixins/MixinSwalLoading';
import 'Js/@mixins/MixinSwalSuccessError';
import App from './components/App.vue';

window.validator = new Validator();
Validator.localize('id', id);

Vue.use(VeeValidate, {
	locale: 'id'
});
Vue.use(Vuex);

Vue.component('app', App);

const store = new Vuex.Store(defaultStore);

const app = new Vue({
	el: '#app',
	store,
	mixins: [mixinAuth],
	mounted() {
		try {
			document.getElementById('preloading').remove();
		} catch (e) {
			document.getElementById('preloading').style.display = 'none';
		}
	}
});
