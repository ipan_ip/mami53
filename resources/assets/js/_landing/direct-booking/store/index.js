const store = {
	state: {
		token: document.head.querySelector('meta[name="csrf-token"]').content,
		authCheck: {},
		authData: {},
		testimonial: [],
		owner: null
	},
	mutations: {
		setTestimonial(state, payload) {
			state.testimonial = payload;
		},
		setAuthCheck(state, payload) {
			state.authCheck = payload;
		},
		setAuthData(state, payload) {
			state.authData = payload;
		},
		setOwner(state, payload) {
			state.owner = payload;
		}
	},
	actions: {
		fetchTestimonial({ commit }) {
			return new Promise((resolve, reject) => {
				axios
					.get('/testimony')
					.then(response => {
						if (response.data.status) {
							commit('setTestimonial', response.data.testimonies);
							resolve();
						} else {
							bugsnagClient.notify(JSON.stringify(response));
							alert(
								'Galat tidak diketahui.\nSilakan cek koneksi internet Anda dan coba lagi.'
							);
						}
					})
					.catch(error => {
						reject(error);
						bugsnagClient.notify(error);
						this.swalError(null, 'Terjadi galat. Silakan coba lagi.');
					});
			});
		},
		fetchOwnerData({ commit }) {
			return new Promise((resolve, reject) => {
				axios
					.get('/owner/data/profile')
					.then(response => {
						if (response.data.status) {
							commit('setOwner', response.data);
							resolve();
						} else {
							bugsnagClient.notify(JSON.stringify(response));
							alert(
								'Galat tidak diketahui.\nSilakan cek koneksi internet Anda dan coba lagi.'
							);
						}
					})
					.catch(error => {
						reject(error);
						bugsnagClient.notify(error);
						this.swalError(null, 'Terjadi galat. Silakan coba lagi.');
					});
			});
		}
	}
};

export default store;
