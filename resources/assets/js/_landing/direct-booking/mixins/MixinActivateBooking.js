import EventBus from '../event-bus';
import MixinOwnerStatus from './MixinOwnerStatus';
import * as storage from 'Js/@utils/storage';

export default {
	mixins: [MixinOwnerStatus],
	data() {
		return {
			ownerDashboardUrl: oxWebUrl
		};
	},
	methods: {
		activateBooking(source) {
			tracker('moe', [
				'[Owner] Landing Page Booking Langsung - Aktifkan Sekarang',
				{
					interface: navigator.isMobile ? 'mobile' : 'desktop',
					is_booking: this.isBBKActive,
					is_premium: this.isPremium,
					redirection_source: source
				}
			]);

			if (this.isLogin) {
				if (this.isOwner) {
					const link = this.isBBKActive
						? `${this.ownerDashboardUrl}/booking/manage/status`
						: `${this.ownerDashboardUrl}/kos/booking/register`;

					if (
						storage.local.getItem('activate_booking_redirection_source') !==
						'Landing Page BBK'
					) {
						storage.local.setItem(
							'activate_booking_redirection_source',
							'Landing Page BBK'
						);
					}

					window.open(link, '_blank');
				} else {
					EventBus.$emit('showRestrictionModal');
				}
			} else {
				EventBus.$emit('showOwnerLoginModal');
			}
		}
	}
};
