export default {
	computed: {
		isLogin() {
			return this.$store.state.authCheck.all;
		},
		isOwner() {
			return this.$store.state.authCheck.owner;
		},
		isBBKActive() {
			return this.isLogin && this.isOwner
				? this.$store.state.owner.membership.is_mamipay_user
				: false;
		},
		isPremium() {
			if (this.isLogin && this.isOwner) {
				return (
					!this.$store.state.owner.membership.expired &&
					this.$store.state.owner.membership.status === 'Premium'
				);
			}

			return false;
		}
	}
};
