import 'Js/@mixins/MixinNavigatorIsMobile';
import 'Js/@mixins/MixinSwalLoading';
import 'Js/@mixins/MixinSwalSuccessError';

import 'bangul-vue/dist/bangul.css';

import mixinAuth from 'Js/@mixins/MixinAuth';

import { translateWords } from 'Js/@utils/langTranslator.js';
import debounce from 'lodash/debounce';

import { swiper, swiperSlide } from 'vue-awesome-swiper';
import Dayjs from 'vue-dayjs';
import 'dayjs/locale/id';

import App from './components/App.vue';

window.debounce = debounce;
window.translateWords = translateWords;

Vue.use(Dayjs, {
	lang: 'id'
});

Vue.component('app', App);

/* Register swiper as global component */
Vue.component('swiper', swiper);
Vue.component('swiper-slide', swiperSlide);

const app = new Vue({
	el: '#app',
	mixins: [mixinAuth],
	data() {
		return {
			state: {
				token: document.head.querySelector('meta[name="csrf-token"]').content,
				authCheck: {},
				authData: {}
			}
		};
	},
	mounted() {
		try {
			document.getElementById('preloading').remove();
		} catch (e) {
			document.getElementById('preloading').style.display = 'none';
		}
	}
});
