import _find from 'lodash/find';
import _findIndex from 'lodash/findIndex';
import _get from 'lodash/get';
import _slice from 'lodash/slice';

import IconArrowDown from 'Js/@icons/ArrowDown';

import LinkCTA from 'Js/@components/LinkCTA';
import RoomCard from 'Js/@components/RoomCardMobile';
import SkeletonLoader from 'Js/@components/SkeletonLoader';
import UnsupportedLocationAlert from 'Js/@components/UnsupportedLocationAlert';

import TitleSection from './../TitleSection';

import { genderLabels } from 'Js/@utils/kostFilter';
import { translateWords } from 'Js/@utils/langTranslator.js';
import { generateRoomData, createSetOfData } from 'Js/@utils/roomCardHelper.js';
import { makeAPICall } from 'Js/@utils/makeAPICall.js';

export default {
	components: {
		IconArrowDown,
		LinkCTA,
		RoomCard,
		TitleSection,
		SkeletonLoader,
		UnsupportedLocationAlert
	},
	data() {
		return {
			selectedLocation: 'Semua Kota',
			promotedRooms: [],
			isShowGeoLocationAlert: false,
			isLoading: false,
			isEmptyList: false,
			locationOptions: [],
			okeKostId: 0,
			superKostId: 0
		};
	},
	created() {
		this.getKostLevelId();
	},
	mounted() {
		this.getCityList();
	},
	methods: {
		getKostLevelId() {
			axios
				.get('/config/general')
				.then(response => {
					const kostLevels = response.data['kost-level'].list || [];
					kostLevels.forEach(kostLevel => {
						if (kostLevel.type === 'oke') {
							this.okeKostId = parseInt(kostLevel.id);
						}

						if (kostLevel.type === 'super') {
							this.superKostId = parseInt(kostLevel.id);
						}
					});
				})
				.catch(error => {
					bugsnagClient.notify(error);
				});
		},
		getCityList() {
			axios
				.get('/filters/promo/cities')
				.then(response => {
					if (response && response.status) {
						const cityData = _get(response.data, 'cities', []);
						this.locationOptions = [this.selectedLocation, ...cityData];
						const params = {
							...this.defaultParam
						};
						this.getPromotedRooms(params);
					}
				})
				.catch(error => {
					bugsnagClient.notify(error);
				});
		},
		setData(rooms) {
			const limit = _get(this.defaultParam, 'limit', 4);
			const roomBeforeGenerated = _slice(rooms, 0, limit);
			const generatedRoomData = generateRoomData(
				roomBeforeGenerated,
				false,
				this.navigator.isMobile
			);
			this.promotedRooms = this.navigator.isMobile
				? generatedRoomData
				: createSetOfData(generatedRoomData, 4, false);
			this.isLoading = false;
		},
		setLoveStatus(id, status) {
			const roomData = _find(this.promotedRooms, room => room._id === id);
			const roomIndex = _findIndex(this.promotedRooms, room => room._id === id);
			roomData.love_by_me = status;
			this.promotedRooms.splice(roomIndex, 1, roomData);
		},
		async getPromotedRooms(param) {
			this.isLoading = true;
			const response = await makeAPICall({
				method: 'post',
				url: '/stories/list',
				data: param
			});
			if (
				response &&
				typeof response === 'object' &&
				response.rooms &&
				translateWords
			) {
				const roomsData = _get(response, 'rooms', '');
				const decryptedRooms = roomsData ? translateWords(roomsData) : [];
				this.setData(decryptedRooms);
				this.isEmptyList = decryptedRooms.length === 0;
			} else {
				this.isLoading = false;
				this.isEmptyList = true;
				this.promotedRooms = [];
			}
		},
		handleClickRoom(room) {
			const url = _get(room, 'share_url');
			const seq = _get(room, '_id');

			tracker('moe', [
				'Homepage Recommended Promo Kos Click',
				{
					is_booking: _get(room, 'is_booking', false),
					is_mamirooms: _get(room, 'is_mamirooms', false),
					is_premium: _get(room, 'is_premium_owner', false),
					is_promoted: _get(room, 'is_promoted', false),
					is_owner: this.isOwner,
					property_id: seq,
					property_type: 'Kost',
					property_name: _get(room, 'room-title'),
					property_monthly_price: _get(room, 'price_title_format.price', 0),
					property_rent_type: _get(
						room,
						'price_title_format.rent_type_unit',
						''
					),
					property_last_updated_date: _get(room, 'last_updated'),
					property_update_status: _get(room, 'updated'),
					property_url: _get(room, 'share_url'),
					property_gender: genderLabels[_get(room, 'gender', 0)],
					property_room_available: _get(room, 'available_room'),
					property_photo_url_medium: _get(room, 'photo_url.medium'),
					property_status: _get(room, 'status_kos'),
					property_city: _get(room, 'city'),
					property_subdistrict: _get(room, 'subdistrict'),
					property_area: _get(room, 'area_label')
				}
			]);

			window.open(`${url}?redirection_source=home promo recommendation`);
		}
	},
	computed: {
		isOwner() {
			return _get(authCheck, 'owner');
		},
		defaultParam() {
			return {
				filters: {
					promotion: true,
					property_type: 'kost',
					place: []
				},
				include_promoted: true,
				limit: this.navigator.isMobile ? 4 : 8,
				offset: 0
			};
		}
	},
	watch: {
		selectedLocation(updatedVal) {
			const params = { ...this.defaultParam };

			if (updatedVal.toLowerCase() !== 'semua kota') {
				params.filters.place = [updatedVal];
			} else {
				params.filters.place = [];
			}
			this.getPromotedRooms(params);
		}
	}
};
