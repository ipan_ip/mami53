import _find from 'lodash/find';
import _findIndex from 'lodash/findIndex';
import _get from 'lodash/get';
import _slice from 'lodash/slice';

import IconArrowDown from 'Js/@icons/ArrowDown';
import LinkCTA from 'Js/@components/LinkCTA';
import RoomCard from 'Js/@components/RoomCardMobile';
import SkeletonLoader from 'Js/@components/SkeletonLoader';
import TitleSection from './../TitleSection';
import UnsupportedLocationAlert from 'Js/@components/UnsupportedLocationAlert';

import { genderLabels } from 'Js/@utils/kostFilter';
import { translateWords } from 'Js/@utils/langTranslator.js';
import { generateRoomData, createSetOfData } from 'Js/@utils/roomCardHelper.js';
import { makeAPICall } from 'Js/@utils/makeAPICall.js';
import { round } from 'Js/@utils/math';

export default {
	components: {
		IconArrowDown,
		LinkCTA,
		RoomCard,
		TitleSection,
		SkeletonLoader,
		UnsupportedLocationAlert
	},
	data() {
		return {
			selectedLocation: '',
			isShowGeoLocationAlert: false,
			isLoading: false,
			isEmptyList: false,
			myLocationOption: {
				id: 'myLocation',
				name: 'Lokasi Saya'
			},
			recommendedRooms: [],
			okeKostId: 0,
			recommendationCities: [],
			superKostId: 0
		};
	},
	created() {
		this.getKostLevelId();
	},
	mounted() {
		this.getCitiesList();
	},
	methods: {
		async getKostLevelId() {
			const response = await makeAPICall({
				method: 'get',
				url: '/config/general'
			});
			if (response) {
				const kostLevels = response['kost-level'].list || [];
				kostLevels.forEach(kostLevel => {
					if (kostLevel.type === 'oke') {
						this.okeKostId = parseInt(kostLevel.id);
					}

					if (kostLevel.type === 'super') {
						this.superKostId = parseInt(kostLevel.id);
					}
				});
			}
		},

		setData(rooms) {
			const limit = _get(this.defaultParam, 'limit', 4);
			const roomBeforeGenerated = _slice(rooms, 0, limit);
			const generatedRoomData = generateRoomData(
				roomBeforeGenerated,
				false,
				this.navigator.isMobile
			);
			this.recommendedRooms = this.navigator.isMobile
				? generatedRoomData
				: createSetOfData(generatedRoomData, 4, false);
			this.isLoading = false;
		},

		getCitiesList() {
			this.isLoading = true;
			axios
				.get('/cities')
				.then(response => {
					if (response && response.status) {
						const citiesData = _get(response.data, 'cities', []);
						if (!citiesData.length) {
							this.isEmptyList = true;
							this.isLoading = false;
						} else {
							this.recommendationCities = [
								...citiesData,
								this.myLocationOption
							];
							this.selectedLocation = citiesData[0];

							const location = _get(this.selectedLocation, 'location', []);

							const params = {
								...this.defaultParam,
								landing_source: _get(
									this.selectedLocation,
									'landing_source',
									''
								),
								location: location.map(item => [
									round(_get(item, '[0]', 0), 6),
									round(_get(item, '[1]', 0), 6)
								])
							};
							this.getRecommendationRooms(params);
						}
					} else {
						this.isEmptyList = true;
						this.isLoading = false;
						this.recommendedRooms = [];
					}
				})
				.catch(error => {
					this.isEmptyList = true;
					this.isLoading = false;
					this.recommendedRooms = [];
					bugsnagClient.notify(error);
				});
		},
		setLoveStatus(id, status) {
			const roomData = _find(this.recommendedRooms, room => room._id === id);
			const roomIndex = _findIndex(
				this.recommendedRooms,
				room => room._id === id
			);
			roomData.love_by_me = status;
			this.recommendedRooms.splice(roomIndex, 1, roomData);
		},
		async getRecommendationRooms(param) {
			if (!this.isLoading) this.isLoading = true;
			const response = await makeAPICall({
				method: 'post',
				url: '/stories/list',
				data: param
			});
			if (
				response &&
				typeof response === 'object' &&
				response.rooms &&
				translateWords
			) {
				const roomsData = _get(response, 'rooms', '');
				const decryptedRooms = roomsData ? translateWords(roomsData) : [];
				this.setData(decryptedRooms);
				this.isEmptyList = decryptedRooms.length === 0;
			} else {
				this.isEmptyList = true;
				this.recommendedRooms = [];
				this.isLoading = false;
			}
		},
		getCurrentPosition() {
			const options = {
				enableHighAccuracy: false,
				timeout: 10000,
				maximumAge: 1800000
			};

			const onSuccess = res => {
				this.getRoomsByCurrentLocation(res.coords);
			};

			const onError = () => {
				this.selectedLocation = this.recommendationCities[0].name;
				this.isShowGeoLocationAlert = true;
			};

			if (navigator.geolocation) {
				navigator.geolocation.getCurrentPosition(onSuccess, onError, options);
			} else {
				this.isShowGeoLocationAlert = true;
			}
		},
		getRoomsByCurrentLocation({ latitude, longitude }) {
			const DISTANT_RATIO = 0.02;
			const params = {
				...this.defaultParam,
				location: [
					[
						round(longitude - DISTANT_RATIO, 6),
						round(latitude - DISTANT_RATIO, 6)
					],
					[
						round(longitude + DISTANT_RATIO, 6),
						round(latitude + DISTANT_RATIO, 6)
					]
				]
			};

			this.getRecommendationRooms(params);
		},
		handleGoToMore() {
			window.location.href = `/kost/${_get(
				this.selectedLocation,
				'landing_source',
				''
			)}`;
		},
		handleClickRoom(room) {
			const url = _get(room, 'share_url');
			const seq = _get(room, '_id');

			tracker('moe', [
				'Homepage Recommended Kos Click',
				{
					is_booking: _get(room, 'is_booking', false),
					is_mamirooms: _get(room, 'is_mamirooms', false),
					is_premium: _get(room, 'is_premium_owner', false),
					is_promoted: _get(room, 'is_promoted', false),
					is_owner: this.isOwner,
					property_id: seq,
					property_type: 'Kost',
					property_name: _get(room, 'room-title'),
					property_monthly_price: _get(room, 'price_title_format.price', 0),
					property_rent_type: _get(
						room,
						'price_title_format.rent_type_unit',
						''
					),
					property_last_updated_date: _get(room, 'last_updated'),
					property_update_status: _get(room, 'updated'),
					property_url: _get(room, 'share_url'),
					property_gender: genderLabels[_get(room, 'gender', 0)],
					property_room_available: _get(room, 'available_room'),
					property_photo_url_medium: _get(room, 'photo_url.medium'),
					property_status: _get(room, 'status_kos'),
					property_city: _get(room, 'city'),
					property_subdistrict: _get(room, 'subdistrict'),
					property_area: _get(room, 'area_label')
				}
			]);

			window.open(`${url}?redirection_source=home recommendation`);
		}
	},
	computed: {
		locationOptions() {
			return [...this.recommendationCities];
		},
		isOwner() {
			return _get(authCheck, 'owner');
		},
		defaultParam() {
			return {
				filters: {
					gender: [0, 1, 2],
					price_range: [0, 10000000],
					rent_type: 2,
					property_type: 'kost',
					random_seeds: Math.floor(Math.random() * (1000 - 0 + 1)) + 0,
					flash_sale: false
				},
				sorting: { fields: 'price', direction: '-' },
				include_promoted: true,
				include_pinned: true,
				landing_source: '',
				limit: this.navigator.isMobile ? 4 : 8,
				offset: 0,
				location: [],
				is_available: true
			};
		}
	},
	watch: {
		selectedLocation(updatedVal, prevVal) {
			if (typeof prevVal !== 'object') return;

			if (updatedVal.id === 'myLocation') {
				this.getCurrentPosition();
			} else {
				const location = _get(updatedVal, 'location', []);

				const params = {
					...this.defaultParam,
					landing_source: _get(updatedVal, 'landing_source', ''),
					location: location.map(item => [
						round(_get(item, '[0]', 0), 6),
						round(_get(item, '[1]', 0), 6)
					])
				};

				this.getRecommendationRooms(params);
			}
		}
	}
};
