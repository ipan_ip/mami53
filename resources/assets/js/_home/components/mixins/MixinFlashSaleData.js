import _find from 'lodash/find';
import _findIndex from 'lodash/findIndex';
import _get from 'lodash/get';
import _slice from 'lodash/slice';

import IconArrowDown from 'Js/@icons/ArrowDown';

import LinkCTA from 'Js/@components/LinkCTA';
import RoomCard from 'Js/@components/RoomCardMobile';
import SkeletonLoader from 'Js/@components/SkeletonLoader';
import UnsupportedLocationAlert from 'Js/@components/UnsupportedLocationAlert';

import TitleSection from './../TitleSection';

import { genderLabels } from 'Js/@utils/kostFilter';
import { translateWords } from 'Js/@utils/langTranslator.js';
import { generateRoomData, createSetOfData } from 'Js/@utils/roomCardHelper.js';
import { makeAPICall } from 'Js/@utils/makeAPICall.js';

export default {
	components: {
		IconArrowDown,
		LinkCTA,
		RoomCard,
		TitleSection,
		SkeletonLoader,
		UnsupportedLocationAlert
	},
	data() {
		return {
			selectedLocation: 'Semua Kota',
			locationOptions: [],
			flashRooms: [],
			isShowGeoLocationAlert: false,
			getIsLoading: true,
			isListLoading: true,
			isEmptyList: false,
			flashSaleData: null,
			getFlashSaleStatus: '',
			getFlashSaleTimeline: [],
			defaultParam: {
				page: 1,
				filters: {
					gender: [0, 1, 2],
					rent_type: 2,
					property_type: 'kos',
					flash_sale: true
				},
				landings: [],
				limit: 8,
				offset: 0,
				total: 0
			},
			isShowing: true
		};
	},
	destroyed() {
		this.resetTimer();
	},
	mounted() {
		this.defaultParam.limit = this.navigator.isMobile ? 4 : 8;
		this.fetchFlashSaleData();
	},
	methods: {
		resetData() {
			this.isShowing = false;
			this.isEmptyList = true;
			this.getIsLoading = false;
			this.isListLoading = false;
		},
		async fetchFlashSaleData() {
			this.getIsLoading = true;
			this.isListLoading = true;
			const response = await makeAPICall({
				method: 'get',
				url: 'flash-sale/running'
			});
			if (response) {
				const payload = response.data;
				if (payload) {
					this.getFlashSaleStatus = 'running';
					this.selectedLocation = payload.areas[0].name;
					this.locationOptions = payload.areas.map(value => value.name);
					this.defaultParam.landings = payload.areas[0].landings;
					this.getFlashSaleTimeline = [payload.start, payload.end];
					this.flashSaleData = payload;
					this.getIsLoading = false;
					this.getFlashRooms(this.defaultParam);
					this.callCountdownTimer();
				} else {
					this.resetData();
				}
			} else {
				this.resetData();
			}
		},
		handleGoToMore() {
			const url = `/kost-promo-ngebut/${this.selectedLocation
				.replace(/ /g, '_')
				.toLowerCase()}`;
			window.location.href = url;
		},
		setData(rooms) {
			const limit = _get(this.defaultParam, 'limit', 4);
			const roomBeforeGenerated = _slice(rooms, 0, limit);
			const generatedRoomData = generateRoomData(
				roomBeforeGenerated,
				false,
				this.navigator.isMobile
			);
			this.flashRooms = this.navigator.isMobile
				? generatedRoomData
				: createSetOfData(generatedRoomData, 4, false);
			this.isListLoading = false;
		},
		setLoading(status) {
			this.getIsLoading = status;
			this.isListLoading = status;
		},
		setLoveStatus(id, status) {
			const roomData = _find(this.flashRooms, room => room._id === id);
			const roomIndex = _findIndex(this.flashRooms, room => room._id === id);
			roomData.love_by_me = status;
			this.flashRooms.splice(roomIndex, 1, roomData);
		},
		async getFlashRooms(param) {
			if (!this.isListLoading) this.isListLoading = true;
			const response = await makeAPICall({
				method: 'post',
				url: '/stories/list',
				data: param
			});

			if (
				response &&
				typeof response === 'object' &&
				response.rooms &&
				translateWords
			) {
				const rooms = translateWords(response.rooms) || [];

				this.setData(rooms);
				this.isEmptyList = this.flashRooms.length < 1;
			} else {
				this.isListLoading = false;
				this.isEmptyList = true;
			}
		},
		handleClickRoom(room) {
			const url = _get(room, 'share_url');
			const seq = _get(room, '_id');

			tracker('moe', [
				'Homepage Recommended Promo Kos Click',
				{
					is_booking: _get(room, 'is_booking', false),
					is_mamirooms: _get(room, 'is_mamirooms', false),
					is_premium: _get(room, 'is_premium_owner', false),
					is_promoted: _get(room, 'is_promoted', false),
					is_owner: this.isOwner,
					property_id: seq,
					property_type: 'Kost',
					property_name: _get(room, 'room-title'),
					property_monthly_price: _get(room, 'price_title_format.price', 0),
					property_rent_type: _get(
						room,
						'price_title_format.rent_type_unit',
						''
					),
					property_last_updated_date: _get(room, 'last_updated'),
					property_update_status: _get(room, 'updated'),
					property_url: _get(room, 'share_url'),
					property_gender: genderLabels[_get(room, 'gender', 0)],
					property_room_available: _get(room, 'available_room'),
					property_photo_url_medium: _get(room, 'photo_url.medium'),
					property_status: _get(room, 'status_kos'),
					property_city: _get(room, 'city'),
					property_subdistrict: _get(room, 'subdistrict'),
					property_area: _get(room, 'area_label')
				}
			]);

			window.open(`${url}?redirection_source=home promo ngebut`);
		}
	},
	computed: {
		isOwner() {
			return _get(authCheck, 'owner');
		}
	},
	watch: {
		selectedLocation(updatedVal) {
			const landingData = _find(
				this.flashSaleData.areas,
				area => area.name === updatedVal
			);
			const params = {
				...this.defaultParam
			};
			params.landings = landingData.landings;
			this.getFlashRooms(params);
		}
	}
};
