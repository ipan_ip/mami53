require('../starter2');

window.Vue = require('vue').default;

import Vuex from 'vuex';
Vue.use(Vuex);

import mixinAuth from '../@mixins/MixinAuth';

require('../@mixins/MixinCenterModal');
require('../@mixins/MixinScrollTop');
require('Js/@mixins/MixinSwalLoading');
require('Js/@mixins/MixinSwalSuccessError');

const App = require('./components/landing-project/App.vue').default;
const ContainerLandingProject = require('./components/landing-project/ContainerLandingProject.vue')
	.default;

// Container Component
Vue.component('app', App);
Vue.component('container-landing-project', ContainerLandingProject);

// Global Components
Vue.component(
	'mami-loading',
	require('../@components/MamiLoading.vue').default
);
Vue.component(
	'mami-loading-inline',
	require('../@components/MamiLoadingInline.vue').default
);
Vue.component(
	'mami-loading-list',
	require('../@components/MamiLoadingList.vue').default
);
Vue.component('mami-footer', require('../@components/MamiFooter.vue').default);
Vue.component(
	'breadcrumb-trails',
	require('../@components/BreadcrumbTrails.vue').default
);
Vue.component(
	'location-osm',
	require('../@components/LocationOsm.vue').default
);
Vue.component(
	'list-placeholder',
	require('../@components/ListPlaceholder.vue').default
);
Vue.component(
	'landing-unit-list',
	require('../@components/RoomList.vue').default
);

// Local Components
Vue.component(
	'landing-title',
	require('./components/LandingTitle.vue').default
);
Vue.component(
	'landing-description',
	require('./components/LandingDescription.vue').default
);
Vue.component(
	'landing-photo',
	require('./components/LandingPhoto.vue').default
);
Vue.component(
	'landing-available',
	require('./components/LandingAvailable.vue').default
);
Vue.component('landing-map', require('./components/LandingMap.vue').default);
Vue.component('landing-data', require('./components/LandingData.vue').default);
Vue.component(
	'landing-data-address',
	require('./components/LandingDataAddress.vue').default
);
Vue.component(
	'landing-data-phone',
	require('./components/LandingDataPhone.vue').default
);
Vue.component(
	'landing-data-facility',
	require('./components/LandingDataFacility.vue').default
);
Vue.component(
	'landing-data-info',
	require('./components/LandingDataInfo.vue').default
);
Vue.component(
	'landing-data-type',
	require('./components/LandingDataType.vue').default
);
Vue.component(
	'landing-data-tower',
	require('./components/LandingDataTower.vue').default
);
Vue.component(
	'landing-data-share',
	require('./components/LandingDataShare.vue').default
);
Vue.component('landing-unit', require('./components/LandingUnit.vue').default);
Vue.component(
	'landing-child-title',
	require('./components/LandingChildTitle.vue').default
);
Vue.component(
	'landing-recommendation',
	require('./components/LandingRecommendation.vue').default
);

const store = new Vuex.Store({
	state: {
		token: document.head.querySelector('meta[name="csrf-token"]').content,
		authCheck: {},
		authData: {},
		project: {},
		suggestions: {},
		filters: {
			unitType: '',
			furnished: 'all',
			priceRange: [0, 50000000],
			tagIds: [],
			rentType: 2
		},
		sorting: {
			fields: 'price',
			direction: '-'
		},
		extras: {
			landingType: 'project',
			windowWidth: $(window).width(),
			random_seeds: Math.floor(Math.random() * (1000 - 0 + 1)) + 0
		}
	},
	mutations: {
		setAuthCheck(state, payload) {
			state.authCheck = payload;
		},
		setAuthData(state, payload) {
			state.authData = payload;
		},
		changeSection(state, payload) {
			state.section = payload;
		},
		setProjectData(state, payload) {
			state.project = payload;
		},
		updateWindowWidth(state, payload) {
			state.extras.windowWidth = payload;
		},
		setSuggestionsData(state, payload) {
			state.suggestions = payload;
		},
		setFilterUnitType(state, payload) {
			state.filters.unitType = payload;
		},
		setFilterFurnished(state, payload) {
			state.filters.furnished = payload;
		},
		setFilterUnitPriceRange(state, payload) {
			state.filters.priceRange = payload;
		},
		setFilterTagIds(state, payload) {
			state.filters.tagIds = payload;
		},
		setFilterRentType(state, payload) {
			state.filters.rentType = payload;
		},
		setSortingDirection(state, payload) {
			state.sorting.direction = payload;
		}
	}
});

Vue.config.productionTip = false;

const app = new Vue({
	el: '#app',
	store,
	mixins: [mixinAuth],
	created() {
		this.$store.commit('setProjectData', mamiApartmentProject);
		this.$store.commit('setSuggestionsData', mamiApartmentSuggestion);
	},
	mounted() {
		try {
			document.getElementById('prerender').remove();
		} catch (e) {
			document.getElementById('prerender').style.display = 'none';
		}
	}
});
