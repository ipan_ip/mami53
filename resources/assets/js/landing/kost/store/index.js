import _get from 'lodash/get';
import { sanitizeFilterTag } from 'Js/@utils/kostFilter';
import { translateWords } from 'Js/@utils/langTranslator.js';
import { makeAPICall } from 'Js/@utils/makeAPICall.js';
import { round } from 'Js/@utils/math';

export default {
	state: {
		token: document.head.querySelector('meta[name="csrf-token"]').content,
		authCheck: {},
		authData: {},
		centerpoint: {},
		landing: {
			filters: {
				gender: [],
				price_range: [10000, 20000000],
				tag_ids: [],
				level_info: [],
				rent_type: 2,
				property_type: 'all',
				random_seeds: Math.floor(Math.random() * (1000 - 0 + 1)) + 0,
				booking: false,
				flash_sale: false,
				mamichecker: false,
				mamirooms: false,
				goldplus: []
			},
			sorting: {
				field: 'price',
				direction: '-'
			},
			include_promoted: false,
			landing_source: '',
			limit: 20,
			location: [],
			point: {},
			offset: 0,
			page: 1,
			referer: 'landing',
			radius: null
		},
		isGoldPlus: false,
		isLoading: true,
		tagRooms: [],
		tagShare: [],
		tagRules: [],
		rentTypeList: [],
		isFacLoading: true,
		isFlashSaleAvailable: false,
		extras: {
			landing_connector: [],
			breadcrumb: [],
			place: null,
			landing_related_area: [],
			landing_related_campus: [],
			description: {
				description_1: '',
				description_2: '',
				description_3: ''
			},
			centerLatitude: null,
			centerLongitude: null,
			cornerMap: [],
			cluster: {},
			list: {},
			isClusterOpen: false
		},
		faqs: {},
		historyPoint: -1
	},
	getters: {
		getAllParams: state => {
			return state.landing;
		},
		getKostSorting: state => {
			return [state.landing.sorting.field, state.landing.sorting.direction];
		},
		getKostFilters: state => {
			return state.landing.filters;
		},
		getTagRooms: state => {
			return state.tagRooms;
		},
		getTagShare: state => {
			return state.tagShare;
		},
		getTagRules: state => {
			return state.tagRules;
		},
		getRentTypeList: state => {
			return state.rentTypeList;
		},
		getRooms: state => {
			return _get(state.extras, 'list.rooms', []);
		},
		getEmptyList: state => {
			return !_get(state.extras, 'list.rooms', []).length;
		},
		getTotalRooms: state => {
			return _get(state.extras, 'list.total', 0);
		},
		getClusterRooms: state => {
			return _get(state.extras, 'cluster.rooms', []);
		},
		getIsFacilitiesExist: state => {
			return state.tagRooms.length && state.tagShare.length;
		},
		getClusterStatus: state => {
			return state.extras.isClusterOpen;
		}
	},
	mutations: {
		setFlashSaleAvailability(state, payload) {
			state.isFlashSaleAvailable = payload;
		},
		setIsGoldPlus(state, payload) {
			state.isGoldPlus = payload;
		},
		setFacLoading(state, payload) {
			state.isFacLoading = payload;
		},
		setTagRooms(state, payload) {
			const tagRoomsList = sanitizeFilterTag(payload);
			state.tagRooms = tagRoomsList;
		},
		setTagShare(state, payload) {
			const tagShareList = sanitizeFilterTag(payload);
			state.tagShare = tagShareList;
		},
		setTagRules(state, payload) {
			const tagRulesList = sanitizeFilterTag(payload);
			state.tagRules = tagRulesList;
		},
		setRentTypeList(state, payload) {
			const rentList = payload.map(rent => {
				return {
					...rent,
					value: rent.id,
					name: rent.rent_name
				};
			});
			state.rentTypeList = rentList;
		},
		setPoint(state, payload) {
			state.landing.point = payload;
		},
		setIsLoading(state, payload) {
			state.isLoading = payload;
		},
		setFilter(state, payload) {
			state.landing.filters = {
				...state.landing.filters,
				...payload
			};

			if (state.extras.isClusterOpen) state.extras.isClusterOpen = false;
		},
		setSort(state, payload) {
			state.landing.sorting.field = payload[0];
			state.landing.sorting.direction = payload[1];
		},

		setTagList(state, payload) {
			state.tagList = payload;
		},
		setTagListOthers(state, payload) {
			state.tagListOthers = payload;
		},
		setAuthCheck(state, payload) {
			state.authCheck = payload;
		},
		setAuthData(state, payload) {
			state.authData = payload;
		},
		setMapCenter(state, payload) {
			state.centerpoint = payload;
		},
		setKostFilterType(state, payload) {
			if (typeof payload === 'string') {
				const kostType = payload.split(',').map(item => {
					return parseInt(item, 10);
				});

				state.landing.filters.gender = kostType;
			} else {
				state.landing.filters.gender = payload;
			}
		},
		setKostFilterRentType(state, payload) {
			state.landing.filters.rent_type = parseInt(payload);
		},
		setKostFilterFlashSale(state, payload) {
			state.landing.filters.flash_sale = payload;
		},
		setKostFilterPriceRange(state, payload) {
			state.landing.filters.price_range = payload;
		},
		setKostFilterTagIds(state, payload) {
			state.landing.filters.tag_ids = payload;
		},
		setKostFilterLevel(state, payload) {
			state.landing.filters.level_info = payload;
		},
		setKostFilterBooking(state, payload) {
			state.landing.filters.booking = payload;
		},
		setKostSorting(state, payload) {
			state.landing.sorting.fields = payload[0];
			state.landing.sorting.direction = payload[1];
		},
		setKostFilter(state, payload) {
			state.landing.filters = payload;
		},
		setKostLandingSource(state, payload) {
			state.landing.landing_source = payload;
		},

		/**
		 * Make sure you pass this format for the payload:
		 * [ [long, lat], [long, lat] ]
		 */
		setKostLocation(state, payload) {
			payload = payload.map(item => [
				round(_get(item, '[0]', 0), 6),
				round(_get(item, '[1]', 0), 6)
			]);

			state.landing.location = payload;
		},
		setKostPageList(state, payload) {
			state.landing.page = payload;
		},
		setKostOffset(state, payload) {
			state.landing.offset = payload;
		},
		setKostRadius(state, payload) {
			state.landing.radius = payload;
		},
		setKostLandingConnector(state, payload) {
			state.extras.landing_connector = payload;
		},
		setKostBreadcrumb(state, payload) {
			state.extras.breadcrumb = payload;
		},
		setKostPlace(state, payload) {
			state.extras.place = payload;
		},
		setKostLandingRelatedArea(state, payload) {
			state.extras.landing_related_area = payload;
		},
		setKostLandingRelatedCampus(state, payload) {
			state.extras.landing_related_campus = payload;
		},
		setKostLandingDescription(state, payload) {
			state.extras.description.description_1 = payload.description_1;
			state.extras.description.description_2 = payload.description_2;
			state.extras.description.description_3 = payload.description_3;
		},
		setKostLatitude(state, payload) {
			state.extras.centerLatitude = payload;
		},
		setKostLongitude(state, payload) {
			state.extras.centerLongitude = payload;
		},
		setKostCornerMap(state, payload) {
			state.extras.cornerMap = payload;
		},
		setKostCluster(state, payload) {
			state.extras.cluster = payload;
		},
		setKostList(state, payload) {
			state.extras.list = payload;
		},
		setClusterOpened(state, payload) {
			state.extras.isClusterOpen = payload;
		},
		setLandingFaq(state, payload) {
			state.faqs = payload;
		},
		setHistoryPoint(state) {
			state.historyPoint = state.historyPoint - 1;
		}
	},
	actions: {
		settingFilter({ commit }, payload) {
			commit('setFilter', payload);
		},
		settingSort({ commit, dispatch }, payload) {
			commit('setSort', payload);
			dispatch('fetchRooms');
		},
		settingTagIds({ commit, dispatch }, payload) {
			commit('setFilter', payload.content);
			if (payload.search) {
				dispatch('fetchRooms');
			}
		},
		async fetchRooms({ commit, state, dispatch }) {
			window.scrollTo({ top: 0, behavior: 'smooth' });
			commit('setKostLandingSource', '');
			if (state.extras.isClusterOpen) {
				dispatch('fetchClusterList');
			} else {
				commit('setIsLoading', true);
				if (state.landing.location.length === 0) {
					const payload = [
						[106.69793128967285, -6.296188782911225],
						[106.76608085632326, -6.271703381316111]
					];
					commit('setKostLocation', payload);
				}
				const params = state.landing;
				const genderFilter = {
					gender: params.filters.gender.length
						? params.filters.gender
						: [0, 1, 2]
				};
				try {
					const response = await makeAPICall({
						method: 'post',
						url: '/stories/list',
						data: {
							...params,
							filters: {
								...params.filters,
								...genderFilter
							}
						}
					});

					if (response) {
						const rooms = translateWords(response.rooms) || [];
						commit('setKostList', { ...response, rooms });
					} else {
						commit('setKostList', []);
					}
				} catch (error) {
					commit('setKostList', []);
					bugsnagClient.notify(error);
				} finally {
					commit('setIsLoading', false);
				}

				if (window.innerWidth > 1280) {
					try {
						const resRoomCluster = await makeAPICall({
							method: 'post',
							url: '/stories/cluster',
							data: {
								...params,
								filters: {
									...params.filters,
									...genderFilter
								}
							}
						});

						resRoomCluster && commit('setKostCluster', resRoomCluster);
					} catch (error) {
						bugsnagClient.notify(error);
					}
				}
			}
		},
		async fetchFacilitiesList({ commit }) {
			commit('setFacLoading', true);
			try {
				const response = await makeAPICall({
					method: 'get',
					url: '/stories/filters'
				});
				if (response) {
					commit('setTagRooms', response.fac_room);
					commit('setTagShare', response.fac_share);
					commit('setTagRules', response.kos_rule);
					commit('setRentTypeList', response.rent_type);
				}
			} catch (error) {
				bugsnagClient.notify(error);
			} finally {
				commit('setFacLoading', false);
			}
		},
		async fetchClusterList({ commit, state }) {
			commit('setIsLoading', true);
			const params = state.landing;
			const genderFilter = {
				gender: params.filters.gender.length ? params.filters.gender : [0, 1, 2]
			};
			try {
				const response = await makeAPICall({
					method: 'post',
					url: '/room/list/cluster',
					data: {
						...params,
						filters: {
							...params.filters,
							...genderFilter
						}
					}
				});

				response && commit('setKostList', response);
			} catch (error) {
				bugsnagClient.notify(error);
			} finally {
				commit('setIsLoading', false);
			}
		}
	}
};
