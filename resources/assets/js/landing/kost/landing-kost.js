require('../../starter3');

window.Vue = require('vue').default;

import Vuex from 'vuex';
import Storage from 'vue-ls';
import StoreData from './store';
import mixinAuth from '../../@mixins/MixinAuth';

const options = {
	namespace: 'mamikos',
	name: 'ls',
	storage: 'session'
};

Vue.use(Vuex);
Vue.use(Storage, options);

require('../../@mixins/MixinNavigatorIsMobile');
require('../../@mixins/MixinSwalLoading');
require('../../@mixins/MixinSwalSuccessError');

const App = require('./components/App.vue').default;

// Container Component
Vue.component('app', App);

const store = new Vuex.Store(StoreData);

const app = new Vue({
	el: '#app',
	store,
	mixins: [mixinAuth],
	created() {
		let filterData = null;

		filterData = this.$ls.get('filter' + window.location.pathname);

		this.$store.commit(
			'setKostFilterRentType',
			filterData === null ? rentType : filterData.filters.rent_type
		);
		this.$store.commit(
			'setKostFilterTagIds',
			filterData === null ? tagIds : filterData.filters.tag_ids
		);
		this.$store.commit(
			'setKostFilterLevel',
			filterData === null ? [] : filterData.filters.level_info
		);

		this.$store.commit('setKostLandingSource', slug);
		this.$store.commit('setKostLocation', locPage);
		this.$store.commit('setKostLandingConnector', landing_connector);
		this.$store.commit('setKostBreadcrumb', breadcrumb);
		this.$store.commit('setKostPlace', place);
		this.$store.commit('setKostLandingRelatedArea', landingRelatedArea);
		this.$store.commit('setKostLandingRelatedCampus', landingRelatedCampus);
		this.$store.commit('setKostLandingDescription', description);
		this.$store.commit('setKostLatitude', centerLatitude);
		this.$store.commit('setKostLongitude', centerLongitude);
		this.$store.commit('setKostCornerMap', cornerMap);
		this.$store.commit('setKostRadius', radius);
		this.$store.commit('setLandingFaq', faqs);
	},
	mounted() {
		try {
			document.getElementById('prerender').remove();
		} catch (e) {
			document.getElementById('prerender').style.display = 'none';
		}
	}
});
