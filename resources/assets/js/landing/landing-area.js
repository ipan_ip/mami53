require('../starter2');

window.Vue = require('vue').default;

import Vuex from 'vuex';
Vue.use(Vuex);

import mixinAuth from '../@mixins/MixinAuth';

require('../@mixins/MixinCenterModal');
require('../@mixins/MixinScrollTop');
require('Js/@mixins/MixinSwalLoading');
require('Js/@mixins/MixinSwalSuccessError');

const App = require('./components/landing-area/App.vue').default;
const ContainerLandingArea = require('./components/landing-area/ContainerLandingArea.vue')
	.default;

// Container Component
Vue.component('app', App);
Vue.component('container-landing-area', ContainerLandingArea);

// Global Components
Vue.component(
	'mami-loading',
	require('../@components/MamiLoading.vue').default
);
Vue.component(
	'mami-loading-inline',
	require('../@components/MamiLoadingInline.vue').default
);
Vue.component(
	'mami-loading-list',
	require('../@components/MamiLoadingList.vue').default
);
Vue.component('mami-footer', require('../@components/MamiFooter.vue').default);
Vue.component('leaflet-map', require('../@components/LeafletMap.vue').default);
Vue.component(
	'breadcrumb-trails',
	require('../@components/BreadcrumbTrails.vue').default
);
Vue.component(
	'list-placeholder',
	require('../@components/ListPlaceholder.vue').default
);
Vue.component(
	'landing-unit-list',
	require('../@components/RoomList.vue').default
);
Vue.component(
	'related-list',
	require('../@components/RelatedList.vue').default
);

// Local Components
Vue.component(
	'landing-title',
	require('./components/LandingTitle.vue').default
);
Vue.component(
	'landing-description',
	require('./components/LandingDescription.vue').default
);
Vue.component(
	'landing-photo',
	require('./components/LandingPhoto.vue').default
);
Vue.component(
	'landing-project-list',
	require('../@components/ProjectList.vue').default
);
Vue.component(
	'landing-child-title',
	require('./components/LandingChildTitle.vue').default
);
Vue.component(
	'landing-switch',
	require('./components/LandingSwitch.vue').default
);

Vue.component('landing-area', require('./components/LandingArea.vue').default);

const store = new Vuex.Store({
	state: {
		token: document.head.querySelector('meta[name="csrf-token"]').content,
		authCheck: {},
		authdata: {},
		area: {},
		extras: {
			landingType: 'area',
			apartmentType: 'unit',
			windowWidth: $(window).width(),
			random_seeds: Math.floor(Math.random() * (1000 - 0 + 1)) + 0,
			tags: {
				unit_facilities: [],
				project_facilities: []
			},
			tag_ids: [],
			tag_ids_project: []
		}
	},
	mutations: {
		setAuthCheck(state, payload) {
			state.authCheck = payload;
		},
		setAuthData(state, payload) {
			state.authData = payload;
		},
		changeSection(state, payload) {
			state.section = payload;
		},
		setAreaData(state, payload) {
			state.area = payload;
		},
		setMapCenter(state, payload) {
			state.center_latitude = payload.lat;
			state.center_longitude = payload.lng;
		},
		changeApartmentType(state, payload) {
			state.extras.apartmentType = payload;
		},
		updateWindowWidth(state, payload) {
			state.extras.windowWidth = payload;
		},
		setUnitTags(state, payload) {
			state.extras.tags.unit_facilities = payload;
		},
		setProjectTags(state, payload) {
			state.extras.tags.project_facilities = payload;
		},
		setFilterFacilities(state, payload) {
			state.extras.tag_ids = payload;
		},
		setFilterFacilitiesProject(state, payload) {
			state.extras.tag_ids_project = payload;
		}
	}
});

Vue.config.productionTip = false;

const app = new Vue({
	el: '#app',
	store,
	mixins: [mixinAuth],
	created() {
		this.$store.commit('setAreaData', mamiLanding);
	},
	mounted() {
		try {
			document.getElementById('prerender').remove();
		} catch (e) {
			document.getElementById('prerender').style.display = 'none';
		}
	}
});
