import ApolloClient from 'apollo-boost';

const apolloClient = new ApolloClient({
	uri: 'https://help.mamikos.com/graphql'

	/* commented because CORS in the staging kacahelp, will be revert it back once its handled */

	// process.env.NODE_ENV === 'development'
	// 	? 'https://kacahelp-dev.kerupux.com/graphql'
	// 	: 'https://help.mamikos.com/graphql'
});

export default apolloClient;
