import 'Js/@mixins/MixinNavigatorIsMobile';
import 'Js/@mixins/MixinSwalLoading';
import mixinAuth from 'Js/@mixins/MixinAuth';
import StoreData from './store';
import routes from './routes';
import Vuex from 'vuex';
import VueRouter from 'vue-router';
import Dayjs from 'vue-dayjs';
import 'dayjs/locale/id';
import VueApollo from 'vue-apollo';
import apolloConfig from './appollo';

/* eslint-disable */
Vue.use(VueApollo);
Vue.use(Dayjs, {
	lang: 'id'
});
Vue.use(VueRouter);
Vue.use(Vuex);
import App from './App.vue';
Vue.config.productionTip = false;

const store = new Vuex.Store(StoreData);
const router = new VueRouter({
	mode: 'history',
	base: '/flash-sale',
	routes
});
const apolloProvider = new VueApollo({
	defaultClient: apolloConfig
});

new Vue({
	el: '#app',
	components: { App },
	mixins: [mixinAuth],
	store,
	router,
	apolloProvider,

	mounted() {
		try {
			document.getElementById('preloading').remove();
		} catch (e) {
			document.getElementById('preloading').style.display = 'none';
		}
	}
});
