import LandingPage from '../FlashSaleLanding';
import ListPage from '../FlashSaleList';

export default [
	{ path: '/promo-ngebut', component: LandingPage },
	{
		path: '/kost-promo-ngebut',
		component: ListPage
	},
	{
		path: '/kost-promo-ngebut/:area',
		component: ListPage
	}
];
