import { getParams } from 'Js/@utils/queryString';
import { makeAPICall } from 'Js/@utils/makeAPICall.js';
import { translateWords } from 'Js/@utils/langTranslator.js';

export default {
	state: {
		authCheck: {},
		authData: {},
		faqData: [],
		areas: [],
		searchModalStatus: false,
		selectedAreas: 0,
		flashSaleData: null,
		flashSaleStatus: '',
		sourceWebview: null,
		activeFaq: 0,
		flashRooms: [],
		isLoading: true,
		isListLoading: true,
		isEmptyList: false,
		params: {
			page: 1,
			filters: {
				gender: [0, 1, 2],
				rent_type: 2,
				property_type: 'kos',
				price_range: [0, 15000000],
				flash_sale: true
			},
			sorting: {
				field: 'price',
				direction: '-'
			},
			landings: [],
			limit: 20,
			offset: 0,
			total: 0
		}
	},
	getters: {
		getSourceWebview: state => {
			return state.sourceWebview;
		},
		getAuthCheck: state => {
			return state.authCheck;
		},
		getSearchStatus: state => {
			return state.searchModalStatus;
		},
		getEmptyList: state => {
			return state.isEmptyList;
		},
		getAllFaq: state => {
			return state.faqData;
		},
		getFlashSaleStatus: state => {
			return state.flashSaleStatus;
		},
		getFlashSaleTimeline: state => {
			return state.flashSaleData
				? [state.flashSaleData.start, state.flashSaleData.end]
				: '';
		},
		getBannerData: state => {
			return state.flashSaleData ? state.flashSaleData.banner : '';
		},
		getIsWebview: state => {
			return state.sourceWebview !== null;
		},
		getActiveQuestion: state => {
			return state.activeFaq;
		},
		getRooms: state => {
			return state.flashRooms;
		},
		getIsLoading: state => {
			return state.isLoading;
		},
		getIsListLoading: state => {
			return state.isListLoading;
		},
		getFilter: state => {
			return state.params.filters;
		},
		getCurrentPage: state => {
			return state.params.page;
		},
		getTotalPage: state => {
			return Math.ceil(state.params.total / state.params.limit);
		},
		getSorting: state => {
			return [state.params.sorting.field, state.params.sorting.direction];
		},
		getDisplayNumber: state => {
			const start = state.params.offset + 1;
			const end = state.params.offset + state.flashRooms.length;
			return `${start} - ${end} Kos dari ${state.params.total}`;
		},
		getLocation: state => {
			return state.areas;
		},
		getSelectedLocation: state => {
			return state.selectedAreas;
		}
	},
	mutations: {
		setAuthCheck(state, payload) {
			state.authCheck = payload;
		},
		setAuthData(state, payload) {
			state.authData = payload;
		},
		setEmptyList(state, payload) {
			state.isEmptyList = payload;
		},
		setFAQStatus(state, payload) {
			state.activeFaq = payload !== state.activeFaq ? payload : -1;
		},
		setFlashSaleData(state, payload) {
			state.flashSaleData = payload;
		},
		setFlashSaleStatus(state, payload) {
			state.flashSaleStatus = payload;
		},
		setSourceWebview(state, payload) {
			state.sourceWebview = payload;
		},
		setFilter(state, payload) {
			state.params.filters = { ...state.params.filters, ...payload };
		},
		setSort(state, payload) {
			state.params.sorting.field = payload[0];
			state.params.sorting.direction = payload[1];
		},
		setAllFilter(state, payload) {
			state.params.filters = payload;
		},
		setPage(state, payload) {
			state.params.page = payload;
		},
		setLanding(state, payload) {
			state.params.landings = payload.landingOptions;
			state.selectedAreas = payload.selected;
		},
		setRooms(state, payload) {
			state.flashRooms = payload.rooms;
			state.params.total = payload.total;
			state.isListLoading = false;
		},
		setOffset(state, payload) {
			state.params.offset = payload;
		},
		setLoading(state, payload) {
			state.isLoading = payload;
		},
		setListLoading(state, payload) {
			state.isListLoading = payload;
		},
		setAreaOptions(state, payload) {
			state.areas = payload;
		},
		setSearchStatus(state, payload) {
			state.searchModalStatus = payload;
		},
		setFAQ(state, payload) {
			state.faqData = payload;
		}
	},
	actions: {
		fetchFlashSaleWithUrl({ commit, dispatch }, searchParam) {
			return new Promise((resolve, reject) => {
				commit('setLoading', true);
				commit('setListLoading', true);
				dispatch('settingSource');
				axios
					.get(`/flash-sale/running`)
					.then(response => {
						const sourceData = response.data.data;
						commit('setFlashSaleData', sourceData);
						commit('setFlashSaleStatus', sourceData ? 'running' : 'empty');
						dispatch('settingAreaLandings', sourceData ? sourceData.areas : []);
						commit('setLoading', false);
						if (sourceData) {
							let areaIndex = 0;
							if (searchParam) {
								const findIndex = sourceData.areas.findIndex(
									value =>
										value.name.toLowerCase() ===
										searchParam.toLowerCase().replace(/_/g, ' ')
								);
								areaIndex = findIndex > -1 ? findIndex : 0;
							}
							const selectedPayload = {
								landingOptions: sourceData.areas[areaIndex].landings,
								selected: areaIndex
							};
							commit('setLanding', selectedPayload);
						}
						dispatch('fetchFlashSaleRoom');
						resolve(true);
					})
					.catch(error => {
						reject(error);
					});
			});
		},
		async fetchFlashSaleData({ commit, dispatch }) {
			commit('setLoading', true);
			commit('setListLoading', true);
			dispatch('settingSource');
			const response = await makeAPICall({
				method: 'get',
				url: '/flash-sale'
			});
			if (response) {
				const payload = response;
				let status = '';
				let sourceData = null;
				if (payload.total_running > 0) {
					sourceData = payload.datas.find(
						flashsale =>
							flashsale.is_running === true && flashsale.is_finished === false
					);
					status = 'running';
				} else if (payload.total_upcoming > 0) {
					sourceData = payload.datas.find(
						flashsale =>
							flashsale.is_running === false && flashsale.is_finished === false
					);
					status = 'upcoming';
				} else {
					status = 'empty';
				}
				commit('setFlashSaleData', sourceData);
				commit('setFlashSaleStatus', status);
				dispatch('settingAreaLandings', sourceData ? sourceData.areas : []);
				commit('setLoading', false);
				if (sourceData) {
					const selectedPayload = {
						landingOptions: sourceData.areas[0].landings,
						selected: 0
					};
					commit('setLanding', selectedPayload);
				}
				dispatch('fetchFlashSaleRoom');
			}
		},
		async fetchFlashSaleRoom({ commit, state }) {
			commit('setListLoading', true);
			const response = await makeAPICall({
				method: 'post',
				url: '/stories/list',
				data: state.params
			});

			if (typeof response === 'object' && response.rooms && translateWords) {
				const rooms = translateWords(response.rooms) || [];
				if (rooms.length > 0) {
					const payload = {
						rooms,
						total: response.total
					};
					commit('setEmptyList', false);
					commit('setRooms', payload);
				} else {
					commit('setEmptyList', true);
				}
			} else {
				commit('setEmptyList', true);
				commit('setRooms', []);
			}

			commit('setListLoading', false);
		},
		settingSource({ commit }) {
			const params = getParams(null);
			const acquiredParams = params.source ? params.source[0] : null;
			commit('setSourceWebview', acquiredParams);
		},
		settingFilter({ commit, dispatch }, payload) {
			commit('setFilter', payload);
			dispatch('fetchFlashSaleRoom');
		},
		settingSort({ commit, dispatch }, payload) {
			commit('setSort', payload);
			dispatch('fetchFlashSaleRoom');
		},
		settingPage({ commit, state, dispatch }, payload) {
			commit('setPage', payload);
			commit('setOffset', (payload - 1) * state.params.limit);
			dispatch('fetchFlashSaleRoom');
		},
		settingLandings({ commit, state, dispatch }, payload) {
			payload = payload || 0;

			const selectedPayload = {
				landingOptions: state.flashSaleData.areas[payload].landings,
				selected: payload
			};
			commit('setLanding', selectedPayload);
			dispatch('fetchFlashSaleRoom');
		},
		settingAreaLandings({ commit }, payload) {
			const areaOptions = payload.map((value, index) => {
				return {
					label: value.name,
					value: index
				};
			});
			commit('setAreaOptions', areaOptions);
		},
		settingAllFilter({ commit, state, dispatch }, payload) {
			commit('setSort', payload.sortingPayload);
			const filters = {
				gender: payload.filterPayload.gender,
				rent_type: payload.filterPayload.rent_type,
				property_type: 'kos',
				price_range: payload.filterPayload.price_range,
				flash_sale: true
			};
			const selectedPayload = {
				landingOptions:
					state.flashSaleData.areas[payload.selectedLocationPayload].landings,
				selected: payload.selectedLocationPayload
			};
			commit('setAllFilter', filters);
			commit('setLanding', selectedPayload);
			dispatch('fetchFlashSaleRoom');
		}
	}
};
