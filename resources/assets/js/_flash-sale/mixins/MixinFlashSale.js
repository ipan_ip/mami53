export default {
	data() {
		return {
			remaining: [
				{
					value: '0',
					tag: 'Hari'
				},
				{
					value: '0',
					tag: 'Jam'
				},
				{
					value: '0',
					tag: 'Menit'
				},
				{
					value: '0',
					tag: 'Detik'
				}
			],
			countdownInterval: null,
			hasVisibilityEvent: false
		};
	},

	methods: {
		setRemaining(payload) {
			this.remaining = this.remaining.map(data => {
				return {
					...data,
					value: payload[data.tag]
				};
			});
		},
		callCountdownTimer() {
			let useDate = null;
			const statusFlashSale = this.getFlashSaleStatus;
			if (statusFlashSale === 'running') {
				useDate = this.getFlashSaleTimeline[1];
			} else if (statusFlashSale === 'upcoming') {
				useDate = this.getFlashSaleTimeline[0];
			}
			document.addEventListener('visibilitychange', this.checkVisibility);
			this.hasVisibilityEvent = true;
			const useDateParse = this.$dayjs(useDate).format('D MMMM YYYY HH:mm:ss');
			if (useDateParse) {
				this.countdownInterval = setInterval(() => {
					const now = new Date().getTime();
					const countdownEnd = new Date(useDateParse).getTime();
					const distance = countdownEnd - now;
					if (distance > 0) {
						const days = Math.floor(distance / (1000 * 60 * 60 * 24));
						const hours = Math.floor(
							(distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
						);
						const minutes = Math.floor(
							(distance % (1000 * 60 * 60)) / (1000 * 60)
						);
						const seconds = Math.floor((distance % (1000 * 60)) / 1000);
						const data = {
							Hari: this.appendZero(days),
							Jam: this.appendZero(hours),
							Menit: this.appendZero(minutes),
							Detik: this.appendZero(seconds)
						};
						this.setRemaining(data);
					} else {
						this.resetTimer();
						this.setLoading(true);
						setTimeout(() => {
							this.fetchFlashSaleData();
						}, 1500);
					}
				}, 1000);
			}
		},
		appendZero(value) {
			return value.toString().length === 1 ? `0${value}` : value.toString();
		},
		resetTimer() {
			clearInterval(this.countdownInterval);
			document.removeEventListener('visibilitychange', this.checkVisibility);
			this.hasVisibilityEvent = false;
			this.countdownInterval = null;
		},
		settingDocumentMeta({ url, title, description, keywords }) {
			document.title = title;
			document.head
				.querySelector('meta[name="description"]')
				.setAttribute('content', description);
			document.head
				.querySelector('meta[name="twitter:title"]')
				.setAttribute('content', title);
			document.head
				.querySelector('meta[name="twitter:url"]')
				.setAttribute('content', url);
			document.head
				.querySelector('meta[name="twitter:description"]')
				.setAttribute('content', description);
			document.head
				.querySelector('meta[property="og:title"]')
				.setAttribute('content', title);
			document.head
				.querySelector('meta[property="og:url"]')
				.setAttribute('content', url);
			document.head
				.querySelector('meta[property="og:description"]')
				.setAttribute('content', description);
			document.head
				.querySelector('meta[name="keywords"]')
				.setAttribute('content', keywords);
		},
		openSearchBarFromLanding() {
			this.getIsWebview
				? window.Android.openSearch()
				: this.setSearchStatus(true);
		},
		openSearchBarFromList() {
			this.getIsWebview
				? window.Android.openSearchFromList()
				: this.setSearchStatus(true);
		},
		handleTimerByDocumentVisibility() {
			if (document.hidden) return;
			this.resetTimer();
			this.callCountdownTimer();
		}
	},

	computed: {
		showCondition() {
			return this.totalTime === 0 || this.getIsLoading;
		}
	},

	destroyed() {
		this.hasVisibilityEvent &&
			document.removeEventListener('visibilitychange', this.checkVisibility);
	}
};
