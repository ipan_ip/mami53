import { mapState } from 'vuex';

import _get from 'lodash/get';

import MixinAuthTrackers from './MixinAuthTrackers';

export default {
	mixins: [MixinAuthTrackers],
	data() {
		return {
			token: document.head.querySelector('meta[name="csrf-token"]').content,
			isLoginFailed: false
		};
	},
	computed: {
		...mapState(['userRole'])
	},
	methods: {
		/**
		 * Currently used specifically for Forget Password flow (only login without
		 * redirection).
		 * Will be updated to match the existing Login flow along with overall Auth
		 * features improvements.
		 */
		authenticateUser(payload) {
			payload.token = this.token;

			axios
				.post(`/auth/${this.userRole.key}/login`, payload)
				.then(response => {
					if (response.data.status) {
						this.trackLoginActivities();

						if (this.userRole.key === 'owner') {
							const accessToken = _get(
								response.data,
								'login.oauth.access_token',
								null
							);
							const refreshToken = _get(
								response.data,
								'login.oauth.refresh_token',
								null
							);
							const options = {
								samesite: 'strict',
								domain: oxOauth2Domain,
								'max-age': '604800' // 7 days
							};

							Cookies.set('access_token', accessToken, options);
							Cookies.set('refresh_token', refreshToken, options);
						}
					} else {
						this.swalError(
							null,
							'Terjadi galat pada proses login. Silakan coba lagi.'
						);
						this.isLoginFailed = true;
						this.trackLoginActivities(response.data.meta.message);
					}
				})
				.catch(error => {
					this.swalError(
						null,
						'Terjadi galat pada proses login. Silakan coba lagi.'
					);
					bugsnagClient.notify(error);
				});
		}
	}
};
