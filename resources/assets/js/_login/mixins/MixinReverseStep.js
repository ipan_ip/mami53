import { mapState } from 'vuex';

export default {
	computed: {
		...mapState({
			userRole: state => state.userRole,
			currentForgetPasswordStep: state => state.forgetPassword.currentStepKey
		}),
		isCurrentlyAtLastStep() {
			// To Do: add Login and Register last step keys
			return this.currentForgetPasswordStep === 'success-state';
		}
	},
	methods: {
		reverseStep() {
			this.isCurrentlyAtLastStep
				? this.returnToHomePage()
				: this.$router.go(-1);
		},
		returnToHomePage() {
			this.userRole.key === 'owner'
				? window.location.replace(oxWebUrl)
				: (window.location.href = '/');
		}
	}
};
