import { mapState } from 'vuex';

export default {
	computed: {
		...mapState(['userRole']),
		trackerTag() {
			return this.userRole.key === 'owner' ? '[Owner]' : '[User]';
		},
		interfacePayload() {
			return { interface: navigator.isMobile ? 'mobile' : 'desktop' };
		}
	},
	methods: {
		trackLoginActivities(errorMessage = null) {
			tracker('moe', [
				`${this.trackerTag} Login`,
				{
					login_result: !errorMessage,
					fail_reason: errorMessage
				}
			]);
		},
		trackForgetPasswordRequested() {
			tracker('moe', [
				`${this.trackerTag} Forget Password Requested`,
				{
					request_by: 'phone_number',
					...this.interfacePayload
				}
			]);
		},
		trackForgetPasswordOTPMethodSelectionVisited() {
			tracker('moe', [
				`${this.trackerTag} Forget Password - Choose OTP Method`,
				{
					...this.interfacePayload,
					request_from: 'forget password'
				}
			]);
		},
		trackOTPFormVisited(source, otpMethod) {
			tracker('moe', [
				`${this.trackerTag} Visit Input OTP Form`,
				{
					...this.interfacePayload,
					request_from: source,
					otp_method: otpMethod
				}
			]);
		},
		trackOTPResent(source, otpMethod) {
			tracker('moe', [
				`${this.trackerTag} Click Resend OTP`,
				{
					...this.interfacePayload,
					request_from: source,
					otp_method: otpMethod
				}
			]);
		}
	}
};
