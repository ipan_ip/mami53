import forgetPassword from './modules/ForgetPassword';

export default {
	modules: {
		forgetPassword
	},
	state: {
		token: document.head.querySelector('meta[name="csrf-token"]').content,
		authCheck: {},
		authData: {},
		captchaKey: '',
		profile: {
			username: '',
			phoneNumber: '',
			email: '',
			password: '',
			otpCode: '',
			captcha: ''
		},
		userRole: {
			key: '',
			label: ''
		},
		redirectPath: ''
	},
	mutations: {
		setProfileUsername(state, payload) {
			state.profile.username = payload;
		},
		setProfilePhoneNumber(state, payload) {
			state.profile.phoneNumber = payload;
		},
		setProfileEmail(state, payload) {
			state.profile.email = payload;
		},
		setProfilePassword(state, payload) {
			state.profile.password = payload;
		},
		setProfileOtpCode(state, payload) {
			state.profile.otpCode = payload;
		},
		setRegistrationData(state, payload) {
			state.profile.username = payload.username;
			state.profile.phoneNumber = payload.phoneNumber;
			state.profile.email = payload.email;
			state.profile.password = payload.password;
			state.profile.captcha = payload.captcha;
		},
		setCaptchaKey(state, payload) {
			state.captchaKey = payload;
		},
		setAuthCheck(state, payload) {
			state.authCheck = payload;
		},
		setAuthData(state, payload) {
			state.authData = payload;
		},
		setUserRole(state, payload) {
			state.userRole = payload;
		},
		setRedirectPath(state, payload) {
			state.redirectPath = payload;
		}
	}
};
