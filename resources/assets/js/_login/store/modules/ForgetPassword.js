export default {
	namespaced: true,
	state: {
		currentStepKey: 'input-id',
		selectedUserIdType: '',
		userEmailOrPhoneNumberValue: '',
		otpMethods: [],
		selectedOTPMethod: {
			key: null,
			label: ''
		},
		otpResendCounter: {
			sms: 0,
			whatsapp: 0,
			email: 0
		},
		inputedOTPCode: null,
		isForgetPasswordSuccess: false
	},
	mutations: {
		setCurrentStepKey(state, payload) {
			state.currentStepKey = payload;
		},
		setSelectedUserIdType(state, payload) {
			state.selectedUserIdType = payload;
		},
		setUserEmailOrPhoneNumberValue(state, payload) {
			state.userEmailOrPhoneNumberValue = payload;
		},
		setOTPMethods(state, payload) {
			state.otpMethods = payload;
		},
		setSelectedOTPMethod(state, payload) {
			state.selectedOTPMethod = payload;
		},
		setSMSOTPResendCounter(state, payload) {
			state.otpResendCounter.sms = payload;
		},
		setWhatsAppOTPResendCounter(state, payload) {
			state.otpResendCounter.whatsapp = payload;
		},
		setEmailOTPResendCounter(state, payload) {
			state.otpResendCounter.email = payload;
		},
		setInputedOTPCode(state, payload) {
			state.inputedOTPCode = payload;
		},
		setIsForgetPasswordSuccess(state, payload) {
			state.isForgetPasswordSuccess = payload;
		}
	},
	getters: {
		steps: (state, getters, rootState) => {
			const role = rootState.userRole;

			return [
				{
					id: 1,
					key: 'input-id',
					title: 'Lupa Password',
					description:
						'Masukkan nomor handphone yang terdaftar di Mamikos, dan lakukan verifikasi untuk membuat password baru.',
					component: 'ForgetPasswordIdInput'
				},
				{
					id: 2,
					key: 'select-otp-method',
					title: 'Pilih Metode Verifikasi',
					description: `Kami akan mengirimkan kode verifikasi sesuai dengan metode yang ${
						role.key === 'owner' ? 'Anda' : 'kamu'
					} pilih di bawah ini.`,
					component: 'ForgetPasswordOTPMethodSelection'
				},
				{
					id: 3,
					key: 'input-otp',
					title: `Verifikasi Akun ${role.label} Kos`,
					description:
						state.selectedUserIdType === 'email'
							? `Alamat email ini telah terdaftar sebagai akun ${role.label.toLowerCase()} kos di Mamikos.`
							: `Nomor handphone ini telah terdaftar sebagai akun ${role.label.toLowerCase()} kos di Mamikos.`,
					component: 'ForgetPasswordOTPInput'
				},
				{
					id: 4,
					key: 'input-new-password',
					title: 'Ubah Password',
					description:
						role.key === 'owner'
							? 'Password baru akan digunakan untuk akses ke akun Mamikos Anda.'
							: 'Password baru akan kamu gunakan untuk akses ke akun Mamikos.',
					component: 'ForgetPasswordNewPasswordInput'
				},
				{
					id: 5,
					key: 'success-state',
					title: 'Password Berhasil Diubah',
					description: '',
					component: 'ForgetPasswordSuccessState'
				}
			];
		},
		selectedOTPResendCounter: state => {
			return state.otpResendCounter[
				state.selectedOTPMethod.label.toLowerCase()
			];
		},
		currentStep: (state, getters) => {
			return (
				getters.steps.find(step => step.key === state.currentStepKey) ||
				getters.steps[0]
			);
		},
		otpResendCounterWarningMessage: state => otpMethod => {
			return `Mohon tunggu ${state.otpResendCounter[otpMethod]} detik lagi untuk kirim ulang kode verifikasi.`;
		}
	},
	actions: {
		startOTPResendCountdown({ state, commit }) {
			const otpMethod = state.selectedOTPMethod.label;

			commit(`set${otpMethod}OTPResendCounter`, 60);

			const counter = setInterval(() => {
				const currentCounter = state.otpResendCounter[otpMethod.toLowerCase()];

				commit(`set${otpMethod}OTPResendCounter`, currentCounter - 1);

				if (currentCounter === 0) {
					clearInterval(counter);
					commit(`set${otpMethod}OTPResendCounter`, 0);
				}
			}, 1000);
		}
	}
};
