import Vuex from 'vuex';
import VueRouter from 'vue-router';
import VeeValidate, { Validator } from 'vee-validate';
import VueLazyload from 'vue-lazyload';

import id from 'vee-validate/dist/locale/id';
import defaultStore from './store';
import routePath from './router/paths';

import App from './components/App.vue';

import mixinAuth from 'Js/@mixins/MixinAuth';

require('../starter');

window.Vue = require('vue').default;
window.md5 = require('md5');
window.debounce = require('lodash/debounce');
window.validator = new Validator();

Validator.localize('id', id);

Vue.use(Vuex);
Vue.use(VueRouter);
Vue.use(VueLazyload);
Vue.use(VeeValidate, {
	locale: 'id'
});

Vue.component('app', App);

Vue.config.productionTip = false;

/* eslint-disable no-unused-expressions */
import('Js/@mixins/MixinScrollTop');
import('Js/@mixins/MixinGetQueryString');
import('Js/@mixins/MixinNavigatorIsMobile');

const store = new Vuex.Store(defaultStore);

const router = new VueRouter({
	mode: 'history',
	base: '/',
	routes: routePath
});

router.beforeEach((to, from, next) => {
	const { interface: webviewInterface } = from.query;
	const { interface: webviewInterfaceNext } = to.query;
	const webviewInterfaceTypes = ['ios', 'android'];

	if (
		webviewInterfaceTypes.includes(webviewInterface) &&
		!webviewInterfaceTypes.includes(webviewInterfaceNext)
	) {
		next({
			path: to.path,
			query: { ...to.query, interface: webviewInterface }
		});
	} else {
		next();
	}
});

/* eslint-disable no-unused-vars */
const app = new Vue({
	el: '#app',
	mixins: [mixinAuth],
	store,
	router,
	created() {
		Cookies.set('samesite', true, { sameSite: 'strict' });
	},
	mounted() {
		/* eslint-disable no-undef */
		this.$store.commit('setCaptchaKey', captchaKey);
	}
});
