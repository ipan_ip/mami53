const ContainerLogin = () => import('Js/_login/components/ContainerLogin.vue');
const ContainerRegister = () =>
	import('Js/_login/components/ContainerRegister.vue');
const ForgetPassword = () => import('Js/_login/components/ForgetPassword.vue');

const routePath = [
	{
		path: '/login',
		name: 'ContainerLogin',
		component: ContainerLogin,
		props: { user: 'select' },
		meta: { title: 'Login - Mamikos' }
	},

	{
		path: '/login-pemilik',
		name: 'ContainerLogin',
		component: ContainerLogin,
		props: { user: 'owner' },
		meta: { title: 'Login Pemilik Iklan - Mamikos' }
	},
	{
		path: '/register-pemilik',
		name: 'ContainerRegister',
		component: ContainerRegister,
		props: { user: 'owner' },
		meta: { title: 'Registrasi Pemilik Iklan - Mamikos' }
	},
	{
		path: '/lupa-password-pemilik',
		name: 'forget-password-owner',
		component: ForgetPassword,
		meta: { title: 'Lupa Password Pemilik - Mamikos' }
	},

	{
		path: '/login-pencari',
		name: 'ContainerLogin',
		component: ContainerLogin,
		props: { user: 'tenant' },
		meta: { title: 'Login Pencari Kost - Mamikos' }
	},
	{
		path: '/register-pencari',
		name: 'ContainerLogin',
		component: ContainerRegister,
		props: { user: 'tenant' },
		meta: { title: 'Registrasi Pencari Kost - Mamikos' }
	},

	{
		path: '/lupa-password-pencari',
		name: 'forget-password-tenant',
		component: ForgetPassword,
		meta: { title: 'Lupa Password Pencari - Mamikos' }
	}
];

export default routePath;
