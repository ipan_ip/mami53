require('Js/starter');

window.Vue = require('vue').default;

import VueRouter from 'vue-router';
Vue.use(VueRouter);

import Vuex from 'vuex';
Vue.use(Vuex);

import mixinAuth from 'Js/@mixins/MixinAuth';

import infiniteScroll from 'vue-infinite-scroll';
Vue.use(infiniteScroll);

require('Js/@mixins/MixinCenterModal');
require('Js/@mixins/MixinScrollTop');
require('Js/@mixins/MixinSwalLoading');
require('Js/@mixins/MixinSwalSuccessError');

const ContainerNewestList = require('./components/ContainerNewestList.vue')
	.default;

// Container Component
Vue.component('container-newest-list', ContainerNewestList);

// Global Components
Vue.component('breadcrumb-trails', () =>
	import('Js/@components/BreadcrumbTrails')
);
Vue.component('mami-loading', import('Js/@components/MamiLoading'));

Vue.component('mami-loading-inline', () =>
	import('Js/@components/MamiLoadingInline')
);
Vue.component('room-list', () => import('Js/@components/RoomList'));

// Local Components
Vue.component(
	'empty-room',
	require('./components/NewestListViewEmpty.vue').default
);
Vue.component(
	'section-title',
	require('./components/SectionTitle.vue').default
);

// Router
const newestListPagination = require('./components/NewestListPagination.vue')
	.default;
const newestListAreaPagination = require('./components/NewestListAreaPagination.vue')
	.default;

const routes = [
	{ path: '/terbaru', component: newestListPagination },
	{ path: '/terbaru/:place/', component: newestListAreaPagination }
];

const router = new VueRouter({
	mode: 'history',
	routes
});

const store = new Vuex.Store({
	state: {
		authCheck: {},
		authData: {}
	},
	mutations: {
		setAuthCheck(state, payload) {
			state.authCheck = payload;
		},
		setAuthData(state, payload) {
			state.authData = payload;
		}
	}
});

Vue.config.productionTip = false;

const app = new Vue({
	el: '#app',
	mixins: [mixinAuth],
	router,
	store
});
