import _get from 'lodash/get';

const mixinBookingRequestDate = {
	watch: {
		checkinDate(checkinDate) {
			this.checkinDateCookies = checkinDate;
		}
	},
	computed: {
		maxCheckinDateMonthRange() {
			return _get(this.$store, 'state.detail.max_month_checkin') || 2;
		},
		checkinDate: {
			get() {
				return _get(this.$store, 'state.params.checkin', '');
			},
			set(checkinDate) {
				this.$store.commit(
					'setCheckin',
					this.formatDate(checkinDate, 'YYYY-MM-DD')
				);
			}
		},
		checkinDateConstructor: {
			get() {
				return this.checkIsValidDate(this.checkinDate)
					? this.$dayjs(this.checkinDate).toDate()
					: null;
			},
			set(date) {
				this.checkinDate = date;
			}
		},
		disabledDates() {
			return {
				to: this.$dayjs().subtract(1, 'day').$d,
				from: this.$dayjs().add(this.maxCheckinDateMonthRange || 2, 'month').$d
			};
		},
		checkinDateCookies: {
			get() {
				const checkinDate = Cookies.get('mamiBookingCheckin');
				return this.checkIsValidDate(checkinDate)
					? new Date(checkinDate)
					: null;
			},
			set(checkinDate) {
				if (this.checkIsValidDate(checkinDate)) {
					Cookies.set('mamiBookingCheckin', new Date(checkinDate), {
						expires: 1
					});
				} else {
					Cookies.remove('mamiBookingCheckin');
				}
			}
		},
		authData() {
			return this.$store.state.authData;
		},
		detailKos() {
			return this.$store.state.detail;
		},
		bookingParams() {
			return this.$store.state.params;
		}
	},
	methods: {
		checkIsValidDate(date) {
			return !!date && this.$dayjs(date).isValid();
		},
		formatDate(val, format) {
			if (this.checkIsValidDate(val)) {
				return this.$dayjs(val)
					.locale('id')
					.format(format);
			}
			return '';
		},
		isInRangeDate(date) {
			const isAfterRange = this.$dayjs(date).isAfter(this.disabledDates.from);
			const isBeforeRange = this.$dayjs(date).isBefore(this.disabledDates.to);
			return !isAfterRange && !isBeforeRange;
		},
		trackViewedCalendar() {
			tracker('moe', [
				'[User] Booking - Calendar Viewed',
				{
					interface: this.isMobile ? 'mobile' : 'desktop',
					user_id: _get(this.authData, 'all.id', null),
					property_type: _get(this.detailKos, 'type', null),
					property_name: _get(this.detailKos, 'room_title', null),
					property_updated_at: _get(this.detailKos, 'updated_at', null),
					tenant_gender:
						_get(this.bookingParams, 'contact_gender') === 'female'
							? 'Perempuan'
							: 'Laki-Laki',
					property_id: _get(this.detailKos, 'seq', null),
					property_area_city: _get(this.detailKos, 'area_city', null),
					property_area_subdistrict: _get(
						this.detailKos,
						'area_subdistrict',
						null
					),
					goldplus_status: _get(this.detailKos, 'goldplus_status', null),
					redirection_source:
						this.$router.currentRoute.path === '/date'
							? 'booking_form'
							: 'property_detail',
					tenant_name: _get(this.authData, 'all.name', null),
					is_mamirooms: _get(this.detailKos, 'is_mamirooms', false)
				}
			]);
		}
	}
};

export default mixinBookingRequestDate;
