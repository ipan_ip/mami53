import dayjs from 'dayjs';
import isBetween from 'dayjs/plugin/isBetween';
import customParseFormat from 'dayjs/plugin/customParseFormat';
import utc from 'dayjs/plugin/utc';
import _get from 'lodash/get';
import isToday from 'dayjs/plugin/isToday';

dayjs.extend(isBetween);
dayjs.extend(customParseFormat);
dayjs.extend(utc);
dayjs.extend(isToday);

const mixinBookingAfterNightValidation = {
	data() {
		return {
			nightValidation: false
		};
	},
	computed: {
		authData() {
			return this.$store.state.authData;
		},
		detailKos() {
			return this.$store.state.detail;
		},
		bookingParams() {
			return this.$store.state.params;
		},
		bookingCheckinDate() {
			return this.$store.state.params.checkin;
		},
		isMatchedToday() {
			const today = dayjs(new Date()).format('YYYY-MM-DD');
			return today === this.bookingCheckinDate;
		},
		isAvailableBookingTime() {
			return dayjs()
				.utcOffset(7)
				.isBetween(
					dayjs(
						this.detailKos.available_booking_time_start,
						'HH:mm:ss'
					).utcOffset(7, true),
					dayjs(
						this.detailKos.available_booking_time_end,
						'HH:mm:ss'
					).utcOffset(7, true)
				);
		}
	},
	watch: {
		isMatchedToday: {
			immediate: true,
			handler(value) {
				if (value) {
					this.handleNightValidation();
				} else {
					this.nightValidation = false;
				}
			}
		}
	},
	methods: {
		handleNightValidation() {
			const availableBookingTime = !(
				this.detailKos.booking_time_restriction_active &&
				!this.isAvailableBookingTime
			);

			this.nightValidation = !!(
				!availableBookingTime && !!this.bookingCheckinDate
			);
		},
		formatTime(time) {
			return dayjs(time, 'HH:mm:ss').format('H.mm');
		},
		handleValidationMessage(withRules) {
			const availableBookingTimeStart = this.formatTime(
				this.detailKos.available_booking_time_start
			);
			const availableBookingTimeEnd = this.formatTime(
				this.detailKos.available_booking_time_end
			);

			const nightTime = dayjs()
				.utcOffset(7)
				.isAfter(
					dayjs(
						this.detailKos.available_booking_time_end,
						'HH:mm:ss'
					).utcOffset(7, true)
				);

			const dawnTime = dayjs()
				.utcOffset(7)
				.isBefore(
					dayjs(
						this.detailKos.available_booking_time_start,
						'HH:mm:ss'
					).utcOffset(7, true)
				);

			let warningMessage;

			if (withRules) {
				if (nightTime) {
					warningMessage = `Maaf, sudah lewat pukul ${availableBookingTimeEnd} WIB. Kamu baru bisa mulai masuk kos paling cepat besok.`;
				} else if (dawnTime) {
					warningMessage = `Jika pilih tanggal masuk kos hari ini, kamu baru bisa mengajukan sewa mulai pukul ${availableBookingTimeStart} WIB`;
				}
			} else {
				warningMessage = `Tanggal masuk kos hari ini hanya bisa diajukan/di-booking pukul ${availableBookingTimeStart} - ${availableBookingTimeEnd} WIB.`;
			}

			return warningMessage;
		},
		trackTimeWarningExecuted() {
			tracker('moe', [
				'[User] Booking - Time Warning Executed',
				{
					interface: this.isMobile ? 'mobile' : 'desktop',
					user_id: _get(this.authData, 'all.id', null),
					property_type: _get(this.detailKos, 'type', null),
					property_name: _get(this.detailKos, 'room_title', null),
					property_updated_at: _get(this.detailKos, 'updated_at', null),
					tenant_gender:
						_get(this.bookingParams, 'contact_gender') === 'female'
							? 'Perempuan'
							: 'Laki-Laki',
					property_id: _get(this.detailKos, 'seq', null),
					property_area_city: _get(this.detailKos, 'area_city', null),
					property_area_subdistrict: _get(
						this.detailKos,
						'area_subdistrict',
						null
					),
					goldplus_status: _get(this.detailKos, 'goldplus_status', null),
					redirection_source:
						this.$router.currentRoute.path === '/date'
							? 'booking_form'
							: 'property_detail',
					tenant_name: _get(this.authData, 'all.name', null),
					is_mamirooms: _get(this.detailKos, 'is_mamirooms', false)
				}
			]);
		}
	}
};

export default mixinBookingAfterNightValidation;
