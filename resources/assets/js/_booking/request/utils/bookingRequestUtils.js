import _get from 'lodash/get';
import dayjs from 'dayjs';
import 'dayjs/locale/id';
import * as storage from 'Js/@utils/storage';

export const rentCountDictionary = {
	daily: {
		amount: 1,
		unit: 'day',
		label: 'Hari'
	},
	weekly: {
		amount: 7,
		unit: 'day',
		label: 'Minggu',
		independentLabel: true
	},
	monthly: {
		amount: 1,
		unit: 'month',
		label: 'Bulan'
	},
	quarterly: {
		amount: 3,
		unit: 'month',
		label: 'Bulan'
	},
	semiannually: {
		amount: 6,
		unit: 'month',
		label: 'Bulan'
	},
	yearly: {
		amount: 1,
		unit: 'year',
		label: 'Tahun'
	}
};

export const checkIsValidDate = date => {
	return dayjs(date).isValid();
};

export const formatDate = (date, format) => {
	if (checkIsValidDate(date)) {
		return dayjs(date)
			.locale('id')
			.format(format);
	}
	return '';
};

export const getDurationLabel = (duration, type) => {
	if (duration && _get(rentCountDictionary, type, null)) {
		const { amount, independentLabel, label } = rentCountDictionary[type];
		const absoluteDuration = independentLabel ? 1 : amount;
		return `${duration * absoluteDuration} ${label}`;
	}
	return '';
};

export const getEstimateCheckout = (checkin, duration, rentCountType) => {
	if (checkin && duration && _get(rentCountDictionary, rentCountType, null)) {
		const { amount, unit } = rentCountDictionary[rentCountType];
		const relativeUnitDuration = amount * duration;
		const checkinDate = new Date(checkin);
		let estimateCheckout = null;
		if (unit === 'month') {
			estimateCheckout = new Date(
				checkinDate.setMonth(checkinDate.getMonth() + relativeUnitDuration)
			);
		} else if (unit === 'day') {
			estimateCheckout = new Date(
				checkinDate.setDate(checkinDate.getDate() + relativeUnitDuration)
			);
		} else if (unit === 'year') {
			estimateCheckout = new Date(
				checkinDate.setFullYear(
					checkinDate.getFullYear() + relativeUnitDuration
				)
			);
		}
		return estimateCheckout;
	}
	return null;
};

export const getMoeSessionId = () => {
	if (storage.local.getItem('MOE_DATA')) {
		const moeData = JSON.parse(storage.local.getItem('MOE_DATA'));
		return _get(moeData, 'SESSION.sessionKey', null);
	}
	return null;
};
