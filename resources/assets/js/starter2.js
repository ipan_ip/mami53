Object.assign = require('object-assign');
require('es6-promise/auto');

window.$ = window.jQuery = require('jquery');

require('bootstrap-sass');

window.swal = require('sweetalert2/dist/sweetalert2.js');

window.axios = require('axios');
window.axios.defaults.headers.common = {
	'X-Requested-With': 'XMLHttpRequest'
};
window.axios.defaults.baseURL = '/garuda';
window.axios.defaults.headers = {
	'Content-Type': 'application/json',
	'X-GIT-Time': '1406090202',
	Authorization: 'GIT WEB:WEB'
};
