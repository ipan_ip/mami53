/* eslint-disable no-unused-vars */
import 'Js/@mixins/MixinNavigatorIsMobile';
import 'Js/@mixins/MixinSwalLoading';
import 'Js/@mixins/MixinSwalSuccessError';
import Vuex from 'vuex';
import mixinAuth from 'Js/@mixins/MixinAuth';
import StoreData from './store';
import App from './components/App.vue';

const store = new Vuex.Store(StoreData);

Vue.component('app', App);

const app = new Vue({
	el: '#app',
	mixins: [mixinAuth],
	data() {
		return {
			state: {
				token: document.head.querySelector('meta[name="csrf-token"]').content,
				authCheck: {},
				authData: {}
			}
		};
	},
	store,
	mounted() {
		try {
			document.getElementById('preloading').remove();
		} catch (e) {
			document.getElementById('preloading').style.display = 'none';
		}
	}
});
