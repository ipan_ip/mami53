import { mapGetters, mapActions } from 'vuex';
import { genderOptions, sortingOptions } from 'Js/@utils/kostFilter';
import MixinIsMobile from './MixinIsMobile';

export default {
	mixins: [MixinIsMobile],
	computed: {
		...mapGetters([
			'getKostFilters',
			'getRentTypeList',
			'getKostSorting',
			'getKostLocationFilter'
		]),
		interface() {
			return this.isMobile ? 'mobile' : 'desktop';
		},
		areaData() {
			let areaText = 'Semua Area';

			if (window.child) {
				areaText = window.child.area_name;
			}

			const areaData = [
				{
					area_name: 'Semua Area',
					slug: ''
				},
				...window.parent.children
			];

			return {
				title: areaText,
				icon: 'search-icon',
				data: areaData,
				active: true
			};
		},
		genderData() {
			const genderFilter = this.getKostFilters.gender;
			let genderText = 'Semua Tipe Kos';

			if (genderFilter.length && genderFilter.length < 3) {
				genderText = genderOptions
					.filter(gender => ~genderFilter.indexOf(gender.value))
					.map(gender => gender.name)
					.join(', ');
			}

			return {
				title: genderText,
				icon: 'gender-icon',
				data: genderOptions,
				getValue: genderFilter,
				active: Boolean(genderFilter.length)
			};
		},
		rentData() {
			const rentFilter = this.getKostFilters.rent_type;
			const rentData = this.getRentTypeList.find(
				rent => +rent.value === +rentFilter
			);
			const rentText = rentData ? rentData.name : 'Bulanan';

			return {
				title: rentText,
				data: this.getRentTypeList,
				getValue: rentFilter,
				active: true
			};
		},
		priceData() {
			const isDefaultPrice =
				+this.getKostFilters.price_range[0] === 1000 &&
				+this.getKostFilters.price_range[20000000];
			const minPriceText = this.generatePriceText(
				this.getKostFilters.price_range[0]
			);
			const maxPriceText = this.generatePriceText(
				this.getKostFilters.price_range[1]
			);
			const title = isDefaultPrice
				? 'Harga'
				: `${minPriceText} - ${maxPriceText}`;

			return {
				title,
				active: !isDefaultPrice
			};
		},
		sortData() {
			const sortText = sortingOptions.find(
				sort => sort.value.join() === this.getKostSorting.join()
			);

			return {
				title: sortText ? sortText.name : 'Paling direkomendasikan',
				data: sortingOptions,
				icon: 'sorting-icon',
				getValue: this.getKostSorting,
				active: true
			};
		}
	},
	methods: {
		...mapActions([
			'settingFilter',
			'fetchRooms',
			'settingSort',
			'settingLocationFilter'
		]),
		setMainFilter(value, name) {
			const payload = {
				[name]: value
			};
			this.settingFilter(payload);
		},
		setFilterPrice(priceRange) {
			const payload = { price_range: priceRange };
			this.settingFilter(payload);
		},
		generatePriceText(price) {
			const priceLength = price.toString().length;
			const priceCurrency = price
				.toString()
				.replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
			const priceSplit = priceCurrency.split(',');
			const firstPrice = priceSplit[0];
			const secondPrice =
				firstPrice.length < 3
					? priceSplit[1].substring(0, 2)
					: priceSplit[1].substring(0, 1);

			return priceLength > 6
				? `Rp ${firstPrice},${secondPrice} juta`
				: `Rp ${firstPrice} ribu`;
		},
		scrollToTop() {
			setTimeout(() => {
				window.scrollTo({ top: 0, behavior: 'smooth' });
			}, 50);
		}
	}
};
