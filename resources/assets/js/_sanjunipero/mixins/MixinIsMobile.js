export default {
	computed: {
		isMobile() {
			return window.innerWidth <= 768;
		}
	}
};
