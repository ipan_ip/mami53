import { makeAPICall } from 'Js/@utils/makeAPICall.js';

export default {
	state: {
		token: document.head.querySelector('meta[name="csrf-token"]').textContent,
		authCheck: {},
		authData: {},
		mainData: {
			kos: {
				params: {
					filters: {
						gender: [],
						price_range: [10000, 20000000],
						tags_ids: [],
						level_info: [],
						rent_type: 2,
						property_type: 'all',
						random_seeds: Math.floor(Math.random() * (1000 - 0 + 1)) + 0,
						booking: 0,
						flash_sale: false
					},
					sorting: {
						field: 'price',
						direction: '-'
					},
					location: [],
					point: {},
					include_promoted: false,
					limit: 20,
					offset: 0,
					slug: window.location.pathname
				},
				kosList: [],
				clusterList: [],
				isEmptyList: false,
				totalRooms: 0,
				responseData: null,
				hasMore: false,
				tagRooms: [],
				tagShare: [],
				tagRules: [],
				rentTypeList: []
			}
		},
		isMoreLoading: false,
		isLoading: true,
		isFacLoading: true,
		activeType: 'kos'
	},
	getters: {
		getAllParams: state => {
			return state.mainData.kos.params;
		},
		getRooms: state => {
			return state.mainData.kos.kosList;
		},
		getKostFilters: state => {
			return state.mainData.kos.params.filters;
		},
		getKostLocationFilter: state => {
			return state.mainData.kos.params.location;
		},
		getKostSorting: state => {
			return [
				state.mainData.kos.params.sorting.field,
				state.mainData.kos.params.sorting.direction
			];
		},
		getTotalRooms: state => {
			return state.kos.totalRooms;
		},
		getRentTypeList: state => {
			return state.mainData.kos.rentTypeList;
		},
		getFacilitiesExist: state => {
			return (
				state.mainData.kos.tagRooms.length && state.mainData.kos.tagShare.length
			);
		},
		getTagRules: state => {
			return state.mainData.kost.tagRules;
		}
	},
	mutations: {
		setAuthCheck(state, payload) {
			state.authCheck = payload;
		},
		setAuthData(state, payload) {
			state.authData = payload;
		},
		setLoading(state, payload) {
			state.isLoading = payload;
		},
		setFacLoading(state, payload) {
			state.isFacLoading = payload;
		},
		setMoreLoading(state, payload) {
			state.isMoreLoading = payload;
		},
		setEmptyList(state, payload) {
			state.mainData[state.activeType].isEmptyList = payload;
		},
		setHasMore(state, payload) {
			state.mainData[state.activeType].hasMore = payload;
		},
		setOffset(state, payload) {
			state.mainData[state.activeType].params.offset = payload;
		},
		setSort(state, payload) {
			state.mainData[state.activeType].params.sorting.field = payload[0];
			state.mainData[state.activeType].params.sorting.direction = payload[1];
		},
		setFilter(state, payload) {
			state.mainData[state.activeType].params.filters = {
				...state.mainData[state.activeType].params.filters,
				...payload
			};
		},
		setLocationFilter(state, payload) {
			state.mainData[state.activeType].params.location = payload;
		},
		setRentTypeList(state, payload) {
			const rentList = payload.map(rent => {
				return {
					...rent,
					value: rent.id,
					name: rent.rent_name
				};
			});
			state.mainData.kos.rentTypeList = rentList;
		},
		setRooms(state, payload) {
			if (payload.status) {
				if (payload.isRequestMore) {
					state.mainData[state.activeType][`${state.activeType}List`] = [
						...state.mainData[state.activeType][`${state.activeType}List`],
						...payload.rooms
					];
				} else {
					state.mainData[state.activeType][`${state.activeType}List`] =
						payload.rooms;
				}
				state.mainData[state.activeType].totalRooms = payload.total;
			} else {
				state.mainData[state.activeType][`${state.activeType}List`] = [];
				state.mainData[state.activeType].totalRooms = 0;
			}
		},
		setResponseData(state, payload) {
			state.mainData[state.activeType].responseData = payload;
		}
	},
	actions: {
		settingInitialLocationFilter({ commit, dispatch }) {
			let locationValue;
			if (window.child) {
				locationValue = [
					[window.child.longitude_1, window.child.latitude_1],
					[window.child.longitude_2, window.child.latitude_2]
				];
			} else {
				locationValue = [];
			}
			commit('setLocationFilter', locationValue);
		},
		settingFilter({ commit, dispatch }, payload) {
			commit('setFilter', payload);
		},
		settingSort({ commit, dispatch }, payload) {
			commit('setSort', payload);
			dispatch('fetchRooms');
		},
		async fetchFacilitiesList({ commit }) {
			commit('setFacLoading', true);
			try {
				const response = await makeAPICall({
					method: 'get',
					url: '/stories/filters'
				});
				if (response.status) {
					commit('setRentTypeList', response.rent_type);
				}
			} catch (error) {
				bugsnagClient.notify(error);
			} finally {
				commit('setFacLoading', false);
			}
		},
		async fetchRooms({ commit, state, dispatch }, loadingMore = false) {
			commit('setLoading', true);

			const params = state.mainData[state.activeType].params;
			const filters = params.filters;
			const sorting = params.sorting;
			const genderFilter = {
				gender: filters.gender.length ? filters.gender : [0, 1, 2]
			};

			const parameter = {
				gender: genderFilter.gender.toString(),
				price_range: filters.price_range.toString(),
				tags_ids: filters.tags_ids.toString(),
				level_info: filters.level_info.toString(),
				rent_type: filters.rent_type,
				property_type: filters.property_type,
				random_seeds: filters.random_seeds,
				booking: filters.booking,
				flash_sale: filters.flash_sale,
				mamirooms: filters.mamirooms,
				include_promoted: filters.include_promoted,
				mamichecker: filters.mamichecker,
				virtual_tour: filters.virtual_tour,
				sorting_field: sorting.field,
				sorting_direction: sorting.direction,
				location: params.location.toString(),
				limit: params.limit,
				offset: params.offset,
				slug: params.slug
			};

			const response = await makeAPICall({
				method: 'get',
				url: '/sanjunipero/list',
				params: parameter
			});

			if (response) {
				const rooms = response.rooms || [];

				if (rooms.length > 0) {
					const payload = {
						status: true,
						rooms,
						total: response.total,
						isRequestMore: loadingMore
					};
					commit('setEmptyList', false);
					commit('setRooms', payload);
				} else {
					commit('setEmptyList', true);
					commit('setRooms', {
						status: false
					});
				}
				commit('setHasMore', response['has-more']);
				commit('setResponseData', response);
				commit('setLoading', false);
			} else {
				commit('setEmptyList', true);
				commit('setRooms', { status: false });
				commit('setHasMore', false);
				commit('setLoading', false);
			}
		}
	}
};
