require('Js/starter');

window.Vue = require('vue').default;

import VueRouter from 'vue-router';
Vue.use(VueRouter);

import Vuex from 'vuex';
Vue.use(Vuex);

import mixinAuth from 'Js/@mixins/MixinAuth';
import VueLazyload from 'vue-lazyload';
Vue.use(VueLazyload);

require('Js/@mixins/MixinNavigatorIsMobile');
require('Js/@mixins/MixinCenterModal');
require('Js/@mixins/MixinScrollTop');
require('Js/@mixins/MixinGetQueryString');
require('Js/@mixins/MixinSwalLoading');
require('Js/@mixins/MixinSwalSuccessError');

const ContainerUserPromo = require('./components/user-promo/ContainerUserPromo.vue')
	.default;

// Container Component
Vue.component('container-user-promo', ContainerUserPromo);

// Global Components
Vue.component('breadcrumb-trails', () =>
	import('Js/@components/BreadcrumbTrails')
);
Vue.component('mami-loading', () => import('Js/@components/MamiLoading'));
Vue.component('mami-loading-inline', () =>
	import('Js/@components/MamiLoadingInline')
);
Vue.component('room-list', () => import('Js/@components/RoomList'));

// Local Components
Vue.component(
	'section-title',
	require('./components/user-profile/SectionTitle.vue').default
);
Vue.component(
	'promo-event',
	require('./components/user-promo/PromoEvent.vue').default
);

const store = new Vuex.Store({
	state: {
		authCheck: {},
		authData: {}
	},
	mutations: {
		setAuthCheck(state, payload) {
			state.authCheck = payload;
		},
		setAuthData(state, payload) {
			state.authData = payload;
		}
	}
});

Vue.config.productionTip = false;

const app = new Vue({
	el: '#app',
	mixins: [mixinAuth],
	store
});
