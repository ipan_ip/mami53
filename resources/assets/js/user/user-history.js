require('Js/starter');

window.Vue = require('vue').default;

import VueRouter from 'vue-router';
Vue.use(VueRouter);

import Vuex from 'vuex';
Vue.use(Vuex);

import mixinAuth from 'Js/@mixins/MixinAuth';
import VueLazyload from 'vue-lazyload';

Vue.use(VueLazyload);

require('Js/@mixins/MixinCenterModal');
require('Js/@mixins/MixinNavigatorIsMobile');
require('Js/@mixins/MixinScrollTop');
require('Js/@mixins/MixinGetQueryString');
require('../@mixins/MixinSwalLoading');

const ContainerUserHistory = require('./components/user-history/ContainerUserHistory.vue')
	.default;

// Container Component
Vue.component('container-user-history', ContainerUserHistory);

// Global Components

Vue.component('breadcrumb-trails', () =>
	import('Js/@components/BreadcrumbTrails')
);

Vue.component('room-list', () => import('Js/@components/RoomList'));

Vue.component('mami-loading-inline', () =>
	import('Js/@components/MamiLoadingInline')
);

// Local Components
Vue.component(
	'history-tabs',
	require('./components/user-history/HistoryTabs.vue').default
);
Vue.component(
	'history-kost-list-empty',
	require('./components/user-history/HistoryKostListEmpty.vue').default
);

const historyView = require('./components/user-history/HistoryView.vue')
	.default;

const routes = [
	{ path: '/', redirect: '/view' },
	{
		path: '/view',
		component: historyView,
		props: { stat: 'view' }
	},
	{
		path: '/favourite',
		component: historyView,
		props: { stat: 'love' }
	}
];

const router = new VueRouter({
	mode: 'history',
	base: '/history',
	routes
});

const store = new Vuex.Store({
	state: {
		token: document.head.querySelector('meta[name="csrf-token"]').content,
		authCheck: {},
		authData: {},
		section: 'viewed'
	},
	mutations: {
		setAuthCheck(state, payload) {
			state.authCheck = payload;
		},
		setAuthData(state, payload) {
			state.authData = payload;
		}
	}
});

Vue.config.productionTip = false;

const app = new Vue({
	el: '#app',
	router,
	mixins: [mixinAuth],
	store
});
