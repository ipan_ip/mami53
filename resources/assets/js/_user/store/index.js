const defaultStore = {
	state: {
		token: document.head.querySelector('meta[name="csrf-token"]').content,
		authCheck: {},
		authData: {},
		criteria: {},
		userProfile: {},
		userEditProfile: {},
		allCity: [],
		statePhoneVerification: false,
		timerPhoneVerification: '',
		profileBreadcrumb: [],
		userMenuActive: 'user-menu',
		userPhoto: '',
		formState: false,
		userBalance: 'xxxxx',
		virtualAccount: [],
		userHistory: '',
		bankList: '',
		userKost: {},
		statusRent: {},
		maxDateTenantCheckout: '',
		extras: {
			idImg: null,
			selfieImg: null,
			cardIdVal: 'e_ktp',
			currentId: 'e_ktp'
		},
		voucherCount: 0,
		isVoucherDetail: false,
		isUserLoggedInViaSocial: false,
		bookingNotification: {
			draft: 0,
			lastSeen: 0
		},
		historyKostDetail: {},
		historyDetailCheckin: '',
		historyDetailChecout: '',
		roomDetail: {},
		reviewData: {},
		successStopRent: false,
		totalPoin: [],
		isBlacklistedOnHistory: false,
		redirectionSource: '[User] MamiPoin Page'
	},
	actions: {
		getSaldo({ commit }) {
			axios
				.get('/user/saldo')
				.then(response => {
					if (response.data.status) {
						commit('setUserBalance', response.data.data.amount);
						commit('setUserVirtualAccount', response.data.data.va);
					}
				})
				.catch(error => {
					bugsnagClient.notify(error);
				});
		},
		getHistory({ commit }) {
			axios
				.get('/user/transaction/history')
				.then(response => {
					if (response.data.status) {
						commit('setUserHistory', response.data.data);
					}
				})
				.catch(error => {
					alert(error);
					bugsnagClient.notify(error);
				});
		},
		getBankList({ commit, state }) {
			axios
				.get('/user/bank')
				.then(response => {
					if (response.data.status) {
						commit('setBankList', response.data.data);
					}
				})
				.catch(error => {
					alert(error);
					bugsnagClient.notify(error);
				});
		},
		getVoucherCount({ commit, state }) {
			axios.get('/user/voucher/count').then(response => {
				if (response.data.status) {
					commit('setVoucherCount', response.data.count);
				}
			});
		},
		getTotalPoin({ commit }) {
			axios
				.get('/user/point/total')
				.then(response => {
					if (response.data.status) {
						commit('setTotalPoin', response.data);
					}
				})
				.catch(error => {
					alert(error);
					bugsnagClient.notify(error);
				});
		}
	},
	mutations: {
		setVoucherCount(state, payload) {
			state.voucherCount = payload;
		},
		setAuthCheck(state, payload) {
			state.authCheck = payload;
		},
		setAuthData(state, payload) {
			state.authData = payload;
		},
		setProfileUser(state, payload) {
			state.userProfile = payload;
		},
		setuserEditProfile(state, payload) {
			state.userEditProfile = payload;
		},
		setValue(state, payload) {
			for (const prop in payload) {
				state.userEditProfile[prop] = payload[prop];
			}
		},
		setAllCity(state, payload) {
			state.allCity = payload;
		},
		setStatePhoneVerification(state, payload) {
			state.statePhoneVerification = payload;
		},
		setPhotoMedium(state, payload) {
			state.userProfile.photo.medium =
				'/general/img/pictures/default-user-image.png';
		},
		setPhotoUser(state, payload) {
			state.userPhoto = payload;
		},
		setMenu(state, payload) {
			state.userMenuActive = payload;
		},
		setDetailVoucher(state, payload) {
			state.isVoucherDetail = payload;
		},
		setUserBalance(state, payload) {
			state.userBalance = payload;
		},
		setUserVirtualAccount(state, payload) {
			state.virtualAccount = payload;
		},
		setUserHistory(state, payload) {
			state.userHistory = payload;
		},
		setBankList(state, payload) {
			state.bankList = payload;
		},
		setEmail(state, payload) {
			state.userProfile.email = payload;
		},
		setBreadcrumb(state, payload) {
			if (payload === 'user') {
				state.profileBreadcrumb = [
					{ name: 'Home', url: '/' },
					{ name: 'User', url: '/user' }
				];
			} else if (payload === 'mamipay') {
				state.profileBreadcrumb = [
					{ name: 'Home', url: '/' },
					{ name: 'User', url: '/user' },
					{ name: 'Mamipay', url: '/isi-saldo' }
				];
			}
		},
		setFormState(state, payload) {
			state.formState = payload;
		},
		setIsUserLoggedInViaSocial(state, payload) {
			state.isUserLoggedInViaSocial = payload;
		},
		updateName(state, payload) {
			state.userEditProfile.name = payload;
		},
		updateBirthday(state, payload) {
			state.userEditProfile.birthday = payload;
		},
		updateGender(state, payload) {
			state.userEditProfile.gender = payload;
		},
		updateCity(state, payload) {
			state.userEditProfile.city = payload;
		},
		updateMartial(state, payload) {
			state.userEditProfile.marital_status = payload;
		},
		updateEducation(state, payload) {
			state.userEditProfile.last_education = payload;
			state.userEditProfile.education = payload;
		},
		updateEmergency(state, payload) {
			state.userEditProfile.phone_number_additional = payload;
		},
		updatePosition(state, payload) {
			state.userEditProfile.work = payload;
			state.userEditProfile.position = payload;
		},
		updateWorkPlace(state, payload) {
			state.userEditProfile.work_place = payload;
		},
		updateDescription(state, payload) {
			state.userEditProfile.description = payload;
		},
		updateSemester(state, payload) {
			state.userEditProfile.semester = payload;
		},
		setJobs(state, payload) {
			state.userEditProfile.jobs = payload;
		},
		setUserKost(state, payload) {
			state.userKost = payload;
		},
		setStatusRent(state, payload) {
			state.statusRent = payload;
		},
		setMaxDateTenantCheckout(state, payload) {
			state.setMaxDateTenantCheckout = payload;
		},
		setIdImg(state, payload) {
			state.extras.idImg = payload;
		},
		setSelfieImg(state, payload) {
			state.extras.selfieImg = payload;
		},
		setCardId(state, payload) {
			state.extras.cardIdVal = payload;
		},
		setCurrentId(state, payload) {
			state.extras.currentId = payload;
		},
		setDraftNotification(state, payload) {
			state.bookingNotification.draft = payload;
		},
		setLastSeenNotification(state, payload) {
			state.bookingNotification.lastSeen = payload;
		},
		setHistoryKostDetail(state, payload) {
			state.historyKostDetail = payload;
		},
		setHistoryDetailCheckin(state, payload) {
			state.historyDetailCheckin = payload;
		},
		setHistoryDetailCheckout(state, payload) {
			state.historyDetailCheckout = payload;
		},
		setRoomDetail(state, payload) {
			state.roomDetail = payload;
		},
		setReviewData(state, payload) {
			state.reviewData = payload;
		},
		setSuccessStopRent(state, payload) {
			state.successStopRent = payload;
		},
		setTotalPoin(state, payload) {
			state.totalPoin = payload;
		}
	},
	getters: {
		userId: state => {
			return state.authData.all.id;
		},
		token: state => {
			return state.token;
		},
		name: state => {
			return state.userEditProfile.name;
		},
		gender: state => {
			if (
				state.userEditProfile.gender === 'Laki-Laki' ||
				state.userEditProfile.gender === 'male'
			) {
				state.userEditProfile.gender = 'male';
			} else {
				state.userEditProfile.gender = 'female';
			}
			return state.userEditProfile.gender;
		},
		photoMedium: state => {
			return state.userProfile.photo;
		},
		userMenuActive: state => {
			return state.userMenuActive;
		},
		bookingNotificationCount: state => {
			return Object.values(state.bookingNotification).reduce((acc, current) => {
				return acc + current;
			}, 0);
		}
	}
};

export default defaultStore;
