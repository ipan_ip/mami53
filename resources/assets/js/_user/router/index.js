import VueRouter from 'vue-router';

window.debounce = require('lodash/debounce');
Vue.use(VueRouter);

const UserVerificationAccount = require('../components/UserVerificationAccount')
	.default;
const UserEditProfile = require('../components/UserEditProfile').default;
const UserKost = require('../components/UserKost').default;
const UserKostSearch = require('../components/UserKostSearch').default;

const UserHistoryTransaction = require('../components/UserHistoryTransaction')
	.default;
const UserHistoryTransactionDetail = require('../components/UserHistoryTransactionDetail')
	.default;
const UserVoucher = require('../components/UserVoucher').default;
const UserVoucherDetail = require('../components/UserVoucherDetail').default;
const UserProfileSettings = require('../components/UserProfileSettings')
	.default;

const UserBookingSection = require('../components/UserBookingSection').default;
const BookingListMain = require('../components/booking/history/BookingListMain')
	.default;

const DraftBookingMain = require('../components/booking/draft/DraftBookingMain.vue')
	.default;
const LastSeenBookingMain = require('../components/booking/last-seen/LastSeenBookingMain.vue')
	.default;

const HistoryKost = require('../components/history-kost/HistoryKostMain.vue')
	.default;
const HistoryKostList = require('../components/history-kost/HistoryKostList.vue')
	.default;
const HistoryKostDetail = require('../components/history-kost/HistoryKostDetail.vue')
	.default;

const MamipoinHistory = require('../components/mamipoin/MamipoinHistory.vue')
	.default;
const MamipoinExpired = require('../components/mamipoin/expired/MamipoinExpired')
	.default;
const MamipoinGuideline = require('../components/mamipoin/guideline/MamipoinGuideline.vue')
	.default;
const UserMamipoin = require('../components/UserMamipoin.vue').default;
const routes = [
	{ path: '/', component: UserVerificationAccount },
	{ path: '/verifikasi-akun', component: UserVerificationAccount },
	{ path: '/edit-profil', component: UserEditProfile },
	{ path: '/kost-saya', component: UserKost },
	{
		path: '/riwayat-kos',
		name: 'Riwayat Kos',
		component: HistoryKost,
		children: [
			{
				path: '',
				name: 'HistoryKostList',
				component: HistoryKostList
			},
			{
				path: '/riwayat-kos/:contractId',
				name: 'HistoryKostDetail',
				component: HistoryKostDetail
			}
		]
	},
	{
		path: '/booking',
		component: UserBookingSection,
		children: [
			{
				path: '',
				component: BookingListMain
			},
			{
				path: 'draft',
				component: DraftBookingMain
			},
			{
				path: 'last-seen',
				component: LastSeenBookingMain
			}
		]
	},
	{ path: '/cari-kost', component: UserKostSearch },
	{ path: '/riwayat-transaksi', component: UserHistoryTransaction },
	{
		path: '/riwayat-transaksi/:invoiceId',
		component: UserHistoryTransactionDetail
	},
	{
		path: '/mamipoin',
		component: UserMamipoin
	},
	{
		path: '/mamipoin/history',
		component: MamipoinHistory
	},
	{
		path: '/mamipoin/expired',
		component: MamipoinExpired
	},
	{
		path: '/mamipoin/guideline',
		component: MamipoinGuideline
	},
	{ path: '/voucherku', component: UserVoucher },
	{
		path: '/voucherku/detail/',
		name: 'Voucher Detail',
		component: UserVoucherDetail,
		props: true
	},
	{ path: '/pengaturan', component: UserProfileSettings }
];

const router = new VueRouter({
	mode: 'history',
	base: '/user',
	routes,
	scrollBehavior(to, from, savedPosition) {
		return {
			selector: '#contentBox'
		};
	}
});

export default router;
