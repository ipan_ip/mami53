const mixinMamipoin = {
	data() {
		return {
			allPoin: [],
			loadAllPoin: false,
			allPoinHasNextPage: '',
			allPoinPage: 2,
			earnPoin: [],
			loadEarnPoin: false,
			earnPoinHasNextPage: '',
			earnPoinPage: 2,
			redeemPoin: [],
			loadRedeemPoin: false,
			redeemPoinHasNextPage: '',
			redeemPoinPage: 2,
			expiredPoin: [],
			loadExpiredPoin: false,
			expiredPoinHasNextPage: '',
			expiredPoinPage: 2,

			// List expired data
			loadExpiredData: false,
			expiredData: [],
			expiredMonth: '',
			isBlacklistOnExpiredPoin: false,
			// guideline
			guideline: [],
			isLoadGuideline: false,
			// TnC
			contentTnc: '',
			isLoadTnc: false
		};
	},

	computed: {
		isTenantBlacklist() {
			return this.$store.state.totalPoin.point === null ? true : false;
		}
	},
	watch: {
		isTenantBlacklist(status) {
			if (status === true) {
				this.$router.push({ path: '/kost-saya' });
			}
		}
	},

	mounted() {
		if (this.isTenantBlacklist === true) {
			this.$router.push({ path: '/kost-saya' });
		}
	},
	methods: {
		toThousand(poin) {
			return poin ? poin.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.') : 0;
		},

		getPoin(type, page) {
			return axios.get(`/user/point/history?type=${type}&page=${page}&limit=8`);
		},

		getDataExpired() {
			this.loadExpiredData = true;
			axios
				.get('/user/point/expiry')
				.then(response => {
					this.expiredData = response.data.near_expiry;
					this.expiredMonth = response.data.expiry_in;
					this.isBlacklistOnExpiredPoin = response.data.point;
					this.loadExpiredData = false;
				})
				.catch(error => {
					if (error) {
						this.swalError(null, 'Terjadi galat. Silakan coba lagi.');
						bugsnagClient.notify(error);
					}
					this.loadExpiredData = false;
				});
		},

		getAllPoin() {
			this.loadAllPoin = true;
			this.getPoin('all', 1)
				.then(response => {
					this.allPoin = [...this.allPoin, ...response.data.data];
					this.allPoinHasNextPage = response.data.has_next;
					this.$store.state.isBlacklistedOnHistory =
						response.data.is_blacklisted;
					this.loadAllPoin = false;
				})
				.catch(error => {
					this.swalError(null, 'Terjadi galat. Silakan coba lagi.');
					bugsnagClient.notify(error);
					this.loadAllPoin = false;
				});
		},

		getRecievedPoin() {
			this.loadEarnPoin = true;
			this.getPoin('earn', 1)
				.then(response => {
					this.earnPoin = [...this.earnPoin, ...response.data.data];
					this.earnPoinHasNextPage = response.data.has_next;
					this.loadEarnPoin = false;
				})
				.catch(error => {
					this.swalError(null, 'Terjadi galat. Silakan coba lagi.');
					bugsnagClient.notify(error);
					this.loadEarnPoin = false;
				});
		},

		getRedeemPoin() {
			this.loadRedeemPoin = true;
			this.getPoin('redeem', 1)
				.then(response => {
					this.redeemPoin = [...this.redeemPoin, ...response.data.data];
					this.redeemPoinHasNextPage = response.data.has_next;
					this.loadRedeemPoin = false;
				})
				.catch(error => {
					this.swalError(null, 'Terjadi galat. Silakan coba lagi.');
					bugsnagClient.notify(error);
					this.loadRedeemPoin = false;
				});
		},
		getExpiredPoin() {
			this.loadExpiredPoin = true;
			this.getPoin('expired', 1)
				.then(response => {
					this.expiredPoin = [...this.expiredPoin, ...response.data.data];
					this.expiredPoinHasNextPage = response.data.has_next;
					this.loadExpiredPoin = false;
				})
				.catch(error => {
					this.swalError(null, 'Terjadi galat. Silakan coba lagi.');
					bugsnagClient.notify(error);
					this.loadExpiredPoin = false;
				});
		},

		nextAllPoint() {
			this.loadAllPoin = true;
			this.getPoin('all', this.allPoinPage)
				.then(response => {
					this.allPoin = [...this.allPoin, ...response.data.data];
					this.allPoinHasNextPage = response.data.has_next;
					this.allPoinPage++;
					this.loadAllPoin = false;
				})
				.catch(error => {
					this.swalError(null, 'Terjadi galat. Silakan coba lagi.');
					bugsnagClient.notify(error);
					this.loadAllPoin = false;
				});
		},
		nextEarnPoint() {
			this.loadEarnPoin = true;
			this.getPoin('earn', this.earnPoinPage)
				.then(response => {
					this.earnPoin = [...this.earnPoin, ...response.data.data];
					this.earnPoinHasNextPage = response.data.has_next;
					this.earnPoinPage++;
					this.loadEarnPoin = false;
				})
				.catch(error => {
					this.swalError(null, 'Terjadi galat. Silakan coba lagi.');
					bugsnagClient.notify(error);
					this.loadEarnPoin = false;
				});
		},
		nextRedeemPoint() {
			this.loadRedeemPoin = true;
			this.getPoin('redeem', this.redeemPoinPage)
				.then(response => {
					this.redeemPoin = [...this.redeemPoin, ...response.data.data];
					this.redeemPoinHasNextPage = response.data.has_next;
					this.redeemPoinPage++;
					this.loadRedeemPoin = false;
				})
				.catch(error => {
					this.swalError(null, 'Terjadi galat. Silakan coba lagi.');
					bugsnagClient.notify(error);
					this.loadRedeemPoin = false;
				});
		},
		nextExpiredPoint() {
			this.loadExpiredPoin = true;
			this.getPoin('expired', this.expiredPoinPage)
				.then(response => {
					this.expiredPoin = [...this.expiredPoin, ...response.data.data];
					this.expiredPoinHasNextPage = response.data.has_next;
					this.expiredPoinPage++;
					this.loadExpiredPoin = false;
				})
				.catch(error => {
					this.swalError(null, 'Terjadi galat. Silakan coba lagi.');
					bugsnagClient.notify(error);
					this.loadExpiredPoin = false;
				});
		},

		getGuideLine() {
			this.isLoadGuideline = true;
			axios
				.get('/user/point/activity?target=tenant')
				.then(response => {
					this.guideline = response.data.data;
					this.isLoadGuideline = false;
				})
				.catch(error => {
					this.swalError(null, 'Terjadi galat. Silakan coba lagi.');
					bugsnagClient.notify(error);
					this.isLoadGuideline = false;
				});
		},
		getTnC() {
			this.isLoadTnc = true;
			axios
				.get('/user/point/tnc')
				.then(response => {
					this.contentTnc = response.data.tnc;
					this.isLoadTnc = false;
				})
				.catch(error => {
					this.swalError(null, 'Terjadi galat. Silakan coba lagi.');
					bugsnagClient.notify(error);
					this.isLoadTnc = false;
				});
		}
	}
};

export default mixinMamipoin;
