const mixinVoucherTracker = {
	data() {
		return {
			userInterface: 'desktop',
			userDevices: [
				{
					device: 'webOS',
					interface: 'mobile'
				},
				{
					device: 'Android',
					interface: 'mobile-android'
				},
				{
					device: 'iPhone',
					interface: 'mobile-ios'
				},
				{
					device: 'iPad',
					interface: 'mobile-ios'
				}
			]
		};
	},

	computed: {
		authData() {
			return this.$store.state.authData;
		}
	},

	methods: {
		checkUserDevice() {
			let found = false;

			this.userDevices.forEach(data => {
				if (navigator.userAgent.indexOf(data.device) !== -1) {
					this.userInterface = data.interface;
					found = true;
				}
			});

			if (!found && window.innerWidth <= 800) {
				this.userInterface = 'mobile';
			}
		}
	}
};

export default mixinVoucherTracker;
