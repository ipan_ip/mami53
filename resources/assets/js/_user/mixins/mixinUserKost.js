const mixinUserKost = {
	computed: {
		getStatusRent() {
			return (
				(this.$store.state.statusRent.hasOwnProperty('status') &&
					this.$store.state.statusRent) || {
					status: false,
					checkout_date: false,
					message: ''
				}
			);
		}
	},

	methods: {
		getStatusRentTenant() {
			const contractId = this.$store.state.userKost.contract_id;

			axios
				.get(`/contract/terminate-status/${contractId}`, {
					baseURL: '/user/mamipay/request'
				})
				.then(response => {
					const dataResponse = response.data;
					if (dataResponse.status) {
						this.$store.commit('setStatusRent', dataResponse.data);
					}
				})
				.catch(error => {
					bugsnagClient.notify(error);
					this.swalError(null, 'Terjadi galat. Silakan coba lagi.');
				});
		}
	}
};

export default mixinUserKost;
