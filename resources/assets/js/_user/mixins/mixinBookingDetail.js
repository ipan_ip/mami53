const mixinBookingDetail = {
	data() {
		return {
			isLoading: true,
			checkinDate: '',
			checkoutDate: '',
			createdDate: '',
			isShowBookingModalFacilities: false,
			isModalCancel: false,
			isModalCancelSuccess: false
		};
	},

	computed: {
		isBookingFailed() {
			return (
				this.bookingStatus.status === 'cancelled' ||
				this.bookingStatus.status === 'rejected' ||
				this.bookingStatus.status === 'expired'
			);
		},
		roomGender() {
			if (this.bookingDetail.room.gender === 1) {
				return 'Putra';
			} else if (this.bookingDetail.room.gender === 2) {
				return 'Putri';
			} else {
				return 'Campur';
			}
		},
		cancelReasonLabel() {
			if (this.bookingStatus.status === 'cancelled') {
				return 'Pembatalan';
			} else if (this.bookingStatus.status === 'rejected') {
				return 'Penolakan';
			} else {
				return 'Expired';
			}
		},
		rentCount() {
			if (this.bookingDetail.rent_count_type === 'yearly') {
				return {
					rentType: 'tahun',
					rentPrice: this.bookingDetail.room.price.yearly
				};
			} else if (this.bookingDetail.rent_count_type === 'daily') {
				return {
					rentType: 'hari',
					rentPrice: this.bookingDetail.room.price.daily
				};
			} else if (this.bookingDetail.rent_count_type === 'quarterly') {
				return {
					rentType: '3 bulan',
					rentPrice: this.bookingDetail.room.price.quarterly
				};
			} else if (this.bookingDetail.rent_count_type === 'semiannually') {
				return {
					rentType: '6 bulan',
					rentPrice: this.bookingDetail.room.price.semiannualy
				};
			} else if (this.bookingDetail.rent_count_type === 'weekly') {
				return {
					rentType: 'minggu',
					rentPrice: this.bookingDetail.room.price.weekly
				};
			} else {
				return {
					rentType: 'bulan',
					rentPrice: this.bookingDetail.room.price.monthly
				};
			}
		},
		roomFacility() {
			return [
				{ label: 'Fasilitas Kamar', value: this.bookingDetail.room.fac_room },
				{
					label: 'Fasilitas Kamar Mandi',
					value: this.bookingDetail.room.fac_bath
				},
				{ label: 'Fasilitas Umum', value: this.bookingDetail.room.fac_share },
				{ label: 'Parkir', value: this.bookingDetail.room.fac_park },
				{ label: 'Akses Lingkungan', value: this.bookingDetail.room.fac_near }
			];
		},
		showCountdownTimer() {
			return (
				this.bookingStatus.status === 'waiting_payment' &&
				this.bookingDetail.payment_expired_date
			);
		}
	},

	methods: {
		updateDateFormat(localeId = 'en') {
			this.checkinDate = this.$dayjs(this.bookingDetail.checkin_date)
				.locale(localeId)
				.format('dddd, DD MMM YYYY');
			this.checkoutDate = this.$dayjs(this.bookingDetail.checkout_date)
				.locale(localeId)
				.format('dddd, DD MMM YYYY');
			this.createdDate = this.$dayjs(this.bookingDetail.created_at)
				.locale(localeId)
				.format('DD MMMM YYYY');
		},
		openMap() {
			window.open(
				`https://www.google.com/maps/search/?api=1&query=${this.bookingDetail.room.latitude},${this.bookingDetail.room.longitude}`,
				'_blank'
			);
		},
		openKostList() {
			this.$router.push({ path: '/kost-saya' });
		},
		openModalFac() {
			this.isShowBookingModalFacilities = true;
		},
		closeModalFac() {
			this.isShowBookingModalFacilities = false;
		},
		cancelBooking() {
			this.isModalCancel = true;
		},
		closeModalCancel() {
			this.isModalCancel = false;
		},
		showModalCancelSuccess() {
			this.closeModalCancel();
			this.isModalCancelSuccess = true;
		}
	}
};

export default mixinBookingDetail;
