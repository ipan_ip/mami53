import Dayjs from 'vue-dayjs';
import 'dayjs/locale/id';

import Vuex from 'vuex';

import VeeValidate, { Validator } from 'vee-validate';
import id from 'vee-validate/dist/locale/id';

import defaultStore from './store/index';
import router from './router/index';

import VueMasonry from 'vue-masonry-css';
import VueClipboard from 'vue-clipboard2';

import 'Js/@mixins/MixinNavigatorIsMobile';
import 'Js/@mixins/MixinSwalLoading';
import 'Js/@mixins/MixinSwalSuccessError';
import App from './components/App.vue';
import mixinAuth from 'Js/@mixins/MixinAuth';

window.md5 = require('md5');

Vue.use(VueMasonry);
Vue.use(VueClipboard);

window.debounce = require('lodash/debounce');

Vue.use(Dayjs, {
	lang: 'id'
});

Vue.use(Vuex);
window.validator = new Validator();
Validator.localize('id', id);
Vue.use(VeeValidate, {
	locale: 'id'
});

Vue.component('breadcrumb-trails', () =>
	import('Js/@components/BreadcrumbTrails.vue')
);

Vue.component('app', App);

const store = new Vuex.Store(defaultStore);

const app = new Vue({
	el: '#app',
	router,
	store,
	mixins: [mixinAuth],
	created() {
		this.$dayjs.locale('id');
	},
	mounted() {
		try {
			document.getElementById('preloading').remove();
		} catch (e) {
			document.getElementById('preloading').style.display = 'none';
		}
	}
});
