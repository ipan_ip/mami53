require('../starter3');

window.Vue = require('vue').default;

import BootstrapVue from 'bootstrap-vue';
Vue.use(BootstrapVue);

import Vuex from 'vuex';
Vue.use(Vuex);

import mixinAuth from '../@mixins/MixinAuth';

import VueLazyload from 'vue-lazyload';
Vue.use(VueLazyload, {
	preLoad: 1.3,
	error: '/assets/loading/mamikos_placeholder_load.svg',
	loading: '/assets/loading/mamikos_placeholder_load.svg',
	attempt: 2
});

// Common Style
require('../../css/@appmamikos-new.css');

const App = require('./components/landing-agent/App.vue').default;
const ContainerLandingAgent = require('./components/landing-agent/ContainerLandingAgent.vue')
	.default;

// Global Components
Vue.component(
	'landing-navbar',
	require('./components/landing-agent/LandingNavbar.vue').default
);
Vue.component('login-user', () => import('Js/@global/components/LoginUserNew'));

// Container Component
Vue.component('app', App);
Vue.component('container-landing-agent', ContainerLandingAgent);

// Local Component
Vue.component(
	'landing-agent-big-banner',
	require('./components/landing-agent/LandingAgentBigBanner.vue').default
);
Vue.component(
	'landing-agent-big-banner-swiper',
	require('./components/landing-agent/LandingAgentBigBannerSwiper.vue').default
);
Vue.component(
	'landing-agent-testimonial',
	require('./components/landing-agent/LandingAgentTestimonial.vue').default
);
Vue.component(
	'landing-agent-story',
	require('./components/landing-agent/LandingAgentTestimonialStory.vue').default
);
Vue.component(
	'landing-agent-form',
	require('./components/landing-agent/LandingAgentForm.vue').default
);
Vue.component(
	'landing-agent-modal',
	require('./components/landing-agent/LandingAgentInfoModal.vue').default
);

const store = new Vuex.Store({
	state: {
		authCheck: {},
		authData: {}
	},
	mutations: {
		setAuthCheck(state, payload) {
			state.authCheck = payload;
		},
		setAuthData(state, payload) {
			state.authData = payload;
		}
	}
});

Vue.config.productionTip = false;

const app = new Vue({
	el: '#app',
	store,
	mixins: [mixinAuth]
});
