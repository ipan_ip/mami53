require('../starter3');

// window.debounce = require('lodash/debounce');

window.Vue = require('vue').default;

import BootstrapVue from 'bootstrap-vue';
Vue.use(BootstrapVue);

import id from 'vee-validate/dist/locale/id';
import VeeValidate, { Validator } from 'vee-validate';
window.validator = new Validator();
Validator.localize('id', id);
Vue.use(VeeValidate, {
	locale: 'id'
});

import VueLazyload from 'vue-lazyload';
Vue.use(VueLazyload, {
	preLoad: 1.3,
	error: '/assets/loading/mamikos_placeholder_load.svg',
	loading: '/assets/loading/mamikos_placeholder_load.svg',
	attempt: 2
});

import mixinAuth from '../@mixins/MixinAuth';

require('../@mixins/MixinSwalLoading');
require('../@mixins/MixinSwalSuccessError');

// Common Style
require('../../css/@appmamikos-new.css');

const App = require('./components/landing-standard/App.vue').default;
const ContainerLandingStandard = require('./components/landing-standard/ContainerLandingStandard.vue')
	.default;

// Global Components

// Container Component
Vue.component('app', App);
Vue.component('container-landing-standard', ContainerLandingStandard);

Vue.component(
	'standard-section-one',
	require('./components/landing-standard/StandardSectionOne.vue').default
);
Vue.component(
	'standard-section-two',
	require('./components/landing-standard/StandardSectionTwo.vue').default
);
Vue.component(
	'standard-section-three',
	require('./components/landing-standard/StandardSectionThree.vue').default
);
Vue.component(
	'standard-section-four',
	require('./components/landing-standard/StandardSectionFour.vue').default
);
Vue.component(
	'landing-navbar',
	require('./components/landing-agent/LandingNavbar.vue').default
);

Vue.config.productionTip = false;

const app = new Vue({
	el: '#app',
	mixins: [mixinAuth],
	data() {
		return {
			state: {
				authCheck: {},
				authData: {}
			}
		};
	}
});
