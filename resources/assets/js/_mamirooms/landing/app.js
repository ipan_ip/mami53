import 'Js/@mixins/MixinSwalLoading';
import 'Js/@mixins/MixinSwalSuccessError';
import App from './components/App.vue';
import mixinAuth from 'Js/@mixins/MixinAuth';

Vue.component('app', App);

const app = new Vue({
	el: '#app',
	mixins: [mixinAuth],
	data() {
		return {
			state: {
				token: document.head.querySelector('meta[name="csrf-token"]').content,
				authCheck: {},
				authData: {},
				register: {
					conditions: conditions,
					facilities: facilities
				}
			}
		};
	},
	mounted() {
		try {
			document.getElementById('preloading').remove();
		} catch (e) {
			document.getElementById('preloading').style.display = 'none';
		}
	}
});
