require('Js/starter');

// window.debounce = require('lodash/debounce');

window.Vue = require('vue').default;

import Vuex from 'vuex';
Vue.use(Vuex);

import mixinAuth from 'Js/@mixins/MixinAuth';

const App = require('./components/App.vue').default;
const ContainerGraduationPhoto = require('./components/ContainerGraduationPhoto.vue')
	.default;

// Common Style
// require('Js/css/@appmamikos-new.css');

// Global Components
Vue.component('breadcrumb-trails', () =>
	import('Js/@components/BreadcrumbTrails')
);
Vue.component('list-placeholder', () =>
	import('Js/@components/ListPlaceholder')
);
Vue.component('room-list', () => import('Js/@components/RoomList'));

// Container Component
Vue.component('app', App);
Vue.component('container-graduation-photo', ContainerGraduationPhoto);

// Local Component
Vue.component(
	'graduation-photo-navbar',
	require('./components/GraduationPhotoNavbar.vue').default
);
Vue.component(
	'graduation-photo-big-banner',
	require('./components/GraduationPhotoBigBanner.vue').default
);
Vue.component(
	'graduation-photo-recommendation-kost',
	require('./components/GraduationPhotoRecommendationKost.vue').default
);
Vue.component(
	'graduation-photo-recommendation-job',
	require('./components/GraduationPhotoRecommendationJob.vue').default
);
Vue.component('job-list', require('./components/VacancyList.vue').default);

const store = new Vuex.Store({
	state: {
		authCheck: {},
		authData: {}
	},
	mutations: {
		setAuthCheck(state, payload) {
			state.authCheck = payload;
		},
		setAuthData(state, payload) {
			state.authData = payload;
		}
	}
});

Vue.config.productionTip = false;

const app = new Vue({
	el: '#app',
	store,
	mixins: [mixinAuth]
});
