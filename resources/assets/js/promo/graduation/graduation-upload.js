require('Js/starter');

// window.debounce = require('lodash/debounce');

window.Vue = require('vue').default;

import Vuex from 'vuex';
Vue.use(Vuex);

import mixinAuth from 'Js/@mixins/MixinAuth';

import App from './components/uploader/App.vue';

// Global Components
Vue.component('room-list', () => import('Js/@components/RoomList'));

// Container Component
Vue.component('app', App);

Vue.component(
	'graduation-photo-navbar',
	require('./components/GraduationPhotoNavbar.vue').default
);
Vue.component(
	'container-graduation-upload',
	require('./components/uploader/ContainerGraduationUpload.vue').default
);
Vue.component(
	'upload-photo',
	require('./components/uploader/GraduationPhotoUploader.vue').default
);

// Local Component
Vue.component(
	'graduation-uploader',
	require('./components/uploader/GraduationUploader.vue').default
);

const store = new Vuex.Store({
	state: {
		authCheck: {},
		authData: {}
	},
	mutations: {
		setAuthCheck(state, payload) {
			state.authCheck = payload;
		},
		setAuthData(state, payload) {
			state.authData = payload;
		}
	}
});

Vue.config.productionTip = false;

const app = new Vue({
	el: '#app',
	store,
	mixins: [mixinAuth]
});
