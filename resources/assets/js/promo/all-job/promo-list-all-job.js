require('Js/starter');

window.debounce = require('lodash/debounce');

window.Vue = require('vue').default;

import mixinAuth from 'Js/@mixins/MixinAuth';

require('Js/@mixins/MixinCenterModal');
require('Js/@mixins/MixinScrollTop');

const ContainerPromoList = require('./components/ContainerPromoList.vue')
	.default;

// Container Component
Vue.component('container-promo-list', ContainerPromoList);

// Global Components
Vue.component('breadcrumb-trails', () =>
	import('Js/@components/BreadcrumbTrails')
);
Vue.component('list-placeholder', () =>
	import('Js/@components/ListPlaceholder')
);
Vue.component('mami-loading-inline', () =>
	import('Js/@components/MamiLoadingInline')
);
Vue.component('room-list', () => import('Js/@components/RoomList'));

// Local Components
Vue.component('promo-title', require('./components/PromoTitle.vue').default);
Vue.component('promo-filter', require('./components/PromoFilter.vue').default);
Vue.component('vacancy-list', require('./components/VacancyList.vue').default);
Vue.component('promo-list', require('./components/PromoList.vue').default);
Vue.component(
	'promo-list-pagination',
	require('./components/PromoListPagination.vue').default
);

Vue.config.productionTip = false;

const app = new Vue({
	el: '#app',
	mixins: [mixinAuth],
	data() {
		return {
			state: {
				authCheck: {},
				authData: {},
				article: '',
				extras: {
					isBusy: false
				}
			}
		};
	},
	created() {
		this.state.article = article;
	},
	mounted() {
		try {
			document.getElementById('prerender').remove();
		} catch (e) {
			document.getElementById('prerender').style.display = 'none';
		}
	}
});
