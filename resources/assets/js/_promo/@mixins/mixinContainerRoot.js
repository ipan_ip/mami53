import EventBus from 'Js/_promo/event-bus/event-bus';
import _get from 'lodash/get';
import { makeAPICall } from 'Js/@utils/makeAPICall.js';

import { genderTypes, rentTypes } from 'Js/@utils/kostFilter';

const mixinContainerRoot = {
	data() {
		return {
			rooms: [],
			params: {
				filter: {
					gender: [0, 1, 2],
					price_range: [0, 15000000],
					property_type: 'kost',
					rent_type: 2,
					random_seeds: Math.floor(Math.random() * (1000 - 0 + 1)) + 0
				},
				sorting: {
					field: 'price',
					direction: '-'
				},
				limit: 20,
				total: 0,
				offset: 0
			},
			place: this.$root.state.selectedArea.value,

			cityTitle: '',
			cityList: {},

			isLoading: true,
			isInitialLoading: true,
			isRoomEmpty: false,

			busy: false,

			currentPage: 1,
			limit: 20,
			requestLimit: 90,
			roomDataCount: 1,
			roomDataLoaded: 18,

			showTopButton: false,

			navigations: [
				{ name: 'Cari kost', url: '/cari' },
				{ name: 'Cari apartemen', url: '/apartemen' },
				{ name: 'Cari lowongan kerja', url: '/loker' }
			]
		};
	},

	computed: {
		article() {
			return this.$root.state.article;
		},
		totalPage() {
			let total = Math.ceil(this.params.total / this.params.limit);
			if (total >= 10) {
				total = 10;
			}
			return Number(total);
		},
		roomsDisplayNumber() {
			let a = this.params.offset + 1;
			let b = this.params.offset + this.rooms.length;
			return `${a} - ${b} Kos dari ${this.params.total}`;
		},
		specificArea() {
			return this.$root.state.selectedSpecificArea;
		},
		parentLanding() {
			return this.$root.state.parentLanding;
		},
		subLocation() {
			return this.$root.state.subLocation;
		},
		landingTitle() {
			return this.$root.state.landingTitle;
		},
		landingType() {
			return this.$root.state.landingType;
		}
	},

	created() {
		this.initListener();
		this.initCityList();
		this.getRoomList();
	},

	methods: {
		// --------------------------------------------------
		// FILTER
		// --------------------------------------------------
		initListener() {
			EventBus.$on('filterPlaceChanged', payload => {
				window.open(`/booking/${payload}`, '_self');
			});
			EventBus.$on('filterSpecificPlaceChanged', payload => {
				window.open(`${payload.slug}`, '_self');
			});
			EventBus.$on('filterGenderChanged', payload => {
				this.setFilterGender(payload);
			});
			EventBus.$on('filterRentTypeChanged', payload => {
				this.setFilterRentType(payload);
			});
			EventBus.$on('filterPriceRangeChanged', payload => {
				this.setFilterPriceRange(payload);
			});
			EventBus.$on('filterSortingChanged', payload => {
				this.setFilterSorting(payload);
			});
			EventBus.$on('filterSetFromModal', payload => {
				this.setFilterAll(payload);
			});
		},
		setFilterPlace(payload) {
			this.selectedSpecificArea = false;
			if (payload != 'all') {
				history.pushState({}, null, `/${this.landingType}/${payload}`);
			} else {
				history.pushState({}, null, `/${this.landingType}/`);
			}
			this.generateTitle(payload);
			this.place = payload;
			if (this.landingType == 'booking') {
				document.location.reload();
				this.getRoomList();
			} else {
				this.refreshList();
			}
		},
		setFilterGender(payload) {
			this.params.filter.gender = payload;
			this.refreshList();
		},
		setFilterRentType(payload) {
			this.params.filter.rent_type = payload.rentType;
			this.params.filter.price_range = payload.priceRange;
			this.refreshList();
		},
		setFilterPriceRange(payload) {
			this.params.filter.price_range = payload;
			this.refreshList();
		},
		setFilterSorting(payload) {
			this.params.sorting.field = payload[0];
			this.params.sorting.direction = payload[1];
			this.refreshList();
		},
		setFilterAll(payload) {
			this.params.filter.gender = payload.gender;
			this.params.filter.price_range = payload.price_range;
			this.params.filter.rent_type = payload.rent_type;
			this.params.sorting.field = payload.sorting[0];
			this.params.sorting.direction = payload.sorting[1];
			this.refreshList();
		},
		initCityList() {
			if (this.$root.state.cityList == null) {
				this.getCityListApi();
			} else {
				this.cityList = this.$root.state.cityList;
			}
		},
		async getCityListApi() {
			const cityListTemp = {};

			const response = await makeAPICall({
				method: 'get',
				url: '/filters/promo?type=daily'
			});

			if (response && typeof response === 'object' && response.cities) {
				Object.keys(response.cities).forEach(element => {
					cityListTemp[element.replace(/\s+/g, '-').toLowerCase()] = {
						label: response.cities[element],
						slug: element.replace(/\s+/g, '-').toLowerCase()
					};
				});

				this.cityList = cityListTemp;
			}
		},

		// --------------------------------------------------
		// ROOM LIST
		// --------------------------------------------------
		async getRoomList() {
			this.isLoading = true;
			this.isRoomEmpty = false;

			const response = await makeAPICall({
				method: 'post',
				url: '/stories/list',
				data: this.getParams()
			});

			if (
				response &&
				typeof response === 'object' &&
				response.rooms &&
				translateWords
			) {
				const rooms = translateWords(response.rooms) || [];

				this.rooms = rooms;
				this.params.total = response.total;
				this.isLoading = false;
				if (response.total === 0) {
					this.isRoomEmpty = true;
				}
				this.isInitialLoading = false;

				this.updateEventTrack();
			} else {
				this.isLoading = false;
				this.isRoomEmpty = true;
			}
		},
		getParams() {
			const params = {};
			params.filters = this.params.filter;
			params.sorting = this.params.sorting;
			params.limit = this.params.limit;
			params.offset = this.params.offset;
			params.referrer = 'promo';
			params.include_promoted = true;
			params.filters.flash_sale = false;

			// custom based on the landing
			if (this.landingType === 'booking') {
				params.filters.booking = 1;
				if (this.parentLanding.length > 0 && this.selectedSpecificArea) {
					params.location = this.place;
					params.filters.property_type = 'kost';
					params.filters = this.params.filter;
				}
			}

			if (this.landingType === 'kost-bebas') params.filters.tag_ids = [59];
			if (this.landingType === 'kost-harian') {
				if (this.isInitialLoading) params.filters.rent_type = 0;
				params.filters.tag_ids = [59];
			}
			if (this.landingType === 'kost-km-dalam') params.filters.tag_ids = [1];
			if (this.landingType === 'kost-pasutri') params.filters.tag_ids = [60];

			// LOCATION

			if (this.place == 'Semua Kota' || this.place == 'all') {
				delete params.filters.place;
				delete params.location;
			} else {
				if (this.landingType == 'booking') {
					// only if booking, get coordinate
					if (typeof this.cityList[this.place] == 'undefined') {
						this.isRoomEmpty = true;
					} else if (this.subLocation != null && this.subLocation.length > 0) {
						params.location = this.subLocation;
					} else {
						params.location = this.cityList[this.place].location;
					}
				} else {
					// else, set filters.place array with place label
					if (this.cityList[this.place] == 'undefined') {
						this.isRoomEmpty = true;
					} else {
						params.filters.place = [
							this.stringCapitalize(this.place).replace('-', ' ')
						];
					}
				}
			}
			return params;
		},
		refreshList() {
			this.params.offset = 0;
			this.busy = false;
			if (this.$refs.pagination) this.currentPage = 1;
			this.getRoomList();
			this.gotoTop();
		},

		// --------------------------------------------------
		// PAGINATION
		// --------------------------------------------------
		onPageChanged(page) {
			let dataOffset = (page - 1) * 18;
			this.params.offset = dataOffset;

			let count = 18 * (page - 1) + 1;
			this.roomDataCount = count;

			let inpage = 18 * (page - 1) + this.rooms.length;
			this.roomDataLoaded = inpage;

			this.getRoomList();
			this.gotoTop();
		},

		// --------------------------------------------------
		// ARTICLE
		// --------------------------------------------------

		// --------------------------------------------------
		// MISC
		// --------------------------------------------------
		gotoTop() {
			setTimeout(() => {
				window.scrollTo({ top: 0, behavior: 'smooth' });
			}, 50);
		},

		generateTitle(payload) {
			const cityTitle = payload.replace('-', ' 	');
			if (landingType == 'booking') {
				document.title = `Booking Kost ${this.stringCapitalize(
					cityTitle
				)} - Harga Lebih Murah dari Hotel - Mamikos`;
			}
		},

		stringCapitalize(item) {
			return item.replace(/(^|\s)([a-z])/g, (m, p1, p2) => {
				return p1 + p2.toUpperCase();
			});
		},

		updateEventTrack() {
			tracker('moe', [
				'[User] Visit LP Booking',
				{
					city: this.place,
					filter_gender:
						genderTypes[
							_get(this.params, 'filter.gender', [0, 1, 2]).toString()
						],
					filter_rent_periode:
						rentTypes[_get(this.params, 'filter.rent_type', 2)],
					filter_price_min: _get(this.params, 'filter.price_range[0]', 0),
					filter_price_max: _get(this.params, 'filter.price_range[1]', 0),
					page_number: this.currentPage,
					max_page_number: this.totalPage,
					is_empty_result: this.isRoomEmpty,
					number_of_result: _get(this.params, 'total', 0)
				}
			]);
		}
	}
};

export default mixinContainerRoot;
