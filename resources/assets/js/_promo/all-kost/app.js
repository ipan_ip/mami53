import 'Js/@mixins/MixinNavigatorIsMobile';
import 'Js/@mixins/MixinSwalLoading';
import 'Js/@mixins/MixinSwalSuccessError';
import App from './components/App.vue';
import mixinAuth from 'Js/@mixins/MixinAuth';

import VueRouter from 'vue-router';
Vue.use(VueRouter);

Vue.component('app', App);

// Global Components
Vue.component('breadcrumb-trails', () =>
	import('Js/@components/BreadcrumbTrails')
);
Vue.component('list-placeholder', () =>
	import('Js/@components/ListPlaceholder')
);
Vue.component('mami-loading-inline', () =>
	import('Js/@components/MamiLoadingInline')
);
Vue.component('room-list', () => import('Js/@components/RoomList'));

Vue.config.productionTip = false;

const app = new Vue({
	el: '#app',
	mixins: [mixinAuth],
	data() {
		return {
			state: {
				authCheck: {},
				authData: {},
				article: '',
				cityTitle: '',
				cityList: {},
				landingTitle: '',
				landingType: '',
				selectedArea: {},
				urlParams: {}
			}
		};
	},
	created() {
		this.state.article = article;
		this.state.cityTitle = cityTitle;
		this.state.selectedArea = selectDefault;
		this.state.landingTitle = landingTitle;
		this.state.landingType = landingType;
		if (cityList === null) {
			this.state.cityList = cityList;
		} else {
			Object.assign(this.state.cityList, cityList);
		}
		this.state.urlParams = urlParams;
	},
	mounted() {
		try {
			document.getElementById('preloading').remove();
		} catch (e) {
			document.getElementById('preloading').style.display = 'none';
		}
	}
});
