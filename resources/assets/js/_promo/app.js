import 'Js/@mixins/MixinNavigatorIsMobile';
import 'Js/@mixins/MixinSwalLoading';
import 'Js/@mixins/MixinSwalSuccessError';
import App from './booking/App.vue';
import mixinAuth from 'Js/@mixins/MixinAuth';

import { translateWords } from 'Js/@utils/langTranslator.js';

import VueRouter from 'vue-router';
Vue.use(VueRouter);

Vue.component('app', App);

// Global Components
Vue.component('breadcrumb-trails', () =>
	import('Js/@components/BreadcrumbTrails')
);
Vue.component('list-placeholder', () =>
	import('Js/@components/ListPlaceholder')
);
Vue.component('mami-loading-inline', () =>
	import('Js/@components/MamiLoadingInline')
);
Vue.component('room-list', () => import('Js/@components/RoomList'));

Vue.config.productionTip = false;

window.translateWords = translateWords;

const app = new Vue({
	el: '#app',
	mixins: [mixinAuth],
	data() {
		return {
			state: {
				authCheck: {},
				authData: {},
				article: '',
				cityTitle: '',
				cityList: {},
				landingTitle: '',
				landingType: '',
				selectedArea: {},
				selectedSpecificArea: false,
				filters: [],
				parentLanding: [],
				parentId: '',
				subLocation: null
			}
		};
	},
	created() {
		this.state.article = article;
		this.state.cityTitle = cityTitle;
		this.state.selectedArea = selectDefault;
		this.state.landingTitle = landingTitle;
		this.state.landingType = landingType;
		if (cityList === null) {
			this.state.cityList = cityList;
		} else {
			Object.assign(this.state.cityList, cityList);
		}
		if (typeof parentLanding !== 'undefined') {
			this.state.parentLanding = parentLanding;
		}
		if (typeof subLocation !== 'undefined') {
			this.state.subLocation = subLocation;
		}
		if (typeof filters !== 'undefined') {
			this.state.filters = filters;
		}
		if (typeof parentId !== 'undefined') {
			this.state.parentId = parseInt(parentId);
		}
	},
	mounted() {
		try {
			document.getElementById('preloading').remove();
		} catch (e) {
			document.getElementById('preloading').style.display = 'none';
		}
	}
});
