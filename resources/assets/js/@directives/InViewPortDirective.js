/**
 * directive to check if the top of component is visible on the viewport or not
 * you can use it with attach this custom directive v-inviewport
 * use your callback like v-inviewport="your callback"
 * if you want to add offset (like if you use skeleton for the placeholder)
 * just add the number offset like this v-inviewport:250="your callback"
 * it will call your callback directly by default if even 1px
 * from the top component is already on the viewport
 */

export default {
	inviewport: {
		isLiteral: true,
		inserted: (el, binding) => {
			const checkElementVisibility = () => {
				const rect = el.getBoundingClientRect();
				let offset = parseInt(binding.arg);

				offset =
					typeof offset === 'number' && !isNaN(offset) ? parseInt(offset) : 0;

				const rectOffset = rect.top + offset;
				const inView =
					rectOffset <=
					(window.innerHeight || document.documentElement.clientHeight);

				if (inView) {
					binding.value();
					window.removeEventListener('scroll', checkElementVisibility);
				}
			};
			window.addEventListener('scroll', checkElementVisibility);
			checkElementVisibility();
		}
	}
};
