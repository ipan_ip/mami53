require('es6-promise/auto');

import 'vue-strap/dist/isMobileBrowser.js';

window.axios = require('axios');
window.axios.defaults.headers.common = {
	'X-Requested-With': 'XMLHttpRequest'
};
window.axios.defaults.baseURL = '/garuda';
window.axios.defaults.headers = {
	'Content-Type': 'application/json',
	'X-GIT-Time': '1406090202',
	Authorization: 'GIT WEB:WEB'
};

window.Vue = require('vue').default;
Vue.config.productionTip = false;

import VueLazyload from 'vue-lazyload';
Vue.use(VueLazyload);

import Toasted from 'vue-toasted';
Vue.use(Toasted);

import bugsnag from '@bugsnag/js';
import bugsnagVue from '@bugsnag/plugin-vue';

window.bugsnagClient = bugsnag(bugsnagApiKey);
bugsnagClient.use(bugsnagVue, Vue);

window.Cookies = require('js-cookie');

window.swal = require('sweetalert2/dist/sweetalert2.js');
import 'sweetalert2/dist/sweetalert2.min.css';
