const mixinAuth = {
	created() {
		try {
			//with Vuex
			this.$store.commit('setAuthCheck', authCheck);
			this.$store.commit('setAuthData', authData);
		} catch (e) {
			//without Vuex
			this.$root.state.authCheck = authCheck;
			this.$root.state.authData = authData;
		}
	}
};

export default mixinAuth;
