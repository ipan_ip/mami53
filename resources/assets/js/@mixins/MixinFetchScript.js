const mixinFetchScript = {
	methods: {
		fetchExternalScript(path, callback) {
			const script = document.createElement('script');

			script.setAttribute('src', path);
			document.head.appendChild(script);

			if (typeof callback !== 'undefined' && callback !== null) {
				script.onload = () => {
					this.$nextTick(() => {
						callback();
					});
				};
			}
		}
	}
};

export default mixinFetchScript;
