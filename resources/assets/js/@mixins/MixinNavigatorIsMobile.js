Vue.mixin({
	computed: {
		navigator() {
			const mobileScreenBreakpoint = 768;
			const width =
				window.innerWidth ||
				document.documentElement.clientWidth ||
				document.body.clientWidth;

			return { isMobile: navigator.isMobile || width < mobileScreenBreakpoint };
		}
	}
});
