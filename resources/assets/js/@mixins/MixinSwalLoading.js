Vue.mixin({
	methods: {
		openSwalLoading(text, color) {
			if (text == null) {
				text = '';
			}

			if (color == null) {
				color = '#1BAA56';
			}

			swal({
				text: text,
				allowOutsideClick: false,
				allowEscapeKey: false,
				allowEnterKey: false,
				confirmButtonText: '',
				confirmButtonColor: color,
				width: '280px',
				onOpen() {
					swal.showLoading();
				}
			});
		},
		closeSwalLoading() {
			swal.close();
		}
	}
});
