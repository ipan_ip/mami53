import _get from 'lodash/get';

export default {
	computed: {
		token() {
			return document.head.querySelector('meta[name="csrf-token"]').content;
		},
		authData() {
			return this.$store ? this.$store.state.authData : authData;
		},
		authCheck() {
			return this.$store ? this.$store.state.authCheck : authCheck;
		},
		notOwner() {
			return this.authCheck.user || !this.authCheck.all;
		}
	},
	methods: {
		loveThisPage(room) {
			if (this.authCheck.all) {
				if (this.authCheck.user) {
					this.toggleLovePost(room);
				} else {
					this.swalError(
						null,
						'Silakan logout akun Pemilik dan login akun Pencari terlebih dahulu'
					);
				}
			} else {
				this.$emit('toggleLoginModal');
			}
		},
		handleSubmitFavMoEngage(room, response) {
			tracker('moe', [
				'[User] Submit Favorit',
				{
					is_booking: _get(room, 'is_booking', false),
					is_premium: _get(room, 'is_premium_owner', false),
					is_promoted: _get(room, 'is_promoted', false),
					is_promo_ngebut: _get(room, 'is_flash_sale', false),
					goldplus_status: _get(room, 'goldplus_status', null),
					success_status: _get(response, 'data.status', false),
					property_type: _get(room, 'roomType', 'apartemen'),
					property_id: _get(room, '_id', 0),
					property_city: room.area_city
						? room.area_city
						: room.city
						? room.city
						: '',
					property_name: room.room_title
						? room.room_title
						: room['room-title']
						? room['room-title']
						: '',
					room_available: _get(room, 'available_room', '')
				}
			]);
		},
		toggleLovePost(room) {
			this.openSwalLoading();
			axios
				.post('/stories/' + room._id + '/love', {
					_id: room._id,
					_token: this.token
				})
				.then(response => {
					if (response.data.status) {
						if (response.data.success === 'love') {
							this.setLoveStatus(room._id, true);
							this.closeSwalLoading();
							this.$toasted.show('Sukses tersimpan', {
								className: 'fav-share-widget-custom-toast-success',
								position: 'bottom-center',
								duration: 2000
							});
							this.handleSubmitFavMoEngage(room, response);
						} else if (response.data.success === 'unlove') {
							this.setLoveStatus(room._id, false);
							this.closeSwalLoading();
						}
					} else {
						this.closeSwalLoading();
						this.$toasted.show('Gagal, silahkan coba lagi', {
							className: 'fav-share-widget-custom-toast-fail',
							position: 'bottom-center',
							duration: 2000
						});
					}
				})
				.catch(error => {
					this.closeSwalLoading();
					bugsnagClient.notify(error);
					this.$toasted.show('Gagal, silahkan coba lagi', {
						className: 'fav-share-widget-custom-toast-fail',
						position: 'bottom-center',
						duration: 2000
					});
				});
		}
	}
};
