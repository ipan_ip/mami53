Vue.mixin({
	methods: {
		swalSuccess(title, text, onConfirm) {
			if (title == null) {
				title = '';
			}

			if (text == null) {
				text = '';
			}

			if (onConfirm == null) {
				onConfirm = () => {};
			}

			swal({
				title: title,
				text: text,
				type: 'success',
				confirmButtonColor: '#1baa56'
			}).then(() => {
				onConfirm();
			});
		},
		swalSuccessTimer(title, text, onDismiss) {
			if (title == null) {
				title = '';
			}

			if (text == null) {
				text = '';
			}

			if (onDismiss == null) {
				onDismiss = () => {};
			}

			swal({
				title: title,
				text: text,
				type: 'success',
				timer: 3000,
				allowOutsideClick: false,
				allowEscapeKey: false,
				allowEnterKey: false,
				confirmButtonColor: '#1baa56',
				onOpen() {
					swal.showLoading();
				}
			}).then(
				() => {},
				dismiss => {
					onDismiss();
				}
			);
		},
		swalSuccessPrompt(
			title,
			text,
			confirmText,
			cancelText,
			onConfirm,
			onDismiss
		) {
			if (title == null) {
				title = '';
			}

			if (text == null) {
				text = '';
			}

			if (confirmText == null) {
				confirmText = 'Ya';
			}

			if (cancelText == null) {
				cancelText = 'Tidak';
			}

			if (onConfirm == null) {
				onConfirm = () => {};
			}

			swal({
				title: title,
				text: text,
				type: 'success',
				allowOutsideClick: false,
				allowEscapeKey: false,
				showCancelButton: true,
				confirmButtonColor: '#1baa56',
				cancelButtonColor: '#ec4a0c',
				confirmButtonText: confirmText,
				cancelButtonText: cancelText
			}).then(result => {
				if (result.value) {
					onConfirm();
				} else if (dismiss) {
					onDismiss();
				}
			});
		},
		swalError(errors, error, onConfirm) {
			if (onConfirm == null) {
				onConfirm = () => {};
			}
			if (errors !== null && error == null) {
				let errorTxt = '<ul>';
				for (i = 0; i < errors.length; i++) {
					errorTxt += '<li>' + errors[i] + '</li>';
				}
				errorTxt += '</ul>';
				swal({
					title: '',
					type: 'error',
					html: errorTxt,
					confirmButtonColor: '#ff3860'
				}).then(result => {
					if (result) {
						onConfirm();
					}
				});
			} else if (error !== null && errors == null) {
				swal({
					title: '',
					type: 'error',
					text: error,
					confirmButtonColor: '#ff3860'
				}).then(result => {
					if (result) {
						onConfirm();
					}
				});
			}
		},
		swalWarning(warning, onConfirm, confirmationText) {
			if (onConfirm == null) {
				onConfirm = () => {};
			}

			if (warning == null) {
				warning = '';
			}

			let confirmText = confirmationText != null ? confirmationText : 'OK';

			swal({
				title: '',
				text: warning,
				type: 'warning',
				confirmButtonColor: '#ec4a0c',
				confirmButtonText: confirmText
			}).then(result => {
				onConfirm();
			});
		}
	}
});
