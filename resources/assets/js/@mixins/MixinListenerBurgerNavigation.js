const mixinListenerBurgerNavigation = {
  data() {
    return {
      stateNavbarToggle: false,
    };
  },

  mounted() {
    this.buttonBurgerListenerAnimation();
  },

  methods: {
    buttonCloseMenu() {
      this.iconBar[0].style.transform = 'translate3d(0, 0, 0) rotate(0deg)';
      this.iconBar[1].style.transform = 'translate3d(0, 0, 0) rotate(0deg)';
      this.iconBar[2].style.width = '11px';
      this.stateNavbarToggle = false;
    },
    buttonOpenMenu() {
      this.iconBar[0].style.transform = 'translate3d(0, 7px, 0) rotate(45deg)';
      this.iconBar[1].style.transform = 'translate3d(0, 1px, 0) rotate(-45deg)';
      this.iconBar[2].style.width = '0px';
      this.stateNavbarToggle = true;
    },
    buttonCheckMenu() {
      if (this.stateNavbarToggle) {
        this.buttonCloseMenu();
      } else {
        this.buttonOpenMenu();
      }
    },
    buttonBurgerListenerAnimation() {
      const targetNode = document.getElementsByClassName('navbar-collapse')[0];
      const config = { attributes: true, childList: true, subtree: true };
      const callback = (mutationsList, observer) => {
        for (let mutation of mutationsList) {
          if (mutation.type === 'attributes') {
            this.buttonCheckMenu();
          }
        }
      };
      const observer = new MutationObserver(callback);
      observer.observe(targetNode, config);
    },
  }
}

export default mixinListenerBurgerNavigation;