const MixinHideSBWidget = {
	data() {
		return {
			sbWidgetElement: null
		};
	},
	methods: {
		initSBWidgetElement() {
			this.sbWidgetElement = document.querySelector('#sb_widget') || null;
		},
		setSBWidgetHiddenStatus(hiddenValue) {
			const displayStyle = hiddenValue ? 'none' : 'unset';

			this.sbWidgetElement.style.display = displayStyle;
		}
	},
	async mounted() {
		try {
			await this.initSBWidgetElement();

			if (this.sbWidgetElement) this.setSBWidgetHiddenStatus(true);
		} catch (e) {}
	},
	async destroyed() {
		try {
			if (this.sbWidgetElement) await this.setSBWidgetHiddenStatus(false);
		} catch (e) {}
	}
};

export default MixinHideSBWidget;
