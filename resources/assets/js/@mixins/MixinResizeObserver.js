const mixinResizeObserver = {
	mounted() {
		if (window.attachEvent) {
			window.attachEvent('onresize', () => {
				this.handleResize();
			});
		} else if (window.addEventListener) {
			window.addEventListener('resize', () => {
				this.handleResize();
			});
		}
	}
};

export default mixinResizeObserver;
