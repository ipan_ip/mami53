const mixinAlertError = {
	methods: {
		showError(errors) {
			if (errors != undefined) {
				let errorTxt = '<ul style="text-align: left">';
				for (let i = 0; i < errors.length; i++) {
					errorTxt += '<li>' + errors[i] + '</li>';
				}
				errorTxt += '</ul>';
				swal({
					title: '',
					type: 'error',
					html: errorTxt
				});
			}
		}
	}
};

export default mixinAlertError;