export default {
	data() {
		return {
			pageTopPosition: 0
		};
	},
	methods: {
		updateBodyToBeFixed(status) {
			if (!status) {
				const top = window.scrollY;
				this.pageTopPosition = top;
				document.body.style.position = 'fixed';
				document.body.style.top = `-${top}px`;
				document.body.style.width = '100vw';
			} else {
				document.body.style.position = 'initial';
				document.body.style.top = '';
				document.body.style.width = 'auto';
				window.scrollTo(0, this.pageTopPosition);
			}
		}
	}
};
