const mixinKostLevel = {
	created() {
		this.getKostLevel();
	},
	methods: {
		getKostLevel() {
			this.isLoading = true;
			axios
				.get('/config/general')
				.then(response => {
					this.isLoading = false;

					this.tagKostLevel = response.data['kost-level'].list;

					this.isCampaignInternal =
						response.data['kost-level'].campaign.internal;

					this.isLoading = false;
				})
				.catch(error => {
					bugsnagClient.notify(error);
				});
		}
	}
};

export default mixinKostLevel;
