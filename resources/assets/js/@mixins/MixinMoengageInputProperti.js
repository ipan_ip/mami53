const mixinMoengageInputProperti = {
	methods: {
		moengageInputPropertiEvent(propertyType, inputType, params) {
			if (propertyType == 'Kost') {
				tracker('moe', [
					'[Owner] Add Kost',
					{
						input_type: inputType,
						kost_id: 0,
						property_name: params.name,
						available_room: params.room_available,
						property_address: params.address,
						property_gender: params.gender,
						area_size: params.room_size,
						building_year: params.building_year,
						room_count: params.room_count,
						price_monthly: params.price_monthly,
						price_daily: params.price_daily,
						price_weekly: params.price_weekly,
						price_yearly: params.price_yearly,
						kost_facilities: params.fac_property,
						room_facilities: params.fac_room,
						bath_facilities: params.fac_bath,
						area_city: params.city,
						area_subdistrict: params.subdistrict
					}
				]);
			} else if (propertyType == 'Apartment') {
				tracker('moe', [
					'[Owner] Add Apartment',
					{
						input_type: inputType,
						apartment_id: 0,
						property_name: params.unit_name,
						room_facilities: params.fac_room,
						room_isfurnished: params.is_furnished,
						unit_type: params.unit_type,
						unit_facilities: params.fac_unit,
						price_monthly: params.price_monthly,
						price_daily: params.price_daily,
						area_city: null,
						area_subdistrict: null
					}
				]);
			} else if (propertyType == 'Jobs') {
				tracker('moe', [
					'[Owner] Add Jobs',
					{
						input_type: inputType,
						industry_id: 0,
						company_name: params.place_name,
						job_address: params.address,
						jobs_last_education: params.last_education,
						jobs_position: params.jobs_name,
						jobs_min_salary: params.jobs_salary,
						jobs_max_salary: params.jobs_max_salary,
						jobs_type: params.jobs_type,
						jobs_salary_time: params.jobs_salary_time,
						job_expired_date: params.expired_date,
						job_workplace: params.workplace,
						job_city: params.city,
						job_subdistrict: params.subdistrict
					}
				]);
			}
		}
	}
};

export default mixinMoengageInputProperti;
