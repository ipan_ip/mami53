Vue.mixin({
	mounted() {
		try {
			$(document).ready(function() {
				$('[data-toggle="tooltip"]').tooltip();
				$('[data-toggle="popover"]').popover();
			});
		} catch (e) {}
	}
});
