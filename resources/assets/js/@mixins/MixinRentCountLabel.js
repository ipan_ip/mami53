const mixinRentCountLabel = {
	computed: {
		userKost() {
			return this.$store.state.userKost || null;
		},
		bookingDetail() {
			return this.$store.state.bookingDetail || null;
		},
		priceLabel() {
			let rentCount = 'monthly';
			if (this.userKost !== null) {
				rentCount = this.userKost.booking_data.rent_count_type;
			} else if (this.bookingDetail !== null) {
				rentCount = this.bookingDetail.booking_data.rent_count_type;
			}

			if (rentCount == 'weekly') {
				return 'mingguan';
			} else if (rentCount == 'monthly') {
				return 'bulanan';
			} else if (rentCount == 'quarterly') {
				return '3 bulanan';
			} else if (rentCount == 'semiannually') {
				return '6 bulanan';
			} else if (rentCount == 'yearly') {
				return 'tahunan';
			}
		}
	}
};

export default mixinRentCountLabel;
