const mixinTrackingClass = {
	methods: {
		applyTrackingClass(selectorName, className) {
			try {
				const elements = document.querySelectorAll(selectorName);

				if (elements && elements.length > 0) {
					for (let element of elements) {
						element.classList.add(className);
					}
				}
			} catch (e) {}
		}
	}
};

export default mixinTrackingClass;
