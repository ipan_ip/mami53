const mixinUploadImage = {
	data() {
		return {
			loadedImage: null,
			isLoadingImage: false,
			isLoadingImageFailed: false
		};
	},
	methods: {
		/**
		 * A handler method for selecting image file from user's device and set the
		 * value to 'loadedImage' in data property.
		 * e.g this.uploadImage(event, 8000000, 1920)
		 *
		 * @param {Object} event Default event parameter obtained from 'input'
		 * element event listener.
		 * @param {Number} maxSize Custom maximum file size of the image (in bytes).
		 * @param {Number} resizeWidth Custom new image width (without px) used for
		 * scaled resize.
		 */
		uploadImage(event, maxSize, resizeWidth) {
			const imageFile = event.target.files[0];

			this.isLoadingImageFailed = false;
			this.isLoadingImage = true;

			// Start reading the file when its type is an actual image.
			if (imageFile && imageFile.type.includes('image')) {
				// Create FileReader object.
				const fileReader = new FileReader();

				// Get file's base64 Data URI.
				fileReader.readAsDataURL(imageFile);

				// Listen to fileReader's 'load' event.
				fileReader.addEventListener('load', readerEvent => {
					// Get file's base64 Data URI.
					const readerResult = readerEvent.target.result;

					// Resize image if maxSize is provided and exceeded by imageFile size value.
					if (imageFile.size >= maxSize)
						this.resizeImage(readerResult, resizeWidth);
					else {
						this.isLoadingImage = false;
						this.loadedImage = readerResult;
					}
				});
			} else {
				this.isLoadingImageFailed = true;
				this.isLoadingImage = false;
			}
		},
		/**
		 * This method is used for resizing an image and set the value to
		 * 'loadedImage' in data property.
		 *
		 * @param {base64} source base64 image Data URI
		 * @param {Number} resizeWidth Custom new image width for scaled resize
		 * (default: 1024px)
		 */
		resizeImage(source, resizeWidth = 1024) {
			if (!this.isLoadingImage) this.isLoadingImage = true;

			// Create Image object.
			const image = new Image();

			// Set image's src property to source value.
			image.src = source;

			// Listen to image's 'load' event.
			image.addEventListener('load', imageEvent => {
				// Create canvas element.
				const canvasElement = document.createElement('canvas');

				// Scale the image to resizeWidth value and keep the aspect ratio.
				const scaleFactor = resizeWidth / imageEvent.target.width;

				// Set canvas size to the new desired size.
				canvasElement.width = resizeWidth;
				canvasElement.height = imageEvent.target.height * scaleFactor;

				// Draw image in canvas.
				const canvasContext = canvasElement.getContext('2d');
				canvasContext.drawImage(
					imageEvent.target,
					0,
					0,
					canvasElement.width,
					canvasElement.height
				);

				// Get the base64-encoded Data URI from the resized image.
				this.loadedImage = canvasContext.canvas.toDataURL(
					imageEvent.target,
					'image/png',
					0
				);

				this.isLoadingImage = false;
			});
		}
	}
};

export default mixinUploadImage;
