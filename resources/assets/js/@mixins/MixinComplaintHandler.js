export default {
	components: {
		ComplaintModal: () => import('Js/@components/complaint-form/ComplaintModal')
	},

	data() {
		return {
			isComplaintModalLoaded: false,
			isComplaintModalOpened: false
		};
	},

	methods: {
		openComplaintModal() {
			this.isComplaintModalLoaded = true;
			this.isComplaintModalOpened = true;
		}
	}
};
