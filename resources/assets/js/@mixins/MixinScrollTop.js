const mixinScrollTop = {
	created() {
		window.scrollTo(0, 0);
	}
};

export default mixinScrollTop;
