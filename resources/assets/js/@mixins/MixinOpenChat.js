const mixinOpenChat = {
	methods: {
		openChat() {
			try {
				document.querySelector('#sb_widget .widget').click();
			} catch (e) {
				console.error(e);
			}
		},
		openGroupChannel(channelUrl) {
			if (typeof sbWidget == 'object') {
				sbWidget.openChatRoom(channelUrl);
			}
		}
	}
};

export default mixinOpenChat;
