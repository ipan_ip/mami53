const mixinBigCities = {
	data() {
		return {
			bigCitiesSrc: [
				{
					title: 'JOGJAKARTA',
					img: '/general/img/pictures/landmarks/kota_jogjakarta.png',
					url: '/kost/kost-jogja-murah'
				},
				{
					title: 'JABODETABEK',
					img: '/general/img/pictures/landmarks/kota_jabodetabek.png',
					url: '/kost/kost-jakarta-murah'
				},
				{
					title: 'SURABAYA',
					img: '/general/img/pictures/landmarks/kota_surabaya.png',
					url: '/kost/kost-surabaya-murah'
				},
				{
					title: 'BANDUNG',
					img: '/general/img/pictures/landmarks/kota_bandung.png',
					url: '/kost/kost-bandung-murah'
				},
				{
					title: 'MALANG',
					img: '/general/img/pictures/landmarks/kota_malang.png',
					url: '/kost/kost-malang-murah'
				},
				{
					title: 'SEMARANG',
					img: '/general/img/pictures/landmarks/kota_semarang.png',
					url: '/kost/kost-semarang-murah'
				},
				{
					title: 'BALI',
					img: '/general/img/pictures/landmarks/kota_bali.png',
					url: '/kost/kost-denpasar-bali-murah'
				},
				{
					title: 'MAKASSAR',
					img: '/general/img/pictures/landmarks/kota_makassar.png',
					url: '/kost/kost-makassar-murah'
				},
				{
					title: 'MEDAN',
					img: '/general/img/pictures/landmarks/kota_medan.png',
					url: '/kost/kost-medan-murah'
				}
			]
		};
	},

	methods: {
		handleClickCity(city, index) {
			tracker('moe', [
				'[User] Homepage City Click',
				{
					city_name: city.title,
					city_order: index
				}
			]);

			window.location.href = city.url;
		}
	},

	computed: {
		bigCities() {
			return this.bigCitiesSrc
				.map(a => [Math.random(), a])
				.sort((a, b) => a[0] - b[0])
				.map(a => a[1]);
		}
	}
};

export default mixinBigCities;
