require('../starter2');

window.debounce = require('lodash/debounce');

window.Vue = require('vue').default;

import VueRouter from 'vue-router';
Vue.use(VueRouter);

import Vuex from 'vuex';
Vue.use(Vuex);

import VueLazyload from 'vue-lazyload';
Vue.use(VueLazyload, {
	preLoad: 1.3,
	error: '/assets/loading/mamikos_placeholder_load.svg',
	loading: '/assets/loading/mamikos_placeholder_load.svg',
	attempt: 2
});

import infiniteScroll from 'vue-infinite-scroll';
Vue.use(infiniteScroll);

// Global Mixins
import mixinAuth from 'Js/@mixins/MixinAuth';

require('Js/@mixins/MixinScrollTop');
require('Js/@mixins/MixinSwalLoading');
require('Js/@mixins/MixinSwalSuccessError');
require('Js/@mixins/MixinInitBootstrapJs');

const App = require('./components/forum-user/App.vue').default;

// Container Component
Vue.component('app', App);

// Route Components
const viewForumHome = require('./components/forum-user/ViewForumHome.vue')
	.default;
const viewForumLanding = require('./components/forum-user/ViewForumLanding.vue')
	.default;
const viewForumDetail = require('./components/forum-user/ViewForumDetail.vue')
	.default;

const routes = [
	{
		path: '/',
		name: 'home',
		component: viewForumHome
	},
	{
		path: '/:category',
		name: 'landing',
		component: viewForumLanding
	},
	{
		path: '/:category/:thread',
		name: 'detail',
		component: viewForumDetail
	}
];

const router = new VueRouter({
	mode: 'history',
	base: '/forum',
	routes
});

const store = new Vuex.Store({
	state: {
		token: document.head.querySelector('meta[name="csrf-token"]').content,
		authCheck: {},
		authData: {},
		userData: {},
		params: {
			category: {},
			question: '',
			answer: '',
			report_answer: '',
			report_thread: ''
		},
		extras: {
			shareBtnShow: false
		}
	},
	mutations: {
		setAuthCheck(state, payload) {
			state.authCheck = payload;
		},
		setAuthData(state, payload) {
			state.authData = payload;
		},
		setUserData(state, payload) {
			state.userData = payload;
		},
		setParamsCategory(state, payload) {
			state.params.category = payload;
		},
		setParamsQuestion(state, payload) {
			state.params.question = payload;
		},
		setParamsAnswer(state, payload) {
			state.params.answer = payload;
		},
		setParamsReportAnswer(state, payload) {
			state.params.report_answer = payload;
		},
		setParamsReportThread(state, payload) {
			state.params.report_thread = payload;
		},
		toggleShareBtnShow(state, payload) {
			state.extras.shareBtnShow = payload;
		}
	}
});

Vue.config.productionTip = false;

const app = new Vue({
	el: '#app',
	mixins: [mixinAuth],
	router,
	store,
	created() {
		this.$store.commit('setUserData', forumUser);
	}
});
