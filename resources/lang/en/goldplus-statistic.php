<?php
return [
    'error' => [
        'runtime_exception' => [
            'user_id_was_null'          => 'User Id was accidentaly null!',
            'initiate_object_was_null'  => 'Initiate object was null!',
            'url_statistic_option_not_valid'    => 'Expected RE : URL of statistic option does not valid!',
            'invalid_song_id'            => 'Invalid song id!',
            'invalid_kost_level_id'      => 'Invalid kost level id!',
            'invalid_uri_param'          => 'Invalid uri param for detail kost GP!',
            'goldplus_statistic_ids_was_null' => 'GOLDPLUS_STATISTIC_IDS value accidentally was null!',
        ],
        'filter' => [
            'filter_not_valid'      => 'Filter does not valid!',
            'report_type_not_valid' => 'Report type does not valid',
            'required'              => 'Filter list required',
            'selected_filter_not_valid' => 'The selected filter list is invalid'
        ],
    ],
    'label' => [
        'tooltip' => [
            'growth' => [
                'chat_growth_up'    => 'Chat masuk naik :growth%',
                'chat_growth_down'  => 'Chat masuk turun :growth%',
                'visit_growth_up'   => 'Kunjungan naik :growth%',
                'visit_growth_down' => 'Kunjungan turun :growth%',
                'unique_visit_growth_up'    => 'Pengunjung naik :growth%',
                'unique_visit_growth_down'  => 'Pengunjung turun :growth%',
                'favorite_growth_up'        => 'Peminat kos naik :growth%',
                'favorite_growth_down'      => 'Peminat kos turun :growth%',   
            ],
            'exact_value' => [
                'chat'          => ':value Chat masuk',
                'visit'         => ':value Kunjungan',
                'unique_visit'  => ':value Pengunjung',
                'favorite'      => ':value Peminat Kos',
            ],
        ]
    ],
    'validation' => [
        'error' => [
            'room_not_exists' => 'Room does not exists',
            'kost_not_exists'   => 'Kost not found.'
        ]
        ],
    'badge' => [
        'gp1' => 'Goldplus 1',
        'gp2' => 'Goldplus 2',
        'gp3' => 'Goldplus 3',
        'gp4' => 'Goldplus 4',
    ]
];