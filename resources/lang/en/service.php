<?php

return [
    'thanos-request-booking' => [
        'error' => [
            'runtime-exception' => [
                'file-not-found' => 'File sumber tidak ditemukan. Mohon cek kembali file path yang Anda inputkan!',
                'file-not-valid' => 'File sumber tidak valid.',
            ],
            'empty-param-data' => 'Data log tidak ditemukan'
        ],
    ],
];
