<?php

use App\Enums\Activity\ActivationCodeVia;

return [
    ActivationCodeVia::class => [
        ActivationCodeVia::SMS => 'SMS',
        ActivationCodeVia::WHATSAPP => 'WhatsApp',
    ],
];
