<?php

use App\Entities\Activity\ActivationCodeType;

return [
    'description' => [
        ActivationCodeType::OWNER_VERIFICATION => 'Owner Registration',
        ActivationCodeType::OWNER_EDIT_PHONE_NUMBER => 'Owner Edit Phone Number',
        ActivationCodeType::OWNER_FORGET_PASSWORD => 'Owner Forget Password',
        ActivationCodeType::TENANT_VERIFICATION => 'Tenant Registration',
        ActivationCodeType::TENANT_FORGET_PASSWORD => 'Tenant Forget Password',
        ActivationCodeType::REGISTRATION => 'Registration',
        ActivationCodeType::LOGIN => 'Login',
        ActivationCodeType::VERIFICATION => 'User Verification',
        ActivationCodeType::MAMIPAY_ACTIVATION => 'Mamipay Activation',
        ActivationCodeType::CHANGE_PHONE_NUMBER => 'Tenant Phone Verification',
        ActivationCodeType::USER_UPDATE => 'User Update Profile',
        ActivationCodeType::USER_VERIFICATION => 'User Verification',
    ]
];
