<?php

return [
    'owner' => [
        'add_tenant_from_link' => [
            'onboarding'    => 'Jangan lewatkan kesempatan mendapat MamiPoin ekstra yang bisa Anda tukarkan dengan hadiah menarik dari Mamikos.',
            'help_link'     => 'https://help.mamikos.com',
        ]
    ],
    'tenant' => [
        'add_tenant_from_link' => [
            'onboarding'    => 'Dengan mengisi dan melengkapi data, kamu berkesempatan mendapatkan promo-promo di Mamikos, khusus untuk penyewa.',
            'privacy_link'  => 'https://mamikos.com/privacy',
        ]
    ]
];