<?php

return [
    'message' => '<#> Kode verifikasi Mamikos Anda adalah :code. ' .
        'Harap tidak memberitahukan kode ini kepada siapapun. ' .
        'Karyawan Mamikos tidak pernah meminta kode verifikasi. ' .
        config('api.app-identifier.android-hash')
];
