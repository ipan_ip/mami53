<?php

return [
    'email-not-valid'       => 'Email not valid',
    'date-not-valie'        => 'Date not valid',
    'verify-link-not-valid' => 'Verify link not valid',
    'user-not-valid'        => 'User not valid',
    'email-does-not-exist'  => 'Email does not exist',
    'user-does-not-exist'  => 'Email does not exist',
];