<?php

$assetGpOnboardingUrl = '/assets/icons/goldplus/onboarding/';

return array(

  /*
  |--------------------------------------------------------------------------
  | API Language Lines
  |--------------------------------------------------------------------------
  |
  | The following language lines are used by the API
  |
  */

  'authorization' => array(
    'time_header_required'          => 'Authorization: X-GIT-Time Header Required',
    'authorization_header_invalid'  => 'Authorization: Authorization Header not Valid',
    'access_token_invalid'          => 'Authorization: Signature Invalid',
    'authorization_header_required' => 'Authorization: Authorization Header is required.',
    'token_invalid'                 => 'Authorization: Guest / User Token Invalid',
    'access_token_expired'          => 'Authorization: Access Token Expired',
    'signin_required'					      => 'Authorization: Sign-in is required.',
    'unauthenticated'               => 'Silakan login terlebih dahulu',
  ),

  'auth' => array(
    'phone_registered' => 'Phone number already registered.',
    'owner' => [
      'phone_number' => [
        'required' => 'Nomor HP harus diisi',
        'invalid' => 'Nomor HP dan password tidak sesuai',
        'unverified' => 'Nomor HP anda belum terverifikasi',
        'legacy' => [
            'forget_password' => 'Nomor tidak ditemukan',
        ],
      ],
      'email' => [
        'invalid' => 'Email dan password tidak sesuai',
        'forbidden' => 'Silahkan gunakan nomor HP',
      ],
    ],
    'tenant' => [
      'phone_number' => [
        'invalid' => 'Nomor HP dan password tidak sesuai',
        'forbidden' => 'Nomor HP dan password tidak sesuai, silakan login menggunakan gmail atau FB',
      ],
      'email' => [
        'invalid' => 'Email dan password tidak sesuai',
        'forbidden' => 'Email dan password tidak sesuai, silakan login menggunakan gmail atau FB',
      ]
    ],
    'verification' => [
      'code' => [
        'too_frequent' => 'Mohon tunggu :seconds detik lagi untuk kirim ulang kode verifikasi.',
      ],
    ],
  ),

  'apple-auth' => [
    'exception' => [
      'empty-client-or-identity-token' => 'Client user or identity token was empty',
    ],
    'not-verified-user'     => 'Not verified user',
    'authentication-failed' => 'Gagal autentifikasi, silahkan coba lagi!'
  ],

  'owner' => [
    'verification' => [
      'code' => [
        'too_frequent'  => 'Tunggu :minutes menit untuk kirim ulang kode.',
        'send_failed'   => 'Kode verifikasi gagal dikirim',
        'success'       => 'Success',
        'invalid'       => 'Kode tidak valid, harap periksa kembali.',
      ]
    ],
    'exception' => [
      'not-owned' => ':user tidak memiliki akses di :resource ini',
      'not-found' => ':resource ini tidak ditemukan'
    ],
    'input' => [
      'kost' => [
        'validation' => [
          'price' => [
            'lower-than-discount' => [
              \App\Entities\Room\Element\Price::STRING_DAILY => 'Harga harian yang dimasukkan terlalu rendah dari diskon',
              \App\Entities\Room\Element\Price::STRING_WEEKLY => 'Harga mingguan yang dimasukkan terlalu rendah dari diskon',
              \App\Entities\Room\Element\Price::STRING_MONTHLY => 'Harga bulanan yang dimasukkan terlalu rendah dari diskon',
              \App\Entities\Room\Element\Price::STRING_QUARTERLY => 'Harga 3 bulanan yang dimasukkan terlalu rendah dari diskon',
              \App\Entities\Room\Element\Price::STRING_SEMIANNUALLY => 'Harga 6 bulanan yang dimasukkan terlalu rendah dari diskon',
              \App\Entities\Room\Element\Price::STRING_ANNUALLY => 'Harga tahunan yang dimasukkan terlalu rendah dari diskon',
              \App\Entities\Room\Element\Price::STRING_YEARLY => 'Harga tahunan yang dimasukkan terlalu rendah dari diskon'
            ],
            'flash-sale' => [
              \App\Entities\Room\Element\Price::STRING_DAILY => 'Promo ngebut aktif, tidak bisa update harga harian',
              \App\Entities\Room\Element\Price::STRING_WEEKLY => 'Promo ngebut aktif, tidak bisa update harga mingguan',
              \App\Entities\Room\Element\Price::STRING_MONTHLY => 'Promo ngebut aktif, tidak bisa update harga bulanan',
              \App\Entities\Room\Element\Price::STRING_QUARTERLY => 'Promo ngebut aktif, tidak bisa update harga 3 bulanan',
              \App\Entities\Room\Element\Price::STRING_SEMIANNUALLY => 'Promo ngebut aktif, tidak bisa update harga 6 bulanan',
              \App\Entities\Room\Element\Price::STRING_ANNUALLY => 'Promo ngebut aktif, tidak bisa update harga tahunan',
              \App\Entities\Room\Element\Price::STRING_YEARLY => 'Promo ngebut aktif, tidak bisa update harga tahunan'
            ]
          ]
        ]
      ]
    ]
  ],

  'tenant' => [
    'verification' => [
      'code' => [
        'too_frequent'  => 'Tunggu :minutes menit untuk kirim ulang kode.',
        'send_failed'   => 'Kode verifikasi gagal dikirim',
        'success'       => 'Success',
        'invalid'       => 'Kode tidak valid, harap periksa kembali.',
      ],
      'email' => [
        'exists' => 'Email sudah digunakan oleh akun lain',
      ]
    ],
      'error' => [
        'not_found' => 'User data tidak ditemukan',
    ]
  ],

  'input' => array(
    'required'           => 'Input: POST input can\'t be empty',
    'json_decode_failed' => 'Input: POST input can\'t be decoded, your input must be in json format',
    'change_password' => [
      'tenant' => [
        'old_password' => [
          'invalid' => 'Password lama tidak valid'
        ],
        'new_password' => [
          'unchanged' => 'Password tidak boleh sama',
          'string' => 'Gunakan kombinasi huruf dan angka'
        ]
      ]
    ],
    'forget_password' => [
      'tenant' => [
        'duplicate' => 'Tidak menerima kode OTP? Silakan hubungi CS Mamikos: 0813-2511-1171 (Whatsapp Only)',
      ],
      'owner' => [
        'duplicate' => 'Tidak menerima kode OTP? Silakan hubungi CS Mamikos: 0813-2511-1171 (Whatsapp Only)',
      ],
      'wrong_section' => 'Nomor handphone Anda tidak terdaftar. Pastikan jenis akun yang Anda pilih sebelumnya (Pemilik Kos/Pencari Kos) sudah sesuai.',
      'not_found' => 'Nomor handphone Anda tidak terdaftar.',
      'send_failed' => 'Pengiriman kode verifikasi gagal',
      'social' => 'Anda belum mendaftarkan nomor handphone Anda, atau Anda login menggunakan Google, Facebook, atau Apple ID.',
      'failed' => 'Verifikasi gagal',
      'duplicate' => [
        'message' => 'Nomor HP ini sudah digunakan untuk verifikasi di akun lain.',
        'cta_message' => 'Mohon hubungi CS Mamikos.',
        'wa_number' => '081325111171',
        'pretext' => 'Halo, nomor handphone/email saya :identifier sudah pernah digunakan untuk verifikasi di akun lain. Mohon bantuannya.'
      ]
    ],
    'email' => [
      'required' => 'Mohon masukkan email',
      'required_without' => 'Mohon masukkan email atau nomor HP',
      'email' => 'Mohon masukkan email yang valid', // Error message for email format
      'format' => 'Gunakan format email seperti: mami@mamikos.com',
      'unique' => 'Email ini sudah terdaftar',
      'exists' => 'Mohon masukkan email yang terdaftar',
      'owner_not_found' => 'Alamat email tidak terdaftar sebagai pemilik kos',
      'tenant_not_found' => 'Alamat email tidak terdaftar sebagai pencari kos',
      'social' => 'Alamat email tidak terdaftar',
    ],
    'gender' => [
      'required' => 'Mohon masukkan jenis kelamin',
      'string' => 'Mohon masukkan jenis kelamin yang valid',
      'in' => 'Mohon masukkan jenis kelamin yang valid'
    ],
    'occupation' => [
      'required' => 'Mohon masukkan okupasi',
      'string' => 'Mohon masukkan okupasi yang valid',
      'in' => 'Mohon masukkan okupasi yang valid'
    ],
    'marital_status' => [
      'required' => 'Mohon masukkan status',
      'string' => 'Mohon masukkan status yang valid',
      'in' => 'Mohon masukkan status yang valid',
    ],
    'parent_phone_number' => [
      'required_if' => 'Mohon masukkan nomor',
      'regex' => 'Mohon diawali dengan 08',
      'min' => 'Mohon masukkan nomor HP yang valid',
      'max' => 'Mohon masukkan nomor HP yang valid',
      'invalid' => 'Mohon masukkan nomor HP yang valid',
    ],
    'parent_name' => [
      'required_if' => 'Nama tidak boleh kosong',
      'alpha' => 'Mohon masukkan karakter alfabet',
      'min' => 'Minimal :min karakter',
      'max' => 'Maksimal :max karakter',
    ],
    'phone_number' => [
      'required' => 'Masukkan nomor handphone.',
      'required_without' => 'Mohon masukkan email atau nomor HP.',
      'regex' => 'Nomor handphone harus diawali dengan 08.',
      'min' => 'Nomor handphone kurang dari :min karakter.',
      'max' => 'Nomor handphone lebih dari :max karakter.',
      'invalid' => 'Mohon masukkan nomor HP yang valid',
      'unique' => 'Nomor yang Anda masukkan tidak valid atau sudah terdaftar.',
      'exists' => 'Masukkan nomor handphone yang terdaftar.',
      'owner_not_found' => 'Nomor HP tidak terdaftar sebagai pemilik kos.',
      'tenant_not_found' => 'Nomor HP tidak terdaftar sebagai pencari kos.',
    ],
    'code' => [
      'required' => 'Kode verifikasi harus diisi'
    ],
    'password' => [
      'required' => 'Password tidak boleh kosong.',
      'string' => 'Gunakan kombinasi huruf dan angka.',
      'min' => 'Password kurang dari :min karakter',
      'max' => 'Password maksimal :max karakter',
      'password.min' => 'Password minimal :min karakter',
      'confirmed' => 'Konfirmasi password tidak sama.',
    ],
    'name' => [
      'required' => 'Nama tidak boleh kosong',
      'alpha' => 'Mohon masukkan karakter alfabet',
      'min' => 'Minimal :min karakter',
      'max' => 'Maksimal :max karakter',
    ],
    'available_room' => [
      'required' => 'Ketersediaan Ruangan harus diisi',
      'numeric' => 'Jumlah kamar kosong harus diisi'
    ],
    'room_number' => [
      'required' => 'Nomor kamar wajib diisi',
      'string' => 'Nomor kamar tidak valid',
      'min' => 'Nomor kamar tidak boleh kurang dari :min karakter',
      'max' => 'Nomor kamar tidak boleh lebih dari :max karakter',
    ],
    'start_date' => [
      'required' => 'Tanggal masuk wajib diisi',
      'date_format' => 'Tanggal masuk harus dalam format Y-m-d',
    ],
    'rent_type' => [
      'required' => 'Hitungan sewa wajib diisi',
      'string' => 'Hitungan sewa tidak valid',
      'in' =>  'Hitungan sewa tidak valid',
    ],
    'amount' => [
      'required' => 'Harga sewa wajib diisi',
      'numeric' => 'Harga sewa harus berisi angka',
      'min' => 'Harga sewa tidak boleh kurang dari :min',
    ],
    'duration' => [
      'required' => 'Durasi sewa wajib diisi',
      'integer' => 'Durasi sewa harus berisi angka',
      'min' => 'Durasi sewa tidak boleh kurang dari :min hari',
    ],
    'fine_amount' => [
      'numeric' => 'Biaya denda harus berisi angka',
    ],
    'fine_maximum_length' => [
      'integer' => 'Durasi denda harus berisi angka',
    ],
    'fine_duration_type' => [
      'string' => 'Unit durasi denda tidak valid',
      'in' => 'Unit durasi denda tidak valid',
    ],
    'additional_costs' => [
      'array' => 'Biaya lain tidak valid',
    ],
    'additional_costs.*.field_title' => [
      'required' => 'Nama biaya wajib diisi',
      'string' => 'Nama biaya tidak valid',
      'min' => 'Nama biaya tidak boleh kurang dari :min karakter',
      'max' => 'Nama biaya tidak boleh lebih dari :max karakter',
    ],
    'additional_costs.*.field_value' => [
      'required' => 'Jumlah biaya wajib diisi',
      'numeric' => 'Jumlah biaya harus berisi angka',
    ],
    'additional_costs.*.cost_type' => [
      'required' => 'Jenis biaya wajib diisi',
      'string' => 'Jenis biaya tidak valid',
      'in' => 'Jenis biaya tidak valid'
    ],
    'price_yearly' => [
      'numeric' => 'Harga Sewa Tahunan harus berupa angka',
      'digit_min' => 'Harga Sewa Tahunan harus minimal :digit karakter',
      'required_without_all' => 'Harga sewa harus diisi salah satu',
    ],
    'price_monthly' => [
      'numeric' => 'Harga Sewa Bulanan harus berupa angka',
      'digit_min' => 'Harga Sewa Bulanan harus minimal :digit karakter',
      'required_without_all' => 'Harga sewa harus diisi salah satu',
    ],
    'price_weekly' => [
      'numeric' => 'Harga Sewa Mingguan harus berupa angka',
      'digit_min' => 'Harga Sewa Mingguan harus minimal :digit karakter',
      'required_without_all' => 'Harga sewa harus diisi salah satu',
    ],
    'price_daily' => [
      'numeric' => 'Harga Sewa Harian harus berupa angka',
      'digit_min' => 'Harga Sewa Harian harus minimal :digit karakter',
      'required_without_all' => 'Harga sewa harus diisi salah satu',
    ],
    'price_3_month' => [
      'numeric' => 'Harga Sewa 3 Bulanan harus berupa angka',
      'digit_min' => 'Harga Sewa 3 Bulanan harus minimal :digit karakter',
      'required_without_all' => 'Harga sewa harus diisi salah satu',
    ],
    'price_6_month' => [
      'numeric' => 'Harga Sewa 6 Bulanan harus berupa angka',
      'digit_min' => 'Harga Sewa 6 Bulanan harus minimal :digit karakter',
      'required_without_all' => 'Harga sewa harus diisi salah satu',
    ],
    'tenant' => [
      'active' => [
        'title' => 'Penyewa telah memiliki kontrak',
        'message' => 'Kontrak Baru hanya diperuntukan untuk penyewa yang belum memiliki kontrak sebelumnya.'
      ]
    ],
    'invoice_id' => [
      'required' => 'invoice_id field is required'
    ],
    'cost_value' => [
      'required' => 'cost_value field is required'
    ],
    'cost_title' => [
      'required' => 'cost_title field is required'
    ],
    'cost_type' => [
      'required' => 'cost_type field is required'
    ],
    'remarks_note' => [
      'max' => 'Maksimal :max karakter',
    ],
  ),

  'request' => [
    'success' => 'Request successful, entity corresponding to the request has been sent',
  ],

  'access' => array(
    'forbidden' => 'Disallowed to run this resource.',
  ),

  'exception' => array(
    'not_found' => 'Route: Resource Not Found, it must be mistype in URL.',
  ),

  'recaptcha' => [
    'please_complete_recaptcha'   => 'Please complete recaptcha input!',
    'validation_rules_not_valid'  => 'Violation rules does not valid!',
  ],

  'transaction' => [
    'fail' => 'Gagal menyimpan data ke database'
  ],
  'auto_bbk' => [
    'error' => [
      'user_not_exists' => 'Data user tidak ditemukan.',
      'invalid_autobbk_activation' => 'Anda bukan atau belum menjadi user Mamipay.',
    ]
  ],
  'mamipay' => [
    'owner' => [
      'error' => [
        'no_property_owned' => 'Owner :phoneNumber tidak memiliki properti',
        'not_registered' => 'Owner :phoneNumber belum terdaftar sebagai MamiPAY Owner',
        'add_failed' => 'Failed to add mamipay owner',
        'add_unknown' => 'Something when wrong when adding mamipay owner',
      ],
      'success' => [
        'registered' => 'Owner telah terdaftar sebagai MamiPAY Owner',
        'add_success' => 'Success to add mamipay owner',
      ],
    ],
  ],
  'room' => [
    'duplicate' => [
      'error' => [
        'more_than_limit' => 'Tidak boleh duplikat kost lebih dari :limit',
        'less_than_limit' => 'Tidak boleh duplikat kost kurang dari :limit',
      ],
    ],
  ],
  'user-verification' => [
    'options' => [
      App\Entities\User\UserMedia::TYPE_E_KTP => [
        'title' => 'KTP',
      ],
      App\Entities\User\UserMedia::TYPE_SIM => [
        'title' => 'SIM',
      ],
      App\Entities\User\UserMedia::TYPE_PASSPORT => [
        'title' => 'Paspor',
      ],
      App\Entities\User\UserMedia::TYPE_KK => [
        'title' => 'Kartu Keluarga',
        'subtitle' => 'KK',
      ],
      App\Entities\User\UserMedia::TYPE_KTM => [
        'title' => 'Kartu Tanda Mahasiswa',
        'subtitle' => 'KTM',
      ],
    ],
  ],
  'goldplus' => [
    'filters' => [
      config('kostlevel.id.goldplus1') => [
        'value' => 'Goldplus 1'
      ],
      config('kostlevel.id.goldplus2') => [
          'value' => 'Goldplus 2'
      ],
      config('kostlevel.id.goldplus3') => [
          'value' => 'Goldplus 3'
      ],
      config('kostlevel.id.goldplus4') => [
          'value' => 'Goldplus 4'
      ],
    ],
    'filter-all' => [
      'key' => 0,
      'value' => 'Semua',
    ],
    'success' => [
      'regenerate-report' => 'Regenerating report (:roomId, :roomName) is being processed',
    ],
    'error' => [
      'regenerate-report' => 'Regenerating report is failed. :message',
      'status' => 'The selected Status is invalid.'
    ],
    'action-code' => [
      \App\Enums\GoldPlus\AdminActionCode::REGENERATE_REPORT => [
        'message' => 'Report re-generated',
      ],
    ],
    'widget' => [
      'label' => [
        'active-goldplus' => 'Properti Terdaftar GoldPlus',
        'in-review-goldplus' => 'Sedang di-review',
      ],
    ],
    'onboarding' => [
      'title' => 'Manfaat Maksimal GoldPlus',
      'items' => [
        [
          'title' => 'Listing Kos Diprioritaskan (GP3)',
          'body' => 'Dapatkan kunjungan lebih banyak. Kos Anda dapat langsung disewa online melalui aplikasi dan website Mamikos.'
        ],
        [
          'title' => 'Kelola Kos Digital',
          'body' => 'Atur usaha kos Anda, dari pemasaran, booking, hingga pembayaran sewa, dari mana saja dan kapan saja.'
        ],
        [
          'title' => 'Pengingat Tagihan Otomatis',
          'body' => 'Sistem Mamikos membantu mengingatkan pembayaran sewa lewat WhatsApp, Email, dan SMS.'
        ],
        [
          'title' => 'Laporan Keuangan Real-Time',
          'body' => 'Monitor pemasukan, pengeluaran, hingga kinerja kunjungan iklan Anda secara real-time di aplikasi/website.'
        ],
        [
          'title' => 'Layanan Tambahan',
          'body' => 'Dapatkan akses dan harga spesial untuk layanan asuransi, Foto & Video Profesional, dan jasa pemeliharaan kos.'
        ],
      ]
      ],
      'onboarding-gp-business'  => [
        [
          'img_url'     => config('app.url') . $assetGpOnboardingUrl . 'ic_promote.svg',
          'title'       => 'Naikkan Iklan Anda',
          'description' => 'Gunakan MamiAds untuk memperluas jaringan iklan.',
        ],
        [
          'img_url'     => config('app.url') . $assetGpOnboardingUrl . 'ic_observe_performance.svg',
          'title'       => 'Memantau Performa Kos',
          'description' => 'Lihat performa kos Anda setelah menggunakan GoldPlus.',
        ],
        [
          'img_url'     => config('app.url') . $assetGpOnboardingUrl . 'ic_observe_property.svg',
          'title'       => 'Cek Properti Sekitar',
          'description' => 'Anda dapat memantau kondisi bisnis kos sekitar.',
        ],
        [
          'img_url'     => config('app.url') . $assetGpOnboardingUrl . 'ic_mamicoin.svg',
          'title'       => 'Menggunakan MamiPoin',
          'description' => 'Lebih banyak point yang Anda dapatkan bersama GoldPlus. Tukarkan dengan hadiah menarik.',
        ],
        [
          'img_url'     => config('app.url') . $assetGpOnboardingUrl . 'ic_bill_management.svg',
          'title'       => 'Kelola Tagihan',
          'description' => 'Anda bisa melihat catatan tagihan penyewa dan mengirim pengingat pembayaran.',
        ],
      ]
  ],
);
