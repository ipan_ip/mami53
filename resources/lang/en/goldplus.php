<?php

use App\Entities\GoldPlus\Enums\GoldplusStatus;

return [
    'package' => [
        'charging' => 'Komisi booking :percentage dari tagihan pertama.'
    ],
    'status' => [
        'submission' => [
            GoldplusStatus::REGISTER => 'Daftar GoldPlus',
            GoldplusStatus::REVIEW => 'Sedang Direview',
            GoldplusStatus::WAITING => 'Menunggu Pembayaran',
            GoldplusStatus::GP1 => 'GoldPlus 1',
            GoldplusStatus::GP2 => 'GoldPlus 2',
            GoldplusStatus::GP3 => 'GoldPlus 3',
            GoldplusStatus::GP4 => 'GoldPlus 4',
        ]
    ],
    'error' => [
        'submission' => [
            'has_active_submission' => 'Pengajuan sebelumnya masih diproses.',
        ],
    ],
];