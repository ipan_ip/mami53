<?php

use App\Entities\User\UserVerificationAccount;

return [
    'user-identity' => [
        UserVerificationAccount::IDENTITY_CARD_STATUS_VERIFIED => [
            'scheme' => 'status_identity/verified',
            'title' => 'Anda telah terverifikasi!',
            'body' => 'Hore, berhasil! Proses Booking Kos jadi lebih mudah dan aman. Coba fiturnya sekarang yuk!',
        ],
        UserVerificationAccount::IDENTITY_CARD_STATUS_CANCELLED => [
            'scheme' => 'status_identity/cancelled',
            'title' => 'Admin membatalkan verifikasi identitas',
            'body' => 'Anda bisa mencoba kembali dengan memasukkan informasi yang benar.',
        ],
        UserVerificationAccount::IDENTITY_CARD_STATUS_REJECTED => [
            'scheme' => 'status_identity/rejected',
            'title' => 'Verifikasi gagal',
            'body' => [
                UserVerificationAccount::REJECT_REASON_DATA_INVALID => 'Pastikan Anda memasukkan data yang benar.',
                UserVerificationAccount::REJECT_REASON_IMAGE_UNCLEAR => 'Mohon perhatikan kualitas file gambar yang Anda upload.',
                UserVerificationAccount::REJECT_REASON_IMAGE_REJECTED => 'File gambar yang Anda upload ditolak.',
                'other' => 'Pastikan data dan gambar yang Anda masukkan benar.',
            ],
        ],
    ],
    'booking-draft' => [
        'created' => [
            'title'     => 'Ada Booking Kos yang masih menunggu nih!',
            'message'   => 'Yuk, selesaikan Booking segera sebelum kamar Kos ini diisi orang lain',
            'scheme'    => 'list_booking?draft=true'
        ],
    ],
    'admin' => [
        'reject-remark' => [
            'Mohon ganti foto dengan posisi horizontal (landscape).',
            'Mohon ganti foto agar tidak menampilkan nomor HP.',
            'Mohon ganti foto agar tidak menampilkan alamat.',
            'Mohon ganti foto agar horizontal dan tidak diedit dengan foto lain.',
            'Mohon lengkapi foto kos (bangunan depan, dalam kamar, fasilitas, dll) dengan posisi horizontal (landscape).',
            'Mohon ganti foto agar tidak menampilkan cap, ataupun konten perusahaan lain.',
            'Mohon lengkapi alamat kos dengan nomor rumah, serta RT/ RW.',
            'Mohon tambahkan ukuran kamar yang benar/ valid.',
            'Mohon perbaiki harga kos agar formatnya benar/ valid.',
            'Mohon sesuaikan peta lokasi dengan alamat kos.',
            'Anda telah memiliki Iklan Kos dengan data yang sama.',
            'Mohon ganti foto agar berbeda dengan Iklan Kos (tipe/ harga) Anda yang lain.',
            'Mohon ganti file foto dengan resolusi lebih besar.',
            'Bukan data iklan kos. Silakan gunakan iklan kategori Barang dan Jasa.',
            'Apartemen hanya dapat diiklankan oleh pengguna Premium. Silakan hubungi CS untuk info lengkap.',
            'Kos hanya dapat diiklankan oleh pengguna Premium. Silakan lengkapi data dan upgrade ke Premium.',
        ],
        'bbk-reject-remark' => [
            'Mohon pastikan nama pemilik rekening sama dengan nama lengkap Anda.'
        ],
    ],
    'new-tenant-contract-active' => [
        'title'     => 'Hore, kamu sudah menjadi penyewa kos!',
        'message'   => 'Klik untuk melihat kontrak sewamu di :room',
        'scheme'    => 'kost_saya'
    ],
    'submit-booking-owner-notification' => [
        'real-time' => [
            'title'     => 'Ada Pengajuan Sewa Baru',
            'message'   => 'Calon penyewa baru menunggu Anda. Konfirmasikan sekarang.',
            'scheme'    => 'booking_detail/:bookingId'
        ],
        'first' => [
            'title'     => 'Calon Penyewa Menunggu Konfirmasi Anda',
            'message'   => 'Segera lakukan konfirmasi booking untuk pengajuan sewa :roomName',
            'scheme'    => 'booking_detail/:bookingId'
        ],
        'second' => [
            'title'     => 'Anda Belum Konfirmasi Booking Ini',
            'message'   => 'Jangan sampai kehilangan calon penyewa kos. Ayo, segera konfirmasi pengajuan sewa :roomName',
            'scheme'    => 'booking_detail/:bookingId'
        ],
        'third' => [
            'title'     => 'Jangan Sampai Kehilangan Calon Penyewa',
            'message'   => 'Segera lakukan konfirmasi untuk pengajuan sewa :roomName',
            'scheme'    => 'booking_detail/:bookingId'
        ],
    ],
    'near-expired-booking-tenant-notification' => [
        'third' => [
            'title'     => 'Lakukan Pembayaran Sebelum 6 Jam Lagi',
            'message'   => 'Booking Anda akan berakhir. Segera lakukan pembayaran sebelum kamar kos ini menjadi milik orang lain',
            'scheme'    => 'list_booking?id=:bookingId'
        ],
        'second' => [
            'title'     => 'Waktu Anda Tinggal 3 Jam Lagi',
            'message'   => 'Masa pembayaran Anda akan berakhir. Segera lakukan pembayaran sewa :roomName',
            'scheme'    => 'list_booking?id=:bookingId'
        ],
        'first' => [
            'title'     => '60 Menit Lagi, Waktu Pembayaran Habis!',
            'message'   => 'Yuk bayar sekarang! Belum tentu kamar kos ini bisa kamu booking lagi lho. Klik untuk detail.',
            'scheme'    => 'list_booking?id=:bookingId'
        ],
    ],
    'owner-empty-room-contract-reminder' => [
        'title'     => 'Penyewa kos ini belum punya kamar',
        'message'   => 'Mohon segera pilih kamar untuk :tenantName',
        'scheme'    => 'booking_detail/:bookingId'
    ],
    'room-price-component-response-message' => [
        'created'       => 'Biaya berhasil diubah',
        'deleted'       => 'Biaya berhasil dihapus',
        'switch_on'     => 'Biaya berhasil di-aktifkan',
        'switch_off'    => 'Biaya berhasil di-nonaktifkan'
    ],
];
