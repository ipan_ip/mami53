<p>
	Dear Administrator,
</p>
<br>
<p>
	Seorang owner dengan nomor telepon <b>{{$owner->user->phone_number}}</b> menginginkan kostnya yang bernama 
	<b>{{ $room->name }}</b> bisa diikutkan dalam program booking di MamiKos.
</p>
<br>
<p>
	Mohon segera di-follow up dengan prosedur yang berlaku.
</p>