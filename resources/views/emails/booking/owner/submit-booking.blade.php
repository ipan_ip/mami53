<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="format-detection" content="telephone=no" />
        <meta
            name="viewport"
            content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=no;"
        />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
        <title>{{ $subject }}</title>
        <link rel="preload" href="/css/fonts/lato.css" as="style">
        <link rel="stylesheet" href="/css/fonts/lato.css">

        <style>
            * {
                word-break: break-word;
            }

            p {
                margin: 0;
            }

            @media only screen and (max-width: 767px),
                screen and (max-device-width: 767px) {
                .mobile-hide {
                    display: none !important;
                }

                .mobile-full-width {
                    width: 100% !important;
                }

                .mobile-text-center {
                    text-align: center !important;
                }

                .mobile-download {
                    padding-left: 0 !important;
                }

                .mamikos-copyright {
                    padding-top: 8px;
                }

                .mamikos-help-tnc {
                    margin-right: 16px !important;
                }

                .mamikos-footer-brand {
                    padding-top: 28px !important;
                }

                .mamikos-action-button {
                    margin-top: 32px !important;
                }

                .mamikos-info {
                    padding: 24px 0 !important;
                }

                .mamikos-social-media {
                    padding: 32px 0 24px !important;
                }

                .mamikos-email-body {
                    padding: 24px 0 0 !important;
                }

                .mamikos-email-content-table {
                    padding: 24px 0 !important;
                }

                .mamikos-email-title {
                    margin-bottom: 32px !important;
                }

                .mamikos-email-header {
                    padding: 24px 0 28px !important;
                }
            }
        </style>
    </head>

    <body
        style="
            font-family: 'Lato', sans-serif;
            font-size: 20px;
            line-height: 30px;
            margin: 0 auto;
            padding: 0;
            box-sizing: border-box;
            background-color: #fff;
            color: #404040;
            max-width: 768px;
            width: 100%;
        "
    >
        <table
            align="center"
            cellpadding="0"
            cellspacing="0"
            style="
                border-collapse: collapse;
                margin: 0 auto;
                width: 100%;
                background-color: #fff;
            "
        >
            <thead>
                <tr>
                    <td>
                        <table
                            border="0"
                            cellpadding="0"
                            cellspacing="0"
                            align="center"
                            style="width: 100%; padding: 0 24px;"
                        >
                            <tr>
                                <td
                                    align="center"
                                    class="mamikos-email-header"
                                    style="padding: 24px 0; border-bottom: 1px solid #e8e8e8;"
                                >
                                    <a href="https://www.mamikos.com">
                                        <img
                                            style="width: 186px; display: inherit;"
                                            src="https://mamikos.com/general/img/logo/mamikos_logo.png"
                                            alt="Mamikos Logo"
                                        />
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td style="padding: 0 24px;">
                        <table
                            border="0"
                            cellpadding="0"
                            cellspacing="0"
                            class="mamikos-email-body"
                            width="100%"
                            style="padding: 42px 24px;"
                        >
                            <tr>
                                <td>
                                    <h1
                                        class="mamikos-email-title"
                                        style="
                                            margin: 0;
                                            font-weight: 900;
                                            font-size: 32px;
                                            line-height: 40px;
                                            margin-bottom: 48px;
                                        "
                                    >
                                        {{ $subject }}
                                    </h1>
                                    <h3
                                        style="
                                            font-weight: bold;
                                            margin-bottom: 16px;
                                            margin-top: 0;
                                            font-size: 20px;
                                            line-height: 28px;
                                        "
                                    >
                                        Hai, {{ $owner_name }}
                                    </h3>
                                    <p style="margin-bottom: 0;">
                                        Anda mendapatkan satu calon penyewa baru untuk {{ $room_name }}. Berikut adalah detailnya:
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 32px0;" class="mamikos-email-content-table">
                                    <table
                                        border="0"
                                        cellpadding="0"
                                        cellspacing="0"
                                        width="100%"
                                        style="
                                            padding: 8px 24px;
                                            border: 1px solid #e8e8e8;
                                            border-radius: 16px;
                                        "
                                    >
                                        <tr>
                                            <td>
                                                <table width="100%">
                                                    <tr>
                                                        <td
                                                            style="
                                                                padding: 16px 0;
                                                                border-bottom: 1px solid #e8e8e8;
                                                            "
                                                        >
                                                            Nama kos: <b>{{ $room_name }}</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td
                                                            style="
                                                                padding: 16px 0;
                                                                border-bottom: 1px solid #e8e8e8;
                                                            "
                                                        >
                                                            Nama calon penyewa: <b>{{ $tenant_name }}</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td
                                                            style="
                                                                padding: 16px 0;
                                                                border-bottom: 1px solid #e8e8e8;
                                                            "
                                                        >
                                                            Periode sewa: <b>{{ $duration }}</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding: 16px 0;">
                                                            Tanggal masuk: <b>{{ $checkin_date }}</b>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>
                                        Jangan buat {{ $tenant_name }} menunggu. Segera lakukan konfirmasi booking agar Anda tidak kehilangan calon penyewa kos.
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <a
                                        class="mamikos-action-button"
                                        href="{{ $url }}"
                                        style="
                                            font-size: 16px;
                                            background: #1baa56;
                                            border-radius: 4px;
                                            padding: 14px 16px;
                                            line-height: 20px;
                                            max-width: 244px;
                                            width: 100%;
                                            font-weight: bold;
                                            color: #fff;
                                            text-decoration: none;
                                            display: inline-block;
                                            margin-top: 48px;
                                        "
                                    >
                                        Konfirmasi Sekarang
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center" style="padding-top: 34px; padding-bottom: 20px;">
                        <a
                            href="https://www.linkedin.com/company/mamikos/"
                            style="
                                padding: 8px;
                                border: 1px solid #ddd;
                                width: 32px;
                                height: 32px;
                                display: inline-block;
                                border-radius: 50%;
                                box-sizing: border-box;
                                margin: 0 8px;
                            "
                        >
                            <img
                                src="https://mamikos.com/general/img/icons/social-media/ic_linkedin.png"
                                alt="Linkedin Mamikos"
                                style="
                                    width: 100%;
                                    height: 100%;
                                    display: block;
                                    object-fit: contain;
                                    object-position: center;
                                "
                            />
                        </a>
                        <a
                            href="https://www.instagram.com/mamikosapp/"
                            style="
                                padding: 8px;
                                border: 1px solid #ddd;
                                width: 32px;
                                height: 32px;
                                display: inline-block;
                                border-radius: 50%;
                                box-sizing: border-box;
                                margin: 0 8px;
                            "
                        >
                            <img
                                src="https://mamikos.com/general/img/icons/social-media/ic_instagram.png"
                                alt="Instagram Mamikos"
                                style="
                                    width: 100%;
                                    height: 100%;
                                    display: block;
                                    object-fit: contain;
                                    object-position: center;
                                "
                            />
                        </a>
                        <a
                            href="https://twitter.com/mamikosapp"
                            style="
                                padding: 8px;
                                border: 1px solid #ddd;
                                width: 32px;
                                height: 32px;
                                display: inline-block;
                                border-radius: 50%;
                                box-sizing: border-box;
                                margin: 0 8px;
                            "
                        >
                            <img
                                src="https://mamikos.com/general/img/icons/social-media/ic_twitter.png"
                                alt="Twitter Mamikos"
                                style="
                                    width: 100%;
                                    height: 100%;
                                    display: block;
                                    object-fit: contain;
                                    object-position: center;
                                "
                            />
                        </a>
                        <a
                            href="https://www.facebook.com/Mamikos-Cari-Kos-Gampang-1043376389026435/?fref=ts"
                            style="
                                padding: 8px;
                                border: 1px solid #ddd;
                                width: 32px;
                                height: 32px;
                                display: inline-block;
                                border-radius: 50%;
                                box-sizing: border-box;
                                margin: 0 8px;
                            "
                        >
                            <img
                                src="https://mamikos.com/general/img/icons/social-media/ic_facebook.png"
                                alt="Facebook Mamikos"
                                style="
                                    width: 100%;
                                    height: 100%;
                                    display: block;
                                    object-fit: contain;
                                    object-position: center;
                                "
                            />
                        </a>
                    </td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td>
                        <table
                            border="0"
                            cellpadding="0"
                            cellspacing="0"
                            width="100%"
                            style="padding: 0 24px;"
                        >
                            <tr>
                                <td>
                                    <table
                                        border="0"
                                        cellpadding="0"
                                        cellspacing="0"
                                        width="100%"
                                        style="border-top: 1px solid #e8e8e8; padding-bottom: 44px;"
                                    >
                                        <tr>
                                            <td>
                                                <table
                                                    width="50%"
                                                    align="left"
                                                    border="0"
                                                    cellpadding="0"
                                                    cellspacing="0"
                                                    style="padding-top: 24px;"
                                                    class="mobile-full-width mamikos-footer-brand"
                                                >
                                                    <tr>
                                                        <td class="mobile-text-center">
                                                            <a href="https://www.mamikos.com">
                                                                <img
                                                                    style="width: 186px;"
                                                                    src="https://mamikos.com/general/img/logo/mamikos_logo.png"
                                                                    alt="Mamikos Logo"
                                                                />
                                                            </a>
                                                            <p
                                                                style="
                                                                    font-size: 16px;
                                                                    line-height: 24px;
                                                                    margin-top: 10px;
                                                                    margin-bottom: 0;
                                                                "
                                                            >
                                                                {{-- <b>Tagline placed in here</b> --}}
                                                            </p>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table
                                                    width="50%"
                                                    align="left"
                                                    border="0"
                                                    cellpadding="0"
                                                    cellspacing="0"
                                                    style="padding-top: 28px; padding-left: 28px;"
                                                    class="mobile-full-width mobile-download"
                                                >
                                                    <tr>
                                                        <td class="mobile-text-center">
                                                            <p
                                                                class="mobile-hide"
                                                                style="
                                                                    font-weight: bold;
                                                                    font-size: 14px;
                                                                    line-height: 20px;
                                                                    margin-bottom: 16px;
                                                                    margin-top: 0;
                                                                "
                                                            >
                                                                Download Mamikos App
                                                            </p>
                                                            <table
                                                                cellspacing="0"
                                                                cellpadding="0"
                                                                width="100%"
                                                            >
                                                                <tr>
                                                                    <td class="mobile-text-center">
                                                                        <a
                                                                            href="https://play.google.com/store/apps/details?id=com.git.mami.kos&utm_campaign=DAppAndroFooter&utm_source=DownloadAppFooter&utm_medium=DownloadAppFooter&utm_term=DownloadAppFooter"
                                                                            style="display: inline-block;"
                                                                        >
                                                                            <img
                                                                                src="https://www.mamikos.com/general/img/pictures/download_gplay.png"
                                                                                alt="download google play"
                                                                                style="
                                                                                    width: 135px;
                                                                                    margin-right: 16px;
                                                                                "
                                                                            />
                                                                        </a>
                                                                        <a
                                                                            href="https://itunes.apple.com/id/app/mami-kos/id1055272843"
                                                                            style="display: inline-block;"
                                                                        >
                                                                            <img
                                                                                src="https://www.mamikos.com/general/img/pictures/download_ios.png"
                                                                                alt="download app store"
                                                                                style="width: 135px;"
                                                                            />
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td
                                    style="
                                        padding-bottom: 28px;
                                        border-bottom: 1px solid #e8e8e8;
                                    "
                                    class="mobile-text-center"
                                >
                                    <p
                                        style="
                                            font-size: 12px;
                                            line-height: 18px;
                                            color: #757575;
                                            margin: 0;
                                        "
                                    >
                                        Update your
                                        <a href="#" style="color: inherit;">email preferences</a> to
                                        choose the types of emails you receive, or you can
                                        <a href="#" style="color: inherit;">unsubscribe</a> from all
                                        future emails.
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td class="mamikos-info" style="padding: 20px 0;">
                                    <table
                                        border="0"
                                        cellpadding="0"
                                        cellspacing="0"
                                        style="width: 100%;"
                                    >
                                        <tr>
                                            <td
                                                class="mobile-text-center"
                                                style="font-size: 12px; line-height: 18px;"
                                            >
                                                <table
                                                    align="left"
                                                    border="0"
                                                    cellpadding="0"
                                                    cellspacing="0"
                                                    class="mobile-full-width mamikos-help"
                                                >
                                                    <tr>
                                                        <td>
                                                            <a
                                                                href="https://mamikos.com/privacy"
                                                                class="mamikos-help-tnc"
                                                                style="margin-right: 48px; color: #757575;"
                                                                >Kebijakan Privasi</a
                                                            >
                                                            <a
                                                                href="https://help.mamikos.com/"
                                                                style="color: #757575;"
                                                                >Pusat Bantuan</a
                                                            >
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table
                                                    align="right"
                                                    border="0"
                                                    cellpadding="0"
                                                    cellspacing="0"
                                                    class="mobile-full-width"
                                                >
                                                    <tr>
                                                        <td
                                                            style="text-align: right;"
                                                            class="mobile-text-center mamikos-copyright"
                                                        >
                                                            <p style="margin: 0; color: #757575;">
                                                                © 2020 Mamikos.com All Rights Reserved
                                                            </p>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </tfoot>
        </table>
    </body>
</html>
