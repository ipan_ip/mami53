<style>
.row-name {
	text-align: right;
	padding-right: 10px;
}
</style>

<p>
	Dear Administrator,
</p>
<br>
<p>
	Seorang pengguna menginginkan bergabung dengan Mamikos Network.
</p>
<p>
	Berikut detail pengguna tersebut : 
	<table>
		<tr>
			<th class="row-name">Nama</th>
			<td>{{$userName}}</td>
		</tr>
		<tr>
			<th class="row-name">Nomor Telepon</th>
			<td>{{$userPhone}}</td>
		</tr>
		<tr>
			<th class="row-name">Email</th>
			<td>{{$userEmail}}</td>
		</tr>
		<tr>
			<th class="row-name">Status</th>
			<td>{{$userStatus}}</td>
		</tr>
		<tr>
			<th class="row-name">Lokasi</th>
			<td>
				@if($kostInputAddress != '')
					{{ $kostInputAddress }}
				@else
					{{ $kostAddress }}
				@endif
			</td>
		</tr>
		@if($kostAddressDetail != '')
		<tr>
			<th class="row-name">Alamat Lengkap Kost</th>
			<td>{{$kostAddressDetail}}</td>
		</tr>
		@endif
	</table>
</p>
