<p>
	Dear Administrator,
</p>
<br>
<p>
	Seorang pengguna dengan nama <b>{{$name}}</b> memerlukan bantuan untuk menambahkan data listing apartemen..
</p>
<p>
	Nomor handphone pengguna tersebut adalah <b>{{$phone_number}}</b>.
	<br>
	Serta alamat email pengguna tersebut adalah <b>{{$email}}</b>.
</p>