<!doctype html>
<html>
<head>
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title></title>
    <style>
        body {
            padding: 0;
            margin: 0;
        }

        html {
            -webkit-text-size-adjust: none;
            -ms-text-size-adjust: none;
        }

        @media only screen and (max-device-width: 680px), only screen and (max-width: 680px) {
            *[class="table_width_100"] {
                width: 96% !important;
            }

            *[class="border-right_mob"] {
                border-right: 1px solid #dddddd;
            }

            *[class="mob_100"] {
                width: 100% !important;
            }

            *[class="mob_center"] {
                text-align: center !important;
            }

            *[class="mob_center_bl"] {
                float: none !important;
                display: block !important;
                margin: 0px auto;
            }

            .iage_footer a {
                text-decoration: none;
                color: #929ca8;
            }

            img.mob_display_none {
                width: 0px !important;
                height: 0px !important;
                display: none !important;
            }

            img.mob_width_50 {
                width: 40% !important;
                height: auto !important;
            }
        }

        .table_width_100 {
            width: 680px;
        }
    </style>
</head>
<body>
<img src="http://www.google-analytics.com/collect?v=1&tid=UA-71556143-1&cid={{ $userEmail }}&t=event&ec=opensubscribekost&ea=open&el=email&cs=newsletter&cm=email&cn=emailsubscribekost">
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<!--
Responsive Email Template by @keenthemes
A component of Metronic Theme - #1 Selling Bootstrap 3 Admin Theme in Themeforest: http://j.mp/metronictheme
Licensed under MIT
-->

<div id="mailsub" class="notification" align="center">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="min-width: 320px;">
        <tr>
            <td align="center" bgcolor="#eff3f8">
                <table width="680" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <table border="0" cellspacing="0" cellpadding="0" class="table_width_100" width="100%" style="max-width: 680px; min-width: 300px;">
                                <!--content 1 -->

                                <tr>
                                    <td align="center" bgcolor="#fbfcfd">
                                        <table width="90%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td align="center">
                                                    <!-- padding -->
                                                    <div style="height: 60px; line-height: 60px; font-size: 10px;"> </div>
                                                        <div style="line-height: 44px;">
                                                            <a href="https://mamikos.com/kost" title="" class="" target="_blank">
                                                                <img align="center" alt="" src="https://gallery.mailchimp.com/c71f7d8328ec79801a850156d/images/f6f9157f-7390-44a7-a5c2-d206857534ec.jpg" width="600">
                                                            </a>
                                                        </div>
                                                    <!-- padding -->
                                                    <div style="height: 40px; line-height: 40px; font-size: 10px;"> </div>
                                                </td>
                                            </tr>


                                        </table>
                                    </td>
                                </tr>
                                <!--content 1 END-->

                                <!--content Jogja -->
                                @if ($roomsJogja != null)
                                <tr>
                                    <td align="center" bgcolor="#ffffff" style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #eff2f4;">
                                        <table width="94%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td align="center">
                                                    <!-- padding -->
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td align="center">
                                                                <div style="text-align: center;">
                                                                    <a href="https://mamikos.com/kost/kost-jogja-murah?utm_source=email&utm_medium=email&utm_campaign=rekomendasikostjogja&utm_content=rekomendasikost">
                                                                        <img data-file-id="2653465" height="50" width="150" src="https://gallery.mailchimp.com/c71f7d8328ec79801a850156d/images/3677b504-2694-4de0-b2ad-d315e8d53eaa.png">
                                                                    </a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    @foreach($roomsJogja as $room)
                                                    <div class="mob_100" style="float: left; display: inline-block; width: 33%;">
                                                        <table class="mob_100" width="100%" border="0" cellspacing="0"
                                                               cellpadding="0" align="left" style="border-collapse: collapse;">
                                                            <tr>
                                                                <td align="center" style="line-height: 14px; padding: 0 10px;">
                                                                    <p></p>
                                                                    <!-- padding -->
                                                                    <div style="line-height: 14px;">
                                                                        <a href="{{ $room['share_url'] }}" target="_blank"
                                                                           style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
                                                                            <font face="Arial, Helvetica, sans-serif" size="2"
                                                                                  color="#596167">
                                                                                <img src="{{ $room['photo_url']['medium'] }}"
                                                                                     width="185" alt="Image 1" border="0"
                                                                                     style="display: block; width: 100%; height: auto;"/>
                                                                            </font>
                                                                            <br>
                                                                            <font face="Arial, Helvetica, sans-serif" size="2"
                                                                                  color="#596167">
                                                                                <strong>
                                                                                    <a href="{{ $room['share_url'] }}"
                                                                                       target="_blank"
                                                                                       style="text-decoration: none; color: #000;">{{ $room['room_title'] }}
                                                                                    </a>
                                                                                </strong>
                                                                            </font>
                                                                            <p></p>
                                                                            <a style="display:block; width:100%; padding:5px; text-decoration:none; background: #27AB27;">
                                                                                <span style="color:#FFFFFF">
                                                                                    <strong>
                                                                                        @if($room['price_title_format'] != null)
                                                                                            {{ $room['price_title_format']['currency_symbol'] }}.{{ $room['price_title_format']['price']}} /{{  $room['price_title_format']['rent_type_unit']}}
                                                                                        @endif
                                                                                    </strong>
                                                                                </span>
                                                                            </a>
                                                                        </a>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    @endforeach
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><!-- padding -->
                                                    <div style="height: 40px; line-height: 40px; font-size: 10px;"> </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                 </tr>
                                @endif
                                <!--content Jogja END-->

                                <!--content Surabaya -->

                                @if ($roomsSurabaya != null)
                                <tr>
                                    <td align="center" bgcolor="#ffffff" style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #eff2f4;">
                                        <table width="94%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td align="center">
                                                    <!-- padding -->
                                                    <p></p>
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td align="center">
                                                                <div style="text-align: center;"><a
                                                                            href="https://mamikos.com/kost/kost-surabaya-murah?utm_source=email&utm_medium=email&utm_campaign=rekomendasikostsurabaya&utm_content=rekomendasikost"><img
                                                                                data-file-id="2653465" height="50" width="150"
                                                                                src="https://gallery.mailchimp.com/c71f7d8328ec79801a850156d/images/ba5af292-3c6d-4fd0-a062-cebc7ef45bba.png"></a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    @foreach($roomsSurabaya as $room)
                                                        <div class="mob_100" style="float: left; display: inline-block; width: 33%;">
                                                            <table class="mob_100" width="100%" border="0" cellspacing="0"
                                                                   cellpadding="0" align="left" style="border-collapse: collapse;">
                                                                <tr>
                                                                    <td align="center" style="line-height: 14px; padding: 0 10px;">
                                                                        <p></p>
                                                                        <!-- padding -->
                                                                        <div style="line-height: 14px;">
                                                                            <a href="{{ $room['share_url'] ?? "https://mamikos.com/kost" }}" target="_blank"
                                                                               style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
                                                                                <font face="Arial, Helvetica, sans-serif" size="2"
                                                                                      color="#596167">
                                                                                    <img src="{{ $room['photo_url']['medium'] }}"
                                                                                         width="185" alt="Image 1" border="0"
                                                                                         style="display: block; width: 100%; height: auto;"/>
                                                                                </font>
                                                                                <br>
                                                                                <font face="Arial, Helvetica, sans-serif" size="2"
                                                                                      color="#596167">
                                                                                    <strong>
                                                                                        <a href="{{ $room['share_url'] ?? "https://mamikos.com/kost"}}"
                                                                                           target="_blank"
                                                                                           style="text-decoration: none; color: #000;">{{ $room['room_title'] }}
                                                                                        </a>
                                                                                    </strong>
                                                                                </font>
                                                                                <p></p>
                                                                                <a style="display:block; width:100%; padding:5px; text-decoration:none; background: #27AB27;">
                                                                                <span style="color:#FFFFFF">
                                                                                    <strong>
                                                                                        @if($room['price_title_format'] != null)
                                                                                            {{ $room['price_title_format']['currency_symbol'] }}.{{ $room['price_title_format']['price']}} /{{  $room['price_title_format']['rent_type_unit']}}
                                                                                        @endif
                                                                                    </strong>
                                                                                </span>
                                                                                </a>
                                                                            </a>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    @endforeach
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><!-- padding -->
                                                    <div style="height: 40px; line-height: 40px; font-size: 10px;"> </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                @endif
                                <!--content Surabaya END-->

                                <!--content Jakarta -->
                                @if($roomsJakarta != null)
                                <tr>
                                    <td align="center" bgcolor="#ffffff" style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #eff2f4;">
                                        <table width="94%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td align="center">
                                                    <!-- padding -->
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <p></p>
                                                        <tr>
                                                            <td align="center">
                                                                <div style="text-align: center;">
                                                                    <a href="https://mamikos.com/kost/kost-jakarta-murah?utm_source=email&utm_medium=email&utm_campaign=rekomendasikostjakarta&utm_content=rekomendasikos"><img
                                                                                data-file-id="2653465" height="50" width="150"
                                                                                src="https://gallery.mailchimp.com/c71f7d8328ec79801a850156d/images/34ce5928-99e1-492f-bb63-37ac8d89a05c.png"></a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    @foreach($roomsJakarta as $room)
                                                        <div class="mob_100" style="float: left; display: inline-block; width: 33%;">
                                                            <table class="mob_100" width="100%" border="0" cellspacing="0" cellpadding="0" align="left" style="border-collapse: collapse;">
                                                                <tr>
                                                                    <td align="center" style="line-height: 14px; padding: 0 10px;">
                                                                        <p></p>
                                                                        <!-- padding -->
                                                                        <div style="line-height: 14px;">
                                                                            <a href="{{ $room['share_url'] }}" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
                                                                                <font face="Arial, Helvetica, sans-serif" size="2"
                                                                                      color="#596167">
                                                                                    <img src="{{ $room['photo_url']['medium'] }}"
                                                                                         width="185" alt="Image 1" border="0"
                                                                                         style="display: block; width: 100%; height: auto;"/>
                                                                                </font>
                                                                                <br>
                                                                                <font face="Arial, Helvetica, sans-serif" size="2"
                                                                                      color="#596167">
                                                                                    <strong>
                                                                                        <a href="{{ $room['share_url'] }}" target="_blank" style="text-decoration: none; color: #000;">
                                                                                            {{ $room['room_title'] }}
                                                                                        </a>
                                                                                    </strong>
                                                                                </font>
                                                                                <p></p>
                                                                                <a style="display:block; width:100%; padding:5px; text-decoration:none; background: #27AB27;">
                                                                                <span style="color:#FFFFFF">
                                                                                    <strong>
                                                                                        @if($room['price_title_format'] != null)
                                                                                            {{ $room['price_title_format']['currency_symbol'] }}.{{ $room['price_title_format']['price']}} /{{  $room['price_title_format']['rent_type_unit']}}
                                                                                        @endif
                                                                                    </strong>
                                                                                </span>
                                                                                </a>
                                                                            </a>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    @endforeach
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><!-- padding -->
                                                    <div style="height: 80px; line-height: 80px; font-size: 10px;"> </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                @endif
                                <!--content Jakarta END-->

                                <!--footer -->
                                <tr>
                                    <td class="iage_footer" align="center" bgcolor="#ffffff">
                                        <!-- padding -->
                                        <div style="height: 40px; line-height: 40px; font-size: 10px;"> </div>

                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td align="center">
                                                    <font face="Arial, Helvetica, sans-serif" size="3" style="font-size: 13px;">
                                                        <span style="font-family: Arial, Helvetica, sans-serif; font-size: 13px;">
                                                            <strong>Download App Mamikos</strong>
                                                        </span>
                                                    </font>
                                                    <p></p>
                                                    <div>
                                                        <a href="https://play.google.com/store/apps/details?id=com.git.mami.kos">
                                                            <img data-file-id="2209861" height="52"
                                                                    src="https://gallery.mailchimp.com/00257f71c6b3830ab649a4cdd/images/e7c19b1a-4265-4cbd-a648-f6a69c5c8649.png"
                                                                    style="border: 0px initial ; width: 150px; height: 52px; margin: 0px;"
                                                                    width="150">
                                                        </a>
                                                        <a href="https://itunes.apple.com/id/app/mami-kos/id1055272843">
                                                            <img data-file-id="2209865" height="52"
                                                                    src="https://gallery.mailchimp.com/00257f71c6b3830ab649a4cdd/images/0729dc32-b1bc-4029-a381-1c900674c2b1.png"
                                                                    style="border: 0px initial ; width: 150px; height: 52px; margin: 0px;"
                                                                    width="150">
                                                        </a>
                                                    </div>
                                                </td>
                                                <td valign="top" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">

                                                    <em>Silakan Hubungi CS Mamikos :</em><br>
                                                    Email : <a href="mailto:saran@mamikos.com" target="_blank">saran@mamikos.com</a><br>
                                                    WA: <a href="tel:+6287734003208" target="_blank">0877-3400-3208</a>
                                                </td>
                                            </tr>
                                        </table>

                                        <!-- padding -->
                                        <div style="height: 30px; line-height: 30px; font-size: 10px;"> </div>
                                    </td>
                                </tr>
                                <!--footer END-->
                                <tr>
                                    <td>
                                        <!-- padding -->
                                        <div style="height: 40px; line-height: 40px; font-size: 10px;"> </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
</body>
</html>