<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title></title>
</head>
<body>
    <div>
        This is body of test mail via {{ $smtpHost }}<br>
        Requested at {{ $requestedAt }}<br>
        <br>
        Thank you!
    </div>
</body>
</html>