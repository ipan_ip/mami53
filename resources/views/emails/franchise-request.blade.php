<p>
	Dear Administrator,
</p>
<br>
<p>
	Seorang pengguna dengan nama <b>{{$userName}}</b> berniat mendaftarkan kost-nya dalam program standardisasi MamiKos. User tersebut berstatus <b>{{$userStatus}}</b>.
</p>
<p>
	Nomor handphone pengguna tersebut adalah <b>{{$userPhone}}</b>.
	<br>
	Serta alamat email pengguna tersebut adalah <b>{{$userEmail}}</b>.
</p>
<br>
<p>
	User tersebut mempunyai kost bernama : <b>{{$kostName}}</b>.<br>
	@if($kostUrl != '')
		URL di Mamikos : <a href="{{$kostUrl}}">{{$kostUrl}}</a>
	@endif
</p>
