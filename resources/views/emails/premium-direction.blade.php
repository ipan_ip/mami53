<p>
	Dear Administrator,
</p>
<br>
<p>
	Seorang pengguna dengan nama <b>{{$userName}}</b> menginginkan informasi tentang paket premium di Mamikos. User tersebut berstatus <b>{{$userStatus}}</b>.
</p>
<p>
	Nomor handphone pengguna tersebut adalah <b>{{$userPhone}}</b>.
	<br>
	Serta alamat email pengguna tersebut adalah <b>{{$userEmail}}</b>.
</p>
<p>
	Adapun item-item penjelasan yang diinginkan adalah sebagai berikut : 
	<ul>
		@foreach($requestItems as $requestItem)
			<li>{{$requestItem}}</li>
		@endforeach
	</ul>
</p>
<br>
<p>
	User tersebut mempunyai kost bernama : <b>{{$kostName}}</b>.<br>
	@if($kostUrl != '')
		URL di Mamikos : <a href="{{$kostUrl}}">{{$kostUrl}}</a>
	@endif
</p>
