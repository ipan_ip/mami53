<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>List verified</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker3.css" />
        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">

        <!-- Latest compiled and minified JavaScript -->
        <script src="/js/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
    </head>
    <body>
        <div class="container" style="margin-top: 50px;">

            <div class="row">
                {{-- <form class="form-horizontal" method="GET" action="">
                    <div class="form-group">
                        <select name="p" class="form-control">
                            @for ($i=1;$i<=$total_week;$i++)
                                <option value="{{ $i }}" @if ($i == $now_week) selected="true" @endif>Minggu ke {{ $i }}</option>
                            @endfor
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="submit" name="submit" value="Filter" class="btn btn-xs btn-primary" />
                    </div>
                </form> --}}
                <form class="">
                    <div class="form-group col-md-4">
                        <label>Week</label>
                        <input class="form-control" type="text" value="{{ $now_week }}" readonly/>
                        <p style="font-size: 11px; color: #ff0000;">{!! $note !!}</p>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Sorting with date</label>
                        <div class="input-group input-daterange">
                            <input type="text" class="form-control" name="start" value="{{ $start }}">
                            <div class="input-group-addon">to</div>
                            <input type="text" class="form-control" name="end" value="{{ $end }}">
                        </div>
                    </div>
                    <div class="form-group col-md-2">
                        <label>Action</label>
                        <div style="width: 100%;" class="input-group">
                            <input style="width: 100%;" type="submit" name="submit" value="Filter" class="btn btn-success" />
                        </div>
                    </div>
                </form>
            </div>
            
            <div style="margin-top: 50px;" class="row">
                <div class="col-md-12">
                @if ($room_total < 1) 
                    <label style="width: 100%;" class="alert alert-danger">Empty data</label>
                @else
                    <table class="table" width="100%">
                        <tr>
                            <th>No</th>
                            <th>Agent</th>
                            <th>Kost Name</th>
                            <th>Reason</th>
                            <th>Phone</th>
                            <th>Uploaded</th>
                            <th>Status</th>
                            <th>Verified / Unverified In</th>
                        </tr>

                        @foreach($designers as $key => $value)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ $value->agents->name }}</td>
                                <td>{{ $value->name }}</td>
                                <td>
                                    @if (isset($value->listing_reason) AND count($value->listing_reason) > 0)
                                        {{ $value->listing_reason[0]->content }}
                                    @else
                                        -
                                    @endif
                                </td>
                                <td>{{ $value->owner_phone }}</td>
                                <td>{{ date('d-m-Y H:i:s', strtotime($value->created_at)) }}</td>
                                <td>
                                    @if ($value->expired_phone == '1')
                                        <span class="label label-danger">Not listed</span>  
                                    @else 
                                        @if (1 < 2)
                                            @if ($value->is_active == 'true')
                                                <span class="label label-success">verified</span>
                                            @else
                                                <span class="label label-default">unverified</span>
                                            @endif
                                        @endif
                                    @endif
                                </td>
                                <td>
                                    @if (1 < 2)
                                        @if ($value->expired_phone == '1')
                                          -     
                                        @else
                                            @if (isset($value->room_verify) AND count($value->room_verify) > 0) 
                                                {{ date('d-m-Y H:i:s', strtotime($value->room_verify[0]->created_at)) }}  
                                            @else
                                                -
                                            @endif
                                        @endif
                                    @endif
                                </td>
                                
                            </tr>
                        @endforeach
                    </table>
                @endif
                </div>
            </div>
        </div>
        <script type="text/javascript">
			 $('.input-daterange').datepicker({
                format: 'yyyy-mm-dd',
              });
        </script>
    </body>
</html>
