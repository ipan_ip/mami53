<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>List verified</title>
        <link rel="stylesheet" href="{{mix_url('dist/css/vendor.css')}}">
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <table class="table" width="100%">
                        <tr>
                            <th>No</th>
                            <th>Agent</th>
                            <th>Kost Name</th>
                            <th>Reason</th>
                            <th>Phone</th>
                            <th>Uploaded</th>
                            <th>Status</th>
                            <th>Verified / Unverified In</th>
                        </tr>

                        @foreach($designers as $key => $designer)
                          @if ($designer['week'] != null)
                            <tr style="background: #48CFAD;">
                               <td colspan="8" style="color: #fff;"><strong>Minggu ke {{ $designer['week'] }}</strong></td> 
                            </tr>
                          @endif  
                        
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ $designer['agent_name'] }}</td>
                                <td>{{ $designer['name'] }}</td>
                                <td>{{ $designer['listing_reason'] }}</td>
                                <td>{{ $designer['phone'] }}</td>
                                <td>{{ date('d-m-Y H:i:s', strtotime($designer['created_at'])) }}</td>
                                <td>
                                    @if ($designer['expired_phone'] == '1')
                                        <span class="label label-danger">Not listed</span>  
                                    @else 
                                        @if (1 < 2)
                                            @if ($designer['is_active'] == 'true')
                                                <span class="label label-success">verified</span>
                                            @else
                                                <span class="label label-default">unverified</span>
                                            @endif
                                        @endif
                                    @endif
                                </td>
                                <td>
                                    @if (1 < 2)
                                        @if ($designer['expired_phone'] == '1')
                                          -     
                                        @else
                                          {{ $designer['verified_at'] ? date('d-m-Y H:i:s', strtotime($designer['verified_at'])) : ' - '}}  
                                        @endif
                                    @endif
                                </td>
                                
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>
