@extends('guest.layouts.login')
@section('content')
<div class="form-box" id="login-box">
    <div class="header">Forgot Password</div>
    {{ Form::open(array('url'=>'auth/password','method'=>'POST')) }}
    <div class="body bg-gray">
        <p class="text-red">{{ Session::get('error_message') }}</p>

        @if($errors->has('email') || Session::get('error_type') === 1)
        <div class="form-group has-error">
            @if($errors->has('email'))
            <label class="control-label" for="email">
                <i class="fa fa-times-circle-o"></i> {{ $errors->first('email') }}
            </label>
            @else
            <label class="control-label" for="email">
                Email
            </label>
            @endif
        @else
        <div class="form-group">
        @endif
            <input type="text" name="email" class="form-control" placeholder="Phone Number"
                value="{{ Input::old('email'); }}"/>
        </div>
    </div>

    <div class="footer">
        <button type="submit" class="btn bg-olive btn-block">Send Password via SMS</button>
    </div>
    {{ Form::close() }}

    <div class="margin text-center">
        <span>Mamikos - Cari Kos Gampang & Akurat</span>
        <br/>
        <span>Gongsin International Trasindo Mobile - {{ date('Y', time()) }}</span>
        <br/>
    </div>
</div>
@stop
