@extends('guest.layouts.login')

@section('content')
<div class="form-box" id="login-box">
    <div class="header">Sign In</div>

    {{ Form::open(array('url'=>'auth')) }}

    <div class="body bg-gray">
        @if(Session::get('message'))
            <p>{{ Session::get('message') }}</p>
        @endif
        <p class="text-red">{{ Session::get('error_message') }}</p>

        @if($errors->has('email') || Session::get('error_type') === 1)

        <div class="form-group has-error">
            @if($errors->has('email'))
            <label class="control-label" for="email">
                <i class="fa fa-times-circle-o"></i> {{ $errors->first('email') }}
            </label>
            @else
            <label class="control-label" for="email">
                Email
            </label>
            @endif
        @else
        <div class="form-group">
        @endif
            <input type="text" name="email" class="form-control" placeholder="Phone Number"
                value="{{ request()->old('email') }}"/>
        </div>

        @if($errors->has('password') || Session::get('error_type') === 1)
        <div class="form-group has-error">
            @if($errors->has('password'))
            <label class="control-label" for="password">
                <i class="fa fa-times-circle-o"></i> {{ $errors->first('password') }}
            </label>
            @else
            <label class="control-label" for="password">
                Password
            </label>
            @endif
        @else
        <div class="form-group">
        @endif
            <input type="password" name="password" class="form-control" placeholder="Password"/>
        </div>

        <div class="form-group">
            <input type="checkbox" name="remember_me"/> Remember me
        </div>
    </div>
    <div class="footer">
        <button type="submit" class="btn bg-olive btn-block">Sign me in</button>
    </div>

    {{ Form::close() }}

    <div class="margin text-center">
        <span>Mamikos - Cari Kos Gampang & Akurat</span>
        <br/>
        <span>GIT Mobile - {{ date('Y') }}</span>
        <br/>
    </div>
</div>
@stop
