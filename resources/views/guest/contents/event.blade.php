@extends('guest.layouts.index')
@section('content')
<div class="panel-group" id="accordion">
  @foreach ($events as $event) 
    <div class="panel panel-notice">
      <div class="panel-heading accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#event-{{ $event->id }}">
        <h4 class="panel-title">
          <a>
            {{ $event->title }} <span class="label label-warning">N</span>
            <div class="date">
              {{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $event->created_at)->toDateTimeString() }}
            </div>
          </a>
        </h4>
      </div>
      <div id="event-{{ $event->id }}" class="panel-collapse out collapse">
        <div class="panel-body">
          {{ $event->description }}
        </div>
        <a href="hairclick.wejoin.us://designer_detail/{{ $event->designer_id }}">
          <img src="{{ $event->image_url }}" alt="">
        </a>
      </div>
    </div>
  @endforeach
</div>
@stop