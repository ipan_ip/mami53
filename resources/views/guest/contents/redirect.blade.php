<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title></title>
  <link rel="stylesheet" href="">
  <script>
  var myApp = {
    redirectSchemaWithFallback : function (schema) 
    {
      console.log(schema);
      // Enabling Schema with Fallback if The User not Installed the App
      if( navigator.userAgent.match(/Android/i) || (navigator.userAgent.toLowerCase().indexOf("android") > -1) ) 
      {

 //       window.location.replace('market://details?id=com.git.stories');

        // The Reason set to 1000, to make sure, the play store not override the app
        // Set TimeOut to Low, The Anomaly will Happen
        setTimeout(function(){

          window.location.replace('https://play.google.com/store/apps/details?id=com.git.mami.kos');
        }, 100);
      }
      // else if ( navigator.userAgent.match(/iPhone/i) )
      else if ( navigator.userAgent.match(/(iPad|iPhone|iPod)/g) )
      {
       window.location.replace('https://itunes.apple.com/ca/app/mami-kos/id1055272843?mt=8');
      }
     else
     {
        setTimeout(function(){    

          window.location.replace('https://play.google.com/store/apps/details?id=com.git.mami.kos');

         }, 100);
      }
     },
  };

   myApp.redirectSchemaWithFallback('test');
 </script>
</head>
<body>
  
</body>
</html>

