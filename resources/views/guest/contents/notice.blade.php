@extends('guest.layouts.index')
@section('content')
<div class="panel-group" id="accordion">
  @foreach ($notices as $notice) 
    <div class="panel panel-notice">
      <div class="panel-heading accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#notice-{{ $notice->id }}">
        <h4 class="panel-title">
          <a>
            {{ $notice->title }} <span class="label label-warning">N</span>
            <div class="date">
              {{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $notice->created_at)->toDateTimeString() }}
            </div>
          </a>
        </h4>
      </div>
      <div id="notice-{{ $notice->id }}" class="panel-collapse collapse out">
        <div class="panel-body">
          {{ $notice->description }}
        </div>
      </div>
    </div>
  @endforeach
</div>
@stop