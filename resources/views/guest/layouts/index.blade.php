<!DOCTYPE html>
<html lang="en">
  <head>
    @include('guest.partials.meta')
    @include('guest.partials.style')
    @yield('style')
  </head>

  <body>
    @yield('content')
    @include('guest.partials.script')
    @yield('script')
  </body>
</html>
