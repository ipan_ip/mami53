<!DOCTYPE html>
<html lang="en" class="bg-black">
    <head>

        @include('guest.partials.meta')
        @include('guest.partials.login.style')

        <style>
            .bg-black {
                background: url('pw_maze_black.png');
            }

            .form-box .form-group.has-error .form-control {
                border: 1px solid rgba(197, 28, 28, 1) !important;
            }

            .form-group.has-error label {
                color: #A94442;
            }

            .text-red {
                color: rgba(197, 28, 28, 1) !important;
            }
        </style>
    </head>

    <body class="bg-black">
        @yield('content')
        @include('guest.partials.login.script')
    </body>

</html>
