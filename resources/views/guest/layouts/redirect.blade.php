<!DOCTYPE html>
<html lang="en">
  <head>
    @include('guest.partials.meta')
    @yield('style')
  </head>

  <body>
    @yield('script')
  </body>
</html>
