<!-- Meta
================================================== -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="description" content="{{ isset($data['message'])?$data['message']:'Mamikos Admin' }}">
  <meta name="author" content="Pissue App Team">
  <meta name="image" content="{{ isset($data['image_link'])?$data['image_link']:'A' }}">
  <meta property="og:locale" content="en_US" />
  <meta property="og:type" content="Quiz" />
  <meta property="og:title" content="{{ isset($data['title'])?$data['title']:'Pissue App' }}" />
  <meta property="og:description" content="{{ isset($data['message'])?$data['message']:'Pissue App' }}" />
  <meta property="og:url" content="http://pissue.com" />
  <meta property="og:site_name" content="Mamikos Admin" />
  <meta property="article:author" content="Mamikos Team" />
  <meta property="article:section" content="Mobile" />
  <meta property="article:published_time" content="2015-08-08T14:41:57+00:00" />
  <meta property="article:modified_time" content="2015-08-08T15:03:21+00:00" />
  <meta property="og:updated_time" content="2015-06-08T15:03:21+00:00" />
  <meta property="og:image" content="{{ isset($data['image_link'])?$data['image_link']:'A' }}" />
  
  @include('web.@meta.favicon')

  <title>{{ isset($pageTitle)?$pageTitle:'Mamikos Admin' }}</title>