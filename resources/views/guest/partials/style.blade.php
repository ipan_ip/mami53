<!-- Stylesheet
================================================== -->

  <!-- Bootstrap Core 3.2.0 -->
  <link media="all" type="text/css" rel="stylesheet" href='{{      url('assets/vendor/bootstrap/css/bootstrap.min.css') }}' />

  <!-- Bootstrap Theme 3.2.0 -->
  <link media="all" type="text/css" rel="stylesheet" href='{{      url('assets/vendor/bootstrap/css/bootstrap-theme.min.css') }}' />

  <!-- Custom styles for this template -->
  <link media="all" type="text/css" rel="stylesheet" href='{{      url('assets/main/css/index.css') }}' />

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Custom Stylesheet -->
  @if ( ! empty($asset['stylesheet']) )
    @foreach ($asset['stylesheet'] as $stylesheet)
      <link media="all" type="text/css" rel="stylesheet" href='{{      url($stylesheet) }}' />
    @endforeach
  @endif