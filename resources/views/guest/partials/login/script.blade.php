<!--  Javascript
================================================== -->

  <!-- jQuery -->
  <script src='{{      url('assets/vendor/jquery/jquery.min.js') }}' ></script>

  <!-- Bootstrap -->
  <script src='{{      url('assets/vendor/bootstrap/js/bootstrap.min.js') }}' ></script>

  <!-- DATA TABLES SCRIPT -->
  <script src='{{      url('assets/vendor/admin-lte/js/plugins/datatables/jquery.dataTables.js') }}' ></script>
  <script src='{{      url('assets/vendor/admin-lte/js/plugins/datatables/dataTables.bootstrap.js') }}' ></script>

  <!-- AdminLTE App -->
  <script src='{{      url('assets/vendor/admin-lte/js/AdminLTE/app.js') }}' ></script>

  <!-- AdminLTE for demo purposes -->
  <script src='{{      url('assets/vendor/admin-lte/js/AdminLTE/demo.js') }}' ></script>

  <!-- Main -->
  <script src='{{      url('assets/main/js/index.js') }}' ></script>

  <!-- Custom Javascript -->
  @if ( ! empty($asset['javascript']) )
    @foreach ($asset['javascript'] as $javascript)
      <script src='{{      url($javascript) }} ' ></script>
    @endforeach
  @endif