<!-- Stylesheet
================================================== -->

  <!-- Bootstrap Core 3.2.0 -->
  <link media="all" type="text/css" rel="stylesheet" href='{{      url('assets/vendor/bootstrap/css/bootstrap.min.css') }}' />

  <!-- Bootstrap Theme 3.2.0 -->
  <link media="all" type="text/css" rel="stylesheet" href='{{      url('assets/vendor/bootstrap/css/bootstrap-theme.min.css') }}' />

  <!-- font Awesome -->
  <link media="all" type="text/css" rel="stylesheet" href='{{      url('assets/vendor/admin-lte/css/font-awesome.min.css') }}' />

  <!-- Theme style -->
  <link media="all" type="text/css" rel="stylesheet" href='{{      url('assets/vendor/admin-lte/css/AdminLTE.css') }}' />

  <!-- Custom styles for this template -->
  <link media="all" type="text/css" rel="stylesheet" href='{{      url('assets/main/css/index.css') }}' />

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
  <![endif]-->

  <!-- Custom Stylesheet -->
  @if ( ! empty($asset['stylesheet']) )
    @foreach ($asset['stylesheet'] as $stylesheet)
      <link media="all" type="text/css" rel="stylesheet" href='{{      url($stylesheet) }}' />
    @endforeach
  @endif
