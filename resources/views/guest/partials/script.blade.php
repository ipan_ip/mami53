<!--  Javascript
================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->

  <!-- jQuery 1.11.1 -->
  <script src='{{      url('assets/vendor/jquery/jquery.min.js') }}'></script>
  <!-- // <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> -->

  <!-- Bootstrap -->
  <script src='{{      url('assets/vendor/bootstrap/js/bootstrap.min.js') }}'></script>

  <!-- Main -->
  <script src='{{      url('assets/main/js/index.js') }}'></script>

  <!-- Custom Javascript -->
  @if ( ! empty($asset['javascript']) )
    @foreach ($asset['javascript'] as $javascript)
      <script src='{{      url($javascript) }}' ></script>
    @endforeach
  @endif