<!DOCTYPE html>
<html>
  <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

      <meta name="robots" content="noindex, nofollow">

      <title>Mohon maaf, Mamikos sedang Maintenance</title>

      <link rel="shortcut icon" href="/assets/favicon.ico">

      <link rel="preload" href="/css/fonts/lato.css" as="style">
      <link rel="stylesheet" href="/css/fonts/lato.css">

      <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                font-family: 'Lato', sans-serif;
                font-weight: 300;
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-orient: vertical;
                -webkit-box-direction: normal;
                -ms-flex-direction: column;
                flex-direction: column;
                -webkit-box-pack: start;
                -ms-flex-pack: start;
                justify-content: flex-start;
            }

            .maintenance {
                margin: 0 auto;
            }

            .content {
                max-width: 678px;
                padding: 1rem;
                text-align: center;
            }

            header {
                font-size: 24px;
                color: #242424;
            }

            img {
                margin-top: 1rem;
                width: 40%;
                height: auto;
            }

            section {
                margin: 3rem 0 3rem;
                font-size: 1rem;
                color: #484848;
            }
            
            .btn-mamigreen {
                color: #fff;
                background-color: #1baa56;
                border-color: #239923;
                border-radius: 4px;
                padding: 12px 100px;
            }
            
            .btn-mamigreen:active,
            .btn-mamigreen:hover,
            .btn-mamigreen:focus {
                color: #fff;
                background-color: #1f881f;
                border-color: #1b771b;
                text-decoration: none;
                cursor: pointer;
            }

            @media (max-width: 425px) {
                img {
                    width: 75%;
                    height: auto;
                }
                section {
                    margin: 2rem 0 2rem;
                }

                .btn {
                    padding: 12px 50px;
                }
            }

            @media (min-width: 768px) {
                .content {
                    margin-top: 70px;
                }
                
                header {
                    margin-bottom: 2rem;
                }
            }
      </style>
  </head>

  <body>
      <div class="maintenance" title="Mohon maaf, Mamikos sedang Maintenance">
          <div class="content">
              <header>
				<p><strong>Ooops!</strong></p>
			</header>
			<img
				src="/assets/maintenance_503.svg"
				alt="maintenance"
				title="Maintenance"
			/>
			<section>
				<p>Mamikos maintenance dulu, ya.</p>
				<p>
					Untuk kembali ke halaman sebelumnya, silakan muat ulang beberapa saat
					lagi.
				</p>
			</section>
			<a class="btn btn-mamigreen" onclick="location.reload()"><strong>Muat Ulang</strong></a>
          </div>
      </div>

      <noscript>Mohon maaf, Mamikos sedang Maintenance</noscript>
  </body>
</html>
