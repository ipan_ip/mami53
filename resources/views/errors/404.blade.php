<!DOCTYPE html>
<html lang="id">
	<head resource="/">
		@include('web.@meta.common-meta-link')

		<link rel="dns-prefetch" href="https://www.googletagmanager.com">
		<link rel="dns-prefetch" href="https://www.google-analytics.com">

		<link rel="preconnect" href="https://static.mamikos.com" crossorigin>

		<meta name="robots" content="noindex, nofollow">

		<title>Maaf Halaman Tidak Ditemukan</title>
		<meta name="description" content="Halaman Tidak Ditemukan. Mau cari info kost di area Jogja, Jakarta, Surabaya, atau di seluruh Indonesia? Coba Mamikos App. Cari Kost Jogja, Kost Bandung, Kost Jakarta, Kost Surabaya, Kost Depok, Makin Gampang!">

		<meta property="og:title" content=">Maaf Halaman Tidak Ditemukan">
		<meta property="og:image" content="/general/img/pictures/share-image-default.jpg">
		<meta property="og:description" content="Halaman Tidak Ditemukan. Mau cari info kost di area Jogja, Jakarta, Surabaya, atau di seluruh Indonesia? Coba Mamikos App. Cari Kost Jogja, Kost Bandung, Kost Jakarta, Kost Surabaya, Kost Depok, Makin Gampang!">

		<meta name="twitter:title" content=">Maaf Halaman Tidak Ditemukan">
		<meta name="twitter:description" content="Halaman Tidak Ditemukan. Mau cari info kost di area Jogja, Jakarta, Surabaya, atau di seluruh Indonesia? Coba Mamikos App. Cari Kost Jogja, Kost Bandung, Kost Jakarta, Kost Surabaya, Kost Depok, Makin Gampang!">
		<meta name="twitter:image" content="/general/img/pictures/share-image-default.jpg">
		<link rel="preload" href="/css/fonts/lato.css" as="style">
    <link rel="stylesheet" href="/css/fonts/lato.css">

		<link rel="preload" href="{{ mix_url('dist/css/common.css') }}" as="style">
		<link rel="preload" href="{{ mix_url('dist/js/common.js') }}" as="script">
		<link rel="preload" href="{{ mix_url('dist/js/404-page.js') }}" as="script">

		@include('web.@script.polyfill-snippet')

		@include('web.@script.utility')

		@include('web.@script.third-parties-sdk-head')

		@include('web.@script.gtm-head')

		@include('web.@script.bugsnag-api-key')

		@include('web.@script.global-auth')

		<link rel="stylesheet" href="{{ mix_url('dist/css/common.css') }}">
	</head>

	<body>
		@include('web.@script.gtm-body')

		@include('web.@script.service-worker')

		<div id="app">
			<container-404></container-404>
		</div>

		<script src="{{ mix_url('dist/js/common.js') }}"></script>
		<script src="{{ mix_url('dist/js/404-page.js') }}"></script>

		@include('web.@script.session-token')

		@include('web.@script.browser-alert')
	</body>
</html>
