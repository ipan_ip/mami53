<!DOCTYPE html>
<html>
  <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

      <meta name="robots" content="noindex, nofollow">

      <title>Mohon maaf terjadi kesalahan - 500</title>

      @include('web.@meta.favicon')

      <link rel="preload" href="/css/fonts/lato.css" as="style">
      <link rel="stylesheet" href="/css/fonts/lato.css">

    <style>
      .btn {
        font-family: "Lato", sans-serif;
        color: #f95516;
        background-color: #fff;
        padding: 7.5px 40px;
        border-radius: 5px;
        background-image: none;
        border: 2px solid #f95516;
        white-space: nowrap;
        cursor: pointer;
      }
      .btn:focus,
      .btn:hover {
        color: #fff;
        background-color: #f95516;
        outline: 0;
      }
      .btn:active {
        background-color: #d64711;
      }

      .btn-reverse {
        color: #fff;
        background-color: #f95516;
      }
      .btn-reverse:focus,
      .btn-reverse:hover {
        color: #fff;
        background-color: #E04205;
        outline: 0;
      }
      .btn-reverse:active {
        background-color: #E04205;
      }

      body {
        font-family: "Lato", sans-serif;
        font-size: 16px;
        line-height: 1.5;
        color: #333;
        background-color: #fff;
        min-height: 100%;
        position: relative;
        overflow-x: hidden;
      }
      .container {
        padding-left: 15px;
        padding-right: 15px;
        margin-left: auto;
        margin-right: auto;
      }
      .content {
        margin-top: 150px;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -ms-flex-pack: distribute;
        justify-content: space-around;
      }
      .error-caption,
      .error-img {
        display: inline-block;
      }
      .content hr {
        border: 1px solid #ddd;
      }
      .error-caption {
        min-width: 220px;
      }
      .title h2 {
        margin-bottom: 0;
        font-weight: 500;
      }
      .sub-title h1 {
        margin: 0;
        font-size: 40px;
      }
      .sub-title.down h1 {
        margin-bottom: 20px;
      }
      .info p {
        margin-top: 20px;
        margin-bottom: 0;
        font-size: 20px;
      }
      .sub-info p {
        margin-top: 5px;
        margin-bottom: 30px;
        font-size: 20px;
      }
      img.img-large {
        width: 100%;
        height: auto;
      }
      .error-caption h1,
      .error-caption h2 {
        color: #f95516;
      }
      .error-caption p {
        color: #707070;
      }
      .img-small {
        display: none;
      }
      .separate-right {
        margin-right: 10px;
      }
      @media (max-width: 767px) {
        .btn {
          padding: 5px 20px;
        }
        .content {
          margin-top: 100px;
          display: -webkit-box;
          display: -ms-flexbox;
          display: flex;
          -webkit-box-orient: vertical;
          -webkit-box-direction: reverse;
          -ms-flex-direction: column-reverse;
          flex-direction: column-reverse;
        }
        .error-caption,
        .error-img {
          margin: 0 auto;
          width: 250px;
        }
        .img-large {
          display: none;
        }
        .img-small {
          display: block;
        }
        .error-caption {
          text-align: center;
        }
        .title h2 {
          font-size: 20px;
        }
        .sub-title h1 {
          font-size: 28px;
        }
        .info p,
        .sub-info p {
          font-size: 14px;
        }
      }
    </style>
  </head>

  <body>
      <div class="container" title="Mohon maaf terjadi kesalahan. Silakan ulangi beberapa saat lagi.">

        <div class="content">
          <div class="error-caption">
            <span class="title"><h2>Error 500 !</h2></span>
            <span class="sub-title up"><h1> Waduh ada yang</h2></span>
            <span class="sub-title down"><h1>gak beres nih</h1></span>
            <hr>
            <span class="info"><p>Mohon maaf terjadi kesalahan.</p></span>
            <span class="sub-info"><p>Silakan ulangi beberapa saat lagi.</p></span>
            <a href="/" role="button" class="separate-right">
              <button class="btn btn-reverse">KE BERANDA</button>
            </a>
            <a href="javascript:void(0)" role="button" onclick="location.reload()">
              <button class="btn">COBA LAGI</button>
            </a>
          </div>

          <div class="error-img">
            <img src="/assets/mamikos_error500.svg" class="img-large" alt="mamikos_error500">
            <img src="/assets/mamikos_error500_small.svg" class="img-small" alt="mamikos_error500">
          </div>
        </div>

      </div>

      <noscript>Mohon maaf terjadi kesalahan. Silakan ulangi beberapa saat lagi.</noscript>
  </body>
</html>
