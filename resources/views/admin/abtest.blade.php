<html>
    <body>
        <h1>Fetching the data, please wait..</h1>
        <pre></pre>
        <script>
        function callAPI() { 
            url = '/admin/abtest?mode=list_experiment';
            resp = fetch(url, {
                method: 'POST',
                body: JSON.stringify({page: 0, limit: 5})
            })
            .then(resp => 
                resp.text()
            )
            .then(text => {
                document.getElementsByTagName('body')[0].innerHTML = text;
            });
        };

        callAPI();
        </script>
    </body>
</html>