<!DOCTYPE html>
<html lang="en" ng-app="myApp">
  <head>
    @include('guest.partials.meta')
    @include('admin.partials.style')
    @yield('style')
  </head>

  <body class="skin-black" ng-controller="MainCtrl">
    <!-- Loader -->
    <div class="modal"></div>
    <!-- header logo: style can be found in header.less -->
    <header class="header">
      @include('admin.partials.header')
    </header>

    <div class="wrapper row-offcanvas row-offcanvas-left">
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="left-side sidebar-offcanvas">
        @include('admin.partials.left-side')
      </aside>

      <!-- Right side column. Contains the navbar and content of the page -->
      <aside class="right-side">
        <!-- The Main Content is placed here -->
        @include('admin.partials.right-side')
      </aside>

    </div>
    @include('admin.partials.script')
    @yield('script')
  </body>
</html>