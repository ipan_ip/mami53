<div class="box box-primary accordion-table">
  <div class="box-header">
    <h3 class="box-title">{{ $title }}</h3>
    @if (!isset($collapsable) || $collapsable)
      <div class="box-tools pull-right">
        <button
          class="btn btn-box-tool"
          data-widget="collapse"
          data-toggle="tooltip"
          title="Collapse"
        >
          <i class="fa fa-minus"></i>
        </button>
      </div>
    @endif
  </div>

  <div class="box-body no-padding">
    <div class="table-responsive" style="overflow-x: auto">
      <table class="table table-striped">
        <thead>
          <tr>
            @foreach ($headers as $header)
              <th>{{ $header }}</th>
            @endforeach
          </tr>
        </thead>
        <tbody>
          @if (!empty($slot->toHtml()))
            {{ $slot }}
          @else
            <tr class="no-data">
                <td colspan="{{ count($headers) }}" align="center">{{ $noData ?? 'No data' }}</td>
            </tr>
          @endif
        </tbody>
        @if (isset($tfoot) && !empty($tfoot->toHtml()))
          <tfoot>
            {{ $tfoot }}
          </tfoot>
        @endif
      </table>
    </div>
  </div>
</div>