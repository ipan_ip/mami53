<!--  Javascript
================================================== -->

        <!-- jQuery -->


        <!-- Bootstrap -->
        <script src='{{      url('assets/vendor/bootstrap/js/bootstrap.min.js') }}'></script>

        <!-- DATA TABLES SCRIPT -->
        <script src='{{      url('assets/vendor/admin-lte/js/plugins/datatables/jquery.dataTables.js') }}'></script>
        <script src='{{      url('assets/vendor/admin-lte/js/plugins/datatables/dataTables.bootstrap.js') }}'></script>

        <script src='{{      url('assets/vendor/admin-lte/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}'></script>

        <!-- AdminLTE App -->
        <script src='{{      url('assets/vendor/admin-lte/js/AdminLTE/app.js') }}'></script>

        <!-- Jquery Validate -->
        <!-- <script src='{{      url('assets/vendor/jquery/validate/dist/additional-methods.min.js') }}'></script> -->
        <!-- <script src='{{      url('assets/vendor/jquery/validate/dist/jquery.validate.min.js') }}'></script> -->

        <!-- Bootstrap Validator -->
        <script src='{{      url('assets/vendor/bootstrap-validator/js/bootstrapValidator.min.js') }}'></script>
        <!-- <script src='{{      url('assets/vendor/bootstrap/bootstrapvalidator-0.4.5/dist/js/bootstrapValidator.js') }}) }}'></script> -->

        <!-- InputMask -->
        <!-- <script src='{{      url('assets/vendor/admin-lte/js/plugins/input-mask/jquery.inputmask.js') }}'></script>
        <script src='{{      url('assets/vendor/admin-lte/js/plugins/input-mask/jquery.inputmask.date.extensions.js') }}'></script>
        <script src='{{      url('assets/vendor/admin-lte/js/plugins/input-mask/jquery.inputmask.extensions.js') }}) }}'></script> -->

        <!-- Price Format -->
        <!-- <script src='{{      url('assets/vendor/jquery/price_format/jquery.price_format.2.0.min.js') }}'></script> -->

        <!-- Main JS -->
        <script src='{{      url('assets/main/js/index_main.js') }}'></script>
        <script src='{{ url('assets/main/js/link_delete.js') }}'></script>

        <!-- Custom Javascript -->
        @if ( ! empty($asset['javascript']) )
            @foreach ($asset['javascript'] as $javascript)
              <script src='{{      url($javascript) }}') }}'></script>
            @endforeach
        @endif
