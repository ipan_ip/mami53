<!-- sidebar: style can be found in sidebar.less -->
<section class="sidebar">

    <!-- Sidebar user panel -->
    <div class="user-panel">
        <div class="pull-left image">
            <img src="{{ Auth::user()->getPhotoUserAttribute() }}" class="img-circle" alt="User Image" />
        </div>
        <div class="pull-left info">
            <p>Hello, 
                @if (Auth::user()->name != null)
                    {{ Auth::user()->name }}
                @elseif (Auth::user()->nickname != null)
                    {{ Auth::user()->nickname }}
                @else
                    Super Admin
                @endif
            </p>
            <small><i class="fa fa-briefcase" style="color:#F5F5F5;margin-right:5px;"></i> {{ ucwords(Auth::user()->role) }}</small>
        </div>
    </div>
    <!-- sidebar menu: : style can be found in sidebar.less -->

    @include('admin.partials.left-side-menu-admin');

</section>
<!-- /.sidebar -->
