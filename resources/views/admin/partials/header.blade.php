<a href="{{ URL::to('/admin') }}" class="logo">
    <!-- Add the class icon to your logo image or logo icon to add the margining -->
    Mamikos <strong>Admin</strong>
</a>
<!-- Header Navbar: style can be found in header.less -->
<nav class="navbar navbar-static-top" role="navigation">
    <!-- Sidebar toggle button-->
    <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </a>
    <div class="navbar-right">
        <ul class="nav navbar-nav">
            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-user-circle"></i>
                    <span>{{ Auth::user()->name }} <i class="caret"></i></span>
                </a>
                <ul class="dropdown-menu">
                    <!-- User image -->
                    <li class="user-header bg-black">
                        <img src="{{ Auth::user()->getPhotoUserAttribute() }}" class="img-circle" alt="User Image" />
                        <p>
                            @if (Auth::user()->name != null)
                                {{ Auth::user()->name }}
                            @elseif (Auth::user()->nickname != null)
                                {{ Auth::user()->nickname }}
                            @else
                                Super Admin
                            @endif
                            <small>{{ ucwords(Auth::user()->role) }}</small>
                        </p>
                    </li>
                    <!-- Menu Footer-->
                    <li class="user-footer">
                        <div class="pull-left">
                            <a href="/admin/account/change-password" class="btn btn-default btn-sm"><i class="fa fa-key"></i> Ubah Password</a>
                        </div>
                        <div class="pull-right">
                            <a href="/auth/admin/logout" class="btn btn-default btn-sm">Sign Out <i class="fa fa-sign-out"></i></a>
                        </div>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</nav>