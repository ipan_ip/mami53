<ul class="sidebar-menu">
    @permission('access-kost')
    <li class="{{ activeSlug(2, 'room') || !activeSlug(3, 'ucode') }}">
        <a href="{{ URL::route('admin.room.index', ['#room']) }}" id="room">
            <i class="fa fa-home"></i>
            <span>Kost</span>
        </a>
    </li>
    @endpermission

    <li class="{{ activeSlug(2, 'giant') }}" style="">
        <a href="{{ Url::to('admin/abtest') }}">
            <i class="fa fa-users"></i>
            <span>A/B Test</span>
        </a>
    </li>

    @permission('access-kost-additional')
    <li class="{{ activeSlug(2, 'two') }}">
        <a href="{{ url('admin/room/two') }}/#room-additional" id="room-additional">
            <i class="fa fa-home"></i>
            <span>Kost (Additional)</span>
        </a>
    </li>
    @endpermission

    @permission('access-lpl-score')
    <li class="{{ activeSlug(2, 'lpl-score') }}">
        <a href="{{ URL::route('admin.lpl-score.index', ['#index']) }}" id="lpl-score">
            <i class="fa fa-sort-numeric-desc"></i>
            <span>LPL Criteria</span>
        </a>
    </li>
    @endpermission

    @permission(['access-kost', 'access-booking'])
    <li class="treeview {{ activeSlug(3, 'ucode')}}">
        <a href="javascript:void(0);" id="ucode">
            <i class="fa fa-hashtag"></i>
            <span>Booking Code</span>
        </a>
        <ul class="treeview-menu menu-open" style="display:block;">
            <li>
                <a href="{{ URL::route('admin.room.ucode.index', ['#ucode']) }}" id="ucode-list">
                    <i class="fa fa-qrcode"></i>
                    <span>Code List</span>
                </a>
            </li>
            <li>
                <a href="{{ URL::route('admin.room.ucode.setting', ['#ucode']) }}" id="ucode-setting" disabled>
                    <i class="fa fa-wrench"></i>
                    <span>Code Setting</span>
                </a>
            </li>
        </ul>
    </li>
    @endpermission

    @permission('access-kost')
    <li class="{{ activeSlug(2, 'vrtour') }}">
        <a href="{{ URL::route('admin.vrtour.index', ['#vrtour']) }}" id="vrtour">
            <i class="fa fa-home"></i>
            <span>VR Tour</span>
        </a>
    </li>
    @endpermission

    @permission('access-tag')
    <li class="treeview {{ activeSlug(2, 'tag') || activeSlug(2, 'facility') }}">
        <a href="javascript:void(0);">
            <i class="fa fa-bath"></i>
            <span>Facilities</span>
        </a>
        <ul class="treeview-menu menu-open" style="display:block;">
            <li>
                <a href="{{ URL::route('admin.facility.category.index', ['#facility-category']) }}"
                   id="facility-category">
                    <i class="fa fa-bath"></i>
                    <span>Categories</span>
                </a>
            </li>
            <li>
                <a href="{{ URL::route('admin.facility.type.index', ['#facility-type']) }}" id="facility-type">
                    <i class="fa fa-bath"></i>
                    <span>Types</span>
                </a>
            </li>
            <li>
                <a href="{{ URL::route('admin.tag.index', ['#tag-list']) }}" id="tag-list">
                    <i class="fa fa-tags"></i>
                    <span>Tags</span>
                </a>
            </li>
            <li>
                <a href="{{ URL::route('admin.tag-max-renter.index', ['#tag-max-renter']) }}" id="tag-max-renter">
                    <i class="fa fa-users"></i>
                    <span>Max Renter by Tag</span>
                </a>
            </li>
        </ul>
    </li>
    @endpermission

    <!-- Room-Classes feature ~ Used on API v1.2.2 -->
    @permission('access-room-class')
    <li class="{{ activeSlug(2, 'room-class') }}">
        <a href="{{ URL::route('admin.room-class.index', ['#kost-classes']) }}" id="kost-classes">
            <i class="fa fa-bars"></i>
            <span>Kost Classes</span>
            <i class="fa fa-exclamation-circle pull-right" style="font-size:18px;"></i>
        </a>
    </li>
    @endpermission

    @permission('access-kost')
    <li class="{{ activeSlug(2, 'house_property') }}">
        <a href="{{ URL::route('admin.house_property.index', ['#house-property']) }}" id="house-property">
            <i class="fa fa-th"></i>
            <span>House property</span>
        </a>
    </li>
    @endpermission

    @permission('access-apartment-project')
    <li class="{{ activeSlug(3, 'project') }}">
        <a href="{{ url('admin/apartment/project') }}?#apartment-project" id="apartment-project">
            <i class="fa fa-th"></i>
            <span>Apartment Project</span>
        </a>
    </li>
    @endpermission

    @permission('access-kost-level')
    <li class="treeview {{ activeSlug(2, 'kost-level') }}">
        <a href="javascript:void(0);" id="kost-level">
            <i class="fa fa-home"></i>
            <span>Level Management</span>
        </a>
        <ul class="treeview-menu menu-open" style="display:block;">
            <li>
                <a href="{{ route('admin.property.level.index') }}">
                    <i class="fa fa-wrench"></i>
                    <span>Property Level</span>
                </a>
            </li>
            <li>
                <a href="{{ URL::to('admin/kost-level/level#kost-level') }}">
                    <i class="fa fa-wrench"></i>
                    <span>Kost Level</span>
                </a>
            </li>
            <li>
                <a href="{{ URL::to('admin/kost-level/room-level#kost-level') }}">
                    <i class="fa fa-wrench"></i>
                    <span>Room Level</span>
                </a>
            </li>
            <li>
                <a href="{{ URL::to('admin/kost-level/kost-list#kost-level') }}">
                    <i class="fa fa-list"></i>
                    <span>Kost List</span>
                </a>
            </li>
            <li>
                <a href="{{ URL::to('admin/property-package/') }}">
                    <i class="fa fa-handshake-o"></i>
                    <span>Property Package</span>
                </a>
            </li>
            <li>
                <a href="{{ URL::to('admin/kost-value/') }}">
                    <i class="fa fa-money"></i>
                    <span>Kost Benefit</span>
                </a>
            </li>
            <li>
                <a href="{{ URL::to('admin/assign-kost-value/') }}">
                    <i class="fa fa-home"></i>
                    <span>Assign Benefit to Kost</span>
                </a>
            </li>
            <li>
                <a href="{{ URL::to('admin/kost-level/faq#kost-level') }}">
                    <i class="fa fa-question"></i>
                    <span>FAQ</span>
                </a>
            </li>
        </ul>
    </li>
    @endpermission

    @permission('access-kost-property')
    <li class="treeview {{ activeSlug(2, 'kost-level') }}">
        <a href="javascript:void(0);" id="kost-level">
            <i class="fa fa-home"></i>
            <span>Property Management</span>
        </a>
        <ul class="treeview-menu menu-open" style="display:block;">
            <li>
                <a href="{{ route('admin.property.index') }}">
                    <i class="fa fa-wrench"></i>
                    <span>Property</span>
                </a>
            </li>
        </ul>
    </li>
    @endpermission

    @permission('access-call')
    <li class="{{ activeSlug(2, 'call') }}">
        <a href="{{ URL::route('admin.call.index', ['#call']) }}" id="call">
            <i class="fa fa-th"></i>
            <span>Call</span>
        </a>
    </li>
    @endpermission

    @permission('access-notification')
    <li class="{{ activeSlug(2, 'wa-notification') }}">
        <a href="{{ URL::route('admin.wa-notification.index', ['#wa-notification']) }}" id="wa-notification">
            <i class="fa fa-whatsapp"></i>
            <span>WhatsApp & SMS Notification</span>
        </a>
    </li>
    @endpermission

    @permission('access-notification')
    <li class="{{ activeSlug(2, 'notification') }}">
        <a href="{{ URL::route('admin.notif.index', ['#notification']) }}" id="notification">
            <i class="fa fa-th"></i>
            <span>Notification</span>
        </a>
    </li>
    @endpermission

    @permission('access-notification')
    <li class="{{ activeSlug(2, 'notification-log') }}">
        <a href="{{ URL::route('admin.notification.log', ['#notification-log']) }}" id="notification-log">
            <i class="fa fa-th"></i>
            <span>Notification Log</span>
        </a>
    </li>
    @endpermission

    @permission('access-landing-page')
    <li class="treeview {{ activeSlug(2, 'sanjunipero') }}">
        <a href="javascript:void(0);" id="sanjunipero">
            <i class="fa fa-briefcase"></i>
            <span>Sanjunipero</span>
        </a>
        <ul class="treeview-menu menu-open" style="display:block;">
            <li>
                <a href="{{ route('admin.sanjunipero.parent.index') }}">
                    <i class="fa fa-thermometer-empty"></i>
                    <span>Parent</span>
                </a>
            </li>
            <li>
                <a href="{{ route('admin.sanjunipero.child.index') }}">
                    <i class="fa fa-thermometer-three-quarters"></i>
                    <span>Child</span>
                </a>
            </li>
        </ul>
    </li>
    @endpermission

    @permission('access-landing-page')
    <li class="{{ activeSlug(2, 'landing') }}">
        <a href="{{ URL::route('admin.landing.index', ['#landing']) }}" id="landing">
            <i class="fa fa-th"></i>
            <span>Landing Page</span>
        </a>
    </li>

    <li class="{{ activeSlug(2, 'landing-meta') }}">
        <a href="{{ URL::route('admin.landing.meta.index', ['#landing-meta']) }}" id="landing-meta">
            <i class="fa fa-th"></i>
            <span>Landing Meta + OG</span>
        </a>
    </li>

    <li class="{{ activeSlug(2, 'house_property/landing') }}">
        <a href="/admin/house_property/landing?#houseproperti-landing" id="ouseproperti-landing">
            <i class="fa fa-th"></i>
            <span>Landing House Property</span>
        </a>
    </li>
    @endpermission

    @permission('access-landing-apartment')
    <li class="{{ activeSlug(2, 'landing-apartment') }}">
        <a href="{{ URL::route('admin.landing-apartment.index', ['#landing-apartment']) }}" id="landing-apartment">
            <i class="fa fa-th"></i>
            <span>Landing Apartment</span>
        </a>
    </li>
    @endpermission

    @permission('access-landing-content')
    <li class="{{ activeSlug(2, 'landing-content') }}">
        <a href="{{ URL::route('admin.landing-content.index', ['#landing-content']) }}" id="landing-content">
            <i class="fa fa-th"></i>
            <span>Landing Content</span>
        </a>
    </li>
    @endpermission

    @permission('access-landing-jobs')
    <li class="{{ activeSlug(2, 'jobs-landing') }}">
        <a href="{{ URL::route('admin.jobs-landing.index', ['#jobs-landing']) }}" id="jobs-landing">
            <i class="fa fa-th"></i>
            <span>Landing Jobs</span>
        </a>
    </li>
    @endpermission

    @permission('access-landing-suggestion')
    <li class="{{ activeSlug(2, 'landing-suggestion') }}">
        <a href="{{ URL::route('admin.landing-suggestion.index', ['#suggestion-landing']) }}" id="suggestion-landing">
            <i class="fa fa-th"></i>
            <span>Landing Suggestion</span>
        </a>
    </li>
    @endpermission

    @permission('access-landing-booking')
    <li class="{{ activeSlug(2, 'booking-landing') }}">
        <a href="{{ URL::route('admin.booking.landing.index', ['#booking-landing']) }}" id="booking-landing">
            <i class="fa fa-map-o"></i>
            <span>Landing Booking</span>
        </a>
    </li>
    @endpermission

    @permission('access-landing-list-creator')
    <li class="{{ activeSlug(2, 'list-creator') }}">
        <a href="{{ Url::to('admin/list-creator') }}?#list-creator" id="list-creator">
            <i class="fa fa-list"></i>
            <span>Landing List Creator</span>
        </a>
    </li>
    @endpermission

    @permission('access-home-static')
    <li class="{{ activeSlug(2, 'home-static-landing') }}">
        <a href="{{ Url::to('admin/home-static-landing') }}?#home-static-landing" id="home-static-landing">
            <i class="fa fa-list"></i>
            <span>Home Static</span>
        </a>
    </li>
    @endpermission

    @permission('access-form-download-soal')
    <li class="{{ activeSlug(2, 'download-exam') }}">
        <a href="{{ URL::route('admin.download-exam.index', ['#download-exam']) }}" id="download-exam">
            <i class="fa fa-th"></i>
            <span>Form Download Soal</span>
        </a>
    </li>
    @endpermission

    @permission('access-sms-broadcast')
    <li class="{{ activeSlug(2, 'sms') }}">
        <a href="{{ URL::route('admin.sms.index', ['#sms']) }}" id="sms">
            <i class="fa fa-th"></i>
            <span>SMS Broadcast</span>
        </a>
    </li>
    @endpermission

    @permission('access-event')
    <li class="{{ activeSlug(2, 'event') }}">
        <a href="{{ URL::route('admin.event.index', ['#event']) }}" id="event">
            <i class="fa fa-th"></i>
            <span>Event</span>
        </a>
    </li>
    @endpermission

    @permission('access-notification-list')
    <li class="treeview {{ activeSlug(2, 'notification-list') }}">
        <a href="javascript:void(0);" id="notification-list">
            <i class="fa fa-bell"></i>
            <span>User Notification List</span>
        </a>
        <ul class="treeview-menu menu-open" style="display:block;">
            <li>
                <a href="{{ URL::to('admin/notification-list/category#notification-list') }}">
                    <i class="fa fa-wrench"></i>
                    <span>Notification Categories</span>
                </a>
            </li>
        </ul>
    </li>
    @endpermission

    @permission('access-promoted-kost')
    <li class="{{ activeSlug(2, 'recommendation') }}">
        <a href="{{ URL::route('admin.promoted.index', ['#promoted']) }}" id="promoted">
            <i class="fa fa-th"></i>
            <span>Promoted Kosts</span>
        </a>
    </li>
    @endpermission

    @permission('access-kost-owner')
    <li class="{{ activeSlug(2, 'owner') }}">
        <a href="{{ URL::route('admin.owner.index', ['#owner']) }}" id="owner">
            <i class="fa fa-user"></i>
            <span>Kost Owner</span>
        </a>
    </li>
    @endpermission

    <!-- Owner Loyalty feature ~ Used on API v1.5.3 -->
    @permission('access-owner-loyalty')
    <li class="{{ activeSlug(2, 'loyalty') }}">
        <a href="{{ URL::route('admin.loyalty', ['#loyalty']) }}" id="loyalty">
            <i class="fa fa-trophy"></i>
            <span>Owner Loyalty</span>
        </a>
    </li>
    @endpermission

    @permission('access-polling')
    <li class="treeview {{ activeSlug(2, 'polling') }}">
        <a href="{{ route('admin.polling.index', ['#polling']) }}" id="polling">
            <i class="fa fa-th"></i>
            <span>Polling Management</span>
        </a>
        <ul class="treeview-menu menu-open" style="display:block;">
            <li>
                <a href="{{ route('admin.polling.index', ['#polling']) }}">
                    <i class="fa fa-list"></i>
                    <span>Polling</span>
                </a>
            </li>
            <li>
                <a href="{{ route('admin.polling.question.index', ['#polling']) }}">
                    <i class="fa fa-list"></i>
                    <span>Polling Question</span>
                </a>
            </li>
            <li>
                <a href="{{ route('admin.polling.user.index', ['#polling']) }}">
                    <i class="fa fa-list"></i>
                    <span>User Polling</span>
                </a>
            </li>
        </ul>
    </li>
    @endpermission

    @permission(['access-point', 'view-point'])
    <li class="treeview {{ activeSlug(2, 'point') }}">
        <a href="javascript:void(0);" id="point">
            <i class="fa fa-trophy"></i>
            <span>Point Management</span>
        </a>
        <ul class="treeview-menu menu-open" style="display:block;">
            @permission('access-point-core')
            <li>
                <a href="{{ URL::route('admin.point.expiry.index', []).'#point' }}">
                    <i class="fa fa-wrench"></i>
                    <span>Expiry</span>
                </a>
            </li>
            <li>
                <a href="{{ URL::route('admin.point.segment.index', []).'#point' }}">
                    <i class="fa fa-wrench"></i>
                    <span>Segment</span>
                </a>
            </li>
            <li>
                <a href="{{ URL::route('admin.point.activity.index', []).'#point' }}">
                    <i class="fa fa-wrench"></i>
                    <span>Activity</span>
                </a>
            </li>
            <li>
                <a href="{{ URL::route('admin.point.room-group.index', []).'#point' }}">
                    <i class="fa fa-wrench"></i>
                    <span>Owner Room Group</span>
                </a>
            </li>
            <li>
                <a href="{{ URL::route('admin.point.blacklist.index', []).'#point' }}">
                    <i class="fa fa-wrench"></i>
                    <span>Owner Point Blacklist</span>
                </a>
            </li>
            @endpermission
            <li>
                <a href="{{ URL::route('admin.point.user.index', []).'#point' }}">
                    <i class="fa fa-users"></i>
                    <span>User Point</span>
                </a>
            </li>
            @permission('access-point')
            <li>
                <a href="{{ URL::route('admin.point.setting.index', [ 'target' => 'owner' ]).'#point' }}">
                    <i class="fa fa-wrench"></i>
                    <span>Owner Point Setting</span>
                </a>
            </li>
            <li>
                <a href="{{ URL::route('admin.point.setting.index', [ 'target' => 'tenant' ]).'#point' }}">
                    <i class="fa fa-wrench"></i>
                    <span>Tenant Point Setting</span>
                </a>
            </li>
            @endpermission
        </ul>
    </li>
    @endpermission

    @permission(['access-reward-loyalty', 'view-reward-loyalty'])
    <li class="treeview {{ activeSlug(2, 'reward-loyalty') }}">
        <a href="javascript:void(0);" id="reward-loyalty">
            <i class="fa fa-gift"></i>
            <span>Loyalty Reward Management</span>
        </a>
        <ul class="treeview-menu menu-open" style="display:block;">
            <li>
                <a href="{{ URL::route('admin.reward-loyalty.list.index', ['#reward-loyalty']) }}">
                    <i class="fa fa-list"></i>
                    <span>Reward List</span>
                </a>
            </li>
            @permission('access-reward-loyalty-core')
            <li>
                <a href="{{ URL::route('admin.reward-loyalty.type.index', ['#reward-loyalty']) }}">
                    <i class="fa fa-wrench"></i>
                    <span>Reward Type</span>
                </a>
            </li>
            @endpermission
        </ul>
    </li>
    @endpermission

    @permission('access-testimonial')
    <li class="{{ activeSlug(2, 'testimonial') }}">
        <a href="{{ URL::route('admin.testimonial.index', ['#testimonial']) }}" id="testimonial">
            <i class="fa fa-commenting-o"></i>
            <span>Owner Testimonial</span>
        </a>
    </li>
    @endpermission

    <!-- Top Owner feature ~ Used on API v1.2.2 -->
    @permission('access-top-owner')
    <li class="{{ activeSlug(2, 'top-owner') }}">
        <a href="{{ URL::route('admin.top-owner.index', ['#top-owner']) }}" id="top-owner">
            <i class="fa fa-thumbs-up"></i>
            <span>Top Owner</span>
            <i class="fa fa-exclamation-circle pull-right" style="font-size:18px;"></i>
        </a>
    </li>
    @endpermission

    <!-- Hostile Owner feature ~ Used on API v1.3.4 -->
    @permission('access-hostile-owner')
    <li class="{{ activeSlug(2, 'hostile-owner') }}">
        <a href="{{ URL::route('admin.hostile-owner.index', ['#hostile-owner']) }}" id="hostile-owner">
            <i class="fa fa-exclamation-triangle"></i>
            <span>Hostile Owner</span>
        </a>
    </li>
    @endpermission

    @permission('access-competitor')
    <li class="treeview {{ activeSlug(2, 'competitor') }}">
        <a href="javascript:void(0);" id="competitor">
            <i class="fa fa-user-secret"></i>
            <span>Competitor Management</span>
        </a>
        <ul class="treeview-menu menu-open" style="display:block;">
            <li>
                <a href="{{ URL::route('admin.competitor.user.index', []).'#competitor' }}">
                    <i class="fa fa-user"></i>
                    <span>User</span>
                </a>
            </li>
            <li>
                <a href="{{ URL::route('admin.competitor.fake-phone.index', []).'#competitor' }}">
                    <i class="fa fa-reply"></i>
                    <span>Fake Phone</span>
                </a>
            </li>
        </ul>
    </li>
    @endpermission

    <!-- Mami Checker feature ~ Used on API v1.4.2 -->
    @permission('access-mami-checker')
    <li class="{{ activeSlug(2, 'mami-checker') }}">
        <a href="{{ URL::route('admin.mami-checker.index', ['#mami-checker']) }}" id="mami-checker">
            <i class="fa fa-check-square-o"></i>
            <span>Mami Checker</span>
        </a>
    </li>
    @endpermission

    @permission('access-kost-agent')
    <li class="{{ activeSlug(2, 'igen') }}">
        <a href="{{ URL::route('admin.igen', ['#igen']) }}" id="igen">
            <i class="fa fa-th"></i>
            <span>Kost Agen</span>
        </a>
    </li>
    @endpermission

    @permission('access-kost-review')
    <li class="{{ activeSlug(2, 'review') }}">
        <a href="{{ URL::route('admin.review.index', ['#review']) }}" id="review">
            <i class="fa fa-th"></i>
            <span>Kost Review</span>
        </a>
    </li>
    @endpermission

    @permission('access-kost-report')
    <li class="{{ activeSlug(2, 'report') }}">
        <a href="{{ URL::route('admin.room.report.index', ['#room-report']) }}" id="room-report">
            <i class="fa fa-flag"></i>
            <span>Kost Report</span>
        </a>
    </li>
    @endpermission

    @permission('access-zenziva-inbox')
    <li class="{{ activeSlug(2, 'zenziva-inbox') }}">
        <a href="{{ URL::route('admin.zenziva-inbox.index', ['#zenziva']) }}" id="zenziva">
            <i class="fa fa-th"></i>
            <span>Zenziva Inbox</span>
        </a>
    </li>
    @endpermission

    @permission('access-area')
    <li class="{{ activeSlug(2, 'areas') }}">
        <a href="{{ URL::route('admin.areas.index', ['#areas']) }}" id="areas">
            <i class="fa fa-th"></i>
            <span>Area</span>
        </a>
    </li>
    @endpermission

    @permission('access-area-big-mapper')
    <li class="{{ activeSlug(2, 'area-big-mapper') }}">
        <a href="{{ URL::route('admin.area-big-mapper.index', ['#big-mapper']) }}" id="big-mapper">
            <i class="fa fa-th"></i>
            <span>Area Big Mapper</span>
        </a>
    </li>
    @endpermission

    @permission('access-geocode')
        <li class="treeview {{ activeSlug(2, 'geocode') }}" style="height: auto;">
            <a href="javascript:void(0);">
                <i class="fa fa-map-marker"></i>
                <span>Area Geocode</span>
            </a>
            <ul class="treeview-menu menu-open" style="display:block;">
                <li>
                    <a href="{{ URL::route('admin.geocode.index', ['#geocode']) }}" id="geocode">
                        <i class="fa fa-map-marker"></i>
                        <span>Master Data</span>
                    </a>
                </li>
                <li>
                    <a href="{{ URL::route('admin.geocode.mapping', ['#mapping']) }}" id="mapping">
                        <i class="fa fa-map-marker"></i>
                        <span>Mapping Data</span>
                    </a>
                </li>
            </ul>
        </li>
    @endpermission

    @permission('access-premium-account-request')
    <li class="{{ activeSlug(2, 'request') }}">
        <a href="{{ URL::route('admin.request.index', ['#request']) }}" id="request">
            <i class="fa fa-money"></i>
            <span>Premium Account Request</span>
        </a>
    </li>
    <li class="{{ activeSlug(2, 'premium-plus') }}">
        <a href="{{ URL::route('admin.premium-plus.index', ['#premium-plus']) }}" id="premium-plus">
            <i class="fa fa-money"></i>
            <span>GP4 Management</span>
        </a>
    </li>
    @endpermission

    @permission('access-pay-confirmation')
    <li class="{{ activeSlug(2, 'premium') }}">
        <a href="{{ URL::route('admin.premium.index', ['#premium']) }}" id="premium">
            <i class="fa fa-users"></i>
            <span>Pay Confirmation</span>
        </a>
    </li>
    <li class="{{ activeSlug(2, 'premium-cashback') }}">
        <a href="{{ URL::route('admin.premium-cashback.index', ['#premium-cashback']) }}" id="premium-cashback">
            <i class="fa fa-money"></i>
            <span>Premium Cashback History</span>
        </a>
    </li>
    <li class="{{ activeSlug(3, 'faq') }}">
        <a href="/admin/premium/faq?#premium-faq" id="premium-faq">
            <i class="fa fa-users"></i>
            <span>Premium FAQ</span>
        </a>
    </li>
    @endpermission

    @permission('access-kost-owner')
    <li class="treeview">
        <a href="javascript:void(0);">
            <i class="fa fa-th"></i>
            <span>MamiPAY</span>
        </a>
        <ul class="treeview-menu menu-open" style="display:block;">
            <li>
                <a href="{{ URL::route('admin.mamipay-owner.index', ['#mamipay-owner']) }}" id="mamipay-owner">
                    <i class="fa fa-bolt"></i>
                    <span>MamiPAY Owner</span>
                </a>
            </li>
            <li>
                <a href="{{ URL::route('admin.mamipay-invoice.index', ['#mamipay-invoice']) }}" id="mamipay-invoice">
                    <i class="fa fa-qrcode"></i>
                    <span>MamiPAY Paid Invoice</span>
                </a>
            </li>
        </ul>
    </li>
    @endpermission

    @permission('access-bank')
    <li class="{{ activeSlug(2, 'bank') }}">
        <a href="{{ URL::route('admin.bank.index', ['#bank']) }}" id="bank">
            <i class="fa fa-money"></i>
            <span>Bank</span>
        </a>
    </li>
    @endpermission

    @permission('access-click-pricing')
    <li class="{{ activeSlug(2, 'click-pricing') }}">
        <a href="{{ URL::route('admin.click-pricing.index', ['#click-pricing']) }}" id="click-pricing">
            <i class="fa fa-th"></i>
            <span>Click Pricing</span>
        </a>
    </li>
    @endpermission

    @permission('access-premium-package')
    <li class="{{ activeSlug(2, 'paket') }}">
        <a href="{{ URL::route('admin.paket.index', ['#paket']) }}" id="paket">
            <i class="fa fa-check"></i>
            <span>Paket Premium</span>
        </a>
    </li>
    @endpermission

    @permission('access-promo-owner')
    <li class="{{ activeSlug(2, 'promo') }}">
        <a href="{{ URL::route('admin.promo.index', ['#promo']) }}" id="promo">
            <i class="fa fa-check"></i>
            <span>Promo Owner</span>
        </a>
    </li>
    @endpermission

    @permission('access-verification-code')
    <li class="{{ activeSlug(2, 'verification') }}">
        <a href="{{ URL::route('admin.verification.index', ['#verification']) }}" id="verification">
            <i class="fa fa-check"></i>
            <span>Kode Verifikasi</span>
        </a>
    </li>
    @endpermission

    @permission('access-voucher-program')
    <li class="{{ activeSlug(2, 'voucher') }}">
        <a href="{{ URL::route('admin.voucher.index', ['#voucher']) }}" id="voucher">
            <i class="fa fa-ticket"></i>
            <span>Voucher Program</span>
        </a>
    </li>
    @endpermission

    @permission('access-notification-survey')
    <li class="{{ activeSlug(2, 'notification-survey') }}">
        <a href="{{ URL::route('admin.notification-survey.index', ['#notification-survey']) }}"
            id="notification-survey">
            <i class="fa fa-ticket"></i>
            <span>Notification Survey</span>
        </a>
    </li>
    @endpermission

    @permission('access-discount-management')
    <li class="treeview {{ activeSlug(2, 'discount-management') || activeSlug(1, 'flash-sale') }}">
        <a href="javascript:void(0);">
            <i class="fa fa-percent"></i>
            <span>Promo Ngebut</span>
        </a>
        <ul class="treeview-menu menu-open" style="display:block;">
            <li>
                <a href="{{ URL::route('admin.flash-sale.index', ['#flash-sale']) }}" id="flash-sale">
                    <i class="fa fa-percent"></i>
                    <span>Promo Ngebut Management</span>
                </a>
            </li>
            <li>
                <a href="{{ URL::route('admin.booking.discount', ['#discount-management']) }}" id="discount-management">
                    <i class="fa fa-percent"></i>
                    <span>Discount Management</span>
                </a>
            </li>
        </ul>
    </li>
    @endpermission

    @permission('access-booking')
    <li class="treeview menu-open" style="height: auto;">
        <a href="#" id="booking"><i class="fa fa-ticket"></i> Booking</a>

        <ul class="treeview-menu menu-open" style="display: block;">
            @permission('access-booking-rooms')
            {{-- <li>
                <a href="{{ URL::route('admin.booking.rooms') }}"><i class="fa fa-tags"></i> Booking Rooms</a>
            </li> --}}
            @endpermission

            @permission('access-data-booking')
            <li>
                <a
                    href="{{ URL::route('admin.booking.users', ['#booking-users']) }}"
                    id="booking-users"><i
                    class="fa fa-user"></i> Data Booking</a>
            </li>
            <li>
                <a href="/admin/booking/owner/request/#booking-owner" id="booking-owner"><i class="fa fa-user"></i>
                    Booking Owner Req</a>
            </li>
            <li>
                <a href="/admin/booking/owner/instant-booking/#instant-booking" id="instant-booking"><i
                    class="fa fa-user"></i> Owner Instant Booking</a>
            </li>
            <li>
                <a href="/admin/booking/acceptance-rate" id="booking-acceptance-rate">
                    <i class="fa fa-user"></i> Acceptance Rate
                </a>
            </li>
            <li>
                <a href="/admin/booking/reject-reason" id="booking-reject-reason">
                    <i class="fa fa-user"></i> Reject Reason
                </a>
            </li>
            <li>
                <a href="/admin/booking/monitor" id="booking-dashboard-bse">
                    <i class="fa fa-dashboard"></i> Dashboard BSE
                </a>
            </li>
            @endpermission

            @permission('access-booking-payment')
            {{-- <li>
                <a href="{{ URL::route('admin.booking.payments') }}"><i class="fa fa-money"></i> Booking Payments</a>
            </li> --}}
            @endpermission
        </ul>
    </li>
    @endpermission

    @permission('access-booking')
        <li class="{{ activeSlug(2, 'contract') }}">
            <a href="{{ URL::route('admin.contract.index', ['#contract']) }}" id="call">
                <i class="fa fa-th"></i>
                <span>Contract</span>
            </a>
        </li>
    @endpermission

    @permission('access-booking')
        <li class="{{ activeSlug(2, 'dbet-link') }}">
            <a href="{{ URL::route('admin.dbet-link.index', ['#dbet-link']) }}" id="dbet-link">
                <i class="fa fa-th"></i>
                <span>DBET Link</span>
            </a>
        </li>
    @endpermission

    @permission('access-marketplace-product')
    <li class="{{ activeSlug(2, 'marketplace') && activeSlug(3, 'product') }}">
        <a href="{{ URL::route('admin.marketplace.product', ['#marketplace-product']) }}" id="marketplace-product">
            <i class="fa fa-th"></i>
            <span>Marketplace Product</span>
        </a>
    </li>
    @endpermission

    @permission('access-reward')
    <li class="{{ activeSlug(2, 'reward') }}">
        <a href="{{ URL::route('admin.reward.index', ['#reward']) }}" id="reward">
            <i class="fa fa-th"></i>
            <span>Reward</span>
        </a>
    </li>
    @endpermission

    @permission('access-photobooth')
    <li class="{{ activeSlug(2, 'photobooth') }}">
        <a href="{{ URL::route('admin.photobooth.index', ['#photobooth']) }}" id="photobooth">
            <i class="fa fa-th"></i>
            <span>Photobooth</span>
        </a>
    </li>
    @endpermission

    @permission('access-jobs')
    <li class="{{ activeSlug(2, 'jobs') }}">
        <a href="{{ URL::route('admin.jobs.index', ['#jobs']) }}" id="jobs">
            <i class="fa fa-th"></i>
            <span>Jobs</span>
        </a>
    </li>
    @endpermission

    @permission('access-aggregator')
    <li class="{{ activeSlug(2, 'aggregator') }}">
        <a href="{{ Url::to('admin/aggregator') }}?#agregator" id="agregator">
            <i class="fa fa-random"></i>
            <span>Aggregator Partner</span>
        </a>
    </li>
    @endpermission

    @permission('access-referral')
    <li class="{{ activeSlug(2, 'referrer') }}">
        <a href="{{ URL::route('admin.referrer.index', ['#referrer']) }}" id="referrer">
            <i class="fa fa-th"></i>
            <span>Referral</span>
        </a>
    </li>
    @endpermission

    @permission('access-mca')
    <li class="{{ activeSlug(2, 'mca') }}">
        <a href="{{ URL::route('admin.mca.index', ['#mca']) }}" id="mca">
            <i class="fa fa-th"></i>
            <span>MCA</span>
        </a>
    </li>
    @endpermission

    @permission('access-mitula-config')
    <li class="{{ activeSlug(2, 'mitula') }}">
        <a href="{{ URL::route('admin.mitula.config', ['#mitula']) }}" id="mitula">
            <i class="fa fa-code"></i>
            <span>Mitula Config</span>
        </a>
    </li>
    @endpermission

    @permission(['access-chat', 'access-chat-question'], true)
    <li class="treeview">
        <a href="javascript:void(0);">
            <i class="fa fa-comment"></i>
            <span>Chat</span>
        </a>
        @endpermission
        <ul class="treeview-menu menu-open" style="display:block;">
            @permission('access-chat')
            <li class="{{ activeSlug(2, 'room/chat') }}">
                <a href="{{ URL::route('admin.room.chat', ['#chat']) }}" id="chat">
                    <i class="fa fa-comment"></i>
                    <span>Chat Room</span>
                </a>
            </li>
            @endpermission

            @permission('access-chat-question')
            <li class="{{ activeSlug(2, 'question') }}">
                <a href="{{ URL::route('admin.question.index', ['#question']) }}" id="question">
                    <i class="fa fa-comment"></i>
                    <span>Chat Questions</span>

                </a>
            </li>
            @endpermission
        </ul>
        @permission(['access-chat', 'access-chat-question'], true)
    </li>
    @endpermission

    @permission('access-forum')
    <li class="treeview">
        <a href="#">
            <i class="fa fa-th"></i>
            <span>Forum</span>
            <span class="pull-right-container">
            </span>
        </a>

        <ul class="treeview-menu menu-open" style="display: block;">
            @permission('access-forum-user')
            <li>
                <a href="{{ URL::route('admin.forum.user.senior-list', ['#forum']) }}" id="forum">
                    <i class="fa fa-circle-o"></i>
                    User Kakak
                </a>
            </li>
            @endpermission

            @permission('access-forum-category')
            <li>
                <a href="{{ URL::route('admin.forum-category.index', ['#forum-category']) }}" id="forum-category">
                    <i class="fa fa-circle-o"></i>
                    Kategori
                </a>
            </li>
            @endpermission

            @permission('access-forum-thread')
            <li>
                <a href="{{ URL::route('admin.forum-thread.index', ['#forum-thread']) }}" id="forum-thread">
                    <i class="fa fa-circle-o"></i>
                    Pertanyaan
                </a>
            </li>
            @endpermission

            @permission('access-forum-report')
            <li>
                <a href="{{ URL::route('admin.forum-report.index', ['#forum-report']) }}" id="forum-report">
                    <i class="fa fa-circle-o"></i>
                    Laporan
                </a>
            </li>
            @endpermission
        </ul>
    </li>
    @endpermission

    @permission('owner-survey')
    <li class="{{ activeSlug(2, 'survey') }}">
        <a href="{{ URL::to('admin/survey/owner') }}?#survey" id="survey">
            <i class="fa fa-th"></i>
            <span>Owner Survey</span>
        </a>
    </li>
    @endpermission

    @permission('access-agents')
    <li class="{{ activeSlug(2, 'agents') }}">
        <a href="{{ Url::to('admin/agents') }}?#agent" id="agent">
            <i class="fa fa-user"></i>
            <span>Agents</span>
        </a>
    </li>
    @endpermission

    @permission('access-consultant')
    <li class="treeview {{ activeSlug(4, 'consultant') }}">
        <a href="javascript:void(0);">
            <i class="fa fa-hashtag"></i>
            <span>Consultant</span>
        </a>
        <ul class="treeview-menu menu-open" style="display:block;">
            <li>
                <a href="{{ URL::route('admin.consultant.index', ['#consultant']) }}" id="consultant">
                    <i class="fa fa-home"></i>
                    <span>Consultant Profile</span>
                </a>
            </li>
            <li>
                <a href="{{ URL::route('admin.consultant.mapping.index', ['#consultant-mapping']) }}"
                   id="consultant-mapping">
                    <i class="fa fa-wrench"></i>
                    <span>Consultant Mapping</span>
                </a>
            </li>
            <li>
                <a href="{{ URL::route('admin.consultant.potential-tenant.index', ['#potential-tenant']) }}"
                   id="potential-tenant">
                    <i class="fa fa-address-book"></i>
                    <span>Potential Tenant</span>
                </a>
            </li>
            <li>
                <a href="{{ URL::route('admin.consultant.potential-owner.index') }}"
                   id="potential-owner">
                    <i class="fa fa-users"></i>
                    <span>Potential owner</span>
                </a>
            </li>
            <li>
                <a href="{{ URL::route('admin.consultant.room-mapping.index') }}" id="consultant-room-mapping">
                    <i class="fa fa-street-view"></i>
                    <span>Consultant Kost Mapping</span>
                </a>
            </li>
            <li>
                <a href="{{ URL::route('admin.consultant.activity-management.index') }}" id="consultant-activity-management">
                    <i class="fa fa-tasks"></i>
                    <span>Activity Management</span>
                </a>
            </li>
            <li>
                <a href="{{ URL::route('admin.consultant.sales-motion.index') }}" id="consultant-sales-motion">
                    <i class="fa fa-line-chart"></i>
                    <span>Sales Motion</span>
                </a>
            </li>
        </ul>
    </li>
    @endpermission

    @permission('access-cs')
    <li class="treeview {{ activeSlug(4, 'cs-admin') || activeSlug(4, 'ucode-setting') }}">
        <a href="javascript:void(0);">
            <i class="fa fa-hashtag"></i>
            <span>CS Admin</span>
        </a>
        <ul class="treeview-menu menu-open" style="display:block;">
            <li>
                <a href="{{ URL::route('admin.cs-admin.index', ['#cs-admin']) }}" id="cs-admin">
                    <i class="fa fa-user-md"></i>
                    <span>CS Admin</span>
                </a>
            </li>
            <li>
                <a href="{{ URL::route('admin.chat.config', ['#chat-config']) }}" id="chat-config">
                    <i class="fa fa-comment"></i>
                    <span>CS Status</span>
                </a>
            </li>
        </ul>
    </li>
    @endpermission

    @permission(['access-role', 'access-permission'], true)
    <li class="treeview {{ activeSlug(4, 'cs-admin') || activeSlug(4, 'ucode-setting') }}">
        <a href="javascript:void(0);">
            <i class="fa fa-hashtag"></i>
            <span>Roles and Permissions</span>
        </a>
        @endpermission
        <ul class="treeview-menu menu-open" style="display:block;">
            @permission('access-role')
            <li class="{{ activeSlug(2, 'role') }}">
                <a href="{{ Url::to('admin/role') }}?#role" id="role">
                    <i class="fa fa-lock"></i>
                    <span>Roles</span>
                </a>
            </li>
            @endpermission

            @permission('access-permission')
            <li class="{{ activeSlug(2, 'permission') }}">
                <a href="{{ Url::to('admin/permission') }}?#permission" id="permission">
                    <i class="fa fa-lock"></i>
                    <span>Permissions</span>
                </a>
            </li>
            @endpermission
        </ul>
        @permission(['access-role', 'access-permission'], true)
    </li>
    @endpermission

    <li class="treeview {{ activeSlug(2, 'change-password') }}">
        <a href="{{ Url::to('admin/account/change-password') }}?#change-passowrd" id="change-passowrd">
            <i class="fa fa-user"></i>
            <span>Account</span>
        </a>
        @permission('user-account')
        <ul class="treeview-menu menu-open" style="display:block;">
            <li>
                <a href="{{ Url::to('admin/account/users') }}" id="user-account">
                    <i class="fa fa-user"></i>
                    <span>Users</span>
                </a>
            </li>
        </ul>
        @endpermission
    </li>

    @permission('access-testing')
    <li class="treeview {{ activeSlug(2, 'testing') }}">
        <a href="javascript:void(0);">
            <i class="fa fa-user"></i>
            <span>Testing</span>
        </a>
        <ul class="treeview-menu menu-open" style="display:block;">
            <li class="{{ activeSlug(2, 'role') }}">
                <a href="{{ Url::to('admin/testing/testers') }}" id="testing-testers">
                    <i class="fa fa-user"></i>
                    <span>Testers</span>
                </a>
            </li>
        </ul>
    </li>
    @endpermission

    @permission('access-verification-id')
    <li class="{{ activeSlug(2, 'verification-identity') }}">
        <a href="{{ Url::to('admin/account/verification-identity-card') }}?#verification-identity"
           id="verification-identity">
            <i class="fa fa-user"></i>
            <span>User Verification Identitiy Card</span>
        </a>
    </li>
    @endpermission

    @permission('access-shortlink')
    <li class="{{ activeSlug(2, 'shortlink') }}" style="">
        <a href="{{ Url::to('admin/shortlink') }}?#shortlink" id="shortlink">
            <i class="fa fa-home"></i>
            <span>Shortlink</span>
        </a>
    </li>
    @endpermission

    <li class="{{ activeSlug(2, 'whitelist-features') }}">
        <a href="{{ URL::to('admin/whitelist-features') }}">
            <i class="fa fa-wrench"></i>
            <span>Whitelist Features</span>
        </a>
    </li>

    @permission('access-slider')
    <li class="{{ activeSlug(2, 'slider') }}" style="">
        <a href="{{ Url::to('admin/slider') }}?#index" id="index">
            <i class="fa fa-sliders"></i>
            <span>Sliders</span>
        </a>
    </li>
    @endpermission

    @permission(['access-career'])
    <li class="treeview {{ activeSlug(4, 'ucode-list') || activeSlug(4, 'ucode-setting') }}">
        <a href="javascript:void(0);">
            <i class="fa fa-hashtag"></i>
            <span>Career Page</span>
        </a>
        <ul class="treeview-menu menu-open" style="display:block;">
            <li>
                <a href="{{ URL::route('admin.career.location.index', ['#career-location']) }}" id="career-location">
                    <i class="fa fa-hashtag"></i>
                    <span>Location</span>
                </a>
            </li>

            <li>
                <a href="{{ URL::route('admin.career.position.index', ['#career-postition']) }}" id="career-position">
                    <i class="fa fa-hashtag"></i>
                    <span>Position</span>
                </a>
            </li>

            <li>
                <a href="{{ URL::route('admin.career.vacancy.index', ['#career-vacancy']) }}" id="career-vacancy">
                    <i class="fa fa-hashtag"></i>
                    <span>Vacancy</span>
                </a>
            </li>
        </ul>
    </li>
    @endpermission

    @permission('access-patrick-kost')
    <li class="{{ activeSlug(2, 'patrick') }}">
        <a href="{{ Url::to('admin/patrick/list') }}?#patrick-kost" id="patrick-kost">
            <i class="fa fa-ticket"></i>
            <span>Patrick Project Kos</span>
        </a>
    </li>
    <li class="{{ activeSlug(3, 'house_property') }}">
        <a href="{{ Url::to('admin/patrick/house_property/list') }}?#patrick-houseproperty" id="patrick-houseproperty">
            <i class="fa fa-ticket"></i>
            <span>Patrick Project House</span>
        </a>
    </li>
    @endpermission

    @permission('access-patrick-apartment')
    <li class="{{ activeSlug(3, 'apartemen') }}">
        <a href="{{ Url::to('admin/patrick/apartemen/list') }}?#patrick-apartemen" id="patrick-apartemen">
            <i class="fa fa-ticket"></i>
            <span>Patrick Project Apartemen</span>
        </a>
    </li>
    @endpermission

    @permission('access-patrick-vacancy')
    <li class="{{ activeSlug(3, 'vacancy') }}">
        <a href="{{ Url::to('admin/patrick/vacancy/list') }}?#patrick-vacancy" id="patrick-vacancy">
            <i class="fa fa-ticket"></i>
            <span>Patrick Project Vacancy</span>
        </a>
    </li>
    @endpermission

    @permission('access-agents')
    <li class="{{ activeSlug(2, 'giant') }}" style="">
        <a href="{{ Url::to('admin/giant/data') }}?#giant" id="giant">
            <i class="fa fa-home"></i>
            <span>Agent App</span>
        </a>
    </li>
    @endpermission

    @permission('access-gold-plus')
    <li class="treeview {{ activeSlug(2, 'gold-plus') }}">
        <a href="javascript:void(0);">
            <i class="fa fa-plus"></i>
            <span>Gold Plus</span>
        </a>
        <ul class="treeview-menu menu-open" style="display:block;">
            <li class="">
                <a href="/admin/gold-plus" id="role">
                    <i class="fa fa-angle-right"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            @permission('access-gold-plus-package')
            <li class="">
                <a href="/admin/gold-plus/package" id="gold-plus-package">
                    <i class="fa fa-archive"></i>
                    <span>Package</span>
                </a>
            </li>
            @endpermission
        </ul>
    </li>
    @endpermission

    {{-- Empty list with margin to handle the cropped view --}}
    <li style="margin-bottom: 50px"></li>
</ul>
