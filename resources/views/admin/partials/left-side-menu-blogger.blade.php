<ul class="sidebar-menu">
	<li class="{{ activeSlug(3, 'project') }}">
	    <a href="{{ url('admin/apartment/project') }}">
	        <i class="fa fa-th"></i>
	        <span>Apartment Project</span>
	    </a>
	</li>

    <li class="{{ activeSlug(2, 'landing') }}">
        <a href="{{ URL::route('admin.landing.index') }}">
            <i class="fa fa-th"></i>
            <span>Landing Page</span>
        </a>
    </li>
    <li class="{{ activeSlug(2, 'landing-apartment') }}">
        <a href="{{ URL::route('admin.landing-apartment.index') }}">
            <i class="fa fa-th"></i>
            <span>Landing Apartment</span>
        </a>
    </li>
    <li class="{{ activeSlug(2, 'jobs-landing') }}">
        <a href="{{ URL::route('admin.jobs-landing.index') }}">
            <i class="fa fa-th"></i>
            <span>Landing Jobs</span>
        </a>
    </li>
</ul>
