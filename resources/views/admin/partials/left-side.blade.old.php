<!-- sidebar: style can be found in sidebar.less -->
<section class="sidebar">

    <!-- Sidebar user panel -->
    <div class="user-panel">
        <div class="pull-left image">
            <img src="{{ asset('assets/vendor/admin-lte/img/avatar3.png') }}" class="img-circle" alt="User Image" />
        </div>
        <div class="pull-left info">
            <p>Hello, {{ $user['nick_name'] }}</p>

            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
    </div>
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
        <li class="{{ HTML::activeSlug(2, 'dashboard') }}">
            <a href="{{ URL::route('admin.dashboard.index') }}">
                <i class="fa fa-dashboard"></i> <span>{{ Lang::get('hairclick.menu.dashboard') }}</span>
            </a>
        </li>
        <li class="{{ HTML::activeSlug(2, 'stories') }}">
            <a href="{{ URL::route('admin.stories.index') }}">
                <i class="fa fa-th"></i>
                <span>{{ Lang::get('hairclick.menu.stories') }}</span>
            </a>
        </li>
        <li class="{{ HTML::activeSlug(2, 'user') }}">
            <a href="{{ URL::route('admin.user.index') }}">
                <i class="fa fa-th"></i>
                <span>{{ Lang::get('hairclick.menu.users') }}</span>
            </a>
        </li>
        <li class="{{ HTML::activeSlug(2, 'shop') }}">
            <a href="{{ URL::route('admin.shop.index') }}">
                <i class="fa fa-th"></i>
                <span>{{ Lang::get('hairclick.menu.shop') }}</span>
            </a>
        </li>
        <li class="{{ HTML::activeSlug(2, 'tag') }}">
            <a href="{{ URL::route('admin.tag.index') }}">
                <i class="fa fa-th"></i>
                <span>{{ Lang::get('hairclick.menu.tags') }}</span>
            </a>
        </li>
        <li class="treeview {{ HTML::activeSlug(2, array('area', 'region')) }}">
            <a href="#">
                <i class="fa fa-th"></i>
                <span>{{ Lang::get('hairclick.menu.geography') }}</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li class="{{ HTML::activeSlug(2, 'area') }}">
                    <a href="{{ URL::route('admin.area.index') }}"><i class="fa fa-angle-double-right"></i> Area</a></li>
                <li class="{{ HTML::activeSlug(2, 'region') }}">
                    <a href="{{ URL::route('admin.region.index') }}"><i class="fa fa-angle-double-right"></i> Region</a></li>
            </ul>
        </li>
        <li class="{{ HTML::activeSlug(2, 'notif') }}">
            <a href="{{ URL::route('admin.notif.index') }}">
                <i class="fa fa-th"></i>
                <span>{{ Lang::get('hairclick.menu.notif') }}</span>
            </a>
        </li>
        <li class="{{ HTML::activeSlug(2, 'notice') }}">
            <a href="{{ URL::route('admin.notice.index') }}">
                <i class="fa fa-th"></i>
                <span>{{ Lang::get('hairclick.menu.notice') }}</span>
            </a>
        </li>
        <li class="{{ HTML::activeSlug(2, 'event') }}">
            <a href="{{ URL::route('admin.event.index') }}">
                <i class="fa fa-th"></i>
                <span>{{ Lang::get('hairclick.menu.event') }}</span>
            </a>
        </li>
        <li class="{{ HTML::activeSlug(2, 'verify_style') }}">
            <a href="{{ URL::route('admin.style.index') }}">
                <i class="fa fa-th"></i>
                <span>Verify Style</span>
            </a>
        </li>
        <li class="{{ HTML::activeSlug(2, 'booking') }}">
            <a href="{{ URL::route('admin.booking.index') }}">
                <i class="fa fa-th"></i>
                <span>Booking</span>
            </a>
        </li>  
    </ul>
</section>
<!-- /.sidebar -->