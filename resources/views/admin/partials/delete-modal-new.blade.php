<!-- Modal -->
<div class="modal fade" id="modalDelete">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Delete Confirmation</h4>
      </div>
      <div class="modal-body">
        <p>Are you sure you want to delete
            <strong>@{{ deleteResource }}</strong>
            <strong>@{{ deleteName }}</strong>
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        {{ Form::open(array('method' => 'DELETE', 'url' => 'deleteUrl', 'style' => 'display: inline-block;', 'id' => 'formDeleteId')) }}
            <button type="submit" class="btn btn-primary">Yes</button>
        {{ Form::close() }}
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- /.modal -->