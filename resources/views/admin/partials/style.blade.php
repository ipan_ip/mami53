<!-- Stylesheet
================================================== -->

    <!-- Bootstrap Core 3.2.0 -->
    <link media="all" type="text/css" rel="stylesheet" href='{{      url('assets/vendor/bootstrap/css/bootstrap.min.css') }}' />
    <link media="all" type="text/css" rel="stylesheet" href='{{      url('assets/vendor/bootstrap/css/bootstrap-theme.min.css') }}' />
    <!-- Bootstrap Theme 3.2.0 -->
    <!-- <link media="all" type="text/css" rel="stylesheet" href='{{      url('assets/vendor/bootstrap/css/bootstrap-theme.min.css') }} /> ) }}'-->

    <!-- font Awesome -->
    <link media="all" type="text/css" rel="stylesheet" href='{{      url('assets/vendor/admin-lte/css/font-awesome.min.css') }}' />
    <!-- v4.7.0 -->
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>

    <!-- bootstrap-wysihtml5 -->
    <link media="all" type="text/css" rel="stylesheet" href='{{      url('assets/vendor/admin-lte/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}' />

    <!-- Ionicons -->
    <link media="all" type="text/css" rel="stylesheet" href='{{      url('assets/vendor/admin-lte/css/ionicons.min.css') }}' />

    <!-- DATA TABLES -->
    <link media="all" type="text/css" rel="stylesheet" href='{{      url('assets/vendor/admin-lte/css/datatables/dataTables.bootstrap.css') }}' />

    <!-- Theme style -->
    <link media="all" type="text/css" rel="stylesheet" href='{{      url('assets/vendor/admin-lte/css/AdminLTE.css') }}' />

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('favicon-ico.png') }}">

    <!-- Bootstrap Validator -->
    <link media="all" type="text/css" rel="stylesheet" href='{{      url('assets/vendor/bootstrap-validator/css/bootstrapValidator.min.css') }}' />

    <!-- Main CSS -->
    <link media="all" type="text/css" rel="stylesheet" href='{{      url('assets/main/css/index.css') }}' />

    <script src='{{ url('assets/vendor/jquery/jquery.min.js') }}'></script>

        <!-- <link rel="stylesheet" href="{{ url('http://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css') }}"> -->
        <link rel="stylesheet" href="{{ url('assets/vendor/jquery-ui-1.11.4/jquery-ui.min.css') }}">
        <link rel="stylesheet" href="{{ url('assets/vendor/jquery-ui-1.11.4/jquery-ui.theme.min.css') }}">
        <!-- <script src="https://code.jquery.com/ui/1.11.1/jquery-ui.js"></script> -->
        <script src="{{ url('assets/vendor/jquery-ui-1.11.4/jquery-ui.min.js') }}"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <!-- Custom Stylesheet -->
    @if ( ! empty($asset['stylesheet']) )
        @foreach ($asset['stylesheet'] as $stylesheet)
        @endforeach
    @endif

    <style>
        .header {
	        position: fixed !important;
        }
        .sidebar-offcanvas {
            position: fixed;
        }
        .sidebar {
            overflow-y: auto;
            height: 100vh;
        }
        .sidebar > .sidebar-menu > li.active > a {
            background: none repeat scroll 0% 0% #109E4B !important;
        }

        ::-webkit-scrollbar {
            width: 7px;
        }

        ::-webkit-scrollbar-thumb {
            background: #109E4B !important;
        }
    </style>
