<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Mamikos Admin v1.0 | Dashboard</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        {{-- {{ HTML::style('packages/admin-lte/css/bootstrap.min.css') }} --}}
        <!-- bootstrap 3.1.1 -->
        {{ HTML::style('packages/bootstrap/bootstrap-3.1.1/css/bootstrap.min.css') }}
        <!-- font Awesome -->
        {{ HTML::style('packages/admin-lte/css/font-awesome.min.css') }}
        <!-- Ionicons -->
        {{ HTML::style('packages/admin-lte/css/ionicons.min.css') }}
        <!-- DATA TABLES -->
        {{ HTML::style('packages/admin-lte/css/datatables/dataTables.bootstrap.css') }}
        <!-- Theme style -->
        {{ HTML::style('packages/admin-lte/css/AdminLTE.css') }}
        <!-- Fonts Lato Regular -->
        <!-- {{ HTML::style('packages/fonts/lato/Lato-Regular.ttf') }} -->
        <!-- Favicon -->
        <link rel="shortcut icon" href="{{ 'assets/main/img/favicon.png?v=3' }}">
        <!-- Bootstrap Validator -->
        {{ HTML::style('packages/bootstrap/bootstrapvalidator-0.4.5/dist/css/bootstrapValidator.min.css') }}
        <!-- Custom CSS -->
        {{ HTML::style('main.css') }}



        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-black">
        <!-- header logo: style can be found in header.less -->
        <header class="header">
            <a href="../index.html" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                Bussiness Simulation
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                <span>{{ $user['name'] }} <i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header bg-light-blue">
                                    <img src="{{ asset('packages/admin-lte/img/avatar3.png') }}" class="img-circle" alt="User Image" />
                                    <p>
                                        {{ $user['name'] }} - Web Developer
                                        <small>Member since May. 2014</small>
                                    </p>
                                </li>
                                <!-- Menu Body -->
                                <li class="user-body">
                                    <div class="col-xs-4 text-center">
                                        <a href="#">Followers</a>
                                    </div>
                                    <div class="col-xs-4 text-center">
                                        <a href="#">Sales</a>
                                    </div>
                                    <div class="col-xs-4 text-center">
                                        <a href="#">Friends</a>
                                    </div>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="#" class="btn btn-default btn-flat">Profile</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="{{ url('auth/logout') }}" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <div class="sidebar-logo">
                        <img src="{{ asset('logo-white-transparent.png') }}" alt="" width="64px">
                    </div>
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="{{ asset('packages/admin-lte/img/avatar3.png') }}" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p>Hello, {{ $user['nick_name'] }}</p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li class="active">
                            <a href="../index.html">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                            </a>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-th"></i>
                                <span>Participants</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="charts/morris.html"><i class="fa fa-angle-double-right"></i> Users</a></li>
                                <li><a href="charts/flot.html"><i class="fa fa-angle-double-right"></i> Groups</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-laptop"></i>
                                <span>UI Elements</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="UI/general.html"><i class="fa fa-angle-double-right"></i> General</a></li>
                                <li><a href="UI/icons.html"><i class="fa fa-angle-double-right"></i> Icons</a></li>
                                <li><a href="UI/buttons.html"><i class="fa fa-angle-double-right"></i> Buttons</a></li>
                                <li><a href="UI/sliders.html"><i class="fa fa-angle-double-right"></i> Sliders</a></li>
                                <li><a href="UI/timeline.html"><i class="fa fa-angle-double-right"></i> Timeline</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-edit"></i> <span>Forms</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="forms/general.html"><i class="fa fa-angle-double-right"></i> General Elements</a></li>
                                <li><a href="forms/advanced.html"><i class="fa fa-angle-double-right"></i> Advanced Elements</a></li>
                                <li><a href="forms/editors.html"><i class="fa fa-angle-double-right"></i> Editors</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-table"></i> <span>Tables</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="tables/simple.html"><i class="fa fa-angle-double-right"></i> Simple tables</a></li>
                                <li><a href="tables/data.html"><i class="fa fa-angle-double-right"></i> Data tables</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="calendar.html">
                                <i class="fa fa-calendar"></i> <span>Calendar</span>
                                <small class="badge pull-right bg-red">3</small>
                            </a>
                        </li>
                        <li>
                            <a href="mailbox.html">
                                <i class="fa fa-envelope"></i> <span>Mailbox</span>
                                <small class="badge pull-right bg-yellow">12</small>
                            </a>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-folder"></i> <span>Examples</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="examples/invoice.html"><i class="fa fa-angle-double-right"></i> Invoice</a></li>
                                <li><a href="examples/login.html"><i class="fa fa-angle-double-right"></i> Login</a></li>
                                <li><a href="examples/register.html"><i class="fa fa-angle-double-right"></i> Register</a></li>
                                <li><a href="examples/lockscreen.html"><i class="fa fa-angle-double-right"></i> Lockscreen</a></li>
                                <li><a href="examples/404.html"><i class="fa fa-angle-double-right"></i> 404 Error</a></li>
                                <li><a href="examples/500.html"><i class="fa fa-angle-double-right"></i> 500 Error</a></li>
                                <li><a href="examples/blank.html"><i class="fa fa-angle-double-right"></i> Blank Page</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-folder"></i>  Multilevel Menu
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>

                            <ul class="treeview-menu">
                                <li class="treeview">
                                    <a href="#">
                                        First level
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </a>

                                    <ul class="treeview-menu">
                                        <li class="treeview">
                                            <a href="#">
                                                Second level
                                                <i class="fa fa-angle-left pull-right"></i>
                                            </a>

                                            <ul class="treeview-menu">
                                                <li>
                                                    <a href="#">Third level</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Dashboard
                        <small>Control panel</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            @if(Session::get('message'))
                            <p>{{ Session::get('message') }}</p>
                            @endif

                            <!-- table -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">List Users</h3>
                                    <div class="box-tools pull-right">
                                        <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                                        <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
                                    </div>

                                </div><!-- /.box-header -->
                                <div class="box-body no-padding">
                                    <div class="horizontal-wrapper">
                                        <div class="btn-horizontal-group bg-default">
                                            <button type="submit" class="btn-add btn btn-primary btn-sm">
                                                <i class="fa fa-plus">&nbsp;</i>Add Users</button>
                                        </div>
                                    </div>
                                    <div style="padding: 10px;">Something</div>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->

                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">List Users</h3>
                                    <div class="box-tools pull-right">
                                        <button type="submit" class="btn-add btn btn-primary btn-sm">
                                                <i class="fa fa-plus">&nbsp;&nbsp;</i>Add Users</button>
                                    </div>

                                </div><!-- /.box-header -->
                                <div class="box-body no-padding">
                                    <div style="padding: 10px;">Something</div>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->

                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">List Users</h3>
                                    <div class="box-tools pull-right">
                                        <button type="submit" class="btn-add btn btn-primary btn-sm">
                                                <i class="fa fa-plus">&nbsp;&nbsp;</i>Add Users</button>
                                        <div class="divider">&nbsp;</div>
                                        <button class="btn btn-primary btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        <button class="btn btn-primary btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>

                                </div><!-- /.box-header -->
                                <div class="box-body no-padding">
                                    <div style="padding: 10px;">Something</div>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->

                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">List Users</h3>
                                    <div class="box-tools pull-right">
                                        <button type="submit" class="btn-add btn btn-primary btn-sm">
                                                <i class="fa fa-plus">&nbsp;&nbsp;</i>Add Users</button>
                                        <button class="btn btn-primary btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        <button class="btn btn-primary btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>

                                </div><!-- /.box-header -->
                                <div class="box-body no-padding">
                                    <div style="padding: 10px;">Something</div>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->


                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">List Users</h3>
                                    <div class="box-tools pull-right">
                                        <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                                        <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
                                        <button type="submit" class="btn-add btn btn-primary btn-sm">
                                                <i class="fa fa-plus">&nbsp;&nbsp;</i>Add Users</button>
                                                <div class="divider">&nbsp;</div>
                                        <button class="btn btn-primary btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        <button class="btn btn-primary btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>

                                </div><!-- /.box-header -->
                                <div class="box-body no-padding">
                                    <div class="horizontal-wrapper">
                                        <div class="btn-horizontal-group bg-default">
                                            <button type="submit" class="btn-add btn btn-primary btn-sm">
                                                <i class="fa fa-plus">&nbsp;</i>Add Users</button>
                                        </div>
                                    </div>

                                    <table id="example1" class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th width="30px;">No</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Group</th>
                                                <th class="table-action-column" width="100px">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1.</td>
                                                <td>Faisal Ahmad Sulhan</td>
                                                <td>faisalsulhan@gmail.com</td>
                                                <td><a href="#">Gemastik 7</a></td>
                                                <td class="table-action-column">
                                                    <div class="btn-action-group">
                                                        <a href=""><i class="fa fa-file-text fa-info"></i></a>
                                                        <a href=""><i class="hide fa fa-pencil"></i></a>
                                                        <a href=""><i class="fa fa-trash-o"></i></a>
                                                        <a href=""><i class="fa fa-check-square-o"></i></a>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>2.</td>
                                                <td>Faisal Ahmad Sulhan</td>
                                                <td>faisalsulhan@gmail.com</td>
                                                <td><a href="#">Gemastik 7</a></td>
                                                <td class="table-action-column">
                                                    <div class="btn-action-group">
                                                        <a href=""><i class="fa fa-file-text fa-info"></i></a>
                                                        <a href=""><i class="fa fa-pencil"></i></a>
                                                        <a href=""><i class="fa fa-trash-o"></i></a>
                                                        <a href=""><i class="fa fa-ban"></i></a>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>3.</td>
                                                <td>Faisal Ahmad Sulhan</td>
                                                <td>faisalsulhan@gmail.com</td>
                                                <td><a href="#">Gemastik 7</a></td>
                                                <td class="table-action-column">
                                                    <div class="btn-action-group">
                                                        <a href=""><i class="fa fa-file-text fa-info"></i></a>
                                                        <a href=""><i class="fa fa-pencil"></i></a>
                                                        <a href=""><i class="fa fa-trash-o"></i></a>
                                                        <a href=""><i class="fa fa-ban"></i></a>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>4.</td>
                                                <td>Faisal Ahmad Sulhan</td>
                                                <td>faisalsulhan@gmail.com</td>
                                                <td><a href="#">Gemastik 7</a></td>
                                                <td class="table-action-column">
                                                    <div class="btn-action-group">
                                                        <a href=""><i class="fa fa-file-text fa-info"></i></a>
                                                        <a href=""><i class="fa fa-pencil"></i></a>
                                                        <a href=""><i class="fa fa-trash-o"></i></a>
                                                        <a href=""><i class="fa fa-ban"></i></a>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>5.</td>
                                                <td>Faisal Ahmad Sulhan</td>
                                                <td>faisalsulhan@gmail.com</td>
                                                <td><a href="#">Gemastik 7</a></td>
                                                <td class="table-action-column">
                                                    <div class="btn-action-group">
                                                        <a href=""><i class="fa fa-file-text fa-info"></i></a>
                                                        <a href=""><i class="fa fa-pencil"></i></a>
                                                        <a href=""><i class="fa fa-trash-o"></i></a>
                                                        <a href=""><i class="fa fa-check-square-o"></i></a>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>6.</td>
                                                <td>Faisal Ahmad Sulhan</td>
                                                <td>faisalsulhan@gmail.com</td>
                                                <td><a href="#">Gemastik 7</a></td>
                                                <td class="table-action-column">
                                                    <div class="btn-action-group">
                                                        <a href=""><i class="fa fa-file-text fa-info"></i></a>
                                                        <a href=""><i class="fa fa-pencil"></i></a>
                                                        <a href=""><i class="fa fa-trash-o"></i></a>
                                                        <a href=""><i class="fa fa-check-square-o"></i></a>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>7.</td>
                                                <td>Faisal Ahmad Sulhan</td>
                                                <td>faisalsulhan@gmail.com</td>
                                                <td><a href="#">Gemastik 7</a></td>
                                                <td class="table-action-column">
                                                    <div class="btn-action-group">
                                                        <a href=""><i class="fa fa-file-text fa-info"></i></a>
                                                        <a href=""><i class="fa fa-pencil"></i></a>
                                                        <a href=""><i class="fa fa-trash-o"></i></a>
                                                        <a href=""><i class="fa fa-ban"></i></a>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>8.</td>
                                                <td>Faisal Ahmad Sulhan</td>
                                                <td>faisalsulhan@gmail.com</td>
                                                <td><a href="#">Gemastik 7</a></td>
                                                <td class="table-action-column">
                                                    <div class="btn-action-group">
                                                        <a href=""><i class="fa fa-file-text fa-info"></i></a>
                                                        <a href=""><i class="fa fa-pencil"></i></a>
                                                        <a href=""><i class="fa fa-trash-o"></i></a>
                                                        <a href=""><i class="fa fa-check-square-o"></i></a>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>9.</td>
                                                <td>Faisal Ahmad Sulhan</td>
                                                <td>faisalsulhan@gmail.com</td>
                                                <td><a href="#">Gemastik 7</a></td>
                                                <td class="table-action-column">
                                                    <div class="btn-action-group">
                                                        <a href=""><i class="fa fa-file-text fa-info"></i></a>
                                                        <a href=""><i class="fa fa-pencil"></i></a>
                                                        <a href=""><i class="fa fa-trash-o"></i></a>
                                                        <a href=""><i class="fa fa-check-square-o"></i></a>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>10.</td>
                                                <td>Faisal Ahmad Sulhan</td>
                                                <td>faisalsulhan@gmail.com</td>
                                                <td><a href="#">Gemastik 7</a></td>
                                                <td class="table-action-column">
                                                    <div class="btn-action-group">
                                                        <a href=""><i class="fa fa-file-text fa-info"></i></a>
                                                        <a href=""><i class="fa fa-pencil"></i></a>
                                                        <a href=""><i class="fa fa-trash-o"></i></a>
                                                        <a href=""><i class="fa fa-check-square-o"></i></a>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>11.</td>
                                                <td>Faisal Ahmad Sulhan</td>
                                                <td>faisalsulhan@gmail.com</td>
                                                <td><a href="#">Gemastik 7</a></td>
                                                <td class="table-action-column">
                                                    <div class="btn-action-group">
                                                        <a href=""><i class="fa fa-file-text fa-info"></i></a>
                                                        <a href=""><i class="fa fa-pencil"></i></a>
                                                        <a href=""><i class="fa fa-trash-o"></i></a>
                                                        <a href=""><i class="fa fa-check-square-o"></i></a>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>12.</td>
                                                <td>Faisal Ahmad Sulhan</td>
                                                <td>faisalsulhan@gmail.com</td>
                                                <td><a href="#">Gemastik 7</a></td>
                                                <td class="table-action-column">
                                                    <div class="btn-action-group">
                                                        <a href=""><i class="fa fa-file-text fa-info"></i></a>
                                                        <a href=""><i class="fa fa-pencil"></i></a>
                                                        <a href=""><i class="fa fa-trash-o"></i></a>
                                                        <a href=""><i class="fa fa-check-square-o"></i></a>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>No</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Group</th>
                                                <th class="table-action-column">Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                            <!-- /.table -->

                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">History</h3>
                                </div><!-- /.box-header -->
                                <div class="box-body no-padding">
                                    <table id="example2" class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th width="30px;">No</th>
                                                <th>Case</th>
                                                <th>Name</th>
                                                <th>Success Rate</th>
                                                <th width="80px;" class="text-center">Percentage</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1.</td>
                                                <td>Model 1</td>
                                                <td>Trial 1 of Model 1</td>
                                                <td>
                                                    <div class="progress sm">
                                                        <div class="progress-bar progress-bar-blue" style="width: 70%"></div>
                                                    </div>
                                                </td>
                                                <td class="text-center"><span class="badge bg-blue">70%</span></td>
                                            </tr>
                                            <tr>
                                                <td>2.</td>
                                                <td>Model 1</td>
                                                <td>Trial 2 of Model 1</td>
                                                <td>
                                                    <div class="progress sm">
                                                        <div class="progress-bar progress-bar-blue" style="width: 93%"></div>
                                                    </div>
                                                </td>
                                                <td class="text-center"><span class="badge bg-blue">70%</span></td>
                                            </tr>
                                            <tr>
                                                <td>3.</td>
                                                <td>Model 2</td>
                                                <td>Trial 1 of Model 2</td>
                                                <td>
                                                    <div class="progress sm">
                                                        <div class="progress-bar progress-bar-blue" style="width: 50%"></div>
                                                    </div>
                                                </td>
                                                <td class="text-center"><span class="badge bg-blue">70%</span></td>
                                            </tr>
                                            <tr>
                                                <td>4.</td>
                                                <td>Model 2</td>
                                                <td>Trial 2 of Model 2</td>
                                                <td>
                                                    <div class="progress sm">
                                                        <div class="progress-bar progress-bar-blue" style="width: 78%"></div>
                                                    </div>
                                                </td>
                                                <td class="text-center"><span class="badge bg-blue">70%</span></td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th width="30px;">No</th>
                                                <th>Case</th>
                                                <th>Name</th>
                                                <th>Success Rate</th>
                                                <th class="text-center">Percentage</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                            <!-- /.table -->

                            <!-- Form -->
                            <!-- general form elements disabled -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Insert User</h3>
                                </div><!-- /.box-header -->

                                <form role="form">
                                    <div class="box-body">
                                        <!-- text input -->
                                        <div class="form-group bg-default">
                                            <label>Name</label>
                                            <input type="text" class="form-control" placeholder="Type Name"/>
                                        </div>

                                        <div class="form-group">
                                            <label>Email</label>
                                            <input type="email" class="form-control" placeholder="Type Email"/>
                                        </div>

                                        <div class="form-group">
                                            <label>Password</label>
                                            <input type="password" class="form-control" placeholder="Type Password"/>
                                        </div>

                                        <div class="form-group">
                                            <label>Confirm Password</label>
                                            <input type="password" class="form-control" placeholder="Type Password Again"/>
                                        </div>

                                        <!-- select -->
                                        <div class="form-group">
                                            <label>Select</label>
                                            <select class="form-control">
                                                <option>Group 1</option>
                                                <option>Group 2</option>
                                                <option>Group 3</option>
                                                <option>Group 4</option>
                                                <option>Group 5</option>
                                            </select>
                                        </div>
                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>

                                </form>

                            </div><!-- /.box -->
                            <!-- /.form -->

                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Insert User</h3>
                                </div><!-- /.box-header -->

                                <form class="form-horizontal form-bordered" role="form" id="formInsertUser">
                                    <div class="box-body no-padding">
                                        <div class="form-group has-success has-feedback bg-default">
                                            <label for="inputName" class="col-sm-2 control-label">Name</label>
                                            <div class="col-sm-10">
                                              <input type="text" class="form-control" id="inputName" placeholder="Name"
                                                minlength="2" required>
                                            <span class="glyphicon glyphicon-ok form-control-feedback"></span>
                                              <!-- <span for="inputName" class="help-block">Error Message</span> -->
                                            </div>

                                          </div>
                                        <div class="form-group has-success has-feedback bg-default">
                                            <label for="inputEmail" class="col-sm-2 control-label">Email</label>
                                            <div class="col-sm-10">
                                              <input type="email" class="form-control" id="inputEmail" placeholder="Email"
                                                minlength="2" required>
                                            <span class="glyphicon glyphicon-ok form-control-feedback"></span>
                                            </div>
                                          </div>
                                          <div class="form-group bg-default">
                                            <label for="inputPassword" class="col-sm-2 control-label">Password</label>
                                            <div class="col-sm-10">
                                              <input type="password" class="form-control" id="inputPassword" placeholder="Password">
                                            </div>
                                          </div>
                                          <div class="form-group bg-default">
                                            <label for="inputConfirmPassword" class="col-sm-2 control-label">Confirm Password</label>
                                            <div class="col-sm-10">
                                              <input type="password" class="form-control" id="inputConfirmPassword" placeholder="Type Password Again">
                                            </div>
                                          </div>
                                          <!-- select -->
                                            <div class="form-group bg-default">
                                                <label for="inputSelectGroup" class="col-sm-2 control-label">Select Group</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" id="inputSelectGroup">
                                                        <option>Group 1</option>
                                                        <option>Group 2</option>
                                                        <option>Group 3</option>
                                                        <option>Group 4</option>
                                                        <option>Group 5</option>
                                                    </select>
                                                </div>
                                            </div>
                                          <div class="form-group">
                                            <div class="col-sm-offset-2 col-sm-10">
                                              <button type="submit" class="btn btn-primary">Submit</button>
                                              <button type="reset" class="btn btn-default">Reset</button>
                                            </div>
                                          </div>
                                    </div>
                                </form>

                            </div><!-- /.box -->
                            <!-- /.form -->

                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Insert User</h3>
                                </div><!-- /.box-header -->

                                <form class="form-horizontal form-bordered" role="form" id="formInsertUser2">
                                    <div class="box-body no-padding">
                                        <div class="form-group bg-default">
                                            <label for="inputName" class="col-sm-2 control-label">Name</label>
                                            <div class="col-sm-10">
                                              <input type="text" class="form-control" id="inputName" name="name" placeholder="Name">
                                            </div>

                                          </div>
                                        <div class="form-group bg-default">
                                            <label for="inputEmail" class="col-sm-2 control-label">Email</label>
                                            <div class="col-sm-10">
                                              <input type="email" class="form-control" id="inputEmail" name="email" placeholder="Email">
                                            </div>
                                          </div>
                                          <div class="form-group bg-default">
                                            <label for="inputPassword" class="col-sm-2 control-label">Password</label>
                                            <div class="col-sm-10">
                                              <input type="password" class="form-control" id="inputPassword" name="password" placeholder="Password">
                                            </div>
                                          </div>
                                          <div class="form-group bg-default">
                                            <label for="inputConfirmPassword" class="col-sm-2 control-label">Confirm Password</label>
                                            <div class="col-sm-10">
                                              <input type="password" class="form-control" id="inputConfirmPassword" name="confirmPassword" placeholder="Type Password Again">
                                            </div>
                                          </div>
                                          <!-- select -->
                                            <div class="form-group bg-default">
                                                <label for="inputSelectGroup" class="col-sm-2 control-label">Select Group</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" id="inputSelectGroup" name="group">
                                                        <option value="">Select a group</option>
                                                        <option>Group 1</option>
                                                        <option>Group 2</option>
                                                        <option>Group 3</option>
                                                        <option>Group 4</option>
                                                        <option>Group 5</option>
                                                    </select>
                                                </div>
                                            </div>
                                          <div class="form-group">
                                            <div class="col-sm-offset-2 col-sm-10">
                                              <button type="submit" class="btn btn-primary">Submit</button>
                                              <button type="reset" class="btn btn-default"
                                                onclick="$('#formInsertUser2').bootstrapValidator('resetForm', true);">Reset</button>
                                            </div>
                                          </div>
                                    </div>
                                </form>

                            </div><!-- /.box -->
                            <!-- /.form -->

                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Insert Group</h3>
                                </div><!-- /.box-header -->

                                <form class="form-horizontal form-bordered" role="form" id="formInsertGroup">
                                    <div class="box-body no-padding">
                                        <div class="form-group bg-default">
                                            <label for="inputName" class="col-sm-2 control-label">Name</label>
                                            <div class="col-sm-10">
                                              <input type="text" class="form-control" id="inputName" name="name" placeholder="Name">
                                            </div>

                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-sm-10">
                                              <button type="submit" class="btn btn-primary">Submit</button>
                                              <button type="reset" class="btn btn-default"
                                                onclick="$('#formInsertGroup').bootstrapValidator('resetForm', true);">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                            </div><!-- /.box -->
                            <!-- /.form -->

                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Insert Variable</h3>
                                </div><!-- /.box-header -->

                                <form class="form-horizontal form-bordered" role="form" id="formInsertVariable">
                                    <div class="box-body no-padding">
                                        <div class="form-group bg-default">
                                            <div class="col-sm-10 col-sm-offset-2 text-muted"><p>The price of the product you want to sell to the market. </p></div>
                                            <label for="formInputSellingPrice" class="col-sm-2 control-label">Selling Price</label>
                                            <div class="col-sm-10">
                                                <div class="input-group">
                                                    <span class="input-group-addon">IDR</span>
                                                    <input type="text" class="form-control currency" id="formInputSellingPrice" name="formSellingPrice" placeholder="Selling Price">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group bg-default">
                                            <div class="col-sm-10 col-sm-offset-2 text-muted"><p>The cost of safety you want to use on your product.</p></div>
                                            <label for="formInputSafety" class="col-sm-2 control-label">Safety</label>
                                            <div class="col-sm-10">
                                                <div class="input-group">
                                                    <span class="input-group-addon">IDR</span>
                                                    <input type="text" class="form-control currency" id="formInputSafety" name="formSafety" placeholder="Safety">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group bg-default">
                                            <div class="col-sm-10 col-sm-offset-2 text-muted"><p>The cost of feature you want to use on your product.</p></div>
                                            <label for="formInputFeature" class="col-sm-2 control-label">Feature</label>
                                            <div class="col-sm-10">
                                                <div class="input-group">
                                                    <span class="input-group-addon">IDR</span>
                                                    <input type="text" class="form-control currency" id="formInputFeature" name="formFeature" placeholder="Feature">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group bg-default">
                                            <div class="col-sm-10 col-sm-offset-2 text-muted"><p>The cost of product performance you want to use on your product.</p></div>
                                            <label for="formInputProductPerformance" class="col-sm-2 control-label">Product Performance</label>
                                            <div class="col-sm-10">
                                                <div class="input-group">
                                                    <span class="input-group-addon">IDR</span>
                                                    <input type="text" class="form-control currency" id="formInputProductPerformance" name="formProductPerformance" placeholder="Product Performance">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group bg-default">
                                            <div class="col-sm-10 col-sm-offset-2 text-muted"><p>The cost of service you want to provide to the customer of your product.</p></div>
                                            <label for="formInputAfterSellService" class="col-sm-2 control-label">After Sell Service</label>
                                            <div class="col-sm-10">
                                                <div class="input-group">
                                                    <span class="input-group-addon">IDR</span>
                                                    <input type="text" class="form-control currency" id="formInputAfterSellService" name="formAfterSellService" placeholder="After Sell Service">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group bg-default">
                                            <div class="col-sm-10 col-sm-offset-2 text-muted"><p>The cost of branding you want to use on your product.</p></div>
                                            <label for="formInputBranding" class="col-sm-2 control-label">Branding</label>
                                            <div class="col-sm-10">
                                                <div class="input-group">
                                                    <span class="input-group-addon">IDR</span>
                                                    <input type="text" class="form-control currency" id="formInputBranding" name="formBranding" placeholder="Branding">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group bg-default">
                                            <div class="col-sm-10 col-sm-offset-2 text-muted"><p>The cost of extra feature you want to use on your product.</p></div>
                                            <label for="formInputExtraFeature" class="col-sm-2 control-label">Extra Feature</label>
                                            <div class="col-sm-10">
                                                <div class="input-group">
                                                    <span class="input-group-addon">IDR</span>
                                                    <input type="text" class="form-control currency" id="formInputExtraFeature" name="formExtraFeature" placeholder="Extra Feature">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group bg-default">
                                            <div class="col-sm-10 col-sm-offset-2 text-muted"><p>The cost of design you want to use on your product.</p></div>
                                            <label for="formInputDesign" class="col-sm-2 control-label">Design</label>
                                            <div class="col-sm-10">
                                                <div class="input-group">
                                                    <span class="input-group-addon">IDR</span>
                                                    <input type="text" class="form-control currency" id="formInputDesign" name="formDesign" placeholder="Design">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group bg-default">
                                            <div class="col-sm-10 col-sm-offset-2 text-muted"><p>The cost of advertising you want to use to promote your product.</p></div>
                                            <label for="formInputAdvertising" class="col-sm-2 control-label">Advertising</label>
                                            <div class="col-sm-10">
                                                <div class="input-group">
                                                    <span class="input-group-addon">IDR</span>
                                                    <input type="text" class="form-control currency" id="formInputAdvertising" name="formAdvertising" placeholder="Advertising">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group bg-default">
                                            <div class="col-sm-10 col-sm-offset-2 text-muted"><p>The time in days you want the product to be ready to sell to the market.</p></div>
                                            <label for="formInputTimeToMarket" class="col-sm-2 control-label">Time To Market</label>
                                            <div class="col-sm-10">
                                                <div class="input-group">
                                                    <span class="input-group-addon">Days</span>
                                                    <input type="text" class="form-control currency" id="formInputTimeToMarket" name="formTimeToMarket" placeholder="Time To Market">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-sm-10">
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                                <button type="reset" class="btn btn-default"
                                                    onclick="$('#formInsertVariable').bootstrapValidator('resetForm', true);">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                            </div><!-- /.box -->
                            <!-- /.form -->



                        </div>
                    </div>
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->


        <!-- jQuery 2.1.1 -->
        {{ HTML::script('packages/jquery/jquery-2.1.1.min.js') }}
        <!-- Bootstrap 3.0.2 -->
        <!-- {{ HTML::script('packages/admin-lte/js/bootstrap.min.js') }} -->
        <!-- Bootstrap 3.1.1 -->
        {{ HTML::script('packages/bootstrap/bootstrap-3.1.1/js/bootstrap.min.js') }}
        <!-- DATA TABLES SCRIPT -->
        {{ HTML::script('packages/admin-lte/js/plugins/datatables/jquery.dataTables.js') }}
        {{ HTML::script('packages/admin-lte/js/plugins/datatables/dataTables.bootstrap.js') }}
        <!-- AdminLTE App -->
        {{ HTML::script('packages/admin-lte/js/AdminLTE/app.js') }}
        <!-- AdminLTE for demo purposes -->
        {{ HTML::script('packages/admin-lte/js/AdminLTE/demo.js') }}
        <!-- Jquery Validate -->
        {{ HTML::script('packages/jquery/validate/dist/additional-methods.min.js') }}
        {{ HTML::script('packages/jquery/validate/dist/jquery.validate.min.js') }}
        <!-- Bootstrap Validator -->
        {{ HTML::script('packages/bootstrap/bootstrapvalidator-0.4.5/dist/js/bootstrapValidator.min.js') }}
        <!-- InputMask -->
        {{ HTML::script('packages/admin-lte/js/plugins/input-mask/jquery.inputmask.js') }}
        {{ HTML::script('packages/admin-lte/js/plugins/input-mask/jquery.inputmask.date.extensions.js') }}
        {{ HTML::script('packages/admin-lte/js/plugins/input-mask/jquery.inputmask.extensions.js') }}
        <!-- Price Format -->
        {{ HTML::script('packages/jquery/price_format/jquery.price_format.2.0.min.js') }}

        <!-- page script -->
        <script type="text/javascript">
            $(function() {
                $('form').find('.currency').priceFormat({
                    prefix: '',
                    centsSeparator: '',
                    thousandsSeparator: ',',
                    centsLimit: 0,
                });
                $("[data-mask]").inputmask();

                $("#example1").dataTable();

                $("#example2").dataTable();

                $.validator.setDefaults({
                    highlight: function(element) {
                        // $(element).closest('.form-group').addClass('has-error');
                        $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                    },
                    unhighlight: function(element) {
                        // $(element).closest('.form-group').removeClass('has-error');
                        $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                    },
                    errorElement: 'span',
                    errorClass: 'help-block',
                    errorPlacement: function(error, element) {
                        // if(element.parent('.input-group').length) {
                        //     error.insertAfter(element.parent());
                        // } else {
                        //     error.insertAfter(element);
                        // }
                        if (element.parent('.input-group').length || element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
                            error.insertAfter(element.parent());
                        } else {
                            error.insertAfter(element);
                        }
                    }
                });

                $('#formInsertUser').validate();

                $('#formInsertUser2').bootstrapValidator({
                    excluded: [':disabled', ':hidden', ':not(:visible)'],
                    feedbackIcons: {
                        valid: 'glyphicon glyphicon-ok',
                        invalid: 'glyphicon glyphicon-remove',
                        validating: 'glyphicon glyphicon-refresh'
                    },
                    live: 'enabled',
                    message: 'This value is not valid',
                    submitButtons: 'button[type="submit"]',
                    // submitHandler: null,
                    // trigger: null,
                    fields: {
                        name: {
                            message: 'The username is not valid',
                            validators: {
                                notEmpty: {
                                    message: 'The username is required and cannot be empty'
                                },
                                stringLength: {
                                    min: 6,
                                    max: 30,
                                    message: 'The username must be more than 6 and less than 30 characters long'
                                }
                            }
                        },
                        email: {
                            validators: {
                                notEmpty: {
                                    message: 'The email is required and cannot be empty'
                                },
                                emailAddress: {
                                    message: 'The input is not a valid email address'
                                }
                            }
                        },
                        password: {
                            validators: {
                                notEmpty: {
                                    message: 'The password is required and cannot be empty'
                                },
                                stringLength: {
                                    min: 6,
                                    max: 30,
                                    message: 'The password must be more than 6 and less than 30 characters long'
                                },
                            }
                        },
                        confirmPassword: {
                            validators: {
                                notEmpty: {
                                    message: 'The password is required and cannot be empty'
                                },
                                stringLength: {
                                    min: 6,
                                    max: 30,
                                    message: 'The password must be more than 6 and less than 30 characters long'
                                },
                                identical: {
                                    field: 'password',
                                    message: 'The password and its confirm are not the same'
                                },
                            }
                        },
                        group: {
                            validators: {
                                notEmpty: {
                                    message: 'The group is required and cannot be empty'
                                },
                            }
                        }

                    }
                });

                $('#formInsertGroup').bootstrapValidator({
                    excluded: [':disabled', ':hidden', ':not(:visible)'],
                    feedbackIcons: {
                        valid: 'glyphicon glyphicon-ok',
                        invalid: 'glyphicon glyphicon-remove',
                        validating: 'glyphicon glyphicon-refresh'
                    },
                    live: 'enabled',
                    message: 'This value is not valid',
                    submitButtons: 'button[type="submit"]',
                    // submitHandler: null,
                    // trigger: null,
                    fields: {
                        name: {
                            message: 'The username is not valid',
                            validators: {
                                notEmpty: {
                                    message: 'The username is required and cannot be empty'
                                },
                                stringLength: {
                                    min: 6,
                                    max: 30,
                                    message: 'The username must be more than 6 and less than 30 characters long'
                                }
                            }
                        }

                    }
                });

                $('#formInsertVariable').bootstrapValidator({
                    excluded: [':disabled', ':hidden', ':not(:visible)'],
                    feedbackIcons: {
                        valid: 'glyphicon glyphicon-ok',
                        invalid: 'glyphicon glyphicon-remove',
                        validating: 'glyphicon glyphicon-refresh'
                    },
                    live: 'enabled',
                    message: 'This value is not valid',
                    submitButtons: 'button[type="submit"]',
                    // submitHandler: null,
                    // trigger: null,
                    fields: {
                        formSellingPrice: {
                            message: 'The username is not valid',
                            validators: {
                                notEmpty: {
                                    message: 'The selling price is required and cannot be empty'
                                },
                                stringLength: {
                                    min: 2,
                                    max: 10,
                                    message: 'The selling price must be more than 2 and less than 6 characters long'
                                }
                            }
                        },
                    }
                });
            });
        </script>

    </body>
</html>
