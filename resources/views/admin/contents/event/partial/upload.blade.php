<form id="addPhotosForm{{$mediaName}}"
      action="{{ route($route) }}"
      method="POST"
      class="dropzone">
    {{ csrf_field() }}
    <input type="hidden" name="media_type" value="{{$mediaType}}">
</form>
