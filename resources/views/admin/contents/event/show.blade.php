@extends('admin.layouts.main')
@section('style')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/dropzone.css">
@endsection
@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="form-horizontal form-bordered" id="formUploadPhotos">
                <div class="form-group bg-default">
                    <label for="addPhotosFormEvent" class="col-sm-2 control-label">Event Photos</label>
                    <div class="col-sm-10">
                        @include('admin.contents.event.partial.upload', ['route' => 'admin.media.upload', 'mediaName' => 'Event', 'mediaType' => 'user_photo'])
                    </div>
                </div>

                {!! Form::open(array('route' => array($formAction, $event->id), 'method' => $formMethod, 'class' => 'form-horizontal form-bordered', 'role' => 'form', 'id' => 'formInsertEvent')) !!}
                <div class="form-group">
                    <div id="id_wrapper">
                        <input type="hidden" id="photo_id" name="photo_id">
                    </div>
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" id="submit-button" class="btn btn-primary">Submit</button>
                        <a href="{{ URL::route('admin.event.edit', $event->id) }}">
                            <button type="button" class="btn btn-default">Back</button>
                        </a>
                    </div>
                </div>
            {{ Form::close() }}<!-- /.form -->
            </div>
        </div>
    </div><!-- /.box -->
@endsection
@section('script')
    <script type="application/javascript">
        $(document).ready(function() {
            $('#submit-button').prop('disabled', true);
        });
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/dropzone.js"></script>
    <script type="application/javascript">
        $('#addPhotosFormEvent').dropzone({
            paramName: "media",
            maxFilesize: 3,
            maxFiles: 1,
            accepFiles: '.jpg, .jpeg, .png, .bmp',
            dictDefaultMessage: 'Upload photo for event',
            success: function (file, response) {
                console.log(response.media.id);
                $('#photo_id').val(response.media.id);
                $('#submit-button').prop('disabled', false);
            }
        });
    </script>
@endsection
