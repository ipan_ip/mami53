@extends('admin.layouts.main')
@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <a href="{{ URL::route('admin.event.create') }}">
                        <form action="" class="form-inline" method="get" style="text-align:right;padding:10px;">
                            <a href="{{ route( $createRoute ) }}" class="btn btn-primary btn-sm pull-left">
                                <i class="fa fa-plus">&nbsp;</i>Add Event
                            </a>
                            <input type="text" name="title" class="form-control input-sm"  placeholder="Nama Event"  autocomplete="off" value="{{ Request::get('title') }}">
                            {{ Form::select('is_owner', [
                                '' => "Owner & Not Owner", 
                                1 => 'Owner Only',
                                0 => 'Not Owner Only',
                            ], request()->input('is_owner'), array('class' => 'form-group btn btn-default form-control')) }}
                            {{ Form::select('is_published', [
                                '' => "Published & Not Published", 
                                1 => 'Published Only',
                                0 => 'Not Published Only',
                            ], request()->input('is_published'), array('class' => 'pull-right-sort form-group btn btn-default form-control')) }}
                            <button class="btn btn-primary btn-md" id="buttonSearch"><i class="fa fa-search">&nbsp;</i>Search</button>
                        </form>
                    </a>
                </div>
            </div>

            <table id="tableListEvent" class="table table-striped">
                <thead>
                    <tr>
                        <th width="30px;">No</th>
                        <th>Event Image</th>
                        <th>Event Title</th>
                        <th>Is Owner</th>
                        <th>Published</th>
                        <th>Order</th>
                        <th>Description</th>
                        <th class="table-action-column" width="100px">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($rowsEvents as $rowEventKey => $rowEvent)
                    <tr>
                        <td>{{ $rowEventKey+1 }}</td>
                        <td>
                            @if($rowEvent->photo != null)
                            <img src="{{$rowEvent->photo->getMediaUrl()['small']}}" alt="" class="img-thumbnail">
                            @endif
                        </td>
                        <td>{{ $rowEvent->title }}</td>
                        <td>@if($rowEvent->is_owner == '1')
                              <span class="label label-success">Yes</span>
                            @else
                              <span class="label label-danger">No</span>
                            @endif  
                        </td>
                        <td>@if($rowEvent->is_published == '1')
                              <span class="label label-success">Yes</span>
                            @else
                              <span class="label label-danger">No</span>
                            @endif  
                        </td>
                        <td>{{ $rowEvent->ordering }}</td>
                        <td>{{ $rowEvent->description }}</td>
                        <td class="table-action-column">
                            <div class="btn-action-group">
                                <a href="{{ URL::route('admin.event.edit', $rowEvent->id) }}">
                                    <i class="fa fa-pencil"></i></a>
                                <a href="{{ route('admin.event.delete', $rowEvent->id)}}" onclick="return confirm('Are you sure you want to delete this item?');">
                                    <i class="fa fa-trash-o"></i></a>
                                <a href="{{ route( $publishRoute, $rowEvent->id )}}">
                                    @if($rowEvent->is_published == 0)
                                    <i class="fa fa-check"></i>
                                    @else
                                    <i class="fa fa-ban"></i>
                                    @endif
                                </a>    
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th width="30px;">No</th>
                        <th>Event Image</th>
                        <th>Event Title</th>
                        <th>Is Owner</th>
                        <th>Published</th>
                        <th>Order</th>
                        <th>Description</th>
                        <th class="table-action-column" width="100px">Action</th>
                    </tr>
                </tfoot>
            </table>
        </div><!-- /.box-body -->

        <div class="box-body">
            {{ $rowsEvents->appends(Request::except('page'))->links() }}
        </div>
    </div><!-- /.box -->
    <!-- table -->
@stop
@section('script')
<!-- page script -->
<script type="text/javascript">
    $(function()
    {
        // $("#tableListEvent").dataTable();
    });
</script>
@stop