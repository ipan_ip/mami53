@extends('admin.layouts.main')

@section('style')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/dropzone.css">
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>
        {{ Form::open([
            'route' => $formAction,
            'method' => $formMethod,
            'class' => 'form-horizontal form-bordered',
            'role' => 'form',
            'id' => 'formInsertEvent'
        ]) }}
        <div class="box-body no-padding">
            <div class="form-group bg-default">
                <label for="inputTitle" class="col-sm-2 control-label">Title</label>
                <div class="col-sm-10">
                    <input type="text"
                        class="form-control"
                        placeholder="Title"
                        id="inputTitle"
                        name="title"
                        value="{{ $rowEvent->title }}"
                        required
                    >
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputTitle" class="col-sm-2 control-label">Order</label>
                <div class="col-sm-10">
                    <select name="ordering" class="form-control">
                        @for ($i=0;$i<=$ordering;$i++)
                            @if ($rowEvent->ordering == $i+1)
                                <option value="{{ $i+1 }}" selected="true">{{ $i+1 }}</option>
                            @else 
                                <option value="{{ $i+1 }}">{{ $i+1 }}</option>
                            @endif
                        @endfor
                    </select>
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputTitle" class="col-sm-2 control-label">Is Owner</label>
                <div class="col-sm-10">
                    <select name="is_owner" class="form-control">
                        @foreach($isOwner AS $key => $value)
                            @if ($isOwnerOption == $key)
                                <option value="{{ $key }}" selected="true">{{ $value }}</option>
                            @else 
                                <option value="{{ $key }}">{{ $value }}</option>
                            @endif  
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputTitle" class="col-sm-2 control-label">Redirect browser</label>
                <div class="col-sm-10">
                    <select name="redirect_browser" class="form-control">
                        @foreach($redirectBrowser AS $key => $value)
                            <option value="{{ $key }}" @if ($key == $rowEvent->redirect_browser) selected="true" @endif>{{ $value }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputDescription" class="col-sm-2 control-label">Description</label>
                <div class="col-sm-10">
                    <textarea class="form-control"
                        placeholder="Description"
                        id="inputDescription"
                        name="description"
                        required
                    >{{ $rowEvent->description }}</textarea>
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputUrl" class="col-sm-2 control-label">URL Event</label>
                <div class="col-sm-10">
                    <input type="text"
                        class="form-control"
                        placeholder="URL Action"
                        id="inputUrl"
                        name="action_url"
                        value="{{ $rowEvent->action_url }}"
                    >
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputScheme" class="col-sm-2 control-label">Scheme Event</label>
                <div class="col-sm-10">
                    <input type="text"
                        class="form-control"
                        placeholder="Scheme Action"
                        id="inputScheme"
                        name="scheme_url"
                        value="{{ $rowEvent->scheme_url }}"
                    >
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="blob_label" class="col-sm-2 control-label">Broadcast</label>
                <div class="col-sm-10">
                    <select name="is_broadcast" class="form-control">
                        <option value="0" @if ($rowEvent->is_broadcast == 0) selected="true" @endif>No</option>
                        <option value="1" @if ($rowEvent->is_broadcast == 1) selected="true" @endif>Yes</option>
                    </select>
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputCategory" class="col-sm-2 control-label">Notification Category</label>
                <div class="col-sm-10">
                    <select name="category_id" class="form-control">
                            <option value="0">All</option>
                        @foreach($categories as $category)
                            <option value="{{ $category->id }}" @if ($category->id == $rowEvent->category_id) selected="true" @endif>{{ $category->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <!-- <div class="form-group bg-default">
                <label for="use-scheme" class="control-label col-sm-2">Scheme App</label>
                <div class="col-sm-10">
                    <input type="checkbox" name="use_scheme" value="1" {-- $rowEvent->use_scheme == 1 ? 'checked="checked"' : '' --}>
                </div>
            </div> -->

            <div class="form-group bg-default">
                <label for="photo" class="control-label col-sm-2">Foto</label>
                <div class="col-sm-10">
                    <div id="photo"
                        action="{{ url('admin/media') }}"
                        method="POST"
                        class="dropzone"
                        uploadMultiple="no"
                    >
                        {{ csrf_field() }}
                        <input type="hidden" name="media_type" value="event_photo">
                    </div>
                    <p class="help-block">
                        Rekomendasi rasio foto event 5:2
                    </p>
                </div>
            </div>

            <div id="photo-wrapper">
                <input type="hidden" name="photo_id" id="photo_id" value="{{$rowEvent->photo_id}}">
            </div>

            <div class="form-group bg-default">
                <div class="col-sm-offset-2 col-sm-10">
                    <div id="photo-preview">
                        @if(!is_null($photo))
                            <img src="{{ $photo->getMediaUrl()['small'] }}" alt="">
                        @endif
                    </div>
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="auto-live" class="control-label col-sm-2">Publish</label>
                <input type="checkbox" name="auto_live" id="auto-live" value="1" {{ $rowEvent->is_published == 1 ? 'checked="checked"' : '' }}>
            </div>

            <div class="form-group bg-default divider">
                <div class="col-sm-10 col-sm-push-2">
                    <h4>Event Targeting (Khusus Owner)</h4>
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputAreaBig" class="col-sm-2 control-label">Area Big</label>
                <div class="col-sm-10">
                    <input type="text"
                        class="form-control"
                        placeholder="Area Big"
                        id="inputAreaBig"
                        name="area_big"
                        value="{{ $rowEvent->area_big }}"
                    >
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputAreaCity" class="col-sm-2 control-label">Area City</label>
                <div class="col-sm-10">
                    <input type="text"
                        class="form-control"
                        placeholder="Area City"
                        id="inputAreaCity"
                        name="area_city"
                        value="{{ $rowEvent->area_city }}"
                    >
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputAreaSubdistrict" class="col-sm-2 control-label">Area Subdistrict</label>
                <div class="col-sm-10">
                    <input type="text"
                        class="form-control"
                        placeholder="Area Subdistrict"
                        id="inputAreaSubdistrict"
                        name="area_subdistrict"
                        value="{{ $rowEvent->area_subdistrict }}"
                    >
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary">{{ $buttonText }}</button>
                    <a href="{{ URL::route('admin.event.index') }}">
                        <button type="button" class="btn btn-default">Cancel</button>
                    </a>
                    <button type="reset"
                        class="btn btn-default"
                        onclick="$('#formInsertEvent').bootstrapValidator('resetForm', true);"
                    >Reset</button>
                </div>
            </div>
        </div>
    {{ Form::close() }}
    </div>
@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/dropzone.js"></script>
    <script type="application/javascript">
        Dropzone.autoDiscover = false;

        Dropzone.options.photo = {
            paramName : "media",
            params: {
                'media_type': 'event_photo',
                'watermarking': false
            },
            maxFilesize : 6,
            maxFiles: 1,
            accepFiles: '.jpg, .jpeg, .png, .bmp',
            dictDefaultMessage : 'Upload photo for event',
            addRemoveLinks: true,
            success: function(file, response) {
                $('#photo-wrapper').html('');
                $('#photo-wrapper').append('<input type="hidden" name="photo_id" value="' + response.media.id + '">');
                $('#photo-preview').html('');
            },
            init: function() {
                this.on('removedfile', function(file) {
                    $('#photo-wrapper').html('');
                    $('#photo-preview').html('');
                });
            }
        }

        $('#photo').dropzone();
    </script>
@endsection
