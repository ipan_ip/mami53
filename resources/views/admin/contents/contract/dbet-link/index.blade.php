@extends('admin.layouts.main')

@section('style')
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
    <style type="text/css">
        .modal {
            background: none;
        }
        .dropdown-menu > li > a:hover {
            background-color: #3c8dbc;
            color: #000000;
        }

        .dropdown-menu > li > form > a {
            display: block;
            padding: 3px 20px;
            clear: both;
            font-weight: 400;
            line-height: 1.42857143;
            color: #333;
            white-space: nowrap;
        }

        .dropdown-menu > li > form > a:hover {
            background-color: #e8e8e8;
            color: #000000;
        }

    </style>
@endsection

@section('content')
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">Filter</h3>
        </div>
        <div class="box-body">
            <form action="" method="get">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Search By</label>
                            <select name="filter" class="form-control">
                                <option></option>
                                @foreach($filters as $key => $val)
                                    <option @if(request()->input('filter') == $key) selected @endif value="{{ $key }}">{{ $val }}</opion>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <label>Search Value</label>
                            <input type="text" name="search_value" class="form-control" placeholder="Search Value" value="{{ request()->input('search_value') }}" autocomplete="off" style="margin-right:2px" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group pull-right">
                            <label>&nbsp;</label><br>
                            <button type="submit" class="btn btn-primary btn-md">
                                <i class="fa fa-search">&nbsp;</i>Search
                            </button>
                            <a href="{{ route('admin.dbet-link.create') }}" class="btn btn-success btn-md">
                                <i class="fa fa-plus">&nbsp;</i>Add New
                            </a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">DBET from Tenant Link</h3>
        </div>

        <div class="box-body no-padding">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Owner Name</th>
                            <th>Kos Name</th>
                            <th>Owner Phone</th>
                            <th>Kos City</th>
                            <th>Link</th>
                            <th class="text-center">#</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($linkRaw->isNotEmpty() && isset($links['data']))
                            @foreach($links['data'] as $link)
                                <tr>
                                    <td>{{ $link['owner_name'] }}</td>
                                    <td>{{ $link['room_name'] }}</td>
                                    <td>{{ $link['owner_phone'] }}</td>
                                    <td>{{ $link['room_city'] }}</td>
                                    <td>
                                        @if($link['link'] !== null)
                                            <a href="{{ $link['link'] }}">{{ $link['link'] }}</a>
                                        @else
                                            -
                                        @endif
                                    </td>
                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                                <i class="fa fa-cogs"></i> Actions
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu pull-right">
                                                <li>
                                                    <a href="{{ route('admin.dbet-link.edit', $link['id']) }}" title="Edit">
                                                        <i class="fa fa-pencil"></i> Edit
                                                    </a>
                                                </li>
                                                <li>
                                                    <form action="{{ route('admin.dbet-link.destroy', $link['id']) }}" method="POST" style="display: block;">
                                                        <input type="hidden" name="_method" value="DELETE">
                                                        <a href="#" onclick="if (!confirm('Are you sure want to delete ?')) return false; else $(this).parent().submit();" title="Delete">
                                                            <i class="fa fa-trash"></i> Delete
                                                        </a>
                                                    </form>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @else    
                            <tr>
                                <td colspan="6">Data Not Found</td>
                            </tr>
                        @endif    
                    </tbody>
                </table>
            </div>
        </div>

        <div class="box-body no-padding">
            <div class="col-md-12 bg-default">
                @if($linkRaw) {{ $linkRaw->appends(Request::except('page'))->links() }} @endif
            </div>
        </div>
    </div>


@endsection

@section('script')
@endsection