@extends('admin.layouts.main')
@section('style')
    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" />
    <link href="//cdn.jsdelivr.net/npm/select2-bootstrap-theme@0.1.0-beta.10/dist/select2-bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/booking-form-admin.css') }}">
@endsection
@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">DBET from Tenant Link</h3>
        </div>

        <form method="POST" action="{{ isset($data) ? route('admin.dbet-link.update', $data->id) : route('admin.dbet-link.store') }}" class="form-horizontal form-bordered">
            @csrf
            @if (isset($data))
                <input type="hidden" name="_method" value="PUT">
            @endif
            <div class="box-body no-padding">

                @if(isset($data))
                    <div class="form-group bg-default">
                        <label for="select-tag" class="control-label col-sm-2">Kost Name</label>
                        <div class="col-sm-10">
                            <input type="text" disabled class="form-control" value="{{ $data->room->name ?? '-' }}">
                        </div>
                    </div>

                    <div class="form-group bg-default">
                        <label for="select-tag" class="control-label col-sm-2">Kost City</label>
                        <div class="col-sm-10">
                            <input type="text" disabled class="form-control" value="{{ $data->room->area_city ?? '-' }}">
                        </div>
                    </div>

                    <div class="form-group bg-default">
                        <label for="select-tag" class="control-label col-sm-2">Owner Name</label>
                        <div class="col-sm-10">
                            <input type="text" disabled class="form-control" value="{{ $data->room->verified_owner->user->name ?? '-' }}">
                        </div>
                    </div>

                    <div class="form-group bg-default">
                        <label for="select-tag" class="control-label col-sm-2">Owner Phone</label>
                        <div class="col-sm-10">
                            <input type="text" disabled class="form-control" value="{{ $data->room->verified_owner->user->phone_number ?? '-' }}">
                        </div>
                    </div>
                @else
                    <div class="form-group bg-default">
                        <label for="inputRoom" class="control-label col-sm-2">Select Kost</label>
                        <div class="col-sm-10">
                            <select id="inputRoom" class="form-control select2-single"></select>
                            <input type="hidden" class="form-control" name="designer_id" required>
                        </div>
                    </div>    
                @endif

                <div class="form-group bg-default">
                    <label for="is_required_identity" class="control-label col-sm-2">Required Identity</label>
                    <div class="col-sm-10">
                        <select id="is_required_identity" name="is_required_identity" class="form-control">
                            <option @if(isset($data) && $data->is_required_identity == 1) selected @endif value="1">YES</option>
                            <option @if(isset($data) && $data->is_required_identity == 0) selected @endif value="0">NO</option>
                        </select>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="is_required_due_date" class="control-label col-sm-2">Required Due Date</label>
                    <div class="col-sm-10">
                        <select id="is_required_due_date" name="is_required_due_date" class="form-control">
                            <option @if(isset($data) && $data->is_required_due_date == 1) selected @endif value="1">YES</option>
                            <option @if(isset($data) && $data->is_required_due_date == 0) selected @endif value="0">NO</option>
                        </select>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="due_date" class="control-label col-sm-2">Due Date</label>
                    <div class="col-sm-10">
                        <input type="number" min="1" max="31" class="form-control" id="due_date" name="due_date" value="{{ isset($data) ? $data->due_date : 1 }}">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <div class="col-sm-10 col-sm-push-2">
                        <a href="{{ route('admin.dbet-link.index') }}" class="btn btn-default">Back</a>
                        <button type="submit" class="btn btn-primary">{{ isset($data) ? 'Update' : 'Save' }}</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('script')
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/jquery-steps@1.1.0/build/jquery.steps.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/jquery-mask-plugin@1.14.15/dist/jquery.mask.min.js"></script>
    <script>
        $(function() {
            $('#inputRoom').select2({
                theme: "bootstrap",
                placeholder: "Room",
                ajax: {
                    url: '{{ url('admin/room/search/api') }}',
                    data: function (params) {
                        return {
                            search: params.term,
                            page: params.page || 1
                        }
                    },
                    processResults: function (data, params) {
                        var page = params.page || 1;
                        if (data.data.length < 1) var total_count = 0;
                        else var total_count = data.data[0].total_count;
                        return {
                            results: $.map(data.data, item => { 
                                return {
                                    id: item.song_id,
                                    text: item.id+ ' | ' +item.name
                                }
                            }),
                            pagination: {
                                more: (page * 10) <= total_count
                            }
                        };
                    },
                    cache: true
                }
            });

            $("#inputRoom").on('select2:select', e => {
                var selected = e.params.data;
                if (selected) {
                    $('input[name=designer_id]').val(selected.id);
                }
            });
        });
    </script>
@endsection
