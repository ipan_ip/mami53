@extends('admin.layouts.main')

@section('style')
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
    <style type="text/css">
        .modal {
            background: none;
        }
        .dropdown-menu > li > a:hover {
            background-color: #3c8dbc;
            color: #000000;
        }
    </style>
@endsection

@section('content')
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">Filter</h3>
        </div>
        <div class="box-body">
            <form action="" method="get">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Search By</label>
                            <select name="search_by" class="form-control">
                                <option></option>
                                @foreach($filters['searchBy'] as $key => $val)
                                    <option @if(request()->input('search_by') == $key) selected @endif value="{{ $key }}">{{ $val }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Search Value</label>
                            <input type="text" name="search_value" class="form-control" placeholder="Search Value" value="{{ request()->input('search_value') }}" autocomplete="off" style="margin-right:2px" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Kost Level</label>
                            <select name="kost_level" class="form-control">
                                <option></option>
                                @foreach($filters['kostLevels'] as $key => $val)
                                    <option @if(request()->input('kost_level') == $val->id) selected @endif value="{{ $val->id }}">{{ $val->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Room Level</label>
                            <select name="room_level" class="form-control">
                                <option></option>
                                @foreach($filters['roomLevels'] as $key => $val)
                                    <option @if(request()->input('room_level') == $val->id) selected @endif value="{{ $val->id }}">{{ $val->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Funnel</label>
                            <select name="funnel" class="form-control">
                                <option></option>
                                @foreach($filters['funnels'] as $key => $val)
                                    <option @if(request()->input('funnel') == $key) selected @endif value="{{ $key }}">{{ $val }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-md">
                                <i class="fa fa-search">&nbsp;</i>Search
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Data Contracts</h3>
        </div>

        <div class="box-body no-padding">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Kos Name</th>
                            <th>Owner Phone</th>
                            <th width="20%">Room Number & Level</th>
                            <th>Penyewa</th>
                            <th>Kos Level</th>
                            <th width="20%">Contract Duration - Period</th>
                            <th>Booking Status</th>
                            <th>DBET Consultant</th>
                            <th>DBET Owner</th>
                            <th>Contract Status</th>
                            <th>Terminated reason</th>
                            <th class="text-center">#</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($contractRaws->isNotEmpty() && isset($contracts['data']))
                            @foreach($contracts['data'] as $contract)
                                <tr>
                                    <td>{{ $contract['room'] != null ?  $contract['room']['name'] : '-' }}</td>
                                    <td>{{ $contract['room_owner'] != null ? $contract['room_owner']['phone_number']: '-' }}</td>
                                    <td>
                                        Number: <b> {{ $contract['room_number'] }} </b> <br>
                                        Level: <b> {{ $contract['room_unit_level'] != null ? $contract['room_unit_level']['name']: '-' }}</b>
                                    </td>
                                    <td>
                                        {{ $contract['tenant'] != null ? $contract['tenant']['name']: '-' }} <br>
                                        <b>{{ $contract['tenant'] != null ? $contract['tenant']['phone_number']: '-' }}</b>
                                    </td>
                                    <td>{{ $contract['room_level'] != null ? $contract['room_level']['name']: '-' }}</td>
                                    <td>    
                                        {{ $contract['duration_formatted'] }} <br>
                                        <b>{{ $contract['start_date'] . ' - ' . $contract['end_date'] }}</b>
                                    </td>
                                    <td>
                                        @if($contract['is_from_booking'] === true)
                                            <span class="label label-success">Yes</span> <br>
                                            <span class="label label-info">
                                                {{ $contract['booking_user'] != null ? $contract['booking_user']['status']: '-' }}
                                            </span>
                                        @else
                                            <span class="label label-danger">No</span>
                                        @endif
                                    </td>
                                    <td>
                                        @if($contract['is_from_consultant'] === true)
                                            <span class="label label-success">Yes</span>
                                        @else
                                            <span class="label label-danger">No</span>
                                        @endif
                                    </td>
                                    <td>
                                        @if($contract['is_from_owner'] === true)
                                            <span class="label label-success">Yes</span>
                                        @else
                                            <span class="label label-danger">No</span>
                                        @endif
                                    </td>
                                    <td>
                                        <span class="label @if($contract['status'] == 'active') label-success @elseif($contract['status'] == 'booked') label-warning @else label-danger @endif">
                                            {{ $contract['status'] }}
                                        </span>
                                    </td>
                                    <td>{{ $contract['booking_user'] != null ? $contract['booking_user']['reject_reason']: '-' }}</td>
                                    <td class="table-action-column" style="padding-right:15px!important;">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                                <i class="fa fa-cogs"></i> Actions
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu pull-right">
                                                <li>
                                                    <a href="{{ $contract['tenant']['whatsapp_phone_number_url'] }}" target="_blank" title="Chat WA Penyewa">
                                                        <i class="fa fa-phone"></i> Chat WA Penyewa
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#" class="see-invoice" title="See Invoice" contract-id="{{ $contract['id'] }}">
                                                        <i class="fa fa-money"></i> See Invoice
                                                    </a>
                                                </li>
                                                @if (!in_array($contract['status'], ['terminated', 'cancelled']))
                                                    <li>
                                                        <a 
                                                            href="#" 
                                                            class="change-room-number" 
                                                            title="Change Room Number" 
                                                            room-unit="{{ $contract['room_number'] }}"
                                                            room-unit-id="{{ $contract['room_unit'] != null ? $contract['room_unit']['id']: '' }}"
                                                            room-level="{{ $contract['room_unit_level'] != null ? $contract['room_unit_level']['name']: '' }}" 
                                                            contract-id="{{ $contract['id'] }}"
                                                        >
                                                            <i class="fa fa-clipboard"></i> Change Room Number
                                                        </a>
                                                    </li>
                                                @endif
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="15">Data Not Found</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>

        <div class="box-body no-padding">
            <div class="col-md-12 bg-default">
                @if($contractRaws) {{ $contractRaws->appends(Request::except('page'))->links() }} @endif
            </div>
        </div>
    </div>

    <div class="modal" id="modal-invoice" role="dialog" aria-labelledby="modalLabel">
        <div class="modal-dialog modal-lg" role="modal">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top:2px;">
                        <i class="fa fa-lg fa-times-circle"></i>
                    </button>
                    <h4 class="modal-title">
                        <span id="modal-title">Invoice</span>
                    </h4>
                </div>
                <div class="modal-body">
                    <table class="table table-responsive table-bordered">
                        <thead>
                            <th class="text-center">Number</th>
                            <th class="text-center">Name</th>
                            <th class="text-center">link</th>
                            <th class="text-center">Jatuh Tempo</th> 
                            <th class="text-center">Status</th> 
                        </thead>
                        <tbody id="modal-container">
                            
                        </tbody>
                    </table>
                    <br>
                    Note: <br>
                    <b>Open Invoice link to see total amount</b>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="modal-change-room-number" role="dialog" aria-labelledby="modalLabel">
        <div class="modal-dialog modal-lg" role="modal">
            <div class="modal-content">
                <form id="form-change-room-number">
                    <div class="modal-header">
                        <button type="button" class="close close-modal-change-room-number" data-dismiss="modal" aria-label="Close" style="margin-top:2px;">
                            <i class="fa fa-lg fa-times-circle"></i>
                        </button>
                        <h4 class="modal-title">
                            <span id="modal-title">Change Room Number</span>
                        </h4>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="contract_id">
                        <div class="form-group row">
                            <label for="currentRoomNumber" class="col-sm-2 col-form-label">Current Room Number</label>
                            <div class="col-sm-10">
                                <input type="text" readonly class="form-control" id="currentRoomNumber">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="currentRoomNumber" class="col-sm-2 col-form-label">Choose Room Number</label>
                            <div class="col-sm-10">
                                <select name="designer_room_id" id="listRoomNumber" class="form-control"></select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary pull-left">Submit</button>
                        <button type="button" class="btn btn-danger close-modal-change-room-number" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.js"></script>
    <script type="text/javascript">

        var invoiceModal = $('#modal-invoice');
        var invoiceElement = invoiceModal.find('#modal-container');

        function triggerAlert(type, message) {
            Swal.fire({
                type: type,
                customClass: {
                    container: 'custom-swal'
                },
                html: message
            });
        }

        function triggerLoading(message) {
            Swal.fire({
                title: message,
                customClass: {
                    container: 'custom-swal'
                },
                allowEscapeKey: false,
                allowOutsideClick: false,
                onOpen: () => {
                    swal.showLoading();
                }
            });
        }

        function createInvoiceRecordElement(invoice)
        {
            var shorlinkUrl = '<a href="'+invoice.shortlink_url+'" target="_blank">'+invoice.shortlink_url+'</a>';
            var labelStatus = 'label-danger';
            if (invoice.status == 'paid') {
                labelStatus = 'label-success';
            } else if (invoice.status == 'unpaid') {
                labelStatus = 'label-warning';
            }
           
            var status = '<span class="label '+labelStatus+'">'+invoice.status+'</span>';;
            var html = '';
                html += '<tr>';
                    html += '<td class="text-center">'+invoice.invoice_number+'</td>';
                    html += '<td class="text-center">'+invoice.name+'</td>';
                    html += '<td class="text-center">'+shorlinkUrl+'</td>';
                    html += '<td class="text-center">'+invoice.scheduled_date+'</td>';
                    html += '<td class="text-center">'+status+'</td>';
                html += '</tr>';
            return html;          
        }

        function createInvoiceElement(invoices)
        {
            invoiceElement.html('');
            $.each( invoices, function( key, invoice ) {
                var html = createInvoiceRecordElement(invoice);
                invoiceElement.append(html);
            });
        }

        $(document).on('click', '.see-invoice', function(e) {
            e.preventDefault();
            var contractId = $(this).attr('contract-id');
            $.ajax({
                type: 'GET',
                url: '/admin/contract/invoice/' + contractId,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                beforeSend: function() {
                    triggerLoading();
                },
                success: response => {
                    invoiceModal.modal('show');
                    if (response.status == true) {
                        createInvoiceElement(response.data)
                    }
                },
                error: error => {
                    if (Swal.isLoading()) Swal.close();
                    triggerAlert('error', '<h3>Oops!</h3><h5>Something wrong happened.<br/>Please try again in 10 minutes.</h5>');
                },
                complete: function () {
                    if (Swal.isLoading()) Swal.close();
                }
            });
        });

        $(document).on('click', '.change-room-number', function(e) {
            e.preventDefault();
            var contractId = $(this).attr('contract-id');
            var roomUnit = $(this).attr('room-unit');
            var roomUnitId = $(this).attr('room-unit-id');
            var roomLevel = $(this).attr('room-level');
            $.ajax({
                type: 'GET',
                url: '/admin/contract/room-allotment/' + contractId,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                beforeSend: function() {
                    triggerLoading();
                },
                success: response => {
                    $('#form-change-room-number input[name=contract_id]').val(contractId);
                    var roomLevelLabel = roomLevel != '' ? ' - ' + roomLevel : '';
                    $('#currentRoomNumber').val(roomUnit + roomLevelLabel);
                    $('#modal-change-room-number').show();
                    if (response.status == true) {
                        $('#listRoomNumber').html('');
                        $.each( response.data, function( key, data ) {
                            var selected = (data.id == roomUnitId ? 'selected' : '');
                            var disabled = (data.disable == true ? 'disabled' : '');
                            var levelLabel = (data.level_name !== null) ? ' - ' + data.level_name : '';
                            var html = '';
                                html += '<option value="'+data.id+'" '+selected+' '+disabled+'>'+data.name+levelLabel+'</option>';

                            $('#listRoomNumber').append(html);
                        });
                    }
                },
                error: error => {
                    if (Swal.isLoading()) Swal.close();
                    triggerAlert('error', '<h3>Oops!</h3><h5>Something wrong happened.<br/>Please try again in 10 minutes.</h5>');
                },
                complete: function () {
                    if (Swal.isLoading()) Swal.close();
                }
            });
        });

        $(document).on('submit', '#form-change-room-number', function(e) {
            e.preventDefault();
            var contractId = $('#form-change-room-number input[name=contract_id]').val();
            var formData = $(this).serialize();
            $.ajax({
                type: 'POST',
                url: '/admin/contract/room-allotment/' + contractId,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: formData,
                beforeSend: function() {
                    triggerLoading();
                },
                success: response => {
                    if (Swal.isLoading()) Swal.close();
                    if (response.status == true) {
                        $('#modal-change-room-number').hide();
                        triggerAlert('success', '<h3>Success!</h3>Data room number successfully replaced<h5></h5>');
                        location.reload();
                    } else {
                        $('#modal-change-room-number').hide();
                        triggerAlert('error', '<h3>Oops!</h3><h5>Something wrong happened.<br/>'+response.meta.message+'</h5>');
                    }
                },
                error: error => {
                    if (Swal.isLoading()) Swal.close();
                    triggerAlert('error', '<h3>Oops!</h3><h5>Something wrong happened.<br/>Please try again in 10 minutes.</h5>');
                },
                complete: function () {
                    if (Swal.isLoading()) Swal.close();
                }
            });
        })

        $(document).on('change', 'form select[name=search_by]', function(e) {
            e.preventDefault();
            $('form input[name=search_value]').val('');
        });

        $(document).on('click', '.close-modal-change-room-number', function(e) {
            e.preventDefault();
            $('#modal-change-room-number').hide();
        });
    </script>
@endsection