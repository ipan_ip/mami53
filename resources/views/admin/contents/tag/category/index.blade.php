@extends('admin.layouts.main')

@section('style')
	<style>
		.table > tbody > tr > td
		{
			vertical-align: middle;
		}
		
		.font-large
		{
			font-size: 1.5em;
		}
		
		.font-semi-large
		{
			font-size: 1.25em;
		}
		
		.font-grey
		{
			color: #9E9E9E;
		}
		
		.dropdown-menu > li > a:hover
		{
			color: #333
		}
		
		.label-grey
		{
			background-color: #CFD8DC;
		}
		
		[hidden]
		{
			display: none !important;
		}
		
		/* Select2 tweak */
		
		.input-group .select2-container,
		.form-group .select2-container
		{
			position: relative !important;
			z-index: 2;
			float: left !important;
			width: 100% !important;
			margin-bottom: 0 !important;
			display: table !important;
			table-layout: fixed !important;
		}
		
		/* Sweetalert */
		
		.swal-shown
		{
			height: auto !important;
		}
		
		.custom-swal
		{
			z-index: 10000 !important;
		}
		
		.custom-wide-swal
		{
			width: 640px !important;
			font-size: 13px !important;
		}
		
		/* Custom table row */
		tr.inactive td
		{
			background-color: #E6C8C8;
		}
		
		/* Modal tweak */
		
		.modal-open
		{
			overflow: hidden;
			position: fixed;
			width: 100%;
		}
		
		
		/* Centering the modal */
		
		.modal
		{
			text-align: center;
			padding: 0 !important;
		}
		
		.modal:before
		{
			content: '';
			display: inline-block;
			height: 100%;
			vertical-align: middle;
			margin-right: -4px;
		}
		
		.modal-dialog
		{
			display: inline-block;
			text-align: left;
			vertical-align: middle;
		}
	</style>
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
	<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet"/>
	<link href="//cdn.jsdelivr.net/npm/select2-bootstrap-theme@0.1.0-beta.10/dist/select2-bootstrap.min.css"
	      rel="stylesheet"/>
@endsection

@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title"><i class="fa fa-bath"></i> {{ $boxTitle }}</h3>
            <div class="box-tools pull-right">
                <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <button type="submit" class="btn-add btn btn-primary btn-sm actions" data-action="add">
                        <i class="fa fa-plus">&nbsp;</i>Add Facility Category
                    </button>
                </div>
            </div>
            <table id="tableFacilityCategory" class="table table-striped">
                <thead>
                    <tr>
                        <th width="30px;">No</th>
                        <th>Name</th>
                        <th style="width:40%">Note</th>
                        <th>Created</th>
                        <th>Updated</th>
                        <th>Creator</th>
                        <th class="table-action-column"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($categories as $rowKey => $row)
                    <tr>
                        <td>{{ $rowKey+1 }}</td>
                        <td class="font-semi-large">{{ $row->name }}</td>
                        <td>{{ $row->note }}</td>
                        <td>
                            {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $row->created_at)->format('d M Y') }}
                            <br>
                            <small><i class="fa fa-clock-o"></i>  {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $row->created_at)->format('H:i') }}</small>
                        </td>
                        <td>
                            {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $row->updated_at)->format('d M Y') }}
                            <br>
                            <small><i class="fa fa-clock-o"></i> {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $row->updated_at)->format('H:i') }}</small>
                        </td>
                        <td>{{ $row->creator->name }}<br><small>{{ $row->creator->role }}</small></td>
                        <td class="text-center table-action-column" style="padding-right:15px!important;">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default btn-sm dropdown-toggle"
                                        data-toggle="dropdown">
                                    <i class="glyphicon glyphicon-cog"></i> Actions
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a href="#" class="actions" data-action="edit" data-data="{{ $row }}"><i
                                                    class="fa fa-edit"></i> Edit</a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a href="#" class="actions" data-action="remove"
                                           data-data="{{ $row }}"><i
                                                    class="fa fa-trash-o"></i> Remove</a>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
    <!-- table -->

    @include('admin.contents.tag.partials.category-modal')
@stop
@section('script')
<script src="//cdn.jsdelivr.net/npm/holderjs@2.9.6/holder.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.js"></script>
<!-- page script -->
<script type="text/javascript">
    function triggerAlert(type, message) {
        Swal.fire({
            type: type,
            customClass: {
                container: 'custom-swal'
            },
            html: message
        });
    }

    function triggerLoading(message) {
        Swal.fire({
            title: message,
            customClass: {
                container: 'custom-swal'
            },
            allowEscapeKey: false,
            allowOutsideClick: false,
            onOpen: () => {
                swal.showLoading();
            }
        });
    }

    function ajaxCall(type, title, html, method, url, payload, input, btnText, btnClass, needRefresh = true) {
        Swal.fire({
            type: type,
            title: title,
            html: html,
            buttonsStyling: btnClass == null,
            confirmButtonText: btnText,
            confirmButtonClass: btnClass,
            showCancelButton: true,
            cancelButtonClass: (btnClass !== null) ? 'btn btn-default' : null,
            customClass: 'custom-swal',
            showLoaderOnConfirm: true,
            input: input,
            preConfirm: (response) => {
                if (input !== null) {
                    let number = response;
                    if (!response) {
                        return Swal.showValidationMessage('Please provide a valid phone number!');
                    }

                    payload = {
                        "template_id": payload.id,
                        "number": response
                    };
                }

                console.log("This is the payload:", payload);

                return $.ajax({
                    type: method,
                    url: url,
                    data: payload,
                    dataType: 'json',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: (response) => {
                        if (response.success === false) {
                            Swal.showValidationMessage(response.message);
                        }

                        return;
                    },
                    error: (error) => {
                        Swal.showValidationMessage('Request Failed: ' + error.message);
                    }
                });
            },
            allowOutsideClick: () => !Swal.isLoading()
        }).then((result) => {
            if (!result.dismiss) {
                if (result.value.success == true) {
                    Swal.fire({
                        type: 'success',
                        title: result.value.title,
                        html: result.value.message,
                        onClose: () => {
                            if (needRefresh) window.location.reload();
                        }
                    });
                } else {
                    Swal.fire({
                        type: 'error',
                        title: "Failed!",
                        html: result.value.message
                    });
                }
            }
        });
    }


    $(function()
    {
        // Helper variable
        let _editing, _data, _modalTitle;
        let modal = $('#category-modal');

        // Actions button listeners
        $('.actions').click((e) => {
            e.preventDefault();

            const action = $(e.currentTarget).data('action');

            // Setup Notification
            if (action === 'add') {
                _editing = false;
                _modalTitle = "New Facility Category";

                modal.modal('show');
            }

            // Editing action
            else if (action === 'edit') {
                _editing = true;
                _data = $(e.currentTarget).data('data');
                _modalTitle = "Edit Facility Category";

                modal.modal('show');
            }

            // Remove action
            else if (action === 'remove') {
                _data = $(e.currentTarget).data('data');

                let payload = {
                    id: _data.id
                };

                let msg = '<h5>Deleting facility category will affect all Kos/Apartment using it!<br/><strong>Please do with caution!</strong></h5>';

                ajaxCall('warning', 'Facility Category Removal', msg + 'Click [Remove] to continue', 'POST', '/admin/facility/category/remove', payload, null, 'Remove', 'btn btn-danger');
            }
        });

        // :: Modal Events ::
        modal
            .on("show.bs.modal", (e) => {
                modal.data('bs.modal').options.keyboard = false;
                modal.data('bs.modal').options.backdrop = 'static';

                $('#modalTitle').html(_modalTitle);

                // Form elements
                const nameInput = $('#name');
                const noteInput = $('#note');
                const submitButton = $('#submit');

                function getFormData() {
                    return $('#addForm').serializeArray();;
                }

                function validateFormData() {
                    if (nameInput.val() === '') {
                        triggerAlert('error', "<h5>Category name is required!</h5>");
                        nameInput.parent().addClass('has-error');
                        nameInput.focus();
                        return false;
                    }

                    return true;
                }

                // Fill input elements value when in Editing mode
                if (_editing) {
                    nameInput.val(_data.name);
                    noteInput.val(_data.note);
                }

                // Submit the form
                submitButton.on('click', e => {
                    e.preventDefault();

                    if (validateFormData()) {
                        // compile dataObject
                        let dataObject = getFormData();

                        let title;
                        if (_editing) title = "Facility category successfully updated!";
                        else title = "New facility category successfully saved!";

                        Swal.fire({
                            type: 'question',
                            customClass: {
                                container: 'custom-swal'
                            },
                            title: 'Save new facility category?',
                            showCancelButton: true,
                            confirmButtonText: 'Submit',
                            showLoaderOnConfirm: true,
                            preConfirm: () => {
                                if (_editing) {
                                    dataObject.push({
                                        "name": "id",
                                        "value": _data.id
                                    });
                                }

                                return $.ajax({
                                    type: 'POST',
                                    url: "/admin/facility/category",
                                    dataType: 'json',
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                    },
                                    data: dataObject
                                });
                            },
                            allowOutsideClick: () => !Swal.isLoading()
                        }).then((result) => {
                            if (!result.dismiss) {
                                const response = result.value;

                                if (!response.success) {
                                    triggerAlert('error', '<h5>' + response.message + '</h5>');
                                } else {
                                    modal.modal('toggle');

                                    let title;
                                    if (_editing) title = "Facility category successfully updated!";
                                    else title = "New facility category successfully saved!";

                                    Swal.fire({
                                        type: 'success',
                                        customClass: {
                                            container: 'custom-swal'
                                        },
                                        title: title,
                                        html: "<h5>" + response.message + "</h5>",
                                        onClose: () => {
                                            window.location.reload();
                                        }
                                    });
                                }

                            }
                        })
                    }
                })
            })
            .on("shown.bs.nodal", (e) => {
                // 
            })
            .on("hide.bs.modal", (e) => {
                //
            });
    });
</script>
@stop