@extends('admin.layouts.main')

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

        @php
            $url = URL::to('admin/tag-max-renter');
            if (isset($tagMaxRenter)) {
                $url = URL::to('admin/tag-max-renter/' . $tagMaxRenter->id);
            }
        @endphp
        <form action="{{ $url }}" method="POST" class="form-horizontal form-bordered">
            @if (isset($tagMaxRenter))
            <input type="hidden" name="_method" value="PUT">
            @endif
            
            <div class="box-body no-padding">
                <div class="form-group bg-default">
                    <label for="select-tag" class="control-label col-sm-2">Tag</label>
                    <div class="col-sm-2">
                        <select name="tag_id" id="select-tag" class="form-control">
                            @php
                                $tagId = 0;
                                if (isset($tagMaxRenter)) {
                                    $tagId = $tagMaxRenter->tag->id;
                                }
                            @endphp
                            @foreach ($tags as $tag)
                            <option value="{{ $tag->id }}" @if (old('tag_id', $tag->id) == $tagId) selected="true" @endif>{{ $tag->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="input-max-renter" class="control-label col-sm-2">Max Renter</label>
                    <div class="col-sm-10">
                        @php
                            $tagMaxRenterNumber = 0;
                            if (isset($tagMaxRenter)) {
                                $tagMaxRenterNumber = $tagMaxRenter->max_renter;
                            }
                        @endphp
                        <input type="number" name="max_renter" id="input-max-renter" class="form-control" value="{{ old('max_renter', $tagMaxRenterNumber) }}">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <div class="col-sm-10 col-sm-push-2">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
