@extends('admin.layouts.main')

@section('style')
	<style>
		.table > tbody > tr > td
		{
			vertical-align: middle;
		}
		
		.font-large
		{
			font-size: 1.5em;
		}
		
		.font-semi-large
		{
			font-size: 1.25em;
		}
		
		.font-grey
		{
			color: #9E9E9E;
		}
		
		.dropdown-menu > li > a:hover
		{
			color: #333
		}
		
		.label-grey
		{
			background-color: #CFD8DC;
		}
		
		[hidden]
		{
			display: none !important;
		}
		
		/* Select2 tweak */
		
		.input-group .select2-container,
		.form-group .select2-container
		{
			position: relative !important;
			z-index: 2;
			float: left !important;
			width: 100% !important;
			margin-bottom: 0 !important;
			display: table !important;
			table-layout: fixed !important;
		}
		
		/* Sweetalert */
		
		.swal-shown
		{
			height: auto !important;
		}
		
		.custom-swal
		{
			z-index: 10000 !important;
		}
		
		.custom-wide-swal
		{
			width: 640px !important;
			font-size: 13px !important;
		}
		
		/* Custom table row */
		tr.inactive td
		{
			background-color: #E6C8C8;
		}
		
		/* Modal tweak */
		
		.modal-open
		{
			overflow: hidden;
			position: fixed;
			width: 100%;
		}
		
		
		/* Centering the modal */
		
		.modal
		{
			text-align: center;
			padding: 0 !important;
		}
		
		.modal:before
		{
			content: '';
			display: inline-block;
			height: 100%;
			vertical-align: middle;
			margin-right: -4px;
		}
		
		.modal-dialog
		{
			display: inline-block;
			text-align: left;
			vertical-align: middle;
		}

        /* orderable tweak */    
        #orderable tr 
        {
            cursor: move!important;
        }
	</style>
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
	<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet"/>
	<link href="//cdn.jsdelivr.net/npm/select2-bootstrap-theme@0.1.0-beta.10/dist/select2-bootstrap.min.css"
	      rel="stylesheet"/>
@endsection

@section('content')
	<!-- table -->
	<div class="box box-primary">
		<div class="box-header">
			<h3 class="box-title"><i class="fa fa-tags"></i> {{ $boxTitle }}</h3>
		</div><!-- /.box-header -->
		<div class="box-body no-padding">
			<div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default form-inline" style="padding-left: 10px;padding-right: 10px;">
					<!-- Add button -->
                    <div class="col-md-6 bg-default" style="padding-bottom:10px;">
                        <button type="submit" class="btn-add btn btn-primary btn-sm actions" data-action="add">
                            <i class="fa fa-plus">&nbsp;</i>Add Tag
                        </button>
                        <a href="{{ URL::route('admin.label.index') }}">
                            <button type="submit" class="btn-add btn btn-success btn-sm">
                                Label
                            </button>
                        </a>
                    </div>  
                    <!-- Search filters -->
                    <div class="col-md-6 bg-default" style="padding-bottom:10px;text-align:right">
                        <form action="" method="get">
                            <input type="text" name="q" class="form-control input-sm" placeholder="Search Tag Name"
                                autocomplete="off" value="{{ request()->input('q') }}">
                            <button class="btn btn-primary btn-sm" id="buttonSearch"><i
                                    class="fa fa-search">&nbsp;</i>Search</button>
                        </form>
                    </div>
                </div>
			</div>
			<div class="box-body">
				<table id="tableListTag" class="table table-striped">
					<thead>
					<tr>
						<th width="30px;">No</th>
						<th>Name</th>
                        <th>Type</th>
                        <th class="text-center">Photo Upload Enabled?</th>
                        <th class="text-center">Top facility?</th>
                        <th class="text-center">Show in List?</th>
                        <th class="text-center">Show in Filter?</th>
                        <th class="text-center">Icon</th>
						<th class="text-center">Icon Small</th>
						<th class="text-center">Updated</th>
						<th class="table-action-column"></th>
					</tr>
					</thead>
					<tbody id="orderable">
					@foreach ($rowsTag as $rowTagKey => $rowTag)
                        <tr data-id="{{$rowTag->id}}" class="{{!$rowTag->is_top? '':'able'}}">
							<td>{{ $rowTagKey+1 }}</td>
							<td class="font-semi-large">{{ $rowTag->name }}</td>
							<td>
                                @foreach ($rowTag->types as $type)
                                    <small>[{{ $type->name }}] </small>
                                @endforeach
                            </td>
                            <td class="text-center">
                                @if ($rowTag->is_photo_upload_enabled == 1)
                                    <span class="label label-success">Yes</span>
                                @else
                                    <span class="label label-default">No</span>
                                @endif
                            </td>
							<td class="text-center">
								@if ($rowTag->is_top == 1)
                                    <span class="label label-success">Yes</span>
                                @else
                                    <span class="label label-default">No</span>
								@endif
							</td>
                            <td class="text-center">
                                @if ($rowTag->code == 'list')
                                    <span class="label label-success">Yes</span>
                                @else
                                    <span class="label label-default">No</span>
                                @endif
                            </td>
                            <td class="text-center">
								@if ($rowTag->is_for_filter == 1)
                                    <span class="label label-success">Yes</span>
                                @else
                                    <span class="label label-default">No</span>
								@endif
							</td>
							<td class="text-center">
								@if($rowTag->photo)
									<img src="{{ $rowTag->photo->getMediaUrl()['real'] }}"
									    {{-- data-src="holder.js/60x60?text=Not \n Found" --}}
									    class="img-thumbnail" alt="">
								@else
									No Icon
								@endif
							</td>
							<td class="text-center">
								@if($rowTag->photoSmall)
									<img src="{{ $rowTag->photoSmall->getMediaUrl()['real'] }}"
									    {{-- data-src="holder.js/40x40?text=..." --}}
									    class="img-thumbnail" alt="">
								@else
									No Icon
								@endif
							</td>
							<td class="text-center">
                                {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $rowTag->updated_at)->format('d M Y') }}
                                <br>
                                <small><i class="fa fa-clock-o"></i> {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $rowTag->updated_at)->format('H:i') }}</small>
							</td>
							<td class="text-center table-action-column" style="padding-right:15px!important;">
								<div class="btn-group">
									<button type="button" class="btn btn-default btn-sm dropdown-toggle"
									        data-toggle="dropdown">
										<i class="glyphicon glyphicon-cog"></i> Actions
										<span class="caret"></span>
									</button>
									<ul class="dropdown-menu pull-right">
										<li>
											<a href="#" class="actions" data-action="edit" data-data="{{ $rowTag }}"><i
														class="fa fa-edit"></i> Edit</a>
                                        </li>
                                        <li class="divider"></li>
                                        <li>
                                            @if ($rowTag->is_photo_upload_enabled == 0)
                                                <a href="#" class="actions" data-action="enable" data-data="{{ $rowTag }}"><i
                                                            class="fa fa-upload"></i> Enable Photo Upload</a>
                                            @else
                                                <a href="#" class="actions" data-action="disable"
                                                    data-data="{{ $rowTag }}"><i
                                                            class="fa fa-ban"></i> Disable Photo Upload</a>
                                            @endif
                                        </li>
                                        <li>
                                            @if ($rowTag->is_top == 0)
                                                <a href="#" class="actions" data-action="set" data-data="{{ $rowTag }}"><i
                                                            class="fa fa-star"></i> Set As Top Fac</a>
                                            @else
                                                <a href="#" class="actions" data-action="unset"
                                                   data-data="{{ $rowTag }}"><i
                                                            class="fa fa-star-o"></i>Unset From Top Fac</a>
                                            @endif
                                        </li>
                                        @if ($rowTag->types->count() == 2)
                                        <li>
                                                <a href="#" class="actions" data-action="delete-second-tag-type" data-data="{{ $rowTag }}"><i
                                                            class="fa fa-eraser"></i> Delete Second Type </a>
                                        </li>
                                        @endif
                                        <li>
                                            @if ($rowTag->code == 'list')
                                                <a href="#" class="actions" data-action="undisplay" data-data="{{ $rowTag }}"><i
                                                            class="fa fa-check-square-o"></i> Remove from List</a>
                                            @else
                                                <a href="#" class="actions" data-action="display"
                                                   data-data="{{ $rowTag }}"><i
                                                            class="fa fa-check-square"></i> Display in List</a>
                                            @endif
                                        </li>
                                        <li>
                                            @if ($rowTag->is_for_filter == 0)
                                                <a href="#" class="actions" data-action="add-filter" data-data="{{ $rowTag }}"><i
                                                            class="fa fa-plus-square"></i> Add To Facility Filter </a>
                                            @else
                                                <a href="#" class="actions" data-action="remove-filter"
                                                   data-data="{{ $rowTag }}"><i
                                                            class="fa fa-minus-square"></i> Remove From Facility Filter</a>
                                            @endif
                                        </li>
										<li class="divider"></li>
										<li>
											<a href="#" class="actions" data-action="remove"
											   data-data="{{ $rowTag }}"><i
														class="fa fa-trash-o"></i> Remove</a>
										</li>
									</ul>
								</div>
							</td>
						</tr>
					@endforeach
					</tbody>
				</table>
			</div>
        </div><!-- /.box-body -->
        <div class="box-footer text-center">
            {{ $rowsTag->appends(Request::except('page'))->links() }}
        </div>
	</div><!-- /.box -->
	<!-- table -->
	
	@include('admin.contents.tag.partials.tag-modal')

@stop

@section('script')
	<script src="//cdn.jsdelivr.net/npm/holderjs@2.9.6/holder.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>
	<script src="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.js"></script>
	<!-- page script -->
	<script type="text/javascript">
        function triggerAlert(type, message) {
            Swal.fire({
                type: type,
                customClass: {
                    container: 'custom-swal'
                },
                html: message
            });
        }

        function triggerLoading(message) {
            Swal.fire({
                title: message,
                customClass: {
                    container: 'custom-swal'
                },
                allowEscapeKey: false,
                allowOutsideClick: false,
                onOpen: () => {
                    swal.showLoading();
                }
            });
        }

        function ajaxCall(type, title, html, method, url, payload, input, btnText, btnClass, needRefresh = true) {
            Swal.fire({
                type: type,
                title: title,
                html: html,
                buttonsStyling: btnClass == null,
                confirmButtonText: btnText,
                confirmButtonClass: btnClass,
                showCancelButton: true,
                cancelButtonClass: (btnClass !== null) ? 'btn btn-default' : null,
                customClass: 'custom-swal',
                showLoaderOnConfirm: true,
                input: input,
                preConfirm: (response) => {
                    if (input !== null) {
                        let number = response;
                        if (!response) {
                            return Swal.showValidationMessage('Please provide a valid phone number!');
                        }

                        payload = {
                            "template_id": payload.id,
                            "number": response
                        };
                    }

                    console.log("This is the payload:", payload);

                    return $.ajax({
                        type: method,
                        url: url,
                        data: payload,
                        dataType: 'json',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: (response) => {
                            if (response.success === false) {
                                Swal.showValidationMessage(response.message);
                            }

                            return;
                        },
                        error: (error) => {
                            Swal.showValidationMessage('Request Failed: ' + error.message);
                        }
                    });
                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                if (!result.dismiss) {
                    if (result.value.success == true) {
                        Swal.fire({
                            type: 'success',
                            title: result.value.title,
                            html: result.value.message,
                            onClose: () => {
                                if (needRefresh) window.location.reload();
                            }
                        });
                    } else {
                        Swal.fire({
                            type: 'error',
                            title: "Failed!",
                            html: result.value.message
                        });
                    }
                }
            });
        }

        $(function () {
            // Helper variable
            let _editing, _data, _modalTitle;
            let modal = $('#tag-modal');

            // Table orderable handler
            $('#orderable').sortable({
                cursor: 'move',
                axis: 'y',
                items : '> tr.able',
                update: (e, ui) => {
                    triggerLoading('Menyimpan urutan...');

                    var order = []; 
                    $('#orderable tr').each( function(e) {
                        order.push({
                            id: parseInt($(this).data('id')),
                            order: $(this).index() + 1
                        });
                    });
                    
                    order = JSON.stringify(order);
                    $.ajax({
                        type: 'POST',
                        url: "/admin/tag/order",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        contentType: 'application/json',
                        data: order,
                        success: (response) => {
                            if (!response.success) {
                                triggerAlert('error', response.message);
                                return;
                            }

                            Swal.fire({
                                type: 'success',
                                title: "Berhasil menyimpan urutan!",
                                text: "Halaman akan di-refresh setelah Anda klik OK",
                                onClose: () => {
                                    window.location.reload();
                                }
                            });
                        },
                        error: (error) => {
                            triggerAlert('error', error.message);
                        }
                    });
                }
            });

            // Actions button listeners
            $('.actions').on('click', e => {
                e.preventDefault();

                const action = $(e.currentTarget).data('action');

                // Setup Notification
                if (action === 'add') {
                    _editing = false;
                    _modalTitle = "<i class='fa fa-tag'></i> New Facility Tag";

                    modal.modal('show');
                }

                // Editing action
                else if (action === 'edit') {
                    _editing = true;
                    _data = $(e.currentTarget).data('data');
                    _modalTitle = "<small>Update Facility Tag:</small><br/><i class='fa fa-tag'></i> " + _data.name;

                    modal.modal('show');
                }

                // Enable / Disable photos upload
                else if (action === 'enable' || action === 'disable') {
                    let title, html, btnText, type, payload;

                    _data = $(e.currentTarget).data('data');

                    if (action === 'enable') {
                        type = 'question';
                        title = 'Enable photo upload for tag ' + _data.name.toUpperCase() + '?';
                        html = null;
                        btnText = 'Enable';
                        payload = {
                            id: _data.id
                        };
                    } else {
                        type = 'warning';
                        title = 'Disable photo upload for tag ' + _data.name.toUpperCase() + '?';
                        html = null;
                        btnText = 'Disable';
                        payload = {
                            id: _data.id
                        };
                    }

                    ajaxCall(type, title, html, 'POST', "/admin/tag/" + action, payload, null, btnText, null);
                }

                // Set / Unset as Top Facility
                else if (action === 'set' || action === 'unset') {
                    let title, html, btnText, type, payload;

                    _data = $(e.currentTarget).data('data');

                    if (action === 'set') {
                        type = 'question';
                        title = 'Set ' + _data.name.toUpperCase() + ' as Top Facility?';
                        html = null;
                        btnText = 'Set';
                        payload = {
                            id: _data.id
                        };
                    } else {
                        type = 'warning';
                        title = 'Unset ' + _data.name.toUpperCase() + ' from Top Facility?';
                        html = null;
                        btnText = 'Unset';
                        payload = {
                            id: _data.id
                        };
                    }

                    ajaxCall(type, title, html, 'POST', "/admin/tag/" + action, payload, null, btnText, null);
                }

                // Display / undisplay in story/list
                else if (action === 'display' || action === 'undisplay') {
                    let title, html, btnText, type, payload;

                    _data = $(e.currentTarget).data('data');

                    if (action === 'display') {
                        type = 'question';
                        title = 'Display ' + _data.name.toUpperCase() + ' to List?';
                        html = null;
                        btnText = 'Display';
                        payload = {
                            id: _data.id
                        };
                    } else if (action === 'undisplay') {
                        type = 'warning';
                        title = 'Undisplay ' + _data.name.toUpperCase() + ' from list?';
                        html = null;
                        btnText = 'Undisplay';
                        payload = {
                            id: _data.id
                        };
                    }

                    ajaxCall(type, title, html, 'POST', "/admin/tag/" + action, payload, null, btnText, null);
                }

                // add/remove facility from Facility Filter
                else if (action === 'add-filter' || action === 'remove-filter') {
                    let title, html, btnText, type, payload;

                    _data = $(e.currentTarget).data('data');

                    if (action === 'add-filter') {
                        type = 'question';
                        title = 'Add ' + _data.name.toUpperCase() + ' to filter?';
                        html = null;
                        btnText = 'Add';
                        payload = {
                            id: _data.id
                        };
                    } else if (action === 'remove-filter') {
                        type = 'warning';
                        title = 'Remove ' + _data.name.toUpperCase() + ' from filter?';
                        html = null;
                        btnText = 'Remove';
                        payload = {
                            id: _data.id
                        };
                    }

                    ajaxCall(type, title, html, 'POST', "/admin/tag/" + action, payload, null, btnText, null);
                }                

                // Remove action
                else if (action === 'remove') {
                    _data = $(e.currentTarget).data('data');

                    let payload = {
                        id: _data.id
                    };

                    let msg = '<h5>Deleting tag will affect all Kos/Apartment using it!<br/><strong>Please do with caution!</strong></h5>';

                    ajaxCall('warning', 'Tag Removal', msg + 'Click [Remove] to continue', 'POST', '/admin/tag/remove', payload, null, 'Remove', 'btn btn-danger');
                }

                else if (action === 'delete-second-tag-type') {
                    _data = $(e.currentTarget).data('data');

                    let payload = {
                        id: _data.id
                    };

                    let msg = '<h5>Delete Second Label</h5>';

                    ajaxCall('warning', 'Type Removal', msg + 'Click [Remove] to continue', 'POST', '/admin/tag/delete-second-type', payload, null, 'Remove', 'btn btn-danger');
                }
            });

            // :: Modal Events ::
            modal
                .on("show.bs.modal", (e) => {
                    if (!_editing) {
                        $('#tip').html("Photo upload support will be enabled by default.<br>You may change it later by using button [<i class='fa fa-wrench'></i> Actions] > [<i class='fa fa-ban'></i> Disable Photo Upload]");
                    }

                    modal.data('bs.modal').options.keyboard = false;    
                    modal.data('bs.modal').options.backdrop = 'static';

                    $('#modalTitle').html(_modalTitle);

                    // Form elements
                    const form = $('#tagForm');
                    const nameInput = $('#inputName');
                    const labelInput = $('#inputLabel');
                    const labelInput2 = $('#inputLabel2');
                    const iconInput = $('#inputIcon');
                    const iconSmallInput = $('#inputIconSmall');
                    const submitButton = $('#submit');


                    function getFormData() {
                        // const formData = form.serializeArray();
                        let formData = new FormData($(form)[0]);

                        if (_editing) {
                            formData.append('id', _data.id);
                        }

                        return formData;
                    }

                    function initialState() {
                        if (_editing) {
                            nameInput.val(_data.name);
                            if (_data.types[0]) {
                            labelInput.val(_data.types[0].name);
                            }
                            if (_data.types[1]) {
                            labelInput2.val(_data.types[1].name);
                            }
                        }
                    }

                    function validateFormData() {

                        if (nameInput.val() === '') {
                            triggerAlert('error', "<h5>Tag name required!</h5>");
                            nameInput.parent().addClass('has-error');
                            nameInput.focus();
                            return false;
                        }

                        if (labelInput.val() === '') {
                            triggerAlert('error', "<h5>Tag label required! Fill it with <strong>fac_room</strong>, <strong>fac_share</strong> or <strong>fac_bath</strong>.</h5>");
                            labelInput.parent().addClass('has-error');
                            labelInput.focus();
                            return false;
                        }

                        return true;
                    }
                    // Main actions
                    initialState()

                    // Events
                    nameInput.on('keyup', () => {
                        if (nameInput.parent().hasClass('has-error')) {
                            nameInput.parent().removeClass('has-error');
                        }
                    });


                    // Submit the form
                    submitButton.on('click', e => {
                        e.preventDefault();

                        if (validateFormData()) {
                            // compile dataObject
                            const dataObject = getFormData();
                            
                            Swal.fire({
                                type: 'question',
                                customClass: {
                                    container: 'custom-swal'
                                },
                                title: 'Save facility tag data?',
                                showCancelButton: true,
                                confirmButtonText: 'Submit',
                                showLoaderOnConfirm: true,
                                preConfirm: () => {
                                    return $.ajax({
                                        type: 'POST',
                                        url: "/admin/tag",
                                        dataType: 'json',
                                        processData: false,
                                        contentType: false,
                                        headers: {
                                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                        },
                                        data: dataObject
                                    });
                                },
                                allowOutsideClick: () => !Swal.isLoading()
                            }).then((result) => {
                                if (!result.dismiss) {
                                    const response = result.value;

                                    if (!response.success) {
                                        triggerAlert('error', '<h5>' + response.message + '</h5>');
                                    } else {
                                        $('#tag-modal').modal('toggle');
                                        
                                        if (_editing) {
                                            title = "Facility tag successfully updated";
                                        } else {
                                            title = "Facility tag successfully added";
                                        }

                                        Swal.fire({
                                            type: 'success',
                                            customClass: {
                                                container: 'custom-swal'
                                            },
                                            title: title,
                                            html: "<h5>Page will be refreshed after you clicked OK</h5>",
                                            onClose: () => {
                                                window.location.reload();
                                            }
                                        });
                                    }

                                }
                            });
                        }
                    })
                })
                .on("hide.bs.modal", (e) => {
                    // Reset all selectors
                    $('#inputSelectCategory').empty().select2('destroy');
                    $('#inputSelectType').empty().select2('destroy');

                    $('#tip').html('');
                });
        });
	</script>
@stop