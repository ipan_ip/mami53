<div class="modal" id="tag-modal" role="dialog" aria-labelledby="modalLabel">
	<div class="modal-dialog" role="modal">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top:2px;"><i class="fa fa-lg fa-times-circle"></i></button>
				<h4 class="modal-title"><span id="modalTitle"></span></h4>
			</div>
			<div class="modal-body">
				<form id="tagForm" action="#" method="post" enctype="multipart/form-data">
					<div class="row" style="margin-top:10px;">
						<div class="col-md-8">
							<div class="form-group">
								<label for="inputLabel" class>Label 1</label>
								<select class="form-control chosen-select" id="inputLabel" name="label">
									<option value='kos_rule'>kos_rule</option>
									<option value='keyword'>keyword</option>
									<option value='emotion' >emotion</option>
									<option value='concern' >concern</option>
									<option value='theme' >theme</option>
									<option value='category'>category</option>
									<option value='gender'>gender</option>
									<option value='period'>period</option>
									<option value='fac_room'>fac_room</option>
									<option value='fac_share'>fac_share</option>
									<option value='fac_bath'>fac_bath</option>
									<option value='fac_outside' >fac_outside</option>
									<option value='fac_park'>fac_park</option>
									<option value='fac_near'>fac_near</option>
									<option value='fac_project'>fac_project</option>
									<option value='fac_price'>fac_price</option>
								</select>
							</div>
						</div>
						<div class="col-md-8">
							<div class="form-group">
								<label for="inputLabel2" class>Label 2 (optional only for new type kos_rule)</label>
								<select class="form-control chosen-select" id="inputLabel2" name="label2">
									<option value=''></option>
									<option value='kos_rule'>kos_rule</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row" style="margin-top:10px;">
						<div class="col-md-8">
							<div class="form-group">
								<label for="inputName">Name</label>
								<input type="text" class="form-control" id="inputName" name="name">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-5">
							<div class="form-group">
								<label for="inputIcon">Icon Large</label>
								<input type="file" name="icon" id="inputIcon">
							</div>
						</div>
						<div class="col-md-5">
							<div class="form-group">
								<label for="inputIconSmall">Icon Small</label>
								<input type="file" name="icon_small" id="inputIconSmall">
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<div class="row">
					<div class="col-md-8 text-left">
						<small id="tip"></small>
					</div>
					<div class="col-md-4">
						<button type="button" class="btn btn-primary" id="submit">
							<i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save
						</button>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</div>