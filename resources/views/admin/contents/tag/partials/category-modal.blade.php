<div class="modal" id="category-modal" role="dialog" aria-labelledby="modalLabel">
	<div class="modal-dialog" role="modal">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top:2px;"><i
							class="fa fa-lg fa-times-circle"></i></button>
				<h4 class="modal-title"><i class="fa fa-plus"></i>&nbsp;&nbsp;<span id="modalTitle"></span></h4>
			</div>
			<div class="modal-body">
				<form id="addForm" action="" method="post">
					<div class="row">
						<div class="col-md-8">
							<div class="form-group">
								<label for="name">Name</label>
								<input type="text" id="name" name="name" class="form-control">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="note">Note</label>
								<textarea class="form-control" name="note" id="note" rows="3" style="width: 100%"></textarea>
								<span class="help-block">*Optional</span>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="submit">
					<i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save
				</button>
			</div>
		</div>
	</div>
</div>