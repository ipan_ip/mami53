@extends('admin.layouts.main')

@section('content')
    <?php App::setLocale('id'); ?>
    <!-- table -->
    <div class="box box-primary">
        <!-- box-header -->
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>
        <!-- box-header -->

        <!-- box-body -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <!-- Top bar -->
                <div class="btn-horizontal-group bg-default form-inline" style="padding-left: 10px;">
                    @if(Auth::user())
                        <div class="form-group col-md-6 bg-default" style="padding-bottom:10px;text-align:left">
                            <label
                                for="parent"
                                class="col-sm-2 control-label"
                                style="margin-top: 1%;"
                            >
                                Filter by Parents
                            </label>
                            <div class="col-sm-10">
                                <form
                                    action="{{ route("admin.sanjunipero.child.index") }}"
                                    method="get"
                                >
                                    <select
                                        class="form-control chosen-select"
                                        id="parent"
                                        name="parent"
                                        tabindex="1"
                                        data-placeholder="Choose parent"
                                    >
                                        <option value=0>All</option>
                                        @foreach ($parents as $parent)
                                            <option
                                                value="{{ $parent->id }}"
                                                {{ ! empty($parent_id) && ($parent_id == $parent->id) ? 'selected' : '' }}
                                            >
                                                {{ ucfirst($parent->name) }}
                                            </option>
                                        @endforeach
                                    </select>

                                    <button class="btn btn-success btn-md" id="buttonSearch">
                                        <i class="fa fa-search">&nbsp;</i>Search
                                    </button>
                                </form>
                            </div>
                        </div>

                        <div class="col-md-6 bg-default" style="padding-bottom:10px;text-align:right;padding-left: 0px">
                            <a href="{{ URL::route('admin.sanjunipero.child.create') }}">
                                <button type="button" name="button" class="btn-add btn btn-primary btn-sm" style="margin-left:0px">
                                    <i class="fa fa-plus">&nbsp;</i> Add New child
                                </button>
                            </a>
                        </div>
                    @endif
                </div>
            </div>

            <table id="tableListLanding" class="table table-striped">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Slug</th>
                        <th>Parent (ID || Slug)</th>
                        <th>Area Type</th>
                        <th>Area Name</th>
                        <th>Last Update</th>
                        <th class="table-action-column" width="120px">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($rows as $key => $row)
                        <tr class="{{ $row->is_active === 1 ? 'active-sanjunipero-child' : '' }}">
                            <td>{{ ($key+1) }}</td>
                            <td>{{ $row->slug }}</td>
                            <td>{{ $row->parent->id }} || {{ $row->parent->slug }}</td>
                            <td>{{ $row->area_type }}</td>
                            <td>{{ $row->area_name }}</td>
                            <td>{{ $row->updated_at }}</td>
                            <td class="table-action-column">
                                <div class="btn-action-group">
                                    @permission('access-landing-page')
                                        <a href="{{ route("admin.sanjunipero.parent.edit", $row->parent->id) }}" title="Edit Parent">
                                            <i class="fa fa-files-o"></i>
                                        </a>
                                        <a href="{{ route("admin.sanjunipero.child.edit", $row->id) }}" title="Edit Child">
                                            <i class="fa fa-edit"></i>
                                        </a>

                                        @if ($row->is_active === 0)
                                            <a class="activateChildSanjunipero" data-url="{{ route("admin.sanjunipero.child.activate", $row->id) }}" title="Activate">
                                                <i class="fa fa-check-circle-o"></i>
                                            </a>
                                        @else
                                            <a class="deactivateChildSanjunipero" data-url="{{ route("admin.sanjunipero.child.deactivate", $row->id) }}" title="Deactivate">
                                                <i class="fa fa-circle-o"></i>
                                            </a>
                                        @endif

                                        <a href="{{ url("/kos/{$row->url_landing}")}}" title="Open {{$row->area_name}} page">
                                            <i class="fa fa-external-link"></i>
                                        </a>
                                    @endpermission
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>No</th>
                        <th>Slug</th>
                        <th>Parent (ID || Slug)</th>
                        <th>Area Type</th>
                        <th>Area Name</th>
                        <th>Last Update</th>
                        <th class="table-action-column" width="120px">Action</th>
                    </tr>
                </tfoot>
            </table>
            <div class="box-body no-padding">
                @if (empty($parent_id))
                    {{ $rows->links() }}
                @endif
            </div>
        </div>
        <!-- box-body -->

    </div>
@stop

@section('style')
    {{ HTML::style('assets/vendor/jquery-chosen/chosen.css') }}
    {{ HTML::style('assets/vendor/jquery-chosen-custom/chosen.css') }}
    <link rel="stylesheet" href="/assets/vendor/summernote/summernote.css?v=05042018">

    <style>
        #chosenForm .chosen-choices {
            border: 1px solid #ccc;
            border-radius: 4px;
            min-height: 34px;
            padding: 6px 12px;
        }
        #chosenForm .form-control-feedback {
            /* To make the feedback icon visible */
            z-index: 100;
        }

        tr.active-sanjunipero-child td {
            background-color: #C8E6C9 !important;
        }
    </style>
@stop

@section('script')
    {{ HTML::script('assets/vendor/jquery-chosen/chosen.jquery.js') }}
    <script src="/assets/vendor/summernote/summernote.min.js"></script>
    <script type="text/javascript">
        var config = {
            '.chosen-select': {
                width: '70%',
                max_selected_options: 3
            }
        }
        for (var selector in config) {
            $(selector).chosen(config[selector])
        }

        $(document).ready(function(){
            $('.activateChildSanjunipero').click(function(){alert('activate');
                $.post( $(this).data('url'), function( response ) {
                    afterSuccessIsActive(response, this)
                });
            })

            $('.deactivateChildSanjunipero').click(function(){alert('deactivate');
                $.post( $(this).data('url'), function( response ) {
                    afterSuccessIsActive(response, this)
                });
            })

            function afterSuccessIsActive(response, element) {
                alert(response.message)
                location.reload(true)
            }
        })
    </script>
@stop