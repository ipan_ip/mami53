@extends('admin.layouts.main')

@section('content')
<div class="box box-primary">
    <!-- box-header -->
    <div class="box-header">
        <h3 class="box-title">{{ $boxTitle }}</h3>
    </div>
    <!-- box-header -->

    {{ 
        Form::open([
            'url' => $formUrl,
            'method' => $formMethod,
            'class' => 'form-horizontal form-bordered',
            'role' => 'form',
            'id' => 'formInsertChild',
            'enctype' => 'multipart/form-data'
        ])
    }}

    <div class="box-body no-padding">

        <div class="form-group bg-default">
            <label
                for="parentId"
                class="col-sm-2 control-label"
            >
                Parents
            </label>
            <div class="col-sm-8">
                <select
                    class="form-control chosen-select"
                    id="parentId"
                    name="parent_id"
                    tabindex="1"
                    data-placeholder="Choose parent"
                >
                    @foreach ($row->parent_id as $type)
                        <option value="{{ $type->id }}"{{ $type->selected }}>{{ ucfirst($type->name) }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group bg-default">
            <label 
                for="inputIcon"
                class="col-sm-2 control-label"
            >
                Area Type
            </label>
            <div class="col-sm-2">
                <select
                    name="area_type"
                    id=""
                    class="form-control"
                >
                    @foreach ($row->area_types as $type)
                        <option value="{{$type}}"
                            {{ ( (!empty($row->area_type)) && ($type === $row->area_type) ) ? 'selected' : '' }}
                        >
                            {{ucfirst($type)}}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group bg-default">
            <label
                for="image"
                class="col-sm-2 control-label"
            >
                Area Name
            </label>
            <div class="col-sm-8">
                <input 
                    type="text"
                    class="form-control"
                    placeholder="For area name"
                    id="areaName"
                    name="area_name"
                    value="{{$row->area_name}}"
                >
            </div>
        </div>

        <div class="form-group bg-default">
            <label
                for="image"
                class="col-sm-2 control-label"
            >
                Slug
            </label>
            <div class="col-sm-8">
                <input 
                    type="text"
                    class="form-control"
                    placeholder="For slug"
                    id="slug"
                    name="slug"
                    value="{{$row->slug}}"
                >
            </div>
        </div>

        <div class="form-group bg-default">
            <label for="location" class="col-sm-2 control-label">Location</label>
            <div class="col-sm-4">
                <input
                    type="text"
                    class="form-control"
                    placeholder="Search Location"
                    id="inputGeoName"
                    name="geo_name"
                />
                <div>
                    <div style="background: #EEE; width: 560px; height: 470px; margin: 20px 0px">
                        <div id="map-canvas"></div>
                    </div>
                </div>
                <div class="input-group">
                    <div class="input-group-addon">Kiri Bawah (latitude, longitude)</div>
                    <input
                        type="text"
                        class="form-control"
                        placeholder="Latitude, Longitude"
                        id="inputCoordinateBottomLeft"
                        name="coordinate_1"
                        value="{{ $row->coordinate_1 }}"
                    >
                </div>
                <div class="input-group">
                    <div class="input-group-addon">Kanan Atas (latitude, longitude)</div>
                    <input
                        type="text"
                        class="form-control"
                        placeholder="Latitude, Longitude"
                        id="inputCoordinateTopRight"
                        name="coordinate_2"
                        value="{{ $row->coordinate_2 }}">
                </div>
            </div>
        </div>
        
        <div class="form-group bg-default">
            <label
                for="meta description"
                class="col-sm-2 control-label"
            >
                Meta Description
            </label>
            <div class="col-sm-10">
                <textarea
                    rows="8"
                    type="text"
                    class="form-control"
                    id="metaDescription"
                    name="meta_desc"
                >{{$row->meta_desc}}</textarea>
            </div>
        </div>

        <div class="form-group bg-default">
            <label
                for="meta keywords"
                class="col-sm-2 control-label"
            >
                Meta Keywords
            </label>
            <div class="col-sm-10">
                <textarea
                    rows="8"
                    type="text"
                    class="form-control"
                    id="metaKeywords"
                    name="meta_keywords"
                >{{$row->meta_keywords}}</textarea>
            </div>
        </div>

        <div class="form-group bg-default">
            <label
                for="active-record"
                class="col-sm-2 control-label"
            >
                Active
            </label>
            <div class="col-sm-10">
                <input
                    type="checkbox"
                    name="is_active"
                    id="is_active"
                    value=1
                    {{ $row->is_active === 1 ? 'checked="checked"' : '' }}
                >
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button
                    type="submit"
                    class="btn btn-primary"
                >
                    Save
                </button>
                @if (Request::is('admin/*'))
                    <a href="{{ $formCancel }}">
                        <button type="button" class="btn btn-default">Cancel</button>
                    </a>
                @endif
            </div>
        </div>

    </div>

    {{ Form::close() }}
</div>
@stop

@section('style')
    {{ HTML::style('assets/vendor/jquery-chosen/chosen.css') }}
    {{ HTML::style('assets/vendor/jquery-chosen-custom/chosen.css') }}
    <link rel="stylesheet" href="{{ mix_url('dist/vendor/leaflet/leaflet.css') }}">
    <link rel="stylesheet" href="/assets/vendor/summernote/summernote.css?v=05042018">

    <style>
        #chosenForm .chosen-choices {
            border: 1px solid #ccc;
            border-radius: 4px;
            min-height: 34px;
            padding: 6px 12px;
        }
        #chosenForm .form-control-feedback {
            /* To make the feedback icon visible */
            z-index: 100;
        }
    </style>
@stop

@section('script')
    {{ HTML::script('assets/vendor/jquery-chosen/chosen.jquery.js') }}
    <script src="https://maps.googleapis.com/maps/api/js?libraries=places&language=id&key={{ENV('MIX_GOOGLE_MAPS_JAVASCRIPT_API_KEY')}}"></script>
    <script src="{{ mix_url('dist/vendor/leaflet/leaflet.js') }}"></script>
    <script src="/assets/vendor/summernote/summernote.min.js"></script>
    <script type="text/javascript">
        var config = {
            '.chosen-select': {
                width: '100%',
                max_selected_options: 3
            }
        }
        for (var selector in config) {
            $(selector).chosen(config[selector])
        }

        $(document).ready(function(){
            $('#slug').focusout(function(){
                var text = $(this).val()
                $(this).val(slugify(text))
            })
        })

        function slugify(string) {
            const a = 'àáâäæãåāăąçćčđďèéêëēėęěğǵḧîïíīįìłḿñńǹňôöòóœøōõőṕŕřßśšşșťțûüùúūǘůűųẃẍÿýžźż·/_,:;'
            const b = 'aaaaaaaaaacccddeeeeeeeegghiiiiiilmnnnnoooooooooprrsssssttuuuuuuuuuwxyyzzz------'
            const p = new RegExp(a.split('').join('|'), 'g')

            return string.toString().toLowerCase()
                .replace(/\s+/g, '-') // Replace spaces with -
                .replace(p, c => b.charAt(a.indexOf(c))) // Replace special characters
                .replace(/&/g, '-and-') // Replace & with 'and'
                .replace(/[^\w\-]+/g, '') // Remove all non-word characters
                .replace(/\-\-+/g, '-') // Replace multiple - with single -
                .replace(/^-+/, '') // Trim - from start of text
                .replace(/-+$/, '') // Trim - from end of text
        }

        var form = document.getElementById('formInsertChild')
        if (form.addEventListener) {
            form.addEventListener("submit", function(e){
                var reg = /^[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?),\s*[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?)$/
                var inputCoordinateBottomLeftTest = reg.test( document.getElementById('inputCoordinateBottomLeft').value.trim() )
                var inputCoordinateTopRightTest = reg.test( document.getElementById('inputCoordinateTopRight').value.trim() )

                if ( (inputCoordinateBottomLeftTest !== true) || (inputCoordinateTopRightTest !== true) ) {
                    e.preventDefault()
                    alert('Please check your location (latitude, longitude)')
                }
            })
        }

        // leaflet starts here
        function addMarkerListener(map, marker, pos) {
            marker.on('dragend', function(evt) {
                var latlng = evt.target.getLatLng();

                var lat = latlng.lat;
                var lng = latlng.lng;

                $('#inputCoordinate' + pos).val(lat + ',' + lng);
            });
        }

        function addPlaceChangedListener(autocomplete, map, marker) {
            autocomplete.addListener('place_changed', function() {
                marker[0].setOpacity(0);
                marker[1].setOpacity(0);

                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    // User entered the name of a Place that was not suggested and
                    // pressed the Enter key, or the Place Details request failed.
                    window.alert(
                        "No details available for input: '" + place.name + "'"
                    );
                    return;
                }

                // map.setCenter(place.geometry.location);
                map.setView(
                    {
                        lat: place.geometry.location.lat(),
                        lng: place.geometry.location.lng()
                    },
                    12
                );

                bottomLeft = {
                    lat: place.geometry.location.lat() - 0.05,
                    lng: place.geometry.location.lng() - 0.05
                };

                topRight = {
                    lat: place.geometry.location.lat() + 0.05,
                    lng: place.geometry.location.lng() + 0.05
                };

                marker[0].setLatLng(bottomLeft);
                marker[1].setLatLng(topRight);
                marker[0].setOpacity(1);
                marker[1].setOpacity(1);

                $('#inputCoordinateBottomLeft').val(
                    bottomLeft.lat + ',' + bottomLeft.lng
                );
                $('#inputCoordinateTopRight').val(topRight.lat + ',' + topRight.lng);
            });
        }

        @if (!is_null($row->coordinate_1) && !is_null($row->coordinate_2))
            centerPos = {
                lat: parseFloat(
                    '{{ (float) (($row->latitude_1 + $row->latitude_2) / 2) }}'
                ),
                lng: parseFloat(
                    '{{ (float) (($row->longitude_1 + $row->longitude_2) / 2) }}'
                )
            };
        @else
            centerPos = { lat: -7.7858485, lng: 110.3680087 };
        @endif

        var map = L.map('map-canvas', {
            // Set latitude and longitude of the map center (required)
            center: centerPos,
            // Set the initial zoom level, values 0-18, where 0 is most zoomed-out (required)
            zoom: 12
        });

        var tiles = new L.tileLayer("{{ config('services.osm.host') }}/hot/{z}/{x}/{y}.png").addTo(map);

        var marker = [];
        var markerImage = [];
        var position = [];

        // marker kiri bawah
        markerImage[0] = '{{ asset('assets/icons/kiribawah.png') }}';

        @if(!is_null($row->latitude_1))
            position[0] = {
                lat: parseFloat('{{ $row->latitude_1 }}'),
                lng: parseFloat('{{ $row->longitude_1 }}')
            };
        @else
            position[0] = {
                lat: centerPos.lat - 0.05,
                lng: centerPos.lng - 0.05
            };
        @endif

        marker[0] = new L.Marker(position[0], {
            draggable: true,
            icon: new L.icon({
                iconUrl: markerImage[0],
                iconSize: [60, 57],
                iconAnchor: [30, 57]
            })
        }).addTo(map);

        addMarkerListener(map, marker[0], 'BottomLeft');

        // marker kanan atas
        markerImage[1] = '{{ asset('assets/icons/kananatas.png') }}';

        @if(!is_null($row->latitude_2))
            position[1] = {
                lat: parseFloat('{{ $row->latitude_2 }}'),
                lng: parseFloat('{{ $row->longitude_2 }}')
            };
        @else
            position[1] = {
                lat: centerPos.lat + 0.05,
                lng: centerPos.lng + 0.05
            };
        @endif

        marker[1] = new L.Marker(position[1], {
            draggable: true,
            icon: new L.icon({
                iconUrl: markerImage[1],
                iconSize: [60, 57],
                iconAnchor: [30, 57]
            })
        }).addTo(map);

        addMarkerListener(map, marker[1], 'TopRight');

        var autocomplete = new google.maps.places.Autocomplete(document.getElementById('inputGeoName'));
        autocomplete.setFields(['address_components', 'geometry']);

        addPlaceChangedListener(autocomplete, map, marker);
    </script>
@stop