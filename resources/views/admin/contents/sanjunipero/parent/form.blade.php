@extends('admin.layouts.main')

@section('content')
<div class="box box-primary">
    <!-- box-header -->
    <div class="box-header">
        <h3 class="box-title">{{ $boxTitle }}</h3>
    </div>
    <!-- box-header -->

    {{ 
        Form::open([
            'url' => $formUrl,
            'method' => $formMethod,
            'class' => 'form-horizontal form-bordered',
            'role' => 'form',
            'id' => 'formInsertDesigner',
            'enctype' => 'multipart/form-data'
        ])
    }}

    <div class="box-body no-padding">

        <div class="form-group bg-default">
            <label
                for="image"
                class="col-sm-2 control-label"
            >
                Slug
            </label>
            <div class="col-sm-10">
                <input 
                    type="text"
                    class="form-control"
                    placeholder="For slug"
                    id="slug"
                    name="slug"
                    value="{{$row->slug}}"
                >
            </div>
        </div>

        <div class="form-group bg-default">
            <label
                for="typeKost"
                class="col-sm-2 control-label"
            >
                Type Kost
            </label>
            <div class="col-sm-10">
                <select
                    class="form-control chosen-select"
                    multiple
                    id="typeKost"
                    name="type_kost[]"
                    tabindex="1"
                    data-placeholder="Choose type"
                >
                    @foreach ($row->type_kost as $type)
                        <option value="{{ $type->name }}"{{ $type->selected }}>{{ ucfirst($type->name) }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group bg-default">
            <label
                for="titleTag"
                class="col-sm-2 control-label"
            >
                Title Tag
            </label>
            <div class="col-sm-10">
                <input 
                    type="text"
                    class="form-control"
                    placeholder="For title tag"
                    id="titleTag"
                    name="title_tag"
                    value="{{$row->title_tag}}"
                >
            </div>
        </div>

        <div class="form-group bg-default">
            <label
                for="titleHeader"
                class="col-sm-2 control-label"
            >
                Title Header
            </label>
            <div class="col-sm-10">
                <input 
                    type="text"
                    class="form-control"
                    placeholder="For title header"
                    id="titleHeader"
                    name="title_header"
                    value="{{$row->title_header}}"
                >
            </div>
        </div>

        <div class="form-group bg-default">
            <label
                for="subtitleHeader"
                class="col-sm-2 control-label"
            >
                Subtitle Header
            </label>
            <div class="col-sm-10">
                <input 
                    type="text"
                    class="form-control"
                    placeholder="For subtitle header"
                    id="subtitle_header"
                    name="subtitle_header"
                    value="{{$row->subtitle_header}}"
                >
            </div>
        </div>

        <div class="form-group bg-default">
            <label
                for="meta description"
                class="col-sm-2 control-label"
            >
                Meta Description
            </label>
            <div class="col-sm-10">
                <textarea
                    rows="8"
                    type="text"
                    class="form-control"
                    id="metaDescription"
                    name="meta_desc"
                >{{$row->meta_desc}}</textarea>
            </div>
        </div>

        <div class="form-group bg-default">
            <label
                for="meta keywords"
                class="col-sm-2 control-label"
            >
                Meta Keywords
            </label>
            <div class="col-sm-10">
                <textarea
                    rows="8"
                    type="text"
                    class="form-control"
                    id="metaKeywords"
                    name="meta_keywords"
                >{{$row->meta_keywords}}</textarea>
            </div>
        </div>

        <div class="form-group bg-default">
            <label
                for="faqQuestion"
                class="col-sm-2 control-label"
            >
                FAQ Question
            </label>
            <div class="field_wrapper_question col-sm-10">
                @if (!empty($row->faq_question))
                    @foreach ($row->faq_question as $key => $question)
                        <div style="margin-bottom: 5px">
                            <input
                                type="text"
                                name="faq_question[]"
                                style="width:90em"
                                value="{{$question}}"
                            >

                            @if ($key === 0)
                                <a
                                    href="javascript:void(0);"
                                    class="add_button"
                                    title="Add field"
                                >
                                    <i class="fa fa-plus-circle"></i>
                                </a>
                            @else
                                <a
                                    href="javascript:void(0);"
                                    class="remove_button"
                                >
                                    <i class="fa fa-minus-circle"></i>
                                </a>
                            @endif
                        </div>
                    @endforeach
                @else
                    <div style="margin-bottom: 5px">
                        <input
                            type="text"
                            name="faq_question[]"
                            style="width:90em"
                            value=""
                        >
                        <a
                            href="javascript:void(0);"
                            class="add_button"
                            title="Add field"
                        >
                            <i class="fa fa-plus-circle"></i>
                        </a>
                    </div>
                @endif
            </div>
        </div>

        <div class="form-group bg-default">
            <label
                for="faqAnswer"
                class="col-sm-2 control-label"
            >
                FAQ Answer
            </label>
            <div class="field_wrapper_answer col-sm-10">
                @if (!empty($row->faq_answer))
                    @foreach ($row->faq_answer as $key => $answer)
                        <div style="margin-bottom: 5px">
                            <input 
                                type="text"
                                name="faq_answer[]"
                                style="width:90em"
                                value="{{$answer}}"
                            >
                        </div>
                    @endforeach
                @else
                    <div style="margin-bottom: 5px">
                        <input 
                            type="text"
                            name="faq_answer[]"
                            style="width:90em"
                            value=""
                        >
                    </div>
                @endif
            </div>
        </div>

        <div class="form-group bg-default">
            <label
                for="imageUrl"
                class="col-sm-2 control-label"
            >
                Image Url (OG Image)
            </label>
            <div class="col-sm-10">
                <input 
                    type="text"
                    class="form-control"
                    placeholder="For og:image"
                    id="image"
                    name="image_url"
                    value="{{$row->image_url}}"
                >
            </div>
        </div>

        <div class="form-group bg-default" id="inputHeaderWeb">
            <label
                for="imageUrl"
                class="col-sm-2 control-label"
            >
                Header Image Desktop
            </label>
            <div class="media col-sm-10">
                <a
                    class="pull-left thumbnail"
                    id="wrapper-img"
                >
                    <img
                        style="background: #EEE; cursor: pointer;"
                        class="media-object img-responsive img-thumbnail"
                        width="250px"
                        src="{{ $row->desktop_header_photo }}"
                        data-src="holder.js/250x140/thumbnail" alt=""
                    >
                </a>
                <div class="media-body">
                    <input id="fileupload" type="file" name="media" class="hidden">
                    <input id="photo_id" type="hidden" name="desktop_header_media_id" value="{{ $row->desktop_header_media_id }}">
                    <div id="progress" class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <div class="col-sm-6">
                        <div id="original-file-info" class="row"></div>
                    </div>
                    <div class="col-sm-6">
                        <div id="uploaded-file-info" class="row"></div>
                    </div>
                    <br>
                </div>
            </div>
        </div>

        <div class="form-group bg-default" id="inputHeaderMweb">
            <label
                for="imageUrl"
                class="col-sm-2 control-label"
            >
                Header Image Mobile 
            </label>
            <div class="media col-sm-10">
                <a
                    class="pull-left thumbnail"
                    id="wrapper-img"
                >
                    <img
                        style="background: #EEE; cursor: pointer;"
                        class="media-object img-responsive img-thumbnail"
                        width="250px"
                        src="{{ $row->mobile_header_photo }}"
                        data-src="holder.js/250x140/thumbnail" alt=""
                    >
                </a>
                <div class="media-body">
                    <input id="fileupload" type="file" name="media" class="hidden">
                    <input id="photo_id" type="hidden" name="mobile_header_media_id" value="{{ $row->mobile_header_media_id }}">
                    <div id="progress" class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <div class="col-sm-6">
                        <div id="original-file-info" class="row"></div>
                    </div>
                    <div class="col-sm-6">
                        <div id="uploaded-file-info" class="row"></div>
                    </div>
                    <br>
                </div>
            </div>
        </div>

        <div class="form-group bg-default">
            <label
                for="active-record"
                class="col-sm-2 control-label"
            >
                Active
            </label>
            <div class="col-sm-10">
                <input
                    type="checkbox"
                    name="is_active"
                    id="is_active"
                    value=1
                    {{ $row->is_active === 1 ? 'checked="checked"' : '' }}
                >
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button
                    type="submit"
                    class="btn btn-primary"
                >
                    Save
                </button>
                @if (Request::is('admin/*'))
                    <a href="{{ $formCancel }}">
                        <button type="button" class="btn btn-default">Cancel</button>
                    </a>
                @endif
            </div>
        </div>
    </div>

    {{ Form::close() }}
</div>
@stop

@section('style')
    {{ HTML::style('assets/vendor/jquery-chosen/chosen.css') }}
    {{ HTML::style('assets/vendor/jquery-chosen-custom/chosen.css') }}
    <link rel="stylesheet" href="/assets/vendor/summernote/summernote.css?v=05042018">

    <style>
        #chosenForm .chosen-choices {
            border: 1px solid #ccc;
            border-radius: 4px;
            min-height: 34px;
            padding: 6px 12px;
        }
        #chosenForm .form-control-feedback {
            /* To make the feedback icon visible */
            z-index: 100;
        }
    </style>
@stop

@section('script')
    {{ HTML::script('assets/vendor/jquery-chosen/chosen.jquery.js') }}
    <script src="/assets/vendor/summernote/summernote.min.js"></script>
    <script type="text/javascript">
        var config = {
            '.chosen-select': {
                width: '100%',
                max_selected_options: 3
            }
        }
        for (var selector in config) {
            $(selector).chosen(config[selector])
        }
        $(".chosen-select").bind("chosen:maxselected", function () {
            alert('Maximal type kost only 3, please press ESC button on your keyboard if this message can`t be hide.')
        })

        $(document).ready(function(){
            var maxField = 5 //Input fields increment limitation
            var addButton = $('.add_button') //Add button selector
            var wrapperQuestion = $('.field_wrapper_question') //Input field wrapper
            var wrapperAnswer = $('.field_wrapper_answer') //Input field wrapper
            var fieldHTMLQuestion = '<div style="margin-bottom: 5px">'+
                '<input type="text" name="faq_question[]" style="width:90em" value=""/>'+
                '<a href="javascript:void(0);" class="remove_button">'+
                    ' <i class="fa fa-minus-circle"></i>'+
                '</a>'+
                '</div>'
            var fieldHTMLAnswer = '<div style="margin-bottom: 5px">'+
                '<input type="text" name="faq_answer[]" style="width:90em" value=""/>'+
                '</div>'
            var x = 1 //Initial field counter is 1
            
            //Once add button is clicked
            $(addButton).click(function(){
                //Check maximum number of input fields
                if (
                    (x < maxField)
                    && ( $('.remove_button').length < (maxField-1) )
                ) {
                    x++; //Increment field counter
                    $(wrapperQuestion).append(fieldHTMLQuestion) //Add field html
                    $(wrapperAnswer).append(fieldHTMLAnswer) //Add field html
                }
            });
            
            //Once remove button is clicked
            $(wrapperQuestion).on('click', '.remove_button', function(e){
                e.preventDefault()
                var index = $(this).parent('div').index()
                $(this).parent('div').remove() //Remove field html
                $(wrapperAnswer).children().eq(index).remove()
                x-- //Decrement field counter
            })

            //Convert slug
            $('#slug').focusout(function(){
                var text = $(this).val()
                $(this).val(slugify(text))
            })
        })

        function slugify(string) {
            const a = 'àáâäæãåāăąçćčđďèéêëēėęěğǵḧîïíīįìłḿñńǹňôöòóœøōõőṕŕřßśšşșťțûüùúūǘůűųẃẍÿýžźż·/_,:;'
            const b = 'aaaaaaaaaacccddeeeeeeeegghiiiiiilmnnnnoooooooooprrsssssttuuuuuuuuuwxyyzzz------'
            const p = new RegExp(a.split('').join('|'), 'g')

            return string.toString().toLowerCase()
                .replace(/\s+/g, '-') // Replace spaces with -
                .replace(p, c => b.charAt(a.indexOf(c))) // Replace special characters
                .replace(/&/g, '-and-') // Replace & with 'and'
                .replace(/[^\w\-]+/g, '') // Remove all non-word characters
                .replace(/\-\-+/g, '-') // Replace multiple - with single -
                .replace(/^-+/, '') // Trim - from start of text
                .replace(/-+$/, '') // Trim - from end of text
        }
    </script>

    <!-- Holder for Upload File Thumbnail -->
    {{ HTML::script('assets/vendor/holder/holder.js') }}
    <!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
    {{ HTML::script('assets/vendor/jquery-file-upload/js/vendor/jquery.ui.widget.js') }}
    <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
    {{ HTML::script('assets/vendor/jquery-file-upload/js/jquery.iframe-transport.js') }}
    <!-- The basic File Upload plugin -->
    {{ HTML::script('assets/vendor/jquery-file-upload/js/jquery.fileupload.js') }}
    <!-- The File Upload processing plugin -->
    {{ HTML::script('assets/vendor/jquery-file-upload/js/jquery.fileupload-process.js') }}
    <!-- The File Upload validation plugin -->
    {{ HTML::script('assets/vendor/jquery-file-upload/js/jquery.fileupload-validate.js') }}
    <!-- File Upload Main -->
    {{ HTML::script('assets/vendor/file-upload/reguler.js') }}
    <script>
        $(function () {
        // Change this to the location of your server-side upload handler
        @if (Request::is('admin/*'))
            var url = "{{ url('admin/media') }}"
        @else
            var url = "{{ url('user/media') }}"
        @endif

        myFileUpload($('#inputHeaderWeb'), url, 'sanjunipero_header_desktop', '', false)
        myFileUpload($('#inputHeaderMweb'), url, 'sanjunipero_header_mobile', '', false)
    });
    </script>
@stop