@extends('admin.layouts.main')

@section('style')
    <style>
        tr.active-sanjunipero-parent td {
            background-color: #C8E6C9 !important;
        }
    </style>
@endsection

@section('content')
    <?php App::setLocale('id'); ?>
    <!-- table -->
    <div class="box box-primary">
        <!-- box-header -->
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>
        <!-- box-header -->

        <!-- box-body -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    @if(Auth::user()->role == 'administrator')
                        <a href="{{ URL::route('admin.sanjunipero.parent.create') }}">
                            <button type="button" name="button" class="btn-add btn btn-primary btn-sm">
                                <i class="fa fa-plus">&nbsp;</i> Add New Parent
                            </button>
                        </a>
                    @endif
                </div>
            </div>

            <table id="tableListLanding" class="table table-striped">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Type Page</th>
                        <th>Slug</th>
                        <th>Title Header</th>
                        <th>Last Update</th>
                        <th class="table-action-column" width="120px">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($rows as $key => $row)
                        <tr class="{{ $row->is_active === 1 ? 'active-sanjunipero-parent' : '' }}">
                            <td>{{ ($key+1) }}</td>
                            <td>{{ ucwords(str_replace(',', ', ', $row->type_kost)) }}</td>
                            <td>{{ $row->slug }}</td>
                            <td>{{ $row->title_header }}</td>
                            <td>{{ $row->updated_at }}</td>
                            <td class="table-action-column">
                                <div class="btn-action-group">
                                    @permission('access-landing-page')
                                    <a href="{{ route("admin.sanjunipero.parent.edit", $row->id) }}" title="Edit Parent">
                                        <i class="fa fa-edit"></i>
                                    </a>

                                    @if ($row->is_active === 0)
                                        <a class="activateParentSanjunipero" data-url="{{ route("admin.sanjunipero.parent.activate", $row->id) }}" title="Activate">
                                            <i class="fa fa-check-circle-o"></i>
                                        </a>
                                    @else
                                        <a class="deactivateParentSanjunipero" data-url="{{ route("admin.sanjunipero.parent.deactivate", $row->id) }}" title="Deactivate">
                                            <i class="fa fa-circle-o"></i>
                                        </a>
                                    @endif

                                    <a href="{{ url("/kos/{$row->url_landing}")}}" title="Open {{$row->title_header}} page">
                                        <i class="fa fa-external-link"></i>
                                    </a>
                                    @endpermission
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>No</th>
                        <th>Type Page</th>
                        <th>Slug</th>
                        <th>Title Header</th>
                        <th>Last Update</th>
                        <th class="table-action-column" width="120px">Action</th>
                    </tr>
                </tfoot>
            </table>
            <div class="box-body no-padding">
                {{ $rows->links() }}
            </div>
        </div>
        <!-- box-body -->

    </div>
@stop

@section('script')
    <script type="text/javascript">
        $(document).ready(function(){
            $('.activateParentSanjunipero').click(function(){
                $.post( $(this).data('url'), function( response ) {
                    afterSuccessIsActive(response, this)
                });
            })

            $('.deactivateParentSanjunipero').click(function(){
                $.post( $(this).data('url'), function( response ) {
                    afterSuccessIsActive(response, this)
                });
            })

            function afterSuccessIsActive(response, element) {
                alert(response.message)
                location.reload(true)
            }
        })
    </script>
@stop