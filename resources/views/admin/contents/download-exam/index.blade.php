@extends('admin.layouts.main')

@section('content')
	<!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <a href="{{ URL::route('admin.download-exam.create') }}">
                        <button type="submit" class="btn-add btn btn-primary btn-sm">
                            <i class="fa fa-plus">&nbsp;</i>Tambah Form Download Soal</button>
                    </a>
                    <form action="" class="form-inline" method="get" style="text-align:right;padding:10px;">
                        <input type="text" name="q" class="form-control input-sm"  placeholder="Title / Slug"  autocomplete="off" value="{{ request()->input('q') }}">
                        <button class="btn btn-primary btn-md" id="buttonSearch"><i class="fa fa-search">&nbsp;</i>Search</button>
                    </form>
                </div>
            </div>

            <table  class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Title</th>
                        <th>Slug</th>
                        <th>Tipe</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                 @foreach($exams as $exam)
                     <tr @if ($exam->redirect_id > 0 || !is_null($exam->redirect_id)) style="color: #ff0000;" @endif>
                         <td>{{ $exam->id }}</td>
                         <td>{{ $exam->title }}</td>
                         <td>{{ $exam->slug }}</td>
                         <td>{{ $exam->form_type }}</td>
                         <td>
                            <a href="/admin/download-exam/redirect/{{ $exam->id }}"><i class="fa fa-external-link"></i></a>
                            <a href="{{route('admin.download-exam.edit', $exam->id)}}" title="edit">
                                <i class="fa fa-pencil"></i>
                            </a>

                            <a href="{{route('admin.download-exam.delete', $exam->id)}}" title="delete">
                                <i class="fa fa-trash-o"></i>
                            </a>
                         </td>
                     </tr> 
                 @endforeach
                </tbody>
                
            </table>
        </div><!-- /.box-body -->

        <div class="box-body no-padding">
            {{ $exams->appends(Request::except('page'))->links() }}
        </div>
    </div><!-- /.box -->
    <!-- table -->
@endsection