@extends('admin.layouts.main')

@section('content')
<div class="box box-primary" style="padding: 10px;">
    <div class="box-header">
        <h3 class="box-title">{{ $boxTitle }}</h3>
    </div>
	<form action="" method="post">
	    <div class="form-group">
	        <label for="destination_id">ID Landing Tujuan</label>
            <input type="number" name="destination_id" class="form-control" value="{{ $download->redirect_id }}" />
	    </div>
	    <button type="submit" class="btn btn-default">Submit</button>
	</form>
</div>
@endsection
