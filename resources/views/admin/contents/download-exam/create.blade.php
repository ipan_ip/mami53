@extends('admin.layouts.main')

@section('style')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/dropzone.css">
    <link rel="stylesheet" href="/assets/vendor/summernote/summernote.css?v=05042018">

    <style>
        .note-group-select-from-files {
            display: none;
        }
    </style>
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

        <form action="{{ URL::route('admin.download-exam.store') }}" method="POST" class="form-horizontal form-bordered">
            <div class="box-body no-padding">
                <div class="form-group bg-default">
                    <label for="title" class="control-label col-sm-2">Judul</label>
                    <div class="col-sm-10">
                        <input type="text" name="title" id="title" value="{{ old('title') }}" class="form-control" max="190">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="slug" class="control-label col-sm-2">Slug</label>
                    <div class="col-sm-10">
                        <input type="text" name="slug" id="slug" class="form-control" value="{{ old('slug') }}" max="190">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="subtitle" class="control-label col-sm-2">Subtitle</label>
                    <div class="col-sm-10">
                        <input type="text" name="subtitle" id="subtitle" class="form-control" value="{{ old('subtitle') }}" max="190">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="excerpt" class="control-label col-sm-2">Ringkasan Konten</label>
                    <div class="col-sm-10">
                        <textarea name="excerpt" id="excerpt" class="form-control" rows="10">{{ old('excerpt') }}</textarea>
                        <p class="helper-block">File gambar diusahakan di bawah 500KB</p>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="content" class="control-label col-sm-2">Konten</label>
                    <div class="col-sm-10">
                        <textarea name="content" id="content" class="form-control" rows="10">{{ old('content') }}</textarea>
                        <p class="helper-block">File gambar diusahakan di bawah 500KB</p>
                    </div>
                </div>
                
                <div class="form-group bg-default">
                    <label for="form-type" class="control-label col-sm-2">Tipe Form</label>
                    <div class="col-sm-10">
                        <select name="form_type" id="form-type" class="form-control">
                            @foreach($formTypes as $type) 
                                <option value="{{ $type }}" {{ old('form_type') == $type ? 'selected="selected"' : '' }}>{{$type}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="parent-id" class="control-label col-sm-2">Parent ID</label>
                    <div class="col-sm-10">
                        <input type="text" name="parent_id" id="parent-id" class="form-control" value="{{ old('parent_id') }}" max="5">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label class="control-label col-sm-2">File untuk di-download</label>
                    <div class="col-sm-10">
                        <div id="downloadExamFiles"
                              action="{{ URL::route('admin.download-exam.file.upload') }}"
                              method="POST"
                              class="dropzone">
                            {{ csrf_field() }}
                        </div>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <div class="col-sm-10 col-sm-push-2">
                        <div id="files-wrapper"></div>
                    </div>
                </div>
                
                <div class="form-group bg-default">
                    <div class="col-sm-10 col-sm-push-2">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/dropzone.js"></script>
<script src="/assets/vendor/summernote/summernote.min.js"></script>
<script src="/assets/vendor/summernote/plugin/summernote-image-attributes.js"></script>

<script>
    function slugify(text)
    {
      return text.toString().toLowerCase()
        .replace(/\s+/g, '-')           // Replace spaces with -
        .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
        .replace(/\-\-+/g, '-')         // Replace multiple - with single -
        .replace(/^-+/, '')             // Trim - from start of text
        .replace(/-+$/, '');            // Trim - from end of text
    }

    $(document).ready(function() {
        slug_state = 'unchanged';

        $('#title').on('keyup', function(e) {
            if(slug_state == 'unchanged') {
                slug = slugify($(this).val());

                $('#slug').val(slug);
            }
        });

        $('#slug').on('keyup', function(e) {
            slug_state = 'changed';
        });


        $('#excerpt, #content').summernote({
            height: 300,
            toolbar: [
                ['style', ['style', 'bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['insert', ['picture', 'link', 'video', 'table', 'hr']],
                ['misc', ['codeview']]
            ],
            popover: {
                image: [
                    ['custom', ['imageAttributes']],
                    ['remove', ['removeMedia']]
                ],
            },
            imageAttributes:{
                icon:'<i class="note-icon-pencil"/>',
                removeEmpty:false, // true = remove attributes | false = leave empty if present
                disableUpload: true // true = don't display Upload Options | Display Upload Options
            }
        });

        Dropzone.autoDiscover = false;

        Dropzone.options.downloadExamFiles = {
            paramName : "file",
            maxFilesize : 8,
            accepFiles: '.jpg, .jpeg, .png, .bmp, .zip, .pdf, .doc, .docx, .xls, .xlsx, .ppt, .pptx, .csv',
            dictDefaultMessage : 'Upload File',
            addRemoveLinks: true,
            success : function (file, response) {

                file_item = '<div class="file-item" data-filename="' + file.name + '">' +
                                '<input type="hidden" name="files[]" value="' +response.uploadedFile.id+ '" data-filename="' + file.name + '">' +
                                '<input type="text" class="form-control" name="file_names[]" value="' + response.uploadedFile.file_name + '">' +
                            '</div>'
                // $('#files-wrapper').append('<input type="hidden" name="files[]" value="' +response.uploadedFile.id+ '" data-filename="' + file.name + '">');
                $('#files-wrapper').append(file_item);


            },
            init: function() {
                this.on('removedfile', function(file) {
                    // $('input[name="files[]"]').each(function(id, el) {
                    //  if($(el).data('filename') == file.name) {
                    //      $(el).remove();
                    //  }
                    // });
                    $('.file-item').each(function(id, el) {
                        if($(el).data('filename') == file.name) {
                            $(el).remove();
                        }
                    });
                });
            }
        }

        $('#downloadExamFiles').dropzone();
    });

    
</script>
@endsection
