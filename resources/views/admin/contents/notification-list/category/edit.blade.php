@extends('admin.layouts.main')

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

        <form action="{{ URL::to('admin/notification-list/category/' . $category->id) }}" method="POST" class="form-horizontal form-bordered">
            <input type="hidden" name="_method" value="PUT">
            
            <div class="box-body no-padding">
                <div class="form-group bg-default">
                    <label for="category-name" class="control-label col-sm-2">Category Name</label>
                    <div class="col-sm-10">
                        <input type="text" name="name" id="category-name" class="form-control" value="{{ old('name', $category->name) }}">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="category-icon" class="control-label col-sm-2">Category Icon</label>
                    <div class="col-sm-10">
                        <input type="text" name="icon" id="category-icon" class="form-control" value="{{ old('icon', $category->icon) }}">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <div class="col-sm-10 col-sm-push-2">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
