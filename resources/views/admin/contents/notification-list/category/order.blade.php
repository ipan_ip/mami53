@extends('admin.layouts.main')

@section('content')
	<div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

        <form action="{{ URL::to('admin/notification-list/category/order') }}" method="POST">
            {{ Form::hidden('actions', '', ['id' => 'hdn-actions']) }}
            <div class="box-body no-padding">
                <div class="horizontal-wrapper">
                    <div class="btn-horizontal-group bg-default form-inline">
                        <div class="col-md-12 bg-default" style="padding-bottom:10px;">
                            <button type="submit" class="btn btn-primary">Save Button</button>
                        </div>
                    </div>
                </div>

                <table  class="table table-striped">
                    <thead>
                        <tr>
                            <th>Category Name</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        $count = count($categories);
                        $i = 1;
                    ?>
                    @foreach($categories as $category)
                        <tr>
                            <td>
                                <span id="name-{{ $category->id }}">{{ $category->name }}</span>
                                <span id="order-{{ $category->id }}" style="display: none">{{ $category->order }}</span>
                            </td>
                            <td>
                                @if($i > 1)
                                <a href="#" onclick="moveUp(this);" title="Move Up">
                                    <i class="fa fa-arrow-up"></i>
                                </a>
                                @endif
                                @if($i < $count)
                                <a href="#" onclick="moveDown(this);" title="Move Down">
                                    <i class="fa fa-arrow-down"></i>
                                </a>
                                @endif
                            </td>
                        </tr>
                        <?php $i++; ?>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </form>
    </div>

    <script type="text/javascript">
        function moveUp(el)
        {
            var curId = $(el).parent().parent().find('td span').first().attr('id').replace('name-', '');
            var targetId = $(el).parent().parent().prev().find('td span').first().attr('id').replace('name-', '');
            var curOrder = $(el).parent().parent().find('td span').last().text();
            var targetOrder = $(el).parent().parent().prev().find('td span').last().text();
            var orderGap = targetOrder - curOrder;

            var curEl = $('#name-' + curId);
            var curClone = curEl.clone();
            var targetEl = $('#name-' + targetId);
            var targetClone = targetEl.clone();
            curEl.replaceWith(targetClone);
            targetEl.replaceWith(curClone);

            var hdnInput = $('#hdn-actions');
            if (hdnInput.val() !== '') hdnInput.val(hdnInput.val() + '|');
            hdnInput.val(hdnInput.val() + curId + ',' + targetId + ',' + orderGap);
        }
        function moveDown(el)
        {
            var curId = $(el).parent().parent().find('td span').first().attr('id').replace('name-', '');
            var targetId = $(el).parent().parent().next().find('td span').first().attr('id').replace('name-', '');
            var curOrder = $(el).parent().parent().find('td span').last().text();
            var targetOrder = $(el).parent().parent().next().find('td span').last().text();
            var orderGap = targetOrder - curOrder;

            var curEl = $('#name-' + curId);
            var curClone = curEl.clone();
            var targetEl = $('#name-' + targetId);
            var targetClone = targetEl.clone();
            curEl.replaceWith(targetClone);
            targetEl.replaceWith(curClone);

            var hdnInput = $('#hdn-actions');
            if (hdnInput.val() !== '') hdnInput.val(hdnInput.val() + '|');
            hdnInput.val(hdnInput.val() + curId + ',' + targetId + ',' + orderGap);
        }
    </script>
@endsection
