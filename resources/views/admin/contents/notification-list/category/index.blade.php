@extends('admin.layouts.main')

@section('content')
	<div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default form-inline">
                    <div class="col-md-6 bg-default" style="padding-bottom:10px;">
                        <a href="{{ URL::to('admin/notification-list/category/create#notification-list') }}" class="btn btn-primary">
                            <i class="fa fa-plus">&nbsp;</i>Add Notification Category
                        </a>
                        <a href="{{ URL::to('admin/notification-list/category/order#notification-list') }}" class="btn btn-primary">
                            <i class="fa fa-refresh">&nbsp;</i>Re-order Notification Category
                        </a>
                    </div>
                    <div class="col-md-6 bg-default" style="padding-bottom:10px;text-align:right;">
                        <form action="" method="GET">
                            <input type="text" name="q" class="form-control"  placeholder="Category Name"  autocomplete="off" value="{{ request()->input('q') }}">
                            <button type="submit" class="btn btn-primary" id="buttonSearch">
                                <i class="fa fa-search">&nbsp;</i>Search
                            </button>
                        </form>
                    </div>
                </div>
            </div>

            <table  class="table table-striped">
                <thead>
                    <tr>
                        <th>Category Name</th>
                        <th>Category Icon</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($categories as $category)
                    <tr>
                        <td>{{ $category->name }}</td>
                        <td>
                            @if (!empty($category->icon))
                                <img src="{{ $category->icon }}" alt="{{ $category->name }}" />
                            @endif
                        </td>
                        <td>
                            <a href="{{ URL::to('admin/notification-list/category/' . $category->id . '/edit#notification-list') }}" title="Edit">
                                <i class="fa fa-pencil"></i>
                            </a>
                            <a href="{{ URL::to('admin/notification-list/category/' . $category->id . '/delete') }}" onclick="if(!confirm('Are you sure want to delete category {{ $category->name }}?')) return false;" title="Delete">
                                <i class="fa fa-trash"></i>
                            </a>
                        </td>
                    </tr> 
                @endforeach
                </tbody>
            </table>
        </div>

        <div class="box-body no-padding">
            <div class="col-md-12 bg-default">
                {{ $categories->appends(Request::except('page'))->links() }}
            </div>
        </div>
    </div>

    <script>
        
    </script>
@endsection
