@extends('admin.layouts.main')

@section('style')
<style>
    .discounted {
        color: #bbb;
        font-weight: bold;
        text-decoration: line-through;
    }
</style>
@endsection

@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
            <div class="box-tools pull-right">
                <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <form action="" class="form-inline" method="get" style="text-align:right;padding:10px;">
                        <select name="status" class="form-control input-sm">
                            <option value="">Semua</option>
                            @foreach($statusOptions as $key => $status)
                                <option value="{{ $key }}" {{ request()->input('status') == $key ? 'selected="selected"' : '' }}>{{ $status }}</option>
                            @endforeach
                        </select>
                        <input type="text" name="q" class="form-control input-sm"  placeholder="Nama Produk"  autocomplete="off" value="{{ request()->input('q') }}">
                        <button class="btn btn-primary btn-md" id="buttonSearch"><i class="fa fa-search">&nbsp;</i>Search</button>
                    </form>
                </div>
            </div>

            <table  class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nama Produk</th>
                        <th>Harga</th>
                        <th>Satuan</th>
                        <th>Tanggal Buat</th>
                        <th>Status</th>
                        <th>Aktif ?</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($marketplaceProducts as $key => $product)
                    <tr>
                        <td>{{ $product->id }}</td>
                        <td>@if ($product->availability == "sold_out") <label class="label label-success">Sold out</label> @endif {{ $product->title }}</td>
                        <td>
                            @if(!is_null($product->price_sale))
                                <span class="discounted">Rp. {{ number_format($product->price_regular, 0, ',', '.') }}</span><br>
                                <strong>Rp. {{ number_format($product->price_sale, 0, ',', '.') }}</strong>
                            @else
                                <strong>Rp. {{ number_format($product->price_regular, 0, ',', '.') }}</strong>
                            @endif
                        </td>
                        <td>{{ $product->price_unit }}</td>
                        <td>{{ $product->created_at->format('Y-m-d H:i:s') }}</td>
                        <td>
                            @if($product->status == $product::STATUS_NEW)
                                <span class="label label-success">Baru</span>
                            @elseif($product->status == $product::STATUS_WAITING)
                                <span class="label label-warning">Menunggu</span>
                            @elseif($product->status == $product::STATUS_VERIFIED)
                                <span class="label label-primary">Terverifikasi</span>
                            @elseif($product->status == $product::STATUS_INACTIVE)
                                <span class="label label-default">Nonaktif</span>
                            @elseif($product->status == $product::STATUS_REJECTED)
                                <span class="label label-danger">Ditolak</span>
                            @endif
                        </td>
                        <td>
                            @if($product->is_active == 1) 
                                <span class="label label-primary">Yes</span> 
                            @else 
                                <span class="label label-danger">No</span>
                            @endif
                        </td>
                        <td>
                            <a href="{{ URL::route('admin.marketplace.product.edit', $product->id) }}" title="Edit">
                                <i class="fa fa-pencil"></i>
                            </a>

                            <a href="{{ URL::route('admin.marketplace.product.delete', $product->id) }}" title="Hapus">
                                <i class="fa fa-trash-o"></i>
                            </a>
                        </td>
                    </tr> 
                @endforeach
                </tbody>
                
            </table>
        </div><!-- /.box-body -->
        <div class="box-body no-padding">
            {{ $marketplaceProducts->appends(Request::except('page'))->links() }}
        </div>
    </div><!-- /.box -->
    <!-- table -->
@endsection