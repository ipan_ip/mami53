@extends('admin.layouts.main')

@section('style')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/dropzone.css">
<link rel="stylesheet" href="{{ mix_url('dist/vendor/leaflet/leaflet.css') }}">

@endsection

@section('content')
	<div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

		<form action="{{ URL::route('admin.marketplace.product.update', $marketplaceProduct->id) }}" method="POST" class="form-horizontal form-bordered">
			<div class="box-body no-padding">
				<div class="form-group bg-default">
					<label for="title" class="control-label col-sm-2">Judul</label>
					<div class="col-sm-10">
						<input type="text" name="title" id="title" class="form-control" value="{{ old('title', $marketplaceProduct->title) }}">
					</div>
				</div>

				<div class="form-group bg-default">
					<label for="description" class="control-label col-sm-2">Deskripsi</label>
					<div class="col-sm-10">
						<textarea name="description" id="description" class="form-control" rows="5">{{ old('description', $marketplaceProduct->description) }}</textarea>
					</div>
				</div>

				<div class="form-group bg-default">
					<label for="contact-phone" class="control-label col-sm-2">Nomor Kontak</label>
					<div class="col-sm-10">
						<input type="text" name="contact_phone" id="contact-phone" class="form-control" value="{{ old('contact_phone', $marketplaceProduct->contact_phone)  }}">
					</div>
				</div>

				<div class="form-group bg-default">
					<label for="price-regular" class="control-label col-sm-2">Harga</label>
					<div class="col-sm-10">
						<input type="number" name="price_regular" id="price-regular" class="form-control" value="{{ old('price_regular', $marketplaceProduct->price_regular) }}">
					</div>
				</div>

				<div class="form-group bg-default">
					<label for="price-sale" class="control-label col-sm-2">Harga Diskon</label>
					<div class="col-sm-10">
						<input type="number" name="price_sale" id="price-sale" class="form-control" value="{{ old('price_sale', $marketplaceProduct->price_sale) }}">
					</div>
				</div>

				<div class="form-group bg-default">
					<label for="price-unit" class="control-label col-sm-2">Satuan Harga</label>
					<div class="col-sm-10">
						<input type="text" name="price_unit" id="price-unit" class="form-control" value="{{ old('price_unit', $marketplaceProduct->price_unit) }}">
					</div>
				</div>

				<div class="form-group bg-default">
	                <label for="inputGeoName" class="col-sm-2 control-label">Koordinat</label>
	                <div class="col-sm-10">
	                    <input type="text" class="form-control" placeholder="Geo Name"
	                           id="inputGeoName" name="geo_name">
	                </div>
	                <div class="col-sm-10 col-sm-offset-2">
	                    <div style="background: #EEE; width: 100%; height: 300px;">
	                        <div id="map-canvas"></div>
	                    </div>
	                </div>
	                <div class="col-sm-10 col-sm-offset-2">
	                	<div class="input-group">
	                		<input type="text" class="form-control" placeholder="latitude"
                               id="latitude" name="latitude" value="{{ old('latitude', $marketplaceProduct->latitude) }}">
                        	<input type="text" class="form-control" placeholder="longitude"
                               id="longitude" name="longitude" value="{{ old('longitude', $marketplaceProduct->longitude) }}">
                       </div>
	                </div>
	            </div>

	            <div class="form-group bg-default">
					<label for="address" class="control-label col-sm-2">Alamat</label>
					<div class="col-sm-10">
						<input type="text" name="address" id="address" class="form-control" value="{{ old('address', $marketplaceProduct->address) }}">
					</div>
				</div>

	            <div class="form-group bg-default">
					<label for="coverphoto" class="control-label col-sm-2">Foto</label>
					<div class="col-sm-10">
						<div id="coverphoto"
						      action="{{ url('admin/media') }}"
						      method="POST"
						      class="dropzone"
						      uploadMultiple="no">
						    {{ csrf_field() }}
						    <input type="hidden" name="media_type" value="style_photo">
						</div>
						<p class="helper-block">Meng-upload cover photo akan otomatis mengganti cover photo yg sudah ada</p>
					</div>
				</div>

				<div id="photo-wrapper">
					@if(!is_null(request()->photo) || !is_null($photoCover))
						<input type="hidden" name="photo_cover" value="{{ old('photo_cover', $photoCover->id) }}">
					@endif
				</div>

				<div class="form-group bg-default">
					<div class="col-sm-10 col-sm-push-2">
						@if(!is_null($photoCover))
							<img src="{{ $photoCover->getMediaUrl()['small'] }}" class="photo-preview" alt="photo-preview">
						@endif
					</div>
				</div>

				<div class="form-group bg-default">
					<label for="otherphoto" class="control-label col-sm-2">Foto</label>
					<div class="col-sm-10">
						<div id="otherphoto"
						      action="{{ url('admin/media') }}"
						      method="POST"
						      class="dropzone"
						      uploadMultiple="no">
						    {{ csrf_field() }}
						    <input type="hidden" name="media_type" value="style_photo">
						</div>
					</div>
				</div>

				<div id="photo-other-wrapper">
					@if(is_null(request()->photo_other))
						@foreach($photoOthers as $photoOther)
							<input type="hidden" name="photo_other[]" value="{{ $photoOther->id }}">
						@endforeach
					@else 
						@foreach(request()->photo_other as $photoOther)
							<input type="hidden" name="photo_other[]" value="{{ $photoOther }}">
						@endforeach
					@endif
				</div>

				<div class="form-group bg-default">
					<div class="col-sm-10 col-sm-push-2">
						@if(is_null(request()->photo_other))
							@foreach($photoOthers as $photoOther)
								<img src="{{ $photoOther->getMediaUrl()['small'] }}" class="photo-preview-other" alt="photo-preview" data-id="{{$photoOther->id}}">
								<a href="#" class="remove-preview" data-id="{{$photoOther->id}}">Remove</a>
							@endforeach
						@endif
					</div>
				</div>

				<div class="form-group bg-default">
					<label for="status" class="control-label col-sm-2">Status</label>
					<div class="col-sm-10">
						<select name="status" class="form-control" id="status">
							<option value="" {{ in_array(old('status', $marketplaceProduct->status), ['new', 'waiting']) ? 'selected="selected"' : (is_null(old('status', $marketplaceProduct->status)) && $marketplaceProduct->is_active == 0 ? 'selected="selected"' : '') }}>Menunggu</option>
							@foreach($statusOptions as $key => $status)
								<option value="{{$key}}" {{ old('status', $marketplaceProduct->status) == $key ? 'selected="selected"' : (is_null(old('status', $marketplaceProduct->status)) && $marketplaceProduct->is_active == 1 && $key == 'verified' ? 'selected="selected"' : '') }}>{{ $status }}</option>
							@endforeach
						</select>
					</div>
				</div>

				<div class="form-group bg-default">
					<div class="col-sm-10 col-sm-push-2">
						<button type="submit" class="btn btn-primary">Save</button>
					</div>
				</div>
			</div>
		</form>
    </div>
@endsection

@section('script')
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&language=id&key={{ENV('MIX_GOOGLE_MAPS_JAVASCRIPT_API_KEY')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/dropzone.js"></script>
<script src="{{ mix_url('dist/vendor/leaflet/leaflet.js') }}"></script>

<script>
	centerPos = {
        lat: parseFloat('{{ $marketplaceProduct->latitude }}'), 
        lng: parseFloat('{{$marketplaceProduct->longitude}}') 
    };

	var map = L.map('map-canvas', {
        // Set latitude and longitude of the map center (required)
        center: centerPos, 
        // Set the initial zoom level, values 0-18, where 0 is most zoomed-out (required)
        zoom: 12
    });

    var tiles = new L.tileLayer("{{ config('services.osm.host') }}/hot/{z}/{x}/{y}.png").addTo(map);

    var marker = new L.Marker(centerPos, {
        draggable: true
    }).addTo(map);

    var autocomplete = new google.maps.places.Autocomplete(document.getElementById('inputGeoName'));
    autocomplete.setFields(['address_components', 'geometry']);

    var geocoder = new google.maps.Geocoder();

    marker.on('dragend', function(evt) {
		var latlng = evt.target.getLatLng();

        var lat = latlng.lat;
        var lng = latlng.lng;

		geocoder.geocode({'latLng': latlng}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				if (results[1]) {

					map.setView(latlng);

					$('#inputGeoName').val(results[1].formatted_address);
					$('#latitude').val(lat);
					$('#longitude').val(lng);
				} else {
					alert('No results found');
				}
			} else {
				alert('Geocoder failed due to: ' + status);
			}
		});
	});


	autocomplete.addListener('place_changed', function() {
		marker.setOpacity(0);
		var place = autocomplete.getPlace();
		if (!place.geometry) {
			// User entered the name of a Place that was not suggested and
			// pressed the Enter key, or the Place Details request failed.
			window.alert("No details available for input: '" + place.name + "'");
			return;
		}

		latlng = {
            lat: place.geometry.location.lat(),
            lng: place.geometry.location.lng()
        };

        map.setView(latlng);
        marker.setLatLng(latlng);
        marker.setOpacity(1);

		$('#latitude').val(place.geometry.location.lat);
		$('#longitude').val(place.geometry.location.lng);
	});
</script>

<script>
	$(document).ready(function() {
		// centerPos = {lat: {{ $marketplaceProduct->latitude }}, lng: {{$marketplaceProduct->longitude}} } 

		// var map = new google.maps.Map(document.getElementById("map-canvas"),
  // 			{
  // 				center: centerPos,
  // 				zoom:12
  // 			}
  // 		);

  // 		var marker = new google.maps.Marker({
		// 	position: centerPos,
		// 	map: map,
		// 	draggable: true,

  //       });

  //       var autocomplete = new google.maps.places.Autocomplete(document.getElementById('inputGeoName'));
  //       autocomplete.bindTo('bounds', map);

  //       var geocoder = new google.maps.Geocoder();

  //       google.maps.event.addListener(marker, 'dragend', function(evt) {
		// 	var lat = evt.latLng.lat();
		// 	var lng = evt.latLng.lng();

		// 	var latlng = new google.maps.LatLng(lat, lng);

		// 	geocoder.geocode({'latLng': latlng}, function(results, status) {
		// 		if (status == google.maps.GeocoderStatus.OK) {
		// 			if (results[1]) {

		// 				map.setCenter(latlng);
		// 				marker.setPosition(latlng);

		// 				$('#inputGeoName').val(results[1].formatted_address);
		// 				$('#latitude').val(lat);
		// 				$('#longitude').val(lng);
		// 			} else {
		// 				alert('No results found');
		// 			}
		// 		} else {
		// 			alert('Geocoder failed due to: ' + status);
		// 		}
		// 	});
		// });


		// autocomplete.addListener('place_changed', function() {
		// 	marker.setVisible(false);
		// 	var place = autocomplete.getPlace();
		// 	if (!place.geometry) {
		// 		// User entered the name of a Place that was not suggested and
		// 		// pressed the Enter key, or the Place Details request failed.
		// 		window.alert("No details available for input: '" + place.name + "'");
		// 		return;
		// 	}

		// 	map.setCenter(place.geometry.location);
		// 	marker.setPosition(place.geometry.location);
		// 	marker.setVisible(true);

		// 	$('#latitude').val(place.geometry.location.lat);
		// 	$('#longitude').val(place.geometry.location.lng);
		// });


		Dropzone.autoDiscover = false;
        Dropzone.options.coverphoto = {
        	paramName : "media",
            maxFilesize : 6,
            maxFiles: 1,
            accepFiles: '.jpg, .jpeg, .png, .bmp',
            dictDefaultMessage : 'Upload photo for cover',
            addRemoveLinks: true,
            success : function (file, response) {
            	if($('#photo-wrapper input[name=photo]').size() > 0) {
            		$('.photo-preview').remove();
            		$('#photo-wrapper input[name=photo]').val(response.media.id);
            	} else {
            		$('#photo-wrapper').append('<input type="hidden" name="photo_cover" value="' +response.media.id+ '">');
            	}
            },
		    init: function() {
		        this.on("removedfile", function(file) {
		            console.log('remove');
        			$('#photo-wrapper input[name=photo]').remove();
		        })
		    }
		};

		$('#coverphoto').dropzone();


		Dropzone.options.otherphoto = {
			paramName : "media",
            maxFilesize : 6,
            accepFiles: '.jpg, .jpeg, .png, .bmp',
            dictDefaultMessage : 'Upload other photos',
            addRemoveLinks: true,
            success : function (file, response) {
                $('#photo-other-wrapper').append('<input type="hidden" name="photo_other[]" value="' +response.media.id+ '" data-filename="' + file.name + '">');
            },
            init: function() {
		    	this.on('removedfile', function(file) {
		    		$('input[name="photo_other[]"]').each(function(id, el) {
		    			if($(el).data('filename') == file.name) {
		    				$(el).remove();
		    			}
		    		});
		    	});
		    }
		};

		$('#otherphoto').dropzone();

		$('.remove-preview').on('click',function(e) {
			e.preventDefault();

			thisid = $(this).data('id');

			$('.photo-preview-other').each(function(idx, el) {
				if($(el).data('id') == thisid) {
					$(el).remove();
				}
			});

			$('input[name="photo_other[]"]').each(function(idx, el) {
				if($(el).val() == thisid) {
					$(el).remove();
				}
			});

			$(this).remove();
		});
	});
</script>
@endsection