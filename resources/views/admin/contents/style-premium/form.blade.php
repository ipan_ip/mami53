@extends('admin.layouts.main')

@section('style')
<style>
		.table > tbody > tr > td
		{
			vertical-align: middle;
		}
		
		.font-large
		{
			font-size: 1.5em;
		}
		
		.font-semi-large
		{
			font-size: 1.25em;
		}
		
		.font-grey
		{
			color: #9E9E9E;
		}
		
		.dropdown-menu > li > a:hover
		{
			color: #333
		}
		
		.label-grey
		{
			background-color: #CFD8DC;
		}
		
		[hidden]
		{
			display: none !important;
		}
		
		/* Select2 tweak */
		
		.input-group .select2-container,
		.form-group .select2-container
		{
			position: relative !important;
			z-index: 2;
			float: left !important;
			width: 100% !important;
			margin-bottom: 0 !important;
			display: table !important;
			table-layout: fixed !important;
		}
		
		/* Sweetalert */
		
		.swal-shown
		{
			height: auto !important;
		}
		
		.custom-swal
		{
			z-index: 10000 !important;
		}
		
		.custom-wide-swal
		{
			width: 640px !important;
			font-size: 13px !important;
		}
	</style>
{{ HTML::style('assets/vendor/jquery-chosen/chosen.css') }}
{{ HTML::style('assets/vendor/jquery-chosen-custom/chosen.css') }}
{{ HTML::style('assets/vendor/jquery-file-upload/css/jquery.fileupload.css') }}
@stop
@section('content')
<div class="box box-primary">
  <div class="box-header">
    <h3 class="box-title">{!! $boxTitle !!}</h3>
  </div><!-- /.box-header -->
  {{ Form::open(array('url' => $formAction, 'method' => $formMethod,
      'class' => 'form-horizontal form-bordered', 'role' => 'form', 'id' => 'formInsertStyle')) }}
  <div class="box-body no-padding">
    <div id="cards">
      <div id="formCards">
        {{-- Selector --}}
        <div class="form-group bg-default">
          <label for="inputSelectType1" class="col-sm-2 control-label">Type</label>
          <div class="col-sm-2">
            <select class="form-control" id="inputSelectType1" name="cards[0][type]" tabindex="2"
              data-placeholder="Select Card Type">
              <option value="image" @if($rowStyle->type == 'image') selected @endif>Photo</option>
              <option value="quiz" @if($rowStyle->type == 'quiz') selected @endif>Quiz</option>
              <option value="text" @if($rowStyle->type == 'text') selected @endif>Text</option>
            </select>
          </div>
        </div>
        {{-- Photo input --}}
        <div class="form-group bg-default" id="cardInputImage1">
          <label for="fileupload" class="col-sm-2 control-label">Photo / Image</label>
          <div class="col-sm-10">
            <div id="cardUpload1" class="media">
              <a class="pull-left thumbnail" id="wrapper-img">
                <img style="background: #EEE; cursor: pointer;" class="media-object img-responsive img-thumbnail"
                  width="250px" src="{{ $rowStyle->photo }}" data-src="holder.js/360x240/thumbnail" alt="">
              </a>
              <div class="media-body">
                <input id="fileupload1" class="form-control" type="text" name="media1" placeholder="Photo URL" disabled>
                <input id="sourceImg" type="hidden" name="cards[0][url_ori]" value="{{ $rowStyle->url_ori }}">
                <br>
                <input id="fileupload" type="file" name="media" class="hidden">
                <input id="photo_id" type="hidden" name="cards[0][photo_id]" value="{{ $rowStyle->photo_id }}">
                <!-- The global progress bar -->
                <div id="progress" class="progress">
                  <div class="progress-bar"></div>
                </div>
                <div class="col-sm-6">
                  <div id="original-file-info" class="row"></div>
                </div>
                <div class="col-sm-6">
                  <div id="uploaded-file-info" class="row"></div>
                </div>
                <br>
                <!-- The container for the uploaded files -->
                <div id="files" class="files"></div>
              </div>
            </div>
          </div>
        </div>
        {{-- Quiz input --}}
        <div class="form-group bg-default" id="cardInputSubType1" @if($rowStyle->type != 'quiz') hidden @endif>
          <label for="inputSelectSubType1" class="col-sm-2 control-label">Sub Type</label>
          <div class="col-sm-2">
            <select class="form-control" id="inputSelectSubType1" name="cards[0][sub_type]" tabindex="2"
              data-placeholder="Select Card Sub Type">
              <option value="text" @if($rowStyle->type == 'text') selected @endif>Text</option>
              <option value="gif" @if($rowStyle->type == 'gif') selected @endif>GIF</option>
              <option value="image" @if($rowStyle->type == 'image') selected @endif>Image</option>
            </select>
          </div>
        </div>
        {{-- Text input --}}
        <div class="form-group bg-default" @if($rowStyle->type != 'text') hidden @endif>
          <label for="inputDescription" class="col-sm-2 control-label">Text</label>
          <div class="col-sm-10">
            <textarea class="form-control" rows="3" id="cardInputDescription" name="cards[0][description]"
              placeholder="Card Description">{{ $rowStyle->description }}</textarea>
          </div>
        </div>

        {{-- Additional inputs --}}
        <div class="form-group bg-default">
          <label for="inputDescription" class="col-sm-2 control-label">Description</label>
          <div class="col-sm-10">
            <textarea class="form-control" rows="3" id="cardInputDescription" name="cards[0][description]" placeholder="Card Description" {{ (isset($is_cover) && $is_cover) ? 'disabled="disabled"' : '' }}>{{ $rowStyle->description }}</textarea>
          </div>
        </div>
        <div class="form-group bg-default">
          <label for="cardInputSource" class="col-sm-2 control-label">Source</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" rows="3" id="cardInputSource" name="cards[0][source]"
              placeholder="Source Content" value="{{ $rowStyle->source }}"></input>
          </div>
        </div>
        <div class="form-group bg-default">
          <label for="cardInputOrdering" class="col-sm-2 control-label">Order</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" rows="3" id="cardInputOrdering" name="cards[0][ordering]"
              placeholder="kasih order (10, 20, 30, 40, 50 ....)" value="{{ $rowStyle->ordering }}"></input>
          </div>
        </div>

      </div>
    </div>

    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-primary">Submit</button>
        <a href="{{ $formCancel }}">
          <button type="button" class="btn btn-default">Cancel</button>
        </a>
        <button type="reset" class="btn btn-default"
          onclick="$('#formInsertStyle').bootstrapValidator('resetForm', true);">Reset</button>
      </div>
    </div>
  </div>
  {{ Form::close() }}
  <!-- /.form -->
</div><!-- /.box -->
@stop
@section('script')
{{ HTML::script('assets/vendor/jquery-chosen/chosen.jquery.js') }}
<script type="text/javascript">
  $(function()
  {
    var config = {
      '.chosen-select'           : {width: '100%'}
    }
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    }
  });
</script>
<!-- Holder for Upload File Thumbnail -->
{{ HTML::script('assets/vendor/holder/holder.js') }}
<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
{{ HTML::script('assets/vendor/jquery-file-upload/js/vendor/jquery.ui.widget.js') }}
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
{{ HTML::script('assets/vendor/file-upload/load-image.all.min.js') }}
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
{{ HTML::script('assets/vendor/file-upload/canvas-to-blob.min.js') }}
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
{{ HTML::script('assets/vendor/jquery-file-upload/js/jquery.iframe-transport.js') }}
<!-- The basic File Upload plugin -->
{{ HTML::script('assets/vendor/jquery-file-upload/js/jquery.fileupload.js') }}
<!-- The File Upload processing plugin -->
{{ HTML::script('assets/vendor/jquery-file-upload/js/jquery.fileupload-process.js') }}
<!-- The File Upload image preview & resize plugin -->
{{ HTML::script('assets/vendor/jquery-file-upload/js/jquery.fileupload-image.js') }}
<!-- The File Upload validation plugin -->
{{ HTML::script('assets/vendor/jquery-file-upload/js/jquery.fileupload-validate.js') }}
<!-- File Upload Main -->
{{ HTML::script('assets/vendor/file-upload/premium.js') }}
<script>
  $(function() {
    // Change this to the location of your server-side upload handler
    @if (Request::is('admin/*'))
      var url = "{{ url('admin/media') }}";
    @else
      var url = "{{ url('user/media') }}";
    @endif

    myFileUpload($('#wrapper-file-upload'), url, 'style_photo');
    myFileUpload($('#cardInputImage1'), url, 'style_photo');
});
</script>

<!-- page script -->
<script type="text/javascript">
  @if(Session::get('laravelValidatorJSON') === null)
myGlobal.laravelValidator = null;
@else
myGlobal.laravelValidator = {
    {
        Session::get('laravelValidatorJSON')
    }
};
@endif

$(function() {
    $formInsertStyle = $('#formInsertStyle');

    $form = $formInsertStyle;

    formStoreStyleRules = {
        title: {
            message: 'The title is not valid',
            validators: {
                notEmpty: {
                    message: 'The title is required and cannot be empty'
                },
                stringLength: {
                    min: 3,
                    max: 30,
                    message: 'The title must be more than 3 and less than 30 characters long'
                }
            }
        },
        photo_id: {
            feedbackIcons: 'false',
            message: 'The photo is not valid',
            validators: {
                notEmpty: {
                    message: 'The photo is required and cannot be empty'
                },
            }
        },
        gender: {
            message: 'The gender is not valid',
            validators: {
                notEmpty: {
                    message: 'The gender is required and cannot be empty'
                },
            }
        },
        'emotion_ids[]': {
            message: 'The emotion is not valid',
            validators: {
                callback: {
                    message: 'Please choose at least one emotion',
                    callback: function(value, validator) {
                        // Get the selected options
                        var options = validator.getFieldElements('emotion_ids[]').val();
                        return (options != null && options.length >= 1);
                    }
                }
            }
        },
        'keyword_ids[]': {
            message: 'The user keyword is not valid',
            validators: {
                callback: {
                    message: 'Please choose at least one keyword',
                    callback: function(value, validator) {
                        // Get the selected options
                        var options = validator.getFieldElements('keyword_ids[]').val();
                        return (options != null && options.length >= 1);
                    }
                }
            }
        },
    };

    formUpdateStyleRules = {
        title: {
            message: 'The title is not valid',
            validators: {
                notEmpty: {
                    message: 'The title is required and cannot be empty'
                },
                stringLength: {
                    min: 3,
                    max: 30,
                    message: 'The title must be more than 3 and less than 30 characters long'
                }
            }
        },
        photo_id: {
            feedbackIcons: 'false',
            message: 'The photo is not valid',
            validators: {
                notEmpty: {
                    message: 'The photo is required and cannot be empty'
                },
            }
        },
        gender: {
            message: 'The gender is not valid',
            validators: {
                notEmpty: {
                    message: 'The gender is required and cannot be empty'
                },
            }
        },
        'emotion_ids[]': {
            message: 'The emotion is not valid',
            validators: {
                callback: {
                    message: 'Please choose at least one emotion',
                    callback: function(value, validator) {
                        // Get the selected options
                        var options = validator.getFieldElements('emotion_ids[]').val();
                        return (options != null && options.length >= 1);
                    }
                }
            }
        },
        'keyword_ids[]': {
            message: 'The user keyword is not valid',
            validators: {
                callback: {
                    message: 'Please choose at least one keyword',
                    callback: function(value, validator) {
                        // Get the selected options
                        var options = validator.getFieldElements('keyword_ids[]').val();
                        return (options != null && options.length >= 1);
                    }
                }
            }
        },
    };

    myGlobal.bootstrapValidatorDefaults.excluded = ':disabled';

    @if(Route::currentRouteName() === 'admin.designer.style.premium.create')
    myGlobal.bootstrapValidatorDefaults.fields = formStoreStyleRules;
    @elseif(Route::currentRouteName() === 'admin.designer.style.premium.edit')
    myGlobal.bootstrapValidatorDefaults.fields = formUpdateStyleRules;
    @endif

    $formInsertStyle
        .find('[name="emotion_ids"]')
        .chosen()
        // Revalidate the color when it is changed
        .change(function(e) {
            $formInsertDesigner.bootstrapValidator('revalidateField', 'emotion_ids[]');
        })
        .end()
        .find('[name="keyword_ids"]')
        .chosen()
        // Revalidate the color when it is changed
        .change(function(e) {
            $formInsertDesigner.bootstrapValidator('revalidateField', 'keyword_ids[]');
        })
        .end()
        .bootstrapValidator(myGlobal.bootstrapValidatorDefaults);

    updateValidateStatus($formInsertStyle.data('bootstrapValidator'), myGlobal.laravelValidator);
});
</script>

<!-- Javascript Card -->
<script type="text/javascript">
  $("#inputSelectType1").change(function() {
    var select = $("#inputSelectType1 option:selected").val();
    switch (select) {
        case "image":
        case "text":
        case "quiz":
    }
    switch (select) {
        case "text":
            $('#cardInputImage1').hide();
            break;

        case "image":
        case "quiz":
            $('#cardInputImage1').show();
            break;
    }
    switch (select) {
        case "text":
        case "image":
            $('#cardInputSubType1').hide();
            break;

        case "quiz":
            $('#cardInputSubType1').show();
            break;
    }
});

/*add new card*/
$("#addCard").click(function() {
    /*Plih input berdasarkan select*/
    $("#inputSelectType" + intId).change(function() {
        var select = $("#inputSelectType" + intId + " option:selected").val();
        switch (select) {
            case "image":
            case "text":
            case "quiz":
        }
        switch (select) {
            case "text":
                $('#cardInputImage' + intId).hide();
                break;

            case "image":
            case "quiz":
                $('#cardInputImage' + intId).show();
                break;
        }
        switch (select) {
            case "text":
            case "image":
                $('#cardInputSubType' + intId).hide();
                break;

            case "quiz":
                $('#cardInputSubType' + intId).show();
                break;
        }
    });

    /*Upload foto*/
    @if(Request::is('admin/media/*'))
    var url = "{{ url('admin/media/premium') }}";
    @else
    var url = "{{ url('media/premium') }}";
    @endif

    myFileUpload($('#cardUpload' + intId), url, 'style_photo', validator);

    <?php $login_type = Request::is('admin/*') ? 'admin' : 'user'; ?>
});
</script>
@stop