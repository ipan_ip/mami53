@extends('admin.layouts.main')
@section('style')
<style>
    .table>tbody>tr>td {
        vertical-align: middle;
    }

    .font-large {
        font-size: 1.5em;
    }

    .font-semi-large {
        font-size: 1.15em;
    }

    .font-grey {
        color: #9E9E9E;
    }

    .dropdown-menu>li>a:hover {
        color: #333
    }

    .label-grey {
        background-color: #CFD8DC;
    }

    tr.discount-active td {
        background-color: #C8E6C9;
    }


    /* Sweetalert */

    .swal-shown {
        height: auto !important;
    }

    .custom-swal {
        z-index: 10000 !important;
    }

    .custom-wide-swal {
        width: 640px !important;
        font-size: 13px !important;
    }

    /* Select2 tweak */

    .input-group .select2-container,
    .form-group .select2-container {
        position: relative !important;
        z-index: 2;
        float: left !important;
        width: 100% !important;
        margin-bottom: 0 !important;
        display: table !important;
        table-layout: fixed !important;
    }

    /* Modal tweak */

    .modal-open {
        overflow: hidden;
        position: fixed;
        width: 100%;
    }


    /* Centering the modal */

    .modal {
        text-align: center;
        padding: 0 !important;
    }

    .modal:before {
        content: '';
        display: inline-block;
        height: 100%;
        vertical-align: middle;
        margin-right: -4px;
    }

    .modal-dialog {
        display: inline-block;
        text-align: left;
        vertical-align: middle;
    }
</style>
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
<link rel="stylesheet" href="//cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
@stop

@section('content')
<!-- table -->
<div class="box box-default">
    <div class="box-header">
        <h3 class="box-title">{!! $boxTitle !!}</h3>
    </div><!-- /.box-header -->
    <div class="box-body row">
        <div class="col-md-12">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <a href="{{ URL::route($createAction, $designerId) }}">
                        <button type="submit" class="btn-add btn btn-primary btn-sm">
                            <i class="fa fa-plus">&nbsp;</i>Add Media</button>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <table id="tableListStyle" class="table table-striped">
                <thead>
                    <tr>
                        <th class="text-center">Type</th>
                        <th class="text-center" width="10%">Media</th>
                        <th width="40%" style="padding-left:25px!important;">Description</th>
                        <th class="text-center">Source</th>
                        <th class="text-center">Uploaded</th>
                        <th class="text-center" style="margin-right:100px!important;"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($rowsStyle as $rowStyleKey => $rowStyle)
                    <tr>
                        <td class="text-center">
                            @if ($rowStyle->type == 'quiz')
                            <i class="fa fa-gift fa-2x "></i><br><small>Quiz</small>
                            @elseif ($rowStyle->type == 'text')
                            <i class="fa fa-text-height fa-2x"></i><br><small>Text</small>
                            @else
                            <i class="fa fa-photo fa-2x font-grey"></i><br><small>Photo</small>
                            @endif
                        </td>
                        <td class="text-center">
                            @if ($rowStyle->type == 'image')
                                <a href="{{ $rowStyle->photo_url['large'] }}" data-fancybox="gallery">
                                    <img width="160" class="img-thumbnail" src="{{ $rowStyle->photo_url['small'] }}"
                                    alt="{{ $rowStyle->description }}"
                                    data-src="holder.js/150x84?text=Foto \n Tidak \n Ditemukan">
                                </a>
                            @endif
                        </td>
                        <td style="padding-left:25px!important;">
                            <span class="font-semi-large">{{ $rowStyle->description }}</span>
                            @if (strpos($rowStyle->description, '-cover') !== FALSE)
                                <br><span class="label label-{{ $designer->photo_id == $rowStyle->photo_id ? 'success' : 'default' }}"><i class="fa fa-bookmark"></i> Cover Photo</span>
                            @endif
                        </td>
                        <td class="text-center">{{ $rowStyle->source }}</td>
                        <td class="text-center">{{ \Carbon\Carbon::parse($rowStyle->created_at)->format('d M Y') }}</td>
                        <td class="table-action-column text-center">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default btn-sm dropdown-toggle"
                                    data-toggle="dropdown">
                                    <i class="glyphicon glyphicon-cog"></i> Actions
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                    @if ($rowStyle->type == 'image')
                                        @if (strpos($rowStyle->description, '-cover') === FALSE)
                                        <li>
                                            <a href="{{ route($coverAction, $rowStyle->id) }}" title="Set As Cover">
                                                <i class="fa fa-bookmark"></i> Set As Cover Photo
                                            </a>
                                        </li>
                                        @endif
                                        <li>
                                            <a href="{{ route($rotateAction, array($rowStyle->id)) }}" title="Rotate Image">
                                                <i class="fa fa-rotate-right"></i> Rotate Image
                                            </a>
                                        </li>
                                    @endif
                                    <li>
                                        <a href="{{ route($editAction, array($rowStyle->designer_id, $rowStyle->id)) }}"
                                            title="Edit Media">
                                            <i class="fa fa-pencil"></i> Edit Media
                                        </a>
                                    </li>
                                    <?php $description = str_replace(",","",str_replace('"','',$rowStyle->description)); ?>
                                    @if (strpos($rowStyle->description, '-cover') === FALSE)
                                    <li class="text-danger">
                                        <a href="{{ route($deleteAction, [$designerId, $rowStyle->id]) }}"
                                            title="Delete Media">
                                            <i class="fa fa-trash-o"></i> Remove Media
                                        </a>
                                    </li>
                                    @endif
                                </ul>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div><!-- /.box-body -->
</div><!-- /.box -->
<!-- table -->
@stop

@section('script')
<!-- page script -->
<script src="//cdn.jsdelivr.net/npm/holderjs@2.9.6/holder.min.js"></script>
<script src="//cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
<script type="text/javascript">
    // Sweetalert functions
function triggerAlert(type, message) {
    Swal.fire({
        type: type,
        customClass: {
            container: 'custom-swal'
        },
        title: message
    });
}

function triggerLoading(message) {
    Swal.fire({
        title: message,
        customClass: {
            container: 'custom-swal'
        },
        allowEscapeKey: false,
        allowOutsideClick: false,
        onOpen: () => {
            swal.showLoading();
        }
    });
}
</script>
@stop