<table id="tableListDesigner" class="table" style="padding-left:10px !important;">
	<thead>
	<tr>
		<th width="150px"></th>
		<th>Owner</th>
		<th>Testimony</th>
		<th>Status</th>
		<th>Created / Updated</th>
		<th class="table-action-column" width="10%"></th>
	</tr>
	</thead>
	<tbody>
	@if (!count($rowsData))
		<tr>
			<td class="text-center" colspan="14">
				<div class="callout callout-danger">No data to show!<br>Click <strong>[Add
						Testimony]</strong> to add new testimony data
				</div>
			</td>
		</tr>
	@else
		@foreach ($rowsData as $rowDataKey => $rowData)
			<tr style="border-top:none!important;">
				<td class="text-center">
					<a href="{{ isset($rowData['media_url']) ? $rowData['media_url']['large'] : '' }}" data-fancybox
					   data-caption="{{ $rowData['owner_name'] }}">
						<img width="120" class="img-circle"
						     src="{{ isset($rowData['media_url']) ? $rowData['media_url']['medium'] : '' }}">
					</a>
				</td>
				<td style="font-size:14px">
                    <span class="font-semi-large">
                        {{ $rowData['owner_name'] }}
                    </span>
					<br/>
					<i class="fa fa-home"></i> {{ $rowData['kost_name'] }}
				</td>
				<td style="width: 40%;">
					{!! $rowData['quote'] !!}
				</td>
				<td class="font-semi-large">
					@if ($rowData['is_active'] !== 1)
						<span class="label label-danger" style="margin-right:10px;"><i
									class="fa fa-ban"></i> Inactive</span>
					@else
						<span class="label label-success" style="margin-right:10px;"><i class="fa fa-check"></i> Active</span>
					@endif
				</td>
				<td>
					{{ date('j F Y', strtotime($rowData['updated_at'])) }}
				</td>
				<td class="table-action-column">
					<div class="btn-group">
						<button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
							<i class="glyphicon glyphicon-cog"></i> Actions
							<span class="caret"></span>
						</button>
						<ul class="dropdown-menu pull-right">
							<li>
								<a href="#" class="actions" data-action="edit" data-data="{{ $rowData }}"><i
											class="fa fa-edit"></i> Edit</a>
							</li>
							<li>
								@if ($rowData['is_active'] !== 1)
									<a href="#" class="actions" data-action="activate" data-data="{{ $rowData }}"><i
												class="fa fa-check-circle"></i>Activate</a>
								@else
									<a href="#" class="actions" data-action="deactivate" data-data="{{ $rowData }}"><i
												class="fa fa-ban"></i>Deactivate</a>
								@endif
							</li>
							<li class="divider"></li>
							<li>
								<a href="#" class="actions" data-action="remove" data-data="{{ $rowData }}"><i
											class="fa fa-trash"></i> Remove</a>
							</li>
						</ul>
					</div>
				</td>
			</tr>
		@endforeach
	@endif
	</tbody>
</table>