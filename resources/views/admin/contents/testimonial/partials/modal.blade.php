{{-- Add / Update Modal --}}
<div class="modal" id="modal" role="dialog" aria-labelledby="modalLabel">
	<div class="modal-dialog" role="modal">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top:2px;"><i
							class="fa fa-lg fa-times-circle"></i></button>
				<h4 class="modal-title"><i class="fa fa-plus"></i>&nbsp;&nbsp;<span id="modalTitle"></span></h4>
			</div>
			<div class="modal-body">
				{{-- <form id="form" action="" method="post"> --}}
				<div class="row">
					<div class="col-md-4">
						<div class="row">
							<div class="form-group col-md-12 text-center">
								<form id="photoSection" action="/admin/media" method="POST" class="dropzone">
									{{ csrf_field() }}
									<input type="hidden" name="media_type" value="user_style">
								</form>
								<small class="text-muted"><strong>Penting!</strong> Pastikan foto mempunyai rasio 1:1
									(square) untuk hasil display terbaik, dan ukuran foto tidak melebihi 5MB.</small>
								<input type="hidden" id="photoId" name="photo_id" value="" data-filename="">
							</div>
						</div>
					</div>
					<div class="col-md-8">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label for="name">Owner Name</label>
									<input type="text" id="owner_name" name="owner_name" class="form-control">
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label for="content">Kost Name</label>
									<input type="text" id="kost_name" name="kost_name" class="form-control">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<label for="target_user">Testimony</label>
						<textarea class="form-control" name="quote" id="content" rows="5"
						          style="width: 100%"></textarea>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6" style="padding-top: 15px!important;padding-bottom: 15px!important;">
						<div class="form-group">
							<label for="is_active">Activate After Saving</label><br/>
							<input type="checkbox"class="form-control" id="activate" name="is_active" data-toggle="toggle" data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger" checked>
						</div>
					</div>
				</div>
				{{-- </form> --}}
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="submit">
					<i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save
				</button>
			</div>
		</div>
	</div>
</div>