@extends('admin.layouts.main')

@section('style')
<style>
    .table>tbody>tr>td {
        vertical-align: middle;
    }

    .font-large {
        font-size: 1.5em;
    }

    .font-semi-large {
        font-size: 1.2em;
    }

    .font-grey {
        color: #9E9E9E;
    }

    .dropdown-menu>li>a:hover {
        color: #333
    }

    .label-grey {
        background-color: #CFD8DC;
    }

    [hidden] {
        display: none !important;
    }

    /* Select2 tweak */

    .input-group .select2-container,
    .form-group .select2-container {
        position: relative !important;
        z-index: 2;
        float: left !important;
        width: 100% !important;
        margin-bottom: 0 !important;
        display: table !important;
        table-layout: fixed !important;
    }

    /* Sweetalert */

    .swal-shown {
        height: auto !important;
    }

    .custom-swal {
        z-index: 10000 !important;
    }

    .custom-wide-swal {
        width: 640px !important;
        font-size: 13px !important;
    }

    /* Custom table row */
    tr.inactive td {
        background-color: #E6C8C8;
    }

    /* Modal tweak */

    .modal-open {
        overflow: hidden;
        position: fixed;
        width: 100%;
    }


    /* Centering the modal */

    .modal {
        text-align: center;
        padding: 0 !important;
    }

    .modal:before {
        content: '';
        display: inline-block;
        height: 100%;
        vertical-align: middle;
        margin-right: -4px;
    }

    .modal-dialog {
        display: inline-block;
        text-align: left;
        vertical-align: middle;
    }

    .clockpicker-popover {
        z-index: 999999 !important;
    }

    .autocomplete-suggestions {
        border: 1px solid #999;
        background: #FFF;
        overflow: auto;
    }

    .autocomplete-suggestion {
        padding: 2px 5px;
        white-space: nowrap;
        overflow: hidden;
    }

    .autocomplete-selected {
        background: #F0F0F0;
    }

    .autocomplete-suggestions strong {
        font-weight: normal;
        color: #3399FF;
    }

    .autocomplete-group {
        padding: 2px 5px;
    }

    .autocomplete-group strong {
        display: block;
        border-bottom: 1px solid #000;
    }
</style>
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
<link rel="stylesheet" href="//cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/dropzone@5.5.1/dist/min/dropzone.min.css">
<link href="//gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
@endsection

@section('content')
<!-- table -->
<div class="box box-default">
    <!-- Box Header -->
    <div class="box-header">
        <div class="col-md-6 text-center">
            <h3 class="box-title"><i class="fa fa-commenting-o"></i> {{ $boxTitle }}</h3>
        </div>
        <!-- Add button -->
        <div class="col-md-6">
            <div class="form-horizontal pull-right" style="padding-top: 10px;">
                <button class="btn btn-success actions" data-action="add" style="margin-right: 10px;">
                    <i class="fa fa-plus"></i>&nbsp;&nbsp;Add Testimony
                </button>
            </div>
        </div>
    </div>
    <!-- /.box-header -->

    <div class="box-body" style="padding-top: 30px;">
        {{-- Table --}}
        @include('admin.contents.testimonial.partials.table')

    </div><!-- /.box-body -->
    {{-- pagination --}}
    @if (count($rowsData))
    <div class="box-body text-center">
        {{ $rowsData->appends(Request::except('page'))->links() }}
    </div>
    @endif
    {{-- /.pagination --}}
</div><!-- /.box -->
<!-- table -->

@include('admin.contents.testimonial.partials.modal')

@stop

@section('script')
<!-- Holder for Upload File Thumbnail -->
<script src="//cdn.jsdelivr.net/npm/holderjs@2.9.6/holder.min.js"></script>
<script src="//cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/devbridge-autocomplete@1.4.10/dist/jquery.autocomplete.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/dropzone@5.5.1/dist/min/dropzone.min.js"></script>
<script src="//gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

<!-- page script -->
<script type="text/javascript">
    Dropzone.autoDiscover = false;

        function triggerAlert(type, message) {
            Swal.fire({
                type: type,
                customClass: {
                    container: 'custom-swal'
                },
                html: message
            });
        }

        function triggerLoading(message) {
            Swal.fire({
                title: message,
                customClass: {
                    container: 'custom-swal'
                },
                allowEscapeKey: false,
                allowOutsideClick: false,
                onOpen: () => {
                    swal.showLoading();
                }
            });
        }

        function viewPhoto(data) {
            Swal.fire({
                title: data.name,
                text: data.email,
                imageUrl: data.src
            });
        }

        function ajaxCall(type, title, html, method, url, payload, input, btnText, btnClass, needRefresh = true) {
            Swal.fire({
                type: type,
                title: title,
                html: html,
                buttonsStyling: btnClass == null,
                confirmButtonText: btnText,
                confirmButtonClass: btnClass,
                showCancelButton: true,
                cancelButtonClass: (btnClass !== null) ? 'btn btn-default' : null,
                customClass: 'custom-swal',
                showLoaderOnConfirm: true,
                input: input,
                preConfirm: (response) => {
                    if (input !== null) {
                        let number = response;
                        if (!response) {
                            return Swal.showValidationMessage('Please provide a valid phone number!');
                        }

                        payload = {
                            "template_id": payload.id,
                            "number": response
                        };
                    }

                    return $.ajax({
                        type: method,
                        url: url,
                        data: payload,
                        dataType: 'json',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: (response) => {
                            if (response.success == false) {
                                Swal.close();
                                triggerAlert('error', response.message);
                            }

                            return;
                        },
                        error: (error) => {
                            Swal.close();
                            triggerAlert('error', JSON.stringify(error));
                        }
                    });
                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                if (!result.dismiss != 'cancel') {
                    if (result.value.success == true) {
                        Swal.fire({
                            type: 'success',
                            title: result.value.title,
                            html: result.value.message,
                            onClose: () => {
                                if (needRefresh) window.location.reload();
                            }
                        });
                    } else {
                        Swal.fire({
                            type: 'error',
                            title: "Failed!",
                            html: result.value.message
                        });
                    }
                }
            });
        }

        $(function () {
            // Helper variable
            let _editing, _data, _modalTitle;

            // Actions button listeners
            $('.actions').click((e) => {
                e.preventDefault();

                const action = $(e.currentTarget).data('action');

                // Setup Notification
                if (action === 'add') {
                    _editing = false;
                    _modalTitle = "New Owner Testimony";

                    $('#modal').modal('show');
                }

                // Editing action
                else if (action === 'edit') {
                    _editing = true;
                    _data = $(e.currentTarget).data('data');

                    _modalTitle = "Update Owner Testimony";

                    $('#modal').modal('show');
                }

                // Activate / Deactivate action
                else if (action === 'activate' || action === 'deactivate') {
                    let title, html, btnText, url, type, payload;

                    _data = $(e.currentTarget).data('data');

                    if (action === 'activate') {
                        type = 'question';
                        title = 'Activate Testimony?';
                        html = null;
                        btnText = 'Activate';
                        url = "/admin/testimonial/activate";
                        payload = {
                            status: 1,
                            id: _data.id
                        };
                    } else {
                        type = 'warning';
                        title = 'Deactivate Testimony?';
                        html = null;
                        btnText = 'Deactivate';
                        url = "/admin/testimonial/deactivate";
                        payload = {
                            status: 0,
                            id: _data.id
                        };
                    }

                    ajaxCall(type, title, html, 'POST', url, payload, null, btnText, null);
                }

                // Removing testimonial data
                else if (action == 'remove') {
                    _data = $(e.currentTarget).data('data');

                    Swal.fire({
                        title: 'Remove owner testimony data?',
                        type: 'warning',
                        buttonsStyling: false,
                        confirmButtonClass: 'btn btn-danger',
                        showCancelButton: true,
                        cancelButtonClass: 'btn btn-default',
                        showLoaderOnConfirm: true,
                        confirmButtonText: 'Yes, remove it!',
                        preConfirm: (val) => {
                            return $.ajax({
                                type: 'POST',
                                url: "/admin/testimonial/remove",
                                dataType: 'json',
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                data: {
                                    'id': _data.id,
                                },
                                success: (response) => {
                                    return response;
                                },
                                error: (error) => {
                                    triggerAlert('error', error.message);
                                }
                            });
                        },
                        allowOutsideClick: () => !Swal.isLoading()
                    }).then((result) => {
                        var res = result.value;
                        if (res.success) {
                            Swal.fire({
                                type: 'success',
                                title: "Owner testimony data successfully removed",
                                text: "Page will be refreshed after you clicked OK",
                                onClose: () => {
                                    window.location.reload();
                                }
                            });
                        } else {
                            triggerAlert('error', res.message);
                        }
                    })
                }
            });

            // :: Modal Events ::
            let modal = $('#modal');
            modal
                .on("show.bs.modal", (e) => {
                    modal.data('bs.modal').options.keyboard = false;
                    modal.data('bs.modal').options.backdrop = 'static';

                    // reset photoId
                    $('#photoId').val('');

                    // helper variables (modal scope)
                    let rawImage = null;

                    // Form elements
                    const modalTitle = $('#modalTitle');
                    const photoSection = $('#photoSection');
                    const ownerNameInput = $('#owner_name');
                    const kostNameInput = $('#kost_name');
                    const quoteInput = $('#content');
                    const activateCheckbox = $('#activate');
                    const submitButton = $('#submit');

                    function validateFormData() {
                        if ($('#photoId').val() === '') {
                            triggerAlert('error', "<h5>Please upload Owner Photo first!</h5>");
                            $('#photoId').parent().addClass('has-error');
                            return false;
                        }

                        if (ownerNameInput.val() === '') {
                            triggerAlert('error', "<h5>Owner Name is required!</h5>");
                            ownerNameInput.parent().addClass('has-error');
                            ownerNameInput.focus();
                            return false;
                        }

                        if (kostNameInput.val() === '') {
                            triggerAlert('error', "<h5>Owner Name is required!</h5>");
                            kostNameInput.parent().addClass('has-error');
                            kostNameInput.focus();
                            return false;
                        }

                        if (quoteInput.val() === '') {
                            triggerAlert('error', "<h5>Owner Testimony is required!</h5>");
                            quoteInput.parent().addClass('has-error');
                            quoteInput.focus();
                            return false;
                        }

                        return true;
                    }

                    function getFormData() {
                        let formData = new Object();

                        formData.photo_id = $('#photoId').val();
                        formData.owner_name = ownerNameInput.val();
                        formData.kost_name = kostNameInput.val();
                        formData.quote = quoteInput.val();
                        formData.is_active = activateCheckbox.prop('checked') == true ? 1 : 0;
                        formData.creator_id = {{ Auth::user()->id }};

                        if (_editing) formData.id = _data.id;

                        return formData;
                    }

                    // Initialize components
                    modalTitle.html(_modalTitle);
                    activateCheckbox.bootstrapToggle('on');

                    // Set state for editing mode
                    if (_editing) {
                        $('#photoId').val(_data.photo_id);
                        ownerNameInput.val(_data.owner_name);
                        kostNameInput.val(_data.kost_name);
                        quoteInput.val(_data.quote);

                        if (_data.is_active != '1') {
                            activateCheckbox.bootstrapToggle('off');
                        }
                    }

                    // initiate Dropzone
                    if (Dropzone.instances.length == 0) {
                        photoSection.dropzone({
                            paramName: "media",
                            url: '/admin/media',
                            maxFilesize: 2,
                            maxFiles: 1,
                            acceptedFiles: 'image/*',
                            dictDefaultMessage: 'Click to upload owner photo',
                            dictCancelUpload: 'Cancel',
                            dictRemoveFile: 'Remove',
                            addRemoveLinks: true,
                            thumbnailWidth: null,
                            thumbnailHeight: null,
                            success: function (file, response) {
                                var photoInput = $('#photoId');
                                photoInput.val(response.media.id);
                                photoInput.attr('data-filename', file.name);
                            },
                            init: function () {
                                // generate existing photo
                                if (_editing) {
                                    let mockFile = {name: _data.owner_name + 'jpg', size: 12345};

                                    if (_data.media_url !== undefined) {
                                        this.emit('thumbnail', mockFile, _data.media_url.medium);
                                    }
                                }

                                this.on("addedfile", function (file) {
                                    triggerLoading('Mengupload foto..');
                                });
                                this.on("thumbnail", function (file, dataUrl) {
                                    $('.dz-image').last().find('img').attr({width: '100%', height: '100%'});
                                }),
                                    this.on("success", function (file) {
                                        $('.dz-image').css({"width": "100%", "height": "auto"});
                                        if (Swal.isLoading) Swal.close();
                                    })
                                this.on("maxfilesexceeded", function (file) {
                                    this.removeAllFiles();
                                    this.addFile(file);
                                });
                                this.on("removedfile", function (file) {
                                    $.get('/admin/media/user/' + $('#photoId').val(), (data, status) => {
                                        console.log(data, status);
                                    });
                                    $('#photoId').val('');
                                });
                            }
                        });
                    }

                    ownerNameInput.devbridgeAutocomplete({
                        dataType: 'json',
                        serviceUrl: '/admin/testimonial/ajax/owner-name',
                        appendTo: ownerNameInput.parent(),
                        minChars: 3
                    });

                    kostNameInput.devbridgeAutocomplete({
                        dataType: 'json',
                        serviceUrl: '/admin/testimonial/ajax/kost-name',
                        appendTo: kostNameInput.parent(),
                        minChars: 3
                    });

                    // Submit the form
                    submitButton.on('click', e => {
                        e.preventDefault();

                        if (validateFormData()) {
                            // compile dataObject
                            let dataObject = getFormData();

                            Swal.fire({
                                type: 'question',
                                customClass: {
                                    container: 'custom-swal'
                                },
                                title: 'Save owner testimony data?',
                                showCancelButton: true,
                                confirmButtonText: 'Save',
                                showLoaderOnConfirm: true,
                                preConfirm: () => {
                                    return $.ajax({
                                        type: 'POST',
                                        url: "/admin/testimonial",
                                        dataType: 'json',
                                        headers: {
                                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                        },
                                        data: dataObject
                                    });
                                },
                                allowOutsideClick: () => !Swal.isLoading()
                            }).then((result) => {
                                if (!result.dismiss) {
                                    const response = result.value;

                                    if (!response.success) {
                                        triggerAlert('error', '<h5>' + response.message + '</h5>');
                                    } else {
                                        $('#add-modal').modal('toggle');

                                        Swal.fire({
                                            type: 'success',
                                            customClass: {
                                                container: 'custom-swal'
                                            },
                                            title: "Owner testimony successfully saved!",
                                            html: "<h5>" + response.message + "</h5>",
                                            onClose: () => {
                                                window.location.reload();
                                            }
                                        });
                                    }

                                }
                            })
                        }
                    })
                });
        });
</script>
@stop