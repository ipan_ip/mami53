@extends('admin.layouts.main')

@section('style')
<style>
    .table>tbody>tr>td {
        vertical-align: middle;
    }

    .font-large {
        font-size: 1.5em;
    }

    .font-semi-large {
        font-size: 1.15em;
    }

    .font-grey {
        color: #9E9E9E;
    }

    .dropdown-menu>li>a:hover {
        color: #333
    }

    .label-grey {
        background-color: #CFD8DC;
    }

    tr.checker-active td {
        background-color: #C8E6C9;
    }


    /* Sweetalert */

    .swal-shown {
        height: auto !important;
    }

    .custom-swal {
        z-index: 10000 !important;
    }

    .custom-wide-swal {
        width: 640px !important;
        font-size: 13px !important;
    }


    /* Wixard twekas */

    .wizard>.content>.body {
        width: 100% !important;
        padding: 0 !important;
    }


    /* Select2 tweak */

    .input-group .select2-container,
    .form-group .select2-container {
        position: relative !important;
        z-index: 2;
        float: left !important;
        width: 100% !important;
        margin-bottom: 0 !important;
        display: table !important;
        table-layout: fixed !important;
    }


    /* Modal tweak */

    .modal-open {
        overflow: hidden;
        position: fixed;
        width: 100%;
    }


    /* Centering the modal */

    .modal {
        text-align: center;
        padding: 0 !important;
    }

    .modal:before {
        content: '';
        display: inline-block;
        height: 100%;
        vertical-align: middle;
        margin-right: -4px;
    }

    .modal-dialog {
        display: inline-block;
        text-align: left;
        vertical-align: middle;
    }
</style>
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" />
<link href="//cdn.jsdelivr.net/npm/select2-bootstrap-theme@0.1.0-beta.10/dist/select2-bootstrap.min.css"
    rel="stylesheet" />
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/dropzone@5.5.1/dist/min/dropzone.min.css">
@endsection

@section('content')
<div class="box box-default">
    <!-- Box Header -->
    <div class="box-header">
        <div class="col-md-12 text-center">
            <h3 class="box-title"><i class="fa fa-check-square-o"></i> {{ $contentHeader }}</h3>
        </div>
    </div>
    <!-- /.box-header -->

    <div class="box-body no-padding">

        {{-- Top Bar --}}
        <div class="horizontal-wrapper">
            <div class="btn-horizontal-group bg-default form-inline" style="padding-left: 10px;padding-right: 10px;">
                <!-- Add button -->
                <div class="col-md-6 bg-default" style="padding-bottom:10px;">
                    <a href="#" class="btn btn-success btn pull-left actions" data-action="add">
                        <i class="fa fa-plus">&nbsp;</i>Add Checker Profile
                    </a>
                </div>
                <!-- Search filters -->
                <div class="col-md-6 bg-default" style="padding-bottom:10px;text-align:right">
                    <form action="" method="get">
                        <input type="text" name="q" class="form-control input-sm" placeholder="Nama Checker"
                            autocomplete="off" value="{{ request()->input('q') }}">
                        <button class="btn btn-primary btn-md" id="buttonSearch"><i
                                class="fa fa-search">&nbsp;</i>Search</button>
                    </form>
                </div>
            </div>
        </div>

        {{-- Table --}}
        <table class="table">
            <thead>
                <tr>
                    <th></th>
                    <th>Nama Lengkap</th>
                    <th width="20%">Domisili</th>
                    <th class="text-center">Jenis Kelamin</th>
                    <th class="text-center">Work Place</th>
                    <th class="text-center">Tgl Aktif</th>
                    <th class="text-center">Checked Kost</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($rowsChecker as $index => $checker)
                <tr>
                    <td class="text-center">
                        <a href="#" data-image="{{ $checker->photo_url['large'] }}" data-name="{{ $checker->user->name }}"
                            data-email="{{ $checker->user->email }}" data-toggle="lightbox" data-max-width="400">
                            <img src="{{ $checker->photo_url['small'] }}" class="img-circle" width="80">
                        </a>
                    </td>
                    <td>
                        <strong class="font-semi-large">{{ $checker->user->name }}</strong>
                        <br><small>{{ $checker->user->phone_number }}</small>
                        <br><small>{{ $checker->user->email }}</small>
                    </td>
                    <td>
                        {{ $checker->user->address }}
                    </td>
                    <td class="text-center">
                        {{ $checker->user->gender == 'male' ? 'Laki-laki' : 'Perempuan' }}
                    </td>
                    <td class="text-center">
                        {{ $checker->user->work_place }}
                    </td>
                    <td class="text-center">
                        {{ \Carbon\Carbon::parse($checker->created_at)->format('d M Y') }}
                    </td>
                    <td class="text-center">
                        {{ $checker->trackings->count() }}
                    </td>
                    <td class="table-action-column" style="padding-right:15px!important;">
                        <div class="btn-group">
                            <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-cog"></i> Actions
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu pull-right">
                                <li>
                                    <a href="#" class="actions" data-action="update" data-checker="{{ $checker }}"><i
                                            class="glyphicon glyphicon-edit"></i> Edit Profil</a>
                                </li>
                                <li>
                                    <a href="#" class="actions" data-action="remove" data-checker="{{ $checker }}"><i
                                            class="glyphicon glyphicon-trash"></i> Hapus Profil</a>
                                </li>
                            </ul>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>

        </table>
    </div><!-- /.box-body -->

    <div class="box-body no-padding">
        {{ $rowsChecker->appends(Request::except('page'))->links() }}
    </div>
</div><!-- /.box -->
<!-- table -->

<!-- Modal -->
<div class="modal" id="modal" role="dialog" aria-labelledby="modalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top:2px;"><i
                        class="fa fa-lg fa-times-circle"></i></button>
                <h4 class="modal-title" id="modalTitle"></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-12 col-md-offset-0">
                                <div class="box box-solid box-default">
                                    <div class="box-header">
                                        <strong>User Data Source:</strong>
                                    </div>
                                    <div class="box-body row">
                                        <div class="form-group col-md-4">
                                            <select class="form-control" name="dataSource" id="dataSource">
                                                <option value="new" selected>Data Baru</option>
                                                <option value="existing">Data Dari DB</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-8 col-md-offset-0">
                                            <select class="form-control" name="dataUser" id="dataUser"
                                                disabled></select>
                                            <input type="hidden" id="userId" name="user_id" value="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-8 col-md-offset-0">
                                <label for="name" class="control-label">Nama Lengkap</label>
                                <input type="text" class="form-control" placeholder="" id="name" name="name">
                            </div>
                            <div class="form-group col-md-4 col-md-offset-0">
                                <label for="gender" class="control-label">Jenis Kelamin</label>
                                <select class="form-control" name="gender" id="gender">
                                    <option value='female' selected>Perempuan</option>
                                    <option value='male'>Laki-laki</option>
                                </select>
                            </div>
                            <div class="form-group col-md-6 col-md-offset-0">
                                <label for="email" class="control-label">Email</label>
                                <input type="text" class="form-control" placeholder="" id="email" name="email">
                            </div>
                            <div class="form-group col-md-6 col-md-offset-0">
                                <label for="phone" class="control-label">Phone</label>
                                <input type="text" class="form-control" placeholder="" id="phone" name="phone">
                            </div>
                            <div class="form-group col-md-12 col-md-offset-0">
                                <label for="address" class="control-label">Alamat Lengkap</label>
                                <input type="text" class="form-control" placeholder="" id="address" name="address">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="workPlace" class="control-label">Work Place</label>
                                <input type="text" class="form-control" placeholder="" id="workPlace" name="workPlace">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row">
                            <div class="form-group col-md-12 text-center">
                                <form id="photo" action="/admin/media" method="POST" class="dropzone">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="media_type" value="user_style">
                                </form>
                                <small class="text-muted"><strong>Penting!</strong> Pastikan foto mempunyai rasio 1:1 (square) untuk hasil display terbaik, dan ukuran foto tidak melebihi 5MB.</small>
                                <input type="hidden" id="photoId" name="photo_id" value="" data-filename="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="submit">
                    Simpan <i class="fa fa-disk-o"></i></button>
            </div>
        </div>
    </div>
</div>
<!-- /. modal -->

@endsection

@section('script')
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/dropzone@5.5.1/dist/min/dropzone.min.js"></script>
<script>
    Dropzone.autoDiscover = false;
</script>
<script>
    // Sweetalert functions
function triggerAlert(type, message) {
    Swal.fire({
        type: type,
        customClass: {
            container: 'custom-swal'
        },
        title: message
    });
}

function triggerLoading(message) {
    Swal.fire({
        title: message,
        customClass: {
            container: 'custom-swal'
        },
        allowEscapeKey: false,
        allowOutsideClick: false,
        onOpen: () => {
            swal.showLoading();
        }
    });
}

function viewPhoto(data) {
    Swal.fire({
        title: data.name,
        text: data.email,
        imageUrl: data.src
    });
}

/*
DOM
*/
$(function() {
    var token = '{{ csrf_token() }}';

    // Initial variables
    var _checker;
    var _editing = false;
    var _fromDB = false;

    function clearUserData() {
        if (_fromDB) _fromDB = false;
        $('#dataUser').val(null).trigger('change');
        $('#dataUser').prop('disabled', true);
        $('#userId').val('');
        $('#name').val('');
        $('#email').val('');
        $('#phone').val('');
        $('#gender').val('female').trigger('change');
        $('#workPlace').val('');
        $('#address').val('');
        $('#photoId').val('');
    }

    // Photo Popup
    $(document).on('click', '[data-toggle="lightbox"]', function(e) {
        viewPhoto({
            src: $(this).data('image'),
            name: $(this).data('name'),
            email: $(this).data('email')
        });
    });

    // Actions button listeners
    $('.actions').click((e) => {
        e.preventDefault();
        var action = $(e.currentTarget).data('action');

        if (action != 'add') _checker = $(e.currentTarget).data('checker');

        // Add new
        if (action == 'add') {
            $('#modal').modal('show');
        }

        // Updating score
        else if (action == 'update') {
            console.log(_checker);

            _editing = true;
            $('#modal').modal('show');
        }

        // Removing checker data
        else if (action == 'remove') {
            Swal.fire({
                title: 'Hapus Data Profil Mami-Checker?',
                type: 'warning',
                buttonsStyling: false,
                confirmButtonClass: 'btn btn-danger',
                showCancelButton: true,
                cancelButtonClass: 'btn btn-default',
                showLoaderOnConfirm: true,
                confirmButtonText: 'Yes, remove it!',
                preConfirm: (val) => {
                    return $.ajax({
                        type: 'POST',
                        url: "/admin/mami-checker/remove",
                        dataType: 'json',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: {
                            'id': _checker.id,
                        },
                        success: (response) => {
                            return response;
                        },
                        error: (error) => {
                            triggerAlert('error', error.message);
                        }
                    });
                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                var res = result.value;
                if (res.success) {
                    Swal.fire({
                        type: 'success',
                        title: "Profile telah berhasil dihapus",
                        text: "Halaman akan di-refresh setelah Anda klik OK",
                        onClose: () => {
                            window.location.reload();
                        }
                    });
                } else {
                    triggerAlert('error', res.message);
                }
            })
        }

    });

    // Add modal event
    $('#modal')
        .on("show.bs.modal", (e) => {
            $('#modal').data('bs.modal').options.keyboard = false;
            $('#modal').data('bs.modal').options.backdrop = 'static';

            if (!_editing) $('#modalTitle').html('<i class="fa fa-user-circle"></i> Tambah Profile Mami Checker');
            else $('#modalTitle').html('<i class="fa fa-edit"></i>  Edit Profile Mami Checker: ' + _checker.user.name);

            // reset photoId
            $('#photoId').val('');

            // helper variables (modal scope)
            var rawImage = null;

            // Function to populate data
            function populateData(data) {
                $('#userId').val(data.id);
                $('#name').val(data.name);
                $('#email').val(data.email);
                $('#phone').val(data.phone_number);
                if (data.gender != null) {
                    var gender = data.gender.replace(/\s/g, ''); // remove white spaces
                    if (gender != '') $('#gender').val(gender).trigger('change');
                }
                $('#workPlace').val(data.work_place);
                $('#address').val(data.address);
                if (data.photo_id != null) $('#photoId').val(data.photo_id);
            }

            // Function to fetch single user details
            function applyUser(id) {
                triggerLoading('Mengambil data user..');

                $.ajax({
                    type: 'POST',
                    url: "/admin/users/user",
                    data: {
                        id: id
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: (data) => {
                        if(data != null) {
                            _fromDB = true;
                            populateData(data)
                        }

                        if (Swal.isLoading()) Swal.close();
                    },
                    error: (error) => {
                        Swal.fire({
                            type: 'error',
                            customClass: {
                                container: 'custom-swal'
                            },
                            title: 'Error',
                            text: error
                        })
                    }
                });
            }

            function getFormData() {
                var formData = {};

                formData.name = $('#name').val();
                formData.email = $('#email').val();
                formData.phone = $('#phone').val();
                formData.gender = $('#gender').val();
                formData.work_place = $('#workPlace').val();
                formData.address = $('#address').val();
                formData.photo_id = $('#photoId').val();

                if (_editing) {
                    formData.id = _checker.id;
                } else {
                    if (_fromDB) {
                        formData.id = $('#userId').val();
                        formData.from_db = _fromDB;
                    }
                }

                return formData;
            }

            function validateData() {
                if ($("#dataSource").val() == 'existing') {
                    if ($('#dataUser').val() == null || $('#dataUser').val() == '') {
                        Swal.fire({
                            type: 'error',
                            customClass: {
                                container: 'custom-swal'
                            },
                            title: 'Pilih user terlebih dahulu'
                        }).then((result) => {
                            $('#dataUser').select2('open');
                        });
                        return false;
                    }
                }
                if ($('#name').val() == '') {
                    triggerAlert('error', 'Isikan nama lengkap');
                    return false;
                }
                if ($('#email').val() == '') {
                    triggerAlert('error', 'Isikan alamat email');
                    return false;
                }
                if ($('#phone').val() == '') {
                    triggerAlert('error', 'Isikan nomor telepon');
                    return false;
                }
                if ($('#gender').val() == null || $('#gender').val() == '') {
                    triggerAlert('error', 'Pilih jenis kelamin');
                    return false;
                }
                if ($('#address').val() == '') {
                    triggerAlert('error', 'Isikan alamat domisili');
                    return false;
                }
                if ($('#workPlace').val() == '') {
                    triggerAlert('error', 'Isikan work place');
                    return false;
                }
                if ($('#photoId').val() == '' || $('#photoId').val() == 0) {
                    triggerAlert('error', 'Upload foto terlebih dahulu');
                    return false;
                }

                return true;
            }

            // if on editing, then populate data
            if (_editing) populateData(_checker.user);

            // initiate Dropzone
            if (Dropzone.instances.length == 0) {
                $('#photo').dropzone({
                    paramName : "media",
                    url: '/admin/media',
                    maxFilesize: 5,
                    maxFiles: 1,
                    acceptedFiles: 'image/*',
                    dictDefaultMessage: 'Click to upload a photo',
                    dictCancelUpload: 'Cancel',
                    dictRemoveFile: 'Delete',
                    addRemoveLinks: true,
                    thumbnailWidth: null,
                    thumbnailHeight: null,
                    success : function (file, response) {
                        var photoInput = $('#photoId');
                        photoInput.val(response.media.id);
                        photoInput.attr('data-filename', file.name);
                    },
                    init: function() {
                        // generate existing photo
                        if (_editing) {
                            var mockFile = { name: _checker.user.name + 'jpg', size: 12345 };
                            this.emit('thumbnail', mockFile, _checker.photo_url.medium);
                        }

                        this.on("addedfile", function(file) {
                            triggerLoading('Mengupload foto..');
                        });
                        this.on("thumbnail", function(file, dataUrl) {
                            $('.dz-image').last().find('img').attr({width: '100%', height: '100%'});
                        }),
                        this.on("success", function(file) {
                            $('.dz-image').css({"width":"100%", "height":"auto"});
                            if (Swal.isLoading) Swal.close();
                        })
                        this.on("maxfilesexceeded", function(file) {
                            this.removeAllFiles();
                            this.addFile(file);
                        });
                        this.on("removedfile", function(file) {
                            $.get('/admin/media/user/' + $('#photoId').val(), (data, status) => {
                                console.log(data, status);
                            });
                            $('#photoId').val('');
                        });
                    }
                });
            }

            // initiate dropdowns
            $('#dataSource').select2({
                theme: "bootstrap",
                placeholder: "Pilih Jenis Data User",
                minimumResultsForSearch: -1,
                dropdownParent: $("#modal")
            });

            if (_editing) $('#dataSource').prop('disabled', true);

            $('#dataUser').select2({
                theme: "bootstrap",
                placeholder: 'Cari user',
                language: {
                    searching: function() {
                        return "Sedang mengambil daftar user...";
                    },
                    inputTooShort: function() {
                        return 'Ketik nama untuk mencari..';
                    },
                    noResults: function(){
                        return "User tidak ditemukan!";
                    }
                },
                ajax: {
                    url: '/admin/users/checker',
                    data: function (params) {
                        return {
                            search: params.term,
                            page: params.page || 1
                        }
                    },
                    processResults: function (data, params) {
                        var page = params.page || 1;
                        if (data.data.length < 1) var total_count = 0;
                        else var total_count = data.data[0].total_count;
                        return {
                            results: $.map(data.data, item => {
                                return {
                                    id: item.id,
                                    text: item.name + ' (' + item.email + ')'
                                }
                            }),
                            pagination: {
                                more: (page * 10) <= total_count
                            }
                        };
                    },
                    cache: true
                },
                minimumInputLength: 3,
                dropdownParent: $("#modal")
            });

            $('#gender').select2({
                theme: "bootstrap",
                minimumResultsForSearch: -1,
                dropdownParent: $("#modal")
            });

            // Dropdown listeners
            $("#dataSource").on('select2:select', e => {
                var selected = e.params.data.id;
                if (selected == 'existing') {
                    $('#dataUser').prop('disabled', false);
                    $('#dataUser').val(null).trigger('change');
                } else {
                    clearUserData();
                }
            });

            $("#dataUser").on('select2:select', e => {
                var selected = e.params.data.id;
                if (selected != '') {
                    applyUser(selected);
                }
            });

            // Submit the form
            $('#submit').on('click', e => {
                var dataObject = getFormData();

                if (validateData()) {
                    Swal.fire({
                        type: 'question',
                        customClass: {
                            container: 'custom-swal'
                        },
                        title: 'Simpan data checker?',
                        showCancelButton: true,
                        confirmButtonText: 'Submit',
                        showLoaderOnConfirm: true,
                        preConfirm: () => {
                            return $.ajax({
                                type: 'POST',
                                url: "/admin/mami-checker/profile",
                                dataType: 'json',
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                data: dataObject,
                                success: (response) => {
                                    if (!response.success) {
                                        triggerAlert('error', response.message);
                                        return;
                                    }
                                    return
                                },
                                error: (error) => {
                                    triggerAlert('error', error);
                                }
                            });
                        },
                        allowOutsideClick: () => !Swal.isLoading()
                    }).then((result) => {
                        if (!result.dismiss) {
                            $('#modal').modal('toggle');

                            Swal.fire({
                                type: 'success',
                                customClass: {
                                    container: 'custom-swal'
                                },
                                title: "Berhasil menyimpan data checker!",
                                html: "Halaman akan di-refresh setelah Anda klik OK",
                                onClose: () => {
                                    window.location.reload();
                                }
                            });
                        }
                    })
                }

            })

        })
        .on("hidden.bs.modal", (e) => {
            clearUserData();

            // destroy selectorS
            $('#dataUser').empty().select2('destroy');
            $('#dataSource').select2('destroy');
            $('#gender').select2('destroy');

            // reset
            _editing = false;
            _fromDB = false;
        });

});
</script>
@endsection