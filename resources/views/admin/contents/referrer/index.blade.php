@extends('admin.layouts.main')
@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
            <div class="box-tools pull-right">
               <a href="{{ URL::route('admin.referrer.create') }}" class="btn btn-xs btn-danger" style="color: #fff;">Add</a>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">

                </div>
            </div>

            <table id="tableListTag" class="table table-striped">
                <thead>
                    <tr>
                        <th width="30px;">No</th>
                        <th>User Phone</th>
                        <th>Name</th>
                        <th>Code</th>
                        <!--<th class="table-action-column">Action</th>-->

                    </tr>
                </thead>
                <tbody>
                   @foreach ($referrer as $key => $value)
                    <tr>
                        <td>{{ $key+1 }}</td>
                        <td>@if (isset($value->user)) 
                                {{ $value->user->phone_number }}
                            @else 
                                "-"
                            @endif    
                        </td>
                        <td>@if (isset($value->user)) 
                                {{ $value->user->name }}
                            @else 
                                "-"
                            @endif
                        </td>
                        <td>{{ $value->code }}</td>
                        <!--<td class="table-action-column">
                            <div class="btn-action-group">
                                <a href="{{ URL::route('admin.referrer.edit', $value->id) }}" title="Edit">
                                    <i class="fa fa-pencil"></i>
                                </a>
                            </div>
                        </td>-->
                    </tr>
                    @endforeach
                </tbody>
              
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
    <!-- table -->

    @include('admin.partials.delete-modal')
@stop
@section('script')
<!-- page script -->
<script type="text/javascript">
    $(function()
    {
        $("#tableListTag").dataTable();
    });
</script>
@stop