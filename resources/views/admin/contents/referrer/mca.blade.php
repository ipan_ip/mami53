@extends('admin.layouts.main')
@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
            <div class="box-tools pull-right">
               <a href="{{ URL::route('admin.referrer.create') }}" class="btn btn-xs btn-danger" style="color: #fff;">Add</a>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">

                </div>
            </div>

            <table id="tableListTag" class="table table-striped">
                <thead>
                    <tr>
                        <th width="30px;">No</th>
                        <th>MCA</th>
                        <th>Referral</th>
                        <th>Room Status</th>
                        <th>Status</th>
                        <th class="table-action-column">Action</th>

                    </tr>
                </thead>
                <tbody>
                   @foreach ($tracking as $key => $value)
                    <tr>
                        <td>{{ $key+1 }}</td>
                        <td>@if (isset($value->referrer->user)) 
                                {{ $value->referrer->user->name }}
                                {{ $value->referrer->user->phone_number }}
                            @else 
                                "-"
                            @endif    
                        </td>
                        <td>{{ $value->user->name }} / {{ $value->user->phone_number }}</td>
                        <td>{{ $value->room->name }} 
                            @if ($value->room->is_active == 'true') 
                               <span class="label label-success">True</span>
                            @else 
                               <span class="label label-danger">False</span>
                            @endif
                        </td>
                        <td>@if ($value->reward_status == 'waiting')
                                <span class="label label-danger">False</span>
                            @else
                                <span class="label label-success">True</span>
                            @endif        
                        </td>
                        <td class="table-action-column">
                            <div class="btn-action-group">
                                @if ($value->room->is_active == 'true' AND $value->reward_status == 'waiting')
                                    <a href="{{ URL::to('admin/mca/verify', $value->id) }}" title="Edit"><i class="fa fa-arrow-up"></i></a>
                                @else
                                    <label class="label label-success">Success</label>
                                @endif
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
              
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
    <!-- table -->

    @include('admin.partials.delete-modal')
@stop
@section('script')
<!-- page script -->
<script type="text/javascript">
    $(function()
    {
        $("#tableListTag").dataTable();
    });
</script>
@stop