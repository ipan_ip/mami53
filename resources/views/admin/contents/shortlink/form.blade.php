@extends('admin.layouts.main')
@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>
        {{ Form::open(array('route' => $formAction, 'method' => $formMethod, 'class' => 'form-horizontal form-bordered', 'role' => 'form', 'id' => 'formInsertShortlink', 'enctype' => 'multipart/form-data')) }}
            <div class="box-body no-padding">
                <div class="form-group bg-default">
                    <label for="inputName" class="col-sm-2 control-label">Original URL</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" placeholder="Original URL" id="inputOriginalUrl" name="original_url" value="{{ $rowShortlink->original_url }}">
                    </div>
                </div>
                <div class="form-group bg-default">
                    <label for="inputName" class="col-sm-2 control-label">Custom Short URL</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" placeholder="Short Code" id="inputShortCode" name="short_url" value="{{ $rowShortlink->short_url }}">
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <a href="{{ URL::route('admin.shortlink.index') }}">
                            <button type="button" class="btn btn-default">Cancel</button>
                        </a>
                        <button type="reset" class="btn btn-default" onclick="$('#formInsertShortlink').bootstrapValidator('resetForm', true);">Reset</button>
                    </div>
                </div>
        </div>
        {{ Form::close() }}<!-- /.form -->
    </div><!-- /.box -->
@stop
@section('script')