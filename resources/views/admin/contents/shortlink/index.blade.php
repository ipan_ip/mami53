@extends('admin.layouts.main')
@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
            <div class="box-tools pull-right">
                <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <a href="{{ URL::route('admin.shortlink.create') }}">
                        <button type="submit" class="btn-add btn btn-primary btn-sm">
                            <i class="fa fa-plus"></i> Create Short Link
                        </button>
                    </a>
                </div>
            </div>

            <table id="tableListShortlink" class="table table-striped">
                <thead>
                    <tr>
                        <th width="30px;">No</th>
                        <th>Original URL</th>
                        <th>Short link</th>
                        <th class="table-action-column">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($rowsShortlink as $rowShortlinkKey => $rowShortlink)
                        <tr>
                            <td>{{ $rowShortlinkKey+1 }}</td>
                            <td>{{ $rowShortlink->original_url }}</td>
                            <td>{{ $baseUrl.$rowShortlink->short_url }}</td>
                            <td class="table-action-column">
                                <div class="btn-action-group">
                                    <a href="{{ URL::route('admin.shortlink.edit', $rowShortlink->id) }}">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    <a href="{{ url('admin/shortlink/destroy', $rowShortlink->id ) }}">
                                        <i class="fa fa-trash-o"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>No</th>
                        <th>Original URL</th>
                        <th>Short link</th>
                        <th class="table-action-column">Action</th>
                    </tr>
                </tfoot>
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
    <!-- table -->

    @include('admin.partials.delete-modal')
@stop
@section('script')
<!-- page script -->
<script type="text/javascript">
    $(function()
    {
        $("#tableListShortlink").dataTable();
    });
</script>
@stop