@extends('admin.layouts.main')

@section('style')
    <style>
        tr.report-row-description td {
            background: #f3f4f5;
        }

        tr.report-row-description td:first-child {
            padding-left: 30px;
        }

        .report-summary .box-header {
            min-height: 65px;
            text-align: center;
        }

        .report-summary .box-header .box-title {
            float: none;
        }

        .report-summary .box-body {
            font-size: 32px;
            font-weight: bold;
            text-align: center;
            padding-bottom: 15px;
        }

        .badge.badge-followed-up {
            background: #27ab27;
        }
    </style>
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>
    </div>

    <div class="row">
        <div class="col-md-2">
            <div class="box box-primary report-summary">
                <div class="box-header">
                    <h3 class="box-title">Laporan Foto</h3>
                </div>

                <div class="box-body no-padding">
                    {{ isset($summary['photo']) ? $summary['photo'] : 0 }}
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="box box-primary report-summary">
                <div class="box-header">
                    <h3 class="box-title">Laporan Alamat</h3>
                </div>

                <div class="box-body no-padding">
                    {{ isset($summary['address']) ? $summary['address'] : 0 }}
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="box box-primary report-summary">
                <div class="box-header">
                    <h3 class="box-title">Laporan Telepon</h3>
                </div>

                <div class="box-body no-padding">
                    {{ isset($summary['phone']) ? $summary['phone'] : 0 }}
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="box box-primary report-summary">
                <div class="box-header">
                    <h3 class="box-title">Laporan Harga</h3>
                </div>

                <div class="box-body no-padding">
                    {{ isset($summary['price']) ? $summary['price'] : 0 }}
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="box box-primary report-summary">
                <div class="box-header">
                    <h3 class="box-title">Laporan Fasilitas</h3>
                </div>

                <div class="box-body no-padding">
                    {{ isset($summary['facility']) ? $summary['facility'] : 0 }}
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="box box-primary report-summary">
                <div class="box-header">
                    <h3 class="box-title">Laporan Lainnya</h3>
                </div>

                <div class="box-body no-padding">
                    {{ isset($summary['other']) ? $summary['other'] : 0 }}
                </div>
            </div>
        </div>
    </div>

	<!-- table -->
    <div class="box box-primary">
        <div class="box-body no-padding">
            <table  class="table">
                <thead>
                    <tr>
                        <th>Tanggal</th>
                        <th>User</th>
                        <th>Email</th>
                        <th>Telp.</th>
                        <th>Jenis Laporan</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($reports as $key => $report)
                    <tr class="report-row-parent">
                        <td>{{ $report->created_at->format('Y-m-d H:i:s') }}</td>
                        <td>{{ $report->user->name }}</td>
                        <td>{{ $report->user->email }}</td>
                        <td>{{ $report->user->phone_number }}</td>
                        <td>
                            <span class="badge">{{ $report->report_type }}</span>&nbsp;
                            @foreach($report->children as $child)
                                <span class="badge">{{ $child->report_type }}</span>&nbsp;
                            @endforeach
                        </td>
                        <td>
                            @if($report->status == $report::REPORT_STATUS_NEW)
                                <a href="{{ route('admin.room.report.followedup', $report->id) }}">
                                    <i class="fa fa-check"></i>
                                </a>
                            @else
                                <span class="badge badge-followed-up">{{ $report->status }}</span>&nbsp;
                            @endif
                        </td>
                    </tr>
                    @if(!is_null($report->report_description) && $report->report_description != '')
                        <tr class="report-row-description">
                            <td colspan="6">
                                <p><strong>Deskripsi Laporan</strong></p>
                                <p>
                                    {{ $report->report_description }}
                                </p>
                                
                            </td>
                        </tr>
                    @endif
                @endforeach
                </tbody>
                
            </table>
        </div><!-- /.box-body -->
        <div class="box-body no-padding">
            {{ $reports->appends(Request::except('page'))->links() }}
        </div>
    </div><!-- /.box -->
    <!-- table -->
@endsection