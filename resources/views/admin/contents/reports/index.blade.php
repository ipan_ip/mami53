@extends('admin.layouts.main')

@section('content')
	<!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <form action="" class="form-inline" method="get" style="text-align:right;padding:10px;">
                        <select name="sort" class="form-control">
                            @foreach($sortTypes as $key => $sortType)
                                <option value="{{$key}}" {{ request()->get('sort') == $key ? 'selected="selected"': '' }}>{{ $sortType }}</option>
                            @endforeach
                        </select>
                        <input type="text" name="q" class="form-control input-sm"  placeholder="Nama Kost"  autocomplete="off" value="{{ request()->input('q') }}">
                        <button class="btn btn-primary btn-md" id="buttonSearch"><i class="fa fa-search">&nbsp;</i>Search</button>
                    </form>
                </div>
            </div>

            <table  class="table table-striped">
                <thead>
                    <tr>
                        <th>ID Kost</th>
                        <th>Nama Kost</th>
                        <th>Jumlah Laporan</th>
                        <th>Laporan Terakhir</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($rooms as $key => $room)
                    <tr>
                        <td>{{ $room->id }}</td>
                        <td>{{ $room->name }}</td>
                        <td>{{ $room->report_summary->count() }}</td>
                        <td>{{ \Carbon\Carbon::parse($room->report_summary->first()->created_at)->format('Y-m-d H:i:s') }}</td>
                        <td class="table-action-column">
                            <div class="btn-action-group">
                                <a href="{{ route('admin.room.report.show', $room->id) }}"><i class="fa fa-eye"></i></a>
                            </div>
                            
                        </td>
                    </tr> 
                @endforeach
                </tbody>
                
            </table>
        </div><!-- /.box-body -->
        <div class="box-body no-padding">
            {{ $rooms->appends(Request::except('page'))->links() }}
        </div>
    </div><!-- /.box -->
    <!-- table -->
@endsection