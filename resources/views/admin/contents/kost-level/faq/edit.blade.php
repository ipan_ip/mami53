@extends('admin.layouts.main')

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

        <form action="{{ URL::to('admin/kost-level/faq/' . $faq->id) }}" method="POST" class="form-horizontal form-bordered">
            <input type="hidden" name="_method" value="PUT">

            <div class="form-group bg-default">
                <label for="faq-question" class="control-label col-sm-2">Question</label>
                <div class="col-sm-10">
                        <input type="text" name="question" id="faq-question" class="form-control" value="{{ old('question', $faq->question) }}">
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="faq-answer" class="control-label col-sm-2">Answer</label>
                <div class="col-sm-10">
                    <input type="text" name="answer" id="faq-answer" class="form-control" value="{{ old('answer', $faq->answer) }}">
                </div>
            </div>
            
            <div class="form-group bg-default">
                    <div class="col-sm-10 col-sm-push-2">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
