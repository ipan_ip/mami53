@extends('admin.layouts.main')

@section('content')
	<div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default form-inline">
                    <div class="col-md-6 bg-default" style="padding-bottom:10px;">
                        <a href="{{ URL::to('admin/kost-level/faq/create#kost-level') }}" class="btn btn-primary">
                            <i class="fa fa-plus">&nbsp;</i>Add Level FAQ
                        </a>
                    </div>
                    <div class="col-md-6 bg-default" style="padding-bottom:10px;text-align:right;">
                        <form action="" method="GET">
                            <input type="text" name="q" class="form-control"  placeholder="Search"  autocomplete="off" value="{{ request()->input('q') }}">
                            <button type="submit" class="btn btn-primary" id="buttonSearch">
                                <i class="fa fa-search">&nbsp;</i>Search
                            </button>
                        </form>
                    </div>
                </div>
            </div>

            <table  class="table table-striped">
                <thead>
                    <tr>
                        <th>Question</th>
                        <th>Answer</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($faq as $faqItem)
                    <tr>
                        <td>{{ $faqItem->question }}</td>
                        <td>{{ $faqItem->answer }}</td>
                        <td>
                            <a href="{{ URL::to('admin/kost-level/faq/' . $faqItem->id . '/edit#kost-level') }}" title="Edit">
                                <i class="fa fa-pencil"></i>
                            </a>
                            <form action="{{ URL::to('admin/kost-level/faq/' . $faqItem->id) }}" method="POST" style="display: inline;">
                                <input type="hidden" name="_method" value="DELETE">
                                <a href="#" onclick="if(!confirm('Are you sure want to delete FAQ {{ $faqItem->question }}?')) return false; else $(this).parent().submit();" title="Delete">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </form>
                        </td>
                    </tr> 
                @endforeach
                </tbody>
            </table>
        </div>

        <div class="box-body no-padding">
            <div class="col-md-12 bg-default">
                {{ $faq->appends(Request::except('page'))->links() }}
            </div>
        </div>
    </div>

    <script>
        
    </script>
@endsection
