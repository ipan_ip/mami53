@extends('admin.layouts.main')

@section('style')
<style>
    /* Select2 tweak */
    .input-group .select2-container,
    .form-group .select2-container {
        position: relative !important;
        z-index: 2;
        float: left !important;
        width: 100% !important;
        margin-bottom: 0 !important;
        display: table !important;
        table-layout: fixed !important;
    }

    .select2-container .select2-search__field {
        width: 100% !important;
    }

    /* Sweetalert */
    .swal2-container {
        z-index: 99999 !important;
    }

    .swal-shown {
        height: auto !important;
    }

    .custom-swal {
        z-index: 10000 !important;
    }

    .custom-wide-swal {
        width: 480px !important;
        font-size: 12px !important;
    }
</style>
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" />
<link href="//cdn.jsdelivr.net/npm/select2-bootstrap-theme@0.1.0-beta.10/dist/select2-bootstrap.min.css"
    rel="stylesheet" />
<link rel="stylesheet" href="/assets/vendor/summernote/summernote.css?v=05042018">
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

        <form action="#" method="POST" class="form-horizontal form-bordered">
            <input type="hidden" name="_method" value="PUT">

            <div class="box-body no-padding">
                <div class="form-group bg-default">
                    <label for="level-name" class="control-label col-sm-2">Level Name</label>
                    <div class="col-sm-10">
                        <input type="text" name="name" id="level-name" class="form-control" value="{{ old('name', $level->name) }}">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="level-key" class="control-label col-sm-2">Key</label>
                    <div class="col-sm-10">
                        <input type="text" name="key" id="level-key" class="form-control" value="{{ old('key', $level->key) }}">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="level-order" class="control-label col-sm-2">Order</label>
                    <div class="col-sm-2">
                        <select name="order" id="level-order" class="form-control">
                            @foreach ($order as $availOrder)
                            <option value="{{ $availOrder }}" @if (old('order', $level->order) == $availOrder) selected="true" @endif>{{ $availOrder }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="level-regular" class="control-label col-sm-2">Is Regular Level</label>
                    <div class="col-sm-2">
                        <select name="is_regular" id="level-regular" class="form-control">
                            <option value="0" @if (old('is_regular', $level->is_regular) == '0') selected="true" @endif>No</option>
                            <option value="1" @if (old('is_regular', $level->is_regular) == '1') selected="true" @elseif (!$isRegularAvail) disabled @endif>Yes</option>
                        </select>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="level-partner" class="control-label col-sm-2">Is Partner Level</label>
                    <div class="col-sm-2">
                        <select name="is_partner" id="level-partner" class="form-control">
                            <option value="0" @if (old('is_partner', $level->is_partner) == '0') selected="true" @endif>No</option>
                            <option value="1" @if (old('is_partner', $level->is_partner) == '1') selected="true" @endif>Yes</option>
                        </select>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="has-value" class="control-label col-sm-2">Set Level Values?</label>
                    <div class="col-sm-2">
                        <select name="has_value" id="has-value" class="form-control">
                            <option value="0" @if (old('has_value', $level->has_value) == '0') selected="true" @endif>No</option>
                            <option value="1" @if (old('has_value', $level->has_value) == '1') selected="true" @endif>Yes</option>
                        </select>
                    </div>
                </div>

                <div id="hidden-level-values" class="form-group bg-default hidden">
                    <label for="level_values[]" class="control-label col-sm-2">Level Values</label>
                    <div class="col-sm-10 col-md-6">
                        <select name="level_values[]" class="form-control level-values" multiple="multiple"></select>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="level-hidden" class="control-label col-sm-2">Status</label>
                    <div class="col-sm-2">
                        <select name="is_hidden" id="level-hidden" class="form-control">
                            <option value="0" @if (old('is_hidden', $level->is_hidden) == '0') selected="true" @endif>Show</option>
                            <option value="1" @if (old('is_hidden', $level->is_hidden) == '1') selected="true" @endif>Hidden</option>
                        </select>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="level-notes" class="control-label col-sm-2">Notes</label>
                    <div class="col-sm-10">
                        <input type="text" name="notes" id="level-notes" class="form-control" value="{{ old('notes', $level->notes) }}">
                    </div>
                </div>

                <div class="bg-default">
                    <div class="form-group row">
                        <div class="control-label col-sm-2"><b>Benefits</b></div>
                        <div class="col-sm-10">
                            <button type="button" onclick="addItem(this);" class="btn btn-success">Add Benefit</button>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-2">&nbsp;</div>
                        <div class="col-sm-9">
                            <input type="text" name="benefits[]" class="form-control" value="{{ old('benefits.0', optional($level->benefits->first())->text) }}">
                            <input type="hidden" name="benefit_id[]" value="{{ old('benefit_id.0', optional($level->benefits->first())->id) }}">
                        </div>
                        <div class="col-sm-1">
                            <a href="#" onclick="removeItem(this);" title="Remove" style="opacity:0.5;pointer-events:none;">
                                <i class="fa fa-trash fa-2x"></i>
                            </a>
                        </div>
                    </div>
                    @if (count($level->benefits) > 1)
                        @php
                            $benefits = $level->benefits->toArray();
                            array_shift($benefits);
                        @endphp
                        @foreach ($benefits as $benefit)
                            <div class="form-group row">
                                <div class="col-sm-2">&nbsp;</div>
                                <div class="col-sm-9">
                                    <input type="text" name="benefits[]" class="form-control" value="{{ $benefit['text'] }}">
                                    <input type="hidden" name="benefit_id[]" value="{{ $benefit['id'] }}">
                                </div>
                                <div class="col-sm-1">
                                    <a href="#" onclick="removeItem(this);" title="Remove">
                                        <i class="fa fa-trash fa-2x"></i>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>

                <div class="bg-default">
                    <div class="form-group row">
                        <div class="control-label col-sm-2"><b>Criterias</b></div>
                        <div class="col-sm-10">
                            <button type="button" onclick="addItem(this);" class="btn btn-success">Add Criteria</button>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-2">&nbsp;</div>
                        <div class="col-sm-9">
                            <input type="text" name="criterias[]" class="form-control" value="{{ old('criterias.0', optional($level->criterias->first())->text) }}">
                            <input type="hidden" name="criteria_id[]" value="{{ old('criteria_id.0', optional($level->criterias->first())->id) }}">
                        </div>
                        <div class="col-sm-1">
                            <a href="#" onclick="removeItem(this);" title="Remove" style="opacity:0.5;pointer-events:none;">
                                <i class="fa fa-trash fa-2x"></i>
                            </a>
                        </div>
                    </div>
                    @if (count($level->criterias) > 1)
                        @php
                            $criterias = $level->criterias->toArray();
                            array_shift($criterias);
                        @endphp
                        @foreach ($criterias as $criteria)
                            <div class="form-group row">
                                <div class="col-sm-2">&nbsp;</div>
                                <div class="col-sm-9">
                                    <input type="text" name="criterias[]" class="form-control" value="{{ $criteria['text'] }}">
                                    <input type="hidden" name="criteria_id[]" value="{{ $criteria['id'] }}">
                                </div>
                                <div class="col-sm-1">
                                    <a href="#" onclick="removeItem(this);" title="Remove">
                                        <i class="fa fa-trash fa-2x"></i>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>

                <div class="form-group bg-default">
                    <label for="level-fee" class="control-label col-sm-2">Charging Fee</label>
                    <div class="col-sm-2">
                        <input step="any" type="number" min="0" name="charging_fee" id="level-count" class="form-control" value="{{ (float) old('charging_fee', $level->charging_fee) }}">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="level-fee-type" class="control-label col-sm-2">Charging Type</label>
                    <div class="col-sm-2">
                        <select name="charging_type" class="form-control">
                            <option value="percentage"  @if($level->charging_type == 'percentage') selected @endif>Percentage</option>
                            <option value="amount"  @if($level->charging_type == 'amount') selected @endif>Amount</option>
                        </select>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="level-charging-rule" class="control-label col-sm-2">Charging Rules For</label>
                    <div class="col-sm-6">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" name="charge_for_booking" type="checkbox" value=1 @if($level->charge_for_booking) checked @endif>
                            <label class="form-check-label">Booking Contract</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" name="charge_for_consultant" type="checkbox" value=1 @if($level->charge_for_consultant) checked @endif>
                            <label class="form-check-label">Consultant Contract</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" name="charge_for_owner" type="checkbox" value=1 @if($level->charge_for_owner) checked @endif>
                            <label class="form-check-label">Owner Contract</label>
                        </div>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="level-invoice-type" class="control-label col-sm-2">Charging Invoice Type</label>
                    <div class="col-sm-2">
                        <select name="charge_invoice_type" class="form-control">
                            <option value="all" @if($level->charge_invoice_type == 'all') selected @endif>All</option>
                            <option value="per_contract" @if($level->charge_invoice_type == 'per_contract') selected @endif>Per-Contract</option>
                            <option value="per_tenant" @if($level->charge_invoice_type == 'per_tenant') selected @endif>Per-Tenant</option>
                        </select>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="level-tnc" class="control-label col-sm-2">Terms & Conditions</label>
                    <div class="col-sm-10">
                        <textarea name="tnc" id="level-tnc" class="form-control">{{ old('tnc', $level->tnc) }}</textarea>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="level-count" class="control-label col-sm-2">Join Count</label>
                    <div class="col-sm-2">
                        <input type="number" min="0" name="join_count" id="level-count" class="form-control" value="{{ (int) old('join_count', $level->join_count) }}">
                    </div>
                </div>

                {{-- {{ dd($level->room_level->id) }} --}}

                <div class="form-group bg-default">
                    <label for="room-level" class="control-label col-sm-2">Room Level</label>
                    <div class="col-sm-2">
                        <select name="room_level_id" id="room-level" class="form-control">
                            <option value="" disabled>Choose room level</option>
                            @foreach($roomLevels as $roomLevel)
                                <option value="{{ $roomLevel->id }}" {{ !is_null($level->room_level) && $level->room_level->id === $roomLevel->id ? 'selected' : '' }}>{{ $roomLevel->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <div class="col-sm-10 col-sm-push-2">
                        <button id="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('script')
<script src="/assets/vendor/summernote/summernote.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.js"></script>

<script type="text/javascript">

    function triggerAlert(type, message) {
        Swal.fire({
            type: type,
            customClass: {
                container: 'custom-swal'
            },
            html: message
        });
    }

    function triggerLoading(message) {
        Swal.fire({
            title: message,
            customClass: {
                container: 'custom-swal'
            },
            allowEscapeKey: false,
            allowOutsideClick: false,
            onOpen: () => {
                swal.showLoading();
            }
        });
    }

    function addItem(el)
    {
        var parentClonedItem = $(el).parent().parent().parent();
        var itemToClone = $(el).parent().parent().next();
        var clonedItem = itemToClone.clone();
        clonedItem.find('input').first().val('');
        clonedItem.find('input').first().next().val('');
        clonedItem.find('a').first().removeAttr('style');
        clonedItem.appendTo(parentClonedItem);
    }

    function removeItem(el)
    {
        $(el).parent().parent().remove();
    }

    // DOM events
    $(function() {

        function initiateLandingSelector($selected) {
            $('.level-values').select2({
                theme: "bootstrap",
                placeholder: 'Type to search..',
                language: {
                    searching: function() {
                        return "Fetching Level Values..";
                    },
                    inputTooShort: function() {
                        return 'Type to search..';
                    },
                    noResults: function(){
                        return "Level Values not found!";
                    }
                },
                ajax: {
                    url: '/admin/kost-value/values',
                    data: function (params) {
                        return {
                            search: params.term,
                            page: params.page || 1
                        }
                    },
                    processResults: function (data, params) {
                        var page = params.page || 1;
                        if (data.data.length < 1) var total_count = 0;
                        else var total_count = data.data[0].total_count;
                        return {
                            results: $.map(data.data, item => { 
                                return {
                                    id: item.id, 
                                    text: item.name
                                }
                            }),
                            pagination: {
                                more: (page * 10) <= total_count
                            }
                        };
                    },
                    cache: true
                },
                closeOnSelect: false,
                multiple:true
            });
        }

        function cleanUpSelector() {
            $('.level-values').val("").trigger('change');
        }

        $('#level-tnc').summernote({
            height: 200,
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', []],
                ['color', ['color']],
                ['para', ['ul', 'ol']],
                ['insert', ['link']],
                ['misc', ['codeview']]
            ]
        });

        // Dropdown "Set Level Values?" on-change event
        $('#has-value').on('change', (e) => {
            let val = $(e.target).val();
            let hiddenDiv = $('#hidden-level-values');
            
            if (val == 1) {
                if (hiddenDiv.hasClass('hidden')) {
                    hiddenDiv.removeClass('hidden');
                }
            } else {
                if (!hiddenDiv.hasClass('hidden')) {
                    hiddenDiv.addClass('hidden');
                    cleanUpSelector();
                }
            }
        });

        // Submit event
        $('#submit').click((e) => {
            e.preventDefault();

            let dataObject = $('form').serialize();

            Swal.fire({
                type: 'question',
                customClass: {
                    container: 'custom-swal'
                },
                title: 'Save Kost Level data?',
                showCancelButton: true,
                confirmButtonText: 'Yes, save it',
                showLoaderOnConfirm: true,
                preConfirm: () => {
                    return $.ajax({
                        type: 'POST',
                        url: "/admin/kost-level/level/{{ $level->id }}",
                        dataType: 'json',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: dataObject
                    });
                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                if (!result.dismiss) {
                    let response = result.value;

                    if (response.success !== true) {
                        if (typeof response.message === 'object') {
                            let messages = "";

                            $.each(response.message, (key, value) => {
                                messages += value + "<br/>";
                            });

                            response.message = messages;
                        }

                        triggerAlert('error', '<h5>' + response.message + '</h5>');
                    } else {
                        Swal.fire({
                            type: 'success',
                            customClass: {
                                container: 'custom-swal'
                            },
                            title: response.message,
                            onClose: () => {
                                window.location.replace("{{ url('admin/kost-level/level') }}");
                            }
                        });
                    }

                }
            });
        });

        // Initialize dropdown "Level Values"
        initiateLandingSelector();

        // Initialize hidden div
        @if ($level->has_value == '1')
            let hiddenDiv = $('#hidden-level-values');

            if (hiddenDiv.hasClass('hidden')) {
                hiddenDiv.removeClass('hidden');
            }

            // Assign dropdown values
            @foreach ($level->values as $value)
                var option = new Option("{{ $value->name }}", {{ $value->id }}, true, true);
                $('.level-values').append(option).trigger('change');
            @endforeach
        @endif
    })
</script>
@endsection
