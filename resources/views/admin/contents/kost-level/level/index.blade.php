@extends('admin.layouts.main')

<style>
    #text-on-mid > th {
        vertical-align: middle;
        text-align: center;
    }

    table, th {
        border: 2px solid #ddd;
    }
</style>

@section('content')
	<div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default form-inline">
                    <div class="col-md-6 bg-default" style="padding-bottom:10px;">
                        <a href="{{ URL::to('admin/kost-level/level/create#kost-level') }}" class="btn btn-primary">
                            <i class="fa fa-plus">&nbsp;</i>Add Kost Level
                        </a>
                    </div>
                    <div class="col-md-6 bg-default" style="padding-bottom:10px;text-align:right;">
                        <form action="" method="GET">
                            <input type="text" name="q" class="form-control"  placeholder="Level Name"  autocomplete="off" value="{{ request()->input('q') }}">
                            <button type="submit" class="btn btn-primary" id="buttonSearch">
                                <i class="fa fa-search">&nbsp;</i>Search
                            </button>
                        </form>
                    </div>
                </div>
            </div>

            <table  class="table table-striped" style="font-size: 12px !important;">
                <thead>
                    <tr id="text-on-mid">
                        <th rowspan="2">ID</th>
                        <th rowspan="2">Level Name</th>
                        <th rowspan="2">Key</th>
                        <th rowspan="2">Benefit</th>
                        <th rowspan="2">Criteria</th>
                        <th rowspan="2">Status</th>
                        <th rowspan="2">Room Level</th>
                        <th colspan="2">Charging</th>
                        <th colspan="3">Charge for Contract</th>
                        <th rowspan="2">Invoice Type</th>
                        <th rowspan="2">Values</th>
                        <th rowspan="2">Notes</th>
                        <th rowspan="2">Actions</th>
                    </tr>
                    <tr>
                        <th>Fee</th>
                        <th>Type</th>
                        <th>Booking</th>
                        <th>Consultant</th>
                        <th>Owner</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($level as $levelItem)
                    <tr>
                        <td>{{ $levelItem->id }}</td>
                        <td>
                            {{ $levelItem->name }}
                            @if ($levelItem->is_regular)
                                &nbsp;<span class="label label-success">Regular</span>
                            @endif
                            @if ($levelItem->is_partner)
                                &nbsp;<span class="label label-success">Partner</span>
                            @endif
                        </td>
                        <td>
                            {{ $levelItem->key }}
                        </td>
                        <td>
                            @if (!count($levelItem->benefits))
                                -
                            @else
                                <ul>
                                @foreach ($levelItem->benefits as $benefit)
                                    <li>{{ $benefit->text }}</li>
                                @endforeach
                                </ul>
                            @endif
                        </td>
                        <td>
                            @if (!count($levelItem->criterias))
                                -
                            @else
                                <ul>
                                @foreach ($levelItem->criterias as $criteria)
                                    <li>{{ $criteria->text }}</li>
                                @endforeach
                                </ul>
                            @endif
                        </td>
                        <td>
                            @if ($levelItem->is_hidden)
                                Hide
                            @else
                                Show
                            @endif
                        </td>
                        <td>{{ $levelItem->room_level ? $levelItem->room_level->name : '-' }}</td>
                        <td>{{ $levelItem->charging_fee }}</td>
                        <td>{{ $levelItem->charging_type }}</td>
                        <td>@if($levelItem->charge_for_booking) yes @else no @endif</td>
                        <td>@if($levelItem->charge_for_consultant) yes @else no @endif</td>
                        <td>@if($levelItem->charge_for_owner) yes @else no @endif</td>
                        <td>{{$levelItem->charge_invoice_type}}</td>
                        <td>
                            @if($levelItem->has_value) 
                                @foreach ($levelItem->values as $value)
                                    <p><small><span class="label label-primary">{{ $value->name }}</span></small></p>
                                @endforeach
                            @endif
                        </td>
                        <td>{{ $levelItem->notes }}</td>
                        <td>
                            <a href="{{ URL::to('admin/kost-level/level/' . $levelItem->id . '/edit#kost-level') }}" title="Edit">
                                <i class="fa fa-pencil"></i>
                            </a>
                            <form action="{{ URL::to('admin/kost-level/level/' . $levelItem->id) }}" method="POST" style="display: inline;">
                                <input type="hidden" name="_method" value="DELETE">
                                <a href="#" onclick="if(!confirm('Are you sure want to delete level {{ $levelItem->name }}?')) return false; else $(this).parent().submit();" title="Delete">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </form>
                        </td>
                    </tr> 
                @endforeach
                </tbody>
            </table>
        </div>

        <div class="box-body no-padding">
            <div class="col-md-12 bg-default">
                {{ $level->appends(Request::except('page'))->links() }}
            </div>
        </div>
    </div>

    <script>
        
    </script>
@endsection
