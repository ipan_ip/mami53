@extends('admin.layouts.main')

@section('content')
	<div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default form-inline">
                    <div class="col-md-12 bg-default" style="padding-bottom:10px;">
                        @php
                            $userOwner = optional($kost->owners->first())->user;
                            $ownerName = '';
                            if (!is_null($userOwner)) {
                                $ownerName = $userOwner->name;
                            }
                        @endphp
                        <input type="text" class="form-control" value="{{ $kost->name }}" disabled>
                        <input type="text" class="form-control" value="{{ $ownerName }}" disabled>
                    </div>
                </div>
            </div>

            <table  class="table table-striped">
                <thead>
                    <tr>
                        <th>Log</th>
                        <th>Actor</th>
                        <th>Created</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($levelHistory as $historyItem)
                    <tr>
                        @php
                            if (is_null($historyItem->kost_level)) {
                                continue;
                            }
                            $log = $historyItem->action;
                            if (!is_null($log) && $log != 'remove_flag') {
                                $log = ucfirst($log);
                                if ($historyItem->is_force) {
                                    $log = 'Force ' . $log;
                                }
                            } else {
                                $log = 'Flag';
                                if ($historyItem->flag_reminder) {
                                    $log .= ' Reminder';
                                } elseif ($historyItem->action == 'remove_flag') {
                                    $log = 'Remove ' . $log;
                                }
                                $flag = $historyItem->flag;
                                if ($flag > 1) {
                                    $log .= ' Downgraded';
                                } elseif ($flag) {
                                    $log .= ' Upgrade';
                                } else {
                                    $log .= ' Downgrade';
                                }
                            }
                            $log .= ' to ' . $historyItem->kost_level->name;
                        @endphp
                        <td>{{ $log }}</td>
                        @php
                            $actor = $historyItem->user;
                            if (!is_null($actor)) {
                                $actorName = $actor->name;
                                if ($actorName == $ownerName) {
                                    $actorName = 'Owner';
                                }
                            } else {
                                $actorName = 'System';
                            }
                        @endphp
                        <td>{{ $actorName }}</td>
                        <td>{{ $historyItem->created_at }}</td>
                    </tr> 
                @endforeach
                </tbody>
            </table>
        </div>

        <!--div class="box-body no-padding">
            <div class="col-md-12 bg-default">
                {{-- $levelHistory->appends(Request::except('page'))->links() --}}
            </div>
        </div-->
    </div>

    <script>
        
    </script>
@endsection
