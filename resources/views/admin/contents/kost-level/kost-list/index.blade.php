@extends('admin.layouts.main')

@section('content')
	<div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default form-inline">
                    <div class="col-md-3 bg-default" style="padding-bottom:10px;">
                        <a href="{{ URL::to('admin/kost-level/kost-list/upload#kost-level') }}" class="btn btn-primary">
                            <i class="fa fa-file">&nbsp;</i>Upload CSV
                        </a>
                    </div>
                    <div class="col-md-9 bg-default" style="padding-bottom:10px;text-align:right;">
                        <form action="" method="GET">
                            <input type="text" name="kost-name" class="form-control"  placeholder="Kost Name"  autocomplete="off" value="{{ request()->input('kost-name') }}">
                            <input type="text" name="owner-name" class="form-control"  placeholder="Owner Name"  autocomplete="off" value="{{ request()->input('owner-name') }}">
                            <input type="number" name="owner-phone" class="form-control"  placeholder="Owner Phone Number"  autocomplete="off" value="{{ request()->input('owner-phone') }}">
                            <select name="level-id" class="form-control">
                                <option value="0">All Level</option>
                                <option value="100" @if (request()->input('level-id') == 100) selected="true" @endif>Unassigned</option>
                                @foreach ($kostLevelList as $levelItem)
                                    <option value="{{ $levelItem->id }}" @if (request()->input('level-id') == $levelItem->id) selected="true" @endif>{{ $levelItem->name }}</option>
                                @endforeach
                            </select>
                            <button type="submit" class="btn btn-primary" id="buttonSearch">
                                <i class="fa fa-search">&nbsp;</i>Search
                            </button>
                        </form>
                    </div>
                </div>
            </div>

            <table  class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Kost Name</th>
                        <th>Owner Name</th>
                        <th>Owner Phone Number</th>
                        <th>Level</th>
                        <th>Flag</th>
                        <th>Last Update</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($kost as $kostItem)
                    <tr>
                        @php
                            $owners = $kostItem->owners;
                            if (!count($owners)) {
                                $owner = [
                                    'name' => '',
                                    'phone_number' => '',
                                    'hostility' => ''
                                ];
                            } else {
                                $owner = $owners->first()->user->toArray();
                            }
                        @endphp
                        <td>{{ $kostItem->song_id }}</td>
                        <td>
                            {{ $kostItem->name }}
                            @if ($owner['hostility'])
                                <label class="label label-danger">Hostile</label>
                            @endif
                        </td>
                        <td>{{ $owner['name'] }}</td>
                        <td>{{ $owner['phone_number'] }}</td>
                        @php
                            $level = $kostItem->level;
                            if (!count($level)) {
                                $kostLevelName = 'Unassigned';
                                $flagLevelStatus = '-';
                                $lastUpdateLevel = '-';
                            } else {
                                $kostLevelName = $level->first()->name;
                                $flagLevelId = $level->first()->pivot->flag_level_id;
                                if ($flagLevelId) {
                                    $flagLevelStatus = 'Downgrade';
                                    if ($level->first()->pivot->flag > 1) {
                                        $flagLevelStatus = 'Downgraded';
                                    } elseif ($level->first()->pivot->flag) {
                                        $flagLevelStatus = 'Upgrade';
                                    }
                                    $flagLevelStatus .= ' ' . $kostLevel[$flagLevelId]->name;
                                } else {
                                    $flagLevelStatus = '-';
                                }
                                $lastUpdateLevel = $level->first()->pivot->updated_at;
                            }
                        @endphp
                        <td>{{ $kostLevelName }}</td>
                        <td>{{ $flagLevelStatus }}</td>
                        <td>{{ $lastUpdateLevel }}</td>
                        <td>
                            <a href="{{ URL::to('admin/kost-level/kost-list/' . $kostItem->song_id . '/edit#kost-level') }}">
                                Edit Kost Level
                            </a>
                            &nbsp;-&nbsp;
                            <a href="{{ URL::to('admin/kost-level/room-list/' . $kostItem->song_id .'#kost-level') }}">
                                Room List
                            </a>
                            &nbsp;-&nbsp;
                            <a href="{{ URL::to('admin/kost-level/kost-list/' . $kostItem->song_id . '/history#kost-level') }}">
                                History
                            </a>
                        </td>
                    </tr> 
                @endforeach
                </tbody>
            </table>
        </div>

        <div class="box-body no-padding">
            <div class="col-md-12 bg-default">
                {{ $kost->appends(Request::except('page'))->links() }}
            </div>
        </div>
    </div>

    <script>
        
    </script>
@endsection
