@extends('admin.layouts.main')

@section('content')
    <script>
        @if(Session::get('record_updated'))
            if (confirm('Kost successfully updated. Do you want to assign the Room Level?')) {
                window.location.href = '{{ url("/admin/kost-level/room-list/$kost->song_id#kost-level") }}'
            } else {
                window.location.href = '{{ url("/admin/kost-level/kost-list#kost-level") }}'
            }
        @endif
    </script>

    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

        <form action="{{ URL::to('admin/kost-level/kost-list/' . $kost->song_id) }}" method="POST" class="form-horizontal form-bordered">
            <input type="hidden" name="_method" value="PUT">
            
            <div class="box-body no-padding">
                <div class="form-group bg-default">
                    <label for="kost-name" class="control-label col-sm-2">Kost Name</label>
                    <div class="col-sm-10">
                        <input type="text" id="kost-name" class="form-control" value="{{ $kost->name }}" disabled>
                    </div>
                </div>

                @php
                    $userOwner = optional($kost->owners->first())->user;
                    $ownerName = '';
                    if (!is_null($userOwner)) {
                        $ownerName = $userOwner->name;
                    }
                @endphp
                <div class="form-group bg-default">
                    <label for="owner-name" class="control-label col-sm-2">Owner Name</label>
                    <div class="col-sm-10">
                        <input type="text" id="owner-name" class="form-control" value="{{ $ownerName }}" disabled>
                    </div>
                </div>

                @php
                    $level = $kost->level;
                    if (!count($level)) {
                        $flagLevelStatus = '-';
                        $lastUpdateLevel = '-';
                    } else {
                        $flagLevelId = $kost->level->first()->pivot->flag_level_id;
                        if ($flagLevelId) {
                            $flagLevelStatus = 'Downgrade';
                            if ($kost->level->first()->pivot->flag > 1) {
                                $flagLevelStatus = 'Downgraded';
                            } elseif ($kost->level->first()->pivot->flag) {
                                $flagLevelStatus = 'Upgrade';
                            }
                            $flagLevelStatus .= ' ' . $kostLevel[$flagLevelId]->name;
                        } else {
                            $flagLevelStatus = '-';
                        }
                        $lastUpdateLevel = $kost->level->first()->pivot->updated_at;
                    }
                @endphp
                <div class="form-group bg-default">
                    <label for="level-flag" class="control-label col-sm-2">Flag</label>
                    <div class="col-sm-8">
                        <input type="text" id="level-flag" class="form-control" value="{{ $flagLevelStatus }}" disabled>
                        <span><em>Last Update on {{ $lastUpdateLevel }}</em></span>
                    </div>
                    <div class="col-sm-2">
                        <button type="button" class="btn btn-danger" @if ($flagLevelStatus == '-') disabled @else onclick="$('#form-remove-flag').submit();" @endif>Remove Flag</button>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="level-id" class="control-label col-sm-2">Level</label>
                    <div class="col-sm-10">
                        <select name="level" id="level-id" class="form-control">
                            @foreach ($kostLevelList as $levelItem)
                            <option value="{{ $levelItem->id }}" @if (old('level', optional($kost->level->first())->id) == $levelItem->id || ($levelItem->is_regular && is_null($kost->level->first()))) selected="true" @endif>{{ $levelItem->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                
                <div class="form-group bg-default">
                    <label for="is-charge-by-room" class="control-label col-sm-2">Charge by</label>
                    <div class="col-sm-10">
                        <select name="is_charge_by_room" id="is-charge-by-room" class="form-control">
                            <option value="0" @if (old('is_charge_by_room', $isChargeByRoom) == '0') selected="true" @endif>Kost</option>
                            <option value="1" @if (old('is_charge_by_room', $isChargeByRoom) == '1') selected="true" @endif>Room</option>
                        </select>
                        <span><em>Room charged by Kost Charging or Room Charging.</em></span>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="force-update" class="control-label col-sm-2">Force Update</label>
                    <div class="col-sm-10">
                        <input type="checkbox" name="is_force" id="force-update" class="form-control" value="1" @if (old('is_force', true) == '1') checked @endif>
                    </div>
                </div>
                
                <div class="form-group bg-default">
                    <div class="col-sm-10 col-sm-push-2">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </form>

        <form action="{{ URL::to('admin/kost-level/kost-list/' . $kost->song_id . '/removeFlag') }}" method="POST" id="form-remove-flag">
            <button type="submit" class="hidden">Remove Flag</button>
        </form>
    </div>
@endsection
