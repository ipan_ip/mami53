@extends('admin.layouts.main')

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

        <form action="{{ URL::to('admin/kost-level/kost-list/processCsv') }}" method="POST" enctype="multipart/form-data" class="form-horizontal form-bordered">
            <div class="box-body no-padding">
                <div class="form-group bg-default">
                    <label for="upload-csv" class="control-label col-sm-2">CSV File</label>
                    <div class="col-sm-10">
                        <input type="file" name="level_csv" id="upload-csv" accept=".csv" class="form-control" onchange="validateFile(this);">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <div class="col-sm-10 col-sm-push-2">
                        <button type="submit" class="btn btn-primary">Upload</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <script type="text/javascript">
        function validateFile(el)
        {
            var file = $(el).prop('files')[0];
            var filename = $(el).val();

            var maxSize =  1024 * 1024 * 1024;
            var size = file.size;
            if (size > maxSize) {
                $(el).val('');
                alert('Max filesize is 1MB.');
            }

            var allowedExt = 'csv';
            var extension = filename.split('.').pop();
            if (extension !== allowedExt) {
                $(el).val('');
                alert('Only CSV file allowed.');
            }
        }
    </script>
@endsection
