@extends('admin.layouts.main')

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

        <form action="{{ URL::route('admin.photobooth.store') }}" method="POST" class="form-horizontal form-bordered">
            <div class="box-body no-padding">
                <div class="form-group bg-default">
                    <label for="inputStartDate" class="col-sm-2 control-label">Tanggal Mulai</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" placeholder="Tanggal Mulai" id="inputStartDate" name="start_date"  value="{{ old('start_date') }}">
                    </div>
                </div>
                <div class="form-group bg-default">
                    <label for="inputEndDate" class="col-sm-2 control-label">Tanggal Selesai</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" placeholder="Tanggal Selesai" id="inputEndDate" name="end_date"  value="{{ old('end_date') }}">
                    </div>
                </div>
                <div class="form-group bg-default">
                    <label for="inputUniversity" class="col-sm-2 control-label">Universitas</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" placeholder="Universitas" id="inputUniversity" name="university"  value="{{ old('university') }}">
                    </div>
                </div>
                <div class="form-group bg-default">
                    <label for="inputQuota" class="col-sm-2 control-label">Quota</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" placeholder="Quota" id="inputQuota" name="quota"  value="{{ old('quota') }}">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
