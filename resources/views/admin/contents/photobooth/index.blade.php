@extends('admin.layouts.main')

@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div><!-- /.box-header -->

        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <a href="{{ URL::route('admin.photobooth.create') }}">
                        <button type="submit" class="btn-add btn btn-primary btn-sm">
                            <i class="fa fa-plus">&nbsp;</i>Tambah Photobooth Event</button>
                    </a>
                    <form action="" class="form-inline" method="get" style="text-align:right;padding:10px;">
                        <input type="text" name="q" class="form-control input-sm"  placeholder="University"  autocomplete="off" value="{{ request()->input('q') }}">
                        <button class="btn btn-primary btn-md" id="buttonSearch"><i class="fa fa-search">&nbsp;</i>Search</button>
                    </form>
                </div>
            </div>
        </div>

        <hr>

        <div class="box-body no-padding">

            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>University</th>
                        <th>Subscribers</th>
                        <th class="table-action-column" width="120px">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($photoboothEvents as $event)
                    <tr>
                        <td>{{ $event->id }}</td>
                        <td>{{ $event->start_date->format('Y-m-d') }}</td>
                        <td>{{ $event->end_date->format('Y-m-d') }}</td>
                        <td>{{ $event->university }}</td>
                        <td>{{ $event->subscriber_count }}</td>
                        <td class="table-action-column">
                            <div class="btn-action-group">
                                <a href="{{route('admin.photobooth.edit', $event->id)}}" title="edit">
                                    <i class="fa fa-pencil"></i>
                                </a>

                                <a href="{{route('admin.photobooth.subscribers', $event->id)}}" title="subscriber">
                                    <i class="fa fa-user"></i>
                                </a>                                
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="box-body no-padding">
                {{ $photoboothEvents->appends(Request::except('page'))->links() }}
            </div>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
    <!-- table -->
@endsection
