@extends('admin.layouts.main')
@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
            <div class="box-tools pull-right">
                <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">

            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <a href="{{ URL::route('admin.areas.create') }}">
                        <button type="submit" class="btn-add btn btn-primary btn-sm">
                            <i class="fa fa-plus">&nbsp;</i>Add Area</button>
                    </a>
                </div>
            </div>

            <table id="tableListTag" class="table table-striped">
                <thead>
                    <tr>
                        <th width="30px;">No</th>
                        <th>Type</th>
                        <th>Parent Name</th>
                        <th>Name</th>
                        <th>Ordering</th>
                        <th class="table-action-column">Action</th>
                    </tr>
                </thead>
                <tbody>
                  @foreach ($rowsArea AS $rowAreaKey => $rowArea )
                       <tr>
                        <td>{{ $rowAreaKey+1 }}</td>
                        <td>{{ $rowArea->type }}</td>
                        <td>{{ $rowArea->parent_name }}</td>
                        <td>{{ $rowArea->name }}</td>
                        <td>{{ $rowArea->ordering }}</td>
                        <td class="table-action-column">
                            <div class="btn-action-group">
                                <a href="{{ URL::route('admin.areas.edit', $rowArea->id) }}">
                                    <i class="fa fa-pencil"></i>
                                </a>

                                <a href="{{ URL::to('admin/areas/destroy', $rowArea->id) }}">
                                    <i class="fa fa-trash-o"></i>
                                </a>
                            </div>
                        </td>
                    </tr>
                  @endforeach
                </tbody>
                
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
    <!-- table -->

@stop
@section('script')
<!-- page script -->
<script type="text/javascript">
    $(function()
    {
        $("#tableListTag").dataTable();
    });
</script>
@stop