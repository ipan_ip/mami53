@extends('admin.layouts.main')
@section('content')
<div class="box box-primary">
  <div class="box-header">
      <h3 class="box-title">{{ $boxTitle }}</h3>
  </div><!-- /.box-header -->
  {{ Form::open(array('route' => $formAction, 'method' => $formMethod,
      'class' => 'form-horizontal form-bordered', 'role' => 'form', 'id' => 'formInsertArea', 'enctype' => 'multipart/form-data')) }}
      <div class="box-body no-padding">
            <!-- select -->
            <div class="form-group bg-default">
                <label for="inputSelectType" class="col-sm-2 control-label">Select Type</label>
                <div class="col-sm-10">
                    <select class="form-control" id="inputSelectType" name="type">
                        @foreach ($rowsAreaType as $rowAreaType)
                          @if ($rowsArea->type == $rowAreaType)
                          <option value="{{ $rowAreaType }}" selected="selected">{{ $rowAreaType }}</option>
                          @else
                          <option value="{{ $rowAreaType }}">{{ $rowAreaType }}</option>
                          @endif
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group bg-default">
              <label for="inputName" class="col-sm-2 control-label">Name</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" placeholder="Name"
                  id="inputName" name="name" value="{{ $rowsArea->name }}">
              </div>
            </div>

            <div class="form-group bg-default">
              <label for="inputParentName" class="col-sm-2 control-label">Parent Name</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" placeholder="Parent Name"
                  id="inputParentName" name="parent_name" value="{{ $rowsArea->parent_name }}">
              </div>
            </div>

            <div class="form-group bg-default">
              <label for="inputlatitude1" class="col-sm-2 control-label">Latitude 1</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" placeholder="Latitude 1"
                  id="inputlatitude1" name="latitude_1" value="{{ $rowsArea->latitude_1 }}">
              </div>
            </div>

            <div class="form-group bg-default">
              <label for="inputLongitude1" class="col-sm-2 control-label">Longitude 1</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" placeholder="Longitude 1"
                  id="inputLongitude1" name="longitude_1" value="{{ $rowsArea->longitude_1 }}">
              </div>
            </div>

            <div class="form-group bg-default">
              <label for="inputLatitude2" class="col-sm-2 control-label">Latitude 2</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" placeholder="Latitude 2"
                  id="inputLatitude2" name="latitude_2" value="{{ $rowsArea->latitude_2 }}">
              </div>
            </div>

            <div class="form-group bg-default">
              <label for="longitude2" class="col-sm-2 control-label">Longitude 2</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" placeholder="Longitude 2"
                  id="longitude2" name="longitude_2" value="{{ $rowsArea->longitude_2 }}">
              </div>
            </div>

            <div class="form-group bg-default">
              <label for="ordering" class="col-sm-2 control-label">Ordering</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" placeholder="Urutan"
                  id="ordering" name="ordering" value="{{ $rowsArea->ordering }}">
              </div>
            </div>
            
            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="{{ URL::route('admin.areas.index') }}">
                  <button type="button" class="btn btn-default">Cancel</button>
                </a>
                <button type="reset" class="btn btn-default"
                  onclick="$('#formInsertArea').bootstrapValidator('resetForm', true);">Reset</button>
              </div>
            </div>
      </div>
  {{ Form::close() }}<!-- /.form -->
</div><!-- /.box -->
@stop
@section('script')
<!-- page script -->
<script type="text/javascript">

    @if (Session::get('laravelValidatorJSON') === null)
      myGlobal.laravelValidator = null;
    @else
      myGlobal.laravelValidator = {{ Session::get('laravelValidatorJSON') }};
    @endif


    $(function()
    {
      myGlobal.baseUrl = "{{ url('') . '/' }}";

      $formInsertArea = $('#formInsertArea');

      $( "#inputSelectType" ).change(function() {
        $formInsertArea
          .bootstrapValidator('updateStatus', 'name', 'NOT_VALIDATED')
          .bootstrapValidator('validateField', 'name');
      });

      formStoreTagRules = {
          name: {
              message: 'The name is not valid',
              validators: {
                  notEmpty: {
                      message: 'The name is required and cannot be empty'
                  },
                  stringLength: {
                      min: 3,
                      max: 30,
                      message: 'The name must be more than 3 and less than 30 characters long'
                  },
                  // Send { username: 'its value', type: 'username' } to the back-end
                  remote: {
                      message: 'The name is available',
                      url: myGlobal.baseUrl + 'admin/tag/unique',
                      data: function(validator) {
                          return {
                              type : 'tag.name',
                              fields : {
                                name : validator.getFieldElements('name').val(),
                                type : validator.getFieldElements('type').val()
                              }
                          };
                      },
                  }
              }
          },
          type: {
              validators: {
                  notEmpty: {
                      message: 'The type is required and cannot be empty'
                  },
              }
          }
      };

      formUpdateTagRules = {
          name: {
              message: 'The name is not valid',
              validators: {
                  notEmpty: {
                      message: 'The name is required and cannot be empty'
                  },
                  stringLength: {
                      min: 1,
                      max: 30,
                      message: 'The name must be more than 1 and less than 30 characters long'
                  }
              }
          },
          type: {
              validators: {
                  notEmpty: {
                      message: 'The type is required and cannot be empty'
                  },
              }
          }
      };

      @if (Route::currentRouteName() === 'admin.tag.create')
        myGlobal.bootstrapValidatorDefaults.fields = formStoreTagRules;
      @elseif (Route::currentRouteName() === 'admin.tag.edit')
        myGlobal.bootstrapValidatorDefaults.fields = formUpdateTagRules;
      @endif


      $formInsertArea.bootstrapValidator(myGlobal.bootstrapValidatorDefaults);

      updateValidateStatus($formInsertArea.data('bootstrapValidator'), myGlobal.laravelValidator);

    });
</script>
@stop