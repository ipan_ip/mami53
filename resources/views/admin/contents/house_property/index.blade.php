@extends('admin.layouts.main')

@section('style')
    <style>
        tr.group-niche td,
        .text-niche {
            color: #c58112;
        }

    </style>
@endsection

@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">

            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">

                    <div class="row">
                        <div class="col-md-12" style="margin: 0px 10px;">
                            <form class="form-inline" action="" method="GET">
                                <div class="form-group">
                                    <label class="sr-only" for="exampleInputEmail3">Name</label>
                                    <input type="text" name="name" class="form-control" id="exampleInputEmail3" placeholder="Name" value="{{ request()->input('name') }}" />
                                </div>
                                <div class="form-group">
                                    <label class="sr-only" for="exampleInputPassword3">Phone number</label>
                                    <input type="text"  name="phone" class="form-control" id="exampleInputPassword3" placeholder="Phone number" value="{{ request()->input('phone') }}" />
                                </div>
                                <div class="form-group">
                                    <label class="sr-only" for="exampleInputPassword3">Agent name</label>
                                    <input type="text"  name="agent_name" class="form-control" id="exampleInputPassword3" placeholder="Agent name" value="{{ request()->input('agent_name') }}" />
                                </div>
                                <select name="status" class="form-control">
                                    @foreach ($status as $key => $value)
                                        <option value="{{ $key }}" @if (request()->input('status') == $key) selected="true" @endif>{{ $value }}</option>
                                    @endforeach
                                </select>
                                <select name="type" class="form-control">
                                    @foreach ($type as $key => $value)
                                        <option value="{{ $key }}" @if (request()->input('type') == $key) selected="true" @endif>{{ $value }}</option>
                                    @endforeach
                                </select>
                                <button type="submit" class="btn btn-default">Filter</button>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
            
            <div class="row" style=" padding: 10px;">
                <div class="col-md-12">
                    <a href="{{ URL::to('admin/house_property/create') }}" class="col-md-12 btn btn-success">Add new</a>
                </div>
            </div>
            
            <table id="tableListLanding" class="table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Phone</th>
                        <th>Song id</th>
                        <th>Name</th>
                        <th>Address</th>
                        <th>Status</th>
                        <th class="table-action-column" width="120px">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($villa as $rowLandingKey => $value)
                    <tr @if ($value->is_active == 1) style="background: #5cb85c; color: #ffffff;" @endif>
                        <td>{{ $value->id }}</td>
                        <td>
                            @if (!is_null($value->owner_phone) and strlen($value->owner_phone) > 0)
                                {{ $value->owner_phone }}
                            @else
                                {{ $value->manager_phone }}
                            @endif

                            @if ($value->is_verified_phone == 0)
                                <a href="{{ URL::to('admin/house_property/verify/phone', $value->id) }}" class="label label-success">verify now</a>
                            @endif
                        </td>
                        <td>{{ $value->song_id }}</td>
                        <td><strong>{{ ucwords($value->name) }}</strong> <br/>{{ $value->live_at }}</td>
                        <td>{{ $value->address }} <br/>
                            <strong>Area city: </strong> {{ $value->area_city }} <br/>
                            <strong>Subdistrict: </strong> {{ $value->area_subdistrict }}
                        </td>
                        <td>
                            @if ($value->is_active == 0)
                                <label class="label label-danger">Waiting verify</label>
                            @else
                                <label class="label label-default">Active</label>
                            @endif
                        </td>
                        <td>
                            <a href="/{{ $value->type == 'villa' ? 'sewa' : 'disewakan' }}/{{ $value->slug }}" target="_blank" class="btn btn-xs btn-warning">View</a>
                            @if ($value->is_active == 0)
                                <a href="/admin/house_property/verify/{{ $value->id }}" class="btn btn-xs btn-success">Set live</a>
                            @else
                                <a href="/admin/house_property/verify/{{ $value->id }}" class="btn btn-xs btn-danger">Not verified</a>
                            @endif
                            <a href="/admin/house_property/{{ $value->id }}/edit" class="btn btn-primary btn-xs">Edit</a>
                            <a href="/admin/house_property/card/{{ $value->id }}?type=house_property" class="btn btn-primary btn-xs">Cards</a>
                            <a href="/admin/house_property/reject/{{ $value->id }}?hidden-form=true" class="btn btn-danger btn-xs"><i class="fa fa-flag"></i></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="box-body no-padding">
                {{ $villa->appends(Request::except('page'))->links() }}
            </div>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
    <!-- table -->
@endsection
