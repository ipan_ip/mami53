@extends('admin.layouts.main')

@section('content')
    <div class="box box-primary">

        <div class="row">
            <div class="col-md-12" style="margin-top: 20px;">
                <ul>
                    @foreach ($reject_reason as $key => $value)
                        <li><strong>{{ $value->created_at->format("d M Y H:i") }}</strong> {{ $value->detail }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
        @if (!request()->has('hidden-form'))
        <form action="" method="POST" id="formInsertDesigner" class="form-horizontal form-bordered" role="form">  
            {{ csrf_field() }}        
            <div class="box-body no-padding">
                <div class="form-group bg-default">
                    <label for="inputName" class="col-sm-2 control-label">Reject reason</label>
                    <div class="col-sm-10">
                        <textarea type="text" class="form-control" placeholder="Reject reason"
                            id="inputName" name="reject_description" ></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-primary" style="width: 100%;">Save</button>
                    </div>
                </div>
            </div>
        </form><!-- /.form -->
        @endif
    </div><!-- /.box -->
@stop