<form id="addPhotosForm{{$mediaName}}"
      action="{{ url($url) }}"
      method="POST"
      class="dropzone">
    {{ csrf_field() }}
    <input type="hidden" name="media_type" value="{{$mediaType}}">
    <input type="hidden" name="room_type" value="property" />
    <input type="hidden" name="type" value="{{ $type }}" />
    <input type="hidden" name="propertyId" value="{{ $propertyId }}" />
    <input type="hidden" name="propertyName" value="{{ $propertyName }}" />
</form>
