@extends('admin.layouts.main')
@section('style')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/dropzone.css">
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Upload Image for property</h3>
        </div>
        <div class="form-horizontal form-bordered" id="formUploadPhotos">
            <div class="box-body no-padding">
                <div class="form-group bg-default">
                    <label for="addPhotosFormCover" class="col-sm-2 control-label">Foto Cover</label>
                    <div class="col-sm-4">
                        @if(isset($media['cover']))
                            @foreach($media['cover'] as $photo)
                                <div class="col-md-3">
                                    {{-- link_to(url('admin/media',$photo['id']),'Delete', array('class' => 'fa fa-times-circle')) --}}
                                    <a href="">
                                        <img class="img-responsive" src="{{ $photo['photo_url']['small'] }}" alt="">
                                    </a>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="addPhotosFormBuilding" class="col-sm-2 control-label">Foto Bangunan</label>
                    <div class="col-sm-6">
                        @include('admin.contents.house_property.card.form-upload', ['url' => 'admin/media', 'mediaName' => 'Building', 'type' => 'bangunan', 'mediaType' => 'style_photo', 'propertyId' => $propertyId, 'propertyName' => $propertyName ])
                    </div>
                    <div class="col-sm-4">
                        @if(isset($media['bangunan']))
                            @foreach($media['bangunan'] as $photo)
                                <div class="col-md-3">
                                    {{-- link_to(url('admin/media',$photo['id']),'Delete', array('class' => 'fa fa-times-circle')) --}}
                                    <a href="">
                                        <img class="img-responsive" src="{{ $photo['photo_url']['small'] }}" alt="">
                                    </a>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="addPhotosFormRoom" class="col-sm-2 control-label">Foto Kamar</label>
                    <div class="col-sm-6">
                        @include('admin.contents.house_property.card.form-upload', ['url' => 'admin/media', 'mediaName' => 'Room', 'type' => 'kamar', 'mediaType' => 'style_photo', 'propertyId' => $propertyId, 'propertyName' => $propertyName ])
                    </div>
                    <div class="col-sm-4">
                        @if(isset($media['kamar']))
                            @foreach($media['kamar'] as $photo)
                                <div class="col-md-3">
                                    {{-- link_to(url('admin/media',$photo['id']),'Delete', array('class' => 'fa fa-times-circle')) --}}
                                    <a href="">
                                        <img class="img-responsive" src="{{ $photo['photo_url']['small'] }}" alt="">
                                    </a>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="addPhotosFormBathRoom" class="col-sm-2 control-label">Foto Kamar mandi</label>
                    <div class="col-sm-6">
                        @include('admin.contents.house_property.card.form-upload', ['url' => 'admin/media', 'mediaName' => 'Bathroom', 'type' => 'kamar-mandi', 'mediaType' => 'style_photo', 'propertyId' => $propertyId, 'propertyName' => $propertyName ])
                    </div>
                    <div class="col-sm-4">
                        @if(isset($media['kamar-mandi']))
                            @foreach($media['kamar-mandi'] as $photo)
                                <div class="col-md-3">
                                    {{-- link_to(url('admin/media',$photo['id']),'Delete', array('class' => 'fa fa-times-circle')) --}}
                                    <a href="">
                                        <img class="img-responsive" src="{{ $photo['photo_url']['small'] }}" alt="">
                                    </a>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="addPhotosFormOther" class="col-sm-2 control-label">Foto Fasilitas lain</label>
                    <div class="col-sm-6">
                        @include('admin.contents.house_property.card.form-upload', ['url' => 'admin/media', 'mediaName' => 'Other', 'type' => 'lainnya', 'mediaType' => 'style_photo', 'propertyId' => $propertyId, 'propertyName' => $propertyName ])
                    </div>
                    <div class="col-sm-4">
                        @if(isset($media['lainnya']))
                            @foreach($media['lainnya'] as $photo)
                                <div class="col-md-3">
                                    {{-- link_to(url('admin/media',$photo['id']),'Delete', array('class' => 'fa fa-times-circle')) --}}
                                    <a href="">
                                        <img class="img-responsive" src="{{ $photo['photo_url']['small'] }}" alt="">
                                    </a>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>

            <div id="wrapper"></div>
            {!! Form::close() !!}

            <div class="row">
                <div class="col-md-12" style="margin: 10px;">
                    <a href="{{ $back }}" class="btn btn-medium btn-success" style="width: 100%;">Back to image list</a>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/dropzone.js"></script>
    <script>
        Dropzone.autoDiscover = false;

        $('#addPhotosFormCover').dropzone({
            paramName : "media",
            maxFilesize : 6,
            maxFiles : 1,
            accepFiles: '.jpg, .jpeg, .png, .bmp',
            dictDefaultMessage : 'Upload photo for cover',
            addRemoveLinks: true,
            success : function (file, response) {
                $('#wrapper input[name="cover[]"]').remove();
                $('#wrapper').append('<input type="hidden" name="cover[]" value="' +response.media.id+ '">');
            }
        });

        $('#addPhotosFormRound').dropzone({
            paramName : "media",
            maxFilesize : 8,
            maxFiles: 1,
            accepFiles: '.jpg, .jpeg, .png, .bmp',
            dictDefaultMessage : 'Upload photo for 360 photos',
            addRemoveLinks: true,
            success : function (file, response) {
                $('#wrapper input[name="round[]"]').remove();
                $('#wrapper').append('<input type="hidden" name="round[]" value="' +response.media.id+ '">');
            }
        });
        $('#addPhotosFormBuilding').dropzone({
            paramName : "media",
            maxFilesize : 6,
            accepFiles: '.jpg, .jpeg, .png, .bmp',
            dictDefaultMessage : 'Upload photo for building',
            success : function (file, response) {
                $('#wrapper').append('<input type="hidden" name="building[]" value="' +response.media.id+ '">');
            }
        });
        $('#addPhotosFormRoom').dropzone({
            paramName : "media",
            maxFilesize : 6,
            accepFiles: '.jpg, .jpeg, .png, .bmp',
            dictDefaultMessage : 'Upload photo for rooms',
            success : function (file, response) {
                $('#wrapper').append('<input type="hidden" name="room[]" value="' +response.media.id+ '">');
            }
        });
        $('#addPhotosFormBathroom').dropzone({
            paramName : "media",
            maxFilesize : 6,
            accepFiles: '.jpg, .jpeg, .png, .bmp',
            dictDefaultMessage : 'Upload photo for bathroom',
            success : function (file, response) {
                $('#wrapper').append('<input type="hidden" name="bathroom[]" value="' +response.media.id+ '">');
            }
        });
        $('#addPhotosFormOther').dropzone({
            paramName : "media",
            maxFilesize : 6,
            accepFiles: '.jpg, .jpeg, .png, .bmp',
            dictDefaultMessage : 'Upload photo for other facilities',
            success : function (file, response) {
                $('#wrapper').append('<input type="hidden" name="other[]" value="' +response.media.id+ '">');
            }
        });
    </script>
@endsection
