@extends('admin.layouts.main')
@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
            <div class="box-tools pull-right">
                <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <a href="">
                        <a href="/admin/house_property/card/{{ $propertyId }}/edit" class="btn-add btn btn-primary btn-sm">
                            <i class="fa fa-plus">&nbsp;</i>Add Card</button>
                        </a>
                    </a>
                </div>
            </div>

            <table id="tableListStyle" class="table table-striped">
                <thead>
                    <tr>
                        <th width="30px;">No</th>
                        <th>Content</th>
                        <th>Type</th>
                        <th>Description</th>
                        <th class="table-action-column" width="100px">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($media as $key => $value)
                    <tr>
                        <td>{{ $key+1 }}</td>
                        <td>
                            <?php $tempMedia = ($value ? $value->photo_url : null); ?>
                            @if ($tempMedia)
                                <img width="150" class="img-thumbnail" src="{{ $tempMedia['small'] }}" alt="">
                            @else
                                <img width="200" class="img-thumbnail" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjAiIGhlaWdodD0iNzciIHZpZXdCb3g9IjAgMCA2MCA3NyIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PGRlZnMvPjxyZWN0IHdpZHRoPSI2MCIgaGVpZ2h0PSI3NyIgZmlsbD0iI0VFRSIvPjxnPjx0ZXh0IHg9IjE0LjcxODc1IiB5PSIzOC41IiBzdHlsZT0iZmlsbDojQUFBO2ZvbnQtd2VpZ2h0Om5vcm1hbDtmb250LWZhbWlseTpBcmlhbCwgSGVsdmV0aWNhLCBPcGVuIFNhbnMsIHNhbnMtc2VyaWYsIG1vbm9zcGFjZTtmb250LXNpemU6MTFweDtkb21pbmFudC1iYXNlbGluZTpjZW50cmFsIj5JbWFnZTwvdGV4dD48L2c+PC9zdmc+" alt="Image [60x77]" style="width: 60px; height: 77px;">
                            @endif
                        </td>
                        <!-- <td>
                            <img width="60" class="img-thumbnail" src="http://hairclick.wejoin.us/std_api/standard-server-api/public/uploads/cache/data/user/2014-08-25/XgbV2Uf2-120x160.jpg" alt="">
                        </td> -->
                        <td>{{ $value->type }}</td>
                        <td>
                            @if ($value->house_property->cover_id == $value->id)
                                <span class="label label-primary">Cover Photo</span>
                            @endif
                            {{ $value->description }}
                        </td>
                        <td class="table-action-column">
                            @if ($value->house_property->cover_id != $value->id)
                                <a href="/admin/house_property/{{ $value->house_property->id }}/card/{{ $value->id }}/set_cover" class="btn btn-xs btn-primary" style="margin-bottom:10px;">Set cover</a>
                                {{ Form::open(array('url' => 'admin/house_property/card/' . $value->id, 'class' => 'pull-right')) }}
                                {{ Form::hidden('_method', 'DELETE') }}
                                {{ Form::submit('Delete', array('class' => 'btn btn-warning btn-xs')) }}
                            {{ Form::close() }}
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>No</th>
                        <th>Content</th>
                        <th>Type</th>
                        <th>Description</th>
                        <th class="table-action-column">Action</th>
                    </tr>
                </tfoot>
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
    <!-- table -->

    @include('admin.partials.delete-modal-new')
@stop
@section('script')
<!-- page script -->
<script type="text/javascript">
    $(function()
    {
        $("#tableListStyle").dataTable();
        $("#btn-fb-form").click(function(){
            $('#fb-form').show();
        });
    });
</script>
@stop
