@extends('admin.layouts.main')

@section('content')
	<!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
            <div class="box-tools pull-right">
                <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <form action="" class="form-inline" method="get" style="text-align:right;padding:10px;">
                        <input type="text" name="q" class="form-control input-sm"  placeholder="Booking Code"  autocomplete="off" value="{{ request()->input('q') }}">
                        <button class="btn btn-primary btn-md" id="buttonSearch"><i class="fa fa-search">&nbsp;</i>Search</button>
                    </form>
                </div>
            </div>

            <table  class="table table-striped">
                <thead>
                    <tr>
                        <th>Booking Code</th>
                        <th>Bank Destination</th>
                        <th>User Bank</th>
                        <th>User Bank Account</th>
                        <th>User Account Number</th>
                        <th>Total Payment</th>
                        <th>Payment Date</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                 @foreach($bookingPayments as $key => $bookingPayment)
                     <tr>
                         <td>{{ $bookingPayment->booking_user->booking_code }}</td>
                         <td>{{ $bookingPayment->bank->name }}</td>
                         <td>{{ $bookingPayment->bank_source }}</td>
                         <td>{{ $bookingPayment->account_name }}</td>
                         <td>{{ $bookingPayment->account_number }}</td>
                         <td>Rp. {{ number_format($bookingPayment->total_payment, 0, ',', '.') }}</td>
                         <td>{{ \Carbon\Carbon::parse($bookingPayment->payment_date)->format('Y-m-d') }}</td>
                         <td>
                            @if($bookingPayment->status != 'verified' && \Carbon\Carbon::parse($bookingPayment->booking_user->checkin_date)->setTime(23,59,59)->gt( \Carbon\Carbon::now()))
                                <a href="{{ URL::route('admin.booking.payments.verify', $bookingPayment->id) }}" title="Konfirmasi Pembayaran"><i class="fa fa-check"></i></a>
                            @endif
                         </td>
                     </tr> 
                 @endforeach
                </tbody>
                
            </table>
        </div><!-- /.box-body -->

        <div class="box-body no-padding">
            {{ $bookingPayments->appends(Request::except('page'))->links() }}
        </div>
    </div><!-- /.box -->
    <!-- table -->
@endsection