@extends('admin.layouts.main')

@section('content')
	<div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }} ({{$bookingDesigner->room->name}})</h3>
        </div>

		<form action="{{ URL::route('admin.booking.rooms.type.update', [$bookingDesigner->designer_id, $bookingDesigner->id]) }}" method="POST" class="form-horizontal form-bordered">
			<div class="box-body no-padding">
				<div class="form-group bg-default">
					<label for="type" class="control-label col-sm-2">Tipe</label>
					<div class="col-sm-10">
						<select name="type" id="type" class="form-control">
							@foreach($bookingType as $type) 
								<option value="{{$type}}" {{ old('type') == $type ? 'selected="selected"' : ($bookingDesigner->type == $type ? 'selected="selected"' : '') }}>{{$type}}</option>
							@endforeach
						</select>
					</div>
				</div>

				<div class="form-group bg-default">
					<label for="name" class="control-label col-sm-2">Nama Tipe Kamar</label>
					<div class="col-sm-10">
						<input type="text" name="name" id="name" class="form-control" value="{{ old('name', $bookingDesigner->name) }}">
					</div>
				</div>

				<div class="form-group bg-default">
					<label for="description" class="control-label col-sm-2">Deskripsi</label>
					<div class="col-sm-10">
						<textarea name="description" id="description" class="form-control" rows="5">{{ old('description', $bookingDesigner->description) }}</textarea>
					</div>
				</div>

				<div class="form-group bg-default">
					<label for="max_guest" class="control-label col-sm-2">Jumlah Tamu Maksimal</label>
					<div class="col-sm-10">
						<input type="text" name="max_guest" id="max_guest" class="form-control" value="{{ old('max_guest', $bookingDesigner->max_guest)  }}">
					</div>
				</div>

				<div class="form-group bg-default">
					<label for="minimum_stay" class="control-label col-sm-2">Lama booking minimal</label>
					<div class="col-sm-10">
						<input type="text" name="minimum_stay" id="minimum_stay" class="form-control" value="{{ old('minimum_stay', $bookingDesigner->minimum_stay) }}">
					</div>
				</div>

				<div class="form-group bg-default">
					<label for="available_room" class="control-label col-sm-2">Jumlah Kamar</label>
					<div class="col-sm-10">
						<input type="text" name="available_room" id="available_room" class="form-control" value="{{ old('available_room', $bookingDesigner->available_room) }}">
					</div>
				</div>

				<div class="form-group bg-default">
					<label for="price" class="control-label col-sm-2">Harga <span class="type-unit"></span></label>
					<div class="col-sm-10">
						<input type="number" name="price" id="price" class="form-control" value="{{ old('price', $bookingDesigner->price) }}">
					</div>
				</div>

				<div class="form-group bg-default">
					<label for="sale_price" class="control-label col-sm-2">Harga Diskon</label>
					<div class="col-sm-10">
						<input type="number" name="sale_price" id="sale_price" class="form-control" value="{{ old('sale_price', $bookingDesigner->sale_price) }}">
					</div>
				</div>

				<div class="form-group bg-default">
					<label for="extra_guest_price" class="control-label col-sm-2">Harga tambahan per tamu (jika ada)</label>
					<div class="col-sm-10">
						<input type="number" name="extra_guest_price" id="extra_guest_price" class="form-control" value="{{ old('extra_guest_price') != null ? old('extra_guest_price') :  (isset($priceComponents['extra_guest']) ? $priceComponents['extra_guest']->price : '')}}">
						<span class="help-block">Harga akan ditambahkan jika tamu per kamar lebih dari 1</span>
					</div>
				</div>

				<div class="form-group bg-default">
					<div class="col-sm-10 col-sm-push-2">
						<label for="is_active" class="checkbox">
							<input type="checkbox" name="is_active" id="is_active" class="form-control" value="1" {{ old('is_active') != null && old('is_active') == 1 ? 'checked="checked"' : ($bookingDesigner->is_active == 1 ? 'checked="checked"' : '')  }}> &nbsp; Is Active
						</label>
						
					</div>
				</div>

				<div class="form-group bg-default">
					<div class="col-sm-10 col-sm-push-2">
						<button type="submit" class="btn btn-primary">Save</button>
					</div>
				</div>
			</div>
		</form>
    </div>
@endsection

@section('script')
<script>
	$(document).ready(function() {
		$('#type').on('change', function(e) {
			$('.type-unit').text($(this).val());
		});

		$('#type').trigger('change');
	});
</script>
@endsection