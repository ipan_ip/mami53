@extends('admin.layouts.main')

@section('content')
	<div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

		<div class="row">
			<div class="col-md-12" style="padding: 0px 40px;">
				@foreach ($reject_reason as $key => $value)
					<p style="margin: 10px;"><strong>{{ isset($value->user) ? $value->user->name : "" }}</strong> - {{ $value->content }} - {{ $value->created_at->format("d M Y H:i") }}</p>
				@endforeach
			</div>
		</div>

        <form action="/admin/booking/owner/reject/{{ $room->id }}" method="POST" class="form-horizontal form-bordered" name="rejectRemarkForm">
			<div class="box-body no-padding">
				<div class="form-group bg-default">
					@if ($bbkRequest->status == 'waiting')
						@foreach ($reject_reason_select as $value)
						<div class="radio col-sm-11 col-sm-push-1">
							<label><input type="radio" name="reject_remark" value="{{ $value }}"> {{ $value }}</label>
						</div>
						@endforeach
						<div class="radio col-sm-11 col-sm-push-1">
							<label><input type="radio" name="reject_remark" value="textarea" checked> Lainnya<div><br></div><textarea maxlength="300" style="width:1000px;" name="reject_remark_text" class="form-control" rows="4"></textarea></label>
						</div>
					@endif
                </div>
                <div class="form-group bg-default">
                    <div class="col-sm-11 col-sm-push-1">
						<label for="previous-menu" class="btn btn-sm btn-success">Back to Previous Menu</label>
						@if ($bbkRequest->status == 'waiting')
							<button type="submit" name="action" value="single" class="btn btn-danger">Reject</button>
							<button type="submit" name="action" value="bulk" class="btn btn-danger">Reject Bulk (Total {{ $bbkRequest->user->booking_owner_requests_count }} BBK Request)</button>
						@endif
                    </div>
                </div>
			</div>
		</form>
		<form action="/admin/booking/owner/request" method="GET">
			<input type="hidden" name="room_name" value="{{ $room->name }}">
			<input type="submit" id="previous-menu" class="hidden" onclick="this.disabled=true;this.form.submit();" />
		</form>
    </div>
@endsection
{{-- 
@section('script')
<script>
function disableInput(){
	var radios = document.rejectRemarkForm.reject_remark;

	for (var i=0, iLen=radios.length; i<iLen; i++) {
		radios[i].disabled = true;
	}
    document.rejectRemarkForm.reject_remark_text.disabled = true;
}
</script>
@endsection --}}