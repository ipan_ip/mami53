@extends('admin.layouts.main')

@section('style')
    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" />
    <link href="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.5/daterangepicker.min.css">
    <link rel="stylesheet" href="/css/admin-consultant.css">

    <style>
        .dropdown-menu>li>a:hover {
            color: #333
        }
        .table-responsive {
            overflow: auto;
            height: 625px;
        }

        @media screen and (min-width: 2000px) {
            .table-responsive {
                height: auto;
            }
        }
        .mb-50 {
            margin-bottom: 50px;
        }

        .whatsapp-link {
            background-color: #25D366;
            color: #eee;
            border-radius: 2px;
            padding: 2px;
            line-height: 16px;
            width: 20px;
            height: 20px;
            text-align: center;
            display: inline-block;
        }

        .whatsapp-link:hover {
            color: #fff;
        }

        .hasDatepicker[readonly] {
            background-color: #fff;
            cursor: default;
        }
        .top-action-table {
            display: flex;
            margin-bottom: 20px;
            padding: 0 10px;
            justify-content: space-between;
            align-items: center;
        }

        .refund-reason {
            display: none;
        }

        .toggle-filter-head {
            display: inline-block;
            color: #368FBA;
            font-size: 14px;
            vertical-align: sub;
            padding: 5px;
            cursor: pointer;
            margin-left: 3px;
        }

        .filter-separator {
            font-size: 16px;
            line-height: 24px;
            font-weight: bold;
            padding: 12px 0;
        }

        .filter-button-container {
            padding-top: 12px;
            font-size: 0;
        }

        .filter-button-container .form-group {
            margin-bottom: 0;
        }

        .daterange {
            position: relative;
        }

        .daterange::after {
            content: "\f073";
            font: normal normal normal 14px/1 FontAwesome;
            position: absolute;

        }

        .daterange:read-only {
            background-color: #fff;
            cursor: pointer;
        }

        #buttonDownload {
            color: #368FBA;
            border: 0;
            background-color: transparent;
            padding: 5px;
            margin-top: 8px;
            margin-right: 10px;
            float: right;
        }

        #buttonDownload span {
            text-decoration: underline;
            margin-right: 4px;
        }

        .top-action-table {
            margin-bottom: 8px;
            margin-top: 16px;
            border-top: 0;
        }

        .total-data {
            font-size: 12px;
        }

        .false-table-header {
            display: table;
            table-layout: fixed;
        }

        .false-table-header .cell {
            display: table-cell;
            vertical-align: top;
        }

        .table-container {
            overflow-x: auto;
        }

        .table {
            table-layout: fixed;
        }

        .table td {
            border: 1px solid #ddd;
        }

        .table td div {
            margin-bottom: 8px;
        }

        .table td div:last-child {
            margin-bottom: 4px;
        }

        .table > thead > tr > th {
            text-align: center;
            vertical-align: top;
            position: sticky;
            top: 0;
            background-color: #f3f4f5;
            z-index: 5;
            border: 0;
        }

        .table > thead > tr > th::after {
            content: '';
            position: absolute;
            left: 0;
            top: 0;
            right: 0;
            bottom: 0;
            border-top: 1px solid #ddd;
            border-right: 1px solid transparent;
            border-bottom: 1px solid #ddd;
            border-left: 1px solid #ddd;
        }

        .table td:nth-child(3),
        .table td:nth-child(4) {
            text-align: center;
        }

        .table th:nth-of-type(1) {
            width: 125px;
        }

        .table th:nth-of-type(2) {
            width: 105px;
        }

        .table th:nth-of-type(3) {
            width: 138px;
        }

        .table th:nth-of-type(4) {
            width: 124px;
        }

        .table th:nth-of-type(5) {
            width: 115px;
        }


        .table th:nth-of-type(6) {
            width: 152px;
        }

        .table th:nth-of-type(7) {
            width: 198px;
        }

        .table th:nth-of-type(8) {
            width: 124px;
        }

        .table th:nth-of-type(9) {
            width: 139px;
        }

        .table th:nth-of-type(10) {
            width: 124px;
        }

        .table th:nth-of-type(11) {
            width: 124px;
        }

        .table th:nth-of-type(12) {
            width: 138px;
        }

        .table th:nth-of-type(13) {
            width: 124px;
            border-right: 1px solid #ddd;
        }

        table.table.table-custom-striped > tbody > tr > td {
            background-color: #fff;
        }
        table.table.table-custom-striped > tbody > tr:nth-child(2n) > td {
            background-color: #f3f4f5;
        }
    </style>
@endsection

@section('content')
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title" style="float: none; display: inline-block; vertical-align: middle;">Filter</h3>
            <div class="toggle-filter-head"></div>
        </div>
        <!-- /.box-header -->
        <div class="box-body" style="display: none;">
            <form id="query-form" action="" method="get">
                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Booking Code</label>
                            <input type="text" name="booking_code" class="form-control input-md"  placeholder="Ex: MAMI123123123"  autocomplete="off" value="{{ request()->input('booking_code') }}">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Owner Phone</label>
                            <input type="text" name="owner_phone" class="form-control input-md"  placeholder="Ex: 08123456789"  autocomplete="off" value="{{ request()->input('owner_phone') }}">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Kos Name</label>
                            <input type="text" name="kost_name" class="form-control input-md"  placeholder="Ex: Kos Mamikos"  autocomplete="off" value="{{ request()->input('kost_name') }}">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Tenant Phone</label>
                            <input type="text" name="phone" class="form-control input-md"  placeholder="Ex: 081987654321"  autocomplete="off" value="{{ request()->input('phone') }}">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Tenant Name</label>
                            <input type="text" name="contact_name" class="form-control input-md"  placeholder="Ex: Dale Cooper"  autocomplete="off" value="{{ request()->input('contact_name') }}">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Cities</label>
                            <select class="form-control" name="area_city[]" multiple="multiple" id="cities" data-placeholder="Choose cities">
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Kos Level</label>
                            <select class="form-control" name="kost_level[]" id="kosLevel" data-placeholder="Choose Kos Level"></select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Kos Type</label>
                            <select name="kost_type" id="kost_type" class="form-control">
                                <optgroup label="Kos Type">
                                    <option value="all_non_testing">All</option>
                                    <option value="mamirooms_non_testing">Singgahsini</option>
                                    <option value="non_mamirooms_non_testing">Non-Singgahsini</option>
                                </optgroup>
                                <optgroup label="Testing (for QA)">
                                    <option value="all_testing">All Testing</option>
                                    <option value="mamirooms_testing">Singgahsini Testing</option>
                                    <option value="non_mamirooms_testing">Non-Singgahsini Testing</option>
                                </optgroup>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="filter-separator">
                            Proses Booking
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Booking Status</label>
                            {{ Form::select('status', [
                                    0 => 'All',
                                    1 => 'Booked',
                                    2 => 'Confirmed',
                                    3 => 'Rejected',
                                    4 => 'Cancelled',
                                    5 => 'Cancelled by Admin',
                                    6 => 'Expired',
                                    7 => 'Expired Date',
                                    8 => 'Expired By Owner',
                                    9 => 'Terminated',
                                    10 => 'Verified',
                                    11 => 'Finished',
                                    12 => 'Checked In',
                                ], request()->input('status'), array('class' => 'form-control')) }}
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Instant Booking Status</label>
                            <select name="instant_booking" id="instant-booking" class="form-control">
                                <option value="" {{ request()->input('instant_booking') === null ? 'selected' : '' }}>All</option>
                                <option value="true" {{ request()->input('instant_booking') === 'true' ? 'selected' : '' }}>Active</option>
                                <option value="false" {{ request()->input('instant_booking') === 'false' ? 'selected' : '' }}>Non Active</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Remarks</label>
                            <select name="note_category" id="note-category" class="form-control">
                                <option value="" {{ request()->input('note_category') === null ? 'selected' : '' }}>All</option>
                                <option value="survey" {{ request()->input('note_category') === 'survey' ? 'selected' : '' }}>Akan survei</option>
                                <option value="will_pay" {{ request()->input('note_category') === 'will_pay' ? 'selected' : '' }}>Akan bayar</option>
                                <option value="paid_directly_to_owner" {{ request()->input('note_category') === 'paid_directly_to_owner' ? 'selected' : '' }}>Sudah bayar ke permilik</option>
                                <option value="no_response" {{ request()->input('note_category') === 'no_response' ? 'selected' : '' }}>Tidak merespon chat</option>
                                <option value="unreachable_phone_number" {{ request()->input('note_category') === 'unreachable_phone_number' ? 'selected' : '' }}>Nomor HP tidak dapat dihubungi</option>
                                <option value="already_had_another_kost" {{ request()->input('note_category') === 'already_had_another_kost' ? 'selected' : '' }}>Sudah dapat kos lain</option>
                                <option value="on_discussion" {{ request()->input('note_category') === 'on_discussion' ? 'selected' : '' }}>Masih mempertimbangkan</option>
                                <option value="kost_under_renovation" {{ request()->input('note_category') === 'kost_under_renovation' ? 'selected' : '' }}>Kos sedang renovasi</option>
                                <option value="kost_full" {{ request()->input('note_category') === 'kost_full' ? 'selected' : '' }}>Kos sudah penuh</option>
                                <option value="tenant_recommends_other_kost" {{ request()->input('note_category') === 'tenant_recommends_other_kost' ? 'selected' : '' }}>Tenant ingin rekomendasi kos lain</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Booking Date</label>

                            @if ((is_null(request()->input('from')) || request()->input('from') == '') && (is_null(request()->input('to')) || request()->input('to') == ''))
                            <input type="text" class="form-control input-md daterange booking-date" placeholder="Choose Range" value="" readonly />
                            @else
                            <input type="text" class="form-control input-md daterange booking-date" placeholder="Choose Range" value="{{ request()->input('from') }} - {{ request()->input('to') }}" readonly />
                            @endif

                            <input type="hidden" name="from" value="{{ request()->input('from') }}" id="start-date" readonly />
                            <input type="hidden" name="to" value="{{ request()->input('to') }}" id="end-date" readonly />
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="filter-separator">
                            Proses Disburse/Refund
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Payment Status</label>
                            {{ Form::select('payment_status', [
                                    'all' => 'All',
                                    'dp_paid' => 'DP Paid - ST Paid/Unpaid',
                                    'dp_paid_no_settlement' => 'DP Paid - ST Unpaid',
                                    'paid' => 'Full Payment & DP - ST Paid',
                                    'unpaid' => 'Unpaid'
                                ], request()->input('payment_status'), array('class' => 'form-control')) }}
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Transfer Permission</label>
                            {{ Form::select('transfer_permission', [
                                    'all' => 'All',
                                    'confirmed' => 'Confirmed',
                                    'allow_disburse' => 'Allow Disburse',
                                    'allow_refund' => 'Allow Refund',
                                    'disbursed' => 'Disbursed',
                                    'refunded' => 'Refunded',
                                ], request()->input('transfer_permission'), array('class' => 'form-control')) }}
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Checkin Date</label>

                            @if ((is_null(request()->input('checkin_from')) || request()->input('checkin_from') == '' ) && (is_null(request()->input('checkin_to')) || request()->input('checkin_to') == ''))
                            <input type="text" class="form-control input-md daterange checkin-date" placeholder="Choose Range" value="" readonly />
                            @else
                            <input type="text" class="form-control input-md daterange checkin-date" placeholder="Choose Range" value="{{ request()->input('checkin_from') }} - {{ request()->input('checkin_to') }}" readonly />
                            @endif

                            <input type="hidden" name="checkin_from" value="{{ request()->input('checkin_from') }}" id="checkin-date-from" autocomplete="off" readonly />
                            <input type="hidden" name="checkin_to" value="{{ request()->input('checkin_to') }}" id="checkin-date-to" autocomplete="off" readonly />
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Checkout Date</label>

                            @if ((is_null(request()->input('checkout_from')) || request()->input('checkout_from') == '' ) && (is_null(request()->input('checkout_to')) || request()->input('checkout_to') == ''))
                            <input type="text" class="form-control input-md daterange checkout-date" placeholder="Choose Range" value="" readonly />
                            @else
                            <input type="text" class="form-control input-md daterange checkout-date" placeholder="Choose Range" value="{{ request()->input('checkout_from') }} - {{ request()->input('checkout_to') }}" readonly />
                            @endif

                            <input type="hidden" name="checkout_from" value="{{ request()->input('checkout_from') }}" id="checkout-date-from" autocomplete="off" readonly />
                            <input type="hidden" name="checkout_to" value="{{ request()->input('checkout_to') }}" id="checkout-date-to" autocomplete="off" readonly />
                        </div>
                    </div>
                    <!-- /.col -->

                    <div class="col-md-12">
                        <div class="filter-button-container">
                            <div class="form-group">
                                <button type="button" class="btn btn-primary btn-md" id="buttonSearch" style="width: 175px;"><i class="fa fa-search">&nbsp;</i>Cari</button>
                                <button type="button" class="btn btn-default btn-md" id="buttonReset" style="margin-left: 16px;">Reset Filter</button>
                                {{-- <button type="button" class="btn btn-primary btn-md" id="buttonDownload"><i class="fa fa-search">&nbsp;</i>Download CSV</button> --}}
                            </div>
                        </div>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
            <!-- /.row -->
        </div>
        <!-- /.box-body -->
    </div>

	<!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
            <a href="{{ url('/admin/booking/users/create') }}" class="btn btn-default" style="margin-top: 5px;">Booking Now</a>
            <button type="button" id="buttonDownload"><span>Download CSV</span>&nbsp;<i class="fa fa-download"></i></button>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper top-action-table">
                <div class="total-data">Total data: {{ $count }}</div>
            </div>

            <div style="padding: 0 10px;">
                <div class="table-responsive">

                    <table class="table table-custom-striped">
                        <thead>
                            <tr>
                                <th>Action</th>
                                <th>Request Time<br>&amp; Status</th>
                                <th>Booking Code</th>
                                <th>Transfer Permission</th>
                                <th>Detail Tenant</th>
                                <th>Tags &amp; Remarks</th>
                                <th>Detail Kos</th>
                                <th>Room Available<br>&amp; Last Update</th>
                                <th>Detail Price</th>
                                <th>Checkin Date / <br>Has Checkin</th>
                                <th>Exp. Owner / <br>Exp. Tenant</th>
                                <th>Additional Price by Admin</th>
                                <th>Checkout Date / <br>Has Checkout</th>
                            </tr>
                        </thead>

                        <tbody>
                        @foreach($bookingUsers as $key => $bookingUser)
                            <tr>
                                <td class="table-action-column" style="padding-right:15px!important;">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                            <i class="fa fa-cogs"></i> Actions
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a href="{{ URL::route('admin.booking.users.show', $bookingUser['id']) }}" title="Lihat Detail" class="action-recalculate">
                                                    <i class="fa fa-eye"></i> Detail
                                                </a>
                                            </li>

                                            @if($bookingUser['is_booked'])
                                                <li>
                                                    <a href="{{ URL::route('admin.booking.users.edit', $bookingUser['id']) }}" title="Ubah" class="action-recalculate">
                                                        <i class="fa fa-edit"></i> Edit
                                                    </a>
                                                </li>
                                            @endif

                                            <li>
                                                @if($bookingUser['has_been_booked'])
                                                    @if ($bookingUser['checkout_date'] > \Carbon\Carbon::today())
                                                        <a href="{{ URL::route('admin.booking_accept_wizard.accept', $bookingUser['id']) }}" title="Konfirmasi Ketersediaan Kamar" class="action-recalculate">
                                                            <i class="fa fa-check"></i> Confirm
                                                        </a>
                                                    @endif
                                                @endif
                                            </li>

                                            <li>
                                                @if($bookingUser['is_booked'] || $bookingUser['is_verified'] || $bookingUser['is_paid'])
                                                    <a data-toggle="modal" data-target="#modal-cancel-{{ $bookingUser['id'] }}" title="Batalkan Booking" class="action-recalculate">
                                                        <i class="fa fa-ban"></i> Cancel
                                                    </a>
                                                @endif
                                            </li>

                                            <li>
                                                @if($bookingUser['is_booked'])
                                                    <a data-toggle="modal" data-target="#modal-info-{{ $bookingUser['id'] }}" title="Tolak Booking" class="action-recalculate">
                                                        <i class="fa fa-warning"></i> Rejected
                                                    </a>
                                                @endif
                                            </li>

                                            <li>
                                                @if($bookingUser['is_guarantee_requested'])
                                                    <a href="{{ URL::route('admin.booking.users.refund', $bookingUser['id']) }}" title="Refund">
                                                        <i class="fa fa-money"></i>
                                                    </a>
                                                @endif
                                            </li>

                                            <li>
                                                @if($bookingUser['is_verified'])
                                                    <a data-toggle="modal" data-target="#modal-check-in-{{ $bookingUser['id'] }}" title="Check In">
                                                        <i class="fa fa-check-square"></i> Check In
                                                    </a>
                                                @endif
                                            </li>

                                            <li>
                                                @if (Auth::user()->consultant)
                                                    <a onclick="(e) => e.preventDefault(); editNote({{ $bookingUser['id'] }}, '{{ $bookingUser['note']['category'] }}', '{{ $bookingUser['note']['content'] }}');" title="Edit Note">
                                                        <i class="fa fa-clipboard"></i> Edit Consultant Note
                                                    </a>
                                                @else
                                                    <a onclick="(e) => e.preventDefault(); showNote({{ $bookingUser['id'] }},  '{{ $bookingUser['note']['content'] }}');" title="View Note">
                                                        <i class="fa fa-clipboard"></i> Show Consultant Note
                                                    </a>
                                                @endif
                                            </li>

                                            @if (in_array($bookingUser['status'], ['verified', 'rejected', 'paid',
                                                'checked_in',
                                                'cancel_by_admin',
                                                'terminated',
                                                'finished',
                                                'expired']))
                                                @if (!is_null($bookingUser['transfer_permission']))
                                                    <li>
                                                        <a data-toggle="modal" data-target="#modal-disburse-{{ $bookingUser['id'] }}" title="Set Transfer Permission" class="action-recalculate">
                                                            <i class="fa fa-refresh"></i> Transfer Permission
                                                        </a>
                                                    </li>
                                                @endif
                                            @endif
                                        </ul>
                                    </div>
                                </td>

                                <td>
                                    <div style="margin-bottom: 36px;">{{ $bookingUser['created_at'] }}</div>
                                    <div>
                                        @if ($bookingUser['is_booked'])
                                            <span class="label label-info">{{ $bookingUser['status'] ?? "" }}</span>
                                        @elseif($bookingUser['is_verified'])
                                            <span class="label label-success">{{ $bookingUser['status'] ?? "" }}</span>
                                        @elseif($bookingUser['is_paid'] || $bookingUser['is_confirmed'])
                                            <span class="label label-primary">{{ $bookingUser['status'] ?? "" }}</span>
                                        @elseif($bookingUser['is_rejected'] || $bookingUser['is_cancelled'] || $bookingUser['is_cancelled_by_admin'] || $bookingUser['is_terminated'])
                                            <span class="label label-danger">{{ $bookingUser['status'] ?? "" }}</span>
                                        @elseif($bookingUser['is_expired'])
                                            <span class="label label-warning">{{ $bookingUser['status'] ?? "" }}</span>
                                        @elseif($bookingUser['is_expired_date'] )
                                            <span class="label label-danger">{{ $bookingUser['status'] ?? "" }}</span>
                                        @else
                                            <span class="label label-info">{{ $bookingUser['status'] ?? "" }}</span>
                                        @endif
                                    </div>
                                    <div>
                                        <strong>Created By :</strong>
                                        <br>
                                        <strong style="text-transform: capitalize;">{{ $bookingUser['changed_by'] }}</strong>
                                    </div>
                                </td>

                                <td>{{ $bookingUser['booking_code'] }}</td>

                                <td>
                                    <span class="label label-info">{{ $bookingUser['transfer_permission'] ?? "-" }}</span>
                                </td>

                                <td>
                                    <div>{{ $bookingUser['contact_name'] }}</div>
                                    <div><strong>Total Tenant: </strong>{{ $bookingUser['guest_total'] }}</div>
                                    <div>{{ $bookingUser['contact_phone'] }}</div>
                                    <div><a class="whatsapp-link" data-phone="{{ $bookingUser['contact_phone'] }}" href="" target="_blank"><i class="fa fa-whatsapp"></i></a></div>
                                </td>

                                <td>
                                    <div>
                                        @forelse ($bookingUser['tags'] as $tag)
                                            <span class="label label-info">{{ $tag->name }}</span>&nbsp;
                                        @empty
                                            <b>-</b>
                                        @endforelse
                                    </div>
                                    <div>
                                        <strong>Remarks :</strong>
                                        <br>
                                        @switch($bookingUser['note']['category'])
                                            @case('survey')
                                                Akan survei
                                                @break

                                            @case('will_pay')
                                                Akan bayar
                                                @break

                                            @case('paid_directly_to_owner')
                                                Sudah bayar ke pemilik
                                                @break

                                            @case('no_response')
                                                Tidak merespon chat
                                                @break

                                            @case('unreachable_phone_number')
                                                Nomor HP tidak dapat dihubungi
                                                @break

                                            @case('already_had_another_kost')
                                                Sudah dapat kos lain
                                                @break

                                            @case('on_discussion')
                                                Masih mempertimbangkan
                                                @break

                                            @case('kost_under_renovation')
                                                Kos sedang renovasi
                                                @break

                                            @case('kost_full')
                                                Kos sudah penuh
                                                @break

                                            @case('tenant_recommends_other_kost')
                                                Tenant ingin rekomendasi kos lain
                                                @break

                                            @default
                                                -
                                        @endswitch
                                    </div>
                                    <div>
                                        <strong>Note :</strong>
                                        <br>
                                        {{ !empty($bookingUser['note']['content']) ? $bookingUser['note']['content'] : '-' }}
                                    </div>
                                    <div>
                                        <strong>Updated By :</strong>
                                        <br>
                                        {{ !empty($bookingUser['note']['updated_by']) ? $bookingUser['note']['updated_by'] : '-' }}
                                    </div>
                                </td>

                                <td>
                                    @if ($bookingUser['is_room_deleted'])
                                        <strong>KOS DELETED</strong>
                                    @else
                                    <div>
                                        <strong>Kos Name: </strong>{{ $bookingUser['room_name'] }}
                                    </div>
                                    <div>
                                        <strong>Kos Level:</strong> {{ $bookingUser['kost_level'] ?? 'No' }}
                                    </div>
                                    <div>
                                        {{ $bookingUser['owner_phone'] }}
                                    </div>
                                    <div>
                                        <a class="whatsapp-link" data-phone="{{ $bookingUser['owner_phone'] }}" href="" target="_blank"><i class="fa fa-whatsapp"></i></a>
                                    </div>
                                    <div>
                                        <strong>Status of Instant Booking</strong>
                                        <br>
                                        @if ($bookingUser['is_instant_booking'])
                                            <span class="label label-success">Active</span>
                                        @else
                                            <span class="label label-danger">Non Active</span>
                                        @endif
                                    </div>
                                    <div>
                                        <strong>{{ $bookingUser['area_city'] }}</strong>
                                    </div>
                                    @endif
                                </td>

                                <td>
                                    <div><strong>{{ $bookingUser['room_available'] }}</strong></div>
                                    <div>{{ $bookingUser['kos_last_update'] }}</div>
                                </td>

                                <td>
                                    <div>
                                        <strong>Original Price :</strong>
                                        <br>
                                        Rp. {{ $bookingUser['original_price'] }}
                                    </div>
                                    <div>
                                        <strong>Additional Price by Owner :</strong>
                                        <br>
                                        @if (count($bookingUser['additional_prices_by_owner']) > 0)
                                            @foreach ($bookingUser['additional_prices_by_owner'] as $price)
                                                {{ $price->name.' - '.$price->price }} <br>
                                            @endforeach
                                        @else
                                            -
                                        @endif
                                    </div>
                                    <div>
                                        <strong>Rent Count Type :</strong>
                                        <br>
                                        {{ $bookingUser['rent_count_type'] }}
                                    </div>
                                </td>

                                <td>
                                    <div>{{ $bookingUser['checkin_date'] }}</div>
                                    <div>
                                        @if ($bookingUser['has_checked_in'])
                                            <i class="fa fa-check" style="color:green"></i>
                                        @else
                                            <i class="fa fa-times" style="color:red"></i>
                                        @endif
                                    </div>
                                </td>

                                <td>
                                    <div>{!! !empty($bookingUser['expired_date_by_owner']) ? $bookingUser['expired_date_by_owner'] : '-' !!}</div>
                                    <div>/</div>
                                    <div>{!! !empty($bookingUser['expired_date_by_tenant']) ? $bookingUser['expired_date_by_tenant'] : '-'!!}</div>
                                </td>

                                <td>
                                    @if (!empty($bookingUser['additional_prices']))
                                        <div>
                                            @foreach ($bookingUser['additional_prices'] as $key => $prices)
                                                {{ $prices['key'].' '.$prices['value'] }} <br>
                                            @endforeach
                                        </div>
                                    @endif
                                    <div>
                                        @php
                                            $createEditBookingUrl =
                                                !is_null($bookingUser['instant_booking_owner_id']) && !empty($bookingUser['additional_prices'])
                                                ? route('admin.booking.owner.instant-booking.edit', ['id' => $bookingUser['instant_booking_owner_id']])
                                                : route('admin.booking.owner.instant-booking.create')
                                        @endphp

                                        <a href="{{ $createEditBookingUrl }}" target="_blank" class="btn btn-default">Add <i class="fa fa-plus"></i></a>
                                    </div>
                                </td>

                                <td>
                                    <div>{{ $bookingUser['checkout_date'] }}</div>
                                    <div>
                                        @if ($bookingUser['has_checked_out'])
                                            <i class="fa fa-check" style="color:green"></i>
                                        @else
                                            <i class="fa fa-times" style="color:red"></i>
                                        @endif
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div><!-- /.box-body -->
        <div class="box-body">
            {{ $rowsBookingUser->appends(Request::except('page'))->links() }}
        </div>
    </div><!-- /.box -->
    <!-- table -->
    <link href="{{ asset('assets/vendor/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" >
    <script src="{{ asset('assets/vendor/moment/moment.min.js') }}"></script>

    <script src="{{ asset('assets/vendor/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>

    <!-- modal -->
    @foreach($bookingUsers as $key => $bookingUser)
        <div class="modal modal-info fade" id="modal-cancel-{{ $bookingUser['id'] }}">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        Berikan alasan pembatalan booking terhadap pemilik kost
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </div>
                    <form action="{{ URL::route('admin.booking.users.cancel', $bookingUser['id']) }}" method="POST">
                        <div class="modal-body">
                            <div class="box-body no-padding">
                                <div class="form-group bg-default">
                                    <select class="form-control reason" name="cancel_id">
                                        @foreach ($cancelReasons as $reason)
                                            <option value="{{ $reason->id }}" >{{ $reason->description }}</option>
                                        @endforeach
                                            <option>Lainnya</option>
                                    </select>
                                </div>
                                <div class="form-group bg-default otherReason">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline pull-left forClose" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-outline">Send</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="modal modal-info fade" id="modal-info-{{ $bookingUser['id'] }}">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </div>
                    <form action="{{ URL::route('admin.booking.users.reject', $bookingUser['id']) }}" method="POST">
                        <div class="modal-body">
                            <input type="hidden" name="room_id" value="">
                            <div class="box-body no-padding">
                                <div class="form-group bg-default">
                                    <label>Reject Reason</label>
                                    <select class="form-control reason" name="reject_id">
                                        @foreach ($rejectReasons as $reason)
                                            <option value="{{ $reason->id }}" >{{ $reason->description }}</option>
                                        @endforeach
                                            <option>Lainnya</option>
                                    </select>
                                </div>
                                <div class="form-group bg-default otherReason">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline pull-left forClose" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-outline">Send</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="modal modal-info fade" id="modal-check-in-{{ $bookingUser['id'] }}">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <b>Masukkan waktu Check-In</b>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </div>
                    <form action="{{ URL::route('admin.booking.users.check-in', $bookingUser['id']) }}" method="POST">
                        <div class="modal-body">
                            <div class="box-body no-padding">
                                <div class="form-group bg-default">
                                    <input type='text' class="form-control datetimepicker" name="datetime_checkin" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-outline">Send</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="modal modal-info fade transfer-permission-modal" id="modal-disburse-{{ $bookingUser['id'] }}">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <b>Transfer Permission Status</b>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </div>
                    <form action="{{ URL::route('admin.booking.users.transfer-permission', $bookingUser['id']) }}" method="POST">
                        <div class="modal-body">
                            <div class="box-body no-padding">
                                <div class="form-group bg-default">
                                    <select class="form-control permissionStatus" name="status">
                                        @if(!empty($bookingUser['transfer_permission']) && !is_null($bookingUser['transfer_permission_rule']))
                                            @foreach($bookingUser['transfer_permission_rule'] as $key => $value)
                                                <option class="form-control" value="{{ $key }}">{{ $value }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group refund-reason"></div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-outline">Send</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endforeach
    <!-- /.modal -->

    @section('script')
        <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.27.0/moment.min.js"></script>
        <script src="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.5/daterangepicker.min.js"></script>
        <link href="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.min.css" rel="stylesheet"/>

        <script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/ScrollMagic.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/plugins/debug.addIndicators.min.js"></script>

        <script>
            // edit note ------------------------------------------------
            function editNote(id, category, content) {
                Swal.fire({
                    title: 'Type some notes...',
                    html: `
                        <label for="swal-select-editnote">Remarks</label>
                        <br />
                        <select id="swal-select-editnote" class="swal2-select" style="margin-top: 4px;">
                            <option value="survey" ${ category === 'survey' ? 'selected' : '' }>Akan survei</option>
                            <option value="will_pay" ${ category === 'will_pay' ? 'selected' : '' }>Akan bayar</option>
                            <option value="paid_directly_to_owner" ${ category === 'paid_directly_to_owner' ? 'selected' : '' }>Sudah bayar ke pemilik</option>
                            <option value="no_response" ${ category === 'no_response' ? 'selected' : '' }>Tidak merespon chat</option>
                            <option value="unreachable_phone_number" ${ category === 'unreachable_phone_number' ? 'selected' : '' }>Nomor HP tidak dapat dihubungi</option>
                            <option value="already_had_another_kost" ${ category === 'already_had_another_kost' ? 'selected' : '' }>Sudah dapat kos lain</option>
                            <option value="on_discussion" ${ category === 'on_discussion' ? 'selected' : '' }>Masih mempertimbangkan</option>
                            <option value="kost_under_renovation" ${ category === 'kost_under_renovation' ? 'selected' : '' }>Kos sedang renovasi</option>
                            <option value="kost_full" ${ category === 'kost_full' ? 'selected' : '' }>Kos sudah penuh</option>
                            <option value="tenant_recommends_other_kost" ${ category === 'tenant_recommends_other_kost' ? 'selected' : '' }>Tenant ingin rekomendasi kos lain</option>
                        </select>
                        <br />
                        <label for="swal-input-editnote">Notes</label>
                        <br />
                        <input type="text" id="swal-input-editnote" class="swal2-input" value="${content}" style="margin-top: 4px;" />
                    `,
                    showCancelButton: true,
                    confirmButtonText: 'Save',
                    showLoaderOnConfirm: true,
                    preConfirm: () => {
                        const editnoteCategory = $('#swal-select-editnote').val();
                        const editnoteContent = $('#swal-input-editnote').val();

                        let form = new FormData();
                        form.append('category', editnoteCategory);
                        form.append('content', editnoteContent);

                        let response = fetch(`/admin/booking/users/${id}/note`, {
                            method: 'POST',
                            body: form
                        })
                        .then(response => {
                            if (response.status) {
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Success',
                                    text: 'Your note has been saved'
                                });

                                window.location.reload();
                            }
                        })
                        .catch(error => {
                            alert(error);
                        });
                    },
                    allowOutsideClick: () => !Swal.isLoading()
                });
            }

            function showNote(id, content) {
                swal.fire({
                    title: 'Consultant Note',
                    text: content,
                });
            }


            $(function() {
                // select cities ------------------------------------------------
                var preDatas = {!! json_encode($areaCities) !!}
                var cityDatas = $.map({!! json_encode($cities) !!}, function (city) {
                    return {
                        id: city,
                        text: city
                    }
                });

                var selectCities = $("#cities").select2({
                    placeHolder: "Select Cities",
                    allowClear: true,
                    closeOnSelect: false,
                    tags: false,
                    width: '100%',
                    tokenSeparators: [','],
                    selectOnClose: false,
                    language: {
                        noResults: function(ada){
                            return "No Results Found";
                        }
                    },
                    data: cityDatas
                });

                if (preDatas) {
                    preDatas.forEach(function(e){
                        if(!selectCities.find('option:contains(' + e + ')').length)
                            selectCities.append($(`<option value="${e}">`).text(e));
                    });
                }

                selectCities.val(preDatas).trigger("change");

                // kos level select2 ------------------------------------------------
                const kosLevelObject = {!! json_encode($kostLevels) !!};
                const kosLevelArray = $.map(kosLevelObject, function(value, key) {
                    return {
                        id: key,
                        text: value
                    }
                });

                $('#kosLevel').select2({
                    multiple: true,
                    allowClear: true,
                    closeOnSelect: false,
                    tags: false,
                    width: '100%',
                    tokenSeparators: [','],
                    selectOnClose: false,
                    language: {
                        noResults: function(ada){
                            return "No Results Found";
                        }
                    },
                    data: kosLevelArray
                });

                const preselected = {!! json_encode(request()->input("kost_level")) !!};
                $('#kosLevel').val(preselected).trigger('change');

                const preselectedKosType = '{{ request()->input("kost_type") }}'

                $('#kost_type').select2({
                    width: '100%'
                });

                if (preselectedKosType) {
                    $('#kost_type').val(preselectedKosType).trigger('change');
                }


                // manual form reset ------------------------------------------------
                $('#buttonReset').on('click', function(e) {
                    e.preventDefault();

                    $('#query-form').find('input, select').val('');
                    $('#cities').val('').trigger('change');
                    $('#kosLevel').val('').trigger('change');
                    $('select[name="status"]').val(0);
                    $('select[name="transfer_permission"], select[name="payment_status"]').val('all');

                    $('#query-form').submit();
                });


                // toggle filter head ------------------------------------------------

                let $toggleFilterHead = $('.toggle-filter-head');
                let $filterBody = $toggleFilterHead.parent('.box-header').next('.box-body');

                const hideText = 'Sembunyikan <i class="fa fa-angle-up"></i>';
                const showText = 'Tampilkan Filter <i class="fa fa-angle-down"></i>'

                $toggleFilterHead.html(showText);

                $toggleFilterHead.on('click', function() {
                    $(this).html($(this).html() === showText ? hideText : showText);
                    $filterBody.stop().slideToggle();
                });

                $('.reason').change(function() {
                    otherReason = $(this).val();
                    if (otherReason == "Lainnya") {
                        $('.otherReason').html('<label>Lainnya</label><textarea name="other_reason" class="form-control" id=""></textarea>');
                    } else {
                        $('.otherReason').children().remove();
                    }
                });

                $('.forClose').click(function() {
                    $('.otherReason').children().remove();
                });

                $('.datepicker').datepicker({ dateFormat : 'dd-mm-yy'});

                $('#buttonDownload').click((e) => {
                    $('#query-form').attr('action', '/admin/booking/users/download').submit();
                });

                $('#buttonSearch').click((e) => {
                    $('#query-form').attr('action', '/admin/booking/users').submit();
                });

                $('.permissionStatus').on('change', function() {
                    let selectedStatus = $(this).val();
                    let $selectRefundReason = $(this).parent().next();
                    let $submit = $(this).parents('form').find('button[type=submit]');

                    const $selectReasonElm = $(`{{
                        Form::select('refund_reason', [
                            'room_is_full' => 'Kamar penuh',
                            'rejected_by_owner' => 'Ditolak Owner',
                            'room_not_like_in_ads' => 'Kamar tidak sesuai dengan iklan',
                            'booking_is_cancelled' => 'Tidak jadi sewa kos',
                            'non_instant_booking' => 'Non Instant Booking',
                            'owner_not_accept_mamipay' => 'Owner tidak menerima pembayaran via Mamikos'
                        ], array(), array('class' => 'form-control select-refund-reason', 'placeholder' => 'Pilih alasan refund'))
                    }}`);

                    if (selectedStatus === 'allow_refund') {
                        $selectRefundReason.show();
                        $selectRefundReason.html($selectReasonElm);
                        $submit.prop('disabled', true);

                        $selectReasonElm.on('change', function() {
                            if ($(this).val()) {
                                $submit.prop('disabled', false);
                            } else {
                                $submit.prop('disabled', true);
                            }
                        });
                    } else {
                        $selectRefundReason.hide();
                        $selectRefundReason.empty();
                        $submit.prop('disabled', false);
                    }
                });

                $('.transfer-permission-modal').on('show.bs.modal', function(e) {
                    let $permissionStatusSelect = $(this).find('.permissionStatus');
                    $permissionStatusSelect.change();
                });

                // datepicker ------------------------------------------------------------------------------------
                $('.datetimepicker').datetimepicker({
                    inline: true,
                    sideBySide: true
                });

                $('.daterange').daterangepicker({
                    opens: 'left',
                    autoUpdateInput: false,
                    locale: {
                        format: 'DD-MM-YYYY'
                    }
                });

                $('.daterange').on('apply.daterangepicker', function(ev, picker) {
                    $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
                });

                $('.daterange.booking-date').on('apply.daterangepicker', function(ev, picker) {
                    $('#start-date').val(picker.startDate.format('DD-MM-YYYY'));
                    $('#end-date').val(picker.endDate.format('DD-MM-YYYY'));
                });

                $('.daterange.checkin-date').on('apply.daterangepicker', function(ev, picker) {
                    $('#checkin-date-from').val(picker.startDate.format('DD-MM-YYYY'));
                    $('#checkin-date-to').val(picker.endDate.format('DD-MM-YYYY'));
                });

                $('.daterange.checkout-date').on('apply.daterangepicker', function(ev, picker) {
                    $('#checkout-date-from').val(picker.startDate.format('DD-MM-YYYY'));
                    $('#checkout-date-to').val(picker.endDate.format('DD-MM-YYYY'));
                });


                $(".chosen-select").chosen({
                    no_results_text: "Oops, nothing found!"
                });

                // $("#kostLevel").addClass("chosen-select");

                $('.whatsapp-link').each(function() {
                    const phone = $(this).data('phone');
                    const international = function() {
                        if (+phone[0] !== 0) {
                            console.error('ada nomor telpon yang ngga pakai 0 didepannya')
                        }
                        return `+62${phone.substr(1)}`;
                    };
                    $(this).attr('href', `https://wa.me/${international()}`);
                });
            });
        </script>
    @endsection
@endsection
