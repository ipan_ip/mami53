@extends('admin.layouts.main')

@section('style')
<style>
    .total-price {
        font-weight: bold;
    }

    .total-price td:first-child {
        text-align: right;
    }

    .accordion-table .btn.btn-box-tool,
    .title-section button {
      border: none;
      box-shadow: unset;
      background-color: transparent;
    }

    .detail-booking-title-page {
        margin-bottom: 20px;
        display: flex;
        align-items: center;
    }

    .detail-booking-title-page a,
    .detail-booking-title-page a:hover {
        color: #333;
        font-size: 20px;
    }

    .detail-booking-title-page h2 {
        margin: 0 0 0 15px;
    }
</style>
@endsection

@section('content')
    @php
        $previousUrl = url()->current() == url()->previous() ? url()->route('admin.booking.users') : url()->previous();
    @endphp

    <section class="detail-booking-title-page">
        <a href="{{ $previousUrl }}">
            <i class="fa fa-arrow-left"></i>
        </a>
        <h2>
            Detail Booking
        </h2>
    </section>

    <div class="row">
        <div class="col-sm-4">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Data Booking - {{ $bookingUser['booking_data']['booking_code'] }}</h3>
                </div><!-- /.box-header -->
                <div class="box-body no-padding">
                    <div class="form-horizontal form-bordered">
                        <div class="form-group">
                            <label class="control-label col-sm-4">Kost Name</label>
                            <div class="col-sm-8">
                                <p class="form-control-static">{{ $bookingUser['room_data']['name']  }}</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-4">Address</label>
                            <div class="col-sm-8">
                                <p class="form-control-static">{{ $bookingUser['room_data']['address'] }}</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-4">City</label>
                            <div class="col-sm-8">
                                <p class="form-control-static">{{ $bookingUser['room_data']['area_city']  }}</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-4">Subdistrict</label>
                            <div class="col-sm-8">
                                <p class="form-control-static">{{ $bookingUser['room_data']['area_subdistrict']  }}</p>
                            </div>
                        </div>

                        @if ($contractTypeKost === null)
                            <div class="form-group">
                                <label class="control-label col-sm-4">Room Number</label>
                                <div class="col-sm-8">
                                    <p class="form-control-static">{{ $bookingUser['booking_data']['name'] ?? "-" }}</p>
                                </div>
                            </div>
                        @else
                            <div class="form-group">
                                <label class="control-label col-sm-4">Room Number</label>
                                <div class="col-sm-8">
                                    <p class="form-control-static">{{ $contractTypeKost['name'] ?? "-" }}</p>
                                </div>
                            </div>
                        @endif

                        <div class="form-group">
                            <label class="control-label col-sm-4">Rent Count</label>
                            <div class="col-sm-8">
                                <p class="form-control-static">{{ $bookingUser['booking_data']['rent_count_type'] }}</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-4">Original Price</label>
                            <div class="col-sm-8">
                                <p class="form-control-static">{{ $bookingUser['booking_data']['original_price'] }}</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-4">Duration</label>
                            <div class="col-sm-8">
                                <p class="form-control-static">
                                    {{ \Carbon\Carbon::parse($bookingUser['booking_data']['checkin'])->format('Y-m-d')  }} -&nbsp;
                                    {{ \Carbon\Carbon::parse($bookingUser['booking_data']['checkout'])->format('Y-m-d') }}&nbsp;
                                    ( {{ $bookingUser['booking_data']['duration_string'] }} )
                                </p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-4">Name</label>
                            <div class="col-sm-8">
                                <p class="form-control-static">{{ $bookingUser['user_booking']['name']  }}</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-4">Phone Number</label>
                            <div class="col-sm-8">
                                <p class="form-control-static">{{ $bookingUser['user_booking']['phone']  }}</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-4">Email</label>
                            <div class="col-sm-8">
                                <p class="form-control-static">{{ $bookingUser['user_booking']['email']  }}</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-4">Total Tenant</label>
                            <div class="col-sm-8">
                                <p class="form-control-static">{{ $bookingUser['booking_data']['guest_total'] }}</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-4">Pasangan Suami Istri</label>
                            <div class="col-sm-8">
                                <p class="form-control-static">{{ $bookingUser['booking_data']['is_married'] ? 'Ya' : 'Tidak' }}</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-4">Status</label>
                            <div class="col-sm-8">
                                <p class="form-control-static"><b>{{ $bookingUser['booking_data']['status'] }}</b></p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-4">Transfer Permission (refund/disburse)</label>
                            <div class="col-sm-8">
                                <p class="form-control-static">
                                    @if(!empty($bookingUser['payment']['transfer_permission']))
                                        <b>{{ $bookingUser['payment']['transfer_permission'] }}</b>
                                    @else
                                        <b>-</b>
                                    @endif
                                </p>
                            </div>
                        </div>

                        @if(!is_null($bookingUser['booking_data']['tenant_checkin_time'] ))
                            <div class="form-group">
                                <label class="control-label col-sm-4">Waktu masuk Tenant</label>
                                <div class="col-sm-8">
                                    <p class="form-control-static">{{ $bookingUser['booking_data']['tenant_checkin_time'] }}</p>
                                </div>
                            </div>
                        @endif

                        @if(!is_null($bookingUser['booking_data']['cancel_reason']))
                            <div class="form-group">
                                <label class="control-label col-sm-4">Reason</label>
                                <div class="col-sm-8">
                                    <p class="form-control-static">{{ $bookingUser['booking_data']['cancel_reason'] }}</p>
                                </div>
                            </div>
                        @endif

                        <div class="form-group">
                            <label class="control-label col-sm-4">Created At</label>
                            <div class="col-sm-8">
                                <p class="form-control-static">{{ $bookingUser['booking_data']['date'] }}</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-4">Owner Name</label>
                            <div class="col-sm-8">
                                <p class="form-control-static">{{ !empty($bookingUser['room_data']['owner_name']) ? $bookingUser['room_data']['owner_name'] : 'Data is Not Available' }}</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-4">Owner Phone</label>
                            <div class="col-sm-8">
                                <p class="form-control-static">{{ !empty($bookingUser['room_data']['owner_phone']) ? $bookingUser['room_data']['owner_phone'] : 'Data is Not Available' }}</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-4">Manager Name</label>
                            <div class="col-sm-8">
                                <p class="form-control-static">{{ !empty($bookingUser['room_data']['manager_name']) ? $bookingUser['room_data']['manager_name'] : 'Data is Not Available' }}</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-4">Manager Phone</label>
                            <div class="col-sm-8">
                                <p class="form-control-static">{{ !empty($bookingUser['room_data']['manager_phone']) ? $bookingUser['room_data']['manager_phone'] : 'Data is Not Available' }}</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-4">Office Phone</label>
                            <div class="col-sm-8">
                                <p class="form-control-static">{{ !empty($bookingUser['room_data']['office_phone']) ? $bookingUser['room_data']['office_phone'] : 'Data is Not Available' }}</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-4">From POTS</label>
                            <div class="col-sm-8">
                                <p class="form-control-static">
                                    {{ $bookingUser['booking_data']['from_pots'] ? 'Ya' : 'Tidak' }}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            @component('admin.components.accordion-table', [
                'title' => 'Contract Detail',
                'headers' => ['Link', 'Invoice Number', 'Title', 'Amount', 'Status', 'Paid Date']
            ])
                @if(!is_null($invoices))
                    @foreach ($invoices as $invoice)
                        <tr>
                            <td>
                                <a href="{{ optional($invoice)->invoice_link }}" target="_blank">
                                    {{ optional($invoice)->invoice_link }}
                                </a>
                            </td>
                            <td>{{ optional($invoice)->invoice_number }}</td>
                            <td>{{ optional($invoice)->name }}</td>
                            @if (optional($invoice)->amount != null)
                                <td>
                                    {{ 'Rp. ' . number_format($invoice->amount, 0, ',', '.') }}
                                </td>
                            @else
                                <td> - </td>
                            @endif
                            <td>
                                @if (optional($invoice)->status == 'paid')
                                    <span class="label label-success">
                                        {{ optional($invoice)->status }}
                                    </span>
                                @else
                                    <span class="label label-warning">
                                        {{ optional($invoice)->status }}
                                    </span>
                                @endif
                            </td>
                            <td>{{ optional($invoice)->paid_at }}</td>
                        </tr>
                    @endforeach
                @endif
            @endcomponent

            @component('admin.components.accordion-table', [
                'title' => 'Transfer Status to Owner',
                'headers' => ['Paid At', 'Transfer Status', 'Transfer Permission', 'Transfer Permission Status', 'Bank Account', 'Bank Account Number', 'Bank Account Owner']
            ])
                @foreach ($transfer_status as $item)
                    <tr>
                        <td>{{ $item['paid_at'] }}</td>
                        <td>
                            @component('admin.contents.booking.partials.status_chip')
                                {{ $item['transfer_status'] }}
                            @endcomponent
                        </td>
                        <td>
                            <span class="text-capitalize">{{ $item['transfer_permission'] }}</span>
                        </td>
                        <td>
                            @component('admin.contents.booking.partials.status_chip')
                                {{ $item['transfer_permission_status'] }}
                            @endcomponent
                        </td>
                        <td>
                            <span class="text-capitalize">{{ $item['bank_account'] }}</span>
                        </td>
                        <td>{{ $item['bank_account_number'] }}</td>
                        <td>
                            <span class="text-capitalize">{{ $item['bank_account_owner'] }}</span>
                        </td>
                    </tr>
                @endforeach
            @endcomponent

            @component('admin.components.accordion-table', [
                'title' => 'Detail Cost',
                'headers' => ['Name', 'Total']
            ])
                @if(!empty($prices) && count($prices) > 0)
                    <tr>
                        <td>Cost</td>
                        @if (!is_null($contract))
                            <td>{{ 'Rp. ' . number_format($contract->basic_amount, 0, ',', '.') }}</td>
                        @else
                            <td>{{ $prices['base']['price_total_string'] }}</td>
                        @endif
                    </tr>
                    <tr>
                        <td>Pro Rata</td>
                        @if (!is_null($contract))
                            <td>{{ 'Rp. ' . number_format($contract->billing_rule->first_amount, 0, ',', '.') }}</td>
                        @else
                            <td> - </td>
                        @endif
                    </tr>
                    @foreach ($prices['other'] as $item)
                        <tr>
                            <td>{{ $item['price_label'] }}</td>
                            <td>{{ $item['price_total_string'] }}</td>
                        </tr>
                    @endforeach

                    @slot('tfoot')
                        <tr>
                            <td><b>Fine (*if late payment)</b></td>
                            <td>{{ $prices['fine']['price_total_string'] }}</td>
                        </tr>
                    @endslot
                @endif
            @endcomponent

            @component('admin.components.accordion-table', [
                'title' => 'Status Data Booking',
                'headers' => ['Status', 'Changed By', 'Date'],
                'noData' => 'Status data booking not found'
            ])
                @if(!is_null($bookingUser['statuses']))
                    @foreach ($bookingUser['statuses'] as $status)
                        <tr>
                            <td>{{ $status['status'] }}</td>
                            <td>{{ $status['changed_by'] }}</td>
                            <td>{{ $status['created_at'] }}</td>
                        </tr>
                    @endforeach
                @endif
            @endcomponent

            @component('admin.components.accordion-table', [
                'title' => 'History Data Booking',
                'headers' => ['User Email', 'Key', 'Last Value', 'New Value', 'Created At']
            ])
                @foreach ($revisionables as $revisionable)
                    <tr>
                        <td>{{ $revisionable->user->email ?? "-" }}</td>
                        <td>{{ $revisionable['key'] }}</td>
                        <td>{{ $revisionable['old_value'] }}</td>
                        <td>{{ $revisionable['new_value'] }}</td>
                        <td>{{ $revisionable['created_at'] }}</td>
                    </tr>
                @endforeach
            @endcomponent

            @component('admin.components.accordion-table', [
                'headers' => ['Notes', 'Activity', 'Created At', 'Created By', 'updated At', 'Updated By', 'Deleted At'],
                'collapsable' => false
            ])
                @php
                    $hasNotes = !empty($bookingUser['note']['content']) || !empty($bookingUser['note']['activity']);
                    $isConsultant = !!Auth::user()->consultant;
                @endphp

                @slot('title')
                    Notes &nbsp;
                    @if ($isConsultant)
                        @if ($hasNotes)
                            <button class="btn btn-success btn-sm" onclick="editNote('{{ $bookingUser['note']['activity'] }}', '{{ $bookingUser['note']['content'] }}')">Edit</button>
                        @else
                            <button class="btn btn-success btn-sm" onclick="editNote()">Create</button>
                        @endif
                    @else
                        <button disabled class="btn btn-success btn-sm">{{ $hasNotes ? 'Edit' : 'Create' }}</button>
                    @endif
                @endslot
                <tr>
                    <td>{{ $bookingUser['note']['content'] }}</td>
                    <td>{{ $bookingUser['note']['activity'] }}</td>
                    <td>{{ $bookingUser['note']['created_at'] }}</td>
                    <td>{{ $bookingUser['note']['created_by'] }}</td>
                    <td>{{ $bookingUser['note']['updated_at'] }}</td>
                    <td>{{ $bookingUser['note']['updated_by'] }}</td>
                    <td>-</td>
                </tr>
                @foreach($noteHistories as $note)
                    <tr>
                        <td>{{ $note->content }}</td>
                        <td>{{ $note->activity }}</td>
                        <td>{{ $note->created_at }}</td>
                        <td>{{ $bookingUser['note']['created_by'] }}</td>
                        <td>{{ $note->updated_at }}</td>
                        <td>{{ !is_null($note->consultant) ? $note->consultant->name : '-' }}</td>
                        <td>{{ $note->deleted_at }}</td>
                    </tr>
                @endforeach
            @endcomponent
        </div>
    </div>
@endsection

@section('script')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

    <script>
        function editNote(category = 'survey', content = '') {
            Swal.fire({
                title: 'Type some notes...',
                html: `
                    <label for="swal-select-editnote">Remarks</label>
                    <br />
                    <select id="swal-select-editnote" class="swal2-select" style="margin-top: 4px;">
                        <option value="survey" ${ category === 'survey' ? 'selected' : '' }>Akan survei</option>
                        <option value="will_pay" ${ category === 'will_pay' ? 'selected' : '' }>Akan bayar</option>
                        <option value="paid_directly_to_owner" ${ category === 'paid_directly_to_owner' ? 'selected' : '' }>Sudah bayar ke pemilik</option>
                        <option value="no_response" ${ category === 'no_response' ? 'selected' : '' }>Tidak merespon chat</option>
                        <option value="unreachable_phone_number" ${ category === 'unreachable_phone_number' ? 'selected' : '' }>Nomor HP tidak dapat dihubungi</option>
                        <option value="already_had_another_kost" ${ category === 'already_had_another_kost' ? 'selected' : '' }>Sudah dapat kos lain</option>                
                        <option value="on_discussion" ${ category === 'on_discussion' ? 'selected' : '' }>Masih mempertimbangkan</option>>
                        <option value="kost_under_renovation" ${ category === 'kost_under_renovation' ? 'selected' : '' }>Kos sedang renovasi</option>
                        <option value="kost_full" ${ category === 'kost_full' ? 'selected' : '' }>Kos sudah penuh</option>
                        <option value="tenant_recommends_other_kost" ${ category === 'tenant_recommends_other_kost' ? 'selected' : '' }>Tenant ingin rekomendasi kos lain</option>
                    </select>
                    <br />
                    <label for="swal-input-editnote">Notes</label>
                    <br />
                    <input type="text" id="swal-input-editnote" class="swal2-input" value="${content}" style="margin-top: 4px;" />
                `,
                showCancelButton: true,
                confirmButtonText: 'Save',
                showLoaderOnConfirm: true,
                preConfirm: (value) => {
                    const editnoteCategory = $('#swal-select-editnote').val();
                    const editnoteContent = $('#swal-input-editnote').val();

                    let form = new FormData();
                    form.append('category', editnoteCategory);
                    form.append('content', editnoteContent);

                    let response = fetch("/admin/booking/users/{{ $bookingUser['id'] }}/note", {
                        method: 'POST',
                        body: form
                    })
                    .then(response => {
                        if (response.status) {
                            Swal.fire({
                                icon: 'success',
                                title: 'Success',
                                text: 'Your note has been saved'
                            });

                            window.location.reload();
                        }
                    })
                    .catch(error => {
                        alert(error);
                    });
                },
                allowOutsideClick: () => !Swal.isLoading()
            });
        }
    </script>
@endsection
