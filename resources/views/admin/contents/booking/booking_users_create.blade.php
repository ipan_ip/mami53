@extends('admin.layouts.main')

@section('style')
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" />
    <link href="//cdn.jsdelivr.net/npm/select2-bootstrap-theme@0.1.0-beta.10/dist/select2-bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/booking-form-admin.css') }}">
@endsection

@section('content')
    <div class="box box-default">
        <!-- Box Header -->
        <div class="box-header">
            <div class="col-md-12 text-center">
                <h3 class="box-title"><i class="fa fa-note"></i> {{ $boxTitle }}</h3>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="container">
                <div class="row">
                    <section>
                    <div class="wizard">
                        <h1>Form Booking</h1>
                        <div class="wizard-inner">
                            <div class="connecting-line"></div>
                            <ul class="nav nav-tabs" role="tablist">

                                <li role="presentation" class="active">
                                    <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Step 1">
                                        <span class="round-tab">
                                            <i class="fa fa-home"></i>
                                        </span>
                                    </a>
                                </li>

                                <li role="presentation" class="disabled">
                                    <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Step 2">
                                        <span class="round-tab">
                                            <i class="fa fa-user"></i>
                                        </span>
                                    </a>
                                </li>

                                <li role="presentation" class="disabled">
                                    <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="Step 3">
                                        <span class="round-tab">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                    </a>
                                </li>

                                <li role="presentation" class="disabled">
                                    <a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="Complete">
                                        <span class="round-tab">
                                            <i class="glyphicon glyphicon-ok"></i>
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <form role="form" action="{{ route('admin.booking.users.store') }}" method="POST">
                            <div class="tab-content">
                                <div class="tab-pane active" role="tabpanel" id="step1">
                                    <h3>Rooms</h3>
                                    <p>
                                        <div class="box-body">
                                            <div class="form-group">
                                                <label>Select Room</label>
                                                <div class="input-group">
                                                    <select id="inputRoom" class="form-control select2-single">
                                                    </select>
                                                    <input type="hidden" class="form-control" id="inputRoomId" name="room_id" required="">
                                                    <input type="hidden" class="form-control" id="inputRoomPriceWeekly" name="room_price_weekly" required="">
                                                    <input type="hidden" class="form-control" id="inputRoomPriceMonthly" name="room_price_monthly" required="">
                                                    <input type="hidden" class="form-control" id="inputRoomPriceQuarterly" name="room_price_quarterly" required="">
                                                    <input type="hidden" class="form-control" id="inputRoomPriceSemiannually" name="room_price_semiannually" required="">
                                                    <input type="hidden" class="form-control" id="inputRoomPriceYearly" name="room_price_yearly" required="">

                                                    <input type="hidden" name="session_id" id="inputSesssionID">
                                                </div>
                                                <!-- /.input group -->
                                            </div>
                                        </div>
                                    </p>
                                    <ul class="list-inline pull-right">
                                        <li><button type="button" class="btn btn-primary next-step button-step-1">next</button></li>
                                    </ul>
                                </div>

                                <div class="tab-pane" role="tabpanel" id="step2">
                                    <h3>Identity</h3>
                                    <p>
                                        <div class="box-body">
                                            <div class="form-group">
                                                <label>Name (or search by phone number)</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-user"></i>
                                                    </div>
                                                    <select id="inputTenant" class="form-control select2-single">
                                                    </select>
                                                    <input type="hidden" name="tenant_name" class="form-control" id="inputTenantName">

                                                    <input type="hidden" name="tenant_id" class="form-control" id="inputTenantId">
                                                </div>
                                                <!-- /.input group -->
                                            </div>
                                            <div class="form-group">
                                                <label>Gender</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-venus-mars"></i>
                                                    </div>
                                                    <select class="form-control" name="tenant_gender" id="inputGender">
                                                        <option value="female">Female</option>
                                                        <option value="male">Male</option>
                                                    </select>
                                                </div>
                                                <!-- /.input group -->
                                            </div>
                                            <div class="form-group">
                                                <label>Job</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-wrench"></i>
                                                    </div>
                                                    <input type="text" name="tenant_job" class="form-control" id="inputJob">
                                                </div>
                                                <!-- /.input group -->
                                            </div>
                                            <div class="form-group">
                                                <label>Email</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-send"></i>
                                                    </div>
                                                    <input type="email" name="tenant_email" class="form-control" id="inputEmail" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" oninvalid="alert('Email is not valid');">
                                                </div>
                                                <!-- /.input group -->
                                            </div>
                                            <div class="form-group">
                                                <label>Phone Number</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-phone"></i>
                                                    </div>
                                                    <input type="text" name="tenant_phone_number" class="form-control" id="inputPhoneNumber">
                                                </div>
                                                <!-- /.input group -->
                                            </div>
                                        </div>
                                    </p>
                                    <ul class="list-inline pull-right">

                                        <li><button type="button" class="btn btn-default prev-step">Previous</button></li>
                                        <li><button type="button" class="btn btn-primary next-step duration-step button-step-2">next</button></li>
                                    </ul>
                                </div>

                                <div class="tab-pane" role="tabpanel" id="step3">
                                    <h3>Duration</h3>
                                    <p>
                                        <div class="box-body">
                                            <div class="form-group">
                                                <label>Check In Date</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>
                                                    <input type="text" name="checkin_date" class="form-control" id="inputCheckin">
                                                </div>
                                                <!-- /.input group -->
                                            </div>
                                            <div class="form-group">
                                                <label>Rent Count</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar-check-o"></i>
                                                    </div>
                                                    <select class="form-control" name="rent_count" id="inputRentCount">
                                                    </select>
                                                </div>
                                                <!-- /.input group -->
                                            </div>
                                            <div class="form-group">
                                                <label>Duration of the Lease</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar-check-o"></i>
                                                    </div>
                                                    <select class="form-control" name="duration" id="inputDuration">
                                                        
                                                    </select>
                                                </div>
                                                <!-- /.input group -->
                                            </div>
                                        </div>
                                    </p>
                                    <ul class="list-inline pull-right">
                                        <li><button type="button" class="btn btn-default prev-step">Previous</button></li>
                                        <li><button type="button" class="btn btn-default next-step last-step button-step-3">next</button></li>
                                    </ul>
                                </div>

                                <div class="tab-pane" role="tabpanel" id="complete">
                                    <h3>Preview</h3>
                                    <div class="preview"></div>
                                    <ul class="list-inline pull-right">
                                        <li><button type="button" class="btn btn-default prev-step">Previous</button></li>
                                        <li><button type="submit" class="btn btn-primary btn-info-full next-step">Submit</button></li>
                                    </ul>
                                </div>

                                <div class="clearfix"></div>
                            </div>
                        </form>
                    </div>
                </section>
               </div>
            </div>
        </div>
    </div>

    @section('script')
        <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>
        <script src="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.js"></script>
        <script src="//cdn.jsdelivr.net/npm/jquery-steps@1.1.0/build/jquery.steps.min.js"></script>
        <script src="//cdn.jsdelivr.net/npm/jquery-mask-plugin@1.14.15/dist/jquery.mask.min.js"></script>

        {{-- Rooms Select2 --}}
        <script>
        $(function() {
            $('#inputRoom').select2({
                theme: "bootstrap",
                placeholder: "Room",
                ajax: {
                    url: '{{ url('admin/room/search/api') }}',
                    data: function (params) {
                        return {
                            search: params.term,
                            page: params.page || 1
                        }
                    },
                    processResults: function (data, params) {
                        var page = params.page || 1;
                        if (data.data.length < 1) var total_count = 0;
                        else var total_count = data.data[0].total_count;
                        return {
                            results: $.map(data.data, item => { 
                                return {
                                    id: item.id,
                                    price_weekly: item.price_weekly,
                                    price_monthly: item.price_monthly,
                                    price_quarterly: item.price_quarterly,
                                    price_semiannually: item.price_semiannually,
                                    price_yearly: item.price_yearly,

                                    text: item.id+ ' | ' +item.name
                                }
                            }),
                            pagination: {
                                more: (page * 10) <= total_count
                            }
                        };
                    },
                    cache: true
                }
            });

            // Dropdown listeners
            $("#inputRoom").on('select2:select', e => {
                var selected = e.params.data;
                if (selected) {
                    $('#inputRoomId').val(selected.id);
                    $('#inputRoomPriceWeekly').val(selected.price_weekly);
                    $('#inputRoomPriceMonthly').val(selected.price_monthly);
                    $('#inputRoomPriceQuarterly').val(selected.price_quarterly);
                    $('#inputRoomPriceSemiannually').val(selected.price_semiannually);
                    $('#inputRoomPriceYearly').val(selected.price_yearly);
                }
            });
        });
        </script>

        {{-- Users Select2 --}}
        <script>
        $(function() {
            $('#inputTenant').select2({
                theme: "bootstrap",
                placeholder: "Tenant",
                ajax: {
                    url: '{{ url('admin/users/tenant') }}',
                    data: function (params) {
                        return {
                            search: params.term,
                            page: params.page || 1
                        }
                    },
                    processResults: function (data, params) {
                        var page = params.page || 1;
                        if (data.data.length < 1) var total_count = 0;
                        else var total_count = data.data[0].total_count;
                        return {
                            results: $.map(data.data, item => {
                                return {
                                    id: item.id, 
                                    text: item.id+ ' | ' +item.name
                                }
                            }),
                            pagination: {
                                more: (page * 10) <= total_count
                            }
                        };
                    },
                    cache: true
                }
            });

            // Dropdown listeners
            $("#inputTenant").on('select2:select', e => {
                var selected = e.params.data.id;
                if (selected != '') {
                    applyUser(selected);
                }
            });

            // Function to fetch single user details
            function applyUser(id) {
                $.ajax({
                    type: 'GET',
                    url: '{{ url('admin/users/tenant/') }}' + '/' + id,
                    success: (data) => {
                        $('#inputTenantId').val(data.data.id);
                        $('#inputTenantName').val(data.data.name);
                        $('#inputEmail').val(data.data.email);
                        $('#inputPhoneNumber').val(data.data.phone_number);
                        $('#inputGender').val(data.data.gender);
                        $('#inputJob').val(data.data.job);
                    }
                });
            }
        });
        </script>

        {{-- Duration of The Lease --}}
        <script type="text/javascript">
            var getDuration = function (rentCountType)  {
                $.ajax({
                    type: 'GET',
                    url: '{{ url('/admin/booking/rent-count') }}'+'/'+rentCountType,
                    success: (data) => {
                        $('#inputDuration option').each(function() {
                            $(this).remove();
                        });

                        $.each( data.data, function( i, item ) {
                            var index = i+1;
                            var listItem = "<option value="+index+">"+item+"</option>";
                            $( "#inputDuration" ).append( listItem );
                        });
                    }
                });
            } 

            // Dropdown listeners
            $('#inputRentCount').on('change', function() {
                var rentCountType = $(this).val();
                getDuration(rentCountType);
            });

            // Duration Step
            $('.duration-step').click(function (e) {
                $("#inputRentCount option").remove();

                if ($('#inputRoomPriceWeekly').val() > 0) {
                     $("#inputRentCount").append(GetOption('Weekly', 'weekly'));
                }
                
                if ($('#inputRoomPriceMonthly').val() > 0) {
                     $("#inputRentCount").append(GetOption('Monthly', 'monthly'));
                }

                if ($('#inputRoomPriceQuarterly').val() > 0) {
                    $("#inputRentCount").append(GetOption('Quarterly', 'quarterly'));
                }

                if ($('#inputRoomPriceSemiannually').val() > 0) {
                    $("#inputRentCount").append(GetOption('Semiannually', 'semiannually'));
                }

                if ($('#inputRoomPriceYearly').val() > 0) {
                    $("#inputRentCount").append(GetOption('Yearly', 'yearly'));
                }
            });

            function GetOption(text, value) {
                return "<option value = '" + value + "'>" + text + "</option>"
            }

            $('.button-step-2').click(function (e) {
                var rentCountType = $('#inputRentCount').val();
                getDuration(rentCountType, true);
            });
        </script>

        {{-- Steps --}}
        <script type="text/javascript">
            $(document).ready(function () {
                //Initialize tooltips
                $('.nav-tabs > li a[title]').tooltip();
                
                //Wizard
                $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

                    var $target = $(e.target);
                
                    if ($target.parent().hasClass('disabled')) {
                        return false;
                    }
                });

                $(".button-step-1").click(function (e) {
                    if (!$('#inputRoomId').val() || !$('#inputRoom').val()) {
                        Swal.fire({
                            type: 'error',
                            title: "Failed!",
                            html: '<strong> Room is Required</strong>'
                        });
                    } else {
                        var $active = $('.wizard .nav-tabs li.active');
                        $active.next().removeClass('disabled');
                        nextTab($active);
                    }
                });

                $(".button-step-2").click(function (e) {
                    if (!$('#inputTenantId').val() || !$('#inputTenant').val()) {
                        Swal.fire({
                            type: 'error',
                            title: "Failed!",
                            html: '<strong> Tenant is Required</strong>'
                        });
                    } else if (!$('#inputJob').val()) {
                        Swal.fire({
                            type: 'error',
                            title: "Failed!",
                            html: '<strong> Job is Required</strong>'
                        });
                    } else if (!$('#inputEmail').val()) {
                        Swal.fire({
                            type: 'error',
                            title: "Failed!",
                            html: '<strong> Email is Required</strong>'
                        });
                    } else if (!$('#inputPhoneNumber').val()) {
                        Swal.fire({
                            type: 'error',
                            title: "Failed!",
                            html: '<strong> Phone Number is Required</strong>'
                        });
                    } else {
                        var $active = $('.wizard .nav-tabs li.active');
                        $active.next().removeClass('disabled');
                        nextTab($active);
                    }
                });


                $(".button-step-3").click(function (e) {
                    if (!$('#inputCheckin').val()) {
                        Swal.fire({
                            type: 'error',
                            title: "Failed!",
                            html: '<strong> Check-In Date is Required</strong>'
                        });
                    } else if(!$('#inputDuration').val()) {
                        Swal.fire({
                            type: 'error',
                            title: "Failed!",
                            html: '<strong> Duration is Required</strong>'
                        });
                    } else {
                        var $active = $('.wizard .nav-tabs li.active');
                        $active.next().removeClass('disabled');
                        nextTab($active);
                    }
                });

                $(".prev-step").click(function (e) {
                    var $active = $('.wizard .nav-tabs li.active');
                    prevTab($active);

                });

                // Last Step - Preview Data
                $(".last-step").click(function (e) {
                    $("#preview-data").remove();

                    var template = "<ul class='list-group' id='preview-data'>"+
                                "<li class='list-group-item'> Tenant Name : <b>"+$("#inputTenantName").val()+"</b>"+
                                "<li class='list-group-item'> Tenant Email : <b>"+$("#inputEmail").val()+"</b>"+
                                "<li class='list-group-item'> Tenant Job : <b>"+$("#inputJob").val()+"</b>"+
                                "<li class='list-group-item'> Tenant Phone Number : <b>"+$("#inputPhoneNumber").val()+"</b>"+
                                "<li class='list-group-item'> Tenant Gender : <b>"+$("#inputGender").val()+"</b>"+
                                "<li class='list-group-item'> Checkin Date : <b>"+$("#inputCheckin").val()+"</b>"+
                                "<li class='list-group-item'> Rent Count Type : <b>"+$("#inputRentCount").val()+"</b>"+
                                "<li class='list-group-item'> Duration : <b>"+$("#inputDuration option:selected").text()+"</b>"+
                                "<li class='list-group-item'> Room Name : <b>"+$("#inputRoom option:selected").text()+"</b>"
                    $( ".preview" ).append(template);
                });
            });

            function nextTab(elem) {
                $(elem).next().find('a[data-toggle="tab"]').click();
            }
            function prevTab(elem) {
                $(elem).prev().find('a[data-toggle="tab"]').click();
            }
        </script>

        {{-- Datepicker --}}
        <script type="text/javascript">
            //Date picker
            $('#inputCheckin').datepicker({
              autoclose: true,
              dateFormat: 'yy-mm-dd'
            })

            var moedata = localStorage.getItem('MOE_DATA');
            var parse = JSON.parse(moedata)
            $('#inputSesssionID').val(parse.SESSION.sessionKey);
        </script>
    @endsection
@endsection