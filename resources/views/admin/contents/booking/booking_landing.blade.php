@extends('admin.layouts.main')

@section('style')
<style>
.table>tbody>tr>td {
	vertical-align: middle;
}

.font-large {
	font-size: 1.5em;
}

.font-semi-large {
	font-size: 1.15em;
}

.font-grey {
	color: #9E9E9E;
}

.dropdown-menu>li>a:hover {
	color: #333
}

.label-grey {
	background-color: #CFD8DC;
}


/* Sweetalert */

.swal-wide {
	width: 850px !important;
	/* font-size: 14px!important; */
}

.swal-shown {
	height: auto!important;
}

.custom-swal {
	z-index: 10000!important;
}

.custom-wide-swal {
	width:850px !important;
}


/* Wixard twekas */

.wizard>.content>.body {
	width: 100% !important;
	padding: 0 !important;
}


/* Select2 tweak */

.input-group .select2-container,
.form-group .select2-container {
	position: relative !important;
	z-index: 2;
	float: left !important;
	width: 100% !important;
	margin-bottom: 0 !important;
	display: table !important;
	table-layout: fixed !important;
}


/* Summernote tweak */

.note-group-select-from-files {
	display: none;
}


/* Modal tweak */

.modal-open {
	overflow: hidden;
	position: fixed;
	width: 100%;
}


/* Centering the modal */

.modal {
	text-align: center;
	padding: 0!important;
}

.modal:before {
	content: '';
	display: inline-block;
	height: 100%;
	vertical-align: middle;
	margin-right: -4px;
}

.modal-dialog {
	display: inline-block;
	text-align: left;
	vertical-align: middle;
}

#map-canvas, #view-map-canvas {
	min-height: 400px;
}

/* Sortable tweak */
#sortable tr {
    cursor: move!important;
}
</style>
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" />
<link href="//cdn.jsdelivr.net/npm/select2-bootstrap-theme@0.1.0-beta.10/dist/select2-bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/jquery-steps@1.1.0/demo/css/jquery.steps.min.css">
<link rel="stylesheet" href="/assets/vendor/summernote/summernote.css">
<link rel="stylesheet" href="{{ mix_url('dist/vendor/leaflet/leaflet.css') }}">
<!-- <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/smartwizard@4.3.1/dist/css/smart_wizard_theme_arrows.min.css"> -->
@endsection

@section('content')

    @include('admin.contents.booking.partials.config_modal')
    @include('admin.contents.booking.partials.add_modal')
    @include('admin.contents.booking.partials.map_modal')

	<!-- table -->
    <div class="box box-solid box-default">

        <!-- Box Header -->
        <div class="box-header">
            <div class="col-md-12 text-center">
                <h3 class="box-title"><i class="fa fa-map-o"></i> Landing Booking <small> Control Panel</small></h3>
            </div>
        </div>
        <!-- /.box-header -->

        <div class="box-body no-padding">
            
            <!-- Top bar -->
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default form-inline" style="padding-left: 10px;padding-right: 10px;">
                    <!-- Add button -->
                    <div class="col-md-6 bg-default" style="padding-bottom:10px;">
                        <a href="#" id="btn-config" class="btn btn-success btn-md" data-title="Configure Main Landing"><i class="fa fa-cog"></i> Parent Landing Setting</a>
                        <a href="#" id="btn-add" class="btn btn-primary btn-md" data-title="Add New Booking Landing"><i class="fa fa-plus"></i> Add Landing</a>
                        <a href="/admin/booking/landing/specific" id="btn-add" class="btn btn-primary btn-md" data-title="Add New Booking Landing"><i class="fa fa-plus"></i> Add Landing Specific</a>
                    </div>
                    <!-- Search filters -->
                    <div class="col-md-6 bg-default" style="padding-bottom:10px;text-align:right">
                        <form action="" method="get">
                            <input type="text" name="q" class="form-control input-sm"  placeholder="Cari title / keyword"  autocomplete="off" value="{{ request()->input('q') }}"> 
                            <button class="btn btn-primary btn-md" id="buttonSearch"><i class="fa fa-search">&nbsp;</i>Search
                            </button>
                        </form>
                    </div>
                </div>
            </div>

            <!-- Table -->
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th class="text-center"></th>
                        <th></th>
                        <th class="text-center">Active?</th>
                        <th class="text-center">Default?</th>
                        <th class="text-center">Jml Kost Booking</th>
                        <th class="text-center">Meta Data</th>
                        <th class="text-center">Template</th>
                        <th class="text-center">Area</th>
                        <th class="text-center">Update</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody id="sortable">
                @foreach($landings as $landing)
                    <tr id="{{ $landing->id }}">
                        <td class="text-center" style="margin-left:10px!important;"><i class="fa fa-arrows"></i></td>
                        @if ($landing->is_active == 1 && $landing->is_default == 1)
                        <td class="text-success">
                        @elseif ($landing->is_active == 0)
                        <td class="font-grey">
                        @else
                        <td>
                        @endif
                            <strong>{{ $landing->title }}</strong>
                            <br/>
                            @if ($landing->is_active == 1 && $landing->is_default == 1)
                                @if ($landing->type == 'booking-specific')
                                    <small><a class="text-success" href="{{ env('APP_URL') }}/booking/{{ $landing->slug }}" target="_blank"><i class="fa fa-cloud"></i> /booking/{{ $landing->landing_parent->slug }}/{{ $landing->slug }}</a></small>
                                @else
                                    <small><a class="text-success" href="{{ env('APP_URL') }}/booking/{{ $landing->slug }}" target="_blank"><i class="fa fa-cloud"></i> /booking/{{ $landing->slug }}</a></small>
                                @endif
                            @else
                                @if ($landing->type == 'booking-specific')
                                    <small><i class="fa fa-cloud"></i> /booking/{{ $landing->landing_parent->slug }}/{{ $landing->slug }}</small>
                                @else
                                    <small><i class="fa fa-cloud"></i> /booking/{{ $landing->slug }}</small>
                                @endif
                            @endif
                        </td>
                        <td class="text-center">
                            @if ($landing->is_active == 1)
                                <i class="fa fa-check text-success font-large"></span>
                            @else
                                <i class="fa fa-ban text-danger font-large"></span>
                            @endif
                        </td>
                        <td class="text-center">
                            @if ($landing->is_default == 1)
                                <i class="fa fa-check text-success font-large"></span>
                            @else
                                <i class="fa fa-ban text-danger font-large"></span>
                            @endif
                        </td>
                        <td class="text-center font-semi-large"><strong>{{ $landing->room_count }}</strong></td>
                        <td class="text-center">
                            <a href="#" class="btn btn-xs btn-primary actions" data-action="view-meta-data" data-title="Meta Data: {{ $landing->title }}" data-landing="{{ $landing }}">
                                <i class="fa fa-tags"></i> Lihat Meta Data
                            </a>
                        </td>
                        <td class="text-center">
                            <a href="#" class="btn btn-xs btn-primary actions" data-action="view-template" data-title="Template Article: {{ $landing->title }}" data-landing="{{ $landing }}">
                                <i class="fa fa-list-alt"></i> Lihat Template
                            </a>
                        </td>
                        <td class="text-center">
                            <a href="#" class="btn btn-xs btn-primary actions" data-action="view-area" data-title="Landing Area: {{ $landing->title }}" data-landing="{{ $landing }}">
                                <i class="fa fa-map-marker"></i> Lihat Area
                            </a>
                        </td>
                        <td class="text-center">{{ date('j F Y', strtotime($landing->updated_at)) }}<br/><small>@ {{ date('H:i', strtotime($landing->updated_at)) }}</small></td>
                        <td class="table-action-column" style="padding-right:15px!important;">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-cogs"></i> Actions
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                    @if ($landing->is_active == 0)
                                        <li>
                                            <a href="#" class="actions" data-action="activate" data-landing="{{ $landing }}"><i class="fa fa-arrow-up"></i>Activate Landing</a>
                                        </li>
                                    @else
                                        <li>
                                            <a href="#" class="actions" data-action="deactivate" data-landing="{{ $landing }}"><i class="fa fa-arrow-down"></i>Deactivate Landing</a>
                                        </li>
                                    @endif
                                    @if ($landing->is_default == 0 && $landing->is_active == 1)
                                    <li>
                                        <a href="#" class="actions" data-action="set" data-landing="{{ $landing }}"><i class="fa fa-asterisk"></i> Set as Default Landing</a>
                                    </li>
                                    @endif
                                    <li>
                                        @if ($landing->type == "booking-specific")
                                            <a href="/admin/booking/landing/specific/{{$landing->id}}"><i class="fa fa-edit"></i> Edit Landing specific</a>
                                        @else
                                            <a href="#" class="actions" data-action="update" data-landing="{{ $landing }}"><i class="fa fa-edit"></i> Edit Landing</a>
                                        @endif
                                    </li>
                                    @if ($landing->is_active == 0)
                                    <li>
                                        <a href="#" class="actions" data-action="remove" data-landing="{{ $landing }}"><i class="fa fa-trash-o"></i> Hapus Landing</a>
                                    </li>
                                    @endif
                                </ul>
                            </div>
                        </td>
                    </tr> 
                @endforeach
                </tbody>
                
            </table>
            
        </div><!-- /.box-body -->

        <div class="box-body no-padding row">
            <div class="col-md-12 text-center">
            {{ $landings->appends(Request::except('page'))->links() }}
            </div>
        </div>
                      
    </div><!-- /.box -->

@endsection

@section('script')
<script src="/assets/vendor/summernote/summernote.min.js"></script>
<script src="/assets/vendor/summernote/plugin/summernote-image-attributes.js"></script>
<script src="//maps.googleapis.com/maps/api/js?libraries=places&language=id&key={{ENV('MIX_GOOGLE_MAPS_JAVASCRIPT_API_KEY')}}"></script>
<script src="{{ mix_url('dist/vendor/leaflet/leaflet.js') }}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/jquery-steps@1.1.0/build/jquery.steps.min.js"></script>
<script>

// Sweetalert functions
function triggerAlert(type, message) {
    Swal.fire({
        type: type,
        customClass: {
            container: 'custom-swal'
        },
        title: message
    });
}

function triggerLoading(message) {
    Swal.fire({
        title: message,
        customClass: {
            container: 'custom-swal'
        },
        allowEscapeKey: false,
        allowOutsideClick: false,
        onOpen: () => {
            swal.showLoading();
        }
    });
}

// Fn to calculate map's centerpoint
function getCenterPoint(data) {
    var option = JSON.parse(data);
    var lng = (parseFloat(option.location[0][0]) + parseFloat(option.location[1][0])) / 2;
    var lat = (parseFloat(option.location[0][1]) + parseFloat(option.location[1][1])) / 2;
    var center = [lng, lat];
    return center;
}

// Fn for capitalising words
function titleCase(str) {
   var splitStr = str.toLowerCase().split(' ');
   for (var i = 0; i < splitStr.length; i++) {
       // You do not need to check if i is larger than splitStr length, as your for does that for you
       // Assign it back to the array
       splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);     
   }
   // Directly return the joined string
   return splitStr.join(' '); 
}
$(function() {
    var token = '{{ csrf_token() }}';

    // Initial map instance container
    var map, view_map;

    var selectedLatitude,
        selectedLongitude,
        selectedCity;
    var _landing;
    var _editing = false;

    var selectedArea = [];

    $(document).on('click', '.actions', (e) => {
        e.preventDefault();
        var action = $(e.currentTarget).data('action');
        _landing = $(e.currentTarget).data('landing');

        // View acticle template
        if (action == 'view-meta-data') {
            var swal_content = "<br/><div class='text-left'>" + 
                "<h5><strong>Meta Keyword</strong></h5>" +
                "<pre>" + _landing.keyword + "</pre>" +
                "<br/><h5><strong>Meta Description</strong></h5>" +
                "<pre>" + _landing.description + "</pre>" +
                "</div><br/><br/>";

            Swal.fire({
                title: $(e.currentTarget).data('title'),
                customClass: 'custom-wide-swal',
                html: swal_content,
                showCloseButton: true,
                confirmButtonText: 'CLOSE'
            });
        }

        // View acticle template
        if (action == 'view-template') {
            var swal_content = "<div class='text-left'><pre>" + _landing.article + "</pre></div>";
            Swal.fire({
                title: $(e.currentTarget).data('title'),
                customClass: 'custom-wide-swal',
                html: swal_content,
                showCloseButton: true,
                confirmButtonText: 'CLOSE'
            });
        }

        // View option
        if (action == 'view-area') {
            $('#mapModal').modal('show');
        }

        // Updating score
        if (action == 'activate' || action == 'deactivate') {
            if (action == 'activate') {
                var title = 'Aktifkan Landing?';
                var text = false;
                var resTitle = 'Berhasil mengaktifkan landing!';
                var type = 'question';
                var confirmButtonText = 'Yes!'
                var status = 1;
            } else {
                if (_landing.is_default == '1') {
                    var title = 'Non-aktifkan Landing Default?';
                    var text = '<strong>Landing ini adalah landing default untuk URL /booking/' + _landing.slug + '".</strong><br/><br/>Anda harus set landing lain dalam satu kota yang sama SEBAGAI DEFAULT. Jika tidak, maka URL tidak akan bisa diakses!<br/><br/>Yakin untuk melanjutkan?';
                    var type = 'warning';
                    var confirmButtonText = 'Yes, deactivate now!'
                } else {
                    var title = 'Non-aktifkan Landing?';
                    var text = false;
                    var type = 'warning';
                    var confirmButtonText = 'Yes'
                }
                var resTitle = 'Berhasil menonaktifkan landing!';
                var status = 0;
            }

            Swal.fire({
                title: title,
                html: text,
                type: type,
                showCancelButton: true,
                showLoaderOnConfirm: true,
                confirmButtonText: confirmButtonText,
                preConfirm: (val) => {
                    return $.ajax({
                        type: 'POST',
                        url: "/admin/booking/landing/activate",
                        dataType: 'json',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: {
                            'id': _landing.id,
                            'slug': _landing.slug,
                            'status': status,
                            '_token': token
                        },
                        success: (response) => {
                            if (!response.success) {
                                Swal.insertQueueStep({
                                    type: 'error',
                                    title: response.message
                                });
                                return;
                            }
                        },
                        error: (error) => {
                            Swal.insertQueueStep({
                                type: 'error',
                                title: 'Error: ' + error.message
                            });
                        }
                    });
                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                if (result.value) {
                    Swal.fire({
                        type: 'success',
                        title: resTitle,
                        text: "Halaman akan di-refresh setelah Anda klik OK",
                        onClose: () => {
                            window.location.reload();
                        }
                    });
                }
            })
        }

        // Set as deafult landing
        if (action == 'set') {
            Swal.fire({
                title: 'Set sebagai default landing?',
                type: 'question',
                showCancelButton: true,
                showLoaderOnConfirm: true,
                confirmButtonText: 'Yes',
                preConfirm: (val) => {
                    return $.ajax({
                        type: 'POST',
                        url: "/admin/booking/landing/set",
                        dataType: 'json',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: {
                            'id': _landing.id,
                            'slug': _landing.slug,
                            '_token': token
                        },
                        success: (response) => {
                            if (!response.success) {
                                Swal.insertQueueStep({
                                    type: 'error',
                                    title: response.message
                                });
                                return;
                            }
                        },
                        error: (error) => {
                            Swal.insertQueueStep({
                                type: 'error',
                                title: 'Error: ' + error.message
                            });
                        }
                    });
                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                if (result.value) {
                    Swal.fire({
                        type: 'success',
                        title: "Berhasil set default landing baru",
                        text: "Halaman akan di-refresh setelah Anda klik OK",
                        onClose: () => {
                            window.location.reload();
                        }
                    });
                }
            })
        }

        // Updating score
        if (action == 'update') {
            _editing = true;
            $('#addModal').modal('show');
        }

        // Removing landing data
        if (action == 'remove') {
            Swal.fire({
                title: 'Hapus Landing?',
                type: 'warning',
                buttonsStyling: false,
                confirmButtonClass: 'btn btn-danger',
                showCancelButton: true,
                cancelButtonClass: 'btn btn-default',
                showLoaderOnConfirm: true,
                confirmButtonText: 'Yes, remove it!',
                preConfirm: (val) => {
                    return $.ajax({
                        type: 'POST',
                        url: "/admin/booking/landing/remove",
                        dataType: 'json',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: {
                            'id': _landing.id,
                            '_token': token
                        },
                        success: (response) => {
                            if (!response.success) {
                                Swal.insertQueueStep({
                                    type: 'error',
                                    title: response.message
                                });
                                return;
                            }
                        },
                        error: (error) => {
                            Swal.insertQueueStep({
                                type: 'error',
                                title: 'Error: ' + error.message
                            });
                        }
                    });
                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                if (result.value) {
                    Swal.fire({
                        type: 'success',
                        title: "Landing telah berhasil dihapus",
                        text: "Halaman akan di-refresh setelah Anda klik OK",
                        onClose: () => {
                            window.location.reload();
                        }
                    });
                }
            })
        }

    });

    // Modal triggers
    $('#btn-add').on('click', (e) => {
        e.preventDefault();
        $('#addModal').modal('show');
    });

    $('#btn-config').on('click', (e) => {
        e.preventDefault();
        $('#configModal').modal('show');
    });

    // Add modal events
    $('#addModal')
        .on("show.bs.modal", (e) => {
            $('#addModal').data('bs.modal').options.keyboard = false;
            $('#addModal').data('bs.modal').options.backdrop = 'static';

            var landingData, landingOption;

            if (_editing) {
                landingData = _landing;
                landingOption = JSON.parse(_landing.option);

                // generate center point values
                var centerPoint = getCenterPoint(_landing.option);
                selectedLatitude = centerPoint[1];
                selectedLongitude = centerPoint[0];
                
                var cleanedCity = _landing.slug.replace(/-/g, ' ');
                selectedCity = titleCase(cleanedCity);
            } else {
                // initial values
                selectedLatitude = -7.7858485;
                selectedLongitude = 110.3680087;
                selectedCity = 'Yogyakarta';
            }

            // initialize wizard
            var wizardEl = $(document).find('#wizard-for-child');
            var wizard = wizardEl.steps({
                headerTag: "h3",
                bodyTag: "section",
                titleTemplate: '<span class="font-semi-large">#index#. #title#</span>',
                transitionEffect: "slide",
                saveState: true,
                preloadContent: false,
                autoFocus: true,
                enableContentCache: false,
                transitionEffectSpeed: 100,

                onInit: () => {
                    // initiate select2 dropdown
                    var selector = $('#citySelector');
                    selector.select2({
                        theme: "bootstrap",
                        language: {
                            searching: function() {
                                return "Sedang mengambil daftar kota...";
                            }
                        },
                        minimumResultsForSearch: -1,
                        dropdownParent: $("#addModal")
                    });

                    // Fetching selector options
                    $.ajax({
                        type: 'GET',
                        dataType: 'json',
                        async: false,
                        url: '/admin/booking/landing/ajax'
                    }).then((response) => {
                        // compile the option and append to Select2
                        $.each(response, (index, data) => {
                            if (_editing && selectedCity != '') {
                                if (selectedCity == data.text) {
                                    var option = new Option(data.text, data.id, true, true);
                                } else {
                                    var option = new Option(data.text, data.id, false, false);
                                }
                            } else {
                                var option = new Option(data.text, data.id, false, false);
                            }
                            selector.append(option).trigger('change');
                        })
                    })

                    // value assignation
                    if (_editing) {
                        var option = JSON.parse(_landing.option);
                        $('#landingTitle').val(option.title);
                        $('#landingKeyword').val(_landing.keyword);
                        $('#landingDescription').val(_landing.description);
                    }

                },

                onStepChanging: (e, currentIndex, newIndex) => {

                    // if moving to section "Area"
                    if (currentIndex == 0 && newIndex == 1) {
                        // validate "keyword" value
                        if ($('#landingTitle').val() == '') {
                            triggerAlert('error', "Masukkan Title!");
                            $('#landingTitle').focus();
                            return false;
                        }

                        // validate "slug" value
                        if ($('#landingKeyword').val() == '') {
                            triggerAlert('error', "Masukkan Meta Keyword!");
                            $('#landingKeyword').focus();
                            return false;
                        }

                        // validate "slug" value
                        if ($('#landingDescription').val() == '') {
                            triggerAlert('error', "Masukkan Meta Description!");
                            $('#landingDescription').focus();
                            return false;
                        }

                        // validate citySelector value
                        var selected = $("#citySelector option:selected").text();
                        if (!selected) {
                            triggerAlert('error', "Pilih kota terlebih dahulu!");
                            
                            return false;
                        }

                        if (selectedCity != selected || (_editing && selected != titleCase(landingData.slug.replace(/-/g, ' ')))) {
                            triggerLoading('Rendering map...');
                            console.log('Rendering map...');

                            // resetting markers location
                            selectedArea = [];

                            // Get city's gelocation and pin them down!
                            $.ajax({
                                    url: "/admin/booking/landing/geocode",
                                    type: "post",
                                    dataType: 'json',
                                    async: false,
                                    timeout: 30000,
                                    data: {
                                        city: selected
                                    }
                                })
                                .done(function(response, textStatus, jqXHR) {
                                    // save to a global variable
                                    if (response.latitude != null && response.longitude != null) {
                                        selectedLatitude = response.latitude;
                                        selectedLongitude = response.longitude;
                                        selectedCity = selected;

                                        if (Swal.isVisible()) Swal.close();
                                    } else {
                                        Swal.hideLoading();
                                        triggerAlert('warning', "Tidak dapat mendapatkan geolocation. Silahkan atur secara manual.");
                                    }
                                })
                                .fail(function(jqXHR, textStatus, errorThrown) {
                                    // Log the error to the console
                                    console.error(
                                        "The following error occurred: " +
                                        textStatus, errorThrown
                                    );

                                    Swal.hideLoading();
                                    triggerAlert('error', "The following error occurred: " + textStatus);
                                });
                        } else {
                            if (_editing) {
                                selectedArea = [
                                    {
                                        lat: landingOption.location[1][1],
                                        lng: landingOption.location[1][0]
                                    }, 
                                    {
                                        lat: landingOption.location[0][1],
                                        lng: landingOption.location[0][0]
                                    }
                                ]
                            }
                        }

                    }

                    // if moving from section "Area" to "Description"
                    if (currentIndex == 1 && newIndex == 2) {
                        // Save map as image
                        // leafletImage(map, function(err, canvas) {
                        //     console.log(canvas);
                        // });
                    }

                    return true;
                },

                onStepChanged: (e, currentIndex, priorIndex) => {
                    // section "Details"
                    if (currentIndex == 0) {
                        // 
                    }

                    // section "Area"
                    if (currentIndex == 1) {

                        /*
                        MAP FUNCTIONS
                        */
                        function addMarkerListener(map, marker, pos) {
                            marker.on('dragend', function(evt) {
                                var latlng = evt.target.getLatLng();

                                var lat = latlng.lat;
                                var lng = latlng.lng;

                                selectedArea[parseInt(pos) - 1] = {
                                    lat: lat,
                                    lng: lng
                                };

                                $('#inputLatitude' + pos).val(lat);
                                $('#inputLongitude' + pos).val(lng);
                            });
                        }

                        // values assignation
                        if (_editing) {
                            console.log(selectedArea, landingOption.location);
                            $('#inputLatitude1').val(landingOption.location[1][1]);
                            $('#inputLongitude1').val(landingOption.location[1][0]);
                            $('#inputLatitude2').val(landingOption.location[0][1]);
                            $('#inputLongitude2').val(landingOption.location[0][0]);
                        }

                        /*
                        ============= LeafletJs ============= 
                        */
                        // destroy map instance if already existed
                        if (map !== undefined && map !== null) {
                            map.remove();
                        }

                        var centerPos = {
                            lat: selectedLatitude,
                            lng: selectedLongitude
                        };

                        map = L.map('map-canvas', {
                            center: centerPos,
                            zoom: 12,
                            minZoom: 10,
                            maxZoom: 13
                        });

                        var tiles = new L.tileLayer("{{ config('services.osm.host') }}/{z}/{x}/{y}.png", {
                            attribution: '&copy; 2019 Mamikos.com'
                        }).addTo(map);

                        var marker = [];
                        var markerImage = [];
                        var position = [];

                        // marker kiri bawah
                        markerImage[0] = "{{ asset('assets/icons/kiribawah.png') }}";

                        if (!_editing) {
                            // check if markers position is changed
                            if (selectedArea[0] != undefined && 
                                (
                                    selectedArea[0].lat != $('#inputLatitude2').val() || 
                                    selectedArea[0].lng != $('#inputLongitude2').val()
                                )
                            ) {
                                console.log('use existing marker');
                                position[0] = {
                                    'lat': selectedArea[0].lat,
                                    'lng': selectedArea[0].lng
                                }
                            } else {
                                console.log('generating new marker');
                                position[0] = {
                                    'lat': centerPos.lat - 0.05,
                                    'lng': centerPos.lng - 0.05
                                }
                            }
                        } else {
                            // check if markers position is changed
                            if (selectedArea[0].lat != $('#inputLatitude2').val() || 
                                selectedArea[0].lng != $('#inputLongitude2').val()
                            ){
                                console.log('Using existing marker1');
                                position[0] = {
                                    'lat': selectedArea[0].lat,
                                    'lng': selectedArea[0].lng
                                }
                            } else {
                                console.log('Generating new marker1');
                                position[0] = {
                                    'lat': centerPos.lat - 0.05,
                                    'lng': centerPos.lng - 0.05
                                }
                            }
                        }
                        

                        marker[0] = new L.Marker(position[0], {
                            draggable: true,
                            icon: new L.icon({
                                iconUrl: markerImage[0],
                                iconSize: [60, 57],
                                iconAnchor: [30, 57]
                            })
                        }).addTo(map);

                        // set for 1st time
                        if (!_editing) {
                            $('#inputLatitude1').val(position[0].lat);
                            $('#inputLongitude1').val(position[0].lng);
                        }

                        addMarkerListener(map, marker[0], '1');

                        // marker kanan atas
                        markerImage[1] = "{{ asset('assets/icons/kananatas.png') }}";

                        if (!_editing) {
                            // check if markers position is changed
                            if (selectedArea[1] != undefined && 
                                (
                                    selectedArea[1].lat != $('#inputLatitude1').val() || 
                                    selectedArea[1].lng != $('#inputLongitude1').val()
                                )
                            ) {
                                console.log('use existing marker2');
                                position[1] = {
                                    'lat': selectedArea[1].lat,
                                    'lng': selectedArea[1].lng
                                }
                            } else {
                                console.log('generating new marker2');
                                position[1] = {
                                    'lat': centerPos.lat + 0.05,
                                    'lng': centerPos.lng + 0.05
                                }
                            }
                        } else {
                            // check if markers position is changed
                            if (selectedArea[1].lat != $('#inputLatitude1').val() || 
                                selectedArea[1].lng != $('#inputLongitude1').val()
                            ) {
                                console.log('Using existing marker2');
                                position[1] = {
                                    'lat': selectedArea[1].lat,
                                    'lng': selectedArea[1].lng
                                }
                            } else {
                                console.log('Generating new marker2');
                                position[1] = {
                                    'lat': centerPos.lat + 0.05,
                                    'lng': centerPos.lng + 0.05
                                }
                            }
                        }

                        marker[1] = new L.Marker(position[1], {
                            draggable: true,
                            icon: new L.icon({
                                iconUrl: markerImage[1],
                                iconSize: [60, 57],
                                iconAnchor: [30, 57]
                            })
                        }).addTo(map);

                        // set for 1st time
                        if (!_editing) {
                            $('#inputLatitude2').val(position[1].lat);
                            $('#inputLongitude2').val(position[1].lng);
                        }

                        addMarkerListener(map, marker[1], '2');

                        map.scrollWheelZoom.disable();

                        /* 
                        ============= End of LeafletJs ============= 
                        */

                    }

                    // section "Description"
                    if (currentIndex == 2) {
                        $('#article').summernote({
                            height: 300,
                            toolbar: [
                                ['style', ['bold', 'italic', 'underline', 'clear']],
                                ['font', ['strikethrough', 'superscript', 'subscript']],
                                ['color', ['color']],
                                ['para', ['ul', 'ol', 'paragraph']],
                                ['insert', ['table', 'hr']],
                                ['misc', ['codeview']]
                            ],
                            imageAttributes: {
                                icon: '<i class="note-icon-pencil"/>',
                                removeEmpty: false, // true = remove attributes | false = leave empty if present
                                disableUpload: true // true = don't display Upload Options | Display Upload Options
                            }
                        });

                        if (_editing) {
                            // Value assignation
                            $('#article').summernote('code', landingOption.article);
                        }
                    }
                },

                onFinishing: () => {
                    // validate "slug" value
                    if ($('#description').val() == '') {
                        triggerAlert('error', "Isikan deskripsi");
                        return false;
                    }

                    return true;
                },

                onFinished: () => {
                    var datastring = $("#add-form").serialize();

                    if (_editing) {
                        // append landing ID
                        var idString = '&id=' + landingData.id;
                        datastring += idString;

                        var url = "/admin/booking/landing/update";
                    } else {
                        var url = "/admin/booking/landing";
                    }
                    
                    Swal.fire({
                        type: 'question',
                        customClass: {
                            container: 'custom-swal'
                        },
                        title: 'Simpan data landing?',
                        showCancelButton: true,
                        confirmButtonText: 'Submit',
                        showLoaderOnConfirm: true,
                        preConfirm: () => {
                            return $.ajax({
                                type: 'POST',
                                url: url,
                                dataType: 'json',
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                data: datastring,
                                success: (response) => {
                                    if (!response.success) {
                                        triggerAlert('error', response.message);
                                        return;
                                    }

                                    triggerAlert('success', 'Landing Berhasil Disimpan');
                                },
                                error: (error) => {
                                    triggerAlert('error', error.message);
                                }
                            });
                        },
                        allowOutsideClick: () => !Swal.isLoading()
                    }).then((result) => {
                        if (!result.dismiss) {
                            $('#addModal').modal('toggle');

                            Swal.fire({
                                type: 'success',
                                customClass: {
                                    container: 'custom-swal'
                                },
                                title: "Berhasil menyimpan data landing baru!",
                                text: "Halaman akan di-refresh setelah Anda klik OK",
                                onClose: () => {
                                            
                                    window.location.reload();
                                }
                            });
                        }

                    })
                }
            });

            // End of wizard events
        })
        .on("hidden.bs.modal", (e) => {
            // destroy selector
            $('#citySelector').empty().select2('destroy');

            // destroy Summernote
            $('#article').summernote('destroy');

            // destroy wizard
            $('#wizard-for-child').steps('destroy');

            // finish editing
            _editing = false;
            selectedCity = '';
            landingOption = '';
        });

    // Config modal events
    $('#configModal')
        .on("show.bs.modal", (e) => {
            $('#configModal').data('bs.modal').options.keyboard = false;
            $('#configModal').data('bs.modal').options.backdrop = 'static';

            // getting parent landing data
            var _landing;
            $.ajax({
                type: 'GET',
                dataType: 'json',
                async: false,
                url: '/admin/booking/landing/parent',
                success: (response) => {
                    if (!response) {
                        Swal.fire({
                            type: 'error',
                            title: "Gagal mengambil data! Silakan refresh halam dan coba lagi.",
                            customClass: {
                                container: 'custom-swal'
                            }
                        });

                        return;
                    }

                    _landing = response;
                },
                error: (error) => {
                    Swal.fire({
                        type: 'error',
                        title: 'Error: ' + error.message,
                        customClass: {
                            container: 'custom-swal'
                        }
                    });
                }
            })

            // initialize wizard
            var wizardEl = $(document).find('#wizard-for-parent');
            var wizard = wizardEl.steps({
                headerTag: "h3",
                bodyTag: "section",
                titleTemplate: '<span class="font-semi-large">#index#. #title#</span>',
                transitionEffect: "slide",
                saveState: true,
                preloadContent: false,
                autoFocus: true,
                enablePagination: true,
                enableContentCache: false,
                transitionEffectSpeed: 100,

                onInit: () => {
                    // field assignments
                    $('#configLandingTitle').val(_landing.option.title);
                    $('#configLandingKeyword').val(_landing.keyword);
                    $('#configLandingDescription').val(_landing.description);
                    $('#configArticle').html(_landing.option.article);

                    // summernotinitiate Summernote
                    $('#configArticle').summernote({
                        height: 300,
                            toolbar: [
                                ['style', ['bold', 'italic', 'underline', 'clear']],
                                ['font', ['strikethrough', 'superscript', 'subscript']],
                                ['color', ['color']],
                                ['para', ['ul', 'ol', 'paragraph']],
                                ['insert', ['table', 'hr']],
                                ['misc', ['codeview']]
                            ],
                            imageAttributes: {
                                icon: '<i class="note-icon-pencil"/>',
                                removeEmpty: false, // true = remove attributes | false = leave empty if present
                                disableUpload: true // true = don't display Upload Options | Display Upload Options
                            }
                    });
                },

                onStepChanging: (e, currentIndex, newIndex) => {
                    // if moving to section "Description"
                    if (currentIndex == 0 && newIndex == 1) {
                        // validate "keyword" value
                        if ($('#configLandingTitle').val() == '') {
                            triggerAlert('error', "Masukkan Title!");
                            $('#configLandingTitle').focus();
                            return false;
                        }

                        // validate "slug" value
                        if ($('#configLandingKeyword').val() == '') {
                            triggerAlert('error', "Masukkan Meta Keyword!");
                            $('#configLandingKeyword').focus();
                            return false;
                        }

                        // validate "slug" value
                        if ($('#configLandingDescription').val() == '') {
                            triggerAlert('error', "Masukkan Meta Description!");
                            $('#configLandingDescription').focus();
                            return false;
                        }
                    }

                    return true;
                },

                onFinishing: () => {
                    // validate "configArticle" value
                    var article = $('#configArticle').summernote('code');
                    if (article == '') {
                        triggerAlert('error', "Isikan Artikel");
                        return false;
                    }

                    return true;
                },

                onFinished: () => {
                    var datastring = $("#config-form").serialize();
                    
                    Swal.fire({
                        type: 'question',
                        customClass: {
                            container: 'custom-swal'
                        },
                        title: 'Simpan Data Parent Landing?',
                        showCancelButton: true,
                        confirmButtonText: 'Submit',
                        showLoaderOnConfirm: true,
                        preConfirm: () => {
                            return $.ajax({
                                type: 'POST',
                                url: "/admin/booking/landing/parent",
                                dataType: 'json',
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                data: datastring,
                                success: (response) => {
                                    if (!response.success) {
                                        triggerAlert('error', response.message);
                                        return;
                                    }
                                },
                                error: (error) => {
                                    triggerAlert('error', error.message);
                                }
                            });
                        },
                        allowOutsideClick: () => !Swal.isLoading()
                    }).then((result) => {
                        if (!result.dismiss) {
                            $('#configModal').modal('toggle');

                            Swal.fire({
                                type: 'success',
                                customClass: {
                                    container: 'custom-swal'
                                },
                                title: "Parent Landing Berhasil Diupdate"
                            });
                        }

                    })
                }
            })
        })
        .on("hidden.bs.modal", (e) => {
            // destroy Summernote
            $('#configArticle').summernote('destroy');

            // destroy wizard
            $('#wizard-for-parent').steps('destroy');
        });

    // Map modal events
    $('#mapModal')
        .on("show.bs.modal", (e) => {
            setTimeout(function() {
                view_map.invalidateSize();
            }, 10);

            var landingOption = JSON.parse(_landing.option);

            // generate center point values
            var centerPoint = getCenterPoint(_landing.option);
            var latitudeData = centerPoint[1];
            var longitudeData = centerPoint[0];

            // values assignation
            $('#map-modal-title').text(landingOption.title);
            $('#lat1').text(landingOption.location[1][1]);
            $('#lng1').text(landingOption.location[1][0]);
            $('#lat2').text(landingOption.location[0][1]);
            $('#lng2').text(landingOption.location[0][0]);
            $('#covered-room').text(_landing.room_count);

            // destroy map instance if already existed
            if (view_map !== undefined && view_map !== null) {
                view_map.remove();
            }

            var centerPos = {
                lat: latitudeData,
                lng: longitudeData
            };

            // console.log(centerPos, )

            view_map = L.map('view-map-canvas', {
                center: centerPos,
                zoom: 12,
                minZoom: 8,
                maxZoom: 13
            });

            var tiles = new L.tileLayer("{{ config('services.osm.host') }}/hot/{z}/{x}/{y}.png", {
                attribution: '&copy; 2019 Mamikos.com'
            }).addTo(view_map);

            var marker = [];
            var markerImage = [];
            var position = [];

            // marker kiri bawah
            markerImage[0] = "{{ asset('assets/icons/kiribawah.png') }}";

            position[0] = {
                'lat': landingOption.location[1][1],
                'lng': landingOption.location[1][0]
            }

            marker[0] = new L.Marker(position[0], {
                icon: new L.icon({
                    iconUrl: markerImage[0],
                    iconSize: [60, 57],
                    iconAnchor: [30, 57]
                })
            }).addTo(view_map);

            // marker kanan atas
            markerImage[1] = "{{ asset('assets/icons/kananatas.png') }}";

            position[1] = {
                'lat': landingOption.location[0][1],
                'lng': landingOption.location[0][0]
            }

            marker[1] = new L.Marker(position[1], {
                icon: new L.icon({
                    iconUrl: markerImage[1],
                    iconSize: [60, 57],
                    iconAnchor: [30, 57]
                })
            }).addTo(view_map);
            
            // create a rectangle layer
            L.rectangle([position[0], position[1]], {color: "#1baa56", weight: 1}).addTo(view_map);

            // disable scroll by mouse wheel
            view_map.scrollWheelZoom.disable();

        })
        .on("hidden.bs.modal", (e) => {
            // 
        })

    // Table sortable handler
    $('#sortable').sortable({
        cursor: 'move',
        axis: 'y',
        update: (e, ui) => {
            triggerLoading('Menyimpan urutan...');

            var order = []; 
            $('#sortable tr').each( function(e) {
                order.push({
                    id: parseInt($(this).attr('id')),
                    sort: $(this).index() + 1
                });
            });
            
            order = JSON.stringify(order);
            $.ajax({
                type: 'POST',
                url: "/admin/booking/landing/sort",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                contentType: 'application/json',
                data: order,
                success: (response) => {
                    if (!response.success) {
                        triggerAlert('error', response.message);
                        return;
                    }

                    Swal.fire({
                        type: 'success',
                        title: "Berhasil menyimpan urutan!",
                        text: "Halaman akan di-refresh setelah Anda klik OK",
                        onClose: () => {
                            window.location.reload();
                        }
                    });
                },
                error: (error) => {
                    triggerAlert('error', error.message);
                }
            });
        }
    });
});
</script>
@endsection