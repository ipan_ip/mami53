<table class="table table-bordered">
    <tbody>
    <tr>
        <th>Description of Reject Reason</th>
        <th>Status Reject Reason</th>
        <th>Type of Reason</th>
        <th>Status Kos</th>
        <th>Affecting Acceptance</th>
        <th>Date Registered</th>    
        <th>Last Updated</th>
        <th>Action</th>
    </tr>
    @if(isset($rejectReasons))
        @foreach ($rejectReasons as $rejectReason)
            <tr>
                <td>{{ $rejectReason->description }}</td>
                <td>
                    @if($rejectReason->is_active)
                        <span class="label label-success">Active</span>
                    @else
                        <span class="label label-danger">Non Active</span>
                    @endif
                </td>
                <td>
                    @if( $rejectReason->type == "reject" )
                        <span class="label label-danger">Reject</span>
                    @elseif( $rejectReason->type == "cancel" )
                        <span class="label label-warning">Cancel</span>
                    @endif
                </td>
                <td>
                    @if($rejectReason->make_kost_not_available)
                        <span class="label label-danger">Kost Penuh</span>
                    @else
                        <span class="label label-success">Masih Tersedia</span>
                    @endif
                </td>
                <td>
                    @if($rejectReason->is_affecting_acceptance)
                        <span class="label label-success">Active</span>
                    @else 
                        <span class="label label-danger">Non Active</span>
                    @endif 
                </td>
                <td>{{ $rejectReason->updated_at }}</td>
                <td>{{ $rejectReason->created_at }}</td>
                <td class="table-action-column" style="padding-right:15px!important;">
                    <div class="btn-group">
                        <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-cogs"></i> Actions
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a href="{{ route("admin.booking.reject-reason.edit", $rejectReason->id) }}" title="Edit" class="action-recalculate">
                                    <i class="fa fa-edit"></i> Edit
                                </a>
                            </li>
                            <li class="toForm">
                                <a href="#" title="Delete" class="action-recalculate" onclick="deleteById({{$rejectReason->id}})">
                                    <i class="fa fa-trash"></i> Delete
                                </a>
                                <form action="{{ route('admin.booking.reject-reason.destroy', $rejectReason->id) }}" method="POST" id="forForm{{$rejectReason->id}}">
                                    @method('DELETE')
                                    @csrf
                                </form>
                            </li>
                        </ul>   
                    </div>
                </td>    
            </tr>
        @endforeach

    @endif
    </tbody>
</table>