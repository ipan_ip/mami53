@extends('admin.layouts.main')

@section('style')
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" />
    <link href="//cdn.jsdelivr.net/npm/select2-bootstrap-theme@0.1.0-beta.10/dist/select2-bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/booking-form-admin.css') }}">
@endsection

@section('content')
    <div class="row">
        <div class="col-md-8">
            <div class="box box-primary">
                <div class="box-header">
                    @isset($rejectReason)
                        <h3 class="box-title">Form Edit Reject Reason Page</h3>
                    @else
                        <h3 class="box-title">Form Create Reject Reason Booking</h3>
                    @endisset
                </div>
                    @isset($rejectReason)
                        <form action="{{ route('admin.booking.reject-reason.update',  $rejectReason->id) }}" method="POST" class="form-horizontal form-bordered">
                    @else
                        <form action="{{ route('admin.booking.reject-reason.store') }}" method="POST" class="form-horizontal form-bordered">    
                    @endisset
                    @csrf
                    <div class="box-body no-padding">
                        <div class="form-group bg-default">
                            <label for="" class="control-label col-sm-2">Description</label>
                            <div class="col-sm-10">
                                @isset($rejectReason)
                                    <input type="text" name="description" class="form-control" value="{{ $rejectReason->description }}" required>
                                @else 
                                <input type="text" name="description" class="form-control" required>
                                @endisset
                            </div>
                        </div>

                        <div class="form-group bg-default">
                            <label for="" class="control-label col-sm-2">Status Reject Reason</label>
                            <div class="col-sm-10">
                                <select name="is_active" class="form-control">
                                    <option value="1" @isset($rejectReason) {{ ($rejectReason['is_active']) ? 'selected' : '' }} @endisset>Active</option>
                                    <option value="0" @isset($rejectReason) {{ (!$rejectReason['is_active']) ? 'selected' : '' }} @endisset>Non Active</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group bg-default">
                            <label for="" class="control-label col-sm-2">Affecting Acceptance</label>
                            <div class="col-sm-10">
                                <select name="is_affecting_acceptance" class="form-control">
                                    <option value="1" @isset($rejectReason) {{ ($rejectReason['is_affecting_acceptance']) ? 'selected' : '' }} @endisset>Active</option>
                                    <option value="0" @isset($rejectReason) {{ (!$rejectReason['is_affecting_acceptance']) ? 'selected' : '' }} @endisset>Non Active</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group bg-default">
                            <label for="" class="control-label col-sm-2">Type of Reason</label>
                            <div class="col-sm-10">
                                <select name="type" class="form-control reject" id="rejectSelect">
                                    <option value="reject" @isset($rejectReason) {{ ($rejectReason['type'] == 'reject') ? 'selected' : '' }} @endisset>Reject</option>
                                    <option value="cancel" @isset($rejectReason) {{ ($rejectReason['type'] == 'cancel') ? 'selected' : '' }} @endisset>Cancel</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group bg-default">
                            <label for="" class="control-label col-sm-2">Status Kost</label>
                            <div class="col-sm-10">
                                <select name="make_kost_not_available" class="form-control" id="changeStatus">
                                    @isset($rejectReason)
                                        @if($rejectReason['type'] == 'cancel')
                                            <option value='0'>Masih Tersedia</option>
                                        @elseif($rejectReason['type'] == 'reject')
                                            <option value='0' @isset($rejectReason) {{ (!$rejectReason['make_kost_not_available']) ? 'selected' : '' }} @endisset>Masih Tersedia</option>
                                            <option value='1' @isset($rejectReason) {{ ($rejectReason['make_kost_not_available']) ? 'selected' : '' }} @endisset>Kos Penuh</option>    
                                        @endif
                                    @else
                                        <option value='0'>Masih Tersedia</option>
                                        <option value='1'>Kos Penuh</option>
                                    @endisset
                                </select>
                            </div>
                        </div>

                        <div class="form-group bg-default">
                            <div class="col-sm-10 col-sm-push-2">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </div>
                    @isset($rejectReason)
                    @method('PUT')
                    @endisset
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/jquery-steps@1.1.0/build/jquery.steps.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/jquery-mask-plugin@1.14.15/dist/jquery.mask.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script> 

    <script>
        $('#rejectSelect').change(function() {
            kos = $(this).val();
            if(kos == 'cancel'){
                $('#changeStatus').html("<option value='0'>Masih Tersedia</option>");
            } else if(kos == 'reject') {
                $('#changeStatus').html("<option value='0' @isset($rejectReason) {{ (!$rejectReason['make_kost_not_available']) ? 'selected' : '' }} @endisset>Masih Tersedia</option><option value='1' @isset($rejectReason) {{ ($rejectReason['make_kost_not_available']) ? 'selected' : '' }} @endisset>Kos Penuh</option>");
            }
        });
    </script>
@endsection