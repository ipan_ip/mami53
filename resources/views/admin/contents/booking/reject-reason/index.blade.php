@extends('admin.layouts.main')

@section('style')
<style>
    /* Sweetalert */

    .swal-shown {
        height: auto !important;
    }

    .custom-swal {
        z-index: 10000 !important;
    }

    .custom-wide-swal {
        width: 640px !important;
        font-size: 13px !important;
    }
</style>
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" />
@endsection

@section('content')
    <div class="row">
        <div class="col-md-8">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Search</h3>
                </div>
                    <form action="" method="GET" class="form-horizontal form-bordered" id="query-form">
                        <div class="form-group bg-default">
                            <label for="" class="control-label col-sm-2">Description of Reject Reason</label>
                            <div class="col-sm-10">
                                <input type="text" name="description" value="{{ request()->input('description') }}" class="form-control">
                            </div>
                        </div>
                        <div class="form-group bg-default">
                            <label for="" class="control-label col-sm-2">Type of Reason</label>
                            <div class="col-sm-10">
                                {{ Form::select('type', [
                                    0 => 'All',
                                    1 => 'Cancel',
                                    2 => 'Reject',
                                ], request()->input('type'), array('class' => 'form-control')) }}
                            </div>
                        </div>

                        <div class="form-group bg-default">
                            <label for="" class="control-label col-sm-2">Status Reject Reason</label>
                            <div class="col-sm-10">
                                {{ Form::select('is_active', [
                                    0 => 'All',
                                    1 => 'Active',
                                    2 => 'Non Active',
                                ], request()->input('is_active'), array('class' => 'form-control')) }}
                            </div>
                        </div>

                        <div class="form-group bg-default">
                            <div class="col-sm-10 col-sm-push-2">
                                <button type="submit" class="btn btn-primary" id="buttonSearch">Search</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">{{ $boxTitle }}</h3>
                    <div class="box-tools pull-right">
                        <a href="{{ URL::to('admin/booking/reject-reason/create') }}" class="btn btn-primary pull-right">Create</a>
                    </div>
                </div>

                <div class="box-body no-padding">
                    @include('admin.contents.booking.reject-reason.partials.table')
                </div>
                <div class="box-body no-padding">
                    {{ $rejectReasons->appends(Request::except('page'))->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.js"></script>

    <script type="text/javascript">
        function deleteById(id){
                var form = $('form#forForm' + id);
                Swal.fire({
                    title: 'Are you sure?',
                    text: "",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, change it!',
                    cancelButtonText: 'No.',
                }).then((result) => {
                    if (result.value) {
                        Swal.fire(
                            'Change Status!',
                            'Change status successfully.',
                            'success'
                        )
                        form.submit();
                    }
            });
        }

        $('#buttonSearch').click((e) => {
            $('#query-form').attr('action', '/admin/booking/reject-reason').submit();
        });
    </script>

@endsection