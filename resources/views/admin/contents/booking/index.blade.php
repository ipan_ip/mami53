@extends('admin.layouts.main')
@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
            <div class="box-tools pull-right">
                <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                   
                </div>
            </div>

            <table id="tableListArea" class="table table-striped">
                <thead>
                    <tr>
                        <th width="30px;">No</th>
                        <th>Photo</th>
                        @if(Request::is('admin/*'))
                            <th>Designer</th>
                            <th>User</th>
                            <th>Phone Number</th>
                        @else
                            <th>Name</th>
                        @endif
                        <th>Time</th>
                        <th>Status</th>
                        <th>Note</th>
                        <th class="table-action-column" width="20%">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($rowsBooking as $rowBookingKey => $rowBooking)
                    <tr>
                        <td>{{ $rowBookingKey+1 }}</td>
                        <td width="10%"><img width="36" height="48" src="{{ $rowBooking->photo['small'] }}"></td>
                        @if(Request::is('admin/*'))
                            <td>{{ $rowBooking->designer }}</td>
                            <td>{{ $rowBooking->name }}</td>
                            <td>{{ $rowBooking->phone_number }}</td>
                        @else
                            <td>{{ $rowBooking->name }}</td>
                        @endif
                        <td>{{ $rowBooking->time }}</td>
                        <td>{{ ucfirst($rowBooking->status) }}</td>
                        <td>{{ $rowBooking->note }}</td>
                        <td class="table-action-column">
                            <?php $actions = array('accepted','confirmed','deleted','canceled','rejected') ?>

                            @foreach($actions as $action)
                                @if(in_array($action,$rowBooking->action ))
                                    <a href="#">
                                        <button class="btn-add btn btn-primary btn-sm" onclick="updateBookingStatus({{ $rowBooking->id }},'{{$action}}')">
                                       {{ ucfirst(str_replace('ed','',$action)) }}</button>
                                    </a>
                                @endif
                            @endforeach

                            <div class="btn-action-group">
                                <a href="{{ '#' }}">
<!--                                     <i class="fa fa-pencil"></i></a> -->
                                <a href="#" ng-click="showConfirmDelete('Area', {{ "#" }}, '{{ '' }}');">
                                   <!--  <i class="fa fa-trash-o"></i></a> -->
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th width="30px;">No</th>
                        <th>Photo</th>
                        @if(Request::is('admin/*'))
                            <th>Designer</th>
                            <th>User</th>
                            <th>Phone Number</th>
                        @else
                            <th>Name</th>
                        @endif
                        <th>Time</th>
                        <th>Status</th>
                        <th>Note</th>
                        <th class="table-action-column" width="20%">Action</th>
                    </tr>
                </tfoot>
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
    <!-- table -->

    <!-- Form used for updating status -->
    {{ Form::open(array('url' => URL::Route(Request::segment(1).'.booking.update_status'), 'method' => 'post', 'id'=> 'form_update_booking_status')) }}
        <input type="hidden" id="booking_id" name="booking_id" />
        <input type="hidden" id="status" name="status" />
        <input type="hidden" id="message" name="message" />
    {{ Form::close() }}

@stop
@section('script')
<!-- page script -->
<script type="text/javascript">
    $(function()
    {
        $("#tableListArea").dataTable();
    });

    function updateBookingStatus(bookingId, status){
        $('#booking_id').val(bookingId);
        $('#status').val(status);

        var def_message;
        switch(status) {
            case 'accepted':
                def_message  = 'We would be glad to meet you';
                break;
            case 'rejected':
                def_message  = 'Sorry, but we are so busy';
                break;
            case 'confirmed':
                def_message  = 'Thanks for coming. Cheers';
                break;
            case 'canceled':
                def_message  = 'We have unexpected event. Sorry for unconvenient';
                break;
            default:
                def_message = '';
        }
    message = prompt('Please enter your message',def_message);

    if(message != null)
    {
        $('#message').val(message);
        $('#form_update_booking_status').submit();
    }
    
    }
</script>
@stop