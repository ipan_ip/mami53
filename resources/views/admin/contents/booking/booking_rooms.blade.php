@extends('admin.layouts.main')

@section('style')
<style>
	.discount-text {
		text-decoration: line-through;
	}
</style>
@endsection

@section('content')
	<!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
            <div class="box-tools pull-right">
                <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <form action="" class="form-inline" method="get" style="text-align:right;padding:10px;">
                        <input type="text" name="q" class="form-control input-sm"  placeholder="Nama Kost"  autocomplete="off" value="{{ request()->input('q') }}">
                        <button class="btn btn-primary btn-md" id="buttonSearch"><i class="fa fa-search">&nbsp;</i>Search</button>
                    </form>
                </div>
            </div>

            <table  class="table table-striped">
                <thead>
                    <tr>
                        <th>Kost Name</th>
                        <th># of Booking Rooms</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($rooms as $key => $room)
                    <tr>
                        <td>{{ $room->name }}</td>
                        <td>{{ count($room->booking_designers) }}</td>
                        <td>
                            <a href="{{ URL::route('admin.booking.rooms.type', $room->id) }}" title="manage">
                                <i class="fa fa-sun-o"></i>
                            </a>
                        </td>
                    </tr> 
                @endforeach
                </tbody>
                
            </table>
        </div><!-- /.box-body -->

        <div class="box-body no-padding">
            {{ $rooms->appends(Request::except('page'))->links() }}
        </div>
    </div><!-- /.box -->
    <!-- table -->
@endsection