@php
  $chipThemes = array(
    'transferred' => 'success',
    'pending' => 'warning',
    'processing' => 'info',
    'failed' => 'error'
  );
@endphp

<span class="label label-{{ $chipThemes["{$slot}"] ?? 'default' }}">
  {{ $slot ?? '-' }}
</span>