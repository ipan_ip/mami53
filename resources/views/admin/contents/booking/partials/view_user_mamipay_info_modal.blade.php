@push('script_stacks')
<script>
    function openMamipayInfoModal(accName, accNumber, bankName, bankCity) {
        Swal.fire({
            title: 'Mamipay Information',
            html: `
            <div style="text-align: left; font-size: 13px" >
                <strong>Account Name: </strong> ${accName} </br>
                <strong>Account Number: </strong> ${accNumber} </br>
                <strong>Bank Name: </strong> ${bankName} </br>
                <strong>Bank City: </strong> ${bankCity}
            </div>
            `,
        });
    }
</script>
@endpush
