    <!-- Modal for PARENT LANDING -->
    <div class="modal" id="configModal" role="dialog" aria-labelledby="modalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top:2px;"><i class="fa fa-lg fa-times-circle"></i></button>
                    <h4 class="modal-title"><i class="fa fa-cog"></i> Parent Landing Setting</h4>
                </div>
                <!-- <div class="modal-body"> -->
                <div class="modal-body">
                    <form action="#" method="POST" id="config-form">
                        <div id="wizard-for-parent">
                            <h3>Landing Details</h3>
                            <section class="row">
                                <div class="form-group col-md-8">
                                    <label for="keyword" class="control-label">Title</label>
                                    <input type="text" class="form-control" placeholder="" id="configLandingTitle" name="config-title">
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="slug" class="control-label">Meta Keyword</label>
                                    <input type="text" class="form-control" placeholder="" id="configLandingKeyword" name="config-keyword">
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="slug" class="control-label">Meta Description</label>
                                    <input type="text" class="form-control" placeholder="" id="configLandingDescription" name="config-description">
                                </div>
                            </section>
                            <h3>Template</h3>
                            <section class="row">
                                <div class="form-group col-md-12">
                                    <label for="description" class="control-label">Article Template</label>
                                    <textarea name="config-article" id="configArticle" class="form-control" rows="10"></textarea>
                                </div>
                            </section>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- /. modal for PARENT LANDING -->