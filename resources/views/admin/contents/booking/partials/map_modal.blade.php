    <!-- Modal for MAP -->
    <div class="modal" id="mapModal" role="dialog" aria-labelledby="modalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top:2px;"><i class="fa fa-lg fa-times-circle"></i></button>
                        <h4 class="modal-title"><i class="fa fa-map-marker"></i> Area: <span id="map-modal-title"></span></h4>
                    </div>
                    <!-- <div class="modal-body"> -->
                    <div class="modal-body">
                        <div class="row">
                                <div class="col-md-9">
                                    <div id="view-map-canvas"></div>
                                </div>
                                <div class="col-md-3">
                                    <div class="box box-solid box-default">
                                        <div class="box-header">
                                            <div class="box-title">
                                                <i class="fa fa-map-marker"></i> Kanan Atas
                                            </div>
                                        </div>
                                        <div class="box-body">
                                            <small>Latitude</small>
                                            <br><span id="lat1"></span>
                                            <br><small>Longitude</small>
                                            <br><span id="lng1"></span>
                                        </div>
                                    </div>
                                    <div class="box box-solid box-default">
                                        <div class="box-header">
                                            <div class="box-title">
                                                <i class="fa fa-map-marker"></i> Kiri Bawah 
                                            </div>
                                        </div>
                                        <div class="box-body">
                                            <small>Latitude</small>
                                            <br><span id="lat2"></span>
                                            <br><small>Longitude</small>
                                            <br><span id="lng2"></span>
                                        </div>
                                    </div>
                                    <div class="box box-solid box-default">
                                            {{-- <div class="box-header">
                                                <div class="box-title">
                                                    <i class="fa fa-home"></i> Jumlah Kost
                                                </div>
                                            </div> --}}
                                            <div class="box-body">
                                                Jumlah kost booking yang tercover di area ini:<br/>
                                                <span id="covered-room" class="font-large"></span>
                                            </div>
                                        </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /. modal for MAP -->