<div style="display: none" id="mamipayActivationForm">
    <form method="POST" action="" onsubmit="submitMamipayActivationForm(this); return false;" style="text-align: left">
        @csrf
        <div class="form-group">
            <label>Owner Phone Number</label>
            <input class="form-control" name="owner_phone_number" type="text">
        </div>
        <div class="form-group">
            <label>Owner Name</label>
            <input class="form-control" name="owner_name" type="text">
        </div>
        <div class="form-group">
            <label>Account Name</label>
            <input class="form-control" name="bank_account_owner" type="text">
        </div>
        <div class="form-group">
            <label>Bank Name</label>
            <select class="form-control" name="bank_name">
                <option value="">- Please Select -</option>
                @foreach($bankList as $bank)
                    <option value="{{ $bank->bank_code }}">{{ $bank->bank_name }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label>Bank City</label>
            <input class="form-control" name="bank_account_city" type="text">
        </div>
        <div class="form-group">
            <label>Account Number</label>
            <input class="form-control" name="bank_account_number" type="text">
        </div>
        <div class="form-group">
            <input class="btn btn-info" type="submit" value="Add" />
        </div>
    </form>
</div>

@push('script_stacks')
    <script>

        const ERR_CODE_MAMIPAY_VERIFICATION_NOT_ELIGIBLE = [1, 3];

        // Stateful selected kost. Should be resetted every modal opened.
        var addBookingRequestState;

        function openAddNewRequestModal() {

            // Reset selected kost state everytime the modal is opened
            addBookingRequestState = function () {
                var selectedKostIds = new Set();
                var validatedPriceKostIds = new Set();
                var phoneNumber;
                var userId;

                return {
                    toggleSelectedKostIds: function (obj) {
                        var isSelected = $(obj).is(':checked');
                        var kostId = $(obj).val();
                        if (isSelected) {
                            selectedKostIds.add(kostId);
                        } else {
                            selectedKostIds.delete(kostId);
                        }
                    },
                    toggleValidatedPrice: function (obj) {
                        var isSelected = $(obj).is(':checked');
                        var kostId = $(obj).val();
                        if (isSelected) {
                            validatedPriceKostIds.add(kostId);
                        } else {
                            validatedPriceKostIds.delete(kostId);
                        }
                    },
                    getSelectedKostIds: function () {
                        return [...selectedKostIds]; // convert the set into array
                    },
                    getValidatePriceKostIds: function () {
                        return [...validatedPriceKostIds]; // convert the set into array
                    },
                    userId: function (newUserId) {
                        if (newUserId) {
                            userId = newUserId;
                        }
                        return userId;
                    },
                    phoneNumber: function (newPhoneNumber) {
                        if (newPhoneNumber) {
                            phoneNumber = newPhoneNumber;
                        }
                        return phoneNumber;
                    },
                    isAllSelectedKostValidated: function () {
                        for (let selectedKostId of selectedKostIds) {
                            if (!validatedPriceKostIds.has(selectedKostId)) {
                                return false
                            }
                        }
                        return true
                    },
                }
            }();

            Swal.mixin({
                confirmButtonText: 'Selanjutnya &rarr;',
                showCancelButton: true,
                cancelButtonText: 'Batal',
                width: 800,
                progressSteps: ['1', '2', '3', '4', '5']
            }).queue(
                [{
                        title: 'Cari Kos',
                        input: 'text',
                        inputPlaceholder: 'Nomor Handphone',
                        preConfirm: (input) => {
                            addBookingRequestState.phoneNumber(input);
                            return true;
                        }
                    },
                    {
                        title: 'Verifikasi Owner',
                        html: 'verifikasi..',
                        onOpen: () => {
                            verifyOwner(addBookingRequestState.phoneNumber());
                        }
                    },
                    {
                        title: 'Pilih Property',
                        html: 'mengambil data..',
                        onOpen: () => {
                            getKostByOwnerId(addBookingRequestState.userId());
                        },
                        preConfirm: () => {
                            return validateSelectedKost();
                        }
                    },
                    {
                        title: 'Verifikasi Property',
                        text: 'verifikasi...',
                        onOpen: () => {
                            Swal.getConfirmButton().setAttribute('disabled', '');
                        },
                    },
                    {
                        title: 'Selesai',
                        text: 'mengirim request...',
                        confirmButtonText: 'Selesai',
                        onOpen: () => {
                            Swal.getConfirmButton().setAttribute('disabled', '');
                            submitBBKRequest(addBookingRequestState.userId());
                        }
                    }
                ]
            );
        }

        function verifyOwner(phone_number) {
            Swal.getConfirmButton().setAttribute('disabled', '');
            var request = new XMLHttpRequest();
            request.open('GET', `/admin/booking-request-wizard/validate-user-by-phone-number/${phone_number}`);

            request.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    var responseData = JSON.parse(this.responseText);
                    var message = responseData.meta.message;
                    var status = responseData.status;
                    addBookingRequestState.userId(responseData.user_id);

                    var htmlContent = '';
                    if (status) {
                        htmlContent = `
                    <div style="text-align:center">
                        <i class="fa fa-check" style="font-size: 90px;color: #5cb85c;"></i>
                        <h3>${message}</h3>
                    </div>
                    `;
                        Swal.getConfirmButton().removeAttribute('disabled');
                    } else {
                        if (ERR_CODE_MAMIPAY_VERIFICATION_NOT_ELIGIBLE.includes(responseData.error_code)) {
                            // Phone number not eligible
                            htmlContent = `
                        <div style="text-align:center">
                            <i class="fa fa-exclamation" style="font-size: 90px;color: #d9534f;"></i>
                            <h3>${message}</h3>
                        </div>
                        `;
                        } else {
                            // Mamipay Owner not activated. But user found
                            htmlContent = `
                        <div style="text-align:center">
                            <i class="fa fa-exclamation" style="font-size: 90px;color: #d9534f;"></i>
                            <h3>${message}</h3>
                            <a href="javascript:showMamipayActivationForm()" class="btn btn-primary">Aktifkan MamiPAY</a>
                        </div>
                        `;
                        }
                    }

                    $(Swal.getContent()).html(htmlContent);
                }
            };

            request.send()
        }

        function showMamipayActivationForm() {
            $('[name="owner_phone_number"]').val(addBookingRequestState.phoneNumber()).trigger('change');
            htmlContent = $('#mamipayActivationForm').html();
            $(Swal.getContent()).html(htmlContent);
        }

        function getKostByOwnerId(user_id, limit = 5, offset = 0) {

            Swal.getConfirmButton().setAttribute('disabled', '');
            var request = new XMLHttpRequest();
            request.open('GET', `/admin/booking-request-wizard/get-room/${user_id}?limit=${limit}&offset=${offset}`);

            if ($(Swal.getContent()).find('.table')) {
                $(Swal.getContent()).find('.table').css({
                    'opacity': '0.5'
                });
            } else {
                $(Swal.getContent()).html('mengambil data ...');
            }

            request.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    var responseData = JSON.parse(this.responseText);
                    var message = responseData.meta.message;
                    var status = responseData.status;

                    var rows = '';
                    var pagination = '';
                    if (status) {
                        $.each(responseData.rooms, function (i, room) {
                            var disabled = room.is_booking ? 'disabled' : '';
                            var checked = addBookingRequestState.getSelectedKostIds().includes(room.id.toString()) ? 'checked' : '';

                            rows += `
                            <tr>
                                <td style="width:32px">
                                    <input ${disabled} ${checked} type="checkbox" value="${room.id}" onClick="addBookingRequestState.toggleSelectedKostIds(this)" />
                                </td>
                                <td>
                                    ${room.name}
                                </td>
                                <td>
                                    ${room.label_info}
                                </td>
                            </tr>
                            `;
                        });

                        if (responseData.total_item > 1) {
                            var perPage = limit;
                            var pageCount = Math.ceil(responseData.total_item / perPage);
                            var activePage = responseData.active_page;

                            paginationItems = '';
                            var offset = 0;
                            for (var x = 1; x <= pageCount; x++) {
                                var active = activePage == x ? ' active' : '';
                                paginationItems += `
                                <li class="page-item${active}">
                                    <a class="page-link" href="javascript:getKostByOwnerId(${user_id}, ${perPage}, ${offset});">${x}</a>
                                </li>
                            `;
                                offset = x * perPage;
                            }
                            pagination = `
                            <div id="property-pagination">
                                <ul class="pagination">
                                    ${paginationItems}
                                </ul>
                            </div>
                            `;

                        }
                        var htmlContent = `
                        <table class="table" style="text-align:left;">
                            ${rows}
                        </table>
                        ${pagination}
                        <a href="/admin/room#room" target="_blank" class="btn btn-sm btn-primary">Kost belum ada didalam list</a>
                        `;

                        $(Swal.getContent()).html(htmlContent);

                        Swal.getConfirmButton().removeAttribute('disabled');
                    }
                }
            };

            request.send();
        }

        function validateSelectedKost(limit = 5, offset = 0) {
            Swal.getConfirmButton().setAttribute('disabled', '');
            var currentData = addBookingRequestState.getSelectedKostIds();

            if (currentData.length == 0) {
                alert('Mohon pilih property terlebih dahulu');
                return false;
            } else {
                var request = new XMLHttpRequest();
                request.open('POST', `/admin/booking-request-wizard/validate-kost?limit=${limit}&offset=${offset}`);
                request.setRequestHeader("Content-type", "application/json");
                var param = {
                    'kost_ids': currentData
                };

                request.onreadystatechange = function () {
                    if (this.readyState == 4 && this.status == 200) {
                        var responseData = JSON.parse(this.responseText);
                        var message = responseData.meta.message;
                        var status = responseData.status;

                        var htmlContent = '';
                        if (status) {
                            var rows = '';
                            $.each(responseData.validated_rooms, function (index, value) {
                                rows += `
                                <tr>
                                    <td colspan=3 style="background-color:#f4f4f4;">${value.kost_name}</td>
                                    <td tyle="background-color:#f4f4f4;"><input type="checkbox" value="${value.kost_id}" onclick="checkValidPrice(this)">Konfirmasi harga benar</td>
                                </tr>
                                <tr>
                                    <td>Harian</td>
                                    <td>${value.price.daily}</td>
                                    <td>3-Bulanan</td>
                                    <td>${value.price.quarterly}</td>
                                </tr>
                                <tr>
                                    <td>Mingguan</td>
                                    <td>${value.price.weekly}</td>
                                    <td>6-Bulanan</td>
                                    <td>${value.price.semiannualy}</td>
                                </tr>
                                <tr>
                                    <td>Bulanan</td>
                                    <td>${value.price.monthly}</td>
                                    <td>Tahunan</td>
                                    <td>${value.price.yearly}</td>
                                </tr>
                            `;
                            });
                            htmlContent = `
                            <i class="fa fa-check" style="font-size: 90px;color: #5cb85c;"></i>
                            <table class="table" style="text-align:left; font-size:14px">
                                ${rows}
                            </table>
                        `;
                            Swal.update({
                                'title': 'Check Harga'
                            });
                        } else {
                            htmlContent +=
                                '<i class="fa fa-exclamation" style="font-size: 90px;color: #d9534f;"></i>'
                            htmlContent += '<table class="table" style="text-align:left;">';
                            var rows = '';
                            $.each(responseData.validated_rooms, function (index, value) {
                                rows += `
                            <tr>
                                <td>${value.kost_name}</td>
                                <td>${value.kost_status}</td>
                            </tr>
                            `;
                            });
                            htmlContent += '</table>';
                            htmlContent += '<p style="color:red;">' + message + '</p>';
                            htmlContent = `
                            <i class="fa fa-exclamation" style="font-size: 90px;color: #d9534f;"></i>
                            <table class="table" style="text-align:left;">
                                ${rows}
                            </table>
                            <p style="color:red;">${message}</p>
                        `;
                            Swal.getConfirmButton().setAttribute('disabled', '');
                        }

                        var pagination = '';

                        if (responseData.total_item > 1) {
                            var perPage = limit;
                            var pageCount = Math.ceil(responseData.total_item / perPage);
                            var activePage = responseData.active_page;

                            pagination = '<div id="property-pagination"><ul class="pagination">';
                            var offset = 0;
                            var paginationItems = '';
                            for (var x = 1; x <= pageCount; x++) {
                                var active = activePage == x ? ' active' : '';
                                paginationItems += `
                                <li class="page-item${active}">
                                    <a class="page-link" href="javascript:validateSelectedKost(${perPage},${offset});">${x}</a>
                                </li>
                            `;
                                offset = x * perPage;
                            }
                            pagination = `
                        <div id="property-pagination">
                            <ul class="pagination">
                                ${paginationItems}
                            </ul>
                        </div>
                        `;

                        }

                        htmlContent += pagination;

                        $(Swal.getContent()).html(htmlContent);
                    }
                };

                request.send(JSON.stringify(param));
            }
        }

        function checkValidPrice(obj) {
            addBookingRequestState.toggleValidatedPrice(obj);

            if (addBookingRequestState.isAllSelectedKostValidated()) {
                Swal.getConfirmButton().removeAttribute('disabled');
            } else {
                Swal.getConfirmButton().setAttribute('disabled', '');
            }
        }

        function submitBBKRequest(user_id) {
            var request = new XMLHttpRequest();
            request.open('POST', '/admin/booking-request-wizard/create-request');
            request.setRequestHeader("Content-type", "application/json");
            var param = {
                'kost_ids': addBookingRequestState.getSelectedKostIds(),
                'user_id': user_id
            }

            request.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    var responseData = JSON.parse(this.responseText);
                    var message = responseData.meta.message;
                    var status = responseData.status;

                    var htmlContent = '';
                    if (status) {
                        htmlContent = `
                    <i class="fa fa-check" style="font-size: 90px; color: #5cb85c;"></i>
                    <p style="text-align:center;">${message}</p>
                    `;
                        Swal.getConfirmButton().removeAttribute('disabled');
                    } else {
                        htmlContent = `
                    <i class="fa fa-exclamation" style="font-size: 90px; color: #d9534f;"></i>
                    <p style="text-align:center;">${message}</p>
                    `;
                    }

                    $(Swal.getContent()).html(htmlContent);
                }
            };

            request.send(JSON.stringify(param));
        }

        // Mamipay form related
        $('[name="bank_account_city"]').autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "/admin/mamipay-owner/search-city",
                    dataType: "json",
                    data: {
                        city_name: request.term
                    },
                    success: function (data) {
                        response(data);
                    }
                });
            },
            minLength: 2
        });

        function submitMamipayActivationForm(obj) {
            $.ajax({
                type: 'POST',
                url: '/admin/booking-request-wizard/add-mamipay-owner',
                dataType: "json",
                data: $(obj).serialize(),
                success: function (result) {
                    if (result.status) {
                        verifyOwner(addBookingRequestState.phoneNumber());
                    }
                }
            });
        }
    </script>
@endpush
