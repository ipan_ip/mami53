    <!-- Add Modal -->
    <div class="modal" id="addModal" role="dialog" aria-labelledby="modalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top:2px;"><i class="fa fa-lg fa-times-circle"></i></button>
                    <h4 class="modal-title">New Booking Landing Data</h4>
                </div>
                <!-- <div class="modal-body"> -->
                <div class="modal-body">
                    <form action="#" method="POST" id="add-form">
                        <div id="wizard-for-child">
                            <h3>Landing Details</h3>
                            <section class="row">
                                <div class="form-group col-md-8">
                                    <label for="keyword" class="control-label">Title</label>
                                    <input type="text" class="form-control" placeholder="" id="landingTitle" name="title">
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="slug" class="control-label">Meta Keyword</label>
                                    <input type="text" class="form-control" placeholder="" id="landingKeyword" name="keyword">
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="slug" class="control-label">Meta Description</label>
                                    <input type="text" class="form-control" placeholder="" id="landingDescription" name="description">
                                </div>
                                <div class="form-group col-md-5">
                                    <label for="city" class="control-label">Pilih Kota</label>
                                    <select class="form-control" name="city" id="citySelector"></select>
                                </div>
                            </section>
                            <h3>Area Adjustment</h3>
                            <section class="row">
                                <div class="form-group col-md-9">
                                    <label for="map" class="control-label">Klik/geser marker hijau untuk merubah jangkauan area</label>
                                    <div id="map-canvas" name="map"></div>
                                </div>
                                <div class="form-group col-md-3">
                                    <div class="form-group col-md-12">
                                        <label for="latitude_2" class="control-label">Kanan Atas</label>
                                        <input type="text" class="form-control" placeholder="Latitude" id="inputLatitude2" name="latitude_2">
                                        <input type="text" class="form-control" placeholder="Longitude" id="inputLongitude2" name="longitude_2">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="latitude_1" class="control-label">Kiri Bawah</label>
                                        <input type="text" class="form-control" placeholder="Latitude" id="inputLatitude1" name="latitude_1">
                                        <input type="text" class="form-control" placeholder="Longitude" id="inputLongitude1" name="longitude_1">
                                    </div>
                                </div>
                            </section>
                            <h3>Template</h3>
                            <section class="row">
                                <div class="form-group col-md-12">
                                    <label for="description" class="control-label">Article Template</label>
                                    <textarea name="article" id="article" class="form-control" rows="10"></textarea>
                                </div>
                            </section>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- /. add modal -->