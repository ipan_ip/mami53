@extends('admin.layouts.main')

@section('style')
<style>
	.discount-text {
		text-decoration: line-through;
	}
</style>
@endsection

@section('content')
	<!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
            <div class="box-tools pull-right">
                <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
            </div>
        </div>
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
               <div class="btn-horizontal-group bg-default">
                    <div class="form-inline">
                        <a href="{{ URL::route('admin.booking.rooms.type.create', $room->id) }}" class="btn btn-primary btn-sm pull-left">
                            <i class="fa fa-plus">&nbsp;</i> Add Booking Room
                        </a>
                    </div>
                </div>
            </div>

            <table  class="table table-striped">
                <thead>
                    <tr>
                        <th>Kost Name</th>
                        <th>Room Type Name</th>
                        <th>Booking Type</th>
                        <th>Is Active</th>
                        <th>Total Room</th>
                        <th>Max Guest</th>
                        <th>Minimum Stay</th>
                        <th>Price</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                 @foreach($bookingDesigners as $key => $bookingDesigner)
                     <tr>
                         <td>{{ $room->name }}</td>
                         <td>{{ $bookingDesigner->name }}</td>
                         <td>{{ $bookingDesigner->type }}</td>
                         <td>{{ $bookingDesigner->is_active == 1 ? 'Ya' : 'Tidak' }}</td>
                         <td>{{ $bookingDesigner->available_room }}</td>
                         <td>{{ $bookingDesigner->max_guest }}</td>
                         <td>{{ $bookingDesigner->minimum_stay }}</td>
                         <td>
                         	Rp. 
                         	@if($bookingDesigner->sale_price > 0) 
								<span class="discount-text">{{ number_format($bookingDesigner->price, 0, ',', '.') }}</span> 
								<span>{{ number_format($bookingDesigner->sale_price, 0, ',', '.') }}</span>
                         	@else 
								{{ number_format($bookingDesigner->price, 0, ',', '.') }}
                         	@endif
                         </td>
                         <td>
                             <a href="{{ URL::route('admin.booking.rooms.type.edit', [$bookingDesigner->designer_id, $bookingDesigner->id]) }}">
                                 <i class="fa fa-pencil"></i>
                             </a>
                             <a href="{{ URL::route('admin.booking.rooms.type.calendar', [$bookingDesigner->designer_id, $bookingDesigner->id]) }}" title="Kalender Booking">
                                 <i class="fa fa-calendar"></i>
                             </a>
                         </td>
                     </tr> 
                 @endforeach
                </tbody>
                
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
    <!-- table -->
@endsection