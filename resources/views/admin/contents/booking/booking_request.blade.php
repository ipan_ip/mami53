@extends('admin.layouts.main')
@section('content')
    {{-- Modals --}}
    @include('admin.contents.booking.partials.view_user_mamipay_info_modal')
    @include('admin.contents.booking.partials.add_new_booking_owner_request_modal')

	<!-- table -->
    <div class="box box-primary" id="starting">
        <div class="box-header" style="padding-top: 10px;">
            <h3 class="box-title">{{ $boxTitle }}</h3>
            <a href="javascript:openAddNewRequestModal()" class="btn btn-default">Add New</a>
            <div class="box-tools pull-right">
                <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <form action="" class="form-inline" method="get" style="text-align:right;padding:10px;">
                        <input type="text" name="from" class="form-control input-sm datepicker" placeholder="Start Date" value="{{ request()->input('from') }}" id="start-date" autocomplete="off" style="margin-right:2px" />
                        <input type="text" name="to" class="form-control input-sm datepicker" placeholder="End Date" value="{{ request()->input('to') }}" id="end-date" autocomplete="off" style="margin-right:2px" />
                        <input type="text" name="room_name" class="form-control input-sm"  placeholder="Kost name"  autocomplete="off" value="{{ request()->input('room_name') }}">
                        <input type="text" name="area_city" class="form-control input-sm"  placeholder="Area city"  autocomplete="off" value="{{ request()->input('area_city') }}">
                        <input type="text" name="owner_phone" class="form-control input-sm"  placeholder="Owner phone"  autocomplete="off" value="{{ request()->input('owner_phone') }}">
                        {{
                            Form::select(
                                'property_status',
                                [
                                    'all' => 'All',
                                    'verified' => 'Verified',
                                    'not_verified' => 'Not Verified',
                                    'not_active' => 'Not Active',
                                    'reject' => 'Reject'
                                ],
                                request()->input('property_status'),
                                array('class' => 'pull-right-sort form-group btn btn-default')
                            )
                        }}
                        {{
                            Form::select(
                                'registered_by',
                                [
                                    'all' => 'All',
                                    'admin' => 'Admin',
                                    'system' => 'System',
                                    'consultant' => 'Consultant'
                                ],
                                request()->input('registered_by'),
                                array('class' => 'pull-right-sort form-group btn btn-default')
                            )
                        }}
                        {{
                            Form::select(
                                'sort_by',
                                [
                                    'request_date_desc' => 'Sort By - Request Date (Descending)',
                                    'request_date_asc' => 'Sort By - Request Date (Ascending)',
                                    'updated_date_desc' => 'Sort By - Newest',
                                    'updated_date_asc' => 'Sort By - Oldest',
                                    'registered_by_desc' => 'Sort By - Registered By (Descending)',
                                    'registered_by_asc' => 'Sort By - Registered By (Ascending)',
                                ],
                                request()->input('sort_by'),
                                array('class' => 'pull-right-sort form-group btn btn-default')
                            )
                        }}
                        <button class="btn btn-primary btn-md" id="buttonSearch"><i class="fa fa-search">&nbsp;</i>Search</button>
                    </form>
                </div>
            </div>
            <table  class="table table-striped">
                <thead>
                    <tr>
                        <th>Request date</th>
                        <th>Activation date</th>
                        <th>Owner</th>
                        <th>Kos</th>
                        <th>Price</th>
                        <th>Registered by</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                 @foreach($booking_request as $key => $value)
                    @if (!isset($value->room))
                        @continue;
                    @endif
                    <tr>
                        <td>
                            {{ $value->created_at->format("d M Y H:i") }}
                            @if ($value->status == "approve")
                                <label class="label label-success">Verified</label>
                            @elseif ($value->status == 'reject')
                                <label class="label label-danger">Rejected</label>
                            @elseif ($value->status == 'not_active')
                                <label class="label label-warning">Not active</label>
                            @endif
                        </td>
                        <td>
                            @if (isset($value->booking_room) && ($value->booking_room->is_active))
                                {{ $value->booking_room->created_at->format("d M Y H:i")}}
                            @else
                                -
                            @endif
                        </td>
                        <td>
                            <strong>Name: </strong> {{ optional($value->user)->name }} <br/>
                            <strong>Phone: </strong> {{ optional($value->user)->phone_number }} <a v-on:click="showKostList({{ optional($value->user)->phone_number }})" class="label label-success">Add</a> <br/>
                            <strong>Email: </strong> {{ optional($value->user)->email }}
                            @if ($value->mamipay_owner_profile)
                                <br/>
                                <a class="btn btn-xs btn-default" href="javascript:openMamipayInfoModal(
                                    '{{ $value->mamipay_owner_profile->bank_account_owner }}',
                                    '{{ $value->mamipay_owner_profile->bank_account_number }}',
                                    '{{ $value->mamipay_owner_profile->bank_name }}',
                                    '{{ $value->mamipay_owner_profile->bank_city ? $value->mamipay_owner_profile->bank_city->city_name : "-" }}'
                                    );">View Mamipay Info</a> <br/>
                            @endif
                        <td>
                            {{ $value->room->id }} - {{ $value->room->name }} <br/>
                            <strong>Area city: </strong> {{ $value->room->area_city }} <br/>
                            <strong>Room available: </strong> {{ $value->room->room_available }} <br/>
                            <strong>Room total: </strong> {{ $value->room->room_count }}  <br/>
                            <strong>Room size: </strong> {{ isset($value->room->size) ? $value->room->size : "-" }}  <br/>
                            <strong>Room floor: </strong> {{ isset($value->room->floor) ? $value->room->floor : "-" }}  <br/>
                            <strong>Type: </strong>
                            @if ($value->room->is_mamirooms)
                                Mamirooms
                            @elseif ($value->room->is_testing)
                                Testing
                            @elseif ($value->room->is_booking)
                                Booking
                            @else
                                -
                            @endif
                            <br/>
                            <a class="btn btn-xs btn-default" style="margin-top: 5px;" href="/room/{{ $value->room->slug }}"><i class="fa fa-eye"></i> View Listing</a> <br/>
                            <a class="btn btn-xs btn-default" style="margin-top: 5px;" href="/admin/room/{{ $value->room->id }}/edit"><i class="fa fa-wrench"></i> Edit Kost</a>
                        </td>
                        <td>
                            <strong>Daily: </strong> {{ number_format($value->room->price_daily, 0, ".", ".") }} <br/>
                            <strong>Weekly: </strong> {{  number_format($value->room->price_weekly, 0, ".", ".") }} <br/>
                            <strong>Monthly: </strong> {{ number_format($value->room->price_monthly, 0, ".", ".") }} <br/>
                            <strong>3-Monthly: </strong> {{ number_format($value->room->price_quarterly, 0, ".", ".") }} <br/>
                            <strong>6-Monthly: </strong> {{ number_format($value->room->price_semiannually, 0, ".", ".") }} <br/>
                            <strong>Yearly: </strong> {{ number_format($value->room->price_yearly, 0, ".", ".") }}
                        </td>
                        <td>
                            <strong>{{ $value->registered_by }}</strong>
                        </td>
                        <td>
                            <a href="{{ $value->room->share_url }}" target="_blank" class="btn btn-xs btn-default">view</a><br/>

                            @if ($value->status == "waiting")
                                <a
                                    class="btn btn-xs btn-warning"
                                    style="margin-top: 5px;"
                                    v-on:click="approveBBK({{$value}})"
                                >
                                    Approve
                                </a><br/>
                                <a
                                    class="btn btn-xs btn-danger"
                                    style="margin-top: 5px;"
                                    href="/admin/booking/owner/reject-reason/{{ $value->room->id }}"
                                >
                                    Reject
                                </a>
                            @elseif ($value->status == 'approve')
                                <a
                                    v-on:click="showConfirmModal(
                                        {
                                            title: 'Deactivate',
                                            text: 'Are you sure deactivate this room?',
                                            icon: 'warning',
                                            confirmButtonText: 'Deactivate'
                                        },
                                        '/admin/booking/owner/deactivate/{{ $value->id }}'
                                    )"
                                    class="btn btn-xs"
                                >
                                    Deactivate
                                </a>
                            @endif

                            @if  ($value->registered_by != 'system')
                                <a href="/admin/booking/owner/request/{{ $value->id }}/registered/system" onclick="return confirm('Are you sure you want to mark this item?');" class="btn btn-xs btn-warning" style="margin-top: 5px;">Registered by system</a> <br/>
                            @endif

                            @if ($value->registered_by != 'consultant')
                                <a href="/admin/booking/owner/request/{{ $value->id }}/registered/consultant" onclick="return confirm('Are you sure you want to mark this item?');" class="btn btn-xs btn-warning" style="margin-top: 5px;">Registered by consultant</a> <br/>
                            @endif

                            @if ($value->registered_by != 'admin')
                                <a href="/admin/booking/owner/request/{{ $value->id }}/registered/admin" onclick="return confirm('Are you sure you want to mark this item?');" class="btn btn-xs btn-warning" style="margin-top: 5px;">Registered by admin</a> <br/>
                            @endif

                            @if ($value->room->bbk_reject_reason_count > 0)
                                <a href="/admin/booking/owner/reject-reason/{{ $value->room->id }}" class="btn btn-xs btn-default" style="margin-top: 5px;">Reject History</a>
                            @endif
                        </td>
                    </tr>
                 @endforeach
                </tbody>

            </table>
        </div><!-- /.box-body -->

        <div class="box-body no-padding">
            {{ $booking_request->appends(Request::except('page'))->links() }}
        </div>
        <div class="modal fade" id="showModal" tabindex="-1" role="dialog">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add room booking</h4>
              </div>
              <div class="modal-body">
                <div class="">
                    <form v-on:submit.prevent="addRoomToBooking()">
                        <div v-for="value in rooms" style="margin-top: 10px; font-size: 16px;">
                            <input v-if="value.is_booking == 1" :checked="value.is_booking" :id="`${value.id}`" :value="`${value.id}`" type="checkbox" name="room" v-model="addroom.id" checked/>
                            <input v-else type="checkbox" name="room" :id="`${value.id}`" :value="`${value.id}`" v-model="addroom.id"/>
                            <strong>${value.name}</strong>
                            <label v-if="value.status == 'verified'" class="label label-success">${value.status}</label>
                            <label v-else class="label label-danger">${value.status}</label>
                        </div>
                        <button style="margin-top: 20px; width: 100%;" type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
              </div>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </div><!-- /.box -->
    <!-- table -->
@stop
@section('script')
<script type="text/javascript" src="/js/vue/vue.2.5.13.js"></script>
<script type="text/javascript" src="/js/vue/vue-resource.1.3.5.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

<script type="text/javascript">
    new Vue({
      el: '#starting',
      delimiters: ['${','}'],
      data: {
        loading: true,
        owner_id: 0,
        rooms: [],
        addroom: {"id": []}
      },
      mounted: function() {
      },
      methods: {
        showKostList: function(phone) {
            this.loading = true;
            this.$http.get("/admin/owner/room/0"+phone)
                .then((response) => {
                    this.loading = false;
                    this.rooms = []
                    this.owner_id = 0;
                    if (response.data.status == false) {
                        alert(response.data.meta.message);
                    } else{
                        this.rooms = response.data.room;
                        this.owner_id = response.data.owner_id;
                        $("#showModal").addClass("in");
                        $("#showModal").css({ display: "block" });
                    }
                })
            .catch((error) => {
                alert("Error");
            })
        },
        approveBBK: function(bbkRequest)
        {
            const message = `
                You will also approve <strong>${bbkRequest.user.booking_owner_requests_count} BBK request from this owner</strong>. <br/> <br />
                Owner Name: ${bbkRequest.user.name} <br />
                Owner Phone: ${bbkRequest.user.phone_number}
            `;
            return this.showConfirmModal(
                {
                    title: "Are you sure to approve BBK request on this item?",
                    html: message,
                    icon: 'success',
                    confirmButtonText: 'Yes, approve it!'
                },
                '/admin/booking/owner/approve-bulk/'+bbkRequest.user.id
            );
        },
        // rejectBBK: function(bbkRequest)
        // {
        //     if (bbkRequest.user.approve_count > 0) {
        //         message = `
        //             This listing already has active BBK on owner's previous listings. You will also reject <strong>${bbkRequest.user.booking_owner_requests_count} BBK request from this owner</strong>. <br /> <br />
        //             Owner Name: ${bbkRequest.user.name} <br />
        //             Owner Phone: ${bbkRequest.user.phone_number}
        //         `;
        //     } else {
        //         message = `
        //             You will also reject <strong>${bbkRequest.user.booking_owner_requests_count} BBK request from this owner</strong>. <br/> <br />
        //             Owner Name: ${bbkRequest.user.name} <br />
        //             Owner Phone: ${bbkRequest.user.phone_number}
        //         `;
        //     }

        //     return this.showConfirmModal(
        //         {
        //             title: "Are you sure to reject BBK request on this item?",
        //             html: message,
        //             icon: 'error',
        //             confirmButtonText: 'Yes, reject it!'
        //         },
        //         '/admin/booking/owner/reject-bulk/'+bbkRequest.user.id
        //     );
        // },
        showConfirmModal: function(options, target)
        {
            Swal.fire({
                ...{
                    title: 'Are you sure?',
                    icon: 'success',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33'
                },
                ...options
            }).then((result) => {
                if (result.value) {
                    window.location = target
                }
            })
            .catch(error => {
                if(confirm(options.text)) {
                    window.location = target
                }
            });

        },
        addRoomToBooking: function()
        {
            data = JSON.stringify(this.addroom.id);
            console.log(data);
            this.$http.get('/admin/owner/room?room_id='+data+"&owner_id="+this.owner_id)
                .then((response) => {
                    if (response.data.status == false) {
                        alert("Error");
                    } else {
                        $("#showModal").removeClass("in");
                        $("#showModal").css({ display: "none" });
                        window.location.href = "/admin/booking/owner/request";
                    }
                })
                .catch((error) => {
                    aler("Error");
                })
        }
      }
    });

    $('.datepicker').datepicker({ dateFormat : 'dd-mm-yy'});
</script>
@stack('script_stacks')
@stop