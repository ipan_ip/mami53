@extends('admin.layouts.main')

@section('style')
	<!-- custom css -->
	<style>
		.table>tbody>tr>td {
			vertical-align: middle;
		}

		.font-large {
			font-size: 1.5em;
		}

		.font-semi-large {
			font-size: 1.2em;
		}

		.font-grey {
			color: #9E9E9E;
		}

		.dropdown-menu>li>a:hover {
			color: #333
		}

		.label-grey {
			background-color: #CFD8DC;
		}

		[hidden] {
			display: none !important;
		}

		/* Select2 tweak */

		.input-group .select2-container,
		.form-group .select2-container {
			position: relative !important;
			z-index: 2;
			float: left !important;
			width: 100% !important;
			margin-bottom: 0 !important;
			display: table !important;
			table-layout: fixed !important;
		}

		/* Sweetalert */

		.swal2-container {
			z-index: 99999 !important;
		}

		.swal-shown {
			height: auto !important;
		}

		.custom-swal {
			z-index: 10000 !important;
		}

		.custom-wide-swal {
			width: 480px !important;
			font-size: 12px !important;
		}

		/* Custom table row */
		tr.inactive td {
			background-color: #E6C8C8;
		}

		/* Modal tweak */

		.modal-open {
			overflow: hidden;
			position: fixed;
			width: 100%;
		}


		/* Centering the modal */

		.modal {
			text-align: center;
			padding: 0 !important;
		}

		.modal:before {
			content: '';
			display: inline-block;
			height: 100%;
			vertical-align: middle;
			margin-right: -4px;
		}

		.modal-dialog {
			display: inline-block;
			text-align: left;
			vertical-align: middle;
		}

		/* Grid */
		.grid {
			position: relative;
		}

		.item {
			display: block;
			position: absolute;
			width: 100px;
			height: 100px;
			margin: 5px;
			z-index: 1;
			background: #000;
			color: #fff;
		}

		.item.muuri-item-dragging {
			z-index: 3;
		}

		.item.muuri-item-releasing {
			z-index: 2;
		}

		.item.muuri-item-hidden {
			z-index: 0;
		}

		.item-content {
			position: relative;
			width: 100%;
			height: 100%;
		}

		/* Blink effect */
		.blink {
			animation: blink 1s infinite;
		}

		@keyframes blink {
			0% {
				opacity: 1;
			}

			75% {
				opacity: 1;
			}

			76% {
				opacity: 0;
			}

			100% {
				opacity: 0;
			}
		}

		.bootstrap-datetimepicker-widget {
			z-index: 9999 !important;
		}
	</style>
	<!-- end custom css -->

	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.css">
	<link rel="stylesheet" href="//gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css">
	<link rel="stylesheet" href="//unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.css">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" />
	<link rel="stylesheet" href="//cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
@endsection

@section('content')
	<!-- box -->
    <div class="box box-primary">
		<!-- box-header -->
		<div class="box-header">
			@if(!empty($error))
			<div class="alert alert-danger"> {{ $error }}</div>
			@endif
			<div class="col-md-6 text-center">
				<h3 class="box-title"><i class="fa fa-percent"></i> {{ $boxTitle }}</h3>
			</div>
			<!-- add button -->
			<div class="col-md-6">
				<div class="form-horizontal pull-right" style="padding-top: 10px;">
					<div class="btn-group">
						<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" data-boundary="window">
							<i class="fa fa-caret-down"></i> Actions
						</button>
						<ul class="dropdown-menu pull-right action-button">
							<li>
								<a href="#" onclick="actionActivate()"><i class="fa fa-arrow-up"></i> Activate</a>
							</li>
							<li>
								<a href="#" onclick="actionDeactivate()"><i class="fa fa-arrow-down"></i> Deactivate</a>
							</li>
							<li role="presentation" class="divider">
							</li>
							<li>
								<a href="#" onclick="actionBarCalculate()"><i class="fa fa-refresh"></i> Recalculate BAR Scores</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!-- /.box-header -->

		<!-- box-body -->
		<div class="box-body">
			<!-- Table -->
			@include('admin.contents.booking.acceptance-rate.partials.table')
			<!-- table -->
		</div>
		<!-- /.box-body -->
	</div>
	<!-- /.box -->
@stop

@section('script')
	<script src="//cdn.jsdelivr.net/npm/holderjs@2.9.6/holder.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>
	<script src="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.js"></script>
	<script src="//gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.js"></script>
	<script src="//unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment-with-locales.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
	<script src="//cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
    <script>
		function triggerLoading(message) {
			Swal.fire({
				title: message,
				customClass: {
					container: 'custom-swal'
				},
				allowEscapeKey: false,
				allowOutsideClick: false,
				onOpen: () => {
					swal.showLoading();
				}
			});
		}

		function ajaxLoadData(params) {
			let url = '/admin/booking/acceptance-rate/data';

			$.get(url + '?' + $.param(params.data)).then(function (res) {
				params.success(res)
			})
		}

		function queryParams(params) {
			let newParams = {};

			newParams.search = params.searchText;
			newParams.limit = params.pageSize;
			newParams.page = params.pageNumber;

			return newParams
		}

		function kosNameFormatter(value) {
			return value;
		}

		function rateFormatter(value) {
			return `${value}%`;
		}

		function avgFormatter(value) {
			return `${value} minutes`;
		}

		function statusFormatter(value) {
			if (value == 0) {
				return "<span class='label label-default'>INACTIVE</span>";
			} else {
				return "<span class='label label-success'>ACTIVATE</span>";
			}
		}

		function actionActivate() {
			Swal.fire({
				title: 'Activate booking acceptance rate?',
				html: "<h5>This will activate for showing booking acceptance rate on all bisa booking kos.<br/><br />Are you sure to continue?</h5>",
				type: 'warning',
				confirmButtonColor: '#f56954',
				showCancelButton: true,
				confirmButtonText: 'Yes, continue!',
				preConfirm: (val) => {
					triggerLoading('Processing...<br/><small>This can take up a minutes. Do not refresh your browser!</small>');

					return $.ajax({
						type: 'GET',
						url: "/admin/booking/acceptance-rate/update/1",
						dataType: 'json',
						headers: {
							'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr("content")
						},
						done: () => {
							if (Swal.isLoading) Swal.close();
						},
						success: (response) => {
							return response;
						},
						error: (error) => {
							triggerAlert('error', '<h5>' + error.responseJSON.meta.message + '</h5>');
						}
					});
				},
				allowOutsideClick: () => !Swal.isLoading()
			}).then((result) => {
				var res = result.value;
				if (res.status === true) {
					Swal.fire({
						type: 'success',
						title: "Action has been successfully applied.",
						text: "All bisa booking kos will show booking acceptance rate",
						onClose: () => {
							$('#table').bootstrapTable('refreshOptions', {});
						}
					});
				} else {
					triggerAlert('error', res.meta.message);
				}
			});
		}

		function actionDeactivate() {
			Swal.fire({
				title: 'Deactivate booking acceptance rate?',
				html: "<h5>This will set to not showing booking acceptance rate on all bisa booking kos.<br/><br />Are you sure to continue?</h5>",
				type: 'warning',
				confirmButtonColor: '#f56954',
				showCancelButton: true,
				confirmButtonText: 'Yes, continue!',
				preConfirm: (val) => {
					triggerLoading('Processing...<br/><small>This can take up a minutes. Do not refresh your browser!</small>');

					return $.ajax({
						type: 'GET',
						url: "/admin/booking/acceptance-rate/update/0",
						dataType: 'json',
						headers: {
							'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr("content")
						},
						done: () => {
							if (Swal.isLoading) Swal.close();
						},
						success: (response) => {
							return response;
						},
						error: (error) => {
							triggerAlert('error', '<h5>' + error.responseJSON.meta.message + '</h5>');
						}
					});
				},
				allowOutsideClick: () => !Swal.isLoading()
			}).then((result) => {
				var res = result.value;
				if (res.status === true) {
					Swal.fire({
						type: 'success',
						title: "Action has been successfully applied.",
						text: "All bisa booking kos will not show booking acceptance rate",
						onClose: () => {
							$('#table').bootstrapTable('refreshOptions', {});
						}
					});
				} else {
					triggerAlert('error', res.meta.message);
				}
			});
		}

		function actionBarCalculate() {
			Swal.fire({
				title: 'Recalculate all booking acceptance rate?',
				html: "<h5>This will sent in background job to recalculate booking acceptance rate to all bisa booking room.<br/><br />Are you sure to continue?</h5>",
				type: 'warning',
				confirmButtonColor: '#f56954',
				showCancelButton: true,
				confirmButtonText: 'Yes, continue!',
				preConfirm: (val) => {
					triggerLoading('Processing...<br/><small>This can take up a minutes.');

					return $.ajax({
						type: 'GET',
						url: "/admin/booking/acceptance-rate/recalculate",
						dataType: 'json',
						headers: {
							'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr("content")
						},
						done: () => {
							if (Swal.isLoading) Swal.close();
						},
						success: (response) => {
							return response;
						},
						error: (error) => {
							triggerAlert('error', '<h5>' + error.responseJSON.meta.message + '</h5>');
						}
					});
				},
				allowOutsideClick: () => !Swal.isLoading()
			}).then((result) => {
				var res = result.value;
				if (res.status === true) {
					Swal.fire({
						type: 'success',
						title: "Job has been successfully submitted.",
						text: "All bisa booking kos is on calculation proccess",
						onClose: () => {
							$('#table').bootstrapTable('refreshOptions', {});
						}
					});
				} else {
					triggerAlert('error', res.meta.message);
				}
			});
		}

		$(function() {
			// onready
			$('#table').on('load-success.bs.table', (data) => {
				Holder.addTheme("img-thumbnail", {
					background: '#EEE',
					foreground: '#AAA',
					size: 10,
					fontweight: 'normal'
				});
			})
		});
	</script>
@stop
