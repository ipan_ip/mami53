<table id="table" class="table table-responsive" data-toggle="table" data-show-refresh="true" data-search="true"
    data-show-search-button="false" data-side-pagination="server" data-pagination="true" data-ajax="ajaxLoadData"
    data-query-params="queryParams" data-query-params-type="">
    <thead>
        <tr>
            <th data-field="room_name" data-formatter="kosNameFormatter">Kos Name</th>
            <th class="text-center" data-field="owner_name">Owner Name</th>
            <th class="text-center" data-field="owner_phone">Owner Phone</th>
            <th class="text-center" data-field="rate" data-formatter="rateFormatter">Rate</th>
            <th class="text-center" data-field="avg_time" data-formatter="avgFormatter">Average Time</th>
            <th class="text-center" data-field="updated_at">Last Update</th>
            <th class="text-center" data-field="is_active" data-formatter="statusFormatter">Status</th>
        </tr>
    </thead>
</table>
