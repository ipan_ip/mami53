@extends('admin.layouts.main')

@section('style')
<style>
    .wizard.vertical>.content {
        min-height: 500px;
    }

    .label-discount-source {
        margin-bottom: 0;
        margin-top: 10px;
    }

    .table>tbody>tr>td {
        vertical-align: middle;
    }

    .font-large {
        font-size: 1.5em;
    }

    .font-semi-large {
        font-size: 1.15em;
    }

    .font-grey {
        color: #9E9E9E;
    }

    .dropdown-menu>li>a:hover {
        color: #333
    }

    .label-grey {
        background-color: #CFD8DC;
    }

    tr.discount-active td {
        background-color: #C8E6C9;
    }


    /* Sweetalert */

    .swal-shown {
        height: auto !important;
    }

    .custom-swal {
        z-index: 10000 !important;
    }

    .custom-wide-swal {
        width: 640px !important;
        font-size: 13px !important;
    }


    /* Wixard twekas */

    .wizard>.content>.body {
        width: 100% !important;
        padding: 0 !important;
    }


    /* Select2 tweak */

    .input-group .select2-container,
    .form-group .select2-container {
        position: relative !important;
        z-index: 2;
        float: left !important;
        width: 100% !important;
        margin-bottom: 0 !important;
        display: table !important;
        table-layout: fixed !important;
    }


    /* Summernote tweak */

    .note-group-select-from-files {
        display: none;
    }


    /* Modal tweak */

    .modal-open {
        overflow: hidden;
        position: fixed;
        width: 100%;
    }


    /* Centering the modal */

    .modal {
        text-align: center;
        padding: 0 !important;
    }

    .modal:before {
        content: '';
        display: inline-block;
        height: 100%;
        vertical-align: middle;
        margin-right: -4px;
    }

    .modal-dialog {
        display: inline-block;
        text-align: left;
        vertical-align: middle;
    }
</style>
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" />
<link href="//cdn.jsdelivr.net/npm/select2-bootstrap-theme@0.1.0-beta.10/dist/select2-bootstrap.min.css"
    rel="stylesheet" />
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/jquery-steps@1.1.0/demo/css/jquery.steps.min.css">
@endsection

@section('content')
<div class="box box-default">
    <!-- Box Header -->
    <div class="box-header">
        <div class="col-md-12 text-center">
            <h3 class="box-title"><i class="fa fa-percent"></i> {{ $boxTitle }}</h3>
        </div>
    </div>
    <!-- /.box-header -->

    <div class="box-body no-padding">

        {{-- Top Bar --}}
        <div class="horizontal-wrapper">
            <div class="btn-horizontal-group bg-default form-inline" style="padding-left: 10px;padding-right: 10px;">
                <div class="col-md-6 bg-default" style="padding-bottom:10px;">
                    <!-- Add button -->
                    <a href="#" class="btn btn-success btn pull-left actions" data-action="add">
                        <i class="fa fa-plus">&nbsp;</i>Add Data
                    </a>
                    <!-- Remove All button -->
                    <a href="#" class="btn btn-danger btn pull-left actions" data-action="removeAll" style="margin-left:10px;">
                        <i class="fa fa-trash">&nbsp;</i>Remove All
                    </a>
                </div>
                <!-- Search filters -->
                <div class="col-md-6 bg-default" style="padding-bottom:10px;text-align:right">
                    <form action="" method="get">
                        {{ Form::select('room-type', $roomTypeFilter, request()->input('room-type'), array('class' => 'pull-right-sort form-group btn btn-default')) }}
                        <input type="text" name="q" class="form-control input-sm" placeholder="Nama Kost"
                            autocomplete="off" value="{{ request()->input('q') }}">
                        <button class="btn btn-primary btn-md" id="buttonSearch"><i
                                class="fa fa-search">&nbsp;</i>Search</button>
                    </form>
                </div>
            </div>
        </div>

        {{-- Table --}}
        <table class="table">
            <thead>
                <tr>
                    <th></th>
                    <th>Dibuat</th>
                    <th>Nama Kost</th>
                    <th class="text-center">Bayar</th>
                    <th>Real Price</th>
                    <th style="min-width:130px">Disc Proportion</th>
                    <th>Discount</th>
                    <th>Listing Price</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($discounts as $index => $discount)
                {{-- <tr class="{{ $discount->is_active == 1 ? 'discount-active' : '' }}"> --}}
                <tr>
                    <td class="text-center">
                        @if ($discount->is_active == 1)
                        <i class="fa fa-2x fa-check-circle-o text-success" data-toggle="tooltip" data-placement="right"
                            title="Diskon Aktif"></i>
                        @else
                        <i class="fa fa-2x fa-times-circle-o text-danger" data-toggle="tooltip" data-placement="right"
                            title="Diskon Tidak Aktif"></i>
                        @endif
                    </td>
                    <td class="{{ $discount->is_active != 1 ? 'font-grey' : '' }}">
                        {{ date('d M Y', strtotime($discount->created_at)) }}
                    </td>
                    <td class="{{ $discount->is_active != 1 ? 'font-grey' : '' }}">
                        <span class="font-semi-large"><strong>{{ $discount->room->name }}</strong></span>
                        <br/>
                        <small>
                            <a href="{{ $discount->ad_link }}" target="_blank"
                                class="{{ $discount->is_active != 1 ? 'font-grey' : 'text-success' }}">
                                <i class="fa fa-globe"></i> {{ $discount->ad_link_stripped }}
                            </a>
                        </small>
                        <br/>
                        @if ($discount->room->is_mamirooms)
                        <span class="label label-sm {{ $discount->is_active ? 'label-primary' : 'label-grey'}}" data-toggle="tooltip" title="Verified by Mamikos"><i
                                class="fa fa-check-circle"></i> Mamiroom</span>
                        @endif
                        @if ($discount->room->is_booking)
                        <span class="label label-sm {{ $discount->is_active ? 'label-success' : 'label-grey'}}" data-toggle="tooltip" title="Bisa Booking"><i
                                class="fa fa-bookmark"></i> Booking</span>
                        @endif
                    </td>
                    <td class="text-center {{ $discount->is_active != 1 ? 'font-grey' : '' }}">
                        {{ $discount->price_type }}
                    </td>
                    <td class="font-semi-large text-muted {{ $discount->is_active != 1 ? 'font-grey' : '' }}"
                        style="font-weight: 700 !important;">
                        {{ number_format($discount->real_price, 0) }}
                    </td>
                    <td class="{{ $discount->is_active != 1 ? 'font-grey' : '' }}">
                        Mamikos : <br>
                        <strong>
                            {{ number_format($discount->proportion['mamikos']['nominal'], 0) }} ({{ $discount->proportion['mamikos']['percentage'] }}%)
                        </strong> <br>
                        Owner : <br>
                        <strong>
                            {{ number_format($discount->proportion['owner']['nominal'], 0) }} ({{ $discount->proportion['owner']['percentage'] }}%)
                        </strong> <br>
                        Markup : <br>
                        <strong>
                            {{ number_format($discount->markup_price, 0) }} ({{ $discount->markup_value_formatted }})
                        </strong> 
                    </td>
                    <td class="{{ $discount->is_active != 1 ? 'font-grey' : '' }}">
                        <i class="fa fa-arrow-down"></i> {{ $discount->discount_value_formatted }}
                    </td>
                    <td class="font-semi-large text-success {{ $discount->is_active != 1 ? 'font-grey' : '' }}"
                        style="font-weight: 700 !important;">
                        {{ number_format($discount->listing_price, 0) }}
                    </td>
                    <td class="table-action-column" style="padding-right:15px!important;">
                        <div class="btn-group">
                            <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-cog"></i> Actions
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu pull-right">
                                <li>
                                    <a href="#" class="actions" data-action="update" data-discount="{{ $discount }}"><i
                                            class="glyphicon glyphicon-edit"></i> Edit Discount</a>
                                </li>
                                <li>
                                    <a href="#" class="actions" data-action="remove" data-discount="{{ $discount }}"><i
                                            class="glyphicon glyphicon-trash"></i> Hapus Discount</a>
                                </li>
                            </ul>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>

        </table>
    </div><!-- /.box-body -->

    <div class="box-body no-padding">
        {{ $discounts->appends(Request::except('page'))->links() }}
    </div>
</div><!-- /.box -->
<!-- table -->

<!-- Modal -->
<div class="modal" id="modal" role="dialog" aria-labelledby="modalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top:2px;"><i
                        class="fa fa-lg fa-times-circle"></i></button>
                <h4 class="modal-title" id="modalTitle"></h4>
            </div>
            <!-- <div class="modal-body"> -->
            <div class="modal-body">
                <form action="#" method="POST" id="form">
                    <div id="wizard">
                        <h3><i class="fa fa-arrow-circle-up"></i> SET MARKUP</h3>
                        <section class="row">
                            <div class="form-group col-md-5">
                                <label for="city" class="control-label">Pilih Jenis Harga</label>
                                <select class="form-control" name="price_type" id="priceType"></select>
                            </div>
                            <div class="form-group col-md-7 col-md-offset-0">
                                <label for="slug" class="control-label">Current Price</label>
                                <input type="text" class="form-control" placeholder="" id="currentPrice"
                                    name="current_price" disabled>
                            </div>
                            <div class="form-group col-md-5 col-md-offset-0">
                                <label for="keyword" class="control-label">Markup Type</label>
                                <select class="form-control" name="markup_type" id="markupType">
                                    <option value='percentage' selected>Percentage</option>
                                    <option value='nominal'>Nominal</option>
                                </select>
                            </div>
                            <div class="form-group col-md-7">
                                <label for="slug" class="control-label">Markup Value</label>
                                <input type="text" class="form-control" placeholder="" id="markupValue"
                                    name="markup_value">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="slug" class="control-label">Marked Up Price</label>
                                <input type="text" class="form-control input-lg" placeholder="" id="markupPrice"
                                    name="markup_price" disabled>
                                <small class="form-text text-muted"><strong>Marked Up Price</strong> adalah harga yang
                                    akan dicoret saat ditampilkan di halaman listing atau detail kost.</small>
                            </div>
                        </section>
                        <h3><i class="fa fa-percent"></i> SET DISCOUNT</h3>
                        <section class="row">
                            <div class="col-md-6 text-center">
                                <div class="callout callout-info">
                                    <small for="city" class="control-label">Real Price</small>
                                    <div id="realPriceText" class="font-large text-muted"
                                        style="font-weight: 700 !important;"></div>
                                </div>
                            </div>
                            <div class="col-md-6 text-center">
                                <div class="callout callout-warning">
                                    <small for="city" class="control-label">Marked Up Price</small>
                                    <div id="markupPriceText" class="font-large text-warning"
                                        style="font-weight: 700 !important;text-decoration: line-through!important;">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group col-md-12">
                                <label for="discountSource" class="control-label">Discount Source</label>
                                <div style="overflow:hidden;">
                                    <select class="form-control" name="discount_source" id="discountSource" data-container="body">
                                        <option value='mamikos' selected>Mamikos</option>
                                        <option value='owner'>Owner</option>
                                        <option value='mamikos_and_owner'>Mamikos & Owner</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group col-md-5 col-md-offset-0 hide">
                                <label for="keyword" class="control-label">Discount Type</label>
                                <select class="form-control" name="discount_type" id="discountType">
                                    <option value='percentage' selected>Percentage</option>
                                    <option value='nominal'>Nominal</option>
                                </select>
                            </div>
                            <div class="form-group col-md-7 hide">
                                <label for="slug" class="control-label">Discount Value</label>
                                <input type="text" class="form-control" placeholder="" id="discountValue"
                                    name="discount_value">
                            </div>

                            <!-- discount for mamikos -->
                            <div class="form-group col-md-12 label-discount-source discount-mamikos">
                                <label for="label-mamikos-source" class="control-label">Discount Source : Mamikos</label>
                            </div>
                            <div class="form-group col-md-5 col-md-offset-0 discount-mamikos">
                                <label for="discountTypeMamikos" class="control-label">Discount Type</label>
                                <select class="form-control" name="discount_type_mamikos" id="discountTypeMamikos">
                                    <option value='percentage' selected>Percentage</option>
                                    <option value='nominal'>Nominal</option>
                                </select>
                            </div>
                            <div class="form-group col-md-7 discount-mamikos">
                                <label for="discountValueMamikos" class="control-label">Discount Value</label>
                                <input type="text" class="form-control" placeholder="" id="discountValueMamikos"
                                    name="discount_value_mamikos">
                            </div>
                            <!-- end of mamikos -->

                            <!-- discount for owner -->
                            <div class="form-group col-md-12 label-discount-source discount-owner">
                                <label for="label-mamikos-source" class="control-label">Discount Source : Owner</label>
                            </div>
                            <div class="form-group col-md-5 col-md-offset-0 discount-owner">
                                <label for="discountTypeOwner" class="control-label">Discount Type</label>
                                <select class="form-control" name="discount_type_owner" id="discountTypeOwner">
                                    <option value='percentage' selected>Percentage</option>
                                    <option value='nominal'>Nominal</option>
                                </select>
                            </div>
                            <div class="form-group col-md-7 discount-owner">
                                <label for="discountValueOwner" class="control-label">Discount Value</label>
                                <input type="text" class="form-control" placeholder="" id="discountValueOwner"
                                    name="discount_value_owner">
                            </div>
                            <!-- end of owner -->

                            <div class="form-group col-md-6">
                                <label for="slug" class="control-label">Listing Price</label>
                                <input type="text" class="form-control input-lg" placeholder="" id="finalPrice"
                                    name="final_price" disabled>
                            </div>

                        </section>
                        <h3><i class="fa fa-bars"></i> SUMMARY</h3>
                        <section class="row">
                            <div class="form-group col-md-12">
                                <div class="col-md-1 text-center">
                                    <i class="fa fa-exclamation-circle fa-2x float-left"></i>
                                </div>
                                <div class="col-md-11">
                                    <p>Periksa kembali setting discount dan pastikan semua data sudah benar.
                                        <br />Klik <strong>[Previous]</strong> untuk mengkoreksi data.<br />Klik
                                        <strong>[Finish]</strong> untuk menyimpan.</p>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Discount Setup Summary</h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <small>Nama Kost</small>
                                                <p id="summaryNamaKost"></p>
                                            </div>
                                            <div class="col-md-6">
                                                <small>Alamat</small>
                                                <p id="summaryAlamat"></p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <small>Harga Asli</small>
                                                <p id="summaryRealPrice" class="font-large text-muted"></p>
                                            </div>
                                            <div class="col-md-4">
                                                <small>Harga Markup</small>
                                                <p id="summaryMarkupPrice" class="font-large text-warning"></p>
                                            </div>
                                            <div class="col-md-4">
                                                <small>Harga Listing</small>
                                                <p id="summaryListingPrice" class="font-large text-success"></p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <small>Tipe Markup</small>
                                                <p id="summaryMarkupType"></p>
                                            </div>
                                            <div class="col-md-3">
                                                <small>Jumlah</small>
                                                <p id="summaryMarkupValue"></p>
                                            </div>
                                        </div>
                                        <div class="row">
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <h4><strong>Owner Discount</strong></h4>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <small>Tipe Markup</small>
                                                        <p id="summaryDiscountOwnerType"></p>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <small>Jumlah</small>
                                                        <p id="summaryOwnerDiscountValue"></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <h4><strong>Mamikos Discount</strong></h4>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <small>Tipe Diskon</small>
                                                        <p id="summaryDiscountMamikosType"></p>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <small>Jumlah</small>
                                                        <p id="summaryMamikosDiscountValue"></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /. modal -->

@endsection

@section('script')
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/jquery-steps@1.1.0/build/jquery.steps.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/jquery-mask-plugin@1.14.15/dist/jquery.mask.min.js"></script>
<script>
    // Sweetalert functions
function triggerAlert(type, message) {
    Swal.fire({
        type: type,
        customClass: {
            container: 'custom-swal'
        },
        title: message
    });
}

function triggerLoading(message) {
    Swal.fire({
        title: message,
        customClass: {
            container: 'custom-swal'
        },
        allowEscapeKey: false,
        allowOutsideClick: false,
        onOpen: () => {
            swal.showLoading();
        }
    });
}

/*
DOM
*/
$(function() {
    var token = '{{ csrf_token() }}';

    // Initial variables
    var processedRoom, processedURL, _discount;
    var _editing = false;
    var _availableDiscounts = ['price_daily', 'price_weekly', 'price_monthly', 'price_annually', 'price_daily_usd', 'price_weekly_usd', 'price_monthly_usd', 'price_annually_usd'];

    $('.actions').click((e) => {
        e.preventDefault();
        var action = $(e.currentTarget).data('action');

        if (action != 'add') _discount = $(e.currentTarget).data('discount');

        // Add new
        if (action == 'add') {
            Swal.fire({
                title: 'Masukkan URL Kost',
                html: '<div class="callout callout-info">'+
                        '<p>Contoh URL yang valid:<br/><strong class="font-grey">{{ env('APP_URL') }}/room/kost-sleman-kost-putri-eksklusif-kost-mamirooms-mackahaus-sinduadi-sleman</strong></p>'+
                        '</div>',
                input: 'text',
                confirmButtonText: 'Cek URL',
                showCancelButton: true,
                customClass: 'custom-wide-swal',
                showLoaderOnConfirm: true,
                preConfirm: (val) => {
                    // validate if it's a valid url
                    var is_url = false;
                    if (~val.indexOf("mamikos.com/room/kost-") || ~val.indexOf("/room/kost-")) {
                        is_url = true;
                    } else {
                        Swal.showValidationMessage('URL tidak valid!');
                        return false;
                    }

                    if (is_url) {
                        // just get the "slug"
                        var slug = /[^/]*$/.exec(val)[0];
                        return $.ajax({
                            type: 'POST',
                            url: "/admin/booking/discount/verify",
                            dataType: 'json',
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            data: {
                                'slug': slug,
                                '_token': token
                            },
                            success: (response) => {
                                if (!response.success) {
                                    Swal.showValidationMessage(response.message);
                                }
                            
                                // check already applied discounts
                                if (response.available) {
                                    if (response.available.length < 1) {
                                        Swal.showValidationMessage('Semua discount sudah terpasang untuk kost ini. Silahkan edit atau hapus discount lama.');
                                    }

                                    _availableDiscounts = response.available;
                                }

                                processedURL = val;

                                return;
                            },
                            error: (error) => {
                                Swal.showValidationMessage('Request Failed: ' + error.message);
                            }
                        });
                    }
                    
                },
                allowOutsideClick: () => !Swal.isLoading()

            }).then((result) => {
                if (!result.dismiss) {
                    var room = result.value.room;
                    if (room) {
                        Swal.fire({
                            title: room.name,
                            html: "Pastikan bahwa ini adalah kost yang benar<br>Lalu klik <strong>[Confirm]</strong> untuk memulai setup discount.",
                            customClass: 'custom-wide-swal',
                            confirmButtonText: 'Confirm',
                            showCancelButton: true,
                            imageUrl: room.photo_url.large
                        }).then((result) => {
                            if (result.value) {
                                processedRoom = room;
                                
                                $('#modal').modal('show');
                            }
                        })
                    }
                }
            });
        }

        // Updating score
        else if (action == 'update') {
            _editing = true;
            $('#modal').modal('show');
        }

        // Removing discount data
        else if (action == 'remove') {
            Swal.fire({
                title: 'Hapus Data Discount?',
                type: 'warning',
                text: 'Harga di listing akan kembali pada harga semula!',
                buttonsStyling: false,
                confirmButtonClass: 'btn btn-danger',
                showCancelButton: true,
                cancelButtonClass: 'btn btn-default',
                showLoaderOnConfirm: true,
                confirmButtonText: 'Yes, remove it!',
                preConfirm: (val) => {
                    return $.ajax({
                        type: 'POST',
                        url: "/admin/booking/discount/remove",
                        dataType: 'json',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: {
                            'id': _discount.id,
                            '_token': token
                        },
                        success: (response) => {
                            if (!response.success) {
                                Swal.insertQueueStep({
                                    type: 'error',
                                    title: response.message
                                });
                                return;
                            }
                        },
                        error: (error) => {
                            Swal.insertQueueStep({
                                type: 'error',
                                title: 'Error: ' + error.message
                            });
                        }
                    });
                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                if (result.value) {
                    Swal.fire({
                        type: 'success',
                        title: "Discount telah berhasil dihapus",
                        text: "Halaman akan di-refresh setelah Anda klik OK",
                        onClose: () => {
                            window.location.reload();
                        }
                    });
                }
            })
        }

        // Removing all discount data
        else if (action == 'removeAll') {
            Swal.fire({
                title: 'Anda ingin menghapus semua data discount?',
                type: 'warning',
                text: 'Semua data discount akan di hapus',
                buttonsStyling: false,
                confirmButtonClass: 'btn btn-danger',
                showCancelButton: true,
                cancelButtonClass: 'btn btn-default',
                showLoaderOnConfirm: true,
                confirmButtonText: 'Yes, remove all!',
                preConfirm: (val) => {
                    return $.ajax({
                        type: 'POST',
                        url: "/admin/booking/discount/remove-all",
                        dataType: 'json',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: (response) => {
                            if (!response.success) {
                                Swal.insertQueueStep({
                                    type: 'error',
                                    title: response.message
                                });
                                return;
                            }
                        },
                        error: (error) => {
                            Swal.insertQueueStep({
                                type: 'error',
                                title: 'Error: ' + error.message
                            });
                        }
                    });
                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                if (result.value) {
                    var resp = result.value;
                    if (resp.success == true) {
                        Swal.fire({
                            type: 'success',
                            title: "Semua data discount telah berhasil dihapus",
                            text: "Halaman akan di-refresh setelah Anda klik OK",
                            onClose: () => { window.location.reload(); }
                        });
                    } else {
                        var err = resp.message ? resp.message : '';
                        Swal.fire({
                            type: 'error',
                            customClass: { container: 'custom-swal' },
                            title: 'Error',
                            html: err,
                            allowOutsideClick: true
                        });
                    }
                }
            })
        }

    });

    // Add modal event
    $('#modal')
        .on("show.bs.modal", (e) => {
            $('#modal').data('bs.modal').options.keyboard = false;
            $('#modal').data('bs.modal').options.backdrop = 'static';

            if (!_editing) $('#modalTitle').html('Set Discount: ' + processedRoom.name + ' <small>[ID '+processedRoom.song_id+']</small>');
            else $('#modalTitle').html('Edit Discount: ' + _discount.room.name + ' <small>[ID '+_discount.room.song_id+']</small>')

            // initiate helper variables
            var _selectedPriceType, 
                _selectedMarkupType, 
                _selectedDiscountType,
                _selectedDiscountTypeMamikos,
                _selectedDiscountTypeOwner,
                _selectedDiscountSource;
            
            // set default
            _selectedDiscountSource = 'mamikos';

            // initialize wizard
            $('#wizard').steps({
                headerTag: "h3",
                bodyTag: "section",
                titleTemplate: '<span class="font-semi-large">#title#</span>',
                transitionEffect: "slide",
                stepsOrientation: "vertical",
                saveState: true,
                preloadContent: false,
                autoFocus: true,
                enableContentCache: false,
                transitionEffectSpeed: 100,

                onInit: () => {
                    // initial markup type
                    if (!_editing) _selectedMarkupType = 'percentage';
                    else _selectedMarkupType = _discount.markup_type;

                    // initiate dropdowns
                    $('#priceType').select2({
                        theme: "bootstrap",
                        minimumResultsForSearch: -1,
                        dropdownParent: $("#modal")
                    });

                    $('#markupType').select2({
                        theme: "bootstrap",
                        minimumResultsForSearch: -1,
                        dropdownParent: $("#modal")
                    });

                    // calculating functions
                    function calculateMarkupPrice() {
                        var markupPrice = 0;
                        var currentPrice = parseInt($('#currentPrice').cleanVal());
                        var markupValue = parseInt($('#markupValue').cleanVal());
                        
                        if (_selectedMarkupType == 'percentage') {
                            var markupAmount = currentPrice * (markupValue / 100);
                            markupPrice = Math.round(currentPrice + markupAmount);
                        }
                        else {
                            markupPrice = currentPrice + markupValue;
                        }

                        $('#markupPrice').val(markupPrice);
                        $('#markupPrice').unmask();
                        $('#markupPrice').mask('000.000.000', {reverse: true});
                    }

                    // disable priceType dropdown when editing 
                    if (_editing) $('#priceType').prop('disabled', true);

                    // values assignation
                    if (_editing) $('#markupType').val(_discount.markup_type).trigger('change');

                    // populating priceType selector data
                    if (_editing) {
                        processedRoom = _discount.room;
                        _selectedPriceType = _discount.price_type_value;
                        var isDefaultSelected = true;
                    } else {
                        var isDefaultSelected = false;
                    }

                    // price_daily
                    if (processedRoom.price_daily != 0 && processedRoom.price_daily != null) {
                        if (!_editing) {
                            if ($.inArray("price_daily", _availableDiscounts) !== -1) {
                                var newOption = new Option('Harga Harian', 'price_daily', false, false);
                                if (!isDefaultSelected) {
                                    newOption.selected = true;
                                    _selectedPriceType = 'price_daily';
                                    isDefaultSelected = true;
                                }
                                $('#priceType').append(newOption).trigger('change');
                            }
                        } else {
                            var newOption = new Option('Harga Harian', 'price_daily', false, false);
                            if (_selectedPriceType == 'price_daily') {
                                newOption.selected = true;
                            }
                            $('#priceType').append(newOption).trigger('change');
                        }
                    }
                    // price_weekly
                    if (processedRoom.price_weekly != 0 && processedRoom.price_weekly != null) {
                        if (!_editing) {
                            if ($.inArray("price_weekly", _availableDiscounts) !== -1) {
                                var newOption = new Option('Harga Mingguan', 'price_weekly', false, false);
                                if (!isDefaultSelected) {
                                    newOption.selected = true;
                                    _selectedPriceType = 'price_weekly';
                                    isDefaultSelected = true;
                                }
                                $('#priceType').append(newOption).trigger('change');
                            }
                        } else {
                            var newOption = new Option('Harga Mingguan', 'price_weekly', false, false);
                            if (_selectedPriceType == 'price_weekly') {
                                newOption.selected = true;
                            }
                            $('#priceType').append(newOption).trigger('change');
                        }
                    }
                    // price_monthly
                    if (processedRoom.price_monthly != 0 && processedRoom.price_monthly != null) {
                        if (!_editing) {
                            if ($.inArray("price_monthly", _availableDiscounts) !== -1) {
                                var newOption = new Option('Harga Bulanan', 'price_monthly', false, false);
                                // it's a default priceType, so it will override 'isDefaultSelected' value
                                newOption.selected = true; 
                                _selectedPriceType = 'price_monthly';
                                isDefaultSelected = true;
                                $('#priceType').append(newOption).trigger('change');
                            }
                        } else {
                            var newOption = new Option('Harga Bulanan', 'price_monthly', false, false);
                            if (_selectedPriceType == "price_monthly") {
                                newOption.selected = true;
                            }
                            $('#priceType').append(newOption).trigger('change');
                        }
                    }

                    // price_quarterly
                    if (
                        processedRoom.price_quarterly != 0
                        && processedRoom.price_quarterly != null
                    ) {
                        if (!_editing) {
                            if ($.inArray("price_quarterly", _availableDiscounts) !== -1) {
                                var newOption = new Option('Harga 3 Bulanan', 'price_quarterly', false, false);
                                // it's a default priceType, so it will override 'isDefaultSelected' value
                                if (!isDefaultSelected) {
                                    newOption.selected = true;
                                    _selectedPriceType = 'price_quarterly';
                                    isDefaultSelected = true;
                                }
                                $('#priceType').append(newOption).trigger('change');
                            }
                        } else {
                            var newOption = new Option('Harga 3 Bulanan', 'price_quarterly', false, false);
                            if (_selectedPriceType == "price_quarterly") {
                                if (_selectedPriceType == "price_quarterly") {
                                    newOption.selected = true;
                                }
                            }
                            $('#priceType').append(newOption).trigger('change');
                        }
                    }

                    // price_semiannually
                    if (
                        processedRoom.price_semiannually != 0
                        && processedRoom.price_semiannually != null
                    ) {
                        if (!_editing) {
                            if ($.inArray("price_semiannually", _availableDiscounts) !== -1) {
                                var newOption = new Option('Harga 6 Bulanan', 'price_semiannually', false, false);
                                // it's a default priceType, so it will override 'isDefaultSelected' value
                                if (!isDefaultSelected) {
                                    newOption.selected = true;
                                    _selectedPriceType = 'price_semiannually';
                                    isDefaultSelected = true;
                                }
                                $('#priceType').append(newOption).trigger('change');
                            }
                        } else {
                            var newOption = new Option('Harga 6 Bulanan', 'price_semiannually', false, false);
                            if (_selectedPriceType == "price_semiannually") {
                                if (_selectedPriceType == "price_semiannually") {
                                    newOption.selected = true;
                                }
                            }
                            $('#priceType').append(newOption).trigger('change');
                        }
                    }
                    // price_yearly
                    if (processedRoom.price_yearly != 0 && processedRoom.price_yearly != null) {
                        if (!_editing) {
                            if ($.inArray("price_yearly", _availableDiscounts) !== -1) {
                                var newOption = new Option('Harga Tahunan', 'price_yearly', false, false);
                                if (!isDefaultSelected) {
                                    newOption.selected = true;
                                    _selectedPriceType = 'price_yearly';
                                    isDefaultSelected = true;
                                }
                                $('#priceType').append(newOption).trigger('change');
                            }
                        } else {
                            var newOption = new Option('Harga Tahunan', 'price_yearly', false, false);
                            if (_selectedPriceType == "price_yearly") {
                                newOption.selected = true;
                            }
                            $('#priceType').append(newOption).trigger('change');
                        }  
                    }
                    // price_daily_usd
                    if (processedRoom.price_daily_usd != 0 && processedRoom.price_daily_usd != null && $.inArray("price_daily_usd", _availableDiscounts) !== -1) {
                        if (!_editing) {
                            if ($.inArray("price_daily_usd", _availableDiscounts) !== -1) {
                                var newOption = new Option('Harga Harian USD', 'price_daily_usd', false, false);
                                $('#priceType').append(newOption).trigger('change');
                            }
                        } else {
                            var newOption = new Option('Harga Harian USD', 'price_daily_usd', false, false);
                            if (_selectedPriceType == 'price_daily_usd') {
                                newOption.selected = true;
                            }
                            $('#priceType').append(newOption).trigger('change');
                        }
                    }
                    // price_weekly_usd
                    if (processedRoom.price_weekly_usd != 0 && processedRoom.price_weekly_usd != null) {
                        if (!_editing) {
                            if ($.inArray("price_weekly_usd", _availableDiscounts) !== -1) {
                                var newOption = new Option('Harga Mingguan USD', 'price_weekly_usd', false, false);
                                $('#priceType').append(newOption).trigger('change');
                            }
                        } else {
                            var newOption = new Option('Harga Mingguan USD', 'price_weekly_usd', false, false);
                            if (_selectedPriceType == 'price_weekly_usd') {
                                newOption.selected = true;
                            }
                            $('#priceType').append(newOption).trigger('change');
                        }
                    }
                    // price_monthly_usd
                    if (processedRoom.price_monthly_usd != 0 && processedRoom.price_monthly_usd != null) {
                        if (!_editing) {
                            if ($.inArray("price_monthly_usd", _availableDiscounts) !== -1) {
                                var newOption = new Option('Harga Bulanan USD', 'price_monthly_usd', false, false);
                                $('#priceType').append(newOption).trigger('change');
                            }
                        } else {
                            var newOption = new Option('Harga Bulanan USD', 'price_monthly_usd', false, false);
                            if (_selectedPriceType == 'price_monthly_usd') {
                                newOption.selected = true;
                            }
                            $('#priceType').append(newOption).trigger('change');
                        }
                    }
                    // price_yearly_usd
                    if (processedRoom.price_yearly_usd != 0 && processedRoom.price_yearly_usd != null) {
                        if (!_editing) {
                            if ($.inArray("price_yearly_usd", _availableDiscounts) !== -1) {
                                var newOption = new Option('Harga Tahunan USD', 'price_yearly_usd', false, false);
                                $('#priceType').append(newOption).trigger('change');
                            }
                        } else {
                            var newOption = new Option('Harga Tahunan USD', 'price_yearly_usd', false, false);
                            if (_selectedPriceType == 'price_yearly_usd') {
                                newOption.selected = true;
                            }
                            $('#priceType').append(newOption).trigger('change');
                        }
                    }

                    // Generate 'currentPrice'
                    if (!_editing) {
                        var data = $("#priceType").select2('data');
                        
                        switch (data[0].id) {
                            case 'price_daily':
                                $('#currentPrice').val(processedRoom.price_daily);
                                break;
                            case 'price_weekly':
                                $('#currentPrice').val(processedRoom.price_weekly);
                                break;
                            case 'price_monthly':
                                $('#currentPrice').val(processedRoom.price_monthly);
                                break;
                            case 'price_quarterly':
                                $('#currentPrice').val(processedRoom.price_quarterly);
                                break;
                            case 'price_semiannually':
                                $('#currentPrice').val(processedRoom.price_semiannually);
                                break;
                            case 'price_yearly':
                                $('#currentPrice').val(processedRoom.price_yearly);
                                break;
                            case 'price_daily_usd':
                                $('#currentPrice').val(processedRoom.price_daily_usd);
                                break;
                            case 'price_weekly_usd':
                                $('#currentPrice').val(processedRoom.price_weekly_usd);
                                break;
                            case 'price_monthly_usd':
                                $('#currentPrice').val(processedRoom.price_monthly_usd);
                                break;
                            case 'price_yearly_usd':
                                $('#currentPrice').val(processedRoom.price_yearly_usd);
                                break;
                            default:
                                break;
                        }
                    } else {
                        $('#currentPrice').val(_discount.real_price);
                    }
                    
                    $('#currentPrice').mask('000.000.000', {reverse: true});

                    // masking up 'markupValue' & set default value
                    if (!_editing) {
                        $('#markupValue').val(0);
                        $('#markupValue').mask('##0%', {reverse: true});
                    } else {
                        $('#markupValue').val(_discount.markup_value);
                        if (_selectedMarkupType == 'percentage') $('#markupValue').mask('##0%', {reverse: true});
                        else $('#markupValue').mask('000.000.000', {reverse: true});
                    }

                    // initial 'calculateMarkupPrice()'
                    calculateMarkupPrice();

                    // Setup listeners
                    $("#priceType").on('select2:select', (e) => {
                        var selected = e.params.data.id;
                        _selectedPriceType = selected;
                        $('#currentPrice').val(processedRoom[selected]);
                        $('#currentPrice').unmask();
                        $('#currentPrice').mask('000.000.000', {reverse: true});
                        calculateMarkupPrice();
                    });

                    $("#markupType").on('select2:select', (e) => {
                        var selected = e.params.data.id;
                        _selectedMarkupType = selected;
                        $("#markupValue").val(0);
                        $('#markupValue').unmask();
                        if (selected == 'percentage') $('#markupValue').mask('##0%', {reverse: true});
                        else $('#markupValue').mask('000.000.000', {reverse: true});
                        calculateMarkupPrice();
                    });

                    $("#markupValue").on('keyup', (e) => {
                        calculateMarkupPrice();
                    });

                    $(".discount-owner").hide();
                },

                onStepChanging: (e, currentIndex, newIndex) => {

                    // if moving to section "Set Discount"
                    if (currentIndex == 0 && newIndex == 1) {
                        // validate "markupValue"
                        if ($('#markupValue').cleanVal() == '') {
                            triggerAlert('error', "Isikan Markup Value!");
                            $('#markupValue').focus();
                            return false;
                        }

                        var percentageFormat = Math.round((parseInt($('#markupValue').cleanVal()) / parseInt($('#currentPrice').cleanVal())) * 100);
                        var tempMarkupValue = $('#markupValue').val();
                        if (_selectedMarkupType === 'nominal') tempMarkupValue += ' (' + percentageFormat + '%)';

                        Swal.fire({
                            type: 'warning',
                            customClass: {
                                container: 'custom-swal'
                            },
                            title: 'Konfirmasi',
                            html: `<strong>Markup value terisi sejumlah ${tempMarkupValue}</strong><br/>apakah kamu yakin untuk melanjutkan?`,
                            showCancelButton: true,
                            confirmButtonText: 'Tidak',
                            cancelButtonText: 'Ya, Lanjutkan',
                            allowOutsideClick: false
                        }).then((result) => {
                            if (!result.dismiss) {
                                $('#wizard').steps('previous');
                            }
                        })
                    }

                    // if moving from section "Set Discount" to "Summary"
                    if (currentIndex == 1 && newIndex == 2) {
                        
                        if (_selectedDiscountType == 'percentage' &&  parseInt($('#discountValue').cleanVal()) >= 100) {
                            triggerAlert('error', "Discount maksimal 99%!");
                            return false;
                        }
                        
                        if (_selectedDiscountType == 'nominal' &&  parseInt($('#discountValue').cleanVal()) > parseInt($('#markupPrice').cleanVal())) {
                            triggerAlert('error', "Discount tidak boleh melebihi harga markup!");
                            return false;
                        }

                        if (
                            _selectedDiscountSource == 'mamikos' 
                            && _selectedDiscountTypeMamikos  == 'percentage'
                            && parseInt($('#discountValueMamikos').cleanVal()) >= 100
                        ) {
                            triggerAlert('error', "Discount maksimal 99%!");
                            return false;
                        }

                        if (
                            _selectedDiscountSource == 'owner' 
                            && _selectedDiscountTypeOwner  == 'percentage'
                            && parseInt($('#discountValueOwner').cleanVal()) >= 100
                        ) {
                            triggerAlert('error', "Discount maksimal 99%!");
                            return false;
                        }

                        if (
                            _selectedDiscountSource == 'mamikos_and_owner' 
                        ) {
                            if (
                                _selectedDiscountTypeOwner  == 'percentage' 
                                && (parseInt($('#discountValueOwner').cleanVal()) >= 100)
                            ) {
                                triggerAlert('error', "Discount maksimal 99%!");
                                return false;
                            }

                            if (
                                _selectedDiscountTypeMamikos  == 'percentage' 
                                && (parseInt($('#discountValueMamikos').cleanVal()) >= 100)
                            ) {
                                triggerAlert('error', "Discount maksimal 99%!");
                                return false;
                            }

                            // last please check whether all discount is not exceed 100%
                            if (
                                _selectedDiscountTypeMamikos == 'percentage'
                                && _selectedDiscountTypeOwner == 'percentage'
                            ) {
                                if (
                                    (parseInt($('#discountValueOwner').cleanVal()) + parseInt($('#discountValueOwner').cleanVal())) 
                                    >= 100
                                ) {
                                    triggerAlert('error', "Discount maksimal 99%!");
                                    return false;
                                }
                            }
                        }

                        // if listingPrice is less than realPrice
                        var listingPrice = parseInt($('#finalPrice').cleanVal());
                        var realPrice = parseInt($('#currentPrice').cleanVal());

                        if (listingPrice < realPrice) {
                            Swal.fire({
                                type: 'warning',
                                customClass: {
                                    container: 'custom-swal'
                                },
                                title: 'Mohon Konfirmasi',
                                html: '<strong>Harga Listing lebih kecil dari Harga Asli.</strong><br/>Tetap Lanjutkan?',
                                showCancelButton: true,
                                confirmButtonText: 'Edit Ulang',
                                cancelButtonText: 'Ya, Lanjutkan',
                                allowOutsideClick: false
                            }).then((result) => {
                                if (!result.dismiss) {
                                    $('#wizard').steps('previous');
                                }
                            })
                        }

                        if (listingPrice == realPrice) {
                            Swal.fire({
                                type: 'warning',
                                customClass: {
                                    container: 'custom-swal'
                                },
                                title: 'Mohon Konfirmasi',
                                html: '<strong>Harga Listing sama dengan Harga Asli.</strong><br/>Tetap Lanjutkan?',
                                showCancelButton: true,
                                confirmButtonText: 'Edit Ulang',
                                cancelButtonText: 'Ya, Lanjutkan',
                                allowOutsideClick: false
                            }).then((result) => {
                                if (!result.dismiss) {
                                    $('#wizard').steps('previous');
                                }
                            })
                        }
                    }

                    return true;
                },

                onStepChanged: (e, currentIndex, priorIndex) => {
                    // section "Set Markup"
                    if (currentIndex == 0) {
                        // 
                    }

                    // section "Set Discount"
                    if (currentIndex == 1) {

                        // initial _selectedDiscountType value
                        if (!_editing) {
                            if ($('#discountValueMamikos').val() == '' || $('#discountValueMamikos').val() == null) {
                                _selectedDiscountTypeMamikos = 'percentage';
                            }

                            if ($('#discountValueOwner').val() == '' || $('#discountValueOwner').val() == null) {
                                _selectedDiscountTypeOwner = 'percentage';
                            }

                            if ($('#discountValue').val() == '' || $('#discountValue').val() == null) {
                                _selectedDiscountType = 'percentage';
                            }
                        } else {
                            // TODO: Check for handling editing section
                            _selectedDiscountTypeMamikos    = _discount.discount_type_mamikos;
                            _selectedDiscountTypeOwner      = _discount.discount_type_owner;
                            _selectedDiscountType           = _discount.discount_type;
                        }

                        function calculateFinalPrice() {

                            var finalPrice = 0;
                            var markupPrice = parseInt($('#markupPrice').cleanVal());

                            var discountValue = parseInt($('#discountValue').cleanVal());
                            var discountValueMamikos = parseInt($('#discountValueMamikos').cleanVal());
                            var discountValueOwner = parseInt($('#discountValueOwner').cleanVal());

                            if (_selectedDiscountSource == 'mamikos') {
                                if (_selectedDiscountTypeMamikos == 'percentage') {
                                    var discountAmount = markupPrice * (discountValueMamikos / 100);
                                    finalPrice = Math.round(markupPrice - discountAmount);
                                } else {
                                    finalPrice = markupPrice - discountValueMamikos;
                                }
                            } else if (_selectedDiscountSource == 'owner') {
                                if (_selectedDiscountTypeOwner == 'percentage') {
                                    var discountAmount = markupPrice * (discountValueOwner / 100);
                                    finalPrice = Math.round(markupPrice - discountAmount);
                                } else {
                                    finalPrice = markupPrice - discountValueOwner;
                                }
                            } else {
                                if (_selectedDiscountTypeMamikos == 'percentage' && _selectedDiscountTypeOwner == 'percentage') {
                                    var discountAmount = markupPrice * ((discountValueOwner + discountValueMamikos) / 100);
                                    finalPrice = Math.round(markupPrice - discountAmount);
                                } else if (_selectedDiscountTypeMamikos == 'nominal' && _selectedDiscountTypeOwner == 'nominal') {
                                    finalPrice = markupPrice - (discountValueOwner + discountValueMamikos);
                                } else { 
                                    if (_selectedDiscountTypeOwner == 'percentage') {
                                        var discountAmount = markupPrice * (discountValueOwner / 100);
                                        finalPrice = Math.round(markupPrice - discountAmount);
                                    } else {
                                        finalPrice = Math.round(markupPrice - discountValueOwner);
                                    }

                                    if (_selectedDiscountTypeMamikos == 'percentage') {
                                        var discountAmount = markupPrice * (discountValueMamikos / 100);
                                        finalPrice = Math.round(finalPrice - discountAmount);
                                    } else {
                                        finalPrice = Math.round(finalPrice - discountValueMamikos);
                                    }
                                }
                            }
                            
                            $('#finalPrice').val(finalPrice);
                            $('#finalPrice').unmask();
                            $('#finalPrice').mask('000.000.000', {reverse: true});
                        }

                        // initiate 'discountType' dropdown
                        $('#discountType').select2({
                            theme: "bootstrap",
                            minimumResultsForSearch: -1,
                            dropdownParent: $("#modal")
                        });

                        // initiate 'discountSource' dropdown
                        $('#discountSource').select2({
                            theme: "bootstrap",
                            minimumResultsForSearch: -1,
                            dropdownParent: $("#modal")
                        });

                        // initiate 'discountTypeMamikos' dropdown
                        $('#discountTypeMamikos').select2({
                            theme: "bootstrap",
                            minimumResultsForSearch: -1,
                            dropdownParent: $("#modal")
                        });

                        // initiate 'discountTypeOwner' dropdown
                        $('#discountTypeOwner').select2({
                            theme: "bootstrap",
                            minimumResultsForSearch: -1,
                            dropdownParent: $("#modal")
                        });
                        
                        $("#discountSource").on('select2:select', (e) => {
                            var selected = e.params.data.id;
                            _selectedDiscountSource = selected;
                            
                            $(".discount-owner").hide();
                            $(".discount-mamikos").hide();

                            if (selected == 'mamikos') $(".discount-mamikos").show();
                            if (selected == 'owner') $(".discount-owner").show();

                            if (selected == 'mamikos_and_owner') {
                                $(".discount-mamikos").show();
                                $(".discount-owner").show();
                            }

                            // recalculate
                            calculateFinalPrice();
                        });

                        // display result of 'realPrice' and 'markupPrice'
                        var realPriceData = $('#currentPrice').val();
                        $('#realPriceText').html(realPriceData);
                        var markupPriceData = $('#markupPrice').val();
                        $('#markupPriceText').html(markupPriceData);

                        // masking up 'markupValue' & set initial value
                        if (!_editing) {
                            if ($('#discountValue').val() == '' || $('#discountValue').val() == null) {
                                $('#discountValue').val(0);
                                $('#discountValue').mask('##0%', {reverse: true});
                            }

                            if ($('#discountValueMamikos').val() == '' || $('#discountValueMamikos').val() == null) {
                                $('#discountValueMamikos').val(0);
                                $('#discountValueMamikos').mask('##0%', {reverse: true});
                            }

                            if ($('#discountValueOwner').val() == '' || $('#discountValueOwner').val() == null) {
                                $('#discountValueOwner').val(0);
                                $('#discountValueOwner').mask('##0%', {reverse: true});
                            }
                        } else {
                            // set discount source when update
                            $('#discountSource').val(_discount.discount_source).trigger('change.select2');
                            _selectedDiscountSource = _discount.discount_source;

                            $(".discount-owner").hide();
                            $(".discount-mamikos").hide();

                            if (_selectedDiscountSource == 'mamikos') $(".discount-mamikos").show();
                            if (_selectedDiscountSource == 'owner') $(".discount-owner").show();
                            if (_selectedDiscountSource == 'mamikos_and_owner') {
                                $(".discount-mamikos").show();
                                $(".discount-owner").show();
                            }
                            // end of set discount source

                            $('#discountType').val(_discount.discount_type).trigger('change');
                            $('#discountValue').val(_discount.discount_value);
                            if (_selectedDiscountType == 'percentage') $('#discountValue').mask('##0%', {reverse: true});
                            else $('#discountValue').mask('000.000.000', {reverse: true});

                            $('#discountTypeMamikos').val(_discount.discount_type_mamikos).trigger('change.select2');
                            $('#discountValueMamikos').val(_discount.discount_value_mamikos);
                            if (_selectedDiscountTypeMamikos == 'percentage') $('#discountValueMamikos').mask('##0%', {reverse: true});
                            else $('#discountValueMamikos').mask('000.000.000', {reverse: true});

                            $('#discountTypeOwner').val(_discount.discount_type_owner).trigger('change.select2');
                            $('#discountValueOwner').val(_discount.discount_value_owner);
                            if (_selectedDiscountTypeOwner == 'percentage') $('#discountValueOwner').mask('##0%', {reverse: true});
                            else $('#discountValueOwner').mask('000.000.000', {reverse: true});

                            calculateFinalPrice();
                        }

                        // Setup listeners
                        $("#discountType").on('select2:select', (e) => {
                            var selected = e.params.data.id;
                            _selectedDiscountType = selected;
                            $("#discountValue").val(0);
                            $('#discountValue').unmask();
                            if (selected == 'percentage') $('#discountValue').mask('##0%', {reverse: true});
                            else $('#discountValue').mask('000.000.000', {reverse: true});
                            calculateFinalPrice();
                        });

                        // Setup listeners for discountType mamikos
                        $("#discountTypeMamikos").on('select2:select', (e) => {
                            var selected = e.params.data.id;
                            _selectedDiscountTypeMamikos = selected;
                            $("#discountValueMamikos").val(0);
                            $('#discountValueMamikos').unmask();
                            if (selected == 'percentage') $('#discountValueMamikos').mask('##0%', {reverse: true});
                            else $('#discountValueMamikos').mask('000.000.000', {reverse: true});
                            calculateFinalPrice();
                        });

                        // Setup listeners for discountType owner
                        $("#discountTypeOwner").on('select2:select', (e) => {
                            var selected = e.params.data.id;
                            _selectedDiscountTypeOwner = selected;
                            $("#discountValueOwner").val(0);
                            $('#discountValueOwner').unmask();
                            if (selected == 'percentage') $('#discountValueOwner').mask('##0%', {reverse: true});
                            else $('#discountValueOwner').mask('000.000.000', {reverse: true});
                            calculateFinalPrice();
                        });

                        $("#discountValueMamikos").on('keyup', (e) => {
                            calculateFinalPrice();
                        });
                        $("#discountValueOwner").on('keyup', (e) => {
                            calculateFinalPrice();
                        });
                        $("#discountValue").on('keyup', (e) => {
                            calculateFinalPrice();
                        });

                        // initial 'calculateMarkupPrice()'
                        calculateFinalPrice();

                        setMaskValue('#discountValueMamikos', _selectedDiscountTypeMamikos);
                        setMaskValue('#discountValueOwner', _selectedDiscountTypeOwner);
                    }

                    // section "Summary"
                    if (currentIndex == 2) {

                        // compile 'priceType' suffix
                        var priceTypeSuffix = '';
                        switch (_selectedPriceType) {
                            case 'price_daily' :
                            case 'price_daily_usd' :
                                priceTypeSuffix = '/hr';
                                break;
                            case 'price_yearly' :
                            case 'price_yearly_usd' :
                                priceTypeSuffix = '/thn';
                                break;
                            case 'price_weekly' :
                            case 'price_weekly_usd' :
                                priceTypeSuffix = '/mgg';
                                break;
                            case 'price_quarterly' :
                                priceTypeSuffix = '/3 bln';
                                break;
                            case 'price_semiannually' :
                                priceTypeSuffix = '/6 bln';
                                break;
                            // monthly is the default
                            default:
                                priceTypeSuffix = '/bln';
                                break;
                        }

                        // Setup values
                        $('#summaryNamaKost').html(processedRoom.name);
                        $('#summaryAlamat').html(processedRoom.address);
                        $('#summaryRealPrice').html($('#currentPrice').val() + '<small>' + priceTypeSuffix + '</small>');
                        $('#summaryMarkupPrice').html($('#markupPrice').val() + '<small>' + priceTypeSuffix + '</small>');
                        $('#summaryListingPrice').html($('#finalPrice').val() + '<small>' + priceTypeSuffix + '</small>');
                        $('#summaryMarkupType').html(_selectedMarkupType);
                        if (_selectedMarkupType == 'nominal') {
                            var percentageFormat = Math.round((parseInt($('#markupValue').cleanVal()) / 
                                parseInt($('#currentPrice').cleanVal())) * 100);
                            $('#summaryMarkupValue').html($('#markupValue').val() + ' (' + percentageFormat + '%)');
                        } else {
                            $('#summaryMarkupValue').html($('#markupValue').val());
                        }

                        var percentageFormat = 0;
                        var percentageFormatOwner = 0;
                        var percentageFormatMamikos = 0;

                        var totalPrice = parseInt($('#markupValue').cleanVal()) + parseInt($('#currentPrice').cleanVal());

                        percentageFormat = Math.round((parseInt($('#discountValue').cleanVal()) / totalPrice) * 100);
                        percentageFormatOwner = Math.round((parseInt($('#discountValueOwner').cleanVal()) / totalPrice) * 100);
                        percentageFormatMamikos = Math.round((parseInt($('#discountValueMamikos').cleanVal()) / totalPrice) * 100);

                        if (_selectedDiscountSource == 'mamikos') {
                            $('#summaryDiscountOwnerType').html('-');
                            $('#summaryDiscountMamikosType').html(_selectedDiscountTypeMamikos);

                            if (_selectedDiscountTypeMamikos == 'nominal') {
                                $('#summaryMamikosDiscountValue').html($('#discountValueMamikos').val() + ' (' + percentageFormatMamikos + '%)');
                                $('#summaryOwnerDiscountValue').html('-');
                            } else {
                                $('#summaryMamikosDiscountValue').html($('#discountValueMamikos').val());
                                $('#summaryOwnerDiscountValue').html('-');
                            }
                        } else if (_selectedDiscountSource == 'owner') {
                            $('#summaryDiscountOwnerType').html(_selectedDiscountTypeOwner);
                            $('#summaryDiscountMamikosType').html('-');

                            if (_selectedDiscountTypeOwner == 'nominal') {
                                $('#summaryOwnerDiscountValue').html($('#discountValueOwner').val() + ' (' + percentageFormatOwner + '%)');
                                $('#summaryMamikosDiscountValue').html('-');
                            } else {
                                $('#summaryOwnerDiscountValue').html($('#discountValueOwner').val());
                                $('#summaryMamikosDiscountValue').html('-');
                            }
                        } else { // for mamikos_and_owner
                            $('#summaryDiscountOwnerType').html(_selectedDiscountTypeOwner);
                            $('#summaryDiscountMamikosType').html(_selectedDiscountTypeMamikos);

                            if (_selectedDiscountTypeMamikos == 'nominal') {
                                $('#summaryMamikosDiscountValue').html($('#discountValueMamikos').val() + ' (' + percentageFormatMamikos + '%)');
                            } else {
                                $('#summaryMamikosDiscountValue').html($('#discountValueMamikos').val());
                            }

                            if (_selectedDiscountTypeOwner == 'nominal') {
                                $('#summaryOwnerDiscountValue').html($('#discountValueOwner').val() + ' (' + percentageFormatOwner + '%)');
                            } else {
                                $('#summaryOwnerDiscountValue').html($('#discountValueOwner').val());
                            }
                        }
                    }
                },

                onFinished: () => {

                    // sanitize '_selectedPriceType
                    var cleanedPriceType = _selectedPriceType.replace('price_', '');
                    if (cleanedPriceType == 'yearly') {
                        cleanedPriceType = 'annually';
                    }
                    if (cleanedPriceType == 'yearly_usd') {
                        cleanedPriceType = 'annually_usd';
                    }

                    // compile datas
                    var dataObject = {
                        'designer_id'           : processedRoom.id,
                        'price_type'            : cleanedPriceType,
                        'price_type_origin'     : _selectedPriceType,
                        'real_price'            : $('#currentPrice').cleanVal(),
                        'ad_link'               : processedURL,
                        'markup_type'           : _selectedMarkupType,
                        'markup_value'          : $('#markupValue').cleanVal(),
                        'discount_type'         : _selectedDiscountType,
                        'discount_type_mamikos' : _selectedDiscountTypeMamikos,
                        'discount_type_owner'   : _selectedDiscountTypeOwner,
                        'discount_value_mamikos': $('#discountValueMamikos').cleanVal(), 
                        'discount_value_owner'  : $('#discountValueOwner').cleanVal(), 
                        'discount_source'       : _selectedDiscountSource,
                        'is_active'             : 1
                    };

                    // append ID if editing
                    if (_editing) dataObject.id = _discount.id;

                    Swal.fire({
                        type: 'question',
                        customClass: {
                            container: 'custom-swal'
                        },
                        title: 'Simpan data discount?',
                        showCancelButton: true,
                        confirmButtonText: 'Submit',
                        showLoaderOnConfirm: true,
                        preConfirm: () => {
                            return $.ajax({
                                type: 'POST',
                                url: "/admin/booking/discount",
                                dataType: 'json',
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                data: dataObject,
                                success: (response) => {
                                    if (!response.success) {
                                        triggerAlert('error', response.message);
                                        return;
                                    }

                                    return
                                },
                                error: (error) => {
                                    triggerAlert('error', error);
                                }
                            });
                        },
                        allowOutsideClick: () => !Swal.isLoading()
                    }).then((result) => {
                        if (!result.dismiss) {
                            $('#modal').modal('toggle');

                            if (result.value.success == false) {
                                Swal.fire({
                                    type: 'error',
                                    customClass: {container: 'custom-swal'},
                                    title: "Gagal menyimpan data discount!",
                                    html: result.value.message,
                                    onClose: () => { window.location.reload(); }
                                });
                            } else {
                                Swal.fire({
                                    type: 'success',
                                    customClass: {container: 'custom-swal'},
                                    title: "Berhasil menyimpan data discount!",
                                    html: "Halaman akan di-refresh setelah Anda klik OK",
                                    onClose: () => { window.location.reload(); }
                                });
                            }
                        }
                    })
                }
            });

            // End of wizard events
        })

        .on("hidden.bs.modal", (e) => {
            $('#priceType').empty().select2('destroy');
            $('#markupType').select2('destroy');

            // destroy discountSource
            if ($('#discountSource').data('select2')) {
                $('#discountSource').select2('destroy');
            }

            // destroy discountTypeMamikos
            if ($('#discountTypeMamikos').data('select2')) {
                $('#discountTypeMamikos').select2('destroy');
            }

            // destroy discountTypeOwner
            if ($('#discountTypeOwner').data('select2')) {
                $('#discountTypeOwner').select2('destroy');
            }

            // destroy discountType
            if ($('#discountType').data('select2')) {
                $('#discountType').select2('destroy');
            }

            // reset _editing
            _editing = false;

            // destroy wizard
            $('#wizard').steps('destroy');
        });
});

function setMaskValue(el, selected){
    var temp = $(el).cleanVal();
    if (selected == 'percentage') {
        $(el).mask('##0%', {reverse: true})
        $(el).val(temp + '%');
    } else {
        $(el).val(temp).mask('000.000.000', {reverse: true})
    } 
}
</script>
@endsection
