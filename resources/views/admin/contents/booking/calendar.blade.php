@extends('admin.layouts.main')

@section('style')
<link rel="stylesheet" href="{{ url('assets/vendor/calendar/css/calendar.min.css') }}">
<style>
    .event {
        background: #27ab27;
    }

    @if(count($eventClasses) > 0)
        @foreach($eventClasses as $key => $eventClass)
            .{{ $eventClass }} {
                background: #27a{{ str_pad(substr($key, 0, 3), 3, '0')  }};
            }
        @endforeach
    @endif

    span[data-cal-date] {
        color: #000;
        font-weight: bold;
    }


    .day-highlight {
        background: #27ab27;
    }

    .day-hightlight span[data-cal-date] {
        color: #fff;
        font-weight: bold;
    }

    [class*="cal-cell"]:hover {
        background: #27ab27;
    }

    [class*="cal-cell"]:hover span[data-cal-date] {
        color: #fff;
        font-weight: bold;
    }

    .page-title {
        font-size: 20px;
        font-weight: bold;
        padding: 15px;
    }
</style>
@endsection

@section('content')
	<!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
            
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <h4 class="page-title">{{ $currentMonth . ' ' . $currentYear }}</h4>

            <div class="btn-group" role="group">
                <a href="{{ URL::route('admin.booking.rooms.type.calendar', [$bookingDesigner->designer_id, $bookingDesigner->id]) }}?year={{ $previousYear }}&month={{ $previousMonth }}" id="btn-prev" class="btn btn-primary">Bulan Sebelumnya</a>
                <a href="{{ URL::route('admin.booking.rooms.type.calendar', [$bookingDesigner->designer_id, $bookingDesigner->id]) }}?year={{ $nextYear }}&month={{ $nextMonth }}" id="btn-next" class="btn btn-primary">Bulan Selanjutnya</a>
            </div>

            <p>&nbsp;</p>

            <div id="calendar"></div>

            <p>&nbsp;</p>

            <div class="btn-group" role="group">
                <a href="{{ URL::route('admin.booking.rooms.type.calendar', [$bookingDesigner->designer_id, $bookingDesigner->id]) }}?year={{ $previousYear }}&month={{ $previousMonth }}" id="btn-prev" class="btn btn-primary">Bulan Sebelumnya</a>
                <a href="{{ URL::route('admin.booking.rooms.type.calendar', [$bookingDesigner->designer_id, $bookingDesigner->id]) }}?year={{ $nextYear }}&month={{ $nextMonth }}" id="btn-next" class="btn btn-primary">Bulan Selanjutnya</a>
            </div>
        </div>

        
    </div>
    
@endsection

@section('script')
<script src="{{ url('js/underscore.min.js') }}"></script>
<script src="{{ url('assets/vendor/calendar/js/calendar.js') }}"></script>

<script>
    $(function() {
        var calendar = $("#calendar").calendar(
        {
            tmpl_path: "/assets/vendor/calendar/tmpls/",
            events_source: {!! json_encode($eventSources) !!},
            day: '{{ date($currentYear . '-' . $currentMonth . '-01', time()) }}',
            weekbox: false,
            display_week_numbers: false
        });
    });
</script>
@endsection