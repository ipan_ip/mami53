@extends('admin.layouts.main')

@section('style')
<style>
    .total-price {
        font-weight: bold;
    }

    .total-price td:first-child {
        text-align: right;
    }
</style>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Data Booking - {{ $bookingUser->booking_code }}</h3>
                </div><!-- /.box-header -->
                <div class="box-body no-padding">
                    <div class="form-horizontal form-bordered">
                        <div class="form-group">
                            <label class="control-label col-sm-4">Nama Kost</label>
                            <div class="col-sm-8">
                                <p class="form-control-static">{{ $bookingUser->booking_designer->room->name  }}</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-4">Tipe Kamar</label>
                            <div class="col-sm-8">
                                <p class="form-control-static">{{ $bookingUser->booking_designer->name  }}</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-4">Tipe Booking</label>
                            <div class="col-sm-8">
                                <p class="form-control-static">{{ $bookingUser->rent_count_type  }}</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-4">Durasi Booking</label>
                            <div class="col-sm-8">
                                <p class="form-control-static">
                                    {{ \Carbon\Carbon::parse($bookingUser->checkin_date)->format('Y-m-d')  }} -&nbsp;
                                    {{ \Carbon\Carbon::parse($bookingUser->checkout_date)->format('Y-m-d') }}&nbsp;
                                    {{ '(' . $rentType . ')' }}
                                </p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-4">Nama Kontak</label>
                            <div class="col-sm-8">
                                <p class="form-control-static">{{ $bookingUser->contact_name  }}</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-4">No. Telepon Kontak</label>
                            <div class="col-sm-8">
                                <p class="form-control-static">{{ $bookingUser->contact_phone  }}</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-4">Email Kontak</label>
                            <div class="col-sm-8">
                                <p class="form-control-static">{{ $bookingUser->contact_email  }}</p>
                            </div>
                        </div>

                        @if(!is_null($bookingUser->admin_waiting_time))
                            <div class="form-group">
                                <label class="control-label col-sm-4">Batas waktu konfirmasi admin</label>
                                <div class="col-sm-8">
                                    <p class="form-control-static">{{ \Carbon\Carbon::parse($bookingUser->admin_waiting_time)->format('Y-m-d H:i:s')  }}</p>
                                </div>
                            </div>
                        @endif

                        @if(is_null($bookingUser->admin_waiting_time) && !is_null($bookingUser->expired_date))
                            <div class="form-group">
                                <label class="control-label col-sm-4">Batas waktu pembayaran</label>
                                <div class="col-sm-8">
                                    <p class="form-control-static">{{ \Carbon\Carbon::parse($bookingUser->expired_date)->format('Y-m-d H:i:s')  }}</p>
                                </div>
                            </div>
                        @endif

                        <div class="form-group">
                            <label class="control-label col-sm-4">Status Booking</label>
                            <div class="col-sm-8">
                                <p class="form-control-static">{{ $bookingUser->status  }}</p>
                            </div>
                        </div>

                        @if(!is_null($bookingUser->cancel_reason))
                            <div class="form-group">
                                <label class="control-label col-sm-4">Alasan Pembatalan</label>
                                <div class="col-sm-8">
                                    <p class="form-control-static">{{ $bookingUser->cancel_reason  }}</p>
                                </div>
                            </div>
                        @endif

                        <div class="form-group">
                                <label class="control-label col-sm-4">Tanggal Buat</label>
                                <div class="col-sm-8">
                                    <p class="form-control-static">{{ $bookingUser->created_at  }}</p>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Biaya Lainnya - {{ $bookingUser->booking_code }}</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <form action="{{ URL::route('admin.booking.users.confirm', $bookingUser->id) }}" method="POST" class="form-horizontal">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Harga Sewa*</label>
                                    <div class="col-sm-9">
                                        <input type="numeric" class="form-control" value="{{ number_format($price, 0, '.', ',') }}" name="room_price">
                                        <input type="hidden" class="form-control" value="{{ $price }}" name="price">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">No Kamar*</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="room_number" id="inputRoomNumber" placeholder="No Kamar" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Biaya Tetap</label>
                                    <div class="col-sm-9 fixed-billing">
                                        <input type="button" id="collapsible" class="btn btn-success" data-toggle="collapse" data-target="#intro" value="+">
                                    </div>
                                </div>
                                <div id="intro" class="collapse">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Denda</label>
                                        <div class="col-sm-9">
                                            <input type="numeric" class="form-control" name="fine_amount">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Maximum Telat</label>
                                        <div class="col-sm-9">
                                            <input type="numeric" class="form-control" name="fine_maximum_length">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Tipe Durasi Denda</label>
                                        <div class="col-sm-9">
                                            <select name="fine_duration_type" class="form-control" id="">
                                                <option value="day">Hari</option>
                                                <option value="week">Minggu</option>
                                                <option value="month">Bulan</option>
                                                <option value="year">Tahun</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Tanggal Penagihan Tetap</label>
                                        <div class="col-sm-9">
                                            <input type="numeric" class="form-control" name="billing_date">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Biaya Prorata Sewa Pertama</label>
                                        <div class="col-sm-9">
                                            <input type="numeric" class="form-control" name="first_amount">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Biaya Deposit</label>
                                        <div class="col-sm-9">
                                            <input type="numeric" class="form-control" name="deposit">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">DP</label>
                                        <div class="col-sm-9">
                                            <input type="numeric" class="form-control" name="down_payment">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-9 additional-price">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-5 control-label">Biaya Tambahan Lainnya</label>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Nama</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" name="field_title" type="text" id="field_title" placeholder="Title">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Biaya</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" type="numeric" name="field_value" id="field_value" placeholder="Value">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"></label>
                                    <div class="col-sm-9">
                                        <button type="button" class="add-row" value="Add Row">Tambah</button>
                                        <button type="button" class="delete-row">Hapus</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <button type="submit" class="btn bg-olive btn-flat pull-right" style="margin-top: -3px; margin-bottom: 24px; margin-right: 14px;">Confirm</button>
                        </div>
                    </form>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-8">
                                    
                                </div>
                            </div>
                            <table class="table table-striped">
                                <tbody>
                                    <tr>
                                        <th>Title</th>
                                        <th>Value</th>
                                        <th>Hapus</th>
                                    </tr>
                                    <tr>
                                    </tr>
                                </tbody>
                                <tfoot>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                    </div>
                </div>
                <div class="box-footer">
                </div>
            </div>
        </div>
    </div>

    {{-- Modal Create Room --}}
    <div class="modal fade" id="additional-price">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Additional Price</h4>
                    <br>
                    <hr>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="box-body">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Nama</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="additional_price_name" name="additional_price_name">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Harga</label>
                                <div class="col-sm-9">
                                    <input type="numeric" class="form-control" name="fine_maximum_length">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Tipe Durasi Denda</label>
                                <div class="col-sm-9">
                                    <select name="fine_duration_type" class="form-control" id="">
                                        <option value="day">Hari</option>
                                        <option value="week">Minggu</option>
                                        <option value="month">Bulan</option>
                                        <option value="year">Tahun</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-info pull-right">Add</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        <!-- /.modal-content -->
        </div>
    <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    @section('script')
        <script type="text/javascript">
            $(document).ready(function(){
                $(".add-row").click(function(){
                    if (($("#field_value").val() == 0) || ($("#field_value").val() == '')) {
                        alert("Biaya tidak boleh kosong atau kurang dari 0");
                    }
                    
                    if (($("#field_title").val() != '') && ($("#field_value").val() != '') && ($("#field_value").val() > 0)) {
                        var fieldTitle = $("#field_title").val();
                        var fieldValue = $("#field_value").val();
                        var formAdditionalPrice = "<input class='form-group' type='hidden' name='additional_field_title[]' value='" + fieldTitle + "'><input class='form-group' type='hidden' name='additional_field_value[]' value='" + fieldValue + "'>"
                        var markup = "<tr><td>" + fieldTitle + "</td><td>" + fieldValue + "</td><td><input type='checkbox' name='record'></td></tr>";
                        $("table tbody").append(markup);
                        $(".additional-price").append(formAdditionalPrice);
                    }
                });
                
                // Find and remove selected table rows
                $(".delete-row").click(function(){
                    $("table tbody").find('input[name="record"]').each(function(){
                        if($(this).is(":checked")){
                            $(this).parents("tr").remove();
                        }
                    });
                });
            });    

            function handleClick(){
                this.value = (this.value == '+' ? '-' : '+');
                if (this.value == '+') {
                    $(".fixed-billing").append("<input class='form-group' type='hidden' name='fixed_billing' value='false'>");
                } else if (this.value == '-') {
                    $(".fixed-billing").append("<input class='form-group' type='hidden' name='fixed_billing' value='true'>");
                }
            }
            document.getElementById('collapsible').onclick=handleClick;
        </script>
    @endsection
@endsection