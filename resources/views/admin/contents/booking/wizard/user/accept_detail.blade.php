@extends('admin.layouts.main')

@section('style')
<style>
.step-header {
    margin-bottom: 20px;
}

.step-header ul{
    list-style: none none;
    margin: 0px 0px 0px;
    padding: 0px 0px 0px;
    text-align: center;
}

.step-header li{
    background-color: #ccc;
    color: #FFF;
    padding: 5px 15px;
    display: inline-block;
    font-size: 20px;
    font-weight: bold;
    margin: 0px 30px;
    border-radius: 20px;
}

.step-header li.active{
    background-color: #109E4B;
}

.wizard-title{
    text-align: center;
    font-size: 20px;
    font-weight: bold;
}

table td{
    vertical-align: middle !important;
}

.step-footer{
    text-align:center;
}

.wizard-holder {
    display:none;
}

.wizard-holder.active {
    display:block;
}

.has-error{
    border-color:#ff0000;
}
.box-title.note {
    font-size: 11px!important;
    padding: 0 10px!important;
}
</style>
@endsection

@section('content')
<div class="step-header">
    <ul>
    @foreach($steps as $k=>$step)
        <li class="step-item-count step-item-{{$k+1}}">{{$step['title']}}</li>
    @endforeach
    </ul>
</div>
<form action="" method="POST" id="accept-wizard">
@csrf
<article id="wizard-step-1" class="clearfix wizard-holder">
    <div class="wizard-title" style="margin-bottom:20px;">
    Harga Sewa dan Tanggal Penagihan Tetap (Prorata)
    </div>

    <div class="col-sm-7">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Informasi Calon Penghuni</h3>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <tr>
                        <td style="width:180px;">Nama Calon Penghuni</td>
                        <td><input class="form-control" name="name" type="text" value="{{$booking->contact_name}}" readonly/></td>
                    </tr>
                    <tr>
                        <td>No HP Penghuni</td>
                        <td><input class="form-control" name="phone_number" type="text" value="{{$booking->contact_phone}}" readonly/></td>
                    </tr>
                    <tr>
                        <td>Email Calon Penghuni</td>
                        <td><input class="form-control" name="email" type="text" value="{{$booking->contact_email}}" readonly/></td>
                    </tr>
                    <tr>
                        <td>Tanggal Masuk (check-in)</td>
                        <td><input class="form-control" name="start_date" type="text" value="{{date('Y-m-d',strtotime($booking->checkin_date))}}" readonly/></td>
                    </tr>
                    <tr>
                        <td>Hitungan Sewa Digunakan</td>
                        <td><input class="form-control" name="rent_type" type="text" value="{{$booking->rent_count_type}}" readonly/></td>
                    </tr>
                    <tr>
                        <td>Durasi Sewa</td>
                        <td><input class="form-control" name="duration" type="text" value="{{$booking->stay_duration}}" readonly/></td>
                    </tr>
                    <tr>
                        <td>Waktu Request Booking</td>
                        <td><input class="form-control" type="text" value="{{$booking->created_at->format('d F Y')}}" readonly/></td>
                    </tr>
                    <tr>
                        <td>Harga Listing</td>
                        <td><input class="form-control" type="text" value="{{ $booking->price - ( $booking->mamikos_discount_value + $booking->owner_discount_value) }}" readonly/></td>
                    </tr>
                    <tr>
                        <td>Harga Asli Owner</td>
                        <td><input class="form-control" type="text" value="{{ $booking->price - $booking->owner_discount_value }}" readonly/></td>
                    </tr>
                </table>
                
            </div>
        </div>
    </div>

    <div class="col-sm-5">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">&nbsp;</h3>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <tr>
                        <td style="width:150px;">Harga Sewa</td>
                        <td><input oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" type="text" name="amount" value="{{ $booking->price - ( $booking->mamikos_discount_value + $booking->owner_discount_value) }}" class="form-control required"/></td>
                    </tr>
                    <tr>
                        <td>No Kamar</td>
                        <td>
{{--                            <input type="text" name="room_number" value="" class="form-control required"/>--}}
                            <select name="designer_room_id" class="form-control">
                                <option value="">Pilih di Tempat</option>
                                    @foreach ($room_units as $unit)
                                        <option value="{{ $unit['id'] }}" @if($unit['disable']) disabled @endif>
                                            {{ $unit['name'] . ' - ' . $unit['floor'] . ' - ' . (($unit['is_gold_plus']) ? $unit['level_name'] : 'No')}}
                                        </option>
                                    @endforeach
                            </select>
                        </td>
                    </tr>
                    @if (!in_array($booking->rent_count_type,['weekly','daily']))
                        <tr>
                            <td>Tanggal Penagihan Tetap</td>
                            <td><select class="form-control" name="billing_date"><option value="">- Pilih -</option>@for($x=1; $x<=28; $x++)<option value="{{$x}}">{{$x}}</option> @endfor</select></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><a href="javascript:$.fn.prorataCalculation();" class="btn btn-warning btn-xs">Hitung Biaya Prorata</a>&nbsp;&nbsp;<a href="javascript:$.fn.cancelProrata();" class="btn btn-danger btn-xs">Batalkan</a></td>
                        </tr>
                        <tr>
                            <td>Biaya Prorata</td>
                            <td><input readonly oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" type="text" class="form-control" value="" name="first_amount"/></td>
                        </tr>
                    @endif
                </table>
            </div>
        </div>
    </div>

    <div class="col-xs-12 step-footer">
        <a href="/admin/booking/users?#booking-users" class="btn btn-info">Batalkan</a>&nbsp;&nbsp;<a href="javascript:$.fn.wizardActive(2,true);" class="btn btn-success">Lanjutkan</a>
    </div>

</article>

<article id="wizard-step-2" class="clearfix wizard-holder">
    <div class="wizard-title" style="margin-bottom:20px;">
    Tambahan Biaya
    </div>

    <div class="col-sm-6">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Tambahan Biaya Tetap</h3>
                <p class="box-title note">Tambahan biaya tetap : Biaya yang dibebankan di setiap tagihan</p>
            </div>
            <div class="box-body">
                <table class="table">
                     <tr id="biaya-tetap-input">
                        <td>
                            <input type="text" class="form-control field-title" placeholder="Nama Biaya"/>
                        </td>
                        <td>
                            <input type="text" class="form-control field-value" placeholder="Nominal" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"/>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><a href="javascript:$.fn.tambahBiaya('fixed');" class="btn btn-xs btn-success">Tambahkan</a></td>
                    </tr>
                    <tr class="tambah-biaya-tetap">
                        <td></td><td></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

    <div class="col-sm-4" style="display: none;">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Tambahan Biaya Lainnya</h3>
            </div>
            <div class="box-body">
                <table class="table">
                    <tr id="biaya-lainnya-input">
                        <td><input type="text" class="form-control field-title" value="" placeholder="Nama Biaya"/></td>
                        <td><input type="text" class="form-control field-value" value="" placeholder="Nominal" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"/></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><a href="javascript:$.fn.tambahBiaya('other');" class="btn btn-xs btn-success">Tambahkan</a></td>
                    </tr>
                    <tr class="tambah-biaya-lainnya">
                        <td></td><td></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Aturan Denda</h3>
            </div>
            <div class="box-body">
                @php
                    $fineType       = null;
                    $fineTypeValue  = null;
                    $dpPercentage   = null;
                    if(isset($room_price_component['fine']['meta'])) {
                        foreach($room_price_component['fine']['meta'] as $meta) {
                            if($meta['key'] == 'fine_duration_type') {
                                $fineType = $meta['value'];
                            } else if ($meta['key'] == 'fine_maximum_length') {
                                $fineTypeValue = $meta['value'];
                            }
                        }
                    }
                    if(isset($room_price_component['dp']['meta'])) {
                        foreach($room_price_component['dp']['meta'] as $meta) {
                            if($meta['key'] == 'percentage') {
                                $dpPercentage = $meta['value'];
                            }
                        }
                    }
                @endphp
                <table class="table">
                    <tr>
                        <td><input oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" type="text" name="fine_maximum_length" class="form-control fine-config" value="{{ $fineTypeValue != null ? $fineTypeValue : '' }}" placeholder="Setelah"/> </td>
                        <td>
                            <select class="form-control fine-config" name="fine_duration_type">
                                <option value="">-Pilih Tipe Hitungan-</option>
                                @foreach($fine_types as $typeKey => $typeValue)
                                    <option @if($fineType != null && $typeKey == $fineType) selected @endif value="{{ $typeKey }}">{{ $typeValue }}</option>
                                @endforeach
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Sebesar</td>
                        <td><input oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" type="text" class="form-control fine-config" name="fine_amount" value="{{ isset($room_price_component['fine']['price']) ? $room_price_component['fine']['price'] : '' }}" placeholder="Nominal"/></td>
                    </tr>
                </table>
            </div>
            <div class="box-header">
                <h3 class="box-title">Deposit</h3>
            </div>
            <div class="box-body">
                <input oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" type="text" class="form-control" name="deposit_amount" value="{{ isset($room_price_component['deposit']['price']) ? $room_price_component['deposit']['price'] : '' }}" placeholder="Nominal"/>
            </div>
        </div>
    </div>

    <div class="col-xs-12 step-footer">
        <a href="javascript:$.fn.wizardActive(1);" class="btn btn-info">Kembali</a>&nbsp;&nbsp;<a href="javascript:$.fn.wizardActive(3,true);" class="btn btn-success">Lanjutkan</a>
    </div>

</article>

<article id="wizard-step-3" class="clearfix wizard-holder">
    <div class="wizard-title" style="margin-bottom:20px;">
    Down Payment
    </div>

    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header use-dp-head" style="text-align:center;">
                <h3 class="box-title" style="text-align:center; float:none;">Apakah Menggunakan Down Payment?</h3>
            </div>
            <div class="box-body use-dp-head" style="text-align:center;">
                <table class="table">
                    <tr>
                        <td>
                            <a href="javascript:$.fn.useDP(true);" style="width:120px;" class="btn btn-xs btn-success">Ya</a>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="use-dp" style="display:none;">
                <div class="box-body">
                    <input type="hidden" name="use_dp" value="{{ isset($room_price_component['dp']) ? 1 : 0 }}">
                    <table class="table">
                        <tr>
                            <td>
                                <select class="form-control" id="dp-percentage" name="dp_percentage">
                                    <option value="">-Pilih Tipe-</option>
                                    <option @if($dpPercentage != null && $dpPercentage == 10) selected @endif value="10">10% dari harga sewa</option>
                                    <option @if($dpPercentage != null && $dpPercentage == 20) selected @endif value="20">20% dari harga sewa</option>
                                    <option @if($dpPercentage != null && $dpPercentage == 30) selected @endif value="30">30% dari harga sewa</option>
                                    <option @if($dpPercentage != null && $dpPercentage == 40) selected @endif value="40">40% dari harga sewa</option>
                                    <option @if($dpPercentage != null && $dpPercentage == 50) selected @endif value="50">50% dari harga sewa</option>
                                    <option value="manual">Isi Manual</option>
                                </select>
                            </td>
                            <td>
                                <input type="text" class="form-control" name="dp_amount" placeholder="Nominal" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" value=""/>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <a href="javascript:$.fn.tambahDP();" class="btn btn-xs btn-success">Tambahkan</a>&nbsp;&nbsp;
                                <a href="javascript:$.fn.useDP(false);" class="btn btn-xs btn-warning">Batalkan</a>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="box-body dp-detail" style="display:none;">
                    <table class="table table-striped">
                        <tr>
                            <td>Batas Pembayaran DP</td>
                            <td>Batas Pelunasan</td>
                        </tr>
                        <tr>
                            <td class="tanggal-dp"></td>
                            <td class="tanggal-st"></td>
                        </tr>
                        <tr>
                            <td>Biaya Pembayaran DP</td>
                            <td>Biaya Pelunasan</td>
                        </tr>
                        <tr>
                            <td class="nominal-dp"></td>
                            <td class="nominal-st"></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xs-12 step-footer">
        <a href="javascript:$.fn.wizardActive(2);" class="btn btn-info">Kembali</a>&nbsp;&nbsp;<a href="javascript:$.fn.wizardActive(4,true);" class="btn btn-success">Lanjutkan</a>
    </div>

</article>

<article id="wizard-step-4" class="clearfix wizard-holder">
    <div class="wizard-title" style="margin-bottom:20px;">
    Konfirmasi
    </div>
    <div class="col-sm-6">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Informasi Calon Penghuni</h3>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <tr>
                        <td style="width:180px;">Nama Calon Penghuni</td>
                        <td>: {{$booking->contact_name}}</td>
                    </tr>
                    <tr>
                        <td>No HP Penghuni</td>
                        <td>: {{$booking->contact_phone}}</td>
                    </tr>
                    <tr>
                        <td>Email Calon Penghuni</td>
                        <td>: {{$booking->contact_email}}</td>
                    </tr>
                    <tr>
                        <td>Tanggal Masuk (check-in)</td>
                        <td>: {{date('d F Y',strtotime($booking->checkin_date))}}</td>
                    </tr>
                    <tr>
                        <td>Hitungan Sewa Digunakan</td>
                        <td>: {{$booking->rent_count_type}}</td>
                    </tr>
                    <tr>
                        <td>Durasi Sewa</td>
                        <td>: {{$booking->stay_duration}}</td>
                    </tr>
                    <tr>
                        <td>Waktu Request Booking</td>
                        <td>: {{$booking->created_at->format('d F Y')}}</td>
                    </tr>
                    <tr>
                        <td>Harga Listing</td>
                        <td>: {{ $booking->price - ( $booking->mamikos_discount_value + $booking->owner_discount_value) }}</td>
                    </tr>
                    <tr>
                        <td>Harga Asli Owner</td>
                        <td>: {{ $booking->price - ( $booking->owner_discount_value) }}</td>
                    </tr>
                </table>
            </div>
            <div class="box-header">
                <h3 class="box-title">Tambahan Biaya Tetap</h3>
            </div>
            <div class="box-body">
                <table class="table table-striped table-biaya-tetap">
                    <tbody>
                        <tr><td>-</td></tr>
                    </tbody>
                </table>
            </div>
            <div class="box-header" style="display: none;">
                <h3 class="box-title">Tambahan Biaya Lainnya</h3>
            </div>
            <div class="box-body" style="display: none;">
                <table class="table table-striped table-biaya-lainnya">
                    <tbody>
                        <tr><td>-</td></tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Aturan Denda (berlaku setelah)</h3>
            </div>
            <div class="box-body">
                <table class="table table-striped table-denda">
                    <tbody>
                        <tr><td>-</td></tr>
                    </tbody>
                </table>
            </div>
            <div class="box-header">
                <h3 class="box-title">Deposit</h3>
            </div>
            <div class="box-body">
                <table class="table table-striped table-deposit">
                    <tbody>
                        <tr><td>-</td></tr>
                    </tbody>
                </table>
            </div>
            <div class="box-header">
                <h3 class="box-title">Down Payment</h3>
            </div>
            <div class="box-body">
                <table class="table table-striped table-downpayment">
                    <tbody>
                        <tr><td>-</td></tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-xs-12 step-footer">
        <a href="javascript:$.fn.wizardActive(3);" class="btn btn-info">Kembali</a>&nbsp;&nbsp;<a href="javascript:$.fn.submitData();" class="btn btn-success">Konfirmasi</a>
    </div>
</article>
</form>
@endsection

@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment-with-locales.min.js"></script>
<script type="text/javascript">

    var useDpData = 0;
    $(document).ready(function(e) {
        useDpData = "{{ isset($room_price_component['dp']) ? 1 : 0 }}";
        var currentDataFirstLoad = JSON.parse(window.sessionStorage.addTenant_{{$booking->id}});
        
        if (!currentDataFirstLoad.use_dp) {    
            if (useDpData == 1) {
                $('#dp-percentage').trigger('change');
                $.fn.useDP(true);
                $.fn.tambahDP();
            }
        }

        if (!currentDataFirstLoad.costs) {
            @if(isset($room_price_component['additional']))
                @foreach($room_price_component['additional'] as $additionalPrice)
                    var name = "{{ $additionalPrice['name'] }}";
                    var price = "{{ $additionalPrice['price'] * $multiplication_additional_price }}";

                    $.fn.tambahBiayaFromDB('fixed', name, price);
                @endforeach
            @endif
        }
    
    });

    $.fn.pad2 = function(number) {
        return (number < 10 ? '0' : '') + number;
    }

    $.fn.saveState = function(n)
    {
        var errors = [];

        var currentData = JSON.parse(window.sessionStorage.addTenant_{{$booking->id}}) ? JSON.parse(window.sessionStorage.addTenant_{{$booking->id}}) : {};

        if (n==1) {
            $('#wizard-step-'+n).find('select,input').each(function(){
                if ($(this).attr('name') === undefined ) {
                   return;
                }

                $(this).removeClass('has-error');
                
                if ($(this).hasClass('required') && $(this).val()=='') {
                    $(this).addClass('has-error');
                    errors.push($(this).attr('name'));
                }

                if ($('[name="billing_date"]').val() && !$('[name="first_amount"]').val()) {
                    $('input[name="first_amount"]').addClass('has-error');
                    errors.push('first_amount');
                }
            
                currentData[$(this).attr('name')] = $(this).val();
            });
        }

        if (n==2) {
            var errors = [];

            $('.fine-config').remove('has-error');

            var isFilled = false;

            $('.fine-config').each(function(){
                if ($(this).val()!=='') {
                    isFilled = true;
                }
            });

            if (isFilled) {
                $('.fine-config').each(function(){
                    if ($(this).val()=='') {
                       $(this).addClass('has-error');
                       errors.push($(this).attr('name'));
                    }
                });
            }

            $('#wizard-step-'+n).find('select,input').each(function(){
                if ($(this).attr('name') === undefined ) {
                   return;
                }

                currentData[$(this).attr('name')] = $(this).val();
            });
        }

        window.sessionStorage.addTenant_{{$booking->id}} = JSON.stringify(currentData);

        return errors;
        
    }

    $.fn.prorataCalculation = function()
    {
        var amount = $('input[name="amount"]').val();
        var fixedDate = $('select[name="billing_date"]').val();

        if (fixedDate) {
            var checkinBySchedule = moment("{{date('Y-m-',strtotime($booking->checkin_date))}}"+$.fn.pad2(fixedDate),'YYYY-MM-DD');
            var nextDate = '';

            switch("{{$booking->rent_count_type}}") {
                case 'daily':
                    nextDate = checkinBySchedule.add(1,'days').calendar();
                    break;
                case 'weekly':
                    nextDate = checkinBySchedule.add(1,'weeks').calendar();
                    break;
                case 'monthly':
                    nextDate = checkinBySchedule.add(1,'M').calendar();
                    break;
                case 'quarterly':
                    nextDate = checkinBySchedule.add(3,'M').calendar();
                    break;
                case 'semiannually':
                    nextDate = checkinBySchedule.add(6,'M').calendar();
                    break;
                case 'anually':
                    nextDate = checkinBySchedule.add(1,'Y').calendar();
                    break;
                default:
                    nextDate = checkinBySchedule.add(1,'M').calendar();
                    break;
            }

            var startMoment = moment("{{date('Y-m-',strtotime($booking->checkin_date))}}"+$.fn.pad2(fixedDate),'YYYY-MM-DD');
            var nextMoment = moment(nextDate,'MM/DD/YYYY');
            var totalDaysPerDuration = nextMoment.diff(startMoment,'days');

            var checkinDate = {{date('j',strtotime($booking->checkin_date))}};
            var fixedDateGreaterThanCheckin = fixedDate > checkinDate ? true : false;

            var diffDays = 0;

            if (fixedDateGreaterThanCheckin) {
                diffDays = fixedDate - checkinDate;
            } else {
                diffDays = nextMoment.diff(moment("{{date('Y-m-d',strtotime($booking->checkin_date))}}",'YYYY-MM-DD'),'days');
            }

            var proRata = Math.round(diffDays * ({{$booking->listing_price}}/totalDaysPerDuration));
            
            if (proRata) {
                $('input[name="first_amount"]').val(proRata);
            }else {
                $('select[name="billing_date"]').val('');
            }
        }
    }

    $.fn.cancelProrata = function()
    {
        $('select[name="billing_date"]').val('');
        $('input[name="first_amount"]').val('');
    }

    $.fn.prorataCalculation();

    $.fn.reloadBiaya = function() {
        var currentData = JSON.parse(window.sessionStorage.addTenant_{{$booking->id}});
        $('.additional-cost-row').remove();
        $.each(currentData,function(key,value){
            if (key=='costs') {
                $.each(value,function(x,v){
                    if (v) {
                        if (v.cost_type=='fixed') {
                            var inputFixed = '<input name="costs[fixed]['+x+'][name]" value="'+v.cost_title+'" type="hidden"/><input name="costs[fixed]['+x+'][value]" value="'+v.cost_value+'" type="hidden"/>';
                            $('.tambah-biaya-tetap').after('<tr class="additional-cost-row"><td>'+v.cost_title+inputFixed+'</td><td>'+v.cost_value+' <a style="float:right;" class="fa fa-trash" href="javascript:$.fn.removeBiaya('+x+')"></a></td></tr>');
                        } else {
                            var inputOther = '<input name="costs[other]['+x+'][name]" value="'+v.cost_title+'" type="hidden"/><input name="costs[other]['+x+'][value]" value="'+v.cost_value+'" type="hidden"/>';
                            $('.tambah-biaya-lainnya').after('<tr class="additional-cost-row"><td>'+v.cost_title+inputOther+'</td><td>'+v.cost_value+' <a style="float:right;" class="fa fa-trash" href="javascript:$.fn.removeBiaya('+x+')"></a></td></tr>');
                        }
                    }
                });
            }
        });
    }

    $.fn.removeBiaya = function(i) {
        var currentData = JSON.parse(window.sessionStorage.addTenant_{{$booking->id}});
        delete currentData.costs[i];
        window.sessionStorage.addTenant_{{$booking->id}} = JSON.stringify(currentData);
        $.fn.reloadBiaya();
    }

    $.fn.tambahBiaya = function(type)
    {
        var element = type == 'fixed' ? $('#biaya-tetap-input') : $('#biaya-lainnya-input');
        element.find('input').removeClass('has-error');

        var title = element.find('input.field-title').val();
        var value = element.find('input.field-value').val();

        if (!title) {
           element.find('input.field-title').addClass('has-error');
        }

        if (!value) {
            element.find('input.field-value').addClass('has-error');
        }

        if (title && value) {
            var currentData = JSON.parse(window.sessionStorage.addTenant_{{$booking->id}}) ? JSON.parse(window.sessionStorage.addTenant_{{$booking->id}}) : {};
            
            var cost = {};
            cost['cost_title']=title;
            cost['cost_value']=value;
            cost['cost_type']=type;
            
            if(!currentData['costs']) {
                currentData['costs'] = [];
            }

            currentData.costs.push(cost);

            window.sessionStorage.addTenant_{{$booking->id}} = JSON.stringify(currentData);

            $.fn.reloadBiaya();

            element.find('input.field-title').val('');
            element.find('input.field-value').val('');
        }
    }

    $.fn.tambahBiayaFromDB = function(type, title, value)
    {
        if (title && value) {
            var currentData = JSON.parse(window.sessionStorage.addTenant_{{$booking->id}}) ? JSON.parse(window.sessionStorage.addTenant_{{$booking->id}}) : {};
            
            var cost = {};
            cost['cost_title']=title;
            cost['cost_value']=value;
            cost['cost_type']=type;
            
            if(!currentData['costs']) {
                currentData['costs'] = [];
            }

            currentData.costs.push(cost);

            window.sessionStorage.addTenant_{{$booking->id}} = JSON.stringify(currentData);

            $.fn.reloadBiaya();
        }
    }
    
    $.fn.reloadDP = function()
    {
        if($('#dp-percentage').val()!=='manual' && $('input[name="use_dp"]').val()==1) {
            $('#dp-percentage').trigger('change');
        }

        var currentData = JSON.parse(window.sessionStorage.addTenant_{{$booking->id}}) ? JSON.parse(window.sessionStorage.addTenant_{{$booking->id}}) : {};
        
        $('.nominal-dp').html(currentData.dp_amount);

        $('input[name="dp_amount"]').val(currentData.dp_amount);
        
        var dpDate = moment().add(1,'days').format('D MMMM YYYY');
        var stDate = moment().add(2,'days').format('D MMMM YYYY');

        var dpDateInput = '<input type="hidden" name="dp_date" value="'+moment().add(1,'days').format('YYYY-MM-DD')+'"/>';
        var stDateInput = '<input type="hidden" name="dp_settlement_date" value="{{date('Y-m-d',strtotime($booking->checkin_date))}}"/>';

        $('.tanggal-dp').html(dpDate+dpDateInput);
        $('.tanggal-st').html("{{date('d F Y',strtotime($booking->checkin_date))}}");

        var totalAmount = 0;
        
        totalAmount+= currentData.first_amount ? parseFloat(currentData.first_amount) : parseFloat(currentData.amount);
        totalAmount+= currentData.deposit_amount ? parseFloat(currentData.deposit_amount) : 0;
        
        if (currentData.costs) {
            $.each(currentData.costs,function(k,v){
                if (v) {
                    totalAmount+=parseFloat(v.cost_value);
                }
            });
        }

        var stAmount = totalAmount - currentData.dp_amount;
        $('.nominal-st').html(stAmount);

        if (currentData.use_dp) {
            $('.dp-detail').show();
        } else {
            $('.dp-detail').hide();
        }
    }

    $.fn.useDP = function(use)
    {
        if (use) {
            $('#use-dp').show();
            $('input[name="use_dp"]').val(1);
            $('.use-dp-head').hide();
            $('.dp-detail').hide();
        } else {
            var currentData = JSON.parse(window.sessionStorage.addTenant_{{$booking->id}});

            $('.use-dp-head').show();
            $('#use-dp').hide();
            $('input[name="use_dp"]').val(0);
            $('input[name="dp_amount"]').val('');

            if (currentData.use_dp) {
                currentData.use_dp = 0;
                window.sessionStorage.addTenant_{{$booking->id}} = JSON.stringify(currentData);
            }

            if (currentData.dp_amount) {
                currentData.dp_amount = 0;
                window.sessionStorage.addTenant_{{$booking->id}} = JSON.stringify(currentData);
            }

            if (currentData.dp_percentage) {
                $('[name="dp_percentage"]').val('');
                currentData.dp_percentage = '';
                window.sessionStorage.addTenant_{{$booking->id}} = JSON.stringify(currentData);
            }
        }
    }

    $('#dp-percentage').on('change',function(){
        var currentData = JSON.parse(window.sessionStorage.addTenant_{{$booking->id}});
        $('.dp-detail').hide();
        if ($(this).val()!=='manual') {
            var dpAmount = $(this).val()/100 * {{$booking->price}};
            $('input[name="dp_amount"]').val(Math.round(dpAmount));
            $('input[name="dp_amount"]').attr('readonly','readonly');
        } else {
            $('input[name="dp_amount"]').removeAttr('readonly');
            $('input[name="dp_amount"]').val('');
        }

        if (currentData.dp_amount !== $('input[name="dp_amount"]').val() && currentData.use_dp) {
            $.fn.tambahDP();
        }
    });

    $.fn.tambahDP = function()
    {
        $('#dp-percentage').removeClass('has-error');
        $('input[name="dp_amount"]').removeClass('has-error');

        var dp_percentage = $('#dp-percentage').val();
        var dp_amount = $('input[name="dp_amount"]').val();

        if (!dp_percentage) {
            $('#dp-percentage').addClass('has-error');
        }

        if (!dp_amount) {
            $('input[name="dp_amount"]').addClass('has-error');
        }

        if (dp_percentage && dp_amount) {
            var currentData = JSON.parse(window.sessionStorage.addTenant_{{$booking->id}}) ? JSON.parse(window.sessionStorage.addTenant_{{$booking->id}}) : {};
            
            currentData['use_dp'] = 1;
            currentData['dp_amount'] = dp_amount;
            currentData['dp_percentage'] = dp_percentage;

            window.sessionStorage.addTenant_{{$booking->id}} = JSON.stringify(currentData);

            $.fn.reloadDP();
        }
    }

    $.fn.showConfirmation = function()
    {
        var currentData = JSON.parse(window.sessionStorage.addTenant_{{$booking->id}});
        
        if (currentData.costs) {
            var tableBiayaTetap = '';
            var tableBiayaLainnya = '';
            
            $.each(currentData.costs,function(k,v){
                if (v) {
                    if (v.cost_type=='fixed') {
                        tableBiayaTetap+='<tr><td style="width:25%;">'+v.cost_title+'</td><td>'+v.cost_value+'</td></tr>';
                    } else {
                        tableBiayaLainnya+='<tr><td style="width:25%;">'+v.cost_title+'</td><td>'+v.cost_value+'</td></tr>';
                    }
                }
            });
            
            if (tableBiayaTetap) {
                $('.table-biaya-tetap tbody').html(tableBiayaTetap);
            } else {
                $('.table-biaya-tetap tbody').html('<tr><td>-</td></tr>');
            }

            if (tableBiayaLainnya) {
                $('.table-biaya-lainnya tbody').html(tableBiayaLainnya);
            } else {
                $('.table-biaya-lainnya tbody').html('<tr><td>-</td></tr>');
            }
        }

        if (currentData.use_dp) {
            var dpPilihan = currentData.dp_percentage == 'manual' ? 'Nominal' : currentData.dp_percentage+'%';
            var dbRow = '<tr><td style="width:50%;">Pilihan Down Payment</td><td>'+dpPilihan+'</td></tr>';
            dbRow+= '<tr><td>Batas Pelunasan DP</td><td>Batas Pelunasan ST</td></tr>';
            dbRow+= '<tr><td>'+$('.tanggal-dp').html()+'</td><td>'+$('.tanggal-st').html()+'</td></tr>';
            dbRow+= '<tr><td>Biaya DP</td><td>Biaya Pelunasan</td></tr>';
            dbRow+= '<tr><td>'+$('.nominal-dp').html()+'</td><td>'+$('.nominal-st').html()+'</td></tr>';

            $('.table-downpayment tbody').html(dbRow);

        } else {
            $('.table-downpayment tbody').html('<tr><td>-</td></tr>');
        }

        if (currentData.deposit_amount) {
            $('.table-deposit tbody').html('<tr><td>'+currentData.deposit_amount+'</td></tr>');
        } else {
            $('.table-deposit tbody').html('<tr><td>-</td></tr>');
        }

        if (currentData.fine_amount && currentData.fine_duration_type && currentData.fine_maximum_length) {
            var hitungan = '';

            switch (currentData.fine_duration_type) {
                case 'day':
                    hitungan = 'hari';
                break;
                case 'week':
                    hitungan = 'minggu';
                break;
                case 'month':
                    hitungan = 'bulan';
                break;
                case 'year':
                    hitungan = 'tahun';
                break;
            }

            var rowA = '<tr><td style="width:25%;">'+currentData.fine_maximum_length+'</td><td>'+hitungan+'</td></tr>';
            var rowB = '<tr><td>Sebesar</td><td>'+currentData.fine_amount+'</td></tr>';

            $('.table-denda tbody').html(rowA+rowB);

        } else {
            $('.table-denda tbody').html('<tr><td>-</td></tr>');
        }
    }

    //ENTRY POINT!
    if (!window.sessionStorage.addTenant_{{$booking->id}}) {
        window.sessionStorage.setItem('addTenant_'+{{$booking->id}},JSON.stringify(''));
    } else {
        var currentData = JSON.parse(window.sessionStorage.addTenant_{{$booking->id}});

        $.each(currentData,function(key,value){
            if (key!=='costs') {
                $('[name="'+key+'"]').val(value);
            }
        });

        $.fn.reloadBiaya();
        
        if (currentData.use_dp) {
           $.fn.useDP(true); 
           $.fn.reloadDP();
        }

        $('#dp-percentage').trigger('change');

        $.fn.showConfirmation();
    }

    $.fn.wizardActive = function(n,validate=false)
    {
        var continueNext = true;

        if (validate) {
            var errors = $.fn.saveState(n-1);
            if (errors.length) {
                continueNext = false;
            }
        }

        if (continueNext) {
            if (n==3) {
                $.fn.reloadDP();
            }
            if (n==4) {
                if ($('input[name="use_dp"]').val()==1 && $('input[name="dp_amount"]').val()=='') {
                    return false;
                }

                $.fn.showConfirmation();
            }
            $('.wizard-holder').hide();
            $('.step-item-count').removeClass('active');
            $('#wizard-step-'+n).show();
            $('.step-item-'+n).addClass('active');
        }
    }

    $.fn.wizardActive(1);
    
    $.fn.submitData = function() {
        $('#accept-wizard').submit();
    }
</script>
@endsection