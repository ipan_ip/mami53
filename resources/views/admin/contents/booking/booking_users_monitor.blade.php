@extends('admin.layouts.main')

@section('style')
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
	<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
	<link href="//cdn.jsdelivr.net/npm/select2-bootstrap-theme@0.1.0-beta.10/dist/select2-bootstrap.min.css" rel="stylesheet" />
	<link rel="stylesheet" href="/css/admin-consultant.css">

	<style>
		.no-search .select2-search {
			display: none;
		}
	</style>
@endsection

@section('content')
	<div class="box box-default">
		<!-- box-header -->
		<div class="box-header" style="padding-top: 10px;">
			<div class="col-md-12">
				<h3 class="box-title" style="padding-left: 0;"><i class="fa fa-sticky-note"></i>&nbsp;Booking Users Monitor</h3>
			</div>
		</div>
		<!-- /.box-header -->
	</div>

	<div class="box box-default">
		<div class="box-header with-border" style="padding: 0;">
			<h3 class="box-title">Filter</h3>
		</div>

		<!-- box-body -->
		<div class="box-body no-padding no-overflow">
			<div style="padding: 15px;">
				<div class="row">
					<form action="" id="apply-filter">

						<div class="col-xs-12">
							<div class="row">

								<div class="col-xs-12">
									<label for="">Admin BSE</label>
								</div>

								<div class="col-sm-6">
									<div class="form-group">
										<select name="" id="filter-city-by-username" class="form-control select2-single">
											<option value="all" selected>All</option>
										</select>
									</div>
								</div>
							</div>
						</div>

						<div class="col-xs-12">
							<div class="row">

								<div class="col-xs-12">
									<label for="">Time</label>
								</div>

								<div class="col-sm-6">
									<div class="form-group" style="margin-bottom: 0;">
										<select name="" id="filter-select" class="form-control">
											<option value="today" selected>Today</option>
											<option value="yesterday">Yesterday</option>
											<option value="last_7_days">Last 7 Days</option>
										</select>
									</div>
								</div>
								<div class="col-sm-6">
									<button class="btn btn-primary" type="submit">Apply</button>
								</div>
							</div>
						</div>

					</form>
				</div>
			</div>
		</div>
		<!-- /.box-body -->
	</div>

	<div class="box box-default" id="cards-display" style="display: none;">
		<div class="box-body no-padding no-overflow">
			<div style="padding: 15px 15px 0;">

				<div class="monitor-group blue">
					<div class="small-box" id="value-blue-1">
						<div class="inner">
							<h3>X</h3>
							<p>
								Booking Status:
								<br>
								Booked and Instant Booking Active
							</p>
						</div>
						<a
							href=""
							class="small-box-footer"
						>
							See detail <i class="fa fa-angle-right"></i>
						</a>
					</div>

					<div class="small-box" id="value-blue-2">
						<div class="inner">
							<h3>X</h3>
							<p>
								Booking Status:
								<br>
								Confirmed
							</p>
						</div>
						<a href="" class="small-box-footer">
							See detail <i class="fa fa-angle-right"></i>
						</a>
					</div>

					<div class="small-box" id="value-blue-3">
						<div class="inner">
							<h3>X</h3>
							<p>
								Booking Status:
								<br>
								Paid
							</p>
						</div>
						<a href="" class="small-box-footer">
							See detail <i class="fa fa-angle-right"></i>
						</a>
					</div>

					<div class="small-box" id="value-blue-4">
						<div class="inner">
							<h3>X</h3>
							<p>
								Booking Remarks Status:
								<br>
								Akan Bayar
							</p>
						</div>
						<a href="" class="small-box-footer">
							See detail <i class="fa fa-angle-right"></i>
						</a>
					</div>
				</div>

				<div class="monitor-group green">
					<div class="small-box" id="value-green-1">
						<div class="inner">
							<h3>X</h3>
							<p>
								Booking Remarks Status:
								<br>
								Ingin Survey
							</p>
						</div>
						<a href="" class="small-box-footer">
							See detail <i class="fa fa-angle-right"></i>
						</a>
					</div>

					<div class="small-box" id="value-green-3">
						<div class="inner">
							<h3>X</h3>
							<p>
								Booking Status:
								<br>
								Checked In
							</p>
						</div>
						<a href="" class="small-box-footer">
							See detail <i class="fa fa-angle-right"></i>
						</a>
					</div>

					<div class="small-box" id="value-green-4">
						<div class="inner">
							<h3>X</h3>
							<p>
								Booking Status:
								<br>
								Check In Date
							</p>
						</div>
						<a href="" class="small-box-footer">
							See detail <i class="fa fa-angle-right"></i>
						</a>
					</div>

					<div class="small-box" id="value-green-5">
						<div class="inner">
							<h3>X</h3>
							<p>
								Booking Status:
								<br>
								Check Out Date
							</p>
						</div>
						<a href="" class="small-box-footer">
							See detail <i class="fa fa-angle-right"></i>
						</a>
					</div>
				</div>

				<div class="monitor-group red">
					<div class="small-box" id="value-red-1">
						<div class="inner">
							<h3>X</h3>
							<p>
								Booking Status:
								<br>
								Rejected
							</p>
						</div>
						<a href="" class="small-box-footer">
							See detail <i class="fa fa-angle-right"></i>
						</a>
					</div>

					<div class="small-box" id="value-red-2">
						<div class="inner">
							<h3>X</h3>
							<p>
								Booking Status:
								<br>
								Cancelled
							</p>
						</div>
						<a href="" class="small-box-footer">
							See detail <i class="fa fa-angle-right"></i>
						</a>
					</div>
				</div>

			</div>
		</div>
	</div>
@endsection

@section('script')
	<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.27.0/moment.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
	<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

	<script>
		$(function() {
			const $valueBlue1 = $('#value-blue-1');
			const $valueBlue2 = $('#value-blue-2');
			const $valueBlue3 = $('#value-blue-3');
			const $valueBlue4 = $('#value-blue-4');
			const $valueGreen1 = $('#value-green-1');
			const $valueGreen3 = $('#value-green-3');
			const $valueGreen4 = $('#value-green-4');
			const $valueGreen5 = $('#value-green-5');
			const $valueRed1 = $('#value-red-1');
			const $valueRed2 = $('#value-red-2');
			const $filterSelect = $('#filter-select');
			const $cardsDisplay = $('#cards-display');
			const user = @json(Auth::user()->load(['consultant', 'consultant.roles', 'consultant.mapping']));
			let areaCity = '';
			let consultantId = 'all';

			// select admin bse filter automatically
			if (user.consultant && user.consultant.roles.find(r => r.role === 'admin_bse')) {
				const cities = user.consultant.mapping.map(city => city.area_city);
				const option = new Option(user.consultant.name, user.consultant.id, true, true);

				consultantId = user.consultant.id
				areaCity = areaCityParam(cities);
				$('#filter-city-by-username').append(option).trigger('change');
			}

			// filter by admin bse
			$('#filter-city-by-username').select2({
				theme: "bootstrap",
				dropdownCssClass : 'no-search',
				minimumResultsForSearch: Infinity,
				ajax: {
					url: '{{ url("admin/booking/admin-bse") }}',
					dataType: 'json',
					delay: 250,
					data: function (params) {
						return {
							offset: params.page * 10 || 0
						};
					},
					processResults: function(data, params) {
						params.page = params.page || 0;

						return {
							results: data.data,
							pagination: {
								more: data.pagination.more
							}
						}
					},
					cache: true
				},
				templateResult: (repo) => {
					if (repo.loading) {
						return 'Loading...';
					}
					return repo.name;
				},
				templateSelection: repo => repo.name || repo.text
			});


			$('#filter-city-by-username').on('select2:select', e => {
				const areaCityArray = e.params.data.area_city;

				consultantId = e.params.data.id;

				if (areaCityArray.length) {
					areaCity = areaCityParam(areaCityArray)
				} else {
					areaCity = '';
				}

			});

			function areaCityParam(cities) {
				return cities.reduce((acc, cur) => `${acc}&area_city[]=${cur}`, '');
			}


			const allSelector = [
				$valueBlue1,
				$valueBlue2,
				$valueBlue3,
				$valueBlue4,
				$valueGreen1,
				$valueGreen3,
				$valueGreen4,
				$valueGreen5,
				$valueRed1,
				$valueRed2
			];

			function urlQuery(value = {}, date = {}) {
				return `/admin/booking/users?
				phone=
				&contact_name=
				&owner_phone=
				&kost_name=
				&from=${date.from || ''}
				&to=${date.to || ''}
				&checkin_from=${value.checkin_from || ''}
				&checkin_to=${value.checkin_to || ''}
				&checkout_from=${value.checkout_from || ''}
				&checkout_to=${value.checkout_to || ''}
				&sort=0
				&kost_type=${value.kost_type || ''}
				&status=${value.status || ''}
				&changed_by=all
				&payment_status=${value.payment_status || 'all'}
				&instant_booking=${value.instant_booking || ''}
				&note_category=${value.note_category || ''}
				&transfer_permission=`
				.replace(/\s+/g, '');
			}

			function updateCards() {
				const dateRange = $filterSelect.val();

				const getDataInProcess = axios.get(`/admin/booking/users/count/in-process?created_at=${dateRange}&consultant_id=${consultantId}`)
				.then(response => {
					return response.data.data;
				});

				const getDataOnGoing = axios.get(`/admin/booking/users/count/on-going?created_at=${dateRange}&consultant_id=${consultantId}`)
				.then(response => {
					return response.data.data;
				});

				const getDataFinished = axios.get(`/admin/booking/users/count/finished?created_at=${dateRange}&consultant_id=${consultantId}`)
				.then(response => {
					return response.data.data;
				});

				Swal.fire({
					title: 'Applying filter...',
					onBeforeOpen: () => {
						Swal.showLoading();
					},
					allowOutsideClick: false
				});

				Promise.all([
					getDataInProcess,
					getDataOnGoing,
					getDataFinished
				])
				.then(response => {
					$cardsDisplay.show();
					let allValues = [];

					let queryDate = {};

					switch($filterSelect.val()) {
						case 'today':
							queryDate.from = moment().format('DD-MM-YYYY');
							queryDate.to = moment().format('DD-MM-YYYY');
							break;
						case 'yesterday':
							queryDate.from = moment().subtract(1, 'days').format('DD-MM-YYYY');
							queryDate.to = moment().subtract(1, 'days').format('DD-MM-YYYY');
							break;
						case 'last_7_days':
							queryDate.from = moment().subtract(6, 'days').format('DD-MM-YYYY');
							queryDate.to = moment().format('DD-MM-YYYY');
							break;
					}

					let allQuery = [
						// blue
						{
							status: 1,
							instant_booking: 'true'
						},
						{
							status: 2
						},
						{
							payment_status: 'paid'
						},
						{
							note_category: 'will_pay'
						},

						// green
						{
							note_category: 'survey'
						},
						{
							status: 12
						},
						{
							checkin_from: queryDate.from,
							checkin_to: queryDate.to,
							status: 10
						},
						{
							checkout_from: queryDate.from,
							checkout_to: queryDate.to,
							status: 10
						},

						// red
						{
							status: 3
						},
						{
							status: 4
						}
					];

					// combine all API response into 1 array
					response.forEach(element => {
						const newArray = Object.values(element);
						allValues.push.apply(allValues, newArray);
					});

					allSelector.forEach((element, index) => {
						element.find('h3').text(allValues[index]);
						if (index === 6 || index === 7) {
							element.find('a').attr('href', urlQuery(allQuery[index]) + areaCity);
						} else {
							element.find('a').attr('href', urlQuery(allQuery[index], queryDate) + areaCity);
						}
					});

					Swal.close();
				})
				.catch(error => {
					Swal.fire({
						title: 'Error',
						text: error,
						icon: 'error',
						confirmButtonText: 'Ok'
					});
				});
			}

			updateCards();

			$('#apply-filter').on('submit', function(e) {
				e.preventDefault();

				$cardsDisplay.hide();

				updateCards();
			});
		});
	</script>
@endsection