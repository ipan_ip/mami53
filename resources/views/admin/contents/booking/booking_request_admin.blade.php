@extends('admin.layouts.main')

@section('content')
	<div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

		<form action="" method="POST" class="form-horizontal form-bordered">
			<div class="box-body no-padding">
				@csrf
				<div class="form-group bg-default">
					<label for="name" class="control-label col-sm-2">Owner name</label>
					<div class="col-sm-10">
						<input type="text" name="owner_name" id="name" class="form-control" placeholder="Owner name" />
					</div>
				</div>

				<div class="form-group bg-default">
					<label for="name" class="control-label col-sm-2">Owner Phone</label>
					<div class="col-sm-10">
						<input type="text" name="owner_phone" id="name" class="form-control" placeholder="Owner phone" />
					</div>
				</div>

				<div class="form-group bg-default">
					<label for="name" class="control-label col-sm-2">Rek. Name</label>
					<div class="col-sm-10">
						<input type="text" name="rek_name" id="name" class="form-control" placeholder="Rek. name" />
					</div>
				</div>

				<div class="form-group bg-default">
					<label for="name" class="control-label col-sm-2">Rek. Number</label>
					<div class="col-sm-10">
						<input type="number" name="rek_number" id="name" class="form-control" placeholder="Rek. number" />
					</div>
				</div>

				<div class="form-group bg-default">
					<label for="name" class="control-label col-sm-2">Bank</label>
					<div class="col-sm-10">
						<select name="bank_code" class="form-control">
							@foreach ($banks as $key => $value)
								<option value="{{ $value->bank_code }}">{{ $value->bank_name }}</option>
							@endforeach
						</select>
					</div>
				</div>

				<div class="form-group bg-default">
					<label for="name" class="control-label col-sm-2">Bank city</label>
					<div class="col-sm-10">
						<select name="bank_city_code" class="form-control">
							@foreach ($bank_cities as $key => $value)
								<option value="{{ $value->city_code }}">{{ $value->city_name }}</option>
							@endforeach
						</select>
					</div>
				</div>

				<div class="form-group bg-default">
					<div class="col-sm-10 col-sm-push-2">
						<button type="submit" class="btn btn-primary">Submit</button>
					</div>
				</div>
			</div>
		</form>
    </div>
@endsection