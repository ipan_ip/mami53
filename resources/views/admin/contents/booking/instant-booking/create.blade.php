@extends('admin.layouts.main')

@section('style')
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" />
    <link href="//cdn.jsdelivr.net/npm/select2-bootstrap-theme@0.1.0-beta.10/dist/select2-bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/booking-form-admin.css') }}">
@endsection

@section('content')
    <div class="row">
        <div class="col-md-8">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Form Create Instant Booking</h3>
                </div>

                <form action="{{ URL::to('admin/booking/owner/instant-booking/store') }}" method="POST" enctype="multipart/form-data" class="form-horizontal form-bordered">
                    @csrf
                    <div class="box-body no-padding">
                        <div class="form-group bg-default">
                            <label for="" class="control-label col-sm-2">Owner</label>
                            <div class="col-sm-10">
                                <select id="inputOwner" class="form-control select2-single" required>
                                </select>
                                <input type="hidden" name="owner_id" class="form-control" id="inputOwnerId">
                                <br>
                                <div id="errorMessage" style="color: red;font-weight: bold;"></div>
                            </div>
                        </div>
                        <div class="form-group bg-default">
                            <label for="" class="control-label col-sm-2">Rent Counts</label>
                            <div class="col-sm-10">
                                <select class="form-control select2" name="rent_count[]" multiple="multiple" data-placeholder="Select a rent count" id="inputRentCounts" style="width: 100%;" required>
                                </select>
                            </div>
                        </div>
                        <div class="form-group bg-default">
                            <label for="" class="control-label col-sm-2">Status</label>
                            <div class="col-sm-10">
                                <select name="status" class="form-control">
                                    <option value="true">Active</option>
                                    <option value="false">Deactive</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group bg-default">
                            <label for="" class="control-label col-sm-2">Additional Prices</label>
                            <div class="col-sm-10" id="additionalPrices">
                                <a class="btn btn-social-icon btn-twitter" id="btnAdditionalPrices"><i class="fa fa-plus"></i></a>
                                <br>
                                <b>Note: For Price only for numeric and don't use semicolon, ex: dots, comma, etc.</b>
                                <div class="formAdditionalPrices"></div>
                            </div>
                        </div>

                        <div class="form-group bg-default">
                            <div class="col-sm-10 col-sm-push-2">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/jquery-steps@1.1.0/build/jquery.steps.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/jquery-mask-plugin@1.14.15/dist/jquery.mask.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>   

    <script type="text/javascript">
        {{-- Owners Select2 --}}
        $(function() {
            //Initialize Select2 Elements
            $('.select2').select2()

            $('#inputOwner').select2({
                theme: "bootstrap",
                placeholder: "Owner",
                ajax: {
                    url: '{{ url('admin/users/owner') }}',
                    data: function (params) {
                        return {
                            search: params.term,
                            page: params.page || 1
                        }
                    },
                    processResults: function (data, params) {
                        var page = params.page || 1;
                        if (data.data.length < 1) var total_count = 0;
                        else var total_count = data.data[0].total_count;
                        return {
                            results: $.map(data.data, item => {
                                return {
                                    id: item.id, 
                                    text: item.id+ ' | ' +item.name+ ' | '+item.phone_number
                                }
                            }),
                            pagination: {
                                more: (page * 10) <= total_count
                            }
                        };
                    },
                    cache: true
                }
            });

            // Dropdown listeners
            $("#inputOwner").on('select2:select', e => {
                var selected = e.params.data.id;
                if (selected != '') {
                    $('#inputOwnerId').val(selected);
                    applyUser(selected)
                }
            });

            // Function to fetch single user details
            function applyUser(id) {
                $.ajax({
                    type: 'GET',
                    url: '{{ url('admin/users/owner') }}' + '/' + id + '/rent-counts-active-rooms',
                    success: (data) => {
                        console.log(data.data)
                        var rentCounts = data.data;
                        if (data.data != null) {
                            $.each( rentCounts, function( key, value ) {
                                $("#inputRentCounts").append(GetOption(value, value));
                            });
                        } else {
                            $("#errorMessage").append(data.message)
                        }
                    }
                });
            }

            function GetOption(text, value) {
                return "<option value = '" + value + "'>" + text + "</option>"
            }
        });

        // Add Additional Prices
        $(document).ready(function () {
            $("#btnAdditionalPrices").click(function (e){
                var template = "<div><hr>Description: <input type='text' name='descriptions[]' class='form-control' placeholder='Description' required>"+
                 "Price: <input type='number' name='prices[]' class='form-control' placeholder='price' required>"+
                 "<a href='javascript:void(0);' class='remove'><i class='fa fa-remove'></i></a></div>";
                $(".formAdditionalPrices").append(template); 
            });
            $(document).on("click", "a.remove" , function() {
                $(this).parent().remove();
            });
        });

        $('input[name="prices[]"]').keyup(function(e) {
            if (/\D/g.test(this.value)) {
                // Filter non-digits from input value.
                this.value = this.value.replace(/\D/g, '');
            }
        });
    </script>
@endsection