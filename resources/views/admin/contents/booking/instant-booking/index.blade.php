@extends('admin.layouts.main')

@section('style')
<style>
    /* Sweetalert */

    .swal-shown {
        height: auto !important;
    }

    .custom-swal {
        z-index: 10000 !important;
    }

    .custom-wide-swal {
        width: 640px !important;
        font-size: 13px !important;
    }
</style>
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" />
<link href="//cdn.jsdelivr.net/npm/select2-bootstrap-theme@0.1.0-beta.10/dist/select2-bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="{{ asset('css/booking-form-admin.css') }}">
@endsection

@section('content')
    <div class="row">
        <div class="col-md-8">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Search</h3>
                </div>

                <form action="{{ URL::to('admin/booking/owner/instant-booking') }}" method="GET" enctype="multipart/form-data" class="form-horizontal form-bordered" id="formInsertStyle">
                    <div class="box-body no-padding">
                        <div class="form-group bg-default">
                            <label for="" class="control-label col-sm-2">Owner Phone Number</label>
                            <div class="col-sm-10">
                                <select id="inputPhoneNumber" class="form-control select2-single">
                                    @if(request()->input('phone_number')) <option name="phone_number" value="{{ request()->input('phone_number') }}" selected >{{ request()->input('phone_number') }}</option>@endif
                                </select>
                                @if(request()->input('phone_number'))
                                    <input type="hidden" name="phone_number" value="{{ request()->input('phone_number') }}" class="form-control" id="inputPhone">
                                @else
                                    <input type="hidden" name="phone_number" class="form-control" id="inputPhone">
                                @endif
                                <br>
                                <div id="errorMessage" style="color: red;font-weight: bold;"></div>
                            </div>
                        </div>
                        <div class="form-group bg-default">
                            <label for="" class="control-label col-sm-2">Property Name</label>
                            <div class="col-sm-10">
                                <select id="inputPropertyName" class="form-control select2-single">
                                    @if(request()->input('room_name')) <option name="room_name" value="{{ request()->input('room_name') }}" selected >{{ request()->input('room_name') }}</option>@endif
                                </select>
                                @if(request()->input('room_name'))
                                    <input type="hidden" name="room_name" value="{{ request()->input('room_name') }}" class="form-control" id="inputRoom">
                                @else
                                    <input type="hidden" name="room_name" class="form-control" id="inputRoom">
                                @endif
                                <br>
                                <div id="errorMessage" style="color: red;font-weight: bold;"></div>
                            </div>
                        </div>
                        <div class="form-group bg-default">
                            <label for="" class="control-label col-sm-2">Registered at</label>
                            <div class="col-sm-10">
                                <input type="text" name="created_at" class="form-control input-md datepicker" placeholder="Registered at (ex: 2020-02-02)" value="{{ request()->input('created_at') }}" id="createdAt" autocomplete="off" style="margin-right:2px" />
                            </div>
                        </div>

                        <div class="form-group bg-default">
                            <label for="" class="control-label col-sm-2">Sort By</label>
                            <div class="col-sm-10">
                                <select class="control-label col-sm-4" name="sort_by">
                                    <option value="" >All</option>
                                    <option value="name" @if(request()->input('sort_by') == "name") selected @endif>Name</option>
                                    <option value="phone_number" @if(request()->input('sort_by') == "phone_number") selected @endif>Phone Number</option>
                                    <option value="created_at" @if(request()->input('sort_by') == "created_at") selected @endif>Registered at</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group bg-default">
                            <div class="col-sm-10 col-sm-push-2">
                                <button type="submit" class="btn btn-primary">Search</button>
                                <a href="/admin/booking/owner/instant-booking/#instant-booking" class="btn btn-default">Reset</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">{{ $boxTitle }}</h3>
                    <div class="box-tools pull-right">
                        <a href="{{ url('admin/booking/owner/instant-booking/create') }}" class="btn btn-primary pull-right">Create</a>
                    </div>
                </div>

                <div class="box-body no-padding">
                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <th>Date Registered</th>
                            <th>Owner ID</th>
                            <th>Owner Name</th>
                            <th>Owner Phone Number</th>
                            <th>List of Property</th>
                            <th>Rent Counts</th>
                            <th>Additional Prices</th>
                            <th>Status</th>
                            <th>Status Instant Booking</th>
                            <th>Action</th>
                        </tr>
                        @if(isset($instantBooks))
                            @foreach ($instantBooks as $owner)
                                <tr>
                                    <td>{{ $owner->created_at }}</td>
                                    <td>{{ $owner->owner_id }}</td>
                                    <td>{{ $owner->user->name ?? '' }}</td>
                                    <td>{{ $owner->user->phone_number ?? '' }}</td>
                                    <td>
                                        @if ($owner->user)
                                            @foreach ($owner->user->room_owner_verified as $roomOwner)
                                                @if($roomOwner->room)
                                                    <b>{{ '- '.$roomOwner->room->name }}</b><br>
                                                @endif
                                            @endforeach
                                        @endif
                                    </td>
                                    @if (!is_null($owner->rent_counts))
                                        <td>
                                            @foreach (unserialize($owner->rent_counts) as $rentCount)
                                                - {{ $rentCount }} <br>
                                            @endforeach
                                        </td>
                                    @else
                                        <td>-</td>
                                    @endif

                                    @if (!empty($owner->additional_prices))
                                        @php
                                            $additionalPrices = unserialize($owner->additional_prices);
                                        @endphp
                                        <td>
                                            @foreach ($additionalPrices as $prices)
                                                @if($prices && isset($prices['key']) && isset($prices['value']))
                                                    - {{ $prices['key'] . ' - ' . $prices['value'] }} <br>
                                                @endif
                                            @endforeach
                                        </td>
                                    @else
                                        <td>-</td>
                                    @endif

                                    @if ($owner->is_active)
                                        <td><span class="label label-success">Active</span></td>
                                    @else
                                        <td><span class="label label-danger">Non Active</span></td>
                                    @endif

                                    @if ($owner->is_instant_booking)
                                        <td><span class="label label-success">Active</span></td>
                                    @else
                                        <td><span class="label label-danger">Non Active</span></td>
                                    @endif
                                    <td class="table-action-column" style="padding-right:15px!important;">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                                <i class="fa fa-cogs"></i> Actions
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu pull-right">
                                                <li>
                                                    <a href="{{ url('/admin/booking/owner/instant-booking/edit/'.$owner->id) }}" title="Edit" class="action-recalculate">
                                                        <i class="fa fa-edit"></i> Edit
                                                    </a>
                                                </li>
                                                <li>
                                                    @if (!$owner->is_instant_booking)
                                                        <a href="{{ url('/admin/booking/owner/instant-booking/'.$owner->id.'/status/active') }}" title="Active" class="action-recalculate changeStatus">
                                                            <i class="fa fa-check"></i> Active
                                                        </a>
                                                    @else
                                                        <a href="{{ url('/admin/booking/owner/instant-booking/'.$owner->id.'/status/deactive') }}" title="Active" class="action-recalculate changeStatus">
                                                            <i class="fa fa-refresh"></i> Deactive
                                                        </a>
                                                    @endif
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
                <div class="box-body no-padding">
                    {{ $instantBooks->appends(Request::except('page'))->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/jquery-steps@1.1.0/build/jquery.steps.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/jquery-mask-plugin@1.14.15/dist/jquery.mask.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>

    {{-- Datepicker --}}
    <script type="text/javascript">
        //Date picker
        $('#createdAt').datepicker({
          autoclose: true,
          dateFormat: 'yy-mm-dd'
        })
    </script>

    <script type="text/javascript">
        $(".changeStatus").click(function(e){
            e.preventDefault()
            var url = $(this).attr('href')
            Swal.fire({
                title: 'Are you sure?',
                text: "",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, change it!',
                cancelButtonText: 'No.',
            }).then((result) => {
                if (result.value) {
                    location.href = url;
                    Swal.fire(
                        'Change Status!',
                        'Change status successfully.',
                        'success'
                    )
                }
            });
        });

        $(function() {
            //Initialize Select2 Elements
            $('.select2').select2();

            $('#inputPhoneNumber').select2({
                theme: "bootstrap",
                placeholder: "Phone Number",
                ajax: {
                    url: '{{ url('admin/users/owner') }}',
                    data: function (params) {
                        return {
                            search: params.term,
                            page: params.page || 1
                        }
                    },
                    processResults: function (data, params) {
                        var page = params.page || 1;
                        if (data.data.length < 1) var total_count = 0;
                        else var total_count = data.data[0].total_count;
                        return {
                            results: $.map(data.data, item => {
                                return {
                                    id: item.phone_number, 
                                    text: item.phone_number
                                }
                            }),
                            pagination: {
                                more: (page * 5) <= total_count
                            }
                        };
                    },
                    cache: true
                }
            });

            // Dropdown listeners
            $("#inputPhoneNumber").on('select2:select', e => {
                var selected = e.params.data.id;
                if (selected != '') {
                    $('#inputPhone').val(selected);
                }
            });

            // Property
            $('#inputPropertyName').select2({
                theme: "bootstrap",
                placeholder: "Property Name",
                ajax: {
                    url: '{{ url('admin/room/search/api') }}',
                    data: function (params) {
                        return {
                            search: params.term,
                            page: params.page || 1
                        }
                    },
                    processResults: function (data, params) {
                        var page = params.page || 1;
                        if (data.data.length < 1) var total_count = 0;
                        else var total_count = data.data[0].total_count;
                        return {
                            results: $.map(data.data, item => {
                                return {
                                    id: item.name, 
                                    text: item.name
                                }
                            }),
                            pagination: {
                                more: (page * 5) <= total_count
                            }
                        };
                    },
                    cache: true
                }
            });

            // Dropdown listeners
            $("#inputPropertyName").on('select2:select', e => {
                var selected = e.params.data.id;
                if (selected != '') {
                    $('#inputRoom').val(selected);
                }
            });

            function GetOption(text, value) {
                return "<option value = '" + value + "'>" + text + "</option>"
            }
        });
    </script>
@endsection