@extends('admin.layouts.main')

@section('style')
    {{ HTML::style('assets/vendor/jquery-chosen/chosen.css') }}
    <link rel="stylesheet" href="/assets/vendor/summernote/summernote.css">    
    {{ HTML::style('assets/vendor/jquery-chosen-custom/chosen.css') }}
    <link rel="stylesheet" href="{{ mix_url('dist/vendor/leaflet/leaflet.css') }}">

    <style>
        #chosenForm .chosen-choices {
            border: 1px solid #ccc;
            border-radius: 4px;
            min-height: 34px;
            padding: 6px 12px;
        }
        #chosenForm .form-control-feedback {
            z-index: 100;
        }
        #map-canvas-1,
        #map-canvas-2 {
            height: 300px;
            width: 100%;
        }

        #map-canvas {
            height: 470px;
            width: 100%;
        }

        .marker-label {
            background: #fff;
            font-weight: bold;
            padding: 2px;
        }
    </style>
@endsection

@section('content')
<div class="box box-primary">
  <div class="box-header">
      <h3 class="box-title">{{ $boxTitle }}</h3>
  </div>
    <form class="form-horizontal form-bordered" action="" method="POST">
      <div class="box-body no-padding">

            <div class="form-group bg-default">
              <label for="inputName" class="col-sm-2 control-label">Parent (by title)</label>
              <div class="col-sm-10">
                  <select class="form-control" name="parent_id">
                    @foreach ($landingLists as $key => $value)
                        <option value="{{ $value->id }}" @if($landing->parent_id == $value->id) selected="selected" @endif>
                            {{ $value->option_value->title }}
                        </option>
                    @endforeach
                  </select>
              </div>
            </div>

            <div class="form-group bg-default">
              <label for="inputName" class="col-sm-2 control-label">Title</label>
              <div class="col-sm-10">
                  <input type="text" name="title" value="{{ $filter['title'] }}" class="form-control" required/>
                  <p>Ini adalah title page</p>
              </div>
            </div>

            <div class="form-group bg-default">
              <label for="inputName" class="col-sm-2 control-label">Area</label>
              <div class="col-sm-10">
                  <select class="form-control" name="area">
                    @foreach ($areaType as $value)
                        <option value="{{ $value }}" @if($value == $landing->area) selected="selected" @endif>{{ $value }}</option>
                    @endforeach
                  </select>
              </div>
            </div>

            <div class="form-group bg-default">
              <label for="inputName" class="col-sm-2 control-label">Meta Keyword</label>
              <div class="col-sm-10">
                  <input type="text" name="keyword" value="{{ $landing->keyword }}" class="form-control" />
              </div>
            </div>

            <div class="form-group bg-default">
              <label for="inputName" class="col-sm-2 control-label">Meta Description</label>
              <div class="col-sm-10">
                  <input type="text" name="description" value="{{ $landing->description }}" class="form-control" />
              </div>
            </div>

            <div class="form-group bg-default">
              <label for="inputName" class="col-sm-2 control-label">Slug</label>
              <div class="col-sm-10">
                    <input type="text" placeholder="Slug" value="{{ $landing->slug }}" name="city" class="form-control" />
                    <p>Slug ini ditampilkan dalam opsi filter.</p>
              </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputIndexed" class="col-sm-2 control-label">Indexed</label>
                <div class="col-sm-10">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" checked="checked" name="indexed" value="1">
                            Indexed
                        </label>
                    </div>
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputIndexed" class="col-sm-2 control-label">Price min</label>
                <div class="col-sm-10">
                      <input type="number" class="form-control" @if (isset($filter['filters']['price_range'])) value="{{ $filter['filters']['price_range'][0] }}" @endif name="price_min" required/>
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputIndexed" class="col-sm-2 control-label">Price Max</label>
                <div class="col-sm-10">
                    <input type="number" class="form-control" @if (isset($filter['filters']['price_range'])) value="{{ $filter['filters']['price_range'][1] }}" @endif name="price_max" required/>
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputConcern" class="col-sm-2 control-label">Tags</label>
                <div class="col-sm-10">
                    <select class="form-control chosen-select" multiple id="inputConcern" name="concern_ids[]" tabindex="2">
                        @foreach ($rowsConcern as $rowConcern)
                            <option value="{{ $rowConcern->id }}"
                                    @if (isset($filter['filters']['tag_ids']))
                                        {{ in_array($rowConcern->id, $filter['filters']['tag_ids']) ? 'selected="selected"' : '' }}
                                    @endif
                            >{{ $rowConcern->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputGender" class="col-sm-2 control-label">Gender</label>
                <div class="col-sm-10">
                    <select class="form-control chosen-select" multiple id="inputGender" name="gender[]">
                        @foreach($genderOptions as $key => $gender)
                            <option value="{{ $key }}" 
                                @if (isset($filter['filters']['gender']))
                                    {{ in_array($key, $filter['filters']['gender']) ? 'selected="selected"' : '' }}
                                @endif
                            >{{ $gender }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="rentType" class="col-sm-2 control-label">Rent type</label>
                <div class="col-sm-10">
                    <select class="form-control" id="inputRentType" name="rent_type">
                        @foreach($rentType as $key => $value)
                            <option value="{{ $key }}" 
                                {{-- @if (isset($filter['filters']))
                                    {{ in_array($key, $filter['filters']['rent_type']) ? 'selected="selected"' : '' }}
                                @endif --}}
                                @if ($key === $filter['filters']['rent_type'])
                                    {{'selected="selected"'}}
                                @endif
                            >{{ $value }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputGeoName" class="col-sm-2 control-label">Koordinat</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Geo Name"
                           id="inputGeoName" name="geo_name">
                </div>
                <div class="col-sm-10 col-sm-offset-2">
                    <div style="background: #EEE; width:560px; height:470px;">
                        <div id="map-canvas"></div>
                    </div>
                </div>
                <div class="col-sm-10 col-sm-offset-2">
                    <div class="input-group">
                        @if (isset($filter['location'][0]))
                            <input type="text" class="form-control" placeholder="latitude 1"
                                id="inputLatitude1" name="latitude_1" value="{{ $filter['location'][0][1] }}">
                            <input type="text" class="form-control" placeholder="longitude 1"
                                id="inputLongitude1" name="longitude_1" value="{{ $filter['location'][0][0] }}">
                        @endif
                   </div>

                   <hr>

                   <div class="input-group">
                        @if (isset($filter['location'][1]))
                            <input type="text" class="form-control" placeholder="latitude 2"
                            id="inputLatitude2" name="latitude_2" value="{{ $filter['location'][1][1] }}">
                            <input type="text" class="form-control" placeholder="longitude 2"
                            id="inputLongitude2" name="longitude_2" value="{{ $filter['location'][1][0] }}">
                        @endif
                   </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-2">Template</div>
                <div class="col-sm-10">
                    <textarea name="article" id="configArticle" class="form-control" rows="10">
                    @if (isset($filter['article']))
                        {{ $filter['article'] }}
                    @endif
                    </textarea>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
      </div>
  </form>
</div>
@stop

@section('script')
    {{ HTML::script('assets/vendor/jquery-chosen/chosen.jquery.js') }}
    <script src="/assets/vendor/summernote/summernote.min.js"></script>
    <script src="/assets/vendor/summernote/plugin/summernote-image-attributes.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?libraries=places&language=id&key={{ENV('MIX_GOOGLE_MAPS_JAVASCRIPT_API_KEY')}}"></script>
    <script src="{{ mix_url('dist/vendor/leaflet/leaflet.js') }}"></script>

    <script type="text/javascript">
        function addMarkerListener(map, marker, pos) {
            marker.on('dragend', function(evt) {
                var latlng = evt.target.getLatLng();

                var lat = latlng.lat;
                var lng = latlng.lng;

                $('#inputLatitude' + pos).val(lat);
                $('#inputLongitude' + pos).val(lng);
            });
        }

        function addPlaceChangedListener(autocomplete, map, marker) {
            autocomplete.addListener('place_changed', function() {
                marker[0].setOpacity(0);
                marker[1].setOpacity(0);

                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    // User entered the name of a Place that was not suggested and
                    // pressed the Enter key, or the Place Details request failed.
                    window.alert("No details available for input: '" + place.name + "'");
                    return;
                }

                map.setView({
                    lat: place.geometry.location.lat(),
                    lng: place.geometry.location.lng()
                }, 12);

                bottomLeft = {
                    lat: place.geometry.location.lat() - 0.05,
                    lng: place.geometry.location.lng() - 0.05
                }

                topRight = {
                    lat: place.geometry.location.lat() + 0.05,
                    lng: place.geometry.location.lng() + 0.05
                }

                marker[0].setLatLng(bottomLeft);
                marker[1].setLatLng(topRight);
                marker[0].setOpacity(1);
                marker[1].setOpacity(1);


                $('#inputLatitude1').val(bottomLeft.lat);
                $('#inputLongitude1').val(bottomLeft.lng);
                $('#inputLatitude2').val(topRight.lat);
                $('#inputLongitude2').val(topRight.lng);
            });
        }

        centerPos = {lat: -7.7858485, lng: 110.3680087}

        var map = L.map('map-canvas', {
            // Set latitude and longitude of the map center (required)
            center: centerPos, 
            // Set the initial zoom level, values 0-18, where 0 is most zoomed-out (required)
            zoom: 12
        });

        var tiles = new L.tileLayer("{{ config('services.osm.host') }}/hot/{z}/{x}/{y}.png").addTo(map);

        var marker = [];
        var markerImage = [];
        var position = [];

        // marker kiri bawah
        markerImage[0] = '{{ asset('assets/icons/kiribawah.png') }}';
        
        position[0] = {
            'lat': centerPos.lat - 0.05,
            'lng': centerPos.lng - 0.05
        }

        marker[0] = new L.Marker(position[0], {
            draggable: true,
            icon: new L.icon({
                iconUrl: markerImage[0],
                iconSize: [60, 57],
                iconAnchor: [30, 57]
            })
        }).addTo(map);

        addMarkerListener(map, marker[0], '1');

        // marker kanan atas
        markerImage[1] = '{{ asset('assets/icons/kananatas.png') }}';
        
        position[1] = {
            'lat': centerPos.lat + 0.05,
            'lng': centerPos.lng + 0.05
        }

        marker[1] = new L.Marker(position[1], {
            draggable: true,
            icon: new L.icon({
                iconUrl: markerImage[1],
                iconSize: [60, 57],
                iconAnchor: [30, 57]
            })
        }).addTo(map);

        addMarkerListener(map, marker[1], '2');

        var autocomplete = new google.maps.places.Autocomplete(document.getElementById('inputGeoName'));
        autocomplete.setFields(['address_components', 'geometry']);

        addPlaceChangedListener(autocomplete, map, marker);
    </script>

    <script type="text/javascript">
        var config = {
            '.chosen-select'           : {width: '100%'}
        };
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }

        // summernotinitiate Summernote
        $('#configArticle').summernote({
            height: 300,
                toolbar: [
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['insert', ['table', 'hr']],
                    ['misc', ['codeview']]
                ],
                imageAttributes: {
                    icon: '<i class="note-icon-pencil"/>',
                    removeEmpty: false, // true = remove attributes | false = leave empty if present
                    disableUpload: true // true = don't display Upload Options | Display Upload Options
                }
        });
    </script>
@stop