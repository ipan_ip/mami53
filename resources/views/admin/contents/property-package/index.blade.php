@extends('admin.layouts.main')

@section('style')
	<link rel="stylesheet" href="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.css">
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
	<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />

	<link rel="stylesheet" href="/css/admin-consultant.css">

	<style>
		.remove-property-container {
			width: 100%
		}

		.toolbar-container {
			display: flex;
			justify-content: space-between;
			margin: 10px 0;
			align-items: center;
		}

		.toolbar-container .child {
			flex: 0 0 auto;
			font-size: 0;
		}

		.child button, .child input, .child select {
			vertical-align: middle;
			font-size: 14px;
		}

		.child input {
			margin-left: 16px;
		}

		.child button {
			margin-left: 8px;
		}

		/* .btn-group, .btn-group-vertical {
			position: static;
		}
		*/

		.open > .dropdown-menu {
			left: auto;
			right: 0;
			top: auto;
		}

		a {
			cursor: pointer;
		}

		.search-container {
			display: none;
		}

		.search-container.show-input {
			display: inline-block;
		}

		.the-list {
			display: none;
		}

		.the-list ul {
			padding-left: 16px;
		}

		td:nth-child(10) {
			text-transform: capitalize;
		}

		.right-filter {
			display: flex;
			align-items: center;
		}
		.right-filter .child:not(:last-child) {
			margin-right: 20px;
		}
	</style>
@endsection

@section('content')
	<div class="box box-default">
		<!-- box-header -->
		<div class="box-header" style="padding-top: 10px;">
			<div class="col-md-6">
				<h3 class="box-title" style="padding-left: 0;"><i class="fa fa-handshake-o"></i>&nbsp;Property Package</h3>
			</div>

			<div class="col-md-6">
				<div style="text-align: right;">
					<a class="btn btn-primary" style="margin-top: 5px; color: #fff;" href="/admin/property-package/create"><i class="fa fa-plus"></i>&nbsp;Add New Package</a>
				</div>
			</div>
		</div>
		<!-- /.box-header -->

		<!-- box-body -->
		<div class="box-body no-padding no-overflow">
			<!-- Content -->
      <div class="horizontal-wrapper" style="margin-top: 15px;">
				<div style="padding: 15px;" id="potential-property-package">
					<div id="toolbar" class="toolbar-container">
						<div class="child">
							<select name="filter-type" id="filter-type">
								<option value="" selected>Search By...</option>
								<option value="owner_name">Owner's Name</option>
								<option value="owner_phone">Owner's Phone Number</option>
								<option value="property_name">Property's Name</option>
							</select>
							<div class="search-container">
								<input type="text" id="search-input" placeholder="Search Property">
								<button class="search-button">Search</button>
							</div>
						</div>
						<div class="right-filter">
							<div class="child">
								<select name="" id="contract-status">
									<option value="" selected>All Contract Status</option>
									<option value="inactive">Inactive</option>
									<option value="active">Active</option>
									<option value="terminated">Terminated</option>
								</select>
							</div>
							<div class="child">
								<select name="" id="filter-level">
									<option value="" selected>All Property Level</option>
								</select>
								<button class="search-button">Apply</button>
							</div>
						</div>
					</div>
					<table>
            <thead>
              <tr>
                <th data-field="owner_name">Owner Name</th>
                <th data-field="owner_phone_number">Owner Phone Number</th>
                <th data-field="level">GP Package</th>
								<th data-field="properties" data-formatter="propertyListFormatter">Property List</th>
								<th data-field="total_package">Number of Package</th>
								<th data-field="total_property">Property Count</th>
                <th data-field="total_room_package">Total GP Rooms</th>
								<th data-field="status">Status</th>
                <th data-field="joined_at">Join Date</th>
                <th data-field="updated_at">Created / Updated at</th>
								<th data-field="ended_at">End Date</th>
                <th data-field="id" data-formatter="actionFormatter">Action</th>
              </tr>
            </thead>
          </table>
        </div>
      </div>
		</div>
		<!-- /.box-body -->
	</div>

	<!-- modal add new property -->
	<div class="modal fade" id="modal-add-new-property">
		<div class="modal-flex-container">
			<div class="modal-dialog">
				<div class="modal-content">

					<div class="modal-body" style="text-align: center;">
						<div id="loading-property-list">
							Loading property list...
						</div>
						<div id="property-list" style="display: none;">
							Add New Property:
							<select name="" id="property-list-dropdown">
								<option value="" selected disabled>Choose Property</option>
							</select>
						</div>
					</div>

					<div class="modal-footer">
						<button class="btn" data-dismiss="modal">Cancel</button>
						<button class="btn btn-success" id="go-to-assign" disabled>Save</button>
					</div>

				</div>
			</div>
		</div>
	</div>
	<!-- END modal add new property -->

	<!-- modal property list -->
	<div class="modal fade" id="modal-list-property">
		<div class="modal-flex-container">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
					<div class="modal-body">
						<div id="property-list-container">
							<div class="loading">Getting the property list...</div>
							<div class="the-list">
								<p>Property List:</p>
								<ul></ul>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button class="btn" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END modal property list -->

	<!-- modal remove property -->
	<div class="modal fade" id="modal-remove-property">
		<div class="modal-flex-container">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
					<div class="modal-body">

						<div class="remove-property-loading">
							<div class="loading">Getting the property list...</div>
						</div>
						<div class="remove-property-select2" style="display: none;">
							<select name="" id="remove-property-list">
								<option value=""></option>
							</select>
						</div>

					</div>
					<div class="modal-footer">
						<button class="btn" data-dismiss="modal">Close</button>
						<button class="btn btn-danger button-remove-property" disabled>Remove</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END modal remove property -->
@endsection

@section('script')
	<script src="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
	<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

	<script>
		function actionFormatter(value, row) {
			let action = `
			<div class="btn-group">
				<button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
					<i class="glyphicon glyphicon-cog"></i> Actions <span class="caret"></span>
				</button>
				<ul class="dropdown-menu">
					<li>
						<a href="/admin/property-package/${value}" class="actions"><i class="glyphicon glyphicon-eye-open"></i> See Package Detail</a>
					</li>
			`;

			if (row.ended_at === null) {
				action += `
					<li>
						<a class="actions see-property-list"data-toggle="modal" data-target="#modal-list-property" data-id="${value}"><i class="glyphicon glyphicon-list"></i> See Property List</a>
					</li>
					<li>
						<a class="actions add-new-property" data-toggle="modal" data-target="#modal-add-new-property" data-id="${value}" data-owner-id="${row.owner_id}"><i class="glyphicon glyphicon-plus"></i> Add New Property</a>
					</li>
					<li>
						<a class="actions remove-property" data-id="${value}" data-toggle="modal" data-target="#modal-remove-property"><i class="glyphicon glyphicon-minus"></i> Remove Property</a>
					</li>
					<li>
						<a href="/admin/property-package/update/${value}" class="actions"><i class="glyphicon glyphicon-refresh"></i> Update Contract</a>
					</li>
					<li>
						<a href="" class="actions terminate-contract" data-id="${value}"><i class="glyphicon glyphicon-remove-sign"></i> Terminate Contract</a>
					</li>
					<li>
						<a href="" class="actions delete-contract" data-id="${value}"><i class="glyphicon glyphicon-trash"></i> Delete Contract</a>
					</li>
				`;
			}

			action += `
					</ul>
				</div>
			`;

			return action;
		}

		function propertyListFormatter(value) {
			return value.length ? value.map(val => `- ${val}`).join('<br>') : '-';
		}

		$(function() {
			let $filterType = $('#filter-type');
			let $filterLevel = $('#filter-level');
			let $searchInput = $('#search-input');
			let $filterStatus = $('#contract-status');
			let $table = $('#potential-property-package table');
			let $propertyListDropdown = $('#property-list-dropdown');
			let $propertyListContainer = $('#property-list-container');
			let $removePropertyList = $('#remove-property-list');

			let $searchContainer = $('.search-container');

			let $searchButton = $('.search-button');
			let $goToAssign = $('#go-to-assign');

			let levelValue = '';
			let searchValue = '';
			let searchByValue = '';

			let ownerId = null;
			let propertyContractId = null;
			let propertyId = null;

			$table.bootstrapTable({
				url: `/admin/property-package/index-list?level=${levelValue}&search_by=${searchByValue}&search=${searchValue}`,
				dataField: 'rows',
				showSearchButton: true,
				searchOnEnterKey: true,
				searchAlign: 'left',
				pagination: true,
				pageSize: 15,
				pageList: [],
				sidePagination: 'server'
			});

			// get property level list
			//-----------------------------------------------------------------------------
			axios.get('/admin/property-package/dropdown-list')
			.then(response => {
				responseLevel = response.data.data;

				if (responseLevel.length) {
					$.each(responseLevel, function (i, item) {
						$('#filter-level').append($('<option>', {
							value: item.id,
							text : item.name
						}));
					});
				}
			})
			.catch(error => {
				Swal.fire({
					title: 'Error',
					text: error.response.data.message,
					icon: 'error',
					confirmButtonText: 'Ok'
				});
			});

			//-----------------------------------------------------------------------------

			$searchButton.on('click', function() {
				levelValue = $filterLevel.val() === null ? '' : $filterLevel.val();
				searchValue = $searchInput.val() === null ? '' : $searchInput.val();
				searchByValue = $filterType.val() === null ? '' : $filterType.val();
				filterStatus = $filterStatus.val() || '';

				$table.bootstrapTable('refreshOptions', {
					url: `/admin/property-package/index-list?level=${levelValue}&search_by=${searchByValue}&search=${searchValue}&status=${filterStatus}`
				})
			});

			//-----------------------------------------------------------------------------

			$filterType.val('');

			$filterType.on('input', function() {
				if ($(this).val() !== '') {
					$searchContainer.addClass('show-input');
				} else {
					$searchContainer.removeClass('show-input');
					$searchInput.val('');

					$table.bootstrapTable('refreshOptions', {
						url: `/admin/property-package/index-list?level=${levelValue}&search_by=&search=&status=${filterStatus}`
					})
				}
			});

			//-----------------------------------------------------------------------------

			$('#potential-property-package table').on('click', '.see-property-list', function() {
				propertyContractId = $(this).data('id');

				axios.get(`/admin/property-package/detail/${propertyContractId}`)
				.then(response => {
					const propertyList = response.data.data.properties;
					$.each(propertyList, function (i, item) {
						$propertyListContainer.find('ul').append(`<li>${item.name}</li>`);
					});

					$propertyListContainer.find('.loading').hide();
					$propertyListContainer.find('.the-list').show();
				})
				.catch(error => {
					Swal.fire({
						title: 'Error',
						text: error,
						icon: 'error',
						confirmButton: 'Ok'
					});
				});
			});

			$('#modal-list-property').on('hidden.bs.modal', function() {
				$propertyListContainer.find('.loading').show();
				$propertyListContainer.find('.the-list').hide();

				propertyContractId = '';

				$propertyListContainer.find('ul').html('');
			});


	    // modal remove property
			// -----------------------------------------------------------------------------
			$removePropertyLoading = $('.remove-property-loading');
			$removePropertySelect2 = $('.remove-property-select2');
			$buttonRemoveProperty = $('.button-remove-property');

			$removePropertyList.select2();

			$('#potential-property-package table').on('click', '.remove-property', function() {
				propertyContractId = $(this).data('id');

				axios.get(`/admin/property-package/detail/${propertyContractId}`)
				.then(response => {
					const propertyList = response.data.data.properties.map(obj => {
						return {
							id: obj.id,
							text: obj.name
						}
					});

					$removePropertyLoading.hide();
					$removePropertySelect2.show();

					$removePropertyList.select2({
						placeholder: 'Pilih property',
						allowClear: true,
						width: '100%',
						data: propertyList
					});
				})
				.catch(error => {
					Swal.fire({
						title: 'Error',
						text: error,
						icon: 'error',
						confirmButton: 'Ok'
					});
				});
			});

			// reset when close remove property modal
			$('#modal-remove-property').on('hidden.bs.modal', function() {
				$removePropertyList.empty().append('<option></option>');
				$buttonRemoveProperty.prop('disabled', true);
				$removePropertyLoading.show();
				$removePropertySelect2.hide();

				propertyContractId = null;
			});

			// simple button toggle validation
			$removePropertyList.on('change', function(e) {
				if ($removePropertyList.val()) {
					$buttonRemoveProperty.prop('disabled', false);
				} else {
					$buttonRemoveProperty.prop('disabled', true);
				}
			});

			// removing property
			$buttonRemoveProperty.on('click', function() {
				Swal.fire({
					title: 'Removing property...',
					onBeforeOpen: () => {
						Swal.showLoading();
					},
					allowOutsideClick: false
				});

				$('#modal-remove-property').modal('hide');
				const removePropertyId = $removePropertyList.val();

				axios.put(`/admin/property-package/property/remove/${propertyContractId}`, {
					property_id: removePropertyId
				})
				.then(response => {
					$('#potential-property-package table').bootstrapTable('refresh');

					Swal.fire({
						title: 'Done',
						text: response.data.message,
						icon: 'success',
						showConfirmButton: true
					});
				})
				.catch(error => {
					Swal.fire({
						title: 'Error',
						text: error.response.data.message,
						icon: 'error',
						confirmButtonText: 'Ok'
					});
				});
			});

			//-----------------------------------------------------------------------------

			$('#potential-property-package table').on('click', '.add-new-property', function() {
				ownerId = $(this).data('owner-id');
				propertyContractId = $(this).data('id');

				axios.get(`/admin/property-package/${ownerId}/property-list`)
				.then(response => {
					const propertyList = response.data.data;
					$.each(propertyList, function (i, item) {
						$propertyListDropdown.append($('<option>', {
							value: item.id,
							text : item.name
						}));
					});

					$('#loading-property-list').hide();
					$('#property-list').show();
				})
				.catch(error => {
					Swal.fire({
						title: 'Error',
						text: error.response.data.message,
						icon: 'error',
						confirmButtonText: 'Ok'
					});
				});

			});

			$propertyListDropdown.on('input', function() {
				if ($(this).val() !== '') {
					$goToAssign.prop('disabled', false);
				} else {
					$goToAssign.prop('disabled', true);
				}
			});

			$goToAssign.on('click', function() {
				const propertyValue = $propertyListDropdown.val();
				const propertyContractIdLocal = propertyContractId;

				$('#modal-add-new-property').modal('hide');

				Swal.fire({
					title: 'Adding new property...',
					onBeforeOpen: () => {
						Swal.showLoading();
					},
					allowOutsideClick: false
				});

				axios.post(`/admin/property-package/add-property`, {
					property_contract_id: propertyContractId,
					property_id: propertyValue
				})
				.then(response => {
					Swal.fire({
						title: 'Done',
						text: `New property has been added`,
						icon: 'success',
						showConfirmButton: true
					})
					.then(() => {
						window.location.href = `/admin/property-package/${ownerId}/${propertyValue}/${propertyContractIdLocal}/assign`
					});
				})
				.catch(error => {
					Swal.fire({
						title: 'Error',
						text: error.response.data.message,
						icon: 'error',
						confirmButtonText: 'Ok'
					})
				});

			});

			$('#modal-add-new-property').on('hidden.bs.modal', function() {
				$('#loading-property-list').show();
				$('#property-list').hide();
				$goToAssign.prop('disabled', true);

				propertyContractId = '';

				$propertyListDropdown
					.find('option')
					.remove()
					.end()
					.append('<option value="" selected disabled>Choose Property</option>')
					.val('')
				;
			});

			//-----------------------------------------------------------------------------

			$('#potential-property-package table').on('click', '.terminate-contract', function(e) {
				e.preventDefault();
				const contractId = $(this).data('id');

				Swal.fire({
					text: 'Are you sure you want to terminate this contract?',
					icon: 'warning',
					confirmButtonText: 'Yes',
					cancelButtonColor: '#f4543c',
					cancelButtonText: 'No',
					reverseButtons: true,
					showLoaderOnConfirm: true,
					showCancelButton: true,
					preConfirm: () => {
						return axios.put(`/admin/property-package/terminate/${contractId}`)
						.then(response => {
							$('#potential-property-package table').bootstrapTable('refresh');
							Swal.fire({
								title: 'Done',
								text: `Contract has been terminated`,
								icon: 'success',
								showConfirmButton: true
							});
						})
						.catch(error => {
							Swal.fire({
								title: 'Error',
								text: error.response.data.message,
								icon: 'error',
								confirmButtonText: 'Ok'
							});
						});
					},
					allowOutsideClick: () => !Swal.isLoading()
				});
			});

			$('#potential-property-package table').on('click', '.delete-contract', function(e) {
				e.preventDefault();
				const contractId = $(this).data('id');

				Swal.fire({
					text: 'Are you sure you want to delete this contract?',
					icon: 'warning',
					confirmButtonText: 'Yes',
					cancelButtonColor: '#f4543c',
					cancelButtonText: 'No',
					reverseButtons: true,
					showLoaderOnConfirm: true,
					showCancelButton: true,
					preConfirm: () => {
						return axios.delete(`/admin/property-package/${contractId}`)
						.then(response => {
							$('#potential-property-package table').bootstrapTable('refresh');
							Swal.fire({
								title: 'Done',
								text: `Contract has been deleted`,
								icon: 'success',
								showConfirmButton: true
							});
						})
						.catch(error => {
							Swal.fire({
								title: 'Error',
								text: error.response.data.message,
								icon: 'error',
								confirmButtonText: 'Ok'
							});
						});
					},
					allowOutsideClick: () => !Swal.isLoading()
				});
			});
		});
	</script>
@endsection