@extends('admin.layouts.main')

@section('style')
	<link rel="stylesheet" href="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.css">
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">

	<link rel="stylesheet" href="/css/admin-consultant.css">
@endsection

@section('content')
	<div class="box box-default">
		<!-- box-header -->
		<div class="box-header">
			<div class="col-md-12">
				<h3 class="box-title" style="padding-left: 0;"><i class="fa fa-handshake-o"></i>&nbsp;Package Detail</h3>
			</div>
		</div>
		<!-- /.box-header -->

		<!-- box-body -->
		<div class="box-body no-padding no-overflow">
			<!-- Content -->
      <div class="horizontal-wrapper">
        <div style="padding: 15px;">

					<table class="custom-table">
						<tbody>
							<tr>
								<td>Property Name</td>
								<td>
									<select name="property-name-list" id="property-name-list">
										<option value="" selected disabled>Choose property</option>
									</select>
								</td>
							</tr>
							<tr>
								<td>Owner Phone Number</td>
								<td id="owner-phone-number"></td>
							</tr>
							<tr>
								<td>Property Level</td>
								<td id="property-level"></td>
							</tr>
							<tr>
								<td>Total Package</td>
								<td id="total-package"></td>
							</tr>
						</tbody>
					</table>

					<table class="custom-table" style="margin-top: 16px;">
						<tbody>
							<tr>
								<td>Status</td>
								<td id="status-text"></td>
							</tr>
						</tbody>
					</table>

					<table class="custom-table" style="margin-top: 16px;">
						<tbody>
							<tr>
								<td>Join Date</td>
								<td id="join-date"></td>
							</tr>
							<tr>
								<td>Created At</td>
								<td id="created-at"></td>
							</tr>
							<tr>
								<td>Assigned By</td>
								<td id="assigned-by"></td>
							</tr>
							<tr>
								<td>Updated By</td>
								<td id="updated-by"></td>
							</tr>
							<tr>
								<td>Ended At</td>
								<td id="ended-at"></td>
							</tr>
							<tr>
								<td>Ended By</td>
								<td id="ended-by"></td>
							</tr>
						</tbody>
					</table>

					<div>
						<hr>
						<a class="btn btn-default" href="/admin/property-package/"><i class="fa fa-arrow-left"></i>&nbsp;Back to property package list</a>
					</div>

        </div>
      </div>
		</div>
		<!-- /.box-body -->
	</div>

	<div class="box box-default">
		<!-- box-header -->
		<div class="box-header" style="padding-top: 10px;">
			<div class="col-md-12">
				<h3 class="box-title" style="padding-left: 0;"><i class="fa fa-home"></i>&nbsp;Kos List</h3>
			</div>
		</div>
		<!-- /.box-header -->

		<!-- box-body -->
		<div class="box-body no-padding no-overflow">
			<!-- Content -->
      <div class="horizontal-wrapper">
        <div style="padding: 15px;" id="property-package-detail">

					<table>
						<thead>
							<tr>
								<th data-field="kost_name">Kos Name</th>
								<th data-field="owner_name">Owner Name</th>
								<th data-field="area_city">Area</th>
								<th data-field="total_rooms">Total Rooms</th>
								<th data-field="level">Kos Level</th>
								<th data-field="updated_at">Created / Updated By</th>
								<th data-field="id" data-formatter="actionFormatter">Action</th>
							</tr>
						</thead>
					</table>

        </div>
      </div>
		</div>
		<!-- /.box-body -->
	</div>
@endsection

@section('script')
	<script src="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.27.0/moment.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

	<script>
		function actionFormatter(value, row) {
			let action = '';

			if (row.is_assignable) {
				if (row.has_been_assigned) {
					action = `
						<a class="btn btn-default unassign-all-rooms" title="Unassign all rooms" data-id="${value}" data-name="${row.kost_name}"><i class="glyphicon glyphicon-minus"></i>&nbsp;Unassign</a>
					`;
				} else {
					action = `
						<a class="btn btn-default assign-all-rooms" title="Assign all rooms" data-id="${value}" data-name="${row.kost_name}"><i class="glyphicon glyphicon-ok"></i>&nbsp;Assign</a>
					`;
				}
			}

			return action;
		}

		$(function() {
			// VARZ
			// ------------------------------------------------------
			const $propertyNameList = $('#property-name-list');
			const $ownerPhoneNumber = $('#owner-phone-number');
			const $propertyLevel = $('#property-level');
			const $totalPackage = $('#total-package');
			const $statusText = $('#status-text');

			const $joinDate = $('#join-date');
			const $createdAt = $('#created-at');
			const $assignedBy = $('#assigned-by');
			const $updatedBy = $('#updated-by');
			const $endedAt = $('#ended-at');
			const $endedBy = $('#ended-by');
			// ------------------------------------------------------

			$propertyNameList.val('');

			$('#property-package-detail table').bootstrapTable({
				dataField: 'data'
			});

			axios.get('{{ "/admin/property-package/detail/$propertyContractId" }}')
			.then(response => {
				const result = response.data.data;

				$ownerPhoneNumber.text(result.owner_phone_number);
				$propertyLevel.text(result.package_name);
				$totalPackage.text(result.total_package);
				$statusText.text(result.status);

				$joinDate.text(moment(result.join_date).format('DD-MM-YYYY'));
				$createdAt.text(moment(result.created_at.date).format('DD-MM-YYYY HH:mm'));
				$assignedBy.text(result.assigned_by);
				$updatedBy.text(result.updated_by);
				$endedAt.text(result.ended_at === null ? '-' : moment(result.ended_at).format('DD-MM-YYYY HH:mm'));
				$endedBy.text(result.ended_by);

				if (result.properties.length) {
					$.each(result.properties, function (i, item) {
						$propertyNameList.append($('<option>', {
							value: item.id,
							text : item.name
						}));
					});
					// set default selection with first option
					$propertyNameList.val(result.properties[0].id).change();
				}
			})
			.catch(error => {
				Swal.fire({
					title: 'Error',
					text: error,
					icon: 'error',
					confirmButton: 'Ok'
				});
			});

			$('#property-name-list').on('change', function() {
				const id = $(this).val();
				$('#property-package-detail table').bootstrapTable('refreshOptions', {
					url: `/admin/property-package/kost-list/property/${id}/{{$propertyContractId}}`
				})
			});

			$('#property-package-detail').on('click', '.assign-all-rooms', function() {
				const id = $(this).data('id');
				const propertyName = $(this).data('name');

				Swal.fire({
					text: 'Are you sure you want to assign all rooms?',
					icon: 'warning',
					confirmButtonText: 'Yes',
					cancelButtonColor: '#f4543c',
					cancelButtonText: 'No',
					reverseButtons: true,
					showLoaderOnConfirm: true,
					showCancelButton: true,
					preConfirm: () => {
						return axios.post(`/admin/property-package/${id}/{{$propertyContractId}}/assign-all-rooms`)
						.then(response => {
							Swal.fire({
								title: 'Done',
								text: `All rooms on ${propertyName} are assigned`,
								icon: 'success',
								showConfirmButton: false,
								allowOutsideClick: false
							});

							location.reload();
						})
						.catch(error => {
							Swal.fire({
								title: 'Error',
								text: error.response.data.message,
								icon: 'error',
								confirmButtonText: 'Ok'
							});
						});
					},
					allowOutsideClick: () => !Swal.isLoading()
				})
			});

			$('#property-package-detail').on('click', '.unassign-all-rooms', function() {
				const id = $(this).data('id');
				const propertyName = $(this).data('name');

				Swal.fire({
					text: 'Are you sure you want to unassign all rooms?',
					icon: 'warning',
					confirmButtonText: 'Yes',
					cancelButtonColor: '#f4543c',
					cancelButtonText: 'No',
					reverseButtons: true,
					showLoaderOnConfirm: true,
					showCancelButton: true,
					preConfirm: () => {
						return axios.post(`/admin/property-package/${id}/{{$propertyContractId}}/unassign-all-rooms`)
						.then(response => {
							Swal.fire({
								title: 'Done',
								text: `All rooms on ${propertyName} are unassigned`,
								icon: 'success',
								showConfirmButton: false,
								allowOutsideClick: false
							});

							location.reload();
						})
						.catch(error => {
							Swal.fire({
								title: 'Error',
								text: error.response.data.message,
								icon: 'error',
								confirmButtonText: 'Ok'
							});
						});
					},
					allowOutsideClick: () => !Swal.isLoading()
				})
			});

		});
	</script>
@endsection