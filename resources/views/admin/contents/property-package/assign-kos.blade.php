@extends('admin.layouts.main')

@section('style')
	<link rel="stylesheet" href="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.css">
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">

	<link rel="stylesheet" href="/css/admin-consultant.css">
@endsection

@section('content')
	<div class="box box-default">
		<!-- box-header -->
		<div class="box-header" style="padding-top: 10px;">
			<div class="col-md-12">
				<h3 class="box-title" style="padding-left: 0;"><i class="fa fa-handshake-o"></i>&nbsp;Assign Kos</h3>
			</div>
		</div>
		<!-- /.box-header -->

		<!-- box-body -->
		<div class="box-body no-padding no-overflow">
			<!-- Content -->
      <div class="horizontal-wrapper" style="margin-top: 15px;">
        <div style="padding: 15px;" id="assign-kos-list">

					<table>
						<thead>
							<tr>
								<th data-field="name">Kost Name</th>
								<th data-field="owner_name">Owner Name</th>
								<th data-field="area_city">Area</th>
								<th data-field="room_count">Total Rooms</th>
								<th data-field="updated_at">Created / Updated At</th>
								<th data-field="id" data-formatter="actionFormatter">Action</th>
							</tr>
						</thead>
					</table>

					<div class="row">
						<div class="col-sm-12">
							<hr>
							<div style="text-align: right;">
								<a href="/admin/property-package/" class="btn btn-default">Cancel</a>
								<a href="{{ "/admin/property-package/$propertyContractId" }}" type="submit" class="btn btn-success" style="color: #fff; margin-left: 8px;"><i class="fa fa-save"></i>&nbsp;Save</a>
							</div>
						</div>
					</div>

        </div>
      </div>
		</div>
		<!-- /.box-body -->
	</div>
@endsection

@section('script')
	<script src="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

	<script>
		function actionFormatter(value, row) {
			let action = ''

			if (row.property_id === null) {
				action = `
					<a class="btn btn-primary assign-unassign" style="margin-top: 5px; color: #fff;" data-property-id="{{ $propertyId }}" data-kos-id="${value}"><i class="fa fa-check-square"></i>&nbsp;Assign</a>
				`;
			} else {
				action = `
					<a class="btn btn-primary assign-unassign" style="margin-top: 5px; color: #fff;" data-property-id="{{ $propertyId }}" data-kos-id="${value}"><i class="fa fa-minus-square"></i>&nbsp;Unassign</a>
				`;
			}

			return action;
		}

		$(function() {
			$('#assign-kos-list table').bootstrapTable({
				dataField: 'rows',
				url: '{{ url("/admin/property-package/kost-list/owner/{$ownerId}/property/{$propertyId}") }}'
			});

			$('#assign-kos-list').on('click', '.assign-unassign', function() {
				const propertyId = $(this).data('property-id');
				const kosId = $(this).data('kos-id');

				axios.post('/admin/property-package/toggle-assign', {
					property_id: propertyId,
					kost_id: kosId
				})
				.then(response => {
					$('#assign-kos-list table').bootstrapTable('refresh');
				})
				.catch(error => {
					Swal.fire({
						title: 'Error',
						text: error.response.data.message,
						icon: 'error',
						confirmButton: 'Ok'
					});
				})
			});
		});
	</script>
@endsection