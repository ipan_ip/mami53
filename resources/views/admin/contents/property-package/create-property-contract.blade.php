@extends('admin.layouts.main')

@section('style')
	<link rel="stylesheet" href="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.css">
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/eonasdan-bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css">

	<link rel="stylesheet" href="/css/admin-consultant.css">

	<style>
		.after-search {
			display: none;
			padding-top: 20px;
		}

		#add-new-property {
			margin-top: 10px;
			display: none;
		}
	</style>
@endsection

@section('content')
	<div class="box box-default">
		<!-- box-header -->
		<div class="box-header" style="padding-top: 10px;">
			<div class="col-md-12">
				<h3 class="box-title" style="padding-left: 0;"><i class="fa fa-handshake-o"></i>&nbsp;Property Contract</h3>
			</div>
		</div>
		<!-- /.box-header -->

		<!-- box-body -->
		<div class="box-body no-padding no-overflow">
			<!-- Content -->
      <div class="horizontal-wrapper" style="margin-top: 15px;">
        <div style="padding: 15px;" id="potential-owner-list">

					<form id="submit-create">
						<div class="row">
							<div class="col-sm-6">

								<div class="one-input">
									<p>Owner Phone Number</p>
									<input type="text" id="owner-phone" style="width: calc(100% - 95px); margin-top: 2px;" value="{{ $phone_number }}">
									<button id="search-owner-phone-button" type="button" class="btn btn-success pull-right" style="color: #fff;"><i class="fa fa-search"></i>&nbsp;Search</button>
								</div>

								<div class="after-search">
									<div class="one-input">
										<p>Property Name</p>
										<select name="property-name" id="property-name">
											<option value="" disabled selected>Pilih property</option>
										</select>
										<div id="add-new-property">
											<a type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-add-new-property"><i class="fa fa-plus"></i>&nbsp;Add Property</a>
										</div>
									</div>

									<div class="one-input">
										<p>Owner Name</p>
										<input type="text" id="owner-name" disabled>
									</div>

									<div class="one-input">
										<p>Property Level</p>
										<select name="property-level" id="property-level">
											<option value="" disabled selected>Pilih property level</option>
										</select>
									</div>

									<div class="one-input">
										<p>Join Date</p>
										<input type="text" id="join-date">
									</div>

									<div class="one-input">
										<p>Custom Package</p>
										<input type="checkbox" id="custom-package">
									</div>

									<div class="one-input" id="total-package-container" style="display: none;">
										<p>Total Package</p>
										<input type="text" id="total-package">
									</div>
								</div>

							</div>
						</div>

						<div class="row">
							<div class="col-sm-6">
								<hr>
								<a href="/admin/property-package" class="btn btn-default"><i class="fa fa-arrow-left"></i>&nbsp;back to property package</a>
								<button type="submit" id="submit-form" class="btn btn-success pull-right" style="color: #fff;" disabled>Next</button>
							</div>
						</div>
					</form>

        </div>
      </div>
		</div>
		<!-- /.box-body -->
	</div>

	<!-- modal property list -->
	<div class="modal fade" id="modal-add-new-property">
		<div class="modal-flex-container">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
					<form id="submit-new-property">
						<div class="modal-header">
							<h3>Create New Property</h3>
						</div>
						<div class="modal-body">
							<input type="text" id="input-new-property" placeholder="Add new property name" style="width: 100%;">
						</div>
						<div class="modal-footer">
							<button class="btn" data-dismiss="modal">Cancel</button>
							<button type="submit" id="button-add-new-property" class="btn btn-success" disabled>Add</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- END modal property list -->
@endsection

@section('script')
	<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
	<script src="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.27.0/moment.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/eonasdan-bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>

	<script>
		$('#owner-phone, #total-package').on('input blur paste', function(){
			$(this).val($(this).val().replace(/\D/g, ''));
		});

		$(function() {
			// VARZ
			// ------------------------------------------------------
			let $searchPhone = $('#search-owner-phone-button');
			let $afterSearch = $('.after-search');
			let $ownerPhone = $('#owner-phone');
			let $propertyName = $('#property-name');
			let $ownerName = $('#owner-name');
			let $propertyLevel = $('#property-level');
			let $joinDate = $('#join-date');
			let $customPackage = $('#custom-package');
			let $totalPackageContainer = $('#total-package-container');
			let $totalPackage = $('#total-package');
			let $submitForm = $('#submit-form');
			let $inputNewProperty = $('#input-new-property');

			let responseData;
			let responseLevel;
			// ------------------------------------------------------

			$submitForm.prop('disabled', true);
			$customPackage.iCheck('uncheck');

			// ------------------------------------------------------

			$('#join-date').datetimepicker({
				format: 'DD-MM-YYYY HH:mm'
			});

			$ownerPhone.on('input', function() {
				$afterSearch.hide();
				$submitForm.prop('disabled', true);
			});

			$customPackage.on('ifChanged', function(e) {
				if (e.target.checked) {
					$totalPackageContainer.show();
				} else {
					$totalPackageContainer.hide();
				}
			});

			// find phone
			$searchPhone.on('click', function(e) {
				e.preventDefault();

				$searchPhone.prop('disabled', true);

				// reset form
				// ------------------------------------------------------

				$propertyName
					.find('option')
					.remove()
					.end()
					.append('<option value="" disabled selected>Pilih property</option>')
					.val('')
				;

				$ownerName.val('');

				$propertyLevel
					.find('option')
					.remove()
					.end()
					.append('<option value="" disabled selected>Pilih property level</option>')
					.val('')
				;

				$joinDate.val('');

				$customPackage.prop('checked', false);

				$totalPackage.val(0);

				// ------------------------------------------------------
				// get owner data
				const ownerPhone = $ownerPhone.val();

				const getSearchOwner = axios.get(`/admin/property-package/search-owner/${ownerPhone}`)
				.then(response => {
					responseData = response.data.data;

					$ownerName.val(responseData.owner_name);

					if (responseData.properties.length) {
						$('#add-new-property').hide();

						$.each(responseData.properties, function (i, item) {
							$propertyName.append($('<option>', {
								value: item.id,
								text : item.name
							}));
						});
					} else {
						$('#add-new-property').show();
					}

				});


				const getPropertyLevel = axios.get(`/admin/property-package/dropdown-list`)
				.then(response => {
					responseLevel = response.data.data;

					if (responseLevel.length) {
						$.each(responseLevel, function (i, item) {
							$propertyLevel.append($('<option>', {
								value: item.id,
								text : item.name
							}));
						});
					}
				});


				Promise.all([getSearchOwner, getPropertyLevel])
				.then(() => {
					$afterSearch.show();
					$submitForm.prop('disabled', false);
				})
				.catch(error => {
					Swal.fire({
						title: 'Error',
						text: error.response.data.message,
						icon: 'error',
						confirmButton: 'Ok'
					});
				})
				.finally(() => {
					$searchPhone.prop('disabled', false);
				});
			});

			// ------------------------------------------------------
			// submit form create property package
			$('#submit-create').submit(function(e) {
				e.preventDefault();

				const formattedDate = moment($joinDate.val(), 'DD-MM-YYYY HH:mm').format('YYYY-MM-DD HH:mm[:00]');
				let totalPackage;

				if ($customPackage.is(':checked')) {
					totalPackage = $totalPackage.val() === '' ? 0 : $totalPackage.val()
				} else {
					totalPackage = 0;
				}

				axios.post('/admin/property-package/', {
					property_id: $propertyName.val(),
					property_level_id: $propertyLevel.val(),
					join_date: formattedDate,
					custom_package: $customPackage.is(':checked'),
					total_package: totalPackage
				})
				.then(response => {
					Swal.fire({
						title: 'Success',
						html: 'You will be redirected to assign kos page',
						icon: 'success',
						timer: 1000,
  					timerProgressBar: true,
						showConfirmButton: false,
						allowOutsideClick: false
					})
					.then(result => {
						window.location.href = `/admin/property-package/${response.data.data.properties[0].owner_user_id}/${response.data.data.properties[0].id}/${response.data.data.properties[0].pivot.property_contract_id}/assign`;
					});

				})
				.catch(error => {
					$searchPhone.prop('disabled', false);
					let errorMessage = '';
					$.each(error.response.data.messages, function(index, value) {
						errorMessage += value[0] + '<br>';
					});

					Swal.fire({
						title: 'Error',
						html: errorMessage,
						icon: 'error',
						confirmButton: 'Ok'
					});
				})

				return false;
			});

			// ------------------------------------------------------
			// add new new property events
			$inputNewProperty.on('input', function() {
				$('#button-add-new-property').prop('disabled', $(this).val().length ? false : true);
			});

			$('#submit-new-property').submit(function() {
				const propertyName = $inputNewProperty.val();
				const ownerPhoneNumber = $ownerPhone.val();

				Swal.fire({
					title: 'Adding new property...',
					onBeforeOpen: () => {
						Swal.showLoading();
					},
					allowOutsideClick: false
				});

				axios.post('/admin/property-package/property/new', {
					name: propertyName,
					owner_phone_number: ownerPhoneNumber
				})
				.then(response => {
					$('#modal-add-new-property').modal('hide');
					$('#add-new-property').hide();


					axios.get(`/admin/property-package/search-owner/${ownerPhoneNumber}`)
					.then(response => {
						const responseRefresh = response.data.data;

						$.each(responseRefresh.properties, function (i, item) {
							$propertyName.append($('<option>', {
								value: item.id,
								text : item.name
							}));
						});

						Swal.fire({
							title: 'Done',
							text: `New property has been added`,
							icon: 'success',
							showConfirmButton: true
						});
					})
					.catch(error => {
						console.log(error);
						Swal.fire({
							title: 'Error',
							text: error.response.data.message,
							icon: 'error',
							confirmButtonText: 'Ok'
						});
					});

				})
				.catch(error => {
					Swal.fire({
						title: 'Error',
						text: error.response.data.message,
						icon: 'error',
						confirmButtonText: 'Ok'
					});
				});

				return false;
			})
		});
		</script>
@endsection