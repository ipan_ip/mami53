@extends('admin.layouts.main')

@section('style')
	<link rel="stylesheet" href="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.css">
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/eonasdan-bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css">

	<link rel="stylesheet" href="/css/admin-consultant.css">

	<style>
		.checkbox-upgrade-gp2 {
			margin: 0;
			padding-top: 4px;
			font-weight: normal;
			font-size: 12px;
			color: rgb(158, 89, 89);
			display: none;
		}

		.checkbox-upgrade-gp2 input {
			margin-right: 4px;
			width: auto;
		}
	</style>
@endsection

@section('content')
	<div class="box box-default">
		<!-- box-header -->
		<div class="box-header" style="padding-top: 10px;">
			<div class="col-md-12">
				<h3 class="box-title" style="padding-left: 0;"><i class="fa fa-handshake-o"></i>&nbsp;Update Property Contract</h3>
			</div>
		</div>
		<!-- /.box-header -->

		<!-- box-body -->
		<div class="box-body no-padding no-overflow">
			<!-- Content -->
      <div class="horizontal-wrapper" style="margin-top: 15px;">
				<div style="padding: 15px;" id="potential-owner-list">

					<h3 id="loading-data" style="margin-top: 0;">Loading data...</h3>

					<form id="submit-update" style="display: none;">
						<div class="row">
							<div class="col-sm-6">

								<div class="one-input">
									<p>Owner Phone Number</p>
									<input type="text" id="owner-phone" style="margin-top: 2px;" disabled>
								</div>

								<div class="one-input">
									<p>Owner Name</p>
									<input type="text" id="owner-name" disabled>
								</div>

								<div class="one-input">
									<p>Property Level</p>
									<input type="text" name="property-level" id="property-level" disabled>
									<label for="upgrade-gp2-promo" class="checkbox-upgrade-gp2"><input type="checkbox" id="upgrade-gp2-promo"> Upgrade to Mamikos Goldplus 2 PROMO</label>
								</div>

								<div class="one-input">
									<p>Status</p>
									<input type="text" name="property-status" id="property-status" disabled>
								</div>

								<div class="one-input">
									<p>Join Date</p>
									<input type="text" id="join-date">
								</div>

								<div class="one-input">
									<p>Custom Package</p>
									<input type="checkbox" id="custom-package">
								</div>

								<div class="one-input" id="total-package-container" style="display: none;">
									<p>Total Package</p>
									<input type="text" id="total-package">
								</div>

							</div>
						</div>

						<div class="row">
							<div class="col-sm-6">
								<hr>
								<a href="/admin/property-package" class="btn btn-default"><i class="fa fa-arrow-left"></i>&nbsp;back to property package</a>
								<button type="submit" id="submit-form" class="btn btn-success pull-right" style="color: #fff;">Save</button>
							</div>
						</div>
					</form>

        </div>
      </div>
		</div>
		<!-- /.box-body -->
	</div>
@endsection

@section('script')
	<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
	<script src="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.27.0/moment.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/eonasdan-bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>

	<script>$('#owner-phone, #total-package').on('input blur paste', function(){
			$(this).val($(this).val().replace(/\D/g, ''));
		});

		$(function() {
			// VARZ
			// ------------------------------------------------------
			let $ownerPhone = $('#owner-phone');
			let $ownerName = $('#owner-name');
			let $propertyLevel = $('#property-level');
			let $propertyStatus = $('#property-status');
			let $joinDate = $('#join-date');
			let $customPackage = $('#custom-package');
			let $totalPackageContainer = $('#total-package-container');
			let $totalPackage = $('#total-package');
			let $submitForm = $('#submit-form');

			const $upgradeGp2Promo = $('#upgrade-gp2-promo');

			let responseData;
			let responseLevel;
			// ------------------------------------------------------

			$customPackage.iCheck('uncheck');
			$upgradeGp2Promo.iCheck('destroy');

			// ------------------------------------------------------
			// get existing data
			axios.get(`/admin/property-package/detail/{{ $propertyContractId }}`)
			.then(response => {
				responseData = response.data.data;
				$ownerName.val(responseData.owner_name);
				$ownerPhone.val(responseData.owner_phone_number);
				$propertyLevel.val(responseData.package_name);
				$propertyStatus.val(responseData.status);
				$joinDate.val(moment(responseData.join_date).format('DD-MM-YYYY HH:mm'));

				if (responseData.custom_package) {
					$customPackage.iCheck('check');
					$totalPackage.val(responseData.total_package);
				}

				const checkString = 'goldplus 1 promo';
				const propertyLevelString = responseData.package_name.toLowerCase();

				if (propertyLevelString.includes(checkString)) {
					$('.checkbox-upgrade-gp2').css('display', 'block');
				}

				$('#loading-data').hide();
				$('#submit-update').show();
			})
			.catch(error => {
				Swal.fire({
					title: 'Error',
					text: error.response.data.message,
					icon: 'error',
					confirmButton: 'Ok'
				});
			});

			// ------------------------------------------------------

			$('#join-date').datetimepicker({
				format: 'DD-MM-YYYY HH:mm'
			});

			// hide/show total package input
			$customPackage.on('ifChanged', function(e) {
				if (e.target.checked) {
					$totalPackageContainer.show();
				} else {
					$totalPackageContainer.hide();
				}
			});

			// update
			$('#submit-update').submit(function(e) {
				e.preventDefault();

				Swal.fire({
					title: 'Updating property contract...',
					onBeforeOpen: () => {
						Swal.showLoading();
					},
					allowOutsideClick: false
				});

				const formattedDate = moment($joinDate.val(), 'DD-MM-YYYY HH:mm').format('YYYY-MM-DD HH:mm[:00]');
				let totalPackage;

				if ($customPackage.is(':checked')) {
					totalPackage = $totalPackage.val() === '' ? 0 : $totalPackage.val()
				} else {
					totalPackage = 0;
				}

				axios.put('/admin/property-package/{{ $propertyContractId }}', {
					join_date: formattedDate,
					custom_package: $customPackage.is(':checked'),
					total_package: totalPackage,
					upgrade_to_gp2: $upgradeGp2Promo.is(':checked')
				})
				.then(response => {
					Swal.fire({
						title: 'Success',
						html: 'You will be redirected to property package index page',
						icon: 'success',
						timer: 1000,
  					timerProgressBar: true,
						showConfirmButton: false,
						allowOutsideClick: false
					})
					.then(result => {
						window.location.href = `/admin/property-package`;
					});

				})
				.catch(error => {
					let errorMessage = '';
					$.each(error.response.data.messages, function(index, value) {
							errorMessage += value[0] + '<br>';
					});

					Swal.fire({
						title: 'Error',
						html: errorMessage,
						icon: 'error',
						confirmButton: 'Ok'
					});
				})

				return false;
			});
		});
	</script>
@endsection