@extends('admin.layouts.main')

@section('content')
    <!-- table -->
    <div class="box box-primary">
        <!-- box-header -->
        <div class="box-header">
            <h3 class="box-title">Testers</h3>
        </div>
        <!-- box-header -->
        <!-- box-body -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <form action="" class="form-inline" method="get" style="text-align:right;padding:10px;">
                        <input type="text" name="id" class="form-control input-sm" placeholder="User ID" autocomplete="off" value="{{ request()->input('id') }}">
                        <input type="text" name="name" class="form-control input-sm" placeholder="Name" autocomplete="off" value="{{ request()->input('name') }}">
                        <input type="text" name="phone_number" class="form-control input-sm" placeholder="Phone Number" autocomplete="off" value="{{ request()->input('phone_owner') }}">
                        <input type="email" name="email" class="form-control input-sm" placeholder="Email" autocomplete="off" value="{{ request()->input('email') }}" style="margin-right:10px">
                        <button class="btn btn-primary btn-md" id="buttonSearch"><i class="fa fa-search">&nbsp;</i>Search
                        </button>
                    </form>
                </div>
            </div>

            <table class="table table-striped">
                <thead>
                    <tr>
                        <th class="text-center">No</th>
                        <th class="text-center">User ID</th>
                        <th class="text-center">Name</th>
                        <th class="text-center">Phone Number</th>
                        <th class="text-center">Email</th>
                        <th class="text-center">Created At</th>
                        <th class="text-center">Job & Workplace</th>
                        <th class="text-center">Birthday Date</th>
                        <th class="text-center">Gender</th>
                        <th class="text-center">Is Owner</th>
                        <th class="text-center">Is Verify Phone</th>
                        <th class="text-center">Is Verify Email</th>
                        <th class="text-center">Is Verify KTP</th>
                        <th class="text-center">Device ID</th>
                        <th class="text-center">Is Tester</th>
                        <th class="text-center">Register With</th>
                        <th class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($userOwners as $index => $userOwner)
                        <tr>
                            <td class="text-center">{{ $index+1 }}</td>
                            <td class="text-center">{{ $userOwner->id }}</td>
                            <td class="text-center">{{ $userOwner->name }}</td>
                            <td class="text-center">{{ $userOwner->phone_number }}</td>
                            <td class="text-center">{{ $userOwner->email }}</td>
                            <td class="text-center">{{ $userOwner->created_at }}</td>
                            <td class="text-center">{{ $userOwner->workplace ?? '-' }}</td>
                            <td class="text-center">{{ $userOwner->birthday }}</td>
                            <td class="text-center">{{ $userOwner->gender ?? '-' }}</td>
                            <td class="text-center">{{ $userOwner->is_owner }}</td>
                            <td class="text-center">{{ $userOwner->user_verification_account->is_verify_phone_number ?? '-' }}</td>
                            <td class="text-center">{{ $userOwner->user_verification_account->is_verify_email ?? '-' }}</td>
                            <td class="text-center">{{ $userOwner->user_verification_account->identity_card ?? '-' }}</td>
                            <td class="text-center">{{ $userOwner->last_login->uuid ?? '-' }}</td>
                            <td class="text-center">{{ $userOwner->is_tester ? '✔️ true' : '❌ false' }}</td>
                            <td class="text-center">{{ $userOwner->registerWith() }}</td>
                            <td class="table-action-column">
                            @if ($userOwner->is_tester)
                                <form action="{{ url('admin/testing/testers/' . $userOwner->id . '/recycle-phone') }}" method="post" onsubmit="return confirm('Are you sure you want to recycle phone number {{ $userOwner->phone_number }} from user ID {{ $userOwner->id }} {{ $userOwner->name }}?');">
                                    @csrf
                                    <input type="submit" class="btn btn-xs btn-danger" role="button" value="Recycle Phone">
                                </form>
                                <form action="{{ url('admin/testing/testers/' . $userOwner->id . '/mark-as-tester') }}" method="post" onsubmit="return confirm('Are you sure you want to unmark as tester for user ID {{ $userOwner->id }} {{ $userOwner->name }}?');">
                                    @csrf
                                    <input type="hidden" name="flag" value=0>
                                    <input type="submit" class="btn btn-xs btn-primary" role="button" value="Unmark As Tester">
                                </form>
                            @else
                                <button class="btn btn-xs btn-secondary" role="button" title="Only for tester" onclick="return alert('Only for tester user')">Recycle Phone</button>
                                <form action="{{ url('admin/testing/testers/' . $userOwner->id . '/mark-as-tester') }}" method="post" onsubmit="return confirm('Are you sure you want to mark as tester for user ID {{ $userOwner->id }} {{ $userOwner->name }}?');">
                                    @csrf
                                    <input type="hidden" name="flag" value=1>
                                    <input type="submit" class="btn btn-xs btn-warning" role="button" value="Mark As Tester">
                                </form>
                                
                            @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

        </div><!-- /.box-body -->
        <div class="box-body no-padding">
            {{ $userOwners->appends(Request::except('page'))->links() }}
        </div>
    </div><!-- /.box -->
    <!-- table -->
@endsection