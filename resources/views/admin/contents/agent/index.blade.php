@extends('admin.layouts.main')

@section('style')
<style>
    span.apartment-color,
    tr.apartment-color td {
        color: #2A00FF;
    }
    tr.kost-active td {
        background-color: #DFF9FF;
    }
</style>
@endsection

@section('content')
    <!-- table -->
    <div class="box box-primary">

        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
            <div class="box-tools pull-right">
                <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
            </div>
        </div><!-- /.box-header -->

        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <form action="" class="form-inline" method="get" style="text-align:right;padding:10px;">

                        @if(Auth::user())
                            {{ Form::select('agent_from', $agent_from, request()->input('agent_from'), array('class' => 'pull-right-sort form-group btn btn-primary btn-sm form-control')) }}
                            <input type="text" name="agent" class="form-control input-sm" placeholder="Agent name / Code"  autocomplete="off" value="{{ request()->input('agent') }}">
                            <input type="text" name="owner_phone" class="form-control input-sm" placeholder="Owner phone"  autocomplete="off" value="{{ request()->input('owner_phone') }}">
                            <input type="text" name="from" class="form-control input-sm datepicker" placeholder="Start Date" value="{{ request()->input('from') }}" id="start-date" autocomplete="off" style="margin-right:2px" />
                            <input type="text" name="to" class="form-control input-sm datepicker" placeholder="End Date" value="{{ request()->input('to') }}" id="end-date" autocomplete="off" style="margin-right:2px" />
                            {{ Form::select('input-type', $inputBy, request()->input('input-type'), array('class' => 'pull-right-sort form-group btn btn-primary btn-sm form-control')) }}
                            {{ Form::select('active', $soringActive, request()->input('active'), array('class' => 'pull-right-sort form-group btn btn-primary form-control btn-sm')) }}
                        @endif
                        <button class="btn btn-primary btn-md" id="buttonSearch"><i class="fa fa-search">&nbsp;</i>Search</button>
                    </form>
                </div>
            </div>
            <div class="horizontal-wrapper" style="display:none" id="formStorySearch">
                {{ Form::open(array('url'=>$searchUrl , 'method'=> 'POST')) }}
                    <div class="btn-horizontal-group bg-default row">
                        <div class="col-sm-10">
                            <input type="text" name="search_keyword" class="form-control" />
                        </div>
                        <div class="col-sm-2">
                            <button class="btn btn-sm btn-info">Search</button>
                        </div>
                    </div>
                {{ Form::close() }}
            </div>
                        @if ($telp_now)
                        <div class="col-md-12" style="margin-bottom: 20px;">
                            <input type="hidden" id="user_login" value="{{ Auth::user()->email }}">
                            <input type="button" class="col-md-12 btn btn-md btn-danger" id="telp_now" value="Telp Now" />
                        </div>
                        @endif
        <!-- The table is going here -->
            @include('admin.contents.stories.partial.index-table-agent')
            
        <!-- End of index table -->

        </div><!-- /.box-body -->
        <div class="box-body no-padding">
            {{ $rowsDesigner->appends(Request::except('page'))->links() }}
        </div>
    </div><!-- /.box -->
    <!-- table -->

    @include('admin.partials.delete-modal')
@stop

@section('style')
    <!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> -->
@stop

@section('script')
<!-- Holder for Upload File Thumbnail -->
{{ \Html::script('assets/vendor/holder/holder.js') }}

<!-- Jquery Datepicker -->
<!-- <script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script> -->

<!-- page script -->
<script type="text/javascript">
    
    $(function()
    {
        $('#select-sort-option').change(function(event){
            var url  = '{{ URL::route("admin.room.index") }}' + '?o=' + event.target.value ;

            url += '&st=' + $('#start-date').val();
            url += '&en=' + $('#end-date').val();

            window.location = url;

        });

        $('.datepicker').datepicker({ dateFormat : 'dd-mm-yy'});
    });

    $(function()
    {
        Holder.addTheme("thumbnail", {
            background: '#EEE',
            foreground: '#AAA',
            size: 11,
            fontweight: 'normal',
            text: 'Image'
          });
        // $("#tableListDesigner").dataTable();
    });

    $(function(){


        var token = '{{ csrf_token() }}';

        $('.btn-delete').click(function() {
            var id = $(this).data('id');
            var name = $(this).data('name');

            var confirm = window.confirm("Are you sure want to delete " + name +" ?");

            if (confirm) {
                $.post('/{{Request::segment(1)}}/{{Request::segment(2)}}/' + id, {
                    _method: "delete",
                    _token: token,
                }).success(function(response) {
                    console.log(response);
                    window.location.reload();
                });
            }


        });

    });


    $('#telp_now').click(function(e) {
      phone = $("#phone").val();
      email = $("#user_login").val();

      //$("#loading-phone").append("Loading ................ ");
      $.ajax({
        url: '/admin/squarepants/notify?phone='+phone+"&email="+email+"&id=&kos_name=&search=search",
        type: 'GET',
        dataType: 'json',
        success: function(data) {
        //    console.log(data)
          //$("#loading-phone").empty();  
          if(data.status == false) {
            alert("Gagal ngirim nomer ke hp");
            //$("#loading-phone").append("<div class='alert alert-danger' style='width: 100%;'>Nomer hp tidak sesuai format.</div>");
          } else {
            alert("Berhasil ngirim nomer ke hp");
            //$("#loading-phone").append("<div class='alert alert-success' style='width: 100%;'>Cek hp anda.");
          }
        }
      });
    });

</script>
@stop
