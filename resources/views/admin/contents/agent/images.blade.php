@extends('admin.layouts.main')
@section('content')

<!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Agent Name : {{ $phone }}</h3>
            <div class="box-tools pull-right">
                <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">

            <table id="tableListTag" class="table table-striped">
                <thead>
                    <tr>
                        <th width="30px;">No</th>
                        <th>Kost Name</th>
                        <th>Images</th>
                    </tr>
                </thead>
                <tbody>
                   @foreach ($rowsDesigner AS $key => $room)
                      <tr>
                      	<td>{{ $key+1 }}</td>
                      	<td>{{ $room->name }}</td>
                      	<td>
                      		@if (count($room->list_cards) > 0)
	                            @foreach($room->list_cards AS $ke => $photos)
	                              <img src="{{ $photos['photo_url'] }}" class="col-md-2" />
	                      		@endforeach
                      		@else 
                              Noting ... ?
                      		@endif
                      	</td>
                      </tr>
                   @endforeach
                </tbody>
                
            </table>

        <div class="box-body no-padding">
            {{ $rowsDesigner->appends(Request::except('page'))->links() }}
        </div>

        </div><!-- /.box-body -->
    </div><!-- /.box -->
    <!-- table -->

@stop