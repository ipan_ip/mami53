@extends('admin.layouts.main')
@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
            <div class="box-tools pull-right">
                <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
            </div>
        </div><!-- /.box-header -->

        <div class="row">
            <div class="col-md-12">
                <div class="col-md-12">
                    <a href="{{ URL::to('admin/owner/popup/create') }}" class="col-md-12 btn btn-sm btn-warning" style="margin: 5px 10px 15px 0px;">Create popup owner</a>
                </div>
            </div>
        </div>

        <div class="box-body no-padding">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th width="30px;">No</th>
                        <th>Name</th>
                        <th>Scheme</th>
                        <th>Url</th>
                        <th>Active</th>
                        <th class="table-action-column">Action</th>
                    </tr>
                </thead>
                <tbody>
                   @foreach ($data AS $key => $value)
                        <tr>
                            <td>{{ $key+1 }}</td>
                            <td>{{ $value->title }}</td>
                            <td>{{ $value->scheme }}</td>
                            <td>{{ $value->url }}</td>
                            <td>
                                @if ($value->is_active == 1)
                                    <label class="label label-success">Yes</label>
                                @else
                                    <label class="label label-danger">No</label>
                                @endif
                            </td>
                            <td>
                                <a href="{{ route('admin.popup.edit', $value->id) }}" class="btn btn-xs btn-default">Edit</a>
                                <a href="{{ url('admin/owner/popup/destroy', $value->id) }}" onclick="return confirm('Yakin mau hapus saya?')" title="Delete" class="btn btn-xs btn-danger">Delete</a>
                            </td>
                        </tr>
                        
                   @endforeach
                </tbody>
                
            </table>
            <div class="box-body no-padding">
            {{ $data->appends(Request::except('page'))->links() }}
        </div>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
    <!-- table -->

@stop
@section('script')
<!-- page script -->
<script type="text/javascript">
    $(function()
    {
        $("#tableListTag").dataTable();
    });
</script>
@stop