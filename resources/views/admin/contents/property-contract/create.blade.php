@extends('admin.layouts.main')

@section('content')
    <a class="btn btn-xs bg-navy btn-flat" href="{{ route('admin.property.index') }}">Back</a>
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

        <form action="{{ URL::route('admin.property.contract.store', $property->id) }}" method="POST" class="form-horizontal form-bordered">
            @csrf
            <div class="box-body no-padding">
                <div class="form-group bg-default">
                    <label for="property-name" class="control-label col-sm-2">Property Name</label>
                    <div class="col-sm-10">
                        <div name="property_name" id="property-level-name" class="form-control" >{{ $property->name }}</div>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="property-level" class="control-label col-sm-2">Property Level</label>
                    <div class="col-sm-10">
                        <select class="form-control" name="property_level_id">
                            @foreach($levelList as $key => $value)
                                <option value="{{ $key }}">{{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="joined-at" class="control-label col-sm-2">Joined at</label>
                    <div class="col-sm-10">
                        <input type='text' class="form-control datetimepicker" id="datetime-joindate" name="joined_at" />
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="total-package" class="control-label col-sm-2">Total Package</label>
                    <div class="col-sm-10">
                        <input type="text" name="total_package" id="total-package" class="form-control" value="{{ old('total_package') }}">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <div class="col-sm-10 col-sm-push-2">
                        <button type="submit" class="btn btn-primary">Create</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <link href="{{ asset('assets/vendor/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" >
    <script src="{{ asset('assets/vendor/moment/moment.min.js') }}"></script>

    <script src="{{ asset('assets/vendor/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
    <script type="text/javascript">
            $(function () {
                $('.datetimepicker').datetimepicker({
                    inline: true,
                    sideBySide: true,
                    format: 'Y-MM-DD H:mm:s'
                });
            });
        </script>
@endsection
