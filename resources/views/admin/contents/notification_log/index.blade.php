@extends('admin.layouts.main')

@section('content')
	<!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
            <div class="box-tools pull-right">
                <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <form action="" class="form-inline" method="get" style="text-align:right;padding:10px;">
                        <input type="text" name="notification_id" class="form-control input-sm"  placeholder="Notification ID"  autocomplete="off" value="{{ request()->input('notification_id') }}">
                        <input type="text" name="from" class="form-control input-sm datepicker" placeholder="Start Date" value="{{ request()->input('from') }}" id="start-date" autocomplete="off" style="margin-right:2px" />
                        <input type="text" name="to" class="form-control input-sm datepicker" placeholder="End Date" value="{{ request()->input('to') }}" id="end-date" autocomplete="off" style="margin-right:2px" />

                        {{ Form::select('notification_channel', [
                                0 => 'All',
                                1 => 'Email',
                                2 => 'SMS',
                                3 => 'Push',
                                4 => 'Whatsapp',
                            ], request()->input('notification_channel'), array('class' => 'pull-right-sort form-group btn btn-default')) }}
                        <button class="btn btn-primary btn-md" id="buttonSearch"><i class="fa fa-search">&nbsp;</i>Search</button>
                    </form>
                </div>
            </div>

            <table  class="table table-striped">
                <thead>
                    <tr>
                        <th width="10%">ID</th>
                        <th width="10%">Notification ID</th>
                        <th width="10%">User ID</th>
                        <th width="10%">Channel</th>
                        <th width="10%">Target</th>
                        <th width="10%">Status</th>
                        <th width="20%">Content</th>
                        <th width="20%">Created At</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($logs as $log)
                    <tr>
                        <td width="10%">{{ $log->id }}</td>
                        <td width="10%">{{ $log->notification_id ?? 'Data Not Available' }}</td>
                        <td width="10%">{{ $log->user_id ?? 'Data Not Available' }}</td>
                        <td width="10%">{{ $log->channel }}</td>
                        <td width="10%">{{ chunk_split($log->target, 15, ' ') }}</td>
                        <td width="10%">{{ $log->status }}</td>
                        <td width="20%">
                            <p>{{ urldecode($log->content) }}</p>
                        </td>
                        <td width="20%">{{ $log->created_at }}</td>
                    </tr>
                    @endforeach
                </tbody>

                <tfoot>
                    <tr>
                        <th width="10%">ID</th>
                        <th width="10%">Notification ID</th>
                        <th width="10%">User ID</th>
                        <th width="10%">Channel</th>
                        <th width="10%">Target</th>
                        <th width="10%">Status</th>
                        <th width="20%">Content</th>
                        <th width="20%">Created At</th>
                    </tr>
                </tfoot>
            </table>
        </div><!-- /.box-body -->
        <div class="box-body no-padding">
            {{ $logs->appends(Request::except('page'))->links() }}
        </div>
    </div><!-- /.box -->
    <!-- table -->
    <script type="text/javascript">
        $(function()
        {
            $('#select-sort-option').change(function(event){
                var url  = '{{ URL::route("admin.notification.log") }}' + '?o=' + event.target.value ;

                url += '&st=' + $('#start-date').val();
                url += '&en=' + $('#end-date').val();

                window.location = url;

            });

            $('.datepicker').datepicker({ dateFormat : 'dd-mm-yy'});
        });
    </script>
@endsection