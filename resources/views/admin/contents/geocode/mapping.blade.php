@extends('admin.layouts.main')

@section('style')
<style>
    .table>tbody>tr>td {
        vertical-align: middle;
    }

    .font-large {
        font-size: 1.5em;
    }

    .font-semi-large {
        font-size: 1.15em;
    }

    .font-grey {
        color: #9E9E9E;
    }

    .dropdown-menu>li>a:hover {
        color: #333
    }

    .label-grey {
        background-color: #CFD8DC;
    }

    /* Map */
    #map-canvas {
        height: 480px;
    }
</style>
<link rel="stylesheet" href="//unpkg.com/leaflet@1.7.1/dist/leaflet.css" />
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
@endsection

@section('content')
<div class="box box-default">
    <!-- Box Header -->
    <div class="box-header">
        <div class="col-md-12 text-center">
            <h3 class="box-title"><i class="fa fa-map-marker"></i> {{ $title }}</h3>
        </div>
    </div>
    <!-- /.box-header -->

    <div class="box-body row">
        {{-- Left Column --}}
        <div class="col-md-6" style="padding-left:10px">
            {{-- Search Bar --}}
            <div class="box-body row with-margin">
                <div class="col-lg-12">
                    <form action="" method="get">
                        <div class="input-group input-group-md">
                            <input type="text" name="keyword" class="form-control input-md" placeholder="Type Keyword"
                                autocomplete="off" value="{{ request()->input('keyword') }}">
                            <span class="input-group-btn">
                                <button class="btn btn-primary btn-md" type="submit"><i
                                        class="fa fa-search"></i>&nbsp;Get Suggestions</button>
                            </span>
                        </div><!-- /input-group -->
                    </form>
                </div><!-- /.col-lg-6 -->
            </div><!-- /.row -->

            {{-- Table --}}
            <div class="box-body with-margin">
                @if (empty($rows))
                <h4 class="text-center">Please type keyword to show suggestions</h4>
                @else
                <table class="table">
                    <thead>
                        <tr>
                            <th>Suggestion</th>
                            <th class="text-center">Lat/Long</th>
                            <th class="text-center">Boundary</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($rows as $index => $row)
                        <tr>
                            <td>
                                <h5><strong>{{ $row['title'] }}</strong></h5>
                                {{ $row['area'] }}
                            </td>
                            <td class="text-center">
                                {{ $row['latitude'] }}, {{ $row['longitude'] }}
                            </td>
                            <td class="text-center">
                                @if (isset($row['geocode_id']))
                                <button type="button" class="btn btn-success btn-sm actions" data-action="view"
                                    data-data="{{ $row['geocode_id'] }}">
                                    View Boundary <i class="fa fa-arrow-right"></i>
                                </button>
                                @else
                                <label class="label label-danger">None</label>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                @endif
            </div>
        </div>

        {{-- Map / Right Column --}}
        <div class="col-md-6">
            <div id="map-canvas"></div>
        </div>
    </div><!-- /.box-body -->
</div><!-- /.box -->
<!-- table -->

@endsection

@section('script')
<script src="//unpkg.com/leaflet@1.7.1/dist/leaflet-src.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.js"></script>
<script>
    // Sweetalert functions
    function triggerAlert(type, message) {
        Swal.fire({
            type: type,
            customClass: {
                container: 'custom-swal'
            },
            title: message
        });
    }

    function triggerLoading(message) {
        Swal.fire({
            title: message,
            customClass: {
                container: 'custom-swal'
            },
            allowEscapeKey: false,
            allowOutsideClick: false,
            onOpen: () => {
                swal.showLoading();
            }
        });
    }

    /*
    DOM
    */
    $(function() {
        // Initial variables
        var _data;
        var _geoLayer = null;
        var map = new L.Map('map-canvas');
        var osm = new L.TileLayer('{{ config('services.osm.host') }}/hot/{z}/{x}/{y}.png', {
            attribution: false
        });

        map.addLayer(osm);

        // Actions button listeners
        $('.actions').click((e) => {
            e.preventDefault();
            var action = $(e.currentTarget).data('action');

            // View boundary
            if (action == 'view') {
                _data = $(e.currentTarget).data('data');
                triggerLoading("Rendering boundary...");

                $.ajax({
                    type: 'GET',
                    url: "/admin/geocode/mapping/" + _data,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: (res) => {
                        if (_geoLayer != null) map.removeLayer(_geoLayer);

                        _geoLayer = L.geoJSON(JSON.parse(res));
                        var geom = _geoLayer.addTo(map);
                        map.fitBounds(geom.getBounds());

                        // map.invalidateSize();

                        if (Swal.isLoading()) Swal.close();
                    },
                    error: (error) => {
                        Swal.fire({
                            type: 'error',
                            customClass: {
                                container: 'custom-swal'
                            },
                            title: 'Error',
                            text: error
                        })
                    }
                });
            }
        });
    });
</script>
@endsection