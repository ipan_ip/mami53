@extends('admin.layouts.main')

@section('style')
<style>
    .table>tbody>tr>td {
        vertical-align: middle;
    }

    .font-large {
        font-size: 1.5em;
    }

    .font-semi-large {
        font-size: 1.15em;
    }

    .font-grey {
        color: #9E9E9E;
    }

    .dropdown-menu>li>a:hover {
        color: #333
    }

    .label-grey {
        background-color: #CFD8DC;
    }

    /* Modal tweak */

    .modal-open {
        overflow: hidden;
        position: fixed;
        width: 100%;
    }


    /* Centering the modal */

    .modal {
        text-align: center;
        padding: 0 !important;
    }

    .modal:before {
        content: '';
        display: inline-block;
        height: 100%;
        vertical-align: middle;
        margin-right: -4px;
    }

    .modal-dialog {
        display: inline-block;
        text-align: left;
        vertical-align: middle;
    }

    #map-canvas {
        height: 480px;
    }

    .custom-input-group {
        position: relative;
        display: -ms-flexbox;
        display: flex;
        -ms-flex-align: stretch;
        align-items: stretch;
        width: 100%;
    }

    .custom-addon {
        width: 40%;
    }
</style>
<link rel="stylesheet" href="//unpkg.com/leaflet@1.7.1/dist/leaflet.css" />
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
@endsection

@section('content')
<div class="box box-default">
    <!-- Box Header -->
    <div class="box-header">
        <div class="col-md-12 text-center">
            <h3 class="box-title"><i class="fa fa-map-marker"></i> {{ $title }}</h3>
        </div>
    </div>
    <!-- /.box-header -->

    <div class="box-body">
        {{-- Top Bar --}}
        <div class="horizontal-wrapper">
            <form action="" method="get">
                <div class="row" style="padding-top: 10px">
                    <div class="col-md-5">
                        <div class="input-group custom-input-group">
                            <select name="level" class="custom-addon input-group-addon" value="{{ request()->input('level') }}">
                                <option value="all">All Area</option>
                                <option value="province">Provinsi</option>
                                <option value="city">Kota/Kabupaten</option>
                                <option value="subdistrict">Kecamatan</option>
                                <option value="village">Desa/Kelurahan</option>
                            </select>
                            <input type="text" name="name" class="form-control" placeholder="Type to search..."
                                autocomplete="off" value="{{ request()->input('name') }}">
                            <span class="input-group-btn">
                                <button class="btn btn-primary" type="submit" id="buttonSearch"><i class="fa fa-search">&nbsp;</i>Search</button>
                            </span>
                        </div>
                    </div>
                    <div class="col-md-7"></div>
                </div>
            </form>
        </div>

        {{-- Table --}}
        <table class="table">
            <thead>
                <tr>
                    <th class="text-center">Area Level</th>
                    <th class="text-center">Provinsi</th>
                    <th class="text-center">Kota/Kab.</th>
                    <th class="text-center">Kecamatan</th>
                    <th class="text-center">Desa/Kelurahan</th>
                    <th class="text-center"></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($rows as $index => $row)
                <tr>
                    <td class="text-center">
                        @if (empty($row->city))
                        <label class="label label-success">Provinsi</label>
                        @elseif (empty($row->subdistrict))
                        <label class="label label-warning">Kota/Kabupaten</label>
                        @elseif (empty($row->village))
                        <label class="label label-danger">Kecamatan</label>
                        @else
                        <label class="label label-primary">Desa/Kelurahan</label>
                        @endif
                    </td>
                    <td class="text-center">
                        {{ $row->province }}
                    </td>
                    <td class="text-center">
                        {{ $row->city }}
                    </td>
                    <td class="text-center">
                        {{ $row->subdistrict }}
                    </td>
                    <td class="text-center">
                        {{ $row->village }}
                    </td>
                    <td class="table-action-column" style="padding-right:15px!important;">
                        <button type="button" class="btn btn-default btn-sm actions" data-action="view"
                            data-data="{{ $row }}">
                            <i class="fa fa-map-marker"></i> View Boundary
                        </button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div><!-- /.box-body -->

    <div class="box-body no-padding text-center">
        {{ $rows->appends(Request::except('page'))->links() }}
    </div>
</div><!-- /.box -->
<!-- table -->

<!-- Modal -->
<div class="modal modal-lg" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top:2px;"><i
                        class="fa fa-lg fa-times-circle"></i></button>
                <h4 class="modal-title" id="modalTitle"></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div id="map-canvas"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /. modal -->

@endsection

@section('script')
<script src="//unpkg.com/leaflet@1.7.1/dist/leaflet-src.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.js"></script>
<script>
    // Sweetalert functions
    function triggerAlert(type, message) {
        Swal.fire({
            type: type,
            customClass: {
                container: 'custom-swal'
            },
            title: message
        });
    }

    function triggerLoading(message) {
        Swal.fire({
            title: message,
            customClass: {
                container: 'custom-swal'
            },
            allowEscapeKey: false,
            allowOutsideClick: false,
            onOpen: () => {
                swal.showLoading();
            }
        });
    }

    /*
    DOM
    */
    $(function() {
        // Initial variables
        var _data;
        var _geoLayer;
        var map = new L.Map('map-canvas');
        var osm = new L.TileLayer('{{ config('services.osm.host') }}/hot/{z}/{x}/{y}.png', {
            attribution: false
        });

        map.addLayer(osm);

        // Actions button listeners
        $('.actions').click((e) => {
            e.preventDefault();
            var action = $(e.currentTarget).data('action');

            // View boundary
            if (action == 'view') {
                _data = $(e.currentTarget).data('data');

                triggerLoading("Rendering map...");

                $.ajax({
                    type: 'GET',
                    url: "/admin/geocode/mapping/" + _data.id,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: (res) => {
                        $('#modal').modal('show');

                        if (Swal.isLoading()) Swal.close();

                        if (_geoLayer != null) map.removeLayer(_geoLayer);

                        _geoLayer = L.geoJSON(JSON.parse(res));
                        var geom  = _geoLayer.addTo(map);
                        map.fitBounds(geom.getBounds());
                    },
                    error: (error) => {
                        Swal.fire({
                            type: 'error',
                            customClass: {
                                container: 'custom-swal'
                            },
                            title: 'Error',
                            text: error
                        })
                    }
                });
            }
        });

        // Add modal event
        $('#modal')
            .on("show.bs.modal", (e) => {
                var areaName;
                if (_data.city == '') {
                    areaName = _data.province.toUpperCase();
                } else if (_data.subdistrict == '') {
                    areaName = _data.city.toUpperCase() + ', ' + _data.province.toUpperCase();
                } else if (_data.village == '') {
                    areaName = _data.subdistrict.toUpperCase() + ', ' + _data.city.toUpperCase() + ', ' + _data.province.toUpperCase();
                } else {
                    areaName = _data.village.toUpperCase() + ', ' + _data.subdistrict.toUpperCase() + ', ' + _data.city.toUpperCase() + ', ' + _data.province.toUpperCase();
                }

                $('#modalTitle').html('<i class="fa fa-map-marker"></i>  Boundary of ' + areaName);
            })
            .on('shown.bs.modal', function() {
                map.invalidateSize();
            })
            .on("hidden.bs.modal", (e) => {
                map.removeLayer(_geoLayer);
            });
    });
</script>
@endsection