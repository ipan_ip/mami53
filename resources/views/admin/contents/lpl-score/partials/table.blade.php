<table id="table" 
    class="table table-responsive table-striped" 
    data-toggle="table" 
    data-show-refresh="true" 
    data-search="true"
    data-show-search-button="false" 
    data-side-pagination="server" 
    data-page-size="50"
    data-pagination="true" 
    data-ajax="ajaxLoadData"
    data-query-params="queryParams"
    data-query-params-type=""
    data-use-row-attr-func="true"
    data-reorderable-rows="true">
    <thead>
        <tr>
            <th class="text-center strong" data-field="rank" data-formatter="rankFormatter">Criteria Rank</th>
            <th data-field="items" data-formatter="nameFormatter">Criteria (Name, Score, Attribute, Action)</th>
            <th class="text-center" data-field="actions" data-formatter="actionFormatter">Actions</th> 
        </tr>
    </thead>
</table>