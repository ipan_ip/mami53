@extends('admin.layouts.main')

@section('style')
<style>
    .table>tbody>tr>td {
        vertical-align: middle;
    }

    .font-large {
        font-size: 1.5em;
    }

    .font-semi-large {
        font-size: 1.2em;
    }

    .font-grey {
        color: #9E9E9E;
    }

    .dropdown-menu>li>a:hover {
        color: #333
    }

    .label-grey {
        background-color: #CFD8DC;
    }

    [hidden] {
        display: none !important;
    }

    /* Select2 tweak */

    .input-group .select2-container,
    .form-group .select2-container {
        position: relative !important;
        z-index: 2;
        float: left !important;
        width: 100% !important;
        margin-bottom: 0 !important;
        display: table !important;
        table-layout: fixed !important;
    }

    /* Sweetalert */

    .swal2-container {
        z-index: 99999 !important;
    }

    .swal-shown {
        height: auto !important;
    }

    .custom-swal {
        z-index: 10000 !important;
    }

    .custom-wide-swal {
        width: 480px !important;
        font-size: 12px !important;
    }

    /* Custom table row */
    tr.inactive td {
        background-color: #E6C8C8;
    }

    /* Modal tweak */

    .modal-open {
        overflow: hidden;
        position: fixed;
        width: 100%;
    }


    /* Centering the modal */

    .modal {
        text-align: center;
        padding: 0 !important;
    }

    .modal:before {
        content: '';
        display: inline-block;
        height: 100%;
        vertical-align: middle;
        margin-right: -4px;
    }

    .modal-dialog {
        display: inline-block;
        text-align: left;
        vertical-align: middle;
    }

    /* Grid */
    .grid {
        position: relative;
    }

    .item {
        display: block;
        position: absolute;
        width: 100px;
        height: 100px;
        margin: 5px;
        z-index: 1;
        background: #000;
        color: #fff;
    }

    .item.muuri-item-dragging {
        z-index: 3;
    }

    .item.muuri-item-releasing {
        z-index: 2;
    }

    .item.muuri-item-hidden {
        z-index: 0;
    }

    .item-content {
        position: relative;
        width: 100%;
        height: 100%;
    }

    /* Blink effect */
    .blink {
        animation: blink 1s infinite;
    }

    @keyframes blink {
        0% {
            opacity: 1;
        }

        75% {
            opacity: 1;
        }

        76% {
            opacity: 0;
        }

        100% {
            opacity: 0;
        }
    }

    .bootstrap-datetimepicker-widget {
        z-index: 9999 !important;
    }
</style>
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.css">
<link rel="stylesheet" href="//gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css">
<link rel="stylesheet" href="//unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.css">
<link rel="stylesheet"
    href="//unpkg.com/bootstrap-table@1.18.0/dist/extensions/reorder-rows/bootstrap-table-reorder-rows.css">
<link rel="stylesheet"
    href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" />
<link rel="stylesheet" href="//cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
@endsection

@section('content')
<!-- table -->
<div class="box box-default">
    <!-- Box Header -->
    <div class="box-header">
        @if(!empty($error))
        <div class="alert alert-danger"> {{ $error }}</div>
        @endif
        <div class="col-md-6 text-center">
            <h3 class="box-title"><i class="fa fa-sort-numeric-desc"></i> {{ $boxTitle }}</h3>
        </div>
        <!-- Buttons -->
        <div class="col-md-6">
            <div class="form-horizontal pull-right" style="padding-top: 10px;">
                <button class="btn btn-primary actions" style="margin-right: 10px;" onclick="actionButton('history')">
                    <i class="fa fa-history"></i>&nbsp;&nbsp;View History
                </button>
                <button class="btn btn-default actions" style="margin-right: 10px;"
                    onclick="actionButton('calculator')">
                    <i class="fa fa-calculator"></i>&nbsp;&nbsp;Score Calculator
                </button>
            </div>
        </div>
    </div>
    <!-- /.box-header -->
    <!-- box-body -->
    <div class="box-body">
        <!-- Table -->
        @include('admin.contents.lpl-score.partials.table')
        <!-- table -->
    </div>
</div><!-- /.box -->

@include('admin.contents.lpl-score.partials.modal')

@endsection

@section('script')
<script src="//cdn.jsdelivr.net/npm/holderjs@2.9.6/holder.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.js"></script>
<script src="//gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/TableDnD/1.0.3/jquery.tablednd.min.js"></script>
<script src="//unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.js"></script>
<script src="//unpkg.com/bootstrap-table@1.18.0/dist/extensions/reorder-rows/bootstrap-table-reorder-rows.min.js">
</script>
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment-with-locales.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js">
</script>
<script src="//cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
<script type="text/javascript">
    // Helper variable
    let _editing, _data, _modalTitle;

    function triggerAlert(type, message) {
        Swal.fire({
            type: type,
            customClass: {
                container: 'custom-swal'
            },
            html: message
        });
    }

    function triggerLoading(message) {
        Swal.fire({
            title: message,
            customClass: {
                container: 'custom-swal'
            },
            allowEscapeKey: false,
            allowOutsideClick: false,
            onOpen: () => {
                swal.showLoading();
            }
        });
    }

    function ajaxLoadData(params) {
        let url = '/admin/lpl-score/data';

        $.get(url + '?' + $.param(params.data)).then(function (res) {
            params.success(res)
        })
    }

    function queryParams(params) {
        let newParams = {};

        newParams.search = params.searchText;
        newParams.limit = params.pageSize;
        newParams.page = params.pageNumber;
        
        return newParams
    }

    function rankFormatter(value, row) {
        return `<strong>${value}</strong>`;
    }

    function nameFormatter(value, row) {
        let html = '';
        html += `<table class="table table-bordered table-responsive" style="background-color: #ffffff00;">`;

        value.forEach(el => {
            html +=
                `<tr>
                    <td style="width:40%;">
                        <strong>${el.name}</strong> 
                        <br/>${el.description}
                        <br/>created at : ${el.created_at}
                        <br/>last update : ${el.updated_at}
                    </td>
                    <td style="width:10%;"><span class="label label-success h1" style="font-size: medium;">${el.score}</span></td>
                    <td style="width:25%;"><span class="label label-default">${el.attribute}</span></td>
                    <td style="width:25%;">
                        <a href='#' onclick='actionButton(\"update\", ${JSON.stringify(el)})'><i class='fa fa-edit'></i> Update</a>
                    </td>
                </tr>`;
        });

        html += `</table>`;
        return html;
    }

    function dateFormatter(value, row, index) {
        return row.created_at + ' / ' + value;
    }
    
    function actionFormatter(value, row, index) {
        let html = '<div class="btn-group">'+
            '<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" data-boundary="window">'+
                '<i class="fa fa-caret-down"></i> Actions'+
                '</button>'+
            '<ul class="dropdown-menu pull-right action-button">';
                html += "<li><a href='#' onclick='actionButtonMergeAndSplit(\"update\", " + JSON.stringify(row) + ")'><i class='fa fa-object-ungroup'></i> Merge</a></li>";
                html += "<li><a href='#' onclick='actionButtonMergeAndSplit(\"update\", " + JSON.stringify(row) + ")'><i class='fa fa-object-group'></i> Split</a></li>";
                html += '</ul>'
            '</div>';
        
        return html;
    }

    // Action buttons click event
    function actionButton(action, data) {
        // Show history
        if (action === 'history') {
            _modalTitle = "LPL Criteria History";
            
            $('#modal').modal('show');
        }

        // Update criteria name and/or description
        else if (action === 'update') {
            let title, html, btnText, url, type, payload;

            _data = data;

            if (_data.areas_count < 1) {
                triggerAlert('error', "<h4>Please add some areas first!</h4>" +
                    "<h5>You may do it by clicking button<br/><br />" +
                    "<span class='label label-default'><i class='fa fa-thumb-tack'></i> View/Update Areas</span></h5>");
                return;
            }

            Swal.fire({
                title: 'Update Criteria ' + data.name,
                html: '<br/><br/><div class="row">'+
                            '<div class="col-md-12">'+
                                '<div class="form-group">'+
                                    '<label>Name</label>'+
                                    '<input type="text" class="form-control" id="name"/>'+
                                '</div>'+
                            '</div>'+
                            '<div class="col-md-12">'+
                                '<div class="form-group">'+
                                    '<label>Description</label>'+
                                    '<textarea class="form-control" id="description" rows=5/>'+
                                '</div>'+
                            '</div>'+
                        '</div>',
                // customClass: 'custom-wide-swal',
                showLoaderOnConfirm: true,
                showCancelButton: true,
                confirmButtonText: 'Update',
                onOpen: function() {
                    $("#name").attr('placeholder', _data.name);
                    $("#description").val('').prop('placeholder', _data.description);
                },
                preConfirm: () => {
                    let nameVal = $.trim($('#name').val());
                    let descVal = $.trim($('#description').val());

                    if (nameVal === '' && descVal === '') {
                        return Swal.showValidationMessage('No changes have been made!');
                    }

                    if (nameVal !== '' && descVal === '') {
                        descVal = _data.description;
                    }

                    if (nameVal === '' && descVal !== '') {
                        nameVal = _data.name;
                    }

                    return $.ajax({
                        type: 'POST',
                        url: '/admin/lpl-score/update',
                        dataType: 'json',
                        headers: {
                            'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr("content")
                        },
                        data: {
                            'id': _data.id,
                            'name': nameVal,
                            'description': descVal
                        },
                        success: (response) => {
                            console.log(response);
                            return response;
                        },
                        error: (error) => {
                            triggerAlert('error', '<h5>' + error.responseJSON.meta.message + '</h5>');
                        }
                    });
                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                if (!result.dismiss) {
                    if (result.value.status) {
                        Swal.fire({
                            type: 'success',
                            title: "Criteria has been updated successfully",
                            text: result.value.message,
                            onClose: () => {
                                $('#table').bootstrapTable('refreshOptions', {});
                            }
                        });
                    } else {
                        triggerAlert('error', result.value.meta.message);
                    }
                }
            })
        }

        // Reorder the criteria
        else if (action === 'reorder') {
            _data = data;

            Swal.fire({
                title: 'WARNING!',
                html: "<h4>Changing criteria ordering will impact all Kos datas.</h4>" +
                    "<h5>This action can not be undone!<br/><br/>Are you sure to do this action?</h5>" +
                    `<a type="button" onclick="swal.closeModal(); return false;" class="btn btn-success" aria-label="" style="">WAIT, CONTINUE EDIT!</a>` + 
                    `<a type="button" onclick="window.location.reload();" class="btn btn-warning" aria-label="" >RESET!</a>`,
                type: 'warning',
                confirmButtonText: 'YES, I\'M SURE AND SAVE ALL!',
                preConfirm: (val) => {
                    // Show loader
                    triggerLoading('loading...');

                    return $.ajax({
                        type: 'POST',
                        url: "/admin/lpl-score/reorder",
                        dataType: 'json',
                        headers: {
                            'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr("content")
                        },
                        data: {
                            _data
                        },
                        done: () => {
                            if (Swal.isLoading) Swal.close();
                        },
                        success: (response) => {
                            return response;
                        },
                        error: (error) => {
                            triggerAlert('error', '<h5>' + error.responseJSON.meta.message + '</h5>');
                        }
                    });
                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                var res = result.value;

                if (res.status === true) {
                    Swal.fire({
                        type: 'success',
                        title: "LPL criteria data has been updated successfully",
                        html: "<h5><strong>Please be notice!</strong><br>" +
                            "All Kos scores would be affected in up to 24 hours<br></h5>",
                        onClose: () => {
                            $('#table').bootstrapTable('refreshOptions', {});
                        }
                    });
                } else {
                    triggerAlert('error', res.meta.message);
                }
            })
        }

        // Open calculator page
        else if (action === 'calculator') {
            Swal.fire({
                type: 'warning',
                title: 'Under Maintenance',
                html: 'Will be available under task BG-1658'
            });
        }
    }

    function actionButtonMergeAndSplit(action, data) {
        Swal.fire({
            type: 'warning',
            title: 'Under Maintenance',
            html: 'Will be available soon'
        });
    }

    $(function()
    {
        $('#table').on('reorder-row.bs.table', (e, data) => {
            actionButton('reorder', data);
        });

        // Action button tweak
        var dropdownMenu;       

        $(window).on('show.bs.dropdown', function(e) {        
            dropdownMenu = $(e.target).find('.action-button');
            $('body').append(dropdownMenu.detach());          
            dropdownMenu.css('display', 'block');             
            dropdownMenu.position({                           
                'my': 'left top',                            
                'at': 'left bottom',                         
                'of': $(e.relatedTarget)                      
            })                                                
        });                           

        $(window).on('hide.bs.dropdown', function(e) {        
            $(e.target).append(dropdownMenu.detach());        
            dropdownMenu.hide();                              
        });             

        // :: Modal Events ::
        let modal = $('#modal');
        modal
            .on("show.bs.modal", (e) => {
                triggerLoading('Fetching history data..');

                modal.data('bs.modal').options.keyboard = false;
                modal.data('bs.modal').options.backdrop = 'static';

                const modalTitle = $('#modal-title');
                const modalContainer = $('#modal-container');

                modalTitle.html(_modalTitle);
                modalContainer.empty();

                function compileTable(data) {
                    let html, icon;

                    data.forEach(item => {
                        icon = '<i class="fa fa-edit">';

                        event = item.admin?.name + ' edit '
                            + '<strong>' + item.criteria.name + '</strong> from ' 
                            + item.original_score + ' to '
                            + item.modified_score;

                        html += '<tr><td class="text-center">' 
                            + item.created_at + '</td><td class="text-center">' 
                            + icon + '</td><td>' + event + '</td></tr>';
                    });

                    modalContainer.html(html);
                }

                $.ajax({
                    type: 'GET',
                    url: '/admin/lpl-score/history',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: response => {
                        compileTable(response.data)
                    },
                    error: error => {
                        $('#modal').modal('hide');
                        triggerAlert('error', '<h3>Oops!</h3><h5>Something wrong happened.<br/>Please try again in 10 minutes.</h5>');
                    },
                    complete: function () {
                        if (Swal.isLoading()) Swal.close();
                    }
                });
            })
    });
</script>
@stop