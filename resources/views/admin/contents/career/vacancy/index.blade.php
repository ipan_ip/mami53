@extends('admin.layouts.main')

@section('style')
<style>
    span.apartment-color,
    tr.apartment-color td {
        color: #0D47A1;
    }

    tr.kost-active td {
        background-color: #C8E6C9;
    }

    .table>tbody>tr>td {
        vertical-align: middle;
    }

    .font-large {
        font-size: 1.5em;
    }

    .font-semi-large {
        font-size: 1.15em;
    }

    .font-grey {
        color: #9E9E9E;
    }

    .dropdown-menu>li>a:hover {
        color: #333
    }

    .label-grey {
        background-color: #CFD8DC;
    }

    /* Sweetalert */

    .swal-wide {
        width: 850px !important;
        /* font-size: 14px!important; */
    }

    .swal-shown {
        height: auto !important;
    }

    .custom-swal {
        z-index: 10000 !important;
    }

    .custom-wide-swal {
        width: 850px !important;
    }

    .modal {

        z-index: 1050;
        height: auto;
        width: auto;
        overflow: hidden;
        -webkit-overflow-scrolling: touch;
        background: none;
        outline: 0;
    }

    .modal-backdrop {
        display: none;
    }
</style>
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
@endsection

@section('content')
<!-- table -->
<div class="box box-primary" style="margin-top: 20px;">
    <div class="box-body no-padding">
        <div class="horizontal-wrapper">
            <div class="btn-horizontal-group bg-default">
                <a class="btn btn-primary btn-sm" href="{{ route('admin.career.vacancy.create') }}" id="btn-create"
                    style="margin-left: 20px">
                    <i class="fa fa-plus">&nbsp;</i>Add Vacancy
                </a>
            </div>
        </div>

        <!-- The table is going here -->
        @include('admin.contents.career.vacancy.partial.index-table-admin')
        <!-- End of index table -->

    </div><!-- /.box-body -->
    <div class="box-body no-padding">
        {{ $rowsVacancy->appends(Request::except('page'))->links() }}
    </div>
</div><!-- /.box -->
<!-- table -->
@stop

@section('script')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.js"></script>
    <script type="text/javascript">
        function deleteVacancy(id) {
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!'
            }).then(res => {
                if (res.value) {
                    $(`#delete-vacancy-${id}`).submit();
                }
            })
        }
    </script>
@stop