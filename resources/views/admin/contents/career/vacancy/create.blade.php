@extends('admin.layouts.main')

@section('style')
<style>
    span.apartment-color,
    tr.apartment-color td {
        color: #0D47A1;
    }

    tr.kost-active td {
        background-color: #C8E6C9;
    }

    .table>tbody>tr>td {
        vertical-align: middle;
    }

    .font-large {
        font-size: 1.5em;
    }

    .font-semi-large {
        font-size: 1.15em;
    }

    .font-grey {
        color: #9E9E9E;
    }

    .dropdown-menu>li>a:hover {
        color: #333
    }

    .label-grey {
        background-color: #CFD8DC;
    }

    /* Sweetalert */

    .swal-wide {
        width: 850px !important;
        /* font-size: 14px!important; */
    }

    .swal-shown {
        height: auto !important;
    }

    .custom-swal {
        z-index: 10000 !important;
    }

    .custom-wide-swal {
        width: 850px !important;
    }

    .modal {

        z-index: 1050;
        height: auto;
        width: auto;
        overflow: hidden;
        -webkit-overflow-scrolling: touch;
        background: none;
        outline: 0;
    }

    .modal-backdrop {
        display: none;
    }
</style>
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
@endsection

@section('content')
<!-- table -->
<div class="box box-primary" style="margin-top: 20px;">
    <div class="box-body no-padding">
        <div class="horizontal-wrapper">
            <form method="POST" action="{{ route('admin.career.vacancy.store') }}">
                @csrf
                
                <div class="container" style="padding-left: 45px;">
                    <div class="row" style="margin-top: 25px;">
                        <h4>Create a new Vacancy</h4>
                    </div>

                    <div class="row" style="margin-top: 25px;">
                        <label for="position-select">
                            Position Name
                        </label>
                    </div>

                    <div class="row" style="margin-top: 5px;">
                        <select class="js-example-basic-single {{ $errors->first('position') ? 'is-invalid' : '' }}" id="position-select" name="position" style="width: 90%;">
                            @foreach($positions as $position)
                                <option value="{{ $position->id }}" {{ old('position') === $position->id ? 'selected' : '' }}>{{ $position->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="row" style="margin-top: 25px;">
                        <div class="form-group">
                            <label for="level">Level</label>
                    
                            <input id="level" type="text" class="form-control {{ $errors->first('level') ? 'is-invalid' : '' }}" name="level" placeholder="Junior, Senior, Lead..."
                                style="width: 90%; margin-bottom: 15px;" value="{{ old('level') }}">
                        </div>
                    </div>

                    <div class="row" style="margin-top: 25px;">
                        <label for="locations-select">
                            Locations
                        </label>
                    </div>

                    <div class="row" style="margin-top: 5px;">
                        <select class="js-example-basic-multiple {{ $errors->first('locations') ? 'is-invalid' : '' }}" id="locations-select" name="locations[]" multiple="multiple"
                            style="width: 90%;">
                            @foreach($locations as $location)
                                <option value="{{ $location->id }}" {{ in_array($location->id, (old('locations') ?? [])) ? 'selected' : '' }}>{{ $location->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="row" style="margin-top: 25px;">
                        <div class="form-group" id="description-container">
                            <label for="add-description">Description</label>

                            @if (old('descriptions'))
                                @foreach (old('descriptions') as $description)
                                    <input type="text" class="form-control {{ $errors->first('description') ? 'is-invalid' : '' }}" name="descriptions[]" placeholder="Description"
                                        style="width: 90%; margin-bottom: 15px;" value="{{ $description }}" required>
                                @endforeach
                            @endif

                            <input type="text" class="form-control {{ $errors->first('description') ? 'is-invalid' : '' }}" id="add-description" placeholder="Description" style="width: 90%;">
                        </div>
                    </div>

                    <div class="row" style="margin-top: 25px;">
                        <div class="form-group" id="requirement-container">
                            <label for="add-requirement">Requirement</label>

                            @if (old('requirements'))
                                @foreach (old('requirements') as $requirement)
                                    <input type="text" class="form-control {{ $errors->first('requirement') ? 'is-invalid' : '' }}" name="requirements[]" placeholder="Requirement"
                                        style="width: 90%; margin-bottom: 15px;" value="{{ $requirement }}" required>
                                @endforeach
                            @endif
                    
                            <input type="text" class="form-control {{ $errors->first('requirement') ? 'is-invalid' : '' }}" id="add-requirement" placeholder="Requirement" style="width: 90%;">
                        </div>
                    </div>

                    <div class="row" style="margin-top: 25px;">
                        <div class="form-group">
                            <label for="contact">Contact</label>
                    
                            <input id="contact" type="text" class="form-control{{ $errors->first('contact') ? 'is-invalid' : '' }}" name="contact" placeholder="Contact"
                                style="width: 90%; margin-bottom: 15px;" value="{{ old('contact') }}" required>
                        </div>
                    </div>

                    <div class="row" style="margin-top: 25px;" >
                        <input class="form-check-input" name="is_open" value="1" type="checkbox" id="defaultCheck1" checked="{{ old('is_open') == 1 ? 'checked' : '' }}">

                        <label class="form-check-label" for="defaultCheck1" style="margin-left: 5px;">
                            Available
                        </label>
                    </div>

                    <div class="row" style="margin-top: 25px; margin-bottom: 25px;">
                        <button class="btn btn-primary">Create</button>
                    </div>
                </div>
            </form>
        </div>
    </div><!-- /.box-body -->
</div><!-- /.box -->
<!-- table -->
@stop

@section('script')
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.js-example-basic-multiple').select2({
            width: 'resolve'
        });

        $('.js-example-basic-single').select2({
            width: 'resolve'
        });

        $('#add-description').focus(() => {
            let element = $.parseHTML('<input type="text" class="form-control" name="descriptions[]" placeholder="Description" style="width: 90%; margin-bottom: 15px;" required>')[0];
            let selector = $(element);
            
            selector.blur(() => {
                if (selector.val().length === 0) {
                    selector.remove();
                }
            });

            $('#add-description').before(element);

            selector.focus();
        });

        $('#add-requirement').focus(() => {
            let element = $.parseHTML('<input type="text" class="form-control" name="requirements[]" placeholder="Requirement" style="width: 90%; margin-bottom: 15px;" required>')[0];
            let selector = $(element);

            selector.blur(() => {
                if (selector.val().length === 0) {
                    selector.remove();
                }
            });

            $('#add-requirement').before(element);

            selector.focus();
        });
    });
</script>
@stop