<table id="tableListDesigner" class="table">
    <thead>
        <tr>
            <th></th>
            <th class="text-center">ID</th>
            <th>Posisi</th>
            <th class="text-center">Level</th>
            <th>Requirements</th>
            <th>Descriptions</th>
            <th>Locations</th>
            <th class="text-center">Open</th>
            <th class="text-center">Created</th>
            <th class="text-center">Updated</th>
            <th class="table-action-column" width="10%"></th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach ($rowsVacancy as $rowVacancyKey => $rowVacancy)
        <tr>
            <td></td>
            <td class="text-center">{{ $rowVacancy->id }}</td>
            <td>{{ $rowVacancy->position->name }}</td>
            <td class="text-center">{{ $rowVacancy->level }}</td>
            <td>
                @foreach($rowVacancy->requirements as $requirement)
                    <li>{{ substr($requirement->content, 0, 25) }}...</li>
                @endforeach
            </td>
            <td>
                @foreach($rowVacancy->description as $description)
                    <li>{{ substr($description->content, 0, 25) }}...</li>
                @endforeach
            </td>
            <td>
                <p>
                    @foreach($rowVacancy->locations as $location)
                        {{ $location->name . ($location !== $rowVacancy->locations->last() ? ',' : '') }}
                    @endforeach
                </p>
            </td>
            <td class="text-center">
                @if ($rowVacancy->is_open)
                    <p class="text-success">Yes</p>
                @else
                    <p class="text-danger">No</p>
                @endif
            </td>
            <td class="text-center">{{ $rowVacancy->created_at }}</td>
            <td class="text-center">{{ $rowVacancy->updated_at }}</td>
            <td class="table-action-column" widtd="10%">
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-cog"></i> <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu pull-right">
                        <li>
                            <a title="Edit Location" data-action="edit" data-id="{{ $rowVacancy->id }}"
                                data-name="{{ $rowVacancy->name }}" href="{{ route('admin.career.vacancy.edit', $rowVacancy->id) }}">
                                <i class="fa fa-pencil"></i> Edit Vacancy
                            </a>
                        </li>

                        @if ($rowVacancy->is_open == 1)
                            <li>
                                <a title="Edit Location" data-action="edit" data-id="{{ $rowVacancy->id }}" data-name="{{ $rowVacancy->name }}"
                                    href="{{ route('admin.career.vacancy.toggle', $rowVacancy->id) }}">
                                    <i class="fa fa-times"></i> Close Vacancy
                                </a>
                            </li>
                        @else
                            <li>
                                <a title="Edit Location" data-action="edit" data-id="{{ $rowVacancy->id }}" data-name="{{ $rowVacancy->name }}"
                                    href="{{ route('admin.career.vacancy.toggle', $rowVacancy->id) }}">
                                    <i class="fa fa-check"></i> Open Vacancy
                                </a>
                            </li>
                        @endif
                
                        <li>
                            <form method="POST" action="{{ route('admin.career.vacancy.destroy', $rowVacancy->id) }}" id="delete-vacancy-{{ $rowVacancy->id }}">
                                @csrf
                                @method('DELETE')
                            </form>

                            <a title="Delete Location" data-action="delete" data-id="{{ $rowVacancy->id }}" data-name="{{ $rowVacancy->name }}" onClick="deleteVacancy({{ $rowVacancy->id }})">
                                <i class="fa fa-trash"></i> Delete Vacancy
                            </a>
                        </li>
                    </ul>
                </div>
            </td>
            <td></td>
        </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th></th>
            <th class="text-center">ID</th>
            <th>Posisi</th>
            <th class="text-center">Level</th>
            <th>Requirements</th>
            <th>Descriptions</th>
            <th>Locations</th>
            <th class="text-center">Open</th>
            <th class="text-center">Created</th>
            <th class="text-center">Updated</th>
            <th class="table-action-column" width="10%"></th>
            <th></th>
        </tr>
    </tfoot>
</table>