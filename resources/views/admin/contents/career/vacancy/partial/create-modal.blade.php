<div class="modal" id="create-modal" role="dialog" aria-labelledby="modalLabel">
    <div class="modal-dialog" role="modal">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" id="btn-close" data-dismiss="modal" aria-label="Close"
                    style="margin-top:2px;"><i class="fa fa-lg fa-times-circle"></i></button>
                <h4 class="modal-title"><i class="fa fa-plus"></i>&nbsp;&nbsp;<span id="modalTitle"></span></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group" id="form-group">
                            <form action="#" method="POST" id="form">
                                <label for="name">Position Name</label>
                                <input type="text" id="name" name="name" class="form-control"
                                    placeholder="Developer, Data Scientist,...">
                                <span class="help-block" id="validation-message"></span>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="btn-submit">
                    <i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal-backdrop fade in" id="backdrop"></div>