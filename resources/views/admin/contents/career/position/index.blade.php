@extends('admin.layouts.main')

@section('style')
<style>
    span.apartment-color,
    tr.apartment-color td {
        color: #0D47A1;
    }

    tr.kost-active td {
        background-color: #C8E6C9;
    }

    .table>tbody>tr>td {
        vertical-align: middle;
    }

    .font-large {
        font-size: 1.5em;
    }

    .font-semi-large {
        font-size: 1.15em;
    }

    .font-grey {
        color: #9E9E9E;
    }

    .dropdown-menu>li>a:hover {
        color: #333
    }

    .label-grey {
        background-color: #CFD8DC;
    }

    /* Sweetalert */

    .swal-wide {
        width: 850px !important;
        /* font-size: 14px!important; */
    }

    .swal-shown {
        height: auto !important;
    }

    .custom-swal {
        z-index: 10000 !important;
    }

    .custom-wide-swal {
        width: 850px !important;
    }

    .modal {

        z-index: 1050;
        height: auto;
        width: auto;
        overflow: hidden;
        -webkit-overflow-scrolling: touch;
        background: none;
        outline: 0;
    }

    .modal-backdrop {
        display: none;
    }
</style>
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
@endsection

@section('content')
<!-- table -->
<div class="box box-primary" style="margin-top: 20px;">
    <div class="box-body no-padding">
        <div class="horizontal-wrapper">
            <div class="btn-horizontal-group bg-default">
                <button type="button" class="btn btn-primary btn-sm" id="btn-create" style="margin-left: 20px">
                    <i class="fa fa-plus">&nbsp;</i>Add Position
                </button>
            </div>
        </div>

        <!-- The table is going here -->
        @include('admin.contents.career.position.partial.index-table-admin')
        <!-- End of index table -->

        @include('admin.contents.career.position.partial.create-modal')

    </div><!-- /.box-body -->
    <div class="box-body no-padding">
        {{ $rowsPosition->appends(Request::except('page'))->links() }}
    </div>
</div><!-- /.box -->
<!-- table -->
@stop

@section('script')
<!-- Holder for Upload File Thumbnail -->
{{ \Html::script('assets/vendor/holder/holder.js') }}

<script src="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.js"></script>

<!-- page script -->
<script type="text/javascript">
    const showBackdrop = () => {
        $('#backdrop').css({ display: 'block' });
    }

    const hideBackdrop = () => {
        $('#backdrop').css({ display: 'none' });
    }

    const displayValidation = (res) => {
        if (res.success) {
            $('#create-modal').hide();
            hideBackdrop();            

            Swal.fire(
                'Sucsess!',
                `${res.message} You will be redirected soon.`,
                'success'
            )
            window.location.reload();
        }
        else {
            $('#create-modal').show();
            $('#form-group').addClass('has-error');
            $('#validation-message').html(res.error.name[0]);
        }        
    }

    const submit = (url, data) => {
        $('#create-modal').hide();

        Swal.fire({
            title: 'Processing your request',
            text: 'Please wait, we are currently processing your request',
            timer: 2000,
            timerProgressBar: true,
        });

        return new Promise((resolve, reject) => {
            $.ajax(url, {
                method: 'POST',
                data: data,
                success: res => {
                    Swal.close();
                    resolve(res)
                },
                error: (req, status, error) => {
                    Swal.close();
                    reject({ request: req, status: status, error: error })
                }
            });
        });
    };

    $(function () {
        var token = '{{ csrf_token() }}';

        $('#btn-submit').click(() => {
            $('#form').submit();
        });

        $('#btn-create').click(() => {
            $('#modalTitle').html('Create Position');
            $('#create-modal').show();
            showBackdrop();

            $('#form').unbind('submit');
            $('#form').submit(e => {
                e.preventDefault();

                submit("{{ route('admin.career.position.store') }}", {
                    name: $('#name').val()
                }).then(displayValidation);
            });
        });

        $('[data-action=edit]').click((e) => {
            const id = e.target.getAttribute('data-id');
            const name = e.target.getAttribute('data-name');
            
            $('#modalTitle').html('Edit Position');
            $('#name').val(name);
            $('#create-modal').show();
            showBackdrop();

            $('#form').unbind('submit');
            $('#form').submit(e => {
                e.preventDefault();
                
                submit(`/admin/career/position/${id}`, {
                    '_method': 'PATCH',
                    name: $('#name').val()
                }).then(displayValidation);
            });
        });

        $('[data-action=delete]').click((e) => {
            const id = e.target.getAttribute('data-id');
            const name = e.target.getAttribute('data-name');

            Swal.fire({
                title: `Are you sure you want to delete ${name} ?`,
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    submit(`/admin/career/position/${id}`, {
                        '_method': 'DELETE',
                    }).then(res => {
                        if (res.success) {
                            Swal.fire(
                                'Deleted!',
                                `${name} has been deleted.`,
                                'success'
                            );
                            window.location.reload();
                        }
                        else {
                            Swal.fire(
                                'Error!',
                                `${res.error}`,
                                'error'
                            );
                        }
                    })
                }
            })
        });

        $('#btn-close').click(() => {
            $('#create-modal').hide();
            hideBackdrop();
        });
    });

</script>
@stop