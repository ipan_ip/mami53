<table id="tableListDesigner" class="table">
    <thead>
        <tr>
            <th></th>
            <th>ID</th>
            <th>Lokasi Kantor</th>
            <th class="text-center" width="10%">Jumlah Lowongan</th>
            <th class="table-action-column" width="10%"></th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach ($rowsLocation as $rowLocationKey => $rowLocation)
        <tr>
            <td></td>
            <td>{{ $rowLocation->id }}</td>
            <td>{{ $rowLocation->name }}</td>
            <td class="text-center" widtd="10%">{{ $rowLocation->vacancy_count }}</td>
            <td class="table-action-column" widtd="10%"></td>
            <td class="table-action-column">
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-cog"></i> <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu pull-right">
                        <li>
                            <a title="Edit Location" data-action="edit" data-id="{{ $rowLocation->id }}" data-name="{{ $rowLocation->name }}">
                                <i class="fa fa-pencil"></i> Edit Location
                            </a>
                        </li>

                        <li>
                            <a title="Delete Location" data-action="delete" data-id="{{ $rowLocation->id }}" data-name="{{ $rowLocation->name }}">
                                <i class="fa fa-trash"></i> Delete Location
                            </a>
                        </li>
                    </ul>
                </div>
            </td>
        </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th></th>
            <th>ID</th>
            <th>Lokasi Kantor</th>
            <th class="text-center" width="10%">Jumlah Lowongan</th>
            <th class="table-action-column" width="10%"></th>
            <th></th>
        </tr>
    </tfoot>
</table>