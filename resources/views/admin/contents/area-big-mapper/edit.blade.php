@extends('admin.layouts.main')

@section('style')
    {{ HTML::style('assets/vendor/jquery-chosen/chosen.css') }}
    {{ HTML::style('assets/vendor/jquery-chosen-custom/chosen.css') }}

    <style>
        #chosenForm .chosen-choices {
            border: 1px solid #ccc;
            border-radius: 4px;
            min-height: 34px;
            padding: 6px 12px;
        }
        #chosenForm .form-control-feedback {
            /* To make the feedback icon visible */
            z-index: 100;
        }
    </style>
@endsection

@section('content')
<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">{{ $boxTitle }}</h3>
    </div><!-- /.box-header -->
    <form action="{{ route('admin.area-big-mapper.update', $mapper->id) }}" method="post" class="form-horizontal form-bordered">
        <input type="hidden" name="_method" value="PUT">

        <div class="box-body no-padding">
            <div class="form-group bg-default">
                <label for="inputAreaCity" class="col-sm-2 control-label">Area City</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Area City" id="inputAreaCity" name="area_city"  value="{{ old('area_city', $mapper->area_city) }}">
                </div>
            </div>
            <div class="form-group bg-default">
                <label for="inputAreaBig" class="col-sm-2 control-label">Area Big</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Area Big" id="inputAreaBig" name="area_big"  value="{{ old('area_big', $mapper->area_big) }}">
                </div>
            </div>
            <div class="form-group bg-default">
                <label for="inputProvince" class="col-sm-2 control-label">Provinsi</label>
                <div class="col-sm-10">
                    <select class="form-control chosen-select" id="inputProvince" name="province">
                        <option value=""> NULL</option>
                        @foreach ($provinceList as $province)
                            <option value="{{ $province }}" {{ old('province', $mapper->province) == $province ? 'selected="selected"' : '' }}>{{ $province }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </form>
</div><!-- /.box -->

<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">Manage Landing Kost</h3>
    </div><!-- /.box-header -->

    <form action="{{ route('admin.area-big-mapper.update-landing', $mapper->id) }}" method="post" class="form-horizontal form-bordered" id="manage-landing-form">
        @foreach($landings as $key => $landing) 
            <div class="box-body no-padding landing-wrapper">
                <div class="form-group bg-default">
                    <label for="input-subdistrict-{{ $key }}" class="col-sm-2 control-label">Subdistrict</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" placeholder="Subdistrict" id="input-subdistrict-{{ $key }}" name="subdistrict[]" value="{{ $landing->area_subdistrict }}">
                    </div>
                </div>
                <div class="form-group bg-default">
                    <label for="input-landing-{{ $key }}" class="col-sm-2 control-label">Landing Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control input-landing-name" placeholder="Landing Name" id="input-landing-{{ $key }}" name="landing_name[]"  value="{{ $landing->heading_1 }}" data-toggle="dropdown">
                        <input type="hidden" class="input-landing-id" name="landing_id[]" value="{{ $landing->id }}" id="landing-id-{{ $key }}">
                    </div>
                </div>
                <div class="form-group bg-default">
                    <div class="col-sm-10 col-sm-offset-2">
                        <a href="/admin/area-big-mapper/remove-landing/{{ $landing->id }}" class="btn btn-danger">Hapus</a>
                    </div>
                </div>
            </div>
            <hr>
        @endforeach

        <div class="form-group" id="landing-btn">
            <div class="col-sm-offset-2 col-sm-10">
                <a href="#" class="btn btn-warning" id="landing-adder">Add Landing</a>
                <button type="submit" id="btn-landing-submit" class="btn btn-primary {{ count($landings) == 0 && is_null(old('subdistrict')) ? 'hide' : '' }}">Save</button>
            </div>
        </div>
    </form>
</div>

<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">Manage Landing Apartment</h3>
    </div><!-- /.box-header -->

    <form action="{{ route('admin.area-big-mapper.update-landing-apartment', $mapper->id) }}" method="post" class="form-horizontal form-bordered" id="manage-landing-form">
        @foreach($landingsApartment as $key => $landing) 
            <div class="box-body no-padding landing-wrapper">
                <div class="form-group bg-default">
                    <label for="input-subdistrict-apartment-{{ $key }}" class="col-sm-2 control-label">Subdistrict</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" placeholder="Subdistrict" id="input-subdistrict-apartment-{{ $key }}" name="subdistrict_apartment[]" value="{{ $landing->area_subdistrict }}">
                    </div>
                </div>
                <div class="form-group bg-default">
                    <label for="input-landing-apartment-{{ $key }}" class="col-sm-2 control-label">Landing Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control input-landing-name-apartment" placeholder="Landing Name" id="input-landing-apartment-{{ $key }}" name="landing_name_apartment[]"  value="{{ $landing->heading_1 }}" data-toggle="dropdown">
                        <input type="hidden" class="input-landing-id-apartment" name="landing_id_apartment[]" value="{{ $landing->id }}" id="landing-id-apartment-{{ $key }}">
                    </div>
                </div>
                <div class="form-group bg-default">
                    <div class="col-sm-10 col-sm-offset-2">
                        <a href="/admin/area-big-mapper/remove-landing-apartment/{{ $landing->id }}" class="btn btn-danger">Hapus</a>
                    </div>
                </div>
            </div>
            <hr>
        @endforeach

        <div class="form-group" id="landing-btn-apartment">
            <div class="col-sm-offset-2 col-sm-10">
                <a href="#" class="btn btn-warning" id="landing-adder-apartment">Add Landing</a>
                <button type="submit" id="btn-landing-submit-apartment" class="btn btn-primary {{ count($landingsApartment) == 0 && is_null(old('subdistrict_apartment')) ? 'hide' : '' }}">Save</button>
            </div>
        </div>
    </form>
</div>

<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">Manage Landing Jobs</h3>
    </div><!-- /.box-header -->

    <form action="{{ route('admin.area-big-mapper.update-landing-jobs', $mapper->id) }}" method="post" class="form-horizontal form-bordered" id="manage-landing-form">
        @foreach($landingsJob as $key => $landing) 
            <div class="box-body no-padding landing-wrapper">
                <div class="form-group bg-default">
                    <label for="input-subdistrict-jobs-{{ $key }}" class="col-sm-2 control-label">Subdistrict</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" placeholder="Subdistrict" id="input-subdistrict-jobs-{{ $key }}" name="subdistrict_jobs[]" value="{{ $landing->area_subdistrict }}">
                    </div>
                </div>
                <div class="form-group bg-default">
                    <label for="input-landing-jobs-{{ $key }}" class="col-sm-2 control-label">Landing Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control input-landing-name-jobs" placeholder="Landing Name" id="input-landing-jobs-{{ $key }}" name="landing_name_jobs[]"  value="{{ $landing->heading_1 }}" data-toggle="dropdown">
                        <input type="hidden" class="input-landing-id-jobs" name="landing_id_jobs[]" value="{{ $landing->id }}" id="landing-id-jobs-{{ $key }}">
                    </div>
                </div>
                <div class="form-group bg-default">
                    <div class="col-sm-10 col-sm-offset-2">
                        <a href="/admin/area-big-mapper/remove-landing-jobs/{{ $landing->id }}" class="btn btn-danger">Hapus</a>
                    </div>
                </div>
            </div>
            <hr>
        @endforeach

        <div class="form-group" id="landing-btn-jobs">
            <div class="col-sm-offset-2 col-sm-10">
                <a href="#" class="btn btn-warning" id="landing-adder-jobs">Add Landing</a>
                <button type="submit" id="btn-landing-submit-jobs" class="btn btn-primary {{ count($landingsJob) == 0 && is_null(old('subdistrict_jobs')) ? 'hide' : '' }}">Save</button>
            </div>
        </div>
    </form>
</div>
@endsection



@section('script')
{{ HTML::script('assets/vendor/jquery-chosen/chosen.jquery.js') }}

<script>
    var config = {
        '.chosen-select'           : {width: '100%'}
    };

    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }

    function clickSuggestion(idx) {
        key = idx;
        val = $('.suggestion-item-' + idx).text();
        el = $('.suggestion-item-' + idx);

        $(el).parent().parent().prev('.input-landing-name').val(val);
        $(el).parent().parent().next('.input-landing-id').val(key);
    }

    function clickSuggestionApartment(idx) {
        key = idx;
        val = $('.suggestion-item-' + idx).text();
        el = $('.suggestion-item-' + idx);

        $(el).parent().parent().prev('.input-landing-name-apartment').val(val);
        $(el).parent().parent().next('.input-landing-id-apartment').val(key);
    }

    function clickSuggestionJobs(idx) {
        key = idx;
        val = $('.suggestion-item-' + idx).text();
        el = $('.suggestion-item-' + idx);

        $(el).parent().parent().prev('.input-landing-name-jobs').val(val);
        $(el).parent().parent().next('.input-landing-id-jobs').val(key);
    }

    $(function() {
        landing_key = {{ count($landings) }};
        landing_key_apartment = {{ count($landingsApartment) }};
        landing_key_jobs = {{ count($landingsJob) }};

        $('#landing-adder').on('click', function(e) {
            e.preventDefault();

            html = '<div class="box-body no-padding landing-wrapper">' +
                        '<div class="form-group bg-default">' +
                            '<label for="input-subdistrict-' + landing_key + '" class="col-sm-2 control-label">Subdistrict</label>' +
                            '<div class="col-sm-10">' +
                                '<input type="text" class="form-control" placeholder="Subdistrict" id="input-subdistrict-' + landing_key + '" name="subdistrict[]">' +
                            '</div>' +
                        '</div>' +
                        '<div class="form-group bg-default">' +
                            '<label for="input-landing-' + landing_key + '" class="col-sm-2 control-label">Landing Name</label>' +
                            '<div class="col-sm-10">' +
                                '<input type="text" class="form-control input-landing-name" placeholder="Landing Name" id="input-landing-' + landing_key + '" name="landing_name[]" data-toggle="dropdown">' +
                                '<input type="hidden" class="input-landing-id" name="landing_id[]" id="landing-id-' + landing_key +  '">'
                            '</div>' +
                        '</div>' +
                    '</div><hr>';

            $(html).insertBefore('#landing-btn');

            $('#btn-landing-submit').removeClass('hide');

            landing_key++;
        });

        $('#landing-adder-apartment').on('click', function(e) {
            e.preventDefault();

            html = '<div class="box-body no-padding landing-wrapper">' +
                        '<div class="form-group bg-default">' +
                            '<label for="input-subdistrict-apartment-' + landing_key_apartment + '" class="col-sm-2 control-label">Subdistrict</label>' +
                            '<div class="col-sm-10">' +
                                '<input type="text" class="form-control" placeholder="Subdistrict" id="input-subdistrict-apartment-' + landing_key_apartment + '" name="subdistrict_apartment[]">' +
                            '</div>' +
                        '</div>' +
                        '<div class="form-group bg-default">' +
                            '<label for="input-landing-apartment-' + landing_key_apartment + '" class="col-sm-2 control-label">Landing Name</label>' +
                            '<div class="col-sm-10">' +
                                '<input type="text" class="form-control input-landing-name-apartment" placeholder="Landing Name" id="input-landing-apartment-' + landing_key_apartment + '" name="landing_name_apartment[]" data-toggle="dropdown">' +
                                '<input type="hidden" class="input-landing-id-apartment" name="landing_id_apartment[]" id="landing-id-' + landing_key_apartment +  '">'
                            '</div>' +
                        '</div>' +
                    '</div><hr>';

            $(html).insertBefore('#landing-btn-apartment');

            $('#btn-landing-submit-apartment').removeClass('hide');

            landing_key_apartment++;
        });

        $('#landing-adder-jobs').on('click', function(e) {
            e.preventDefault();

            html = '<div class="box-body no-padding landing-wrapper">' +
                        '<div class="form-group bg-default">' +
                            '<label for="input-subdistrict-jobs-' + landing_key_jobs + '" class="col-sm-2 control-label">Subdistrict</label>' +
                            '<div class="col-sm-10">' +
                                '<input type="text" class="form-control" placeholder="Subdistrict" id="input-subdistrict-jobs-' + landing_key_jobs + '" name="subdistrict_jobs[]">' +
                            '</div>' +
                        '</div>' +
                        '<div class="form-group bg-default">' +
                            '<label for="input-landing-jobs-' + landing_key_jobs + '" class="col-sm-2 control-label">Landing Name</label>' +
                            '<div class="col-sm-10">' +
                                '<input type="text" class="form-control input-landing-name-jobs" placeholder="Landing Name" id="input-landing-jobs-' + landing_key_jobs + '" name="landing_name_jobs[]" data-toggle="dropdown">' +
                                '<input type="hidden" class="input-landing-id-jobs" name="landing_id_jobs[]" id="landing-id-' + landing_key_jobs +  '">'
                            '</div>' +
                        '</div>' +
                    '</div><hr>';

            $(html).insertBefore('#landing-btn-jobs');

            $('#btn-landing-submit-jobs').removeClass('hide');

            landing_key_jobs++;
        });

        var callLandingSuggestion = function(e) {
            $.ajax({
                type: 'POST',
                url: '/admin/area-big-mapper/landing-suggestion',
                data: {
                    'params': e.target.value
                },
                success: function(data) {
                    $(e.target).next('.dropdown-menu').remove();

                    listOption = '<ul class="dropdown-menu suggestion-list" aria-labelledby="dLabel">'
                    for(idx in data.landings) {
                        listOption += '<li><a href="#" class="suggestion-item suggestion-item-' + idx + '" data-key="' + idx + '" onclick="clickSuggestion(' + idx + ')">' + data.landings[idx] + '</a></li>';
                    }

                    listOption += '</ul>';

                    $(listOption).insertAfter($(e.target));

                    $('.suggestion-list').dropdown();
                }
                
            });
        }

        var callLandingSuggestionApartment = function(e) {
            $.ajax({
                type: 'POST',
                url: '/admin/area-big-mapper/landing-suggestion-apartment',
                data: {
                    'params': e.target.value
                },
                success: function(data) {
                    $(e.target).next('.dropdown-menu').remove();

                    listOption = '<ul class="dropdown-menu suggestion-list" aria-labelledby="dLabel">'
                    for(idx in data.landings) {
                        listOption += '<li><a href="#" class="suggestion-item suggestion-item-' + idx + '" data-key="' + idx + '" onclick="clickSuggestionApartment(' + idx + ')">' + data.landings[idx] + '</a></li>';
                    }

                    listOption += '</ul>';

                    $(listOption).insertAfter($(e.target));

                    $('.suggestion-list').dropdown();
                }
                
            });
        }


        var callLandingSuggestionJobs = function(e) {
            $.ajax({
                type: 'POST',
                url: '/admin/area-big-mapper/landing-suggestion-jobs',
                data: {
                    'params': e.target.value
                },
                success: function(data) {
                    $(e.target).next('.dropdown-menu').remove();

                    listOption = '<ul class="dropdown-menu suggestion-list" aria-labelledby="dLabel">'
                    for(idx in data.landings) {
                        listOption += '<li><a href="#" class="suggestion-item suggestion-item-' + idx + '" data-key="' + idx + '" onclick="clickSuggestionJobs(' + idx + ')">' + data.landings[idx] + '</a></li>';
                    }

                    listOption += '</ul>';

                    $(listOption).insertAfter($(e.target));

                    $('.suggestion-list').dropdown();
                }
                
            })
        }

        var delay = (function(){
          var timer = 0;
          return function(callback, ms){
            clearTimeout (timer);
            timer = setTimeout(callback, ms);
          };
        })();

        $(document).on('keyup', '.input-landing-name', function(e) {
            delay(function(){
                callLandingSuggestion(e)
            }, 1000 );
        });

        $(document).on('keyup', '.input-landing-name-apartment', function(e) {
            delay(function(){
                callLandingSuggestionApartment(e)
            }, 1000 );
        });

        $(document).on('keyup', '.input-landing-name-jobs', function(e) {
            delay(function(){
                callLandingSuggestionJobs(e)
            }, 1000 );
        });

    });
</script>
@endsection
