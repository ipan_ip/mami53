@extends('admin.layouts.main')

@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <a href="{{ URL::route('admin.area-big-mapper.create') }}">
                        <button type="button" name="button" class="btn-add btn btn-primary btn-sm">
                            <i class="fa fa-plus">&nbsp;</i> Add Area Big Mapper </button>
                    </a>
                    {{ Form::open(array('method'=>'get','class'=>'form-inline','style'=>'text-align:right;padding:10px;')) }}
                        <input type="text" name="q" class="form-control input-sm"  placeholder="Area"  autocomplete="off" value="{{ old('q', request()->get('q')) }}">
                        <button class="btn btn-primary btn-md" id="buttonSearch"><i class="fa fa-search">&nbsp;</i>Search</button>
                    {{ Form::close() }}
                </div>
            </div>

            <table id="tableListLanding" class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Area City</th>
                        <th>Area Big</th>
                        <th>Province</th>
                        <th class="table-action-column" width="120px">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($mappers as $key => $mapper)
                    <tr>
                        <td>{{ $mapper->id }}</td>
                        <td>{{ $mapper->area_city }}</td>
                        <td>{{ $mapper->area_big }}</td>
                        <td>{{ $mapper->province }}</td>
                        <td class="table-action-column">
                            <div class="btn-action-group">
                                <a href="{{ route("admin.area-big-mapper.edit", $mapper->id) }}" title="Edit">
                                    <i class="fa fa-pencil"></i></a>
                                <a href="{{ route("admin.area-big-mapper.destroy", $mapper->id) }}" class="btn-delete" title="Delete">
                                    <i class="fa fa-trash-o"></i></a>
                                </a>
                                <a href="{{ route("admin.area-big-mapper.sync", $mapper->id) }}" title="Update Area Big in Room">
                                    <i class="fa fa-refresh"></i></a>
                                </a>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="box-body no-padding">
                {{ $mappers->appends(Request::except('page'))->links() }}
            </div>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
    <!-- table -->
@endsection
