@extends('admin.layouts.main')

@section('style')
    {{ HTML::style('assets/vendor/jquery-chosen/chosen.css') }}
    {{ HTML::style('assets/vendor/jquery-chosen-custom/chosen.css') }}

    <style>
        #chosenForm .chosen-choices {
            border: 1px solid #ccc;
            border-radius: 4px;
            min-height: 34px;
            padding: 6px 12px;
        }
        #chosenForm .form-control-feedback {
            /* To make the feedback icon visible */
            z-index: 100;
        }
    </style>
@endsection

@section('content')
<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">{{ $boxTitle }}</h3>
    </div><!-- /.box-header -->
    <form action="{{ route('admin.area-big-mapper.store') }}" method="post" class="form-horizontal form-bordered">
        <div class="box-body no-padding">
            <div class="form-group bg-default">
                <label for="inputAreaCity" class="col-sm-2 control-label">Area City</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Area City" id="inputAreaCity" name="area_city"  value="{{ old('area_city') }}">
                </div>
            </div>
            <div class="form-group bg-default">
                <label for="inputAreaBig" class="col-sm-2 control-label">Area Big</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Area Big" id="inputAreaBig" name="area_big"  value="{{ old('area_big') }}">
                </div>
            </div>
            <div class="form-group bg-default">
                <label for="inputProvince" class="col-sm-2 control-label">Provinsi</label>
                <div class="col-sm-10">
                    <select class="form-control chosen-select" id="inputProvince" name="province">
                        <option value=""> NULL</option>
                        @foreach ($provinceList as $province)
                            <option value="{{ $province }}" {{ old('province') == $province ? 'selected="selected"' : '' }}>{{ $province }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </form>
</div><!-- /.box -->
@endsection



@section('script')
{{ HTML::script('assets/vendor/jquery-chosen/chosen.jquery.js') }}

<script>
    var config = {
        '.chosen-select'           : {width: '100%'}
    };

    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>
@endsection
