@extends('admin.layouts.main')
@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
            <div class="box-tools pull-right">
                <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <form action="" class="form-inline" method="get" style="text-align:right;padding:10px;">
                          <input type="text" name="phone_number" class="form-control input-sm"  placeholder="No Hp owner"  autocomplete="off" value="{{ request()->input('phone_number') }}">          
                        <button class="btn btn-primary btn-sm" id="buttonSearch"><i class="fa fa-search">&nbsp;</i>Search</button>
                    </form>
                </div>
            </div>

            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Owner Name</th>
                        <th>Phone Number</th>
                        <th>Type</th>
                        <th>Package Name</th>
                        <th>Amount</th>
                        <th>Created at</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($premiumCashbackHistory AS $key => $value)
                        <tr>
                            <td>{{ $value->user->name }}</td>
                            <td>{{ $value->user->phone_number }}</td>
                            <td>{{ $value->type }}</td>
                            @if ($value->type == 'topup')
                            <th>{{ $value->balance_request->premium_package->name }}</th>
                            @elseif ($value->type == 'booking_commission')
                            <th>Invoice number : {{ $value->payoutTransaction->invoice->invoice_number }}</th>
                            @else
                            <th>-</th>
                            @endif
                            <td>{{ $value->amount }}</td>
                            <td>{{ $value->created_at->format('d M Y H:i') }}</td>
                        </tr>
                    @endforeach
                </tbody>
                
            </table>
            <div class="box-body no-padding">
            {{ $premiumCashbackHistory->appends(Request::except('page'))->links() }}
        </div>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
    <!-- table -->

@stop