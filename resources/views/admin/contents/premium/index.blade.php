@extends('admin.layouts.main')
@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
            <div class="box-tools pull-right">
                <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">

            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <form action="" class="form-inline" method="get" style="text-align:right;padding:10px;">
                       

                          <input type="text" name="q" class="form-control input-sm"  placeholder="No Hp owner"  autocomplete="off" value="{{ request()->input('q') }}">

                          <input type="text" name="total" class="form-control input-sm"  placeholder="Total"  autocomplete="off" value="{{ request()->input('total') }}">

                          <input type="text" name="name" class="form-control input-sm"  placeholder="Name"  autocomplete="off" value="{{ request()->input('name') }}">

                          {{ Form::select('package', $package, request()->input('package'), array('class' => 'pull-right-sort form-group btn btn-primary btn-sm')) }}

                          {{ Form::select('statuso', $verified, request()->input('statuso'), array('class' => 'pull-right-sort form-group btn btn-primary btn-sm')) }}

          
                        <button class="btn btn-primary btn-sm" id="buttonSearch"><i class="fa fa-search">&nbsp;</i>Search</button>
                    </form>
                </div>
            </div>

            <table class="table table-striped">
                <thead>
                    <tr>
                        <th width="30px;">No</th>
                        <th>Name</th>
                        <th>Hp Owner</th>
                        <th>For</th>
                        <th>Bank</th>
                        <th>Atas Nama</th>
                        <th>Total</th>
                        <th>Tanggal</th>
                        <th>Transfer Ke</th>
                        <th>Active</th>
                        <th class="table-action-column">Action</th>
                    </tr>
                </thead>
                <tbody>
                   @foreach ($rowsAccount AS $key => $account)
                       <tr>
                           <td>{{ $key+1 }}</td>
                           <td>{{ $account['name'] }}</td>
                           <td>{{ $account['phone'] }}</td>
                           <td>@if ($account['for'] == 'package')
                                <span class="label label-success">Buy Package</span>
                               @else
                                @if ($account['for'] == 'trial')
                                 <span class="label label-warning">Trial</span>
                                 :: {{ $account['kos_active_count'] }} kos aktif
                                @else   
                                 <span class="label label-danger">Buy View</span>
                                @endif
                               @endif 
                           </td>
                           <td>{{ $account['bank'] }}</td>
                           <td>{{ $account['bank_name'] }}</td>
                           <td>{{ $account['total'] }}</td>
                           <td>{{ $account['times'] }}</td>
                           <td>{{ $account['to_bank'] }}</td>
                           <td>
                           @if ( $account['is_active'])
                               <span class="label label-success">Yes</span>
                           @else
                              <span class="label label-danger">No</span>
                           @endif 
                          </td>
                           <td>
                                <a href="{{ URL::route('admin.premium.edit', $account['id']) }}" class="btn btn-xs btn-warning">Edit</a>
                                
                                @if ($account['is_active'] == false)
                                    <form action="{{ url('/admin/premium/'.$account['id']) }}" style="margin-top:10px" method="POST">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <button class="btn btn-xs btn-danger" type="submit">Delete</button>
                                    </form>
                                @endif
                            </td>
                       </tr>
                   @endforeach
                </tbody>
                
            </table>
            <div class="box-body no-padding">
            {{ $rowPaginate->appends(Request::except('page'))->links() }}
        </div>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
    <!-- table -->

@stop
@section('script')
<!-- page script -->
<script type="text/javascript">
    $(function()
    {
        $("#tableListTag").dataTable();
    });
</script>
@stop