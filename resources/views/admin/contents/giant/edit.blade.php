@extends('admin.layouts.main')
@section('content')
<div class="box box-primary">
  <div class="box-header">
    <h3 class="box-title">{{ $boxTitle }}</h3>
  </div><!-- /.box-header -->

@if ($dummy->statuses == 'live')
<div class="row">
    <div class="col-md-12" style="padding-left: 20px; padding-right: 20px;">
    <form method="POST" action="{{ URL::to('admin/giant/manual/review', $dummy->room->dummy_review[0]->id) }}">
      <table>
        <tr>
            <td>Kebersihan : </td>
            <td>{{ $dummy->room->dummy_review[0]->cleanliness }}</td>
        </tr>

        <tr>
            <td>Kenyamanan : </td>
            <td>{{ $dummy->room->dummy_review[0]->comfort }}</td>
        </tr>

        <tr>
            <td>Keamanan : </td>
            <td>{{ $dummy->room->dummy_review[0]->safe }}</td>
        </tr>

        <tr>
            <td>Harga : </td>
            <td>{{ $dummy->room->dummy_review[0]->price }}</td>
        </tr>

        <tr>
            <td>Fasilitas kamar : </td>
            <td>{{ $dummy->room->dummy_review[0]->room_facilities }}</td>
        </tr>

        <tr>
            <td>Fasilitas publik : </td>
            <td>{{ $dummy->room->dummy_review[0]->public_facilities }}</td>
        </tr>

        <tr>
            <td>Content: </td>
            <td><textarea name="review_content" style="width: 700px; height: 300px;">{{ $dummy->room->dummy_review[0]->content }}</textarea></td>
        </tr>

        <tr>
            <td>Photo</td>
            <td>
            @if ($dummy->server == 1)
                <img src="http://songturu2.mamikos.com/uploads/cache/data/dummy/{{ $dummy->room->dummy_review[0]->photo }}" style="width: 100px;" />
            @else
                <img src="{{ URL::to('uploads/cache/data/dummy', $dummy->room->dummy_review[0]->photo) }}" style="width: 200px;" />
            @endif
                <input type="hidden" name="phone_number" value="{{ $dummy->agent->phone_number }}" />
                <input type="hidden" name="server" value="{{ $dummy->server }}" />
            </td>
        </tr>

        <tr>
            <td>Other photo</td>
            <td>
                <div class="form-group bg-default">
                    <div class="col-sm-10">
                        <div id="Other"
                              action="{{ url('admin/media') }}"
                              method="POST"
                              class="dropzone"
                              uploadMultiple="no">
                            {{ csrf_field() }}
                            <input type="hidden" name="media_type" value="review_photo">
                        </div>
                    </div>
                </div>
                <div id="photo-other-wrapper"></div>
                <hr>
            </td>
        </tr>

      </table>
        <br/><br/>
        <!-- <a href="{{ URL::to('admin/giant/manual/review', $dummy->room->dummy_review[0]->id) }}?agent_phone={{ $dummy->agent->phone_number }}&server={{ $dummy->server }}&designer_id={{ $dummy->designer_id }}" class="btn btn-sm btn-warning col-md-12">Review Manual</a>
        -->
        <input type="submit" value="Manual Review" class="btn btn-warning col-md-12" />
      </form>
    </div>
</div>
<br/><br/>
@endif

  <form action="{{ URL::to('admin/giant/update', $dummy->id) }}?roomid={{ $dummy->designer_id }}" method="POST" class="form-horizontal form-bordered" id="formInsertDesigner">
  <div class="box-body no-padding">

    <div class="row">
      <div class="col-md-12" style="background: #ff0000;">
        <div class="" style="padding: 4px 10px; font-size: 15px; color: #ffffff;"><strong>Di edit oleh:</strong> {{ $dummy->agent->name }} ({{ $dummy->agent->phone_number }}) , tgl: {{ $dummy->created_at->format('d M Y H:i') }}</div>
      </div>
    </div>

    @if (!$is_user)
        <br/>
        <div class="alert alert-warning">
            <strong>Perhatian sodara sodara !!!</strong> Sepertinya agen ini salah daftar deh, dia nggak punya akun user,
            jadi untuk review bisa dilakukan secara manual atau bisa kalian hubungi agen untuk daftar sebagai user.
        </div>
    @endif

        <br/>
        <br/>
        <div class="alert alert-success">
            <strong>Catatan dari agen : <strong> {{ $dummy->agent_description }}
        </div>

    <div class="row">
        <div class="col-md-12">
            @if (!is_null($checkin_photo))
                @if ($dummy->server == 1)
                <img src="http://songturu2.mamikos.com/uploads/cache/data/checkin/{{ $checkin_photo->file_name }}" style="margin-bottom: 20px;"/>
                @else
                    <img src="{{ URL::to('uploads/cache/data/checkin', $checkin_photo->file_name) }}" style="margin-bottom: 20px;"/>
                @endif
            @endif
        </div>
    </div>
    @if ($dummy->type == 'kos')
    <div class="row" style="">
      <div class="col-md-12">
        <div class="form-group">
            <label class="col-sm-2 control-label" style="color: #fff;">Review</label>
            <div class="col-sm-10">
                @if (isset($dummy->room->dummy_review) AND $dummy->statuses != 'live')
                    <table>
                      <tr>
                          <td>Kebersihan : </td>
                          <td>{{ $dummy->room->dummy_review[0]->cleanliness }}</td>
                      </tr>

                      <tr>
                          <td>Kenyamanan : </td>
                          <td>{{ $dummy->room->dummy_review[0]->comfort }}</td>
                      </tr>

                      <tr>
                          <td>Keamanan : </td>
                          <td>{{ $dummy->room->dummy_review[0]->safe }}</td>
                      </tr>

                      <tr>
                          <td>Harga : </td>
                          <td>{{ $dummy->room->dummy_review[0]->price }}</td>
                      </tr>

                      <tr>
                          <td>Fasilitas kamar : </td>
                          <td>{{ $dummy->room->dummy_review[0]->room_facilities }}</td>
                      </tr>

                      <tr>
                          <td>Fasilitas publik : </td>
                          <td>{{ $dummy->room->dummy_review[0]->public_facilities }}</td>
                      </tr>

                      <tr>
                          <td>Content: </td>
                          <td><textarea name="review_content" style="width: 700px; height: 300px;">{{ $dummy->room->dummy_review[0]->content }}</textarea></td>
                      </tr>

                      <tr>
                          <td>Photo</td>
                          <td>
                          @if ($dummy->server == 1)
                              <img src="http://songturu2.mamikos.com/uploads/cache/data/dummy/{{ $dummy->room->dummy_review[0]->photo }}" style="width: 100px;" />
                          @else
                              <img src="{{ URL::to('uploads/cache/data/dummy', $dummy->room->dummy_review[0]->photo) }}" style="width: 200px;" />
                          @endif
                          </td>
                      </tr>

                      <tr>
                          <td>Other photo</td>
                          <td>
                              <div class="form-group bg-default">
                                  <div class="col-sm-10">
                                      <div id="Other"
                                            action="{{ url('admin/media') }}"
                                            method="POST"
                                            class="dropzone"
                                            uploadMultiple="no">
                                          {{ csrf_field() }}
                                          <input type="hidden" name="media_type" value="review_photo">
                                      </div>
                                  </div>
                              </div>
                              <div id="photo-other-wrapper"></div>
                              <hr>
                          </td>
                      </tr>

                    </table>
                @endif
            </div>
        </div>

      </div>
    </div>
    @endif
    <div class="row">
        <div class="col-md-12">
          <table class="table table-condensed">
            <tr>
                <td>Cover</td>
                <td>
                  @foreach ($photo_cover AS $key => $value)
                      @if ($dummy->server == 1)
                          <img src="http://songturu2.mamikos.com/uploads/cache/data/dummy/{{ $value->file_name }}" style="width: 100px;" />
                      @else
                          <img src="{{ URL::to('uploads/cache/data/dummy', $value->file_name) }}" style="width: 100px;" />
                      @endif
                  @endforeach
                </td>
            </tr>
            <tr>
                <td>Speed Test</td>
                <td>
                  @foreach ($photo_speed AS $key => $value)
                      @if ($dummy->server == 1)
                          <img src="http://songturu2.mamikos.com/uploads/cache/data/dummy/{{ $value->file_name }}" style="width: 100px;" />
                      @else
                          <img src="{{ URL::to('uploads/cache/data/dummy', $value->file_name) }}" style="width: 100px;" />
                      @endif
                  @endforeach

                  @if (count($photo_speed) > 0)
                      <input type="text" style="margin-top: 20px; margin-bottom: 15px;" name="wifi_speed" class="form-control" placeholder="Masukkan angka aja yaa. Jangan huruf" />
                  @endif
                </td>
            </tr>

            <tr>
                <td>Kamar</td>
                <td>
                  @foreach ($photo_bedroom AS $key => $value)
                      @if ($dummy->server == 1)
                          <img src="http://songturu2.mamikos.com/uploads/cache/data/dummy/{{ $value->file_name }}" style="width: 100px;" />
                      @else
                          <img src="{{ URL::to('uploads/cache/data/dummy', $value->file_name) }}" style="width: 100px;" />
                      @endif
                  @endforeach
                </td>
            </tr>

            <tr>
                <td>Kamar mandi</td>
                <td>
                  @foreach ($photo_bathroom AS $key => $value)
                      @if ($dummy->server == 1)
                          <img src="http://songturu2.mamikos.com/uploads/cache/data/dummy/{{ $value->file_name }}" style="width: 100px;" />
                      @else
                          <img src="{{ URL::to('uploads/cache/data/dummy', $value->file_name) }}" style="width: 100px;" />
                      @endif
                  @endforeach
                </td>
            </tr>

            <tr>
                <td>Lainnya</td>
                <td>
                  @foreach ($photo_other AS $key => $value)
                      @if ($dummy->server == 1)
                          <img src="http://songturu2.mamikos.com/uploads/cache/data/dummy/{{ $value->file_name }}" style="width: 100px;" />
                      @else
                          <img src="{{ URL::to('uploads/cache/data/dummy', $value->file_name) }}" style="width: 100px;" />
                      @endif
                  @endforeach
                </td>
            </tr>

          </table>
        </div>
    </div>

    <div class="form-group bg-default">
      <label for="inputName" class="col-sm-2 control-label">Title</label>
      <div class="col-sm-10">
        <input type="hidden" name="apartment_project_id" value="{{ $rowDesigner->apartment_project_id }}">
        <input type="text" class="form-control" placeholder="Title"
        id="inputName" name="name"  value="{{ $rowDesigner->name }}">
        <div id="countTitle" style="font-weight:bold"></div>
      </div>
      </div>

      <div class="form-group bg-default">
      <label for="inputName" class="col-sm-2 control-label">Size</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" placeholder="Size"
        id="inputName" name="size" value="{{ $rowDesigner->size }}" >
        <div id="countTitle" style="font-weight:bold"></div>
      </div>
      </div>

        <div class="form-group bg-default">
        <label for="inputName" class="col-sm-2 control-label">Price Type</label>
        <div class="col-sm-10">

            <select name="price_type" class="form-control">
              <option value="idr" @if($price_type == false) selected="true" @endif >Rupiah</option>
              <option value="usd" @if($price_type == true) selected="true" @endif>Dollar</option>
            </select>
        </div>
        </div>

      <div class="form-group bg-default">
      <label for="inputName" class="col-sm-2 control-label">Price Daily</label>
      <div class="col-sm-10">
        <input type="number" class="form-control" placeholder="Price Daily"
        id="inputName" name="price_daily" value="{{ $pricing['price_daily'] }}" >
        <div id="countTitle" style="font-weight:bold"></div>
      </div>
      </div>

      <div class="form-group bg-default">
      <label for="inputName" class="col-sm-2 control-label">Price Weekly</label>
      <div class="col-sm-10">
        <input type="number" class="form-control" placeholder="Price Weekly"
        id="inputName" name="price_weekly" value="{{ $pricing['price_weekly'] }}" >
        <div id="countTitle" style="font-weight:bold"></div>
      </div>
      </div>

      <div class="form-group bg-default">
      <label for="inputName" class="col-sm-2 control-label">Price Monthly</label>
      <div class="col-sm-10">
        <input type="number" class="form-control" placeholder="Price Monthly"
        id="inputName" name="price_monthly" value="{{ $pricing['price_monthly'] }}" >
        <div id="countTitle" style="font-weight:bold"></div>
      </div>
      </div>

      <div class="form-group bg-default">
      <label for="inputName" class="col-sm-2 control-label">Price Yearly</label>
      <div class="col-sm-10">
        <input type="number" class="form-control" placeholder="Price Yearly"
        id="inputName" name="price_yearly" value="{{ $pricing['price_yearly'] }}" >
        <div id="countTitle" style="font-weight:bold"></div>
      </div>
      </div>

      <div class="form-group bg-default">
      <label for="inputName" class="col-sm-2 control-label">Address</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" placeholder="Address"
        id="inputName" name="address" value="{{ $rowDesigner->address }}" >
        <div id="countTitle" style="font-weight:bold"></div>
      </div>
      </div>

      <div class="form-group bg-default">
      <label for="inputName" class="col-sm-2 control-label">Floor</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" placeholder="Floor"
        id="inputName" name="floor" value="{{ $rowDesigner->floor }}">
        <div id="countTitle" style="font-weight:bold"></div>
      </div>
      </div>

      <div class="form-group bg-default">
      <label for="inputName" class="col-sm-2 control-label">Longitude</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" placeholder="Longitude"
        id="inputLongitude" name="longitude" value="{{ $rowDesigner->longitude }}">
        <div id="countTitle" style="font-weight:bold"></div>
      </div>
      </div>

      <div class="form-group bg-default">
      <label for="inputName" class="col-sm-2 control-label">Latitude</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" placeholder="Latitude"
        id="inputLatitude" name="latitude" value="{{ $rowDesigner->latitude }}">
        <div id="countTitle" style="font-weight:bold"></div>
      </div>
      </div>

      <div class="form-group bg-default">
      <label for="inputName" class="col-sm-2 control-label">Gender</label>
      <div class="col-sm-10">
        <select class="form-control" id="inputSelectType1" name="gender" tabindex="2" data-placeholder="Room Type" >
          <option value=0 @if($rowDesigner->gender == 0) selected @endif>Umum</option>
          <option value=1 @if($rowDesigner->gender == 1) selected @endif>Cowok</option>
          <option value=2 @if($rowDesigner->gender == 2) selected @endif>Cewek</option>
        </select>
      </div>
      </div>

      <div class="form-group bg-default">
      <label for="inputName" class="col-sm-2 control-label">Indexed</label>
      <div class="col-sm-10">
        <div class="checkbox">
          <label>
            <input type="checkbox" @if($rowDesigner->is_indexed == 1) checked="checked" @endif name="indexed">
            Indexed
          </label>
        </div>
      </div>
      </div>

      <div class="form-group bg-default">
      <div class="col-sm-10">
	  </div>
     </div>


	<div class="form-group bg-default">
	  <label for="inputGeoName" class="col-sm-2 control-label">Input Coordinate</label>
	  <div class="col-sm-10">
		<input type="text" class="form-control" placeholder="Geo Name"
		  id="inputGeoName" name="geo_name">
	  </div>
	  <div class="col-sm-10 col-sm-offset-2">
		<div style="background: #EEE; width: 100%; height: 300px;">
		  <div id="map-canvas"></div>
		</div>
	  </div>
	  <div class="col-sm-10 col-sm-offset-2">
		  <div class="input-group">
			<div class="input-group-addon">Coordinate (latitude, longitude)</div>
			<input type="text" class="form-control" placeholder="Latitude, Longitude"
			id="inputCoordinate" name="coordinate" value="{{ $rowDesigner->coordinate }}">
		  </div>
	  </div>
	</div>

    <div class="row">
    <div class="col-md-6">
    <div class="form-group bg-default">
        <label class="col-sm-2 control-label">Subdistrict</label>
        <div class="col-sm-10">
        <input class="form-control" rows="3" id="inputSubdistrict" name="area_subdistrict" placeholder="Subdistrict" value="{{ $rowDesigner->area_subdistrict }}">
        </div>
    </div>

    <div class="form-group bg-default">
        <label class="col-sm-2 control-label">City/Regency</label>
        <div class="col-sm-10">
        <input type="text" id="inputAreaCity" class="form-control input-city-regency" placeholder="Kota" name="area_city" value="{{ $rowDesigner->area_city }}" rows="3" data-toggle="dropdown">
        </div>
    </div>

    <div class="form-group bg-default">
        <label class="col-sm-2 control-label">Area Big</label>
        <div class="col-sm-10">
        <input class="form-control" rows="3" name="area_big" placeholder="Area Big" value="{{ $rowDesigner->area_big }}">
        </div>
    </div>
  </div>

  <div class="col-md-6">
    <div class="form-group bg-default">
        <label class="col-sm-2 control-label">Subdistrict 2</label>
        <div class="col-sm-10">
        <input type="hidden" value="{{ $dummy->latitude }}" name="latitude_2">
        <input class="form-control" rows="3" id="inputSubdistrict_two" name="area_subdistrict_2" placeholder="Subdistrict">
        </div>
    </div>

    <div class="form-group bg-default">
        <label class="col-sm-2 control-label">City/Regency 2</label>
        <div class="col-sm-10">
        <input type="hidden" value="{{ $dummy->longitude }}" name="longitude_2">
        <input type="text" id="inputAreaCity_two" class="form-control input-city-regency" placeholder="Kota" name="area_city_2" rows="3" data-toggle="dropdown">
        </div>
    </div>

  </div>
    </div>

    <div class="row" style="background: #ff0000;">
      <div class="col-md-12">
        <div class="form-group">
            <label class="col-sm-2 control-label" style="color: #fff;">Gunakan Lokasi dari agen ?</label>
            <div class="col-sm-10">
                <select class="form-control" name="use_loc_agent">
                    <option value="1">Yaaaaa</option>
                    <option value="0">Nggak</option>
                </select>
            </div>
        </div>

      </div>
    </div>


    <div class="form-group bg-default">
        <label class="col-sm-2 control-label">Penunjuk Arah</label>
        <div class="col-sm-10">
            <input class="form-control" rows="3" name="guide" placeholder="Petunjuk Arah" value="{{ $rowDesigner->guide }}">
        </div>
    </div>

  <div class="form-group bg-info divider">
      <div class="col-sm-12">
          Contact
      </div>
    </div>

    <div class="form-group bg-default">
      <label class="col-sm-2 control-label">Nama Pemilik</label>
      <div class="col-sm-10">
        <input class="form-control" rows="3" name="owner_name" placeholder="Nama Pemilik" value="{{ $rowDesigner->owner_name }}">
      </div>
    </div>

    <div class="form-group bg-default">
      <label class="col-sm-2 control-label">No. HP Pemilik</label>
      <div class="col-sm-10">
        <input class="form-control" rows="3" id="owner_phone" name="owner_phone" placeholder="No. HP Pemilik" value="{{ $rowDesigner->owner_phone }}">
        <small class="help-block">Lebih dari 1, pisahkan dengan koma</small>
        <br/>
        <!--<p>Telp hanya untuk kalian yg punya MamiCall (SquarepantApp)</p>-->
        <input type="hidden" id="id_id" value="{{ $rowDesigner->id }}">
        <input type="hidden" id="kos_name" value="{{ $rowDesigner->name }}">
        <!--<input type="button" style="width: 100%; text-align: center;" class="btn btn-danger" value="Telp Sekarang"  />-->
        <input type="hidden" id="user_login" value="{{ Auth::user()->email }}">
      </div>
    </div>

    <div class="form-group bg-default">
      <label class="col-sm-2 control-label">Nama Manajer</label>
      <div class="col-sm-10">
        <input class="form-control" rows="3" name="manager_name" placeholder="Nama Manajer" value="{{ $rowDesigner->manager_name }}">
      </div>
    </div>

    <div class="form-group bg-default">
      <label class="col-sm-2 control-label">No. HP Manajer</label>
      <div class="col-sm-10">
        <input class="form-control" rows="3" name="manager_phone" id="pengelola_phone" placeholder="No. HP Manajer" value="{{ $rowDesigner->manager_phone }}">
        <small class="help-block">Lebih dari 1, pisahkan dengan koma</small>
      </div>
    </div>

    <div class="form-group bg-default">
      <label class="col-sm-2 control-label">No. Telpon Kantor</label>
      <div class="col-sm-10">
        <input class="form-control" rows="3" name="office_phone" placeholder="No. Telpon Kantor" disabled="disabled" value="{{ $rowDesigner->office_phone }}">
        <small class="help-block">Lebih dari 1, pisahkan dengan koma</small>
      </div>
    </div>

    <div class="form-group bg-default">
      <label class="col-sm-2 control-label">Nama Agen</label>
      <div class="col-sm-10">
        <input class="form-control" name="agent_name" placeholder="Nama Agen" value="{{ $rowDesigner->agent_name }}">
      </div>
    </div>

    <div class="form-group bg-default">
      <label class="col-sm-2 control-label">Nomor Agen</label>
      <div class="col-sm-10">
        <input class="form-control" name="agent_phone" placeholder="Nomor Agen" value="{{ $rowDesigner->agent_phone }}">
      </div>
    </div>

    <div class="form-group bg-default">
      <label class="col-sm-2 control-label">Email Agen</label>
      <div class="col-sm-10">
        <input class="form-control" name="agent_email" placeholder="Email Agen" value="{{ $rowDesigner->agent_email }}">
      </div>
    </div>

    <div class="form-group bg-default">
      <label class="col-sm-2 control-label">Verifikator</label>
      <div class="col-sm-10">
        <input class="form-control" name="verificator" placeholder="Nama Verifikator" value="{{ $rowDesigner->verificator }}">
      </div>
    </div>

	<div class="form-group bg-info divider">
	  <div class="col-sm-12">
		  Information
	  </div>
	</div>

      <!-- select -->
      <div class="form-group bg-default">
        <label for="inputConcern" class="col-sm-2 control-label">Tags</label>
        <div class="col-sm-10">
          <select class="form-control chosen-select" multiple id="inputConcern" name="concern_ids[]" tabindex="2">
            @foreach ($rowsConcern as $rowConcern)
            <option value="{{ $rowConcern->id }}"
              {{ $rowConcern->selected }}>{{ $rowConcern->name }}</option>
              @endforeach
            </select>
        </div>
          @if (Request::is('admin/*'))
        <div class="col-sm-offset-2 col-sm-10" style="margin-top: 10px;">
            <a href="{{ URL::route('admin.tag.create') }}" type="button"
            class="btn btn-default btn-sm">
            <span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;Insert New Tag
          </a>
        </div>
        @endif
      </div>

          <div class="form-group bg-default">
            <label for="inputDescription" class="col-sm-2 control-label">Fasilitas Kamar lainnya</label>
            <div class="col-sm-10">
              <input class="form-control" rows="3" id="inputDescription" name="fac_room_other" placeholder="Fasilitas" value="{{ $rowDesigner->fac_room_other }}" maxlength="499">
              <!-- <div id="countDesc" style="font-weight:bold"></div> -->
            </div>
          </div>

          <div class="form-group bg-default">
            <label for="inputDescription" class="col-sm-2 control-label">Fasilitas Kamar Mandi lainnya</label>
            <div class="col-sm-10">
              <input class="form-control" rows="3" id="inputDescription" name="fac_bath_other" placeholder="Fasilitas" value="{{ $rowDesigner->fac_bath_other }}" maxlength="499">
              <!-- <div id="countDesc" style="font-weight:bold"></div> -->
            </div>
          </div>

          <div class="form-group bg-default">
            <label for="inputDescription" class="col-sm-2 control-label">Fasilitas Bersama lainnya</label>
            <div class="col-sm-10">
              <input class="form-control" rows="3" id="inputDescription" name="fac_share_other" placeholder="fasilitas" value="{{ $rowDesigner->fac_share_other }}" maxlength="499">
              <!-- <div id="countDesc" style="font-weight:bold"></div> -->
            </div>
          </div>

          <div class="form-group bg-default">
            <label for="inputDescription" class="col-sm-2 control-label">Fasilitas Dekat kos lainnya</label>
            <div class="col-sm-10">
              <input class="form-control" rows="3" id="inputDescription" name="fac_near_other" placeholder="fasilitas" value="{{ $rowDesigner->fac_near_other }}" maxlength="499">
              <!-- <div id="countDesc" style="font-weight:bold"></div> -->
            </div>
          </div>

          <div class="form-group bg-default">
            <label for="inputDescription" class="col-sm-2 control-label">Keterangan lainnya</label>
            <div class="col-sm-10">
              <input class="form-control" rows="3" id="inputDescription" name="remarks" placeholder="keterangan" value="{{ $rowDesigner->remarks }}">
              <!-- <div id="countDesc" style="font-weight:bold"></div> -->
            </div>
          </div>

          <div class="form-group bg-default">
            <label for="inputDescription" class="col-sm-2 control-label">Keterangan Harga Lain</label>
            <div class="col-sm-10">
              <input class="form-control" rows="3" id="inputPriceRemark" name="price_remark" placeholder="Keterangan Harga Lain" value="{{ $rowDesigner->price_remark }}">
            </div>
          </div>

          <div class="form-group bg-default">
            <label class="col-sm-2 control-label">Cacah Kamar</label>
            <div class="col-sm-10">
                <input class="form-control" id="inputRoomCount" name="room_count" placeholder="Total Kamar Yang Ada" value="{{ $rowDesigner->room_count ? : 0 }}">
            </div>
          </div>

          <div class="form-group bg-default">
              <label class="col-sm-2 control-label">Availability</label>
              <div class="col-sm-10">
                  <input type="number" name="room_available" class="form-control" value="{{ $rowDesigner->room_available }}">
              </div>
          </div>

          <div class="form-group bg-default" style="background: #ff0000; color: #ffffff;">
            <label for="available_from_agent" class="col-sm-2 control-label">Kamar yg tersedia dr agen</label>
            <div class="col-sm-10">
              <input class="form-control" id="available_from_agent" value="{{ $dummy->room_available }}" name="room_available_agent" />
              <p>Baca baik2</p>
            </div>

            <label for="pilihpilih" class="col-sm-2 control-label">Gunakan jumlah kamar kosong dr agen ?</label>
            <div class="col-sm-10">
              <select name="room_available_agent_status" class="form-control" id="pilihpilih">
                  <option value="1">Ya</option>
                  <option value="0">Nggak</option>
              </select>
            </div>

          </div>

          <div class="form-group bg-default">
            <label for="inputDescriptionMother" class="col-sm-2 control-label">Description</label>
            <div class="col-sm-10">
              <textarea class="form-control" id="inputDescriptionMother" name="description">{{ $rowDesigner->description }}</textarea>
              <div id="countDesc" style="font-weight:bold"></div>
            </div>
          </div>

          <div class="form-group bg-default" style="background: #ff0000; color: #ffffff;">
            <label for="inputAdminRemark" class="col-sm-2 control-label">Deskripsi dari agen</label>
            <div class="col-sm-10">
              <textarea class="form-control" name="description_agent">{{ $dummy->description }}</textarea>
              <p>Baca baik2</p>
            </div>
            <label for="inputAdminRemark" class="col-sm-2 control-label">Pake dari agen ?</label>
            <div class="col-sm-10">
              <select name="description_agent_status" class="form-control">
                  <option value="0">Nggak</option>
                  <option value="1">Ya</option>
              </select>
            </div>

          </div>

          <div class="form-group bg-default">
            <label for="inputDescription" class="col-sm-2 control-label">Booking</label>
            <div class="col-sm-10">
              <select class="form-control" name="is_booking">
                @if ($rowDesigner->is_booking == 1)
                    <option value="1">Yes</option>
                    <option value="0">No</option>
                @else
                    <option value="0">No</option>
                    <option value="1">Yes</option>
                @endif
              </select>
              <div id="countDesc" style="font-weight:bold"></div>
            </div>
          </div>

          <div class="form-group bg-default">
            <label for="inputAdminRemark" class="col-sm-2 control-label">Youtube Id</label>
            <div class="col-sm-10">
              <input class="form-control" rows="3" id="inputAdminRemark" name="youtube_id" placeholder="Youtube Id" value="{{ $rowDesigner->youtube_id }}">

              ex : https://www.youtube.com/watch?v=<span style="color: #ff0000">Youtube_ID</span>
            </div>
          </div>

          <div class="form-group bg-default">
            <label for="inputAdminRemark" class="col-sm-2 control-label">Status Penginput</label>
            <div class="col-sm-10">
             <select class="form-control" name="agent_status">

                @foreach ($status_agent as $agent)
                    @if ($rowDesigner->agent_status == $agent)
                      <option value="{{ $agent }}" selected="selected">{{ $agent }}</option>
                    @else
                      <option value="{{ $agent }}">{{ $agent }}</option>
                    @endif
                @endforeach

             </select>
            </div>
          </div>

          <div class="form-group bg-default" style="background: #ff0000; color: #ffffff;">
            <label for="inputAdminRemark" class="col-sm-2 control-label">Catatan agen</label>
            <div class="col-sm-10">
              <textarea class="form-control" name="agent_note">{{ $dummy->agent_note }}</textarea>
              <p>Mengapa ditolak atau gmn terserah.</p>
            </div>
            <label for="inputAdminRemark" class="col-sm-2 control-label">Tolak / Nggak</label>
            <div class="col-sm-10">
              <select name="is_reject" class="form-control">
                  <option value="0">Nggak</option>
                  <option value="1">Ya</option>
              </select>
            </div>
            <p>....</p>
            <label for="inputDescriptionMother" class="col-sm-2 control-label">Verificator</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" name="verificator" value="{{ $dummy->verificator }}" placeholder="Ex: Eriana"/>
              <div id="countDesc" style="font-weight:bold"></div>
            </div>

          </div>


        <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-primary" name="act" value="edit">Save</button>

            @if (Request::is('admin/*'))
            @if (!isset($data_all['ref']))
            <button type="reset" class="btn btn-default"
            onclick="$('#formInsertDesigner').bootstrapValidator('resetForm', true);">Reset</button>
            @endif
            @endif
        </div>
        </div>

</form>

<div style="position:fixed; bottom:0px; background: #000; padding:0px;" class="col-md-12">
  <input type="button" class="col-md-3 btn btn-danger" id="squarepantsPengelola" style="border-radius: 0;" value="Telp Pengelola" />
  <input type="button" class="col-md-3 btn btn-warning" id="squarepantsOwner" style="border-radius: 0;" value="Telp Pemilik"/>
  <a href="{{ URL::to('admin/giant/reject', $dummy->id) }}" class="col-md-4 btn btn-primary">Tolak</a>
</div>

</div><!-- /.box -->
@stop
@section('style')
{{ HTML::style('assets/vendor/jquery-chosen/chosen.css') }}
{{ HTML::style('assets/vendor/jquery-chosen-custom/chosen.css') }}
{{ HTML::style('assets/vendor/jquery-file-upload/css/jquery.fileupload.css') }}
    <link rel="stylesheet" href="/assets/vendor/summernote/summernote.css?v=05042018">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/dropzone.css">

<style>
  #chosenForm .chosen-choices {
    border: 1px solid #ccc;
    border-radius: 4px;
    min-height: 34px;
    padding: 6px 12px;
  }
  #chosenForm .form-control-feedback {
    /* To make the feedback icon visible */
    z-index: 100;
  }
</style>
<link rel="stylesheet" href="{{ mix_url('dist/vendor/leaflet/leaflet.css') }}">
@stop
@section('script')
<!-- page script -->
{{ HTML::script('assets/vendor/jquery-chosen/chosen.jquery.js') }}

<script src="/assets/vendor/summernote/summernote.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/dropzone.js"></script>

<script type="text/javascript">
        $('#inputDescriptionMother').summernote({
            height: 300,
            toolbar: [
                ['style', ['style', 'bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['insert', ['link', 'table', 'hr']],
                ['misc', ['codeview']]
            ]
        });

</script>

<script type="text/javascript">
  var config = {
    '.chosen-select'           : {width: '100%'}
  };
  for (var selector in config) {
    $(selector).chosen(config[selector]);
  }
</script>

<script> //count character
      function countCharTitle(val,maxlength) //title card
      {
        var len = val.value.length;
        if (len > maxlength) {
          val.value = val.value.substring(0, maxlength);
        } else {
          $('#countTitle').text(maxlength - len);
        }
      };

      function countCharDesc(val,maxlength)  //Information Description
      {
        var len = val.value.length;
        if (len > maxlength) {
          val.value = val.value.substring(0, maxlength);
        } else {
          $('#countDesc').text(maxlength - len);
        }
      };

      function countCharText(val,maxlength,id) // Text Card
      {
        var len = val.value.length;
        if (len > maxlength) {
          val.value = val.value.substring(0, maxlength);
        } else {
          $('#countText'+id).text(maxlength - len);
        }
      };

      function countCharTextAfterAdd(val,maxlength,id) // Text Card After Adding Card
      {
        var len = val.value.length;
        if (len > maxlength) {
          val.value = val.value.substring(0, maxlength);
        } else {
          $('#countTextAfterAdd'+id).text(maxlength - len);
        }
      };
    </script>
    <!-- Holder for Upload File Thumbnail -->
    {{ HTML::script('assets/vendor/holder/holder.js') }}
    <!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
    {{ HTML::script('assets/vendor/jquery-file-upload/js/vendor/jquery.ui.widget.js') }}
    <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
    {{ HTML::script('assets/vendor/jquery-file-upload/js/jquery.iframe-transport.js') }}
    <!-- The basic File Upload plugin -->
    {{ HTML::script('assets/vendor/jquery-file-upload/js/jquery.fileupload.js') }}
    <!-- The File Upload processing plugin -->
    {{ HTML::script('assets/vendor/jquery-file-upload/js/jquery.fileupload-process.js') }}
    <!-- The File Upload validation plugin -->
    {{ HTML::script('assets/vendor/jquery-file-upload/js/jquery.fileupload-validate.js') }}
    <!-- File Upload Main -->
    {{ HTML::script('assets/vendor/file-upload/reguler.js') }}
    <script>
      $(function () {
    // Change this to the location of your server-side upload handler
    @if (Request::is('admin/*'))
    var url = "{{ url('admin/media') }}";
    @else
    var url = "{{ url('user/media') }}";
    @endif

    myFileUpload($('#wrapper-cover-upload'), url, 'style_photo');
    myFileUpload($('#wrapper-360-upload'), url, 'round_style_photo');
    myFileUpload($('#wrapper-thumbnail-upload'), url, 'style_photo');
    myFileUpload($('#cardUpload1'), url, 'style_photo');
  });

    </script>

    <!-- Javascript Card -->
    <script type="text/javascript">
     $("#inputSelectType1").change(function(){
      var select = $("#inputSelectType1 option:selected").val();
      switch(select){
        case "video":
        $('#cardInputVideoUrl1').show();
        $('#cardInputImage1').hide();
        $('#cardInputSubType1').hide();
        break;

        case "image":
        $('#cardInputVideoUrl1').hide();
        $('#cardInputImage1').show();
        $('#cardInputSubType1').hide();
        break;

        case "text":
        $('#cardInputVideoUrl1').hide();
        $('#cardInputImage1').hide();
        $('#cardInputSubType1').hide();
        break;
        case "quiz":
        case "vote":
        $('#cardInputSubType1').show();
        break;
      }
    });
     <?php
     if (! isset($index)) {
      $index = 0;
     }
     ?>

     /*add new card*/
     $("#addCard").click(function(){
      var validator= 0;
      var type    = 'type';
      var video   = 'video';
      var photo   = 'photo_id';
      var url_ori = 'url_ori';
      var source  = 'source';
      var description = 'description';
      var intId = parseInt($("#formCards div").length/26 + 1,10);
      var index = intId - 1;
      var html  = '<div class="card-item"><div class="form-group bg-info divider"><div class="col-sm-12">Cards '+intId+'</div></div>';
      html += '<div class="form-group bg-default"><label for="inputSelect" class="col-sm-2 control-label">Type</label><div class="col-sm-2">';

      html += '<select class="form-control" id="inputSelectType'+intId+'" name="cards['+index+']['+type+']" tabindex="2" data-placeholder="Select Card Type">';
      html += '<option value="image">Image/GIF</option>';

        //hide temporary
        // html += '<option value="text">Text</option><option value="video">Video</option>';
        html += '<option value="video">Video</option><option value="quiz">Quiz</option><option value="vote">Vote</option>';
        html += '</select></div></div>';

        // Video
        html += '<div class="form-group bg-default" id="cardInputVideoUrl'+intId+'" hidden><label for="inputUrl" class="col-sm-2 control-label">Video URL</label>';
        html += '<div class="col-sm-10"><input type="text" class="form-control" placeholder="Video URL" id="cardInputVideoUrl'+intId+'" name="cards['+index+']['+video+']" value=""></div></div>';
        // For maintain count of div add this
        html += '<div class="form-group bg-default" id="cardInputSubType'+intId+'" hidden><label for="inputSelectSubType'+intId+'" class="col-sm-2 control-label">Sub Type</label><div class="col-sm-2">';
        html += '<select class="form-control" id="inputSelectSubType'+intId+'" name="cards['+index+'][sub_type]" tabindex="2" data-placeholder="Select Card Sub Type">';
        html += '<option value="text">Text</option><option value="gif">GIF</option><option value="image" selected>Image</option>';
        html += '</select></div></div>';

        html += '<div class="form-group bg-default" id="cardInputImage'+intId+'" ><label for="fileupload" class="col-sm-2 control-label">Image/GIF</label><div class="col-sm-10">';
        html += '<div id="cardUpload'+intId+'" class="media"><a class="pull-left thumbnail" id="wrapper-img" ><img style="background: #EEE; width: 120px; height: 160px;" class="media-object" width="120px" height="160px" src="" data-src="holder.js/120x160/thumbnail" alt=""></a><div class="media-body">';
        html += '<input id="fileupload1" class="form-control" type="text" name="media1" placeholder="URL Photo"><input id="sourceImg" type="hidden" name="cards['+index+']['+url_ori+']"><br><span class="btn btn-info btn-sm fileinput-button" id="buttonUpload'+intId+'"><i class="glyphicon glyphicon-plus"></i><span>&nbsp; Add Photo from URL </span></span>&nbsp;';
        html += '<span class="btn btn-primary btn-sm fileinput-button"><i class="glyphicon glyphicon-plus"></i><span>&nbsp; Add Photo.. </span><input id="fileupload" type="file" name="media"><input id="photo_id" type="hidden" name="cards['+index+']['+photo+']" value=""></span><span>';
        html += '<span class="btn btn-danger btn-sm fileinput-button btn-delete" id="buttonDelete{{$index+1}}"><i class="glyphicon glyphicon-plus"></i><span>&nbsp; Delete Photo</span></span></span>';
        html += '<br><br><div id="progress" class="progress"><div class="progress-bar"></div></div>';
        html += '<div class="col-sm-6"><div id="original-file-info" class="row"></div></div>';
        html += '<div class="col-sm-6"><div id="uploaded-file-info" class="row"></div></div><br>';
        html += '<div id="files" class="files"></div></div></div></div></div>';
        html += '<div class="form-group bg-default"><label for="" class="col-sm-2 control-label">Text</label><div class="col-sm-10">';
        html += '<textarea class="form-control" rows="3" id="" name="cards['+index+']['+description+']" maxlength="200" onkeyup="countCharTextAfterAdd(this,200,'+index+')" placeholder="Card Description"></textarea><div style="height:20px;font-weight:bold" id="countTextAfterAdd'+index+'"></div></div>';
        html += '<div class="form-group bg-default"><label for="cardInputSource" class="col-sm-2 control-label">Source</label><div class="col-sm-10"><input type="text" class="form-control" rows="3" id="cardInputSource" name="cards['+index+']['+source+']" placeholder="Source Content"></input></div></div>';
        html += '<div class="form-group bg-default"><label for="cardInputOrdering" class="col-sm-2 control-label">Order</label><div class="col-sm-10"><input type="text" class="form-control" rows="3" id="cardInputOrdering" name="cards['+index+'][ordering]" placeholder="kasih order (10, 20, 30, 40, 50 ....)"></input></div></div></div>';

        $("#formCards").append(html);

        /*Plih input berdasarkan select*/
        console.log("#inputSelectType"+intId);
        $("#inputSelectType"+intId).change(function(){
          var select = $("#inputSelectType"+intId+" option:selected").val();
          switch(select){
            case "video":
            console.log("video"+intId);
            $('#cardInputVideoUrl'+intId).show();
            $('#cardInputImage'+intId).hide();
            $('#cardInputSubType' + intId).hide();
            break;

            case "image":
            $('#cardInputVideoUrl'+intId).hide();
            $('#cardInputImage'+intId).show();
            $('#cardInputSubType' + intId).hide();
            break;

            case "text":
            $('#cardInputVideoUrl'+intId).hide();
            $('#cardInputImage'+intId).hide();
            $('#cardInputSubType' + intId).hide();
            break;

            case "quiz":
            case "vote":
            $('#cardInputSubType1').show();
          }
        });

        //panggil upload file
        @if (Request::is('admin/*'))
        var url = "{{ url('admin/media') }}";
        @else
        var url = "{{ url('user/media') }}";
        @endif

        console.log('nyampe');
        myFileUpload($('#cardUpload'+intId), url, 'style_photo', validator);

        <?php $login_type = Request::is('admin/*')?'admin':'user'; ?>

        /*upload foto from url*/
        $("#buttonUpload"+intId).click(function(){
          var wrapper = $('#cardUpload'+intId);
          var mediaUrl= wrapper.find('#fileupload1').val();
          var url     = "{{ url($login_type . '/mediaUrl?t=style_photo&url=') }}"+mediaUrl;

          uploadFromUrl(wrapper, url, mediaUrl, validator);
        });
      });

$("#removeCard").click(function(){
  var intId = parseInt($("#formCards div").length/26 + 1,10);
  console.log(intId);
  if(intId != 0){
    $('#formCards div').slice(-26).remove();
  }
});

$("#buttonCover").click(function(){
  var validator= 1;
  var wrapper = $('#wrapper-cover-upload');
  var mediaUrl= wrapper.find('#fileupload1').val();
  var url     = "{{ url($login_type . '/mediaUrl?t=user_photo&url=') }}"+mediaUrl;

  window.alert("upload");

  uploadFromUrl(wrapper, url, mediaUrl, validator);
});

$("#buttonThumbnail").click(function(){
  var validator= 1;
  var wrapper = $('#wrapper-thumbnail-upload');
  var mediaUrl= wrapper.find('#fileupload1').val();
  var url     = "{{ url($login_type . '/mediaUrl?t=user_photo&url=') }}"+mediaUrl;

  uploadFromUrl(wrapper, url, mediaUrl, validator);
});

</script>

<script src="https://maps.googleapis.com/maps/api/js?libraries=places&language=id&key={{ENV('MIX_GOOGLE_MAPS_JAVASCRIPT_API_KEY')}}"></script>

<script src="{{ mix_url('dist/vendor/leaflet/leaflet.js') }}"></script>

<script type="text/javascript">
    // leaflet starts here

    var inputGeoName = 'inputGeoName';
    var inputLatLngName = 'inputCoordinate';

    centerPos = {
            lat: parseFloat('{{ ($rowDesigner->latitude + $dummy->latitude) / 2 }}'),
            lng: parseFloat('{{ ($rowDesigner->longitude + $dummy->longitude) / 2 }}')
    }

    position_0 = {
        lat: parseFloat('{{ $rowDesigner->latitude }}'),
        lng: parseFloat('{{ $rowDesigner->longitude }}')
    }

    position_1 = {
        lat: parseFloat('{{ $dummy->latitude }}'),
        lng: parseFloat('{{ $dummy->longitude }}')
    }

    var map = L.map('map-canvas', {
        // Set latitude and longitude of the map center (required)
        center: centerPos,
        // Set the initial zoom level, values 0-18, where 0 is most zoomed-out (required)
        zoom: 12
    });

    var tiles = new L.tileLayer("{{ config('services.osm.host') }}/hot/{z}/{x}/{y}.png").addTo(map);

    markerImage = '{{ asset('assets/icons/kiribawah.png') }}';
    markerImageRed = '{{ asset('assets/icons/red-marker-icon-2x-2.png') }}';
    var marker = [];

    marker[0] = new L.Marker(position_0, {
        draggable: true
    }).addTo(map);
    addMarkerListener(map, marker[0], 'BottomLeft');

    ///////
    marker[1] = new L.Marker(position_1, {
        draggable: true,
        icon: new L.icon({
            iconUrl: markerImageRed,
            iconSize: [30, 57],
            iconAnchor: [30, 57]
        })
    }).addTo(map);

    addMarkerListener(map, marker[1], 'TopRight');
    ////

    function addMarkerListener(map, marker, pos) {
        marker.on('dragend', function(evt) {
            var latlng = evt.target.getLatLng();

            var lat = latlng.lat;
            var lng = latlng.lng;

            $('#inputCoordinate' + pos).val(lat + ',' + lng);
        });
    }

    var autocomplete = new google.maps.places.Autocomplete(document.getElementById('inputGeoName'));
    autocomplete.setFields(['address_components', 'geometry']);

    var geocoder = new google.maps.Geocoder();

    // GET CITY POSITION 2
    var latlng_2 = new google.maps.LatLng({{ $dummy->latitude }}, {{ $dummy->longitude }});
    geocoder.geocode({'latLng': latlng_2}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[1]) {

                        //map.setCenter(latlng_2);
                        //marker.setPosition(latlng_2);

                        //$('#inputGeoName').val(results[1].formatted_address);
                        //$('#inputLatitude').val(lat);
                        //$('#inputLongitude').val(lng);

                        setDistrictAndCity_two(results[0]);
                    } else {
                        alert('No results found');
                    }
                } else {
                    alert('Geocoder failed due to: ' + status);
                }
    });
    // GET CITY POSITION 2

    marker[0].on('dragend', function(evt) {
        var latlng = evt.target.getLatLng();

        var lat = latlng.lat;
        var lng = latlng.lng;

        geocoder.geocode({'latLng': latlng}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[1]) {

                    map.setView(latlng);
                    // marker.setPosition(latlng);

                    $('#inputGeoName').val(results[1].formatted_address);
                    $('#inputLatitude').val(lat);
                    $('#inputLongitude').val(lng);
                    $('#inputCoordinate').val(lat + ',' + lng);

                    setDistrictAndCity(results[0]);
                } else {
                    alert('No results found');
                }
            } else {
                alert('Geocoder failed due to: ' + status);
            }
        });
    });

    function setDistrictAndCity(response) {
        subDistrict = '';
        city = '';
        response.address_components.forEach(function(component) {
            // console.log(component.types);
            if ($.inArray("administrative_area_level_3", component.types) != -1) {
                subDistrict = component.short_name;
            }

            if ($.inArray("administrative_area_level_2", component.types) != -1) {
                city = component.short_name;
            }

        });

        $('#inputSubdistrict').val(subDistrict);
        $('#inputAreaCity').val(city);
    }

    function setDistrictAndCity_two(response) {
        subDistrict = '';
        city = '';
        response.address_components.forEach(function(component) {
            // console.log(component.types);
            if ($.inArray("administrative_area_level_3", component.types) != -1) {
                subDistrict = component.short_name;
            }

            if ($.inArray("administrative_area_level_2", component.types) != -1) {
                city = component.short_name;
            }

        });

        $('#inputSubdistrict_two').val(subDistrict);
        $('#inputAreaCity_two').val(city);
    }

    autocomplete.addListener('place_changed', function() {
        marker.setOpacity(0);
        var place = autocomplete.getPlace();
        if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place.name + "'");
            return;
        }


        latlng = {
            lat: place.geometry.location.lat(),
            lng: place.geometry.location.lng()
        };

        map.setView(latlng);
        marker.setLatLng(latlng);
        marker.setOpacity(1);

        $('#inputLatitude').val(place.geometry.location.lat);
        $('#inputLongitude').val(place.geometry.location.lng);

        $('#inputCoordinate').val(latlng.lat + ',' + latlng.lng);

        setDistrictAndCity(place);
    });

    var latlng = new google.maps.LatLng(<?php echo $rowDesigner->latitude; ?>, <?php echo $rowDesigner->longitude; ?>);

    geocoder.geocode(
      {'latLng': latlng},
      function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
              if (results[0]) {
                  var add= results[0].formatted_address ;
                  var  value=add.split(",");

                  count=value.length;
                  country=value[count-1];
                  state=value[count-2];
                  city=value[count-3];
                  subDistrict=value[count-4];
                  $('#inputSubdistrict').val(subDistrict);
                  $('#inputAreaCity').val(city);
              } else  {
                  x.innerHTML = "address not found";
              }
          } else {
              x.innerHTML = "Geocoder failed due to: " + status;
          }
      }
  );
</script>

<script type="text/javascript">

  // var inputGeoName = 'inputGeoName';
  // var inputLatLngName = 'inputCoordinate';

  // centerPos = {
  //           lat: {{ !is_null($rowDesigner->latitude) ? $rowDesigner->latitude : '-7.7858485' }},
  //           lng: {{ !is_null($rowDesigner->longitude) ? $rowDesigner->longitude :  '110.3680087'}}
  //       }

  //       var map = new google.maps.Map(document.getElementById("map-canvas"),
  //           {
  //               center: centerPos,
  //               zoom:12
  //           }
  //       );

  //       var marker = new google.maps.Marker({
  //           position: centerPos,
  //           map: map,
  //           draggable: true,

  //       });

  //       var autocomplete = new google.maps.places.Autocomplete(document.getElementById('inputGeoName'));
  //       autocomplete.bindTo('bounds', map);

  //       var geocoder = new google.maps.Geocoder();

  //       google.maps.event.addListener(marker, 'dragend', function(evt) {
  //           var lat = evt.latLng.lat();
  //           var lng = evt.latLng.lng();

  //           var latlng = new google.maps.LatLng(lat, lng);

  //           geocoder.geocode({'latLng': latlng}, function(results, status) {
  //               if (status == google.maps.GeocoderStatus.OK) {
  //                   if (results[1]) {

  //                       map.setCenter(latlng);
  //                       marker.setPosition(latlng);

  //                       $('#inputGeoName').val(results[1].formatted_address);
  //                       $('#inputLatitude').val(lat);
  //                       $('#inputLongitude').val(lng);

  //                       setDistrictAndCity(results[0]);
  //                   } else {
  //                       alert('No results found');
  //                   }
  //               } else {
  //                   alert('Geocoder failed due to: ' + status);
  //               }
  //           });
  //       });


  //       autocomplete.addListener('place_changed', function() {
  //           marker.setVisible(false);
  //           var place = autocomplete.getPlace();
  //           if (!place.geometry) {
  //               // User entered the name of a Place that was not suggested and
  //               // pressed the Enter key, or the Place Details request failed.
  //               window.alert("No details available for input: '" + place.name + "'");
  //               return;
  //           }

  //           map.setCenter(place.geometry.location);
  //           marker.setPosition(place.geometry.location);
  //           marker.setVisible(true);

  //           $('#inputLatitude').val(place.geometry.location.lat);
  //           $('#inputLongitude').val(place.geometry.location.lng);

  //           setDistrictAndCity(place);
  //       });

  //       function setDistrictAndCity(response) {
  //           subDistrict = '';
  //           city = '';
  //           response.address_components.forEach(function(component) {
  //               // console.log(component.types);
  //               if ($.inArray("administrative_area_level_3", component.types) != -1) {
  //                   subDistrict = component.short_name;
  //               }

  //               if ($.inArray("administrative_area_level_2", component.types) != -1) {
  //                   city = component.short_name;
  //               }

  //           });

  //           $('#inputSubdistrict').val(subDistrict);
  //           $('#inputAreaCity').val(city);
  //       }

  //          var geocoder;
  //           geocoder = new google.maps.Geocoder();
  //           var latlng = new google.maps.LatLng(<?php echo $rowDesigner->latitude; ?>, <?php echo $rowDesigner->longitude; ?>);

  //           geocoder.geocode(
  //               {'latLng': latlng},
  //               function(results, status) {
  //                   if (status == google.maps.GeocoderStatus.OK) {
  //                       if (results[0]) {
  //                           var add= results[0].formatted_address ;
  //                           var  value=add.split(",");
  //                           console.log(value)
  //                           count=value.length;
  //                           country=value[count-1];
  //                           state=value[count-2];
  //                           city=value[count-3];
  //                           subDistrict=value[count-4];
  //                           $('#inputSubdistrict').val(subDistrict);
  //                           $('#inputAreaCity').val(city);
  //                       } else  {
  //                           x.innerHTML = "address not found";
  //                       }
  //                   } else {
  //                       x.innerHTML = "Geocoder failed due to: " + status;
  //                   }
  //               }
  //           );

  $('#cards').on('click', '.btn-delete',function () {
    $(this).closest('.card-item').remove();
  });

</script>
<script type="text/javascript">

  @if (Session::get('laravelValidatorJSON') === null)
  myGlobal.laravelValidator = null;
  @else
  myGlobal.laravelValidator = {{ Session::get('laravelValidatorJSON') }};
  @endif

  $(function() {
    $formInsertDesigner = $('#formInsertDesigner');
    $form = $formInsertDesigner;

    var formStoreGroupRules = {
      name: {
        message: 'The title is not valid',
        validators: {
          notEmpty: {
            message: 'The title is required and cannot be empty'
          },
          stringLength: {
            min: 3,

            max: 120,

            message: 'The title must be more than 3 and less than 30 characters long'
          }
        }
      },
      cover_photo_id: {
        feedbackIcons: 'false',
        message: 'The photo is not valid',
        validators: {
          notEmpty: {
            message: 'The photo is required and cannot be empty'
          },
        }
      },
      gender: {
        message: 'The gender is not valid',
        validators: {
          notEmpty: {
            message: 'The gender is required and cannot be empty'
          },
        }
      },
      phone: {
        message: 'The phone is not valid',
        validators: {
          stringLength: {
            min: 5,
            max: 15,
            message: 'The phone must be more than 5 and less than 15 characters long'
          }
        }
      },
      'user_id': {
        feedbackIcons: 'false',
        message: 'The user is not valid',
        validators: {
          callback: {
            message: 'Please choose a User',
            callback: function(value, validator) {
                          // Get the selected options
                          var options = validator.getFieldElements('user_id').val();
                          return (true);
                        }
          }
        }
      },
      // 'description': {
      //   message: 'The description is not valid',
      //   validators: {
      //     notEmpty: {
      //       message: 'The descripton is required and cannot be empty'
      //     }
      //   }
      // },
      'address': {
        message: 'The address is not valid',
        validators: {
          notEmpty: {
            message: 'The address is required and cannot be empty'
          }
        }
      },
      coordinate: {
        message: 'The Coordinates is not valid',
        validators: {
          notEmpty: {
            message: 'The coordinate is required and cannot be empty'
          }
        }
      },
    };

    var formUpdateGroupRules = {
      name: {
        message: 'The title is not valid',
        validators: {
          notEmpty: {
            message: 'The title is required and cannot be empty'
          },
          stringLength: {
            min: 3,
            max: 120,
            message: 'The title must be more than 3 and less than 30 characters long'
          }
        }
      },
      cover_photo_id: {
        feedbackIcons: 'false',
        message: 'The photo is not valid',
        validators: {
          notEmpty: {
            message: 'The photo is required and cannot be empty'
          },
        }
      },
      gender: {
        message: 'The gender is not valid',
        validators: {
          notEmpty: {
            message: 'The gender is required and cannot be empty'
          },
        }
      },
      phone: {
        message: 'The phone is not valid',
        validators: {
          stringLength: {
            min: 5,
            max: 25,
            message: 'The phone must be more than 5 and less than 15 characters long'
          }
        }
      },
      'user_id': {
        feedbackIcons: 'false',
        message: 'The user is not valid',
        validators: {
          callback: {
            message: 'Please choose a User',
            callback: function(value, validator) {
              // Get the selected options
              var options = validator.getFieldElements('user_id').val();
              return (true);
            }
          }
        }
      },
      'concern_ids[]': {
        message: 'The user concern is not valid',
        validators: {
          callback: {
            message: 'Please choose at least one concern',
            callback: function(value, validator) {
              // Get the selected options
              var options = validator.getFieldElements('concern_ids[]').val();
              return (true);
            }
          }
        }
      },

      'address': {
        message: 'The address is not valid',
        validators: {
          notEmpty: {
            message: 'The address is not valid'
          }
        }
      },
      coordinate: {
        message: 'The Coordinates is not valid',
        validators: {
          notEmpty: {
            message: 'The coordinates address is not valid'
          }
        }
      },
      manager_phone: {
        message: 'The Phone is not valid',
        validators: {
          notEmpty: {
            message: 'The phone is required and cannot be empty'
          },
          stringLength: {
            min: 7,

            max: 26,

            message: 'The title must be more than 7 and less than 26 characters long'
          }
        }
      }
    };


    @if (Route::currentRouteName() === 'admin.stories.create')
      myGlobal.bootstrapValidatorDefaults.fields = formStoreGroupRules;

    @elseif (Route::currentRouteName() === 'admin.stories.edit')
      myGlobal.bootstrapValidatorDefaults.fields = formUpdateGroupRules;
    @endif

    $('#inputName').blur(function(e) {
      theName = $(this).val();
      $.ajax({
        url: '/admin/room/check-name?room={{$rowDesigner->id}}&name=' + theName,
        type: 'GET',
        dataType: 'json',
        success: function(data) {
          if(data.available == false) {
            alert('Nama kost sudah dipakai sebelumnya');
          }
        }
      });
    });

    myGlobal.bootstrapValidatorDefaults.excluded = ':disabled';

    $formInsertDesigner
      .find('[name="concern_ids"]')
        .chosen()
        .change(function(e) {
          $formInsertDesigner.bootstrapValidator('revalidateField', 'concern_ids[]');
         })
      .end()
      .find('[name="user_id"]')
        .chosen()
        .change(function(e) {
          $formInsertDesigner.bootstrapValidator('revalidateField', 'user_id');
        })
      .end()
      .bootstrapValidator(myGlobal.bootstrapValidatorDefaults);

      updateValidateStatus(
        $formInsertDesigner.data('bootstrapValidator'),
        myGlobal.laravelValidator);
      });
</script>
<script>
   function clickSuggestion(idx) {
        key = idx;
        val = $('.suggestion-item-' + idx).text();
        el = $('.suggestion-item-' + idx);

        $(el).parent().parent().prev('.input-city-regency').val(val);
    };

    var callSuggestionCity = function(e) {
            $.ajax({
                type: 'POST',
                url: '/admin/area-big-mapper/suggestion-city-regency',
                data: {
                    'params': e.target.value
                },
                success: function(data) {
                    $(e.target).next('.dropdown-menu').remove();

                    listOption = '<ul class="dropdown-menu suggestion-list" aria-labelledby="dLabel">'
                    for(idx in data.landings) {
                        listOption += '<li><a href="#" class="suggestion-item suggestion-item-' + idx + '" data-key="' + idx + '" onclick="clickSuggestion(' + idx + ')">' + data.landings[idx] + '</a></li>';
                    }

                    listOption += '</ul>';

                    $(listOption).insertAfter($(e.target));

                    $('.suggestion-list').dropdown();
                }

            })
        };

    var delay = (function(){
          var timer = 0;
          return function(callback, ms){
            clearTimeout (timer);
            timer = setTimeout(callback, ms);
          };
        })();

    $(document).on('keyup', '.input-city-regency', function(e) {
            delay(function(){
              callSuggestionCity(e)
            }, 1000 );
        });



  $('#squarepantsOwner').click(function(e) {
      phone = $("#owner_phone").val();
      id_id = $("#id_id").val();
      email = $("#user_login").val();
      kos_name = $("#kos_name").val();

      //$("#loading-phone").append("Loading ................ ");
      $.ajax({
        url: '/admin/squarepants/notify?phone='+phone+"&email="+email+"&id="+id_id+"&kos_name="+kos_name,
        type: 'GET',
        dataType: 'json',
        success: function(data) {
           // console.log(data)
          //$("#loading-phone").empty();
          if(data.status == false) {
            alert("Gagal ngirim nomer ke hp");
            //$("#loading-phone").append("<div class='alert alert-danger' style='width: 100%;'>Nomer hp tidak sesuai format.</div>");
          } else {
            alert("Berhasil ngirim nomer ke hp");
            //$("#loading-phone").append("<div class='alert alert-success' style='width: 100%;'>Cek hp anda.");
          }
        }
      });
    });

    $('#squarepantsPengelola').click(function(e) {
      phone = $("#pengelola_phone").val();
      id_id = $("#id_id").val();
      email = $("#user_login").val();
      kos_name = $("#kos_name").val();

      //$("#loading-phone").append("Loading ................ ");
      $.ajax({
        url: '/admin/squarepants/notify?phone='+phone+"&email="+email+"&id="+id_id+"&kos_name="+kos_name,
        type: 'GET',
        dataType: 'json',
        success: function(data) {
           // console.log(data)
          //$("#loading-phone").empty();
          if(data.status == false) {
            alert("Gagal ngirim nomer ke hp");
            //$("#loading-phone").append("<div class='alert alert-danger' style='width: 100%;'>Nomer hp tidak sesuai format.</div>");
          } else {
            alert("Berhasil ngirim nomer ke hp");
            //$("#loading-phone").append("<div class='alert alert-success' style='width: 100%;'>Cek hp anda.");
          }
        }
      });
    });

    Dropzone.options.Other = {
            paramName : "media",
            maxFilesize : 6,
            accepFiles: '.jpg, .jpeg, .png, .bmp',
            dictDefaultMessage : 'Upload other photos',
            addRemoveLinks: true,
            success : function (file, response) {
                if(response.media.id != 0){
                    $('#photo-other-wrapper').append('<input type="hidden" name="review_photo[]" value="' +response.media.id+ '" data-filename="' + file.name + '">');
                }
            },
            init: function() {
                this.on('removedfile', function(file) {
                    $('input[name="review_photo[]"]').each(function(id, el) {
                        if($(el).data('filename') == file.name) {
                            $(el).remove();
                        }
                    });
                });
            }
        }

        $('#Other').dropzone();
</script>

@stop
