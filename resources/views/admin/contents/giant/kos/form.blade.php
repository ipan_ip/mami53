@extends('admin.layouts.main')
@section('content')
<div class="box box-primary">
  <div class="box-header">
      <h3 class="box-title">{{ $boxTitle }}</h3>
  </div><!-- /.box-header -->
      <form action="{{ URL::to($action) }}" class="form-horizontal" method="POST">
      {{ csrf_field() }}
      <div class="box-body no-padding">
           
            <div class="form-group bg-default">
              <label for="inputName" class="col-sm-2 control-label">Unique code</label>
              <div class="col-sm-10">
                    <input type="text" class="form-control" name="unique_code" value="{{ $agent->unique_code }}" placeholder="Unique code" required />
              </div>
            </div>

            <div class="form-group bg-default">
              <label for="inputlatitude1" class="col-sm-2 control-label">Type</label>
              <div class="col-sm-10">
                    <select class="form-control" id="inputSelectType" name="type">
                        <option value="kos" @if ($agent->type == 'kos') selected="true" @endif>Kos</option>
                        <option value="apartment" @if ($agent->type == 'apartment') selected="true" @endif>Apartment</option>
                    </select>
              </div>
            </div>

            <div class="form-group bg-default">
              <label for="inputName" class="col-sm-2 control-label">Agent name</label>
              <div class="col-sm-10">
                    <input type="text" class="form-control" name="name" value="{{ $agent->name }}" placeholder="Agent name" required />
              </div>
            </div>

            <div class="form-group bg-default">
              <label for="inputName" class="col-sm-2 control-label">Agent phone</label>
              <div class="col-sm-10">
                    <input type="text" class="form-control" name="phone_number" value="{{ $agent->phone_number }}" placeholder="Agent phone" required/>
              </div>
            </div>

             <!-- select -->
            <div class="form-group bg-default">
                <label for="inputSelectType" class="col-sm-2 control-label">Is Active</label>
                <div class="col-sm-10">
                    <select class="form-control" id="inputSelectType" name="is_active">
                        <option value="1" @if ($agent->is_active == 1) selected="true" @endif>Yes</option>
                        <option value="0" @if ($agent->is_active == 0) selected="true" @endif>No</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </div>
      </div>
    </form>
</div><!-- /.box -->
@stop