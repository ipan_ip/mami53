@extends('admin.layouts.main')
@section('content')
    <!-- table ALTER TABLE dummy_data ADD data_from VARCHAR(10) AFTER is_live -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
            <div class="box-tools pull-right">
                <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
            </div>
        </div><!-- /.box-header -->
        
        <div class="row">
            <div class="col-md-12" style="margin-left: 10px; margin-bottom: 10px;">
               <a href="{{ URL::to('admin/giant/apartment/assign/new') }}" class="btn btn-x btn-primary">Add new agent apartment</a>
            </div>
        </div>
        
        <div class="box-body no-padding">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Agen Name</th>
                    <th>To</th>
                    <th>Is active</th>
                    <th class="table-action-column">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($data AS $key => $value)
                    <tr>
                        <td>{{ $key+1 }}</td>
                        <td>{{ $value->agent->name }} ({{ $value->agent->phone_number }}) </td>
                        <td>{{ $value->assign_to }}</td>
                        <td>@if ($value->status == 1) <label class="label label-success">Yes</label> @else <label class="label label-danger">No</label> @endif</td>
                        <td>
                            <a href="{{ URL::to('admin/giant/apartment/assign/delete', $value->id) }}" class="btn btn-xs btn-danger">Delete</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
            
        </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
    <!-- table -->

     <div class="box-body no-padding">
            {{ $data->appends(Request::except('page'))->links() }}
     </div>

@stop
@section('script')

@stop