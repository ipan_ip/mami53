@extends('admin.layouts.main')
@section('content')
<div class="box box-primary">
  <div class="box-header">
      <h3 class="box-title">{{ $boxTitle }}</h3>
  </div><!-- /.box-header -->
      <form action="{{ URL::to($action) }}" class="form-horizontal" method="POST">
      {{ csrf_field() }}
      <div class="box-body no-padding">
           
            <div class="form-group bg-default">
              <label for="inputName" class="col-sm-2 control-label">Assign to</label>
              <div class="col-sm-10">
                    <select name="agent_id" class="form-control">
                        @foreach ($agent AS $key => $value)
                            <option value="{{ $value->id }}">{{ $value->name }}</option>
                        @endforeach
                    </select>
              </div>
            </div>

            <div class="form-group bg-default">
              <label for="inputlatitude1" class="col-sm-2 control-label">Assign To</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" placeholder="Phone Number"
                  id="inputlatitude1" name="assign_to" value="{{ $data->assign_to }}">
              </div>
            </div>

             <!-- select -->
            <div class="form-group bg-default">
                <label for="inputSelectType" class="col-sm-2 control-label">Is Active</label>
                <div class="col-sm-10">
                    <select class="form-control" id="inputSelectType" name="is_active">
                        <option value="1">Yes</option>
                        <option value="0">No</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </div>
      </div>
    </form>
</div><!-- /.box -->
@stop
@section('script')
<!-- page script -->
<script type="text/javascript">

    @if (Session::get('laravelValidatorJSON') === null)
      myGlobal.laravelValidator = null;
    @else
      myGlobal.laravelValidator = {{ Session::get('laravelValidatorJSON') }};
    @endif


    $(function()
    {
      myGlobal.baseUrl = "{{ url('') . '/' }}";

      $formInsertBank = $('#formInsertBank');

      $( "#inputSelectType" ).change(function() {
        $formInsertBank
          .bootstrapValidator('updateStatus', 'name', 'NOT_VALIDATED')
          .bootstrapValidator('validateField', 'name');
      });

      formStoreTagRules = {
          name: {
              message: 'The name is not valid',
              validators: {
                  notEmpty: {
                      message: 'The name is required and cannot be empty'
                  },
                  stringLength: {
                      min: 3,
                      max: 30,
                      message: 'The name must be more than 3 and less than 30 characters long'
                  },
                  // Send { username: 'its value', type: 'username' } to the back-end
                  remote: {
                      message: 'The name is available',
                      url: myGlobal.baseUrl + 'admin/tag/unique',
                      data: function(validator) {
                          return {
                              type : 'tag.name',
                              fields : {
                                name : validator.getFieldElements('name').val(),
                                type : validator.getFieldElements('type').val()
                              }
                          };
                      },
                  }
              }
          },
          type: {
              validators: {
                  notEmpty: {
                      message: 'The type is required and cannot be empty'
                  },
              }
          }
      };

      formUpdateTagRules = {
          name: {
              message: 'The name is not valid',
              validators: {
                  notEmpty: {
                      message: 'The name is required and cannot be empty'
                  },
                  stringLength: {
                      min: 1,
                      max: 30,
                      message: 'The name must be more than 1 and less than 30 characters long'
                  }
              }
          },
          type: {
              validators: {
                  notEmpty: {
                      message: 'The type is required and cannot be empty'
                  },
              }
          }
      };

      @if (Route::currentRouteName() === 'admin.tag.create')
        myGlobal.bootstrapValidatorDefaults.fields = formStoreTagRules;
      @elseif (Route::currentRouteName() === 'admin.tag.edit')
        myGlobal.bootstrapValidatorDefaults.fields = formUpdateTagRules;
      @endif


      $formInsertBank.bootstrapValidator(myGlobal.bootstrapValidatorDefaults);

      updateValidateStatus($formInsertBank.data('bootstrapValidator'), myGlobal.laravelValidator);

    });
</script>
@stop