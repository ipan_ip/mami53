@extends('admin.layouts.main')
@section('content')
    <!-- table ALTER TABLE dummy_data ADD data_from VARCHAR(10) AFTER is_live -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
            <div class="box-tools pull-right">
                <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
            </div>
        </div><!-- /.box-header -->

            <div class="row">
                <div class="col-md-12" style="margin-left: 20px;">
                        <a href="{{ URL::to('admin/giant/apartment/assign') }}" class="btn btn-sm btn-primary">Agen apartemen</a>
                        <a href="{{ URL::to('admin/giant/kos/list') }}" class="btn btn-sm btn-default">Agents</a>
                </div>
            </div>

            <br/>
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <form action="" class="form-inline" method="get" style="text-align:right;padding:10px;">
                       

                          <input type="text" name="room_name" class="form-control input-sm"  placeholder="Room name"  autocomplete="off" value="{{ request()->input('room_name') }}">

                          <input type="text" name="phone_number" class="form-control input-sm"  placeholder="Agent phone"  autocomplete="off" value="{{ request()->input('phone_number') }}">

                          {{-- <select class="form-control" name="for_type">
                                @foreach ($for_type AS $key => $value)
                                    <option value="{{ $value }}" @if (request()->input('for_type')) selected="true" @endif>{{ $value }}</option>
                                @endforeach
                          </select>
                          --}}
                          
                          <select class="form-control" name="statuses">
                                @foreach ($status AS $key => $value)
                                    <option value="{{ $value }}" @if (request()->input('statuses') == $value) selected="true" @endif>{{ $value }}</option>
                                @endforeach
                          </select>

                          <input type="text" name="from" class="form-control input-sm datepicker" placeholder="Start Date" value="{{ request()->input('from') }}" id="start-date" autocomplete="off" style="margin-right:2px" />
                          <input type="text" name="to" class="form-control input-sm datepicker" placeholder="End Date" value="{{ request()->input('to') }}" id="end-date" autocomplete="off" style="margin-right:2px" />
          
                        <button class="btn btn-primary btn-sm" id="buttonSearch"><i class="fa fa-search">&nbsp;</i>Search</button>
                    </form>
                </div>
            </div>

{{--
            <div class="row">
                <div class="col-md-12">
                        <div class="alert alert-warning">
                            <strong>Live</strong> : {{ $total_live }}
                            || 
                            <strong>Submit</strong> : {{ $total_submit }}
                        </div>
                </div>
            </div>
        --}}
        <div class="box-body no-padding">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Agent</th>
                        <th>Status</th>
                        <th>Submit at - Edited</th>
                        <th class="table-action-column">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data AS $key => $value)
                        <tr>
                            <td>{{ $key+1 }}</td>
                            <td>{{ $value->room->name }} @if (!is_null($value->verificator)) <strong>Verif oleh: </strong> {{ $value->verificator }} @endif</td>
                            <td>{{ $value->agent->name }} ({{ $value->agent->phone_number }})</td>
                            <td>{{ $value->statuses }}</td>
                            <td>{{ $value->created_at->format('d M Y H:i') }} - {{ $value->updated_at->format('d M Y H:i') }}</td>
                            <td>
                                @if (in_array($value->statuses, ['submit', 'live', 'reject']))
                                <a href="{{ URL::to('admin/giant/edit', $value->id) }}" class="btn btn-xs btn-warning">Edit</a>
                                @endif

                                <a href="{{ URL::to('admin/giant/delete', $value->id) }}" class="btn btn-xs btn-danger">Delete</a>
                            </td>
                           
                        </tr>
                    @endforeach
                </tbody>
                
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
    <!-- table -->

     <div class="box-body no-padding">
            {{ $data->appends(Request::except('page'))->links() }}
     </div>

@stop
@section('script')
<script type="text/javascript">

    $(function()
    {
        $('#select-sort-option').change(function(event){
            var url  = '{{ URL::to("admin/giant/data") }}' + '?o=' + event.target.value ;

            url += '&st=' + $('#start-date').val();
            url += '&en=' + $('#end-date').val();

            window.location = url;

        });

        $('.datepicker').datepicker({ dateFormat : 'yy-mm-dd'});
    });

</script>
@stop