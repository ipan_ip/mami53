@extends('admin.layouts.main')
@section('content')
<div class="box box-primary">
  <div class="box-header">
      <h3 class="box-title">{{ $boxTitle }}</h3>
  </div><!-- /.box-header -->
  {{ Form::open(array('url' => $formAction, 'method' => $formMethod,
      'class' => 'form-horizontal form-bordered', 'role' => 'form', 'id' => 'formInsertStyle')) }}
      <div class="box-body no-padding">
            <div class="form-group bg-default">
              <label for="inputTitle" class="col-sm-2 control-label">URL</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" placeholder="Title"
                  id="inputTitle" name="url" value="">
              </div>
            </div>
            <div class="form-group bg-default">
              <label for="inputTitle" class="col-sm-2 control-label">Main Selector</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" placeholder="Title"
                  id="inputTitle" name="main_selector" value="">
              </div>
            </div>

            <?php 
              $fields = array(
                'title'         => array('Title', 'title_field', 'title_get', 'title_attribute'),
                'gender'        => array('Gender','gender_field','gender_get','gender_attribute'),
                'photo'         => array('Photo', 'photo_field', 'photo_get', 'photo_attribute'),
                'photo_1'       => array('Photo 1', 'photo_1_field', 'photo_1_get', 'photo_1_attribute'),
                'photo_2'       => array('Photo 2', 'photo_2_field', 'photo_2_get', 'photo_2_attribute'),
                'photo_3'       => array('Photo 3', 'photo_3_field', 'photo_3_get', 'photo_3_attribute'),
                'photo_4'       => array('Photo 4', 'photo_4_field', 'photo_4_get', 'photo_4_attribute'),
                );

             ?>
            <div class="form-group bg-default">
              <table class="col-sm-8 table table-striped">
                <tr>
                  <th width="17%">Field</th>
                  <th>Selector</th>
                  <th>Get</th>
                  <th>Attr Name</th>
                </tr>
                @foreach ($fields as $field)
                <tr>
                  <td>{{ $field[0] }}</td>
                  <td>
                    <input type="text" class="form-control" placeholder="{{ $field[0] }} Selector"
                      id="inputTitle" name="{{ $field[1] }}" value="">
                  </td>
                  <td>
                    <select class="form-control" name="{{ $field[2] }}" >
                      <option value=""></option>
                      <option value="value">Value</option>
                      <option value="html">HTML Element</option>
                      <option value="attr">Attribute</option>
                    </select>
                  </td>
                  <td>
                    <input type="text" class="form-control" placeholder="For Attribute Type Only"
                      id="inputTitle" name="{{ $field[3] }}" value="">
                  </td>
                </tr>
                @endforeach
              </table>
            </div>
            <div class="col-sm-12 bg-default">
              <div class="col-sm-2"></div>
              <button type="submit" class="btn btn-primary">Crawling</button>
              <a href="{{ $formCancel }}">
                <button type="button" class="btn btn-default">Cancel</button>
              </a>
              <button type="reset" class="btn btn-default"
                onclick="$('.formInsertStyle').bootstrapValidator('resetForm', true);">Reset</button>
            </div>
            <br/>
            <br/>
            <br/>
      </div>
  {{ Form::close() }}<!-- /.form -->
</div><!-- /.box -->
@stop
@section('style')
{{ HTML::style('assets/vendor/jquery-chosen/chosen.css') }}
{{ HTML::style('assets/vendor/jquery-chosen-custom/chosen.css') }}
{{ HTML::style('assets/vendor/jquery-file-upload/css/jquery.fileupload.css') }}
@stop
@section('script')
{{ HTML::script('assets/vendor/jquery-chosen/chosen.jquery.js') }}
<script type="text/javascript">
  $(function()
  {
    var config = {
      '.chosen-select'           : {width: '100%'}
    };
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    }
  });
</script>
<!-- Holder for Upload File Thumbnail -->
{{ HTML::script('assets/vendor/holder/holder.js') }}
<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
{{ HTML::script('assets/vendor/jquery-file-upload/js/vendor/jquery.ui.widget.js') }}
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
{{ HTML::script('assets/vendor/jquery-file-upload/js/jquery.iframe-transport.js') }}
<!-- The basic File Upload plugin -->
{{ HTML::script('assets/vendor/jquery-file-upload/js/jquery.fileupload.js') }}
<!-- The File Upload processing plugin -->
{{ HTML::script('assets/vendor/jquery-file-upload/js/jquery.fileupload-process.js') }}
<!-- The File Upload validation plugin -->
{{ HTML::script('assets/vendor/jquery-file-upload/js/jquery.fileupload-validate.js') }}
<!-- File Upload Main -->
{{ HTML::script('assets/vendor/file-upload/index.js') }}
<script>
  $(function () {
    // Change this to the location of your server-side upload handler
    @if (Request::is('admin/*'))
      var url = "{{ url('admin/media') }}";
    @else
      var url = "{{ url('media') }}";
    @endif
    
    myFileUpload($('#wrapper-file-upload'), url, 'style_photo');
  });
  
</script>
@stop