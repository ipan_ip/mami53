@extends('admin.layouts.main')
@section('content')
	<div class="box box-primary">
		<div class="box-header">
			<h3 class="box-title">{{ $boxTitle }}</h3>
		</div>
		{{ Form::open(array('url' => URL::Route($formAction) , 'files' => true)) }}
			<div class="box-body">
				<div class="form-group">
				    <label for="excel-file">File Excel</label>
				    <input type="file" class="form-control" id="excel-file" placeholder="Excel File Here" name="excel-file" />
				</div>
			</div>
			<div class="box-body">
				<input type="submit" class="btn btn-primary" value="Submit">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
			</div>	
		{{ Form::close() }}
	</div>
@stop
@section('script')

@stop