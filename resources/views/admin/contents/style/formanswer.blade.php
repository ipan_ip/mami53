@extends('admin.layouts.main')
@section('content')
<div class="box box-primary">
  <div class="box-header">
      <h3 class="box-title">{{ $boxTitle }}</h3>
  </div><!-- /.box-header -->
  {{ Form::open(array('url' => $formAction, 'method' => $formMethod,
      'class' => 'form-horizontal form-bordered', 'role' => 'form', 'id' => 'formInsertStyle')) }}
      <div class="box-body no-padding">
            <div id="cards">
              <div id="formCards">          
                <div class="form-group bg-info divider">
                  <div class="col-sm-12">
                    Cards 1
                  </div>
                </div>
                <div class="form-group bg-default">
                  <label for="inputSelectType1" class="col-sm-2 control-label">Type</label>
                  <div class="col-sm-2">
                    <select class="form-control" id="inputSelectType1" name="cards[0][type]" tabindex="2" data-placeholder="Select Card Type">
                        <option value="text" @if($rowStyle->type == 'text') selected @endif>Text</option>
                        <option value="video" @if($rowStyle->type == 'video') selected @endif>Video</option>
                        <option value="image" @if($rowStyle->type == 'image') selected @endif>Image/gif</option>
                        <option value="image" @if($rowStyle->type == 'quiz') selected @endif>Quiz</option>
                    </select>
                  </div>
                </div>
                <div class="form-group bg-default">
                  <label for="inputSelectSubType1" class="col-sm-2 control-label">Sub Type</label>
                  <div class="col-sm-2">
                    <select class="form-control" id="inputSelectSubType1" name="cards[0][sub_type]" tabindex="2" data-placeholder="Select Card Sub Type">
                        <option value="text" @if($rowStyle->type == 'text') selected @endif>Text</option>
                        <option value="gif" @if($rowStyle->type == 'gif') selected @endif>GIF</option>
                        <option value="image" @if($rowStyle->type == 'image') selected @endif>Image</option>
                    </select>
                  </div>
                </div>
                <div class="form-group bg-default" id="cardInputVideoUrl1" @if($rowStyle->type != 'video') hidden @endif>
                  <label for="cardInputVideoUrl" class="col-sm-2 control-label">Video URL</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Video URL"
                      id="cardInputVideoUrl" name="cards[0][video]" value="{{ $rowStyle->video_url }}">
                  </div>
                </div>
                <div class="form-group bg-default" id="cardInputImage1" @if($rowStyle->type != 'image') hidden @endif>
                  <label for="fileupload" class="col-sm-2 control-label">Image/gif</label>
                  <div class="col-sm-10">
                    <div id="cardUpload1" class="media">
                      <a class="pull-left thumbnail" id="wrapper-img" >
                        <img style="background: #EEE; width: 120px; height: 160px;" class="media-object" width="120px" height="160px" src="{{ $rowStyle->photo }}" data-src="holder.js/120x160/thumbnail" alt="">
                      </a>
                      <div class="media-body">
                        <input id="fileupload1" class="form-control" type="text" name="media1" placeholder="URL Photo">
                        <input id="sourceImg" type="hidden" name="cards[0][url_ori]" value="{{ $rowStyle->url_ori }}">
                        <br>
                        <span class="btn btn-info btn-sm fileinput-button" id="buttonUpload1">
                            <i class="glyphicon glyphicon-plus"></i>
                            <span>&nbsp; Add Photo from URL </span>
                        </span>
                        <!-- The fileinput-button span is used to style the file input field as button -->
                        <span class="btn btn-primary btn-sm fileinput-button">
                            <i class="glyphicon glyphicon-plus"></i>
                            <span>&nbsp; Add Photo.. </span>
                            <!-- The file input field used as target for the file upload widget -->
                            <input id="fileupload" type="file" name="media">
                            <input id="photo_id" type="hidden" name="cards[0][photo_id]" 
                              value="{{ $rowStyle->photo_id }}">
                        </span>

            <span >
<!-- <select name="cards[0][type]">
  <option value="image">image</option>
  <option value="gif">gif</option>

</select> -->

            </span>
                        <br>
                        <br>
                        <!-- The global progress bar -->
                        <div id="progress" class="progress">
                            <div class="progress-bar"></div>
                        </div>
                        <div class="col-sm-6">
                          <div id="original-file-info" class="row"></div>
                        </div>
                        <div class="col-sm-6">
                          <div id="uploaded-file-info" class="row"></div>
                        </div>
                        <br>
                        <!-- The container for the uploaded files -->
                        <div id="files" class="files"></div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group bg-default">
                  <label for="inputDescription" class="col-sm-2 control-label">Text</label>
                  <div class="col-sm-10">
                    <textarea class="form-control" rows="3" id="cardInputDescription" name="cards[0][description]" placeholder="Card Description">{{ $rowStyle->description }}</textarea>
                  </div>
                </div>
                <div class="form-group bg-default">
                  <label for="cardInputSource" class="col-sm-2 control-label">Source</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" rows="3" id="cardInputSource" name="cards[0][source]" placeholder="Source Content" value="{{ $rowStyle->source }}"></input>
                  </div>
                </div>

                <div class="form-group bg-default">
                  <label for="cardInputOrdering" class="col-sm-2 control-label">Order</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" rows="3" id="cardInputOrdering" name="cards[0][ordering]" placeholder="kasih order (10, 20, 30, 40, 50 ....)" value="{{ $rowStyle->ordering }}"></input>
                  </div>
                </div>

              </div>
            </div>

            @if(Request::segment(6) != 'edit')
            <div class="form-group bg-default">
              <label for="inputPrice" class="col-sm-2 control-label"></label>
              <div class="col-sm-offset-2 col-sm-10" style="margin-top: 10px;">
                  <a href="" type="button" id="addCard"
                    class="btn btn-default btn-sm">
                    <span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;Add Card
                  </a>
                  <a href="" type="button" id="removeCard"
                    class="btn btn-default btn-sm">
                    <span class="glyphicon glyphicon-minus"></span>&nbsp;&nbsp;Remove Card
                  </a>
                </div>
            </div>
            <div class="form-group bg-info divider">
              <div class="col-sm-12">
              </div>
            </div>
            @endif

            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="{{ $formCancel }}">
                  <button type="button" class="btn btn-default">Cancel</button>
                </a>
                <button type="reset" class="btn btn-default"
                  onclick="$('#formInsertStyle').bootstrapValidator('resetForm', true);">Reset</button>
              </div>
            </div>
      </div>
  {{ Form::close() }}<!-- /.form -->
</div><!-- /.box -->
@stop
@section('style')
{{ HTML::style('assets/vendor/jquery-chosen/chosen.css') }}
{{ HTML::style('assets/vendor/jquery-chosen-custom/chosen.css') }}
{{ HTML::style('assets/vendor/jquery-file-upload/css/jquery.fileupload.css') }}
@stop
@section('script')
{{ HTML::script('assets/vendor/jquery-chosen/chosen.jquery.js') }}
<script type="text/javascript">
  $(function()
  {
    var config = {
      '.chosen-select'           : {width: '100%'}
    };
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    }
  });
</script>
<!-- Holder for Upload File Thumbnail -->
{{ HTML::script('assets/vendor/holder/holder.js') }}
<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
{{ HTML::script('assets/vendor/jquery-file-upload/js/vendor/jquery.ui.widget.js') }}
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
{{ HTML::script('assets/vendor/jquery-file-upload/js/jquery.iframe-transport.js') }}
<!-- The basic File Upload plugin -->
{{ HTML::script('assets/vendor/jquery-file-upload/js/jquery.fileupload.js') }}
<!-- The File Upload processing plugin -->
{{ HTML::script('assets/vendor/jquery-file-upload/js/jquery.fileupload-process.js') }}
<!-- The File Upload validation plugin -->
{{ HTML::script('assets/vendor/jquery-file-upload/js/jquery.fileupload-validate.js') }}
<!-- File Upload Main -->
{{ HTML::script('assets/vendor/file-upload/index.js') }}
<script>
  $(function () {
    // Change this to the location of your server-side upload handler
    @if (Request::is('admin/*'))
      var url = "{{ url('admin/media') }}";
    @else
      var url = "{{ url('user/media') }}";
    @endif
    
    myFileUpload($('#wrapper-file-upload'), url, 'style_photo');
    myFileUpload($('#cardInputImage1'), url, 'style_photo');
  });
  
</script>
<!-- page script -->
<script type="text/javascript">

    @if (Session::get('laravelValidatorJSON') === null)
      myGlobal.laravelValidator = null;
    @else
      myGlobal.laravelValidator = {{ Session::get('laravelValidatorJSON') }};
    @endif


    $(function()
    {
      $formInsertStyle = $('#formInsertStyle');

      $form = $formInsertStyle;

      formStoreStyleRules = {
          title: {
              message: 'The title is not valid',
              validators: {
                  notEmpty: {
                      message: 'The title is required and cannot be empty'
                  },
                  stringLength: {
                      min: 3,
                      max: 30,
                      message: 'The title must be more than 3 and less than 30 characters long'
                  }
              }
          },
          photo_id: {
              feedbackIcons: 'false',
              message: 'The photo is not valid',
              validators: {
                  notEmpty: {
                      message: 'The photo is required and cannot be empty'
                  },
              }
          },
          gender: {
              message: 'The gender is not valid',
              validators: {
                  notEmpty: {
                      message: 'The gender is required and cannot be empty'
                  },
              }
          },
          'emotion_ids[]': {
              message: 'The emotion is not valid',
              validators: {
                  callback: {
                      message: 'Please choose at least one emotion',
                      callback: function(value, validator) {
                          // Get the selected options
                          var options = validator.getFieldElements('emotion_ids[]').val();
                          return (options != null && options.length >= 1);
                      }
                  }
              }
          },
          'keyword_ids[]': {
              message: 'The user keyword is not valid',
              validators: {
                  callback: {
                      message: 'Please choose at least one keyword',
                      callback: function(value, validator) {
                          // Get the selected options
                          var options = validator.getFieldElements('keyword_ids[]').val();
                          return (options != null && options.length >= 1);
                      }
                  }
              }
          },
      };

      formUpdateStyleRules = {
          title: {
              message: 'The title is not valid',
              validators: {
                  notEmpty: {
                      message: 'The title is required and cannot be empty'
                  },
                  stringLength: {
                      min: 3,
                      max: 30,
                      message: 'The title must be more than 3 and less than 30 characters long'
                  }
              }
          },
          photo_id: {
              feedbackIcons: 'false',
              message: 'The photo is not valid',
              validators: {
                  notEmpty: {
                      message: 'The photo is required and cannot be empty'
                  },
              }
          },
          gender: {
              message: 'The gender is not valid',
              validators: {
                  notEmpty: {
                      message: 'The gender is required and cannot be empty'
                  },
              }
          },
          'emotion_ids[]': {
              message: 'The emotion is not valid',
              validators: {
                  callback: {
                      message: 'Please choose at least one emotion',
                      callback: function(value, validator) {
                          // Get the selected options
                          var options = validator.getFieldElements('emotion_ids[]').val();
                          return (options != null && options.length >= 1);
                      }
                  }
              }
          },
          'keyword_ids[]': {
              message: 'The user keyword is not valid',
              validators: {
                  callback: {
                      message: 'Please choose at least one keyword',
                      callback: function(value, validator) {
                          // Get the selected options
                          var options = validator.getFieldElements('keyword_ids[]').val();
                          return (options != null && options.length >= 1);
                      }
                  }
              }
          },
      };

      myGlobal.bootstrapValidatorDefaults.excluded = ':disabled';

      @if (Route::currentRouteName() === 'admin.designer.style.create')
        myGlobal.bootstrapValidatorDefaults.fields = formStoreStyleRules;
      @elseif (Route::currentRouteName() === 'admin.designer.style.edit')
        myGlobal.bootstrapValidatorDefaults.fields = formUpdateStyleRules;
      @endif

      $formInsertStyle
        .find('[name="emotion_ids"]')
            .chosen()
            // Revalidate the color when it is changed
            .change(function(e) {
                $formInsertDesigner.bootstrapValidator('revalidateField', 'emotion_ids[]');
            })
            .end()
        .find('[name="keyword_ids"]')
            .chosen()
            // Revalidate the color when it is changed
            .change(function(e) {
                $formInsertDesigner.bootstrapValidator('revalidateField', 'keyword_ids[]');
            })
            .end()
        .bootstrapValidator(myGlobal.bootstrapValidatorDefaults);

      updateValidateStatus($formInsertStyle.data('bootstrapValidator'), myGlobal.laravelValidator);
    });
</script>
<!-- Javascript Card -->
<script type="text/javascript">
   $("#inputSelectType1").change(function(){
      var select = $("#inputSelectType1 option:selected").val();
      switch(select){
        case "video":
          $('#cardInputVideoUrl1').show();
          $('#cardInputImage1').hide();
          break;

        case "image":
        case "quiz":
          $('#cardInputVideoUrl1').hide();
          $('#cardInputImage1').show();
          break;

        case "text":
          $('#cardInputVideoUrl1').hide();
          $('#cardInputImage1').hide();
          break;
      }
   });

   /*add new card*/
      $("#addCard").click(function(){
          var validator= 0;
          var type    = 'type';
          var video   = 'video';
          var photo   = 'photo_id';
          var url_ori = 'url_ori';
          var source  = 'source';
          var description = 'description';
          var intId = $("#formCards div").length/21 + 1;
          var index = intId - 1;
          var html  = '<div class="form-group bg-info divider"><div class="col-sm-12">Cards '+intId+'</div></div>';

          html += '<div class="form-group bg-default"><label for="inputSelect" class="col-sm-2 control-label">Type</label><div class="col-sm-2">';
          html += '<select class="form-control" id="inputSelectType'+intId+'" name="cards['+index+']['+type+']" tabindex="2" data-placeholder="Select Card Type">';
          html += '<option value="text">Text</option><option value="video">Video</option><option value="image">Image/gif</option>';
          html += '</select></div></div>';
          html += '<div class="form-group bg-default" id="cardInputVideoUrl'+intId+'" hidden><label for="inputUrl" class="col-sm-2 control-label">Video URL</label>';
          html += '<div class="col-sm-10"><input type="text" class="form-control" placeholder="Video URL" id="cardInputVideoUrl'+intId+'" name="cards['+index+']['+video+']" value=""></div></div>';
          html += '<div class="form-group bg-default" id="cardInputImage'+intId+'" hidden><label for="fileupload" class="col-sm-2 control-label">Image/gif</label><div class="col-sm-10">';
          html += '<div id="cardUpload'+intId+'" class="media"><a class="pull-left thumbnail" id="wrapper-img" ><img style="background: #EEE; width: 120px; height: 160px;" class="media-object" width="120px" height="160px" src="" data-src="holder.js/120x160/thumbnail" alt=""></a><div class="media-body">';
          html += '<input id="fileupload1" class="form-control" type="text" name="media1" placeholder="URL Photo"><input id="sourceImg" type="hidden" name="cards['+index+']['+url_ori+']"><br><span class="btn btn-info btn-sm fileinput-button" id="buttonUpload'+intId+'"><i class="glyphicon glyphicon-plus"></i><span>&nbsp; Add Photo from URL </span></span>&nbsp;';
          html += '<span class="btn btn-primary btn-sm fileinput-button"><i class="glyphicon glyphicon-plus"></i><span>&nbsp; Add Photo.. </span><input id="fileupload" type="file" name="media"><input id="photo_id" type="hidden" name="cards['+index+']['+photo+']" value=""></span><span></span>';
          html += '<br><br><div id="progress" class="progress"><div class="progress-bar"></div></div>';
          html += '<div class="col-sm-6"><div id="original-file-info" class="row"></div></div>';
          html += '<div class="col-sm-6"><div id="uploaded-file-info" class="row"></div></div><br>';
          html += '<div id="files" class="files"></div></div></div></div></div>';
          html += '<div class="form-group bg-default"><label for="inputDescription" class="col-sm-2 control-label">Text</label><div class="col-sm-10">';
          html += '<textarea class="form-control" rows="3" id="cardInputDescription" name="cards['+index+']['+description+']" placeholder="Card Description"></textarea></div></div>';
          html += '<div class="form-group bg-default"><label for="cardInputSource" class="col-sm-2 control-label">Source</label><div class="col-sm-10"><input type="text" class="form-control" rows="3" id="cardInputSource" name="cards['+index+']['+source+']" placeholder="Source Content"></input></div></div>';
    html += '<div class="form-group bg-default"><label for="cardInputOrdering" class="col-sm-2 control-label">Order</label><div class="col-sm-10"><input type="text" class="form-control" rows="3" id="cardInputOrdering" name="cards['+index+'][ordering]" placeholder="kasih order (10, 20, 30, 40, 50 ....)"></input></div></div>';

          $("#formCards").append(html);

          /*Plih input berdasarkan select*/
          $("#inputSelectType"+intId).change(function(){
              var select = $("#inputSelectType"+intId+" option:selected").val();
              switch(select){
                case "video":
                  $('#cardInputVideoUrl'+intId).show();
                  $('#cardInputImage'+intId).hide();
                  break;

                case "image":
                  $('#cardInputVideoUrl'+intId).hide();
                  $('#cardInputImage'+intId).show();
                  break;

                case "text":
                  $('#cardInputVideoUrl'+intId).hide();
                  $('#cardInputImage'+intId).hide();
                  break;
              }
           });

          /*Upload foto*/
          // Change this to the location of your server-side upload handler
          @if (Request::is('admin/*'))
            var url = "{{ url('admin/media') }}";
          @else
            var url = "{{ url('media') }}";
          @endif
          
          myFileUpload($('#cardUpload'+intId), url, 'style_photo', validator);

          <?php $login_type = Request::is('admin/*')?'admin':'user'; ?>
          
          /*upload foto from url*/
          $("#buttonUpload"+intId).click(function(){
              var wrapper = $('#cardUpload'+intId);
              var mediaUrl= wrapper.find('#fileupload1').val();
              var url     = "{{ url($login_type . '/mediaUrl?t=style_photo&url=') }}"+mediaUrl;

              uploadFromUrl(wrapper, url, mediaUrl, validator); 
          });
      });
    
    $("#removeCard").click(function(){
        var intId = $("#formCards div").length - 21;
        if(intId != 0){
          $('#formCards div').slice(intId).remove();
        }
    });

    $("#buttonCover").click(function(){
        var validator= 1;
        var wrapper = $('#wrapper-cover-upload');
        var mediaUrl= wrapper.find('#fileupload1').val();
        var url     = "{{ url($login_type . '/mediaUrl?t=user_photo&url=') }}"+mediaUrl;

        uploadFromUrl(wrapper, url, mediaUrl, validator);
        
        // $('#mediaUrl').val('http://localhost/stories/public/uploads/cache/data/user/2015-04-09/EXx0YEYX-240x320.jpg');
        // $('#coba').attr('src', 'http://localhost/stories/public/uploads/cache/data/user/2015-04-09/EXx0YEYX-240x320.jpg');
        // console.log(isi);      
    });

    $("#buttonThumbnail").click(function(){
        var validator= 1;
        var wrapper = $('#wrapper-thumbnail-upload');
        var mediaUrl= wrapper.find('#fileupload1').val();
        var url     = "{{ url($login_type . '/mediaUrl?t=user_photo&url=') }}"+mediaUrl;

        uploadFromUrl(wrapper, url, mediaUrl, validator); 
    });

    $("#buttonUpload1").click(function(){
        var validator= 0;
        var wrapper = $('#cardUpload1');
        var mediaUrl= wrapper.find('#fileupload1').val();
        var url     = "{{ url($login_type . '/mediaUrl?t=user_photo&url=') }}"+mediaUrl;

        uploadFromUrl(wrapper, url, mediaUrl, validator); 
    });
</script>
@stop