@extends('admin.layouts.main')
@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
            <div class="box-tools pull-right">
                <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <a href="{{ URL::route($createAction, $designerId) }}">
                        <button type="submit" class="btn-add btn btn-primary btn-sm">
                            <i class="fa fa-plus">&nbsp;</i>Add Style</button>
                    </a>
                </div>
            </div>

            <table id="tableListStyle" class="table table-striped">
                <thead>
                    <tr>
                        <th width="30px;">No</th>
                        <th>Photo</th>
                        <th>Title</th>
                        <th>Gender</th>
                        <th>Heart Count</th>
                        <th class="table-action-column" width="100px">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($rowsStyle as $rowStyleKey => $rowStyle)
                    <tr>
                        <td>{{ $rowStyleKey+1 }}</td>
                        <td>
                            <?php $tempMedia = Media::getMediaUrl($rowStyle['photo_id']); ?>
                            <img width="60" class="img-thumbnail" src="{{ $tempMedia['small'] }}" alt="">
                        </td>
                        <!-- <td>
                            <img width="60" class="img-thumbnail" src="http://hairclick.wejoin.us/std_api/standard-server-api/public/uploads/cache/data/user/2014-08-25/XgbV2Uf2-120x160.jpg" alt="">
                        </td> -->
                        <td>{{ $rowStyle->title }}</td>
                        <td>{{ $rowStyle->gender }}</td>
                        <td>{{ $rowStyle->like_count }}</td>
                        <td class="table-action-column">
                            <div class="btn-action-group">
                                <a href="{{ URL::route('admin.style.verify.action', array($rowStyle->id)) }}">
                                    <i class="fa fa-pencil"></i></a>
                                <a href="{{ URL::route($editAction, array($designerId, $rowStyle->id)) }}">
                                    <i class="fa fa-pencil"></i></a>
                                <a href="#" ng-click="showConfirmDeleteNew('Style', '{{ URL::route($deleteAction, array($designerId, $rowStyle->id)) }}', '{{ $rowStyle->title }}');">
                                    <i class="fa fa-trash-o"></i></a>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>No</th>
                        <th>Photo</th>
                        <th>Title</th>
                        <th>Gender</th>
                        <th>Heart Count</th>
                        <th class="table-action-column">Action</th>
                    </tr>
                </tfoot>
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
    <!-- table -->

    @include('admin.partials.delete-modal-new')
@stop
@section('script')
<!-- page script -->
<script type="text/javascript">
    $(function()
    {
        $("#tableListStyle").dataTable();
    });
</script>
@stop