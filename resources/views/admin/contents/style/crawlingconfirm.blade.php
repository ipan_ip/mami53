@extends('admin.layouts.main')
@section('content')
<div class="box box-primary">
  <div class="box-header">
      <h3 class="box-title">{{ $boxTitle }}</h3>
  </div><!-- /.box-header -->
      <div class="box-body no-padding">
            <div class="form-group bg-default">
              <table class="col-sm-12 table table-striped">
                <tr>
                   <th>title</th>
                   <th>gender</th>
                   <th>photo</th>
                   <th>photo_1</th>
                   <th>photo_2</th>
                   <th>photo_3</th>
                   <th>photo_4</th>
                </tr>
                @foreach ($styles as $style)
                <tr>
                 <tr>
                   <td>{{ @$style['title']   }}</td>
                   <td>{{ @$style['gender']  }}</td>
                   <td><img src="{{ @$style['photo_0'] }}"></td>
                   <td><img src="{{ @$style['photo_1'] }}"></td>
                   <td><img src="{{ @$style['photo_2'] }}"></td>
                   <td><img src="{{ @$style['photo_3'] }}"></td>
                   <td><img src="{{ @$style['photo_4'] }}"></td>
                </tr>
                </tr>
                @endforeach
              </table>
            </div>
            <div class="col-sm-12 bg-default">
              <div class="col-sm-2"></div>
              <button type="submit" class="btn btn-primary">Crawling</button>
              <button type="reset" class="btn btn-default"
                onclick="$('.formInsertStyle').bootstrapValidator('resetForm', true);">Reset</button>
            </div>
            <br/>
            <br/>
            <br/>
      </div>
</div><!-- /.box -->
@stop
@section('style')
{{ HTML::style('assets/vendor/jquery-chosen/chosen.css') }}
{{ HTML::style('assets/vendor/jquery-chosen-custom/chosen.css') }}
{{ HTML::style('assets/vendor/jquery-file-upload/css/jquery.fileupload.css') }}
@stop
@section('script')
{{ HTML::script('assets/vendor/jquery-chosen/chosen.jquery.js') }}
<script type="text/javascript">
  $(function()
  {
    var config = {
      '.chosen-select'           : {width: '100%'}
    };
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    }
  });
</script>
<!-- Holder for Upload File Thumbnail -->
{{ HTML::script('assets/vendor/holder/holder.js') }}
<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
{{ HTML::script('assets/vendor/jquery-file-upload/js/vendor/jquery.ui.widget.js') }}
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
{{ HTML::script('assets/vendor/jquery-file-upload/js/jquery.iframe-transport.js') }}
<!-- The basic File Upload plugin -->
{{ HTML::script('assets/vendor/jquery-file-upload/js/jquery.fileupload.js') }}
<!-- The File Upload processing plugin -->
{{ HTML::script('assets/vendor/jquery-file-upload/js/jquery.fileupload-process.js') }}
<!-- The File Upload validation plugin -->
{{ HTML::script('assets/vendor/jquery-file-upload/js/jquery.fileupload-validate.js') }}
<!-- File Upload Main -->
{{ HTML::script('assets/vendor/file-upload/index.js') }}
<script>
  $(function () {
    // Change this to the location of your server-side upload handler
    @if (Request::is('admin/*'))
      var url = "{{ url('admin/media') }}";
    @else
      var url = "{{ url('media') }}";
    @endif
    
    myFileUpload($('#wrapper-file-upload'), url, 'style_photo');
  });
  
</script>
@stop