@extends('admin.layouts.main')
@section('content')
<div class="box box-primary">
  <div class="box-header">
      <h3 class="box-title"> 스타일 수정 {{ $boxTitle }} </h3>
  </div><!-- /.box-header -->
  {{ Form::open(array('url' => $formAction, 'method' => $formMethod,
      'class' => 'form-horizontal form-bordered', 'role' => 'form', 'id' => 'formInsertStyle')) }}
      <div class="box-body no-padding">
            <div class="form-group bg-default">
              <label for="inputTitle" class="col-sm-2 control-label">타이틀</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" placeholder="Title"
                  id="inputTitle" name="title" value="{{ $rowStyle->title }}">
              </div>
            </div>
            <div class="form-group bg-default">
              <label for="fileupload" class="col-sm-2 control-label">사진</label>
              <div class="col-sm-10">
              
             <!--  Upload Style Photo -->
                <div id="wrapper-file-upload" class="media">
                  <a class="pull-left thumbnail" id="wrapper-img" >
                    <img style="background: #EEE; width: 120px; height: 160px;" 
                    class="media-object" width="120px" height="160px" 
                    src="{{ $rowStyle->photo }}" 
                    data-src="{{ $rowStyle->photo !== NULL ? $rowStyle->photo : 'holder.js/120x160/thumbnail' }}" alt="">
                  </a>
                  <div class="media-body">
                    <!-- The fileinput-button span is used to style the file input field as button -->
                    <span class="btn btn-primary btn-sm fileinput-button">
                        <i class="glyphicon glyphicon-plus"></i>
                        <span>&nbsp;이미지선택</span>
                        <!-- The file input field used as target for the file upload widget -->
                        <input id="fileupload" type="file" name="media">
                        <input id="photo_id" type="hidden" name="photo_id"
                          value="{{ $rowStyle->photo_id }}">
                    </span>
                   
                    <br>
                    <br>
                    <!-- The global progress bar -->
                    <div id="progress" class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <div class="col-sm-6">
                      <div id="original-file-info" class="row"></div>
                    </div>
                    <div class="col-sm-6">
                      <div id="uploaded-file-info" class="row">
                        <b>URL: </b> {{ @$rowStyle->photo_real }} <br>
                      </div>
                    </div>
                    <br>
                    <!-- The container for the uploaded files -->
                    <div id="files" class="files"></div>
                  </div>
                </div>

                <div id="wrapper-file-upload-1" class="media">
                  <a class="pull-left thumbnail" id="wrapper-img" >
                    <img style="background: #EEE; width: 120px; height: 160px;" 
                    class="media-object" width="120px" height="160px" 
                    src="{{ @$rowStyle->photo_1 }}" 
                    data-src="{{ @$rowStyle->photo_1 !== NULL ? $rowStyle->photo_1_id : 'holder.js/120x160/thumbnail' }}" alt="">
                  </a>
                  <div class="media-body">
                    <!-- The fileinput-button span is used to style the file input field as button -->
                    <span class="btn btn-primary btn-sm fileinput-button">
                        <i class="glyphicon glyphicon-plus"></i>
                        <span>&nbsp;이미지선택</span>
                        <!-- The file input field used as target for the file upload widget -->
                        <input id="fileupload" type="file" name="media">
                        <input id="photo_id" type="hidden" name="photo_id"
                          value="{{ $rowStyle->photo_1_id }}">
                        <input type="hidden" id="photo_1_id" name="photo_1_id" />
                    </span>
                    <span class="btn btn-primary btn-sm remove-image-style" onclick="removePhoto(1)">
                      <span>Remove</span>
                    </span>
                    <br>
                    <br>
                    <!-- The global progress bar -->
                    <div id="progress" class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <div class="col-sm-6">
                      <div id="original-file-info" class="row"></div>
                    </div>
                    <div class="col-sm-6">
                      <div id="uploaded-file-info" class="row">
                        <b>URL: </b> {{ @$rowStyle->photo_1_real }} <br>
                      </div>
                    </div>
                    <br>
                    <!-- The container for the uploaded files -->
                    <div id="files" class="files"></div>
                  </div>
                </div>

                <div id="wrapper-file-upload-2" class="media">
                  <a class="pull-left thumbnail" id="wrapper-img" >
                    <img style="background: #EEE; width: 120px; height: 160px;" 
                    class="media-object" width="120px" height="160px" 
                    src="{{ @$rowStyle->photo_2 }}" 
                    data-src="{{ @$rowStyle->photo_2 !== NULL ? $rowStyle->photo_2_id : 'holder.js/120x160/thumbnail' }}" alt="">
                  </a>
                  <div class="media-body">
                    <!-- The fileinput-button span is used to style the file input field as button -->
                    <span class="btn btn-primary btn-sm fileinput-button">
                        <i class="glyphicon glyphicon-plus"></i>
                        <span>&nbsp;이미지선택</span>
                        <!-- The file input field used as target for the file upload widget -->
                        <input id="fileupload" type="file" name="media">
                        <input id="photo_id" type="hidden" name="photo_id"
                          value="{{ $rowStyle->photo_2_id }}">
                        <input type="hidden" id="photo_2_id" name="photo_2_id" />
                    </span>
                    <span class="btn btn-primary btn-sm remove-image-style" onclick="removePhoto(2)">
                      <span>Remove</span>
                    </span>
                    <br>
                    <br>
                    <!-- The global progress bar -->
                    <div id="progress" class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <div class="col-sm-6">
                      <div id="original-file-info" class="row"></div>
                    </div>
                    <div class="col-sm-6">
                      <div id="uploaded-file-info" class="row">
                        <b>URL: </b> {{ @$rowStyle->photo_2_real }} <br>
                      </div>
                    </div>
                    <br>
                    <!-- The container for the uploaded files -->
                    <div id="files" class="files"></div>
                  </div>
                </div>

                <div id="wrapper-file-upload-3" class="media">
                  <a class="pull-left thumbnail" id="wrapper-img" >
                    <img style="background: #EEE; width: 120px; height: 160px;" 
                    class="media-object" width="120px" height="160px" 
                    src="{{ @$rowStyle->photo_3 }}" 
                    data-src="{{ @$rowStyle->photo_3 !== NULL ? $rowStyle->photo_3_id : 'holder.js/120x160/thumbnail' }}" alt="">
                  </a>
                  <div class="media-body">
                    <!-- The fileinput-button span is used to style the file input field as button -->
                    <span class="btn btn-primary btn-sm fileinput-button">
                        <i class="glyphicon glyphicon-plus"></i>
                        <span>&nbsp;이미지선택</span>
                        <!-- The file input field used as target for the file upload widget -->
                        <input id="fileupload" type="file" name="media">
                        <input id="photo_id" type="hidden" name="photo_id"
                          value="{{ $rowStyle->photo_3_id }}">
                                                  <input type="hidden" id="photo_3_id" name="photo_3_id" />
                    </span>
                    <span class="btn btn-primary btn-sm remove-image-style" onclick="removePhoto(3)">
                      <span>Remove</span>
                    </span>
                    <br>
                    <br>
                    <!-- The global progress bar -->
                    <div id="progress" class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <div class="col-sm-6">
                      <div id="original-file-info" class="row"></div>
                    </div>
                    <div class="col-sm-6">
                      <div id="uploaded-file-info" class="row">
                        <b>URL: </b> {{ @$rowStyle->photo_3_real }} <br>
                      </div>
                    </div>
                    <br>
                    <!-- The container for the uploaded files -->
                    <div id="files" class="files"></div>
                  </div>
                </div>

                <div id="wrapper-file-upload-4" class="media">
                  <a class="pull-left thumbnail" id="wrapper-img" >
                    <img style="background: #EEE; width: 120px; height: 160px;" 
                    class="media-object" width="120px" height="160px" 
                    src="{{ @$rowStyle->photo_4 }}" 
                    data-src="{{ @$rowStyle->photo_4 !== NULL ? $rowStyle->photo_4_id : 'holder.js/120x160/thumbnail' }}" alt="">
                  </a>
                  <div class="media-body">
                    <!-- The fileinput-button span is used to style the file input field as button -->
                    <span class="btn btn-primary btn-sm fileinput-button">
                        <i class="glyphicon glyphicon-plus"></i>
                        <span>&nbsp;이미지선택</span>
                        <!-- The file input field used as target for the file upload widget -->
                        <input id="fileupload" type="file" name="media">
                        <input id="photo_id" type="hidden" name="photo_id"
                          value="{{ $rowStyle->photo_4_id }}">
                                                  <input type="hidden" id="photo_4_id" name="photo_4_id" />
                    </span>
                    <span class="btn btn-primary btn-sm remove-image-style" onclick="removePhoto(4)">
                      <span>Remove</span>
                    </span>
                    <br>
                    <br>
                    
                    <!-- The global progress bar -->
                    <div id="progress" class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <div class="col-sm-6">
                      <div id="original-file-info" class="row"></div>
                    </div>
                    <div class="col-sm-6">
                      <div id="uploaded-file-info" class="row">
                        <b>URL: </b> {{ @$rowStyle->photo_4_real }} <br>
                      </div>
                    </div>
                    <br>
                    <!-- The container for the uploaded files -->
                    <div id="files" class="files"></div>
                  </div>
                </div>
                <input type="hidden" name="photo_id" id="main_photo_id" value="{{ $rowStyle->photo_id }}" />
              <!-- End Upload Style Photo -->
              </div>
            </div>
 
            <div class="form-group bg-default">
              <label for="inputGender" class="col-sm-2 control-label">성별</label>
              <div class="col-sm-10">
                <!-- radio -->
                @foreach ($rowsGender as $rowGenderKey => $rowGender)
                  <div class="radio">
                      <label>
                          <input type="radio" name="gender" class="simple" id="inputGender-{{ $rowGenderKey }}" 
                            value="{{ $rowGender->name }}" {{ $rowGender->checked }}/>
                          <span>{{ ucwords($rowGender->name) }}</span>
                      </label>
                  </div>
                @endforeach
              </div>
            </div>
            <!-- select -->
            @if($designerId == 0)
              <div class="form-group bg-default">
                  <label for="inputEmotion" class="col-sm-2 control-label">Designer</label>
                  <div class="col-sm-10">
                      <select class="form-control chosen-select" id="inputDesignerId" onchange="setDesignerIdFormAction()" tabindex="2">
                          @foreach ($rowsDesigner as $rowDesigner)
                            <option value="{{ $rowDesigner->designer_id }}" >{{ $rowDesigner->name }}</option>
                          @endforeach
                      </select>
                  </div>
              </div>
            @endif

            <div class="form-group bg-default">
                <label for="inputEmotion" class="col-sm-2 control-label">테마 선택</label>
                <div class="col-sm-10">
                    <select class="form-control chosen-select" multiple id="inputEmotion" name="emotion_ids[]" tabindex="2">
                        @foreach ($rowsEmotion as $rowEmotion)
                          <option value="{{ $rowEmotion->id }}" 
                            {{ $rowEmotion->selected }}>{{ $rowEmotion->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-sm-offset-2 col-sm-10" style="margin-top: 10px;">
                  <a href="{{ URL::route('admin.tag.create') }}" type="button" 
                    class="btn btn-default btn-sm">
                    <span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;테마 추가
                  </a>
                </div>
            </div>
            <!-- select -->
            <div class="form-group bg-default">
                <label for="inputEmotion" class="col-sm-2 control-label">키워드 선택</label>
                <div class="col-sm-10">
                    <select class="form-control chosen-select" multiple id="inputEmotion" name="keyword_ids[]" tabindex="2">
                        @foreach ($rowsKeyword as $rowKeyword)
                          <option value="{{ $rowKeyword->id }}" 
                            {{ $rowKeyword->selected }}>{{ $rowKeyword->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-sm-offset-2 col-sm-10" style="margin-top: 10px;">
                  <a href="{{ URL::route('admin.tag.create') }}" type="button" 
                    class="btn btn-default btn-sm">
                    <span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;키워드 추가
                  </a>
                </div>
            </div>
            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">저장</button>
                <a href="{{ $formCancel }}">
                  <button type="button" class="btn btn-default">취소</button>
                </a>
                <button type="reset" class="btn btn-default"
                  onclick="$('.formInsertStyle').bootstrapValidator('resetForm', true);">Reset</button>
              </div>  
            </div>
      </div>
  {{ Form::close() }}<!-- /.form -->
</div><!-- /.box -->
@stop
@section('style')
{{ HTML::style('assets/vendor/jquery-chosen/chosen.css') }}
{{ HTML::style('assets/vendor/jquery-chosen-custom/chosen.css') }}
{{ HTML::style('assets/vendor/jquery-file-upload/css/jquery.fileupload.css') }}
@stop
@section('script')
{{ HTML::script('assets/vendor/jquery-chosen/chosen.jquery.js') }}
<script type="text/javascript">
  $(function()
  {
    var config = {
      '.chosen-select'           : {width: '100%'}
    }
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    }
  });
</script>
<!-- Holder for Upload File Thumbnail -->
{{ HTML::script('assets/vendor/holder/holder.js') }}
<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
{{ HTML::script('assets/vendor/jquery-file-upload/js/vendor/jquery.ui.widget.js') }}
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
{{ HTML::script('assets/vendor/jquery-file-upload/js/jquery.iframe-transport.js') }}
<!-- The basic File Upload plugin -->
{{ HTML::script('assets/vendor/jquery-file-upload/js/jquery.fileupload.js') }}
<!-- The File Upload processing plugin -->
{{ HTML::script('assets/vendor/jquery-file-upload/js/jquery.fileupload-process.js') }}
<!-- The File Upload validation plugin -->
{{ HTML::script('assets/vendor/jquery-file-upload/js/jquery.fileupload-validate.js') }}
<!-- File Upload Main -->
{{ HTML::script('assets/vendor/file-upload/index.js') }}

<script type="text/javascript">
  
  function removePhoto(photoNumber){
    var wrapper = $('#wrapper-file-upload-'+photoNumber);
    wrapper.find('.media-object').attr('src','');
    wrapper.find('#original-file-info').html(' ');
    wrapper.find('#uploaded-file-info').html(' ');
    wrapper.find('#photo_id').val('0');
    wrapper.find('#progress .progress-bar').css('width','0%');
  }

</script>

<script>
  $(function () {
    // Change this to the location of your server-side upload handler
    @if (Request::is('admin/*'))
      var url = "{{ url('admin/media') }}";
    @else
      var url = "{{ url('media') }}";
    @endif

    // Upload Engine for Style Photos    
    myFileUpload($('#wrapper-file-upload'), url, 'style_photo');
    myFileUpload($('#wrapper-file-upload-1'), url, 'style_photo');
    myFileUpload($('#wrapper-file-upload-2'), url, 'style_photo');
    myFileUpload($('#wrapper-file-upload-3'), url, 'style_photo');
    myFileUpload($('#wrapper-file-upload-4'), url, 'style_photo');

    // Mechanism for uploading the photo
    $('#formInsertStyle').submit(function(){      
      for(i=1;i<5;i++){
        var photo_holder = $('#wrapper-file-upload-'+i).find('#photo_id');
        var photo_id = photo_holder.val() || 0;
        $('#photo_'+i+'_id').val(photo_id);
      }
      $("#main_photo_id").val($('#wrapper-file-upload').find('#photo_id').val());
    });

  })

  @if($designerId==0)

    function setDesignerIdFormAction()
    {
        var defaultUrl = '{{ $formAction }}';
        var designerId = $('#inputDesignerId').val();
        var url        = defaultUrl.replace('0',designerId);
        $('#formInsertStyle').attr('action',url);
    } 

  @endif
  
</script>
<!-- page script -->
<script type="text/javascript">

    @if (Session::get('laravelValidatorJSON') === null)
      myGlobal.laravelValidator = null;
    @else
      myGlobal.laravelValidator = {{ Session::get('laravelValidatorJSON') }};
    @endif


    $(function()
    {
      $formInsertStyle = $('#formInsertStyle');

      $form = $formInsertStyle;

      formStoreStyleRules = {
          title: {
              message: 'The title is not valid',
              validators: {
                  notEmpty: {
                      message: 'The title is required and cannot be empty'
                  },
                  stringLength: {
                      min: 3,
                      max: 30,
                      message: 'The title must be more than 3 and less than 30 characters long'
                  }
              }
          },
          photo_id: {
              feedbackIcons: 'false',
              message: 'The photo is not valid',
              validators: {
                  notEmpty: {
                      message: 'The photo is required and cannot be empty'
                  },
              }
          },
          photo_1_id: {
              feedbackIcons: 'false',
              message: 'The photo is not valid',
              validators: {
              }
          },
          photo_2_id: {
              feedbackIcons: 'false',
              message: 'The photo is not valid',
              validators: {
              }
          },
          photo_3_id: {
              feedbackIcons: 'false',
              message: 'The photo is not valid',
              validators: {
              }
          },
          photo_4_id: {
              feedbackIcons: 'false',
              message: 'The photo is not valid',
              validators: {
              }
          },
          gender: {
              message: 'The gender is not valid',
              validators: {
                  notEmpty: {
                      message: 'The gender is required and cannot be empty'
                  },
              }
          },
          'emotion_ids[]': {
              message: 'The emotion is not valid',
              validators: {
                  callback: {
                      message: 'Please choose at least one emotion',
                      callback: function(value, validator) {
                          // Get the selected options
                          var options = validator.getFieldElements('emotion_ids[]').val();
                          return (options != null && options.length >= 1);
                      }
                  }
              }
          },
          'keyword_ids[]': {
              message: 'The user keyword is not valid',
              validators: {
                  callback: {
                      message: 'Please choose at least one keyword',
                      callback: function(value, validator) {
                          // Get the selected options
                          var options = validator.getFieldElements('keyword_ids[]').val();
                          return (options != null && options.length >= 1);
                      }
                  }
              }
          },
      };

      formUpdateStyleRules = {
          title: {
              message: 'The title is not valid',
              validators: {
                  notEmpty: {
                      message: 'The title is required and cannot be empty'
                  },
                  stringLength: {
                      min: 3,
                      max: 30,
                      message: 'The title must be more than 3 and less than 30 characters long'
                  }
              }
          },
          photo_id: {
              feedbackIcons: 'false',
              message: 'The photo is not valid',
              validators: {
                  notEmpty: {
                      message: 'The photo is required and cannot be empty'
                  },
              }
          },
          gender: {
              message: 'The gender is not valid',
              validators: {
                  notEmpty: {
                      message: 'The gender is required and cannot be empty'
                  },
              }
          },
          'emotion_ids[]': {
              message: 'The emotion is not valid',
              validators: {
                  callback: {
                      message: 'Please choose at least one emotion',
                      callback: function(value, validator) {
                          // Get the selected options
                          var options = validator.getFieldElements('emotion_ids[]').val();
                          return (options != null && options.length >= 1);
                      }
                  }
              }
          },
          'keyword_ids[]': {
              message: 'The user keyword is not valid',
              validators: {
                  callback: {
                      message: 'Please choose at least one keyword',
                      callback: function(value, validator) {
                          // Get the selected options
                          var options = validator.getFieldElements('keyword_ids[]').val();
                          return (options != null && options.length >= 1);
                      }
                  }
              }
          },
      };

      myGlobal.bootstrapValidatorDefaults.excluded = ':disabled';

      @if (Route::currentRouteName() === 'admin.stories.style.create')
        myGlobal.bootstrapValidatorDefaults.fields = formStoreStyleRules;
      @elseif (Route::currentRouteName() === 'admin.stories.style.edit')
        myGlobal.bootstrapValidatorDefaults.fields = formUpdateStyleRules;
      @endif

      $formInsertStyle
        .find('[name="emotion_ids"]')
            .chosen()
            // Revalidate the color when it is changed
            .change(function(e) {
                $formInsertDesigner.bootstrapValidator('revalidateField', 'emotion_ids[]');
            })
            .end()
        .find('[name="keyword_ids"]')
            .chosen()
            // Revalidate the color when it is changed
            .change(function(e) {
                $formInsertDesigner.bootstrapValidator('revalidateField', 'keyword_ids[]');
            })
            .end()
        .bootstrapValidator(myGlobal.bootstrapValidatorDefaults);

      updateValidateStatus($formInsertStyle.data('bootstrapValidator'), myGlobal.laravelValidator);
    });
</script>
@stop