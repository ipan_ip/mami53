@extends('admin.layouts.main')

@section('style')
<style>
    .table>tbody>tr>td {
        vertical-align: middle;
    }

    .font-large {
        font-size: 1.5em;
    }

    .font-semi-large {
        font-size: 1.15em;
    }

    .font-grey {
        color: #9E9E9E;
    }

    .dropdown-menu>li>a:hover {
        color: #333
    }

    .label-grey {
        background-color: #CFD8DC;
    }

    tr.discount-active td {
        background-color: #C8E6C9;
    }


    /* Sweetalert */

    .swal-shown {
        height: auto !important;
    }

    .custom-swal {
        z-index: 10000 !important;
    }

    .custom-wide-swal {
        width: 640px !important;
        font-size: 13px !important;
    }

    /* Select2 tweak */

    .input-group .select2-container,
    .form-group .select2-container {
        position: relative !important;
        z-index: 2;
        float: left !important;
        width: 100% !important;
        margin-bottom: 0 !important;
        display: table !important;
        table-layout: fixed !important;
    }

    /* Modal tweak */

    .modal-open {
        overflow: hidden;
        position: fixed;
        width: 100%;
    }


    /* Centering the modal */

    .modal {
        text-align: center;
        padding: 0 !important;
    }

    .modal:before {
        content: '';
        display: inline-block;
        height: 100%;
        vertical-align: middle;
        margin-right: -4px;
    }

    .modal-dialog {
        display: inline-block;
        text-align: left;
        vertical-align: middle;
    }
</style>
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" />
<link rel="stylesheet" href="//cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
@endsection

@section('content')
<!-- table -->
<div class="box box-default">
    <div class="box-header">
        <h3 class="box-title">{!! $boxTitle !!}</h3>
    </div><!-- /.box-header -->
    <div class="box-body row">
        <div class="col-md-12">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <a href="{{ URL::route($createAction, $designerId) }}">
                        <button type="submit" class="btn-add btn btn-sm btn-primary">
                            <i class="fa fa-plus">&nbsp;</i>Add Media</button>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <table id="tableListStyle" class="table table-striped">
                <thead>
                    <tr>
                        <th class="text-center">Type</th>
                        <th class="text-center" width="10%">Media</th>
                        <th width="40%" style="padding-left:25px!important;">Description</th>
                        <th width="40%" style="padding-left:40px!important;"><span class="label label-sm label-success">New</span>   Photo group</th>
                        <th class="text-center">Source</th>
                        <th class="text-center">Uploaded</th>
                        <th class="text-center" style="margin-right:100px!important;"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($rowsStyle as $rowStyleKey => $rowStyle)
                    <tr>
                        <td class="text-center">
                            @if ($rowStyle->type == 'quiz')
                            <i class="fa fa-gift fa-2x "></i><br><small>Quiz</small>
                            @elseif ($rowStyle->type == 'text')
                            <i class="fa fa-text-height fa-2x"></i><br><small>Text</small>
                            @else
                            <i class="fa fa-photo fa-2x font-grey"></i><br><small>Photo</small>
                            @endif
                        </td>
                        <td class="text-center">
                            @if ($rowStyle->type == 'image')
                                <a href="{{ $rowStyle->photo_url['large'] }}" data-fancybox="gallery">
                                    <img width="160" class="img-thumbnail" src="{{ $rowStyle->photo_url['small'] }}"
                                    alt="{{ $rowStyle->description }}"
                                    data-src="holder.js/150x84?text=Foto \n Tidak \n Ditemukan">
                                </a>
                            @endif
                        </td>
                        <td style="padding-left:25px!important;">
                            <span class="font-semi-large">{{ $rowStyle->description }}</span>
                            @if ( $rowStyle->is_cover || strpos($rowStyle->description, '-cover') !== FALSE)
                                <br><span class="label label-{{ $designer->photo_id == $rowStyle->photo_id ? 'success' : 'default' }}"><i class="fa fa-bookmark"></i> Cover Photo</span>
                            @endif
                        </td>
                        <td style="padding-left:25px!important;">
                            @if ($rowStyle->type == 'image')
                                <div class="col-sm-10" id="{{ $rowStyle->id }}">
                                    @if ( $rowStyle->is_cover || strpos($rowStyle->description, '-cover') !== FALSE)
                                    <select class="form-control" name="photo_group" disabled>
                                        @foreach ($photoGroup as $group => $value)
                                            <option value="{{ $group }}" @if ($rowStyle->photo_group_name == $group) selected @endif>{{ $value }}</option>
                                        @endforeach
                                    </select>
                                    @else
                                    <select class="form-control" name="photo_group" >
                                        @foreach ($photoGroup as $group => $value)
                                            <option value="{{ $group }}" @if ($rowStyle->photo_group_name == $group) selected @endif>{{ $value }}</option>
                                        @endforeach
                                    </select>
                                    @endif
                                </div>
                            @endif
                        </td>
                        <td class="text-center">{{ $rowStyle->source }}</td>
                        <td class="text-center">{{ \Carbon\Carbon::parse($rowStyle->created_at)->format('d M Y') }}</td>
                        <td class="table-action-column text-center">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default btn-sm dropdown-toggle"
                                    data-toggle="dropdown">
                                    <i class="glyphicon glyphicon-cog"></i> Actions
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                    @if ($rowStyle->type == 'image')
                                        @if (strpos($rowStyle->description, '-cover') === FALSE)
                                        <li>
                                            <a href="{{ route($coverAction, $rowStyle->id) }}" title="Set As Cover">
                                                <i class="fa fa-bookmark"></i> Set As Cover Photo
                                            </a>
                                        </li>
                                        @endif
                                        <li>
                                            <a href="{{ route($rotateAction, array($rowStyle->id)) }}" title="Rotate Image">
                                                <i class="fa fa-rotate-right"></i> Rotate Image
                                            </a>
                                        </li>
                                            <li>
                                                <a href="{{ route($previewAction, array($rowStyle->id)) }}" data-fancybox="gallery" title="Preview Image">
                                                    <i class="fa fa-picture-o"></i> Preview Image
                                                </a>
                                            </li>
                                    @endif
                                    <li>
                                        <a href="{{ route($editAction, array($rowStyle->designer_id, $rowStyle->id)) }}"
                                            title="Edit Media">
                                            <i class="fa fa-pencil"></i> Edit Media
                                        </a>
                                    </li>
                                    <?php $description = str_replace(",","",str_replace('"','',$rowStyle->description)); ?>
                                    @if (strpos($rowStyle->description, '-cover') === FALSE)
                                    <li class="text-danger">
                                        <a href="{{ route($deleteAction, [$designerId, $rowStyle->id]) }}"
                                            title="Delete Media">
                                            <i class="fa fa-trash-o"></i> Remove Media
                                        </a>
                                    </li>
                                    @endif
                                </ul>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div><!-- /.box-body -->
</div><!-- /.box -->
<!-- table -->

@stop
@section('script')
<!-- page script -->
<script src="//cdn.jsdelivr.net/npm/holderjs@2.9.6/holder.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.js"></script>
<script src="//cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
<script type="text/javascript">
    // Sweetalert functions
    function triggerAlert(type, message) {
        Swal.fire({
            type: type,
            customClass: {
                container: 'custom-swal'
            },
            title: message
        });
    }

    function triggerLoading(message) {
        Swal.fire({
            title: message,
            customClass: {
                container: 'custom-swal'
            },
            allowEscapeKey: false,
            allowOutsideClick: false,
            onOpen: () => {
                swal.showLoading();
            }
        });
    }

    $(function()
    {
        // helper variable
        var _designerId, _checkerId;

        $('#setByMamichecker').click(function(){
            _designerId = $(this).data('designer');
            $('#modal').modal('show');
        });

        $('#unsetByMamichecker').click(function(){
            var trackerId = $(this).data('tracker');

            Swal.fire({
                title: 'Hapus Label Mami-Checker dari video ini?',
                type: 'warning',
                buttonsStyling: false,
                confirmButtonClass: 'btn btn-danger',
                showCancelButton: true,
                cancelButtonClass: 'btn btn-default',
                showLoaderOnConfirm: true,
                confirmButtonText: 'Yes, remove it!',
                preConfirm: (val) => {
                    return $.ajax({
                        type: 'POST',
                        url: "/admin/mami-checker/untrack",
                        dataType: 'json',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: {
                            'id': trackerId,
                        },
                        success: (response) => {
                            return response;
                        },
                        error: (error) => {
                            triggerAlert('error', error.message);
                        }
                    });
                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                var res = result.value;
                if (res.success) {
                    Swal.fire({
                        type: 'success',
                        title: "Label berhasil dihapus",
                        text: "Halaman akan di-refresh setelah Anda klik OK",
                        onClose: () => {
                            window.location.reload();
                        }
                    });
                } else {
                    triggerAlert('error', res.message);
                }
            })
        });

        $('#modal')
        .on("show.bs.modal", (e) => {
            // function to populate checker profile
            function populateCheckerProfile(id) {
                $('#checkerProfile').html('');

                var html = "";

                triggerLoading('Mengambil data user..');

                $.ajax({
                    type: 'POST',
                    url: "/admin/mami-checker/checker",
                    data: {
                        id: id
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: (data) => {
                        if(data != null) {
                            var html = '<div class="panel panel-default">'+
                                    '<div class="panel-body">'+
                                        '<div class="row">'+
                                            '<div class="col-md-5 text-center">'+
                                                '<img class="img-circle" src="'+data.photo_url.small+'" width="150">'+
                                            '</div>'+
                                            '<div class="col-md-7">'+
                                                '<h3>'+data.user.name+'</h3>'+
                                                '<small>Email</small>'+
                                                '<p>'+data.user.email+'</p>'+
                                                '<small>Phone</small>'+
                                                '<p>'+data.user.phone_number+'</p>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>';
                
                            $('#checkerProfile').append(html);
                            $('#submit').removeClass('hidden');

                            _checkerId = data.id;
                        }

                        if (Swal.isLoading()) Swal.close();
                    },
                    error: (error) => {
                        triggerAlert('error', error);
                    }
                });
            }

            // initiate selector
            $('#checker').select2({
                placeholder: 'Pilih profil checker',
                language: {
                    searching: function() {
                        return "Sedang mengambil daftar checker...";
                    },
                    inputTooShort: function() {
                        return 'Ketik nama untuk mencari..';
                    },
                    noResults: function() {
                        return "Profil checker tidak ditemukan! Coba cari nama yang lain.";
                    }
                },
                ajax: {
                    url: '/admin/mami-checker/all',
                    data: function (params) {
                        return {
                            q: params.term,
                            page: params.page || 1
                        }
                    },
                    processResults: function (data, params) {
                        var page = params.page || 1;
                        if (data.data.length < 1) var total_count = 0;
                        else var total_count = data.data[0].total_count;
                        return {
                            results: $.map(data.data, item => { 
                                return {
                                    id: item.id, 
                                    text: item.user.name + ' (' + item.user.email + ')'
                                }
                            }),
                            pagination: {
                                more: (page * 10) <= total_count
                            }
                        };
                    },
                    cache: true
                },
                minimumInputLength: 3,
                dropdownParent: $("#modal")
            });

            // selector listener
            $("#checker").on('select2:select', (e) => {
                var selected = e.params.data.id;
                populateCheckerProfile(selected);
            });
        })
        .on("hidden.bs.modal", (e) => {
            // destroy selector
            $('#checker').empty().select2('destroy');
            $('#submit').addClass('hidden');

            _checkerId = 0;
            _designerId = 0;
        });

        $('#checkerSubmit').click(function(){
            triggerLoading('Menyimpan data..');
            
            $.ajax({
                type: 'POST',
                url: '/admin/mami-checker/track',
                dataType: 'json',
                data: {
                    checkerId: _checkerId,
                    designerId: _designerId,
                    action: 'video-tour'
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: (res) => {
                    if (!res.success) {
                        triggerAlert('error', 'Error: ' + res.message);
                        return;
                    } 
                    
                    $('#modal').modal('hide');
                    Swal.fire({
                        type: 'success',
                        title: "Video berhasil diset By Mamichecker",
                        text: "Halaman akan di-refresh setelah Anda klik OK",
                        onClose: () => {
                            window.location.reload();
                        }
                    });
                },
                error: (error) => {
                    triggerAlert('error', 'Error: ' + error.message);
                }
            });
        });

    });

    $("select[name='photo_group'").on('change', function postinput(){
        var group = $(this).val(); 
        var styleId = $(this).parent().attr('id');
        var url = `{!! $photoGroupUpdate !!}`; 

        triggerLoading('Sedang Melakukan Perubahan Photo Group..');

        $.ajax({
                type: 'PUT',
                url: url,
                dataType: 'json',
                data: {
                    id: styleId,
                    group: group,
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: (res) => {
                    if (!res.success) {
                        triggerAlert('error', 'Error: ' + res.message);
                        window.location.reload();
                        return;
                    } 
                    
                    triggerAlert('success', 'Success: ' + res.message);
                    window.location.reload();
                },
                error: (error) => {
                    triggerAlert('error', 'Error: ' + error.message);
                    window.location.reload();
                }
            });          
    });

</script>
@stop