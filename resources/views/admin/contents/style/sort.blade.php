@extends('admin.layouts.main')
@section('content')
	<div class="box box-primary">
		<div class="box-header">
			<h3 class="box-title">{{ $boxTitle }}</h3>
		</div>
		<div class="box-body no-padding">
			<ol id="sortable-designer">
			    @foreach($rowsStyleJustTitle as $style)
			        <li class="ui-state-default" id="{{$style->id}}">
			                <?php $tempMedia = Media::getMediaUrl($style['photo_id']); ?>
			                <img width="30" class="img-thumbnail" src="{{ $tempMedia['medium']  }}" data-src="holder.js/60x77/thumbnail" alt="">
			            <b>{{$style->title}}</b>
			        </li>
			    @endforeach 
			</ol> 
		</div>
		<div class="box-body">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			<button type="button" onclick="submitSaveOrder()" class="btn btn-primary">Save order</button>
		</div>
	</div>
	
	{{ Form::open(array('url' => route('admin.designer.style.sort'),'method'=>'post','id'=>'form-order')) }}
	    <input type="hidden" value="" name="order" id="order-designer" />
	{{ Form::close() }}
@stop
@section('script')
	<script type="text/javascript">
        function submitSaveOrder(){
        	// sortable to array creating array of new order, eg. (1,3,2,4)
        	// that means swap the position 2 and 3
            $('#order-designer').val($('#sortable-designer').sortable('toArray'));
            $('#form-order').submit();
        }

        $(function() {
            $("#sortable-designer").sortable();
            $("#sortable-designer").disableSelection();
        });
    </script>
@stop