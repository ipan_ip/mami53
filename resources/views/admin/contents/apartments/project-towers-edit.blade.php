@extends('admin.layouts.main')

@section('content')
	<div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

		<form action="{{ URL::route('admin.apartment.project.tower.update', $apartmentProjectTower->id) }}" method="POST" class="form-horizontal form-bordered">
			<div class="box-body no-padding">
				<div class="form-group bg-default">
					<label for="name" class="control-label col-sm-2">Nama Tower</label>
					<div class="col-sm-10">
						<input type="text" name="name" id="name" class="form-control" value="{{ old('name', $apartmentProjectTower->name) }}">
					</div>
				</div>

				<div class="form-group bg-default">
					<label for="status" class="control-label col-sm-2">Status</label>
					<div class="col-sm-10">
						<input type="text" name="status" id="status" class="form-control" value="{{ old('status', $apartmentProjectTower->status) }}">
					</div>
				</div>

				<div class="form-group bg-default">
					<div class="col-sm-10 col-sm-push-2">
						<button type="submit" class="btn btn-primary">Save</button>
					</div>
				</div>
			</div>
		</form>
    </div>
@endsection