@extends('admin.layouts.main')

@section('content')
<div class="box box-primary">
	<div class="box-header">
        <h3 class="box-title">{{ $boxTitle }}</h3>
    </div>

    <form action="/admin/apartment/parse-simple" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="form-horizontal">
    	<div class="box-body no-padding">
    		<div class="form-group bg-default">
	            <label for="file-csv" class="col-sm-2 control-label">File CSV</label>
	            <div class="col-sm-10">
	                <input type="file" name="file_csv" id="file-csv" class="form-control">
	            </div>
	        </div>

	    	<div class="form-group bg-default">
	    		<div class="col-sm-10 col-sm-push-2">
	    			<button type="submit" class="btn btn-primary">Parse</button>
	    		</div>
	        </div>    
    	</div>

    	
    </form>
</div>
@endsection