@extends('admin.layouts.main')
@section('content')
    <!-- table -->
    <div class="box box-primary">

        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div><!-- /.box-header -->

        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    
                    <a href="{{ route('admin.apartment.project.type.create', $apartmentProject->id) }}" class="btn btn-primary btn-sm pull-left">
                        <i class="fa fa-plus">&nbsp;</i>Add Apartment Project Type
                    </a>
                    
                </div>
            </div>

            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nama</th>
                        <th>Kamar</th>
                        <th>Kamar Mandi</th>
                        <th>Luas</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($apartmentProjectTypes as $apartmentProjectType)
                        <tr>
                            <td>{{ $apartmentProjectType->id }}</td>
                            <td>{{ $apartmentProjectType->type_name }}</td>
                            <td>{{ $apartmentProjectType->bedroom_count }}</td>
                            <td>{{ $apartmentProjectType->bathroom_count }}</td>
                            <td>{{ $apartmentProjectType->size }}</td>
                            <td>
                                <a href="{{ route('admin.apartment.project.type.edit', $apartmentProjectType->id) }}" title="edit"><i class="fa fa-pencil"></i></a>

                                <a href="{{ route('admin.apartment.project.type.delete', $apartmentProjectType->id) }}" title="edit"><i class="fa fa-ban"></i></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

        </div><!-- /.box-body -->
    </div><!-- /.box -->
    <!-- table -->

@endSection
