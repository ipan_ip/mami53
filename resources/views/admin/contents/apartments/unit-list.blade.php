@extends('admin.layouts.main')
@section('content')
    <!-- table -->
    <div class="box box-primary">

        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div><!-- /.box-header -->

        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <form action="" class="form-inline" method="get" style="text-align:right;padding:10px;">
                        <a href="{{ route('admin.apartment.unit.create', $apartmentProject->id) }}" class="btn btn-primary btn-sm pull-left">
                            <i class="fa fa-plus">&nbsp;</i>Add Apartment Unit
                        </a>

                        <input type="text" name="phone" class="form-control input-sm" id="phone"  placeholder="Phone number" autocomplete="off" value="{{ request()->input('phone') }}">

                        <input type="text" name="q" class="form-control input-sm"  placeholder="Keywords" autocomplete="off" value="{{ request()->input('q') }}">
                        
                        <button class="btn btn-primary btn-md" id="buttonSearch"><i class="fa fa-search">&nbsp;</i>Search</button>
                    </form>
                </div>
            </div>

            @if ($telp_now)
            <div class="col-md-12" style="margin-bottom: 20px;">
                <input type="hidden" id="user_login" value="{{ Auth::user()->email }}">
                <input type="button" class="col-md-12 btn btn-md btn-danger" id="telp_now" value="Telp Now" />
            </div>
            @endif

            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Photo</th>
                        <th>Nama Unit</th>
                        <th>Owner Phone</th>
                        <th>Agent</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($apartmentUnits as $unit)
                        <tr>
                            <td>{{ $unit->id }}</td>
                            <td>
                                <img width="200" class="img-thumbnail" src="{{ !is_null($unit->photo) ? $unit->photo->getMediaUrl()['small'] : '' }}"
                                    data-src="holder.js/60x77/thumbnail" alt="">
                            </td>
                            <td>{{ $unit->name }}</td>
                            <td>{{ $unit->owner_phone }}</td>
                            <td>{{ $unit->agent_name }}</td>
                            <td>
                                <a href="{{ route('admin.apartment.unit.edit', $unit->id) }}" title="edit"><i class="fa fa-pencil"></i></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

        </div><!-- /.box-body -->
        <div class="box-body no-padding">
            {{ $apartmentUnits->appends(Request::except('page'))->links() }}
        </div>
    </div><!-- /.box -->
    <!-- table -->

@endSection

@section('script')

{{ Html::script('assets/vendor/holder/holder.js') }}
<script type="text/javascript">
    $(function() {
        
    });

        $('#telp_now').click(function(e) {
      phone = $("#phone").val();
      email = $("#user_login").val();

      //$("#loading-phone").append("Loading ................ ");
      $.ajax({
        url: '/admin/squarepants/notify?phone='+phone+"&email="+email+"&id=&kos_name=&search=search",
        type: 'GET',
        dataType: 'json',
        success: function(data) {
        //    console.log(data)
          //$("#loading-phone").empty();  
          if(data.status == false) {
            alert("Gagal ngirim nomer ke hp");
            //$("#loading-phone").append("<div class='alert alert-danger' style='width: 100%;'>Nomer hp tidak sesuai format.</div>");
          } else {
            alert("Berhasil ngirim nomer ke hp");
            //$("#loading-phone").append("<div class='alert alert-success' style='width: 100%;'>Cek hp anda.");
          }
        }
      });
    });
</script>
@endSection
