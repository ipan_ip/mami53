@extends('admin.layouts.main')
@section('content')
    <!-- table -->
    <div class="box box-primary">

        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div><!-- /.box-header -->

        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    
                    <a href="{{ route('admin.apartment.project.tower.create', $apartmentProject->id) }}" class="btn btn-primary btn-sm pull-left">
                        <i class="fa fa-plus">&nbsp;</i>Add Apartment Project Tower
                    </a>
                    
                </div>
            </div>

            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nama</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($apartmentProjectTowers as $apartmentProjectTower)
                        <tr>
                            <td>{{ $apartmentProjectTower->id }}</td>
                            <td>{{ $apartmentProjectTower->name }}</td>
                            <td>{{ $apartmentProjectTower->status }}</td>
                            <td>
                                <a href="{{ route('admin.apartment.project.tower.edit', $apartmentProjectTower->id) }}" title="edit"><i class="fa fa-pencil"></i></a>

                                <a href="{{ route('admin.apartment.project.tower.delete', $apartmentProjectTower->id) }}" title="edit"><i class="fa fa-ban"></i></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

        </div><!-- /.box-body -->
    </div><!-- /.box -->
    <!-- table -->

@endSection
