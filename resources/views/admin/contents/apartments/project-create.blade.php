@extends('admin.layouts.main')

@section('style')
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/dropzone.css">
	{{ HTML::style('assets/vendor/jquery-chosen/chosen.css') }}
	<link rel="stylesheet" href="/assets/vendor/summernote/summernote.css?v=05042018">
    <link rel="stylesheet" href="{{ mix_url('dist/vendor/leaflet/leaflet.css') }}">
	
	<style>
        .chosen-choices {
            border: 1px solid #ccc;
            border-radius: 4px;
            min-height: 34px;
            padding: 6px 12px;
        }

        .note-group-select-from-files {
    		display: none;
    	}
    </style>
@endsection

@section('content')
	<div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

		<form action="{{ URL::route('admin.apartment.project.store') }}" method="POST" class="form-horizontal form-bordered">
			<div class="box-body no-padding">
				<div class="form-group bg-default">
					<label for="name" class="control-label col-sm-2">Nama Project</label>
					<div class="col-sm-10">
						<input type="text" name="name" id="name" value="{{ old('name') }}" class="form-control">
					</div>
				</div>

				<div class="form-group bg-default">
					<label for="address" class="control-label col-sm-2">Alamat</label>
					<div class="col-sm-10">
						<input type="text" name="address" id="address" class="form-control" value="{{ old('address') }}">
					</div>
				</div>

				<div class="form-group bg-default">
					<label for="description" class="control-label col-sm-2">Deskripsi</label>
					<div class="col-sm-10">
						<textarea name="description" id="description" class="form-control" rows="5">{{ old('description') }}</textarea>
					</div>
				</div>

				<div class="form-group bg-default">
	                <label for="inputGeoName" class="col-sm-2 control-label">Koordinat</label>
	                <div class="col-sm-10">
	                    <input type="text" class="form-control" placeholder="Geo Name"
	                           id="inputGeoName" name="geo_name">
	                </div>
	                <div class="col-sm-10 col-sm-offset-2">
	                    <div style="background: #EEE; width: 100%; height: 300px;">
	                        <div id="map-canvas"></div>
	                    </div>
	                </div>
	                <div class="col-sm-10 col-sm-offset-2">
	                	<div class="input-group">
	                		<input type="text" class="form-control" placeholder="latitude"
                               id="latitude" name="latitude" value="{{ old('latitude') }}">
                        	<input type="text" class="form-control" placeholder="longitude"
                               id="longitude" name="longitude" value="{{ old('longitude') }}">
                       </div>
	                </div>
	            </div>

				<div class="form-group bg-default">
					<label for="subdistrict" class="control-label col-sm-2">Subdistrik</label>
					<div class="col-sm-10">
						<input type="text" name="subdistrict" id="subdistrict" class="form-control" value="{{ old('subdistrict') }}">
					</div>
				</div>

				<div class="form-group bg-default">
					<label for="area_city" class="control-label col-sm-2">Kota / Daerah</label>
					<div class="col-sm-10">
						<input type="text" name="area_city" id="area_city" class="form-control input-city-regency" value="{{ old('area_city') }}" data-toggle="dropdown">
					</div>
				</div>

				<div class="form-group bg-default">
					<label for="phone_number_building" class="control-label col-sm-2">Nomor Telepon Gedung</label>
					<div class="col-sm-10">
						<input type="text" name="phone_number_building" id="phone_number_building" class="form-control" value="{{ old('phone_number_building') }}">
					</div>
				</div>

				<div class="form-group bg-default">
					<label for="phone_number_marketing" class="control-label col-sm-2">Nomor Telepon Marketing</label>
					<div class="col-sm-10">
						<input type="text" name="phone_number_marketing" id="phone_number_marketing" class="form-control" value="{{ old('phone_number_marketing') }}">
					</div>
				</div>

				<div class="form-group bg-default">
					<label for="phone_number_other" class="control-label col-sm-2">Nomor Telepon Lainnya</label>
					<div class="col-sm-10">
						<input type="text" name="phone_number_other" id="phone_number_other" class="form-control" value="{{ old('phone_number_other') }}">
					</div>
				</div>

				<div class="form-group bg-default">
					<label for="developer" class="control-label col-sm-2">Pengembang</label>
					<div class="col-sm-10">
						<input type="text" name="developer" id="developer" class="form-control" value="{{ old('developer') }}">
					</div>
				</div>

				<div class="form-group bg-default">
					<label for="unit-count" class="control-label col-sm-2">Jumlah Unit</label>
					<div class="col-sm-10">
						<input type="number" name="unit_count" id="unit-count" class="form-control" value="{{ old('unit_count') }}">
					</div>
				</div>

				<div class="form-group bg-default">
					<label for="floor-count" class="control-label col-sm-2">Jumlah Lantai</label>
					<div class="col-sm-10">
						<input type="number" name="floor_count" id="floor-count" class="form-control" value="{{ old('floor_count') }}">
					</div>
				</div>

				<div class="form-group bg-default">
					<label for="size" class="control-label col-sm-2">Ukuran (m<sup>2</sup>)</label>
					<div class="col-sm-10">
						<input type="number" name="size" id="size" class="form-control" value="{{ old('size') }}">
					</div>
				</div>

				<div class="form-group bg-default">
					<label for="facilities" class="control-label col-sm-2">Fasilitas</label>
					<div class="col-sm-10">
						 <select class="form-control chosen-select" multiple id="facilities" name="facilities[]">
	                        @foreach($tags as $tag)
								<option value="{{ $tag->id }}">{{ $tag->name }}</option>
	                        @endforeach
	                    </select>
					</div>
				</div>

				<div class="form-group bg-default">
					<label for="coverphoto" class="control-label col-sm-2">Foto</label>
					<div class="col-sm-10">
						<div id="coverphoto"
						      action="{{ url('admin/media') }}"
						      method="POST"
						      class="dropzone"
						      uploadMultiple="no">
						    {{ csrf_field() }}
						    <input type="hidden" name="media_type" value="style_photo">
						</div>
					</div>
				</div>

				<div id="photo-cover-wrapper"></div>


				<div class="form-group bg-default">
					<label for="otherphoto" class="control-label col-sm-2">Foto</label>
					<div class="col-sm-10">
						<div id="otherphoto"
						      action="{{ url('admin/media') }}"
						      method="POST"
						      class="dropzone"
						      uploadMultiple="no">
						    {{ csrf_field() }}
						    <input type="hidden" name="media_type" value="style_photo">
						</div>
					</div>
				</div>

				<div id="photo-other-wrapper"></div>

				<div class="form-group bg-default">
					<div class="col-sm-10 col-sm-push-2">
						<button type="submit" class="btn btn-primary">Save</button>
					</div>
				</div>
			</div>
		</form>

		
    </div>
@endsection

@section('script')
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&language=id&key={{ENV('MIX_GOOGLE_MAPS_JAVASCRIPT_API_KEY')}}"></script>
{{ HTML::script('assets/vendor/jquery-chosen/chosen.jquery.js') }}
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/dropzone.js"></script>
<script src="/assets/vendor/summernote/summernote.min.js"></script>
<script src="{{ mix_url('dist/vendor/leaflet/leaflet.js') }}"></script>


<script>
    centerPos = {lat: -7.7858485, lng: 110.3680087}

    var map = L.map('map-canvas', {
        // Set latitude and longitude of the map center (required)
        center: centerPos, 
        // Set the initial zoom level, values 0-18, where 0 is most zoomed-out (required)
        zoom: 12
    });

    var tiles = new L.tileLayer("{{ config('services.osm.host') }}/hot/{z}/{x}/{y}.png").addTo(map);

    var marker = new L.Marker(centerPos, {
        draggable: true
    }).addTo(map);

    var autocomplete = new google.maps.places.Autocomplete(document.getElementById('inputGeoName'));
    autocomplete.setFields(['address_components', 'geometry']);

    var geocoder = new google.maps.Geocoder();

    marker.on('dragend', function(evt) {
        var latlng = evt.target.getLatLng();

        var lat = latlng.lat;
        var lng = latlng.lng;

        geocoder.geocode({'latLng': latlng}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[1]) {

                    map.setView(latlng);

                    $('#inputGeoName').val(results[1].formatted_address);
                    $('#latitude').val(lat);
                    $('#longitude').val(lng);

                    setDistrictAndCity(results[0]);
                } else {
                    alert('No results found');
                }
            } else {
                alert('Geocoder failed due to: ' + status);
            }
        });
    });


    autocomplete.addListener('place_changed', function() {
        marker.setOpacity(0);
        var place = autocomplete.getPlace();
        if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place.name + "'");
            return;
        }

        latlng = {
            lat: place.geometry.location.lat(),
            lng: place.geometry.location.lng()
        };

        map.setView(latlng);
        marker.setLatLng(latlng);
        marker.setOpacity(1);

        $('#latitude').val(place.geometry.location.lat);
        $('#longitude').val(place.geometry.location.lng);

        setDistrictAndCity(place);
    });

    function setDistrictAndCity(response) {
        subDistrict = '';
        city = '';
        response.address_components.forEach(function(component) {
            // console.log(component.types);
            if ($.inArray("administrative_area_level_3", component.types) != -1) {
                subDistrict = component.short_name;
            }

            if ($.inArray("administrative_area_level_2", component.types) != -1) {
                city = component.short_name;      
            }

        });

        $('#subdistrict').val(subDistrict);
        $('#area_city').val(city);
    }
</script>


<script>
	$(document).ready(function() {
		// centerPos = {lat: -7.7858485, lng: 110.3680087}

  //       var map = new google.maps.Map(document.getElementById("map-canvas"),
  // 			{
  // 				center: centerPos,
  // 				zoom:12
  // 			}
  // 		);

  // 		var marker = new google.maps.Marker({
		// 	position: centerPos,
		// 	map: map,
		// 	draggable: true,

  //       });

  //       var autocomplete = new google.maps.places.Autocomplete(document.getElementById('inputGeoName'));
  //       autocomplete.bindTo('bounds', map);

  //       var geocoder = new google.maps.Geocoder();

  //       google.maps.event.addListener(marker, 'dragend', function(evt) {
		// 	var lat = evt.latLng.lat();
		// 	var lng = evt.latLng.lng();

		// 	var latlng = new google.maps.LatLng(lat, lng);

		// 	geocoder.geocode({'latLng': latlng}, function(results, status) {
		// 		if (status == google.maps.GeocoderStatus.OK) {
		// 			if (results[1]) {

		// 				map.setCenter(latlng);
		// 				marker.setPosition(latlng);

		// 				$('#inputGeoName').val(results[1].formatted_address);
		// 				$('#latitude').val(lat);
		// 				$('#longitude').val(lng);

		// 				setDistrictAndCity(results[0]);
		// 			} else {
		// 				alert('No results found');
		// 			}
		// 		} else {
		// 			alert('Geocoder failed due to: ' + status);
		// 		}
		// 	});
		// });


		// autocomplete.addListener('place_changed', function() {
		// 	marker.setVisible(false);
		// 	var place = autocomplete.getPlace();
		// 	if (!place.geometry) {
		// 		// User entered the name of a Place that was not suggested and
		// 		// pressed the Enter key, or the Place Details request failed.
		// 		window.alert("No details available for input: '" + place.name + "'");
		// 		return;
		// 	}

		// 	map.setCenter(place.geometry.location);
		// 	marker.setPosition(place.geometry.location);
		// 	marker.setVisible(true);

		// 	$('#latitude').val(place.geometry.location.lat);
		// 	$('#longitude').val(place.geometry.location.lng);

		// 	setDistrictAndCity(place);
		// });

  //       function setDistrictAndCity(response) {
  //       	subDistrict = '';
  //       	city = '';
  //       	response.address_components.forEach(function(component) {
		// 		// console.log(component.types);
		// 		if ($.inArray("administrative_area_level_3", component.types) != -1) {
		// 			subDistrict = component.short_name;
		// 		}

		// 		if ($.inArray("administrative_area_level_2", component.types) != -1) {
		// 			city = component.short_name;      
		// 		}

		// 	});

		// 	$('#subdistrict').val(subDistrict);
		// 	$('#area_city').val(city);
  //       }




        var config = {
            '.chosen-select'           : {width: '100%'}
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }

        Dropzone.autoDiscover = false;

        Dropzone.options.coverphoto = {
		    paramName : "media",
            maxFilesize : 6,
            maxFiles: 1,
            accepFiles: '.jpg, .jpeg, .png, .bmp',
            dictDefaultMessage : 'Upload photo for cover',
            addRemoveLinks: true,
            success: function(file, response) {
            	$('#photo-cover-wrapper').append('<input type="hidden" name="photo_cover" value="' +response.media.id+ '">');
        	},
        	init: function() {
		    	this.on('removedfile', function(file) {
		    		$('#photo-cover-wrapper').html('');
		    	});
		    }
		}

		$('#coverphoto').dropzone();

		Dropzone.options.otherphoto = {
			paramName : "media",
            maxFilesize : 6,
            accepFiles: '.jpg, .jpeg, .png, .bmp',
            dictDefaultMessage : 'Upload other photos',
            addRemoveLinks: true,
            success : function (file, response) {
                $('#photo-other-wrapper').append('<input type="hidden" name="photo_other[]" value="' +response.media.id+ '" data-filename="' + file.name + '">');
            },
            init: function() {
		    	this.on('removedfile', function(file) {
		    		$('input[name="photo_other[]"]').each(function(id, el) {
		    			if($(el).data('filename') == file.name) {
		    				$(el).remove();
		    			}
		    		});
		    	});
		    }
		}

		$('#otherphoto').dropzone();


        $('#description').summernote({
			height: 300,
			toolbar: [
				['style', ['style', 'bold', 'italic', 'underline', 'clear']],
				['font', ['strikethrough', 'superscript', 'subscript']],
				['color', ['color']],
				['para', ['ul', 'ol', 'paragraph']],
				['insert', ['link', 'table', 'hr']],
				['misc', ['codeview']]
			]
		});

        
	});
</script>
<script>
   function clickSuggestion(idx) {
        key = idx;
        val = $('.suggestion-item-' + idx).text();
        el = $('.suggestion-item-' + idx);

        $(el).parent().parent().prev('.input-city-regency').val(val);
    };

    var callSuggestionCity = function(e) {
            $.ajax({
                type: 'POST',
                url: '/admin/area-big-mapper/suggestion-city-regency',
                data: {
                    'params': e.target.value
                },
                success: function(data) {
                    $(e.target).next('.dropdown-menu').remove();

                    listOption = '<ul class="dropdown-menu suggestion-list" aria-labelledby="dLabel">'
                    for(idx in data.landings) {
                        listOption += '<li><a href="#" class="suggestion-item suggestion-item-' + idx + '" data-key="' + idx + '" onclick="clickSuggestion(' + idx + ')">' + data.landings[idx] + '</a></li>';
                    }

                    listOption += '</ul>';

                    $(listOption).insertAfter($(e.target));

                    $('.suggestion-list').dropdown();
                }
                
            });
        };

    var delay = (function(){
          var timer = 0;
          return function(callback, ms){
            clearTimeout (timer);
            timer = setTimeout(callback, ms);
          };
        })();

    $(document).on('keyup', '.input-city-regency', function(e) {
            delay(function(){
              callSuggestionCity(e)
            }, 1000 );
        });
</script>
@endsection