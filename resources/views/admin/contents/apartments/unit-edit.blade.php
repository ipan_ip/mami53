@extends('admin.layouts.main')

@section('style')
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/dropzone.css">
	{{ HTML::style('assets/vendor/jquery-chosen/chosen.css') }}
    {{ HTML::style('assets/vendor/jquery-chosen-custom/chosen.css') }}
    <link rel="stylesheet" href="/assets/vendor/summernote/summernote.css?v=05042018">

	
	<style>
        .chosen-choices {
            border: 1px solid #ccc;
            border-radius: 4px;
            min-height: 34px;
            padding: 6px 12px;
        }
    </style>
@endsection

@section('content')
	<div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

        <div class="alert alert-warning" role="alert">
        	Form ini hanya untuk mengedit data spesifik apartment saja, silakan gunakan form kost untuk mengedit data lain
        </div>

		<form action="{{ URL::route('admin.apartment.unit.update') }}" method="POST" class="form-horizontal form-bordered">
			<input type="hidden" name="unit_id" value="{{ $apartmentUnit->id }}">
			
			<div class="box-body no-padding">
				<div class="form-group bg-default">
					<label for="name" class="control-label col-sm-2">Nama</label>
					<div class="col-sm-10">
						<p class="form-control-static">{{$apartmentUnit->name}}</p>
					</div>
				</div>

				<div class="form-group bg-default">
					<label for="project_name" class="control-label col-sm-2">Nama Project</label>
					<div class="col-sm-10">
						<p class="form-control-static">{{$apartmentProject->name}}</p>
					</div>
				</div>

				<div class="form-group bg-default">
	                <label for="inputRentType" class="col-sm-2 control-label">Apartment Project</label>
	                <div class="col-sm-10">
	                    <select name="project_id" id="project_id" class="form-control chosen-select">
	                        @foreach($apartment_active as $active_apart)
	                            <option value="{{ $active_apart->id }}" {{ old('project_id', $apartmentUnit->apartment_project_id) == $active_apart->id ? 'selected="selected"' : '' }}>{{ $active_apart->name }}</option>
	                        @endforeach
	                    </select>
	                    <span class="help-block">
							Untuk memindahkan kamar ke apartment lain, silakan pilih apartment tujuan di field ini.
						</span>
	                </div>
	            </div>

				<div class="form-group bg-default">
					<label for="unit_type" class="control-label col-sm-2">Tipe Unit</label>
					<div class="col-sm-10">
						<select name="unit_type" class="form-control" id="unit_type">
							@foreach($typeOptions as $type)
								<option value="{{ $type }}" {{ old('unit_type', $apartmentUnit->unit_type) == $type ? 'selected="selected"' : ($type == 'Lainnya' && !in_array(old('unit_type', $apartmentUnit->unit_type), $typeOptions) ? 'selected="selected"' : '') }}>{{ $type }}</option>
							@endforeach
						</select>
					</div>
				</div>

				<div class="form-group bg-default">
					<label for="unit_type_name" class="control-label col-sm-2">Tipe Unit (custom)</label>
					<div class="col-sm-10">
						<input type="text" name="unit_type_name" id="unit_type_name" value="{{ old('unit_type_name', !in_array($apartmentUnit->unit_type, $typeOptions) ? $apartmentUnit->unit_type : '') }}" class="form-control">
					</div>
				</div>

				<div class="form-group bg-default">
					<label for="unit_number" class="control-label col-sm-2">Nomor Unit</label>
					<div class="col-sm-10">
						<input type="text" name="unit_number" id="unit_number" value="{{ old('unit_number', $apartmentUnit->unit_number) }}" class="form-control">
					</div>
				</div>

				<div class="form-group bg-default">
					<label for="floor" class="control-label col-sm-2">Lantai</label>
					<div class="col-sm-10">
						<input type="text" name="floor" id="floor" value="{{ old('floor', $apartmentUnit->floor) }}" class="form-control">
					</div>
				</div>

				<div class="form-group bg-default">
                    <label for="is_furnished" class="control-label col-sm-2">Furnished</label>
                    <div class="col-sm-10">
                        <select name="is_furnished" class="form-control" id="is_furnished">
                            @foreach($furnishedOptions as $key => $option)
                                <option value="{{ $key }}" {{ $apartmentUnit->furnished == $key ? 'selected="selected"' : '' }}>{{ $option }}</option>
                            @endforeach;
                        </select>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="price_shown" class="control-label col-sm-2">Harga yang Ditampilkan</label>
                    <div class="col-sm-10">
                        <label class="checkbox-inline">
                            <input type="checkbox" name="price_shown[]" value="daily" {{ in_array('daily', old('price_shown', $apartmentUnit->price_shown)) ? 'checked="checked"' : '' }}> Harian
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" name="price_shown[]" value="weekly" {{ in_array('weekly', old('price_shown', $apartmentUnit->price_shown)) ? 'checked="checked"' : '' }}> Mingguan 
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" name="price_shown[]" value="monthly" {{ in_array('monthly', old('price_shown', $apartmentUnit->price_shown)) ? 'checked="checked"' : '' }}> Bulanan 
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" name="price_shown[]" value="yearly" {{ in_array('yearly', old('price_shown', $apartmentUnit->price_shown)) ? 'checked="checked"' : '' }}> Tahunan 
                        </label>
                    </div>
                </div>

				<div class="form-group bg-default">
					<label for="maintenance_price" class="control-label col-sm-2">Biaya Maintenance / Bulan</label>
					<div class="col-sm-10">
						<input type="text" name="maintenance_price" id="maintenance_price" value="{{ old('maintenance_price', $maintenance_price) }}" class="form-control">
					</div>
				</div>

				<div class="form-group bg-default">
					<label for="parking_price" class="control-label col-sm-2">Biaya Parkir</label>
					<div class="col-sm-10">
						<input type="text" name="parking_price" id="parking_price" value="{{ old('parking_price', $parking_price) }}" class="form-control">
					</div>
				</div>

				<div class="form-group bg-default">
					<div class="col-sm-10 col-sm-push-2">
						<button type="submit" class="btn btn-primary">Save</button>
					</div>
				</div>
			</div>
		</form>

		
    </div>

@endsection

@section('script')
{{ HTML::script('assets/vendor/jquery-chosen/chosen.jquery.js') }}
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/dropzone.js"></script>
<script src="/assets/vendor/summernote/summernote.min.js"></script>
<script>
    var config = {
        '.chosen-select'           : {width: '100%'}
    };

    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>

<script>
	$(function() {
		$('#unit_type').on('change', function() {
			if($(this).val() == 'Lainnya') {
				$('#unit_type_name').removeAttr('disabled');
			} else {
				$('#unit_type_name').attr('disabled', 'disabled');
			}
		});

		$('#unit_type').trigger('change');
	});
</script>
@endsection