@extends('admin.layouts.main')

@section('style')
<style>
    span.project-not-complete,
    tr.project-not-complete td {
        color: #ff0000;
        font-weight: bold;
    }
</style>
@endsection


@section('content')
    <!-- table -->
    <div class="box box-primary">

        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div><!-- /.box-header -->

        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <form action="" class="form-inline" method="get" style="text-align:right;padding:10px;">
                        <a href="{{ route('admin.apartment.project.create') }}" class="btn btn-primary btn-sm pull-left">
                            <i class="fa fa-plus">&nbsp;</i>Add Apartment Project
                        </a>

                        <input type="text" name="q" class="form-control input-sm"  placeholder="Nama Project"  autocomplete="off" value="{{ request()->input('q') }}">
                        
                        <button class="btn btn-primary btn-md" id="buttonSearch"><i class="fa fa-search">&nbsp;</i>Search</button>
                    </form>
                </div>
            </div>

            <p class="helper-block">
                Warna <span class="project-not-complete"><strong>Merah</strong></span> menandakan data belum lengkap
            </p>

            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Photo</th>
                        <th>Nama Project</th>
                        <th>Landing Redirect URL</th>
                        <th>Status</th>
                        <th>Jumlah Unit</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($apartmentProjects as $project)
                        <tr class="{{ !is_null($project->area_city) && $project->area_city != '' ? '' : 'project-not-complete' }}">
                            <td>{{ $project->id }}</td>
                            <td>
                                <img width="200" class="img-thumbnail" src="{{ !is_null($project->styles->first()) ? $project->styles->first()->photo->getMediaUrl()['small'] : '' }}"
                                    data-src="holder.js/60x77/thumbnail" alt="">
                            </td>
                            <td>{{ $project->name }}</td>
                            <td><a href="{{ url('/apartemen/'.$project->redirect['area_city'].'/'.$project->redirect['slug']) }}">{{ $project->redirect->name ?? '' }}</a></td>
                            <td>
                             @if($project->is_active == 0)
                                        <span class="label label-danger">Tidak Aktif</span>
                                    @else
                                        <span class="label label-success">Aktif</span>
                                    @endif
                            </td>
                            <td>
                                <div><span class="label label-warning">Aktif : {{ $unitCount[$project->id]['active'] }}</span></div>
                                <n/>
                                <div><span class="label label-warning">Tidak Aktif : {{ $unitCount[$project->id]['inActive'] }}</span></div>
                            </td>
                            <td>
                                @if (\Entrust::hasRole('content-editor'))
                                    <a href="{{ route('admin.apartment.project.edit', $project->id) }}" title="edit"><i class="fa fa-pencil"></i></a>

                                    @if(!is_null($project->area_city) && $project->area_city != '')
                                        <a href="{{ $project->share_url }}" target="_blank" rel="noopener" title="preview">
                                            <i class="fa fa-link"></i>
                                        </a>
                                    @endif
                                @else
                                    <a href="{{ route('admin.apartment.project.edit', $project->id) }}" title="edit"><i class="fa fa-pencil"></i></a>

                                    @permission('access-apartment-project-unit')
                                    <a href="{{ route('admin.apartment.unit.list', $project->id) }}" title="Units"><i class="fa fa-square"></i></a>
                                    @endpermission
        
                                    @permission('access-apartment-project-type')        
                                    <a href="{{ route('admin.apartment.project.type.list', $project->id) }}" title="Types"><i class="fa fa-list-ul"></i></a>
                                    @endpermission

                                    @permission('access-apartment-project-tower')
                                    <a href="{{ route('admin.apartment.project.tower.list', $project->id) }}" title="Tower"><i class="fa fa-calendar"></i></a>
                                    @endpermission

                                    <a href="{{ url('/admin/apartment/project/redirect/landing/' . $project->id) }}" title="Redirect" >
                                        <i class="fa fa-exchange"></i>
                                    </a>

                                    @if(!is_null($project->area_city) && $project->area_city != '')
                                        <a href="{{ $project->share_url }}" target="_blank" rel="noopener" title="preview">
                                            <i class="fa fa-link"></i>
                                        </a>

                                        @permission('access-apartment-project-activation')
                                        @if($project->is_active == 0)
                                            <a href="{{ route('admin.apartment.project.activate', $project->id) }}" title="Aktifkan">
                                                <i class="fa fa-arrow-up"></i>
                                            </a>
                                        @else
                                            <a href="{{ route('admin.apartment.project.deactivate', $project->id) }}" title="Non Aktifkan">
                                                <i class="fa fa-arrow-down"></i>
                                            </a>
                                        @endif
                                        @endpermission

                                    @endif

                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

        </div><!-- /.box-body -->
        <div class="box-body no-padding">
            {{ $apartmentProjects->appends(Request::except('page'))->links() }}
        </div>
    </div><!-- /.box -->
    <!-- table -->

@endSection

@section('script')

{{ Html::script('assets/vendor/holder/holder.js') }}
<script type="text/javascript">
    $(function() {
        
    });
</script>
@endSection
