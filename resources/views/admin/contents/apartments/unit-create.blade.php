@extends('admin.layouts.main')

@section('style')
    <link rel="stylesheet" href="/assets/vendor/summernote/summernote.css?v=05042018">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/dropzone.css">
    {{ HTML::style('assets/vendor/jquery-chosen/chosen.css') }}
    
    <style>
        .chosen-choices {
            border: 1px solid #ccc;
            border-radius: 4px;
            min-height: 34px;
            padding: 6px 12px;
        }
    </style>
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

        <form action="{{ URL::route('admin.apartment.unit.store', $project->id) }}" method="POST" class="form-horizontal form-bordered">
            
            <div class="box-body no-padding">

                <div class="form-group bg-default">
                    <label for="project_id" class="control-label col-sm-2">Apartment Project</label>
                    <div class="col-sm-10">
                        <p class="form-control-static">{{ $project->name }}</p>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="unit_name" class="control-label col-sm-2">Nama</label>
                    <div class="col-sm-10">
                        <input type="text" name="unit_name" id="unit_name" value="{{ old('unit_name') }}" class="form-control">
                    </div>
                </div>

            <div class="form-group bg-default">
                <label for="inputIndexed" class="col-sm-2 control-label">Indexed</label>
                <div class="col-sm-10">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" checked="checked" name="indexed" value="1">
                            Indexed
                        </label>
                    </div>
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputDescriptionMother" class="col-sm-2 control-label">Description</label>
                <div class="col-sm-10">
                    <textarea class="form-control" rows="3" id="inputDescriptionMother" name="description">{{ old('description') }}</textarea>
                </div>
            </div>

                <div class="form-group bg-default">
                    <label for="unit_type" class="control-label col-sm-2">Tipe Unit</label>
                    <div class="col-sm-10">
                        <select name="unit_type" class="form-control" id="unit_type">
                            @foreach($typeOptions as $type)
                                <option value="{{ $type }}" {{ old('unit_type') == $type ? 'selected="selected"' : ($type == 'Lainnya' && !in_array(old('unit_type'), $typeOptions) ? 'selected="selected"' : '') }}>{{ $type }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="unit_type_name" class="control-label col-sm-2">Tipe Unit (custom)</label>
                    <div class="col-sm-10">
                        <input type="text" name="unit_type_name" id="unit_type_name" value="{{ old('unit_type_name') }}" class="form-control">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="unit_number" class="control-label col-sm-2">Nomor Unit</label>
                    <div class="col-sm-10">
                        <input type="text" name="unit_number" id="unit_number" value="{{ old('unit_number') }}" class="form-control">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="floor" class="control-label col-sm-2">Lantai</label>
                    <div class="col-sm-10">
                        <input type="text" name="floor" id="floor" value="{{ old('floor') }}" class="form-control">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="unit_size" class="control-label col-sm-2">Ukuran Unit</label>
                    <div class="col-sm-10">
                        <input type="text" name="unit_size" id="unit_size" value="{{ old('unit_size') }}" class="form-control">
                        <p class="help-block">Dalam m<sup>2</sup></p>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="min_payment" class="control-label col-sm-2">Minimal Pembayaran</label>
                    <div class="col-sm-10">
                        <select name="min_payment" class="form-control">
                            <option value="" >- Pilih Minimal Pembayaran -</option>
                            @foreach($minPaymentOptions as $key => $value)
                                <option value="{{ $key }}" {{ old('min_payment') == $key ? 'selected="selected"' : '' }}>{{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="price_type" class="control-label col-sm-2">Pilih tipe harga</label>
                    <div class="col-sm-10">
                        <select name="price_type" class="form-control" id="price_type">
                            <option value="idr">Rupiah / Idr</option>
                            <option value="usd">Dollar / Usd</option>
                        </select>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="price_daily" class="control-label col-sm-2">Harga Harian</label>
                    <div class="col-sm-10">
                        <input type="number" name="price_daily" id="price_daily" value="{{ old('price_daily') }}" class="form-control">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="price_weekly" class="control-label col-sm-2">Harga Mingguan</label>
                    <div class="col-sm-10">
                        <input type="number" name="price_weekly" id="price_weekly" value="{{ old('price_weekly') }}" class="form-control">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="price_monthly" class="control-label col-sm-2">Harga Bulanan</label>
                    <div class="col-sm-10">
                        <input type="number" name="price_monthly" id="price_monthly" value="{{ old('price_monthly') }}" class="form-control">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="price_yearly" class="control-label col-sm-2">Harga Tahunan</label>
                    <div class="col-sm-10">
                        <input type="number" name="price_yearly" id="price_yearly" value="{{ old('price_yearly') }}" class="form-control">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="price_shown" class="control-label col-sm-2">Harga yang Ditampilkan</label>
                    <div class="col-sm-10">
                        <label class="checkbox-inline">
                            <input type="checkbox" name="price_shown[]" value="daily" {{ in_array('daily', old('price_shown', [])) ? 'checked="checked"' : '' }}> Harian
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" name="price_shown[]" value="weekly" {{ in_array('weekly', old('price_shown', [])) ? 'checked="checked"' : '' }}> Mingguan 
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" name="price_shown[]" value="monthly" {{ in_array('monthly', old('price_shown', [])) ? 'checked="checked"' : '' }}> Bulanan 
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" name="price_shown[]" value="yearly" {{ in_array('yearly', old('price_shown', [])) ? 'checked="checked"' : '' }}> Tahunan 
                        </label>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="maintenance_price" class="control-label col-sm-2">Biaya Maintenance / Bulan</label>
                    <div class="col-sm-10">
                        <input type="number" name="maintenance_price" id="maintenance_price" value="{{ old('maintenance_price') }}" class="form-control">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="parking_price" class="control-label col-sm-2">Biaya Parkir</label>
                    <div class="col-sm-10">
                        <input type="number" name="parking_price" id="parking_price" value="{{ old('parking_price') }}" class="form-control">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="is_furnished" class="control-label col-sm-2">Furnished</label>
                    <div class="col-sm-10">
                        <select name="is_furnished" class="form-control" id="is_furnished">
                            @foreach($furnishedOptions as $key => $option)
                                <option value="{{ $key }}">{{ $option }}</option>
                            @endforeach;
                        </select>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="facilities" class="control-label col-sm-2">Fasilitas</label>
                    <div class="col-sm-10">
                        <select class="form-control chosen-select" multiple id="facilities" name="facilities[]">
                            @foreach ($facilityOptions as $key => $value)
                                <option value="{{ $key }}">{{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="owner_name" class="control-label col-sm-2">Nama Pemilik</label>
                    <div class="col-sm-10">
                        <input type="text" name="owner_name" id="owner_name" value="{{ old('owner_name') }}" class="form-control">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="owner_phone" class="control-label col-sm-2">Nomor Telepon Pemilik</label>
                    <div class="col-sm-10">
                        <input type="text" name="owner_phone" id="owner_phone" value="{{ old('owner_phone') }}" class="form-control">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="coverphoto" class="control-label col-sm-2">Foto Cover</label>
                    <div class="col-sm-10">
                        <div id="coverphoto"
                              action="{{ url('admin/media') }}"
                              method="POST"
                              class="dropzone"
                              uploadMultiple="no">
                            {{ csrf_field() }}
                            <input type="hidden" name="media_type" value="style_photo">
                        </div>
                    </div>
                </div>

                <div id="photo-cover-wrapper"></div>

                <div class="form-group bg-default">
                    <label for="bedroomphoto" class="control-label col-sm-2">Foto Kamar</label>
                    <div class="col-sm-10">
                        <div id="bedroomphoto"
                              action="{{ url('admin/media') }}"
                              method="POST"
                              class="dropzone"
                              uploadMultiple="no">
                            {{ csrf_field() }}
                            <input type="hidden" name="media_type" value="style_photo">
                        </div>
                    </div>
                </div>

                <div id="photo-bedroom-wrapper"></div>

                <div class="form-group bg-default">
                    <label for="bathroomphoto" class="control-label col-sm-2">Foto Kamar Mandi</label>
                    <div class="col-sm-10">
                        <div id="bathroomphoto"
                              action="{{ url('admin/media') }}"
                              method="POST"
                              class="dropzone"
                              uploadMultiple="no">
                            {{ csrf_field() }}
                            <input type="hidden" name="media_type" value="style_photo">
                        </div>
                    </div>
                </div>

                <div id="photo-bathroom-wrapper"></div>

                <div class="form-group bg-default">
                    <label for="Other" class="control-label col-sm-2">Foto Fasilitas lainnya</label>
                    <div class="col-sm-10">
                        <div id="Other"
                              action="{{ url('admin/media') }}"
                              method="POST"
                              class="dropzone"
                              uploadMultiple="no">
                            {{ csrf_field() }}
                            <input type="hidden" name="media_type" value="style_photo">
                        </div>
                    </div>
                </div>
                <div id="photo-other-wrapper"></div>
                <hr>

                <div class="form-group bg-default">
                    <label for="input_as" class="control-label col-sm-2">Input Sebagai</label>
                    <div class="col-sm-10">
                        <select name="input_as" class="form-control">
                            @foreach($inputAsOptions as $value)
                                <option value="{{ $value }}">{{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="input_name" class="control-label col-sm-2">Nama Penginput</label>
                    <div class="col-sm-10">
                        <input type="text" name="input_name" id="input_name" value="{{ old('input_name') }}" class="form-control">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <div class="col-sm-10 col-sm-push-2">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </form>

        
    </div>

@endsection

@section('script')
{{ HTML::script('assets/vendor/jquery-chosen/chosen.jquery.js') }}


<script src="/assets/vendor/summernote/summernote.min.js"></script>

<script type="text/javascript">
        $('#inputDescriptionMother').summernote({
            height: 300,
            toolbar: [
                ['style', ['style', 'bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['insert', ['link', 'table', 'hr']],
                ['misc', ['codeview']]
            ]
        });

</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/dropzone.js"></script>

<script>
    var config = {
        '.chosen-select'           : {width: '100%'}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }

    $(function() {
        $('#unit_type').on('change', function() {
            if($(this).val() == 'Lainnya') {
                $('#unit_type_name').removeAttr('disabled');
            } else {
                $('#unit_type_name').attr('disabled', 'disabled');
            }
        });

        $('#unit_type').trigger('change');

        Dropzone.autoDiscover = false;

        Dropzone.options.coverphoto = {
            paramName : "media",
            maxFilesize : 6,
            maxFiles: 1,
            accepFiles: '.jpg, .jpeg, .png, .bmp',
            dictDefaultMessage : 'Upload photo for cover',
            addRemoveLinks: true,
            success: function(file, response) {
                if(response.media.id != 0) {
                    $('#photo-cover-wrapper').append('<input type="hidden" name="photo_cover" value="' +response.media.id+ '">');
                }
            },
            init: function() {
                this.on('removedfile', function(file) {
                    $('#photo-cover-wrapper').html('');
                });
            }
        }

        Dropzone.options.bedroomphoto = {
            paramName : "media",
            maxFilesize : 6,
            accepFiles: '.jpg, .jpeg, .png, .bmp',
            dictDefaultMessage : 'Upload bedroom photos',
            addRemoveLinks: true,
            success : function (file, response) {
                if(response.media.id != 0){
                    $('#photo-bedroom-wrapper').append('<input type="hidden" name="photo_bedroom[]" value="' +response.media.id+ '" data-filename="' + file.name + '">');
                }
            },
            init: function() {
                this.on('removedfile', function(file) {
                    $('input[name="photo_bedroom[]"]').each(function(id, el) {
                        if($(el).data('filename') == file.name) {
                            $(el).remove();
                        }
                    });
                });
            }
        }

        $('#bedroomphoto').dropzone();

        Dropzone.options.bathroomphoto = {
            paramName : "media",
            maxFilesize : 6,
            accepFiles: '.jpg, .jpeg, .png, .bmp',
            dictDefaultMessage : 'Upload bedroom photos',
            addRemoveLinks: true,
            success : function (file, response) {
                if(response.media.id != 0){
                    $('#photo-bathroom-wrapper').append('<input type="hidden" name="photo_bathroom[]" value="' +response.media.id+ '" data-filename="' + file.name + '">');
                }
            },
            init: function() {
                this.on('removedfile', function(file) {
                    $('input[name="photo_bathroom[]"]').each(function(id, el) {
                        if($(el).data('filename') == file.name) {
                            $(el).remove();
                        }
                    });
                });
            }
        }

        $('#bathroomphoto').dropzone();

        $('#coverphoto').dropzone();


        Dropzone.options.Other = {
            paramName : "media",
            maxFilesize : 6,
            accepFiles: '.jpg, .jpeg, .png, .bmp',
            dictDefaultMessage : 'Upload other photos',
            addRemoveLinks: true,
            success : function (file, response) {
                if(response.media.id != 0){
                    $('#photo-other-wrapper').append('<input type="hidden" name="photo_other[]" value="' +response.media.id+ '" data-filename="' + file.name + '">');
                }
            },
            init: function() {
                this.on('removedfile', function(file) {
                    $('input[name="photo_other[]"]').each(function(id, el) {
                        if($(el).data('filename') == file.name) {
                            $(el).remove();
                        }
                    });
                });
            }
        }

        $('#Other').dropzone();


    });
</script>
@endsection