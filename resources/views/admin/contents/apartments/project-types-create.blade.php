@extends('admin.layouts.main')

@section('style')
<<style>
	.name-custom-group {
		display: none;
	}
</style>
@endsection

@section('content')
	<div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

		<form action="{{ URL::route('admin.apartment.project.type.store', $apartmentProject->id) }}" method="POST" class="form-horizontal form-bordered">
			<div class="box-body no-padding">
				<div class="form-group bg-default">
					<label for="name" class="control-label col-sm-2">Nama Tipe</label>
					<div class="col-sm-10">
						<select name="name" class="form-control" id="name">
							@foreach($typeOptions as $key => $option)
								<option value="{{ $key }}" {{ old('name') == $key ? 'selected="selected"' : '' }} data-bedroom="{{ $option['bedroom'] }}" data-bathroom="{{ $option['bathroom'] }}">
									{{ $key }}
								</option>
							@endforeach
						</select>
					</div>
				</div>

				<div class="form-group bg-default name-custom-group">
					<label for="name_custom" class="control-label col-sm-2">Nama Tipe (custom)</label>
					<div class="col-sm-10">
						<input type="text" name="name_custom" id="nameCustom" class="form-control" value="{{ old('name_custom') }}">
					</div>
				</div>

				<div class="form-group bg-default">
					<label for="bedroomCount" class="control-label col-sm-2">Jumlah Kamar</label>
					<div class="col-sm-10">
						<input type="number" name="bedroom_count" id="bedroomCount" class="form-control" value="{{ old('bedroom_count') }}">
					</div>
				</div>

				<div class="form-group bg-default">
					<label for="bathroomCount" class="control-label col-sm-2">Jumlah Kamar Mandi</label>
					<div class="col-sm-10">
						<input type="number" name="bathroom_count" id="bathroomCount" class="form-control" value="{{ old('bathroom_count') }}">
					</div>
				</div>

				<div class="form-group bg-default">
					<label for="roomSize" class="control-label col-sm-2">Luas</label>
					<div class="col-sm-10">
						<input type="number" name="room_size" id="roomSize" class="form-control" value="{{ old('room_size') }}">
					</div>
				</div>

				<div class="form-group bg-default">
					<div class="col-sm-10 col-sm-push-2">
						<button type="submit" class="btn btn-primary">Save</button>
					</div>
				</div>
			</div>
		</form>

		
    </div>
@endsection

@section('script')

<script>
	$(function() {
		$('#name').on('change', function(e) {
			currentVal = $(this).val();

			dataBedroom = $(this).find('option:selected').data('bedroom');
			dataBathroom = $(this).find('option:selected').data('bathroom');

			// if($('#bedroomCount').val() == '') {
				$('#bedroomCount').val(dataBedroom);
			// }

			// if($('#bathroomCount').val() == '') {
				$('#bathroomCount').val(dataBathroom);
			// }

			if(currentVal == 'Lainnya') {
				$('.name-custom-group').show();
			} else {
				$('.name-custom-group').hide();
			}
		});

		if($('#name').val() == 'Lainnya') {
			$('.name-custom-group').show();
		}
	});
</script>
@endsection