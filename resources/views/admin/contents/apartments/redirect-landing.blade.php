@extends('admin.layouts.main')

@section('style')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/dropzone.css">
    {{ HTML::style('assets/vendor/jquery-chosen/chosen.css') }}
    <link rel="stylesheet" href="/assets/vendor/summernote/summernote.css?v=05042018">
    <link rel="stylesheet" href="{{ mix_url('dist/vendor/leaflet/leaflet.css') }}">

    <style>
        .chosen-choices {
            border: 1px solid #ccc;
            border-radius: 4px;
            min-height: 34px;
            padding: 6px 12px;
        }

        .note-group-select-from-files {
            display: none;
        }
    </style>
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

        <form action="{{ URL::route('admin.apartment.project.redirect.landing.store') }}" method="POST" class="form-horizontal form-bordered">
            <div class="box-body no-padding">
                <div class="form-group bg-default">
                    <input type="hidden" value="{{ $apartmentId }}" name="apartment_id">
                    <label for="facilities" class="control-label col-sm-2">Landing tujuan</label>
                    <div class="col-sm-10">
                        <select class="form-control chosen-select" id="apartment_project" name="apartment_project_landing">
                            <option value="">Pilih Landing Apartment Project Tujuan</option>
                            @foreach($listApartment as $list)
                                <option value="{{ $list->id }}">{{ $list->id . " | " . $list->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group bg-default">
                    <div class="col-sm-10 col-sm-push-2">
                        <button type="submit" class="btn btn-primary">Save</button>
                        <a href="{{ URL::route('admin.apartment.project.index') }}" class="btn btn-warning">Back</a>
                    </div>
                </div>
            </div>
        </form>


    </div>
@endsection

@section('script')
{{ HTML::script('assets/vendor/jquery-chosen/chosen.jquery.js') }}
<script type="text/javascript">
    $(function() {
        var config = {
            '.chosen-select'  : {width: '100%', height: '100%'}
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }
    });
</script>
@endsection
