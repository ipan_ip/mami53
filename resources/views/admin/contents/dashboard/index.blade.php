@extends('admin.layouts.main')
@section('content')
<div class="row">
    <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-yellow">
            <div class="inner">
                <h3>
                    {{ $widgets->user->count }}
                </h3>
                <p>
                    {{ $widgets->user->title }}
                </p>
            </div>
            <div class="icon">
                <i class="ion ion-person-add"></i>
            </div>
            <a href="{{ URL::route('admin.user.index') }}" class="small-box-footer">
                More info <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-red">
            <div class="inner">
                <h3>
                    {{ $widgets->stories->count }}
                </h3>
                <p>
                    {{ $widgets->stories->title }}
                </p>
            </div>
            <div class="icon">
                <i class="ion ion-pie-graph"></i>
            </div>
            <a href="{{ URL::route('admin.stories.index') }}" class="small-box-footer">
                More info <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
</div>
@stop
@section('script')
<!-- page script -->
<script type="text/javascript">
    $(function()
    {

    });
</script>
@stop