@extends('admin.layouts.main')

@section('style')
<style>
    span.apartment-color,
    tr.apartment-color td {
        color: #0D47A1;
    }

    tr.kost-active td {
        background-color: #C8E6C9;
    }

    .table>tbody>tr>td {
        vertical-align: middle;
    }

    .font-huge {
        font-size: 2em;
        /* color: #9E9E9E; */
        font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
        font-weight: 600;
    }

    .font-large {
        font-size: 1.5em;
    }

    .font-semi-large {
        font-size: 1.15em;
    }

    .font-grey {
        color: #9E9E9E;
    }

    .dropdown-menu>li>a:hover {
        color: #333
    }

    .label-grey {
        background-color: #CFD8DC;
    }

    /* Sweetalert */

    .swal-wide {
        width: 850px !important;
        /* font-size: 14px!important; */
    }

    .swal-shown {
        height: auto !important;
    }

    .custom-swal {
        z-index: 10000 !important;
    }

    .custom-wide-swal {
        width: 850px !important;
    }
</style>
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
@endsection

@section('content')
<!-- table -->
<div class="box box-default">
    <!-- Box Header -->
    <div class="box-header" style="padding-top: 10px;">
        <div class="col-md-12 text-center">
            <h3 class="box-title"><i class="fa fa-qrcode"></i> {{ $boxTitle }}</h3>
        </div>
    </div>
    <!-- /.box-header -->
    <!-- Box Body -->
    <div class="box-body" style="padding-top: 10px;">
        {{-- Top Bar --}}
        <div class="horizontal-wrapper">
            <div class="btn-horizontal-group bg-default form-inline" style="padding-left: 10px;padding-right: 10px;">
                <!-- Add button -->
                <div class="col-md-6 bg-default" style="padding-bottom:10px;">
                    <button class="btn btn-danger actions" data-action="bulk">
                        <i class="fa fa-refresh"></i>&nbsp;Regenerate All Codes
                    </button>
                </div>
                <!-- Search filters -->
                <div class="col-md-6 bg-default text-right inline-form" style="padding-bottom:10px">
                    <form action="" method="get">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control input-md"
                                placeholder="Type Kos name / Kos ID" autocomplete="off"
                                value="{{ request()->input('q') }}" style="width:360px;margin-left:20px;">
                            <span class="input-group-btn">
                                <button type="submit" name="search" id="buttonSearch" class="btn btn-primary"><i
                                        class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- The table is going here -->
        <div class="box-body">
            @include('admin.contents.unique-code.partials.table')
        </div>
        <!-- End of table -->

    </div>
    <!-- /.box-body -->
    <div class="box-body no-padding">
        {{ $rowsDesigner->appends(Request::except('page'))->links() }}
    </div>
</div>
<!-- /.box -->

{{-- @include('admin.partials.delete-modal') --}}
@stop

@section('script')
<!-- Holder for Upload File Thumbnail -->
<script src="//cdn.jsdelivr.net/npm/holderjs@2.9.6/holder.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.js"></script>
<script src="//cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>

<!-- page script -->
<script type="text/javascript">
    function triggerAlert(type, message) {
        Swal.fire({
            type: type,
            customClass: {
                container: 'custom-swal'
            },
            html: message
        });
    }

    function triggerLoading(message) {
        Swal.fire({
            title: message,
            customClass: {
                container: 'custom-swal'
            },
            allowEscapeKey: false,
            allowOutsideClick: false,
            onOpen: () => {
                swal.showLoading();
            }
        });
    }

    function ajaxCall(type, title, html, method, url, payload, input, btnText, btnClass, needRefresh = true) {
        Swal.fire({
            type: type,
            title: title,
            html: html,
            buttonsStyling: btnClass == null,
            confirmButtonText: btnText,
            confirmButtonClass: btnClass,
            showCancelButton: true,
            cancelButtonClass: (btnClass !== null) ? 'btn btn-default' : null,
            customClass: 'custom-swal',
            showLoaderOnConfirm: true,
            input: input,
            preConfirm: (response) => {
                if (input !== null) {
                    let number = response;
                    if (!response) {
                        return Swal.showValidationMessage('Please provide a valid phone number!');
                    }

                    payload = {
                        "template_id": payload.id,
                        "number": response
                    };
                }

                return $.ajax({
                    type: method,
                    url: url,
                    data: payload,
                    dataType: 'json',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: (response) => {
                        if (response.success == false) {
                            Swal.close();
                            triggerAlert('error', response.message);
                        }
                            
                        return;
                    },
                    error: (error) => {
                        Swal.close();
                        triggerAlert('error', JSON.stringify(error));
                    }
                });
            },
            allowOutsideClick: () => !Swal.isLoading()
        }).then((result) => {
            if (!result.dismiss != 'cancel') {
                if (result.value.success == true) {
                    Swal.fire({
                        type: 'success',
                        title: result.value.title,
                        html: result.value.message,
                        onClose: () => {
                            if (needRefresh) window.location.reload();
                        }
                    });
                } else {
                    Swal.fire({
                        type: 'error',
                        title: "Failed!",
                        html: result.value.message
                    });
                }
            }
        });
    }

    $(function()
    {
        var token = '{{ csrf_token() }}';

        $('#select-sort-option').change(function(event){
            var url  = '{{ URL::route("admin.room.index") }}' + '?o=' + event.target.value ;

            url += '&st=' + $('#start-date').val();
            url += '&en=' + $('#end-date').val();

            window.location = url;

        });

        $('.datepicker').datepicker({ dateFormat : 'dd-mm-yy'});
        
        Holder.addTheme("thumbnail", {
            background: '#EEE',
            foreground: '#AAA',
            size: 11,
            fontweight: 'normal',
            text: 'Image'
        });

        // Actions button listeners
        $('.actions').click((e) => {
            e.preventDefault();

            const action = $(e.currentTarget).data('action');

            // Bulk generate reports
        	if (action === 'bulk') {
                ajaxCall('warning', "Warning!<br/>This action will generate new codes for all Kos BBK", null, 'GET', "/admin/room/ucode/bulk", null, null, 'Proceed!', null, true);
            }

            // Generate single code
            if (action === 'generate') {
                let payload = {
                    'id': $(e.currentTarget).data('id')
                };

                ajaxCall('warning', "Getting new code for this Kos ?", null, 'POST', "/admin/room/ucode/generate", payload, null, 'Proceed!', null, true);
            }

            // Remove code
            if (action === 'remove') {
                let payload = {
                    'id': $(e.currentTarget).data('id')
                };
                
                ajaxCall('error', "Are you sure to remove the code ?", null, 'POST', "/admin/room/ucode/remove", payload, null, 'Yes, remove it!', null, true);
            }
        });
    });

</script>
@stop