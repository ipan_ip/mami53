@extends('admin.layouts.main')

@section('style')
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">

<style>
    /* Sweet alert confirm button */
    .swal2-styled.swal2-confirm {
        background-color: #f0ad4e;
    }

    /* Sweet aler cancel button */
    .swal2-styled.swal2-cancel {
        background-color: #0275d8;
    }
</style>
@endsection

@section('content')
<div class="box box-primary">
    <div class="box-header">
        @if(isset($edit) && $edit)
            <h3 class="box-title">Edit VR Tour</h3>
        @elseif(isset($create) && $create)
            <h3 class="box-title">Create VR Tour</h3>
        @endif
    </div><!-- /.box-header -->
    @if(isset($edit) && $edit)
        <form method="POST" action="{{ route('admin.vrtour.update', $vrTour->id) }}" class="form-horizontal form-bordered" id="formInsertDesigner">
    @elseif(isset($create) && $create)
        <form method="POST" action="{{ route('admin.vrtour.store', $room->id) }}" class="form-horizontal form-bordered" id="formInsertDesigner">
    @endif
        @csrf
        @if(isset($edit) && $edit)
            @method('PUT')
        @endif
        <div class="box-body no-padding">
            <div class="form-group bg-default">
                <label for="id" class="col-sm-2 control-label">Kost ID</label>

                <div class="col-sm-10">
                    <input type="text" class="form-control" id="id" name="id" value="{{ $room->id }}" disabled>
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="name" class="col-sm-2 control-label">Nama Kost / Apartemen</label>

                <div class="col-sm-10">
                    <input type="text" class="form-control" id="name" name="name" value="{{ $room->name }}" disabled>
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="matterport_id" class="col-sm-2 control-label">Matterport ID</label>

                <div class="col-sm-10">
                    @if ($errors->all())
                        <input type="text" class="form-control" placeholder="Matterport ID" id="matterport_id" name="matterport_id" value="{{ old('matterport_id') }}">
                    @else
                        <input type="text" class="form-control" placeholder="Matterport ID" id="matterport_id" name="matterport_id" value="{{ (isset($edit) && $edit) ? $vrTour->matterport_id : '' }}">
                    @endif

                    <div id="countmatterportId" style="font-weight:bold"></div>
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputName" class="col-sm-2 control-label">Active</label>
                <div class="col-sm-10">
                    <div class="checkbox">
                        <label>
                            @if ($errors->all())
                                <input type="checkbox" name="is_matterport_active" id="is_matterport_active" {{ old('is_matterport_active') == '1' ? 'checked' : '' }} value="1">
                            @elseif (isset($edit) && $edit)
                                <input type="checkbox" name="is_matterport_active" id="is_matterport_active" {{ ($vrTour->is_matterport_active == '1') ? 'checked' : '' }} value="1">
                            @else
                                <input type="checkbox" name="is_matterport_active" id="is_matterport_active" value="1">
                            @endif
                        </label>
                    </div>
                </div>
            </div>
        </div>

        <input type="hidden" name="confirm" id="confirm" value="0">

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">Save</button>

                @if(isset($edit) && $edit)
                    <a href="{{ route('admin.vrtour.index') }}" class="btn btn-default">Cancel</a>
                @elseif(isset($create) && $create)
                    <a href="{{ route('admin.vrtour.index') }}" class="btn btn-default">Cancel</a>
                @endif

                <button type="reset" class="btn btn-default" id="resetForm">Reset</button>
            </div>
        </div>
    </form>
    <!-- /.form -->

</div><!-- /.box -->
@stop
@section('script')

<script src="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.js"></script>

<script type="text/javascript">
    @if(Session::get('laravelValidatorJSON') === null)
    myGlobal.laravelValidator = null;
    @else
    myGlobal.laravelValidator = {
        {
            Session::get('laravelValidatorJSON')
        }
    };
    @endif

    $('#resetForm').click(function () {
        {{-- Remove the matterport ID --}}
        $('#matterport_id').attr('value', '');

        {{-- Remove the checkbox mark on is_matterport_active --}}
        $('#is_matterport_active').removeAttr('checked');
        $('.icheckbox_minimal').attr('aria-checked', false);
        $('.icheckbox_minimal').removeClass('checked');
    });

    $(function () {
        $formInsertDesigner = $('#formInsertDesigner');
        $form = $formInsertDesigner;

        var formRules = {
            matterport_id: {
                message: 'The matterport ID is not valid',
                validators: {
                    notEmpty: {
                        message: 'The matterport ID is required and cannot be empty'
                    },
                    stringLength: {
                        min: 11,

                        max: 11,

                        message: 'The matterport ID must be 11 characters long'
                    }
                }
            },
        };

        myGlobal.bootstrapValidatorDefaults.fields = formRules;

        myGlobal.bootstrapValidatorDefaults.excluded = ':disabled';

        askConfirmation();

        updateValidateStatus(
            $formInsertDesigner.data('bootstrapValidator'),
            myGlobal.laravelValidator);
    });

    function askConfirmation () {
        var isMatterportIdError = ("{{ $errors->has('matterport_id') }}" == "1");
        var errorMessage = "{{ $errors->first('matterport_id') }}";
        var isMatterportIdNotUnique = errorMessage === 'The matterport id has already been taken.';
        var matterportId = $('#matterport_id').val();

        if (isMatterportIdError && isMatterportIdNotUnique) {
            Swal.fire({
                type: 'question',
                title: 'The matterport id you enter has been taken!',
                text: ('Are you sure you want to use the matterport id: ' + matterportId + '?'),
                showCancelButton: true,
                confirmButtonText: 'Yes, use it!',
                cancelButtonText: 'No, cancel!',
            }).then(function (result) {
                if (result.value) {
                    $('#confirm').val(1);
                    $('#formInsertDesigner').submit();
                }
            });
        }
    }
</script>
@stop
