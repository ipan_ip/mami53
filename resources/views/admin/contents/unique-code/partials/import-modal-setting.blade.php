{{-- Import Modal --}}
<div class="modal" id="import-modal" role="dialog" aria-labelledby="modalLabel">
    <div class="modal-dialog" role="modal">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top:2px;"><i
                        class="fa fa-lg fa-times-circle"></i></button>
                <h4 class="modal-title"><i class="fa fa-plus"></i> Import from CSV</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="file">Pilih file csv</label>
                            <input type="file" id="dataFile" name="file" class="form-control input-lg" accept="text/csv">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="import-submit">
                    Save <i class="fa fa-paper-plane"></i></button>
            </div>
        </div>
    </div>
</div>
