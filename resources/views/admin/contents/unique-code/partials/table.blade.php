<table id="tableListDesigner" class="table" style="padding-left:10px !important;">
    <thead>
        <tr>
            <th width="150px"></th>
            <th>Nama Kost</th>
            <th class="text-center">Area</th>
            <th class="text-center">Booking Code</th>
            <th class="text-center">Generator</th>
            <th class="text-center" width="10%">Generated</th>
            <th class="table-action-column" width="10%"></th>
        </tr>
    </thead>
    <tbody>
        @foreach ($rowsRoom as $rowDesignerKey => $rowDesigner)
        <tr style="border-top:none!important;"
            class="callout {{ $rowDesigner['is_active'] == 'true' ? 'callout-success' : 'callout-danger' }}">
            <td class="text-center">
                <a href="{{ $rowDesigner['photo_url_large'] }}" data-fancybox data-caption="{{ $rowDesigner['name'] }}">
                    <img width="150" class="img-thumbnail" src="{{ $rowDesigner['photo_url'] }}"
                        alt="{{ $rowDesigner['name'] }}" data-src="holder.js/150x100?text=Foto \n Tidak \n Ditemukan">
                </a>
            </td>
            <td style="font-size:14px">
                <strong>
                    <small class="font-grey">{{ $rowDesigner['id'] }}</small><br />
                    @if(!is_null($rowDesigner['duplicate_from']))
                    <i class="fa fa-retweet" style="color: #ff0000;"></i>
                    @endif
                    {{ $rowDesigner['name'] }}
                </strong>
                <br />
                @if ($rowDesigner['is_mamirooms'])
                <span class="label label-sm label-success" data-toggle="tooltip" title="Verified by Mamikos"><i
                        class="fa fa-home"></i> Mamirooms</span>
                @endif
                @if ($rowDesigner['verified_by_mamikos'])
                <span class="label label-sm label-primary" data-toggle="tooltip" title="Verified by Mamikos"><i
                        class="fa fa-check-circle"></i> MamiVerify</span>
                @endif
                @if ($rowDesigner['promoted'] == true)
                <span class="label label-sm label-warning" data-toggle="tooltip" title="Promoted"><i
                        class="fa fa-arrow-up"></i> Promoted</span>
                @endif
                @if (isset($rowDesigner['price_monthly']['discount_price']))
                <span class="label label-sm label-primary" data-toggle="tooltip" title="Ada Discount Aktif"><i
                        class="fa fa-percent"></i> Discount</span>
                @endif
                @if ($rowDesigner['is_premium_photo'] == 1)
                <span class="label label-sm label-info" data-toggle="tooltip" title="Premium Photos"><i
                        class="fa fa-camera"></i> Foto Booking</span>
                @endif
                @if ($rowDesigner['by_mamichecker'] == true)
                <span class="label label-sm label-primary" data-toggle="tooltip" title="Video Tour by Mamichecker"><i
                        class="fa fa-video-camera"></i> Video Tour</span>
                @endif
            </td>
            <td class="text-center" class="font-semi-large">
                <i class="fa fa-map-marker"></i> {{ $rowDesigner['normalized_city'] }}
            </td>
            <td class="text-center">
                <span class="font-large label label-info">{{ $rowDesigner['unique_code'] }}</span>
            </td>
            <td align="center">
                {{ $rowDesigner['generator'] }}
            </td>
            <td align="center">
                {{ date('j F Y', strtotime($rowDesigner['generated_at'])) }}
            </td>
            <td class="table-action-column">
                <div class="btn-group">
                    <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-cog"></i> Actions <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu pull-right">
                        <li>
                            <a href="#" class="actions" data-action="generate" data-id="{{ $rowDesigner['id'] }}"><i
                                    class="fa fa-refresh"></i> Generate New Code</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#" class="actions" data-action="remove" data-id="{{ $rowDesigner['id'] }}"><i
                                    class="fa fa-trash-o"></i> Remove Code</a>
                        </li>
                    </ul>
                </div>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>