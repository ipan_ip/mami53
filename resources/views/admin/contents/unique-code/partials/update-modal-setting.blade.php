{{-- Update Modal --}}
<div class="modal" id="update-modal" role="dialog" aria-labelledby="modalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top:2px;"><i
                        class="fa fa-lg fa-times-circle"></i></button>
                <h4 class="modal-title" id="update-modal-title"></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Pilih kota/kabupaten</label>
                            <select class="form-control" name="dataCity" id="dataCityUpdate" multiple="multiple"
                                style="width: 100%"></select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="submit">
                    Update <i class="fa fa-paper-plane"></i></button>
            </div>
        </div>
    </div>
</div>