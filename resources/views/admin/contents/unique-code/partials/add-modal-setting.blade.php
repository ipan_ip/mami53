{{-- Add Modal --}}
<div class="modal" id="add-modal" role="dialog" aria-labelledby="modalLabel">
    <div class="modal-dialog" role="modal">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top:2px;"><i
                        class="fa fa-lg fa-times-circle"></i></button>
                <h4 class="modal-title"><i class="fa fa-plus"></i> Add City Code</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Masukkan kode baru</label>
                            <input type="text" id="dataCode" name="code" maxlength="2" class="form-control input-lg">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Pilih kota/kabupaten</label>
                            <select class="form-control" name="cities" id="dataCity" multiple="multiple"
                                style="width: 100%"></select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="add-submit">
                    Save <i class="fa fa-paper-plane"></i></button>
            </div>
        </div>
    </div>
</div>