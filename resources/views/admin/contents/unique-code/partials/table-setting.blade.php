<table id="tableListDesigner" class="table">
    <thead>
        <tr>
            <th></th>
            <th class="text-center" width="10%">Kode</th>
            <th class="text-center" width="80%">Nama Kota / Kabupaten (Jml Kost Booking Aktif | Non Aktif)</th>
            <th class="table-action-column" width="10%"></th>
        </tr>
    </thead>
    <tbody>
        @if (!count($rowsCode))
        <tr>
            <td class="text-center" colspan="4">
                <div class="callout callout-danger">Tidak ada data untuk ditampilkan!<br>Klik <strong>Add New
                        Code</strong> untuk membuat kode baru</div>
            </td>
        </tr>
        @else
        @foreach ($rowsCode as $key => $row)
        <tr>
            <td class="text-center"></td>

            <td class="text-center code">
                <div class="jumbotron jumbotron-fluid">
                    <h1 class="display-2">{{ $key }}</h1>
                </div>
            </td>

            <td>
                <table class="table table-bordered table-responsive">
                    <tbody>
                        @foreach ($row as $city)
                        <tr>
                            <td>
                                <span class="font-semi-large {{ $city->is_generated ? 'text-success' : '' }}"><i class="fa fa-map-marker"></i> <strong>{{ $city->name }}</strong></span>
                                <span class="label {{ $city->is_generated ? 'label-success' : 'label-default' }}" style="margin-left:10px;">{{ $city->total_booking_rooms_active }} Active | {{ $city->total_booking_rooms_nonactive }} Non Active</span>
                                <span class="pull-right">
                                    @if (!$city->is_generated && ($city->total_booking_rooms_active > 0 || $city->total_booking_rooms_nonactive > 0))
                                    <button type="button" class="btn btn-xs btn-success actions" data-action="generate" data-code="{{ $city->code }}" data-city="{{ $city->name }}">
                                        <i class="fa fa-hashtag"></i> Generate Kode Kost
                                    </button>
                                    @endif
                                    {{-- <button type="button" class="btn btn-xs btn-danger" style="margin-left:10px;">
                                        <i class="fa fa-refresh"></i> Pindah Kode
                                    </button> --}}
                                </span>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </td>

            <td class="table-action-column action-column">
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-cog"></i> Actions <span class="caret"></span>
                    </button>

                    <ul class="dropdown-menu pull-right">
                        <li>
                            <a href="#" title="Change Code" class="actions" data-action="update" data-code="{{ $key }}"
                                data-cities="{{ $row }}">
                                <i class="fa fa-pencil"></i> Ubah Data Kota
                            </a>
                        </li>
                        <li>
                            <a href="#" title="Remove Code" class="actions" data-action="remove" data-code="{{ $key }}"
                                data-cities="{{ $row }}">
                                <i class="fa fa-trash"></i> Hapus Kode
                            </a>
                        </li>
                    </ul>
                </div>
            </td>
        </tr>
        @endforeach
        @endif
    </tbody>
</table>