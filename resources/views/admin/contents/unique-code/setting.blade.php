@extends('admin.layouts.main')

@section('style')
<style>
    .table>tbody>tr>td:not(.code, .action-column) {
        vertical-align: middle;
    }

    .font-large {
        font-size: 1.5em;
    }

    .font-semi-large {
        font-size: 1.15em;
    }

    .font-grey {
        color: #9E9E9E;
    }

    .dropdown-menu>li>a:hover {
        color: #333
    }

    .label-grey {
        background-color: #CFD8DC;
    }

    tr.checker-active td {
        background-color: #C8E6C9;
    }


    /* Sweetalert */

    .swal-shown {
        height: auto !important;
    }

    .custom-swal {
        z-index: 10000 !important;
    }

    .custom-wide-swal {
        width: 640px !important;
        font-size: 13px !important;
    }


    /* Wixard twekas */

    .wizard>.content>.body {
        width: 100% !important;
        padding: 0 !important;
    }


    /* Select2 tweak */

    .input-group .select2-container,
    .form-group .select2-container {
        position: relative !important;
        z-index: 2;
        float: left !important;
        width: 100% !important;
        margin-bottom: 0 !important;
        display: table !important;
        table-layout: fixed !important;
    }


    /* Modal tweak */

    .modal-open {
        overflow: hidden;
        position: fixed;
        width: 100%;
    }


    /* Centering the modal */

    .modal {
        text-align: center;
        padding: 0 !important;
    }

    .modal:before {
        content: '';
        display: inline-block;
        height: 100%;
        vertical-align: middle;
        margin-right: -4px;
    }

    .modal-dialog {
        display: inline-block;
        text-align: left;
        vertical-align: middle;
    }

    /* jumbotron tweak */
    .jumbotron {
        margin-bottom: 2px !important;
        padding: 10px !important;
    }

    h1,
    .h1,
    h2,
    .h2,
    h3,
    .h3 {
        margin-top: 10px !important;
    }
</style>
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" />
<link href="//cdn.jsdelivr.net/npm/select2-bootstrap-theme@0.1.0-beta.10/dist/select2-bootstrap.min.css"
    rel="stylesheet" />
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
@endsection

@section('content')
<!-- table -->
<div class="box box-default">
    <!-- Box Header -->
    <div class="box-header">
        <div class="col-md-12 text-center">
            <h3 class="box-title"><i class="fa fa-wrench"></i> BBK Code Setting</h3>
        </div>
    </div>
    <!-- /.box-header -->

    <div class="box-body no-padding">
        <div class="horizontal-wrapper">
            <div class="btn-horizontal-group bg-default form-inline justify-content-between" style="padding-left: 10px;padding-right: 10px;">
                <!-- Add button -->
                <div class="col col-md-6 bg-default" style="padding-bottom:10px;">
                    <!-- <div class="row"> -->
                        <div class="col-sm-4 col-md-4">
                            <a href="#" id="btn-import" class="btn btn-primary btn-sm" data-title="Import from CSV"><i
                                class="fa fa-plus"></i> Import from CSV</a>
                        </div>
                        
                        <div class="col-sm-3 col-md-3">
                            <a href="#" id="btn-add" class="btn btn-primary btn-sm" data-title="Add New Code"><i
                                class="fa fa-plus"></i> Add New Code</a>
                        </div>
                    <!-- </div> -->
                </div>

                <!-- Search filters -->
                <div class="col col-md-6 bg-default" style="padding-bottom:10px;text-align:right">
                    <form action="" method="get">
                        <input type="text" name="q" id="name" class="form-control input-sm" placeholder="Cari Nama Kota"
                            autocomplete="off" value="{{ request()->input('q') }}">
                        <input type="text" name="c" id="code" class="form-control input-sm" placeholder="Cari Code"
                            autocomplete="off" value="{{ request()->input('c') }}">
                        <button class="btn btn-primary btn-sm" id="buttonSearch">
                            <i class="fa fa-search">&nbsp;</i>Search
                        </button>
                    </form>
                </div>
            </div>
        </div>

        <!-- The table is going here -->
        @include('admin.contents.unique-code.partials.table-setting')
        <!-- End of index table -->

    </div><!-- /.box-body -->
    {{-- <div class="box-body no-padding">
        {{ $rowsCode->appends(Request::except('page'))->links() }}
</div> --}}
</div><!-- /.box -->
<!-- table -->

<!-- Modals -->
@include('admin.contents.unique-code.partials.add-modal-setting')
@include('admin.contents.unique-code.partials.import-modal-setting')
@include('admin.contents.unique-code.partials.update-modal-setting')
<!-- /. modals -->
@stop

@section('script')
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.js"></script>

<!-- page script -->
<script type="text/javascript">
    // Sweetalert functions
    function triggerAlert(type, message) {
        Swal.fire({
            type: type,
            customClass: {
                container: 'custom-swal'
            },
            title: message
        });
    }

    function triggerLoading(message) {
        Swal.fire({
            title: message,
            customClass: {
                container: 'custom-swal'
            },
            allowEscapeKey: false,
            allowOutsideClick: false,
            onOpen: () => {
                swal.showLoading();
            }
        });
    }

    // custom function
    function compileCityArray(data) {
        var cities = [];
        $.each(data, (key, val) => {
            cities.push(val.name);
        });
        return cities;
    }

    function compileCityIdArray(data) {
        var cities = [];
        $.each(data, (key, val) => {
            cities.push(val.id);
        });
        return cities.sort();
    }

    function convertStringArrayToInt(data) {
        var cities = [];
        $.each(data, (key, val) => {
            cities.push(parseInt(val));
        });
        return cities.sort();
    }

    $(function(){
        var token = '{{ csrf_token() }}';

        var _data = null, _code = null;

        $('#btn-add').on('click', (e) => {
            e.preventDefault();
            $('#add-modal').modal('show');
        });

        $('#btn-import').on('click', (e) => {
            e.preventDefault();
            $('#import-modal').modal('show');
        });

        $('.actions').click((e) => {
            e.preventDefault();

            var action = $(e.currentTarget).data('action');
            _code = $(e.currentTarget).data('code');

            if (action != 'generate' && action != 'regenerate') {
                _selectedCities = compileCityArray($(e.currentTarget).data('cities'));
                _selectedCityIds = compileCityIdArray($(e.currentTarget).data('cities'));
            }

            // Updating
            if (action == 'update') {
                $('#update-modal').modal('show');
            }

            // Removing
            else if (action == 'remove') {
                Swal.fire({
                    title: 'Hapus Kode?',
                    type: 'warning',
                    title: 'Hapus kode ' + _code + '?',
                    html: '<h5>PERINGATAN!<br><br>Tindakan ini juga akan menghapus semua kode kost booking yang berada di dalam area kota/kab dengan kode ' + _code + '</h5>',
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-danger',
                    showCancelButton: true,
                    cancelButtonClass: 'btn btn-default',
                    showLoaderOnConfirm: true,
                    confirmButtonText: 'Yes, remove it!',
                    preConfirm: (val) => {
                        return $.ajax({
                            type: 'POST',
                            url: "/admin/room/ucode/remove",
                            dataType: 'json',
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            data: {
                                'cities': _selectedCities,
                                'code': _code,
                                '_token': token
                            },
                            success: (response) => {
                                return;
                            },
                            error: (error) => {
                                Swal.insertQueueStep({
                                    type: 'error',
                                    title: 'Error: ' + error.message
                                });
                            }
                        });
                    },
                    allowOutsideClick: () => !Swal.isLoading()
                }).then((result) => {
                    if (!result.value.success) {
                        Swal.fire({
                            type: 'error',
                            title: "Gagal!",
                            html: '<strong>' + result.value.message + '</strong>'
                        });
                    } else {
                        Swal.fire({
                            type: 'success',
                            title: "Kode berhasil dihapus",
                            text: "Halaman akan di-refresh setelah Anda klik OK",
                            onClose: () => {
                                window.location.reload();
                            }
                        });
                    }
                })
            }

            // Generate room's dodes
            else if (action == 'generate') {
                var city = $(e.currentTarget).data('city');

                Swal.fire({
                    title: 'Generate Code Baru?',
                    html: "<strong>Tindakan ini akan meng-generate kode baru untuk semua kos booking di wilayah " + city + ".<br>Klik Continue untuk melanjutkan.</strong>",
                    type: 'question',
                    showCancelButton: true,
                    showLoaderOnConfirm: true,
                    confirmButtonText: 'Continue',
                    preConfirm: (val) => {
                        return $.ajax({
                            type: 'POST',
                            url: "/admin/room/ucode/generate",
                            dataType: 'json',
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            data: {
                                'city': city,
                                'code': _code,
                                '_token': token
                            },
                            success: (response) => {
                                return
                            },
                            error: (error) => {
                                Swal.insertQueueStep({
                                    type: 'error',
                                    title: 'Error: ' + error.message
                                });
                            }
                        });
                    },
                    allowOutsideClick: () => !Swal.isLoading()
                }).then((result) => {
                    if (result.value.success) {
                        Swal.fire({
                            type: 'success',
                            title: "Berhasil generate kode kos booking di " + city,
                            html: "<strong>Kunjungi menu 'Kost List' untuk melihat hasil generator.</strong><br/>Halaman akan di-refresh setelah Anda klik OK",
                            onClose: () => {
                                window.location.reload();
                            }
                        });
                    } else {
                        triggerAlert('error', result.value.data);
                    }
                })
            }
        });

        // 
        // :: Modal Events ::
        // 
        // Add Modal
        $('#add-modal')
            .on("show.bs.modal", (e) => {
                triggerLoading('Menyiapkan form..');

                $('#add-modal').data('bs.modal').options.keyboard = false;
                $('#add-modal').data('bs.modal').options.backdrop = 'static';

                function getUsedCodes() {
                    var codes = [];
                    $.ajax({
                        type: 'GET',
                        url: '/admin/room/codes'
                    }).then(function (data) {
                        if (data.length > 0) {
                            $.each(data, (key, val) => {
                                codes.push(val);
                            })
                        }
                    });
                    return codes;
                }

                function getFormData() {
                    var formData = {};
                    formData.cities = $('#dataCity').val();
                    formData.code = _code;
                    return formData;
                }

                function validateData() {
                    var updateCityList = $('#dataCity').val();

                    // check if code is already in use
                    if ($.inArray(_code, _usedCodes) !== -1) {
                        Swal.fire({
                            type: 'error',
                            customClass: {
                                container: 'custom-swal'
                            },
                            title: 'Kode ini telah terpakai, silakan gunakan kode yang lain'
                        }).then((result) => {
                            $('#dataCode').val('').focus();
                        });
                        return false;
                    }

                    // check for empty dropdown
                    if (updateCityList == null || updateCityList == '') {
                        Swal.fire({
                            type: 'error',
                            customClass: {
                                container: 'custom-swal'
                            },
                            title: 'Pilih kota/kab terlebih dahulu'
                        }).then((result) => {
                            $('#dataCity').select2('open');
                        });
                        return false;
                    }
                    
                    return true;
                }

                // Populate selector
                var citySelector = $('#dataCity');
                $.ajax({
                    type: 'GET',
                    url: '/admin/room/cities/clean'
                }).then(function (data) {
                    var count = data.length;
                    $.each(data, (index, data) => {
                        var option = new Option(data.name, data.id, false, false);
                        citySelector.append(option).trigger('change');
                    });

                    // manually trigger the `select2:select` event
                    citySelector.trigger({
                        type: 'select2:select',
                        params: {
                            data: data
                        }
                    });
                    
                    if (Swal.isLoading()) Swal.close();
                });

                // initial variables
                var _usedCodes = getUsedCodes();

                // setup dropdown
                $('#dataCity').select2({
                    theme: "bootstrap",
                    dropdownParent: $("#add-modal"),
                    placeholder: 'Ketik nama kota/kab untuk mencari'
                });

                // initially disabled
                var citySelector = $('#dataCity');

                // Listeners
                $('#dataCode').on('keyup', e => {
                    var val = $(e.currentTarget).val();
                    _code = val.toUpperCase();
                })

                // Submit event
                $('#add-submit').on('click', e => {
                    var dataObject = getFormData();

                    if (validateData()) {
                        Swal.fire({
                            type: 'question',
                            customClass: {
                                container: 'custom-swal'
                            },
                            title: 'Simpan data kota/kab dengan kode ' + $('#dataCode').val().toUpperCase() + '?',
                            showCancelButton: true,
                            confirmButtonText: 'Continue',
                            showLoaderOnConfirm: true,
                            preConfirm: () => {
                                return $.ajax({
                                    type: 'POST',
                                    url: "/admin/room/ucode",
                                    dataType: 'json',
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                    },
                                    data: dataObject,
                                    success: (response) => {
                                        return
                                    },
                                    error: (error) => {
                                        triggerAlert('error', error);
                                    }
                                });
                            },
                            allowOutsideClick: () => !Swal.isLoading()
                        }).then((result) => {
                            if (!result.dismiss) {
                                $('#add-modal').modal('toggle');

                                if (!result.value.success) {
                                    Swal.fire({
                                        type: 'error',
                                        title: "Gagal!",
                                        html: '<strong>' + result.value.message + '</strong>'
                                    });
                                } else {
                                    Swal.fire({
                                        type: 'success',
                                        customClass: {
                                            container: 'custom-swal'
                                        },
                                        title: "Berhasil menyimpan kode baru",
                                        html: "Halaman akan di-refresh setelah Anda klik OK",
                                        onClose: () => {
                                            window.location.reload();
                                        }
                                    });
                                }
                            }
                        })
                    }
                })
            })
            .on("hidden.bs.modal", (e) => {
                _editing = false;
                _code = null;

                $('#dataCity').empty().select2('destroy');
            });

        $('#import-modal')
            .on("show.bs.modal", (e) => {

                $('#import-submit').on('click', e => {
                    const dataObject = new FormData();
                    const file = $('#dataFile').prop('files')[0];
                    dataObject.append('file', file);

                    if (file) {
                        Swal.fire({
                            type: 'question',
                            customClass: {
                                container: 'custom-swal'
                            },
                            title: 'Import booking code dari csv ?',
                            showCancelButton: true,
                            confirmButtonText: 'Continue',
                            showLoaderOnConfirm: true,
                            preConfirm: () => {
                                return $.ajax({
                                    type: 'POST',
                                    url: "/admin/room/ucode/import",
                                    contentType: false,
                                    processData: false,
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                    },
                                    data: dataObject,
                                    success: (response) => {
                                        return
                                    },
                                    error: (error) => {
                                        triggerAlert('error', error);
                                    }
                                });
                            },
                            allowOutsideClick: () => !Swal.isLoading()
                        }).then((result) => {
                            if (!result.dismiss) {
                                $('#import-modal').modal('toggle');

                                if (!result.value.success) {
                                    Swal.fire({
                                        type: 'error',
                                        title: "Gagal!",
                                        html: '<strong>' + result.value.data + '</strong>'
                                    });
                                } else {
                                    Swal.fire({
                                        type: 'success',
                                        customClass: {
                                            container: 'custom-swal'
                                        },
                                        title: "Berhasil menyimpan kode baru",
                                        html: "Halaman akan di-refresh setelah Anda klik OK",
                                        onClose: () => {
                                            window.location.reload();
                                        }
                                    });
                                }
                            }
                        })
                    }
                })
            });

        // Update modal event
        $('#update-modal')
            .on("show.bs.modal", (e) => {
                triggerLoading('Mengambil data kota/kabupaten..');

                $('#update-modal').data('bs.modal').options.keyboard = false;
                $('#update-modal').data('bs.modal').options.backdrop = 'static';
                
                $('#update-modal-title').html('<i class="fa fa-edit"></i>  Update data kota/kabupaten dengan kode: <strong>' + _code + '</strong>');

                // setup dropdown
                $('#dataCityUpdate').select2({
                    theme: "bootstrap",
                    dropdownParent: $("#update-modal")
                });

                // Populate selector
                var citySelector = $('#dataCityUpdate');
                $.ajax({
                    type: 'GET',
                    url: '/admin/room/cities'
                }).then(function (data) {
                    var count = data.length;
                    $.each(data, (index, data) => {
                        if (_selectedCities != '') {
                            if ($.inArray(data.name, _selectedCities) !== -1) {
                                var option = new Option(data.name, data.id, true, true);
                            } else {
                                var option = new Option(data.name, data.id, false, false);
                            }
                        } else {
                            var option = new Option(data.name, data.id, false, false);
                        }
                        citySelector.append(option).trigger('change');

                        if (!--count) {
                            if (Swal.isLoading()) Swal.close();
                        }
                    });

                    // manually trigger the `select2:select` event
                    citySelector.trigger({
                        type: 'select2:select',
                        params: {
                            data: data
                        }
                    });
                });

                function getFormData() {
                    var formData = {};
                    formData.cities = $('#dataCityUpdate').val();
                    formData.code = _code;
                    return formData;
                }

                function validateData() {
                    var updateCityList = $('#dataCityUpdate').val();

                    // check for empty dropdown
                    if (updateCityList == null || updateCityList == '') {
                        Swal.fire({
                            type: 'error',
                            customClass: {
                                container: 'custom-swal'
                            },
                            title: 'Pilih kota/kab terlebih dahulu'
                        }).then((result) => {
                            $('#dataCityUpdate').select2('open');
                        });
                        return false;
                    }
                    
                    // check for same values
                    updateCityList = convertStringArrayToInt(updateCityList);
                    var is_equal = (updateCityList.length == _selectedCityIds.length) && updateCityList.every(function(element, index) {
                        return element == _selectedCityIds[index]; 
                    });

                    if (is_equal) {
                        Swal.fire({
                            type: 'error',
                            customClass: {
                                container: 'custom-swal'
                            },
                            title: 'Tidak perubahan data kota/kab'
                        }).then((result) => {
                            $('#dataCityUpdate').select2('open');
                        });
                        return false;
                    }
                    
                    return true;
                }

                // Submit event
                $('#submit').on('click', e => {
                    var dataObject = getFormData();

                    if (validateData()) {
                        Swal.fire({
                            type: 'question',
                            customClass: {
                                container: 'custom-swal'
                            },
                            title: 'Update data kota/kab dengan kode ' + _code + '?',
                            html: '<h5><strong>Tindakan ini tidak dapat dibatalkan!</strong><br><br>Semua kost booking yang berada di dalam area kota/kab yang baru ditambahkan/dihapus, akan kehilangan kode BBK.<br>Anda harus menggenerate ulang kode tersebut. <h5>',
                            showCancelButton: true,
                            confirmButtonText: 'Continue',
                            showLoaderOnConfirm: true,
                            preConfirm: () => {
                                return $.ajax({
                                    type: 'POST',
                                    url: "/admin/room/ucode/update",
                                    dataType: 'json',
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                    },
                                    data: dataObject,
                                    success: (response) => {
                                        return
                                    },
                                    error: (error) => {
                                        triggerAlert('error', error);
                                    }
                                });
                            },
                            allowOutsideClick: () => !Swal.isLoading()
                        }).then((result) => {
                            if (!result.dismiss) {
                                $('#update-modal').modal('toggle');

                                if (!result.value.success) {
                                    Swal.fire({
                                        type: 'error',
                                        title: "Gagal!",
                                        html: '<strong>' + result.value.message + '</strong>'
                                    });
                                } else {
                                    Swal.fire({
                                        type: 'success',
                                        customClass: {
                                            container: 'custom-swal'
                                        },
                                        title: "Berhasil mengupdate data kota/kab!",
                                        html: "Halaman akan di-refresh setelah Anda klik OK",
                                        onClose: () => {
                                            window.location.reload();
                                        }
                                    });
                                }
                            }
                        })
                    }
                })

            })
            .on("hidden.bs.modal", (e) => {
                _editing = false;
                _code = null;

                $('#dataCityUpdate').empty().select2('destroy');
            });
    });
</script>
@stop