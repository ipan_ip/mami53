@extends('admin.layouts.main')

@section('style')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/dropzone.css">
    {{ HTML::style('assets/vendor/jquery-chosen/chosen.css') }}
    {{ HTML::style('assets/vendor/jquery-chosen-custom/chosen.css') }}
    <link rel="stylesheet" href="/assets/vendor/summernote/summernote.css?v=05042018">
    <link rel="stylesheet" href="{{ mix_url('dist/vendor/leaflet/leaflet.css') }}">

    <style>
        #chosenForm .chosen-choices {
            border: 1px solid #ccc;
            border-radius: 4px;
            min-height: 34px;
            padding: 6px 12px;
        }
        #chosenForm .form-control-feedback {
            /* To make the feedback icon visible */
            z-index: 100;
        }

        #map-canvas-1,
        #map-canvas-2 {
            height: 300px;
            width: 100%;
        }

        #map-canvas {
            height: 400px;
            width: 100%;
        }
    </style>
@endsection

@section('content')
<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">{{ $boxTitle }}</h3>
    </div><!-- /.box-header -->
    <form action="{{ route('admin.landing-apartment.store') }}" method="post" class="form-horizontal form-bordered">
        <div class="box-body no-padding">
            <div class="form-group bg-default">
                <label for="inputUrlSlug" class="col-sm-2 control-label">URL Slug</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="URL Slug" id="inputSlugUrl" name="slug"  value="{{ old('slug') }}">
                </div>
            </div>
            <div class="form-group bg-default">
                <label for="inputParentLandingPage" class="col-sm-2 control-label">Parent Landing Page</label>
                <div class="col-sm-10">
                    <select class="form-control chosen-select" id="inputParentLandingPage" name="parent_id" tabindex="2">
                        <option value="0"> NULL</option> 
                        @foreach ($parentOptions as $parentOption)
                            <option value="{{ $parentOption->id }}" {{ old('parent_id') == $parentOption->id ? 'selected="selected"' : '' }}>{{ $parentOption->heading_1 . ' | ' . $parentOption->slug }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group bg-default">
                <label for="inputHeading1" class="col-sm-2 control-label">Heading 1</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Heading 1" id="inputHeading1" name="heading_1"  value="{{ old('heading_1') }}">
                </div>
            </div>
            <div class="form-group bg-default">
                <label for="inputHeading1" class="col-sm-2 control-label">Heading 2</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Heading 2" id="inputHeading2" name="heading_2"  value="{{ old('heading_2') }}">
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputKeyword" class="col-sm-2 control-label">Keyword</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Keyword" id="inputKeyword" name="keyword"  value="{{ old('keyword') }}">
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputGeoName" class="col-sm-2 control-label">Koordinat</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Geo Name"
                           id="inputGeoName" name="geo_name">
                </div>
                <div class="col-sm-10 col-sm-offset-2">
                    <div style="background: #EEE; width: 100%; height: 400px;">
                        <div id="map-canvas"></div>
                    </div>
                </div>
                <div class="col-sm-10 col-sm-offset-2">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="latitude 1"
                           id="inputLatitude1" name="latitude_1" value="{{ old('latitude_1') }}">
                        <input type="text" class="form-control" placeholder="longitude 1"
                           id="inputLongitude1" name="longitude_1" value="{{ old('longitude_1') }}">
                    </div>

                    <hr>

                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="latitude 2"
                           id="inputLatitude2" name="latitude_2" value="{{ old('latitude_2') }}">
                        <input type="text" class="form-control" placeholder="longitude 2"
                           id="inputLongitude2" name="longitude_2" value="{{ old('longitude_2') }}">
                    </div>
                </div>
            </div>

            <!-- <div class="form-group bg-default">
                <label for="inputGeoName2" class="col-sm-2 control-label">Koordinat 2</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Geo Name"
                           id="inputGeoName2" name="geo_name_2">
                </div>
                <div class="col-sm-10 col-sm-offset-2">
                    <div style="background: #EEE; width: 100%; height: 300px;">
                        <div id="map-canvas-2"></div>
                    </div>
                </div>
                <div class="col-sm-10 col-sm-offset-2">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="latitude 2"
                           id="inputLatitude2" name="latitude_2" value="{{ old('latitude_2') }}">
                        <input type="text" class="form-control" placeholder="longitude 2"
                           id="inputLongitude2" name="longitude_2" value="{{ old('longitude_2') }}">
                   </div>
                </div>
            </div> -->

            <div class="form-group bg-default">
                <label for="inputDescription1" class="col-sm-2 control-label">Description Text 1</label>
                <div class="col-sm-10">
                    <textarea rows="8" type="text" class="form-control" id="inputDescription1" name="description_1"> {{ old('description_1')}}</textarea>
                </div>
                <div id="charCount"></div>
            </div>
            <div class="form-group bg-default">
                <label for="inputDescription1" class="col-sm-2 control-label">Description Text 2</label>
                <div class="col-sm-10">
                    <textarea rows="8" type="text" class="form-control" id="inputDescription2" name="description_2" > {{ old('description_2') }}</textarea>
                </div>
            </div>
            <div class="form-group bg-default">
                <label for="inputDescription1" class="col-sm-2 control-label">Description Text 3</label>
                <div class="col-sm-10">
                    <textarea rows="8" type="text" class="form-control" id="inputDescription3" name="description_3" > {{ old('description_3') }}</textarea>
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputPriceMin" class="col-sm-2 control-label">Price Min</label>
                <div class="col-sm-10">
                    <input type="number" class="form-control" placeholder="Price Min" id="inputPriceMin" name="price_min"  value="{{ old('price_min') }}">
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputPriceMax" class="col-sm-2 control-label">Price Max</label>
                <div class="col-sm-10">
                    <input type="number" class="form-control" placeholder="Price Max" id="inputPriceMax" name="price_max"  value="{{ old('price_max') }}">
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputRentType" class="col-sm-2 control-label">Rent Type</label>
                <div class="col-sm-10">
                    <select name="rent_type" id="inputRentType" class="form-control chosen-select">
                        @foreach($rentTypeOptions as $key => $rentType)
                            <option value="{{ $key }}" {{ old('rent_type', 2) == $key ? 'selected="selected"' : '' }}>{{ $rentType }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="unitType" class="col-sm-2 control-label">Unit Type</label>
                <div class="col-sm-10">
                    <select name="unit_type" id="unitType" class="form-control chosen-select">
                        <option value="-" {{ old('unit_type') == '-' || is_null(old('unit_type')) ? 'selected="selected"' : '' }}>Semua</option>
                        @foreach($unitTypeOptions as $key => $unitType)
                            <option value="{{ $unitType }}" {{ old('unit_type') == $unitType ? 'selected="selected"' : '' }}>{{ $unitType }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputRentType" class="col-sm-2 control-label">Perabotan</label>
                <div class="col-sm-10">
                    <select name="is_furnished" id="perabotType" class="form-control chosen-select">
                        <option selected="selected" value="-" {{ old('is_furnished') == '-'? 'selected="selected"' : '' }}>Semua</option>
                        <option value="1" {{ old('is_furnished') == '1' ? 'selected="selected"' : '' }}>Furnished</option>
                        <option value="0" {{ old('is_furnished') == '0' ? 'selected="selected"' : '' }}>Non Furnished</option>
                    </select>
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputTag" class="col-sm-2 control-label">Tags</label>
                <div class="col-sm-10">
                    <select class="form-control chosen-select" multiple id="inputTag" name="tag_ids[]" tabindex="2">
                        @foreach ($tagOptions as $tag)
                            <option value="{{ $tag->id }}" {{ !is_null(old('tag_ids[]')) && in_array($tag->id, old('tag_ids[]')) ? 'selected="selected"' : '' }}>{{ $tag->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="photo" class="control-label col-sm-2">Foto</label>
                <div class="col-sm-10">
                    <div id="photo"
                          action="{{ url('admin/media') }}"
                          method="POST"
                          class="dropzone"
                          uploadMultiple="no">
                        {{ csrf_field() }}
                        <input type="hidden" name="media_type" value="style_photo">
                    </div>
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="dummy-counter" class="col-sm-2 control-label">Dummy Counter</label>
                <div class="col-sm-10">
                    <input type="number" class="form-control" placeholder="Dummy Counter" id="dummy-counter" name="dummy_counter">
                    <p class="help-block">
                        Dummy counter digunakan untuk meng-inject jumlah kost pada meta description landing
                    </p>
                </div>
            </div>


            <div id="photo-wrapper"></div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </form>
</div><!-- /.box -->
@endsection



@section('script')
{{ HTML::script('assets/vendor/jquery-chosen/chosen.jquery.js') }}
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/dropzone.js"></script>
<script src="/assets/vendor/summernote/summernote.min.js"></script>


<script>
    var config = {
        '.chosen-select'           : {width: '100%'}
    };

    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>


<script src="https://maps.googleapis.com/maps/api/js?libraries=places&language=id&key={{ENV('MIX_GOOGLE_MAPS_JAVASCRIPT_API_KEY')}}"></script>
<script src="{{ mix_url('dist/vendor/leaflet/leaflet.js') }}"></script>

<script type="text/javascript">
    function addMarkerListener(map, marker, pos) {
        marker.on('dragend', function(evt) {
            var latlng = evt.target.getLatLng();

            var lat = latlng.lat;
            var lng = latlng.lng;

            $('#inputLatitude' + pos).val(lat);
            $('#inputLongitude' + pos).val(lng);
        });
    }

    function addPlaceChangedListener(autocomplete, map, marker) {
        autocomplete.addListener('place_changed', function() {
            marker[0].setOpacity(0);
            marker[1].setOpacity(0);

            var place = autocomplete.getPlace();
            if (!place.geometry) {
                // User entered the name of a Place that was not suggested and
                // pressed the Enter key, or the Place Details request failed.
                window.alert("No details available for input: '" + place.name + "'");
                return;
            }

            map.setView({
                lat: place.geometry.location.lat(),
                lng: place.geometry.location.lng()
            }, 12);

            bottomLeft = {
                lat: place.geometry.location.lat() - 0.05,
                lng: place.geometry.location.lng() - 0.05
            }

            topRight = {
                lat: place.geometry.location.lat() + 0.05,
                lng: place.geometry.location.lng() + 0.05
            }

            marker[0].setLatLng(bottomLeft);
            marker[1].setLatLng(topRight);
            marker[0].setOpacity(1);
            marker[1].setOpacity(1);


            $('#inputLatitude1').val(bottomLeft.lat);
            $('#inputLongitude1').val(bottomLeft.lng);
            $('#inputLatitude2').val(topRight.lat);
            $('#inputLongitude2').val(topRight.lng);
        });
    }

    centerPos = {lat: -7.7858485, lng: 110.3680087}

    var map = L.map('map-canvas', {
        // Set latitude and longitude of the map center (required)
        center: centerPos, 
        // Set the initial zoom level, values 0-18, where 0 is most zoomed-out (required)
        zoom: 12
    });

    var tiles = new L.tileLayer("{{ config('services.osm.host') }}/hot/{z}/{x}/{y}.png").addTo(map);

    var marker = [];
    var markerImage = [];
    var position = [];

    // marker kiri bawah
    markerImage[0] = '{{ asset('assets/icons/kiribawah.png') }}';
    
    position[0] = {
        'lat': centerPos.lat - 0.05,
        'lng': centerPos.lng - 0.05
    }

    marker[0] = new L.Marker(position[0], {
        draggable: true,
        icon: new L.icon({
            iconUrl: markerImage[0],
            iconSize: [60, 57],
            iconAnchor: [30, 57]
        })
    }).addTo(map);

    addMarkerListener(map, marker[0], '1');

    // marker kanan atas
    markerImage[1] = '{{ asset('assets/icons/kananatas.png') }}';
    
    position[1] = {
        'lat': centerPos.lat + 0.05,
        'lng': centerPos.lng + 0.05
    }

    marker[1] = new L.Marker(position[1], {
        draggable: true,
        icon: new L.icon({
            iconUrl: markerImage[1],
            iconSize: [60, 57],
            iconAnchor: [30, 57]
        })
    }).addTo(map);

    addMarkerListener(map, marker[1], '2');

    var autocomplete = new google.maps.places.Autocomplete(document.getElementById('inputGeoName'));
    autocomplete.setFields(['address_components', 'geometry']);

    addPlaceChangedListener(autocomplete, map, marker);
</script>

<script type="text/javascript">
    // var geocoder = new google.maps.Geocoder();

    // function addMarkerListener(map, marker, pos) {
    //     google.maps.event.addListener(marker, 'dragend', function(evt) {
    //         var lat = evt.latLng.lat();
    //         var lng = evt.latLng.lng();

    //         var latlng = new google.maps.LatLng(lat, lng);

    //         $('#inputLatitude' + pos).val(lat);
    //         $('#inputLongitude' + pos).val(lng);
    //     });
    // }

    // function addPlaceChangedListener(autocomplete, map, marker) {
    //     autocomplete.addListener('place_changed', function() {
    //         marker[0].setVisible(false);
    //         marker[1].setVisible(false);

    //         var place = autocomplete.getPlace();
    //         if (!place.geometry) {
    //             // User entered the name of a Place that was not suggested and
    //             // pressed the Enter key, or the Place Details request failed.
    //             window.alert("No details available for input: '" + place.name + "'");
    //             return;
    //         }

    //         map.setCenter(place.geometry.location);

    //         bottomLeft = {
    //             lat: place.geometry.location.lat() - 0.05,
    //             lng: place.geometry.location.lng() - 0.05
    //         }

    //         topRight = {
    //             lat: place.geometry.location.lat() + 0.05,
    //             lng: place.geometry.location.lng() + 0.05
    //         }

    //         marker[0].setPosition(bottomLeft);
    //         marker[1].setPosition(topRight);
    //         marker[0].setVisible(true);
    //         marker[1].setVisible(true);


    //         $('#inputLatitude1').val(bottomLeft.lat);
    //         $('#inputLongitude1').val(bottomLeft.lng);
    //         $('#inputLatitude2').val(topRight.lat);
    //         $('#inputLongitude2').val(topRight.lng);
    //     });
    // }

    $(function () {

        // centerPos = {lat: -7.7858485, lng: 110.3680087}
        
        // var map = new google.maps.Map(document.getElementById("map-canvas"),
        //     {
        //         center: centerPos,
        //         zoom:12
        //     }
        // );

        // var marker = [];
        // var markerImage = [];
        // var position = [];

        // markerImage[0] = '{{ asset('assets/icons/kiribawah.png') }}';
        
        // position[0] = {
        //     'lat': centerPos.lat - 0.05,
        //     'lng': centerPos.lng - 0.05
        // }

        // marker[0] = new google.maps.Marker({
        //     position: position[0],
        //     map: map,
        //     draggable: true,
        //     icon: markerImage[0]
        // });

        // addMarkerListener(map, marker[0], 1);

        // // marker kanan bawah
        // markerImage[1] = '{{ asset('assets/icons/kananatas.png') }}';
        
        // position[1] = {
        //     'lat': centerPos.lat + 0.05,
        //     'lng': centerPos.lng + 0.05
        // }

        // marker[1] = new google.maps.Marker({
        //     position: position[1],
        //     map: map,
        //     draggable: true,
        //     icon: markerImage[1]
        // });

        // addMarkerListener(map, marker[1], 2);


        // var autocomplete = new google.maps.places.Autocomplete(document.getElementById('inputGeoName'));
        // autocomplete.bindTo('bounds', map);

        // addPlaceChangedListener(autocomplete, map, marker);

        

        $('#inputDescription1, #inputDescription2, #inputDescription3').summernote({
            height: 300,
            toolbar: [
                ['style', ['style', 'bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['insert', ['link', 'table', 'hr']],
                ['misc', ['codeview']]
            ]
        });

        Dropzone.autoDiscover = false;

        Dropzone.options.photo = {
            paramName : "media",
            maxFilesize : 6,
            maxFiles: 1,
            accepFiles: '.jpg, .jpeg, .png, .bmp',
            dictDefaultMessage : 'Upload photo for cover',
            addRemoveLinks: true,
            success: function(file, response) {
                $('#photo-wrapper').append('<input type="hidden" name="photo_cover" value="' +response.media.id+ '">');
            },
            init: function() {
                this.on('removedfile', function(file) {
                    $('#photo-wrapper').html('');
                });
            }
        }

        $('#photo').dropzone();
    });
</script>
@endsection
