@extends('admin.layouts.main')

@section('style')
    <style>
        tr.redirected td {
            color: #ff0000;
        }

        .text-redirected {
            color: #ff0000;
        }
    </style>
@endsection

@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <a href="{{ URL::route('admin.landing-apartment.create') }}">
                        <button type="button" name="button" class="btn-add btn btn-primary btn-sm">
                            <i class="fa fa-plus">&nbsp;</i> Add Landing Apartment </button>
                    </a>
                    {{ Form::open(array('method'=>'get','class'=>'form-inline','style'=>'text-align:right;padding:10px;')) }}
                        <input type="text" name="q" class="form-control input-sm"  placeholder="Keyword"  autocomplete="off" value="{{ Input::old('q') }}">
                        <button class="btn btn-primary btn-md" id="buttonSearch"><i class="fa fa-search">&nbsp;</i>Search</button>
                    {{ Form::close() }}
                </div>
            </div>

            <p class="helper-block">Warna <span class="text-redirected">Merah</span> berarti landing di-redirect ke landing lain</p>

            <table id="tableListLanding" class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Slug</th>
                        <th>Heading 1</th>
                        <th>Heading 2</th>
                        <th>Keyword</th>
                        <th>Parent</th>
                        <th>Price</th>
                        <th>Tags</th>
                        <th>
                            <a href="{{ url()->current() . '?' . http_build_query($sortUrl['view_count']) }}">
                                @if (isset($currentQueryString['sort_dir']) && $currentQueryString['sort_dir'] == 'asc')
                                    <i class="fa fa-arrow-up"></i>
                                @elseif (isset($currentQueryString['sort_dir']) && $currentQueryString['sort_dir'] == 'desc')
                                    <i class="fa fa-arrow-down"></i>
                                @else 
                                    <i class="fa fa-circle"></i>
                                @endif
                                &nbsp;View Count
                            </a>
                        </th>
                        <th>Update</th>
                        <th class="table-action-column" width="120px">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($rowsLanding as $rowLandingKey => $rowLanding)
                    <tr class="{{ !is_null($rowLanding->redirect_id) ? 'redirected' : '' }}">
                        <td>{{ $rowLanding->id }}</td>
                        <td>{{ $rowLanding->slug     }}</td>
                        <td>{{ $rowLanding->heading_1}}</td>
                        <td>{{ $rowLanding->heading_2}}</td>
                        <td>{{ $rowLanding->keyword  }}</td>
                        <td>{{ @$rowLanding->parent->keyword  }}</td>
                        <td>{{ $rowLanding->price_min}} - {{ $rowLanding->price_max }}</td>
                        <td>{{ $rowLanding->tags_name }}</td>
                        <td>{{ $rowLanding->view_count }}</td>
                        <td>{{ date('d M Y H:i:s', strtotime($rowLanding->updated_at)) }}</td>
                        <td class="table-action-column">
                            <div class="btn-action-group">
                                <a href="{{ route("admin.landing-apartment.edit", $rowLanding->id) }}" title="Edit Landing">
                                    <i class="fa fa-pencil"></i></a>

                                @permission('access-landing-apartment-delete')
                                <a href="{{ route("admin.landing-apartment.destroy", $rowLanding->id) }}" class="btn-delete" title="Delete Landing">
                                    <i class="fa fa-trash-o"></i></a>
                                @endpermission

                                <a href="{{ url('https://mamikos.com/daftar/' . $rowLanding->slug) }}" title="Show Preview" target="_blank" rel="noopener">
                                    <i class="fa fa-eye"></i></a>

                                @permission('access-landing-apartment-redirect')    
                                <a href="{{ url('/admin/landing-apartment/redirect/' . $rowLanding->id) }}" title="Redirect" >
                                    <i class="fa fa-exchange"></i>
                                </a>
                                @endpermission

                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="box-body no-padding">
                {{ $rowsLanding->appends(Request::except('page'))->links() }}
            </div>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
    <!-- table -->
@endsection
