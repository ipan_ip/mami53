@extends('admin.layouts.main')

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }} ({{ $notificationSurvey->name }})</h3>
        </div>

        <form action="{{ URL::route('admin.notification-survey.recipient.update', [$notificationSurvey->id, $recipient->id]) }}" method="POST" class="form-horizontal form-bordered">
            <div class="box-body no-padding">
                <div class="form-group bg-default">
                    <label for="user_id" class="control-label col-sm-2">ID User</label>
                    <div class="col-sm-10">
                        <input type="tel" name="user_id" value="{{ old('user_id', $recipient->user_id) }}" id="user-id" class="form-control">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <div class="col-sm-10 col-sm-push-2">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('script')
<script>
    $(document).ready(function() {
        
    });
</script>
@endsection