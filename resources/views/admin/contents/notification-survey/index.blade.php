@extends('admin.layouts.main')

@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <form action="" class="form-inline" method="get" style="text-align:right;padding:10px;">
                        <a href="{{ URL::route('admin.notification-survey.create') }}" class="btn btn-primary btn-sm pull-left">
                            <i class="fa fa-plus">&nbsp;</i> Tambah Notifikasi Survey
                        </a>
                        
                        <input type="text" name="q" class="form-control input-sm"  placeholder="Nama"  autocomplete="off" value="{{ request()->input('q') }}">
                        <button class="btn btn-primary btn-md" id="buttonSearch"><i class="fa fa-search">&nbsp;</i>Search</button>
                    </form>
                </div>
            </div>

            <table  class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nama Survey</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($notificationSurveys as $key => $notificationSurvey)
                    <tr>
                        <td>{{ $notificationSurvey->id }}</td>
                        <td>{{ $notificationSurvey->name }}</td>
                        <td>{{ $notificationSurvey->state }}</td>
                        <td>
                            <a href="{{ URL::route('admin.notification-survey.edit', $notificationSurvey->id) }}" title="Edit">
                                <i class="fa fa-pencil"></i>
                            </a>

                            <a href="{{ URL::route('admin.notification-survey.testnotif', $notificationSurvey->id) }}" title="Kirim Test Notifikasi">
                                <i class="fa fa-location-arrow"></i>
                            </a>

                            <a href="{{ URL::route('admin.notification-survey.recipient.index', $notificationSurvey->id) }}" title="Penerima Voucher">
                                <i class="fa fa-users"></i>
                            </a>



                            @if(in_array($notificationSurvey->state, [$notificationSurvey::STATE_NEW, $notificationSurvey::STATE_PAUSED, $notificationSurvey::STATE_STOPPED]))
                                <a href="{{ URL::route('admin.notification-survey.run', $notificationSurvey->id) }}" title="Mulai Kirim Notifikasi">
                                    <i class="fa fa-play"></i>
                                </a>
                            @elseif($notificationSurvey->state == $notificationSurvey::STATE_RUNNING)
                                <a href="{{ URL::route('admin.notification-survey.pause', $notificationSurvey->id) }}" title="Tunda Kirim Notifikasi">
                                    <i class="fa fa-pause"></i>
                                </a>
                                <a href="{{ URL::route('admin.notification-survey.stop', $notificationSurvey->id) }}" title="Stop Kirim Notifikasi">
                                    <i class="fa fa-stop"></i>
                                </a>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
                
            </table>
        </div><!-- /.box-body -->
        <div class="box-body no-padding">
            {{ $notificationSurveys->appends(Request::except('page'))->links() }}
        </div>
    </div><!-- /.box -->
    <!-- table -->
@endsection