@extends('admin.layouts.main')

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

        <form action="{{ URL::route('admin.notification-survey.store') }}" method="POST" class="form-horizontal form-bordered">
            <div class="box-body no-padding">
                <div class="form-group bg-default">
                    <label for="name" class="control-label col-sm-2">Nama</label>
                    <div class="col-sm-10">
                        <input type="text" name="name" id="name" class="form-control" value="{{ old('name') }}">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="content-sms" class="control-label col-sm-2">Konten SMS</label>
                    <div class="col-sm-10">
                        <input type="text" name="content_sms" id="content-sms" class="form-control" value="{{ old('content_sms') }}">
                        <p class="helper-block">Disarankan maksimal 160 karakter</p>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="content-app" class="control-label col-sm-2">Konten App</label>
                    <div class="col-sm-10">
                        <input type="text" name="content_app" id="content-app" class="form-control" value="{{ old('content_app') }}">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="scheme-sms" class="control-label col-sm-2">Scheme App</label>
                    <div class="col-sm-10">
                        <input type="text" name="scheme_app" id="scheme-app" class="form-control" value="{{ old('scheme_app') }}">
                        <p class="helper-block">
                            Masukkan URL untuk mengarahkan ke webview, atau konsultasi dengan dev untuk mengarahkan ke halaman lain di app
                        </p>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <div class="col-sm-10 col-sm-push-2">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('script')
<script>
    $(document).ready(function() {
        
    });
</script>
@endsection