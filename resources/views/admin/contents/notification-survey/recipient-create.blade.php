@extends('admin.layouts.main')

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }} ({{ $notificationSurvey->name }})</h3>
        </div>

        <form action="{{ URL::route('admin.notification-survey.recipient.store', $notificationSurvey->id) }}" method="POST" class="form-horizontal form-bordered">
            <div class="box-body no-padding">
                <div class="form-group bg-default">
                    <label for="user_ids" class="control-label col-sm-2">ID User</label>
                    <div class="col-sm-10">
                        <textarea name="user_ids" id="user-ids" class="form-control" rows="5" placeholder="555,212,401,dst">{{ old('user_ids') }}</textarea>
                        <p class="helper-block">Pisahkan User ID dengan koma</p>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <div class="col-sm-10 col-sm-push-2">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('script')
<script>
    $(document).ready(function() {
        
    });
</script>
@endsection