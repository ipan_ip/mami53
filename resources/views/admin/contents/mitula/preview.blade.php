@if($rooms)
	<h3>Preview Kost yang Tampil di Feeds</h3>
	<p><em>List dapat berubah sewaktu-waktu tanpa pemberitahuan sebelumnya.</em></p>

	<hr>

	<ul>
		@foreach($rooms as $roomArray)
			@foreach($roomArray as $room)
				<li>{{$room->name}}</li>
			@endforeach
		@endforeach
	</ul>

@else
	<h3>Preview Kost hanya bisa ditampilkan jika jumlah kurang dari 100</h3>
@endif