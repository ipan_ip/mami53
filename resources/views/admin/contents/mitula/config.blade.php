@extends('admin.layouts.main')
@section('content')
  <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">{{ $boxTitle }}</h3>
    </div>
    {{ Form::open(array('url' => route('admin.mitula.config.update'), 'method' => 'post',
      'class' => 'form-horizontal form-bordered', 'role' => 'form', 'id' => 'formUpdateMitula')) }}
      <div class="box-body no-padding">
        <?php $i = 0; ?>
        @foreach($filterCountPair as $fieldName=>$fieldCount)
          <div class="form-group bg-default">
              <label for="{{$fieldName}}" class="col-sm-2 control-label">{{ucwords(implode(' ', explode('_', $fieldName)))}}</label>
              <div class="col-sm-10">
                  <select class="form-control chosen-select" id="{{$fieldName}}" name="{{$fieldName}}">
                      <option value="">-- Pilih Area --</option> 
                      @foreach ($areas as $area)
                          <option value="{{ $area }}"
                              {{ old($fieldName) == $area ? 'selected="selected"' : (old($fieldName) == '' && $filters[$i]['value'] == $area ? 'selected="selected"' : '') }}>{{ $area }}</option>
                      @endforeach
                  </select>
              </div>
          </div>
          
          <div class="form-group bg-default">
            <label for="{{$fieldCount}}" class="col-sm-2 control-label">Jumlah</label>
            <div class="col-sm-10">
              <input type="text" name="{{$fieldCount}}" id="{{$fieldCount}}" class="form-control" value="{{old($fieldCount) != '' ? old($fieldCount) : $outputCounts[$i]['value']}}">
            </div>
          </div>

          <hr>
          <?php $i++; ?>
        @endforeach

        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-primary">Save</button>
            <a href="#" id="btn-preview" class="btn btn-warning">Preview</a>
          </div>
        </div>
      </div>
    {{ Form::close() }}
  </div>

  <div class="modal fade" id="previewer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
          
        </div>
      </div>
    </div>
  </div>


@stop

@section('script')

    {{ HTML::script('assets/vendor/jquery-chosen/chosen.jquery.js') }}
    <script type="text/javascript">
      $(function()
      {

        $('.chosen-select').chosen({'width': '100%'});

        $('#btn-preview').on('click', function(e) {
          e.preventDefault();

          $.ajax({
            url: '/admin/mitula/preview',
            type: 'GET',
            success: function(data) {
              $('#previewer .modal-body').html(data);

              $('#previewer').modal({
                show:true
              });
            }
          })
        });
        
      });
    </script>

@stop

@section('style')
    {{ HTML::style('assets/vendor/jquery-chosen/chosen.css') }}
    {{ HTML::style('assets/vendor/jquery-chosen-custom/chosen.css') }}
@stop