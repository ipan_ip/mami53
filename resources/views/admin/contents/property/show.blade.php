@extends('admin.layouts.main')

@section('style')
<style>
    .total-price {
        font-weight: bold;
    }

    .total-price td:first-child {
        text-align: right;
    }
</style>
@endsection

@section('content')
    <a class="btn btn-xs bg-navy btn-flat" href="{{ route('admin.property.index') }}">Back</a>
    <div class="row">
        <div class="col-sm-4">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Data Property</h3>
                </div>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Owner name</th>
                            <th>Owner phone number</th>
                            <th>Created at</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{ $property->name }}</td>
                            <td>{{ $property->owner_user->name }}</td>
                            <td>{{ $property->owner_user->phone_number }}</td>
                            <td>{{ $property->created_at }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Data Current Property Contract</h3>
                </div>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Level Name</th>
                            <th>Joined at</th>
                            <th>Assigned by</th>
                            <th>Total Package</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if ($property->property_active_contract !== null)
                            <tr>
                                <td>{{ $property->property_active_contract->property_level->name }}</td>
                                <td>{{ $property->property_active_contract->joined_at }}</td>
                                <td>{{ !is_null($property->property_active_contract->assigned_by_user) ? $property->property_active_contract->assigned_by_user->name : 'System' }}</td>
                                <td>{{ $property->property_active_contract->total_package }}</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-sm-8">
        <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Data Current Property Kosts</h3>
                </div>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Kost Name</th>
                            <th>Available Room</th>
                            <th>Area</th>
                            <th>Kost Updated at</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($property->rooms as $room)
                            <tr>
                                <td>{{ $room->name }}</td>
                                <td>{{ $room->available_room }}</td>
                                <td>{{ $room->area_formatted }}</td>
                                <td>{{ $room->kost_updated_date }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Data Property Revision History</h3>
                </div>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Changed by</th>
                            <th>What changed</th>
                            <th>Old Value</th>
                            <th>New Value</th>
                            <th>Changed at</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($property->revisionHistory as $revision)
                            <tr>
                                <td>{{ $revision->user->name }}</td>
                                <td>{{ $revision->key }}</td>
                                <td>{{ $revision->old_value }}</td>
                                <td>{{ $revision->new_value }}</td>
                                <td>{{ $revision->created_at }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Data Property Contract History</h3>
                </div>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Level Name</th>
                            <th>Joined at</th>
                            <th>Assigned by</th>
                            <th>Ended at</th>
                            <th>Ended by</th>
                            <th>Status</th>
                            <th>Total Package</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($property->property_contracts as $contract)
                            <tr>
                                <td>{{ $contract->property_level->name }}</td>
                                <td>{{ $contract->joined_at }}</td>
                                <td>{{ !is_null($contract->assigned_by_user) ? $contract->assigned_by_user->name : 'System' }}</td>
                                <td>{{ $contract->ended_at ?? '-' }}</td>
                                <td>{{ $contract->ended_by_user->name ?? '-' }}</td>
                                <td>{{ $contract->status }}</td>
                                <td>{{ $contract->total_package }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
