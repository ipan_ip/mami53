@extends('admin.layouts.main')

@section('content')
    <a class="btn btn-xs bg-navy btn-flat" href="{{ route('admin.property.index') }}">Back</a>
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

        <form action="{{ URL::route('admin.property.store') }}" method="POST" class="form-horizontal form-bordered">
            @csrf
            <div class="box-body no-padding">
                <div class="form-group bg-default">
                    <label for="property-name" class="control-label col-sm-2">Property Name</label>
                    <div class="col-sm-10">
                        <input type="text" name="property_name" id="property-name" class="form-control" value="{{ old('property_name') }}">
                        <small class="help-block">Format Nama Properti tidak boleh berisi karakter: . , ( ) / - " '</small>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="owner-phone-number" class="control-label col-sm-2">Owner Phone Number</label>
                    <div class="col-sm-10">
                        <input type="text" name="owner_phone_number" id="owner-phone-number" class="form-control" value="{{ old('owner_phone_number') }}">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <div class="col-sm-10 col-sm-push-2">
                        <button type="submit" class="btn btn-primary">Create</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
