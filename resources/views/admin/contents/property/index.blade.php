@extends('admin.layouts.main')

@section('style')
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
<link rel="stylesheet" href="/css/admin-consultant.css">

<style>
    .form-filter .form-group {
        margin: 0px 4px;
    }

    .custom-text-terminate-contract {
        border: none;
        background-color: transparent;
        margin-left: 5px;
    }

    .custom-button-terminate-contract {
        display: block;
        padding: 3px 20px;
        clear: both;
        font-weight: 400;
        line-height: 1.42857143;
        color: #333;
        white-space: nowrap;
        margin-block-end: 0em;
    }
</style>
@endsection

@section('content')
<?php App::setLocale('id'); ?>
<!-- table -->
<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">{{ $boxTitle }}</h3>
        <div class="box-tools pull-right">
            <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
            <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
        </div>
    </div><!-- /.box-header -->
    <div class="box-body no-padding">
        <div class="horizontal-wrapper">
            <div class="btn-horizontal-group bg-default" style="overflow:hidden border:1px; solid #ccc;">
                <form action="" class="form-inline" method="get" style="text-align: right">
                    <div class="row" style="padding:5px;margin: 10px 0px 0px 10px;">
                        <div class="col-sm-3">
                            <div style="float:right;">
                                <label>Property Level :</label>
                                <select class="form-control" name="levels[]" multiple>
                                    @foreach ($propertyLevels as $k => $level)
                                    <option value="{{$k}}" @if(!empty(Request()->levels) && in_array($k, Request()->levels)) selected @endif>{{$level}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Nama Property :</label>
                                <input type="text" name="property_name" class="form-control input-sm" placeholder="Nama Property" autocomplete="off" value="{{ Input::get('property_name') }}">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Nama Owner :</label>
                                <input type="text" name="owner_name" class="form-control input-sm" placeholder="Nama Owner" autocomplete="off" value="{{ Input::get('owner_name') }}">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Nomor Telepon Owner :</label>
                                <input type="text" name="owner_phone_number" class="form-control input-sm" placeholder="Nomor Telepon Owner" autocomplete="off" value="{{ Input::get('owner_phone_number') }}">
                            </div>
                        </div>
                    </div>
                    <div class="row" style="padding:5px;margin: 10px 0px 0px 10px;">
                        <button class="btn btn-primary btn-md" id="buttonSearch"><i class="fa fa-search">&nbsp;</i>Search</button>
                    </div>
                </form>
            </div>
            <div class="btn-horizontal-group bg-default" style="overflow:hidden">
                <a href="{{ route('admin.property.create') }}" class="btn btn-danger" style="float: right;"> Create Property </a>
            </div>
            <table id="tableListDesigner" class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Owner</th>
                        <th>Owner Phone Number</th>
                        <th>Level</th>
                        <th style="width:80px">Date</th>
                        <th class="table-action-column" width="80px">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($properties as $property)
                    <tr>
                        <td>{{ $property->id }}</td>
                        <td>{{ $property->name }}</td>
                        <td>{{ $property->owner_user->name ?? '-' }}</td>
                        <td>{{ $property->owner_user->phone_number ?? '-' }}</td>
                        <td>{{ $property->property_active_contract->property_level->name ?? '-'}}</td>
                        <td>{{ $property->created_at->format('d-m-Y h:i') }}</td>
                        <td class="table-action-column" style="padding-right:15px!important;">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-cogs"></i> Actions
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a class="action-recalculate" href="{{ route('admin.property.show', $property->id) }}">
                                            <i class="fa fa-eye"></i> Detail
                                        </a>
                                    </li>
                                    <li>
                                        <a class="action-recalculate" href="{{ route('admin.property.edit', $property->id) }}">
                                            <i class="fa fa-edit"></i> Edit
                                        </a>
                                    </li>
                                    <li>
                                        <a class="action-recalculate" href="{{ route('admin.property.assign.index', $property->id) }}">
                                            <i class="fa fa-check"></i> Assign Kost
                                        </a>
                                    </li>
                                    @if ($property->property_contracts_count === 0 && $property->rooms_count === 0)
                                        <li>
                                            <a class="action-recalculate remove-button" href="" data-id="{{ $property->id }}">
                                                <i class="fa fa-trash"></i> Remove Mapping
                                            </a>
                                        </li>
                                    @endif
                                    {{-- </li>
                                        @if(!$property->has_active_contract)
                                            <li>
                                                <a class="action-recalculate" href="{{ route('admin.property.contract.create', $property->id) }}">
                                                    <i class="fa fa-clipboard"></i> Create Contract
                                                </a>
                                            </li>
                                        @else
                                            <li>
                                                <form class="custom-button-terminate-contract action-recalculate" action="{{ route('admin.property.contract.terminate', $property->id) }}" method="post">
                                                    @method('PATCH')
                                                    @csrf
                                                        <i class="fa fa-ban"></i>
                                                        <input class="custom-text-terminate-contract" type="submit" value="Terminate Contract"
                                                            onClick="return confirm('Apakah Anda yakin mau akhiri kontrak?')"
                                                        />
                                                </form>
                                            </li>
                                            <li>
                                                <a class="action-recalculate" href="{{ route('admin.property.contract.renew', $property->id) }}">
                                                    <i class="fa fa-clipboard"></i> Renew Contract
                                                </a>
                                            </li>
                                        @endif
                                    </li> --}}
                                </ul>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $properties->appends(Request::except('page'))->links() }}
        </div>
    </div>
</div>
@endsection

@section('script')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

<script>
$(function() {
    $('.remove-button').on('click', function(e) {
        e.preventDefault();
        const propertyId = $(this).data('id');

        Swal.fire({
            title: 'Are you sure you want to remove this mapping?',
            confirmButtonText: 'Yes',
            showCancelButton: true,
            cancelButtonText: 'No',
            showLoaderOnConfirm: true,
            preConfirm: () => {
                return axios.post(`/admin/property/${propertyId}`, {
                    _method: 'DELETE'
                })
                .then(response => {
                    Swal.fire({
                        title: 'Done',
                        text: `Mapping has been removed`,
                        icon: 'success',
                        showConfirmButton: true
                    }).then((result) => {
                        if (result.isConfirmed) {
                            window.location.reload();
                        };
                    });
                })
                .catch(error => {
                    Swal.fire({
                        title: 'Error',
                        text: error.response.data.message,
                        icon: 'error',
                        confirmButtonText: 'Ok'
                    });
                });
            },
            allowOutsideClick: () => !Swal.isLoading()
        });
    });
});
</script>
@endsection