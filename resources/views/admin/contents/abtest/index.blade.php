@extends('admin.layouts.main')

@section('style')
  
@endsection

@section('content')
   <div id="app">
      <app></app>
   </div>
@stop

@section('script')
   <script src="{{ mix_url('dist/js/_admin/abtest/app.js') }}"></script>
@stop
