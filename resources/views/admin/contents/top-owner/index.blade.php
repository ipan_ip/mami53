@extends('admin.layouts.main')

@section('style')
<style>
    .reject-remark-filled i.fa {
        color: #ff0000;
    }
    .table > tbody > tr > td {
        vertical-align: middle;
    }
    .font-large {
        font-size: 1.5em;
    }
    .font-semi-large {
        font-size: 1.05em;
    }
    .font-grey {
        color: #9E9E9E;
    }
</style>
@endsection

@section('content')
    <!-- table -->
    <div class="box box-primary">
        <!-- box-head -->
        <div class="box-head text-center" style="margin:20px;">
            <span class="label label-danger font-semi-large"><i class="fa fa-exclamation-circle"></i> Fitur ini dimatikan untuk sementara! (Per tanggal 20 Mei 2019), kecuali untuk SERVER TESTING. Silakan kontak PM atau Developer untuk info lebih lanjut.</span>
        </div>
        <!-- box-body -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <form action="" class="form-inline" method="get" style="text-align:right;padding:10px;">
                        <input type="text" name="owner_name" class="form-control input-sm" placeholder="Owner Name" autocomplete="off" value="{{ request()->input('owner_name') }}">
                        <input type="text" name="phone_owner" class="form-control input-sm" placeholder="No. Telp. Owner" autocomplete="off" value="{{ request()->input('phone_owner') }}" style="margin-right:10px">
                        <!-- {{ Form::select('sort', $sortFilter, request()->input('sort'), array('class' => 'pull-right-sort form-group btn btn-primary btn-md', 'placeholder'=>'Tanpa Pengurutan')) }}
                        {{ Form::select('statistic', $statisticFilter, request()->input('statistic'), array('class' => 'pull-right-sort form-group btn btn-primary btn-md', 'placeholder'=>'Semua Statistik')) }}
                        {{ Form::select('status', $statusFilter, request()->input('status'), array('class' => 'pull-right-sort form-group btn btn-primary btn-md', 'placeholder'=>'Semua Status')) }} -->
                        <button class="btn btn-primary btn-md" id="buttonSearch"><i class="fa fa-search">&nbsp;</i>Search
                        </button>
                    </form>
                </div>
            </div>
            <div class="horizontal-wrapper" style="display:none" id="formStorySearch">
                {{ Form::open(array('url'=>$searchUrl , 'method'=> 'POST')) }}
                <div class="btn-horizontal-group bg-default row">
                    <div class="col-sm-10">
                        <input type="text" name="search_keyword" class="form-control"/>
                    </div>
                    <div class="col-sm-2">
                        <button class="btn btn-sm btn-info">Search</button>
                    </div>
                </div>
                {{ Form::close() }}
            </div>

            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Owner Name</th>
                        <th>Ver. Phone Number</th>
                        <th class="text-center">Jml Kost/Apt</th>
                        <th class="text-center">Total Report</th>
                        <th class="text-center">Report Bulan Ini</th>
                        <th class="text-center">Total Review</th>
                        <th class="text-center">Avg. Rating</th>
                        <th class="text-center">Pemilik Top Bulan Ini?</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($rowsOwner as $index => $rowOwner)
                        <tr>
                            <td>{{ $index+1 }}</td>
                            <td>
                                <strong>{{ ($rowOwner->user->name == '') ? '-' : $rowOwner->user->name }}</strong><br/>
                                <small>{{ $rowOwner->owner_status }}</small>
                            </td>
                            <td>{{ isset($rowOwner->user) ? $rowOwner->user->phone_number : "--" }}</td>
                            <td class="text-center">{{ $rowOwner->room_count }} Unit</td>
                            <td class="text-center">
                            @if($rowOwner->room->report_summary_count > 1)
                                <span class="font-large text-danger"><strong>{{ $rowOwner->room->report_summary_count }}</strong></span>
                            @else
                                <span class="font-large"><strong>{{ $rowOwner->room->report_summary_count }}</strong></span></td>
                            @endif
                            <td class="text-center">
                            @if($rowOwner->room->report_summary_within_month_count <= 1)
                                <span class="font-large text-success"><strong>{{ $rowOwner->room->report_summary_within_month_count }}</strong></span>
                            @elseif($rowOwner->room->report_summary_within_month_count <= 5)
                                <span class="font-large text-warning"><strong>{{ $rowOwner->room->report_summary_within_month_count }}</strong></span>
                            @else
                                <span class="font-large text-danger"><strong>{{ $rowOwner->room->report_summary_within_month_count }}</strong></span>
                            @endif
                            </td>
                            <td class="text-center">
                            @if($rowOwner->room->review_count > 0)
                                <span class="font-large text-success"><strong>{{ $rowOwner->room->review_count }}</strong></span>
                            @else
                                <span class="font-large font-grey"><strong>{{ $rowOwner->room->review_count }}</strong></span>
                            @endif
                            </td>
                            <td class="text-center">
                            @if($rowOwner->average_rating >= 3)
                                <span class="font-large text-success">
                                    @for ($i = 1; $i <= $rowOwner->average_rating; $i++)
                                    <i class="fa fa-star"></i>
                                    @endfor
                                </span>
                            @elseif($rowOwner->average_rating < 1)
                                <span class="font-large font-grey">-</span>
                            @else
                                <span class="font-large text-danger">
                                    @for ($i = 1; $i <= $rowOwner->average_rating; $i++)
                                    <i class="fa fa-star"></i>
                                    @endfor
                                </span>
                            @endif
                            </td>
                            <td class="text-center">
                            @if($rowOwner->user->is_top_owner == "true")
                                <i class="fa fa-check-circle fa-2x text-success"></i>
                            @else
                                <i class="fa fa-times-circle fa-2x font-grey"></i>
                            @endif
                            </td>
                            <td class="table-action-column">
                            @if ($rowOwner->user->is_top_owner == "true")
                                <a href="{{ URL::to('admin/top-owner/change-status', $rowOwner->user->id) }}" class="btn btn-xs btn-danger" role="button">Non-Aktifkan Badge</a>
                            @else
                                <a href="{{ URL::to('admin/top-owner/change-status', $rowOwner->user->id) }}" class="btn btn-xs btn-success" role="button">Set Sebagai Pemilik Top</a>
                            @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

        </div><!-- /.box-body -->
        <div class="box-body no-padding">
            {{ $rowsOwner->appends(Request::except('page'))->links() }}
        </div>
    </div><!-- /.box -->
    <!-- table -->
@endsection
@section('script')
    <!-- Jquery Datepicker -->
    <!-- <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script> -->

    <script type="text/javascript">
        $(function(){
            var token = '{{ csrf_token() }}';

            $('#select-sort-option').change(function(event){
                var url  = '{{ URL::route("admin.room.index") }}' + '?o=' + event.target.value ;

                url += '&st=' + $('#start-date').val();
                url += '&en=' + $('#end-date').val();

                window.location = url;

            });

            $('.datepicker').datepicker({ dateFormat : 'dd-mm-yy'});

            $('.btn-activate').click(function() {
                var id = $(this).data('id');
                var name = $(this).data('name');

                var confirm = window.confirm("Are you sure want to delete " + name +" ?");

                if (confirm) {
                    $.post('/{{Request::segment(1)}}/{{Request::segment(2)}}/' + id, {
                        _method: "delete",
                        _token: token,
                    }).success(function(response) {
                        console.log(response);
                        window.location.reload();
                    });
                }
            });
        });
    </script>
@endsection
