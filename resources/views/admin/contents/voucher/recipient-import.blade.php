@extends('admin.layouts.main')

@section('content')
	<div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }} ({{ $voucherProgram->name }})</h3>
        </div>

		<form action="{{ URL::route('admin.voucher.recipient.importsubmit', $voucherProgram->id) }}" method="POST" class="form-horizontal form-bordered" enctype="multipart/form-data">
			<div class="box-body no-padding">
				<div class="form-group bg-default">
					<div class="col-sm-10 col-sm-push-2">
						<label for="use_file_email" class="checkbox">
							<input type="checkbox" name="use_file_email" id="use_file_email" class="form-control" value="1" {{ old('use_file_email') == 1 ? 'checked="checked"' : '' }}> Email sudah ada dalam file
						</label>
					</div>
				</div>

				<div id="criteria-container">
					<div class="form-group bg-default">
						<label for="user_type" class="control-label col-sm-2">Tipe User</label>
						<div class="col-sm-4">
							<select name="user_type" class="form-control">
								<option value="all" {{ old('user_type') == 'all' ? 'selected="selected"' : '' }}>Semua</option>
								<option value="user" {{ old('user_type') == 'user' ? 'selected="selected"' : '' }}>User Biasa</option>
								<option value="owner" {{ old('user_type') == 'owner' ? 'selected="selected"' : '' }}>Pemilik Kos</option>
							</select>
						</div>
					</div>

					<div class="form-group bg-default">
						<label for="join_start" class="control-label col-sm-2">Tanggal Daftar Awal</label>
						<div class="col-sm-4">
							<input type="text" name="join_start" id="join_start" class="form-control" value="{{ old('join_start') }}">
						</div>
					</div>

					<div class="form-group bg-default">
						<label for="join_end" class="control-label col-sm-2">Tanggal Daftar Akhir</label>
						<div class="col-sm-4">
							<input type="text" name="join_end" id="join_end" class="form-control" value="{{ old('join_end') }}">
						</div>
					</div>

					<div class="form-group bg-default">
						<label for="sort_type" class="control-label col-sm-2">Urutkan Berdasar</label>
						<div class="col-sm-4">
							<select name="sort_type" class="form-control">
								<option value="random" {{ old('sort_type') == 'random' ? 'selected="selected"' : '' }}>Random</option>
								<option value="join_date" {{ old('sort_type') == 'join_date' ? 'selected="selected"' : '' }}>Tanggal Daftar</option>
							</select>
						</div>
					</div>

					<div class="form-group bg-default">
						<label for="sort_direction" class="control-label col-sm-2">Arah Pengurutan</label>
						<div class="col-sm-4">
							<select name="sort_direction" class="form-control">
								<option value="asc" {{ old('sort_direction') == 'asc' ? 'selected="selected"' : '' }}>Ascending</option>
								<option value="desc" {{ old('sort_type') == 'desc' ? 'selected="selected"' : '' }}>Descending</option>
							</select>
						</div>
					</div>
				</div>
				
				<div class="form-group bg-default">
					<label for="file" class="control-label col-sm-2">File</label>
					<div class="col-sm-10">
						<input type="file" name="file" id="file" class="form-control" value="{{ old('file') }}">
						<span class="help-block">Harus berformat .csv</span>
					</div>
				</div>

				<div class="form-group bg-default">
					<div class="col-sm-10 col-sm-push-2">
						<button type="submit" class="btn btn-primary">Save</button>
					</div>
				</div>
			</div>
		</form>
    </div>
@endsection

@section('script')
<script>
	$(document).ready(function() {
		
	});
</script>
@endsection