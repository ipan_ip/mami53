@extends('admin.layouts.main')

@section('content')
	<div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

		<form action="{{ URL::route('admin.voucher.update', $voucherProgram->id) }}" method="POST" class="form-horizontal form-bordered">
			<input type="hidden" name="_method" value="PUT">

			<div class="box-body no-padding">
				<div class="form-group bg-default">
					<label for="name" class="control-label col-sm-2">Nama</label>
					<div class="col-sm-10">
						<input type="text" name="name" id="name" class="form-control" value="{{ old('name', $voucherProgram->name) }}">
					</div>
				</div>

				<div class="form-group bg-default">
					<label for="email_subject" class="control-label col-sm-2">Subject Email</label>
					<div class="col-sm-10">
						<input type="text" name="email_subject" id="email_subject" class="form-control" value="{{ old('email_subject', $voucherProgram->email_subject) }}">
					</div>
				</div>

				<div class="form-group bg-default">
					<label for="email_template" class="control-label col-sm-2">Email Template</label>
					<div class="col-sm-10">
						<textarea name="email_template" id="email_template" class="form-control" rows="10">{{ old('email_template', $voucherProgram->email_template) }}</textarea>
						<p class="helper-block">Gunakan <strong>[VOUCHERMAMIKOS]</strong> sebagai placeholder untuk kode voucher</p>
					</div>
				</div>

				<div class="form-group bg-default">
					<label for="sms_template" class="control-label col-sm-2">SMS Template</label>
					<div class="col-sm-10">
						<input type="text" name="sms_template" id="sms_template" class="form-control" value="{{ old('sms_template', $voucherProgram->sms_template) }}">
						<p class="helper-block">Gunakan <strong>[VOUCHERMAMIKOS]</strong> sebagai placeholder untuk kode voucher</p>
					</div>
				</div>

				<div class="form-group bg-default">
					<div class="col-sm-10 col-sm-push-2">
						<button type="submit" class="btn btn-primary">Save</button>
					</div>
				</div>
			</div>
		</form>
    </div>
@endsection

@section('script')
<script>
	$(document).ready(function() {
		
	});
</script>
@endsection