@extends('admin.layouts.main')

@section('content')
	<!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">

            <table  class="table table-striped">
                <thead>
                    <tr>
                        <th>Nama File</th>
                        <th>Waktu Import</th>
                        <th>Status</th>
                        <th>Jumlah Baris Diproses</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($recipientFiles as $key => $file)
                    <tr>
                        <td>{{ $file->file_name }}</td>
                        <td>{{ date('Y-m-d H:i:s', strtotime($file->created_at)) }}</td>
                        <td>{{ $file->status }}</td>
                        <td>{{ $file->rows_read }}</td>
                    </tr>
                @endforeach
                </tbody>
                
            </table>
        </div>
    </div><!-- /.box -->
    <!-- table -->
@endsection