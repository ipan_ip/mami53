@extends('admin.layouts.main')

@section('content')
	<div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }} ({{ $voucherProgram->name }})</h3>
        </div>

		<form action="{{ URL::route('admin.voucher.recipient.update', [$voucherProgram->id, $recipient->id]) }}" method="POST" class="form-horizontal form-bordered">
			<div class="box-body no-padding">
				<div class="form-group bg-default">
					<label for="voucher_code" class="control-label col-sm-2">Kode Voucher</label>
					<div class="col-sm-10">
						<input type="text" name="voucher_code" id="voucher_code" class="form-control" value="{{ old('voucher_code', $recipient->voucher_code) }}">
					</div>
				</div>

				<div class="form-group bg-default">
					<label for="email" class="control-label col-sm-2">Email</label>
					<div class="col-sm-10">
						<input type="text" name="email" id="email" class="form-control" value="{{ old('email', $recipient->email) }}">
					</div>
				</div>

				<div class="form-group bg-default">
					<label for="phone_number" class="control-label col-sm-2">Nomor Telepon</label>
					<div class="col-sm-10">
						<input type="text" name="phone_number" id="phone_number" class="form-control" value="{{ old('phone_number', $recipient->phone_number) }}">
					</div>
				</div>

				<div class="form-group bg-default">
					<div class="col-sm-10 col-sm-push-2">
						<button type="submit" class="btn btn-primary">Save</button>
					</div>
				</div>
			</div>
		</form>
    </div>
@endsection

@section('script')
<script>
	$(document).ready(function() {
		
	});
</script>
@endsection