@extends('admin.layouts.main')

@section('content')
	<!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <form action="" class="form-inline" method="get" style="text-align:right;padding:10px;">
                        <a href="{{ URL::route('admin.voucher.create') }}" class="btn btn-primary btn-sm pull-left">
                            <i class="fa fa-plus">&nbsp;</i> Tambah Voucher Program
                        </a>
                        
                        <input type="text" name="q" class="form-control input-sm"  placeholder="Nama"  autocomplete="off" value="{{ request()->input('q') }}">
                        <button class="btn btn-primary btn-md" id="buttonSearch"><i class="fa fa-search">&nbsp;</i>Search</button>
                    </form>
                </div>
            </div>

            <table  class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nama Program</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($voucherPrograms as $key => $voucherProgram)
                    <tr>
                        <td>{{ $voucherProgram->id }}</td>
                        <td>{{ $voucherProgram->name }}</td>
                        <td>{{ $voucherProgram->status }}</td>
                        <td>
                            <a href="{{ URL::route('admin.voucher.edit', $voucherProgram->id) }}" title="Edit">
                                <i class="fa fa-pencil"></i>
                            </a>

                            <a href="{{ URL::route('admin.voucher.testnotif', $voucherProgram->id) }}" title="Kirim Test Notifikasi">
                                <i class="fa fa-location-arrow"></i>
                            </a>

                            <a href="{{ URL::route('admin.voucher.recipient.index', $voucherProgram->id) }}" title="Penerima Voucher">
                                <i class="fa fa-users"></i>
                            </a>

                            @if(in_array($voucherProgram->status, [$voucherProgram::STATUS_CREATED, $voucherProgram::STATUS_PAUSED, $voucherProgram::STATUS_STOPPED, $voucherProgram::STATUS_FINISHED]))
                                <a href="{{ URL::route('admin.voucher.run', $voucherProgram->id) }}" title="Mulai Kirim Notifikasi">
                                    <i class="fa fa-play"></i>
                                </a>
                            @elseif($voucherProgram->status == $voucherProgram::STATUS_RUNNING)
                                <a href="{{ URL::route('admin.voucher.pause', $voucherProgram->id) }}" title="Tunda Kirim Notifikasi">
                                    <i class="fa fa-pause"></i>
                                </a>
                                <a href="{{ URL::route('admin.voucher.stop', $voucherProgram->id) }}" title="Stop Kirim Notifikasi">
                                    <i class="fa fa-stop"></i>
                                </a>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
                
            </table>
        </div><!-- /.box-body -->
        <div class="box-body no-padding">
            {{ $voucherPrograms->appends(Request::except('page'))->links() }}
        </div>
    </div><!-- /.box -->
    <!-- table -->
@endsection