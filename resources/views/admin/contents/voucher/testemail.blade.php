@extends('admin.layouts.main')

@section('content')
	<div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

		<form action="{{ URL::route('admin.voucher.testemailsubmit', $voucherProgram->id) }}" method="POST" class="form-horizontal form-bordered">
			<div class="box-body no-padding">
				<div class="form-group bg-default">
					<label for="destination" class="control-label col-sm-2">Email Tujuan</label>
					<div class="col-sm-10">
						<input type="text" name="destination" id="destination" class="form-control" value="{{ old('destination') }}">
					</div>
				</div>

				<div class="form-group bg-default">
					<div class="col-sm-10 col-sm-push-2">
						<button type="submit" class="btn btn-primary">Kirim</button>
					</div>
				</div>
			</div>
		</form>

		<form action="{{ URL::route('admin.voucher.testsmssubmit', $voucherProgram->id) }}" method="POST" class="form-horizontal form-bordered">
			<div class="box-body no-padding">
				<div class="form-group bg-default">
					<label for="destination_phone" class="control-label col-sm-2">Nomor Telepon Tujuan</label>
					<div class="col-sm-10">
						<input type="text" name="destination_phone" id="destination_phone" class="form-control" value="{{ old('destination_phone') }}">
					</div>
				</div>

				<div class="form-group bg-default">
					<div class="col-sm-10 col-sm-push-2">
						<button type="submit" class="btn btn-primary">Kirim</button>
					</div>
				</div>
			</div>
		</form>
    </div>
@endsection

@section('script')
<script>
	$(document).ready(function() {
		
	});
</script>
@endsection