@extends('admin.layouts.main')

@section('content')
	<!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }} ({{$voucherProgram->name}})</h3>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <form action="" class="form-inline" method="get" style="text-align:right;padding:10px;">
                        <a href="{{ URL::route('admin.voucher.recipient.create', $voucherProgram->id) }}" class="btn btn-primary btn-sm pull-left">
                            <i class="fa fa-plus">&nbsp;</i> Tambah Penerima Voucher
                        </a>
                        <a href="{{ URL::route('admin.voucher.recipient.import', $voucherProgram->id) }}" class="btn btn-primary btn-sm pull-left">
                            <i class="fa fa-download">&nbsp;</i> Import Penerima Voucher
                        </a>
                        <a href="{{ URL::route('admin.voucher.recipient.statusimport', $voucherProgram->id) }}" class="btn btn-primary btn-sm pull-left">
                            <i class="fa fa-info-circle">&nbsp;</i> Status Import
                        </a>
                        
                        <input type="text" name="q" class="form-control input-sm"  placeholder="Email / Kode Voucher / No. HP"  autocomplete="off" value="{{ request()->input('q') }}">
                        <button class="btn btn-primary btn-md" id="buttonSearch"><i class="fa fa-search">&nbsp;</i>Search</button>
                    </form>
                </div>
            </div>

            <table  class="table table-striped">
                <thead>
                    <tr>
                        <th>Kode Voucher</th>
                        <th>Email Penerima</th>
                        <th>Nomor Telepon Penerima</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($recipients as $key => $recipient)
                    <tr>
                        <td>{{ $recipient->voucher_code }}</td>
                        <td>{{ $recipient->email }}</td>
                        <td>{{ $recipient->phone_number }}</td>
                        <td>{{ $recipient->status }}</td>
                        <td>
                           <a href="{{ URL::route('admin.voucher.recipient.edit', [$voucherProgram->id, $recipient->id]) }}" title="Edit">
                                <i class="fa fa-pencil"></i>
                            </a>

                            @if(!(is_null($recipient->email) && is_null($recipient->phone_number)))
                                <a href="{{ URL::route('admin.voucher.recipient.sendnotif', [$voucherProgram->id, $recipient->id]) }}" title="Kirim Notifikasi">
                                    <i class="fa fa-mail-reply"></i>
                                </a>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
                
            </table>
        </div><!-- /.box-body -->
        <div class="box-body no-padding">
            {{ $recipients->appends(Request::except('page'))->links() }}
        </div>
    </div><!-- /.box -->
    <!-- table -->
@endsection