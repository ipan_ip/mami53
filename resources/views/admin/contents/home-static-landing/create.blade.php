@extends('admin.layouts.main')

@section('style')
    {{ HTML::style('assets/vendor/jquery-chosen/chosen.css') }}

    <style>
        .chosen-container .chosen-single {
            height: 33px;
            padding-top: 4px;
        }

        #chosenForm .chosen-choices {
            border: 1px solid #ccc;
            border-radius: 4px;
            min-height: 34px;
            padding: 6px 12px;
        }
        #chosenForm .form-control-feedback {
            /* To make the feedback icon visible */
            z-index: 100;
        }
    </style>
@endsection

@section('content')
<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">{{ $boxTitle }}</h3>
    </div><!-- /.box-header -->
    <form action="{{ route('admin.home-static-landing.store') }}" method="post" class="form-horizontal form-bordered">
        <div class="box-body no-padding">
            <div class="form-group bg-default">
                <label for="inputLandingId" class="col-sm-2 control-label">Landing</label>
                <div class="col-sm-10">
                    <select class="form-control chosen-select" id="inputLandingId" name="landing_id" tabindex="2">
                        @foreach ($kostLandings as $kostLanding)
                            <option value="{{ $kostLanding->id }}"
                                {{ old('landing_id') == $kostLanding->id ? 'selected="selected"' : '' }}>
                                {{ $kostLanding->name }}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group bg-default">
                <label for="inputName" class="col-sm-2 control-label">Name</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Name" id="inputName" 
                        name="name"  value="{{ old('name') }}">
                </div>
            </div>
            <div class="form-group bg-default">
                <label for="inputParentId" class="col-sm-2 control-label">Parent</label>
                <div class="col-sm-10">
                    <select class="form-control chosen-select" id="inputParentId" name="parent_id" tabindex="2">
                        <option value="" disabled selected>Select Parent Landing</option>
                        @foreach ($parentStaticLanding as $parent)
                            <option value="{{ $parent->id }}"
                                    {{ old('parent_id') == $parent->id ? 'selected="selected"' : '' }}>
                                {{ $parent->name }}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </form>
</div><!-- /.box -->
@endsection

@section('script')
    {{ HTML::script('assets/vendor/jquery-chosen/chosen.jquery.js') }}

    <script>
        $(function() {
            $('.chosen-select').chosen({
                width: '100%'
            });
        });
    </script>
@endsection