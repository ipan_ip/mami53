@extends('admin.layouts.main')

@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <form action="" class="form-inline" method="get" style="text-align:right;padding:10px;">
                        <a href="{{ URL::route('admin.home-static-landing.create') }}">
                            <button type="button" name="button" class="btn-add btn btn-primary btn-sm pull-left">
                                <i class="fa fa-plus">&nbsp;</i> Add Landing </button>
                        </a>


                        <input type="text" name="q" class="form-control input-sm"  placeholder="Criteria"  autocomplete="off" value="{{ request()->input('q') }}">
                        <button class="btn btn-primary btn-md" id="buttonSearch"><i class="fa fa-search">&nbsp;</i>Search</button>
                    </form>
                </div>
            </div>

            <table  class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Landing</th>
                        <th>Name</th>
                        <th>Parent</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                @foreach($landings as $landing)
                    <tr>
                        <td>{{ $landing->id ?? "-"}}</td>
                        <td>{{ $landing->landing_kost->keyword ?? "-"}}</td>
                        <td>{{ $landing->name ?? "-"}}</td>
                        <td>{{ $landing->parent->name ?? "-"}}</td>
                        <td>
                            <a href="{{ route('admin.home-static-landing.edit', $landing->id) }}" title="Edit">
                                <i class="fa fa-pencil"></i>
                            </a>

                            <a href="{{ route('admin.home-static-landing.delete', $landing->id) }}" title="Edit">
                                <i class="fa fa-trash-o"></i>
                            </a>
                        </td>
                    </tr> 
                @endforeach
                </tbody>
                
            </table>
        </div><!-- /.box-body -->
        <div class="box-body no-padding">
            {{ $landings->appends(Request::except('page'))->links() }}
        </div>
    </div><!-- /.box -->
    <!-- table -->
@endsection