@extends('admin.layouts.main')

@section('content')
<div class="box box-primary">
    <!-- box-header -->
    <div class="box-header">
        <h3 class="box-title">{{ $boxTitle }}</h3>
    </div>
    <!-- box-header -->

    {{ 
        Form::open([
            'url' => $formUrl,
            'method' => $formMethod,
            'class' => 'form-horizontal form-bordered',
            'role' => 'form',
            'id' => 'formInsertDesigner',
            'enctype' => 'multipart/form-data'
        ])
    }}

    <div class="box-body no-padding">

        <div class="form-group bg-default">
            <label
                for="pageType"
                class="col-sm-2 control-label"
            >
                Page Type
            </label>
            <div class="col-sm-10">
                <select id="page_type" name="page_type" class="form-control">
                    @if (! empty($pageTypes))
                        @foreach ($pageTypes as $pageType)
                            <option
                                value={{$pageType}}
                                @if($row->page_type == $pageType) selected="true" @endif
                            >
                                {{str_replace('_', ' ', ucfirst($pageType))}}
                            </option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>

        <div class="form-group bg-default">
            <label
                for="title"
                class="col-sm-2 control-label"
            >
                Title (For Title Tag & Meta Title)
            </label>
            <div class="col-sm-10">
                <textarea
                    rows="1"
                    maxlength="50"
                    type="text"
                    class="form-control"
                    id="title"
                    name="title"
                    placeholder="For title tag & meta title"
                >
                    {{$row->title}}
                </textarea>
            </div>
        </div>

        <div class="form-group bg-default">
            <label
                for="description"
                class="col-sm-2 control-label"
            >
                Description
            </label>
            <div class="col-sm-10">
                <textarea
                    rows="8"
                    type="text"
                    class="form-control"
                    id="description"
                    name="description"
                    placeholder="For meta-description and og:desc"
                >
                    {{$row->description}}
                </textarea>
            </div>
            <div id="charCount"></div>
        </div>

        <div class="form-group bg-default">
            <label
                for="keywords"
                class="col-sm-2 control-label"
            >
                Keywords
            </label>
            <div class="col-sm-10">
                <textarea
                    rows="8"
                    type="text"
                    class="form-control"
                    id="keywords"
                    name="keywords"
                    placeholder="For meta-keywords"
                > 
                    {{$row->keywords}}
                </textarea>
            </div>
        </div>

        <div class="form-group bg-default">
            <label
                for="image"
                class="col-sm-2 control-label"
            >
                Image Url
            </label>
            <div class="col-sm-10">
                <input 
                    type="text"
                    class="form-control"
                    placeholder="For og:image"
                    id="image"
                    name="image"
                    value="{{$row->image}}"
                >
            </div>
        </div>

        <div class="form-group bg-default">
            <label
                for="active-record"
                class="col-sm-2 control-label"
            >
                Active
            </label>
            <div class="col-sm-10">
                <input
                    type="checkbox"
                    name="is_active"
                    id="is_active"
                    value=1
                    {{ $row->is_active === 1 ? 'checked="checked"' : '' }}
                >
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button
                    type="submit"
                    class="btn btn-primary"
                >
                    Save
                </button>
                @if (Request::is('admin/*'))
                    <a href="{{ $formCancel }}">
                        <button type="button" class="btn btn-default">Cancel</button>
                    </a>
                @endif
            </div>
        </div>
    </div>
</div>
@stop

@section('style')
    {{ HTML::style('assets/vendor/jquery-chosen/chosen.css') }}
    {{ HTML::style('assets/vendor/jquery-chosen-custom/chosen.css') }}
    <link rel="stylesheet" href="/assets/vendor/summernote/summernote.css?v=05042018">
@stop

@section('script')
<script src="/assets/vendor/summernote/summernote.min.js"></script>
<script type="text/javascript">
    $(function () {
        var AreaButton = function (context) {
            var ui = $.summernote.ui
            var button = ui.button({
                contents: '<i class="fa fa-building"/> Area',
                tooltip: 'area',
                click: function () {
                    context.invoke('editor.insertText', '{{$keywords["area"]}}')
                }
            });

            return button.render();
        }

        var TotalPropertyButton = function (context) {
            var ui = $.summernote.ui
            var button = ui.button({
                contents: '<i class="fa fa-sort-numeric-asc"/> Total Property',
                tooltip: 'total property',
                click: function () {
                    context.invoke('editor.insertText', '{{$keywords["counter"]}}')
                }
            });

            return button.render();
        }

        var PropertyNameButton = function (context) {
            var ui = $.summernote.ui
            var button = ui.button({
                contents: '<i class="fa fa-sort-numeric-asc"/> Property Name',
                tooltip: 'property name',
                click: function () {
                    context.invoke('editor.insertText', '{{$keywords["property_name"]}}')
                }
            });

            return button.render();
        }

        var TopRoomFacilitiesButton = function (context) {
            var ui = $.summernote.ui
            var button = ui.button({
                contents: '<i class="fa fa-bath"/> Top Room Facilities',
                tooltip: 'top room facilities',
                click: function () {
                    context.invoke('editor.insertText', '{{$keywords["top_room_facilities"]}}')
                }
            });

            return button.render();
        }

        var RentTypeButton = function (context) {
            var ui = $.summernote.ui
            var button = ui.button({
                contents: '<i class="fa fa-calendar"/> Rent Type',
                tooltip: 'rent type',
                click: function () {
                    context.invoke('editor.insertText', '{{$keywords["rent_type"]}}')
                }
            });

            return button.render();
        }

        var PropertyTypeButton = function (context) {
            var ui = $.summernote.ui
            var button = ui.button({
                contents: '<i class="fa fa-home"/> Property Type',
                tooltip: 'property type',
                click: function () {
                    context.invoke('editor.insertText', '{{$keywords["property_type"]}}')
                }
            });

            return button.render();
        }

        var GenderButton = function (context) {
            var ui = $.summernote.ui
            var button = ui.button({
                contents: '<i class="fa fa-venus-mars"/> Gender',
                tooltip: 'gender',
                click: function () {
                    context.invoke('editor.insertText', '{{$keywords["gender"]}}')
                }
            });

            return button.render();
        }

        var PromoDiscountButton = function (context) {
            var ui = $.summernote.ui
            var button = ui.button({
                contents: '<i class="fa fa-usd"/> Promo/Discount',
                tooltip: 'promo/discount',
                click: function () {
                    context.invoke('editor.insertText', '{{$keywords["promo_discount"]}}')
                }
            });

            return button.render();
        }

        function homepage() {
            $('#title, #keywords, #description').summernote({
                height: 300,
                toolbar: [
                    ['style', ['style', 'bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['insert', ['link', 'table', 'hr']],
                    ['misc', ['codeview']]
                ]
            })
        }
        
        function landingArea() {
            $('#title, #keywords, #description').summernote({
                height: 300,
                toolbar: [
                    ['style', ['style', 'bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['insert', ['link', 'table', 'hr']],
                    ['misc', ['codeview']],
                    ['mybutton', ['area', 'total_property']]
                ],
                buttons: {
                    area: AreaButton,
                    total_property: TotalPropertyButton
                }
            })
        }
        
        function detailKost() {
            $('#title, #keywords, #description').summernote({
                height: 300,
                toolbar: [
                    ['style', ['style', 'bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['insert', ['link', 'table', 'hr']],
                    ['misc', ['codeview']],
                    ['mybutton', [
                        'area',
                        'property',
                        'top_room_facilities',
                        'rent_type',
                        'property_type',
                        'gender',
                        'promo_discount'
                    ]]
                ],
                buttons: {
                    area: AreaButton,
                    property: PropertyNameButton,
                    top_room_facilities: TopRoomFacilitiesButton,
                    rent_type: RentTypeButton,
                    property_type: PropertyTypeButton,
                    gender: GenderButton,
                    promo_discount: PromoDiscountButton
                }
            })
        }

        function changeValue(selectedPageType)
        {
            if (selectedPageType == 'landing_area' || selectedPageType == 'landing_campus') {
                $('#title').summernote('destroy')
                $('#description').summernote('destroy')
                $('#keywords').summernote('destroy')
                landingArea()
            } else if (selectedPageType == 'detail_kost') {
                $('#title').summernote('destroy')
                $('#description').summernote('destroy')
                $('#keywords').summernote('destroy')
                detailKost()
            } else {
                $('#title').summernote('destroy')
                $('#description').summernote('destroy')
                $('#keywords').summernote('destroy')
                homepage()
            }
        }

        $(document).ready(function(){
            homepage()
            var selectedPageType = $('select#page_type').children("option:selected").val()
            changeValue(selectedPageType)
            $("select#page_type").change(function(){
                var selectedPageType = $(this).children("option:selected").val()
                changeValue(selectedPageType)
            });
        })
    });
</script>
@stop