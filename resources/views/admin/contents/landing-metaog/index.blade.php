@extends('admin.layouts.main')

@section('style')
    <style>
        tr.active-metaog td {
            background-color: #C8E6C9 !important;
        }
    </style>
@endsection

@section('content')
    <?php App::setLocale('id'); ?>
    <!-- table -->
    <div class="box box-primary">
        <!-- box-header -->
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
            <div class="box-tools pull-right">
                <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
            </div>
        </div>
        <!-- box-header -->

        <!-- box-body -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    @if(Auth::user()->role == 'administrator')
                        <a href="{{ URL::route('admin.landing.meta.create') }}">
                            <button type="button" name="button" class="btn-add btn btn-primary btn-sm">
                                <i class="fa fa-plus">&nbsp;</i> Add New Meta
                            </button>
                        </a>
                    @endif
                </div>
            </div>

            <table id="tableListLanding" class="table table-striped">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Title</th>
                        <th>Page Type</th>
                        <th>Image</th>
                        <th>Last Update</th>
                        <th class="table-action-column" width="120px">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($rows as $key => $row)
                    <tr class="{{ $row->is_active === 1 ? 'active-metaog' : '' }}">
                        <td>{{ ($key+1) }}</td>
                        <td>{{ $row->title }}</td>
                        <td>{{ str_replace('_', ' ', ucfirst($row->page_type)) }}</td>
                        <td>{{ $row->image }}</td>
                        <td>{{ $row->updated_at }}</td>
                        <td class="table-action-column">
                            <div class="btn-action-group">

                                @permission('access-landing-page-edit')
                                <a href="{{ route("admin.landing.meta.edit", $row->id) }}" title="Edit Landing">
                                    <i class="fa fa-pencil"></i></a>
                                @endpermission

                                {{-- @permission('access-landing-page-delete')
                                @if(Auth::user()->role == 'administrator')
                                    <a href="{{ route("admin.landing.destroy", $rowLanding->id) }}" class="btn-delete" title="Delete Landing">
                                        <i class="fa fa-trash-o"></i></a>
                                @endif
                                @endpermission --}}

                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>No</th>
                        <th>Description</th>
                        <th>Keywords</th>
                        <th>Image</th>
                        <th>Last Update</th>
                        <th class="table-action-column" width="120px">Action</th>
                    </tr>
                </tfoot>
            </table>
            <div class="box-body no-padding">
                {{ $rows->links() }}
            </div>
        </div>
        <!-- box-body -->

    </div>
@stop