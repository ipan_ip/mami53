@extends('admin.layouts.main')

@section('style')
	<!-- custom css -->
	<style>
        .add-margin {
            margin-left: 3px;
        }

		.table>tbody>tr>td {
			vertical-align: middle;
		}

		.font-large {
			font-size: 1.5em;
		}

		.font-semi-large {
			font-size: 1.2em;
		}

		.font-grey {
			color: #9E9E9E;
		}

		.dropdown-menu>li>a:hover {
			color: #333
		}

		.label-grey {
			background-color: #CFD8DC;
		}

		[hidden] {
			display: none !important;
		}

		/* Select2 tweak */

		.input-group .select2-container,
		.form-group .select2-container {
			position: relative !important;
			z-index: 2;
			float: left !important;
			width: 100% !important;
			margin-bottom: 0 !important;
			display: table !important;
			table-layout: fixed !important;
		}

		/* Sweetalert */

		.swal2-container {
			z-index: 99999 !important;
		}

		.swal-shown {
			height: auto !important;
		}

		.custom-swal {
			z-index: 10000 !important;
		}

		.custom-wide-swal {
			width: 480px !important;
			font-size: 12px !important;
		}

		/* Custom table row */
		tr.inactive td {
			background-color: #E6C8C8;
		}

		/* Modal tweak */

		.modal-open {
			overflow: hidden;
			position: fixed;
			width: 100%;
		}


		/* Centering the modal */

		.modal {
			text-align: center;
			padding: 0 !important;
		}

		.modal:before {
			content: '';
			display: inline-block;
			height: 100%;
			vertical-align: middle;
			margin-right: -4px;
		}

		.modal-dialog {
			display: inline-block;
			text-align: left;
			vertical-align: middle;
		}

		/* Grid */
		.grid {
			position: relative;
		}

		.item {
			display: block;
			position: absolute;
			width: 100px;
			height: 100px;
			margin: 5px;
			z-index: 1;
			background: #000;
			color: #fff;
		}

		.item.muuri-item-dragging {
			z-index: 3;
		}

		.item.muuri-item-releasing {
			z-index: 2;
		}

		.item.muuri-item-hidden {
			z-index: 0;
		}

		.item-content {
			position: relative;
			width: 100%;
			height: 100%;
		}

		/* Blink effect */
		.blink {
			animation: blink 1s infinite;
		}

		@keyframes blink {
			0% {
				opacity: 1;
			}

			75% {
				opacity: 1;
			}

			76% {
				opacity: 0;
			}

			100% {
				opacity: 0;
			}
		}

		.bootstrap-datetimepicker-widget {
			z-index: 9999 !important;
		}
	</style>
	<!-- end custom css -->

	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.css">
	<link rel="stylesheet" href="//gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css">
	<link rel="stylesheet" href="//unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.css">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" />
	<link rel="stylesheet" href="//cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
@endsection

@section('content')
	<!-- box -->
    <div class="box box-primary">
		<!-- box-header -->
		<div class="box-header">
			@if(!empty($error))
			<div class="alert alert-danger"> {{ $error }}</div>
			@endif
			<div class="col-md-6 text-center">
				<h3 class="box-title"><i class="fa fa-percent"></i> {{ $boxTitle }}</h3>
			</div>
			<!-- add button -->
			<div class="col-md-6">
				<div class="form-horizontal pull-right" style="padding-top: 10px;">
					<div class="btn-group">
						<button type="button" class="btn btn-primary" onclick="return assignBenefit();">
							Assign Benefit
						</button>
					</div>
				</div>
			</div>
		</div>
		<!-- /.box-header -->

		<!-- box-body -->
		<div class="box-body">
			<!-- Table -->
			@include('admin.contents.assign-kost-value.partials.table')
			<!-- table -->
		</div>
		<!-- /.box-body -->
	</div>
	<!-- /.box -->
@stop

@section('script')
	<script src="//cdn.jsdelivr.net/npm/holderjs@2.9.6/holder.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>
	<script src="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.js"></script>
	<script src="//gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.js"></script>
	<script src="//unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment-with-locales.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
	<script src="//cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
    <script>
		function triggerLoading(message) {
			Swal.fire({
				title: message,
				customClass: {
					container: 'custom-swal'
				},
				allowEscapeKey: false,
				allowOutsideClick: false,
				onOpen: () => {
					swal.showLoading();
				}
			});
		}

		function ajaxLoadData(params) {
			let url = '/admin/assign-kost-value/data';

			$.get(url + '?' + $.param(params.data)).then(function (res) {
				params.success(res)
			})
		}

		function queryParams(params) {
			let newParams = {};

			newParams.search = params.searchText;
			newParams.limit = params.pageSize;
            newParams.page = params.pageNumber;
            
            return newParams;
		}

		function kosNameFormatter(value) {
			return value;
        }

		function benefitFormatter(value) {
            if (!value) return '-';

            var result = '';
            value.forEach(el => {
				result += "<span class='label label-success add-margin'>" + el.title + "</span>";
            });

            return result;
		}

		function actionFormatter(songId, data) {
			var url = "{{ URL::to('admin/assign-kost-value/') }}" + '/' + songId + '/edit';
			var urlPreviewKost = "{{ URL::to('room') }}" + '/' + data.slug;

			var actionHtml = `
				<a href="${url}" title="Edit"><i class="fa fa-pencil"></i> Edit</a> 
				<a href="${urlPreviewKost}" title="Preview"><i class="fa fa-eye"></i> Preview</a>`;
			return actionHtml;
		}

		$(function() {
			// onready
			$('#table').on('load-success.bs.table', (data) => {
				Holder.addTheme("img-thumbnail", {
					background: '#EEE',
					foreground: '#AAA',
					size: 10,
					fontweight: 'normal'
				});
			})
		});

		function assignBenefit() {
			var popUpMsg = `
				<div class="callout callout-info">
					<p>
						Contoh URL yang valid:<br/>
						<strong class="font-grey">
							{{ env('APP_URL') }}/room/kost-sleman-kost-putri-eksklusif-kost-mamirooms-mackahaus-sinduadi-sleman
						</strong>
					</p>
				</div>`;

            Swal.fire({
                title: 'Masukkan URL Kost',
				html: popUpMsg,
				input: 'text',
                confirmButtonText: 'Cek URL',
                showCancelButton: true,
                customClass: 'custom-wide-swal',
                showLoaderOnConfirm: true,
                preConfirm: (val) => {

					var isValidUrl = false;

                    if (~val.indexOf("mamikos.com/room/kost-") || ~val.indexOf("/room/kost-")) {
                        isValidUrl = true;
                    } else {
                        Swal.showValidationMessage('URL tidak valid!');
                        return false;
                    }

                    if (isValidUrl) {
						var slug = /[^/]*$/.exec(val)[0];
						var token = '{{ csrf_token() }}';

                        return $.ajax({
                            type: 'POST',
                            url: "/admin/assign-kost-value/verify",
                            dataType: 'json',
                            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                            data: { 'slug': slug, '_token': token },
                            success: (response) => {
                                if (!response.success) Swal.showValidationMessage(response.message);
                                return;
                            },
                            error: (error) => {
                                Swal.showValidationMessage('Request Failed: ' + error.message);
                            }
                        });
                    }
                    
                },
                allowOutsideClick: () => !Swal.isLoading()

            }).then((result) => {
                if (!result.dismiss) {
					var room = result.value.room;
					var popUpMsg = `
						Pastikan bahwa ini adalah kost yang benar<br>Lalu klik 
							<strong>
								[Confirm]
							</strong>
						untuk memulai setup benefit.`;

                    if (room) {
                        Swal.fire({
                            title: room.name,
                            html: popUpMsg,
                            customClass: 'custom-wide-swal',
                            confirmButtonText: 'Confirm',
                            showCancelButton: true,
                            imageUrl: room.photo_url.large
                        }).then((result) => {
                            if (result.value) {
								var url = "{{ URL::to('admin/assign-kost-value/') }}" + '/' + room.song_id + '/edit';
								window.location = url;
                            }
                        })
                    }
                }
            });
		}
	</script>
@stop
