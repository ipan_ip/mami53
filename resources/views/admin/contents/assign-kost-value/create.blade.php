@extends('admin.layouts.main')


@section('style')
<style>

    .form-bordered .form-group{
        border-bottom: 0px solid #EEE !important;
    }

    /* Modal tweak */
    .modal-open {
        overflow: hidden;
        position: fixed;
        width: 100%;
    }

    /* Centering the modal */
    .modal {
        text-align: center;
        padding: 0 !important;
    }

    .modal:before {
        content: '';
        display: inline-block;
        height: 100%;
        vertical-align: middle;
        margin-right: -4px;
    }

    .modal-dialog {
        display: inline-block;
        text-align: left;
        vertical-align: middle;
    }

    /* Sweetalert */

    .swal-shown {
        height: auto !important;
    }

    .custom-swal {
        z-index: 10000 !important;
    }

    .custom-wide-swal {
        width: 640px !important;
        font-size: 13px !important;
    }

</style>
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
@endsection

@section('content')
    <div style="margin-bottom:15px;">
        <a href="{{ route('admin.assign-kost-value.index') }}">
            <i class="fa fa-backward"></i> kembali <br>
        </a>
    </div>

    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title"><i class="fa fa-money"></i> {{ $boxTitle }}</h3>
        </div>

        <div class="form-horizontal form-bordered">
            <div class="box-body no-padding">
                <div class="form-group bg-default">
                    <label for="slug" class="control-label col-sm-2">Room Slug</label>
                    <div class="col-sm-10">
                        <input type="text" name="slug" id="slug" class="form-control" value="{{ $room->slug }}" disabled>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="name" class="control-label col-sm-2">Room Name</label>
                    <div class="col-sm-10">
                        <input type="text" name="name" id="name" class="form-control" value="{{ $room->name }}" disabled>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="level-name" class="control-label col-sm-2">Room Level</label>
                    <div class="col-sm-10">
                        <input type="text" name="level" id="level" class="form-control" value="{{ $room->level_info['name'] }}" disabled>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="level-name" class="control-label col-sm-2">Kost Level Benefit</label>
                    <div class="col-sm-10">
                        @include('admin.contents.assign-kost-value.partials.table-benefit', ['listBenefit' => $benefitsFromLevel])
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="level-name" class="control-label col-sm-2">Custom Benefit</label>
                    <div class="col-sm-10">
                        @include('admin.contents.assign-kost-value.partials.table-benefit', ['listBenefit' => $benefitsFromKost, 'type' => 'custom-benefit'])
                    </div>
                </div>

                <div class="form-group bg-default">
                    <div class="col-sm-10 col-sm-push-2">
                        <button onclick="return assignBenefitModal()" class="btn btn-primary">Tambah Custom Benefit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal" id="modal" role="dialog" aria-labelledby="modalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top:2px;"><i
                            class="fa fa-lg fa-times-circle"></i></button>
                    <h4 class="modal-title" id="modalTitle">Assign Benefit</h4>
                </div>
                <!-- <div class="modal-body"> -->
                <div class="modal-body">
                    <div class="form-horizontal form-bordered">
                        <form action="{{ URL::route('admin.assign-kost-value.postAddBenefit', $room->song_id) }}" method="POST" id="form">
                            <div class="box-body no-padding">
                                <div class="form-group bg-default">
                                    <label for="kost-name-popup" class="control-label col-sm-2">Room Name</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="kost-name-popup" class="form-control" value="{{ $room->name }}" disabled>
                                    </div>
                                </div>

                                <div class="form-group bg-default">
                                    <label for="add-benefit" class="control-label col-sm-2">Benefit</label>
                                    <div class="col-sm-10">
                                        <select class="form-control" id="add-benefit" name="benefit_id">
                                            @if($availbleBenefitOption->count() > 0)
                                                @foreach ($availbleBenefitOption as $benefit)
                                                <option value="{{ $benefit->id }}">{{ $benefit->name }}</option>
                                                @endforeach
                                            @else
                                                <option value="">Tidak ada benefit yang dapat di tambahkan</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group bg-default">
                                    <div class="col-sm-10 col-sm-push-2">
                                        <button 
                                            class="btn btn-primary" 
                                            @if($availbleBenefitOption->count() <= 0)
                                            disabled
                                            @endif
                                            >
                                            Add Benefit
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('script')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.js"></script>
<script>

    function deleteConfirmation(benefitId) {
        Swal.fire({
            type: 'warning',
            customClass: {
                container: 'custom-swal'
            },
            title: 'Konfirmasi',
            html: '<strong>Apakah anda yakin ingin menghapus benefit ini.</strong><br/>Tetap Lanjutkan?',
            showCancelButton: true,
            confirmButtonText: 'Batalkan',
            cancelButtonText: 'Hapus Benefit',
            allowOutsideClick: false
        }).then((result) => {
            if (!result.dismiss) return;

            var songId = "{{ $room->song_id }}";
            var token = '{{ csrf_token() }}';
            var deleteUrl = "/admin/assign-kost-value/"+songId+"/remove";

            return $.ajax({
                type: 'POST',
                url: deleteUrl,
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    'benefit_id': benefitId,
                    '_token': token
                },
                success: (response) => {
                    if (response.success) {
                        Swal.fire({
                            type: 'success',
                            title: "Benefit berhasil di hapus",
                            text: "Halaman akan di-refresh setelah Anda klik OK",
                            onClose: () => { window.location.reload(); }
                        });
                    } else {
                        Swal.fire({
                            type: 'error',
                            title: "Terjadi Galat",
                            text: response.message,
                        });
                    }
                },
                error: (error) => {
                    Swal.fire({
                        type: 'error',
                        title: "Terjadi Galat",
                        text: response.message,
                    });
                }
            });
        })
    }

    function assignBenefitModal(e) {
        $('#modal').modal('show');
        return;
    }
</script>
@endsection