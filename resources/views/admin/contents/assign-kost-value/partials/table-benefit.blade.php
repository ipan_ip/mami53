<div class="">
    <table id="table" class="table table-responsive table-bordered">
        <thead>
            <tr>
                <th>Name</th>
                <th>Logo Image</th>
                <th>Benefit Description</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @if ($listBenefit) 
                @foreach($listBenefit as $id => $benefit)
                <tr>
                    <td>
                        {{ $benefit['title'] }}
                    </td>
                    <td>
                        <img src="{{ $benefit['icon_small_url'] }}" alt="">
                    </td>
                    <td>
                        {{ $benefit['description'] }}
                    </td>
                    <td>
                        @if (isset($type) && $type == 'custom-benefit')
                            <a title="delete" onclick="return deleteConfirmation({{ $benefit['id'] }})"><i class="fa fa-trash"></i> delete</a>
                        @else 
                            -
                        @endif
                    </td>
                </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="4" class="text-center">
                        no data
                    </td>
                </tr>
            @endif
        </tbody>
    </table>
</div>