<table id="table" class="table table-responsive" data-toggle="table" data-show-refresh="true" data-search="true"
    data-show-search-button="false" data-side-pagination="server" data-pagination="true" data-ajax="ajaxLoadData"
    data-query-params="queryParams" data-query-params-type="">
    <thead>
        <tr>
            <th data-field="name" data-formatter="kosNameFormatter">Kos Name</th>
            <th class="text-center" data-field="level.name">Level</th>
            <th class="text-center" data-field="benefit" data-formatter="benefitFormatter">Benefit</th>
            <th class="text-center" data-field="song_id" data-formatter="actionFormatter">Action</th>
        </tr>
    </thead>
</table>
