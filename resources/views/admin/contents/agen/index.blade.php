@extends('admin.layouts.main')

@section('style')
<style>
    .reject-remark-filled i.fa {
        color: #ff0000;
    }
</style>
@endsection

@section('content')
    <!-- table -->
    <div class="box box-primary">

        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
            <div class="box-tools pull-right">
                <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
            </div>
        </div><!-- /.box-header -->

        <div class="box-body no-padding">
            
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Room ID</th>
                        <th>Room Name</th>
                        <th>Owner Name</th>
                        <th>Owner ID</th>
                        <th>Ver. Phone Number</th>
                        <th>Owner Status</th>
                        <th>Status</th>
                        <th>Agen Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($rowsOwner as $index => $rowOwner)
                        <tr>
                            <td>{{ $index+1 }}</td>
                            <td>{{ $rowOwner->designer_id }}</td>
                            <td>@if ($rowOwner->room->incomplete_room)
                                  <span style="content: '';width: 9px;height: 9px;border-radius: 100%;display: -webkit-inline-box;background-color: red;"></span> 
                                 @endif
                            {{ $rowOwner->room->name }}</td>
                            <td>{{ $rowOwner->room->owner_phone }}</td>
                            <td>{{ $rowOwner->user_id }}</td>
                            <td>{{ isset($rowOwner->user) ? $rowOwner->user->phone_number : "--" }}</td>
                            <td>{{ $rowOwner->owner_status}}</td>

                            @if($rowOwner->status == 'verified')
                                <td><span class="label label-success">{{ $rowOwner->status }}</span></td>
                            @elseif($rowOwner->status == 'unverified')
                                <td><span class="label label-danger">{{ $rowOwner->status }}</span></td>
                            @else
                                <td><span class="label label-info">
                                @if ($rowOwner->status == 'draft2')
                                 Edited
                                @else
                                 {{ $rowOwner->status }}
                                @endif
                                </span></td>
                            @endif

                            <td>{{ isset($rowOwner->room->agent_name) ? $rowOwner->room->agent_name : "--" }}</td>
                            <td class="table-action-column">
                                
                                   <a href="{{ route('admin.owner.verify', $rowOwner->id) }}" class="btn btn-xs btn-primary" title="Verify">
                                            <i class="fa fa-check"></i>
                                   </a>  
                                   <a href="{{ URL::to('admin/owner/agen', $rowOwner->id) }}" class="btn btn-xs btn-danger"><i class="fa fa-share" aria-hidden="true"></i> foward</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

        </div><!-- /.box-body -->
        <div class="box-body no-padding">
            {{ $rowsOwner->appends(Request::except('page'))->links() }}
        </div>
    </div><!-- /.box -->
    <!-- table -->
@endsection
@section('script')
    <!-- Jquery Datepicker -->
    <!-- <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script> -->

    <script type="text/javascript">

        $(function()
        {
            $('#select-sort-option').change(function(event){
                var url  = '{{ URL::route("admin.room.index") }}' + '?o=' + event.target.value ;

                url += '&st=' + $('#start-date').val();
                url += '&en=' + $('#end-date').val();

                window.location = url;

            });

            $('.datepicker').datepicker({ dateFormat : 'dd-mm-yy'});
        });
    </script>
@endsection