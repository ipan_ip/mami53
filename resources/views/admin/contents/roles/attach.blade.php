@extends('admin.layouts.main')

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

        <form action="{{ URL::route('admin.role.users.attach', $role->id) }}" method="POST" class="form-horizontal form-bordered">
            <div class="box-body no-padding">
                <div class="form-group bg-default">
                    <label for="user-name" class="control-label col-sm-2">User</label>
                    <div class="col-sm-10">
                        <input type="text" name="user_name" id="user-name" class="form-control" value="{{ old('user_name') }}" placeholder="Ketikkan email atau nama" data-toggle="dropdown">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="user-id" class="control-label col-sm-2">User ID</label>
                    <div class="col-sm-10">
                        <input type="text" name="user_id" id="user-id" class="form-control" value="{{ old('user_id') }}">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <div class="col-sm-10 col-sm-push-2">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('script')
<script>
    function clickSuggestion(idx) {
        key = idx;
        val = $('.suggestion-item-' + idx).text();
        el = $('.suggestion-item-' + idx);

        $('#user-name').val(val);
        $('#user-id').val(key);
    }

    $(function() {
        var userSuggestion = function(e) {
            $.ajax({
                type: 'POST',
                url: '/admin/role/users/suggestion',
                data: {
                    'params': e.target.value,
                    'role_id': {{ $role->id }},
                },
                success: function(data) {
                    $(e.target).next('.dropdown-menu').remove();

                    listOption = '<ul class="dropdown-menu suggestion-list" aria-labelledby="dLabel">'
                    for(idx in data.users) {
                        listOption += '<li><a href="#" class="suggestion-item suggestion-item-' + idx + '" data-key="' + idx + '" onclick="clickSuggestion(' + idx + ')">' + data.users[idx] + '</a></li>';
                    }

                    listOption += '</ul>';

                    $(listOption).insertAfter($(e.target));

                    $('.suggestion-list').dropdown();
                }
                
            })
        }

        var delay = (function(){
          var timer = 0;
          return function(callback, ms){
            clearTimeout (timer);
            timer = setTimeout(callback, ms);
          };
        })();

        $(document).on('keyup', '#user-name', function(e) {
            delay(function(){
                userSuggestion(e)
            }, 1000 );
        });
    });
</script>
@endsection