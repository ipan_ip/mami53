@extends('admin.layouts.main')

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

        <form action="{{ URL::route('admin.role.store') }}" method="POST" class="form-horizontal form-bordered">
            <div class="box-body no-padding">
                <div class="form-group bg-default">
                    <label for="role-name" class="control-label col-sm-2">Role Name</label>
                    <div class="col-sm-10">
                        <input type="text" name="name" id="role-name" class="form-control" value="{{ old('name') }}">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="display-name" class="control-label col-sm-2">Display Name</label>
                    <div class="col-sm-10">
                        <input type="text" name="display_name" id="display-name" class="form-control" value="{{ old('display_name') }}">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <div class="col-sm-10 col-sm-push-2">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection