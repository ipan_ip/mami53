@extends('admin.layouts.main')

@section('content')
	<!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <a href="{{ URL::route('admin.role.users.attach', $role->id) }}">
                        <button type="button" name="button" class="btn-add btn btn-primary btn-sm">
                            <i class="fa fa-plus">&nbsp;</i> Attach Users </button>
                    </a>
                    <a href="{{ URL::route('admin.role.users.create', $role->id) }}">
                        <button type="button" name="button" class="btn-add btn btn-primary btn-sm">
                            <i class="fa fa-plus">&nbsp;</i> Create Users </button>
                    </a>
                </div>
            </div>

            <table  class="table table-striped">
                <thead>
                    <tr>
                        <th>ID(user_id)</th>
                        <th>Sign-in ID</th>
                        <th>Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($users as $roleUser)
                    <tr>
                        <td>{{ $roleUser->id }}</td>
                        <td>{{ $roleUser->email }}</td>
                        <td>{{ $roleUser->name }}</td>
                        <td>
                            <a href="{{ route('admin.role.users.detach', [$role->id, $roleUser->id]) }}" title="Detach">
                                <i class="fa fa-trash-o"></i>
                            </a>
                            <a href="{{ route('admin.role.users.change-password', [$role->id, $roleUser->id]) }}" title="Change Password">
                                <i class="fa fa-key"></i>
                            </a>
                        </td>
                    </tr> 
                @endforeach
                </tbody>
                
            </table>
        </div>
    </div><!-- /.box -->
    <!-- table -->
@endsection