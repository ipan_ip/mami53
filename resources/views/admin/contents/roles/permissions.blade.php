@extends('admin.layouts.main')

@section('content')
	<!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <a href="{{ URL::route('admin.role.permissions.attach', $role->id) }}" class="btn-add btn btn-primary btn-sm">
                        <!-- <button type="button" name="button"> -->
                            <i class="fa fa-plus">&nbsp;</i> Attach Permission 
                        <!-- </button> -->
                    </a>

                    @if(count($permissions) <= 0)
                        <a href="{{ URL::route('admin.role.permissions.duplicate', $role->id) }}">
                            <button type="button" name="button" class="btn-add btn btn-warning btn-sm">
                                <i class="fa fa-copy">&nbsp;</i> Duplicate Permission </button>
                        </a>
                    @endif
                    
                    <button type="button" name="button" id="flush-role-button" class="btn-add btn btn-primary btn-sm pull-right" style="margin-right:10px;">
                        <i class="fa fa-refresh">&nbsp;</i>Flush Role and Permission
                    </button>
                </div>
            </div>

            <table  class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Permission Name</th>
                        <th>Permission Display Name</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($permissions as $permission)
                    <tr>
                        <td>{{ $permission->id }}</td>
                        <td>{{ $permission->name }}</td>
                        <td>{{ $permission->display_name }}</td>
                        <td>
                            <a href="{{ route('admin.role.permissions.detach', [$role->id, $permission->id]) }}" title="Detach Role">
                                <i class="fa fa-trash-o"></i>
                            </a>
                        </td>
                    </tr> 
                @endforeach
                </tbody>
                
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
    <!-- table -->
    <script>
        $( "#flush-role-button" ).click(function() {
            $.get( "/admin/role/flush", function( data ) {
                // Response Example
                // Case #1. Success
                // {"result":true}
                // Case #2. Fail
                // {"result":false,"errorMessage":"Too frequently called. Interval must be longer than 1 minutes"}
                if (data.result)
                {
                    alert('Success!\nRoles and permissions are updated on the service.')
                }
                else
                {
                    alert('Fail!\n' + data.errorMessage);
                }
            });
        });
    </script>
@endsection