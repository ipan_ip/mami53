@extends('admin.layouts.main')

@section('style')
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
@endsection

@section('content')
	<!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <form action="" class="form-inline" method="get">
                    <div class="btn-horizontal-group bg-default">
                        <a href="{{ URL::route('admin.role.create') }}">
                            <button type="button" name="button" class="btn-add btn btn-primary btn-sm">
                                <i class="fa fa-plus">&nbsp;</i> Add Roles </button>
                        </a>

                        <input type="text" name="user" class="form-control input-sm"  placeholder="User's name or email"  autocomplete="off" value="{{ request()->input('user') }}">

                        <input type="text" name="permission" class="form-control input-sm"  placeholder="Permission's name"  autocomplete="off" value="{{ request()->input('permission') }}">
                    
                        <button class="btn btn-primary btn-sm" id="buttonSearch"><i class="fa fa-search">&nbsp;</i>Search</button>
                    
                        <button type="button" name="button" id="flush-role-button" class="btn-add btn btn-primary btn-sm pull-right" style="margin-right:10px;">
                            <i class="fa fa-refresh">&nbsp;</i>Flush Role and Permission</button>
                    </div>
                </form>
            </div>

            <table  class="table">
                <thead>
                    <tr>
                        <th class="text-center">ID</th>
                        <th class="text-center">Role Name</th>
                        <th class="text-center">Display Name</th>
                        <th class="text-center">Number of Permission</th>
                        <th class="text-center">Number of Users</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                @foreach($roles as $role)
                    <tr>
                        <td align="center" style="vertical-align: middle;">{{ $role->id }}</td>
                        <td align="center" style="vertical-align: middle;">{{ $role->name }}</td>
                        <td align="center" style="vertical-align: middle;">{{ $role->display_name }}</td>
                        <td align="center" style="vertical-align: middle;">{{ $role->permission_count }}</td>
                        <td align="center" style="vertical-align: middle;">{{ $role->user_count }}</td>
                        <td align="center" class="table-action-column">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-cog"></i> <span class="caret"></span>
                                </button>

                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a href="{{ route('admin.role.edit', $role->id) }}" title="Edit">
                                            <i class="fa fa-pencil"></i> Edit Permission
                                        </a>
                                    </li>

                                        <li>
                                            <a class="btn-action" href="#" data-id="{{ $role->id }}" data-name="{{ $role->name }}" data-action="delete-role">
                                                <i class="fa fa-trash"></i> Delete Permission
                                            </a>
                                        </li>

                                    <li>
                                        <a href="{{ route('admin.role.users', $role->id) }}" title="Users">
                                            <i class="fa fa-user"></i> View Users
                                        </a>
                                    </li>

                                    <li>
                                        <a href="{{ route('admin.role.permissions', $role->id) }}" title="Permissions">
                                            <i class="fa fa-paperclip"></i> View Permissions
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr> 
                @endforeach
                </tbody>
                
            </table>
        </div><!-- /.box-body -->
        <div class="box-body no-padding">
            {{ $roles->appends(Request::except('page'))->links() }}
        </div>
    </div><!-- /.box -->
    <!-- table -->
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.js"></script>
    <script>
        var token = '{{ csrf_token() }}'

        $('.btn-action').on('click', (e) => {
            e.preventDefault();
            var _id = $(e.currentTarget).data('id');
            var _name = $(e.currentTarget).data('name');

            Swal.fire({
                    title: `Delete Role ${_name} ?`,
                    text: '',
                    type: 'question',
                    confirmButtonText: 'Yes',
                    showLoaderOnConfirm: true,
                    preConfirm: () => {
                        return $.ajax({
                            type: 'POST',
                            url: `/admin/role/${_id}`,
                            dataType: 'json',
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            data: {
                                '_method': 'DELETE',
                                '_token': token
                            },
                            success: (response) => {
                                if (!response.success) {
                                    Swal.insertQueueStep({
                                        type: 'error',
                                        title: response.message
                                    });
                                    return;
                                }
                            },
                            error: (error) => {
                                Swal.insertQueueStep({
                                    type: 'error',
                                    title: 'Error: ' + error.message
                                });
                            }
                        });
                    },
                    allowOutsideClick: () => !Swal.isLoading()
                }).then((result) => {
                    if (result.value.success) {
                        Swal.fire({
                            type: 'success',
                            title: result.value.message,
                            text: "Halaman akan di-refresh setelah Anda klik OK",
                            onClose: () => {
                                window.location.reload();
                            }
                        });
                    } else {
                        Swal.fire({
                            type: 'error',
                            title: result.value.message,
                            onClose: () => {
                                window.location.reload();
                            }
                        });
                    }
                });
        });

        $( "#flush-role-button" ).click(function() {
            $.get( "/admin/role/flush", function( data ) {
                // Response Example
                // Case #1. Success
                // {"result":true}
                // Case #2. Fail
                // {"result":false,"errorMessage":"Too frequently called. Interval must be longer than 1 minutes"}
                if (data.result)
                {
                    alert('Success!\nRoles and permissions are updated on the service.')
                }
                else
                {
                    alert('Fail!\n' + data.errorMessage);
                }
            });
        });
    </script>
@endsection