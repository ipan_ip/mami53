@extends('admin.layouts.main')

@section('content')
	<!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <form action="" class="form-inline" method="get" style="text-align:right;padding:10px;">
                        <a href="{{ URL::route('admin.aggregator.create') }}">
                            <button type="button" name="button" class="btn-add btn btn-primary btn-sm pull-left">
                                <i class="fa fa-plus">&nbsp;</i> Add Aggregator Partner </button>
                        </a>

                        <input type="text" name="q" class="form-control input-sm"  placeholder="partner name/ feeds url"  autocomplete="off" value="{{ request()->input('q') }}">
                        <button class="btn btn-primary btn-md" id="buttonSearch"><i class="fa fa-search">&nbsp;</i>Search</button>
                    </form>
                </div>
            </div>

            <table  class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Type</th>
                        <th>Partner Name</th>
                        <th>Feeds URL</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                @foreach($partners as $partner)
                    <tr>
                        <td>{{ $partner->id }}</td>
                        <td>{{ $partner->type }}</td>
                        <td>{{ $partner->partner_name }}</td>
                        <td><a href="{{ $partner->feeds_url }}">{{ $partner->feeds_url }}</a></td>
                        <td>
                            <a href="{{ route('admin.aggregator.edit', $partner->id) }}" title="Edit">
                                <i class="fa fa-pencil"></i>
                            </a>
                            <a href="{{ route('admin.aggregator.delete', $partner->id) }}" title="Delete">
                                <i class="fa fa-trash-o"></i>
                            </a>
                        </td>
                    </tr> 
                @endforeach
                </tbody>
                
            </table>
        </div><!-- /.box-body -->
        <div class="box-body no-padding">
            {{ $partners->appends(Request::except('page'))->links() }}
        </div>
    </div><!-- /.box -->
    <!-- table -->
@endsection