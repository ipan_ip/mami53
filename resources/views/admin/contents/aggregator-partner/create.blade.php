@extends('admin.layouts.main')

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

        <form action="{{ URL::route('admin.aggregator.store') }}" method="POST" class="form-horizontal form-bordered">
            <div class="box-body no-padding">

                <div class="form-group bg-default">
                    <label for="type" class="col-sm-2 control-label">Type</label>
                    <div class="col-sm-10">
                        <select name="type" id="type" class="form-control chosen-select">
                            @foreach($typeOptions as $key => $type)
                             <option value="{{ $type }}" {{ old('type') == $type ? 'selected="selected"' : '' }}>{{ $type }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="aggregator-name" class="control-label col-sm-2">Partner Name</label>
                    <div class="col-sm-10">
                        <input type="text" name="partner_name" id="aggregator-name" class="form-control" value="{{ old('partner_name') }}">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="display-name" class="control-label col-sm-2">URL</label>
                    <div class="col-sm-10">
                        <input type="text" name="feeds_url" id="feeds-url" class="form-control" value="{{ old('feeds_url') }}">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <div class="col-sm-10 col-sm-push-2">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection