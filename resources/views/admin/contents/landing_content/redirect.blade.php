@extends('admin.layouts.main')
@section('content')
    <!-- table -->
    <div class="box box-primary" style="padding: 10px;">

        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div><!-- /.box-header -->
       
		<form action="/admin/landing-content/redirect/{{ $landingContents->id }}" method="post">
		  <div class="form-group">
		    <label for="landing_destination">ID Landing Content Tujuan</label>
		    <input type="text" class="form-control" name="landing_destination" placeholder="ID Landing Content Tujuan" id="landing_destination" value="{{ old('landing_destination', $landingContents->redirect_id) }}">
		  </div>
		  <button type="submit" class="btn btn-default">Submit</button>
		</form>

    </div><!-- /.box -->
    <!-- table -->

   
@endsection
