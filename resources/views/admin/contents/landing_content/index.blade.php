@extends('admin.layouts.main')

@section('style')
    <style>
        tr.redirected td {
            color: #ff0000;
        }

        .text-redirected {
            color: #ff0000;
        }
    </style>
@endsection

@section('content')
	<!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <a href="{{ URL::route('admin.landing-content.create') }}">
                        <button type="submit" class="btn-add btn btn-primary btn-sm">
                            <i class="fa fa-plus">&nbsp;</i>Tambah Landing Content</button>
                    </a>
                    <form action="" class="form-inline" method="get" style="text-align:right;padding:10px;">
                        <input type="text" name="q" class="form-control input-sm"  placeholder="Title / Slug"  autocomplete="off" value="{{ request()->input('q') }}">
                        <button class="btn btn-primary btn-md" id="buttonSearch"><i class="fa fa-search">&nbsp;</i>Search</button>
                    </form>
                </div>
            </div>

            <p class="helper-block">Warna <span class="text-redirected">Merah</span> berarti landing di-redirect ke landing lain</p>

            <table  class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Title</th>
                        <th>Slug</th>
                        <th>Type</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                 @foreach($landingContents as $landingContent)
                     <tr class="{{ !is_null($landingContent->redirect_id) ? 'redirected' : '' }}">
                         <td>{{ $landingContent->id }}</td>
                         <td>{{ $landingContent->title }}</td>
                         <td>{{ $landingContent->slug }}</td>
                         <td>{{ $landingContent->type }}</td>
                         <td>
                            @if (\Entrust::hasRole('content-editor')) 
                                <a href="{{route('admin.landing-content.edit', $landingContent->id)}}" title="edit">
                                    <i class="fa fa-pencil"></i>
                                </a>

                                @if ($landingContent->type == 'konten')
                                <a href="https://mamikos.com/konten/{{ $landingContent->slug }}" title="Show Preview" target="_blank" rel="noopener">
                                    <i class="fa fa-eye"></i>
                                </a>
                                @elseif ($landingContent->type == 'loker')
                                <a href="https://mamikos.com/loker/1/post/{{ $landingContent->slug }}" title="Show Preview" target="_blank" rel="noopener">
                                    <i class="fa fa-eye"></i>
                                </a>
                                @elseif ($landingContent->type == 'apartment') 
                                <a href="https://mamikos.com/apartemen/post/{{ $landingContent->slug }}" title="Show Preview" target="_blank" rel="noopener">
                                    <i class="fa fa-eye"></i>
                                </a>
                                @endif
                            @else
                                <a href="{{route('admin.landing-content.edit', $landingContent->id)}}" title="edit">
                                    <i class="fa fa-pencil"></i>
                                </a>

                                <a href="{{ URL::route('admin.landing-content.delete', $landingContent->id) }}" title="Hapus">
                                    <i class="fa fa-trash-o"></i>
                                </a>

                                @if ($landingContent->type == 'konten')
                                <a href="https://mamikos.com/konten/{{ $landingContent->slug }}" title="Show Preview" target="_blank" rel="noopener">
                                    <i class="fa fa-eye"></i>
                                </a>
                                @elseif ($landingContent->type == 'loker')
                                <a href="https://mamikos.com/loker/1/post/{{ $landingContent->slug }}" title="Show Preview" target="_blank" rel="noopener">
                                    <i class="fa fa-eye"></i>
                                </a>
                                @elseif ($landingContent->type == 'apartment') 
                                <a href="https://mamikos.com/apartemen/post/{{ $landingContent->slug }}" title="Show Preview" target="_blank" rel="noopener">
                                    <i class="fa fa-eye"></i>
                                </a>
                                @endif

                                <a href="{{ url('/admin/landing-content/redirect/' . $landingContent->id) }}" title="Redirect" >
                                    <i class="fa fa-exchange"></i>
                                </a>

                            @endif
                         </td>
                     </tr> 
                 @endforeach
                </tbody>
                
            </table>
        </div><!-- /.box-body -->

        <div class="box-body no-padding">
            {{ $landingContents->appends(Request::except('page'))->links() }}
        </div>
    </div><!-- /.box -->
    <!-- table -->
@endsection