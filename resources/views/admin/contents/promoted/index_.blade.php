@extends('admin.layouts.main')
@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
            <div class="box-tools pull-right">
                <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <a href="{{ URL::route('admin.promoted.create') }}">
                        <form action="{{ $searchUrl }}" class="form-inline" method="post" style="text-align:right;padding:10px;">
                        <a href="{{ route( $createRoute ) }}" class="btn btn-primary btn-sm pull-left">
                            <i class="fa fa-plus">&nbsp;</i>Add Promoted
                        </a>
                        <input type="text" name="q" class="form-control input-sm"  placeholder="Nama Promoted Kost"  autocomplete="off" value="{{ Request::get('q') }}">
                        <button class="btn btn-primary btn-md" id="buttonSearch"><i class="fa fa-search">&nbsp;</i>Search</button>
                    </form>
                    </a>
                </div>
            </div>

            <table id="tableListPromoted" class="table table-striped">
                <thead>
                    <tr>
                        <th width="30px;">No</th>
                        <th>Promoted Title</th>
                        <th class="table-action-column" width="100px">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($rowsPromoted as $rowPromotedKey => $rowPromoted)
                    <tr>
                        <td>{{ $rowPromotedKey+1 }}</td>
                        <td>{{ $rowPromoted->name }}</td>
                        <td class="table-action-column">
                            <div class="btn-action-group">
                                <a href="{{ URL::route('admin.promoted.edit', $rowPromoted->id) }}">
                                    <i class="fa fa-pencil"></i></a>
                                <a href="{{ URL::route('admin.promoted.destroy', $rowPromoted->id) }}">
                                    <i class="fa fa-trash-o"></i></a>
                                <a href="{{ route( $publishRoute, $rowPromoted->id )}}">
                                    @if($rowPromoted->is_published == 0)
                                    <i class="fa fa-check"></i>
                                    @else
                                    <i class="fa fa-ban"></i>
                                    @endif
                                </a>    
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th width="30px;">No</th>
                        <th>Promoted Title</th>
                        <th class="table-action-column" width="100px">Action</th>
                    </tr>
                </tfoot>
            </table>
        </div><!-- /.box-body -->
         <div class="box-body no-padding">
            {{ $rowsPromoted->appends(Request::except('page'))->links() }}
        </div>
    </div><!-- /.box -->
    <!-- table -->
@stop
@section('script')
<!-- page script -->
<script type="text/javascript">
    $(function()
    {
        $("#tableListPromoted").dataTable();
    });
</script>
@stop