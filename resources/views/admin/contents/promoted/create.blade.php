@extends('admin.layouts.main')

@section('content')
    <div class="box box-primary" ng-controller="recommendationCtrl">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>
        <form action="{{ URL::route('admin.promoted.store') }}" class="form-horizontal form-bordered" id="formInsertDesigner" method="POST">
            <div class="box-body no-padding">

                <div class="form-group bg-default">
                    <label for="top-kost-1" class="col-sm-2 control-label">Kost</label>
                    <div class="col-sm-10">
                        <div class="col-sm-6" style="padding-left:0px;">
                            <input type="text" class="form-control" rows="3" id="top-kost-1" name="top_kost_1" placeholder="Insert id / name here" ng-model="formKost.id1" value="{{ old('top_kost_1') }}" required/>
                        </div>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" ng-model="formKost.name1" disabled>
                        </div>
                        <div class="col-sm-12" style="padding-left:0px;">
                            <span class="btn btn-primary" style="margin-top:10px;" ng-click="checkName('1',formKost.id1)">Cari</span>
                        </div>
                    </div>
                </div>

                <!-- di div ini ditampilin detail dari kost yang udah diambil begitu juga dengan kost yang lain -->
                <div class="form-group bg-default results" ng-repeat="kost in result_kost1 | limitTo:quantity" ng-show="pick1==false">
                    <div class="col-sm-8 push-2">
                        <label for="top-kost-1" class="col-sm-2 control-label"></label>
                        <div class="col-sm-10" ng-bind="kost.name" ng-click="pickKost('1',kost.name, kost.id)">
                        </div>
                    </div>
                </div>
                <!-- -->
            </div>

            <div class="form-group bg-default">
                <label for="pinned_type" class="col-sm-2 control-label">Tipe</label>
                <div class="col-sm-10">
                    <select name="pinned_type" class="form-control" id="pinned_type">
                        @foreach($pinnedTypeOptions as $key => $option)
                            <option value="{{ $key }}" {{ old('pinned_type') == $key ? 'selected="selected"' : '' }}>{{ $option }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="period_end" class="col-sm-2 control-label">Tanggal Akhir Periode</label>
                <div class="col-sm-10">
                    <input type="text" name="period_end" id="period_end" class="form-control" value="{{ old('period_end') }}" placeholder="format tanggal : YYYY-MM-DD (ex: 2018-01-31)">
                    <p class="help-block">Tanggal harus diisi jika Tipe diisi</p>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button class="btn btn-primary">Save</button>
                </div>
            </div>

        </form>
    </div>
    
@stop
@section('script')
    <!-- start writing the script here -->
    <script>

        myApp.factory("checkName", function($resource) {
            return $resource('/admin/:api/:data',
                    {api:'promoted', data:'search'},
                    {getname: {method:'POST',headers:{'Authorization':'GIT WEB:WEB','X-GIT-Time':'1406090202','Content-Type': 'application/json'}}
                    });
        });

        myApp.controller('recommendationCtrl',['$scope', 'checkName', '$window', function ($scope, checkName, saveTopKost, updateTopKost, window)
        {
            //Recommendation Kost (Top Kost)

            //Check name input

            $scope.checkName =  function (id,name) {
                var param = {
                    "search" : name
                };
                console.log(param);
                checkName.getname(param,function (result) {
                    if (id == '1') {
                        $scope.result_kost1 = result.search_result;
                        $scope.pick1 = false;
                    }

                    console.log(result);

                });
            };

            $checkvalid = false;
            $scope.quantity = 10;
            //Pick kost from list
            $scope.pickKost = function (id,name,idkost) {
                if (id == '1') {
                    $scope.formKost.name1 = name;
                    $scope.formKost.id1 = idkost;
                    $scope.pick1 = true;
                }
            };

            //Send Kost
            $scope.formKost = {
                "name1":"",
                "id1":"<?php echo @$rowTopKosts[0]['designer_id'] ?>",
            };

        }]);

        //disable enter key on submit
        function stopRKey(evt) {
            var evt = (evt) ? evt : ((event) ? event : null);
            var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
            if ((evt.keyCode == 13) && (node.type=="text"))  {return false;}
        }

        document.onkeypress = stopRKey;
    </script>
@stop