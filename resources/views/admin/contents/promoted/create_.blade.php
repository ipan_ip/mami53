@extends('admin.layouts.main')

@section('content')
    <div class="box box-primary" ng-controller="recommendationCtrl">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>
        {{ Form::open(array(
        'route'  => $formAction,
        'method' => $formMethod,
        'class'  => 'form-horizontal form-bordered',
        'role' => 'form',
        'id' => 'formInsertDesigner',
        'name' => 'topkost')) }}
        <div class="box-body no-padding">
            <div class="form-group bg-default">
                <label for="inputName" class="col-sm-2 control-label">Title</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Title"
                           id="inputTitle" name="title"  value="{{ @$rowRecommendation->name }}" ng-model="formKost.title" required>
                    <div id="countTitle" style="font-weight:bold"></div>
                </div>
            </div>

            <div class="form-group bg-info divider">
                <div class="col-sm-12 pull-left">
                    List Top Kost
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="top-kost-1" class="col-sm-2 control-label">Kost 1</label>
                <div class="col-sm-10">
                    <div class="col-sm-6" style="padding-left:0px;">
                        <input type="text" class="form-control" rows="3" id="top-kost-1" name="top_kost_1" placeholder="Insert id / name here" ng-model="formKost.id1" value="<?php echo @$rowTopKosts[0]['designer_id'] ?>" required/>
                    </div>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" ng-model="formKost.name1" disabled>
                    </div>
                    <div class="col-sm-12" style="padding-left:0px;">
                        <span class="btn btn-primary" style="margin-top:10px;" ng-click="checkName('1',formKost.id1)">Cari</span>
                    </div>
                </div>
            </div>

            <!-- di div ini ditampilin detail dari kost yang udah diambil begitu juga dengan kost yang lain -->
            <div class="form-group bg-default results" ng-repeat="kost in result_kost1 | limitTo:quantity" ng-show="pick1==false">
                <div class="col-sm-8 push-2">
                    <label for="top-kost-1" class="col-sm-2 control-label"></label>
                    <div class="col-sm-10" ng-bind="kost.name" ng-click="pickKost('1',kost.name, kost.id)">
                        <!-- <input type="text" class="form-control" rows="3" id="id-detail-kost" name="detail_kost" /> -->
                    </div>
                </div>
            </div>
            <!-- -->

            <div class="form-group bg-default">
                <label for="top-kost-2" class="col-sm-2 control-label">Kost 2</label>
                <div class="col-sm-10">
                    <div class="col-sm-6" style="padding-left:0px;">
                        <input type="text" class="form-control" rows="3" id="top-kost-2" name="top_kost_2" placeholder="Insert id / name here" ng-model="formKost.id2" value="<?php echo @$rowTopKosts[1]['designer_id'] ?>" required/>
                    </div>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" ng-model="formKost.name2" disabled>
                    </div>
                    <div class="col-sm-12" style="padding-left:0px;">
                        <span class="btn btn-primary" style="margin-top:10px;" ng-click="checkName('2',formKost.id2)">Cari</span>
                    </div>
                </div>
            </div>

            <!-- di div ini ditampilin detail dari kost yang udah diambil begitu juga dengan kost yang lain -->
            <div class="form-group bg-default results" ng-repeat="kost in result_kost2 | limitTo:quantity" ng-show="pick2==false">
                <div class="col-sm-8 push-2">
                    <label for="top-kost-1" class="col-sm-2 control-label"></label>
                    <div class="col-sm-10" ng-bind="kost.name" ng-click="pickKost('2',kost.name, kost.id)">
                        <!-- <input type="text" class="form-control" rows="3" id="id-detail-kost" name="detail_kost" /> -->
                    </div>
                </div>
            </div>
            <!-- -->

            <div class="form-group bg-default">
                <label for="top-kost-3" class="col-sm-2 control-label">Kost 3</label>
                <div class="col-sm-10">
                    <div class="col-sm-6" style="padding-left:0px;">
                        <input type="text" class="form-control" rows="3" id="top-kost-3" name="top_kost_3" placeholder="Insert id / name here" ng-model="formKost.id3" value="<?php echo @$rowTopKosts[2]['designer_id'] ?>" required/>
                    </div>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" ng-model="formKost.name3" disabled>
                    </div>
                    <div class="col-sm-12" style="padding-left:0px;">
                        <span class="btn btn-primary" style="margin-top:10px;" ng-click="checkName('3',formKost.id3)">Cari</span>
                    </div>
                </div>
            </div>

            <!-- di div ini ditampilin detail dari kost yang udah diambil begitu juga dengan kost yang lain -->
            <div class="form-group bg-default results" ng-repeat="kost in result_kost3 | limitTo:quantity" ng-show="pick3==false">
                <div class="col-sm-8 push-2">
                    <label for="top-kost-1" class="col-sm-2 control-label"></label>
                    <div class="col-sm-10" ng-bind="kost.name" ng-click="pickKost('3',kost.name, kost.id)">
                        <!-- <input type="text" class="form-control" rows="3" id="id-detail-kost" name="detail_kost" /> -->
                    </div>
                </div>
            </div>
            <!-- -->

            <div class="form-group bg-default">
                <label for="top-kost-4" class="col-sm-2 control-label">Kost 4</label>
                <div class="col-sm-10">
                    <div class="col-sm-6" style="padding-left:0px;">
                        <input type="text" class="form-control" rows="3" id="top-kost-4" name="top_kost_4" placeholder="Insert id / name here" ng-model="formKost.id4" value="<?php echo @$rowTopKosts[3]['designer_id'] ?>" required/>
                    </div>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" ng-model="formKost.name4" disabled>
                    </div>
                    <div class="col-sm-12" style="padding-left:0px;">
                        <span class="btn btn-primary" style="margin-top:10px;" ng-click="checkName('4',formKost.id4)">Cari</span>
                    </div>
                </div>
            </div>

            <!-- di div ini ditampilin detail dari kost yang udah diambil begitu juga dengan kost yang lain -->
            <div class="form-group bg-default results" ng-repeat="kost in result_kost4 | limitTo:quantity" ng-show="pick4==false">
                <div class="col-sm-8 push-2">
                    <label for="top-kost-1" class="col-sm-2 control-label"></label>
                    <div class="col-sm-10" ng-bind="kost.name" ng-click="pickKost('4',kost.name, kost.id)">
                        <!-- <input type="text" class="form-control" rows="3" id="id-detail-kost" name="detail_kost" /> -->
                    </div>
                </div>
            </div>
            <!-- -->

            <div class="form-group bg-default">
                <label for="top-kost-5" class="col-sm-2 control-label">Kost 5</label>
                <div class="col-sm-10">
                    <div class="col-sm-6" style="padding-left:0px;">
                        <input type="text" class="form-control" rows="3" id="top-kost-5" name="top_kost_5" placeholder="Insert id / name here" ng-model="formKost.id5" value="<?php echo @$rowTopKosts[4]['designer_id'] ?>" required/>
                    </div>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" ng-model="formKost.name5" disabled>
                    </div>
                    <div class="col-sm-12" style="padding-left:0px;">
                        <span class="btn btn-primary" style="margin-top:10px;" ng-click="checkName('5',formKost.id5)">Cari</span>
                    </div>
                </div>
            </div>

            <!-- di div ini ditampilin detail dari kost yang udah diambil begitu juga dengan kost yang lain -->
            <div class="form-group bg-default results" ng-repeat="kost in result_kost5 | limitTo:quantity" ng-show="pick5==false">
                <div class="col-sm-8 push-2">
                    <label for="top-kost-1" class="col-sm-2 control-label"></label>
                    <div class="col-sm-10" ng-bind="kost.name" ng-click="pickKost('5',kost.name, kost.id)">
                        <!-- <input type="text" class="form-control" rows="3" id="id-detail-kost" name="detail_kost" /> -->
                    </div>
                </div>
            </div>
            <!-- -->

        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button class="btn btn-primary" ng-click="savekost('<?php echo @$formMethod ?>')" ng-disabled="topkost.$invalid">Save</button>
                @if (Request::is('admin/*'))
                    <a href="{{ Route($formCancel) }}">
                        <button type="button" class="btn btn-default">Cancel</button>
                    </a>
                @endif
            </div>
        </div>
    </div>
    {{ Form::close() }}<!-- /.form -->
    </div><!-- /.box -->
@stop
@section('script')
    <!-- start writing the script here -->
    <script>

        myApp.factory("checkName", function($resource) {
            return $resource('/admin/:api/:data',
                    {api:'promoted', data:'search'},
                    {getname: {method:'POST',headers:{'Authorization':'GIT WEB:WEB','X-GIT-Time':'1406090202','Content-Type': 'application/json'}}
                    });
        });
        myApp.factory("saveTopKost", function($resource) {
            return $resource('/admin/:api/:action',
                    {api:'promoted', action:'save'},
                    {save: {method:'POST',headers:{'Authorization':'GIT WEB:WEB','X-GIT-Time':'1406090202','Content-Type': 'application/json'}}
                    });
        });
        myApp.factory("updateTopKost", function($resource) {
            return $resource('/admin/:api/:action',
                    {api:'promoted', action:'update'},
                    {update: {method:'POST',headers:{'Authorization':'GIT WEB:WEB','X-GIT-Time':'1406090202','Content-Type': 'application/json'}}
                    });
        });

        myApp.controller('recommendationCtrl',['$scope', 'checkName', 'saveTopKost', 'updateTopKost', '$window', function ($scope, checkName, saveTopKost, updateTopKost, window)
        {
            //Recommendation Kost (Top Kost)

            //Check name input

            $scope.checkName =  function (id,name) {
                var param = {
                    "search" : name
                };
                console.log(param);
                checkName.getname(param,function (result) {
                    if (id == '1') {
                        $scope.result_kost1 = result.search_result;
                        $scope.pick1 = false;
                    }else if (id == '2') {
                        $scope.result_kost2 = result.search_result;
                        $scope.pick2 = false;
                    }else if (id == '3') {
                        $scope.result_kost3 = result.search_result;
                        $scope.pick3 = false;
                    }else if (id == '4') {
                        $scope.result_kost4 = result.search_result;
                        $scope.pick4 = false;
                    }else if (id == '5') {
                        $scope.result_kost5 = result.search_result;
                        $scope.pick5 = false;
                    }

                    console.log(result);

                });
            };

            $checkvalid = false;
            $scope.quantity = 10;
            //Pick kost from list
            $scope.pickKost = function (id,name,idkost) {
                if (id == '1') {
                    $scope.formKost.name1 = name;
                    $scope.formKost.id1 = idkost;
                    $scope.pick1 = true;
                }else if (id == '2') {
                    $scope.formKost.name2 = name;
                    $scope.formKost.id2 = idkost;
                    $scope.pick2 = true;
                }else if (id == '3') {
                    $scope.formKost.name3 = name;
                    $scope.formKost.id3 = idkost;
                    $scope.pick3 = true;
                }else if (id == '4') {
                    $scope.formKost.name4 = name;
                    $scope.formKost.id4 = idkost;
                    $scope.pick4 = true;
                }else if (id == '5') {
                    $scope.formKost.name5 = name;
                    $scope.formKost.id5 = idkost;
                    $scope.pick5 = true;
                }
            };

            //Send Kost
            $scope.formKost = {
                "title":"{{ @$rowRecommendation->name }}",
                "name1":"",
                "name2":"",
                "name3":"",
                "name4":"",
                "name5":"",
                "id1":"<?php echo @$rowTopKosts[0]['designer_id'] ?>",
                "id2":"<?php echo @$rowTopKosts[1]['designer_id'] ?>",
                "id3":"<?php echo @$rowTopKosts[2]['designer_id'] ?>",
                "id4":"<?php echo @$rowTopKosts[3]['designer_id'] ?>",
                "id5":"<?php echo @$rowTopKosts[4]['designer_id'] ?>",
            };

            console.log($scope.formKost);

            $scope.savekost = function(method){
                if ( method == 'POST' ){
                    $scope.paramTopKost = {
                        "title":$scope.formKost.title,
                        "kost_1":$scope.formKost.id1,
                        "kost_2":$scope.formKost.id2,
                        "kost_3":$scope.formKost.id3,
                        "kost_4":$scope.formKost.id4,
                        "kost_5":$scope.formKost.id5
                    };
                    saveTopKost.save($scope.paramTopKost,function (result) {
                        console.log(result);
                        console.log($scope.paramTopKost);
                        if(result.error ==  "false"){
                            alert('Data Top Kost berhasil disimpan');
                            window.open("/admin/promote","_self");
                        }else{
                            alert('Gagal menyimpan Top Kost');
                        }
                    });
                }else if ( method == 'PUT' ) {
                    $scope.paramTopKost = {
                        "list_id":"<?php echo @$list_id ?>",
                        "title":$scope.formKost.title,
                        "kost_1":$scope.formKost.id1,
                        "kost_2":$scope.formKost.id2,
                        "kost_3":$scope.formKost.id3,
                        "kost_4":$scope.formKost.id4,
                        "kost_5":$scope.formKost.id5
                    };
                    updateTopKost.update($scope.paramTopKost,function (result) {
                        console.log(result);
                        console.log($scope.paramTopKost);
                        if(result.error ==  "false"){
                            alert('Data Top Kost berhasil disimpan');
                            window.open("/admin/promote","_self");
                        }else{
                            alert('Gagal menyimpan Top Kost');
                        }
                    });
                }
            }


        }]);

        //disable enter key on submit
        function stopRKey(evt) {
            var evt = (evt) ? evt : ((event) ? event : null);
            var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
            if ((evt.keyCode == 13) && (node.type=="text"))  {return false;}
        }

        document.onkeypress = stopRKey;
    </script>
@stop