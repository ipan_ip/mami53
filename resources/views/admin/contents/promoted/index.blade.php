@extends('admin.layouts.main')
@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <form action="" class="form-inline" method="get" style="text-align:right;padding:10px;">
                        <a href="{{ URL::route('admin.promoted.create') }}" class="btn btn-primary btn-sm pull-left">
                            <i class="fa fa-plus">&nbsp;</i>Add Promoted
                        </a>
                        <input type="text" name="q" class="form-control input-sm"  placeholder="Nama Promoted Kost"  autocomplete="off" value="{{ Request::get('q') }}">
                        <button class="btn btn-primary btn-md" id="buttonSearch"><i class="fa fa-search">&nbsp;</i>Search</button>
                    </form>
                </div>
            </div>
            
            <table id="tableListPromoted" class="table table-striped">
                <thead>
                    <tr>
                        <th>ID Kost</th>
                        <th>Nama Kost</th>
                        <th>Tipe</th>
                        <th>User Premium Expired</th>
                        <th>Tanggal Akhir Periode</th>
                        <th class="table-action-column">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($promotedRooms as $key => $promotedRoom)
                    <tr>
                        <td>{{ $promotedRoom->room->id }}</td>
                        <td>{{ $promotedRoom->room->name }}</td>
                        <td>{{ $promotedRoom->type }}</td>
                        <td>
                            @if (count($promotedRoom->room->owners) > 0)
                                <label class="label @if ($promotedRoom->room->owners[0]->user->date_owner_limit > date('Y-m-d')) label-primary @else label-danger  @endif ">{{ $promotedRoom->room->owners[0]->user->date_owner_limit }}</label>
                            @endif
                        </td>
                        <td>{{ $promotedRoom->period_end }}</td>
                        <td class="table-action-column">
                            <a href="{{ URL::route('admin.promoted.edit', $promotedRoom->id) }}" title="Edit">
                                <i class="fa fa-pencil"></i>
                            </a>
                            <a href="{{ URL::route('admin.promoted.remove', $promotedRoom->id) }}" title="Delete">
                                <i class="fa fa-ban"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div><!-- /.box-body -->
         <div class="box-body no-padding">
            {{ $promotedRooms->appends(Request::except('page'))->links() }}
        </div>
    </div><!-- /.box -->
    <!-- table -->
@endsection

@section('script')
<!-- page script -->
<script type="text/javascript">
    $(function()
    {
        
    });
</script>
@endsection