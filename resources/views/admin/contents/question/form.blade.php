@extends('admin.layouts.main')

@section('style')
    <style>
        #chosenForm .chosen-choices {
            border: 1px solid #ccc;
            border-radius: 4px;
            min-height: 34px;
            padding: 6px 12px;
        }
        #chosenForm .form-control-feedback {
            /* To make the feedback icon visible */
            z-index: 100;
        }

        .table > tbody > tr > td
		{
			vertical-align: middle;
		}

		/* Select2 tweak */

		.input-group .select2-container,
		.form-group .select2-container
		{
			position: relative !important;
			z-index: 2;
			float: left !important;
			width: 100% !important;
			margin-bottom: 0 !important;
			display: table !important;
			table-layout: fixed !important;
		}
		
		/* Sweetalert */
		
		.swal-shown
		{
			height: auto !important;
		}
		
		.custom-swal
		{
			z-index: 10000 !important;
		}
		
		.custom-wide-swal
		{
			width: 640px !important;
			font-size: 13px !important;
		}
		
		/* Modal tweak */
		
		.modal-open
		{
			overflow: hidden;
			position: fixed;
			width: 100%;
		}
		
		
		/* Centering the modal */
		
		.modal
		{
			text-align: center;
			padding: 0 !important;
		}
		
		.modal-dialog
		{
			display: inline-block;
			text-align: left;
			vertical-align: middle;
		}

        /* orderable tweak */    
        #orderable tr 
        {
            cursor: move!important;
        }

    </style>

{{ HTML::style('assets/vendor/jquery-chosen/chosen.css') }}
{{ HTML::style('assets/vendor/jquery-chosen-custom/chosen.css') }}

<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet"/>
<link href="//cdn.jsdelivr.net/npm/select2-bootstrap-theme@0.1.0-beta.10/dist/select2-bootstrap.min.css"
      rel="stylesheet"/>
@stop

@section('content')
<div class="box box-primary">
  <div class="box-header">
      <h3 class="box-title">{{ $boxTitle }}</h3>
      <div class="col-md-2">
        <div class="form-horizontal pull-right" style="padding-top: 4px;">
        </div>
    </div>
  </div><!-- /.box-header -->
  {{ Form::open(array('route' => $formAction, 'method' => $formMethod,
      'class' => 'form-horizontal form-bordered', 'role' => 'form', 'id' => 'formInsertTag', 'enctype' => 'multipart/form-data')) }}
      <div class="box-body no-padding">
        
            <div class="form-group bg-default">
              <label for="inputName" class="col-sm-2 control-label">Question</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" placeholder="Pertanyaan"
                  id="inputQuestion" name="question" value="{{ $data->question }}" >
              </div>
            </div>
            <div class="form-group bg-default">
                <label for="inputIcon" class="col-sm-2 control-label">Default Answer</label>
                <div class="col-sm-10">
                    <textarea name="default_answer" id="inputAnswer" class="form-control" cols="50" rows="5" placeholder="Jawaban">{{ $data->answer }}</textarea>
                </div>
            </div>
			<div class="form-group bg-default">
				<label for="inputIcon" class="col-sm-2 control-label">Order</label>
				<div class="col-sm-10">
					<input type="numeric" class="form-control" placeholder="Urutan"
					id="inputOrder" name="order" value="{{ $data->order }}" >
				</div>
			</div>

            <div class="form-group bg-default">
				<label for="inputIcon" class="col-sm-2 control-label">Custom action key</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" placeholder="Button name"
					id="inputOrder" name="custom_action_key" value="{{ $data->custom_action_key }}" >
				</div>
            </div>

            <div class="form-group bg-default">
                <div class="col-sm-2"></div>
                <div class="col-sm-10"><h3>Answer with condition</h3></div>
            </div>
            @foreach ($data->chat_answer as $key => $value)
            <div class="form-group bg-default">
                <div class="col-sm-2">
					<a href="/admin/question/answer/delete/{{ $value->id }}" class="btn btn-xs btn-danger">delete</a>
				</div>
				<div class="col-sm-10">
                    <select class="form-control" name="condition[]" style="margin-bottom: 10px;">
                            @foreach($answer_condition as $key => $condition)
                                <option value="{{ $key }}" @if($key == $value->condition) selected="true" @endif>{{ $condition }}</option>
                            @endforeach
                    </select>
					<textarea type="numeric" class="form-control" id="inputOrder" name="answer[]">{{ $value->answer }}</textarea>
				</div>
			</div>
            @endforeach

            <div class="form-group bg-default">
                <label class="control-label col-sm-2">Add new answer</label>
				<div class="col-sm-10">
                    <select class="form-control" name="condition[]" style="margin-bottom: 10px;">
                            <option value=""></option>
                            @foreach($answer_condition as $key => $condition)
                                <option value="{{ $key }}">{{ $condition }}</option>
                            @endforeach
                    </select>
					<textarea type="numeric" class="form-control" id="inputOrder" name="answer[]"></textarea>
				</div>
			</div>  

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <a href="{{ URL::route('admin.question.index') }}">
                    <button type="button" class="btn btn-default">Cancel</button>
                  </a>
                </div>
              </div>
              
    {{ Form::close() }}<!-- /.form -->

              <div class="form-group bg-info divider">
                <div class="col-sm-2">
                </div>
              </div>
      </div>

  <div class="box-header">
    <div class="col-md-1"></div>
    <h3 class="box-title"> Chat Condition </h3>
    <div class="col-md-2">
      <div class="form-horizontal pull-right" style="padding-top: 4px;">
          <button class="btn btn-success actions" data-action="add" style="margin-right: 50px;">
              <i class="fa fa-plus"></i>&nbsp;&nbsp;Add Criteria
          </button>
      </div>
  </div>

  <div class="col-md-11">
    <div class="alert alert-success" role="alert">
        <ol>
            @foreach ($chat_criteria as $criteria)
                <li><strong>{{$criteria->name}}</strong> : {{$criteria->description}}</li>
            @endforeach    
        </ol>
    </div>
  </div> 

  <div class="box-body">
    <table id="tableListCondition" class="table table-bordered">
        <thead>
        <tr>
            <th width="30px;">No</th>
            <th>Criteria</th>
            <th>Reply</th>
            <th class="table-action-column">Action</th>
        </tr>
        </thead>
        <tbody id="orderable">
        @foreach ($data->chat_condition as $condition)
            <tr data-id="{{$condition->id}}" class="able">
                <td class="text-center">{{ $condition->order }}</td>
                <td style="width: 25%;"> 
                    {{$condition->view_criteria_name}}
                </td>
                <td style="width: 60%;"> {{$condition->reply}} </td>
                <td class="text-center table-action-column" style="padding-right:15px!important;">
                    <div class="btn-group">
                        <button type="button" class="btn btn-default btn-sm dropdown-toggle"
                                data-toggle="dropdown">
                            <i class="glyphicon glyphicon-cog"></i> Actions
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a href="#" class="actions" data-action="edit" 
                                   data-data="{{ $condition }}"><i
                                   class="fa fa-edit"></i> Edit
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#" class="actions" data-action="remove"data-data="{{ $condition }}">
                                    <i class="fa fa-trash-o"></i> Remove
                                </a>
                            </li>
                        </ul>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

</div><!-- /.box -->

@include('admin.contents.question.chat-condition-modal')

@stop

@section('script')

    {{ HTML::script('assets/vendor/jquery-chosen/chosen.jquery.js') }}
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.js"></script>
<!-- page script -->

<script type="text/javascript">

    var config = {
      '.chosen-select'           : {width: '100%'}
    };
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    }

    function triggerAlert(type, message) {
        Swal.fire({
            type: type,
            customClass: {
                container: 'custom-swal'
            },
            html: message
        });
    }

    function triggerLoading(message) {
        Swal.fire({
            title: message,
            customClass: {
                container: 'custom-swal'
            },
            allowEscapeKey: false,
            allowOutsideClick: false,
            onOpen: () => {
                swal.showLoading();
            }
        });
    }

    function ajaxCall(type, title, html, method, url, payload, input, btnText, btnClass, needRefresh = true) {
        Swal.fire({
            type: type,
            title: title,
            html: html,
            buttonsStyling: btnClass == null,
            confirmButtonText: btnText,
            confirmButtonClass: btnClass,
            showCancelButton: true,
            cancelButtonClass: (btnClass !== null) ? 'btn btn-default' : null,
            customClass: 'custom-swal',
            showLoaderOnConfirm: true,
            input: input,
            preConfirm: (response) => {
                if (input !== null) {
                    let number = response;
                    if (!response) {
                        return Swal.showValidationMessage('Please provide a valid phone number!');
                    }

                    payload = {
                        "template_id": payload.id,
                        "number": response
                    };
                }

                console.log("This is the payload:", payload);

                return $.ajax({
                    type: method,
                    url: url,
                    data: payload,
                    dataType: 'json',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: (response) => {
                        if (response.success === false) {
                            Swal.showValidationMessage(response.message);
                        }

                        return;
                    },
                    error: (error) => {
                        Swal.showValidationMessage('Request Failed: ' + error.message);
                    }
                });
            },
            allowOutsideClick: () => !Swal.isLoading()
        }).then((result) => {
            if (!result.dismiss) {
                if (result.value.success == true) {
                    Swal.fire({
                        type: 'success',
                        title: result.value.title,
                        html: result.value.message,
                        onClose: () => {
                            if (needRefresh) window.location.reload();
                        }
                    });
                } else {
                    Swal.fire({
                        type: 'error',
                        title: "Failed!",
                        html: result.value.message
                    });
                }
            }
        });
    }

    $(function () {
        // Helper variable
        let _editing, _data, _modalTitle;
        let modal = $('#chat-condition-modal');
        var questionId = Number("{{$data->id}}");

        // Table orderable handler
        $('#orderable').sortable({
            cursor: 'move',
            axis: 'y',
            items : '> tr.able',
            update: (e, ui) => {
                triggerLoading('Menyimpan urutan...');

                var order = []; 
                $('#orderable tr').each( function(e) {
                    order.push({
                        id: parseInt($(this).data('id')),
                        order: $(this).index() + 1
                    });
                });
                
                order = JSON.stringify(order);
                $.ajax({
                    type: 'POST',
                    url: "/admin/chat-condition/order",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    contentType: 'application/json',
                    data: order,
                    success: (response) => {
                        if (!response.success) {
                            triggerAlert('error', response.message);
                            return;
                        }

                        Swal.fire({
                            type: 'success',
                            title: "Berhasil menyimpan urutan!",
                            text: "Halaman akan di-refresh setelah Anda klik OK",
                            onClose: () => {
                                window.location.reload();
                            }
                        });
                    },
                    error: (error) => {
                        triggerAlert('error', error.message);
                    }
                });
            }
        });

        // Actions button listeners
        $('.actions').on('click', e => {
            e.preventDefault();

            const action = $(e.currentTarget).data('action');

            // Setup Notification
            if (action === 'add') {
                _editing = false;
                _modalTitle = "<i class='fa fa-tag'></i> New Criteria";

                modal.modal('show');
            }

            // Editing action
            else if (action === 'edit') {
                _editing = true;
                _data = $(e.currentTarget).data('data');
                _modalTitle = "<i class='fa fa-tag'></i> Update Criteria #" + _data.order;

                modal.modal('show');
            }              

            // Remove action
            else if (action === 'remove') {
                _data = $(e.currentTarget).data('data');

                let payload = {
                    id: _data.id
                };

                let msg = '<h5>Deleting Condition will affect the chat<br/><strong>Please do with caution!</strong></h5>';

                ajaxCall('warning', 'Condition Removal', msg + 'Click [Remove] to continue', 'POST', '/admin/chat-condition/delete', payload, null, 'Remove', 'btn btn-danger');
            }
        });

        // :: Modal Events ::
        modal
            .on("show.bs.modal", (e) => {

                modal.data('bs.modal').options.keyboard = false;    
                modal.data('bs.modal').options.backdrop = 'static';

                $('#modalTitle').html(_modalTitle);
            
                // Form elements
                const form = $('#conditionForm');
                const criteriaInput = $('#inputCriteria');
                const replyInput = $('#inputReply');
                const submitButton = $('#submit');


                function getFormData() {
                    // const formData = form.serializeArray();
                    let formData = new FormData($(form)[0]);

                    formData.append('chat_question_id', questionId);

                    if (_editing) {
                        formData.append('id', _data.id);
                    }

                    return formData;
                }

                function initialState() {
                    criteriaInput.val('').trigger('chosen:updated');
                    if (_editing) {
                        replyInput.val(_data.reply);
                    } else {
                        replyInput.val('');
                    }
                }

                function validateFormData() {

                    if (criteriaInput.val() === null) {
                        triggerAlert('error', "<h5>Criteria required!</h5>");
                        criteriaInput.parent().addClass('has-error');
                        criteriaInput.focus();
                        return false;
                    }

                    if (replyInput.val() === '') {
                        triggerAlert('error', "<h5>Reply Required!</h5>");
                        replyInput.parent().addClass('has-error');
                        replyInput.focus();
                        return false;
                    }

                    return true;
                }
                // Main actions
                initialState()

                // Submit the form
                submitButton.on('click', e => {
                    e.preventDefault();
                    if (validateFormData()) {
                        // compile dataObject
                        const dataObject = getFormData();
                        
                        Swal.fire({
                            type: 'question',
                            customClass: {
                                container: 'custom-swal'
                            },
                            title: 'Save Condition data?',
                            showCancelButton: true,
                            confirmButtonText: 'Submit',
                            showLoaderOnConfirm: true,
                            preConfirm: () => {
                                return $.ajax({
                                    type: 'POST',
                                    url: "/admin/chat-condition/update",
                                    dataType: 'json',
                                    processData: false,
                                    contentType: false,
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                    },
                                    data: dataObject
                                });
                            },
                            allowOutsideClick: () => !Swal.isLoading()
                        }).then((result) => {
                            if (!result.dismiss) {
                                const response = result.value;

                                if (!response.success) {
                                    triggerAlert('error', '<h5>' + response.message + '</h5>');
                                } else {
                                    $('#chat-condition-modal').modal('toggle');
                                    
                                    if (_editing) {
                                        title = "Condition successfully updated";
                                    } else {
                                        title = "Condition successfully added";
                                    }

                                    Swal.fire({
                                        type: 'success',
                                        customClass: {
                                            container: 'custom-swal'
                                        },
                                        title: title,
                                        html: "<h5>Page will be refreshed after you clicked OK</h5>",
                                        onClose: () => {
                                            window.location.reload();
                                        }
                                    });
                                }

                            }
                        });
                    }
                })
            })
    });
</script>

<script type="text/javascript">

    @if (Session::get('laravelValidatorJSON') === null)
      myGlobal.laravelValidator = null;
    @else
      myGlobal.laravelValidator = {{ Session::get('laravelValidatorJSON') }};
    @endif


    $(function()
    {
      myGlobal.baseUrl = "{{ url('') . '/' }}";

      $formInsertTag = $('#formInsertTag');

      $( "#inputSelectTagType" ).change(function() {
        $formInsertTag
          .bootstrapValidator('updateStatus', 'name', 'NOT_VALIDATED')
          .bootstrapValidator('validateField', 'name');
      });

      formStoreTagRules = {
          name: {
              message: 'The name is not valid',
              validators: {
                  notEmpty: {
                      message: 'The name is required and cannot be empty'
                  },
                  stringLength: {
                      min: 3,
                      max: 30,
                      message: 'The name must be more than 3 and less than 30 characters long'
                  },
                  // Send { username: 'its value', type: 'username' } to the back-end
                  remote: {
                      message: 'The name is available',
                      url: myGlobal.baseUrl + 'admin/tag/unique',
                      data: function(validator) {
                          return {
                              type : 'tag.name',
                              fields : {
                                name : validator.getFieldElements('name').val(),
                                type : validator.getFieldElements('type').val()
                              }
                          };
                      },
                  }
              }
          },
          type: {
              validators: {
                  notEmpty: {
                      message: 'The type is required and cannot be empty'
                  },
              }
          }
      };

      formUpdateTagRules = {
          name: {
              message: 'The name is not valid',
              validators: {
                  notEmpty: {
                      message: 'The name is required and cannot be empty'
                  },
                  stringLength: {
                      min: 1,
                      max: 30,
                      message: 'The name must be more than 1 and less than 30 characters long'
                  }
              }
          },
          type: {
              validators: {
                  notEmpty: {
                      message: 'The type is required and cannot be empty'
                  },
              }
          }
      };

      @if (Route::currentRouteName() === 'admin.tag.create')
        myGlobal.bootstrapValidatorDefaults.fields = formStoreTagRules;
      @elseif (Route::currentRouteName() === 'admin.tag.edit')
        myGlobal.bootstrapValidatorDefaults.fields = formUpdateTagRules;
      @endif


      $formInsertTag.bootstrapValidator(myGlobal.bootstrapValidatorDefaults);

      updateValidateStatus($formInsertTag.data('bootstrapValidator'), myGlobal.laravelValidator);
    });
</script>

@stop