@extends('admin.layouts.main')
@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
            <div class="box-tools pull-right">
                <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
            </div>
        </div><!-- /.box-header -->
          <div class="col-md-11">
            <div class="alert alert-success" role="alert">
                <ol>
                    <li><strong>@sNamaKost</strong> : Menampilkan nama kost</li>
                    <li><strong>@sJumlahKamar</strong> : Menampilkan jumlah kamar kost</li>
                    <li><strong>@sHarga</strong> : Menampilkan harga kost</li>
                    <li><strong>@sAlamat</strong> : Menampilkan alamat kost</li>
                    <li><strong>@sPhone</strong> : Menampilkan nomor pemilik kost</li>
                    <li><strong>@sHpManager</strong> : Menampilkan nomor penjaga kost</li>
                    <li><strong>@sUpdatedDate</strong> : Menampilkan tanggal update kos</li>
                    <li><strong>@sRoomDetail</strong> : Menampilkan link detail kos</li>
                    <li><strong>@sRute</strong> : Menampilkan link rute map kos</li>
                    <li><strong>@sPet</strong> : Menampilkan jawaban bisa bawa hewan atau nggak</li>
                    <li><strong>@sPasutri</strong> : Menampilkan jawaban bisa pasutri atau nggak</li>
                    <li><strong>@sCanBooking</strong> : Menampilkan jawaban bisa booking atau nggak</li>
                    <li><strong>@sActivePromo</strong> : Menampilkan promo aktif</li>
                    <li><strong>@sDiscount</strong> : Menampilkan promo aktif</li>
                    <li><strong>@sAmaran</strong> : Disclaimer Apartemen</li>
                    <li><strong>@sAmarankos</strong> : Disclaimer Kos</li>
                </ol>
            </div>
          </div>  
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <a href="{{ URL::route('admin.question.create') }}" class="btn btn-primary">Create</a>
                </div>
            </div>

            <table id="tableListTag" class="table table-striped">
                <thead>
                    <tr>
                        <th width="30px;">No</th>
                        <th>Id</th>
                        <th>Chat for</th>
                        <th>Question</th>
                        <th>Is active</th>
                        <th>Answer</th>
                        <th class="table-action-column">Action</th>

                    </tr>
                </thead>
                <tbody>
                   @foreach ($questions as $rowQuestionKey => $rowquestion)
                    <tr>
                        <td>{{ $rowQuestionKey+1 }}</td>
                        <td>{{ $rowquestion->id }}</td>
                        <td>
                            @if ($rowquestion->for == "house_property")
                                <label class="label label-default">Kontrakan/Villa</label>
                            @elseif ($rowquestion->for == "kos")
                                <label class="label label-info">Kos</label>
                            @elseif ($rowquestion->for == "marketplace")
                                <label class="label label-warning">Marketplace</label>
                            @else
                                <label class="label label-success">Apartment</label>
                            @endif
                        </td>
                        <td>{{ $rowquestion->question }}</td>
                        <td>
                            @if ($rowquestion->status == 'true')
                                <label class="label label-success">Yes</label>
                            @else 
                                <label class="label label-danger">No</label>
                            @endif
                        </td>
                        <td>{{ $rowquestion->answer }}</td>
                        <td>
                            <a href="{{ URL::to('admin/question/status', $rowquestion->id) }}" class="btn btn-xs @if ($rowquestion->status == 'true') btn-danger @else btn-success @endif">
                                @if ($rowquestion->status == 'true') 
                                    <i class="fa fa-arrow-down"></i> Deactivate 
                                @else 
                                    <i class="fa fa-arrow-up"></i> Activate 
                                @endif
                            </a>
                            <a class="btn btn-warning btn-xs" href="{{ URL::route('admin.question.show', $rowquestion->id) }}">
                                <i class="fa fa-pencil"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
              
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
    <!-- table -->

    @include('admin.partials.delete-modal')
@stop
@section('script')
<!-- page script -->
<script type="text/javascript">
    $(function()
    {
        $("#tableListTag").dataTable();
    });
</script>
@stop