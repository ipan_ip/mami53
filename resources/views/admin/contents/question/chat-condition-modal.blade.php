<div class="modal" id="chat-condition-modal" role="dialog" aria-labelledby="modalLabel">
	<div class="modal-dialog" role="modal">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top:2px;"><i class="fa fa-lg fa-times-circle"></i></button>
				<h4 class="modal-title"><span id="modalTitle"></span></h4>
			</div>
			<div class="modal-body">
				<form id="conditionForm" action="#" method="post" enctype="multipart/form-data">
					<div class="row" style="margin-top:10px;">
						<div class="col-md-8">
							<div class="form-group">
								<label for="inputCriteria" class>Criteria</label>
                                <select class="form-control chosen-select" multiple id="inputCriteria" name="criteria_ids[]">
                                    {{-- @foreach($condition->selected_criteria_name as $criteria)
                                        <option value="{{ $criteria->id }}"
                                           {{$criteria->selected}}>{{ $criteria->name }}
                                        </option>
                                     @endforeach --}}
                                    @foreach ($chat_criteria as $criteria)
                                    <option value="{{ $criteria->id }}">
                                        {{ $criteria->name }}
                                    </option>
                                    @endforeach
                                </select>
							</div>
						</div>
					</div>
					<div class="row" style="margin-top:10px;">
						<div class="col-md-8">
							<div class="form-group">
                                <label for="inputReply">Reply</label>
                                <textarea class="form-control" name="reply" id="inputReply" rows="5"
                                          style="width: 100%"></textarea>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<div class="row">
					<div class="col-md-8 text-left">
					</div>
					<div class="col-md-4">
						<button type="button" class="btn btn-primary" id="submit">
							<i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save
						</button>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</div>