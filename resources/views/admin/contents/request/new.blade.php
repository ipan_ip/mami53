@extends('admin.layouts.main')
@section('style')   
    <style>
        .hiddene { display: none; }
        .shower { display: block; }
    </style>
@endsection
@section('content')
<div class="box box-primary">
	<div class="box-header">
		<h3 class="box-title">{{ $boxTitle }}</h3>
	</div><!-- /.box-header -->

	@if ($errors->any())
		<div class="row">
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		</div>
	@endif

	<form method="POST" action="{{ URL::to('admin/request/request/new') }}" class="form-horizontal form-bordered"> 
		<div class="box-body no-padding">

			@if ($for == 'package')
			<!-- select -->
			<div class="form-group bg-default">
				<label for="idPE" class="col-sm-2 control-label">ID PE</label>
				<div class="col-sm-10">
					<input type="number" id="idPE" name="pe_id" class="form-control" placeholder="ID PE" />
				</div>
			</div>
			@endif

			<!-- select -->
			<div class="form-group bg-default">
				<label for="inputSelectType" class="col-sm-2 control-label">No Hp</label>
				<div class="col-sm-10">
					<input type="number" id="phone" name="phone_number" class="form-control" placeholder="No Hp">
				</div>
			</div>

			<input type="hidden" id="fore" name="for" value="{{ $for }}">
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<input type="button" name="nexted" id="nexted" class="btn btn-default" value="Next" />
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-2 control-label">@if ($for == 'package') Package @else Balance @endif</label>
				<div class="col-sm-10">
					<div id="hasil"></div>
				</div>
			</div>

			@if ($for == 'package')
				<div class="form-group" id="oldpackage">
					<label class="col-sm-2 control-label">Old package</label>
					<div class="col-sm-10">
						<select class="form-control" name="oldpackage">
							<option value="0">Select one -</option>
							@foreach ($oldpackage AS $key => $value)
								<option value="{{ $value->id }}">{{ $value->name }}</option>
							@endforeach
						</select>
					</div>
				</div>
			@endif

			<div class="form-group" id="btn-ac">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-primary">Submit</button>
					<a href="{{ URL::route('admin.paket.index') }}">
					<button type="button" class="btn btn-default">Cancel</button>
					</a>
					<button type="reset" class="btn btn-default"
					onclick="$('#formInsertPaket').bootstrapValidator('resetForm', true);">Reset</button>
				</div>
			</div>
		</div>
	</form>
</div><!-- /.box -->
<script type="text/javascript">
  document.getElementById("btn-ac").classList.add('hiddene');
      $("#oldpackage").hide();
      $('#nexted').click(function(e) {
      
      
      phone = $("#phone").val();
      fore = $("#fore").val();
      
      $("#hasil").append("Loading ................ ");
      
      if (fore == 'package') {
          $("#oldpackage").show();
      }

      $.ajax({
        url: '/admin/request/request/package?phone='+phone+'&for='+fore,
        type: 'GET',
        dataType: 'json',
        success: function(data) {
          $("#hasil").empty();  
          if(data.status == false) {
            document.getElementById("btn-ac").classList.add('hiddene');
            if (fore == 'balance') {
                $("#hasil").append("Gagal menampilkan. User belum premium sebelumnya atau masa aktif premium udah habis.");
            } else {
                $("#hasil").append("User tidak ditemukan atau user sudah melakukan upgrade.");
            }
          } else {
            var project = "<select class='form-control' name='package'>";
            var sale;
            $.each(data.packages, function(index, value) {
              if (value.sale_price == "Rp 0") {
                sale = value.price;
              } else {
                sale = value.sale_price;
              }
              project += "<option value='"+value.id+"'>"+value.name+", Harga: "+sale+"</option>";
            });
            project += "</select>";
            document.getElementById("btn-ac").classList.remove('hiddene');
            $("#hasil").append(project);
          }
        }
      });
    });

</script>
@stop