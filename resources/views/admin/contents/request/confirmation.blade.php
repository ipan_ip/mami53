@extends('admin.layouts.main')
@section('content')
<div class="box box-primary">
  <div class="box-header">
      <h3 class="box-title">{{ $boxTitle }}</h3>
  </div><!-- /.box-header -->
  <form method="POST" action="{{ URL::to('admin/request/confirm', $request->id) }}?for={{ $for }}" class="form-horizontal form-bordered">
      <div class="box-body no-padding">

             <div class="form-group bg-default">
              <label for="inputName" class="col-sm-2 control-label">Username</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" placeholder="Name"
                  id="inputName" name="name" value="@if ($for == 'balance') {{ $request->premium_request->user->name }} @else {{ $request->user->name }} @endif" disabled="true">
              </div>
            </div>

            <div class="form-group bg-default">
              <label for="inputName" class="col-sm-2 control-label">Nama Bank</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" placeholder="Name"
                  id="inputName" name="bank_name" value="">
              </div>
            </div>

            <div class="form-group bg-default">
              <label for="inputName" class="col-sm-2 control-label">Atas Nama</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" placeholder="Atas Nama"
                  id="inputName" name="bank_name_account" value="">
              </div>
            </div>

            <div class="form-group bg-default">
              <label for="inputName" class="col-sm-2 control-label">Jumlah</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" placeholder="Jumlah"
                  id="inputName" name="jumlah" value="">
              </div>
            </div>

            <div class="form-group bg-default">
              <label for="inputName" class="col-sm-2 control-label">Transfer Date</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" placeholder=""
                  id="inputName" name="transfer_date" value="{{ date('Y-m-d') }}" placeholder="2017-01-01 / Tahun-Bulan-Tanggal">
              </div>
            </div>

            <div class="form-group bg-default">
              <label for="inputName" class="col-sm-2 control-label">Ke Bank</label>
              <div class="col-sm-10">
                <select name="bank_id" class="form-control">
                  @foreach ($bank AS $key => $value)
                     <option value="{{ $value->id }}">{{ $value->name }}</option> 
                  @endforeach
                </select>
              </div>
            </div>

            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="{{ URL::route('admin.request.index') }}">
                  <button type="button" class="btn btn-default">Cancel</button>
                </a>
                <button type="reset" class="btn btn-default"
                  onclick="$('#formInsertPaket').bootstrapValidator('resetForm', true);">Reset</button>
              </div>
            </div>
      </div>
  </form>
</div><!-- /.box -->
@stop
@section('script')
<!-- page script -->

<!-- Jquery Datepicker -->
<!-- <script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script> -->


<script type="text/javascript">
      $(function()
    {
        $('#select-sort-option').change(function(event){
            var url  = "http://{{ $_SERVER['HTTP_HOST'] }}{{ $_SERVER['REQUEST_URI'] }}" + '?o=' + event.target.value ;

            url += '&st=' + $('#start-date').val();
            url += '&en=' + $('#end-date').val();

            window.location = url;

        });

        $('.datepicker').datepicker({ dateFormat : 'dd-mm-yy'});
    });


    @if (Session::get('laravelValidatorJSON') === null)
      myGlobal.laravelValidator = null;
    @else
      myGlobal.laravelValidator = {{ Session::get('laravelValidatorJSON') }};
    @endif


    $(function()
    {
      myGlobal.baseUrl = "{{ url('') . '/' }}";

      $formInsertPaket = $('#formInsertPaket');

      $( "#inputSelectType" ).change(function() {
        $formInsertPaket
          .bootstrapValidator('updateStatus', 'name', 'NOT_VALIDATED')
          .bootstrapValidator('validateField', 'name');
      });
      

 
      formStoreTagRules = {
          name: {
              message: 'The name is not valid',
              validators: {
                  notEmpty: {
                      message: 'The name is required and cannot be empty'
                  },
                  stringLength: {
                      min: 3,
                      max: 30,
                      message: 'The name must be more than 3 and less than 30 characters long'
                  },
                  // Send { username: 'its value', type: 'username' } to the back-end
                  remote: {
                      message: 'The name is available',
                      url: myGlobal.baseUrl + 'admin/tag/unique',
                      data: function(validator) {
                          return {
                              type : 'tag.name',
                              fields : {
                                name : validator.getFieldElements('name').val(),
                                type : validator.getFieldElements('type').val()
                              }
                          };
                      },
                  }
              }
          },
          type: {
              validators: {
                  notEmpty: {
                      message: 'The type is required and cannot be empty'
                  },
              }
          }
      };

      formUpdateTagRules = {
          name: {
              message: 'The name is not valid',
              validators: {
                  notEmpty: {
                      message: 'The name is required and cannot be empty'
                  },
                  stringLength: {
                      min: 1,
                      max: 30,
                      message: 'The name must be more than 1 and less than 30 characters long'
                  }
              }
          },
          type: {
              validators: {
                  notEmpty: {
                      message: 'The type is required and cannot be empty'
                  },
              }
          }
      };

      @if (Route::currentRouteName() === 'admin.tag.create')
        myGlobal.bootstrapValidatorDefaults.fields = formStoreTagRules;
      @elseif (Route::currentRouteName() === 'admin.tag.edit')
        myGlobal.bootstrapValidatorDefaults.fields = formUpdateTagRules;
      @endif


      $formInsertPaket.bootstrapValidator(myGlobal.bootstrapValidatorDefaults);

      updateValidateStatus($formInsertPaket.data('bootstrapValidator'), myGlobal.laravelValidator);

    });
</script>
@stop