@extends('admin.layouts.main')
@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
            <div class="box-tools pull-right">
                <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">

        <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <form action="" class="form-inline" method="get" style="text-align:right;padding:10px;">
                          <!-- <input type="text" name="q" class="form-control input-sm"  placeholder="No Hp owner"  autocomplete="off" value="{{ request()->input('q') }}">
                         -->
                          {{ Form::select('statuso', $statuso, request()->input('input'), array('class' => 'form-control input-sm')) }}

                        <button class="btn btn-primary btn-sm" id="buttonSearch"><i class="fa fa-search">&nbsp;</i>Search</button>
                    </form>
                </div>
            </div>
  


            <table id="tableListTag" class="table table-striped">
                <thead>
                    <tr>
                        <th width="30px;">No</th>
                        <th>Owner Phone</th>
                        <th>View</th>
                        <th>Price</th>
                        <th>Request</th>
                        <th>Request Status</th>
                        <th>Status</th>
                        <th class="table-action-column">Action</th>
                    </tr>
                </thead>
                <tbody>
                   @foreach ($rowsRequest AS $key => $request)
                       <tr>
                           <td>{{ $key+1 }}</td>
                           <td>{{ $request['owner_phone'] }}</td>
                           <td>{{ $request['views'] }}</td>
                           <td>{{ "IDR " . number_format($request['price'],2,",",".") }}</td>
                           <td>{{ $request['created_at'] }}</td>
                           <td>
                             @if ($request['expired_status'] == 'true')
                                <span class="label label-danger">Expired</span>
                             @elseif ($request['expired_status'] == 'false')
                                <span class="label label-primary">Belum Konfirmasi</span>
                             @else
                                <span class="label label-success">Success</span>
                             @endif
                           </td>
                           <td>
                             @if ($request['status'] == 1)
                               <span class="label label-success">Confirmation</span>
                             @else
                               <span class="label label-default">Waitting ...</span>
                             @endif
                           </td>
                           
                           <td> 
                              
                              @if ($request['check_confirmation'] == false)
                                <a href="{{ URL::to('admin/request/confirmation', $request['id']) }}?for=balance" class="btn-xs btn btn-warning">Confirm</a>
                              @endif

                              @if ($request['status'] == 0 OR $request['expired_status'] == 'true')
                               <a href="{{ URL::to('admin/request/balance', $request['id']) }}" class="btn-xs btn btn-danger">Cancel</a>  
                              @endif
                           <td>
                            
                          </td> 
                       </tr>
                   @endforeach
                </tbody>
                
            </table>

            <div class="box-body no-padding">
            {{ $rowPaginate->appends(Request::except('page'))->links() }}
        </div>

        </div><!-- /.box-body -->
    </div><!-- /.box -->
    <!-- table -->

@stop
@section('script')
<!-- page script -->
<script type="text/javascript">
    $(function()
    {
        $("#tableListTag").dataTable();
    });
</script>
@stop