@extends('admin.layouts.main')
@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
            <div class="box-tools pull-right">
                <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
            </div>
        </div><!-- /.box-header -->

            <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <form action="" class="form-inline" method="get" style="text-align:right;padding:10px;">
                        <a href="{{ URL::to('admin/request/balance') }}" class="btn btn-xs btn-warning pull-left">View Request</a> <a style="margin-left: 10px;" href="{{ URL::to('admin/request/request/new') }}?for=package" class="btn btn-xs btn-default pull-left">Add New Package Request</a> <a style="margin-left: 10px;" href="{{ URL::to('admin/request/request/new') }}?for=balance" class="btn btn-xs btn-success pull-left">Add New Balance Request</a>

                            <input type="number" name="q" class="form-control input-sm"  placeholder="No Hp owner"  autocomplete="off" value="{{ request()->input('q') }}">
                            <input type="text" name="price_total" class="form-control input-sm"  placeholder="Total harga"  autocomplete="off" value="{{ request()->input('price_total') }}">
                            {{ Form::select('statuso', $statuso, request()->input('input'), array('class' => 'form-control input-sm')) }}

                        <button class="btn btn-primary btn-sm" id="buttonSearch"><i class="fa fa-search">&nbsp;</i>Search</button>
                    </form>
                </div>
            </div>
  
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th width="30px;">No</th>
                        <th></th>
                        <th>Hp Owner</th>
                        <th>Name</th>
                        <th>PE</th>
                        <th>Package</th>
                        <th>Total</th>
                        <th>Tanggal Beli</th>
                        <th>Status Request</th>
                        <th>Tanggal Selesai</th>
                        <th>View</th>
                        <th>Used View</th>
                        <th>Teralokasikan</th>
                        <th>Status</th>   
                        <th class="table-action-column">Action</th>
                    </tr>
                </thead>
                <tbody>
                   @foreach ($rowsRequest AS $key => $request)
                       <tr>
                           <td>{{ $key+1 }}</td>
                           <td>
                             @if ($request['status'] == '1')
                                <a href="/admin/request/deleted/{{ $request['id'] }}?type=all" onclick="return confirm('Are you sure you want to delete this item?');"  title="Delete premium request" style="color: #ff0000;"><i class="fa fa-times"></i></a>
                             @endif
                           </td>
                           <td>{{ $request['owner_phone'] }}</td>
                           <td>{{ $request['owner_name'] }}</td>
                           <td>{{ $request['premium_executive_name'] }}</td>
                           <td>@if ($request['package_for'] == 'trial')
                                    <span class="label label-warning">Trial</span>
                                @elseif ($request['package_for'] == 'balance')
                                    <span class="label label-danger">Balance</span>
                                @else 
                                    <span class="label label-success">Package</span>
                                @endif 
                                {{ $request['package_name'] }}
                           </td>
                           <td>{{ $request['total'] }}</td>
                           <td>{{ $request['created_at'] }}</td>
                           <td>@if ($request['expired_status'] == 'true')
                                  <span class="label label-danger">Expired</span>
                               @elseif ($request['expired_status'] == 'false')
                                  <span class="label label-primary">Belum Konfirmasi</span>
                               @else   
                                  <span class="label label-success">Success</span>   
                               @endif   
                           </td>
                           <td>@if ($request['finish_premium'] == 'Expired')
                                  <label class="label label-danger">Expired</label> 
                                @else
                                  {{ $request['finish_premium'] }}
                                @endif
                           </td>
                           <td>{{ $request['view'] }}</td>
                           <td>{{ $request['now_views'] }}</td>
                           <td>{{ $request['allocated_now'] }}</td>
                           <td>
                           @if ( $request['status'] == '1')
                               <span class="label label-success">Confirmation</span>
                           @else
                              <span class="label label-danger">Waiting</span>
                           @endif 
                          </td>
                          
                          <td>
                             @if ($request['expired_status'] == 'true')
                                  <a class="btn btn-xs btn-default" href="{{ URL::to('admin/request/extend', $request['id']) }}">Extend</a>
                             @endif  
                             <a class="btn btn-xs btn-primary" href="{{ URL::to('admin/request/kost', $request['owner_id']) }}">Kost</a> 
                          @if ($request['expired_status'] == 'true')
                               <a class="btn-xs btn btn-warning" href="{{ URL::to('admin/request/deleted', $request['id']) }}">Cancel</a>  
                          @else

                              @if ($request['status'] == '1')
                                  
                              @else
                                  <a class="btn-xs btn btn-warning" href="{{ URL::to('admin/request/deleted', $request['id']) }}">Cancel</a>  
                                  
                                  @if ($request['confirm'] == false)
                                     <a class="btn btn-xs btn-danger" href="{{ URL::to('admin/request/confirmation', $request['id']) }}">Confirm</a> 
                                  @endif 

                              @endif
                          @endif

                          @if ($request['balance_total'] > 0)
                               <a href="{{ URL::to('admin/request/balance') }}?get={{ $request['id'] }}" class="btn btn-xs btn-default">Balance</a> 
                          @endif
                          </td> 

                       </tr>
                   @endforeach
                </tbody>
                
            </table>

            <div class="box-body no-padding">
            {{ $rowPaginate->appends(Request::except('page'))->links() }}
        </div>
        
        </div><!-- /.box-body -->
    </div><!-- /.box -->
    <!-- table -->

@stop
@section('script')
<!-- page script -->
<script type="text/javascript">
    $(function()
    {
        $("#tableListTag").dataTable();
    });
</script>
@stop