@extends('admin.layouts.main')
@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
            <div class="box-tools pull-right">
                <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="row">
                <div class="col-md-6">
                  <a href="/admin/request/kost/{{ $ownerId }}/allocation" class="btn btn-small btn-default" style="margin: 10px;">Fix premium allocation</a>
                </div>
                <div class="col-md-6">
                    @if (!is_null($premium_request))
                    <form class="form-inline" method="POST" action="">
                      <input type="hidden" name="request_id" value="{{ $premium_request->id }}"/>
                        <input type="number" value="{{ $premium_request->view }}" name="view" class="form-control"/>
                        <input type="number" value="{{ $premium_request->used }}" name="used" class="form-control"/>
                        <input type="number" value="{{ $premium_request->allocated }}" name="allocated" class="form-control"/>
                        <button type="submit" class="btn btn-small btn-danger">Update</button>
                    </form>
                    @endif
                </div>
            </div>
            <table id="tableListTag" class="table table-striped">
                <thead>
                    <tr>
                        <th width="30px;">No</th>
                        <th>Name</th>
                        <th>Status Active</th>
                        <th>Status Promote</th>
                        <th>Last Promote</th>
                        <th class="table-action-column">Action</th>
                    </tr>
                </thead>
                <tbody>
                   @foreach ($kost AS $key => $data)
                       <tr>                      
                           <td>{{ $key+1 }}</td>
                           <td>{{ $data->room->name }}</td>
                           <td>
                           @if ($data->room->is_active == 'true')
                               <span class="label label-primary">True</span>
                           @else
                               <span class="label label-warning">False</span>
                           @endif
                           </td>
                           <td>
                             @if ($data->room->is_promoted == 'true')
                               <span class="label label-success">True</span>
                             @else
                               <span class="label label-danger">False</span>
                             @endif
                           </td>
                           <td>
                             @if (count($data->room->view_promote) > 0)
                                {{ $data->room->view_promote[0]->updated_at }}
                             @else
                                -
                             @endif
                           </td>
                           <td>
                               <a class="btn-xs btn btn-success" href="{{ URL::to('admin/room/promotion', $data->designer_id) }}">Promotion</a>
                               <a href="{{ URL::to('admin/request/kost/delete', $data->designer_id) }}?owner_id={{ $ownerId }}" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-xs btn-danger">Delete</a>
                           </td>
                       </tr>
                   @endforeach
                </tbody>
                
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
    <!-- table -->

@stop
@section('script')
<!-- page script -->
<script type="text/javascript">
    $(function()
    {
        $("#tableListTag").dataTable();
    });
</script>
@stop