@extends('admin.layouts.main')
@section('content')

<div class="box box-primary" id="starting">
    <div class="box-header">
        <h3 class="box-title">{{ $boxTitle }}</h3>
    </div>

    {{ Form::open(array('route' => $formAction, 'method' => $formMethod,
      'class' => 'form-horizontal form-bordered', 'role' => 'form', 'id' => 'formInsertBank', 'enctype' => 'multipart/form-data')) }}
        
        {{ csrf_field() }}
        <div class="box-body no-padding">
            <div class="form-group bg-default">
                <label for="inputLandingId" class="col-sm-2 control-label">Owner Name</label>
                <div class="col-sm-10">
                    <input class="form-control" placeholder="Owner Name" name="name" value="{{ $premiumPlusUser->name }}"/>
                </div>
            </div>
            <div class="form-group bg-default">
                <label for="inputLandingId" class="col-sm-2 control-label">Owner Phone</label>
                <div class="col-sm-10">
                    <input class="form-control" placeholder="Owner phone" name="phone_number" value="{{ $premiumPlusUser->phone_number }}"/>
                </div>
            </div>
            <div class="form-group bg-default">
                <label for="inputName" class="col-sm-2 control-label">Owner Email</label>
                <div class="col-sm-10">
                    <input type="email" class="form-control" placeholder="Owner Email" id="inputName" 
                        name="email"  value="{{ $premiumPlusUser->email }}">
                </div>
            </div>
            <div class="form-group bg-default">
                <label for="inputParentId" class="col-sm-2 control-label">Consultant ID</label>
                <div class="col-sm-10">
                    <input class="form-control" placeholder="Consulatnt ID" type="text" name="consultant_id" value="{{ $premiumPlusUser->consultant_id }}" />
                </div>
            </div>
            <div class="form-group bg-default">
                <label for="inputParentId" class="col-sm-2 control-label">Admin Phone</label>
                <div class="col-sm-10">
                    <input class="form-control" placeholder="Admin phone" type="text" name="admin_phone" value="{{ $premiumPlusUser->admin_phone }}" />
                </div>
            </div>
            <div class="form-group bg-default">
                <label for="inputParentId" class="col-sm-2 control-label">Room ID</label>
                <div class="col-sm-10">
                    <input class="form-control" placeholder="Designer id" type="number" name="designer_id" value="{{ $premiumPlusUser->designer_id }}" />
                </div>
            </div>

            <hr />

            <div class="form-group bg-default">
                <label for="inputParentId" class="col-sm-2 control-label">Request date</label>
                <div class="col-sm-10">
                    <input class="datetimepicker form-control" placeholder="Request date" type="text" name="request_date" value="{{ $premiumPlusUser->request_date }}" />
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputParentId" class="col-sm-2 control-label">Activation date</label>
                <div class="col-sm-10">
                    <input class="datetimepicker form-control" 
                                placeholder="Activation date" 
                                type="text" 
                                name="activation_date" 
                                value="{{ $premiumPlusUser->activation_date }}" 
                                @if(isset($premiumPlusUser->invoices) && $premiumPlusUser->invoices->count() > 0) readonly="readonly" @endif
                    />
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputParentId" class="col-sm-2 control-label">Start date</label>
                <div class="col-sm-10">
                    <input class="datetimepicker form-control" id="startDate" placeholder="Start date" type="text" name="start_date" value="{{ $premiumPlusUser->start_date }}" />
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputParentId" class="col-sm-2 control-label">End date</label>
                <div class="col-sm-10">
                    <input class="datetimepicker form-control" id="endDate" placeholder="End date" type="text" name="end_date" value="{{ $premiumPlusUser->end_date }}" />
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputParentId" class="col-sm-2 control-label">Premium Rate (%)</label>
                <div class="col-sm-10">
                    <input class="form-control" id="premiumRate" placeholder="Premium Rate" min="0" type="number" name="premium_rate" value="{{ $premiumPlusUser->premium_rate }}" />
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputParentId" class="col-sm-2 control-label">Static rate</label>
                <div class="col-sm-10">
                    <input class="form-control" id="staticRate" placeholder="Static rate" min="0" type="number" name="static_rate" value="{{ $premiumPlusUser->static_rate }}" />
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputParentId" class="col-sm-2 control-label">Registered room</label>
                <div class="col-sm-10">
                    <input class="form-control" id="registeredRoom" placeholder="Registered room" type="number" min="0" name="registered_room" value="{{ $premiumPlusUser->registered_room }}" />
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputParentId" class="col-sm-2 control-label">Guaranteed room</label>
                <div class="col-sm-10">
                <input class="form-control" id="guaranteedRoom" placeholder="Guaranteed room" min="0" type="number" name="guaranteed_room" value="{{ $premiumPlusUser->guaranteed_room }}" />
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputParentId" class="col-sm-2 control-label">Releasable room</label>
                <div class="col-sm-10">
                    <div id="releasableRoom" style="font-size: 20px;"></div>
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputParentId" class="col-sm-2 control-label">Total monthly premium</label>
                <div class="col-sm-10">
                    <div id="totalMonthlyPremium" style="font-size: 20px;"></div>
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="premiumDetail" class="col-sm-2 control-label">Premium Detail</label>
                <div class="col-sm-10" id="premiumDetail">
                    @if ($premiumPlusUser->invoices()->count() > 0)
                        @foreach($premiumPlusUser->invoices()->get() as $month => $invoices)
                        <div class="form-group bg-default">
                            <label class="col-sm-2 control-label">Month {{ $month + 1 }}</label>
                            <div class="col-sm-10">
                                <input type="number" class="form-control" disabled value="{{ $invoices->amount }}" />
                            </div>
                        </div>
                        @endforeach
                    @endif
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputParentId" class="col-sm-2 control-label">Grace Period</label>
                <div class="col-sm-10">
                    <input class="form-control" placeholder="Grace period" type="number" name="grace_period" value="{{ $premiumPlusUser->grace_period }}" />
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </div>
    {{ Form::close() }}

</div>

@endsection

@section('script')

<link href="{{ asset('assets/vendor/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" >
<script src="{{ asset('assets/vendor/moment/moment.min.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>

<script type="text/javascript">

    var formState = '{{ $formState }}',
        activationDateStr = '{{ $premiumPlusUser->activation_date }}',
        activationDate = new Date(activationDateStr),
        isActiveUser = '{{ $premiumPlusUser->isActive() }}',
        now = new Date();

    $(document).on('change', '#registeredRoom', function() {
        return releasableRoom();
    });

    $(document).on('change', '#guaranteedRoom', function() {
        totalMontlyPremium();
        releasableRoom();
    });

    function releasableRoom() {
        var registeredRoom = parseInt($('#registeredRoom').val());
        var guaranteedRoom = parseInt($('#guaranteedRoom').val());

        $('#releasableRoom').text(registeredRoom - guaranteedRoom);
    }

    $(document).on('change', '#premiumRate', function() {
        return totalMontlyPremium();
    });

    $(document).on('change', '#staticRate', function() {
        return totalMontlyPremium();
    });

    function totalMontlyPremium()
    {
        var premiumRate = parseInt($('#premiumRate').val());
        var staticRate = parseInt($('#staticRate').val());
        var guaranteedRoom = parseInt($('#guaranteedRoom').val());
        var totalMonthly = ((premiumRate/100) * staticRate) * guaranteedRoom;
        var _totalMonthly = Math.floor(totalMonthly);
        $('#totalMonthlyPremium').text(_totalMonthly);

        detailPremium();
    }

    function detailPremium()
    {
        var startDate = new Date($('#startDate').val()), 
            endDate = new Date($('#endDate').val()),
            premiumDetailElm = $("#premiumDetail");

        activePeriod = monthDiff(startDate, endDate);

        if (activePeriod < 0) {
            return;
        }

        $(premiumDetailElm).children().remove();
        if (formState == 'add') {
            detailPremiumAddPage(activePeriod);
        } else {
            detailPremiumEditPage(activePeriod);
        }
    }

    function detailPremiumAddPage(activePeriod)
    {
        var premiumDetailElm = $("#premiumDetail");

        for (var i = 0;i < activePeriod; i++) {
            monthElm = calculateMonthlyPremium(i, activePeriod);
            $(premiumDetailElm).append($(monthElm));
        }
    }

    function detailPremiumEditPage(activePeriod)
    {
        var premiumDetailElm = $("#premiumDetail");

        for (var i = 0;i < activePeriod; i++) {
            monthElm = calculateMonthlyPremium(i, activePeriod);
            $(premiumDetailElm).append($(monthElm));
        }
    }

    function calculateMonthlyPremium(month, activePeriod)
    {
        var premiumRate = parseInt($('#premiumRate').val()),
            staticRate = parseInt($('#staticRate').val()),
            guaranteedRoom = parseInt($('#guaranteedRoom').val()),
            activePeriod = activePeriod - 1,
            _totalMonthly = ((premiumRate/100) * staticRate) * guaranteedRoom,
            totalMonthly = Math.floor(_totalMonthly),
            templateElm = "<div class=\"form-group bg-default\"><label class=\"col-sm-2 control-label\">Month</label><div class=\"col-sm-10\"><input type=\"number\" class=\"form-control\" disabled value=\"000\" /></div></div>",
            currentMonth = new Date(activationDateStr),
            diffCurrentMonth = monthDiff(activationDate, now),
            existingRecord = '{{ $premiumPlusUser->id }}' > 0;

        if (isActiveUser && existingRecord && guaranteedRoom != '{{ $premiumPlusUser->guaranteed_room }}') {
            currentMonth.setMonth(activationDate.getMonth() + month);
            var diffMonths = monthDiff(activationDate, currentMonth);
            if (diffMonths < diffCurrentMonth) {
                if (month == 0) {
                    totalMonthly = '{{ $premiumPlusUser->total_monthly_premium }}' * 2;
                } else {
                    totalMonthly = '{{ $premiumPlusUser->total_monthly_premium }}';
                }
            } else {
                if (month == 0) {
                    totalMonthly = totalMonthly * 2;
                }

                if (month == activePeriod && activePeriod > 0) {
                    totalMonthly = (guaranteedRoom > '{{ $premiumPlusUser->guaranteed_room }}') ? totalMonthly - '{{ $premiumPlusUser->total_monthly_premium }}' : 0;
                }
            }
        } else {
            
            if (month == 0) {
                totalMonthly = totalMonthly * 2;
            } 
    
            if (month == activePeriod && activePeriod > 0) {
                totalMonthly = 0;
            }

        }


        monthElm = templateElm.replace('000', totalMonthly).replace('Month', 'Month ' + (month + 1));

        return $(monthElm)
    }

    function monthDiff(d1, d2) {
        var months;
        months = (d2.getFullYear() - d1.getFullYear()) * 12;
        months -= d1.getMonth();
        months += d2.getMonth();
        return months <= 0 ? 0 : months;
    }

    $('#totalMonthlyPremium').text('{{ $premiumPlusUser->total_monthly_premium }}');
    $('#releasableRoom').text('{{ $premiumPlusUser->releasable_room }}');

    $(function () {
        $('.datetimepicker').datetimepicker({
            inline: false,
            sideBySide: true,
            format: 'Y-MM-DD H:mm:ss'
        });

        $('.datetimepicker').on('dp.change', function(){
            detailPremium();
        });
    });
</script>
@endsection