@extends('admin.layouts.main')

@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <a href="/admin/premium-plus/create" class="btn-add btn btn-primary btn-sm pull-left">
                            <i class="fa fa-plus">&nbsp;</i> Add
                    </a>
                </div>
            </div>

            <table  class="table table-striped">
                <thead>
                    <tr>
                        <th>Owner</th>
                        <th>Request Data</th>
                        <th>Active Date</th>
                        <th>Active Period</th>
                        <th>Monthly Premium</th>
                        <th>Sisa Saldo</th>
                        <th>Alokasi Harian</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($premiumPlusUser as $key => $value)
                        <tr>
                            <td>
                                <strong>Name:</strong> {{ $value->name }} <br/>
                                <strong>Phone:</strong> {{ $value->phone_number }} <br/>
                                <strong>Email:</strong> {{ $value->email }} <br/>
                                <strong>Kost Name:</strong> {{ $value->designer_id }} - @if (isset($value->room)) {{ $value->room->name }} @endif <br/>
                                <strong>Area City:</strong> @if (isset($value->room)) {{ $value->room->area_city }} @endif <br/>
                                <strong>Account Phone:</strong> {{ $value->user->phone_number }} <br/>
                                <strong>Consultant Phone:</strong> {{ $value->consultant->user->phone_number }}
                            </td>
                            <td>
                                {{ $value->request_date }}
                            </td>
                            <td>
                                {{ $value->activation_date }}
                            </td>
                            <td>
                                {{ $value->active_period }} Bulan
                            </td>
                            <td>
                                <strong>Premium Rate:</strong> {{ $value->premium_rate }} %<br/>
                                <strong>Static Rate:</strong> {{ number_format($value->static_rate, 0, '.', '.') }} <br/>
                                <strong>Registered Room:</strong> {{ $value->registered_room }} <br/>
                                <strong>Guaranteed Room:</strong> {{ $value->guaranteed_room }} <br/>
                                <strong>Releasable Room:</strong> {{ $value->releasable_room }} <br/>
                                <strong>Total:</strong> {{ number_format($value->total_monthly_premium, 0, '.', '.') }}
                            </td>
                            <td>

                            </td>
                            <td>
                                
                            </td>
                            <td>
                                <a href="/admin/premium-plus/{{ $value->id }}/edit" class="btn btn-primary btn-xs">Edit</a>
                                <a href="/admin/room/promotion/{{ $value->designer_id }}" class="btn btn-xs btn-warning">Premium</a>
                                <a href="/admin/premium-plus/{{ $value->id }}/invoice" class="btn btn-xs btn-success">Invoice</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                
            </table>
        </div><!-- /.box-body -->
        <div class="box-body no-padding">
            
        </div>
    </div><!-- /.box -->
    <!-- table -->
@endsection