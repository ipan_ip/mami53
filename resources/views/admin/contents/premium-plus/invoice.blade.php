@extends('admin.layouts.main')

@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    
                </div>
            </div>

            <table  class="table table-striped">
                <thead>
                    <tr>
                        <th></th>
                        <th>Status</th>
                        <th>Detail Payment</th>
                        <th>Payment Due Date</th>
                        <th>Due Date</th>
                        <th>Notif Status</th>
                        <th>Invoice Link</th>
                        <th>Invoice Number</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($premiumPlusInvoice as $key => $value)
                        <tr>
                            <td>
                                {{ $value->name }}
                            </td>
                            <td>
                                {{ $value->status }}
                            </td>
                            <td>
                                {{ $value->amount }}
                            </td>
                            <td>
                                {{ $value->due_date }}
                            </td>
                            <td>
                                {{ $value->premium_plus_user->grace_period }} Hari
                            </td>
                            <td>
                                {{ $value->notif_status }}
                            </td>
                            <td>
                                {{ $value->getDeepLinkInvoice() }}
                            </td>
                            <td>
                                {{ $value->invoice_number }}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                
            </table>
        </div><!-- /.box-body -->
        <div class="box-body no-padding">
            
        </div>
    </div><!-- /.box -->
    <!-- table -->
@endsection