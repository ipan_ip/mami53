@extends('admin.layouts.main')

@section('style')
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="/css/admin-consultant.css">

<style>
    span.apartment-color,
    tr.apartment-color td {
        color: #0D47A1;
    }
    tr.kost-active td {
        background-color: #C8E6C9;
    }

    .table>tbody>tr>td {
	vertical-align: middle;
    }

    .font-large {
        font-size: 1.5em;
    }

    .font-semi-large {
        font-size: 1.15em;
    }

    .font-grey {
        color: #9E9E9E;
    }

    .dropdown-menu>li>a:hover {
        color: #333
    }

    .label-grey {
        background-color: #CFD8DC;
    }

    /* Sweetalert */

    .swal-wide {
        width: 850px !important;
        /* font-size: 14px!important; */
    }

    .swal-shown {
        height: auto!important;
    }

    .custom-swal {
        z-index: 10000!important;
    }

    .custom-wide-swal {
        width:850px !important;
    }

    .just-input, .form-inline .form-group, .select2-container {
        margin-left: 10px;
        margin-right: 0;
        margin-bottom: 15px;
        vertical-align: top;
    }
</style>
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
<link rel="stylesheet" href="//cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
@endsection

@section('content')
    <!-- table -->
    <div class="box box-primary">

        <!-- <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
            <div class="box-tools pull-right">
                <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
            </div>
        </div>/.box-header -->

        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <form action="" class="form-inline" method="get" style="text-align: right; padding: 10px; margin-bottom: -15px;">
                        <input type="text" name="q" id="phone" class="form-control input-sm just-input"  placeholder="Nama Kost"  autocomplete="off" value="{{ request()->input('q') }}">
                        @if(Auth::user())
                            <input type="text" name="agent" class="form-control input-sm just-input" placeholder="Agent name"  autocomplete="off" value="{{ request()->input('agent') }}">
                            <input type="text" name="from" class="form-control input-sm just-input datepicker" placeholder="Start Date" value="{{ request()->input('from') }}" id="start-date" autocomplete="off" />
                            <input type="text" name="to" class="form-control input-sm just-input datepicker" placeholder="End Date" value="{{ request()->input('to') }}" id="end-date" autocomplete="off" />
                            {{ Form::select('o', $sortLists, request()->input('o'), array('class' => 'pull-right-sort form-group btn btn-default')) }}
                            {{ Form::select('input-type', $inputBy, request()->input('input-type'), array('class' => 'pull-right-sort form-group btn btn-default')) }}
                            {{ Form::select('active', $soringActive, request()->input('active'), array('class' => 'pull-right-sort form-group btn btn-default')) }}
                            {{ Form::select('room-type', [
                                99 => "Semua",
                                1 => 'Mamirooms Kost',
                                2 => 'Booking Kost',
                                3 => 'Testing Kost',
                                4 => 'Premium Kost',
                                5 => 'Regular Kost',
                                6 => 'Deleted Kost',
                            ], request()->input('room-type'), array('class' => 'pull-right-sort form-group btn btn-default')) }}
                            {{ Form::select('kost-level', $kostLevelDropdown, request('kost-level'), ['class' => 'pull-right-sort form-group btn btn-default']) }}
                            <select name="area_city" id="cities" data-placeholder="Area/city filter" class="form-group btn btn-default">
                            </select>
                        @endif
                        <button class="btn btn-primary btn-sm" id="buttonSearch" style="margin-left: 10px;"><i class="fa fa-search">&nbsp;</i>Search</button>
                    </form>

                </div>
            </div>

            <div class="horizontal-wrapper" style="display:none" id="formStorySearch">
                {{ Form::open(array('url'=>$searchUrl , 'method'=> 'POST')) }}
                    <div class="btn-horizontal-group bg-default row">
                        <div class="col-sm-10">
                            <input type="text" name="search_keyword" class="form-control" />
                        </div>
                        <div class="col-sm-2">
                            <button class="btn btn-sm btn-info">Search</button>
                        </div>
                    </div>
                {{ Form::close() }}
            </div>

            <div class="horizontal-wrapper" style="padding: 0 15px; border-top: 0;">
                <a href="{{ route('admin.kost.create') }}" class="btn btn-success btn-sm">
                    <i class="fa fa-plus">&nbsp;</i>Add Kost
                </a>
                <a href="{{ route( $createRoute ) }}" class="btn btn-success btn-sm">
                    <i class="fa fa-plus">&nbsp;</i>Add Apt
                </a>
            </div>

            @if ($telp_now)
            <div class="row">
                <div class="col-md-12" style="margin-bottom: 20px;">
                    <input type="hidden" id="user_login" value="{{ Auth::user()->email }}">
                    <input type="button" class="col-md-12 btn btn-md btn-danger" id="telp_now" value="Telp Now" />
                </div>
            </div>
            @endif
            <div class="row">
                <div class="col-md-12">
                    <div style="padding: 0 15px; margin-top: 15px;">
                        <div class="alert alert-success alert-dismissable"><i class="fa fa-info-circle fa-2x"></i> <strong>Properti yang aktif ditandai dengan background HIJAU</strong></div>
                    </div>
                </div>
            </div>

            <!-- The table is going here -->
            @include('admin.contents.stories.partial.index-table-admin')
            <!-- End of index table -->

        </div><!-- /.box-body -->
        <div class="box-body no-padding">
            {{ $rowsDesigner->appends(Request::except('page'))->links() }}
        </div>
    </div><!-- /.box -->
    <!-- table -->

    @include('admin.partials.delete-modal')
@stop

@section('script')
<script src="//cdn.jsdelivr.net/npm/holderjs@2.9.6/holder.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script src="//cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

<!-- page script -->
<script type="text/javascript">

    function buildTableForPriceFilter(data) {
        var content = `
        <table id="table" class="table" border=1 style="font-size:16px">
            <tr>
                <th style="text-align: center;">Data</th>
                <th style="text-align: center;">Value</th>
            </tr>
            <tr>
                <td>final_price_daily</td>
                <td>${numberWithCommas(data.final_price_daily)}</td>
            </tr>
            <tr>
                <td>final_price_monthly</td>
                <td>${numberWithCommas(data.final_price_monthly)}</td>
            </tr>
            <tr>
                <td>final_price_quarterly</td>
                <td>${numberWithCommas(data.final_price_quarterly)}</td>
            </tr>
            <tr>
                <td>final_price_semiannually</td>
                <td>${numberWithCommas(data.final_price_semiannually)}</td>
            </tr>
            <tr>
                <td>final_price_weekly</td>
                <td>${numberWithCommas(data.final_price_weekly)}</td>
            </tr>
            <tr>
                <td>final_price_yearly</td>
                <td>${numberWithCommas(data.final_price_yearly)}</td>
            </tr>
            <tr>
                <td>created_at</td>
                <td>${data.created_at}</td>
            </tr>
            <tr>
                <td>updated_at</td>
                <td>${data.updated_at}</td>
            </tr>

        </table>`;

        return content;
    }

    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    function bulkVerifyKost(toVerify) {
        if (toVerify.length) {
            return axios.post(
                '/admin/room/bulk-verify',
                {
                    'to_verify': toVerify
                }
            );
        }

        return new Promise((resolve) => { resolve(true); });
    }

    function bulkJustVerifyKost(toVerify) {
        if (toVerify.length) {
            return axios.post(
                '/admin/room/bulk-just-verify',
                {
                    'to_verify': toVerify
                }
            );
        }

        return new Promise((resolve) => { resolve(true); });
    }

    function bulkVerifySlug(toVerify) {
        if (toVerify.length) {
            return axios.post(
                '/admin/room/bulk-verify-slug',
                {
                    'to_verify': toVerify
                }
            );
        }

        return new Promise((resolve) => { resolve(true); });
    }

    function addOnClick() {
        $('#selectAllVerify').click(function () {
            $.each($('.verify-duplicate'), function (index, element) {
                element.checked = !element.checked;
            });
        });

        $('#selectAllJustVerify').click(function () {
            $.each($('.justverify-duplicate'), function (index, element) {
                element.checked = !element.checked;
            });
        });

        $('#selectAllVerifySlug').click(function () {
            $.each($('.verify-slug'), function (index, element) {
                element.checked = !element.checked;
            });
        });
    }

    function verifyDuplicatedKostModal(duplicates) {
        let html = '';

        for (duplicate of duplicates) {
            html = html + (
                `<li>
                    <strong>${duplicate.name}</strong>
                    <label>
                        <input type="radio" class="verify-duplicate" name="verify-${duplicate.id}" value="${duplicate.id}"> Verify Listing
                    </label>
                    <label>
                        <input type="radio" class="justverify-duplicate" name="verify-${duplicate.id}" value="${duplicate.id}"> Just Verify Listing
                    </label>
                    <label>
                        <input type="radio" name="verify-${duplicate.id}" value="${duplicate.id}"> Don't Verify Listing
                    </label>
                    <label>
                        <input type="checkbox" class="verify-slug" name="verify-slug-${duplicate.id}" value="${duplicate.id}"> Verify Slug
                    </label>
                </li>`
            );
        }

        Swal.fire({
            title: 'Verify Duplicated Kosts',
            html: (
                '<ul>' +
                    html +
                '</ul><button id="selectAllVerify"><i class="fa fa-check-square"></i> Verify All</button><button id="selectAllJustVerify"><i class="fa fa-check-square"></i> Just Verify All</button><button id="selectAllVerifySlug"><i class="fa fa-check-square"></i> Verify All Slug</button>'
            ),
            showCloseButton: true,
            focusConfirm: true,
            confirmButtonText: 'Verify',
            confirmButtonAriaLabel: 'Verify duplicated kost',
            onOpen: addOnClick,
        }).then(result => {
            if (result.value) {
                let toVerify = [];
                $.each($('.verify-duplicate'), function (index, element) {
                    if (element.checked) {
                        toVerify.push(element.value);
                    }
                });

                let toJustVerify = [];
                $.each($('.justverify-duplicate'), function (index, element) {
                    if (element.checked) {
                        toJustVerify.push(element.value);
                    }
                });

                let toVerifySlug = [];
                $.each($('.verify-slug'), function (index, element) {
                    if (element.checked) {
                        toVerifySlug.push(element.value);
                    }
                });

                bulkVerifyKost(toVerify)
                    .then(res => {
                        bulkJustVerifyKost(toJustVerify);
                    })
                    .then(res => {
                        bulkVerifySlug(toVerifySlug);
                    })
                    .then((res) => {
                        Swal.fire({
                            icon: 'success',
                            title: 'Success!',
                            text: 'Successfully created duplicate kost',
                        });

                        location.reload();
                    });
            }
        });
    }

    function editDuplicatedKostModal(duplicates)
    {
        let html = '';

        for (duplicate of duplicates) {
            html = html + (
                `<li>
                    <strong>${duplicate.name}</strong>
                    <a href="/admin/room/${duplicate.id}/edit" target="_blank"><i class="fa fa-edit"></i> Edit Kost/Apt</a>
                </li>`
            );
        }

        Swal.fire({
            title: 'Edit Duplicated Kosts',
            html: ('<ul>' + html + '</ul>'),
            showCloseButton: true,
            focusConfirm: true,
            confirmButtonText: 'Continue',
            confirmButtonAriaLabel: 'Continue to duplicate kost activation',
        }).then(result => {
            if (result.value) {
                verifyDuplicatedKostModal(duplicates);
            }
        });
    }

    function duplicateKost(id, withPhoto = false)
    {
        let title = null;
        if (withPhoto) {
            title = 'Create Duplicate With Photo';
        }
        else {
            title = 'Create Duplicate';
        }

        Swal.fire({
            title: title,
            input: 'number',
            inputAttributes: {
                autocapitalize: 'off',
                min: 0,
                oninput: "validity.valid||(value=value.replace(/\D+/g, ''))"
            },
            inputValidator: (value) => {
                if (value > 5) {
                    return 'Could not create more than 5 duplicate';
                } else if (value < 0) {
                    return 'Invalid input';
                }
            },
            showCancelButton: true,
            confirmButtonText: 'Create',
            showLoaderOnConfirm: true,
            preConfirm: (duplicateCount) => {
                return axios.post(
                    `/admin/room/duplicate/${id}`,
                    {
                        'duplicate_count': duplicateCount,
                        'is_with_photo': withPhoto
                    }
                )
                    .then(response => {
                        if (!response.status) {
                            throw new Error(response.data.message);
                        }

                        return response.data;
                    })
                    .catch(error => {
                        Swal.showValidationMessage(`Request failed: ${error}`);
                    });
            },
            allowOutsideClick: () => !Swal.isLoading()
        }).then((result) => {
            if (result.value.status) {
                editDuplicatedKostModal(result.value.duplicates);
            }
        });
    }

    function triggerLoading(message) {
        Swal.fire({
            title: message,
            customClass: {
                container: 'custom-swal'
            },
            allowEscapeKey: false,
            allowOutsideClick: false,
            onOpen: () => {
                swal.showLoading();
            }
        });
    }

    function recalculateScore(id) {
        Swal.fire({
            title: 'Recalculate sort score',
            html: "<h5>This action will recalculate sort score of this room.<br/>Are you sure to continue?</h5>",
            type: 'warning',
            confirmButtonColor: '#f56954',
            showCancelButton: true,
            confirmButtonText: 'Yes, continue!',
            preConfirm: (val) => {
                triggerLoading('Processing....');

                return $.ajax({
                    type: 'GET',
                    url: "/admin/room/recalculate/"+id,
                    dataType: 'json',
                    headers: { 'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr("content") },
                    done: () => { if (Swal.isLoading) Swal.close(); },
                    success: (response) => { return response; },
                    error: (error) => {
                        triggerAlert('error', '<h5>' + error.responseJSON.meta.message + '</h5>');
                    }
                });
            },
            allowOutsideClick: () => !Swal.isLoading()
        }).then((result) => {
            var res = result.value;
            if (res.status === true) {
                Swal.fire({
                    type: 'success',
                    title: "Recalculation is success",
                    text: res.name + "has New score " + res.sort_score + " tracking " + res.tracking,
                });
            } else {
                triggerAlert('error', res.meta.message);
            }
        });
    }

    $(function(){
        var token = '{{ csrf_token() }}';

        // cities select2 ---------------------------------------------
        var cityDatas = {!! json_encode($areaCities) !!};

        var selectCities = $("#cities").select2({
            allowClear: true,
            width: 200,
            language: {
                noResults: function(ada){
                    return "No Results Found";
                }
            },
            data: cityDatas
        });

        const preselected = '{{ request()->input("area_city") }}';

        $("#cities").val(preselected);
        $("#cities").trigger('change');

        Holder.addTheme("thumbnail", {
            background: '#EEE',
            foreground: '#AAA',
            size: 10,
            fontweight: 'normal'
          });

        $('#select-sort-option').change(function(event){
            var url  = '{{ URL::route("admin.room.index") }}' + '?o=' + event.target.value ;

            url += '&st=' + $('#start-date').val();
            url += '&en=' + $('#end-date').val();

            window.location = url;

        });

        $('.datepicker').datepicker({ dateFormat : 'dd-mm-yy'});

        $('.btn-delete').click(function() {
            var id = $(this).data('id');
            var name = $(this).data('name');

            var confirm = window.confirm("Are you sure want to delete " + name +" ?");

            if (confirm) {
                $.post('/{{Request::segment(1)}}/{{Request::segment(2)}}/' + id, {
                    _method: "delete",
                    _token: token,
                }).success(function(response) {
                    console.log(response);
                    window.location.reload();
                });
            }


        });

        $('#telp_now').click(function(e) {
            phone = $("#phone").val();
            email = $("#user_login").val();

            //$("#loading-phone").append("Loading ................ ");
            $.ajax({
                url: '/admin/squarepants/notify?phone='+phone+"&email="+email+"&id=&kos_name=&search=search",
                type: 'GET',
                dataType: 'json',
                success: function(data) {
                // console.log(data)
                //$("#loading-phone").empty();
                if(data.status == false) {
                    alert("Gagal ngirim nomer ke hp");
                    //$("#loading-phone").append("<div class='alert alert-danger' style='width: 100%;'>Nomer hp tidak sesuai format.</div>");
                } else {
                    alert("Berhasil ngirim nomer ke hp");
                    //$("#loading-phone").append("<div class='alert alert-success' style='width: 100%;'>Cek hp anda.");
                }
                }
            });
        });

        $('.btn-action').on('click', (e) => {
            e.preventDefault();
            var _action = $(e.currentTarget).data('action');
            var _id = $(e.currentTarget).data('id');

            if (_action == 'activate') {
                var title = 'Aktifkan Foto Premium?';
                var subTitle = '';
                var type = 'question';
                var status = 1;
            } else {
                var title = 'Non Aktifkan Foto Premium?';
                var subTitle = 'Foto kost akan kembali menggunakan foto dengan watermark';
                var type = 'warning';
                var status = 0;
            }

            Swal.fire({
                    title: title,
                    text: subTitle,
                    type: type,
                    confirmButtonText: 'Yes',
                    showLoaderOnConfirm: true,
                    preConfirm: () => {
                        return $.ajax({
                            type: 'POST',
                            url: "/admin/card/premium/activate",
                            dataType: 'json',
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            data: {
                                'id': _id,
                                'status': status,
                                '_token': token
                            },
                            success: (response) => {
                                if (!response.success) {
                                    Swal.insertQueueStep({
                                        type: 'error',
                                        title: response.message
                                    });
                                    return;
                                }
                            },
                            error: (error) => {
                                Swal.insertQueueStep({
                                    type: 'error',
                                    title: 'Error: ' + error.message
                                });
                            }
                        });
                    },
                    allowOutsideClick: () => !Swal.isLoading()
                }).then((result) => {
                    if (result.value.success) {
                        Swal.fire({
                            type: 'success',
                            title: result.value.message,
                            text: "Halaman akan di-refresh setelah Anda klik OK",
                            onClose: () => {
                                window.location.reload();
                            }
                        });
                    } else {
                        Swal.fire({
                            type: 'error',
                            title: result.value.message,
                            onClose: () => {
                                window.location.reload();
                            }
                        });
                    }
                })
        })

        $('.view-source').on('click', (e) => {
            e.preventDefault();
            var data = $(e.currentTarget).data('room');
            var swal_content = "<div class='text-left'><pre>" + data + "</pre></div>";
                Swal.fire({
                    title: 'Data Source',
                    customClass: 'custom-wide-swal',
                    html: swal_content,
                    showCloseButton: true,
                    confirmButtonText: 'CLOSE'
                });
        })

        $('.view-price-filter').on('click', (e) => {
            e.preventDefault();
            triggerLoading('Loading....');

            var id = $(e.currentTarget).data('id');
            var getPriceFilterUrl = '/admin/room/price-filter/' + id;

            $.ajax({
                url: getPriceFilterUrl,
                type: 'GET',
                dataType: 'json',
                success: function(response) {
                    if(response.status == false) {
                        return alert("Gagal melakukan loading data");
                    }

                    var priceFilter = response.data;
                    var swalContent = '';

                    if (!priceFilter) swalContent = 'No available data';
                    else swalContent = buildTableForPriceFilter(priceFilter);

                    Swal.fire({
                        title: 'Data Filter Price',
                        customClass: 'custom-wide-swal',
                        html: swalContent,
                        showCloseButton: true,
                        confirmButtonText: 'CLOSE'
                    });
                }
            });
        })
    });
</script>
@stop
