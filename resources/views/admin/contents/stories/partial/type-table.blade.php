<table id="tableListDesigner" class="table">
    <thead>
        <tr>
            <th class="text-center" style="padding-left:10px;">ID</th>
            <th width="150px"></th>
            <th class="text-center">Type Name</th>
            <th class="text-center">Room Units</th>
            <th class="text-center">Room Size</th>
            <th class="text-center">Max. Occupancy</th>
            <th class="text-center">Tenant Type</th>
            <th class="text-center">Created</th>
            <th class="text-center">Updated</th>
            <th class="table-action-column" width="10%" style="padding-right:10px;"></th>
        </tr>
    </thead>
    <tbody>
        @foreach ($rowsRoom as $rowDesignerKey => $rowDesigner)
            @if ($rowDesigner['caution'])
                @foreach ($rowDesigner['caution_messages'] as $msg)
                    <tr>
                        <td class="callout callout-danger col-md-12 text-danger" colspan="12"><i class="fa fa-exclamation-triangle"></i> {!! $msg !!}</td>
                    </tr>
                @endforeach
                <tr class="callout callout-danger">
            @else
                <tr style="border-top:none!important;">
            @endif
            <td class="text-center" style="padding-left:10px;">
                {{ $rowDesigner['id'] }}
            </td>
            <td class="text-center">
                <a href="{{ $rowDesigner['photo_url_large'] }}" data-fancybox data-caption="{{ $rowDesigner['name'] }}">
                    <img width="150" class="img-thumbnail" src="{{ $rowDesigner['photo_url'] }}"
                        alt="{{ $rowDesigner['name'] }}" data-src="holder.js/150x100?text=Foto \n Tidak \n Ditemukan">
                </a>
            </td>
            <td class="text-center" style="font-size:14px">
                <strong>{{ $rowDesigner['name'] }}</strong>
                <br />
                @if ($rowDesigner['is_available'])
                    <span class="label label-sm label-success" data-toggle="tooltip" title="Available"><i class="fa fa-check-circle"></i> Available</span>
                @else
                    <span class="label label-sm label-danger" data-toggle="tooltip" title="Not Available"><i class="fa fa-times-circle"></i> Unavailable</span>
                @endif
            </td>
            <td class="text-center" >
                {{ $rowDesigner['total_room'] }}
            </td>
            <td class="text-center" >
                {{ $rowDesigner['room_size'] }}
            </td>
            <td class="text-center">
                {{ $rowDesigner['maximum_occupancy'] }}
            </td>
            <td class="text-center" >
                @if (count($rowDesigner['tenant_type']) > 0)
                    @foreach ($rowDesigner['tenant_type'] as $index => $tenantType)
                        <span class="label label-primary">
                            @if ((int) $tenantType === 1)
                                <i class="fa fa-briefcase"></i> Karyawan
                            @elseif ((int) $tenantType === 2)
                                <i class="fa fa-address-card"></i> Suami Istri
                            @else
                                <i class="fa fa-graduation-cap"></i> Mahasiswa
                            @endif
                        </span>
                        @if ($index < count($rowDesigner['tenant_type']) - 1)
                            <div style="margin-bottom:5px;"></div>
                        @endif
                    @endforeach
                @endif
            </td>
            <td class="text-center" >
                {{ $rowDesigner['created_date'] }}
                <br/>{{ $rowDesigner['created_time'] }}
            </td>
            <td class="text-center" >
                {{ $rowDesigner['updated_date'] }}
                <br/>{{ $rowDesigner['updated_time'] }}
            </td>
            <td class="table-action-column" style="padding-right:10px;">
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-cog"></i> Actions <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu pull-right">
                        @if ($rowDesigner['has_room_units'])
                            <li><a href="{{ route('admin.room.type.facility.edit', $rowDesigner['id']) }}" title="Edit Facilities">
                                <i class="fa fa-eye"></i> View Room Units</a></li>
                            <li role="presentation" class="divider"></li>
                        @endif
                        <li><a href="{{ route('admin.room.type.facility.setting', $rowDesigner['id']) }}" title="Setup Facilities">
                                <i class="fa fa-wrench"></i> Facility Setting</a></li>
                        @if ($rowDesigner['is_facility_set'] == true)
                            @if ($rowDesigner['facility_count'] > 0)
                                <li><a href="{{ route('admin.room.type.facility.edit', $rowDesigner['id']) }}" title="Edit Facilities">
                                    <i class="fa fa-bath"></i> Edit Facility Photos</a></li>
                            @else
                                <li><a href="{{ route('admin.room.type.facility.add', $rowDesigner['id']) }}" title="Add Facilities">
                                    <i class="fa fa-bath"></i> Add Facility Photos</a></li>
                            @endif
                        @endif
                    </ul>
                </div>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
