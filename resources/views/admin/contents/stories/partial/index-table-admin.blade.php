<table id="tableListDesigner" class="table">
    <thead>
        <tr>
            <th></th>
            <th width="150px"></th>
            <th>Nama Kost / Apartemen</th>
            <th>Kost Type / Kost Level</th>
            <!-- <th class="text-center">Status</th> -->
            <th class="text-center">Price (IDR)</th>
            <th class="text-center">Available Room</th>
            <th class="text-center" width="5%">Owner Phone</th>
            <th class="text-center" width="5%">Manager Phone</th>
            <th class="text-center">Inputter</th>
            <th class="text-center">Agent</th>
            <th class="text-center" width="10%">Created / Updated</th>
            <th class="table-action-column" width="10%"></th>
        </tr>
    </thead>
    <tbody>
        @foreach ($rowsRoom as $rowDesignerKey => $rowDesigner)
            {{-- no cover photo alert --}}
            @if ($rowDesigner['caution'])
                @foreach ($rowDesigner['caution_messages'] as $msg)
                <tr>
                    <td class="callout callout-danger col-md-12 text-danger" colspan="12"><i class="fa fa-exclamation-triangle"></i> {!! $msg !!}</td>
                </tr>
                @endforeach
            <tr class="callout callout-danger">
            @else
            <tr class="{{ $rowDesigner['is_active'] === 'true' && $rowDesigner['expired_phone'] != 1 ? 'kost-active' : '' }}" style="border-top:none!important;">
            @endif
            <td class="text-center" style="padding-left:10px;">
                @if (!is_null($rowDesigner['apartment_project_id']))
                <i class="fa fa-building fa-2x {{ $rowDesigner['is_active']=='true' ? 'text-success' : '' }}"
                    data-toggle="tooltip" title="Apartemen"></i>
                @else
                <i class="fa fa-home fa-2x {{ $rowDesigner['is_active']=='true' ? 'text-success' : '' }}"
                    data-toggle="tooltip" title="Kost"></i>
                @endif
            </td>
            <td class="text-center">
                <a href="{{ $rowDesigner['photo_url_large'] }}" data-fancybox data-caption="{{ $rowDesigner['name'] }}">
                    <img width="150" class="img-thumbnail" src="{{ $rowDesigner['photo_url'] }}"
                        alt="{{ $rowDesigner['name'] }}" data-src="holder.js/150x100?text=Foto \n Tidak \n Ditemukan">
                </a>
            </td>
            <td style="font-size:14px">
                <small>{{ $rowDesigner['id'] }}</small>
                <br /><strong>
                    @if(!is_null($rowDesigner['duplicate_from']))
                    <i class="fa fa-retweet" style="color: #ff0000;"></i>
                    @endif
                    {{ $rowDesigner['name'] }} {{ $rowDesigner['unique_code'] }}
                </strong>
                <br />{{ $rowDesigner['area_city'] }}
                <br />
                @if ($rowDesigner['verified_by_mamikos'])
                <span class="label label-sm label-primary" data-toggle="tooltip" title="Verified by Mamikos"><i
                        class="fa fa-check-circle"></i> MamiVerify</span>
                @endif
                @if ($rowDesigner['by_mamichecker'] == true)
                <span class="label label-sm label-primary" data-toggle="tooltip" title="Video Tour by Mamichecker"><i
                        class="fa fa-video-camera"></i> Video Tour</span>
                @endif
                @if ($rowDesigner['promoted'] == true)
                <span class="label label-sm label-warning" data-toggle="tooltip" title="Promoted"><i
                        class="fa fa-arrow-up"></i> Promoted</span>
                @endif
                @if ($rowDesigner['is_booking'] == 1)
                <span class="label label-sm label-success" data-toggle="tooltip" title="Bisa Booking"><i
                        class="fa fa-bookmark"></i> Booking</span>
                @endif
                @if (isset($rowDesigner['price_monthly']['discount_price']))
                <span class="label label-sm label-primary" data-toggle="tooltip" title="Ada Discount Aktif"><i
                        class="fa fa-percent"></i> Discount</span>
                @endif
                @if ($rowDesigner['is_premium_photo'] == 1)
                <span class="label label-sm label-info" data-toggle="tooltip" title="Premium Photos"><i
                        class="fa fa-camera"></i> Foto Booking</span>
                @endif
                @if ($rowDesigner['facility_count'] > 0)
                <span class="label label-sm label-warning" data-toggle="tooltip" title="Facility Photos"><i
                    class="fa fa-bath"></i> Facility Photos</span>
                @endif
                @if ($rowDesigner['expired_phone'] == 1)
                    <br/>
                    <span class="label label-sm label-danger" data-toggle="tooltip" title="Phone Expired! Please update owner phone"><i
                        class="fa fa-exclamation-triangle"></i> Phone Expired!</span>
                @endif
            </td>

            <td>
                @if ($rowDesigner['has_room_types'])
                    <span class="badge progress-bar-primary">Room Types</span><br><br>
                @endif

                @if ($rowDesigner['is_booking'])
                    <span class="badge progress-bar-primary">Bisa Booking</span><br><br>
                @endif

                @if ($rowDesigner['is_mamirooms'])
                    <span class="badge progress-bar-success">Mamirooms</span><br><br>
                @endif

                @if ($rowDesigner['is_testing'])
                    <span class="badge progress-bar-danger">Testing</span><br><br>
                @endif

                @if ($rowDesigner['is_premium'])
                    <span class="badge progress-bar-warning">Premium</span><br><br>
                @endif

                @if ($rowDesigner['deleted_at'])
                    <span class="badge progress-bar-secondary">Deleted</span><br><br>
                @endif

                @if (!$rowDesigner['is_booking'] && !$rowDesigner['is_mamirooms'] && !$rowDesigner['is_testing'] && !$rowDesigner['is_premium'])
                    <span>Regular Kost</span><br>
                @endif

                @if (array_get($rowDesigner, 'level'))
                    <span class="label label-default">{{array_get($rowDesigner, 'level.name')}}</span>
                @endif
            </td>

            {{-- <td align="center">{{ $rowDesigner['status'] }} </td> --}}
            <td align="center" class="font-semi-large">
                @if (!isset($rowDesigner['price_monthly']['discount']))
                <span>{{ $rowDesigner['price_monthly']['price'] }}</span>
                @else
                <span style="text-decoration: line-through !important;">{{ $rowDesigner['price_monthly']['price'] }}</span><br />
                <span>{{ $rowDesigner['price_monthly']['discount_price'] }}</span>
                @endif
            </td>
            <td align="center"><span class="font-grey font-large">{{ $rowDesigner['room_available'] }}</span></td>
            <td align="center">
                <strong>{{ $rowDesigner['owner_phone'] }}</strong>
                <br /><a href="{{ URL::to('admin/owner/change', $rowDesigner['id']) }}" class="btn btn-xs btn-danger"><i
                        class="fa fa-user-circle"></i> Change Owner</a>
                @if ($rowDesigner['is_premium_owner'])
                    <br /><span class="label label-sm label-warning">Premium Owner</span>@endif
                @if (!is_null($rowDesigner['last_update_from']))<br /><small>Updated from:
                    <strong>{{ $rowDesigner['last_update_from'] }}</strong></small>@endif
            </td>
            <td align="center"><strong>{{ $rowDesigner['manager_phone'] }}</strong></td>
            <td align="center" style="padding-right:10px;">
                @if (in_array($rowDesigner['agent_status'], ['Agen', 'Lainnya', 'scrap', 'Anak Kos', 'Anak Kost']))
                <label class="label label-primary">{{ ucwords($rowDesigner['agent_status']) }}</label>
                @if ($rowDesigner['check_owner'] == 0)
                <br />
                @if ($rowDesigner['is_active'] == 'false')
                <a href="" title="Kosnya aktifin dl gaes" style="margin-top: 10px;" class="btn btn-xs btn-default"
                    disabled="true">Create Account</a>
                @else
                <a href="{{ URL::to('admin/room/create/owner', $rowDesigner['id']) }}" class="btn btn-xs btn-success"
                    style="margin-top: 10px;"><i class="fa fa-pencil-square-o"></i> Create Account</a>
                @endif
                @endif
                @else
                <span class="label label-default">
                    @if (!empty($rowDesigner['agent_status']))
                    {{ $rowDesigner['agent_status'] }}
                    @else
                    -
                    @endif
                </span>
                @endif
                @if (!is_null($rowDesigner['photo_by_url']))
                <br /><small><a href="{{ URL::to('uploads/cache/data/checkin/'.$rowDesigner['photo_by_url']) }}"
                        target="_blank">View agent photo</a></small>
                @endif
            </td>
            <td align="center">
                {{ $rowDesigner['agent_name'] }}
                @if(!is_null($rowDesigner['verificator']))
                <br />
                <span class="label label-warning" data-toggle="tooltip" title="Verifikator">
                    {{ $rowDesigner['verificator'] }}
                </span>
                @endif
            </td>
            <td align="center">
                {{ date('j F Y', strtotime($rowDesigner['created_at'])) }}
                /<br />{{ date('j F Y', strtotime($rowDesigner['updated_at'])) }}
            </td>
            <td class="table-action-column">
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-cog"></i> <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu pull-right">
                        @if (\Entrust::hasRole('content-editor'))
                        <li><a href="{{ route('admin.room.edit',$rowDesigner['id']) }}" title="Edit Stories"><i
                                    class="fa fa-pencil"></i> Edit Stories</a></li>
                        @if($rowDesigner['is_active'])
                        @if(is_null($rowDesigner['apartment_project_id']))
                        <li><a href="{{ env("APP_URL") }}/room/{{ $rowDesigner['slug'] }}" title="Preview"
                                target="_blank" rel="noopener">
                                <i class="fa fa-eye"></i> Preview
                            </a></li>
                        @else
                        <a href="{{ env("APP_URL") }}/unit/{{ $rowDesigner['apartment_project_slug'] }}/{{ $rowDesigner['slug'] }}"
                            title="Preview" target="_blank" rel="noopener">
                            <i class="fa fa-eye"></i> Preview
                        </a>
                        @endif
                        @endif
                        @else

                        <li class="dropdown-header">Verification</li>
                        @if ($rowDesigner['is_active'] =='false')
                        <li><a href="{{ $rowDesigner['url_verify'] }}" title="Verify" class="text-success">
                                <i class="fa fa-arrow-up"></i> Verify</a></li>
                        @else
                        <li><a href="{{ $rowDesigner['url_verify'] }}" title="Unverify" class="text-danger">
                                <i class="fa fa-arrow-down"></i> Unverify</a></li>
                        @endif

                        @if ($rowDesigner['is_active'] == 'false')
                        <li><a href="{{ $rowDesigner['justverify'] }}" title="Just Verify">
                                        <i class="fa fa-level-up"></i> Just Verify</a></li>
                        @endif

                        <li><a href="{{ url('admin/room/verifytanpasms', $rowDesigner['id']) }}"
                                title="Verify Tanpa SMS"><i class="fa fa-bell-slash-o"></i> Verify Tanpa SMS</a></li>
                        <li><a href="{{ url('admin/room/survey', $rowDesigner['id']) }}" title="Survey"><i
                                    class="fa fa-comments"></i> Survey</a></li>
                        <li><a href="{{ url('admin/room/verifyslug', $rowDesigner['id']) }}" title="Verify Slug"><i
                                    class="fa fa-chain"></i> Verify Slug</a></li>

                        @if ($rowDesigner['is_active'] == 'true')
                        @if ($rowDesigner['verified_by_mamikos'] == 0)
                        <li><a href="{{ $rowDesigner['url_verified_by_mamikos'] }}" title="Verify By Mamikos">
                                <i class="fa fa-bookmark"></i> Set as Verified By Mamikos</a></li>
                        @else
                        <li><a href="{{ $rowDesigner['url_verified_by_mamikos'] }}" title="Unverify By Mamikos">
                                <i class="fa fa-bookmark-o"></i> Set as Unverified By Mamikos</a></li>
                        @endif
                        @endif

                        @if ($rowDesigner['has_room_types'])
                            <li role="presentation" class="divider"></li>
                            <li class="dropdown-header">Room Data</li>
                            <li>
                                <a href="{{ route('admin.room.type.index',$rowDesigner['id']) }}" title="View Room Types">
                                <i class="fa fa-bed"></i> View Room Types</a>
                            </li>
                        @endif

                        <li role="presentation" class="divider"></li>
                        <li class="dropdown-header">Update Data</li>

                        @if(is_null($rowDesigner['apartment_project_id']))
                        <li><a href="{{ route('admin.room.edit',$rowDesigner['id']) }}" title="Edit Kost">
                            <i class="fa fa-edit"></i> Edit Kost</a></li>
                        @else
                        <li><a href="{{ route('admin.room.edit',$rowDesigner['id']) }}" title="Edit Apartment">
                            <i class="fa fa-edit"></i> Edit Apartment</a></li>
                        @endif

                        @if ($rowDesigner['is_facility_set'] == true)
                            @if ($rowDesigner['facility_count'] > 0)
                            <li><a href="{{ route('admin.room.facility.edit', $rowDesigner['id']) }}" title="Edit Facilities">
                                <i class="fa fa-bath"></i> Edit Facility Photos</a></li>
                            @else
                            <li><a href="{{ route('admin.room.facility.add', $rowDesigner['id']) }}" title="Add Facilities">
                                <i class="fa fa-bath"></i> Add Facility Photos</a></li>
                            @endif
                        @endif
                        <li><a href="{{ route('admin.room.facility.setting', $rowDesigner['id']) }}" title="Setup Facilities">
                            <i class="fa fa-wrench"></i> Set Available Facilities</a></li>

                        <!-- Only for Bookable Rooms -->
                        @if ($rowDesigner['is_booking'] == 1)
                            <!-- if premium photo already activated -->
                            @if ($rowDesigner['is_premium_photo'] == 1)
                                <!-- if premium photo is available -->
                                @if ($rowDesigner['is_premium_photo_available'] == 1)
                                <li><a href="#" data-action="deactivate" data-id="{{ $rowDesigner['id'] }}" class="btn-action"
                                        title="Deactivate Photo Booking">
                                        <i class="fa fa-camera"></i> Deactivate Photo Booking
                                    </a></li>
                                <li><a href="{{ route('admin.card.premium.index',$rowDesigner['id']) }}"
                                        title="Edit Photo Booking">
                                        <i class="fa fa-camera"></i> Edit Photo Booking
                                    </a></li>

                                <!-- if premium photo isn't set yet -->
                                @else
                                <li><a href="{{ route('admin.card.premium.index',$rowDesigner['id']) }}"
                                        title="Upload Photo Booking">
                                        <i class="fa fa-cloud-upload"></i> Upload Photo Booking
                                    </a></li>
                                @endif
                                <!-- If premium photo isn't activated yet -->
                                @else
                                <!-- if premium photo is available -->
                                @if ($rowDesigner['is_premium_photo_available'] == 1)
                                <li><a href="#" data-action="activate" data-id="{{ $rowDesigner['id'] }}" class="btn-action"
                                        title="Activate Booking Media">
                                        <i class="fa fa-camera"></i> Activate Booking Media
                                    </a></li>
                                <li><a href="{{ route('admin.card.premium.index',$rowDesigner['id']) }}"
                                        title="Edit Booking Media">
                                        <i class="fa fa-camera"></i> Edit Booking Media
                                    </a></li>
                                <!-- if premium photo isn't available -->
                                @else
                                <li><a href="{{ route('admin.card.premium.index',$rowDesigner['id']) }}"
                                        title="Upload Photo Booking">
                                        <i class="fa fa-cloud-upload"></i> Upload Photo Booking
                                    </a></li>
                                @endif
                            @endif

                            {{-- Video tour  by Mamichecker--}}
                            @if ($rowDesigner['by_mamichecker'] != true)
                            <li>
                                <a href="{{ route('admin.room.video', $rowDesigner['id']) }}" title="Add Video Data">
                                    <i class="fa fa-video-camera"></i> Add Video Tour
                                </a>
                            </li>
                            @else
                            <li>
                                <a href="{{ route('admin.room.video.update', $rowDesigner['id']) }}" title="Update Video Data">
                                    <i class="fa fa-video-camera"></i> Edit Video Tour
                                </a>
                            </li>
                            @endif
                        @endif

                        <li><a href="{{ route('admin.card.index',$rowDesigner['id']) }}" title="Edit Cards">
                                <i class="fa fa-picture-o"></i> Edit Media
                            </a></li>

                        <li>
                            {{-- Edit or create VR Tour --}}
                            @if ($rowDesigner['attachment_id'])
                                <a href="{{ route('admin.vrtour.edit',$rowDesigner['attachment_id']) }}" title="Edit VR Tour">
                                    <i class="fa fa-eye"></i> Edit VR Tour
                                </a>
                            @else
                                <a href="{{ route('admin.vrtour.create',$rowDesigner['id']) }}" title="Create VR Tour">
                                    <i class="fa fa-eye"></i> Create VR Tour
                                </a>
                            @endif
                        </li>

                        @if (is_null($rowDesigner['duplicate_from']))
                            <li>
                                {{-- Create kost duplicate --}}
                                <a href="javascript:duplicateKost({{ $rowDesigner['id'] }}, false)" title="Duplicate Kost">
                                    <i class="fa fa-clone"></i> Duplicate kost
                                </a>

                                {{-- Create kost duplicate with photo --}}
                                <a href="javascript:duplicateKost({{ $rowDesigner['id'] }}, true)" title="Duplicate Kost With Photo">
                                    <i class="fa fa-clone"></i> Duplicate kost With Photo
                                </a>
                            </li>
                        @endif

                        <li>
                            <a href="javascript:recalculateScore({{ $rowDesigner['id'] }})" title="Edit Cards">
                                <i class="fa fa-history"></i> Recalculate sort score
                            </a>
                        </li>

                        <li role="presentation" class="divider"></li>
                        <li class="dropdown-header">Preview</li>
                        @if(is_null($rowDesigner['apartment_project_id']))
                        <li><a href="{{ env("APP_URL") }}/room/{{ $rowDesigner['slug'] }}" title="Preview"
                                target="_blank" rel="noopener">
                                <i class="fa fa-external-link"></i> Preview
                            </a></li>
                        @else
                        <li><a href="{{ env("APP_URL") }}/unit/{{ $rowDesigner['apartment_project_slug'] }}/{{ $rowDesigner['slug'] }}"
                                title="Preview" target="_blank" rel="noopener">
                                <i class="fa fa-external-link"></i> Preview
                            </a></li>
                        @endif

                        @if ($rowDesigner['by_mamichecker'] == true)
                        <li>
                            <a href="{{ $rowDesigner['video_tour_url'] }}" data-fancybox data-caption="Video Tour {{ $rowDesigner['name'] }}" title="View Video Tour">
                                <i class="fa fa-video-camera"></i> Video Tour
                            </a>
                        </li>
                        @endif

                        @if(!is_null($rowDesigner['latitude']) && !is_null($rowDesigner['longitude']))
                        <li><a href="http://maps.google.com/?q={{ $rowDesigner['latitude'] }},{{ $rowDesigner['longitude'] }}"
                                title="Map" target="_blank" rel="noopener">
                                <i class="fa fa-map-marker"></i> Map
                            </a></li>
                        @endif

                        <li>
                            <a href="#" class="view-price-filter" data-id="{{ $rowDesigner['id'] }}"
                                title="Show Price Filter"><i class="fa fa-money"></i> View Price Filter</a>
                        </li>

                        @if(!is_null($rowDesigner['apartment_project_id']))
                        <li><a href="{{ url('admin/room/redirectkost', $rowDesigner['id']) }}" title="Redirect Slug"><i
                                    class="fa fa-share"></i> Redirect Slug</a></li>
                        @endif

                        <li role="presentation" class="divider"></li>
                        <li class="dropdown-header">Set Status</li>

                        @if ($rowDesigner['is_mamirooms'])
                            <li>
                                <a href="{{ route('admin.room.mamirooms.action', ['room' => $rowDesigner['id'], 'action' => 'unset']) }}" title="Set type to Non Mamirooms">
                                    <i class="fa fa-eraser"></i> Set type to Non Mamirooms
                                </a>
                            </li>
                        @else
                            <li>
                                <a href="{{ route('admin.room.mamirooms.action', ['room' => $rowDesigner['id'], 'action' => 'set']) }}" title="Set type to Mamirooms">
                                    <i class="fa fa-feed"></i> Set type to Mamirooms
                                </a>
                            </li>
                        @endif

                        @if ($rowDesigner['is_testing'])
                            <li>
                                <a href="{{ route('admin.room.testing.action', ['room' => $rowDesigner['id'], 'action' => 'unset']) }}" title="Set type to Non Testing">
                                    <i class="fa fa-check-circle-o"></i> Set type to Non Testing
                                </a>
                            </li>
                        @else
                            <li>
                                <a href="{{ route('admin.room.testing.action', ['room' => $rowDesigner['id'], 'action' => 'set']) }}" title="Set type to Testing">
                                    <i class="fa fa-wrench"></i> Set type to Testing
                                </a>
                            </li>
                        @endif

                        @if ($rowDesigner['status'] == 0)
                        <li><a href="{{ $rowDesigner['url_available'] }}" title="Set to story status to full">
                                <i class="fa fa-lock"></i> Set status to full</a></li>
                        @else
                        <li><a href="{{ $rowDesigner['url_available'] }}" title="Set to story status to available">
                                <i class="fa fa-unlock"></i> Set status to available</a></li>
                        @endif
                        <!-- @if ($rowDesigner['is_booking'] == 0)  -->
                        <!-- <li><a href="{{ url('admin/room/booking', $rowDesigner['id']) }}" title="Can Booking"><i class="fa fa-bell"></i> Can Booking</a></li> -->
                        <!-- @else -->
                        <!-- <li><a href="{{ url('admin/room/unbooking', $rowDesigner['id']) }}" title="Can't Booking"><i class="fa fa-bell-o"></i> Can't Booking</a></li> -->
                        <!-- @endif -->

                        @if(!is_null($rowDesigner['apartment_project_id']))
                        <li><a href="{{ url('admin/apartment/project/' . $rowDesigner['apartment_project_id'] . '/edit') }}"
                                title="Apartment Project"><i class="fa fa-road"></i> Apartment Project</a></li>
                        @else
                        <li><a href="{{ url('admin/room/migrate-apartment/' . $rowDesigner['id']) }}"
                                title="Jadikan Apartment Unit"><i class="fa fa-exchange"></i> Jadikan Apartment Unit</a>
                        </li>
                        @endif

                        <li role="presentation" class="divider"></li>
                        <li class="dropdown-header">Analytics</li>
                        <li><a href="{{ url('admin/room/promotion', $rowDesigner['id']) }}" title="History Promote"><i
                                    class="fa fa-history"></i> Promote History</a></li>

                        <li><a href="{{ url('admin/room/call', $rowDesigner['id']) }}"><i class="fa fa-phone"></i> Call
                                History</a></li>

                        <li role="presentation" class="divider"></li>
                        @if(is_null($rowDesigner['apartment_project_id']))
                        <li><a href="{{ url('admin/room/delete',$rowDesigner['id']) }}" title="Delete Kost"
                                onclick="return confirm('Are you sure you want to delete this?')">
                                <i class="fa fa-trash-o"></i> Delete Kost</a></li>
                        @else
                        <li><a href="{{ url('admin/room/delete',$rowDesigner['id']) }}" title="Delete Apartment"
                            onclick="return confirm('Are you sure you want to delete this?')">
                            <i class="fa fa-trash-o"></i> Delete Apartment</a></li>
                        @endif

                        @if ($rowDesigner['can_be_thanosed'])
                        <li role="presentation" class="divider"></li>
                        <li><a href="{{ $rowDesigner['thanos_url'] }}" title="Thanos Snap">
                                    <i class="fa fa-hand-o-down"></i>Thanos</a></li>
                        @endif

                        @if ($rowDesigner['hidden_by_thanos'])
                        <li role="presentation" class="divider"></li>
                        <li><a href="{{ $rowDesigner['unthanos_url'] }}" title="Revert Thanos Snap">
                                    <i class="fa fa-hand-o-up"></i>UnThanos</a></li>
                        @endif

                        <!-- This action only exists on DEVELOPMENT!  -->
                        @if (auth('web')->user()->role == 'administrator' && env('APP_ENV') != 'production')
                        <li role="presentation" class="divider"></li>
                        <li class="dropdown-header">Developer</li>
                        <li>
                            <a href="#" class="view-source" data-room="{{ print_r($rowDesigner, true) }}"
                                title="Show Source"><i class="fa fa-code"></i> View Source</a>
                        </li>
                        @endif

                        @endif
                    </ul>
                </div>
            </td>
        </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th></th>
            <th width="150px"></th>
            <th>Nama Kost / Apartemen</th>
            <th>Kost Type / Kost Level</th>
            <!-- <th class="text-center">Status</th> -->
            <th class="text-center">Price</th>
            <th class="text-center">Available Room</th>
            <th class="text-center" width="5%">Owner Phone</th>
            <th class="text-center" width="5%">Manager Phone</th>
            <th class="text-center">Inputter</th>
            <th class="text-center">Agent</th>
            <th class="text-center" width="10%">Created / Updated</th>
            <th class="table-action-column" width="10%"></th>
        </tr>
    </tfoot>
</table>
