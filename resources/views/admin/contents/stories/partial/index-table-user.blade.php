<table id="tableListDesigner" class="table table-striped">
    <thead>
        <tr>
            <th width="30px;">No</th>
            <th>Photo</th>
            <th>Name</th>
            <th>Status</th>
            <th>Created at / Updated at</th>
            <th class="table-action-column" width="100px">Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($rowsDesigner as $rowDesignerKey => $rowDesigner)
        <tr>
            <td >{{ $rowDesignerKey+1 }}</td>
            <td>
                <?php $tempMedia = Media::getMediaUrl($rowDesigner['photo_id']); ?>
                <img width="200" class="img-thumbnail" src="{{ $tempMedia['small'] }}" data-src="holder.js/60x77/thumbnail" alt="">
            </td>
            <td style="font-size:16px">{{ $rowDesigner['name'] }} </td>
            <td>{{ ($rowDesigner->status == 0) ? 'Available' : 'Full' }}</td>
            <td>
                {{ $rowDesigner->created_at->format('d-m-Y, H:i') }} <hr> {{ $rowDesigner->updated_at->format('d-m-Y, H:i') }}
            </td>
            <td class="table-action-column">
                <div class="btn-action-group">
                    @if ($rowDesigner->is_active=='false')
                        <a href="{{ URL::route('kost-admin.kost.verify.action', array($rowDesigner->id)) }}" title="Verikasi Kost">
                            <i class="fa fa-arrow-up"></i></a>
                    @else
                        <a href="{{ URL::route('kost-admin.kost.unverify.action', array($rowDesigner->id)) }}" title="Sembunyikan">
                            <i class="fa fa-arrow-down"></i></a>
                    @endif

                    @if ($rowDesigner->status == 0)
                        <a href="{{ route('kost-admin.kost.full', $rowDesigner->id) }}" title="Set Status Full">
                            <i class="fa fa-lock"></i></a>
                    @else
                        <a href="{{ route('kost-admin.kost.available', $rowDesigner->id) }}" title="Set Status Available">
                            <i class="fa fa-unlock"></i></a>
                    @endif
                    <a href="{{ URL::route($editRoute, $rowDesigner->id) }}" title="Edit Stories">
                        <i class="fa fa-pencil"></i></a>
                    <a href="#" class="btn-delete" data-id="{{ $rowDesigner->id }}"  data-name="{{ htmlspecialchars($rowDesigner->name) }}" title="Delete Stories">
                        <i class="fa fa-trash-o"></i></a>
                </div>
            </td>
        </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th width="30px;">No</th>
            <th>Photo</th>
            <th>Name</th>
            <th>Status</th>
            <th>Created at / Updated at</th>
            <th class="table-action-column" width="100px">Action</th>
        </tr>
    </tfoot>
</table>
