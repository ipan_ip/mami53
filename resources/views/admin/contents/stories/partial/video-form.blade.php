@extends('admin.layouts.main')
@section('style')
<style>
    .font-large {
        font-size: 1.5em;
    }

    .font-semi-large {
        font-size: 1.15em;
    }

    .font-grey {
        color: #9E9E9E;
    }

    .label-grey {
        background-color: #CFD8DC;
    }
</style>
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" />
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/select2-bootstrap-theme@0.1.0-beta.10/dist/select2-bootstrap.min.css">
@endsection

@section('content')
<form action="" method="POST" class="form-horizontal form-bordered" id="form">
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{!! $boxTitle !!}</h3>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div id="cards">
                <div id="formCards">
                    <div class="form-group bg-default" id="cardInputVideoUrl1">
                        <label for="inputVideo" class="col-sm-2 control-label">Youtube URL</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="video" name="video" value="{{ isset($video) ? $video->source_url : '' }}">
                            <span class="help-block">Contoh URL yang valid: <strong>https://www.youtube.com/watch?v=m5sRRma6BHw</strong> atau <strong>https://youtu.be/m5sRRma6BHw</strong></span>
                        </div>
                    </div>
                    <div class="form-group bg-default">
                        <label for="checker" class="col-sm-2 control-label">Profile Checker</label>
                        <div class="col-sm-4">
                            <select class="form-control" name="checker" id="checker">
                                @if (isset($checker))
                                <option selected="selected" value="{{ $checker->id }}">{{ $checker->user->name }} ({{ $checker->user->email }})</option>
                                @endif
                            </select>
                        </div>
                    <input type="hidden" id="designerId" value="{{ $designerId }}">
                    </div>
                    <div class="form-group bg-default">
                        <div id="checkerProfile" class="col-md-4 col-md-offset-2"></div>
                        {{-- Profile will be here --}}
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <a href="{{ $formCancel }}">
                        <button type="button" class="btn btn-default">Cancel</button>
                    </a>
                    <button type="submit" class="btn btn-primary" id="submit" style="margin-left: 10px!important;">Submit <i class="fa fa-paper-plane"></i></button>
                    @if (isset($checker))
                    <button type="button" class="btn btn-danger pull-right" id="delete"><i class="fa fa-trash"></i>  Hapus Video Tour</button>
                    @endif
                </div>
            </div>
        </div>
    </div>
</form>
<!-- /.form -->
</div><!-- /.box -->
@endsection

@section('script')
<!-- page script -->
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.js"></script>
<script type="text/javascript">
@if (Session::get('laravelValidatorJSON') === null)
    myGlobal.laravelValidator = null;
@else
    myGlobal.laravelValidator = {{ Session::get('laravelValidatorJSON') }};
@endif

// Sweetalert functions
function triggerAlert(type, message) {
    Swal.fire({
        type: type,
        title: message
    });
}

function triggerLoading(message) {
    Swal.fire({
        title: message,
        allowEscapeKey: false,
        allowOutsideClick: false,
        onOpen: () => {
            swal.showLoading();
        }
    });
}


$(function() {
    var checkerProfile = $('#checkerProfile');
    var checkerSelector = $('#checker');

    // helper variable
    var _editing = false;
    var checkerTrackerId = 0;
    var cardId = 0;

    // function to populate checker profile
    function populateCheckerProfile(id) {
        checkerProfile.html('');

        var html = "";

        triggerLoading('Mengambil data profile..');

        $.ajax({
            type: 'POST',
            url: "/admin/mami-checker/checker",
            data: {
                id: id
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: (data) => {
                if(data != null) {
                    var html = '<div class="panel panel-default">'+
                            '<div class="panel-body">'+
                                '<div class="row">'+
                                    '<div class="col-md-5 text-center">'+
                                        '<img class="img-circle" src="'+data.photo_url.small+'" width="150">'+
                                    '</div>'+
                                    '<div class="col-md-7">'+
                                        '<h3>'+data.user.name+'</h3>'+
                                        '<small>Email</small>'+
                                        '<p>'+data.user.email+'</p>'+
                                        '<small>Phone</small>'+
                                        '<p>'+data.user.phone_number+'</p>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>';
        
                    $('#checkerProfile').append(html);
                }

                if (Swal.isLoading()) Swal.close();
            },
            error: (error) => {
                triggerAlert('error', error);
            }
        });
    }

    // initiate selector
    checkerSelector.select2({
        theme: "bootstrap",
        placeholder: 'Ketik nama untuk mencari',
        language: {
            searching: function() {
                return "Sedang mengambil daftar checker...";
            },
            inputTooShort: function() {
                return 'Ketik nama untuk mencari..';
            },
            noResults: function() {
                return "Profil checker tidak ditemukan! Coba cari nama yang lain.";
            }
        },
        ajax: {
            url: '/admin/mami-checker/all',
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page || 1
                }
            },
            processResults: function (data, params) {
                var page = params.page || 1;
                if (data.data.length < 1) var total_count = 0;
                else var total_count = data.data[0].total_count;
                return {
                    results: $.map(data.data, item => { 
                        return {
                            id: item.id, 
                            text: item.user.name + ' (' + item.user.email + ')'
                        }
                    }),
                    pagination: {
                        more: (page * 10) <= total_count
                    }
                };
            }
        }
    });

    // if editing data
    @if (isset($checker))
    _editing = true;
    checkerTrackerId = {{ (isset($tracker)) ? $tracker->id : 0 }};
    cardId = {{ (isset($video)) ? $video->id : 0 }};
    populateCheckerProfile({{ $checker->id }})
    @endif

    // selector listener
    checkerSelector.on('select2:select', (e) => {
        var selected = e.params.data.id;
        populateCheckerProfile(selected);
    });

    // form submit event
    $('#form').on('submit', e => {
        e.preventDefault();

        // get form fields values
        var videoUrl = $('#video').val();
        var checkerId = $("#checker").val();
        var designerId = $("#designerId").val();
        
        // simple validation
        if (videoUrl == '') triggerAlert('error', 'Isikan URL Video!');
        if (checkerId == '' || checkerId == null) triggerAlert('error', 'Pilih profil checker!');

        // saving data
        Swal.fire({
            type: 'question',
            title: 'Simpan data video tour?',
            showCancelButton: true,
            confirmButtonText: 'Submit',
            showLoaderOnConfirm: true,
            preConfirm: () => {
                return $.ajax({
                    type: 'POST',
                    url: "/admin/room/video",
                    dataType: 'json',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {
                        'trackerId': checkerTrackerId,
                        'cardId': cardId,
                        'videoUrl': videoUrl,
                        'checkerId': checkerId,
                        'roomId': designerId
                    },
                    success: (response) => {
                        // return;
                    },
                    error: (error) => {
                        triggerAlert('error', error);
                    }
                });
            },
            allowOutsideClick: () => !Swal.isLoading()
        }).then((result) => {
            console.log(result);
            
            if (!result.dismiss) {
                if (result.value.success) {
                    Swal.fire({
                        type: 'success',
                        title: "Berhasil menyimpan data video tour!",
                        onClose: () => {
                            window.location.replace("{{ $formCancel }}");
                        }
                    });
                } else {
                    triggerAlert('error', result.value.message);
                }
            }
        })
    })

    // delete event
    $('#delete').on('click', () => {
        Swal.fire({
            type: 'question',
            title: 'Hapus data video tour?',
            html: 'Data video yang dihapus tidak akan bisa dikembalikan.',
            showCancelButton: true,
            confirmButtonText: 'Delete',
            showLoaderOnConfirm: true,
            preConfirm: () => {
                return $.ajax({
                    type: 'POST',
                    url: "/admin/room/video/remove",
                    dataType: 'json',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {
                        'trackerId': checkerTrackerId,
                        'cardId': cardId
                    },
                    success: (response) => {
                        // return;
                    },
                    error: (error) => {
                        triggerAlert('error', error);
                    }
                });
            },
            allowOutsideClick: () => !Swal.isLoading()
        }).then((result) => {
            console.log(result);
            
            if (!result.dismiss) {
                if (result.value.success) {
                    Swal.fire({
                        type: 'success',
                        title: "Berhasil menghapus data video tour!",
                        onClose: () => {
                            window.location.replace("{{ $formCancel }}");
                        }
                    });
                } else {
                    triggerAlert('error', result.value.message);
                }
            }
        })
    })
})
</script>
@endsection