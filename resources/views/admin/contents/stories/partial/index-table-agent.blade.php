    <p class="helper-block">
        Warna <span class="apartment-color"><strong>Biru</strong></span> merupakan apartemen
    </p>

    <div class="col-md-11">
        
<div class="alert alert-success"><strong>Jumlah data: </strong> {{ isset($paginate) ? number_format($paginate['total'], 0, ".", ".") : 0 }} :: Verify: {{ $verifiedcount }} , Unverify: {{ $unverifiedcount }} , Notlisted: {{ $notlistedcount }}</div>
    </div>
    
<table id="tableListDesigner" class="table">
    <thead>
        <tr>
            <th width="30px;">No</th>
            <th style = "width : 150px">Photo</th>
            <th>Name</th>
            <th>Price</th>
            <th>Note</th>
            <th>Owner Phone</th>
            <th>Manager Phone</th>
            <th>Avail able</th>
            <th>Input</th>
            <th>Agent</th>
            <th>Area City</th>
            <th width="90px">Create / Update</th>
            <th class="table-action-column" width="120px">Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($rowsRoom as $rowDesignerKey => $rowDesigner)
        <tr class="{{ !is_null($rowDesigner['apartment_project_id']) ? 'apartment-color' : '' }} {{ $rowDesigner['is_active']=='true' ? 'kost-active' : '' }}">
            <td >{{ $rowDesignerKey+1 }} @if(!is_null($rowDesigner['duplicate_from'])) <i class="fa fa-retweet" style="color: #ff0000;"></i> @endif </td>
            <td>
                <img width="200" class="img-thumbnail" src="{{ $rowDesigner['photo_url'] }}"
                    data-src="holder.js/60x77/thumbnail" alt="">
            </td>
            <td style="font-size:16px">{{ $rowDesigner['name'] }}
               @if ($rowDesigner['promoted'] == true)
                   <span class="label label-success">Premium</span>
               @endif
            </td>
            <td>
                @if (!is_null($rowDesigner['price_monthly']))
                    {{ $rowDesigner['price_monthly']['currency_symbol'] }} {{ $rowDesigner['price_monthly']['price'] }}
                @endif
            </td>
            <td>{{ $rowDesigner['agent_note'] }}</td>
            <td align="center">{{ $rowDesigner['owner_phone'] }}
            <a href="{{ URL::to('admin/owner/change', $rowDesigner['id']) }}" class="btn btn-xs btn-danger">Change Owner</a></td>
            <td align="center">{{ $rowDesigner['manager_phone'] }}</td>
            <td align="center"><b>{{ $rowDesigner['room_available'] }}</b></td>
            <td align="center">
                @if (in_array($rowDesigner['agent_status'], ['Agen', 'Lainnya', 'scrap', 'Anak Kos', 'Anak Kost']))
                    <label class="label label-primary">{{ ucwords($rowDesigner['agent_status']) }}</label>
                    @if ($rowDesigner['check_owner'] == 0)
                        @if ($rowDesigner['is_active'] == 'false')
                            <a href="" title="Kosnya aktifin dl gaes" style="margin-top: 10px;" class="btn btn-xs btn-default" disabled="true">Create Account</a>
                        @else
                            <a href="{{ URL::to('admin/room/create/owner', $rowDesigner['id']) }}" class="btn btn-xs btn-success" style="margin-top: 10px;">Create Account</a>
                        @endif
                    @endif
                @else
                    <span class="label label-success">
                        {{ $rowDesigner['agent_status'] }}
                    </span>
                @endif
                
            </td>
            <td align="center">
                {{ $rowDesigner['agent_name'] }}

                @if(!is_null($rowDesigner['verificator']))
                    <br><br>
                    <span class="label label-warning" title="Verifikator">
                        {{ $rowDesigner['verificator'] }}
                    </span>
                @endif
            </td>
            <td>
                {{ $rowDesigner['area_city'] }}
            </td>
            <td align="center">
                {{ $rowDesigner['created_at'] }} <hr> {{ $rowDesigner['updated_at'] }}
            </td>
            <td class="table-action-column">
                <div class="btn-action-group">
                    @if ($rowDesigner['is_active'] =='false')
                            <a id='verify-{{$rowDesigner['id']}}' data-toggle="modal" data-target="#verify-popup" title="Verify"><i class="fa fa-arrow-up"></i></a>
                    @else
                        <a href="{{ $rowDesigner['url_verify'] }}" title="Unverify">
                            <i class="fa fa-arrow-down"></i></a>
                    @endif
                    
                    @if(is_null($rowDesigner['apartment_project_id']))
                        <a href="https://mamikos.com/room/{{ $rowDesigner['slug'] }}" title="Preview" target="_blank" rel="noopener">
                            <i class="fa fa-chain-broken"></i>
                        </a>
                    @else
                        <a href="https://mamikos.com/unit/{{ $rowDesigner['apartment_project_slug'] }}/{{ $rowDesigner['slug'] }}" title="Preview" target="_blank" rel="noopener">
                            <i class="fa fa-chain-broken"></i>
                        </a>
                    @endif
                    
                    <a href="{{ route('admin.room.edit',$rowDesigner['id']) }}" title="Edit Stories">
                        <i class="fa fa-pencil"></i></a>

                    <a href="{{ url('admin/room/delete',$rowDesigner['id']) }}" title="Delete Stories" onclick="return confirm('Sampean Yakin Kate Ngehapus Tah?')">
                        <i class="fa fa-trash-o"></i></a>

                    <a href="{{ route('admin.card.index',$rowDesigner['id']) }}" title="Edit Cards">
                        <i class="fa fa-book"></i>
                    </a>
                    @if ($rowDesigner['is_active'] == 'true' && $rowDesigner['is_booking'] == false)
                        <a href="{{ $rowDesigner['url_set_status_add'] }}" title="Set Kost Status to ADD">
                            <i class="fa fa-plus"></i></a>
                    @endif
    
                </div>
            </td>
        </tr>
        <script>
            var listProperties = null;
            $(function()
            {
                $("#verify-{{$rowDesigner['id']}}" ).click(function() {

                    var roomId = `{{$rowDesigner['id']}}`;
                    var urlForm = `{!! url("/admin/room/agent/verify/") !!}` +'/' + roomId;
                    var url = `{!! url("/admin/room/agent/properties/$rowDesigner[id]") !!}`;
                    var urlAturFoto = `{!! url("/admin/card") !!}` +'/' + roomId ;
                    let option = ``;

                    $('#form-verify').attr('action', urlForm);
                    $('#room-id').val(roomId);
                    $('#atur-foto').attr("href", urlAturFoto);
                    
                    option += `<option value="" disabled selected>Nama Properti</option>`;
                    
                    stopReqListProperties();
                    listProperties = $.ajax({
                        type: 'GET',
                        url: url,
                        dataType: 'json',
                        success: (res) => {
                            var properties = res.data ;
                            var selectedProperty = res.selected_property ;
                            var hasMultipleType = res.has_multiple_type ;

                            if (properties !== undefined && properties.length > 0) {
                                $.each(properties, function(index, property) {
                                    option += `<option value="${property.id}">${property.name}</option>`;
                                });
                            }
                            option += `<option value="{{$rowDesigner['id']}}" id="tambah-properti"><u><b></b>Tambah Properti Baru</b></u></option>`;
                            $('#select-nama-properti').html(option);
                            $('#unit-type').val(res.unit_type);
                            $(`#select-nama-properti option[value="${selectedProperty}"]`).attr("selected", "selected");

                            if(hasMultipleType==1){
                                $("#allow-multiple-type").html('&#9989;').removeClass('btn-danger').addClass('btn-success').attr('style',  'font-size: 10px;').prop('disabled',true);
                                $("#unit-type").prop('disabled',false).attr("placeholder", "Tipe A, Tipe B, Tipe C");
                                $("#has-multiple-type").val(1);
                            }else{
                                $("#allow-multiple-type").html('&#10005;').removeClass('btn-success').addClass('btn-danger').attr('style',  'font-size: 10px;').prop('disabled', false);
                                $("#unit-type").prop('disabled', true).attr("placeholder", "Tipe A, Tipe B, Tipe C");
                                $("#has-multiple-type").val(0);
                            }
                        },
                        error: (error) => {
                            option += `<option value="{{$rowDesigner['id']}}" id="tambah-properti"><u><b></b>Tambah Properti Baru</b></u></option>`;
                            $('#select-nama-properti').html(option);
                        }
                    });                                        
                });
            });
        </script>
        @endforeach
    </tbody>
</table>

<!-- Modal Verify-->
<div class="modal fade" id="verify-popup" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
            <h3 class="modal-title"> <b>Lengkapi Data Kos</b> 
                <button id="close-verify-popup" type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </h3>
        </div>
        <div class="alert alert-danger" role="alert" id="alert-tambah-properti" style="display: none;"></div>
        <div class="modal-body" style="font-size: 10px;">
            {{ Form::open(array('url' => '', 'method' => 'POST',
            'class' => 'form-horizontal form-borderequired', 'role' => 'form', 'id' => 'form-verify')) }}
                <div class="form-group bg-default">
                    <div class="alert alert-danger" role="alert" id="alert-verify" style="display: none;"></div>
                </div>
                <div class="form-group bg-default">
                        <label class="col-sm-2 control-label" style="display: inline;"> <span class="label label-sm label-success">New</span> <span class='required-mark'>*</span>Nama Properti</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="property_id" id="select-nama-properti" required>
                                <option value="" disabled selected>Loading....</option>
                            </select>
                        </div>
                  </div>

                <div class="form-group bg-default">
                        <label class="col-sm-2 control-label" style="display: inline;"> <span class="label label-sm label-success">New</span> <span class='required-mark'>*</span>Tipe Kamar 
                            <span><button type="button" class="btn-danger" id="allow-multiple-type" disabled>&#10005;</button></span>
                        </label>
                        <div class="col-sm-10">
                          <input type="hidden" id="has-multiple-type" name="has_multiple_type">
                          <input type="hidden" id="room-id">
                          <input id="unit-type" class="form-control" rows="3" name="unit_type" placeholder="Loading ...." value="" maxlength="499" required disabled>
                          <p class="text-warning" id="alert-unit-type"></p>
                          <!-- <div id="countDesc" style="font-weight:bold"></div> -->
                        </div>
                </div>

                <div class="form-group bg-default">
                        <label class="col-sm-2 control-label" style="display: inline;"><span class="label label-sm label-success">New</span> <span class='required-mark'>*</span>Atur Foto Kos dan Kamar</label>
                        <div class="col-sm-10">
                          <a id="atur-foto" type="button" class="btn btn-primary" href="" target="_blank">Atur Foto</a>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="submit-verify">Verify</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal" id="cancel-verify">Cancel</button>
                </div>
            {{ Form::close() }}<!-- /.form -->
        </div>
      </div>
    </div>
</div>

<!-- Modal Tambah Properti-->
<div class="modal fade" id="tambah-properti-popup" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
            <h3 class="modal-title"> <b>Tambah Properti</b> 
                <button id="close-tambah-properti-popup" type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </h3>
        </div>
        <div class="modal-body">
            {{ Form::open(array('url' => '', 'method' => 'POST',
            'class' => 'form-horizontal form-borderequired', 'role' => 'form', 'id' => 'form-tambah-properti')) }}  
                <div class="alert alert-danger" role="alert" id="alert-tambah-properti" style="display: none;"></div>
                <div class="form-group bg-default">
                        <label class="col-sm-2 control-label" style="display: inline;"> <span class="label label-sm label-success">New</span> <span class='required-mark'>*</span>Nama Properti</label>
                        <div class="col-sm-10">
                          <input type="hidden" id="designer-id" >
                          <input class="form-control" rows="3" id="input-nama-properti" name="name" placeholder="Nama Properti" value="" maxlength="499" required>
                          <small class="help-block">Format Nama Properti tidak boleh berisi karakter: . , ( ) / - " '</small>
                          <p class="text-warning" id="alert-nama-properti"></p>
                          <!-- <div id="countDesc" style="font-weight:bold"></div> -->
                        </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="submit-tambah-properti" disabled>Tambah</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal" id="cancel-tambah-properti">Batal</button>
                </div>
            {{ Form::close() }}<!-- /.form -->
        </div>
      </div>
    </div>
</div>

<script>
    var propertiReq = null;
    var unitTypeReq = null;
    var tambahPropertiReq = null;
    var submitVerifyReq = null;
    var detailProperiReq = null;
    
    $(function()
    {   
        $(window).keydown(function(event){
            if(event.keyCode == 13) {
              event.preventDefault();
              return false;
            }
        });

        resetFormVerifyAndFormProperty();
        $("#cancel-verify" ).click(function() {
            stopReqListProperties();
            destroyElementFormVerify();
        });

        $("#close-verify-popup" ).click(function() {
            stopReqListProperties();
            destroyElementFormVerify()
        });

        $("#cancel-tambah-properti" ).click(function() {
            stopCheckingPropertyName();
            stopAddPropertyName();
            destroyElementFormProperti();
        });

        $("#close-tambah-properti-popup" ).click(function() {
            stopCheckingPropertyName();
            stopAddPropertyName();
            destroyElementFormProperti();
        });

        $("select[name='property_id']" ).change(function() {

            stopRegDetailProperty();
            $("#allow-multiple-type").html('&#10005;').removeClass('btn-success').addClass('btn-danger').attr('style',  'font-size: 10px;').prop('disabled', true);
            $("#unit-type").prop('disabled', true).attr("placeholder", "Loading ....").val('');
            $("#has-multiple-type").val(0);

            var id = $(this).children(":selected").attr("id");
            var roomId = $(this).val();
            var url = `{!! url("/admin/room/agent/property/") !!}`;
            var urlPropertyDetail = `{!! url("/admin/room/agent/property/") !!}` + '/' + $(this).val();

            $("#alert-verify").text('').hide();

            if(id == 'tambah-properti'){
                $('#form-tambah-properti').attr('action', url);
                $('#verify-popup').modal('hide');
                $('#tambah-properti-popup').modal('toggle');
                let option = `<option value="" disabled selected>Loading ....</option>`;
                $('#select-nama-properti').html(option);
                $("#designer-id").val(roomId);
                destroyElementFormVerify();
            }else{
                detailProperiReq = $.ajax({
                        type: 'GET',
                        url: urlPropertyDetail,
                        dataType: 'json',
                        success: (res) => {
                            
                            if(res==null){
                                $("#allow-multiple-type").html('&#10005;').removeClass('btn-success').addClass('btn-danger').attr('style',  'font-size: 10px;').prop('disabled', true);
                                $("#has-multiple-type").val(0);
                                $("#unit-type").prop('disabled', true).attr("placeholder", "Pilih Nama Properti Yang Valid");
                                return;
                            }

                            if(res.has_multiple_type==1){
                                $("#allow-multiple-type").html('&#9989;').removeClass('btn-danger').addClass('btn-success').attr('style',  'font-size: 10px;').prop('disabled',true);
                                $("#has-multiple-type").val(1);
                                $("#unit-type").prop('disabled', false).attr("placeholder", "Tipe A, Tipe B, Tipe C");

                                }else{
                                    $("#allow-multiple-type").html('&#10005;').removeClass('btn-success').addClass('btn-danger').attr('style',  'font-size: 10px;').prop('disabled', false);
                                    $("#has-multiple-type").val(0);
                                    $("#unit-type").prop('disabled', true).attr("placeholder", "Tipe A, Tipe B, Tipe C");
                                }                               
                            },
                         error: (error) => {
                         }
                     });     
            }               
        });

        $("#input-nama-properti").keyup(function(){
                var url = `{!! url("/admin/room/agent/check-property-name/") !!}` + '/' + $("#designer-id").val();

                $("#submit-tambah-properti").prop('disabled', true);
                $('#alert-nama-properti').attr('class','text-warning').text('Sedang melakukan pengecekkan Nama Properti');
                $(this).closest('.form-group').removeClass('has-feedback has-error').removeClass('has-feedback has-success').addClass('has-feedback has-warning');
                
                stopCheckingPropertyName();

                if(!$(this).val().trim()){
                    $(this).closest('.form-group').removeClass('has-feedback has-success').removeClass('has-feedback has-warning').addClass('has-feedback has-error');
                    $('#alert-nama-properti').attr('class','text-danger').text("Nama Properti tidak boleh kosong");
                    return;
                }

                propertiReq = $.ajax({
                        type: 'GET',
                        url: url,
                        data: {
                            name: $(this).val(),
                        },
                        dataType: 'json',
                        success: (res) => {
                                if (res.success) {                        
                                    $(this).closest('.form-group').removeClass('has-feedback has-error').removeClass('has-feedback has-warning').addClass('has-feedback has-success');
                                    $('#alert-nama-properti').attr('class','').text('');
                                    $("#submit-tambah-properti").prop('disabled', false);
                                }else{
                                    $(this).closest('.form-group').removeClass('has-feedback has-success').removeClass('has-feedback has-warning').addClass('has-feedback has-error');
                                    $('#alert-nama-properti').attr('class','text-danger').text(res.messages);
                                    $("#submit-tambah-properti").prop('disabled', true);
                                }                                
                            },
                         error: (error) => {
                         }
                     });                 
        });

        $("#unit-type").keyup(function(){
                var roomId = $('#room-id').val();
                var url = `{!! url("/admin/room/agent/check-unit-type/") !!}` + '/' + roomId;
                var idProperty = $('#select-nama-properti').val();

                $("#alert-verify").text('').hide();
                $('#alert-unit-type').attr('class','text-warning').text('Sedang melakukan pengecekkan Tipe Kamar');
                $(this).closest('.form-group').removeClass('has-feedback has-error').removeClass('has-feedback has-success').addClass('has-feedback has-warning');

                stopCheckingUnitType();
                if(!idProperty){
                    $(this).closest('.form-group').removeClass('has-feedback has-success').removeClass('has-feedback has-warning').addClass('has-feedback has-error');
                    $('#alert-unit-type').attr('class','text-danger').text("Pilih Nama Properti Terlebih dahulu");
                    return;
                }

                if(!$(this).val().trim()){
                    $(this).closest('.form-group').removeClass('has-feedback has-success').removeClass('has-feedback has-warning').addClass('has-feedback has-error');
                    $('#alert-unit-type').attr('class','text-danger').text("Tipe Kamar tidak boleh kosong");
                    return;
                }

                unitTypeReq = $.ajax({
                        type: 'GET',
                        url: url,
                        data: {
                            name: $(this).val(),
                            id_property: $('#select-nama-properti').val(),
                        },
                        dataType: 'json',
                        success: (res) => {
                                if (res.success) {                        
                                    $(this).closest('.form-group').removeClass('has-feedback has-error').removeClass('has-feedback has-warning').addClass('has-feedback has-success');
                                    $('#alert-unit-type').attr('class','').text('');
                                }else{
                                    $(this).closest('.form-group').removeClass('has-feedback has-success').removeClass('has-feedback has-warning').addClass('has-feedback has-error');
                                    $('#alert-unit-type').attr('class','text-danger').text(res.messages);
                                }                                
                            },
                         error: (error) => {
                         }
                     });                 
        });

        $("#submit-tambah-properti").on('click',function (){
            var url = $('#form-tambah-properti').attr('action');
            var roomId = $('#designer-id').val();
            var propertyName = $('#input-nama-properti').val(); 
            var success = false;

            $("#alert-tambah-properti").text('').hide();
            $('#submit-tambah-properti').prop('disabled', true).html(`<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>Loading...`);
                
            stopAddPropertyName();
            tambahPropertiReq = $.ajax({
                type: 'POST',
                url: url,
                dataType: 'json',
                data: {
                    id: roomId,
                    name: propertyName,
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: (res) => {
                    if (!res.success) {
                        $("#alert-tambah-properti").text(res.messages || 'Terjadi Kesalahan silahkan refresh browser anda').show();
                        $('#submit-tambah-properti').prop('disabled', true).html('').text('Tambah');                        
                        return;
                    } 
                    
                    destroyElementFormProperti();
                },
                error: (error) => {
                    $("#alert-tambah-properti").text('Terjadi Kesalahan silahkan refresh browser anda').show();
                    $('#submit-tambah-properti').prop('disabled', true).html('').text('Tambah');                        
                }
            }); 
                
        });

        $("#submit-verify").on('click',function (){
            var url = $('#form-verify').attr('action');
            var roomId = $('#room-id').val();
            var propertyName = $('#select-nama-properti').val(); 
            var unitType = $('#unit-type').val();
            var redirectUrl = `{!! url("/admin/room/") !!}` + '/' + roomId + '/room-unit';

            $("#alert-verify").text('').hide();
            $('#submit-verify').prop('disabled', true).html(`<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>Loading...`);
                
            stopSubmitVerify();
            submitVerifyReq = $.ajax({
                type: 'POST',
                url: url,
                dataType: 'json',
                data: {
                    id: roomId,
                    property_id: propertyName,
                    unit_type: unitType,
                    has_multiple_type: $("#has-multiple-type").val(),
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: (res) => {
                    if (!res.success) {
                        $("#alert-verify").attr('class','alert alert-danger').text(res.messages || 'Terjadi Kesalahan silahkan refresh browser anda').show();
                        $('#submit-verify').prop('disabled', false).html('').text('Submit');                        
                        return;
                    } 

                    $("#alert-verify").attr('class','alert alert-success').text(res.messages || 'Room has been Verified').show();
                    $('#submit-verify').prop('disabled', false).html('').text('Submit');
                    window.open(redirectUrl, '_blank');
                    window.focus();
                    location.reload();                        
                },
                error: (error) => {
                    $("#alert-verify").attr('class','alert alert-danger').text('Terjadi Kesalahan silahkan refresh browser anda').show();
                    $('#submit-verify').prop('disabled', false).html('').text('Submit');                        
                }
            }); 
                
        });

        $("#allow-multiple-type").on('click', function(){
            var hasMultipleType =  $("#has-multiple-type");
            if(hasMultipleType.val() == 1){
                hasMultipleType.val(0);
                $(this).html('&#10005;').removeClass('btn-success').addClass('btn-danger').attr('style',  'font-size: 10px;');
                $("#unit-type").prop('disabled', true);
            }else{
                hasMultipleType.val(1);
                $(this).html('&#9989;').removeClass('btn-danger').addClass('btn-success').attr('style',  'font-size: 10px;');
                $("#unit-type").prop('disabled', false);
            }
        });
        
    });

    function destroyElementFormProperti(){
            var roomId = $("#designer-id").val();
            var urlForm = `{!! url("/admin/room/agent/verify/") !!}` +'/' + roomId;
            var urlProperti = `{!! url("/admin/room/agent/properties/") !!}` +'/' + roomId;
            var urlAturFoto = `{!! url("/admin/card") !!}` +'/' + roomId ;

            $('#form-verify').attr('action', urlForm);
            
            $('#tambah-properti-popup').modal('hide');
            $('#verify-popup').modal('toggle');
            $('#verify-popup').modal('show');
                
            let option = ``;
            option += `<option value="" disabled selected>Nama Properti</option>`;
            $.ajax({
                type: 'GET',
                url: urlProperti,
                dataType: 'json',
                success: (res) => {
                    var properties = res.data ;
                    var selectedProperty = res.selected_property ;
                    var hasMultipleType = res.has_multiple_type ;

                    if (properties !== undefined && properties.length > 0) {
                        $.each(properties, function(index, property) {
                            option += `<option value="${property.id}">${property.name}</option>`;
                        });
                    }
                    option += `<option value="`+roomId+`" id="tambah-properti"><u><b></b>Tambah Properti Baru</b></u></option>`;
                    $('#select-nama-properti').html(option);
                    $('#unit-type').val(res.unit_type);
                    $(`#select-nama-properti option[value="${selectedProperty}"]`).attr("selected", "selected");

                    if(hasMultipleType==1){
                                $("#allow-multiple-type").html('&#9989;').removeClass('btn-danger').addClass('btn-success').attr('style',  'font-size: 10px;').prop('disabled',true);
                                $("#has-multiple-type").val(1);
                                $("#unit-type").prop('disabled', false).attr("placeholder", "Tipe A, Tipe B, Tipe C");
                    }else{
                        $("#allow-multiple-type").html('&#10005;').removeClass('btn-success').addClass('btn-danger').attr('style',  'font-size: 10px;').prop('disabled', false);
                        $("#has-multiple-type").val(0);
                        $("#unit-type").prop('disabled', true).attr("placeholder", "Tipe A, Tipe B, Tipe C");
                    }
                },
                error: (error) => {
                    option += `<option value="`+roomId+`" id="tambah-properti"><u><b></b>Tambah Properti Baru</b></u></option>`;
                    $('#select-nama-properti').html(option);
                }
            });    

            $("#form-tambah-properti").attr('action', '');
            $("#designer-id").val('');
            $("#input-nama-properti").val('');
            $('#submit-tambah-properti').prop('disabled', true).html('').text('Tambah');                        
            $('#alert-nama-properti').attr('class','text-warning').text('');
            $("#input-nama-properti").closest('.form-group').removeClass('has-feedback has-error').removeClass('has-feedback has-success').removeClass('has-feedback has-warning');
            $("#alert-tambah-properti").text('').hide();
            $("#room-id").val(roomId);
            $('#atur-foto').attr("href", urlAturFoto);
    }

    function destroyElementFormVerify(){
        $('#form-verify').attr('action', '');
        $("#alert-verify").text('').hide();
        let option = `<option value="" disabled selected>Loading ....</option>`;
        $('#select-nama-properti').html(option);
        $('#room-id').val('');
        $('#alert-unit-type').attr('class','text-warning').text('');
        $("#unit-type").closest('.form-group').removeClass('has-feedback has-error').removeClass('has-feedback has-success').removeClass('has-feedback has-warning');
        $("#unit-type").val('');
        stopSubmitVerify();
        $('#atur-foto').attr("href", '');
        $("#allow-multiple-type").html('&#10005;').removeClass('btn-success').addClass('btn-danger').attr('style',  'font-size: 10px;').prop('disabled', true);
        $("#unit-type").prop('disabled', true).attr("placeholder", "Loading ....");
        $("#has-multiple-type").val(0);
    }

    function stopCheckingPropertyName()
    {
        if(propertiReq != null){
            propertiReq.abort();
        }
    }

    function stopCheckingUnitType()
    {
        if(unitTypeReq != null){
            unitTypeReq.abort();
        }
    }

    function stopAddPropertyName()
    {
        if(tambahPropertiReq != null){
            tambahPropertiReq.abort();
        }
    }

    function stopReqListProperties()
    {
        if(listProperties != null){
            listProperties.abort();
        }
    }

    function stopSubmitVerify()
    {
        if(submitVerifyReq != null){
            submitVerifyReq.abort();
        }
    }

    function stopRegDetailProperty()
    {
        if(detailProperiReq != null){
            detailProperiReq.abort();
        }
    }

    function resetFormVerifyAndFormProperty()
    {
        $('#form-verify').attr('action', '');
        let option = `<option value="" disabled selected>Loading ....</option>`;
        $('#select-nama-properti').html(option);
        $("#form-tambah-properti").attr('action', '');
        $("#designer-id").val('');
        $("#input-nama-properti").val('');
        $('#submit-tambah-properti').prop('disabled', true).html('').text('Tambah');                        
        $('#alert-nama-properti').attr('class','text-warning').text('');
        $('#alert-unit-type').attr('class','text-warning').text('');
        $("#input-nama-properti").closest('.form-group').removeClass('has-feedback has-error').removeClass('has-feedback has-success').removeClass('has-feedback has-warning');
        $('#tambah-properti-popup').modal('hide');
        $('#verify-popup').modal('hide');
        $("#alert-tambah-properti").text('').hide();
        $("#unit-type").val('');
        $('#alert-unit-type').attr('class','text-warning').text('');
        $("#unit-type").closest('.form-group').removeClass('has-feedback has-error').removeClass('has-feedback has-success').removeClass('has-feedback has-warning');
    }

</script>
