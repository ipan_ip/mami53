@extends('admin.layouts.main')
@section('style')
    <style>
    .table > tbody > tr > td
    {
        vertical-align: middle;
    }
    
    .font-large
    {
        font-size: 1.5em;
    }
    
    .font-semi-large
    {
        font-size: 1.25em;
        font-weight: 700;

    }

    .font-semi-large-2
    {
        font-size: 1.1em;
        font-weight: 700;

    }
    
    .font-grey
    {
        color: #9E9E9E;
    }

    /*   Change Color for Inactive Tabs     */
    .nav-tabs>li>a {
        color: #9E9E9E;
    }
    </style>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/dropzone.css">
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
    {{ HTML::style('assets/vendor/jquery-file-upload/css/jquery.fileupload.css') }}
@stop
@section('content')
	<div class="box box-success">
		<div class="box-header">
			<h3 class="box-title">{!! $title !!}</h3>
		</div><!-- /.box-header -->
		<div class="box-body">
			<form id="form" action="" method="post">
                <div class="tabpanel justify-content-center">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        @foreach ($structure as $key => $group)
                            <li role="presentation" @if ($key == 0) class="active" @endif>
                                <a href="#tab-{{ $group['id'] }}" aria-controls="#tab-{{ $group['id'] }}" role="tab" data-toggle="tab">
                                    <small>Category</small><br/>
                                    <span class="font-semi-large">{{ $group['name'] }}</span>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        @foreach ($structure as $key => $group)
                            <div role="tabpanel" @if($key == 0) class="tab-pane fade in active" @else class="tab-pane fade in" @endif id="tab-{{ $group['id'] }}">
                                <div class="tabpanel">
                                    <!-- Sub Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        @foreach ($group['types'] as $key => $type)
                                            <li role="presentation" @if ($key == 0) class="active" @endif>
                                                <a href="#subtab-{{ $type['id'] }}" aria-controls="#subtab-{{ $type['id'] }}" role="tab" data-toggle="tab">
                                                    <small>Type</small><br/>
                                                    <span class="font-semi-large">{{ $type['name'] }}</span>
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                    <!-- Sub Tab panes -->
                                    <div class="tab-content">
                                        @foreach ($group['types'] as $key => $type)
                                            <div role="tabpanel" @if($key == 0) class="tab-pane fade in active" @else class="tab-pane fade in" @endif id="subtab-{{ $type['id'] }}">
                                                <div class="tabpanel">
                                                    <!-- Tags Sections -->
                                                    @if (empty($type['tags']))
                                                        <div class="callout callout-warning text-center">
                                                            <span class="font-semi-large">No "{{ $type['name'] }}" facilities/tags are being activated for this Kos !</span><br><br>
                                                            Click button <strong>[ <i class="fa fa-wrench"></i> Set Available Facilities ]</strong> below to set active facility tags
                                                        </div>
                                                    @else
                                                        @foreach ($type['tags'] as $key => $tag)
                                                                <div class="box box-solid box-default" style="border:1px solid #d2d6de;">
                                                                    <div class="box-header">
                                                                        <div class="box-title" style="font-size:1.25em;font-weight:600;">
                                                                            <i class="fa fa-angle-down"></i> {{ $tag['name'] }}
                                                                            @if ($tag['is_top'])
                                                                                <span class="label label-success" style="margin-left:10px;"><i class="fa fa-thumb-tack"></i> Top Facility</span>
                                                                            @endif
                                                                            @if (isset($tag['note']) && $tag['note'] != '')
                                                                                <br/>
                                                                                <div style="margin-left:15px;font-size:0.7em!important;">{{ $type['note'] }}</div>
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                @if ($tag['is_photo_upload_enabled'])
                                                                    <div class="box-body row">
                                                                        @for ($i = 0; $i < 3; $i++)
                                                                            <div class="col-md-4">
                                                                                {{-- Photo upload container --}}
                                                                                <div class="form-group bg-default"
                                                                                    id="uploader-{{ $tag['id'] }}-{{ $i }}">
                                                                                    <div class="col-sm-12">
                                                                                        <div id="cardUpload1-{{ $tag['id'] }}-{{ $i }}"
                                                                                            class="cardUpload1 media">
                                                                                            <div class="col-md-12 text-center">
                                                                                                <a id="wrapper-img-{{ $tag['id'] }}-{{ $i }}"
                                                                                                    onclick="triggerUploader({{ $tag['id'] }}, {{ $i }})"
                                                                                                    data-id="{{ $tag['id'] }}"
                                                                                                    data-index="{{ $i }}">
                                                                                                    @if (isset($tag['facilities_count']) && $tag['facilities_count'] > 0 && isset($tag['facilities'][$i]))
                                                                                                        <img style="background: #EEE; cursor: pointer;"
                                                                                                            class="media-object img-responsive img-thumbnail image"
                                                                                                            data-src="holder.js/360x240?text=Click here \n to upload \n {{ $tag['name'] }}'s \n photo"
                                                                                                            alt="" 
                                                                                                            src="{{ $tag['facilities'][$i]['photo_url'] }}">
                                                                                                    @else
                                                                                                        <img style="background: #EEE; cursor: pointer;"
                                                                                                        class="media-object img-responsive img-thumbnail"
                                                                                                        data-src="holder.js/360x240?text=Click here \n to upload \n {{ $tag['name'] }}'s \n photo"
                                                                                                        alt="">
                                                                                                    @endif
                                                                                                </a>
                                                                                                @if (isset($tag['facilities_count']) && $tag['facilities_count'] > 0 && isset($tag['facilities'][$i]))
                                                                                                    <button type="button" class="btn btn-block btn-danger btn-remove" data-id="{{ $tag['facilities'][$i]['id'] }}"><i class="fa fa-trash-o"></i></button>
                                                                                                @endif
                                                                                            </div>
                                                                                            <div class="media-body col-md-12 text-left">
                                                                                                <input id="sourceImg-{{ $tag['id'] }}-{{ $i }}"
                                                                                                    class="sourceImg" type="hidden">
                                                                                                <br>
                                                                                                <input id="fileupload-{{ $tag['id'] }}-{{ $i }}"
                                                                                                        type="file" name="media"
                                                                                                        class="fileupload hidden">
                                                                                                <div id="original-file-info-{{ $tag['id'] }}-{{ $i }}"
                                                                                                        class="original-file-info callout callout-info" hidden></div>
                                                                                                <div id="uploaded-file-info-{{ $tag['id'] }}-{{ $i }}"
                                                                                                        class="uploaded-file-info callout callout-success" hidden></div>
                                                                                                <!-- The container for the uploaded file -->
                                                                                                <div id="files-{{ $tag['id'] }}-{{ $i }}" class="files"></div>
                                                                                                <!-- The photo_id of the uploaded file -->
                                                                                                @if (isset($tag['facilities_count']) && $tag['facilities_count'] > 0 && isset($tag['facilities'][$i]))
                                                                                                <input id="photo_id-{{ $tag['id'] }}-{{ $i }}"
                                                                                                        class="photo_id" type="hidden"
                                                                                                        name="photo-{{ $tag['id'] }}-{{ $i }}"
                                                                                                        value="{{ $tag['facilities'][$i]['photo_id'] }}">
                                                                                                @else
                                                                                                <input id="photo_id-{{ $tag['id'] }}-{{ $i }}"
                                                                                                        class="photo_id" type="hidden"
                                                                                                        name="photo-{{ $tag['id'] }}-{{ $i }}">
                                                                                                @endif
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        @endfor
                                                                    </div>
                                                                @else
                                                                    <div class="box-body">
                                                                        <div class="callout callout-danger text-center">
                                                                            <span class="font-semi-large font-grey"><i class="fa fa-ban"></i> Photo upload is disabled for this facility tag</span>
                                                                        </div>
                                                                    </div>
                                                                @endif
                                                                </div>
                                                            
                                                        @endforeach
                                                    @endif
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            </form>
            <div class="box-footer text-right">
                <button type="button" class="btn btn-default" onclick="window.location.href = '{{ route('admin.room.index', 'q=' . $roomId) }}'">Cancel</button>
                <button type="button" class="btn btn-warning" onclick="window.location.href = '{{ route('admin.room.facility.setting', $roomId) }}'"><i class="fa fa-wrench"></i> Set Available Facilities</button>
                <button type="button" class="btn btn-primary" onclick="action('submit', {{ $roomId }}, '{{ $scope }}')" style="margin-left:10px;"><i class="fa fa-save"></i> Save</button>
            </div>
		</div>
	</div>
@stop
@section('script')
    <script src="//cdn.jsdelivr.net/npm/holderjs@2.9.6/holder.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.js"></script>
    <!-- The basic File Upload plugin -->
    {{ HTML::script('assets/vendor/jquery-file-upload/js/jquery.fileupload.js') }}
    <!-- The File Upload processing plugin -->
    {{ HTML::script('assets/vendor/jquery-file-upload/js/jquery.fileupload-process.js') }}
    <!-- The File Upload image preview & resize plugin -->
    {{ HTML::script('assets/vendor/jquery-file-upload/js/jquery.fileupload-image.js') }}
    <!-- The File Upload validation plugin -->
    {{ HTML::script('assets/vendor/jquery-file-upload/js/jquery.fileupload-validate.js') }}
	<script>
        function triggerAlert(type, message) {
            Swal.fire({
                type: type,
                customClass: {
                    container: 'custom-swal'
                },
                html: message
            });
        }

        function triggerLoading(message) {
            Swal.fire({
                title: message,
                customClass: {
                    container: 'custom-swal'
                },
                allowEscapeKey: false,
                allowOutsideClick: false,
                onOpen: () => {
                    swal.showLoading();
                }
            });
        }

        function triggerUploader(id, index) {
            const url = "{{ url('admin/media') }}";
            const mediaType = "facility_photo";

            let identifier = '-' + id + '-' + index;
            let wrapperFileUpload = $(document).find('#wrapper-img' + identifier);
            let fileInput = $(document).find('#fileupload' + identifier);

            function readImage(file, mediaType) {
                var deferred = new $.Deferred();

                // var url = window.URL || window.webkitURL; // alternate use
                
                var reader = new FileReader();
                var image  = new Image();
                var info = {};

                reader.readAsDataURL(file);

                reader.onload = function(_file) {
                    image.src    = _file.target.result;              // url.createObjectURL(file);
                    image.onload = function() {
                        info = {
                        status : true,
                        width : this.width,
                        height : this.height,
                        type : file.type,                           // ext only: // file.type.split('/')[1],
                        name : file.name,
                        sizeInKB : Math.round(file.size/1024)
                        };

                        if ((this.width < 120 || this.height < 120) && mediaType != 'tag_photo')
                        {
                        info.status = false;
                        info.error = 'Invalid file width and height: ' +
                            this.width + ' x ' + this.height + ' px. ' +
                            'Width and Height must be at least 640 x 640 px.';
                        }
                        deferred.resolve(info);
                    };
                    image.onerror = function() {
                        info = {
                        status : false,
                        error : 'Invalid file type: '+ file.type
                        };
                        deferred.resolve(info);
                    };
                };

                return deferred.promise();
            }

            fileInput.fileupload({
                url: url,
                formData: {
                    watermarking: true,
                    watermarking_type: "premium",
                    media_type : mediaType
                },
                disableImageResize: /Android(?!.*Chrome)|Opera/
                    .test(window.navigator && navigator.userAgent),
                imageMaxWidth: 1920,
                imageMaxHeight: 1080,
                maxNumberOfFiles: 1,
                maxFileSize: 2000000, // 2MB
                dataType: 'json',
                add: function (e, data) {
                    triggerLoading("Processing Photo...");

                    $('#files' + identifier).html('');
                    $('#uploaded-file-info' + identifier).html('');

                    var addedFile = data.files[0];

                    var fileUpload = $(this);

                    readImage(addedFile, mediaType).done(function (fileInfo)
                    {
                        if (fileInfo.status === true) {
                            if (data.autoUpload || (data.autoUpload !== false &&
                                    fileUpload.fileupload('option', 'autoUpload'))) {
                                data.process().done(function () {
                                    data.submit();
                                });
                            }
                        } else {
                            var error = $('<span class="text-danger"/>').text(fileInfo.error);
                            $('#files' + identifier).html(error);
                        }

                        var fileInfoDisplay = {
                            name : '<b>Name: </b>' + addedFile.name + '<br>',
                            type : '<b>Type: </b>' + addedFile.type + '<br>',
                            size : '<b>Size: </b>' + fileInfo.sizeInKB + ' KB<br>',
                            dimension : '<b>Dimension: </b>' + fileInfo.width + ' x ' + fileInfo.height + ' px<br>',
                        };

                        var message = '';

                        $.each(fileInfoDisplay, function(i, item) {
                            message += item;
                        });

                        $('#original-file-info' + identifier).html(message);
                    });

                    $.blueimp.fileupload.prototype.options.add.call(this, e, data);
                },
                progress: function (e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    console.log(progress + "%");
                },
                fail: function (e, data) {
                    let errorMessage = 'File upload failed. Please refresh the page and try uploading again.';
                    triggerAlert('error', errorMessage);
                    var error = $('<span class="text-danger"/>').text(errorMessage);
                    $(data.context.children()[index])
                        .append('<br>')
                        .append(error);

                    if (Swal.isLoading()) Swal.close();
                },
                done: function (e, data) {
                    if (data.result.status === true)
                    {
                        var photoId = '<b>Photo ID: </b>' + data.result.media.id + '<br>';
                        var name = '<b>Name: </b>' + data.result.media.file_name + '<br>';
                        var url = '<b>URL: </b>' + data.result.media.url.real + '<br>';

                        // Append uploaded file info
                        $('#uploaded-file-info' + identifier).append(name + photoId + url);

                        // Append image to <img> element
                        if (data.result.media.file_name.indexOf(".gif") > 1) wrapperFileUpload.children('.media-object').attr('src',data.result.media.url.real);
                        wrapperFileUpload.children('.media-object').attr('src', data.result.media.url.medium);

                        // Store photo_id
                        $('#photo_id' + identifier).val(data.result.media.id);
                    }
                    else
                    {
                        // Error handling
                        let errorMessage = 'File upload failed. ' + data.result.meta.message;
                        triggerAlert('error', errorMessage);
                        var error = $('<span class="text-danger"/>').text(errorMessage);
                        $('#files' + identifier).html(error);
                    }   

                    if (Swal.isLoading()) Swal.close();  
                }
            })
            .prop('disabled', !$.support.fileInput)
            .parent().addClass($.support.fileInput ? undefined : 'disabled');

            // Trigger file_input click
            fileInput.trigger('click');
        }

        function getFormData(roomId, scope) {
            let formData = $('#form').serializeArray();
            let compiledFormData = [];

            $.each(formData, (key, val) => {
                if (val.value !== '' && 
                    (val.name.indexOf('photo') >= 0 || val.name.indexOf('fid') >= 0)) {
                    compiledFormData.push({
                        name: val.name,
                        value: parseInt(val.value)
                    });
                }
            })

            compiledFormData.push({
                name: 'room_id',
                value: roomId
            });

            return compiledFormData;
        }

        function action(type, roomId, scope) {
            if (type === 'submit') {
                // compile dataObject
                let dataObject = getFormData(roomId, scope);
                let url;

                // Determine URL
                if (scope === 'editing')
                    url = "/admin/room/facility/edit";
                else
                    url = "/admin/room/facility";

                Swal.fire({
                    type: 'question',
                    customClass: {
                        container: 'custom-swal'
                    },
                    title: 'Save facility photos?',
                    showCancelButton: true,
                    confirmButtonText: 'Save',
                    showLoaderOnConfirm: true,
                    preConfirm: () => {
                        return $.ajax({
                            type: 'POST',
                            url: url,
                            dataType: 'json',
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            data: dataObject
                        });
                    },
                    allowOutsideClick: () => !Swal.isLoading()
                }).then((result) => {
                    if (result.dismiss != 'cancel') {
                        const response = result.value;
                        console.log(response);

                        if (response.success == false) {
                            triggerAlert('error', '<h5>' + response.message + '</h5>');
                        } else {
                            let title;
                            if (scope === 'editing') title = "Facility photos successfully updated!";
                            else title = "New facility photos successfully saved!";

                            Swal.fire({
                                type: 'success',
                                customClass: {
                                    container: 'custom-swal'
                                },
                                title: title,
                                html: "<h5>Page will be refreshed after you clicked OK</h5>",
                                onClose: () => {
                                    window.location.reload();
                                }
                            });
                        }
                    }
                })
            }

            else if (type === 'setting') {
                window.location.replace("setting/" + roomId);
            }
        }

        $(function () {
            $('.btn-remove').on('click', e => {
                e.preventDefault();
                const facilityId = $(e.currentTarget).data('id');

                Swal.fire({
                    type: 'warning',
                    customClass: {
                        container: 'custom-swal'
                    },
                    title: 'Delete this photo?',
                    showCancelButton: true,
                    confirmButtonText: 'Delete',
                    showLoaderOnConfirm: true,
                    preConfirm: () => {
                        return $.ajax({
                            type: 'POST',
                            url: '/admin/room/facility/remove',
                            dataType: 'json',
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            data: {
                                'id': facilityId
                            }
                        });
                    },
                    allowOutsideClick: () => !Swal.isLoading()
                }).then((result) => {
                    if (result.dismiss != 'cancel') {
                        const response = result.value;
                        console.log(response);

                        if (response.success == false) {
                            triggerAlert('error', '<h5>' + response.message + '</h5>');
                        } else {
                            Swal.fire({
                                type: 'success',
                                customClass: {
                                    container: 'custom-swal'
                                },
                                title: response.message,
                                html: "<h5>Page will be refreshed after you clicked OK</h5>",
                                onClose: () => {
                                    window.location.reload();
                                }
                            });
                        }
                    }
                })
            });
        })
	</script>
@stop