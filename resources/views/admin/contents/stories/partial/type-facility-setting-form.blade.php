@extends('admin.layouts.main')
@section('style')
    <style>
    .table > tbody > tr > td
		{
			vertical-align: middle;
		}
		
		.font-large
		{
			font-size: 1.5em;
		}
		
		.font-semi-large
		{
			font-size: 1.15em;
		}
		
		.font-grey
		{
			color: #9E9E9E;
		}
		
		.dropdown-menu > li > a:hover
		{
			color: #333
		}
		
		.label-grey
		{
			background-color: #CFD8DC;
		}
		
		[hidden]
		{
			display: none !important;
		}
		
		/* Select2 tweak */
		
		.input-group .select2-container,
		.form-group .select2-container
		{
			position: relative !important;
			z-index: 2;
			float: left !important;
			width: 100% !important;
			margin-bottom: 0 !important;
			display: table !important;
			table-layout: fixed !important;
		}
		
		/* Sweetalert */
		
		.swal-shown
		{
			height: auto !important;
		}
		
		.custom-swal
		{
			z-index: 10000 !important;
		}
		
		.custom-wide-swal
		{
			width: 640px !important;
			font-size: 13px !important;
		}
		
		/* Custom table row */
		tr.inactive td
		{
			background-color: #E6C8C8;
		}
    </style>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/dropzone.css">
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
    {{-- <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/icheck@1.0.2/skins/flat/_all.css"> --}}
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/icheck@1.0.2/skins/square/_all.css">
@stop
@section('content')
	<div class="box box-success">
		<div class="box-header">
			<h3 class="box-title">{!! $title !!}</h3>
		</div><!-- /.box-header -->
		<div class="box-body">
			<form id="form" action="" method="post">
				@foreach ($structure as $group)
					<div class="box box-solid box-success" style="border:1px solid #d2d6de;">
						<div class="box-header">
							<div class="box-title" style="font-size:1.25em;font-weight:600;">
								<i class="fa fa-angle-down"></i> {{ $group['name'] }}
								@if (isset($group['note']) && $group['note'] != '')
									<br/>
									<div style="margin-left:15px;font-size:0.75em!important;">{{ $group['note'] }}</div>
								@endif
							</div>
						</div>
						<div class="box-body">
                            @php
                            $numOfCols = 3;
                            $rowCount = 0;
                            $bootstrapColWidth = 12 / $numOfCols;
                            @endphp
                            <div class="row">
                                @foreach ($group['types'] as $type)
                                <div class="col-md-{{ $bootstrapColWidth }}">
                                    <div class="box box-solid box-default" style="border:1px solid #d2d6de;">
                                        <div class="box-header">    
                                            <div class="box-title" style="font-size:1.25em;font-weight:600;">
                                                <i class="fa fa-angle-down"></i> {{ $type['name'] }}
                                                @if (isset($type['note']) && $type['note'] != '')
                                                    <br/>
                                                    <div style="margin-left:15px;font-size:0.7em!important;">{{ $type['note'] }}</div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="box-body">
                                            <div class="row">
                                                @foreach ($type['tags'] as $tag)
                                                    <div class="col-md-6 font-semi-large">
                                                        @if (in_array($tag['id'], $activeTags))
                                                            <input type="checkbox" class="checkbox" name="{{ $tag['id'] }}" id="tag-{{ $tag['id'] }}" checked/>
                                                        @else
                                                            <input type="checkbox" class="checkbox" name="{{ $tag['id'] }}" id="tag-{{ $tag['id'] }}"/>
                                                        @endif
                                                        <label for="tag-{{ $tag['id'] }}" style="margin-left:5px;">
                                                            {{ $tag['name'] }} 
                                                            @if ($tag['is_top'])
                                                                <span class="label label-success"><i class="fa fa-thumb-tack"></i></span>
                                                            @endif
                                                        </label>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @php
                                    $rowCount++;
                                    if($rowCount % $numOfCols == 0) echo '</div><div class="row">';
                                @endphp
                                @endforeach
                            </div>
						</div>
					</div>
				@endforeach
            </form>
            <div class="box-footer text-right">
                <button type="button" class="btn btn-default" onclick="window.location.href = '{{ route('admin.room.type.index', $parentRoomId) }}'">Cancel</button>
                <button type="button" class="btn btn-warning" onclick="window.location.href = '{{ route('admin.room.type.facility.add', $roomId) }}'"><i class="fa fa-bath"></i> Go To Facility Photos</button>
                <button type="button" class="btn btn-primary" onclick="action('submit', {{ $roomId }}, '{{ $scope }}')" style="margin-left:10px;"><i class="fa fa-save"></i> Save</button>
            </div>
		</div>
	</div>
@stop
@section('script')
    <script src="//cdn.jsdelivr.net/npm/holderjs@2.9.6/holder.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/icheck@1.0.2/icheck.min.js"></script>
	<script>
        function triggerAlert(type, message) {
            Swal.fire({
                type: type,
                customClass: {
                    container: 'custom-swal'
                },
                html: message
            });
        }

        function triggerLoading(message) {
            Swal.fire({
                title: message,
                customClass: {
                    container: 'custom-swal'
                },
                allowEscapeKey: false,
                allowOutsideClick: false,
                onOpen: () => {
                    swal.showLoading();
                }
            });
        }

        function getFormData(roomId, scope) {
            let formData = $('#form').serializeArray();

            formData.push({
                name: 'room_id',
                value: roomId
            });

            return formData;
        }

        function action(type, roomId, scope) {
            if (type === 'submit') {
                // compile dataObject
                let dataObject = getFormData(roomId, scope);

                Swal.fire({
                    type: 'question',
                    customClass: {
                        container: 'custom-swal'
                    },
                    title: 'Save facility settings?',
                    showCancelButton: true,
                    confirmButtonText: 'Save',
                    showLoaderOnConfirm: true,
                    preConfirm: () => {
                        return $.ajax({
                            type: 'POST',
                            url: "/admin/room/type/facility/setting",
                            dataType: 'json',
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            data: dataObject
                        });
                    },
                    allowOutsideClick: () => !Swal.isLoading()
                }).then((result) => {
                    const response = result.value;
                    
                    if (response.success === false) {
                        triggerAlert('error', '<h5>' + response.message + '</h5>');
                    } else {
                        Swal.fire({
                            type: 'success',
                            customClass: {
                                container: 'custom-swal'
                            },
                            title: "Facility setting successfully updated!",
                            html: "<h5>Page will be refreshed after you clicked OK</h5>",
                            onClose: () => {
                                window.location.reload();
                            }
                        });
                    }
                })
            }
        }

        $(document).ready(function(){
            $('.checkbox').iCheck({
                checkboxClass: 'icheckbox_square-green',
                hoverClass: 'hover',
                focusClass: 'focus',
                activeClass: 'active',
                labelHover: true,
                labelHoverClass: 'hover',
                increaseArea: '',
                cursor: true,
            });
        });
	</script>
@stop