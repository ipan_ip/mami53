<form id="addPhotosForm{{$mediaName}}"
      action="{{ url($url) }}"
      method="POST"
      class="dropzone">
    {{ csrf_field() }}
    <input type="hidden" name="media_type" value="{{$mediaType}}">
    <input type="hidden" name="watermarking" value="{{$watermarking}}">
</form>
