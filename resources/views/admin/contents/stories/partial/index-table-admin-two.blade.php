<table id="tableListDesigner" class="table">
<thead>
    <tr>
        <th width="30px;">No</th>
        <th style="width:150px">Photo</th>
        <th>Name / Note</th>
        <th>Owner Phone / <br/>
            Manager Phone</th>
        <th>Input Type</th>
        <th>Price</th>
        <th>Gender</th>
        <th>Phone Status</th>
        <th align="center">Room</th>
        <th>Submit</th>
    </tr>
</thead>
<tbody>
    @foreach ($rowsRoom as $rowDesignerKey => $rowDesigner)

    <tr>
    <form action="{{ url()->route('admin.room.update.two', $rowDesigner['id']) }}" method="post">
        <td >{{ $rowDesignerKey + 1 }}</td>
        <td>
            <img height="200" class="img-thumbnail" src="{{ $rowDesigner['photo_small'] }}"
                    data-src="holder.js/60x77/thumbnail" alt=""><br/>
            <b><i>Last Update : {{ $rowDesigner['kost_updated_date'] }}</i></b>
            <p>
                @if ($rowDesigner['is_promoted'] == 'true')
                    <label class="label label-success">Premium</label>
                @elseif ($rowDesigner['is_premium_owner'])
                    <label class="label label-success">Premium</label>
                @endif
            </p>
        </td>
        <td style="font-size:16px">
            {{ $rowDesigner['name'] }} <br/><br/>
            <textarea name="admin_remark" style="background-color:#F4ED47; font-size:10pt; width:100%" rows="3">{{ $rowDesigner['admin_remark'] }}</textarea><br/>
            <span style="font-size:12px">{{ $rowDesigner['address'] }}</span>
        </td>
        <td align="center">
            @if ($rowDesigner['agent_status'] == 'Agen')
                <span class="label label-primary">Agent</span>
            @else
                <span class="label label-success">
                        {{ $rowDesigner['agent_status'] }}
                    </span>
            @endif
        </td>
        <td style="font-size:16px; position:relative" colspan="6" width="60%">

            <div class="col-sm-6">
                <div class="form-group bg-default">
                    <label for="price_daily" class="col-sm-6 control-label">Price Daily</label>
                    <div class="col-sm-6">
                        <input type="number" value="{{ $rowDesigner['price_daily'] }}" align="right" size="8" width="40px" name="price_daily" placeholder="Price Daily"/>
                    </div>
                </div>
                <div class="form-group bg-default">
                    <label for="price_weekly" class="col-sm-6 control-label">Price Weekly</label>
                    <div class="col-sm-6">
                        <input type="number" value="{{ $rowDesigner['price_weekly'] }}" align="right" size="8" width="40px" name="price_weekly" placeholder="Price Weekly"/>
                    </div>
                </div>
                <div class="form-group bg-default">
                    <label for="price_monthly" class="col-sm-6 control-label">Price Monthly</label>
                    <div class="col-sm-6">
                        <input type="number" value="{{ $rowDesigner['price_monthly'] }}" align="right" size="8" width="40px" name="price_monthly" placeholder="Price Monthly"/>
                    </div>
                </div>
                <div class="form-group bg-default">
                    <label for="price_yearly" class="col-sm-6 control-label">Price Yearly</label>
                    <div class="col-sm-6">
                        <input type="number" value="{{ $rowDesigner['price_yearly'] }}" align="right" size="8" width="40px" name="price_yearly" placeholder="Price Yearly"/>
                    </div>
                </div>

                <div class="form-group bg-default" style="padding-top:120px">
                    <label for="price_yearly" class="col-sm-6 control-label">Kamar Tersedia</label>
                    <div class="col-sm-6">
                        <input type="hidden" name="room_available" value="{{ $rowDesigner['room_available'] }}" />
                        <input type="number" @if ($rowDesigner['hide_allotment']) disabled @endif value="{{ $rowDesigner['room_available'] }}" align="right" size="8" width="40px" name="room_available" placeholder="Kamar Tersedia"/>
                    </div>
                </div>
                <div class="form-group bg-default">
                    <label for="price_yearly" class="col-sm-6 control-label">Jumlah Kamar</label>
                    <div class="col-sm-6">
                        <input type="hidden" name="room_count" value="{{ $rowDesigner['room_count'] }}" />
                        <input type="number" @if ($rowDesigner['hide_allotment']) disabled @endif value="{{ $rowDesigner['room_count'] }}" align="right" size="8" width="40px" name="room_count" placeholder="Jumlah Kamar"/>
                    </div>
                </div>
                @if ($rowDesigner['hide_allotment'])
                    <div style="padding-left:210px; padding-right:0; padding-top:50px">
                        <a type="button" class="btn btn-sm btn-primary" href="{{ url('admin/room/' . $rowDesigner['id'] . '/room-unit') }}">Atur Ketersediaan</a>
                    </div>
                    @if (
                        $rowDesigner['room_unit_empty_with_contract'] > 0 &&
                        ($rowDesigner['room_unit_empty'] != $rowDesigner['room_available'] || $rowDesigner['room_unit_occupied'] != ($rowDesigner['room_count'] - $rowDesigner['room_available']))
                    )
                        <p class="text-danger h6 col-sm-12">
                            <i class="fa fa-info-circle"></i> There are {{ $rowDesigner['room_unit_empty_with_contract'] }} available room(s) related with contract.<br>
                            <i class="fa fa-info-circle"></i> Room Allotment & Designer are not synced!
                        </p>
                        <div style="padding-left:20px">
                            <label for="{{ $rowDesigner['id'] }}" class="btn btn-xs btn-success">Sync Data</label>
                        </div>
                    @elseif (
                        $rowDesigner['room_unit_empty'] != $rowDesigner['room_available'] ||
                        $rowDesigner['room_unit_occupied'] != ($rowDesigner['room_count'] - $rowDesigner['room_available'])
                    )
                        <p class="text-danger h6 col-sm-12">
                            <i class="fa fa-info-circle"></i> Room Allotment & Designer are not synced!
                        </p>
                        <div style="padding-left:20px">
                            <label for="{{ $rowDesigner['id'] }}" class="btn btn-xs btn-success">Sync Data</label>
                        </div>
                    @elseif ($rowDesigner['room_unit_empty_with_contract'] > 0)
                        <p class="text-danger h6 col-sm-12">
                            <i class="fa fa-info-circle"></i> There are {{ $rowDesigner['room_unit_empty_with_contract'] }} available room(s) related with contract.<br>
                        </p>
                        <div style="padding-left:20px">
                            <label for="{{ $rowDesigner['id'] }}" class="btn btn-xs btn-success">Sync Data</label>
                        </div>
                    @endif
                @endif

                <hr/>
                <div class="form-group bg-default" style="padding-left:20px">
                       <div class="row">
                           <label for="price_yearly" class="col-sm-6 control-label">No Hp terverifikasi</label>
                           <div class="col-sm-6">
                           <select class="form-control" name="is_verified_phone">
                               @for ($i=0;$i<=1;$i++)
                                 @if ($i == $rowDesigner['is_verified_phone'])
                                  <option value="{{ $i }}" selected="true">{{ $i == 0 ? "No" : "Yes" }}</option>
                                 @else
                                  <option value="{{ $i }}">{{ $i == 0 ? "No" : "Yes" }}</option>
                                 @endif
                               @endfor
                           </select>
                           </div>
                       </div>

                      <div class="row">
                            <label for="price_yearly" class="col-sm-6 control-label">Alamat Terverifikasi</label>
                       <div class="col-sm-6">
                       <select class="form-control" name="is_verified_address">

                           @for ($i=0;$i<=1;$i++)
                             @if ($i == $rowDesigner['is_verified_address'])
                              <option value="{{ $i }}" selected="true">{{ $i == "0" ? "No" : "Yes" }}</option>
                             @else
                              <option value="{{ $i }}">{{ $i == "0" ? "No" : "Yes" }}</option>
                             @endif
                           @endfor
                       </select>
                       </div>
                      </div>

                      <div class="row">
                          <label for="price_yearly" class="col-sm-6 control-label">Kost Dikunjungi</label>
                       <div class="col-sm-6">
                       <select class="form-control" name="is_visited_kost">
                           @for ($i=0;$i<=1;$i++)
                             @if ($i == $rowDesigner['is_visited_kost'])
                              <option value="{{ $i }}" selected="true">{{ $i == 0 ? "No" : "Yes" }}</option>
                             @else
                              <option value="{{ $i }}">{{ $i == 0 ? "No" : "Yes" }}</option>
                             @endif
                           @endfor
                       </select>
                       </div>
                      </div>


                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group bg-default">
                    <label for="owner_phone" class="col-sm-6 control-label">Owner Phone</label>
                    <div class="col-sm-6">
                        <input value="{{ $rowDesigner['owner_phone'] }}" align="right" width="100%" name="owner_phone" placeholder="Owner Phone"/>
                    </div>
                </div>
                <div class="form-group bg-default">
                    <label for="manager_phone" class="col-sm-6 control-label">Manager Phone</label>
                    <div class="col-sm-6">
                        <input value="{{ $rowDesigner['manager_phone'] }}" align="right" width="100%" name="manager_phone" placeholder="Manager Phone"/>
                    </div>
                </div>
                <div class="form-group bg-default" style="padding-top:50px">
                    <label for="manager_phone" class="col-sm-6 control-label">Gender</label>
                    <div class="col-sm-6">
                        <select name="gender" class="form-control">
                             @foreach($gender AS $key => $value)
                                 @if ($rowDesigner['gender'] == $key)
                                  <option value="{{ $key }}" selected="true">{{ $value }}</option>
                                 @else
                                  <option value="{{ $key }}">{{ $value }}</option>
                                 @endif
                             @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group bg-default" style="padding-top:50px">
                    <label for="manager_phone" class="col-sm-6 control-label">Phone Status</label>
                    <div class="col-sm-6">
                        <select class="form-control" name="expired_phone">
                             @for ($i=0;$i<=1;$i++)
                                @if ($rowDesigner['expired_phone'] == $i)
                                  <option value="{{ $i }}" selected="true">{{ $i == 0 ? "Available" : "Expired" }}</option>
                                @else
                                  <option value="{{ $i }}">{{ $i == 0 ? "Available" : "Expired" }}</option>
                                @endif
                             @endfor
                        </select>
                    </div>
                </div>
                <div class="form-group bg-default" style="padding-top:50px">
                    <label for="kos_status" class="col-sm-6 control-label">Kos Status</label>
                    <div for="kos_status" class="col-sm-6">{{ $rowDesigner['kos_status'] }}</div>
                </div>
                <div class="form-group bg-default" style="padding-top:20px">
                    <label for="manager_phone" class="col-sm-6 control-label">Kost ID</label>
                    <label for="manager_phone" class="col-sm-6 control-label">{{ $rowDesigner['id'] }}</label>
                </div>
                <div class="form-group bg-default" style="padding-top:40px">
                    @if (!is_null($rowDesigner['expired_by']))
                        <label class="col-md-11 alert alert-danger">Expired by <strong>{{ $rowDesigner['expired_by'] }}</strong></label>
                    @endif
                </div>

            </div>
            <div style="bottom:0px; right:10px; position:absolute">
                <input type="submit" class="btn btn-success btn-sm btn-md" value="UPDATE" onclick="this.form.submit(); this.disabled = true;"/>
                <a href="{{ url()->route('admin.room.history-owner', [$rowDesigner['id'], $rowDesigner['owner_id']]) }}" target="_blank" rel="noopener" class="btn btn-sm btn-primary btn-md" id="buttonSearch"><i class="fa fa-search">&nbsp;</i>History</a>
                <input type="button" class="squarepantsOwner col-md-5 btn btn-warning" id="{{ $rowDesigner['id'] }}" style="border-radius: 0;" value="Telp Pemilik"/>
            </div>

            <input type="hidden" id="id_id{{ $rowDesigner['id'] }}" value="{{ $rowDesigner['id'] }}">
            <input type="hidden" id="kos_name{{ $rowDesigner['id'] }}" value="{{ $rowDesigner['name'] }}">
            <input type="hidden" id="user_login{{ $rowDesigner['id'] }}" value="{{ Auth::user()->email }}">
            <input type="hidden" id="owner_phone{{ $rowDesigner['id'] }}" value="{{ $rowDesigner['owner_phone'] }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        </td>
    </form>
    <form method="POST" action="{{ url('admin/room/' . $rowDesigner['id'] . '/room-unit/sync') }}">
        @csrf
        <input type="submit" id="{{ $rowDesigner['id'] }}" class="hidden" onclick="this.disabled=true;this.form.submit();" />
    </form>
    </tr>
    @endforeach
</tbody>
<tfoot>
    <tr>
        <th width="30px;">No</th>
        <th style="width:150px">Photo</th>
        <th>Name / Note</th>
        <th>Owner Phone / <br/>
            Manager Phone</th>
        <th>Price</th>
        <th>Gender</th>
        <th>Phone Status</th>
        <th>Room</th>
        <th>Submit</th>
    </tr>
</tfoot>
</table>