@extends('admin.layouts.main')
@section('style')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/dropzone.css">
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Upload Image for Room</h3>
        </div>
        <div class="form-horizontal form-bordered" id="formUploadPhotos">
            <div class="box-body no-padding">
                <div class="form-group bg-default">
                    <label for="addPhotosFormCover" class="col-sm-2 control-label">Foto Cover</label>
                    {{--@if(!array_key_exists('cover', $photos))--}}
                    <div class="col-sm-6">
                        @include('admin.contents.stories.partial.upload', ['url' => 'admin/media', 'mediaName' => 'Cover', 'mediaType' => 'style_photo', 'watermarking' => 'true'])
                    </div>
                    {{--@endif--}}
                    <div class="col-sm-4">
                        @if(isset($photos['cover']))
                            @foreach($photos['cover'] as $photo)
                                <div class="col-md-3">
                                    {!! link_to(url('admin/media',$photo['id']),'Delete', array('class' => 'fa fa-times-circle')) !!}
                                    <a href="">
                                        <img class="img-responsive" src="{{ $photo['photo_url']['small'] }}" alt="">
                                    </a>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
                <div class="form-group bg-default">
                    <label for="addPhotosFormBuilding" class="col-sm-2 control-label">Foto Bangunan</label>
                    <div class="col-sm-6">
                        @include('admin.contents.stories.partial.upload', ['url' => 'admin/media', 'mediaName' => 'Building', 'mediaType' => 'style_photo', 'watermarking' => 'true'])
                    </div>
                    <div class="col-sm-4">
                        @if(isset($photos['bangunan']))
                            @foreach($photos['bangunan'] as $photo)
                                <div class="col-md-3">
                                    {!! link_to(url('admin/media',$photo['id']),'Delete', array('class' => 'fa fa-times-circle')) !!}
                                    <a href="">
                                        <img class="img-responsive" src="{{ $photo['photo_url']['small'] }}" alt="">
                                    </a>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
                <div class="form-group bg-default">
                    <label for="addPhotosFormRoom" class="col-sm-2 control-label">Foto Kamar</label>
                    <div class="col-sm-6">
                        @include('admin.contents.stories.partial.upload', ['url' => 'admin/media', 'mediaName' => 'Room', 'mediaType' => 'style_photo', 'watermarking' => 'true'])
                    </div>
                    <div class="col-sm-4">
                        @if(isset($photos['kamar']))
                            @foreach($photos['kamar'] as $photo)
                                <div class="col-md-3">
                                    {!! link_to(url('admin/media',$photo['id']),'Delete', array('class' => 'fa fa-times-circle')) !!}
                                    <a href="">
                                        <img class="img-responsive" src="{{ $photo['photo_url']['small'] }}" alt="">
                                    </a>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
                <div class="form-group bg-default">
                    <label for="addPhotosFormBathroom" class="col-sm-2 control-label">Foto Kamar Mandi</label>
                    <div class="col-sm-6">
                        @include('admin.contents.stories.partial.upload', ['url' => 'admin/media', 'mediaName' => 'Bathroom', 'mediaType' => 'style_photo', 'watermarking' => 'true'])
                    </div>
                    <div class="col-sm-4">
                        @if(isset($photos['kamar-mandi']))
                            @foreach($photos['kamar-mandi'] as $photo)
                                <div class="col-md-3">
                                    {!! link_to(url('admin/media',$photo['id']),'Delete', array('class' => 'fa fa-times-circle')) !!}
                                    <a href="">
                                        <img class="img-responsive" src="{{ $photo['photo_url']['small'] }}" alt="">
                                    </a>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
                <div class="form-group bg-default">
                    <label for="addPhotosFormOther" class="col-sm-2 control-label">Foto Fasilitas Lain</label>
                    <div class="col-sm-6">
                        @include('admin.contents.stories.partial.upload', ['url' => 'admin/media', 'mediaName' => 'Other', 'mediaType' => 'style_photo', 'watermarking' => 'true'])
                    </div>
                    <div class="col-sm-4">
                        @if(isset($photos['lainnya']))
                            @foreach($photos['lainnya'] as $photo)
                                <div class="col-md-3">
                                    {!! link_to(url('admin/media',$photo['id']),'Delete', array('class' => 'fa fa-times-circle')) !!}
                                    <a href="">
                                        <img class="img-responsive" src="{{ $photo['photo_url']['small'] }}" alt="">
                                    </a>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
                <div class="form-group bg-default">
                    <label for="addPhotosFormRound" class="col-sm-2 control-label">Foto 360 derajat</label>
                    <div class="col-sm-6">
                        @include('admin.contents.stories.partial.upload', ['url' => 'admin/media', 'mediaName' => 'Round', 'mediaType' => 'round_style_photo', 'watermarking' => 'true'])
                    </div>
                    <div class="col-sm-4">
                        @if(!is_null($photo360))
                            <div class="col-md-3">
                                <a href="{{ route('admin.media.round.destroy', [$room->id, $room->photo_round_id]) }}" class="fa fa-times-circle">Delete</a>
                                <a href="">
                                    <img class="img-responsive" src="{{ $photo360['small'] }}" alt="">
                                </a>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            {!! Form::open(['route' => ['admin.room.submit', $room->id], 'method' => 'post']) !!}
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <a href="/admin/room">
                        <button type="button" class="btn btn-default">Cancel</button>
                    </a>
                </div>
            </div>
            <div id="wrapper">
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/dropzone.js"></script>
    <script>
        Dropzone.autoDiscover = false;

        $('#addPhotosFormCover').dropzone({
            paramName : "media",
            maxFilesize : 6,
            maxFiles : 1,
            accepFiles: '.jpg, .jpeg, .png, .bmp',
            dictDefaultMessage : 'Upload photo for cover',
            addRemoveLinks: true,
            init: function () {
                this.on("removedfile", function (file) {
                    //console.log(file.id)
                    $.get("/admin/media/"+file.id+"/delete", function(data, status) {
                        console.log("Sukses hapus");
                    });
                });
            },
            success : function (file, response) {
                $('#wrapper input[name="cover[]"]').remove();
                $('#wrapper').append('<input type="hidden" name="cover[]" value="' +response.media.id+ '">');
                file["id"] = response.media.id;
            }
        });

        $('#addPhotosFormRound').dropzone({
            paramName : "media",
            maxFilesize : 8,
            maxFiles: 1,
            accepFiles: '.jpg, .jpeg, .png, .bmp',
            dictDefaultMessage : 'Upload photo for 360 photos',
            addRemoveLinks: true,
            init: function () {
                this.on("removedfile", function (file) {
                    //console.log(file.id)
                    $.get("/admin/media/"+file.id+"/delete", function(data, status) {
                        console.log("Sukses hapus");
                    });
                });
            },
            success : function (file, response) {
                $('#wrapper input[name="round[]"]').remove();
                $('#wrapper').append('<input type="hidden" name="round[]" value="' +response.media.id+ '">');
                file["id"] = response.media.id;
            }
        });
        $('#addPhotosFormBuilding').dropzone({
            paramName : "media",
            maxFilesize : 6,
            accepFiles: '.jpg, .jpeg, .png, .bmp',
            dictDefaultMessage : 'Upload photo for building',
            addRemoveLinks: true,
            init: function () {
                this.on("removedfile", function (file) {
                    //console.log(file.id)
                    $.get("/admin/media/"+file.id+"/delete", function(data, status) {
                        console.log("Sukses hapus");
                    });
                });
            },
            success : function (file, response) {
                $('#wrapper').append('<input type="hidden" name="building[]" value="' +response.media.id+ '">');
                file["id"] = response.media.id;
            }
        });
        $('#addPhotosFormRoom').dropzone({
            paramName : "media",
            maxFilesize : 6,
            accepFiles: '.jpg, .jpeg, .png, .bmp',
            dictDefaultMessage : 'Upload photo for rooms',
            addRemoveLinks: true,
            init: function () {
                this.on("removedfile", function (file) {
                    //console.log(file.id)
                    $.get("/admin/media/"+file.id+"/delete", function(data, status) {
                        console.log("Sukses hapus");
                    });
                });
            },
            success : function (file, response) {
                $('#wrapper').append('<input type="hidden" name="room[]" value="' +response.media.id+ '">');
                file["id"] = response.media.id;
            }
        });
        $('#addPhotosFormBathroom').dropzone({
            paramName : "media",
            maxFilesize : 6,
            accepFiles: '.jpg, .jpeg, .png, .bmp',
            addRemoveLinks: true,
            init: function () {
                this.on("removedfile", function (file) {
                    //console.log(file.id)
                    $.get("/admin/media/"+file.id+"/delete", function(data, status) {
                        console.log("Sukses hapus");
                    });
                });
            },
            dictDefaultMessage : 'Upload photo for bathroom',
            success : function (file, response) {
                $('#wrapper').append('<input type="hidden" name="bathroom[]" value="' +response.media.id+ '">');
                file["id"] = response.media.id;
            }
        });
        $('#addPhotosFormOther').dropzone({
            paramName : "media",
            maxFilesize : 6,
            addRemoveLinks: true,
            init: function () {
                this.on("removedfile", function (file) {
                    //console.log(file.id)
                    $.get("/admin/media/"+file.id+"/delete", function(data, status) {
                        console.log("Sukses hapus");
                    });
                });
            },
            accepFiles: '.jpg, .jpeg, .png, .bmp',
            dictDefaultMessage : 'Upload photo for other facilities',
            success : function (file, response) {
                $('#wrapper').append('<input type="hidden" name="other[]" value="' +response.media.id+ '">');
                file["id"] = response.media.id;
            }
        });
    </script>
@endsection
