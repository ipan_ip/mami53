@extends('admin.layouts.main')

@section('style')
    <link rel="stylesheet" href="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.css">
    <style>
        .custom-tab {
            margin-bottom: 20px;
        }

        .custom-tab.nav-tabs > li.active > a,
        .custom-tab.nav-tabs > li.active > a:hover,
        .custom-tab.nav-tabs > li.active > a:focus {
            border: unset;
            border-bottom: 2px solid #3c8dbc;
            font-weight: bold;
        }

        .histories .fixed-table-loading.table.table-bordered.table-hover {
            min-height: 300px;
        }
    </style>
@endsection

@section('content')
    <div class="box box-primary histories">
        <ul class="nav nav-tabs custom-tab" id="history-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link" id="update-kos-tab" data-toggle="tab" href="#update-kos" role="tab" aria-controls="update-kos" aria-selected="true">History Update Kos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="available-room-tab" data-toggle="tab" href="#available-room" role="tab" aria-controls="available-room" aria-selected="false">History Log</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="kos-status-tab" data-toggle="tab" href="#kos-status" role="tab" aria-controls="kos-status" aria-selected="false">History Status Kos</a>
            </li>
        </ul>

        <table id="history-table" class="table">
            <thead>
                <tr>
                    <th data-field="order">Id</th>
                    <th data-field="timestamp" data-formatter="timestampFormatter">Timestamp</th>
                    <th data-field="history">History</th>
                    <th data-field="editor">Update By</th>
                </tr>
            </thead>
        </table>
    </div>
@stop

@section('script')
<script src="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
<script type="text/javascript">
    function timestampFormatter(value) {
        return value ?
            moment(new Date(value)).format('YYYY/MM/DD HH:mm:ss') : '-';
    }

    $(function() {
        const $table = $('#history-table');
        const roomId = `{{ request()->id }}`;
        const ownerId = `{{ request()->owner_id }}`;
        let offset = 0;

        const generateEditor = function(updateBy, ownerId) {
            if (updateBy.name || updateBy.email) {
                return updateBy.is_owner
                    ? `Owner (${updateBy.name} ${ownerId})`
                    : `Admin (${updateBy.email})`
            } else {
                return '-'
            }
        }

        const roomUnitHistoryTemplate = function (log) {
            function filterObject(obj) {
                const arr = ['name','floor','occupied'].reduce(
                    (acc, key) => ([...acc, `${key}: ${obj[key]}`]), []
                );
                return `{ ${arr.join(', ')} }`
            }

            switch (log.action) {
                case 'create':
                    return `<b>Create</b> ${filterObject(log.old_value)}`
                
                case 'update':
                    return `<b>Update</b> ${filterObject(log.old_value)} to ${filterObject(log.new_value)}`
                
                case 'delete':
                    return `<b>Delete</b> ${filterObject(log.old_value)}`

                default:
                    return '-'
            }
        }
        
        const statusTransformer = {
            'draft2': 'edited'
        };

        const options = {
            '#update-kos': {
                url: `{{ route('admin.room.history', ['id' => request()->id]) }}`,
                responseHandler: function(res) {
                    res.data = res.data.map((d, index) => ({
                        ...d,
                        order: offset + index + 1,
                        history: `Changed ${d.field} from <b>${d.old_value}</b> to <b>${d.new_value}</b>`,
                        editor: generateEditor(d.update_by, res.owner_id)
                    }));
                    return res;
                }
            },
            '#available-room': {
                url: `{{ route('admin.room.room-unit.log', ['id' => request()->id ]) }}`,
                responseHandler: function(res) {
                    res.data = res.revisionHistory.map((d, index) => ({
                        ...d,
                        order: offset + index + 1,
                        timestamp: d.created_at,
                        history: roomUnitHistoryTemplate(d),
                        editor: generateEditor(d.update_by, ownerId)
                    }));
                    return res;
                }
            },
            '#kos-status': {
                url: `{{ route('admin.room.history-verify', ['ownerId' => request()->owner_id]) }}`,
                responseHandler: function(res) {
                    res.data = res.data.map((d, index) => ({
                        ...d,
                        order: offset + index + 1,
                        history: `Status kos <b>${statusTransformer[d.history] || d.history}</b>`,
                        editor: generateEditor(d.update_by, ownerId)
                    }));
                    return res;
                }
            }
        }

        const getOptionKey = function(key) {
            return options[key] ? key : '#update-kos'
        };

        const initializeTable = function(option) {
            offset = 0;
            // destroy existing table before init new table
            $table.bootstrapTable('destroy');
            // init new table
            $table.bootstrapTable({
                ...option,
                dataField: 'data',
                pagination: true,
                pageSize: 10,
                sidePagination: 'server',
                pageList: [],
                queryParams: function (params) {
                    offset = params.offset;
                    return { offset: params.offset }
                }
            });
        }

        $('.nav-link').bind('click', function (e) {
            const tab = e.target;
            const optionSelected = options[getOptionKey($(tab).attr('href'))];

            initializeTable(optionSelected);
        });

        $(document).ready(function() {
            const currentOptionKey = getOptionKey(window.location.hash);
            const selectedTab = $(`#history-tab a[href="${currentOptionKey}"]`);
            selectedTab.click();
        });
    });
</script>
@endsection
