@extends('admin.layouts.main')
@section('content')

<!-- table -->
<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">{{ $boxTitle }}</h3>
        <div class="box-tools pull-right">
            <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
            <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
        </div>
    </div><!-- /.box-header -->
    <div class="box-body no-padding">
        <div class="horizontal-wrapper">
            <div class="btn-horizontal-group bg-default">

            </div>
        </div>

        @if ($promotion_total > 1)
          <h4>Karena ada duplikat kos, hapus data dengan owner yang salah</h4>
          <table class="table table-striped">
              @foreach($room as $key => $value)
              <tr>
                  <td>{{ $value->room->name }}</td>
                  <td>
                      {{ !is_null($value->premium_request) ? $value->premium_request->user->phone_number : "" }}
                  </td>
                  <td>
                    <a href="/admin/request/kost/delete/{{ !is_null($value->premium_request) ? $value->designer_id : $value->id }}?owner_id={{ !is_null($value->premium_request) ? $value->premium_request->user_id : 0 }}" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-xs btn-danger">Delete</a>
                  </td>
              </tr>
              @endforeach
          </table>
        @else

        <table id="tableListTag" class="table table-striped">
            <thead>
                <tr>
                    <th>How</th>
                    <th>Keterangan</th>
                   
                </tr>
            </thead>
            <tbody>
              @if ($rowsRoom == null) 
                <tr>
                   <td colspan="5">Tidak ada data</td>
                </tr>
              @else
                 <tr>
                   <td><strong>Owner</strong></td>
                   <td>

                      @if ($useres == null)
                       Tidak ada owner
                      @else 
                       {{ $useres->user->id }} -
                       {{ $useres->user->name }}
                     @endif
                     </td>
                 </tr>
                 <tr>
                   <td>
                     <strong>Kost</strong>
                   </td>
                   <td>{{ $rowsRoom->id }} - {{ $rowsRoom->name }}</td>
                 </tr>

                 <tr>
                   <td><strong>Owner Phone</strong></td>
                   <td>{{ $rowsRoom->owner_phone }}</td>
                 </tr>

                 <tr>
                   <td><strong>Area City</strong></td>
                   <td>{{ $rowsRoom->area_city }}</td>
                 </tr>

                 <tr>
                   <td><strong>Area Subdistrict</strong></td>
                   <td>{{ $rowsRoom->area_subdistrict }}</td>
                 </tr>

                 <tr>
                   <td><strong>Total Saldo Owner</strong></td>
                   <td>
                     @if ($premiumRequest == null)
                      
                     @else
                           {{ $premiumRequest->view }}
                     @endif
                   </td>
                 </tr>
                 
                 <tr>
                   <td><strong>Saldo Teralokasi saat ini</strong></td>
                   <td>
                     @if ($view_promote == null) 
                         -
                     @else
                       {{ $view_promote->total }}
                     @endif        
                   </td>
                 </tr>

                 <tr>
                   <td><strong>Status Kost Promoted</strong></td>
                   <td>
                     @if ($view_promote == null) 
                         -
                     @else
                       @if ($view_promote->is_active == 1)
                         <span class="label label-success">Active</span>
                       @else
                         <span class="label label-default">Not Active</span>
                       @endif
                     @endif      
                   </td>
                 </tr>

                 <tr>
                  <td><strong>History hari ini</strong></td>
                  <td>
                      @foreach ($rowsRoom->ad_history AS $key => $value)
                          <strong>Type : </strong> {{ $value->type }} - <strong>Terpakai: </strong> {{ $value->total }} <br/>
                      @endforeach
                  </td>
                 </tr>

                 <tr>
                   <td><strong>History View Kost Ini</strong></td>
                   <td>
                     @if ($view_promote == null)

                     @else
                      {{ $view_promote->history }}
                     @endif
                   </td>
                 </tr>

                @if ($view_promote != null) 
                
                    @if ($view_promote->is_active == 1)
                         <tr>
                           <td><strong>Pemakaian View Berjalan</strong></td>
                           <td>{{ $view_promote->used }}</td>
                         </tr>
                    @endif     
                @endif 

                 <tr>
                   <td><strong>History Promote</strong></td>
                   <td>
                     @if ($history_promote == null) 
                     -
                     @else
                      @foreach ($history_promote AS $key => $history)

                            <strong>Start Date</strong> {{ $history->start_date }} <br/>
                            <strong>Start View</strong> {{ $history->start_view }} <br/>
                            <strong>End Date</strong> {{ $history->end_date }} <br/>
                            <strong>End View</strong> {{ $history->end_view }} <br/>
                            <strong>Used View</strong> {{ $history->used_view }} <br/>
                            ############################################################<br/>
                      @endforeach
                     @endif
                   </td>
                 </tr>
                 
                 <tr>
                    <td><strong>Pinned Kost</strong></td>
                    <td>
                      
                      @if (is_null($premiumRequest))
                          Belum premium
                      @else
                          @if ($useres->user->date_owner_limit > date('Y-m-d'))
                              <form class="form-horizontal" method="POST" action="{{ URL::to('admin/room/pinned') }}">
                              {{ csrf_field() }}  
                                <div class="form-group col-md-2">
                                  <select name="type" class="form-control">
                                      <option value="click">Klik</option>
                                  </select>
                                </div>
                                <div class="form-group col-md-5">
                                    <input type="hidden" name="user_id" value="{{ $useres->user->id }}">
                                    <input type="hidden" name="song_id" value="{{ $rowsRoom->song_id }}" />                                        
                                    <input type="number" name="allocation" value="{{ $total }}" placeholder="Masukkan jumlah view" class="form-control" />
                                </div>

                                <div class="form-group">
                                  <input type="submit" name="submit" value="Pinned" class="btn btn-success">
                                </div> 
                              </form>
                          @else
                              Belum aktif / Premium anda sudah habis
                          @endif 

                      @endif

                    </td>  
                 </tr>

                  <tr>
                    <td><strong>On / Off Promoted</strong></td>
                    <td>
                        @if ($view_promote == null) 
                          Belum promosi
                      @else
                        @if ($view_promote->is_active == 1)
                          <a href="{{ URL::to('admin/room/pinned/deactivate', $view_promote->id) }}" class="btn btn-danger">Off</a>
                        @else
                          Langsung klik <strong>Pinned</strong> untuk aktifkan premiumnya lagi
                        @endif
                      @endif
                    </td>
                  </tr>
                  <tr>
                    <td><strong>Allocation total</strong></td>
                    <td>
                       @if (!is_null($view_promote)) 
                        <form class="form-horizontal" method="POST" action="?allocation_id={{ $view_promote->id }}">
                          {{ csrf_field() }}
                          <div class="form-group col-md-3">
                            <input type="number" name="total" value="{{ $view_promote->total }}" class="form-control" required/>
                          </div>
                          <div class="form-group col-md-3">
                            <input type="number" name="used" value="{{ $view_promote->used }}" class="form-control" required/>
                          </div>
                          <div class="form-group col-md-3">
                            <input type="number" name="history" value="{{ $view_promote->history }}" class="form-control" required/>
                          </div>
                          <div class="form-group col-md-3">
                            <input type="number" name="daily_budget" value="{{ $view_promote->daily_budget }}" class="form-control" required/>
                          </div>
                          <div class="form-group col-md-3">
                            <input type="submit" class="btn btn-danger" value="Update" />
                          </div>
                        </form>
                      @endif
                    </td>
                  </tr>
               @endif
            </tbody>

        </table>
        @endif
    </div><!-- /.box-body -->
</div><!-- /.box -->
<!-- table -->

@stop
