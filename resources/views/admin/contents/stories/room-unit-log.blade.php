@extends('admin.layouts.main')
@section('content')
    <!-- table -->
    <div class="box box-primary">

        <div class="box-header">
            <h3 class="box-title">List History Edit Ketersediaan Kamar - {{ $kostName }}</h3>
        </div><!-- /.box-header -->

    <table class="table">

      <thead>
       <tr>
          <th>No</th>
          <th>Waktu</th>
          <th>History</th>
       </tr>   
      </thead>

      <tbody>
         
         @foreach ( $revisionHistory AS $key => $history ) 
          <tr>
              <td>{{ $key+1 }}</td>
              <td>{{ $history->created_at }}</td>
              @if($history->action == 'create')
                <td>{{ $history->user_name }} <b>Created</b> {{ json_encode(array_intersect_key($history->old_value, array_flip(['name','floor','occupied']))) }}</td>
              @elseif($history->action == 'update')
                <td>{{ $history->user_name }} <b>Updated</b> {{ json_encode(array_intersect_key($history->old_value, array_flip(['name','floor','occupied']))) }} <b>to</b> {{ json_encode(array_intersect_key($history->new_value, array_flip(['name','floor','occupied']))) }}</td>
              @else
                <td>{{ $history->user_name }} <b>Deleted</b> {{ json_encode(array_intersect_key($history->old_value, array_flip(['name','floor','occupied']))) }}</td>
              @endif


          </tr>
          @endforeach
      </tbody>
    </table>

    </div><!-- /.box -->
    <!-- table -->

   
@stop

@section('style')
    <!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> -->
@stop

@section('script')
<!-- Holder for Upload File Thumbnail -->
{{ \Html::script('assets/vendor/holder/holder.js') }}

<!-- Jquery Datepicker -->
<!-- <script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script> -->


@stop
