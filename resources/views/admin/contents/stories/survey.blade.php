@extends('admin.layouts.main')
@section('content')

<!-- table -->
<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">{{ $boxTitle }}</h3>
        <div class="box-tools pull-right">
            <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
            <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
        </div>
    </div><!-- /.box-header -->
    <div class="box-body no-padding">
        <div class="horizontal-wrapper">
            <div class="btn-horizontal-group bg-default">

            </div>
        </div>

        <table id="tableListTag" class="table table-striped">
            <thead>
                <tr>
                    <th width="30px;">No</th>
                    <th>Nama</th>
                    <th>Detail</th>
                    <th>Serious</th>
                    <th>Forward</th>
                    <th>Time Survey</th>
                    <th>Created at</th>
                </tr>
            </thead>
            <tbody>
              @if ($rowsRoom == null) {
                <tr>
                   <td colspan="5">Tidak ada survey</td>
                </tr>
              @else
                 @foreach ($rowsRoom->survey()->orderBy('id', 'DESC')->get() as $Key => $survey)
                  <tr>
                      <td>{{ $Key+1 }}</td>
                      <td>{{ $survey->name }}</td>
                      <td>Gender : {{ $survey->gender }} ,<br/> Birthday : {{ date('d M Y', strtotime($survey->birthday)) }} ,<br/>
                        Jobs : {{ $survey->jobs }} , <br/>
                        User Login : {{ $survey->user->name }} ,<br>
                        No hp : {{ $survey->user->phone_number }} </td>
                      <td>@if ($survey->serious == 1) 
                             <label class="label label-success">Yes</label>
                          @else
                             <label class="label label-danger">No</label>
                          @endif      
                      </td>
                      <td>@if ($survey->forward == 1)
                              <label class="label label-success">Yes</label>
                          @else
                              <label class="label label-danger">No</label>
                          @endif        
                      </td>
                      <td>{{ date('d M Y H:i', strtotime($survey->time)) }}</td>
                      <td>{{ date('d M Y H:i', strtotime($survey->created_at)) }}</td>
                  </tr>
                  @endforeach
               @endif
            </tbody>

        </table>
    </div><!-- /.box-body -->
</div><!-- /.box -->
<!-- table -->

@stop
