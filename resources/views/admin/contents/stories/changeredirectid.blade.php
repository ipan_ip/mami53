@extends('admin.layouts.main')
@section('content')
    <!-- table -->
    <div class="box box-primary" style="padding: 10px;">

        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
            <div class="box-tools pull-right">
                <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
            </div>
        </div><!-- /.box-header -->
       
		<form action="/admin/room/redirectkost" method="post">
		  <div class="form-group">
		    <label for="exampleInputFile">ID yang di maksud.</label>
		    <input type="hidden" value="{{ $designer_id }}" name="designer_id_lama">
		    <input type="text" class="form-control" name="designer_id" placeholder="masukkan id yang dituju." id="exampleInputFile">
		  </div>
		  <button type="submit" class="btn btn-default">Submit</button>
		</form>

    </div><!-- /.box -->
    <!-- table -->

   
@stop

@section('style')
    <!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> -->
@stop

@section('script')
<!-- Holder for Upload File Thumbnail -->
{{ \Html::script('assets/vendor/holder/holder.js') }}

<!-- Jquery Datepicker -->
<!-- <script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script> -->


@stop
