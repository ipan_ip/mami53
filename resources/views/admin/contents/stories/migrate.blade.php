@extends('admin.layouts.main')

@section('style')
	{{ HTML::style('assets/vendor/jquery-chosen/chosen.css') }}
	
	<style>
        .chosen-container-single .chosen-single {
        	height: 34px;
        	line-height: 34px;
        }
    </style>
@endsection

@section('content')
	<div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

		<form action="{{ URL::route('admin.room.migrate.post', $room->id) }}" method="POST" class="form-horizontal form-bordered">
			
			<div class="box-body no-padding">
				<div class="form-group bg-default">
					<label for="name" class="control-label col-sm-2">Nama</label>
					<div class="col-sm-10">
						<input type="text" name="name" id="name" class="form-control" value="{{ old('name', $room->name) }}">
					</div>
				</div>

				<div class="form-group bg-default">
					<label for="project_name" class="control-label col-sm-2">Nama Project</label>
					<div class="col-sm-10">
						<select name="project_id" id="project_id" class="form-control chosen-select">
							@foreach($apartmentProjects as $apartmentProject)
								<option value="{{ $apartmentProject->id }}">{{ $apartmentProject->name }}</option>
							@endforeach
						</select>	
					</div>
				</div>

				<div class="form-group bg-default">
					<label for="unit_type" class="control-label col-sm-2">Tipe Unit</label>
					<div class="col-sm-10">
						<select name="unit_type" class="form-control" id="unit_type">
							@foreach($typeOptions as $type)
								<option value="{{ $type }}" {{ old('unit_type', $room->unit_type) == $type ? 'selected="selected"' : ($type == 'Lainnya' && !in_array(old('unit_type', $room->unit_type), $typeOptions) ? 'selected="selected"' : '') }}>{{ $type }}</option>
							@endforeach
						</select>
					</div>
				</div>

				<div class="form-group bg-default">
					<label for="unit_type_name" class="control-label col-sm-2">Tipe Unit (custom)</label>
					<div class="col-sm-10">
						<input type="text" name="unit_type_name" id="unit_type_name" value="{{ old('unit_type_name', !in_array($room->unit_type, $typeOptions) ? $room->unit_type : '') }}" class="form-control">
					</div>
				</div>

				<div class="form-group bg-default">
					<label for="unit_number" class="control-label col-sm-2">Ukuran Unit</label>
					<div class="col-sm-10">
						<input type="text" name="unit_size" id="unit_size" value="{{ old('unit_size', $room->size) }}" class="form-control" placeholder="Ukuran Unit">
						<span class="help-block">Ukuran unit biasanya berupa angka (ex: 40, 30, 25)</span>
					</div>
				</div>				

				<div class="form-group bg-default">
					<label for="unit_number" class="control-label col-sm-2">Nomor Unit</label>
					<div class="col-sm-10">
						<input type="text" name="unit_number" id="unit_number" value="{{ old('unit_number', $room->unit_number) }}" class="form-control">
					</div>
				</div>

				<div class="form-group bg-default">
					<label for="floor" class="control-label col-sm-2">Lantai</label>
					<div class="col-sm-10">
						<input type="text" name="floor" id="floor" value="{{ old('floor', $room->floor) }}" class="form-control">
					</div>
				</div>

				<div class="form-group bg-default">
					<label for="is_furnished" class="control-label col-sm-2">Furnished</label>
					<div class="col-sm-10">
						<select name="is_furnished" class="form-control" id="is_furnished">
							@foreach($furnishedOptions as $key => $option)
								<option value="{{ $key }}" {{ $key == $room->is_furnished ? 'selected="selected"' : '' }}>{{ $option }}</option>
							@endforeach
						</select>
					</div>
				</div>

				<div class="form-group bg-default">
					<label for="maintenance_price" class="control-label col-sm-2">Biaya Maintenance / Bulan</label>
					<div class="col-sm-10">
						<input type="text" name="maintenance_price" id="maintenance_price" value="{{ old('maintenance_price', isset($apartmentUnitPriceComponents['maintenance']) ? $apartmentUnitPriceComponents['maintenance']->price_reguler : '') }}" class="form-control">
					</div>
				</div>

				<div class="form-group bg-default">
					<label for="parking_price" class="control-label col-sm-2">Biaya Parkir</label>
					<div class="col-sm-10">
						<input type="text" name="parking_price" id="parking_price" value="{{ old('parking_price', isset($apartmentUnitPriceComponents['parking']) ? $apartmentUnitPriceComponents['parking']->price_reguler : '') }}" class="form-control">
					</div>
				</div>

				<div class="form-group bg-default">
					<div class="col-sm-10 col-sm-push-2">
						<button type="submit" class="btn btn-primary">Save</button>
					</div>
				</div>
			</div>
		</form>

		
    </div>

@endsection

@section('script')

{{ HTML::script('assets/vendor/jquery-chosen/chosen.jquery.js') }}

<script>
	$(function() {
		$('#unit_type').on('change', function() {
			if($(this).val() == 'Lainnya') {
				$('#unit_type_name').removeAttr('disabled');
			} else {
				$('#unit_type_name').attr('disabled', 'disabled');
			}
		});

		$('#unit_type').trigger('change');

		var config = {
            '.chosen-select'           : {width: '100%'}
        };

        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }
	});
</script>
@endsection