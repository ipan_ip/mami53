@extends('admin.layouts.main')
@section('content')

<!-- table -->
<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">{{ $boxTitle }}</h3>
        <div class="box-tools pull-right">
            <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
            <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
        </div>
    </div><!-- /.box-header -->
    <div class="box-body no-padding">
        <div class="horizontal-wrapper">
            <div class="btn-horizontal-group bg-default">

            </div>
        </div>

        <table id="tableListTag" class="table table-striped">
            <thead>
                <tr>
                    <th width="30px;">No</th>
                    <th>Username</th>
                    <th>Phone Number</th>
                    <th>Status Call</th>
                    <th>Date Time</th>
                </tr>
            </thead>
            <tbody>
               @if ($calls == null)
                  <tr>
                    <td colspan="4" style="text-align: center">No list</td>
                  </tr>
               @else
                  @foreach ($calls AS $key => $value)
                   
                   <?php
                   $created_at = $value->created_at->format('d M Y');
                   
                   if ($value->created_at->isToday()) {
                       $dateTime = "Hari ini";
                   } else if ($value->created_at->isYesterday()) {
                       $dateTime = "Kemarin";
                   } else if ($value->created_at->format('Y') == $carbon->year){
                       $dateTime = $value->created_at->format('d M');
                   } else {
                       $dateTime = $created_at;
                   } ?>

                    <tr>
                      <td>{{ $key + 1 }}</td>
                      <td>{{ $value->user->name }}</td>
                      <td>{{ $value->user->phone_number }}</td>
                      <td>@if ($value->status_call == null)
                           <span class="label label-primary"> - </span>
                          @elseif ($value->status_call == 'connect')
                           <span class="label label-success">Connected</span>
                          @else 
                           <span class="label label-danger">Rejected</span> 
                          @endif
                      </td>
                      <td>{{ $dateTime }} - {{ $value->created_at->format('H:i') }}</td>
                    </tr>
                  @endforeach
               @endif
            </tbody>

        </table>
    </div><!-- /.box-body -->
</div><!-- /.box -->
<!-- table -->

@stop
