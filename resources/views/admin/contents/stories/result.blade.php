@extends('admin.layouts.main')
@section('content')
<div class="box box-primary">
  <div class="box-header">
      <h3 class="box-title">{{ $boxTitle }}</h3>
  </div><!-- /.box-header -->
  {{ Form::open(array('url' => $formAction, 'method' => $formMethod,
      'class' => 'form-horizontal form-bordered', 'role' => 'form', 'id' => 'formInsertStyle')) }}
      <div class="box-body no-padding">
            <div id="cards">
              <div id="formCards">
          @foreach($results as $key => $answer)
                <div class="form-group bg-info divider">
                  <div class="col-sm-12">
                    Result {{ $key + 1 }}
                  </div>
                </div>
                <div class="form-group bg-default">
                  <label for="inputSelectType1" class="col-sm-2 control-label">Type</label>
                  <div class="col-sm-2">
                    <select class="form-control" id="inputSelectType1" name="results[{{ $key }}][type]" tabindex="2" data-placeholder="Select Card Type">
                        <option value="text" @if($answer->type == 'text') selected @endif>Text</option>
                        <option value="image" @if($answer->type == 'image') selected @endif>Image</option>
                        <option value="gif" @if($answer->type == 'gif') selected @endif>GIF</option>
                    </select>
                  </div>
                </div>
                <div class="form-group bg-default">
                  <label for="title" class="col-sm-2 control-label">Title</label>
                  <div class="col-sm-10">
                    <input class="form-control" rows="3" id="cardInputTitle" name="results[{{ $key }}][title]" value="{{ $answer->title }}" placeholder="Card Title">
                  </div>
                </div>
                <div class="form-group bg-default">
                  <label for="inputDescription" class="col-sm-2 control-label">Description</label>
                  <div class="col-sm-10">
                    <textarea class="form-control" rows="3" id="cardInputDescription" name="results[{{ $key }}][description]" placeholder="Card Description">{{ $answer->description }}</textarea>
                  </div>
                </div>
                <div class="form-group bg-default" id="cardInputImage{{ $key + 1}}" @if($answer->type != 'image') hidden @endif>
                  <label for="fileupload" class="col-sm-2 control-label">Image/GIF</label>
                  <div class="col-sm-10">
                    <div id="cardUpload{{$key + 1}}" class="media">
                      <a class="pull-left thumbnail" id="wrapper-img" >
                        <img style="background: #EEE; width: 120px; height: 160px;" class="media-object" width="120px" height="160px" src="{{ $answer->photo }}" data-src="holder.js/120x160/thumbnail" alt="">
                      </a>
                      <div class="media-body">
                        <input id="fileupload{{ $key + 1 }}" class="form-control" type="text" name="media1" placeholder="URL Photo">
                        <br>
                        <span class="btn btn-info btn-sm fileinput-button" id="buttonUpload{{ $key + 1}}">
                            <i class="glyphicon glyphicon-plus"></i>
                            <span>&nbsp; Add Photo from URL </span>
                        </span>
                        <!-- The fileinput-button span is used to style the file input field as button -->
                        <span class="btn btn-primary btn-sm fileinput-button">
                            <i class="glyphicon glyphicon-plus"></i>
                            <span>&nbsp; Add Photo.. </span>
                            <!-- The file input field used as target for the file upload widget -->
                            <input id="fileupload" type="file" name="media">
                            <input id="photo_id" type="hidden" name="results[{{ $key }}][photo_id]" 
                              value="{{ $answer->photo_id }}">
                        </span>
                        <br>
                        <br>
                        <!-- The global progress bar -->
                        <div id="progress" class="progress">
                            <div class="progress-bar"></div>
                        </div>
                        <div class="col-sm-6">
                          <div id="original-file-info" class="row"></div>
                        </div>
                        <div class="col-sm-6">
                          <div id="uploaded-file-info" class="row"></div>
                        </div>
                        <br>
                        <!-- The container for the uploaded files -->
                        <div id="files" class="files"></div>
                      </div>
                    </div>
                  </div>
                  <input type="hidden" name="results[{{ $key }}][id]" value="{{ $answer->id }}" />
                </div>
                
        @endforeach
              </div>
            </div>

            @if(Request::segment(6) != 'edit')
            <div class="form-group bg-default">
              <label for="inputPrice" class="col-sm-2 control-label"></label>
              <div class="col-sm-offset-2 col-sm-10" style="margin-top: 10px;">
                  <a href="" type="button" id="addCard"
                    class="btn btn-default btn-sm">
                    <span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;Add Result
                  </a>
                  <a href="" type="button" id="removeCard"
                    class="btn btn-default btn-sm">
                    <span class="glyphicon glyphicon-minus"></span>&nbsp;&nbsp;Remove Result
                  </a>
                </div>
            </div>
            <div class="form-group bg-info divider">
              <div class="col-sm-12">
              </div>
            </div>
            @endif

            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="{{ $formCancel }}">
                  <button type="button" class="btn btn-default">Cancel</button>
                </a>
                <button type="reset" class="btn btn-default"
                  onclick="$('#formInsertStyle').bootstrapValidator('resetForm', true);">Reset</button>
              </div>
            </div>
      </div>
  {{ Form::close() }}<!-- /.form -->
</div><!-- /.box -->
@stop
@section('style')
{{ HTML::style('assets/vendor/jquery-chosen/chosen.css') }}
{{ HTML::style('assets/vendor/jquery-chosen-custom/chosen.css') }}
{{ HTML::style('assets/vendor/jquery-file-upload/css/jquery.fileupload.css') }}
@stop
@section('script')
{{ HTML::script('assets/vendor/jquery-chosen/chosen.jquery.js') }}
<script type="text/javascript">
  $(function()
  {
    var config = {
      '.chosen-select'           : {width: '100%'}
    }
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    }
  });
</script>
<!-- Holder for Upload File Thumbnail -->
{{ HTML::script('assets/vendor/holder/holder.js') }}
<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
{{ HTML::script('assets/vendor/jquery-file-upload/js/vendor/jquery.ui.widget.js') }}
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
{{ HTML::script('assets/vendor/jquery-file-upload/js/jquery.iframe-transport.js') }}
<!-- The basic File Upload plugin -->
{{ HTML::script('assets/vendor/jquery-file-upload/js/jquery.fileupload.js') }}
<!-- The File Upload processing plugin -->
{{ HTML::script('assets/vendor/jquery-file-upload/js/jquery.fileupload-process.js') }}
<!-- The File Upload validation plugin -->
{{ HTML::script('assets/vendor/jquery-file-upload/js/jquery.fileupload-validate.js') }}
<!-- File Upload Main -->
{{ HTML::script('assets/vendor/file-upload/index.js') }}
<script>
  $(function () {
    // Change this to the location of your server-side upload handler
    @if (Request::is('admin/*'))
      var url = "{{ url('admin/media') }}";
    @else
      var url = "{{ url('user/media') }}";
    @endif
    
    myFileUpload($('#wrapper-file-upload'), url, 'style_photo');

  @for($i=1;$i<=sizeof($results);$i++)
    myFileUpload($('#cardInputImage{{$i}}'), url, 'style_photo');
  @endfor
  })
  
</script>
<!-- Card -->
<script type="text/javascript">
   $("#inputSelectType1").change(function(){
      var select = $("#inputSelectType1 option:selected").val();

      switch(select){
        case "text":
          $('#cardInputImage1').hide();
          break;

        case "image":
        case "gif":
          $('#cardInputImage1').show();
          break;
      }
      switch(select){
        case "text":
        case "image":
          $('#cardInputSubType1').hide();
          break;

        case "quiz":
          $('#cardInputSubType1').show();
          break;
      }
   });

   /*add new card*/
      $("#addCard").click(function(){
          var validator= 0;
          var type    = 'type';
          var photo   = 'photo_id';
          var source  = 'source';
          var description = 'description';
          var title   = 'title';
          console.log($("#formCards div").length);
          var intId = $("#formCards div").length/19 + 1;
          var index = intId - 1;
          var html  = '<div class="form-group bg-info divider"><div class="col-sm-12">Result '+intId+'</div></div>';

          html += '<div class="form-group bg-default"><label for="inputSelect" class="col-sm-2 control-label">Type</label><div class="col-sm-2">';
          html += '<select class="form-control" id="inputSelectType'+intId+'" name="results['+index+']['+type+']" tabindex="2" data-placeholder="Select Card Type">';
          html += '<option value="text">Text</option><option value="image" selected>Image</option><option value="gif">GIF</option>';
          html += '</select></div></div>';
          
          html += '<div class="form-group bg-default"><label for="inputTitle" class="col-sm-2 control-label">Title</label><div class="col-sm-10">';
          html += '<input class="form-control" rows="3" id="cardInputTitle" name="results['+index+']['+title+']" placeholder="Result Title"></div></div>';
          html += '<div class="form-group bg-default"><label for="inputDescription" class="col-sm-2 control-label">Description</label><div class="col-sm-10">';
          html += '<textarea class="form-control" rows="3" id="cardInputDescription" name="results['+index+']['+description+']" placeholder="Result Description"></textarea></div></div>';
          html += '<div class="form-group bg-default" id="cardInputImage'+intId+'"><label for="fileupload" class="col-sm-2 control-label">Image/GIF</label><div class="col-sm-10">';
          html += '<div id="cardUpload'+intId+'" class="media"><a class="pull-left thumbnail" id="wrapper-img" ><img style="background: #EEE; width: 120px; height: 160px;" class="media-object" width="120px" height="160px" src="" data-src="holder.js/120x160/thumbnail" alt=""></a><div class="media-body">';
          html += '<input id="fileupload1" class="form-control" type="text" name="media1" placeholder="URL Photo"><br><span class="btn btn-info btn-sm fileinput-button" id="buttonUpload'+intId+'"><i class="glyphicon glyphicon-plus"></i><span>&nbsp; Add Photo from URL </span></span>&nbsp;';
          html += '<span class="btn btn-primary btn-sm fileinput-button"><i class="glyphicon glyphicon-plus"></i><span>&nbsp; Add Photo.. </span><input id="fileupload" type="file" name="media"><input id="photo_id" type="hidden" name="results['+index+']['+photo+']" value=""></span><span></span>';
          html += '<br><br><div id="progress" class="progress"><div class="progress-bar"></div></div>';
          html += '<div class="col-sm-6"><div id="original-file-info" class="row"></div></div>';
          html += '<div class="col-sm-6"><div id="uploaded-file-info" class="row"></div></div><br>';
          html += '<div id="files" class="files"></div></div></div></div></div>';
          html += '<input type="hidden" name="results['+index+'][id]" value="0" />';
          
          $("#formCards").append(html);

          /*Plih input berdasarkan select*/
          $("#inputSelectType"+intId).change(function(){
              var select = $("#inputSelectType"+intId+" option:selected").val();
              switch(select){
                case "text":
                  $('#cardInputImage'+intId).hide();
                  break;

                case "image":
                case "gif":
                  $('#cardInputImage'+intId).show();
                  break;
              }
              switch(select){
                case "text":
                case "image":
                  $('#cardInputSubType'+intId).hide();
                  break;

                case "quiz":
                  $('#cardInputSubType'+intId).show();
                  break;
              }
           });

          /*Upload foto*/
          // Change this to the location of your server-side upload handler
          @if (Request::is('admin/*'))
            var url = "{{ url('admin/media') }}";
          @else
            var url = "{{ url('media') }}";
          @endif
          
          myFileUpload($('#cardUpload'+intId), url, 'style_photo', validator);

          <?php $login_type = Request::is('admin/*')?'admin':'user'; ?>
          
          /*upload foto from url*/
          $("#buttonUpload"+intId).click(function(){
              var wrapper = $('#cardInputImage'+intId);
              var mediaUrl= wrapper.find('#fileupload1').val();
              var url     = "{{ url($login_type . '/mediaUrl?t=style_photo&url=') }}"+mediaUrl;
              console.log('as');
              uploadFromUrl(wrapper, url, mediaUrl, validator); 
          });
      });
    
    $("#removeCard").click(function(){
        var intId = $("#formCards div").length - 19;
        if(intId != 0){
          $('#formCards div').slice(intId).remove();
        }
    });

    $("#buttonCover").click(function(){
        var validator= 1;
        var wrapper = $('#wrapper-cover-upload');
        var mediaUrl= wrapper.find('#fileupload1').val();
        var url     = "{{ url($login_type . '/mediaUrl?t=user_photo&url=') }}"+mediaUrl;

        uploadFromUrl(wrapper, url, mediaUrl, validator);
        
        // $('#mediaUrl').val('http://localhost/stories/public/uploads/cache/data/user/2015-04-09/EXx0YEYX-240x320.jpg');
        // $('#coba').attr('src', 'http://localhost/stories/public/uploads/cache/data/user/2015-04-09/EXx0YEYX-240x320.jpg');
        // console.log(isi);      
    });

    $("#buttonThumbnail").click(function(){
        var validator= 1;
        var wrapper = $('#wrapper-thumbnail-upload');
        var mediaUrl= wrapper.find('#fileupload1').val();
        var url     = "{{ url($login_type . '/mediaUrl?t=user_photo&url=') }}"+mediaUrl;

        uploadFromUrl(wrapper, url, mediaUrl, validator); 
    });

  @for($i=1;$i<=sizeof($results);$i++)
    $("#buttonUpload{{$i}}").click(function()  {
        var validator= 0;
        var wrapper = $('#cardUpload{{$i}}');
        var mediaUrl= wrapper.find('#fileupload{{$i}}').val();
        var url     = "{{ url($login_type . '/mediaUrl?t=user_photo&url=') }}"+mediaUrl;

        uploadFromUrl(wrapper, url, mediaUrl, validator); 
    });
  @endfor
    
</script>
@stop