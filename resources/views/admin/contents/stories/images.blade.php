@extends('admin.layouts.main')

@section('style')
<style>
    span.apartment-color,
    tr.apartment-color td {
        color: #0D47A1;
    }
    tr.kost-active td {
        background-color: #C8E6C9;
    }

    .table>tbody>tr>td {
        vertical-align: middle;
    }

    .font-large {
        font-size: 1.5em;
    }

    .font-semi-large {
        font-size: 1.15em;
    }

    .font-grey {
        color: #9E9E9E;
    }

    .dropdown-menu>li>a:hover {
        color: #333
    }

    .label-grey {
        background-color: #CFD8DC;
    }

    /* Sweetalert */
    .swal-shown {
        height: auto !important;
    }

    .custom-swal {
        z-index: 10000 !important;
    }

    .custom-wide-swal {
        width: 600px !important;
    }
</style>
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
@endsection

@section('content')
<!-- table -->
<div class="box box-primary">
    <div class="box-body">
        <div class="horizontal-wrapper">
            <div class="btn-horizontal-group bg-default">
                <form action="" class="form-inline" method="get" style="text-align:right;padding:10px;">
                    @if(Auth::user())
                    <input type="text" name="q" class="form-control input-sm" placeholder="Nama / ID Kost" autocomplete="off" value="{{ request()->input('q') }}" style="width:360px!important; margin-right:10px!important;">
                    <input type="text" name="start_date" class="form-control input-sm datepicker"
                        placeholder="Start Date" value="{{ request()->input('start_date') }}" id="start-date"
                        autocomplete="off" style="margin-right:2px" />
                    <input type="text" name="end_date" class="form-control input-sm datepicker" placeholder="End Date"
                        value="{{ request()->input('end_date') }}" id="end-date" autocomplete="off"
                        style="margin-right:2px" />
                    @endif
                    <button class="btn btn-primary btn-sm" id="buttonSearch"><i
                            class="fa fa-search">&nbsp;</i>Search</button>
                </form>
            </div>
        </div>
        <table class="table">
            @foreach ($data as $key => $room)
            <tr class="{{ $room['is_active'] == 'true' ? 'kost-active' : '' }}">
                <td width="15%">
                    <small>{{ $room->id }}</small><br/>
                    <strong><a href="{{ $room->share_url }}" target="_blank" class="font-semi-large">{{ $room->name }}</a></strong>
                    <br />{{ $room->area_city }}
                </td>
                <td class="text-right">
                    @foreach ($room->cards as $card)
                    @if (strpos($card->photo['file_name'], '-re') === false)
                    <a href="#" data-card="{{ $card }}" class="photo">
                        <img src="{{ $card->photo_url['small'] }}" alt="{{ $card->description }}" height="80px"
                            class="img-rounded">
                    </a>
                    @else
                    <img src="{{ $card->photo_url['small'] }}" alt="{{ $card->description }}" height="80px"
                            class="img-rounded">
                    @endif
                    @endforeach
                </td>
            </tr>
            @endforeach
        </table>
    </div><!-- /.box-body -->
    <div class="box-body no-padding">
        {{ $data->appends(Request::except('page'))->links() }}
    </div>
</div><!-- /.box -->
<!-- table -->
@stop

@section('script')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.js"></script>
<script>
    $(function() {
        var token = '{{ csrf_token() }}';

        // Sweetalert functions
        function triggerAlert(type, message) {
            Swal.fire({
                type: type,
                customClass: {
                    container: 'custom-swal'
                },
                title: message
            });
        }

        function triggerLoading(message) {
            Swal.fire({
                title: message,
                customClass: {
                    container: 'custom-swal'
                },
                allowEscapeKey: false,
                allowOutsideClick: false,
                onOpen: () => {
                    swal.showLoading();
                }
            });
        }

        $('#select-sort-option').change(function(event){
            var url  = '{{ URL::route("admin.room.index") }}' + '?o=' + event.target.value;
            url += '&st=' + $('#start-date').val();
            url += '&en=' + $('#end-date').val();
            window.location = url;
        });

        $('.datepicker').datepicker({ dateFormat : 'yy-mm-dd'});

        $('.photo').click(e => {
            e.preventDefault();
            var data = $(e.currentTarget).data('card');

            Swal.fire({
                title: room.name,
                html: "<h5><strong>Pastikan foto ini tidak mempunyai watermark!</strong></h5><br>Lalu klik <strong>[Generate]</strong> untuk generate ulang watermark.",
                customClass: 'custom-wide-swal',
                confirmButtonText: 'Generate',
                showCancelButton: true,
                imageUrl: data.photo_url.large,
                preConfirm: (val) => {
                    return $.ajax({
                        type: 'POST',
                        url: "/admin/media/generate",
                        dataType: 'json',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: {
                            media_id: data.photo_id
                        },
                        success: (response) => {
                            return;
                        },
                        error: (error) => {
                            Swal.showValidationMessage('Request Failed: ' + error.message);
                        }
                    });
                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                if (result.value.success) {
                    Swal.fire({
                        type: 'success',
                        title: "Watermark berhasil di-generate ulang",
                        text: "Halaman akan di-refresh setelah Anda klik OK",
                        onClose: () => {
                            window.location.reload();
                        }
                    });
                } else {
                    Swal.fire({
                        type: 'error',
                        title: "Gagal!",
                        html: '<strong>' + result.value.message + '</strong>'
                    });
                }
            })
        });
    });

</script>
@stop