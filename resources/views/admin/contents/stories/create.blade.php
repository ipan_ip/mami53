@extends('admin.layouts.main')

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>
        {{ Form::open(array(
          'route'   => $formAction,
          'method'  => $formMethod,
          'class'   => 'form-horizontal form-bordered',
          'role'    => 'form',
          'id'      => 'formInsertDesigner')) }}
        {{ csrf_field() }}

        <div class="box-body no-padding">
            <div class="form-group bg-default">
                <label for="inputName" class="col-sm-2 control-label">Title</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Title"
                           id="inputName" name="name"  value="{{ old('name') }}">
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputMamirooms" class="col-sm-2 control-label">Mamirooms</label>
                <div class="col-sm-10">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="mamirooms" {{ old('mamirooms') == '1' ? 'checked' : '' }} value="1">
                            Mamirooms
                        </label>
                    </div>
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputTesting" class="col-sm-2 control-label">Testing</label>
                <div class="col-sm-10">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="testing" {{ old('testing') == '1' ? 'checked' : '' }} value="1">
                            Testing
                        </label>
                    </div>
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputSize" class="col-sm-2 control-label">Size</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Size"
                           id="inputSize" name="size" value="{{ old('size') }}" >
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputPriceDaily" class="col-sm-2 control-label">Price Daily</label>
                <div class="col-sm-10">
                    <input type="number" class="form-control" placeholder="Price Daily"
                           id="inputPriceDaily" name="price_daily" value="{{ old('price_daily') }}" >
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputPriceWeekly" class="col-sm-2 control-label">Price Weekly</label>
                <div class="col-sm-10">
                    <input type="number" class="form-control" placeholder="Price Weekly"
                           id="inputPriceWeekly" name="price_weekly" value="{{ old('price_weekly') }}" >
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputPriceMonthly" class="col-sm-2 control-label">Price Monthly</label>
                <div class="col-sm-10">
                    <input type="number" class="form-control" placeholder="Price Monthly"
                           id="inputPriceMonthly" name="price_monthly" value="{{ old('price_monthly') }}" >
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputPriceYearly" class="col-sm-2 control-label">Price Yearly</label>
                <div class="col-sm-10">
                    <input type="number" class="form-control" placeholder="Price Yearly"
                           id="inputPriceYearly" name="price_yearly" value="{{ old('price_yearly') }}" >
                    <div id="countTitle" style="font-weight:bold"></div>
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputName" class="col-sm-2 control-label">Price 3 month</label>
                <div class="col-sm-10">
                    <input type="number" class="form-control" placeholder="Price 3 month"
                    id="inputName" name="3_month" value="{{ old('3_month') }}" >
                    <div id="countTitle" style="font-weight:bold"></div>
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputName" class="col-sm-2 control-label">Price 6 month</label>
                <div class="col-sm-10">
                    <input type="number" class="form-control" placeholder="Price 6 month"
                    id="inputName" name="6_month" value="{{ old('6_month') }}" >
                    <div id="countTitle" style="font-weight:bold"></div>
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputFloor" class="col-sm-2 control-label">Floor</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Floor"
                           id="inputFloor" name="floor" value="{{ old('floor') }}">
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputGender" class="col-sm-2 control-label">Gender</label>
                <div class="col-sm-10">
                    <select class="form-control" id="inputGender" name="gender">
                        @foreach($genderOptions as $key => $gender)
                            <option value="{{ $key }}" {{ old('gender') == $key ? 'selected="selected"' : '' }}>
                                {{ $gender }}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>

            {{--<div class="form-group bg-default">--}}
                {{--<label for="inputGender" class="col-sm-2 control-label">Type</label>--}}
                {{--<div class="col-sm-10">--}}
                    {{--<select class="form-control" id="inputType" name="type">--}}
                        {{--<option value="Kost">Kost</option>--}}
                        {{--<option value="Aparkost">Apartkost</option>--}}
                        {{--<option value="Kostel">Kostel</option>--}}
                    {{--</select>--}}
                {{--</div>--}}
            {{--</div>--}}

            <div class="form-group bg-default">
                <label for="inputIndexed" class="col-sm-2 control-label">Indexed</label>
                <div class="col-sm-10">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" checked="checked" name="indexed" value="1">
                            Indexed
                        </label>
                    </div>
                </div>
            </div>

            <div class="form-group bg-info divider">
                <div class="col-sm-12">
                    Location
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputAddress" class="col-sm-2 control-label">Address</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Address"
                           id="inputAddress" name="address" value="{{ old('address') }}" >
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputGeoName" class="col-sm-2 control-label">Input Coordinate</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Geo Name"
                           id="inputGeoName" name="geo_name">
                </div>
                <div class="col-sm-10 col-sm-offset-2">
                    <div style="background: #EEE; width: 100%; height: 300px;">
                        <div id="map-canvas"></div>
                    </div>
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputLongitude" class="col-sm-2 control-label">Longitude</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Longitude"
                           id="inputLongitude" name="longitude" value="{{ old('longitude') }}">
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputLatitude" class="col-sm-2 control-label">Latitude</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Latitude"
                           id="inputLatitude" name="latitude" value="{{ old('latitude') }}">
                </div>
            </div>

            <div class="form-group bg-default">
                <label class="col-sm-2 control-label" for="inputSubdistrict">Subdistrict</label>
                <div class="col-sm-10">
                    <input id="inputSubdistrict" class="form-control"  name="area_subdistrict" placeholder="Subdistrict" value="{{ old('area_subdistrict') }}">
                </div>
            </div>

            <div class="form-group bg-default">
                <label class="col-sm-2 control-label" for="inputAreaCity">City/Regency</label>
                <div class="col-sm-10">
                    <input type="text" id="inputAreaCity" class="form-control input-city-regency" rows="3" name="area_city" placeholder="Kota" value="{{ old('area_city') }}" data-toggle="dropdown">
                </div>
            </div>

            <div class="form-group bg-default">
                <label class="col-sm-2 control-label" for="inputGuide" >Penunjuk Arah</label>
                <div class="col-sm-10">
                    <input id="inputGuide" class="form-control" name="guide" placeholder="Petunjuk Arah" value="{{ old('guide') }}">
                </div>
            </div>

            <div class="form-group bg-info divider">
                <div class="col-sm-12">
                    Information
                </div>
            </div>

            <!-- select -->
            <div class="form-group bg-default">
                <label for="inputConcern" class="col-sm-2 control-label">Tags</label>
                <div class="col-sm-10">
                    <select class="form-control chosen-select" multiple id="inputConcern" name="concern_ids[]" tabindex="2">
                        @foreach ($rowsConcern as $rowConcern)
                            <option value="{{ $rowConcern->id }}"
                                    {{ !is_null(old('concern_ids')) && in_array($rowConcern->id, old('concern_ids')) ? 'selected="selected"' : '' }}>{{ $rowConcern->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>

        <div class="form-group bg-default">
            <label for="inputFacRoomOther" class="col-sm-2 control-label">Fasilitas Kamar lainnya</label>
            <div class="col-sm-10">
                <input class="form-control" id="inputFacRoomOther" name="fac_room_other" placeholder="Fasilitas" value="{{ old('fac_room_other') }}">
            </div>
        </div>

        <div class="form-group bg-default">
            <label for="inputFacBathOther" class="col-sm-2 control-label">Fasilitas Kamar Mandi lainnya</label>
            <div class="col-sm-10">
                <input class="form-control" rows="3" id="inputFacBathOther" name="fac_bath_other" placeholder="Fasilitas" value="{{ old('fac_bath_other') }}">
            </div>
        </div>

        <div class="form-group bg-default">
            <label for="inputFacShareOther" class="col-sm-2 control-label">Fasilitas Bersama lainnya</label>
            <div class="col-sm-10">
                <input class="form-control" id="inputFacShareOther" name="fac_share_other" placeholder="fasilitas" value="{{ old('fac_share_other') }}">
            </div>
        </div>

        <div class="form-group bg-default">
            <label for="inputFacNearOther" class="col-sm-2 control-label">Fasilitas Dekat kos lainnya</label>
            <div class="col-sm-10">
                <input class="form-control" id="inputFacNearOther" name="fac_near_other" placeholder="fasilitas" value="{{ old('fac_near_other') }}">
            </div>
        </div>

        <div class="form-group bg-default">
            <label for="inputRemarks" class="col-sm-2 control-label">Keterangan lainnya</label>
            <div class="col-sm-10">
                <input class="form-control" id="inputRemarks" name="remarks" placeholder="keterangan" value="{{ old('remarks') }}">
            </div>
        </div>

        <div class="form-group bg-default">
            <label for="inputPriceRemark" class="col-sm-2 control-label">Keterangan Harga Lain</label>
            <div class="col-sm-10">
                <input class="form-control" rows="3" id="inputPriceRemark" name="price_remark" placeholder="Keterangan Harga Lain" value="{{ old('price_remark') }}">
            </div>
        </div>

        <div class="form-group bg-default">
            <label class="col-sm-2 control-label" for="inputRoomCount">Jumlah kamar</label>
            <div class="col-sm-10">
                <input class="form-control" id="inputRoomCount" name="room_count" placeholder="Total Kamar Yang Ada" value="{{ old('room_count') ? : 0 }}">
            </div>
        </div>

        <div class="form-group bg-default">
            <label class="col-sm-2 control-label" for="inputRoomAvailable">Jumlah kamar tersedia</label>
            <div class="col-sm-10">
                <input class="form-control" id="inputRoomAvailable" name="room_available" placeholder="Jumlah kamar kosong" value="{{ old('room_available') ? : 0 }}">
            </div>
        </div>

        <div class="form-group bg-default">
            <label for="inputDescriptionMother" class="col-sm-2 control-label">Description</label>
            <div class="col-sm-10">
                <textarea class="form-control" rows="3" id="inputDescriptionMother" name="description">{{ old('description') }}</textarea>
            </div>
        </div>

        <div class="form-group bg-info divider">
            <div class="col-sm-12">
                Contact
            </div>
        </div>

        <div class="form-group bg-default">
            <label class="col-sm-2 control-label" for="inputOwnerName">Nama Pemilik</label>
            <div class="col-sm-10">
                <input id="inputOwnerName" class="form-control" name="owner_name" placeholder="Nama Pemilik" value="{{ old('owner_name') }}">
            </div>
        </div>

        <div class="form-group bg-default">
            <label class="col-sm-2 control-label" for="inputOwnerPhone">No. HP Pemilik</label>
            <div class="col-sm-10">
                <input id="inputOwnerPhone" class="form-control" rows="3" name="owner_phone" placeholder="No. HP Pemilik" value="{{ old('owner_phone') }}">
            </div>
        </div>

        <div class="form-group bg-default">
            <label class="col-sm-2 control-label" for="inputManagerName">Nama Manajer</label>
            <div class="col-sm-10">
                <input id="inputManagerName" class="form-control" name="manager_name" placeholder="Nama Manajer" value="{{ old('manager_name') }}">
            </div>
        </div>

        <div class="form-group bg-default">
            <label class="col-sm-2 control-label" for="inputManagerPhone">No. HP Manajer</label>
            <div class="col-sm-10">
                <input id="inputManagerPhone" class="form-control" name="manager_phone" placeholder="No. HP Manajer" value="{{ old('manager_phone') }}">
            </div>
        </div>

        <div class="form-group bg-default">
            <label class="col-sm-2 control-label" for="inputAgentName">Nama Agen</label>
            <div class="col-sm-10">
                <input id="inputAgentName" class="form-control" name="agent_name" placeholder="Nama Agen" value="{{ old('agent_name') }}">
            </div>
        </div>

        <div class="form-group bg-default">
            <label class="col-sm-2 control-label" for="inputVerificator">Verifikator</label>
            <div class="col-sm-10">
                <input id="inputVerificator" class="form-control" name="verificator" placeholder="Nama Verifikator" value="{{ old('verificator') }}">
            </div>
        </div>

        <div class="form-group bg-default" for="inputAgentStatus">
            <label class="col-sm-2 control-label">Status Agent</label>
            <div class="col-sm-10">
                <select class="form-control" name="agent_status" id="inputAgentStatus">
                    @foreach ($agent_status AS $agent)
                        <option value="{{ $agent }}" {{ old('agent_status') == $agent ? 'selected="selecte"' : '' }} >{{ $agent }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">Save</button>
                <a href="{{ $formCancel }}">
                    <button type="button" class="btn btn-default">Cancel</button>
                </a>
                <button type="reset" class="btn btn-default"
                        onclick="$('#formInsertDesigner').bootstrapValidator('resetForm', true);">Reset</button>
            </div>
        </div>
    </div>
    {{ Form::close() }}<!-- /.form -->
    </div><!-- /.box -->
@stop

@section('style')
    {{ HTML::style('assets/vendor/jquery-chosen/chosen.css') }}
    {{ HTML::style('assets/vendor/jquery-chosen-custom/chosen.css') }}
    <link rel="stylesheet" href="/assets/vendor/summernote/summernote.css?v=05042018">

    <style>
        #chosenForm .chosen-choices {
            border: 1px solid #ccc;
            border-radius: 4px;
            min-height: 34px;
            padding: 6px 12px;
        }
        #chosenForm .form-control-feedback {
            /* To make the feedback icon visible */
            z-index: 100;
        }
    </style>
@stop

@section('script')
            <!-- page script -->
    {{ HTML::script('assets/vendor/jquery-chosen/chosen.jquery.js') }}

    <script src="/assets/vendor/summernote/summernote.min.js"></script>

<script type="text/javascript">
        $('#inputDescriptionMother').summernote({
            height: 300,
            toolbar: [
                ['style', ['style', 'bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['insert', ['link', 'table', 'hr']],
                ['misc', ['codeview']]
            ]
        });

</script>

    <script type="text/javascript">
        var config = {
            '.chosen-select'           : {width: '100%'}
        };
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }
    </script>

    <script> //count character
        function countCharTitle(val,maxlength) //title card
        {
            var len = val.value.length;
            if (len > maxlength) {
                val.value = val.value.substring(0, maxlength);
            } else {
                $('#countTitle').text(maxlength - len);
            }
        };
    </script>
    <!-- Google Map JS -->

    <script src="https://maps.googleapis.com/maps/api/js?libraries=places&language=id&key={{ENV('MIX_GOOGLE_MAPS_JAVASCRIPT_API_KEY')}}"></script>

    <script type="text/javascript">
        centerPos = {
            lat: {{ !is_null(old('latitude')) ? old('latitude') : '-7.7858485' }},
            lng: {{ !is_null(old('longitude')) ? old('longitude') :  '110.3680087'}}
        };

        var map = new google.maps.Map(document.getElementById("map-canvas"),
            {
                center: centerPos,
                zoom:12
            }
        );

        var marker = new google.maps.Marker({
            position: centerPos,
            map: map,
            draggable: true,

        });

        var autocomplete = new google.maps.places.Autocomplete(document.getElementById('inputGeoName'));
        autocomplete.bindTo('bounds', map);

        var geocoder = new google.maps.Geocoder();

        google.maps.event.addListener(marker, 'dragend', function(evt) {
            var lat = evt.latLng.lat();
            var lng = evt.latLng.lng();

            var latlng = new google.maps.LatLng(lat, lng);

            geocoder.geocode({'latLng': latlng}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[1]) {

                        map.setCenter(latlng);
                        marker.setPosition(latlng);

                        $('#inputGeoName').val(results[1].formatted_address);
                        $('#inputLatitude').val(lat);
                        $('#inputLongitude').val(lng);

                        setDistrictAndCity(results[0]);
                    } else {
                        alert('No results found');
                    }
                } else {
                    alert('Geocoder failed due to: ' + status);
                }
            });
        });


        autocomplete.addListener('place_changed', function() {
            marker.setVisible(false);
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                // User entered the name of a Place that was not suggested and
                // pressed the Enter key, or the Place Details request failed.
                window.alert("No details available for input: '" + place.name + "'");
                return;
            }

            map.setCenter(place.geometry.location);
            marker.setPosition(place.geometry.location);
            marker.setVisible(true);

            $('#inputLatitude').val(place.geometry.location.lat);
            $('#inputLongitude').val(place.geometry.location.lng);

            setDistrictAndCity(place);
        });

        function setDistrictAndCity(response) {
            subDistrict = '';
            city = '';
            response.address_components.forEach(function(component) {
                // console.log(component.types);
                if ($.inArray("administrative_area_level_3", component.types) != -1) {
                    subDistrict = component.short_name;
                }

                if ($.inArray("administrative_area_level_2", component.types) != -1) {
                    city = component.short_name;
                }

            });

            $('#inputSubdistrict').val(subDistrict);
            $('#inputAreaCity').val(city);
        }
    </script>
    <script>
       function clickSuggestion(idx) {
            key = idx;
            val = $('.suggestion-item-' + idx).text();
            el = $('.suggestion-item-' + idx);

            $(el).parent().parent().prev('.input-city-regency').val(val);
        };

        var callSuggestionCity = function(e) {
                $.ajax({
                    type: 'POST',
                    url: '/admin/area-big-mapper/suggestion-city-regency',
                    data: {
                        'params': e.target.value
                    },
                    success: function(data) {
                        $(e.target).next('.dropdown-menu').remove();

                        listOption = '<ul class="dropdown-menu suggestion-list" aria-labelledby="dLabel">'
                        for(idx in data.landings) {
                            listOption += '<li><a href="#" class="suggestion-item suggestion-item-' + idx + '" data-key="' + idx + '" onclick="clickSuggestion(' + idx + ')">' + data.landings[idx] + '</a></li>';
                        }

                        listOption += '</ul>';

                        $(listOption).insertAfter($(e.target));

                        $('.suggestion-list').dropdown();
                    }

                });
            };

        var delay = (function(){
              var timer = 0;
              return function(callback, ms){
                clearTimeout (timer);
                timer = setTimeout(callback, ms);
              };
            })();

        $(document).on('keyup', '.input-city-regency', function(e) {
                delay(function(){
                  callSuggestionCity(e)
                }, 1000 );
            });
    </script>

@stop
