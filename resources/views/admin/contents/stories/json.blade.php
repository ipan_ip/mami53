@extends('admin.layouts.main')
@section('content')
<div class="box box-primary">
  <div class="box-header">
      <h3 class="box-title">{{ $boxTitle }}</h3>
  </div><!-- /.box-header -->
  {{ Form::open(array('route' => $formAction, 'method' => $formMethod,
      'class' => 'form-horizontal form-bordered', 'role' => 'form', 'id' => 'formInsertDesigner')) }}
      <div class="box-body no-padding">
            <div class="form-group bg-default">
              <label for="inputDescription" class="col-sm-2 control-label">JSON Text</label>
              <div class="col-sm-10">
                <textarea class="form-control" rows="30" id="inputJsonText" name="json_text" placeholder="Json Text">
                {{ $json_text }}
                </textarea>
              </div>
            </div>
            <div class="form-group bg-info divider">
              <div class="col-sm-12 pull-left">
                  <span id="spanJsonNotValid" style="color:red">JSON Text is not Valid</span>
                  <span id="spanJsonValid">JSON is valid</span>
              </div>
            </div>


            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary" id="btnFormSave">Save</button>
                @if (Request::is('admin/*'))
                <a href="{{ $formCancel }}">
                  <button type="button" class="btn btn-default">Cancel</button>
                </a>
                @endif
              </div>
            </div>
      </div>
  {{ Form::close() }}<!-- /.form -->
</div><!-- /.box -->
@stop
@section('style')
{{ HTML::style('assets/vendor/jquery-chosen/chosen.css') }}
{{ HTML::style('assets/vendor/jquery-chosen-custom/chosen.css') }}
{{ HTML::style('assets/vendor/jquery-file-upload/css/jquery.fileupload.css') }}
<style>
  #chosenForm .chosen-choices {
      border: 1px solid #ccc;
      border-radius: 4px;
      min-height: 34px;
      padding: 6px 12px;
  }
  #chosenForm .form-control-feedback {
      /* To make the feedback icon visible */
      z-index: 100;
  }
</style>
@stop
@section('script')
<!-- page script -->
{{ HTML::script('assets/vendor/jquery-chosen/chosen.jquery.js') }}
<script type="text/javascript">
  var config = {
    '.chosen-select'           : {width: '100%'}
  };
  for (var selector in config) {
    $(selector).chosen(config[selector]);
  }
</script>
<!-- Holder for Upload File Thumbnail -->
{{ HTML::script('assets/vendor/holder/holder.js') }}
<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
{{ HTML::script('assets/vendor/jquery-file-upload/js/vendor/jquery.ui.widget.js') }}
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
{{ HTML::script('assets/vendor/jquery-file-upload/js/jquery.iframe-transport.js') }}
<!-- The basic File Upload plugin -->
{{ HTML::script('assets/vendor/jquery-file-upload/js/jquery.fileupload.js') }}
<!-- The File Upload processing plugin -->
{{ HTML::script('assets/vendor/jquery-file-upload/js/jquery.fileupload-process.js') }}
<!-- The File Upload validation plugin -->
{{ HTML::script('assets/vendor/jquery-file-upload/js/jquery.fileupload-validate.js') }}
<!-- File Upload Main -->
{{ HTML::script('assets/vendor/file-upload/index.js') }}
<script>
  $(function () {
    // Change this to the location of your server-side upload handler
    @if (Request::is('admin/*'))
      var url = "{{ url('admin/media') }}";
    @else
      var url = "{{ url('media') }}";
    @endif
    
    myFileUpload($('#wrapper-cover-upload'), url, 'user_photo');
    myFileUpload($('#wrapper-thumbnail-upload'), url, 'user_photo');
    myFileUpload($('#cardUpload1'), url, 'style_photo');
  });
  
</script>
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&language=id&key={{ENV('MIX_GOOGLE_MAPS_JAVASCRIPT_API_KEY')}}"></script>
{{ HTML::script('assets/vendor/geocomplete/jquery.geocomplete.js') }}
{{ HTML::script('assets/vendor/google-maps/index.js') }}
<script type="text/javascript">
  $(function(){
    var checkJsonSValidity = function(){
      var jsonText = $('#inputJsonText').val();
      var jsonIsValid = false;
      try{
        JSON.parse(jsonText);
        jsonIsValid = true;
      } catch(e){}

      if(jsonIsValid){
        $('#btnFormSave').removeAttr('disabled'); $('#spanJsonValid').show(); $('#spanJsonNotValid').hide();
      } else {
        $('#btnFormSave').attr('disabled','disabled'); $('#spanJsonValid').hide(); $('#spanJsonNotValid').show();
      }
    };

    setInterval(checkJsonSValidity,500);
  });
  
</script>
@stop