@extends('admin.layouts.main')

@section('style')
<style>
    span.apartment-color,
    tr.apartment-color td {
        color: #0D47A1;
    }
    tr.kost-active td {
        background-color: #C8E6C9;
    }

    .table>tbody>tr>td {
	vertical-align: middle;
    }

    .font-large {
        font-size: 1.5em;
    }

    .font-semi-large {
        font-size: 1.15em;
    }

    .font-grey {
        color: #9E9E9E;
    }

    .dropdown-menu>li>a:hover {
        color: #333
    }

    .label-grey {
        background-color: #CFD8DC;
    }

    /* Sweetalert */

    .swal-wide {
        width: 850px !important;
        /* font-size: 14px!important; */
    }

    .swal-shown {
        height: auto!important;
    }

    .custom-swal {
        z-index: 10000!important;
    }

    .custom-wide-swal {
        width:850px !important;
    }
</style>
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
<link rel="stylesheet" href="//cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
@endsection

@section('content')
    <!-- table -->
    <div class="box box-default">

        <div class="box-header">
            <h3 class="box-title"><i class="fa fa-bed"></i> {{ $boxTitle }}</h3>
            <span class="pull-right">
                    <button class="btn btn-default" onclick="window.location.href = '{{ route('admin.room.index', 'q=' . $parentRoomId) }}'"> Close</button>
            </span>
        </div>

        <div class="box-body no-padding">
            <div class="horizontal-wrapper" style="display:none" id="formStorySearch">
                {{ Form::open(array('url'=>$searchUrl , 'method'=> 'POST')) }}
                    <div class="btn-horizontal-group bg-default row">
                        <div class="col-sm-10">
                            <input type="text" name="search_keyword" class="form-control" />
                        </div>
                        <div class="col-sm-2">
                            <button class="btn btn-sm btn-info">Search</button>
                        </div>
                    </div>
                {{ Form::close() }}
            </div>
                        
            <!-- The table is going here -->
            @include('admin.contents.stories.partial.type-table')
            <!-- End of index table -->

        </div><!-- /.box-body -->
        <div class="box-body no-padding">
            {{ $rowsDesigner->appends(Request::except('page'))->links() }}
        </div>
    </div><!-- /.box -->
    <!-- table -->
@stop

@section('script')
<script src="//cdn.jsdelivr.net/npm/holderjs@2.9.6/holder.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.js"></script>
<script src="//cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
<!-- page script -->
<script type="text/javascript">
    $(function()
    {
        var token = '{{ csrf_token() }}';

        Holder.addTheme("thumbnail", {
            background: '#EEE',
            foreground: '#AAA',
            size: 10,
            fontweight: 'normal'
        });

        $('.btn-action').on('click', (e) => {
            e.preventDefault();
            var _action = $(e.currentTarget).data('action');
            var _id = $(e.currentTarget).data('id');

            if (_action == 'activate') {
                var title = 'Aktifkan Foto Premium?';
                var subTitle = '';
                var type = 'question';
                var status = 1;
            } else {
                var title = 'Non Aktifkan Foto Premium?';
                var subTitle = 'Foto kost akan kembali menggunakan foto dengan watermark';
                var type = 'warning';
                var status = 0;
            }

            Swal.fire({
                    title: title,
                    text: subTitle,
                    type: type,
                    confirmButtonText: 'Yes',
                    showLoaderOnConfirm: true,
                    preConfirm: () => {
                        return $.ajax({
                            type: 'POST',
                            url: "/admin/card/premium/activate",
                            dataType: 'json',
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            data: {
                                'id': _id,
                                'status': status,
                                '_token': token
                            },
                            success: (response) => {
                                if (!response.success) {
                                    Swal.insertQueueStep({
                                        type: 'error',
                                        title: response.message
                                    });
                                    return;
                                }
                            },
                            error: (error) => {
                                Swal.insertQueueStep({
                                    type: 'error',
                                    title: 'Error: ' + error.message
                                });
                            }
                        });
                    },
                    allowOutsideClick: () => !Swal.isLoading()
                }).then((result) => {
                    if (result.value.success) {
                        Swal.fire({
                            type: 'success',
                            title: result.value.message,
                            text: "Halaman akan di-refresh setelah Anda klik OK",
                            onClose: () => {
                                window.location.reload();
                            }
                        });
                    } else {
                        Swal.fire({
                            type: 'error',
                            title: result.value.message,
                            onClose: () => {
                                window.location.reload();
                            }
                        });
                    }
                })
        })

    });

</script>
@stop
