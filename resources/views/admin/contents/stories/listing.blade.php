@extends('admin.layouts.main')
@section('content')
    <?php App::setLocale('id'); ?>
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
            <div class="box-tools pull-right">
                <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <a href="{{ URL::route("{$login_type}.stories.create") }}">
                        <button type="submit" class="btn-add btn btn-primary btn-sm">
                            <i class="fa fa-plus">&nbsp;</i>Add Stories</button>
                    </a>
                @if($login_type == 'admin')
                    <a href="{{URL::route('admin.stories.sorting')}}">
                        <button class="btn btn-primary btn-sm"><i class="fa fa-sort">&nbsp;</i>Sort</button>
                    </a>
                    <a href="{{URL::route('admin.stories.reset.flag')}}" title="This will remove top and higlighted flag in all stories.">
                        <button class="btn btn-primary btn-sm"><i class="fa fa-refresh">&nbsp;</i>Reset Flag</button>
                    </a>
                @endif
                    <button class="btn btn-primary btn-sm" id="buttonSearch"><i class="fa fa-search">&nbsp;</i>Search</button>
                    
                    
                    <select class="pull-right pull-right-sort form-group btn btn-primary btn-sm" id="select-sort-option">
                        <option value="0">Sort By</option>
                        <option value="m">Most Mobile Views</option>
                        <option value="w">Most Web Views</option>
                        <option value="l">Most Likes</option>
                        <option value="c">Most Comments</option>
                        <option value="s">Most Shares</option>
                        <option value="lc">Last Created</option>
                        <option value="lu">Last Updated</option>
                    </select>
                    <input class="pull-right datepicker" type="text" value="{{ $endDate }}" id="end-date" style="margin:2px" />                  
                    <input class="pull-right datepicker" type="text" value="{{ $startDate }}" id="start-date" style="margin:2px" />
                </div>
                
            </div>
            <div class="horizontal-wrapper" style="display:none" id="formStorySearch">
                {{ Form::open(array('url'=>$searchUrl , 'method'=> 'POST')) }}
                    <div class="btn-horizontal-group bg-default row">
                        <div class="col-sm-10">
                            <input type="text" name="search_keyword" class="form-control" />
                        </div>
                        <div class="col-sm-2">
                            <button class="btn btn-sm btn-info">Search</button>    
                        </div>
                    </div>
                {{ Form::close() }}
            </div>
            <table id="tableListDesigner" class="table table-striped">
                <thead>
                    <tr>
                        <th width="30px;">No</th>
                        <th style = "width : 200px">{{ Lang::get('hairclick.page.stories.photo') }}</th>
                        <th>{{ Lang::get('hairclick.page.stories.name') }}</th>
                        <!-- <th>{{ Lang::get('hairclick.page.stories.description') }}</th> -->
                        <th>Mv</th>
                        <th>St</th>
                        <th>WV</th>
                        <th>TV</th>
                        <th>Lk</th>
                        <th>Co</th>
                        <th>Sh</th>
                        <th width="90px">Ca</th>
                        <th class="table-action-column" width="120px">{{ Lang::get('hairclick.page.stories.action') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($rowsDesigner as $rowDesignerKey => $rowDesigner)
                    <tr>
                        <td >{{ $rowDesignerKey+1 }}</td>
                        <td>
                            <?php $tempMedia = Media::getMediaUrl($rowDesigner['photo_id']); ?>
                            <img width="200" class="img-thumbnail" src="{{ $tempMedia['small'] }}" data-src="holder.js/60x77/thumbnail" alt="">
                        </td>
                        <!-- <td>
                            <img width="60" class="img-thumbnail" src="http://hairclick.wejoin.us/std_api/standard-server-api/public/uploads/cache/data/user/2014-08-25/XgbV2Uf2-120x160.jpg" alt="">
                        </td> -->
                        <td style="font-size:16px">{{ $rowDesigner['name'] }} </td>

                        <!-- <td> $rowDesignerdescription </td> -->
                        <td align="center">{{ $rowDesigner['view_count'] }}</td>
                        <td align="center">{{ ($rowDesigner->status == 0) ? 'Available' : 'Full' }}</td>
                        <td align="center">{{ $rowDesigner['web_view_count'] }}</td>
                        <td align="center">{{ $rowDesigner['view_count'] + $rowDesigner['web_view_count'] }}</td>
                        <td align="center">{{ $rowDesigner['match_count'] }}</td>
                        <td align="center">{{ $rowDesigner['comment_count'] }}</td>
                        <td align="center">{{ $rowDesigner['share_count'] }}</td>
                        <td align="center">{{ $rowDesigner->created_at->format('d-m-Y, H:i') }}</td>
                        <td class="table-action-column">
                            <div class="btn-action-group">
                            @if($login_type == 'admin')
                                @if ($rowDesigner->is_top=='false')
                                <a href="{{ URL::route('admin.stories.top.action', array($rowDesigner->id)) }}" title="Send to Top List">
                                    <i class="fa fa-plane"></i></a>
                                @else 
                                <a href="{{ URL::route('admin.stories.untop.action', array($rowDesigner->id)) }}" title="Remove from Top List">
                                    <i class="fa fa-truck"></i></a>
                                @endif

                                @if ($rowDesigner->is_active=='false')
                                <a href="{{ URL::route('admin.stories.verify.action', array($rowDesigner->id)) }}" title="Verify">
                                    <i class="fa fa-arrow-up"></i></a>
                                @else 
                                <a href="{{ URL::route('admin.stories.unverify.action', array($rowDesigner->id)) }}" title="Unverify">
                                    <i class="fa fa-arrow-down"></i></a>
                                @endif
                                <br>
                                @if ($rowDesigner->is_highlight=='false')
                                <a href="{{ URL::route('admin.stories.highlight.action', array($rowDesigner->id)) }}" title="Turn Highligt On">
                                    <i class="fa fa-sun-o"></i></a>
                                @else 
                                <a href="{{ URL::route('admin.stories.unhighlight.action', array($rowDesigner->id)) }}" title="Turn Highlight Off">
                                    <i class="fa fa-moon-o"></i></a>
                                @endif
                            @endif

                                <a href="{{ URL::route($resultAction, array( $rowDesigner->id)) }}" title="Edit Result">
                                    <i class="fa fa-sliders"></i></a>
<br>
                                <a href="{{ URL::route("{$login_type}.stories.style.index", $rowDesigner->id) }}" title="List Card">
                                    <i class="fa fa-file-text fa-info"></i></a>
                                <a href="{{ URL::route("{$login_type}.stories.edit", $rowDesigner->id) }}" title="Edit Stories">
                                    <i class="fa fa-pencil"></i></a>                                   
                                <!-- <a href="#" ng-click="showConfirmDelete('Stories', {{ $rowDesigner->id }}, '{{ htmlspecialchars($rowDesigner->name) }}');" title="Delete Stories">
                                    <i class="fa fa-trash-o"></i></a> -->
                                <a href="#" class="btn-delete" data-id="{{ $rowDesigner->id }}"  data-name="{{ htmlspecialchars($rowDesigner->name) }}" title="Delete Stories">
                                    <i class="fa fa-trash-o"></i></a>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th width="30px;">No</th>
                        <th>Photo</th>
                        <th>Name</th>
                        <th>MobileView</th>
                        <th>Status</th>
                        <th>WebView</th>
                        <th>TotalView</th>
                        <th>Likes</th>
                        <th>Comments</th>
                        <th>Shares</th>
                        <th>Created at</th>
                        <th class="table-action-column" width="100px">Action</th>
                    </tr>
                </tfoot>
            </table>
        </div><!-- /.box-body -->
        <div class="box-body no-padding">
            {{ $rowsDesigner->links() }}
        </div>
    </div><!-- /.box -->
    <!-- table -->

    @include('admin.partials.delete-modal')
@stop

@section('style')
    <!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> -->
@stop

@section('script')
<!-- Holder for Upload File Thumbnail -->
{{ HTML::script('assets/vendor/holder/holder.js') }}

<!-- Jquery Datepicker -->
<!-- <script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script> -->

<!-- page script -->
<script type="text/javascript">
    
    $(function()
    {
        $('#select-sort-option').change(function(event){
            var url  = '{{ URL::route("admin.stories.index") }}' + '?o=' + event.target.value ;
            
            url += '&st=' + $('#start-date').val();
            url += '&en=' + $('#end-date').val();

            window.location = url;

        });

        $('.datepicker').datepicker({ dateFormat : 'dd-mm-yy'});
    });
    
    $(function()
    {
        Holder.addTheme("thumbnail", {
            background: '#EEE',
            foreground: '#AAA',
            size: 11,
            fontweight: 'normal',
            text: 'Image'
          });
        // $("#tableListDesigner").dataTable();
    });
    $(function(){
        $('#buttonSearch').click(function(){
            $("#formStorySearch").show();
        });

        var token = '{{ csrf_token() }}';

        $('.btn-delete').click(function() {
            var id = $(this).data('id');
            var name = $(this).data('name');

            var confirm = window.confirm("Are you sure want to delete " + name +" ?");

            if (confirm) {
                $.post('/admin/stories/' + id, {
                    _method: "delete",
                    _token: token,
                }).success(function(response) {
                    alert(response);
                    window.location.reload();
                });    
            }

            
        });
        
    });
</script>
@stop