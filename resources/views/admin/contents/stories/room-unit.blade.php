@extends('admin.layouts.main')
@section('content')
<div class="box box-primary">
  <div class="box-header">
    <h3 class="box-title">Data Ketersediaan Kamar - {{ $room->name }}</h3>
  </div>
  <div class="box-body">
    <div class="table-responsive">
        <form method="POST" action="{{ url('admin/room/' . $room->id . '/room-unit/delete-bulk') }}">
            @csrf
            <table id="table" class="table">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Select Room(s)</th>
                        <th>Nama / Nomor Kamar</th>
                        <th>Lantai</th>
                        <th>Status Kamar</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if (count($units) == 1)
                    <tr>
                        <td>{{ $units[0]['id'] }}</td>
                        <td><input class="form-check-input" type="checkbox" value="" disabled></td>
                        <td>{{ $units[0]['name'] }}</td>
                        <td>{{ $units[0]['floor'] }}</td>
                        <td>{{ $units[0]['occupied'] ? 'Terisi' : 'Kosong' }}</td>
                        @if ($units[0]['disable'])
                            <td>
                                <span class="d-inline-block" tabindex="0" data-toggle="tooltip" data-placement="bottom" title="Kamar ini terikat dengan kontrak. {{ $units[0]['tenant_name'] }} - {{ $units[0]['tenant_phone'] }}">
                                    <a disabled class="btn btn-sm btn-info" href="#" data-toggle="modal" 
                                    data-target="#editModal" data-path="{{ url('admin/room/' . $room->id . '/room-unit/' . $units[0]['id'] . '/update-row') }}"
                                    data-name="{{ $units[0]['name'] }}" data-floor="{{ $units[0]['floor'] }}" data-occupied="{{ $units[0]['occupied'] }}">Edit</a>
                                </span>
                                <span class="d-inline-block" tabindex="0" data-toggle="tooltip" data-placement="bottom" title="Kamar ini terikat dengan kontrak. {{ $units[0]['tenant_name'] }} - {{ $units[0]['tenant_phone'] }}">
                                    <a disabled class="btn btn-sm btn-danger" href="#" data-toggle="modal" 
                                    data-target="#deleteModal" data-path="{{ url('admin/room/' . $room->id . '/room-unit/' . $units[0]['id'] . '/delete-row') }}" 
                                    data-name="{{ $units[0]['id'] }}">Delete</a>
                                </span>
                            </td>
                        @else
                            <td>
                                <a class="btn btn-sm btn-info" href="#" data-toggle="modal" 
                                data-target="#editModal" data-path="{{ url('admin/room/' . $room->id . '/room-unit/' . $units[0]['id'] . '/update-row') }}"
                                data-name="{{ $units[0]['name'] }}" data-floor="{{ $units[0]['floor'] }}" data-occupied="{{ $units[0]['occupied'] }}">Edit</a>
                                <span class="d-inline-block" tabindex="0" data-toggle="tooltip" data-placement="bottom" title="Jumlah kamar tidak boleh kurang dari satu">
                                    <a disabled class="btn btn-sm btn-danger" href="#" data-toggle="modal" 
                                    data-target="#deleteModal" data-path="{{ url('admin/room/' . $room->id . '/room-unit/' . $units[0]['id'] . '/delete-row') }}" 
                                    data-name="{{ $units[0]['id'] }}">Delete</a>
                                </span>
                            </td>
                        @endif
                    </tr>
                    @else
                    @foreach ($units as $key => $unit)
                    <tr>
                        <td>{{ $unit['id'] }}</td>
                        @if ($unit['disable'])
                            <td><input class="form-check-input" type="checkbox" value="" disabled></td>
                        @else
                            <td><input class="form-check-input" type="checkbox" value="{{ $unit['id'] }}" name="bulk_delete[]"></td>
                        @endif
                        <td>{{ $unit['name'] }}</td>
                        <td>{{ $unit['floor'] }}</td>
                        <td>{{ $unit['occupied'] ? 'Terisi' : 'Kosong' }}</td>
                        @if ($unit['disable'])
                            <td>
                                <span class="d-inline-block" tabindex="0" data-toggle="tooltip" data-placement="bottom" title="Kamar ini terikat dengan kontrak. {{ $unit['tenant_name'] }} - {{ $unit['tenant_phone'] }}">
                                    <a disabled class="btn btn-sm btn-info" href="#" data-toggle="modal" 
                                    data-target="#editModal" data-path="{{ url('admin/room/' . $room->id . '/room-unit/' . $unit['id'] . '/update-row') }}"
                                    data-name="{{ $unit['name'] }}" data-floor="{{ $unit['floor'] }}" data-occupied="{{ $unit['occupied'] }}">Edit</a>
                                </span>
                                <span class="d-inline-block" tabindex="0" data-toggle="tooltip" data-placement="bottom" title="Kamar ini terikat dengan kontrak. {{ $unit['tenant_name'] }} - {{ $unit['tenant_phone'] }}">
                                    <a disabled class="btn btn-sm btn-danger" href="#" data-toggle="modal" 
                                    data-target="#deleteModal" data-path="{{ url('admin/room/' . $room->id . '/room-unit/' . $unit['id'] . '/delete-row') }}" 
                                    data-name="{{ $unit['id'] }}">Delete</a>
                                </span>
                            </td>
                        @else
                            <td>
                                <a class="btn btn-sm btn-info" href="#" data-toggle="modal" 
                                    data-target="#editModal" data-path="{{ url('admin/room/' . $room->id . '/room-unit/' . $unit['id'] . '/update-row') }}"
                                    data-name="{{ $unit['name'] }}" data-floor="{{ $unit['floor'] }}" data-occupied="{{ $unit['occupied'] }}">Edit</a>
                                <a class="btn btn-sm btn-danger" href="#" data-toggle="modal" 
                                    data-target="#deleteModal" data-path="{{ url('admin/room/' . $room->id . '/room-unit/' . $unit['id'] . '/delete-row') }}" 
                                    data-name="{{ $unit['name'] }}">Delete</a>
                            </td>
                        @endif
                    </tr>
                    @endforeach
                    @endif
                </tbody>
            </table>
            <input type="submit" id="submit-form" class="hidden" onclick="this.disabled=true;this.form.submit();" />
        </form>
        <form method="GET" action="{{ url('admin/room/two') }}">
            @csrf
            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteSelectedModal">Delete Selected</button>
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addUnit">+ Tambah Kamar</button>
            <a type="button" class="btn btn-info" href="{{ url('admin/room/' . $room->id . '/edit') }}">Kembali ke Edit Kost</a>
            <input type="hidden" name="q" value="{{ $room->name }}" />
            <button type="submit" class="btn btn-info" onclick="this.disabled=true;this.form.submit();">Kembali ke Kost Additional</button>
            <a type="button" class="btn btn-default pull-right" href="{{ url('admin/room/' . $room->id . '/room-unit/log') }}">History Ketersediaan Kamar</a>
        </form>
    </div>
  </div>
</div>

<!-- Delete Selected Modal-->
<div class="modal fade" id="deleteSelectedModal" tabindex="-1" role="dialog" aria-labelledby="deleteSelectedLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteSelectedLabelTitle">Delete Confirmation</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <label for="submit-form" class="btn btn-sm btn-danger" tabindex="0">Delete</label>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="addUnit" tabindex="-1" role="dialog" aria-labelledby="addUnitLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form method="POST" action="{{ url('admin/room/' . $room->id . '/room-unit/store') }}">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="addUnitLabelTitle">Tambah Kamar</h5>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-form-label">Jumlah tambahan kamar (Max 20) :</label>
                        <input type="number" value="1" min="1" oninput="validity.valid;" class="form-control" name="new_unit">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" onclick="this.disabled=true;this.form.submit();">Add</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Edit Modal-->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Update Confirmation</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="edit-form" method="POST">
                    @csrf
                    <div class="form-group">
                        <label>Nama / Nomor Kamar :</label>
                        <input type="text" maxlength="50" class="form-control" name="name" id="name">
                    </div>
                    <div class="form-group">
                        <label>Lantai :</label>
                        <input type="text" maxlength="50" class="form-control" name="floor" id="floor">
                    </div>
                    <div class="form-group">
                        <label>Status Kamar :</label>
                        <select id="occupied" name="occupied">
                            <option value="1">Terisi</option>
                            <option value="0">Kosong</option>
                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <button class="btn btn-sm btn-info" href="#"
                    onclick="this.disabled=true;event.preventDefault();document.getElementById('edit-form').submit();">Update</button>
            </div>
        </div>
    </div>
</div>

<!-- Delete Modal-->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Delete Confirmation</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <button class="btn btn-sm btn-danger" href="#"
                    onclick="this.disabled=true;event.preventDefault();document.getElementById('delete-form').submit();">Delete</button>
                <form id="delete-form" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </div>
    </div>
</div>
@stop

@section('style')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.21/sp-1.1.1/sl-1.3.1/datatables.min.css"/>
@stop

@section('script')
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.21/sp-1.1.1/sl-1.3.1/datatables.min.js"></script>

<script>
$(document).ready( function () {
    $('#table').DataTable({
        dom: 'Pfrtip',
        searchPanes: {
            controls: false
        },
        columnDefs:[
            {
                searchPanes:{
                    viewTotal: true,
                    show: true,
                },
                targets: [4],
            }
        ],
    });
} );
</script>

<script type="text/javascript">
    $('#deleteSelectedModal').on('show.bs.modal', function (event) {
        var modal = $(this)
        // var array = []
        var checkboxes = document.querySelectorAll('input[type=checkbox]:checked')
        var selectedCount = checkboxes.length
        // for (var i = 0; i < checkboxes.length; i++) {
        //     array.push(checkboxes[i].value)
        // }
        
        modal.find('.modal-body').text('Are you sure you want to delete '+ selectedCount +' room(s)? Rooms selected could only come from the current page')
    })
</script>

<script type="text/javascript">
    $('#deleteModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var path = button.data('path')
        var name = button.data('name')
        var modal = $(this)
        modal.find('.modal-body').text('Are you sure you want to delete "' + name + '" ?')
        modal.find('#delete-form').attr("action", path);
    })
</script>

<script type="text/javascript">
    $('#editModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var name = button.data('name')
        var floor = button.data('floor')
        var path = button.data('path')
        if (button.data('occupied')) {
            // var occupied = true
            document.getElementById("occupied").selectedIndex = 0;
        } else {
            // var occupied = false
            document.getElementById("occupied").selectedIndex = 1;
        }
        var modal = $(this)
        modal.find('.modal-body #name').val(name)
        modal.find('.modal-body #floor').val(floor)
        // modal.find('.modal-body #occupied').checked = occupied
        modal.find('#edit-form').attr("action", path);
    })
</script>
@stop
