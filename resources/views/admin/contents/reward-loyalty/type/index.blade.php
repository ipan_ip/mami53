@extends('admin.layouts.main')

@section('content')
	<div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default form-inline">
                    <div class="col-md-6 bg-default" style="padding-bottom:10px;">
                        <a href="{{ URL::route('admin.reward-loyalty.type.create', ['#reward-loyalty']) }}" class="btn btn-primary">
                            <i class="fa fa-plus">&nbsp;</i>Add Reward Type
                        </a>
                    </div>
                    <div class="col-md-6 bg-default" style="padding-bottom:10px;text-align:right;">
                        <form action="" method="GET">
                            <input type="text" name="q" class="form-control"  placeholder="Reward Type Key"  autocomplete="off" value="{{ request()->input('q') }}">
                            <button type="submit" class="btn btn-primary" id="buttonSearch">
                                <i class="fa fa-search">&nbsp;</i>Search
                            </button>
                        </form>
                    </div>
                </div>
            </div>

            <table  class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Key</th>
                        <th>Name</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($type as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->key }}</td>
                        <td>{{ $item->name }}</td>
                        <td>
                            <a href="{{ URL::route('admin.reward-loyalty.type.edit', [$item->id, '#reward-loyalty']) }}" title="Edit" style="margin-right: 10px;">
                                <i class="fa fa-pencil"></i>
                            </a>
                            <form action="{{ URL::route('admin.reward-loyalty.type.destroy', [$item->id]) }}" method="POST" style="display: inline;">
                                <input type="hidden" name="_method" value="DELETE">
                                <a href="#" onclick="if(!confirm('Are you sure want to delete reward type {{ $item->name }}?')) return false; else $(this).parent().submit();" title="Delete">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </form>
                        </td>
                    </tr> 
                @endforeach
                </tbody>
            </table>
        </div>

        <div class="box-body no-padding">
            <div class="col-md-12 bg-default">
                {{ $type->appends(Request::except('page'))->links() }}
            </div>
        </div>
    </div>
@endsection
