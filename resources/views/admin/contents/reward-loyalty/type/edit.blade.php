@extends('admin.layouts.main')

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

        @php
            $item = null;
            if (isset($type)) {
                $route = URL::route('admin.reward-loyalty.type.update', [$type->id]);
                $item = $type;
            } else {
                $route = URL::route('admin.reward-loyalty.type.store');
            }
        @endphp
        <form action="{{ $route }}" method="POST" class="form-horizontal form-bordered">
            @if (isset($type))
                <input type="hidden" name="_method" value="PUT">
            @endif
            
            <div class="box-body no-padding">
                <div class="form-group bg-default">
                    <label for="reward-type-key" class="control-label col-sm-2">Key</label>
                    <div class="col-sm-10">
                        <input type="text" name="key" id="reward-type-key" class="form-control" value="{{ old('key', optional($item)->key) }}">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="reward-type-name" class="control-label col-sm-2">Name</label>
                    <div class="col-sm-10">
                        <input type="text" name="name" id="reward-type-name" class="form-control" value="{{ old('name', optional($item)->name) }}">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <div class="col-sm-10 col-sm-push-2">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
