@extends('admin.layouts.main')

@section('content')
	<div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

        <div class="box-body">
            <a href="{{route('admin.reward-loyalty.list.show', $reward->id)}}" class="btn btn-primary">&laquo; Back to Redemption List</a>

            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Action</th>
                        <th>Actor</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($histories as $item)
                        <tr>
                            <td>{{$item->created_at->format('j F, Y - H:i')}}</td>
                            <td>
                                @if ($item->action === \App\Entities\Reward\RewardRedeem::STATUS_REDEEMED)
                                    <b>Redeem Reward</b>
                                @else
                                    Change to <b>{{\App\Entities\Reward\RewardRedeem::getStatusDropdown()[$item->action]}}</b>
                                @endif
                            </td>
                            <td>{{$item->user->name}}</td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="100%"><p class="text-muted text-center">There is no any history yet</p></td>
                        </tr>
                    @endforelse
                </tbody>
            </table>

            {!! $histories->links() !!}
        </div>
    </div>
@stop