@section('style')
<link rel="stylesheet" href="/assets/vendor/summernote/summernote.css?v=05042018">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/dropzone.css">
<style>
    .image-preview {
        background-color: #f7f7f9;
        border: 1px solid #e1e1e8;
        margin-top: 10px;
        min-height: 120px;
        display: table;
        width: 100%;
    }
    .image-preview > span {
        display: table-cell;
        vertical-align: middle;
    }
    .icheckbox_minimal {
        margin-right: 5px;
    }
    .checkbox label{
        padding-left: 5px;
    }
    .swal2-popup{
        font-size:1.5rem !important;
    }
    .swal2-icon{
        display:none !important;
    }
    .swal2-actions .swal2-styled.swal2-confirm {
        color: #111;
    }
    .swal2-header, .swal2-actions {
        align-items: normal !important;
        justify-content: normal !important;
    }
    .swal2-actions {
        border-top: 1px solid silver;
        padding-top: 12px;
    }
    .swal2-content {
        text-align: left !important;
    }
    .targeted-user {
        position: relative;
        background: whitesmoke; 
        margin-top: 8px; 
        padding: 8px 30px 8px 8px;
    }
    button.remove-targeted-user {
        position: absolute;
        top: 5px;
        right: 5px;
        opacity: 1;
    }
</style>
@stop

@section('script')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script src="/assets/vendor/summernote/summernote.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/dropzone.js"></script>
<script>
    $('.editor').summernote({
        height: 250,
        toolbar: [
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', []],
            ['color', ['color']],
            ['para', ['ul', 'ol']],
            ['insert', ['link']],
            ['misc', ['codeview']]
        ]
    });
    
    $('.datepicker').datepicker({
        dateFormat:'yy-mm-dd'
    });

    $("#user_target").on('change',function() {
        if ($(this).find('option:selected').text() != "Owner") {
            $("#owner_segment div").addClass('disabled')
            $("input:checkbox[name^=owner_segment]").prop('disabled', true)
        }
        else {
            $("#owner_segment div").removeClass('disabled')
            $("input:checkbox[name^=owner_segment]").prop('disabled', false)
        }
    });

    $('#clear-user-csv').on('click', function () {
        $('[name="user_csv"]').val(null);
    })

    // DropZone
    Dropzone.autoDiscover = false;
    Dropzone.options.photo = {
        paramName : "media",
        params: {
            'media_type': 'reward',
            'watermarking': false
        },
        maxFilesize : 6,
        maxFiles: 1,
        accepFiles: '.jpg, .jpeg, .png, .bmp',
        dictDefaultMessage : 'Upload Image for Reward',
        addRemoveLinks: true,
        success: function(file, response) {
            $('#photo-wrapper').html('');
            $('#photo-wrapper').append('<input type="hidden" name="media_id" value="' + response.media.id + '">');
            $('#photo-preview').html('');
            $('#photo').attr('style', '')
        },
        init: function() {
            $('.dz-message > span').addClass('btn btn-default')

            this.on('removedfile', function(file) {
                $('#photo-wrapper').html('');
                $('#photo-preview').html('');
            });
        }
    }

    $('#photo').dropzone();

    // Popup Confirmation
    document.querySelector('.btn-cancel').addEventListener('click', function (e) {
        e.preventDefault()

        Swal.fire({
            title: 'Are you sure?',
            text: "Please confirm that you want to cancel it.",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#E7E7E7',
            cancelButtonColor: '#398DBB',
            confirmButtonText: 'Yes, Do It!',
            cancelButtonText: 'No, Keep It!'
        }).then((result) => {
            if (result.value) {
                document.location.href = $(this).attr('href')
            }
        })
    });

    document.querySelector('form').addEventListener('submit', function (e)
    {
        // Normalize empty editor
        if ($('textarea[name=tnc').val() == '<p><br></p>') {
            $('textarea[name=tnc').val('')
        }
        if ($('textarea[name=howto').val() == '<p><br></p>') {
            $('textarea[name=howto').val('')
        }

        if (document.querySelector('input[name=media_id]').value == 0) {
            document.querySelector('input[name=media_id]').value = null
        }

        // If in create mode, Skip the confirmation
        if (!document.querySelector('input[name=_method]')) {
            return self.submit()
        }

        e.preventDefault()
        var self = this
        var msg = ''
        var isActive = $('input[name="is_active"]').is(':checked')
        var isPublished = $('input[name="is_published"]').is(':checked')

        if (!isActive && !isPublished) {
            return self.submit()
        }

        var callSwal = function (msg) {
            Swal.fire({
                title: 'Are you sure?',
                html: msg,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#E7E7E7',
                cancelButtonColor: '#398DBB',
                confirmButtonText: 'Yes, Do It!',
                cancelButtonText: 'No, Go Back'
            }).then((result) => {
                if (result.value) {
                    self.submit()
                }
            })
        }

        if (isActive && isPublished) {
            msg = 'This reward is <b>active</b> and <b>published</b>. Please confirm that you still want to update it.'
        }
        else if (isActive) {
            msg = 'This reward is <b>active</b>. Please confirm that you still want to update it.'
        }
        else if (isPublished) {
            msg = 'This reward is <b>published</b>. Please confirm that you still want to update it.'
        }

        callSwal(msg)
    });
</script>
@stop

<div class="row">
    <div class="col-md-6">
        <div class="form-group {{ $errors->first('name') ? 'has-error' : '' }}">
            <label for="">Reward Name</label>
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group {{ $errors->first('description') ? 'has-error' : '' }}">
            <label for="">Description</label>
            {!! Form::textarea('description', null, ['rows' => 4, 'class' => 'form-control']) !!}
        </div>
        <div class="form-group row">
            <div class="col-md-6 {{ $errors->first('start_date') ? 'has-error' : '' }}">
                <label for="">Start Date</label>
                <div class="input-group">
                    {!! Form::text('start_date', null, ['class' => 'form-control datepicker', 'autocomplete' => 'off', 'placeholder' => 'yyyy-mm-dd']) !!}
                    <span class="input-group-addon btn-datepicker"><i class="fa fa-calendar"></i></span>
                </div>
            </div>
            <div class="col-md-6 {{ $errors->first('end_date') ? 'has-error' : '' }}">
                <label for="">End Date</label>
                <div class="input-group">
                    {!! Form::text('end_date', null, ['class' => 'form-control datepicker', 'autocomplete' => 'off', 'placeholder' => 'yyyy-mm-dd']) !!}
                    <span class="input-group-addon btn-datepicker"><i class="fa fa-calendar"></i></span>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-6 {{ $errors->first('quota.total') ? 'has-error' : '' }}">
                <label for="total">Total Quota</label>
                {!! Form::number('quota[total]', null, ['class' => 'form-control']) !!}
                @if (!empty($reward))
                    <p class="help-block" style="font-weight: bold;">
                        Redeemed: {{$redeemedCount}} times
                    </p>
                @endif
            </div>
            <div class="col-md-6 {{ $errors->first('quota.daily') ? 'has-error' : '' }}">
                <label for="daily">Daily Quota</label>
                {!! Form::number('quota[daily]', null, ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-6 {{ $errors->first('quota.total_user') ? 'has-error' : '' }}">
                <label for="">Total Each User Quota</label>
                {!! Form::number('quota[total_user]', null, ['class' => 'form-control']) !!}
            </div>
            <div class="col-md-6 {{ $errors->first('quota.daily_user') ? 'has-error' : '' }}">
                <label for="">Daily Each User Quota</label>
                {!! Form::number('quota[daily_user]', null, ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-6 {{ $errors->first('type_id') ? 'has-error' : '' }}">
                <label for="">Reward Type</label>
                {!! Form::select(
                    'type_id',
                    $type_dropdown,
                    null,
                    ['class' => 'form-control']
                ) !!}
            </div>
            <div class="col-md-6 {{ $errors->first('redeem_value') ? 'has-error' : '' }}">
                <label for="">Redemption Point</label>
                {!! Form::number('redeem_value', null, ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="form-group">
            <label for="sequence">Sequence/Order on Reward List</label>
            {!! Form::number('sequence', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group row">
            <div class="col-md-12 {{ $errors->first('user_target') ? 'has-error' : '' }}">
                <label for="">Target <small style="font-style: italic;">*Reward appear and can be redeemed by targeted user</small></label>
                {!! Form::select(
                    'user_target',
                    $user_targets,
                    null,
                    ['id' => 'user_target', 'class' => 'form-control']
                ) !!}
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-12">
                <label for="">Owner Segment <small style="font-style: italic;">*Reward appear for all owner but only can be redeemed by targeted owner</small></label>
                @foreach (array_chunk($owner_segments, 4, true) as $ownerSegmentRow)
                    <div class="row">
                        @foreach ($ownerSegmentRow as $key => $value)
                            <div class="col-md-3">
                                <div id="owner_segment" class="checkbox">
                                    <label>
                                        @php
                                            $checked = false;
                                            $ownerSegmentTargetType = App\Entities\Reward\RewardTargetConfig::OWNER_SEGMENT_TARGET_TYPE;
                                            if (!empty(old($ownerSegmentTargetType.'.'.$key))) {
                                                $checked = old($ownerSegmentTargetType.'.'.$key);
                                            } elseif (!empty($reward)) {
                                                $checked = ($reward->target_configs->where('type', $ownerSegmentTargetType)->where('value', array_get($gp_levels, $key))->count() > 0);
                                            }

                                            $disabled = true;
                                            if (!empty(old('user_target')) && old('user_target') === App\Entities\Reward\Reward::OWNER_REWARD_USER_TARGET) {
                                                $disabled = false;
                                            } elseif (!empty($reward) && $reward->user_target === App\Entities\Reward\Reward::OWNER_REWARD_USER_TARGET) {
                                                $disabled = false;
                                            }
                                        @endphp
                                        <input type="checkbox" 
                                            name="{{ $ownerSegmentTargetType }}[{{ $key }}]" 
                                            value="1" 
                                            @if ($checked) checked @endif
                                            @if ($disabled) disabled @endif
                                        >
                                        {{ $value }}
                                    </label>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endforeach
            </div>
        </div>

        <div class="form-group">
            <label for="">Targeted User <small style="font-style: italic;">*csv field: User ID. Reward appear and can be redeemed by targeted user</small></label>
            <div class="input-group">
                {!! Form::file('user_csv', ['class' => 'form-control']) !!}
                <div class="input-group-btn">
                    <button type="button" id="clear-user-csv" class="btn btn-default">Clear</button>
                </div>
            </div>

            @if (!empty($reward) && $reward->target->count())
                <div class="targeted-user">
                    {{ implode(', ', $reward->target->pluck('id')->toArray()) }}
                    <button type="button" class="close remove-targeted-user" data-toggle="modal" aria-label="Remove" data-target="#remove-targeted-user-modal">
                        <span class="glyphicon glyphicon-remove text-danger" aria-hidden="true"></span>
                    </button>
                </div>
            @endif
        </div>
        <div class="form-group">
            <label for="">Image <small style="font-style: italic;">*1352x336 px</small></label>
            <div id="photo"
            action="{{ url('admin/media') }}"
            method="POST"
            class="dropzone"
            uploadMultiple="no"
            @if (!empty($reward) && !empty($reward->media))
            style="background: url({{$reward->media->getMediaUrl()['small']}}) no-repeat center center; background-size: cover;"
            @endif
            >
                {{ csrf_field() }}
                {!! Form::hidden('media_type', $media_type) !!}
            </div>
            <div id="photo-wrapper">
                {!! Form::hidden('media_id', null, ['id' => 'media_id']) !!}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group {{ $errors->first('tnc') ? 'has-error' : '' }}">
            <label for="">Terms and Conditions</label>
            {!! Form::textarea('tnc', null, ['class' => 'form-control editor']) !!}
        </div>
        <div class="form-group {{ $errors->first('howto') ? 'has-error' : '' }}">
            <label for="">How to Use</label>
            {!! Form::textarea('howto', null, ['class' => 'form-control editor']) !!}
        </div>
        <div class="form-group row">
            <div class="col-md-6">
                <label for="is_active">
                    {!! Form::checkbox('is_active', 1, null, ['id' => 'is_active']) !!} Active
                </label>
                <p>Reward can be redeemed. <br> Unchecked if you don't want it.</p>
            </div>
            <div class="col-md-6">
                <label for="is_published">
                    {!! Form::checkbox('is_published', 1, null, ['id' => 'is_published']) !!} Publish
                </label>
                <p>Reward appears on user's view. <br> Unchecked if you want to hide it from user.</p>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-6">
                <label for="is_testing">
                    {!! Form::checkbox('is_testing', 1, null, ['id' => 'is_testing']) !!} For Testing
                </label>
                <p>Indicate the reward is for testing purposes.</p>
            </div>
        </div>
    </div>
</div>
<div class="form-group">
    <button type="submit" class="btn btn-primary">
        @if (!empty($reward))
            Update Reward
        @else
            Add Reward
        @endif
    </button>
    &nbsp;&nbsp;
    <a href="{{route('admin.reward-loyalty.list.index')}}" class="btn btn-default btn-cancel">Cancel</a>
</div>

{{-- Remove Targeted User Modal --}}
<div class="modal fade" id="remove-targeted-user-modal" tabindex="-1" role="dialog" aria-labelledby="remove-targeted-user-modal-label">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="remove-targeted-user-modal-label">Remove Targeted User</h5>
            </div>
            <div class="modal-body">
                <p>Are you sure want to remove all targeted users?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" onclick="$('#form-remove-targeted-user').submit();" >Yes, Do It!</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">No, Go Back</button>
            </div>
        </div>
    </div>
</div>
