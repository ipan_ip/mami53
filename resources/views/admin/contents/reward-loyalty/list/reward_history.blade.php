@extends('admin.layouts.main')

@section('content')
	<div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

        <div class="box-body">
            <a href="{{route('admin.reward-loyalty.list.index')}}" class="btn btn-primary">&laquo; Back to Reward List</a>

            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Action</th>
                        <th>Actor</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse (array_get($histories, 'data') as $item)
                        <tr>
                            <td>{{$item['date']}}</td>
                            <td>
                                {{$item['action']}}
                            </td>
                            <td>
                                {{$item['actor']}}
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="100%">There is no item</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>

            {!! optional($histories['paginate'])->links() !!}
        </div>
    </div>
@stop