@extends('admin.layouts.main')

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>
        <div class="box-body">
            {!! Form::open([
                'method' => 'POST',
                'enctype' => 'multipart/form-data',
                'url' => route('admin.reward-loyalty.list.store'),
            ]) !!}
                @include('admin.contents.reward-loyalty.list._form')
            {!! Form::close() !!}
        </div>
    </div>

@stop