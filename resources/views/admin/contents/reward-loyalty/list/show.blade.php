@extends('admin.layouts.main')

@section('style')
<style>
    .form-filter .form-group {
        margin: 0px 4px;
    }
</style>
@stop

@section('script')
<script type="text/javascript">
    $('.datepicker').datepicker({
        dateFormat:'yy-mm-dd'
    });

    var $popupStatus = $('#popup-redeem-status select[name=status_new]')
    $popupStatus.on('change', function (e, originalStatus) {
        var val = $(this).val()

        if (['success', 'failed'].includes(val)) {
            $('#popup-redeem-status input[name=notes]').attr('disabled', false)
        } else {
            $('#popup-redeem-status input[name=notes]').attr('disabled', true)
        }

        if (['success', 'failed'].includes(originalStatus)) {
            $(this).attr('disabled', true)
        } else {
            $(this).attr('disabled', false)
        }

        $('input[name=status]').val(val)
    })

    $('#popup-redeem-status').on('show.bs.modal', function (e) {
        var button = $(e.relatedTarget)
        var modal = $(this)
        var formAction = button.data('formAction')
        
        modal.find('form').attr('action', formAction)
        modal.find('#user-name').html(button.data('userName'))
        modal.find('#reward-name').html(button.data('rewardName'))
        modal.find('select[name=status_new]').val(button.data('status'))
        modal.find('input[name=notes]').val(button.data('notes'))

        $popupStatus.trigger('change', [button.data('status')])
    })

    $('#popup-redeem-status').on('hidden.bs.modal', function (e) {
        var modal = $(this)
        modal.find('input[name=notes]').val('')
    })
</script>
@stop

@section('content')
<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">{{ $boxTitle }}</h3>
    </div>
    
    <div class="box-body with-border">
        {!! Form::model(request()->all(), ['method' => 'GET', 'class' => 'form-inline form-filter text-center', 'autocomplete' => 'off']) !!}
            <div class="form-group">
                {!! Form::text('keyword', null, ['class' => 'form-control', 'placeholder' => 'ID / name / email / phone / recipient', 'style' => 'width: 240px;']) !!}
            </div>
            
            <div class="form-group filter-date-range-container">
                <div class="input-group input-date-filter">
                    {!! Form::text('start_date', null, ['class' => 'form-control datepicker', 'placeholder' => 'yyyy-mm-dd']) !!}
                    <span class="input-group-addon btn-datepicker"><i class="fa fa-calendar"></i></span>
                </div>
                <label class="label-until">until</label>
                <div class="input-group input-date-filter">
                    {!! Form::text('end_date', null, ['class' => 'form-control datepicker', 'placeholder' => 'yyyy-mm-dd']) !!}
                    <span class="input-group-addon btn-datepicker"><i class="fa fa-calendar"></i></span>
                </div>
            </div>
            
            <div class="form-group">
                {!! Form::select('user', [
                '' => 'All',
                'owner' => 'Owner',
                'tenant' => 'Tenant',
                ], null, ['class' => 'form-control']) !!}
            </div>
            
            <div class="form-group">
                {!! Form::select('status', $status_dropdown, null, ['class' => 'form-control']) !!}
            </div>
            
            <div class="form-group action text-center">
                <div>
                    <button type="submit" class="btn btn-primary btn-md">Search</button>
                    <a href="{{route('admin.reward-loyalty.list.show', $reward->id)}}" class="btn btn-default btn-md" data-toggle="tooltip" title="Reset"><i class="fa fa-refresh"></i></a>
                </div>
            </div>
        {!! Form::close() !!}
    </div>

    <div class="box-body">
        <div id="reward-detail">
            <table class="table table-bordered" style="width: 50%;">
                <tbody>
                    <tr>
                        <th colspan="100%" class="text-center">Reward Info</th>
                    </tr>
                    <tr>
                        <td>{{$reward->name}}</td>
                        <td>Redemption Point: {{$reward->redeem_value}}</td>
                    </tr>
                    <tr>
                        <td>
                            {{\Carbon\Carbon::parse($reward->start_date)->format('j F Y H:i')}}
                            -
                            {{\Carbon\Carbon::parse($reward->end_date)->format('j F Y H:i')}}
                        </td>
                        <td>Type: {{optional($reward->type)->name}}</td>
                    </tr>
                    <tr>
                        <td>
                            Redeemed: {{$redeemedCount}},
                            Total Quota: {{$reward->quotas->where('type', 'total')->first()->value}},
                            Eligible User: {{$reward->target->count()}}
                        </td>
                        <td>
                            {!!$reward->is_active
                            ? '<span class="label label-success">Active</span>'
                            : '<span class="label label-danger">Not Active</span>'!!}
                            &nbsp;
                            {!!$reward->is_published
                            ? '<span class="label label-success">Published</span>'
                            : '<span class="label label-danger">Not Published</span>'!!}
                            &nbsp;
                            {!!$reward->is_testing
                            ? '<span class="label label-warning">For Testing</span>'
                            : ''!!}
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <hr>
        
        <div id="redemption-list">
            <h4>Redemption List ({{$redeems->total()}})</h4>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Date</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone Number</th>
                        <th>User</th>

                        @if (in_array($reward->type->key, ['shipping_goods', 'virtual_goods_pulsa', 'virtual_goods_pln']))
                            <th>Recipient</th>
                            <th>Contact</th>
                            <th>
                                @if ($reward->type->key === 'shipping_goods')
                                    Shipping Address
                                @elseif ($reward->type->key === 'virtual_goods_pulsa')
                                    Recipient Phone Number
                                @elseif ($reward->type->key === 'virtual_goods_pln')
                                    PLN Customer ID
                                @endif
                            </th>
                        @endif

                        @if ($reward->type->key === 'shipping_goods')
                            <th>User's Note</th>
                        @endif

                        <th style="width: 80px;">Status</th>
                        <th>Admin's Note</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($redeems as $redeem)
                    <tr>
                        <td>{{$redeem->id}}</td>
                        <td>{{$redeem->created_at->format('j F Y H:i')}}</td>
                        <td>{{$redeem->user->name}}</td>
                        <td>{{$redeem->user->email}}</td>
                        <td>{{$redeem->user->phone_number}}</td>
                        <td>{{$redeem->user->is_owner === 'true' ? 'Owner' : 'Tenant'}}</td>

                        {{-- Display this column when meet these types --}}
                        @if (in_array($reward->type->key, ['shipping_goods', 'virtual_goods_pulsa', 'virtual_goods_pln']))
                            <td>{{$redeem->recipient_name ?: '-'}}</td>
                            <td>{{$redeem->recipient_phone ?: '-'}}</td>
                            <td>{{$redeem->recipient_data ?: '-'}}</td>
                        @endif

                        @if ($reward->type->key === 'shipping_goods')
                            <td>{{$redeem->recipient_notes ?: '-'}}</td>
                        @endif

                        <td>
                            @if (auth()->user()->can('access-reward-loyalty'))
                                <a href="#" data-target="#popup-redeem-status"
                                    data-toggle="modal"
                                    data-form-action="{{route('admin.reward-loyalty.list.updateRedeemStatus', [$reward->id, $redeem->id])}}"
                                    data-user-name="{{$redeem->user->name}}"
                                    data-reward-name="{{$reward->name}}"
                                    data-status="{{$redeem->status}}"
                                    data-notes="{{$redeem->notes}}">
                                    @if ($redeem->status === \App\Entities\Reward\RewardRedeem::STATUS_SUCCESS)
                                        <span class="text-success" style="text-decoration: underline;">
                                    @elseif ($redeem->status === \App\Entities\Reward\RewardRedeem::STATUS_FAILED)
                                        <span class="text-danger" style="text-decoration: underline;">
                                    @elseif ($redeem->status === \App\Entities\Reward\RewardRedeem::STATUS_ONPROCESS)
                                        <span class="text-primary" style="text-decoration: underline;">
                                    @else
                                        <span class="text-muted" style="text-decoration: underline;">
                                    @endif
                                        {{\App\Entities\Reward\RewardRedeem::getStatusDropdown()[$redeem->status]}}
                                    </span>
                                </a>
                            @else
                                {{\App\Entities\Reward\RewardRedeem::getStatusDropdown()[$redeem->status]}}
                            @endif
                        </td>
                        <td>{{$redeem->notes ?: '-'}}</td>
                        <td>
                            <a href="{{route('admin.reward-loyalty.list.redeem.history', [$reward->id, $redeem->id])}}" data-toggle="tooltip" title="History"><i class="fa fa-history"></i></a>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="100%">There is no redemption list yet.</td>
                    </tr>
                    @endforelse
                </tbody>
            </table>

            <div class="text-center">
                {{$redeems->appends([
                    'keyword' => request('keyword'),
                    'start_date' => request('start_date'),
                    'end_date' => request('end_date'),
                    'status' => request('status'),
                    'user' => request('user'),
                ])->links()}}
            </div>
        </div>
    </div>
</div>

{{-- Popup Redeem Update Status --}}
<div class="modal fade" id="popup-redeem-status">
    <div class="modal-dialog modal-sm">
        {!! Form::open(['method' => 'PUT', 'url' => '', 'class' => 'modal-content']) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Redemption Status</h4>
            </div>
            <div class="modal-body">
                <p><b id="user-name">Antoni</b> for <b id="reward-name">Emas Antam - gift series</b></p>
                <div class="form-group">
                    <label>Status</label>
                    {!! Form::hidden('status', null) !!}
                    {!! Form::select('status_new', array_slice($status_dropdown, 1), null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    <label>Notes</label>
                    {!! Form::text('notes', null, ['class' => 'form-control', 'maxlength' => '30']) !!}
                    <span class="help-block">max: 30 character</span>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        {!! Form::close() !!}
    </div>
</div>
@stop