@extends('admin.layouts.main')

@section('style')
<style>
    .form-filter .form-group {
        margin: 0px 4px;
    }
</style>
@stop

@section('script')
<script type="text/javascript">
    $('.datepicker').datepicker({
        dateFormat:'yy-mm-dd'
    });
</script>
@stop

@section('content')
	<div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

        <div class="box-body with-border">
            <form action="" class="form-inline form-filter text-center" autocomplete="off" method="GET">
                <div class="form-group">
                    {{-- <label class="label-filter">ID / Reward Name</label> --}}
                <input name="keyword" type="text" class="form-control" value="{{request('keyword')}}" placeholder="ID / Reward Name" />
                </div>

                <div class="form-group filter-date-range-container">
                    {{-- <label class="label-filter">Expiration Range</label> --}}
                    <div>
                        <div class="input-group input-date-filter">
                            <input type="text" class="form-control datepicker" value="@if(!empty(request()->start_date)){{request()->start_date}}@endif" name="start_date" placeholder="Start Date...">
                            <span class="input-group-addon btn-datepicker"><i class="fa fa-calendar"></i></span>
                        </div>
                        <label class="label-until">until</label>
                        <div class="input-group input-date-filter">
                            <input type="text" class="form-control datepicker" value="@if(!empty(request()->end_date)){{request()->end_date}}@endif" name="end_date" placeholder="End Date...">
                            <span class="input-group-addon btn-datepicker"><i class="fa fa-calendar"></i></span>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    {{-- <label class="label-filter">Status</label> --}}
                    <select class="form-control select2 input-status" name="status">
                        <option value="" {{ !request('status') ? 'selected' : ''}}>All</option>
                        <option value="1" {{ request('status') == '1' ? 'selected' : ''}}>Active</option>
                        <option value="0" {{ request('status') == '0' ? 'selected' : ''}}>Not Active</option>
                    </select>
                </div>

                <div class="form-group">
                    <select class="form-control select2 input-status" name="user_target">
                        <option value="">&laquo; Select &raquo;</option>
                        @foreach ($user_targets as $key => $value)
                            <option value="{{ $key }}" {{ request('user_target') == $key ? 'selected' : '' }}>{{ $value }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    {{-- <label class="label-filter">Testing</label> --}}
                    <select class="form-control select2 input-status" name="show_testing">
                        <option value="0" {{ request('show_testing') == '0' ? 'selected' : ''}}>Hide Testing</option>
                        <option value="1" {{ request('show_testing') == '1' ? 'selected' : ''}}>Show Testing</option>
                        <option value="2" {{ request('show_testing') == '2' ? 'selected' : ''}}>Show All</option>
                    </select>
                </div>

                <div class="form-group action text-center">
                    {{-- <label class="label-filter">&nbsp;</label> --}}
                    <div>
                        <input type="submit" class="btn btn-primary btn-md" value="Filter"/>
                        <a href="{{route('admin.reward-loyalty.list.index')}}" class="btn btn-default btn-md" data-toggle="tooltip" title="Reset"><i class="fa fa-refresh"></i></a>
                    </div>
                </div>
            </form>
        </div>

        <div class="box-body">
            @if (auth()->user()->can('access-reward-loyalty'))
                <div class="col-md-6 bg-default" style="padding-bottom:10px;">
                    <a href="{{route('admin.reward-loyalty.list.create')}}" class="btn btn-primary">
                        <i class="fa fa-plus">&nbsp;</i>Add Reward
                    </a>
                </div>
            @endif

            <div class="horizontal-wrapper">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Reward Name</th>
                            <th>Start</th>
                            <th>
                                <a href="{{route('admin.reward-loyalty.list.index', array_merge(
                                    request()->except('sortBy', 'sortOrder'),
                                    [
                                        'sortBy' => 'end_date',
                                        'sortOrder' => request('sortOrder') == 'asc' ? 'desc' : 'asc',
                                    ]
                                ))}}">
                                    End
                                    @if (request('sortBy') == 'end_date')
                                        @if (request('sortOrder') == 'asc')
                                            <i class="fa fa-angle-up"></i>
                                        @else
                                            <i class="fa fa-angle-down"></i>
                                        @endif
                                    @endif
                                </a>
                            </th>
                            <th>
                                <a href="{{route('admin.reward-loyalty.list.index', array_merge(
                                    request()->except('sortBy', 'sortOrder'),
                                    [
                                        'sortBy' => 'redeem_value',
                                        'sortOrder' => request('sortOrder') == 'asc' ? 'desc' : 'asc',
                                    ]
                                ))}}">
                                    Redemption Point
                                    @if (request('sortBy') == 'redeem_value')
                                        @if (request('sortOrder') == 'asc')
                                            <i class="fa fa-angle-up"></i>
                                        @else
                                            <i class="fa fa-angle-down"></i>
                                        @endif
                                    @endif
                                </a>
                            </th>
                            <th>
                                <a href="{{route('admin.reward-loyalty.list.index', array_merge(
                                    request()->except('sortBy', 'sortOrder'),
                                    [
                                        'sortBy' => 'remaining_quota',
                                        'sortOrder' => request('sortOrder') == 'asc' ? 'desc' : 'asc',
                                    ]
                                ))}}">
                                    Remaining Quota
                                    @if (request('sortBy') == 'remaining_quota')
                                        @if (request('sortOrder') == 'asc')
                                            <i class="fa fa-angle-up"></i>
                                        @else
                                            <i class="fa fa-angle-down"></i>
                                        @endif
                                    @endif
                                </a>
                            </th>
                            <th>
                                <a href="{{route('admin.reward-loyalty.list.index', array_merge(
                                    request()->except('sortBy', 'sortOrder'),
                                    [
                                        'sortBy' => 'sequence',
                                        'sortOrder' => request('sortOrder') == 'asc' ? 'desc' : 'asc',
                                    ]
                                ))}}">
                                    Order
                                    @if (request('sortBy') == 'sequence')
                                        @if (request('sortOrder') == 'asc')
                                            <i class="fa fa-angle-up"></i>
                                        @else
                                            <i class="fa fa-angle-down"></i>
                                        @endif
                                    @endif
                                </a>
                            </th>
                            <th>Target</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($rewards as $r)
                            <tr>
                                <td>{{$r->id}}</td>
                                <td>{{$r->name}}</td>
                                <td>{{\Carbon\Carbon::parse($r->start_date)->format('j F Y H:i')}}</td>
                                <td>{{\Carbon\Carbon::parse($r->end_date)->format('j F Y H:i')}}</td>
                                <td>{{$r->redeem_value}}</td>
                                <td>
                                    {{is_null($r->remaining_quota) ? $r->total_quota_available : $r->remaining_quota}}
                                    /
                                    {{$r->total_quota_available}}
                                </td>
                                <td>
                                    <span class="badge badge-default">{{$r->sequence}}</span>
                                </td>
                                <td>
                                    @if ($r->user_target === App\Entities\Reward\Reward::OWNER_REWARD_USER_TARGET)
                                        &nbsp;<span class="label label-success">OWNER</span>
                                    @elseif ($r->user_target === App\Entities\Reward\Reward::TENANT_REWARD_USER_TARGET)
                                        &nbsp;<span class="label label-primary">TENANT</span>
                                    @else
                                        &nbsp;<span class="label label-default">ALL</span>
                                    @endif
                                </td>
                                <td>
                                    @if ($r->is_active)
                                        <span class="text-success">Active</span>
                                    @else
                                        <span class="text-danger">Not Active</span>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{route('admin.reward-loyalty.list.show', $r->id).'#reward-loyalty'}}" data-toggle="tooltip" title="View Redemption">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                    &nbsp;
                                    @if (auth()->user()->can('access-reward-loyalty'))
                                        <a href="{{route('admin.reward-loyalty.list.edit', $r->id).'#reward-loyalty'}}" data-toggle="tooltip" title="Update">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                    @endif

                                    &nbsp;
                                    <a href="{{route('admin.reward-loyalty.list.history', $r->id).'#reward-loyalty'}}" data-toggle="tooltip" title="History">
                                        <i class="fa fa-history"></i>
                                    </a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td class="text-center" colspan="100%">There is no item</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>

                <div class="text-center">
                    {{$rewards->appends([
                        'keyword' => request('keyword'),
                        'start_date' => request('start_date'),
                        'end_date' => request('end_date'),
                        'status' => request('status'),
                        'show_testing' => request('show_testing'),
                        'user_target' => request('user_target'),
                        'sortBy' => request('sortBy'),
                        'sortOrder' => request('sortOrder'),
                    ])->links()}}
                </div>
            </div>
        </div>
    </div>
@stop