@extends('admin.layouts.main')

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>
        <div class="box-body">
            {!! Form::model($reward, [
                'method' => 'PUT',
                'enctype' => 'multipart/form-data',
                'url' => route('admin.reward-loyalty.list.update', $reward->id),
            ]) !!}
                @include('admin.contents.reward-loyalty.list._form')
            {!! Form::close() !!}
        
            {!! Form::model($reward, [
                'method' => 'PUT',
                'enctype' => 'multipart/form-data',
                'url' => route('admin.reward-loyalty.list.targeted-user.remove', $reward->id),
                'id' => 'form-remove-targeted-user',
            ]) !!}
                <button type="submit" class="hidden">Remove Flag</button>
            {!! Form::close() !!}
        </div>
    </div>

@stop