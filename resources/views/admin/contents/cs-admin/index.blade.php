@extends('admin.layouts.main')

@section('style')
    <style>
        /* Select2 tweak */
        .input-group .select2-container,
        .form-group .select2-container {
            position: relative !important;
            z-index: 2;
            float: left !important;
            width: 100% !important;
            margin-bottom: 0 !important;
            display: table !important;
            table-layout: fixed !important;
        }
        div.required-field label::after { 
            content: "*";
            color: red;
        }
    </style>

    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" />
    <link href="//cdn.jsdelivr.net/npm/select2-bootstrap-theme@0.1.0-beta.10/dist/select2-bootstrap.min.css" rel="stylesheet" />
@endsection

@section('content')
    <div class="box box-default">
        <!-- Box Header -->
        <div class="box-header">
            <div class="col-md-12 text-center">
                <h3 class="box-title"><i class="fa fa-check-square-o"></i> {{ $contentHeader }}</h3>
            </div>
        </div>
        <!-- /.box-header -->

        <div class="box-body no-padding">

            {{-- Top Bar --}}
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default form-inline" style="padding-left: 10px;padding-right: 10px;">
                    <!-- Add button -->
                    <div class="col-md-6 bg-default" style="padding-bottom:10px;">
                        <a href="#" class="btn btn-success btn pull-left actions" data-toggle="modal" data-target="#modal-default">
                            <i class="fa fa-plus">&nbsp;</i>Add CS Admin
                        </a>
                    </div>
                    <!-- Search filters -->
                    <div class="col-md-6 bg-default" style="padding-bottom:10px;text-align:right">
                        <form action="{{ url('/admin/cd-admin') }}" method="get">
                            <input type="text" name="key" class="form-control input-sm" placeholder="Name"
                                autocomplete="off" value="{{ request()->input('key') }}">
                            <button type="submit" class="btn btn-primary btn-md" id="buttonSearch"><i
                                    class="fa fa-search">&nbsp;</i>Search</button>
                        </form>
                    </div>
                </div>
            </div>

            {{-- Table --}}
            <table class="table">
                <thead>
                    <tr>
                        <th></th>
                        <th class="text-center">ID</th>
                        <th class="text-center">User ID</th>
                        <th>Name</th>
                        <th class="text-center">Access Token</th>
                        <th class="text-center">Room Chat</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($csAdmins as $csAdmin)
                    <tr>
                        <td class="text-center">
                            <a href="#" data-image="{{ $csAdmin->photo_url['large'] }}" data-name="{{ $csAdmin->name }}" data-email="{{ $csAdmin->email }}" data-toggle="lightbox" data-max-width="400">
                                <img src="{{ $csAdmin->photo_url['small'] }}" class="img-circle" width="80" height="70">
                            </a>
                        </td>
                        <td class="text-center">{{ $csAdmin->id }}</td>
                        <td class="text-center">{{ $csAdmin->user->id }}</td>
                        <td>
                            <strong class="font-semi-large">{{ $csAdmin->name }}</strong>
                            <br><small>{{ $csAdmin->email }}</small>
                        </td>
                        <td class="text-center">{{ $csAdmin->access_token }}</td>
                        <td class="text-center"><a href="{{ url('/admin/cs-admin/room-chat/'.$csAdmin->id) }}" target="_blank"><i class='fa fa-commenting-o'></i> Open Room</a></td>
                        <td class="table-action-column" style="padding-right:15px!important;">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                    <i class="glyphicon glyphicon-cog"></i> Actions <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a href="#" class="actions" data-toggle="modal" data-target="#modal-update{{ $csAdmin->id }}"><i class="glyphicon glyphicon-edit"></i> Edit Profil</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/admin/cs-admin/delete/'.$csAdmin->id) }}" class="actions" onclick="return confirm('are you sure?')"><i class="glyphicon glyphicon-trash"></i> Hapus Profil</a>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>

            </table>
        </div><!-- /.box-body -->
        <div class="box-footer clearfix">
            {{ $csAdmins->appends(Request::except('page'))->links() }}
        </div>
    </div><!-- /.box -->
    <!-- table -->

    <!-- Modal Created -->
    <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 800px;height: 450px;">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add New CS Admin</h4>
                </div>
                <form class="form-horizontal" method="POST" action="{{ url('/admin/cs-admin/store')}}" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Search Existing User</label>
                                <div class="col-sm-10">
                                    <select id="single" class="form-control select2-single">
                                    </select>
                                    <input type="hidden" id="userId" name="user_id" value="">
                                </div>
                            </div>
                            <hr>
                            <div class="form-group required-field">
                                <label for="inputEmail3" class="col-sm-2 control-label">Name</label>
                                <div class="col-sm-10">
                                    <input type="text" name="name" class="form-control" id="inputName" placeholder="Name" required>
                                </div>
                            </div>
                            <div class="form-group required-field">
                                <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                                <div class="col-sm-10">
                                    <input type="text" name="email" class="form-control" id="inputEmail" placeholder="Email" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Photo</label>
                                <div class="col-sm-10">
                                    <input type="file" class="form-control" id="photoId" name="photo" value="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!-- Modal Edit -->
    @foreach ($csAdmins as $csAdmin)
        <div class="modal fade" id="modal-update{{ $csAdmin->id }}">
            <div class="modal-dialog">
                <div class="modal-content" style="width: 800px;">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Form Update CS Admin : {{ $csAdmin->name }}</h4>
                    </div>
                    <form class="form-horizontal" method="POST" action="{{ url('/admin/cs-admin/update/'.$csAdmin->id) }}" enctype="multipart/form-data">
                        <div class="modal-body">
                            <div class="box-body">
                                <div class="form-group required-field">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Name</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="name" class="form-control" id="inputName" value="{{ $csAdmin->name }}">
                                    </div>
                                </div>
                                <div class="form-group required-field">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="email" class="form-control" id="inputEmail" value="{{ $csAdmin->email }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <img src="{{ $csAdmin->photo_url['medium'] }}" class="img-circle" width="280" height="260">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Photo</label>
                                    <div class="col-sm-10">
                                        <input type="file" class="form-control" id="photoId" name="photo" value="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    @endforeach
    <!-- /.modal -->

@endsection

@section('script')
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>
<script>
$(function() {
    $('#single').select2({
        theme: "bootstrap",
        placeholder: "Name",
        ajax: {
            url: '/admin/users/consultant',
            data: function (params) {
                return {
                    search: params.term,
                    page: params.page || 1
                }
            },
            processResults: function (data, params) {
                var page = params.page || 1;
                if (data.data.length < 1) var total_count = 0;
                else var total_count = data.data[0].total_count;
                return {
                    results: $.map(data.data, item => { 
                        return {
                            id: item.id, 
                            text: item.name + ' (' + item.email + ')'
                        }
                    }),
                    pagination: {
                        more: (page * 10) <= total_count
                    }
                };
            },
            cache: true
        },
        dropdownParent: $("#modal-default")
    });

    // Dropdown listeners
    $("#single").on('select2:select', e => {
        var selected = e.params.data.id;
        console.log(selected);
        if (selected != '') {
            applyUser(selected);
        }
    });

    // Function to fetch single user details
    function applyUser(id) {
        $.ajax({
            type: 'POST',
            url: "/admin/users/user",
            data: {
                id: id
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: (data) => {
                console.log(data)
                $('#userId').val(data.id);
            }
        });
    }
});
</script>


@endsection
