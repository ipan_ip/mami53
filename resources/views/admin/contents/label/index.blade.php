@extends('admin.layouts.main')
@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
            <div class="box-tools pull-right">
                <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <a href="{{ URL::route('admin.label.create') }}">
                        <button type="submit" class="btn-add btn btn-primary btn-sm">
                            <i class="fa fa-plus">&nbsp;</i>Add Label</button>
                    </a>
                </div>
            </div>

            <table id="tableListLabel" class="table table-striped">
                <thead>
                <tr>
                    <th width="30px;">No</th>
                    <th>Name</th>
                    <th>Tags</th>
                    <th>Price Range</th>
                    <th>Priority</th>
                    <th class="table-action-column">Action</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($rowsLabel as $index => $rowLabel)
                        <tr>
                            <td>{{ $index+1 }}</td>
                            <td>{{ $rowLabel->name }}</td>
                            <td>
                                @if ($rowLabel->tag != null && strtolower($rowLabel->tag) !== 'null')
                                    @foreach(json_decode($rowLabel->tag, true) as $tag)
                                        @if (isset($tags[$tag]))
                                            <span class="label label-info">{{ $tags[$tag] }}</span>
                                        @endif
                                    @endforeach
                                @endif
                            </td>
                            <td>{{ $rowLabel->price_min }} - {{ $rowLabel->price_max }}</td>
                            <td>{{ $priority[$rowLabel->priority] }}</td>
                            <td class="table-action-column">
                                <div class="btn-action-group">
                                    <a href="{{ route('admin.label.destroy', $rowLabel->id) }}" title="Delete">
                                        <i class="fa fa-trash-o"></i>
                                    </a>
                                    <a href="{{ route('admin.label.edit', $rowLabel->id) }}" title="Edit">
                                        <i class="fa fa-pencil"></i>
                                    </a>

                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                </tfoot>
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
    <!-- table -->
@stop
@section('script')
    <!-- page script -->
    <script type="text/javascript">
        $(function()
        {
            $("#tableListLabel").dataTable();
        });
    </script>
@stop