@inject('tag', 'App\Entities\Room\Element\Tag')

@extends('admin.layouts.main')
@section('content')
    <div class="box">
        <div class="box-primary">
            <div class="box-header">
                <h3 class="box-title">Create Label</h3>
            </div>

            {!! Form::model($label ,array('route' => $formAction, 'method' => $formMethod, 'class' => 'form-horizontal form-bordered', 'role' => 'form', 'id' => 'formInsertTag')) !!}
            <div class="box-body">
                <div class="no-padding">
                    <div class="form-group">
                        {!! Form::label('name', 'Nama Label',['class' => 'col-sm-2 control-label']) !!}
                        <div class="col-sm-10">
                        {!! Form::text('name', Input::old('name',$label->name), ['class' => 'form-control', 'placeholder' => 'Name', 'required' => true]) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('tags', 'Tags',['class' => 'col-sm-2 control-label']) !!}
                        <div class="col-sm-10">
                        {!! Form::select('tags[]',$tag->all()->pluck('name','id'),Input::old('tags',$label->tags), ['class' => 'form-control chosen-select', 'multiple' => true]) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('price', 'Price',['class' => 'col-sm-2 control-label']) !!}
                        <div class="col-sm-5">
                            {!! Form::number('price_min', Input::old('price_min',$label->price_min), ['class' => 'form-control', 'placeholder' => 'Price minimum', 'required' => true]) !!}
                        </div>
                        <div class="col-sm-5">
                            {!! Form::number('price_max', Input::old('price_max',$label->price_max), ['class' => 'form-control', 'placeholder' => 'Price maximum', 'required' => true]) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('priority', 'Priority',['class' => 'col-sm-2 control-label']) !!}
                        <div class="col-sm-10">
                            {!! Form::select('priority',[0 => 'Highest', 1 => 'High', 2 => 'Medium', 3 => 'Low', 4 => 'Lowest'],Input::old('tags',$label->priority), ['class' => 'form-control']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2">
                            <div class="col-sm-10">
                                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                                <a href="{{ route('admin.label.index') }}">
                                {!! Form::button('Cancel', ['class' => 'btn btn-default']) !!}
                                </a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop
@section('style')
    {{ HTML::style('assets/vendor/jquery-chosen/chosen.css') }}
    {{ HTML::style('assets/vendor/jquery-chosen-custom/chosen.css') }}

    <style>
        #chosenForm .chosen-choices {
            border: 1px solid #ccc;
            border-radius: 4px;
            min-height: 34px;
            padding: 6px 12px;
        }
        #chosenForm .form-control-feedback {
            /* To make the feedback icon visible */
            z-index: 100;
        }
    </style>
@stop
@section('script')
    {{ HTML::script('assets/vendor/jquery-chosen/chosen.jquery.js') }}
    <script type="text/javascript">
        var config = {
            '.chosen-select'           : {width: '100%'}
        };
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }
    </script>
@stop