@extends('admin.layouts.main')

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>
        <div class="box-body">
            {!! Form::open([
                'method' => 'POST',
                'url' => route('admin.polling.question.store'),
                'class' => 'form-horizontal'
            ]) !!}
                @include('admin.contents.polling.question._form')
            {!! Form::close() !!}
        </div>
    </div>

@stop