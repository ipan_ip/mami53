@extends('admin.layouts.main')

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>
        <div class="box-body">
            {!! Form::model($question, [
                'method' => 'PUT',
                'url' => route('admin.polling.question.update', $question->id),
                'class' => 'form-horizontal'
            ]) !!}
                @include('admin.contents.polling.question._form')
            {!! Form::close() !!}
        </div>
    </div>

@stop