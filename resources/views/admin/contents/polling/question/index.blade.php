@extends('admin.layouts.main')

@section('style')
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" />
<link href="//cdn.jsdelivr.net/npm/select2-bootstrap-theme@0.1.0-beta.10/dist/select2-bootstrap.min.css" rel="stylesheet" />
<style>
    .form-filter .form-group {
        margin: 0px 4px;
    }
</style>
@stop

@section('script')
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>
<script>
    $('.select2').select2({
        theme: "bootstrap"
    })
</script>
@endsection

@section('content')
<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">{{ $boxTitle }}</h3>
    </div>

    <div class="box-body with-border row">
        <div class="col-md-3">
            <a href="{{route('admin.polling.question.create', ['#polling'])}}" class="btn btn-primary">
                <i class="fa fa-plus"></i> Add Question
            </a>
        </div>

        {!!Form::model(request(), ['method' => 'GET', 'class' => 'form-inline form-filter text-right col-md-9 mb-2', 'autocomplete' => 'off'])!!}
            <div class="form-group">
                {!! Form::text('keyword', null, ['class' => 'form-control', 'placeholder' => 'ID / Question...']) !!}
            </div>
            <div class="form-group text-left">
                {!!Form::select('polling', $pollingDropdown, null, ['class' => 'form-control select2'])!!}
            </div>
            <div class="form-group">
                {!!Form::select('status', [
                    'all' => 'Status: All',
                    0 => 'Status: Draft',
                    1 => 'Status: Active',
                ], null, ['class' => 'form-control'])!!}
            </div>
            <div class="form-group text-left">
                {!!Form::select('type', $questionTypeDropdown, null, ['class' => 'form-control select2'])!!}
            </div>
            <div class="form-group action text-center">
                <div>
                    <input type="submit" class="btn btn-primary btn-md" value="Filter"/>
                    <a href="{{route('admin.polling.question.index', ['#polling'])}}" class="btn btn-default btn-md" data-toggle="tooltip" title="Reset"><i class="fa fa-refresh"></i></a>
                </div>
            </div>
        {!!Form::close()!!}
    </div>

    <div class="box-body">
        <div class="horizontal-wrapper">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th style="width: 40px;">ID</th>
                        <th>Question</th>
                        <th>Polling</th>
                        <th>Type</th>
                        <th>Sequence</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($questions as $row)
                        <tr>
                            <td>{{$row->id}}</td>
                            <td>{{$row->question}}</td>
                            <td>{{$row->polling->key}}</td>
                            <td>
                                {{App\Entities\Polling\PollingQuestion::getTypesDropdown()[$row->type]}}
                                @if ($row->is_optionable)
                                    <a href="{{route('admin.polling.option.index', ['question' => $row->id, '#polling'])}}" data-toggle="tooltip" title="Manage Options">
                                        <b>({{$row->options_count}} options)</b>
                                    </a>
                                @endif
                            </td>
                            <td><span class="badge">{{$row->sequence}}</span></td>
                            <td>
                                @if ($row->is_active)
                                    <span class="label label-success">Active</span>
                                @else
                                    <span class="label label-default">Draft</span>
                                @endif
                            </td>
                            <td class="text-right">
                                @if ($row->is_optionable)
                                    <a href="{{route('admin.polling.option.index', ['question' => $row->id, '#polling'])}}" data-toggle="tooltip" title="Manage Options">
                                        <i class="fa fa-list"></i>
                                    </a>
                                    &nbsp;
                                @endif
                                <a href="{{route('admin.polling.question.edit', [$row->id, '#polling'])}}" data-toggle="tooltip" title="Update">
                                    <i class="fa fa-pencil"></i>
                                </a>
                                &nbsp;
                                <a href="{{route('admin.polling.question.destroy', [$row->id, '#polling'])}}" class="text-danger" data-method="delete" data-confirm="Are you sure to delete this item?" data-toggle="tooltip" title="Delete">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td class="text-left" colspan="100%">There is no item</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>

            <div class="text-center">
                {{$questions->appends([
                    'keyword' => request('keyword'),
                    'polling' => request('polling'),
                    'type' => request('type'),
                    'status' => request('status'),
                ])->links()}}
            </div>
        </div>
    </div>
</div>
@stop