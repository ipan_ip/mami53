@section('style')
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" />
<link href="//cdn.jsdelivr.net/npm/select2-bootstrap-theme@0.1.0-beta.10/dist/select2-bootstrap.min.css" rel="stylesheet" />
@endsection

@section('script')
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>
<script>
    $('.select2').select2({
        theme: "bootstrap"
    })
</script>
@endsection

<div class="row">
    <div class="col-md-8">
        @if (!empty($question))
            <div class="form-group">
                {!!Form::label('polling_id', 'Polling*', ['class' => 'col-sm-3 control-label'])!!}
                <div class="col-sm-9">
                    {!!Form::hidden('polling_id', null)!!}
                    <p class="form-control-static">{{$question->polling->key}}</p>
                </div>
            </div>
        @else
            <div class="form-group">
                {!!Form::label('polling_id', 'Polling*', ['class' => 'col-sm-3 control-label'])!!}
                <div class="col-sm-9">
                    {!!Form::select('polling_id', $pollingDropdown, null, ['class' => 'form-control select2'])!!}
                </div>
            </div>
        @endif
        <div class="form-group">
            {!!Form::label('question', 'Question*', ['class' => 'col-sm-3 control-label'])!!}
            <div class="col-sm-9">
                {!!Form::textarea('question', null, ['rows' => 3, 'class' => 'form-control'])!!}
            </div>
        </div>
        <div class="form-group">
            {!!Form::label('type', 'Question Type*', ['class' => 'col-sm-3 control-label'])!!}
            <div class="col-sm-9">
                {!!Form::select('type', $questionTypeDropdown, null, ['class' => 'form-control select2'])!!}
            </div>
        </div>
        <div class="form-group">
            {!!Form::label('sequence', 'Sequence', ['class' => 'col-sm-3 control-label'])!!}
            <div class="col-sm-9">
                {!!Form::number('sequence', null, ['class' => 'form-control'])!!}
            </div>
        </div>
        <div class="form-group">
            {!!Form::label('is_active', 'Is Active', ['class' => 'col-sm-3 control-label'])!!}
            <div class="col-sm-9">
                <label class="radio-inline">
                    {!!Form::radio('is_active', 1, !empty($question) ? null : true)!!} Active
                </label>
                <label class="radio-inline">
                    {!!Form::radio('is_active', 0, null)!!} Draft
                </label>
            </div>
        </div>

        <hr>
        <div class="form-group">
            <div class="col-sm-9 col-sm-offset-3">
                <button type="submit" class="btn btn-primary">
                    @if (!empty($question))
                        Update Question
                    @else
                        Add Question
                    @endif
                </button>
                &nbsp;&nbsp;
                <a href="{{route('admin.polling.question.index', ['#polling'])}}" class="btn btn-default btn-cancel">Cancel</a>
            </div>
        </div>
    </div>
</div>