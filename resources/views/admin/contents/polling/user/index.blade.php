@extends('admin.layouts.main')

@section('style')
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" />
<link href="//cdn.jsdelivr.net/npm/select2-bootstrap-theme@0.1.0-beta.10/dist/select2-bootstrap.min.css" rel="stylesheet" />
<style>
    .form-filter .form-group {
        margin: 0px 4px;
    }
</style>
@stop

@section('script')
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>
<script>
    $('.select2').select2({
        theme: "bootstrap"
    })
</script>
@endsection

@section('content')
<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">{{ $boxTitle }}</h3>
    </div>

    <div class="box-body with-border row">
        {!!Form::model(request(), ['method' => 'GET', 'class' => 'form-inline form-filter text-right col-md-12 mb-2', 'autocomplete' => 'off'])!!}
            <div class="form-group">
                {!! Form::text('keyword', null, ['class' => 'form-control', 'placeholder' => 'Search Answer...']) !!}
            </div>
            <div class="form-group text-left">
                {!!Form::select('user', $userDropdown, null, [
                    'class' => 'form-control select2',
                ])!!}
            </div>
            <div class="form-group text-left">
                {!!Form::select('question', $questionDropdown, null, [
                    'class' => 'form-control select2',
                    'style' => 'width: 500px;'
                ])!!}
            </div>
            <div class="form-group action text-center">
                <div>
                    <input type="submit" class="btn btn-primary btn-md" value="Filter"/>
                    <a href="{{route('admin.polling.user.index', ['#polling'])}}" class="btn btn-default btn-md" data-toggle="tooltip" title="Reset"><i class="fa fa-refresh"></i></a>
                </div>
            </div>
        {!!Form::close()!!}
    </div>

    <div class="box-body">
        <div class="horizontal-wrapper">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th style="width: 40px;">ID</th>
                        <th>User</th>
                        <th>[Polling] - Question</th>
                        <th>Answer Option</th>
                        <th>Answer</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($userPollings as $row)
                        <tr>
                            <td>{{$row->id}}</td>
                            <td>{{$row->user->name}}</td>
                            <td>
                                [{{$row->question->polling->key}}] -
                                <span data-toggle="tooltip" title="{{$row->question->question}}">
                                    {{ substr($row->question->question, 0, 25) . (strlen($row->question->question) > 25 ? '...' : '') }}
                                </span>
                            </td>
                            <td>
                                {{optional($row->option)->option ?: '-'}}
                            </td>
                            <td>
                                {{$row->answer}}
                            </td>
                            <td></td>
                        </tr>
                    @empty
                        <tr>
                            <td class="text-left" colspan="100%">There is no item</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>

            <div class="text-center">
                {{$userPollings->appends([
                    'keyword' => request('keyword'),
                    'user' => request('user'),
                    'question' => request('question'),
                ])->links()}}
            </div>
        </div>
    </div>
</div>
@stop