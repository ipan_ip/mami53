@extends('admin.layouts.main')

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>
        <div class="box-body">
            {!! Form::model($option, [
                'method' => 'PUT',
                'url' => route('admin.polling.option.update', $option->id),
                'class' => 'form-horizontal'
            ]) !!}
                @include('admin.contents.polling.option._form')
            {!! Form::close() !!}
        </div>
    </div>

@stop