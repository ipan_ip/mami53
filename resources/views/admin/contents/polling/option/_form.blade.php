@section('style')
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" />
<link href="//cdn.jsdelivr.net/npm/select2-bootstrap-theme@0.1.0-beta.10/dist/select2-bootstrap.min.css" rel="stylesheet" />
@endsection

@section('script')
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>
<script>
    $('.select2').select2({
        theme: "bootstrap"
    })
</script>
@endsection

<div class="row">
    <div class="col-md-8">
        @if (!empty($option))
            <div class="form-group">
                {!!Form::label('polling_text', 'Polling*', ['class' => 'col-sm-3 control-label'])!!}
                <div class="col-sm-9">
                    <p class="form-control-static">{{$option->question->polling->key}}</p>
                </div>
            </div>
            <div class="form-group">
                {!!Form::label('question_id', 'Question*', ['class' => 'col-sm-3 control-label'])!!}
                <div class="col-sm-9">
                    {!!Form::hidden('question_id', null)!!}
                    <p class="form-control-static">{{$option->question->question}}</p>
                </div>
            </div>
        @else
            <div class="form-group">
                {!!Form::label('question_id', 'Question*', ['class' => 'col-sm-3 control-label'])!!}
                <div class="col-sm-9">
                    {!!Form::select('question_id', $questionDropdown, null, ['class' => 'form-control select2', 'style' => 'width: 100%;'])!!}
                </div>
            </div>
        @endif
        <div class="form-group">
            {!!Form::label('option', 'Option*', ['class' => 'col-sm-3 control-label'])!!}
            <div class="col-sm-9">
                {!!Form::textarea('option', null, ['rows' => 3, 'class' => 'form-control'])!!}
            </div>
        </div>
        <div class="form-group">
            {!!Form::label('setting', 'Setting', ['class' => 'col-sm-3 control-label'])!!}
            <div class="col-sm-9">
                {!!Form::select('setting', $settingDropdown, null, ['class' => 'form-control select2'])!!}
            </div>
        </div>
        <div class="form-group">
            {!!Form::label('sequence', 'Sequence', ['class' => 'col-sm-3 control-label'])!!}
            <div class="col-sm-9">
                {!!Form::number('sequence', null, ['class' => 'form-control'])!!}
            </div>
        </div>

        <hr>
        <div class="form-group">
            <div class="col-sm-9 col-sm-offset-3">
                <button type="submit" class="btn btn-primary">
                    @if (!empty($option))
                        Update Option
                    @else
                        Add Option
                    @endif
                </button>
                &nbsp;&nbsp;
                <a href="{{route('admin.polling.option.index', ['#polling'])}}" class="btn btn-default btn-cancel">Cancel</a>
            </div>
        </div>
    </div>
</div>