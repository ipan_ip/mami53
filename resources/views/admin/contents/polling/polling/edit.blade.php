@extends('admin.layouts.main')

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>
        <div class="box-body">
            {!! Form::model($polling, [
                'method' => 'PUT',
                'enctype' => 'multipart/form-data',
                'url' => route('admin.polling.update', $polling->id),
                'class' => 'form-horizontal'
            ]) !!}
                @include('admin.contents.polling.polling._form')
            {!! Form::close() !!}
        </div>
    </div>

@stop