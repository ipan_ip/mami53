@extends('admin.layouts.main')

@section('style')
<style>
    .form-filter .form-group {
        margin: 0px 4px;
    }
</style>
@stop

@section('content')
<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">{{ $boxTitle }}</h3>
    </div>

    <div class="box-body with-border row">
        <div class="col-md-3">
            <a href="{{route('admin.polling.create', ['#polling'])}}" class="btn btn-primary">
                <i class="fa fa-plus"></i> Add Polling
            </a>
        </div>

        {!!Form::model(request(), ['method' => 'GET', 'class' => 'form-inline form-filter text-right col-md-9 mb-2', 'autocomplete' => 'off'])!!}
            <div class="form-group">
                {!! Form::text('keyword', null, ['class' => 'form-control', 'placeholder' => 'ID / Polling Name']) !!}
            </div>
            <div class="form-group">
                {!!Form::select('status', [
                    'all' => 'Status: All',
                    0 => 'Status: Draft',
                    1 => 'Status: Active',
                ], null, ['class' => 'form-control'])!!}
            </div>
            <div class="form-group action text-center">
                <div>
                    <input type="submit" class="btn btn-primary btn-md" value="Filter"/>
                    <a href="{{route('admin.polling.index', ['#polling'])}}" class="btn btn-default btn-md" data-toggle="tooltip" title="Reset"><i class="fa fa-refresh"></i></a>
                </div>
            </div>
        {!!Form::close()!!}
    </div>

    <div class="box-body">
        <div class="horizontal-wrapper">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th style="width: 40px;">ID</th>
                        <th>Name</th>
                        <th>Questions Count</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($pollings as $row)
                        <tr>
                            <td>{{$row->id}}</td>
                            <td>{{$row->key}}</td>
                            <td><span class="badge">{{$row->questions_count}}</span></td>
                            <td>
                                @if ($row->is_active)
                                    <span class="label label-success">Active</span>
                                @else
                                    <span class="label label-default">Draft</span>
                                @endif
                            </td>
                            <td>
                                <a href="{{route('admin.polling.question.index', ['polling' => $row->id, '#polling'])}}" data-toggle="tooltip" title="Manage Questions">
                                    <i class="fa fa-list"></i>
                                </a>
                                &nbsp;
                                <a href="{{route('admin.polling.edit', [$row->id, '#polling'])}}" data-toggle="tooltip" title="Update">
                                    <i class="fa fa-pencil"></i>
                                </a>
                                &nbsp;
                                <a href="{{route('admin.polling.destroy', [$row->id, '#polling'])}}" class="text-danger" data-method="delete" data-confirm="Are you sure to delete this item?" data-toggle="tooltip" title="Delete">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td class="text-left" colspan="100%">There is no item</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>

            <div class="text-center">
                {{$pollings->appends([
                    'keyword' => request('keyword'),
                    'status' => request('status'),
                ])->links()}}
            </div>
        </div>
    </div>
</div>
@stop