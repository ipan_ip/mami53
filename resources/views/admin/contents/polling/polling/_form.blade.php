<div class="row">
    <div class="col-md-8">
        <div class="form-group">
            {!!Form::label('key', 'Polling Name*', ['class' => 'col-sm-3 control-label'])!!}
            <div class="col-sm-9">
                {!!Form::text('key', null, ['class' => 'form-control'])!!}
            </div>
        </div>
        <div class="form-group">
            {!!Form::label('descr', 'Polling Description', ['class' => 'col-sm-3 control-label'])!!}
            <div class="col-sm-9">
                {!!Form::textarea('descr', null, ['rows' => 3, 'class' => 'form-control'])!!}
            </div>
        </div>
        <div class="form-group">
            {!!Form::label('is_active', 'Is Active', ['class' => 'col-sm-3 control-label'])!!}
            <div class="col-sm-9">
                <label class="radio-inline">
                    {!!Form::radio('is_active', 1, !empty($polling) ? null : true)!!} Active
                </label>
                <label class="radio-inline">
                    {!!Form::radio('is_active', 0, null)!!} Draft
                </label>
            </div>
        </div>
        
        <hr>
        <div class="form-group">
            <div class="col-sm-9 col-sm-offset-3">
                <button type="submit" class="btn btn-primary">
                    @if (!empty($polling))
                        Update Polling
                    @else
                        Add Polling
                    @endif
                </button>
                &nbsp;&nbsp;
                <a href="{{route('admin.polling.index', ['#polling'])}}" class="btn btn-default btn-cancel">Cancel</a>
            </div>
        </div>
    </div>
</div>