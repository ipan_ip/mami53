@extends('admin.layouts.main')

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

        <form action="{{ URL::route('admin.permission.update', $permission->id) }}" method="POST" class="form-horizontal form-bordered">
            <input type="hidden" name="_method" value="PUT">
            
            <div class="box-body no-padding">
                <div class="form-group bg-default">
                    <label for="permission-name" class="control-label col-sm-2">Premission Name</label>
                    <div class="col-sm-10">
                        <input type="text" name="name" id="permission-name" class="form-control" value="{{ old('name', $permission->name) }}">
                        <span class="help-block bg-warning">
                            Warning!! Merubah Permission Name dapat membuat fitur yang berkaitan tidak bisa diakses.
                        </span>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="display-name" class="control-label col-sm-2">Display Name</label>
                    <div class="col-sm-10">
                        <input type="text" name="display_name" id="display-name" class="form-control" value="{{ old('display_name', $permission->display_name) }}">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <div class="col-sm-10 col-sm-push-2">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection