@extends('admin.layouts.main')

@section('content')
	<!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <a href="{{ URL::route('admin.permission.create') }}">
                            <button type="button" name="button" class="btn-add btn btn-primary btn-sm">
                                <i class="fa fa-plus">&nbsp;</i> Add Permission </button>
                    </a>

                    <form action="" method="get" class="form-inline pull-right">
                        <input type="text" name="q" class="form-control input-sm"  placeholder="permission name"  autocomplete="off" value="{{ request()->input('q') }}">
                        <button class="btn btn-primary btn-md" id="buttonSearch"><i class="fa fa-search">&nbsp;</i>Search</button>
                    </form>
                </div>
            </div>

            <table  class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Permission Name</th>
                        <th>Display Name</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                @foreach($permissions as $permission)
                    <tr>
                        <td>{{ $permission->id }}</td>
                        <td>{{ $permission->name }}</td>
                        <td>{{ $permission->display_name }}</td>
                        <td>
                            <a href="{{ route('admin.permission.edit', $permission->id) }}" title="Edit">
                                <i class="fa fa-pencil"></i>
                            </a>
                        </td>
                    </tr> 
                @endforeach
                </tbody>
                
            </table>
        </div><!-- /.box-body -->
        <div class="box-body no-padding">
            {{ $permissions->appends(Request::except('page'))->links() }}
        </div>
    </div><!-- /.box -->
    <!-- table -->
    <script>
        $( "#flush-role-button" ).click(function() {
            $.get( "/admin/role/flush", function( data ) {
                // Response Example
                // Case #1. Success
                // {"result":true}
                // Case #2. Fail
                // {"result":false,"errorMessage":"Too frequently called. Interval must be longer than 1 minutes"}
                if (data.result)
                {
                    alert('Success!\nRoles and permissions are updated on the service.')
                }
                else
                {
                    alert('Fail!\n' + data.errorMessage);
                }
            });
        });
    </script>
@endsection