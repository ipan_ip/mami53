@extends('admin.layouts.main')

@section('style')
<style>
    span.apartment-color,
    tr.apartment-color td {
        color: #0D47A1;
    }
    tr.kost-active td {
        background-color: #C8E6C9;
    }

    .table>tbody>tr>td {
	vertical-align: middle;
    }

    .font-large {
        font-size: 1.5em;
    }

    .font-semi-large {
        font-size: 1.15em;
    }

    .font-grey {
        color: #9E9E9E;
    }

    .dropdown-menu>li>a:hover {
        color: #333
    }

    .label-grey {
        background-color: #CFD8DC;
    }

    /* Sweetalert */

    .swal-wide {
        width: 850px !important;
        /* font-size: 14px!important; */
    }

    .swal-shown {
        height: auto!important;
    }

    .custom-swal {
        z-index: 10000!important;
    }

    .custom-wide-swal {
        width:850px !important;
    }
</style>
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
@endsection

@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <form action="" class="form-inline" method="get" style="text-align:right;padding:10px;">
                        <a href="{{ route('admin.room.index') }}" class="btn btn-success btn pull-left">
                            <i class="fa fa-plus">&nbsp;</i>Add VRTour
                        </a>

                        <input type="text" name="q" id="phone" class="form-control input-sm" placeholder="Nama Kost"
                            autocomplete="off" value="{{ request()->input('q') }}">

                        @if(Auth::user())
                            <input type="text" name="agent" class="form-control input-sm" placeholder="Agent name"
                                autocomplete="off" value="{{ request()->input('agent') }}">

                            <input type="text" name="from" class="form-control input-sm datepicker" placeholder="Start Date"
                                value="{{ request()->input('from') }}" id="start-date" autocomplete="off"
                                style="margin-right:2px" />

                            <input type="text" name="to" class="form-control input-sm datepicker" placeholder="End Date"
                                value="{{ request()->input('to') }}" id="end-date" autocomplete="off"
                                style="margin-right:2px" />

                            {{ Form::select('o', [
                                'lc' => 'Last Created',
                                'fc' => 'First Created',
                                'lu' => 'Last Updated',
                                'fu' => 'First Updated',
                                'm'  => 'Most Mobile View',
                                'w'  => 'Most Web View',
                                'l'  => 'Most Likes',
                                'c'  => 'Most Comments',
                                's'  => 'Most Shares',
                            ], request()->input('o'), array('class' => 'pull-right-sort form-group btn btn-default')) }}

                            {{ Form::select('input-type', [
                                "Semua",
                                "Agen",
                                "Pemilik Kos",
                                "Pengelola Kos", 
                                "Lainnya", 
                                "Anak Kos", 
                                "Kos Duplikat", 
                                'Pemilik Apartemen', 
                                'Pengelola Apartemen', 
                                'Agen Apartemen', 
                                'Gojek', 
                                'scrap', 
                                'scrap google', 
                                'scrap koran'
                            ], request()->input('input'), array('class' => 'pull-right-sort form-group btn btn-default')) }}

                            {{ Form::select('active', ["All", "Active - Not active"], request()->input('active'), array('class' => 'pull-right-sort form-group btn btn-default')) }}

                            {{ Form::select('bookable', [
                                99 => "Semua",
                                1 => "Booking Kost", 
                                0 => "Non-booking Kost"
                            ], request()->input('bookable'), array('class' => 'pull-right-sort form-group btn btn-default')) }}
                        @endif

                        <button class="btn btn-primary btn-sm" id="buttonSearch">
                            <i class="fa fa-search">&nbsp;</i>Search
                        </button>
                    </form>
                </div>
            </div>
            <div class="horizontal-wrapper" style="display:none" id="formStorySearch">
                {{-- {{ Form::open(array('url'=>$searchUrl , 'method'=> 'POST')) }}
                    <div class="btn-horizontal-group bg-default row">
                        <div class="col-sm-10">
                            <input type="text" name="search_keyword" class="form-control" />
                        </div>
                        <div class="col-sm-2">
                            <button class="btn btn-sm btn-info">Search</button>
                        </div>
                    </div>
                {{ Form::close() }} --}}
            </div>
            <div class="col-md-12" style="margin-top: 10px;margin-right:10px;">
                <div class="alert alert-success alert-dismissable"><i class="fa fa-info-circle fa-2x"></i> <strong>VR Tour yang aktif ditandai dengan background HIJAU</strong></div>
            </div>
                        
            <!-- The table is going here -->
            @include('admin.contents.vrtour.partial.index-table-admin')
            <!-- End of index table -->

        </div><!-- /.box-body -->
        <div class="box-body no-padding">
            {{ $rowsDesigner->appends(Request::except('page'))->links() }}
        </div>
    </div><!-- /.box -->
    <!-- table -->

    {{-- @include('admin.partials.delete-modal') --}}
@stop

@section('script')
<!-- Holder for Upload File Thumbnail -->
{{ \Html::script('assets/vendor/holder/holder.js') }}

<script src="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.js"></script>

<!-- page script -->
<script type="text/javascript">

    $(function()
    {
        $('#select-sort-option').change(function(event){
            var url  = '{{ URL::route("admin.room.index") }}' + '?o=' + event.target.value ;

            url += '&st=' + $('#start-date').val();
            url += '&en=' + $('#end-date').val();

            window.location = url;

        });

        $('.datepicker').datepicker({ dateFormat : 'dd-mm-yy'});
    });

    $(function()
    {
        Holder.addTheme("thumbnail", {
            background: '#EEE',
            foreground: '#AAA',
            size: 11,
            fontweight: 'normal',
            text: 'Image'
          });
        // $("#tableListDesigner").dataTable();
    });

    $(function(){


        var token = '{{ csrf_token() }}';

        // $('.btn-delete').click(function() {
        //     var id = $(this).data('id');
        //     var name = $(this).data('name');

        //     var confirm = window.confirm("Are you sure want to delete " + name +" ?");

        //     if (confirm) {
        //         $.post('/{{Request::segment(1)}}/{{Request::segment(2)}}/' + id, {
        //             _method: "delete",
        //             _token: token,
        //         }).success(function(response) {
        //             console.log(response);
        //             window.location.reload();
        //         });
        //     }


        // });


        // $('#telp_now').click(function(e) {
        //     phone = $("#phone").val();
        //     email = $("#user_login").val();

        //     //$("#loading-phone").append("Loading ................ ");
        //     $.ajax({
        //         url: '/admin/squarepants/notify?phone='+phone+"&email="+email+"&id=&kos_name=&search=search",
        //         type: 'GET',
        //         dataType: 'json',
        //         success: function(data) {
        //         // console.log(data)
        //         //$("#loading-phone").empty();
        //         if(data.status == false) {
        //             alert("Gagal ngirim nomer ke hp");
        //             //$("#loading-phone").append("<div class='alert alert-danger' style='width: 100%;'>Nomer hp tidak sesuai format.</div>");
        //         } else {
        //             alert("Berhasil ngirim nomer ke hp");
        //             //$("#loading-phone").append("<div class='alert alert-success' style='width: 100%;'>Cek hp anda.");
        //         }
        //         }
        //     });
        // });

        // $('.btn-action').on('click', (e) => {
        //     e.preventDefault();
        //     var _action = $(e.currentTarget).data('action');
        //     var _id = $(e.currentTarget).data('id');

        //     if (_action == 'activate') {
        //         var title = 'Aktifkan Foto Premium?';
        //         var subTitle = '';
        //         var type = 'question';
        //         var status = 1;
        //     } else {
        //         var title = 'Non Aktifkan Foto Premium?';
        //         var subTitle = 'Foto kost akan kembali menggunakan foto dengan watermark';
        //         var type = 'warning';
        //         var status = 0;
        //     }

        //     Swal.fire({
        //             title: title,
        //             text: subTitle,
        //             type: type,
        //             confirmButtonText: 'Yes',
        //             showLoaderOnConfirm: true,
        //             preConfirm: () => {
        //                 return $.ajax({
        //                     type: 'POST',
        //                     url: "/admin/card/premium/activate",
        //                     dataType: 'json',
        //                     headers: {
        //                         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //                     },
        //                     data: {
        //                         'id': _id,
        //                         'status': status,
        //                         '_token': token
        //                     },
        //                     success: (response) => {
        //                         if (!response.success) {
        //                             Swal.insertQueueStep({
        //                                 type: 'error',
        //                                 title: response.message
        //                             });
        //                             return;
        //                         }
        //                     },
        //                     error: (error) => {
        //                         Swal.insertQueueStep({
        //                             type: 'error',
        //                             title: 'Error: ' + error.message
        //                         });
        //                     }
        //                 });
        //             },
        //             allowOutsideClick: () => !Swal.isLoading()
        //         }).then((result) => {
        //             if (result.value.success) {
        //                 Swal.fire({
        //                     type: 'success',
        //                     title: result.value.message,
        //                     text: "Halaman akan di-refresh setelah Anda klik OK",
        //                     onClose: () => {
        //                         window.location.reload();
        //                     }
        //                 });
        //             } else {
        //                 Swal.fire({
        //                     type: 'error',
        //                     title: result.value.message,
        //                     onClose: () => {
        //                         window.location.reload();
        //                     }
        //                 });
        //             }
        //         })
        // })
        
        // $('.view-source').on('click', (e) => {
        //     e.preventDefault();
        //     var data = $(e.currentTarget).data('room');
        //     var swal_content = "<div class='text-left'><pre>" + data + "</pre></div>";
        //         Swal.fire({
        //             title: 'Data Source',
        //             customClass: 'custom-wide-swal',
        //             html: swal_content,
        //             showCloseButton: true,
        //             confirmButtonText: 'CLOSE'
        //         });
        // })

    });

</script>
@stop
