    <table id="tableListDesigner" class="table">
    <thead>
        <tr>
            <th></th>
            <th width="150px"></th>
            <th>Nama Kost / Apartemen</th>
            <!-- <th class="text-center">Status</th> -->
            <th class="text-center" width="10%">Matterport ID</th>
            <th class="text-center" width="10%">VR Tour Active</th>
            <th class="text-center" width="10%">Created / Updated</th>
            <th class="table-action-column" width="10%"></th>
        </tr>
    </thead>
    <tbody>
        @foreach ($rowsRoom as $rowDesignerKey => $rowDesigner)
        <tr class="{{ $rowDesigner['is_vrtour_active']=='true' ? 'kost-active' : '' }}">
            <td class="text-center" style="padding-left:10px;">
                @if (!is_null($rowDesigner['apartment_project_id']))
                    <i class="fa fa-building fa-2x {{ $rowDesigner['is_active']=='true' ? 'text-success' : '' }}" data-toggle="tooltip" title="Apartemen"></i>
                @else
                    <i class="fa fa-home fa-2x {{ $rowDesigner['is_active']=='true' ? 'text-success' : '' }}" data-toggle="tooltip" title="Kost"></i>
                @endif
            </td>

            <td class="text-center">
                <img width="150" class="img-thumbnail" src="{{ $rowDesigner['photoUrl'] }}" data-src="holder.js/60x77/thumbnail" alt="">
            </td>
            
            <td style="font-size:14px">
                {{-- {{ $rowDesigner['matterport_id'] }}<br/> --}}

                <small>{{ $rowDesigner['id'] }}</small>

                <br/>

                <strong>
                    @if(!is_null($rowDesigner['duplicate_from'])) 
                        <i class="fa fa-retweet" style="color: #ff0000;"></i> 
                    @endif

                    {{ $rowDesigner['name'] }}
                </strong>

                <br/>
                
                {{ $rowDesigner['area_city'] }}

                <br/>
                @if ($rowDesigner['verified_by_mamikos'])
                    <span class="label label-sm label-primary" data-toggle="tooltip" title="Verified by Mamikos"><i class="fa fa-check-circle"></i> MamiVerify</span>
                @endif
                @if ($rowDesigner['by_mamichecker'] == true)
                    <span class="label label-sm label-primary" data-toggle="tooltip" title="Checked by Mamichecker"><i class="fa fa-check-square-o"></i> MamiChecker</span>
                @endif
                @if ($rowDesigner['promoted'] == true)
                    <span class="label label-sm label-warning" data-toggle="tooltip" title="Promoted"><i class="fa fa-arrow-up"></i> Promoted</span>
                @endif
                @if ($rowDesigner['is_booking'] == 1)
                    <span class="label label-sm label-success" data-toggle="tooltip" title="Bisa Booking"><i class="fa fa-bookmark"></i> Booking</span>
                @endif
                @if (isset($rowDesigner['price_monthly']['discount_price']))
                    <span class="label label-sm label-primary" data-toggle="tooltip" title="Ada Discount Aktif"><i class="fa fa-percent"></i> Discount</span>
                @endif
                @if ($rowDesigner['is_premium_photo'] == 1)
                    <span class="label label-sm label-info" data-toggle="tooltip" title="Premium Photos"><i class="fa fa-camera"></i> Premium</span>
                @endif
            </td>

            <td align="center">
                {{ $rowDesigner['matterport_id'] }}
            </td>

            <td align="center">
                <p class="{{ $rowDesigner['is_vrtour_active'] ? 'text-success' : 'text-muted' }}">{{ $rowDesigner['is_vrtour_active'] ? 'Active' : 'Not Active' }}</p>
            </td>

            {{-- <td align="center">{{ $rowDesigner['status'] }} </td> --}}
            <td align="center">
                {{ date('j F Y', strtotime($rowDesigner['created_at'])) }} /<br/>{{ date('j F Y', strtotime($rowDesigner['updated_at'])) }}
            </td>

            <td class="table-action-column">
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-cog"></i> <span class="caret"></span>
                    </button>

                    <ul class="dropdown-menu pull-right">
                        @if(is_null($rowDesigner['apartment_project_id']))
                        <li><a href="/room/{{ $rowDesigner['slug'] }}" title="Preview" target="_blank" rel="noopener">
                                <i class="fa fa-external-link"></i> Preview
                            </a></li>
                        @else
                        <li><a href="/unit/{{ $rowDesigner['apartment_project_slug'] }}/{{ $rowDesigner['slug'] }}" title="Preview" target="_blank" rel="noopener">
                                <i class="fa fa-external-link"></i> Preview
                            </a></li>
                        @endif

                        <li>
                            <a href="{{ route('admin.vrtour.edit',$rowDesigner['attachment_id']) }}" title="Edit Cards">
                                <i class="fa fa-pencil"></i> Edit VR Tour
                            </a>
                        </li>

                        <li>
                            <form method="POST" action="{{ route('admin.vrtour.destroy', $rowDesigner['attachment_id']) }}" id="{{ 'deleteVrTour_' . $rowDesigner['attachment_id'] }}">
                                @csrf
                                @method('DELETE')
                            </form>
                            <a title="Edit Cards" onclick="$('#{{ 'deleteVrTour_' . $rowDesigner['attachment_id'] }}').submit()">
                                <i class="fa fa-trash"></i> Delete VR Tour
                            </a>
                        </li>
                    </ul>
                </div>
            </td>
        </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th></th>
            <th width="150px"></th>
            <th>Nama Kost / Apartemen</th>
            <!-- <th class="text-center">Status</th> -->
            <th class="text-center" width="10%">Matterport ID</th>
            <th class="text-center" width="10%">VR Tour Active</th>
            <th class="text-center" width="10%">Created / Updated</th>
            <th class="table-action-column" width="10%"></th>
        </tr>
    </tfoot>
</table>
