@extends('admin.layouts.main')

@section('content')
	<div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default form-inline">
                    <div class="col-md-6 bg-default" style="padding-bottom:10px;">
                        <a href="{{ URL::route('admin.competitor.fake-phone.create', ['#competitor']) }}" class="btn btn-primary">
                            <i class="fa fa-plus">&nbsp;</i>Add Fake Phone
                        </a>
                    </div>
                    <div class="col-md-6 bg-default" style="padding-bottom:10px;text-align:right;">
                        <form action="" method="GET">
                            <input type="text" name="q" class="form-control"  placeholder="Fake Phone"  autocomplete="off" value="{{ request()->input('q') }}">
                            <button type="submit" class="btn btn-primary" id="buttonSearch">
                                <i class="fa fa-search">&nbsp;</i>Search
                            </button>
                        </form>
                    </div>
                </div>
            </div>

            <table  class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Info</th>
                        <th>Comment</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($fakePhone as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->info }}</td>
                        <td>{{ $item->comment }}</td>
                        <td>
                            <a href="{{ URL::route('admin.competitor.fake-phone.edit', [$item->id, '#competitor']) }}" title="Edit" style="margin-right: 10px;">
                                <i class="fa fa-pencil"></i>
                            </a>
                            <form action="{{ URL::route('admin.competitor.fake-phone.destroy', [$item->id]) }}" method="POST" style="display: inline;">
                                <input type="hidden" name="_method" value="DELETE">
                                <a href="#" onclick="if(!confirm('Are you sure want to delete fake phone {{ $item->info }}?')) return false; else $(this).parent().submit();" title="Delete">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </form>
                        </td>
                    </tr> 
                @endforeach
                </tbody>
            </table>
        </div>

        <div class="box-body no-padding">
            <div class="col-md-12 bg-default">
                {{ $fakePhone->appends(Request::except('page'))->links() }}
            </div>
        </div>
    </div>
@endsection
