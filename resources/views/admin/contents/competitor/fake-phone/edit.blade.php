@extends('admin.layouts.main')

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

        @php
            $item = null;
            if (isset($fakePhone)) {
                $route = URL::route('admin.competitor.fake-phone.update', [$fakePhone->id]);
                $item = $fakePhone;
            } else {
                $route = URL::route('admin.competitor.fake-phone.store');
            }
        @endphp
        <form action="{{ $route }}" method="POST" class="form-horizontal form-bordered">
            @if (isset($fakePhone))
                <input type="hidden" name="_method" value="PUT">
            @endif
            
            <div class="box-body no-padding">
                <div class="form-group bg-default">
                    <label for="fake-phone-info" class="control-label col-sm-2">Info</label>
                    <div class="col-sm-10">
                        <input type="text" name="info" id="fake-phone-info" class="form-control" value="{{ old('info', optional($item)->info) }}">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="fake-phone-comment" class="control-label col-sm-2">Comment</label>
                    <div class="col-sm-10">
                        <input type="text" name="comment" id="fake-info-comment" class="form-control" value="{{ old('comment', optional($item)->comment) }}">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <div class="col-sm-10 col-sm-push-2">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
