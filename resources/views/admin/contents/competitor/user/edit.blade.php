@extends('admin.layouts.main')

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

        @php
            $item = null;
            if (isset($compUser)) {
                $route = URL::route('admin.competitor.user.update', [$compUser->id]);
                $item = $compUser;
            } else {
                $route = URL::route('admin.competitor.user.store');
            }
        @endphp
        <form action="{{ $route }}" method="POST" class="form-horizontal form-bordered">
            @if (isset($compUser))
                <input type="hidden" name="_method" value="PUT">
            @endif
            
            <div class="box-body no-padding">
                <div class="form-group bg-default">
                    <label for="competitor-user-id" class="control-label col-sm-2">User ID</label>
                    <div class="col-sm-10">
                        <input type="text" name="user_id" id="competitor-user-id" class="form-control" value="{{ old('user_id', optional($item)->user_id) }}">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="competitor-user-comment" class="control-label col-sm-2">Comment</label>
                    <div class="col-sm-10">
                        <input type="text" name="comment" id="competitor-user-comment" class="form-control" placeholder="User Name (Email)" value="{{ old('comment', optional($item)->comment) }}">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <div class="col-sm-10 col-sm-push-2">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
