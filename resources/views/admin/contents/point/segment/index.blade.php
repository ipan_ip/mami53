@extends('admin.layouts.main')

@section('content')
	<div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default form-inline">
                    <div class="col-md-6 bg-default" style="padding-bottom:10px;">
                        <a href="{{ URL::route('admin.point.segment.create', ['#point']) }}" class="btn btn-primary">
                            <i class="fa fa-plus">&nbsp;</i>Add Segment
                        </a>
                    </div>
                    <div class="col-md-6 bg-default" style="padding-bottom:10px;text-align:right;">
                        <form action="" method="GET">
                            <input type="text" name="type" class="form-control"  placeholder="Type"  autocomplete="off" value="{{ request()->input('type') }}">
                            <select name="target" class="form-control">
                                <option value="">All</option>
                                @foreach ($targetOptions as $key => $value)
                                <option value="{{ $key }}" @if (request('target') == $key) selected="true" @endif>{{ $value }}</option>
                                @endforeach
                            </select>
                            <button type="submit" class="btn btn-primary" id="buttonSearch">
                                <i class="fa fa-search">&nbsp;</i>Search
                            </button>
                        </form>
                    </div>
                </div>
            </div>

            <table  class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Type</th>
                        <th>Title</th>
                        <th>Kost Level</th>
                        <th>Room Level</th>
                        <th>Target</th>
                        <th>Is Published</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($segments as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->type }}</td>
                        <td>{{ $item->title }}</td>
                        @php
                            $level = $item->kost_level;
                            if (empty($level)) {
                                $kostLevelName = 'Unassigned';
                            } else {
                                $kostLevelName = $level->name;
                            }
                        @endphp
                        <td>{{ $kostLevelName }}</td>
                        @php
                            $level = $item->room_level;
                            if (empty($level)) {
                                $roomLevelName = 'Unassigned';
                            } else {
                                $roomLevelName = $level->name;
                            }
                        @endphp
                        <td>{{ $roomLevelName }}</td>
                        <td>{{ $targetOptions[$item->target] }}</td>
                        <td>
                            @if ($item->is_published)
                                <span class="label label-success">Published</span>
                            @else
                                <span class="label label-default">Draft</span>
                            @endif
                        </td>
                        <td>
                            <a href="{{ URL::route('admin.point.segment.edit', [$item->id, '#point']) }}" title="Edit" style="margin-right: 10px;">
                                <i class="fa fa-pencil"></i>
                            </a>
                            <form action="{{ URL::route('admin.point.segment.destroy', [$item->id]) }}" method="POST" style="display: inline;">
                                <input type="hidden" name="_method" value="DELETE">
                                <a href="#" onclick="if(!confirm('Are you sure want to delete segment {{ $item->type }}?')) return false; else $(this).parent().submit();" title="Delete">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </form>
                        </td>
                    </tr> 
                @endforeach
                </tbody>
            </table>
        </div>

        <div class="box-body no-padding">
            <div class="col-md-12 bg-default">
                {{ $segments->appends(Request::except('page'))->links() }}
            </div>
        </div>
    </div>
@endsection
