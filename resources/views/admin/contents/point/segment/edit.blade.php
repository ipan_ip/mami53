@extends('admin.layouts.main')

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

        @php
            $item = null;
            if (isset($segment)) {
                $route = URL::route('admin.point.segment.update', [$segment->id]);
                $item = $segment;
            } else {
                $route = URL::route('admin.point.segment.store');
            }
        @endphp
        <form action="{{ $route }}" method="POST" class="form-horizontal form-bordered">
            @if (isset($segment))
                <input type="hidden" name="_method" value="PUT">
            @endif
            
            <div class="box-body no-padding">
                <div class="form-group bg-default">
                    <label for="segment-type" class="control-label col-sm-2">Type</label>
                    <div class="col-sm-10">
                        <input type="text" name="type" id="segment-type" class="form-control" value="{{ old('type', optional($item)->type) }}">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="segment-title" class="control-label col-sm-2">Title</label>
                    <div class="col-sm-10">
                        <input type="text" name="title" id="segment-title" class="form-control" value="{{ old('title', optional($item)->title) }}">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="segment-kost-level-id" class="control-label col-sm-2">Kost Level</label>
                    <div class="col-sm-10">
                        <select name="kost_level_id" id="segment-kost-level-id" class="form-control">
                            @foreach ($kostLevelList as $levelItem)
                            <option value="{{ $levelItem->id }}" @if (old('kost_level_id', optional($item)->kost_level_id) == $levelItem->id) selected="true" @endif>{{ $levelItem->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="segment-room-level-id" class="control-label col-sm-2">Room Level</label>
                    <div class="col-sm-10">
                        <select name="room_level_id" id="segment-room-level-id" class="form-control">
                            @foreach ($roomLevelList as $levelItem)
                            <option value="{{ $levelItem->id }}" @if (old('room_level_id', optional($item)->room_level_id) == $levelItem->id) selected="true" @endif>{{ $levelItem->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="segment-target" class="control-label col-sm-2">Target</label>
                    <div class="col-sm-10">
                        <select name="target" id="segment-target" class="form-control">
                            @foreach ($targetOptions as $key => $value)
                            <option value="{{ $key }}" @if (old('target', optional($item)->target) == $key) selected="true" @endif>{{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="is-published" class="control-label col-sm-2">Is Published</label>
                    <div class="col-sm-10">
                        {!! Form::select('is_published', [
                            '0' => 'Draft',
                            '1' => 'Published',
                        ], optional($item)->is_published ?: null, ['class' => 'form-control', 'id' => 'is-published']) !!}
                    </div>
                </div>

                <div class="form-group bg-default">
                    <div class="col-sm-10 col-sm-push-2">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
