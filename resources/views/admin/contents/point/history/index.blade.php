@extends('admin.layouts.main')

@section('content')
	<div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default form-inline">
                    <div class="col-md-3 bg-default" style="padding-bottom:10px;">
                        <a href="{{ URL::route('admin.point.user.index', ['#point']) }}" class="btn btn-primary">
                            <i class="fa fa-arrow-left">&nbsp;</i>Back to User Point
                        </a>
                    </div>
                    <div class="col-md-9 bg-default" style="padding-bottom:10px;text-align:right;">
                        <form action="" method="GET">
                            <input type="date" name="start_date" class="form-control" value="{{ request()->input('start_date') }}">
                            <input type="date" name="end_date" class="form-control" value="{{ request()->input('end_date') }}">
                            <select name="activity_id" class="form-control">
                                <option value="999999">All Activity</option>
                                <option value="0" @if (request()->input('activity_id') === '0') selected="true" @endif>Admin Adjustment</option>
                                @foreach ($activityList as $activity)
                                    <option value="{{ $activity->id }}" @if (request()->input('activity_id') == $activity->id) selected="true" @endif>{{ $activity->name }}</option>
                                @endforeach
                            </select>
                            <button type="submit" class="btn btn-primary" id="buttonSearch">
                                <i class="fa fa-search">&nbsp;</i>Search
                            </button>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="jumbotron" style="padding:15px;margin-bottom:15px;">
                    <span><b>{{ $user->name }}</b> as {{ $user->isOwner() ? 'Owner' : 'Tenant' }}</span><br />
                    <span>Email: {{ $user->email ?? '-' }}</span><br />
                    <span>Phone: {{ $user->phone_number ?? '-' }}</span><br />
                </div>
            </div>
            <div class="col-md-8"></div>

            <table  class="table table-striped">
                <thead>
                    <tr>
                        <th>
                            Date
                            @if (request()->input('sort_date') == 1)
                                <a href="{{ route('admin.point.history.index', array_diff(array_merge(request()->input(), ['user' => $user->id]), ['sort_date' => 1])) }}">
                                    <i class="fa fa-arrow-up"></i>
                                </a>
                            @else
                                <a href="{{ route('admin.point.history.index', array_merge(request()->input(), ['user' => $user->id, 'sort_date' => 1])) }}">
                                    <i class="fa fa-arrow-down"></i>
                                </a>
                            @endif
                        </th>
                        <th>Activity</th>
                        <th>Notes</th>
                        <th>Point</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($history as $item)
                    <tr>
                        <td>{{ $item->created_at }}</td>
                        <td>
                            @if ($item->activity)
                                @if ($item->earnable)
                                    {{optional($item->activity)->name .' on '. $item->earnable->invoice_number}}
                                @else
                                    {{optional($item->activity)->name}}
                                @endif
                            @elseif (!$item->activity && $item->redeem)
                                {{'Penukaran "'. $item->redeem->reward->name .'"'}}
                            @elseif (!$item->activity && $item->redeemable)
                                {{'Potongan MamiPoin'}}
                            @else
                                {{ 'Admin Adjustment' }}
                            @endif
                        </td>
                        <td>{{ $item->notes }}</td>
                        <td>{{ $item->value }}</td>
                        <td>{{ $item->balance }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

        <div class="box-body no-padding">
            <div class="col-md-12 bg-default">
                {{ $history->appends(Request::except('page'))->links() }}
            </div>
        </div>
    </div>

    <script type="text/javascript">
        
    </script>
@endsection
