@extends('admin.layouts.main')

@section('style')
    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" />
@stop

@section('script')
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>
    <script>
        $('.select2').select2()

        $('select[name=value]').on('change', function (e) {
            var usertype = $('select[name=value] option:selected')
            if (usertype.val() == 'mamirooms') {
                $('input[name=usertype]').val('mamirooms')
            } else {
                $('input[name=usertype]').val('kost_level')
            }
        })
        $('select[name=value]').trigger('change')
    </script>
@stop

@section('content')
<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">{{ $boxTitle }}</h3>
    </div>

    <div class="box-body" style="min-height: 200px;">
        {!! Form::open(['method' => 'POST', 'url' => route('admin.point.blacklist.store'), 'class' => 'form-horizontal']) !!}
            
            {!! Form::hidden('usertype', null) !!}
            <div class="form-group {{!empty($errors) && $errors->first('value') ? 'has-error' : ''}}">
                <label for="value" class="control-label col-sm-2">User Type</label>
                <div class="col-sm-8">
                    {!! Form::select('value', $usertypeDropdown, null, ['class' => 'form-control select2']) !!}
                </div>
            </div>
            
            <div class="form-group">
                <label class="control-label col-sm-2">Status</label>
                <div class="col-sm-8">
                    <label class="radio-inline">
                        {!! Form::radio('is_blacklisted', 0, true) !!} Whitelisted
                    </label>
                    <label class="radio-inline">
                        {!! Form::radio('is_blacklisted', 1, null) !!} Blacklisted
                    </label>
                </div>
            </div>

            <hr/>
            <div class="form-group">
                <div class="col-sm-2"> </div>
                <div class="col-sm-8">
                    <button type="submit" class="btn btn-primary">Save</button>
                    &nbsp;&nbsp;
                    <a href="{{ route('admin.point.blacklist.index', ['#point']) }}" class="btn btn-default">Back</a>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
</div>
@stop