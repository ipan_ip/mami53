@extends('admin.layouts.main')

@section('content')
<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">{{ $boxTitle }}</h3>
    </div>

    <div class="box-body">
        <div class="horizontal-wrapper" style="margin-bottom: 0">
            <div class="btn-horizontal-group bg-default form-inline">
                <div class="bg-default" style="padding-left: 15px">
                    <a href="{{ URL::route('admin.point.blacklist.create', ['#point']) }}" class="btn btn-primary">
                        <i class="fa fa-plus">&nbsp;</i>Add User Type
                    </a>
                </div>
            </div>
        </div>

        <table  class="table table-striped">
            <thead>
                <tr>
                    <th>No</th>
                    <th>User Type</th>
                    <th>Kost Level ID / Custom Value</th>
                    <th>Blacklist Status</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($blacklists as $no => $item)
                    <tr>
                        <td>{{++$no}}</td>
                        <td>
                            @if ($item->level)
                                @if ($item->level->deleted_at)
                                    <span class="text-danger" data-toggle="tooltip" title="This kost level is deleted">
                                        [Kost Level] {{$item->level->name}}
                                        &nbsp; <i class="fa fa-times"></i>
                                    </span>
                                @else
                                    [Kost Level] {{$item->level->name}}
                                @endif
                            @else
                                {{\App\Entities\Point\PointBlacklistConfig::getUserTypeList($item->usertype)}}
                            @endif
                        </td>
                        <td>{{$item->value}}</td>
                        <td>
                            @if ($item->is_blacklisted)
                                <span class="label label-danger">
                                    Blacklisted
                                </span>
                            @else
                                <span class="label label-success">
                                    Whitelisted
                                </span>
                            @endif

                            &nbsp;
                            @php
                            $msg = $item->is_blacklisted
                                ? 'Apakah anda yakin untuk whitelist?'
                                : 'Apakah anda yakin untuk blacklist?';
                            @endphp
                            <a class="btn btn-default btn-xs" onclick="confirm('{{$msg}}') ? location.href='{{route('admin.point.blacklist.toggle', $item->id)}}' : ''" data-toggle="tooltip" title="Switch Status">
                                <i class="fa fa-toggle-on"></i>
                            </a>
                            &nbsp; &nbsp;
                            <a onclick="confirm('Apakah anda yakin untuk hapus?') ? location.href='{{route('admin.point.blacklist.delete', $item->id)}}' : ''" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%">There is no blacklist yet</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
</div>
@stop