@extends('admin.layouts.main')

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

        <form action="{{ URL::route('admin.point.expiry.change') }}" method="POST" class="form-horizontal form-bordered">
            <div class="box-body no-padding">
                <div class="form-group bg-default">
                    <label for="owner-point-expiry" class="control-label col-sm-2">Owner Point Expiry in</label>
                    <div class="col-sm-3 input-group">
                        <input type="number" min="1" name="owner_expiry_value" id="owner-point-expiry" class="form-control" value="{{ (int) old('owner_expiry_value', $owner_expiry) }}">
                        <div class="input-group-addon">month</div>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="tenant-point-expiry" class="control-label col-sm-2">Tenant Point Expiry in</label>
                    <div class="col-sm-3 input-group">
                        <input type="number" min="1" name="tenant_expiry_value" id="tenant-point-expiry" class="form-control" value="{{ (int) old('tenant_expiry_value', $tenant_expiry) }}">
                        <div class="input-group-addon">month</div>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <div class="col-sm-10 col-sm-push-2">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
