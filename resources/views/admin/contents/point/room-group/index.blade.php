@extends('admin.layouts.main')

@section('content')
	<div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default form-inline">
                    <div class="col-md-12 bg-default" style="padding-bottom:10px;">
                        <a href="{{ URL::route('admin.point.room-group.create', ['#point']) }}" class="btn btn-primary">
                            <i class="fa fa-plus">&nbsp;</i>Add Owner Room Group
                        </a>
                    </div>
                </div>
            </div>

            <table  class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Group</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($roomGroup as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->floor }}-{{ $item->ceil }}</td>
                        <td>
                            <a href="{{ URL::route('admin.point.room-group.edit', [$item->id, '#point']) }}" title="Edit">
                                <i class="fa fa-pencil"></i>
                            </a>
                            <form action="{{ URL::route('admin.point.room-group.destroy', [$item->id]) }}" method="POST" style="display: inline;">
                                <input type="hidden" name="_method" value="DELETE">
                                <a href="#" onclick="if(!confirm('Are you sure want to delete room group {{ $item->floor }}-{{ $item->ceil }}?')) return false; else $(this).parent().submit();" title="Delete">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </form>
                        </td>
                    </tr> 
                @endforeach
                </tbody>
            </table>
        </div>

        <div class="box-body no-padding">
            <div class="col-md-12 bg-default">
                {{ $roomGroup->appends(Request::except('page'))->links() }}
            </div>
        </div>
    </div>
@endsection
