@extends('admin.layouts.main')

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

        @php
            $item = null;
            if (isset($roomGroup)) {
                $route = URL::route('admin.point.room-group.update', [$roomGroup->id]);
                $item = $roomGroup;
            } else {
                $route = URL::route('admin.point.room-group.store');
            }
        @endphp
        <form action="{{ $route }}" method="POST" class="form-horizontal form-bordered">
            @if (isset($roomGroup))
                <input type="hidden" name="_method" value="PUT">
            @endif
            
            <div class="box-body no-padding">
                <div class="form-group bg-default">
                    <label for="room-group-floor" class="control-label col-sm-2">Room Group</label>
                    <div class="col-sm-2">
                        <input type="number" min="0" name="floor" id="room-group-floor" class="form-control" value="{{ old('floor', optional($item)->floor) }}">
                    </div>
                    <div class="col-sm-1">
                        until
                    </div>
                    <div class="col-sm-2">
                        <input type="number" min="0" name="ceil" id="room-group-ceil" class="form-control" value="{{ old('ceil', optional($item)->ceil) }}">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <div class="col-sm-10 col-sm-push-2">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
