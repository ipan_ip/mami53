@extends('admin.layouts.main')

@section('content')
	<div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default form-inline">
                    <div class="col-md-6 bg-default">
                        @if (auth()->user()->can('access-point'))
                            <div style="padding-top: 23px;">
                                <a href="#" class="btn btn-primary"
                                    data-toggle="modal"
                                    data-target="#popup-bulk-adjust-point">
                                    Bulk Adjust Point
                                </a>
                                <a href="#" class="btn btn-primary"
                                    data-toggle="modal"
                                    data-target="#popup-bulk-blacklist">
                                    Bulk Update Blacklist
                                </a>
                            </div>
                        @endif
                    </div>
                    <div class="col-md-6 bg-default">
                        <form action="" method="GET" class="form-inline text-right" style="margin-bottom: 20px;">
                            <div class="form-group">
                                <label class="show text-left">Keyword:</label>
                                <input type="text" name="keyword" value="{{ request('keyword') }}" class="form-control" placeholder="Name / Email / Phone" autocomplete="off" style="width: 200px;">
                            </div>
                            <div class="form-group">
                                <label class="show text-left">User:</label>
                                <select name="user" class="form-control">
                                    <option value="">All</option>
                                    <option value="owner" {{request('user') == 'owner' ? 'selected' : ''}}>Owner</option>
                                    <option value="tenant" {{request('user') == 'tenant' ? 'selected' : ''}}>Tenant</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="show text-left">Status:</label>
                                <select name="status" class="form-control">
                                    <option value="">All</option>
                                    <option value="blacklist" {{request('status') == 'blacklist' ? 'selected' : ''}}>Blacklist</option>
                                    <option value="whitelist" {{request('status') == 'whitelist' ? 'selected' : ''}}>Whitelist</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="show text-left">&nbsp;</label>
                                <button type="submit" class="btn btn-primary" id="buttonSearch">
                                    <i class="fa fa-search">&nbsp;</i>Search
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <table  class="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone Number</th>
                        <th>User</th>
                        <th>
                            <a href="{{route('admin.point.user.index', array_merge(
                                request()->except('sortBy', 'sortOrder'),
                                [
                                    'sortBy' => 'total_point',
                                    'sortOrder' => request('sortOrder') == 'asc' ? 'desc' : 'asc',
                                ]
                            ))}}">
                                Total Point
                                @if (request('sortBy') == 'total_point')
                                    @if (request('sortOrder') == 'asc')
                                        <i class="fa fa-angle-up"></i>
                                    @else
                                        <i class="fa fa-angle-down"></i>
                                    @endif
                                @endif
                            </a>
                        </th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($userPoints as $item)
                        <tr>
                            <td>{{$item->user->name}}</td>
                            <td>{{$item->user->email}}</td>
                            <td>{{$item->user->phone_number}}</td>
                            <td>
                                {{ $item->user->isOwner() ? 'Owner' : 'Tenant' }}
                            </td>
                            <td>{{$item->total}}</td>
                            <td>
                                @if (auth()->user()->can('access-point'))
                                    @if ($item->is_blacklisted)
                                        <a href="#" class="text-danger"
                                            data-toggle="modal"
                                            data-target="#popup-blacklist-user"
                                            data-form-action="{{route('admin.point.user.updateBlacklist', $item->id)}}"
                                            data-user-name="{{$item->user->name}}"
                                            data-action-type="whitelist"
                                            style="text-decoration: underline;">
                                            Blacklist
                                        </a>
                                    @else
                                        <a href="#" class="text-success"
                                            data-toggle="modal"
                                            data-target="#popup-blacklist-user"
                                            data-form-action="{{route('admin.point.user.updateBlacklist', $item->id)}}"
                                            data-user-name="{{$item->user->name}}"
                                            data-action-type="blacklist"
                                            style="text-decoration: underline;">
                                            Whitelist
                                        </a>
                                    @endif
                                @else
                                    {{ $item->is_blacklisted ? 'Blacklist' : 'Whitelist' }}
                                @endif
                            </td>
                            <td>
                                @if (auth()->user()->can('access-point'))
                                    <a href="#"
                                        data-toggle="modal"
                                        data-target="#popup-adjust-point"
                                        data-user-name="{{$item->user->name}}"
                                        data-user-email="{{$item->user->email}}"
                                        data-user-phone="{{$item->user->phone_number}}"
                                        data-user-type="{{ $item->user->isOwner() ? 'Owner' : 'Tenant' }}"
                                        data-form-action="{{route('admin.point.user.updateAdjustPoint', $item->id)}}"
                                        title="Adjust Point" style="margin-right: 10px;">
                                        <i class="fa fa-adjust"></i>
                                    </a>
                                @endif
                                <a href="{{ URL::route('admin.point.history.index', [$item->user_id, '#point']) }}" title="History">
                                    <i class="fa fa-history"></i>
                                </a>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="100%">
                                <p class="text-muted">There is no data yet.</p>
                            </td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>

        <div class="box-body no-padding">
            <div class="col-md-12 bg-default">
                {{ $userPoints->appends(Request::except('page'))->links() }}
            </div>
        </div>
    </div>


    {{-- === Modal Block === --}}

    {{-- Popup Adjust Point --}}
    <div class="modal fade" id="popup-adjust-point">
        <div class="modal-dialog modal-sm">
            <form method="POST" action="#" class="modal-content">
                @method('PUT')

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Adjust Point</h4>
                </div>
                <div class="modal-body">
                    <div style="margin-bottom: 20px;">
                        <p style="margin-bottom: 0px;"><b id="user-name"></b> as <span id="user-type"></span></p>
                        <p id="user-email" style="margin-bottom: 0px;"></p>
                        <p id="user-phone" style="margin-bottom: 0px;"></p>
                    </div>

                    <div>
                        <div class="form-group">
                            <label>Adjustment*</label>
                            <div>
                                <label class="radio-inline">
                                    <input type="radio" name="adjustment" value="topup" class="simple" checked> Topup
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="adjustment" value="topdown" class="simple"> Topdown
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Point Amount*</label>
                            <input type="number" name="amount" class="form-control" required>
                        </div>
                        
                        <div class="form-group">
                            <label>Note*</label>
                            <textarea name="note" rows="3" class="form-control" maxlength="20" required></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>

    {{-- Popup Bulk Adjust Point --}}
    <div class="modal fade" id="popup-bulk-adjust-point">
        <div class="modal-dialog modal-sm">
            <form method="POST" action="{{route('admin.point.user.updateBulkAdjustPoint')}}" class="modal-content" enctype="multipart/form-data">
                @method('PUT')

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Bulk Adjust Point</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <span class="help-block">csv field: user ID, point (use "-" for topdown)</span>
                        <input type="file" name="csv_bulk_adjust_point" class="form-control" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>

    {{-- Popup Blacklist Confirmation --}}
    <div class="modal fade" id="popup-blacklist-user">
        <div class="modal-dialog" role="document">
            <form method="POST" class="modal-content">
                @method('PUT')
                <input type="hidden" name="action_type" value="">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Do you want to <span class="blacklist-type">Action</span> <span class="user-name">This User</span>?</h4>
                </div>
                <div class="modal-body">
                    <p>Please confirm that you want to <b><span class="blacklist-type">Action</span> <span class="user-name">This User</span></b> from Loyalty Point. It means <span class="user-name">This User</span> will <b class="notAble">NOT</b> be able to access and get point <span id="anymoreOrAgain"></span>.</p>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-default">Yes, Do It!</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">No, Go Back</button>
                </div>
            </form>
        </div>
    </div>

    {{-- Popup Bulk Blacklist --}}
    <div class="modal fade" id="popup-bulk-blacklist">
        <div class="modal-dialog modal-sm">
            <form method="POST" action="{{route('admin.point.user.updateBulkBlacklist')}}" class="modal-content" enctype="multipart/form-data">
                @method('PUT')

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Bulk Update Blacklist User</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <span class="help-block">csv field: user ID, blacklist (0 = no, 1 = yes)</span>
                        <input type="file" name="csv_bulk_blacklist" class="form-control" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('script')
    <script>
        // Disable submit button after submit the form
        $('form').each(function () {
            $(this).on('submit', function(e) {
                e.preventDefault()
                var $btnSubmit = $(this).find('button[type=submit]')
                $btnSubmit.button('loading')
                this.submit()
            })
        })

        $('.modal').on('hidden.bs.modal', function (e) {
            $(this).find('button[type=submit]').button('reset')
        })
        // end

        var inputAmount = $('#popup-adjust-point input[name="amount"]')
        var radioAdjustment = $('#popup-adjust-point input[name="adjustment"]')

        radioAdjustment.on('change', function () {
            inputAmount.trigger('keyup')
        })

        inputAmount.on('keyup', function () {
            var amounttVal = $(this).val()
            var adjustmentVal = $('#popup-adjust-point input[name="adjustment"]:checked').val();

            if (adjustmentVal == 'topup') {
                return $(this).val(amounttVal.replace('-', ''))
            }

            if (adjustmentVal == 'topdown') {
                if (!amounttVal.includes('-')) {
                    return $(this).val('-'+ amounttVal)
                }
            }
        })

        $('#popup-adjust-point').on('show.bs.modal', function (e) {
            var button = $(e.relatedTarget)
            var modal = $(this)
            var formAction = button.data('formAction')
            
            modal.find('form').attr('action', formAction)
            modal.find('#user-name').html(button.data('userName'))
            modal.find('#user-email').html(button.data('userEmail'))
            modal.find('#user-phone').html(button.data('userPhone'))
            modal.find('#user-type').html(button.data('userType'))

            radioAdjustment.trigger('change')
        })

        $('#popup-blacklist-user').on('show.bs.modal', function (e) {
            var modal = $(this)
            var button = $(e.relatedTarget)

            modal.find('form').attr('action', button.data('formAction'))
            modal.find('.blacklist-type').html(button.data('actionType'))
            modal.find('.user-name').html(button.data('userName'))
            modal.find('input[name="action_type"]').val(button.data('actionType'))

            if (button.data('actionType') == 'whitelist') {
                modal.find('.notAble').css('display', 'none')
                modal.find('#anymoreOrAgain').text('again')
            } else {
                modal.find('.notAble').css('display', 'inline-block')
                modal.find('#anymoreOrAgain').text('anymore')
            }
        })
    </script>
@endsection