@extends('admin.layouts.main')

@section('content')
	<div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

        <div class="horizontal-wrapper"></div>

        <div class="box-body">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                @foreach ($pointSegments as $segmentItem)
                    <div class="panel panel-default" style="margin-bottom: 20px">
                        <div class="panel-heading" role="tab" id="heading-{{ $segmentItem->type }}">
                            <h4 class="panel-title">
                                @php
                                    $segmentTitle = $segmentItem->title;
                                    if ($segmentItem->type === App\Entities\Point\Point::DEFAULT_TYPE || $segmentItem->type === App\Entities\Point\Point::DEFAULT_TYPE_TENANT) {
                                        $segmentTitle = 'Booking Langsung';
                                    } elseif ($segmentItem->type === App\Entities\Point\Point::MAMIROOMS_TYPE_TENANT) {
                                        $segmentTitle = 'Mamirooms';
                                    } elseif (empty($segmentItem->title)) {
                                        $segmentTitle = strtoupper($segmentItem->type);
                                    }
                                @endphp
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-{{ $segmentItem->type }}" aria-expanded="true" aria-controls="collapse-{{ $segmentItem->type }}">
                                    {{ $segmentTitle }}
                                </a>
                            </h4>
                        </div>
                        <div id="collapse-{{ $segmentItem->type }}" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading-{{ $segmentItem->type }}">
                            <div class="panel-body" style="background: #fff">
                                <div class="panel-group" id="accordion-{{ $segmentItem->type }}" role="tablist" aria-multiselectable="true">
                                    @foreach ($activity as $activityItem)
                                    @php
                                    $colorClass = 'primary';
                                    if ($activityItem->target === 'owner') {
                                        $colorClass = 'success';
                                    }
                                    @endphp
                                    <div class="panel panel-default" style="margin-bottom: 20px">
                                        <div class="panel-heading" role="tab" id="heading-{{ $segmentItem->type .'-'. $activityItem->key }}">
                                            <h4 class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion-{{ $segmentItem->type }}" href="#collapse-{{ $segmentItem->type .'-'. $activityItem->key }}" aria-expanded="true" aria-controls="collapse-{{ $segmentItem->type .'-'. $activityItem->key }}">
                                                    {{ $activityItem->name }}
                                                </a>
                                                &nbsp;<span class="label label-{{ $colorClass }}">{{ strtoupper($activityItem->target) }}</span>
                                            </h4>
                                        </div>
                                        <div id="collapse-{{ $segmentItem->type .'-'. $activityItem->key }}" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading-{{ $segmentItem->type .'-'. $activityItem->key }}">
                                            <div class="panel-body" style="background: #fff">
                                                <form action="{{ URL::route('admin.point.setting.post', ['point' => $segmentItem->type, 'activity' => $activityItem->key, 'target' => $pointTarget]) }}" method="POST">
                                                    <div class="form-group bg-default">
                                                        <table class="table">
                                                            <thead>
                                                                <tr>
                                                                    @if ($activityItem->target === 'owner')
                                                                    <th>Room Group</th>
                                                                    @endif
                                                                    <th>How Many Points are Earned?</th>
                                                                    <th>Point Limit</th>
                                                                    <th>Status</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                            @if ($activityItem->target === 'owner')
                                                            @foreach ($roomGroup as $roomGroupItem)
                                                                <tr>
                                                                    <td>
                                                                        {{ $roomGroupItem->floor }}-{{ $roomGroupItem->ceil }}
                                                                    </td>
                                                                    <td>
                                                                        @php $value = isset($setting[$segmentItem->id][$activityItem->key][$roomGroupItem->id][0]['received_each']) ? $setting[$segmentItem->id][$activityItem->key][$roomGroupItem->id][0]['received_each'] : null; @endphp
                                                                        <input type="number" min="0" name="{{$segmentItem->id}}[{{$activityItem->key}}][{{$roomGroupItem->id}}][received_each]" value="{{ old($segmentItem->id.'.'.$activityItem->key.'.'.$roomGroupItem->id.'.received_each', $value) }}">
                                                                    </td>
                                                                    <td>
                                                                        <select name="{{$segmentItem->id}}[{{$activityItem->key}}][{{$roomGroupItem->id}}][limit_type]">
                                                                            @foreach ($limitOptions as $limitKey => $limitValue)
                                                                            @php $value = isset($setting[$segmentItem->id][$activityItem->key][$roomGroupItem->id][0]['limit_type']) ? $setting[$segmentItem->id][$activityItem->key][$roomGroupItem->id][0]['limit_type'] : null; @endphp
                                                                            <option value="{{ $limitKey }}" @if (old($segmentItem->id.'.'.$activityItem->key.'.'.$roomGroupItem->id.'.limit_type', $value) == $limitKey) selected="true" @endif>{{ $limitValue }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                        @php $value = isset($setting[$segmentItem->id][$activityItem->key][$roomGroupItem->id][0]['limit']) ? ($setting[$segmentItem->id][$activityItem->key][$roomGroupItem->id][0]['limit'] >= 0 ? $setting[$segmentItem->id][$activityItem->key][$roomGroupItem->id][0]['limit'] : 0) : null; @endphp
                                                                        <input type="number" min="0" name="{{$segmentItem->id}}[{{$activityItem->key}}][{{$roomGroupItem->id}}][limit]" value="{{ old($segmentItem->id.'.'.$activityItem->key.'.'.$roomGroupItem->id.'.limit', $value) }}">
                                                                        @php $checked = (isset($setting[$segmentItem->id][$activityItem->key][$roomGroupItem->id][0]['limit']) && $setting[$segmentItem->id][$activityItem->key][$roomGroupItem->id][0]['limit'] < 0); @endphp
                                                                        <input type="checkbox" name="{{$segmentItem->id}}[{{$activityItem->key}}][{{$roomGroupItem->id}}][limit_room]" value="1" @if (!empty(old($segmentItem->id.'.'.$activityItem->key.'.'.$roomGroupItem->id.'.limit_room')) || $checked) checked @endif onchange="toggleDisableLimit(this, false, 0);"/> Total Room
                                                                    </td>
                                                                    <td>
                                                                        @php $isActive = (!isset($setting[$segmentItem->id][$activityItem->key][$roomGroupItem->id][0]['is_active']) || $setting[$segmentItem->id][$activityItem->key][$roomGroupItem->id][0]['is_active']); @endphp
                                                                        <input type="checkbox" name="{{$segmentItem->id}}[{{$activityItem->key}}][{{$roomGroupItem->id}}][is_active]" value="1" @if (!empty(old($segmentItem->id.'.'.$activityItem->key.'.'.$roomGroupItem->id.'.is_active')) || $isActive) checked @endif onchange="toggleDisableForm(this);"/> Active
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                            @else
                                                                <tr>
                                                                    <td>
                                                                        @php $value = isset($setting[$segmentItem->id][$activityItem->key][0][0]['received_each']) ? $setting[$segmentItem->id][$activityItem->key][0][0]['received_each'] : null; @endphp
                                                                        <input step="any" type="number" min="0" name="{{$segmentItem->id}}[{{$activityItem->key}}][0][received_each]" value="{{ (float) old($segmentItem->id.'.'.$activityItem->key.'.0.received_each', $value) }}">
                                                                        @if (in_array($activityItem->key, App\Entities\Point\PointActivity::getTenantActivitiesWithPercentagePointConvertion()))
                                                                            &nbsp;<span>&percnt;</span>
                                                                        @endif
                                                                    </td>
                                                                    <td>
                                                                        <select name="{{$segmentItem->id}}[{{$activityItem->key}}][0][limit_type]">
                                                                            @foreach ($limitOptions as $limitKey => $limitValue)
                                                                            @php $value = isset($setting[$segmentItem->id][$activityItem->key][0][0]['limit_type']) ? $setting[$segmentItem->id][$activityItem->key][0][0]['limit_type'] : null; @endphp
                                                                            <option value="{{ $limitKey }}" @if (old($segmentItem->id.'.'.$activityItem->key.'.0.limit_type', $value) == $limitKey) selected="true" @endif>{{ $limitValue }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                        @php $value = isset($setting[$segmentItem->id][$activityItem->key][0][0]['limit']) ? $setting[$segmentItem->id][$activityItem->key][0][0]['limit'] : null; @endphp
                                                                        <input type="number" min="0" name="{{$segmentItem->id}}[{{$activityItem->key}}][0][limit]" value="{{ old($segmentItem->id.'.'.$activityItem->key.'.0.limit', $value) }}">
                                                                    </td>
                                                                    <td>
                                                                        @php $isActive = (!isset($setting[$segmentItem->id][$activityItem->key][0][0]['is_active']) || $setting[$segmentItem->id][$activityItem->key][0][0]['is_active']); @endphp
                                                                        <input type="checkbox" name="{{$segmentItem->id}}[{{$activityItem->key}}][0][is_active]" value="1" @if (!empty(old($segmentItem->id.'.'.$activityItem->key.'.0.is_active')) || $isActive) checked @endif onchange="toggleDisableForm(this);"/> Active
                                                                    </td>
                                                                </tr>
                                                            @endif
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="form-group bg-default">
                                                        <button type="submit" onclick="if (!confirm('You have change loyalty point settings. Please confirm that you still want to update it.')) return false; else $(this).parent().submit();" class="btn btn-primary">Save</button>
                                                        <button type="button" onclick="if (!confirm('Please confirm that you want to cancel it.')) return false; else location.reload();" class="btn btn-default">Cancel</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="heading-tnc">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-tnc" aria-expanded="true" aria-controls="collapse-tnc">
                                Terms and Conditions
                            </a>
                        </h4>
                    </div>
                    <div id="collapse-tnc" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading-tnc">
                        <div class="panel-body" style="background: #fff">
                            <form action="{{ URL::route('admin.point.setting.tnc', ['target' => $pointTarget]) }}" method="POST">
                                <div class="form-group bg-default">
                                    <label for="point-tnc">Terms & Conditions</label>
                                    <textarea name="tnc" id="point-tnc" class="form-control">@if (!empty(old('tnc'))) {{ old('tnc') }} @else {{ $tnc }} @endif</textarea>
                                </div>
                                <div class="form-group bg-default">
                                    <button type="submit" onclick="if (!confirm('You have change loyalty point settings. Please confirm that you still want to update it.')) return false; else $(this).parent().submit();" class="btn btn-primary">Save</button>
                                    <button type="button" onclick="if (!confirm('Please confirm that you want to cancel it.')) return false; else location.reload();" class="btn btn-default">Cancel</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <link rel="stylesheet" href="/assets/vendor/summernote/summernote.css?v=05042018">
    <script src="/assets/vendor/summernote/summernote.min.js"></script>
    <script type="text/javascript">
        $('#point-tnc').summernote({
            height: 200,
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', []],
                ['color', ['color']],
                ['para', ['ul', 'ol']],
                ['insert', ['link']],
                ['misc', ['codeview']]
            ]
        });

        $('select[name*=limit_type').each(function (index) {
            $(this).on('change', function (event) {
                toggleDisableLimit(this, false, 1);
            });
        });

        $('input[name*=limit_room').each(function (index) {
            $(this).on('ifChanged', function (event) {
                $(this).trigger('change');
            });
        });

        $('input[name*=is_active').each(function (index) {
            $(this).on('ifChanged', function (event) {
                $(this).trigger('change');
            });
        });

        $(document).on('ready', function () {
            $('input[name*=is_active').each(function (index) {
                $(this).trigger('change');
                if ($(this).prop('checked')) {
                    var limitRoomCheck = $(this).parent().parent().parent().find('input[name*=limit_room]');
                    var limitTypeSelect = $(this).parent().parent().parent().find('select[name*=limit_type]')
                    if (limitRoomCheck.prop('checked')) {
                        limitRoomCheck.trigger('change');
                    }
                    if (limitTypeSelect.val() === 'once') {
                        limitTypeSelect.trigger('change');
                    }
                }
            });
        });

        function toggleDisableLimit(el, keepValue, newValue)
        {
            if (newValue === 1) {
                var inputLimit = $(el).parent().find('input').first();
                var elChecked = $(el).val() === 'once';
                var limitCheckbox = $(el).parent().find('input[type=checkbox]').first();
            } else {
                var inputLimit = $(el).parent().parent().find('input').first();
                var elChecked = $(el).prop('checked');
            }
            if (elChecked) {
                if (newValue === 1) {
                    makeCheckboxDisable(limitCheckbox);
                }
                makeInputDisable(inputLimit, keepValue, newValue);
            } else {
                if (newValue === 1) {
                    makeCheckboxEnable(limitCheckbox);
                }
                makeInputEnable(inputLimit);
            }
        }

        function toggleDisableForm(el)
        {
            var limitCheckboxes = $(el).parent().parent().parent().find('input[type=checkbox]').not(':last');
            var limitSelect = $(el).parent().parent().parent().find('select');
            var limitInputs = $(el).parent().parent().parent().find('input[type=number]');
            var elChecked = $(el).prop('checked');
            if (elChecked) {
                limitCheckboxes.each(function (index) {
                    makeCheckboxEnable($(this));
                });
                limitSelect.each(function (index) {
                    makeSelectEnable($(this));
                });
                limitInputs.each(function (index) {
                    makeInputEnable($(this));
                });
            } else {
                limitCheckboxes.each(function (index) {
                    makeCheckboxDisable($(this));
                });
                limitSelect.each(function (index) {
                    makeSelectDisable($(this));
                });
                limitInputs.each(function (index) {
                    makeInputDisable($(this), true, 0);
                });
            }
        }

        function makeInputDisable(input, keepValue, newValue)
        {
            if (!keepValue) {
                input.val(newValue);
            }
            input.attr('readonly', 'readonly');
            input.css('background-color', '#bbbbbb');
            input.css('pointer-events', 'none');
        }

        function makeInputEnable(input)
        {
            input.removeAttr('readonly');
            input.css('background-color', '');
            input.css('pointer-events', '');
        }

        function makeSelectDisable(select)
        {
            select.css('pointer-events', 'none');
            select.css('background-color', '#bbbbbb');
        }

        function makeSelectEnable(select)
        {
            select.css('pointer-events', '');
            select.css('background-color', '');
        }

        function makeCheckboxDisable(checkbox)
        {
            checkbox.iCheck('uncheck');
            checkbox.iCheck('disable');
        }

        function makeCheckboxEnable(checkbox)
        {
            checkbox.iCheck('enable');
        }
    </script>
@endsection
