@extends('admin.layouts.main')

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

        @php
            $item = null;
            if (isset($activity)) {
                $route = URL::route('admin.point.activity.update', [$activity->id]);
                $item = $activity;
            } else {
                $route = URL::route('admin.point.activity.store');
            }
        @endphp
        <form action="{{ $route }}" method="POST" class="form-horizontal form-bordered">
            @if (isset($activity))
                <input type="hidden" name="_method" value="PUT">
            @endif
            
            <div class="box-body no-padding">
                <div class="form-group bg-default">
                    <label for="activity-key" class="control-label col-sm-2">Key</label>
                    <div class="col-sm-10">
                        <input type="text" name="key" id="activity-key" class="form-control" value="{{ old('key', optional($item)->key) }}">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="activity-name" class="control-label col-sm-2">Name</label>
                    <div class="col-sm-10">
                        <input type="text" name="name" id="activity-name" class="form-control" value="{{ old('name', optional($item)->name) }}">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="activity-target" class="control-label col-sm-2">Target</label>
                    <div class="col-sm-2">
                        <select name="target" id="activity-target" class="form-control">
                            @foreach ($target as $targetItem)
                            <option value="{{ $targetItem }}" @if (old('order', optional($item)->target) == $targetItem) selected="true" @endif>{{ $targetItem }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="activity-title" class="control-label col-sm-2">Title</label>
                    <div class="col-sm-10">
                        <input type="text" name="title" id="activity-title" class="form-control" value="{{ old('title', optional($item)->title) }}">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="activity-description" class="control-label col-sm-2">Description</label>
                    <div class="col-sm-10">
                        <textarea name="description" id="activity-description" class="form-control" rows="4">{{ old('description', optional($item)->description) }}</textarea>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="activity-sequence" class="control-label col-sm-2">Sequence/Order</label>
                    <div class="col-sm-10">
                        <input type="number" name="sequence" id="activity-sequence" class="form-control" min="0" max="100" value="{{ old('sequence', optional($item)->sequence) }}">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="activity-status" class="control-label col-sm-2">Status</label>
                    <div class="col-sm-2">
                        @php
                            $disabled = ($ownerReceivedPaymentGeneralAvailable && in_array(optional($item)->key, $nonGeneralOwnerReceivedPaymentActivities));
                        @endphp
                        <select name="is_active" id="activity-status" class="form-control" @if ($disabled) disabled @endif>
                            <option value="1" @if (old('is_active', optional($item)->is_active) == '1') selected="true" @endif>Active</option>
                            <option value="0" @if (old('is_active', optional($item)->is_active) == '0') selected="true" @endif>Inactive</option>
                        </select>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <div class="col-sm-10 col-sm-push-2">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('style')
    <link rel="stylesheet" href="/assets/vendor/summernote/summernote.css?v=05042018">
@endsection

@section('script')
    <script src="/assets/vendor/summernote/summernote.min.js"></script>
    <script>
        $('#activity-description').summernote({
            height: 200,
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', []],
                ['color', ['color']],
                ['para', ['ul', 'ol']],
                ['insert', ['link']],
                ['misc', ['codeview']]
            ]
        });
    </script>
@endsection