@extends('admin.layouts.main')

@section('content')
	<div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default form-inline">
                    <div class="col-md-6 bg-default" style="padding-bottom:10px;">
                        <a href="{{ URL::route('admin.point.activity.create', ['#point']) }}" class="btn btn-primary">
                            <i class="fa fa-plus">&nbsp;</i>Add Activty
                        </a>
                    </div>
                    <div class="col-md-6 bg-default" style="padding-bottom:10px;text-align:right;">
                        <form action="" method="GET">
                            <input type="text" name="q" class="form-control"  placeholder="Activity Key"  autocomplete="off" value="{{ request()->input('q') }}">
                            <select name="t" class="form-control">
                                <option value="">All</option>
                                <option value="owner" {{request('t') == 'owner' ? 'selected' : ''}}>Owner</option>
                                <option value="tenant" {{request('t') == 'tenant' ? 'selected' : ''}}>Tenant</option>
                            </select>
                            <select name="s" class="form-control">
                                <option value="">All</option>
                                <option value="1" {{request('s') == '1' ? 'selected' : ''}}>Active</option>
                                <option value="0" {{request('s') == '0' ? 'selected' : ''}}>Inactive</option>
                            </select>
                            <button type="submit" class="btn btn-primary" id="buttonSearch">
                                <i class="fa fa-search">&nbsp;</i>Search
                            </button>
                        </form>
                    </div>
                </div>
            </div>

            <table  class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Key</th>
                        <th>Name</th>
                        <th>Title</th>
                        <th>Target</th>
                        <th>Status</th>
                        <th>Order</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($activity as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->key }}</td>
                        <td>{{ $item->name }}</td>
                        <td>{{ $item->title ?: '-' }}</td>
                        <td>
                            @if ($item->target === 'owner')
                                &nbsp;<span class="label label-success">OWNER</span>
                            @elseif ($item->target === 'tenant')
                                &nbsp;<span class="label label-primary">TENANT</span>
                            @else
                                -
                            @endif
                        </td>
                        <td>
                            @if ($item->is_active)
                                Active
                            @else
                                Inactive
                            @endif
                        </td>
                        <td><span class="badge badge-default">{{ $item->sequence ?: 0 }}</span></td>
                        <td>
                            <a href="{{ URL::route('admin.point.activity.edit', [$item->id, '#point']) }}" title="Edit" style="margin-right: 10px;">
                                <i class="fa fa-pencil"></i>
                            </a>
                            <form action="{{ URL::route('admin.point.activity.destroy', [$item->id]) }}" method="POST" style="display: inline;">
                                <input type="hidden" name="_method" value="DELETE">
                                <a href="#" onclick="if(!confirm('Are you sure want to delete activity {{ $item->name }}?')) return false; else $(this).parent().submit();" title="Delete">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </form>
                        </td>
                    </tr> 
                @endforeach
                </tbody>
            </table>
        </div>

        <div class="box-body no-padding">
            <div class="col-md-12 bg-default">
                {{ $activity->appends(Request::except('page'))->links() }}
            </div>
        </div>
    </div>
@endsection
