<table class="table">
	<thead>
        <tr>
            <th>Message ID</th>
            <th>Destination</th>
            <th>Status Group</th>
            <th>Status</th>
			<th>Description</th>
			<th>Created</th>
            <th>Updated</th>
            <th></th>
        </tr>
    </thead>
	<tbody>
	@if (!count($rowsData))
		<tr>
			<td class="text-center" colspan="14">
				<div class="callout callout-danger">No report data to show!</div>
			</td>
		</tr>
	@else
		@foreach($rowsData as $index => $data)
			<tr class="{{ !$data->is_active ? 'font-grey' : '' }}">
				<td>
					<strong>{{ $data->message_id }}</strong>
					@if (!$data->refreshable)
					<br><span class="label label-grey">Expired / Not Available</span>
					@endif
				</td>
				<td>
                    <strong>{{ $data->to }}</strong>
				</td>
				<td>
					@if ($data->status_group_id === 3)
					<span class="label label-success"><i class="fa fa-check"></i>&nbsp;
					@elseif ($data->status_group_id === 1)
					<span class="label label-warning"><i class="fa fa-clock-o"></i>&nbsp;
					@else
					<span class="label label-danger"><i class="fa fa-exclamation-triangle"></i>&nbsp;
					@endif
					{{ ucfirst($data->status_group_name) }}
					</span>
				</td>
				<td>
					{{-- @if ($data->status_group_id === 3)
					<span class="label label-success">
					@elseif ($data->status_group_id === 1)
					<span class="label label-warning">
					@else
					<span class="label label-danger">
					@endif --}}
					{{ ucfirst($data->status_name) }}
					{{-- </span> --}}
				</td>
				<td>
					<strong>{{ ucfirst($data->status_description) }}</strong>
				</td>
				<td>
					{{ date('d M Y', strtotime($data->created_at)) }}
					<br/>
					<small>@ {{ date('H:i', strtotime($data->created_at)) }}</small>
				</td>
				<td>
					{{ date('d M Y', strtotime($data->updated_at)) }}<br/>
					<small>@ {{ date('H:i', strtotime($data->updated_at)) }}</small>
				</td>
				<td class="table-action-column" style="padding-right:15px!important;">
					<div class="btn-group">
						<button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
							<i class="glyphicon glyphicon-cog"></i> Actions
							<span class="caret"></span>
						</button>
						<ul class="dropdown-menu pull-right">
							<li>
								<a href="#" class="actions" data-action="display" data-content="{{ $data->content }}"><i
											class="fa fa-commenting-o"></i>Display Message Content</a>
							</li>
							@if ($data->refreshable)
								<li class="divider"></li>
								<li>
									<a href="#" class="actions" data-action="refresh" data-report="{{ $data }}"><i
												class="fa fa-refresh"></i> Refresh Delivery Status</a>
								</li>
							@endif
						</ul>
					</div>
				</td>
			</tr>
		@endforeach
	@endif
	</tbody>

</table>