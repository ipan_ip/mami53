<table class="table">
	<thead>
	<tr>
		<th class="text-center">ID</th>
		<th style="width:35%;">CASE / MESSAGE TEMPLATES</th>
		<th class="text-center">Target User</th>
		<th class="text-center">Dispatch Type</th>
		<th class="text-center">Event / Condition</th>
		<th class="text-center">Schedule</th>
		<th class="text-center">Creator</th>
		<th class="text-center">Created At</th>
		<th></th>
	</tr>
	</thead>
	<tbody>
	@if (!count($rowsData))
		<tr>
			<td class="text-center" colspan="14">
				<div class="callout callout-danger">No data to show!<br>Click <strong>[Add
						Template]</strong> to add new template data
				</div>
			</td>
		</tr>
	@else
		@foreach($rowsData as $index => $data)
			<tr class="{{ !$data->is_active ? 'font-grey' : '' }}">
				<td class="text-center">
					{{ $data->id }}
				</td>
				<td>
					@if (!$data->is_active)
						<span class="label label-danger" style="margin-right:10px;"><i class="fa fa-ban"></i> Inactive</span>
					@endif
					<strong class="font-semi-large">{{ $data->name }}</strong>
					<span class="pull-right">
						<span style="font-size: 1.2em;" class="label {{ $data->whatsapp_label }}">
							<i class="fa fa-whatsapp"></i>
						</span>
						<span style="font-size: 1.2em; margin-left: 4px" class="label {{ $data->sms_label }}">
							@if (!is_null($data->sms_content))
							<i class="fa fa-pencil"></i>
							@endif
							<i class="fa fa-commenting"></i>
						</span>
					</span>
					<br/>
					<div class="panel panel-default panel-collapse panel-body" style="margin-bottom: 0px!important;">
						{!! $data->content !!}
					</div>
				</td>
				<td class="text-center">
					{{ ucfirst($data->target_user) }}
				</td>
				<td class="text-center">
					{{ ucfirst($data->type) }}
				</td>
				<td class="text-center">
					{{ $data->on_event }}
				</td>
				<td class="text-center">
					<strong>{{ ucfirst($data->schedule_period) }}</strong>
					@if ($data->type == 'scheduled')
						@if ($data->schedule_period == 'custom')
							<br><span class="label {{ !$data->is_active ? 'label-grey' : 'label-warning' }}">{{ $data->schedule_date }} @ {{ $data->schedule_time }}</span>
						@else
							<br><span
									class="label {{ !$data->is_active ? 'label-grey' : 'label-warning' }}">@ {{ $data->schedule_time }}</span>
						@endif
					@endif
				</td>
				<td class="text-center">
					{{ $data->creator->name }}
				</td>
				<td class="text-center">
					{{ date('d M Y', strtotime($data->created_at)) }}
				</td>
				<td class="table-action-column" style="padding-right:15px!important;">
					<div class="btn-group">
						<button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
							<i class="glyphicon glyphicon-cog"></i> Actions
							<span class="caret"></span>
						</button>
						<ul class="dropdown-menu pull-right">
							<li>
								<a href="#" class="actions" data-action="validate" data-data="{{ $data }}"><i
											class="fa fa-check-square-o"></i> Validate</a>
							</li>
							@if ($data->is_active)
								<li>
									<a href="#" class="actions" data-action="test" data-data="{{ $data }}"><i
												class="fa fa-paper-plane-o"></i> Send Test</a>
								</li>
							@endif
							<li class="divider"></li>
							<li>
								@if (!$data->sms_active)
									@if ($data->is_active)
										<a href="#" class="actions" data-action="enable" data-data="{{ $data }}"><i
												class="fa fa-comment-o"></i>Enable SMS</a>
									@endif
								@else
									<a href="#" class="actions" data-action="disable" data-data="{{ $data }}"><i
												class="fa fa-comment"></i>Disable SMS</a>
								@endif
								<li>
									<a href="#" class="actions" data-action="sms" data-data="{{ $data }}"><i
												class="fa fa-commenting"></i> Edit SMS Template</a>
								</li>
							</li>
							<li class="divider"></li>
							@if ($data->is_active)
							<li>
								<a href="#" class="actions" data-action="reports" data-data="{{ $data }}"><i
											class="fa fa-flag-o"></i>See Reports</a>
							</li>
							@endif
							<li class="divider"></li>
							<li>
								@if (!$data->is_active)
									<a href="#" class="actions" data-action="activate" data-data="{{ $data }}"><i
												class="fa fa-check-circle"></i>Activate</a>
								@else
									<a href="#" class="actions" data-action="deactivate" data-data="{{ $data }}"><i
												class="fa fa-ban"></i>Deactivate</a>
								@endif
							</li>
						</ul>
					</div>
				</td>
			</tr>
		@endforeach
	@endif
	</tbody>

</table>