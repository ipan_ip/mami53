{{-- Add / Update Modal --}}
<div class="modal" id="add-modal" role="dialog" aria-labelledby="modalLabel">
	<div class="modal-dialog" role="modal">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top:2px;"><i
							class="fa fa-lg fa-times-circle"></i></button>
				<h4 class="modal-title"><i class="fa fa-plus"></i>&nbsp;&nbsp;<span id="modalTitle"></span></h4>
			</div>
			<div class="modal-body">
				<form id="addForm" action="" method="post">
					<div class="row callout callout-warning">
						<div class="col-md-2">
							<i class="fa fa-exclamation-circle fa-4x text-warning"></i>
						</div>
						<div class="col-md-10">
							<strong>Make sure that the template you're about to fill is already approved by
								Whatsapp.</strong> Otherwise, it will never going to be sent to destinations.<br>For
							more info,
							please contact PM or Developer.
						</div>
					</div>
					<div class="row">
						<div class="col-md-8">
							<div class="form-group">
								<label for="name">CASE</label>
								<input type="text" id="name" placeholder="Ex. owner_registered" name="name"
								       class="form-control">
								<span class="help-block"><i class="fa fa-exclamation-circle"></i> Use lowercase and underscore (_) ONLY!</span>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="content">MESSAGE TEMPLATES</label>
								<textarea class="form-control" name="content"
								          placeholder="Ex. Hai @{{owner_name}}, selamat bergabung di *Mamikos*!"
								          id="content" rows="5" style="width: 100%"></textarea>
								<span class="help-block"><i class="fa fa-exclamation-circle"></i> Use double brackets for variable name.<br>
									<i class="fa fa-exclamation-circle"></i> Whatsapp formatting is also supported. Visit <a
											href="https://faq.whatsapp.com/en/android/26000002/" target="_blank">WhatsApp FAQ</a> for details.</span>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<label for="target_user">Target User</label>
							<select name="target_user" id="targetUser">
								<option value="owner" selected="selected">Owner</option>
								<option value="tenant">Tenant</option>
								<option value="all">All</option>
							</select>
						</div>
						<div class="col-md-4" style="padding-top: 25px!important;">
							<input data-three-state="false" data-toggle="checkbox-x" id="alsoSendSms"
							       name="sms_active" value="0" style="padding-top: 20px;">
							<label for="also_send_sms">Also send as SMS</label>
						</div>
						<div class="col-md-4" style="padding-top: 25px!important;">
							<input data-three-state="false" data-toggle="checkbox-x" id="activate" name="is_active"
							       value="0" style="padding-top: 20px;">
							<label for="is_active">Activate after saving</label>
						</div>
					</div>
					<div class="row" style="margin-top: 15px;">
						<div class="col-md-4">
							<div class="form-group">
								<label for="type">Dispatch Type</label>
								<select name="type" id="type">
									<option value="triggered" selected="selected">Triggered</option>
									<option value="scheduled">Scheduled</option>
								</select>
							</div>
						</div>
						<div class="col-md-8">
							<div class="form-group">
								<label for="on_event" id="onEventLabel">On Event</label>
								<select name="on_event" id="onEvent"></select>
							</div>
						</div>
					</div>
					<div class="row" id="sectionScheduled" style="margin-top: 15px;">
						<div class="col-md-4">
							<div class="form-group">
								<label for="schedule_period">Schedule Period</label>
								<select name="schedule_period" id="schedulePeriod">
									<option value="daily">Daily</option>
									<option value="weekly">Weekly</option>
									<option value="biweekly">Biweekly</option>
									<option value="monthly">Monthly</option>
									<option value="custom">Custom</option>
								</select>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label for="schedule_date">Schedule Date</label>
								<input name="schedule_date" id="scheduleDate" disabled="disabled" class="form-control"
								       autocomplete="off">
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label for="schedule_time">Schedule Time</label>
								<input name="schedule_time" id="scheduleTime" class="form-control" autocomplete="off">
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="submit">
					<i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save
				</button>
			</div>
		</div>
	</div>
</div>

{{-- SMS Template Modal --}}
<div class="modal" id="sms-modal" role="dialog" aria-labelledby="modalLabel">
	<div class="modal-dialog" role="modal">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top:2px;"><i
							class="fa fa-lg fa-times-circle"></i></button>
				<h4 class="modal-title"><i class="fa fa-commenting"></i>&nbsp;&nbsp;<span id="sms-modal-title"></span>
				</h4>
			</div>
			<div class="modal-body">
				<form id="smsForm" action="" method="post">
					<div class="row">
						<div class="col-md-8">
							<div class="form-group">
								<label for="name">CASE</label>
								<input type="text" id="sms-name" name="name"
								       class="form-control" disabled>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="content">SMS Template</label>
								<textarea class="form-control" name="content"
								          placeholder="Ex. Hai @{{owner_name}}, selamat bergabung di *Mamikos*!"
								          id="sms-content" rows="5" style="width: 100%"></textarea>
								<span class="help-block"><i class="fa fa-exclamation-circle"></i> Use double brackets for variable name.</span>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger pull-left" id="sms-reset">
					<i class="fa fa-undo"></i>&nbsp;&nbsp;Remove Custom Template
				</button>
				<button type="button" class="btn btn-primary" id="sms-submit">
					<i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Update
				</button>
			</div>
		</div>
	</div>
</div>