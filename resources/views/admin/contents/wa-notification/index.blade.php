@extends('admin.layouts.main')

@section('style')
	<style>
		.table > tbody > tr > td
		{
			vertical-align: middle;
		}
		
		.font-large
		{
			font-size: 1.5em;
		}
		
		.font-semi-large
		{
			font-size: 1em;
		}
		
		.font-grey
		{
			color: #9E9E9E;
		}
		
		.dropdown-menu > li > a:hover
		{
			color: #333
		}
		
		.label-grey
		{
			background-color: #CFD8DC;
		}
		
		[hidden]
		{
			display: none !important;
		}
		
		/* Select2 tweak */
		
		.input-group .select2-container,
		.form-group .select2-container
		{
			position: relative !important;
			z-index: 2;
			float: left !important;
			width: 100% !important;
			margin-bottom: 0 !important;
			display: table !important;
			table-layout: fixed !important;
		}
		
		/* Sweetalert */
		
		.swal-shown
		{
			height: auto !important;
		}
		
		.custom-swal
		{
			z-index: 10000 !important;
		}
		
		.custom-wide-swal
		{
			width: 640px !important;
			font-size: 13px !important;
		}
		
		/* Custom table row */
		tr.inactive td
		{
			background-color: #E6C8C8;
		}
		
		/* Modal tweak */
		
		.modal-open
		{
			overflow: hidden;
			position: fixed;
			width: 100%;
		}
		
		
		/* Centering the modal */
		
		.modal
		{
			text-align: center;
			padding: 0 !important;
		}
		
		.modal:before
		{
			content: '';
			display: inline-block;
			height: 100%;
			vertical-align: middle;
			margin-right: -4px;
		}
		
		.modal-dialog
		{
			display: inline-block;
			text-align: left;
			vertical-align: middle;
		}
		
		/* Dropzone tweaks */
		.dz-progress
		{
			/* progress bar covers file name */
			display: none !important;
		}
		
		.dz-remove
		{
			display: inline-block !important;
			width: 1.2em;
			height: 1.2em;
			position: absolute;
			top: 5px;
			right: 5px;
			z-index: 1000;
			font-size: 1.2em !important;
			line-height: 1em;
			text-align: center;
			font-weight: bold;
			border-radius: 1.2em;
		}
		
		.dz-remove:hover
		{
			text-decoration: none !important;
			opacity: 1;
			cursor: pointer !important;
		}
		
		.dz-error-mark svg g g
		{
			fill: #c00;
		}
		
		.clockpicker-popover
		{
			z-index: 999999 !important;
		}
	</style>
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
	<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet"/>
	<link href="//cdn.jsdelivr.net/npm/select2-bootstrap-theme@0.1.0-beta.10/dist/select2-bootstrap.min.css"
	      rel="stylesheet"/>
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/bootstrap-checkbox-x@1.5.5/css/checkbox-x.min.css">
	<link rel="stylesheet"
	      href="//cdn.jsdelivr.net/npm/bootstrap-datepicker@1.9.0/dist/css/bootstrap-datepicker3.min.css">
	{{ HTML::style('assets/vendor/clockpicker/bootstrap-clockpicker.min.css') }}
@endsection

@section('content')
	<!-- table -->
	<div class="box box-default">
		<!-- Box Header -->
		<div class="box-header">
			<div class="col-md-6 text-center">
				<h3 class="box-title"><i class="fa fa-whatsapp"></i> {{ $boxTitle }}</h3>
			</div>
			<!-- Add button -->
			<div class="col-md-6">
				<div class="form-horizontal pull-right" style="padding-top: 10px;">
					@role('super-admin', 'admin-general', 'developer')
					<button class="btn btn-success actions" data-action="add" style="margin-right: 10px;">
						<i class="fa fa-plus"></i>&nbsp;&nbsp;Add Template
					</button>
					<button class="btn btn-default actions" data-action="check">
						<i class="fa fa-paper-plane"></i>&nbsp;&nbsp;Check Active Scenario
					</button>
					@endrole
				</div>
			</div>
		</div>
		<!-- /.box-header -->
		
		<div class="box-body" style="padding-top: 30px;">
			{{-- Table --}}
			@include('admin.contents.wa-notification.partials.table')
		
		</div><!-- /.box-body -->
		{{-- pagination --}}
		@if (count($rowsData))
			<div class="box-body text-center">
				{{ $rowsData->appends(Request::except('page'))->links() }}
			</div>
		@endif
		{{-- /.pagination --}}
	</div><!-- /.box -->
	<!-- table -->
	
	{{-- Modal --}}
	@include('admin.contents.wa-notification.partials.modal')

@endsection

@section('script')
	<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>
	<script src="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.js"></script>
	<script src="//cdn.jsdelivr.net/npm/bootstrap-checkbox-x@1.5.5/js/checkbox-x.min.js"></script>
	<script src="//cdn.jsdelivr.net/npm/bootstrap-datepicker@1.9.0/dist/js/bootstrap-datepicker.min.js"></script>
	{{ HTML::script('assets/vendor/clockpicker/bootstrap-clockpicker.min.js') }}
	<script type="text/javascript">
        function triggerAlert(type, message) {
            Swal.fire({
                type: type,
                customClass: {
                    container: 'custom-swal'
                },
                html: message
            });
        }

        function triggerLoading(message) {
            Swal.fire({
                title: message,
                customClass: {
                    container: 'custom-swal'
                },
                allowEscapeKey: false,
                allowOutsideClick: false,
                onOpen: () => {
                    swal.showLoading();
                }
            });
        }

        function ajaxCall(type, title, html, method, url, payload, input, btnText, btnClass, needRefresh = true) {
            Swal.fire({
                type: type,
                title: title,
                html: html,
                buttonsStyling: btnClass == null,
                confirmButtonText: btnText,
                confirmButtonClass: btnClass,
                showCancelButton: true,
                cancelButtonClass: (btnClass !== null) ? 'btn btn-default' : null,
                customClass: 'custom-swal',
                showLoaderOnConfirm: true,
                input: input,
                preConfirm: (response) => {
                    if (input !== null) {
                        let number = response;
                        if (!response) {
                            return Swal.showValidationMessage('Please provide a valid phone number!');
                        }

                        payload = {
                            "template_id": payload.id,
                            "number": response
                        };
                    }

                    return $.ajax({
                        type: method,
                        url: url,
                        data: payload,
                        dataType: 'json',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: (response) => {
                            if (response.success == false) {
                                Swal.close();
                                triggerAlert('error', response.message);
                            }
                                
                            return;
                        },
                        error: (error) => {
                            Swal.close();
                            triggerAlert('error', JSON.stringify(error));
                        }
                    });
                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                if (!result.dismiss != 'cancel') {
                    if (result.value.success == true) {
                        Swal.fire({
                            type: 'success',
                            title: result.value.title,
                            html: result.value.message,
                            onClose: () => {
                                if (needRefresh) window.location.reload();
                            }
                        });
                    } else {
                        Swal.fire({
                            type: 'error',
                            title: "Failed!",
                            html: result.value.message
                        });
                    }
                }
            });
        }

        $(function () {
            // Helper variable
            let _editing, _data, _modalTitle;

            // Actions button listeners
            $('.actions').click((e) => {
                e.preventDefault();

                const action = $(e.currentTarget).data('action');

                // Setup Notification
                if (action === 'add') {
                    _editing = false;
                    _modalTitle = "New Notification Template";

                    $('#add-modal').modal('show');
                }

                // Editing action
                else if (action === 'update') {
                    _editing = true;
                    _data = $(e.currentTarget).data('data');
                }

                // Check existing scenario
                else if (action === 'check') {
                    ajaxCall(null, 'Check Scenario', '<h5>Click on [Check] button to start checking active OMNI scenario required for WhatsApp notification service.</h5>', 'GET', "/admin/wa-notification/scenario", null, null, 'Check', null, false);
                }

                // Check template validation status
                else if (action === 'validate') {
                    triggerAlert('warning', '<h5>Template validation will be ready soon!</h5>');
                }

                // Send test
                else if (action === 'test') {
                    _data = $(e.currentTarget).data('data');

                    let payload = {
                        id: _data.id
                    };

                    ajaxCall(null, 'Sending Test Notification', '<h5>Type a valid phone number to begin sending test notification</h5><br/>Make sure that <strong>WhatsApp app is enabled</strong> on destination phone', 'POST', "/admin/wa-notification/test", payload, 'text', 'Send Test Notification', null, false);
                }

                // Check reports
                else if (action === 'reports') {
                    _data = $(e.currentTarget).data('data');
                    console.log(_data);
                    
                    window.location.href = "/admin/wa-notification/reports/" + _data.id;
                }

                // Sync reports
                else if (action === 'sync-reports') {
                    _data = $(e.currentTarget).data('data');
                    _editing = false;

                    let payload = {
                        template_id: _data.id
                    };

                    ajaxCall('question', "Sync reports with WhatsApp server?", null, 'POST', "/admin/wa-notification/reports/sync", payload, null, 'Sync', null, false);
                }

                // Enable / disable SMS
                else if (action === 'enable' || action === 'disable') {
                    let title, html, btnText, url, type, payload;

                    _data = $(e.currentTarget).data('data');

                    if (action === 'enable') {
                        type = 'question';
                        title = 'Enable SMS for this template?';
                        html = null;
                        btnText = 'Enable';
                        url = "/admin/wa-notification/enable";
                        payload = {
                            status: 1,
                            id: _data.id
                        };
                    } else {
                        type = 'warning';
                        title = 'Disable SMS for this template?';
                        html = 'Caution! This acton will ONLY enable notification by WhatsApp!';
                        btnText = 'Disable';
                        url = "/admin/wa-notification/disable";
                        payload = {
                            status: 0,
                            id: _data.id
                        };
                    }

                    ajaxCall(type, title, html, 'POST', url, payload, null, btnText, null);
                }

                // Activate / Deactivate action
                else if (action === 'activate' || action === 'deactivate') {
                    let title, html, btnText, url, type, payload;

                    _data = $(e.currentTarget).data('data');

                    if (action === 'activate') {
                        type = 'question';
                        title = 'Activate this template?';
                        html = null;
                        btnText = 'Activate';
                        url = "/admin/wa-notification/activate";
                        payload = {
                            status: 1,
                            id: _data.id
                        };
                    } else {
                        type = 'warning';
                        title = 'Deactivate this template?';
                        html = 'Warning! This acton will disable the notification being dispatched';
                        btnText = 'Deactivate';
                        url = "/admin/wa-notification/deactivate";
                        payload = {
                            status: 0,
                            id: _data.id
                        };
                    }

                    ajaxCall(type, title, html, 'POST', url, payload, null, btnText, null);
                }

                // Update SMS template action
                else if (action === 'sms') {
                    _data = $(e.currentTarget).data('data');
                    _modalTitle = "Update SMS template for CASE: <strong>" + _data.name + "</strong>";

                    $('#sms-modal').modal('show');
                }
            });

            // :: Modal Events ::
            let modal = $('#add-modal');
            let reportsModal = $('#reports-modal');
            let smsModal = $('#sms-modal');

            modal
                .on("show.bs.modal", (e) => {
                    triggerLoading('Setting up form..');

                    modal.data('bs.modal').options.keyboard = false;
                    modal.data('bs.modal').options.backdrop = 'static';

                    $('#modalTitle').html(_modalTitle);

                    const _target_user = 'owner'; // default value
                    let _activeType;

                    // Form sections
                    const sectionScheduled = $('#sectionScheduled');

                    // Form elements
                    const nameInput = $('#name');
                    const contentInput = $('#content');
                    const targetUserSelector = $('#targetUser');
                    const dispatchTypeSelector = $('#type');
                    const onEventLabel = $('#onEventLabel');
                    const onEventSelector = $('#onEvent');
                    const schedulePeriodSelector = $('#schedulePeriod');
                    const scheduleTime = $('#scheduleTime');
                    const scheduleDate = $('#scheduleDate');
                    const sendAsSmsCheckbox = $('#alsoSendSms');
                    const activateCheckbox = $('#activate');
                    const submitButton = $('#submit');

                    // Functions
                    function initialState() {
                        _activeType = 'triggered';
                        sectionScheduled.hide();
                    }

                    function compileDispatchForm($type) {
                        if ($type === "triggered") {
                            onEventLabel.html('On Event');
                            sectionScheduled.hide();
                            schedulePeriodSelector.val('daily').change();
                            scheduleDate.val('');
                            scheduleTime.val('');
                        } else {
                            onEventLabel.html('Condition');
                            onEventSelector.val(null).change();
                            sectionScheduled.show();
                        }
                    }

                    function setNotificationByType($value) {
                        compileDispatchForm($value);
                    }

                    function setScheduledDateAndTime($value) {
                        if ($value === "custom") {
                            scheduleDate.removeAttr('disabled');
                        } else {
                            scheduleDate.attr('disabled', true);
                        }
                    }

                    function populateOnEventSelector(target) {
                        $.ajax({
                            type: 'POST',
                            url: '/admin/wa-notification/events',
                            data: {
                                user_type: target
                            },
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            success: data => {
                                $.map(data, (val, key) => {
                                    const newOption = new Option(val, key, false, false);
                                    onEventSelector.append(newOption).trigger('change');
                                });
                            },
                            error: error => {
                                console.log(error);
                                // triggerAlert('error', error);
                            },
                            complete: function () {
                                if (Swal.isLoading()) Swal.close();
                            }
                        });

                    }

                    function checkInput(string) {
                        const regex = /^\w+$/;
                        return regex.test(string);
                    }

                    function getFormData() {
                        const formData = $('#addForm').serializeArray();
                        if (_editing) formData['id'] = _data.id;
                        return formData;
                    }

                    function validateFormData() {
                        const targetUserSelectorValue = targetUserSelector.val();
                        const dispatchTypeSelectorValue = dispatchTypeSelector.val();
                        const onEventSelectorValue = onEventSelector.val();

                        if (nameInput.val() === '') {
                            triggerAlert('error', "<h5>Template's CASE is required!</h5>");
                            nameInput.parent().addClass('has-error');
                            nameInput.focus();
                            return false;
                        } else {
                            const nameInputValue = nameInput.val();
                            if (!checkInput(nameInputValue)) {
                                triggerAlert('error', "<h5>Template's CASE is not valid!</h5>Please use ONLY lowercase and underscore");
                                nameInput.parent().addClass('has-error');
                                nameInput.focus();
                                return false;
                            }
                        }

                        if (contentInput.val() === '') {
                            triggerAlert('error', "<h5>MESSAGE TEMPLATES ID is required!</h5>");
                            contentInput.parent().addClass('has-error');
                            contentInput.focus();
                            return false;
                        }

                        if (onEventSelectorValue === '' || onEventSelectorValue == null) {
                            if (_activeType === 'triggered')
                                triggerAlert('error', "<h5>Choose " + targetUserSelectorValue + " event first!</h5>");
                            else
                                triggerAlert('error', "<h5>Choose " + targetUserSelectorValue + " condition first!</h5>");

                            onEventSelector.parent().addClass('has-error');
                            onEventSelector.focus();
                            return false;
                        }

                        const schedulePeriodSelectorValue = schedulePeriodSelector.val();
                        const scheduleTimeValue = scheduleTime.val();

                        if (schedulePeriodSelectorValue === 'custom') {
                            const scheduleDateValue = scheduleDate.val();
                            if (scheduleDateValue === '' || scheduleDateValue == null) {
                                triggerAlert('error', "<h5>Schedule Date is required!</h5>");
                                scheduleDate.parent().addClass('has-error');
                                return false;
                            }
                        }

                        if (_activeType !== 'triggered') {
                            if (scheduleTimeValue === '' || scheduleTimeValue == null) {
                                triggerAlert('error', "<h5>Schedule Time is required!</h5>");
                                scheduleTime.parent().addClass('has-error');
                                scheduleTime.focus();
                                return false;
                            }
                        }

                        return true;
                    }

                    // Components initialization
                    targetUserSelector.select2({
                        theme: "bootstrap",
                        dropdownParent: modal,
                        minimumResultsForSearch: -1
                    });

                    dispatchTypeSelector.select2({
                        theme: "bootstrap",
                        dropdownParent: modal,
                        minimumResultsForSearch: -1
                    });

                    onEventSelector.select2({
                        theme: "bootstrap",
                        dropdownParent: modal,
                        minimumResultsForSearch: -1,
                    });

                    schedulePeriodSelector.select2({
                        theme: "bootstrap",
                        dropdownParent: modal,
                        minimumResultsForSearch: -1
                    });

                    scheduleDate.datepicker({
                        format: 'dd-mm-yyyy',
                        startDate: new Date(new Date().setDate(new Date().getDate() + 1)),
                        endDate: new Date(new Date().setDate(new Date().getDate() + 30)),
                        autoclose: true,
                        todayHighlight: true
                    });

                    scheduleTime.clockpicker({
                        placement: 'top',
                        align: 'left',
                        autoclose: true,
                        now: 'now',
                        minutestep: 5
                    });

                    sendAsSmsCheckbox.checkboxX();
                    activateCheckbox.checkboxX();

                    // Main actions
                    initialState();
                    setNotificationByType(dispatchTypeSelector.find(':selected').val());
                    populateOnEventSelector(targetUserSelector.find(':selected').val());

                    // Events
                    nameInput.on('keyup', () => {
                        if (nameInput.parent().hasClass('has-error')) {
                            nameInput.parent().removeClass('has-error');
                        }
                    });

                    contentInput.on('keyup', () => {
                        if (contentInput.parent().hasClass('has-error')) {
                            contentInput.parent().removeClass('has-error');
                        }
                    });

                    targetUserSelector.on('change', () => {
                        populateOnEventSelector(targetUserSelector.find(':selected').val());
                        onEventSelector.empty().trigger('change');
                    });

                    dispatchTypeSelector.on('change', () => {
                        const value = dispatchTypeSelector.find(':selected').val();
                        _activeType = value;
                        setNotificationByType(value);
                    });

                    onEventSelector.on('change', () => {
                        if (onEventSelector.parent().hasClass('has-error')) {
                            onEventSelector.parent().removeClass('has-error');
                        }
                    });

                    schedulePeriodSelector.on('change', () => {
                        setScheduledDateAndTime(schedulePeriodSelector.find(':selected').val());
                    });

                    scheduleDate.on('click', () => {
                        if (scheduleDate.parent().hasClass('has-error')) {
                            scheduleDate.parent().removeClass('has-error');
                        }
                    });

                    scheduleTime.on('click', () => {
                        if (scheduleTime.parent().hasClass('has-error')) {
                            scheduleTime.parent().removeClass('has-error');
                        }
                    });

                    // Submit the form
                    submitButton.on('click', e => {
                        e.preventDefault();

                        if (validateFormData()) {
                            // compile dataObject
                            const dataObject = getFormData();

                            Swal.fire({
                                type: 'question',
                                customClass: {
                                    container: 'custom-swal'
                                },
                                title: 'Save new template data?',
                                showCancelButton: true,
                                confirmButtonText: 'Submit',
                                showLoaderOnConfirm: true,
                                preConfirm: () => {
                                    return $.ajax({
                                        type: 'POST',
                                        url: "/admin/wa-notification",
                                        dataType: 'json',
                                        headers: {
                                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                        },
                                        data: dataObject
                                    });
                                },
                                allowOutsideClick: () => !Swal.isLoading()
                            }).then((result) => {
                                if (!result.dismiss) {
                                    const response = result.value;

                                    if (!response.success) {
                                        triggerAlert('error', '<h5>' + response.message + '</h5>');
                                    } else {
                                        $('#add-modal').modal('toggle');

                                        Swal.fire({
                                            type: 'success',
                                            customClass: {
                                                container: 'custom-swal'
                                            },
                                            title: "New WhatsApp template successfully saved!",
                                            html: "<h5>" + response.message + "</h5>",
                                            onClose: () => {
                                                window.location.reload();
                                            }
                                        });
                                    }

                                }
                            })
                        }
                    })
                })
                .on("hide.bs.modal", (e) => {
                    //
                });


            smsModal
                .on("show.bs.modal", (e) => {
                    $('#sms-modal-title').html(_modalTitle);

                    const nameInput = $('#sms-name');
                    const contentInput = $('#sms-content');
                    const submitButton = $('#sms-submit');
                    const resetButton = $('#sms-reset');

                    function getSMSFormData() {
                        const formData = $('#smsForm').serializeArray();
                        formData.push({
                            name: 'id',
                            value: _data.id
                        });
                        
                        return formData;
                    }

                    function validateSMSFormData() {
                        if (contentInput.val() === '') {
                            triggerAlert('error', "<h5>Template content is required!</h5>");
                            contentInput.parent().addClass('has-error');
                            contentInput.focus();
                            return false;
                        }

                        if (contentInput.val() === _data.sms_content) {
                            triggerAlert('error', "<h5>Update the content before submitting!</h5>");
                            contentInput.parent().addClass('has-error');
                            contentInput.focus();
                            return false;
                        }

                        return true;
                    }

                    // Set default value
                    nameInput.val(_data.name);

                    if (_data.sms_content == null) {
                        contentInput.text('').attr('placeholder', _data.content.replace(/<(.|\n)*?>/g, ''));
                    } else {
                        contentInput.removeAttr('placeholder').text(_data.sms_content);
                    }

                    contentInput.on('keyup', () => {
                        if (contentInput.parent().hasClass('has-error')) {
                            contentInput.parent().removeClass('has-error');
                        }
                    });

                    // Event handlers
                    resetButton.on('click', () => {
                        Swal.fire({
                                type: 'warning',
                                customClass: {
                                    container: 'custom-swal'
                                },
                                title: 'Remove custom SMS template?',
                                html: '<h5>SMS template for this notification will be falling back to WhatsApp\'s</h5>',
                                showCancelButton: true,
                                confirmButtonText: 'Removed',
                                showLoaderOnConfirm: true,
                                preConfirm: () => {
                                    return $.ajax({
                                        type: 'POST',
                                        url: "/admin/wa-notification/sms/reset",
                                        dataType: 'json',
                                        headers: {
                                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                        },
                                        data: {
                                            id: _data.id
                                        }
                                    });
                                },
                                allowOutsideClick: () => !Swal.isLoading()
                            }).then((result) => {
                                if (!result.dismiss) {
                                    const response = result.value;

                                    if (!response.success) {
                                        triggerAlert('error', '<h5>' + response.message + '</h5>');
                                    } else {
                                        $('#sms-modal').modal('toggle');

                                        Swal.fire({
                                            type: 'success',
                                            customClass: {
                                                container: 'custom-swal'
                                            },
                                            title: "SMS template successfully removed!",
                                            onClose: () => {
                                                window.location.reload();
                                            }
                                        });
                                    }

                                }
                            })
                    })

                    submitButton.on('click', () => {
                        if (validateSMSFormData()) {
                            // compile dataObject
                            const dataObject = getSMSFormData();

                            Swal.fire({
                                type: 'question',
                                customClass: {
                                    container: 'custom-swal'
                                },
                                title: 'Save custom SMS template?',
                                showCancelButton: true,
                                confirmButtonText: 'Submit',
                                showLoaderOnConfirm: true,
                                preConfirm: () => {
                                    return $.ajax({
                                        type: 'POST',
                                        url: "/admin/wa-notification/sms",
                                        dataType: 'json',
                                        headers: {
                                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                        },
                                        data: dataObject
                                    });
                                },
                                allowOutsideClick: () => !Swal.isLoading()
                            }).then((result) => {
                                if (!result.dismiss) {
                                    const response = result.value;

                                    if (!response.success) {
                                        triggerAlert('error', '<h5>' + response.message + '</h5>');
                                    } else {
                                        $('#sms-modal').modal('toggle');

                                        Swal.fire({
                                            type: 'success',
                                            customClass: {
                                                container: 'custom-swal'
                                            },
                                            title: "Custom SMS template successfully saved!",
                                            onClose: () => {
                                                window.location.reload();
                                            }
                                        });
                                    }

                                }
                            })
                        }
                    })
                })
                .on("hide.bs.modal", (e) => {

                })
        });
	</script>
@endsection