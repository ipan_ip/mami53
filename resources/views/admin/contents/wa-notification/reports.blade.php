@extends('admin.layouts.main')

@section('style')
	<style>
		.table > tbody > tr > td
		{
			vertical-align: middle;
		}
		
		.font-large
		{
			font-size: 1.5em;
		}
		
		.font-semi-large
		{
			font-size: 1em;
		}
		
		.font-grey
		{
			color: #9E9E9E;
		}
		
		.dropdown-menu > li > a:hover
		{
			color: #333
		}
		
		.label-grey
		{
			background-color: #CFD8DC;
		}
		
		[hidden]
		{
			display: none !important;
		}
		
		/* Select2 tweak */
		
		.input-group .select2-container,
		.form-group .select2-container
		{
			position: relative !important;
			z-index: 2;
			float: left !important;
			width: 100% !important;
			margin-bottom: 0 !important;
			display: table !important;
			table-layout: fixed !important;
		}
		
		/* Sweetalert */
		
		.swal-shown
		{
			height: auto !important;
		}
		
		.custom-swal
		{
			z-index: 10000 !important;
		}
		
		.custom-wide-swal
		{
			width: 640px !important;
			font-size: 13px !important;
		}
		
		/* Custom table row */
		tr.inactive td
		{
			background-color: #E6C8C8;
		}
		
		/* Modal tweak */
		
		.modal-open
		{
			overflow: hidden;
			position: fixed;
			width: 100%;
		}
		
		
		/* Centering the modal */
		
		.modal
		{
			text-align: center;
			padding: 0 !important;
		}
		
		.modal:before
		{
			content: '';
			display: inline-block;
			height: 100%;
			vertical-align: middle;
			margin-right: -4px;
		}
		
		.modal-dialog
		{
			display: inline-block;
			text-align: left;
			vertical-align: middle;
		}
		
		/* Dropzone tweaks */
		.dz-progress
		{
			/* progress bar covers file name */
			display: none !important;
		}
		
		.dz-remove
		{
			display: inline-block !important;
			width: 1.2em;
			height: 1.2em;
			position: absolute;
			top: 5px;
			right: 5px;
			z-index: 1000;
			font-size: 1.2em !important;
			line-height: 1em;
			text-align: center;
			font-weight: bold;
			border-radius: 1.2em;
		}
		
		.dz-remove:hover
		{
			text-decoration: none !important;
			opacity: 1;
			cursor: pointer !important;
		}
		
		.dz-error-mark svg g g
		{
			fill: #c00;
		}
		
		.clockpicker-popover
		{
			z-index: 999999 !important;
		}
	</style>
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
	<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet"/>
	<link href="//cdn.jsdelivr.net/npm/select2-bootstrap-theme@0.1.0-beta.10/dist/select2-bootstrap.min.css"
	      rel="stylesheet"/>
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/bootstrap-checkbox-x@1.5.5/css/checkbox-x.min.css">
	<link rel="stylesheet"
	      href="//cdn.jsdelivr.net/npm/bootstrap-datepicker@1.9.0/dist/css/bootstrap-datepicker3.min.css">
	{{ HTML::style('assets/vendor/clockpicker/bootstrap-clockpicker.min.css') }}
@endsection

@section('content')
	<!-- table -->
	<div class="box box-default">
		<!-- Box Header -->
		<div class="box-header">
			<div class="col-md-4">
                <h3><i class="fa fa-flag-o"></i> {{ $boxTitle }}
                    <br><small>Template Name: <strong>{{ $templateData->name }}</strong></small>
                </h3>
			</div>
			<!-- Add button -->
			<div class="col-md-8">
                <div class="horizontal-wrapper">
                    <div class="btn-horizontal-group bg-default">
                        <form id="filterForm" action="" class="form-inline" method="get" style="text-align:right;padding:10px;">
                            <input type="text" name="date" class="form-control input-md datepicker" placeholder="Select Report Date" value="{{ request()->input('date') }}" id="date" autocomplete="off" style="margin-right:2px" />
                            {{ Form::select('sort', $sortFilter, request()->input('sort'), [
                                'class' => 'form-control btn btn-primary', 'id' => 'sortFilter'
                            ]) }}
                            {{ Form::select('status', $statusFilter, request()->input('status'), [
                                'class' => 'form-control btn btn-primary', 'id' => 'statusFilter'
                            ]) }}
                            <button class="btn btn-success actions" data-action="sync-reports" data-id="{{ $templateData->id }}" style="margin-left: 10px;">
                                <i class="fa fa-refresh"></i> Sync Reports
                            </button>
                            <br/>
                            <br/>
                            <input type="text" name="q" class="form-control input-md" placeholder="Search by Message ID or Phone Number" autocomplete="off" value="{{ request()->input('q') }}" style="width: 360px;">
                            <button class="btn btn-primary btn-md" id="buttonSearch"><i class="fa fa-search"></i>
                            </button>
                        </form>
                    </div>
                </div>
			</div>
		</div>
		<!-- /.box-header -->
		
		<div class="box-body">
			{{-- Table --}}
			@include('admin.contents.wa-notification.partials.table-reports')
		</div><!-- /.box-body -->
		{{-- pagination --}}
		@if (count($rowsData))
			<div class="box-body text-center">
				{{ $rowsData->appends(Request::except('page'))->links() }}
			</div>
		@endif
		{{-- /.pagination --}}
	</div><!-- /.box -->
	<!-- table -->

@endsection

@section('script')
	<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>
	<script src="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.js"></script>
	<script src="//cdn.jsdelivr.net/npm/bootstrap-checkbox-x@1.5.5/js/checkbox-x.min.js"></script>
	<script src="//cdn.jsdelivr.net/npm/bootstrap-datepicker@1.9.0/dist/js/bootstrap-datepicker.min.js"></script>
	{{ HTML::script('assets/vendor/clockpicker/bootstrap-clockpicker.min.js') }}
	<script type="text/javascript">
        function triggerAlert(type, message, title) {
            title = title == null ? '' : title;

            Swal.fire({
                type: type,
                customClass: {
                    container: 'custom-swal'
                },
                title: title,
                html: message
            });
        }

        function triggerAlertWide(type, message, title) {
            title = title == null ? '' : title;

            Swal.fire({
                type: type,
                customClass: 'custom-wide-swal',
                title: title,
                html: message
            });
        }

        function triggerLoading(message) {
            Swal.fire({
                title: message,
                customClass: {
                    container: 'custom-swal'
                },
                allowEscapeKey: false,
                allowOutsideClick: false,
                onOpen: () => {
                    swal.showLoading();
                }
            });
        }

        function ajaxCall(type, title, html, method, url, payload, input, btnText, btnClass, needRefresh = true) {
            Swal.fire({
                type: type,
                title: title,
                html: html,
                buttonsStyling: btnClass == null,
                confirmButtonText: btnText,
                confirmButtonClass: btnClass,
                showCancelButton: true,
                cancelButtonClass: (btnClass !== null) ? 'btn btn-default' : null,
                customClass: 'custom-swal',
                showLoaderOnConfirm: true,
                input: input,
                preConfirm: (response) => {
                    if (input !== null) {
                        let number = response;
                        if (!response) {
                            return Swal.showValidationMessage('Please provide a valid phone number!');
                        }

                        payload = {
                            "template_id": payload.id,
                            "number": response
                        };
                    }

                    return $.ajax({
                        type: method,
                        url: url,
                        data: payload,
                        dataType: 'json',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: (response) => {
                            return;
                        },
                        error: (error) => {
                            Swal.close();
                            triggerAlert('error', JSON.stringify(error));
                        }
                    });
                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                if (!result.dismiss != 'cancel') {
                    if (result.value.success == true) {
                        Swal.fire({
                            type: 'success',
                            title: result.value.title,
                            html: result.value.message,
                            onClose: () => {
                                if (needRefresh) window.location.reload();
                            }
                        });
                    } else {
                        Swal.fire({
                            type: 'error',
                            title: "Failed!",
                            html: result.value.message,
                            onClose: () => {
                                if (needRefresh) window.location.reload();
                            }
                        });
                    }
                }
            });
        }

        $(function () {
            // Helper variable
            let _editing, _data, _modalTitle;

            // Form elements init
            let form = $('#filterForm');
            let sortFilter = $('#sortFilter');
            let statusFilter = $('#statusFilter');
            let datePicker = $('.datepicker');

            // Datepicker initiali
            datePicker
            .datepicker({ 
                format: 'dd/mm/yyyy',
                todayHighlight: true,
                autoclose: true,
                endDate: '0d',
                clearBtn: true
            })
            .on('changeDate', e => {
                form.trigger('submit');
            });

            // Actions button listeners
            $('.actions').click((e) => {
                e.preventDefault();

                const action = $(e.currentTarget).data('action');

                // Sync reports
                if (action === 'sync-reports') {
                    let id = $(e.currentTarget).data('id');

                    let payload = {
                        template_id: id
                    };

                    ajaxCall('question', "Sync reports with WhatsApp server?", null, 'POST', "/admin/wa-notification/reports/sync", payload, null, 'Sync', null);
                }

                // Refresh reports
                else if (action === 'refresh') {
                    let data = $(e.currentTarget).data('report');

                    let payload = {
                        id: data.id
                    };

                    ajaxCall('question', "Refresh delivery status?", null, 'POST', "/admin/wa-notification/refresh", payload, null, 'Refresh', null);
                }

                // See message copntent
                else {
                    let content = $(e.currentTarget).data('content');

                    triggerAlertWide('info', '<div class="panel"><div class="panel-body text-left"><pre>' + content + '</pre></div></div>', 'Message Content');
                }
            });

            // Filter listeners
            sortFilter.on('change', () => {
                form.trigger('submit');
            });

            statusFilter.on('change', () => {
                form.trigger('submit');
            });

            datePicker
        });
	</script>
@endsection