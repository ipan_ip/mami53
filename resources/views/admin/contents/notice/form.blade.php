@extends('admin.layouts.main')
@section('content')
<div class="box box-primary">
  <div class="box-header">
      <h3 class="box-title">{{ $boxTitle }}</h3>
  </div><!-- /.box-header -->
  {{ Form::open(array('route' => $formAction, 'method' => $formMethod,
      'class' => 'form-horizontal form-bordered', 'role' => 'form', 'id' => 'formInsertNotice')) }}
      <div class="box-body no-padding">
            <div class="form-group bg-default">
              <label for="inputName" class="col-sm-2 control-label">Title</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" placeholder="Title"
                  id="inputTitle" name="title" value="{{ $rowNotice->title }}">
              </div>
            </div>
            <div class="form-group bg-default">
              <label for="inputName" class="col-sm-2 control-label">Description</label>
              <div class="col-sm-10">
                <textarea class="form-control" placeholder="Description"
                  id="inputDescription" name="description">{{ $rowNotice->description }}</textarea>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="{{ URL::route('admin.notice.index') }}">
                  <button type="button" class="btn btn-default">Cancel</button>
                </a>
                <button type="reset" class="btn btn-default"
                  onclick="$('#formInsertNotice').bootstrapValidator('resetForm', true);">Reset</button>
              </div>
            </div>
      </div>
  {{ Form::close() }}<!-- /.form -->
</div><!-- /.box -->
@stop
@section('script')
<!-- page script -->
<script type="text/javascript">

    @if (Session::get('laravelValidatorJSON') === null)
      myGlobal.laravelValidator = null;
    @else
      myGlobal.laravelValidator = {{ Session::get('laravelValidatorJSON') }};
    @endif


    $(function()
    {
      $formInsertNotice = $('#formInsertNotice');

      formStoreNoticeRules = {
          title: {
              message: 'The title is not valid',
              validators: {
                  notEmpty: {
                      message: 'The title is required and cannot be empty'
                  },
                  stringLength: {
                      min: 3,
                      max: 30,
                      message: 'The title must be more than 3 and less than 30 characters long'
                  }
              }
          },
          description: {
              message: 'The description is not valid',
              validators: {
                  notEmpty: {
                      message: 'The description is required and cannot be empty'
                  },
                  stringLength: {
                      min: 3,
                      message: 'The description must be more than 3 characters long'
                  }
              }
          },
      };

      formUpdateNoticeRules = {
          title: {
              message: 'The title is not valid',
              validators: {
                  notEmpty: {
                      message: 'The title is required and cannot be empty'
                  },
                  stringLength: {
                      min: 3,
                      max: 30,
                      message: 'The title must be more than 3 and less than 30 characters long'
                  }
              }
          },
          description: {
              message: 'The description is not valid',
              validators: {
                  notEmpty: {
                      message: 'The description is required and cannot be empty'
                  },
                  stringLength: {
                      min: 3,
                      message: 'The description must be more than 3 characters long'
                  }
              }
          },
      };

      @if (Route::currentRouteName() === 'admin.notice.create')
        myGlobal.bootstrapValidatorDefaults.fields = formStoreNoticeRules;
      @elseif (Route::currentRouteName() === 'admin.notice.edit')
        myGlobal.bootstrapValidatorDefaults.fields = formUpdateNoticeRules;
      @endif


      $formInsertNotice.bootstrapValidator(myGlobal.bootstrapValidatorDefaults);

      updateValidateStatus($formInsertNotice.data('bootstrapValidator'), myGlobal.laravelValidator);
    });
</script>
@stop