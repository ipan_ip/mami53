@extends('admin.layouts.main')
@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
            <div class="box-tools pull-right">
                <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <a href="{{ URL::route('admin.notice.create') }}">
                        <button type="submit" class="btn-add btn btn-primary btn-sm">
                            <i class="fa fa-plus">&nbsp;</i>Add Notice</button>
                    </a>
                </div>
            </div>

            <table id="tableListNotice" class="table table-striped">
                <thead>
                    <tr>
                        <th width="30px;">No</th>
                        <th>{{ Lang::get('hairclick.page.notice.title') }}</th>
                        <th>{{ Lang::get('hairclick.page.notice.description') }}</th>
                        <th class="table-action-column" width="100px">{{ Lang::get('hairclick.page.notice.action') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($rowsNotice as $rowNoticeKey => $rowNotice)
                    <tr>
                        <td>{{ $rowNoticeKey+1 }}</td>
                        <td>{{ $rowNotice->title }}</td>
                        <td>{{ $rowNotice->description }}</td>
                        <td class="table-action-column">
                            <div class="btn-action-group">
                                <a href="{{ URL::route('admin.notice.edit', $rowNotice->id) }}">
                                    <i class="fa fa-pencil"></i></a>
                                <a href="#" ng-click="showConfirmDelete('Notice', {{ $rowNotice->id }}, '{{ $rowNotice->name }}');">
                                    <i class="fa fa-trash-o"></i></a>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>No</th>
                        <th>{{ Lang::get('hairclick.page.notice.title') }}</th>
                        <th>{{ Lang::get('hairclick.page.notice.description') }}</th>
                        <th class="table-action-column">{{ Lang::get('hairclick.page.notice.action') }}</th>
                    </tr>
                </tfoot>
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
    <!-- table -->

    @include('admin.partials.delete-modal')
@stop
@section('script')
<!-- page script -->
<script type="text/javascript">
    $(function()
    {
        $("#tableListNotice").dataTable();
    });
</script>
@stop