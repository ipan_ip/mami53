<table class="table">
    <thead>
        <tr>
            <th></th>
            <th class="text-center">Data Date</th>
            <th>Owner</th>
            <th class="text-center">Total Listed Kost(s)</th>
            <th class="text-center">Booking Activation Date /</th>
            <th class="text-center" colspan="2">Fast Booking Response</th>
            <th class="text-center" colspan="2">Booking Transactions</th>
            <th class="text-center" colspan="2">Review From Tenants</th>
            <th class="text-center">Total Loyalty</th>
            <th class="text-center">Uploder</th>
            <th></th>
        </tr>
        <tr>
            <th colspan="4"></th>
            <th class="text-center">Expiration Date</th>
            <th class="text-center">Total</th>
            <th class="text-center">Points</th>
            <th class="text-center">Total</th>
            <th class="text-center">Points</th>
            <th class="text-center">Total</th>
            <th class="text-center">Points</th>
            <th colspan="2"></th>
        </tr>
    </thead>
    <tbody>
        @if (!count($rowsOwner))
        <tr>
            <td class="text-center" colspan="14">
                <div class="callout callout-danger">Tidak ada data untuk ditampilkan!<br>Klik <strong>[Import Data]</strong> untuk menambahkan data baru</div>
            </td>
        </tr>
        @else
        @foreach($rowsOwner as $index => $owner)
        <tr class="{{ is_null($owner->user) ? 'missing-user' : '' }}">
            <td class="text-center">
                {{--  --}}
            </td>
            <td class="text-center">
                {{ date('d M Y', strtotime($owner->data_date)) }}
            </td>
            <td>
                @if (!is_null($owner->user))
                <strong class="font-semi-large">{{ $owner->user->name }}</strong>
                <br><small><i class="fa fa-phone fa-xs font-grey"></i> {{ $owner->user->phone_number }}</small>
                <br><small><i class="fa fa-envelope fa-xs font-grey"></i> {{ $owner->user->email }}</small>
                @else
                <a href="#" class="btn btn-xs btn-danger actions" data-action="assign" data-owner="{{ $owner }}"><i
                        class="fa fa-user-plus"></i> Missing owner! Click to assign</a>
                @endif
            </td>
            <td class="text-center font-semi-large">
                {{ $owner->total_listed_room }}
            </td>
            <td class="text-center">
                {{ date('d M Y', strtotime($owner->booking_activation_date)) }}
                <br>
                {{ $owner->expiration_date }}
            </td>
            <td class="text-center font-semi-large">
                {{ $owner->total_booking_response }}
            </td>
            <td class="text-center font-semi-large">
                {{ $owner->loyalty_booking_response }}
            </td>
            <td class="text-center font-semi-large">
                {{ $owner->total_booking_transaction }}
            </td>
            <td class="text-center font-semi-large">
                {{ $owner->loyalty_booking_response }}
            </td>
            <td class="text-center font-semi-large">
                {{ $owner->total_review }}
            </td>
            <td class="text-center font-semi-large">
                {{ $owner->loyalty_booking_response }}
            </td>
            <td class="text-center font-semi-large">
                <strong>{{ $owner->total }}</strong> <i class="fa fa-trophy text-muted"></i>
            </td>
            <td class="text-center">
                @if ($owner->importer)
                {{ $owner->importer->name }}
                <br><small>{{ $owner->importer->role }}</small>
                @else
                -
                @endif
            </td>
            <td class="table-action-column" style="padding-right:15px!important;">
                <div class="btn-group">
                    <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-cog"></i> Actions
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu pull-right">
                        @if (is_null($owner->user))
                        <li>
                            <a href="#" class="actions" data-action="assign" data-owner="{{ $owner }}"><i
                                    class="fa fa-user-plus"></i>Assign Owner</a>
                        </li>
                        @endif
                        <li>
                            <a href="#" class="actions" data-action="remove" data-owner="{{ $owner }}"><i
                                    class="fa fa-trash"></i> Hapus Data</a>
                        </li>
                    </ul>
                </div>
            </td>
        </tr>
        @endforeach
        @endif
    </tbody>

</table>