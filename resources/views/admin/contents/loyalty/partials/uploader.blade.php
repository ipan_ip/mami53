<!-- Modal -->
<div class="modal" id="modal" role="dialog" aria-labelledby="modalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <form id="uploadForm" action="/admin/loyalty/import" enctype="multipart/form-data" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top:2px;"><i
                            class="fa fa-lg fa-times-circle"></i></button>
                    <h4 class="modal-title" id="modalTitle"></h4>
                </div>
                <div class="modal-body">
                    <div id="dropzone" class="dropzone text-center"></div>
                    <div style="padding-top:20px;"><strong>Perhatian!</strong>
                        <br/><i class="fa fa-thumb-tack fa-xs"></i>&nbsp;Pastikan jenis dokumen adalah CSV. <a href="https://www.ablebits.com/office-addins-blog/2014/04/24/convert-excel-csv/" target="_blank">Klik disini</a> untuk info lanjut
                        <br/><i class="fa fa-thumb-tack fa-xs"></i>&nbsp;Ukuran dokumen maksimum adalah 1 MB
                        <br/><i class="fa fa-thumb-tack fa-xs"></i>&nbsp;Maksimal 1 dokumen untuk sekali impor
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="uploaderSubmit"><i class="fa fa-refresh"></i>&nbsp;Process Files</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /. modal -->