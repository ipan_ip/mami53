@extends('admin.layouts.main')

@section('style')
<style>
    .table>tbody>tr>td {
        vertical-align: middle;
    }

    .font-large {
        font-size: 1.5em;
    }

    .font-semi-large {
        font-size: 1.15em;
    }

    .font-grey {
        color: #9E9E9E;
    }

    .dropdown-menu>li>a:hover {
        color: #333
    }

    .label-grey {
        background-color: #CFD8DC;
    }

    [hidden] {
        display: none !important;
    }

    /* Select2 tweak */

    .input-group .select2-container,
    .form-group .select2-container {
        position: relative !important;
        z-index: 2;
        float: left !important;
        width: 100% !important;
        margin-bottom: 0 !important;
        display: table !important;
        table-layout: fixed !important;
    }

    /* Sweetalert */

    .swal-shown {
        height: auto !important;
    }

    .custom-swal {
        z-index: 10000 !important;
    }

    .custom-wide-swal {
        width: 640px !important;
        font-size: 13px !important;
    }

    /* Custom table row */
    tr.missing-user td {
        background-color: #E6C8C8;
    }

    /* Modal tweak */

    .modal-open {
        overflow: hidden;
        position: fixed;
        width: 100%;
    }


    /* Centering the modal */

    .modal {
        text-align: center;
        padding: 0 !important;
    }

    .modal:before {
        content: '';
        display: inline-block;
        height: 100%;
        vertical-align: middle;
        margin-right: -4px;
    }

    .modal-dialog {
        display: inline-block;
        text-align: left;
        vertical-align: middle;
    }

    /* Dropzone tweaks */
    .dz-progress {
        /* progress bar covers file name */
        display: none !important;
    }

    .dz-remove
    {
        display:inline-block !important;
        width:1.2em;
        height:1.2em;
        position:absolute;
        top:5px;
        right:5px;
        z-index:1000;
        font-size:1.2em !important;
        line-height:1em;
        text-align:center;
        font-weight:bold;
        border-radius:1.2em;
    }

    .dz-remove:hover
    {
        text-decoration:none !important;
        opacity:1;
        cursor: pointer !important;
    }

    .dz-error-mark svg g g {
        fill: #c00;
    }
</style>
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" />
<link href="//cdn.jsdelivr.net/npm/select2-bootstrap-theme@0.1.0-beta.10/dist/select2-bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/dropzone@5.5.1/dist/min/dropzone.min.css">
@endsection

@section('content')
<!-- table -->
<div class="box box-default">
    <!-- Box Header -->
    <div class="box-header">
        <div class="col-md-12 text-center">
            <h3 class="box-title"><i class="fa fa-trophy"></i> {{ $boxTitle }}</h3>
        </div>
    </div>
    <!-- /.box-header -->

    <div class="box-body no-padding">
        {{-- Top Bar --}}
        <div class="horizontal-wrapper">
            <div class="btn-horizontal-group bg-default form-inline" style="padding-left: 10px;padding-right: 10px;">
                <!-- Add button -->
                <div class="col-md-6 bg-default" style="padding-bottom:10px;">
                    <button class="btn btn-success actions" data-action="import">
                        <i class="fa fa-upload"></i>&nbsp;Import Data
                    </button>
                </div>
                <!-- Search filters -->
                <div class="col-md-6 bg-default text-right inline-form" style="padding-bottom:10px">
                    <form action="" method="get">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control input-md" placeholder="Type owner phone number" autocomplete="off" value="{{ request()->input('q') }}" style="width:200px;margin-left:20px;">
                            <span class="input-group-btn">
                                <button type="submit" name="search" id="buttonSearch" class="btn btn-primary"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        {{-- Table --}}
        @include('admin.contents.loyalty.partials.table')

        {{-- Uploader Modal --}}
        @include('admin.contents.loyalty.partials.uploader')

    </div><!-- /.box-body -->
    {{-- pagination --}}
    @if (count($rowsOwner))
    <div class="box-body text-center">
        {{ $rowsOwner->appends(Request::except('page'))->links() }}
    </div>
    @endif
    {{-- /.pagination --}}
</div><!-- /.box -->
<!-- table -->
@endsection

@section('script')
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/dropzone@5.5.1/dist/min/dropzone.min.js"></script>
<script type="text/javascript">
    // Sweetalert functions
    function triggerAlert(type, message) {
        Swal.fire({
            type: type,
            customClass: {
                container: 'custom-swal'
            },
            html: message
        });
    }

    function triggerLoading(message) {
        Swal.fire({
            title: message,
            customClass: {
                container: 'custom-swal'
            },
            allowEscapeKey: false,
            allowOutsideClick: false,
            onOpen: () => {
                swal.showLoading();
            }
        });
    }

    // disable auto discover
    Dropzone.autoDiscover = false;

    $(function(){
        var token = '{{ csrf_token() }}';

        // Helper variable
        var _owner;

        // Actions button listeners
        $('.actions').click((e) => {
            e.preventDefault();

            var action = $(e.currentTarget).data('action');

            if (action !== 'import') _owner = $(e.currentTarget).data('owner');

            // Import Data
            if (action == 'import') {
                $('#modal').modal('show');
            }

            // Assign owner
            else if (action == 'assign') {
                Swal.fire({
                    title: 'Assign Owner',
                    html: '<br/><select class="form-control" name="dataUser" id="dataUser"></select><br/>',
                    confirmButtonText: 'Submit',
                    showCancelButton: true,
                    customClass: 'custom-swal',
                    showLoaderOnConfirm: true,
                    onOpen: function () {
                        $('#dataUser').select2({
                            theme: "bootstrap",
                            placeholder: 'Ketik nama/phone number untuk mencari..',
                            language: {
                                searching: function() {
                                    return "Sedang mengambil data owner...";
                                },
                                inputTooShort: function() {
                                    return 'Masukkan minimal 3 karakter';
                                },
                                noResults: function(){
                                    return "Owner tidak ditemukan!";
                                }
                            },
                            ajax: {
                                url: '/admin/users/owner',
                                data: function (params) {
                                    return {
                                        search: params.term,
                                        page: params.page || 1
                                    }
                                },
                                processResults: function (data, params) {
                                    var page = params.page || 1;
                                    if (data.data.length < 1) var total_count = 0;
                                    else var total_count = data.data[0].total_count;
                                    return {
                                        results: $.map(data.data, item => { 
                                            return {
                                                id: item.id, 
                                                text: item.name + ' (' + item.phone_number + ')'
                                            }
                                        }),
                                        pagination: {
                                            more: (page * 10) <= total_count
                                        }
                                    };
                                },
                                cache: true
                            },
                            minimumInputLength: 3,
                            dropdownParent: $(".swal2-modal")
                        });
                    },
                    preConfirm: () => {
                        var owner = $('#dataUser').val();

                        // validation
                        if (owner == null) {
                            Swal.showValidationMessage("Pilih owner terlebih dahulu!");
                            return false;
                        } else {
                            return $.ajax({
                                type: 'POST',
                                url: "/admin/loyalty/update",
                                dataType: 'json',
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                data: {
                                    'id': _owner.id,
                                    'user_id': owner,
                                    '_token': token
                                },
                                success: (response) => {
                                    if (!response.success) {
                                        Swal.showValidationMessage(response.message);
                                    }

                                    return;
                                },
                                error: (error) => {
                                    Swal.showValidationMessage('Request Failed: ' + error.message);
                                }
                            });
                        }
                    },
                    allowOutsideClick: () => !Swal.isLoading()
                }).then((result) => {
                    if (!result.dismiss) {
                        if (result.value.success) {
                            Swal.fire({
                                type: 'success',
                                title: "Owner berhasil ditambahkan",
                                text: "Halaman akan di-refresh setelah Anda klik OK",
                                onClose: () => {
                                    window.location.reload();
                                }
                            });
                        } else {
                            Swal.fire({
                                type: 'error',
                                title: "Failed!",
                                html: '<strong>' + result.value.message + '</strong>'
                            });
                        }
                    }
                });
            }

            // Removing data
            else if (action == 'remove') {
                Swal.fire({
                    title: 'Hapus Data Owner Loyalty?',
                    type: 'warning',
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-danger',
                    showCancelButton: true,
                    cancelButtonClass: 'btn btn-default',
                    showLoaderOnConfirm: true,
                    confirmButtonText: 'Yes, remove it!',
                    preConfirm: (val) => {
                        return $.ajax({
                            type: 'POST',
                            url: "/admin/loyalty/remove",
                            dataType: 'json',
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            data: {
                                'id': _owner.id,
                            },
                            success: (response) => {
                                return response;
                            },
                            error: (error) => {
                                triggerAlert('error', error.message);
                            }
                        });
                    },
                    allowOutsideClick: () => !Swal.isLoading()
                }).then((result) => {
                    var res = result.value;
                    if (res.success == true) {
                        Swal.fire({
                            type: 'success',
                            title: "Data telah berhasil dihapus",
                            text: "Halaman akan di-refresh setelah Anda klik OK",
                            onClose: () => {
                                window.location.reload();
                            }
                        });
                    } else {
                        triggerAlert('error', res.message);
                    }
                })
            }
        });

        // Uploader modal handler
        $('#modal')
        .on("show.bs.modal", (e) => {
            $('#modal').data('bs.modal').options.keyboard = false;
            $('#modal').data('bs.modal').options.backdrop = 'static';

            $('#modalTitle').html('<i class="fa fa-upload"></i> Import Data');

            // initiate Dropzone
            if (Dropzone.instances.length == 0) {
                var myDropzone = new Dropzone('#dropzone', {
                    url: '/admin/loyalty/import',
                    method: "POST", 
                    uploadMultiple: false,
                    paramName: "loyalty_file",
                    autoProcessQueue : false,
                    timeout: 300000, // 5 Mins
                    maxFilesize: 1, // 1 MB
                    maxFiles: 1,
                    acceptedFiles: ".csv",
                    addRemoveLinks: true,
                    thumbnailWidth: null,
                    thumbnailHeight: null,
                    dictRemoveFile : "<i class='fa fa-trash text-danger' style='cursor: pointer !important;'></i>",
                    dictFileTooBig: "File terlalu besar. Ukuran maksimal adalah 1 MB",
                    dictInvalidFileType: "Hanya file berjenis CSV yang diperbolehkan",
                    dictCancelUpload: "Cancel",
                    dictMaxFilesExceeded: "Maksimum upload 1 file",
                    dictDefaultMessage: "Klik atau drop dokumen di sini",
                    success : function (file, response) {
                        // 
                    },
                    error: function(file, errorMessage, xhr) {
                        if (xhr) var errTitle = "Error: [" + xhr.status + "] " + xhr.statusText;
                        else var err = "Failed importing files(s)";
                        
                        triggerAlert('error', "<h5>" + errTitle + "</h5>" + errorMessage.message);
                    },
                    init: function() {
                        this.on("maxfilesexceeded", function(file) {
                            this.removeFile(file);
                            triggerAlert('error', "<h5>Hanya diperbolehkan upload 1 file!</h5>");
                        });

                        this.on('sending', function(file, xhr) {
                            triggerLoading("Processing file(s)...");
                        });

                        this.on("success", function (file, response) {
                            if (response.success == true) {
                                $('#modal').modal('toggle');
    
                                Swal.fire({
                                    type: 'success',
                                    title: "Berhasil mengimpor data",
                                    text: "Halaman akan di-refresh setelah Anda klik OK",
                                    onClose: () => {
                                        window.location.reload();
                                    }
                                });
                            } else {
                                triggerAlert('error', response.message);
                            }
                        });

                        this.on("error", function (file, error, xhr) {
                            if(error.message == undefined) {
                                var errorMessage = error;
                            } else {
                                var errorMessage = error.message;
                            }
                            
                            triggerAlert('error', '<h4>Terjadi Error</h4><h5>Cek kembali dokumen yang anda upload</h5>' + errorMessage);
                        });
                    }
                }); 
            }

            // Submit handler
            $('#uploaderSubmit').click(e => {
                e.preventDefault();
                if (myDropzone.files.length == 0){
                    triggerAlert('error', "<h5>Pilih file terlebih dahulu!</h5>");
                } else if(myDropzone.getRejectedFiles().length > 0) {
                    triggerAlert('error', "<h5>Harap periksa kembali file yang bertanda <strong>silang</strong>!</h5>");
                } else {
                    myDropzone.processQueue();
                }
            });
            
        })
        .on("hidden.bs.modal", (e) => {
            // 
        });
    });
</script>
@endsection