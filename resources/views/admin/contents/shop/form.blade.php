@extends('admin.layouts.main')
@section('content')
<div class="box box-primary">
  <div class="box-header">
      <h3 class="box-title">{{ $boxTitle }}</h3>
  </div><!-- /.box-header -->
  {{ Form::open(array('route' => $formAction, 'method' => $formMethod,
      'class' => 'form-horizontal form-bordered', 'role' => 'form', 'id' => 'formInsertShop')) }}
      <div class="box-body no-padding">
            <div class="form-group bg-default">
              <label for="inputName" class="col-sm-2 control-label">Name</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" placeholder="Name"
                  id="inputName" name="name" value="{{ $rowShop->name }}">
              </div>
            </div>
            <!-- select -->
            <div class="form-group bg-default">
                <label for="inputSelectAreaId" class="col-sm-2 control-label">Select Area</label>
                <div class="col-sm-10">
                    <select class="form-control chosen-select" id="inputSelectAreaId" name="area_id"
                      data-placeholder="Choose an Area...">
                        @foreach ($rowsRegion as $rowRegion)
                        <optgroup label="{{ $rowRegion->name }}">
                          @foreach ($rowRegion->areas as $rowArea)
                            @if ($rowShop->area_id === $rowArea->id)
                            <option value="{{ $rowArea->id }}" selected="selected">{{ $rowArea->name }}</option>
                            @else
                            <option value="{{ $rowArea->id }}">{{ $rowArea->name }}</option>
                            @endif
                          @endforeach
                        </optgroup>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group bg-default">
              <label for="inputAddress" class="col-sm-2 control-label">Address</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" placeholder="Address"
                  id="inputAddress" name="address" value="{{ $rowShop->address }}">
              </div>
            </div>
            {{-- Google Maps --}}
            <div class="form-group bg-default">
              <label for="inputGeoName" class="col-sm-2 control-label">Maps Coordinates</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" placeholder="Coordinate"
                  id="inputGeoName" name="geo_name">
              </div>
              <div class="col-sm-10 col-sm-offset-2">
                <div class="wrapper-map-canvas">
                  <div id="map-canvas"></div>
                </div>
              </div>
              <div class="col-sm-10 col-sm-offset-2">
                  <div class="input-group">
                    <div class="input-group-addon">Coordinates (lat,long)</div>
                    <input type="text" class="form-control" placeholder="Name"
                    id="inputCoordinate" name="coordinate" value="{{ $rowShop->coordinate }}">
                  </div>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="{{ URL::route('admin.shop.index') }}">
                  <button type="button" class="btn btn-default">Cancel</button>
                </a>
                <button type="reset" class="btn btn-default"
                  onclick="$('#formInsertTag').bootstrapValidator('resetForm', true);">Reset</button>
              </div>
            </div>
      </div>
  {{ Form::close() }}<!-- /.form -->
</div><!-- /.box -->
@stop
@section('style')
{{ HTML::style('assets/vendor/jquery-chosen/chosen.css') }}
{{ HTML::style('assets/vendor/jquery-chosen-custom/chosen.css') }}
@stop
@section('script')
{{ HTML::script('assets/vendor/jquery-chosen/chosen.jquery.js') }}
<script type="text/javascript">
  $(function()
  {
    var config = {
      '.chosen-select'           : {width: '100%'}
    };
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    }
  });
</script>
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&language=id&key={{ENV('MIX_GOOGLE_MAPS_JAVASCRIPT_API_KEY')}}"></script>
{{ HTML::script('assets/vendor/geocomplete/jquery.geocomplete.js') }}
{{ HTML::script('assets/vendor/google-maps/index.js') }}
<script type="text/javascript">
  var inputGeoName = 'inputGeoName';
  var inputLatLngName = 'inputCoordinate';

  $(function () {
    google.maps.event.addDomListener(window, 'load', function () {
      var input = document.getElementById(inputLatLngName).value;
      if (input !== '') {
        var latlngStr = input.split(',', 2);
        var lat = parseFloat(latlngStr[0]);
        var lng = parseFloat(latlngStr[1]);

        initialize('predefined', lat, lng);
      } else {
        initialize();
      }
      
    });
  });
</script>
<!-- page script -->
<script type="text/javascript">

    @if (Session::get('laravelValidatorJSON') === null)
      myGlobal.laravelValidator = null;
    @else
      myGlobal.laravelValidator = {{ Session::get('laravelValidatorJSON') }};
    @endif

    $(function()
    {
      $formInsertShop = $('#formInsertShop');

      formStoreShopRules = {
          name: {
              message: 'The name is not valid',
              validators: {
                  notEmpty: {
                      message: 'The name is required and cannot be empty'
                  },
                  stringLength: {
                      min: 6,
                      max: 30,
                      message: 'The name must be more than 6 and less than 30 characters long'
                  }
              }
          },
          address: {
              message: 'The address is not valid',
              validators: {
                  notEmpty: {
                      message: 'The address is required and cannot be empty'
                  },
                  stringLength: {
                      min: 6,
                      max: 50,
                      message: 'The address must be more than 6 and less than 50 characters long'
                  }
              }
          },
          coordinate: {
              message: 'The Coordinates is not valid',
              validators: {
                  notEmpty: {
                      message: 'The Coordinates is required and cannot be empty'
                  }
              }
          },
          'area_id': {
              feedbackIcons: 'false',
              message: 'The area is not valid',
              validators: {
                  callback: {
                      message: 'Please choose an Area',
                      callback: function(value, validator) {
                          // Get the selected options
                          var options = validator.getFieldElements('area_id').val();
                          return (options != null && options.length >= 1);
                      }
                  }
              }
          },
      };

      formUpdateShopRules = {
          name: {
              message: 'The name is not valid',
              validators: {
                  notEmpty: {
                      message: 'The name is required and cannot be empty'
                  },
                  stringLength: {
                      min: 6,
                      max: 30,
                      message: 'The name must be more than 6 and less than 30 characters long'
                  }
              }
          },
          address: {
              message: 'The address is not valid',
              validators: {
                  notEmpty: {
                      message: 'The address is required and cannot be empty'
                  },
                  stringLength: {
                      min: 6,
                      max: 50,
                      message: 'The address must be more than 6 and less than 50 characters long'
                  }
              }
          },
          'area_id': {
              feedbackIcons: 'false',
              message: 'The area is not valid',
              validators: {
                  callback: {
                      message: 'Please choose an Area',
                      callback: function(value, validator) {
                          // Get the selected options
                          var options = validator.getFieldElements('area_id').val();
                          return (options != null && options.length >= 1);
                      }
                  }
              }
          },
          coordinate: {
              message: 'The Coordinates is not valid',
              validators: {
                  notEmpty: {
                      message: 'The Coordinates is required and cannot be empty'
                  }
              }
          },
      };

      @if (Route::currentRouteName() === 'admin.shop.create')
        myGlobal.bootstrapValidatorDefaults.fields = formStoreShopRules;
      @elseif (Route::currentRouteName() === 'admin.shop.edit')
        myGlobal.bootstrapValidatorDefaults.fields = formUpdateShopRules;
      @endif

      myGlobal.bootstrapValidatorDefaults.excluded = ':disabled';

      $formInsertShop
        .find('[name="area_id"]')
            .chosen()
            // Revalidate the color when it is changed
            .change(function(e) {
                $formInsertDesigner.bootstrapValidator('revalidateField', 'area_id');
            })
            .end()
        .bootstrapValidator(myGlobal.bootstrapValidatorDefaults);


      $formInsertShop.bootstrapValidator(myGlobal.bootstrapValidatorDefaults);

      updateValidateStatus($formInsertShop.data('bootstrapValidator'), myGlobal.laravelValidator);
    });
</script>
@stop