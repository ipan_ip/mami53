@extends('admin.layouts.main')
@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
            <div class="box-tools pull-right">
                <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <a href="{{ URL::route('admin.shop.create') }}">
                        <button type="submit" class="btn-add btn btn-primary btn-sm">
                            <i class="fa fa-plus">&nbsp;</i>Add Shop</button>
                    </a>
                </div>
            </div>

            <table id="tableListTag" class="table table-striped">
                <thead>
                    <tr>
                        <th width="30px;">No</th>
                        <th>{{ Lang::get('hairclick.page.shop.name') }}</th>
                        <th>{{ Lang::get('hairclick.page.shop.address') }}</th>
                        <th>{{ Lang::get('hairclick.page.shop.coordinates') }}</th>
                        <th>{{ Lang::get('hairclick.page.shop.area') }}</th>
                        <th class="table-action-column" width="100px">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($rowsShop as $rowShopKey => $rowShop)
                    <tr>
                        <td>{{ $rowShopKey+1 }}</td>
                        <td>{{ $rowShop['name'] }}</td>
                        <td>{{ $rowShop['address'] }}</td>
                        <td>{{ $rowShop['latitude'] }}, {{ $rowShop['longitude'] }}</td>
                        <td>{{ $rowShop['area']['name'] }}</td>
                        <td class="table-action-column">
                            <div class="btn-action-group">
                                <a href="{{ URL::route('admin.shop.edit', $rowShop->id) }}">
                                    <i class="fa fa-pencil"></i></a>
                                <a href="#" ng-click="showConfirmDelete('Shop', {{ $rowShop->id }}, '{{ $rowShop->name }}');">
                                    <i class="fa fa-trash-o"></i></a>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th width="30px;">No</th>
                        <th>{{ Lang::get('hairclick.page.shop.name') }}</th>
                        <th>{{ Lang::get('hairclick.page.shop.address') }}</th>
                        <th>{{ Lang::get('hairclick.page.shop.coordinates') }}</th>
                        <th>{{ Lang::get('hairclick.page.shop.area') }}</th>
                        <th class="table-action-column" width="100px">Action</th>
                    </tr>
                </tfoot>
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
    <!-- table -->

    @include('admin.partials.delete-modal')
@stop
@section('script')
<!-- page script -->
<script type="text/javascript">
    $(function()
    {
        $("#tableListTag").dataTable();
    });
</script>
@stop