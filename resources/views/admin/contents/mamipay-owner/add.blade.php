@extends('admin.layouts.main')

@section('content')
    <!-- table -->
  
    <div class="box box-primary">
        
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
            <div class="box-tools pull-right">
                <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
            </div>
        </div><!-- /.box-header -->

        <div class="box-body">
           <form method="POST" action="" style="width:250px">
               @csrf
                <div class="form-group">
                    <label>Owner Phone Number</label>
                    <input class="form-control" name="owner_phone_number" type="text">
                </div>
                <div class="form-group">
                    <label>Owner Name</label>
                    <input class="form-control" name="owner_name" type="text">
                </div>
                <div class="form-group">
                    <label>Account Name</label>
                    <input class="form-control" name="bank_account_owner" type="text">
                </div>
                <div class="form-group">
                    <label>Bank Name</label>
                    <select class="form-control" name="bank_name">
                        <option value="">- Please Select -</option>
                        @foreach ($bankList as $bank)
                            <option value="{{$bank->bank_code}}">{{$bank->bank_name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Bank City</label>
                    <input class="form-control" name="bank_account_city" type="text">
                </div>
                <div class="form-group">
                    <label>Account Number</label>
                    <input class="form-control" name="bank_account_number" type="text">
                </div>                
                <div class="form-group">
                    <input class="btn btn-info" type="submit" value="Add"/>
                </div>
           </form>
        </div><!-- /.box-body -->
        
    </div><!-- /.box -->
@endsection

@section('script')
<script type="text/javascript">
    $('[name="bank_account_city"]').autocomplete({
        source: function( request, response ) {
            $.ajax({
                url: "/admin/mamipay-owner/search-city",
                dataType: "json",
                data: {
                    city_name: request.term
                },
                success: function(data) {
                    response(data);
                }
            });
        },
        minLength: 2
    });
</script>
@endsection
