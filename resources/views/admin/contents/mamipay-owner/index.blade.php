@extends('admin.layouts.main')

@section('style')
<style>
    .reject-remark-filled i.fa {
        color: #ff0000;
    }
</style>
@endsection

@section('content')
    <!-- table -->
    <p><a href="/admin/mamipay-owner/add" class="btn btn-success">Tambah Owner</a></p>
    <div class="box box-primary">
        
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
            <div class="box-tools pull-right">
                <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
            </div>
        </div><!-- /.box-header -->

        <div class="box-body no-padding">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Nama</th>
                        <th>No Handphone</th>
                        <th>Owner ID</th>
                        <th>Jumlah Room</th>
                        <th>Nama Bank</th>
                        <th>Nomor Akun Bank</th>
                        <th>Nama Akun Bank</th>                        
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($rowsOwner as $index => $rowOwner)
                        <tr>
                            <td>@if(!empty($rowOwner->user->name))<a target="_blank" href="/admin/owner?q=&owner_name={{ $rowOwner->user->name }}">{{ $rowOwner->user->name }}</a>@endif</td>
                            <td>@if(!empty($rowOwner->user->phone_number))<a target="_blank" href="/admin/owner?q=&phone_owner={{ $rowOwner->user->phone_number }}">{{ $rowOwner->user->phone_number }}</a>@endif</td>
                            <td>@if(!empty($rowOwner->user->id)){{ $rowOwner->user->id }}@endif</td>
                            <td>{{ $rowOwner->room_count }}</td>
                            <td>{{ $rowOwner->bank_name }}</td>
                            <td>{{ $rowOwner->bank_account_number }}</td>
                            <td>{{ $rowOwner->bank_account_owner }}</td>                       
                            <td>
                                @if($rowOwner->status=='approved')
                                    <span class="label label-success">Approved</span>
                                @elseif($rowOwner->status=='pending')
                                    <span class="label label-warning">Pending</span>
                                @else
                                    <span class="label label-danger">Rejected</span>
                                @endif                                
                            </td>
                            <td class="table-action-column" style="text-align:left;">
                                <div class="btn-action-group"><!--/admin/mamipay-owner/reject/{{ $rowOwner->id}}-->
                                    @if($rowOwner->status=='approved')
                                        <a href="javascript:reject({{ $rowOwner->id}},{{ $rowOwner->user->id}});" title="Reject"><i class="fa fa-ban"></i></a>
                                    @elseif($rowOwner->status=='pending')
                                        <a href="/admin/mamipay-owner/approve/{{ $rowOwner->id}}" title="Approve"><i class="fa fa-check"></i></a>&nbsp;&nbsp;<a href="/admin/mamipay-owner/reject/{{ $rowOwner->id}}" title="Reject"><i class="fa fa-ban"></i></a>
                                    @else
                                        <a href="/admin/mamipay-owner/approve/{{ $rowOwner->id}}" title="Approve"><i class="fa fa-check"></i></a>
                                    @endif 
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div><!-- /.box-body -->
        <div class="box-body no-padding">
            {{ $rowsOwner->appends(Request::except('page'))->links() }}
        </div>

        <script type="text/javascript">
            function reject(oid,uid){
                if (confirm('Apakah Anda yakin reject owner '+uid+'?')) {
                    window.location.href='/admin/mamipay-owner/reject/'+oid;
                } else {
                    return false;
                }
            }
        </script>
    </div><!-- /.box -->
@endsection
