@extends('admin.layouts.main')

@section('style')
<style>
    .reject-remark-filled i.fa {
        color: #ff0000;
    }
</style>
@endsection

@section('content')
    <!-- table -->
    <div class="box box-primary">

        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
            <div class="box-tools pull-right">
                <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
            </div>
        </div><!-- /.box-header -->
        
        <div class="box-body no-padding">
            <form action="" class="form-inline" autocomplete="off" method="get" style="text-align:right;padding:15px; background-color:#f4f4f4;">
                <div class="form-group">
                    <label>Tanggal Invoice : </label>
                    <input type="text" class="form-control datepicker" value="@if(!empty($activeFilter['paid_from'])){{$activeFilter['paid_from']}}@endif" name="paid_from" placeholder="From">
                    <input type="text" class="form-control datepicker" value="@if(!empty($activeFilter['paid_to'])){{$activeFilter['paid_to']}}@endif" name="paid_to" placeholder="To">
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary btn-md" value="Filter"/>
                    <button class="btn btn-secondary btn-md" type="reset" onclick="window.location = window.location.pathname;">Reset</button>
                </div>
            </form>
            <form action="" class="form-inline" autocomplete="off" method="post" style="text-align:right;padding:5px 15px;background-color: #ffa500;">
                <div class="form-group">
                    <select name="csv_type" class="form-control">
                        <option value="">- Pilih Type CSV -</option>
                        <option value="flip">Big Flip</option>
                    </select>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary btn-md" value="Download CSV"/>
                </div>
            </form>
        </div>
        
        <div class="box-body no-padding">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Invoice Number</th>
                        <th>Paid At</th>
                        <th>Total Paid Amount</th>
                        <th>Transfer Status</th>
                        <th>Transfer At</th>
                        <th>Transfer Amount</th>
                        <th>Bank Account</th>
                        <th>Bank Account Number</th>
                        <th>Bank Account Owner</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($rows as $index => $row)
                        <tr>
                            <td>{{ $index+1 }}</td>
                            <td>{{ $row['invoice_number'] }}</td>
                            <td>{{ $row['paid_at'] }}</td>
                            <td>{{ $row['total_paid_amount'] }}</td>
                            <td>{{ $row['transfer_status'] }}</td>
                            <td>{{ $row['transfer_at'] }}</td>
                            <td>{{ $row['transfer_amount'] }}</td>
                            <td>{{ $row['bank_name'] }}</td>
                            <td>{{ $row['bank_account_number'] }}</td>
                            <td>{{ $row['bank_account_owner'] }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div><!-- /.box-body -->
        <div class="box-body no-padding">
            {{ $rowsEntity->appends(Request::except('page'))->links() }}
        </div>

        <script type="text/javascript">
            $('.datepicker').datepicker({
                dateFormat:'yy-mm-dd'
            });
        </script>
    </div><!-- /.box -->
@endsection
