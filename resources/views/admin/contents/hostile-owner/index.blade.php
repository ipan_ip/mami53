@extends('admin.layouts.main')

@section('style')
<link rel="stylesheet" href="/css/admin-consultant.css">

<style>
    .reject-remark-filled i.fa {
        color: #ff0000;
    }
    .table > tbody > tr > td {
        vertical-align: middle;
    }
    .font-large {
        font-size: 1.5em;
    }
    .font-semi-large {
        font-size: 1.15em;
    }
    .font-grey {
        color: #9E9E9E;
    }
    .dropdown-menu > li > a:hover {
        color: #333
    }
    .label-grey {
        background-color: #CFD8DC;
    }

    /* Centering the modal */
    .modal {
        text-align: center;
        padding: 0!important;
    }

    .modal:before {
        content: '';
        display: inline-block;
        height: 100%;
        vertical-align: middle;
        margin-right: -4px;
    }

    .modal-dialog {
        display: inline-block;
        text-align: left;
        vertical-align: middle;
    }

    /* Sweetalert spinner tweak */
    .swal2-loading::after {
        width: 20px !important;
        height: 20px !important;
        border-left-color: #fff !important;
        border-top-color: #fff !important;
        border-bottom-color: #fff !important;
    }

    .custom-input {
        text-align: center !important;
        font-size: 1.5em !important;
        font-family: 'Menlo' !important;
    }

    .table-container {
        overflow-x: auto;
        border: 10px solid transparent;
    }

    .table {
        table-layout: fixed;
    }

    .table th:nth-child(1) {
        width: 40px;
    }

    .table th:nth-child(2) {
        width: 90px;
    }

    .table th:nth-child(3) {
        width: 170px;
    }

    .table th:nth-child(4) {
        width: 240px;
    }

    .table th:nth-child(5) {
        width: 120px;
    }

    .table th:nth-child(6) {
        width: 200px;
    }

    .table th:nth-child(7) {
        width: 110px;
    }

    .table th:nth-child(8) {
        width: 135px;
    }

    .table th:nth-child(9) {
        width: 135px;
    }

    .table th:nth-child(10) {
        width: 125px;
    }

    .table th:nth-child(11) {
        width: 220px;
    }

    .swal2-cancel.btn {
        margin-left: 15px;
    }

</style>
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
@endsection

@section('content')
    <div class="box box-primary">

        <div class="box-body no-padding">

            <!-- Top bar -->
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default form-inline" style="padding-left: 15px;padding-right: 15px;">
                    <div class="row">
                        <!-- Add button -->
                        <div class="col-md-3 bg-default" style="padding-bottom:10px;">
                            <a href="#" class="btn btn-primary btn-md" id="action-add"><i class="fa fa-plus">&nbsp;</i>Add Data</a>
                        </div>
                        <!-- Search filters -->
                        <div class="col-md-9 bg-default" style="padding-bottom:10px;text-align:right">
                            <form action="" method="get">
                                <input type="text" name="owner_name" class="form-control input-sm" placeholder="Nama Owner" autocomplete="off" value="{{ request()->input('owner_name') }}">
                                <input type="text" name="owner_phone_number" class="form-control input-sm" placeholder="No. Telepon" autocomplete="off" value="{{ request()->input('owner_phone_number') }}">
                                <input type="text" name="owner_email" class="form-control input-sm" placeholder="Email" autocomplete="off" value="{{ request()->input('owner_email') }}" style="margin-right:10px">
                                <button class="btn btn-primary btn-md" id="buttonSearch"><i class="fa fa-search">&nbsp;</i>Search
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <!-- table -->
            <div class="table-container">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th class="text-center"></th>
                            <th class="text-center">ID</th>
                            <th>Nama</th>
                            <th>Email</th>
                            <th class="text-center">No. Telepon</th>
                            <th class="text-center">Total Kos/Apt</th>
                            <th class="text-center">Hostility Score</th>
                            <th class="text-center">Created By / At</th>
                            <th class="text-center">Updated By / At</th>
                            <th class="text-center">Hostile Reason</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {{-- {{ dd($rowsOwner) }} --}}
                        @foreach($rowsOwner as $index => $rowOwner)
                            <tr>
                                <td class="text-center">{{ $index+1 }}</td>
                                <td class="text-center">{{ $rowOwner->id }}</td>
                                <td>
                                    <strong class="font-semi-large">{{ ($rowOwner->name == '') ? '-' : $rowOwner->name }}</strong>
                                </td>
                                <td>{{ is_null($rowOwner->email) ? "-" : $rowOwner->email }}</td>
                                <td class="text-center">{{ is_null($rowOwner->phone_number) ? "-" : $rowOwner->phone_number }}</td>
                                <td class="text-center">
                                    <div class="row">
                                        <div class="col-md-6 text-right"><small>Verified</small><br/><span class="font-semi-large">{{ $rowOwner->verified_room_count }}</span></div>
                                        <div class="col-md-6 text-left"><small>Unverified</small><br/><span class="font-semi-large">{{ $rowOwner->total_room_count -  $rowOwner->verified_room_count }}</span></div>
                                    </div>
                                </td>
                                <td class="text-center">
                                <strong>
                                @if($rowOwner->hostility >= 75)
                                    <span class="font-large text-danger">{{ $rowOwner->hostility }}</span>
                                @elseif($rowOwner->hostility >= 50)
                                    <span class="font-large text-warning">{{ $rowOwner->hostility }}</span>
                                @else
                                    <span class="font-large text-default">{{ $rowOwner->hostility }}</span>
                                @endif
                                </strong>
                                </td>

                                <td style="text-align: center;">
                                    <div style="margin-bottom: 4px;">
                                        {{ !is_null($rowOwner->hostility_reason) ? $rowOwner->hostility_reason->creator->name : '-' }}
                                    </div>

                                    <div>
                                        {{ !is_null($rowOwner->hostility_reason) ? $rowOwner->hostility_reason->created_at->toDateString() : '-' }}
                                    </div>
                                </td>

                                <td style="text-align: center;">
                                    <div style="margin-bottom: 4px;">
                                        {{ !is_null($rowOwner->hostility_reason) ? $rowOwner->hostility_reason->updater->name : '-' }}
                                    </div>

                                    <div>
                                        {{ !is_null($rowOwner->hostility_reason) ? $rowOwner->hostility_reason->updated_at->toDateString() : '-' }}
                                    </div>
                                </td>

                                <td style="text-align: center;">
                                    <div>
                                        @if (!is_null($rowOwner->hostility_reason))
                                            @if ($rowOwner->hostility_reason->reason === 'fraud')
                                            Fraud
                                            @elseif ($rowOwner->hostility_reason->reason === 'competitor')
                                            Mamikos competitor
                                            @elseif ($rowOwner->hostility_reason->reason === 'not_paying_gp')
                                            Payment issue
                                            @endif
                                        @else
                                            -
                                        @endif
                                    </div>
                                </td>

                                <td class="table-action-column" style="padding-right:15px!important;">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                            <i class="fa fa-cogs"></i> Actions
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu pull-right">
                                            @if ($rowOwner->room_count > 0)
                                            <li>
                                                <a href="{{ URL::to('admin/hostile-owner/view', $rowOwner->id) }}" class="action-view" data-name="{{ $rowOwner->name }}"><i class="fa fa-eye"></i> Lihat Kos/Apt</a>
                                            </li>
                                            @endif
                                            <li>
                                                <a href="#" class="actions" data-action="update" data-owner="{{ $rowOwner }}"><i class="fa fa-repeat"></i> Ubah Score</a>
                                            </li>

                                            @if (!is_null($rowOwner->hostility_reason))
                                            <li>
                                                <a href="#" class="actions" data-action="update-hostile-reason" data-owner="{{ $rowOwner }}"><i class="fa fa-pencil"></i> Update Hostile Reason</a>
                                            </li>
                                            @else
                                            <li>
                                                <a href="#" class="actions" data-action="add-hostile-reason" data-owner="{{ $rowOwner }}"><i class="fa fa-plus"></i> Add Hostile Reason</a>
                                            </li>
                                            @endif

                                            <li>
                                                <a href="#" class="actions" data-action="reset" data-owner="{{ $rowOwner }}"><i class="fa fa-ban"></i> Reset Score</a>
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- table -->

        </div><!-- /.box-body -->
        <div class="box-body no-padding" style="padding-left:10px;">
            {{ $rowsOwner->appends(Request::except('page'))->links() }}
        </div>
    </div><!-- /.box -->
@endsection

@section('script')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

    <script type="text/javascript">
        $(function(){
            var token = '{{ csrf_token() }}';
            var _owner;
            const inputValue = {
                id: '',
                score: '',
                reason: ''
            };

            function reasonName(string) {
                switch (string) {
                    case 'fraud':
                        return 'Fraud';
                    case 'competitor':
                        return 'Mamikos competitor';
                    case 'not_paying_gp':
                        return 'Payment issue';
                    default:
                        return 'Other reason';
                }
            };

            $('#action-add').click((e) => {
                e.preventDefault();

                // reset _ownerId first
                _ownerId = null;
                inputValue.id = '';
                inputValue.score = '';
                inputValue.reason = '';

                Swal.mixin({
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-primary',
                    progressSteps: ['1', '2', '3'],
                    allowOutsideClick: () => !Swal.isLoading()
                })
                .queue([{
                    // first step
                    title: 'Masukkan ID owner',
                    input: 'text',
                    inputPlaceholder: '123456',
                    inputClass: 'custom-input',
                    confirmButtonText: 'Next &rarr;',
                    showCancelButton: true,
                    cancelButtonClass: 'btn',
                    showLoaderOnConfirm: true,
                    inputValidator: (value) => {
                        return new Promise((resolve) => {
                            if (value !== '') {
                                resolve();
                            } else {
                                resolve('ID owner tidak boleh kosong');
                            }
                        })
                    },
                    preConfirm: (val) => {
                        return axios.get(`/admin/hostile-owner/${val}`)
                        .then(response => {
                            if (!response.data.success) {
                                Swal.showValidationMessage(response.data.message);
                                Swal.hideLoading();
                            } else {
                                _owner = response.data.data;
                                inputValue.id = response.data.data.id;
                            }
                        })
                        .catch(error => {
                            Swal.showValidationMessage('Error: ' + error.message);
                            Swal.hideLoading();
                        });
                    },
                    allowOutsideClick: () => !Swal.isLoading()
                }, {
                    // second step
                    title: 'Set Hostility Score',
                    text: 'Score maksimum adalah 100',
                    input: 'text',
                    inputClass: 'custom-input',
                    confirmButtonText: 'Next &rarr;',
                    showCancelButton: true,
                    cancelButtonClass: 'btn',
                    inputValidator: (value) => {
                        return new Promise((resolve) => {
                            if (value === '' || +value < 0 || +value > 100 || isNaN(+value)) {
                                resolve('Score min 0 dan maks 100');
                            } else {
                                resolve();
                            }
                        })
                    },
                    preConfirm: (val) => {
                        inputValue.score = parseInt(val);
                    }
                }, {
                    // third step
                    title: 'Tambah Hostile Reason',
                    input: 'select',
                    inputOptions: {
                        'fraud': 'Indikasi fraud',
                        'competitor': 'Indikasi kompetitor',
                        'not_paying_gp': 'Tidak bayar minimum charging GP'
                    },
                    inputPlaceholder: 'Pilih alasan hostile',
                    confirmButtonText: '&#10004; Set',
                    showCancelButton: true,
                    cancelButtonClass: 'btn',
                    showLoaderOnConfirm: true,
                    preConfirm: (val) => {
                        inputValue.reason = val;

                        return axios.post('/admin/hostile-owner', {
                            'id': inputValue.id,
                            'score': inputValue.score,
                            'reason': inputValue.reason,
                            '_token': token
                        })
                        .then(response => {
                            if (!response.data.success) {
                                Swal.showValidationMessage(response.data.message);
                                Swal.hideLoading();
                            }
                        })
                        .catch(error => {
                            Swal.showValidationMessage('Error: ' + error.message);
                            Swal.hideLoading();
                        });
                    },
                    allowOutsideClick: () => !Swal.isLoading()
                }])
                .then(result => {
                    if (result.value && result.value.length === 3) {
                        Swal.fire({
                            type: 'success',
                            title: `Berhasil set hostility score pada owner ${_owner.name}`,
                            text: `Halaman akan di-refresh setelah Anda klik OK`,
                            onClose: () => {
                                window.location.reload();
                            }
                        });
                    }
                });
            });


            // update & reset
            $('.actions').click((e) => {
                e.preventDefault();
                var action = $(e.currentTarget).data('action');
                _owner = $(e.currentTarget).data('owner');

                let newValue = '';

                // --------------------------------------------------------------------------------
                // Updating score
                if (action === 'update') {
                    Swal.fire({
                        title: 'Set Hostility Score Baru',
                        text: 'Score maksimum adalah 100',
                        input: 'number',
                        inputValue: _owner.hostility,
                        inputClass: 'custom-input',
                        buttonsStyling: false,
                        confirmButtonClass: 'btn btn-primary',
                        confirmButtonText: '&#10004; Save',
                        showCancelButton: true,
                        cancelButtonClass: 'btn',
                        showLoaderOnConfirm: true,
                        inputValidator: (value) => {
                            return new Promise((resolve) => {
                                if (value === '' || +value < 0 || +value > 100 || isNaN(+value)) {
                                    resolve('Score min 0 dan maks 100');
                                } else {
                                    resolve();
                                }
                            })
                        },
                        preConfirm: (val) => {
                            var score = parseInt(val);
                            return axios.post(`/admin/hostile-owner/${_owner.id}`, {
                                '_method': 'PUT',
                                'id': _owner.id,
                                'score': score,
                                'reason': _owner.hostility_reason.reason,
                                'scope': 'update',
                                '_token': token
                            })
                            .then(response => {
                                newValue = val;
                                if (!response.success) {
                                    Swal.insertQueueStep({
                                        type: 'error',
                                        title: response.message
                                    });
                                    Swal.hideLoading();
                                }
                            })
                            .catch(error => {
                                Swal.insertQueueStep({
                                    type: 'error',
                                    title: 'Error: ' + error.message
                                });
                                Swal.hideLoading();
                            });
                        },
                        allowOutsideClick: () => !Swal.isLoading()
                    })
                    .then(result => {
                        if (result.value) {
                            Swal.fire({
                                type: 'success',
                                title: `Berhasil set hostility score dari ${_owner.hostility} menjadi ${newValue} pada owner ${_owner.name}`,
                                text: "Halaman akan di-refresh setelah Anda klik OK",
                                onClose: () => {
                                    window.location.reload();
                                }
                            });
                        }
                    });
                }

                // --------------------------------------------------------------------------------
                // add hostile reason
                if (action === 'add-hostile-reason') {
                    Swal.fire({
                        title: 'Tambah Hostile Reason',
                        input: 'select',
                        inputOptions: {
                            'fraud': 'Indikasi fraud',
                            'competitor': 'Indikasi kompetitor',
                            'not_paying_gp': 'Tidak bayar minimum charging GP'
                        },
                        inputPlaceholder: 'Pilih alasan hostile',
                        confirmButtonText: '&#10004; Set',
                        showCancelButton: true,
                        cancelButtonClass: 'btn',
                        showLoaderOnConfirm: true,
                        preConfirm: (val) => {
                            return axios.post('/admin/hostile-owner', {
                                'id': _owner.id,
                                'score': _owner.hostility,
                                'reason': val,
                                '_token': token
                            })
                            .then(response => {
                                newValue = val;
                                if (!response.data.success) {
                                    Swal.showValidationMessage(response.data.message);
                                    Swal.hideLoading();
                                }
                            })
                            .catch(error => {
                                Swal.showValidationMessage('Error: ' + error.message);
                                Swal.hideLoading();
                            });
                        },
                        allowOutsideClick: () => !Swal.isLoading()
                    })
                    .then(result => {
                        if (result.value) {
                            Swal.fire({
                                type: 'success',
                                title: `Berhasil menambah hostility reason ${newValue} pada owner ${_owner.name}`,
                                text: `Halaman akan di-refresh setelah Anda klik OK`,
                                onClose: () => {
                                    window.location.reload();
                                }
                            });
                        }
                    });
                }

                // --------------------------------------------------------------------------------
                // update hostile reason
                if (action === 'update-hostile-reason') {
                    Swal.fire({
                        title: 'Update Hostile Reason',
                        input: 'select',
                        inputOptions: {
                            'fraud': 'Indikasi fraud',
                            'competitor': 'Indikasi kompetitor',
                            'not_paying_gp': 'Tidak bayar minimum charging GP'
                        },
                        inputValue: _owner.hostility_reason.reason,
                        inputPlaceholder: 'Pilih alasan hostile',
                        confirmButtonText: '&#10004; Set',
                        showCancelButton: true,
                        cancelButtonClass: 'btn',
                        showLoaderOnConfirm: true,
                        preConfirm: (val) => {
                            return axios.post(`/admin/hostile-owner/${_owner.id}`, {
                                'id': _owner.id,
                                'score': _owner.hostility,
                                'reason': val,
                                '_token': token,
                                '_method': 'patch'
                            })
                            .then(response => {
                                newValue = val;
                                if (!response.data.success) {
                                    Swal.showValidationMessage(response.data.message);
                                    Swal.hideLoading();
                                }
                            })
                            .catch(error => {
                                Swal.showValidationMessage('Error: ' + error.message);
                                Swal.hideLoading();
                            });
                        },
                        allowOutsideClick: () => !Swal.isLoading()
                    })
                    .then(result => {
                        if (result.value) {
                            Swal.fire({
                                type: 'success',
                                title: `Berhasil update hostility reason pada owner ${_owner.name} dari ${reasonName(_owner.hostility_reason.reason)} menjadi ${reasonName(newValue)}`,
                                text: `Halaman akan di-refresh setelah Anda klik OK`,
                                onClose: () => {
                                    window.location.reload();
                                }
                            });
                        }
                    });
                }

                // --------------------------------------------------------------------------------
                // Resetting score
                if (action === 'reset') {
                    Swal.fire({
                        title: 'Reset Score?',
                        text: "Owner ini akan dihapus dari daftar Hostile Owner",
                        type: 'question',
                        buttonsStyling: false,
                        confirmButtonClass: 'btn btn-primary',
                        showCancelButton: true,
                        cancelButtonClass: 'btn',
                        showLoaderOnConfirm: true,
                        confirmButtonText: 'Yes, reset it!',
                        preConfirm: (val) => {
                            return $.ajax({
                                type: 'POST',
                                url: `/admin/hostile-owner/${_owner.id}`,
                                dataType: 'json',
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                data: {
                                    'scope': 'reset',
                                    '_token': token,
                                    '_method': 'DELETE'
                                },
                                success: (response) => {
                                    if (!response.success) {
                                        Swal.insertQueueStep({
                                            type: 'error',
                                            title: response.message
                                        });
                                        return;
                                    }
                                },
                                error: (error) => {
                                    Swal.insertQueueStep({
                                        type: 'error',
                                        title: 'Error: ' + error.message
                                    });
                                }
                            });
                        },
                        allowOutsideClick: () => !Swal.isLoading()
                    }).then((result) => {
                        if (result.value) {
                            Swal.fire({
                                type: 'success',
                                title: "Owner " + _owner.name + " telah dihapus dari daftar Hostile Owner",
                                text: "Halaman akan di-refresh setelah Anda klik OK",
                                onClose: () => {
                                    window.location.reload();
                                }
                            });
                        }
                    })
                }

            });

        });
    </script>
@endsection
