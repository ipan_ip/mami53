<table id="" class="table table-striped">
    <thead>
        <tr>
            <th>Email</th>  
            <th>User ID</th>  
            <th>Designer ID</th>  
            <th>Created at</th>  
        </tr>
    </thead>
    <tbody>
        @foreach($datas as $data)
        <tr>
            <td>{{ $data->email }}</td>
            <td>{{ $data->user_id }}</td>
            <td>{{ $data->designer_id }}</td>
            <td>{{ $data->created_at->format('d-m-Y H:i:s') }}</td>
        </tr>
        @endforeach
    </tbody>
</table>

{{ $datas->appends(Request::except('page'))->links() }}