<table id="" class="table table-striped">
    <thead>
        <tr>
            <th>ID</th>
            <th>Model</th>
            <th>Email</th>
            <th>Created at</th>            
        </tr>
    </thead>
    <tbody>
        @foreach($datas as $data)
        <tr>
            <td>{{ $data->id }}</td>
            <td>{{ $data->model }}</td>
            <td>{{ $data->email }}</td>
            <td>{{ $data->created_at->format('d-m-Y H:i:s') }}</td>
        </tr>
        @endforeach
    </tbody>
</table>

{{ $datas->appends(Request::except('page'))->links() }}