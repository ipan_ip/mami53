<table id="" class="table table-striped">
    <thead>
        <tr>
            <th>User</th>
            <th>User ID</th>
            <th>Designer</th>
            <th>Designer ID</th>
            <th>Created at</th>            
        </tr>
    </thead>
    <tbody>
        @foreach($datas as $data)
        <tr>
            @if($data->user)            
            <td>{{ $data->user->name }}</td>
            @else
            <td></td>
            @endif
            <td>{{ $data->user_id }}</td>
            @if($data->designer)
            <td>{{ str_limit($data->designer->name, 32, '...') }}</td>
            @else
            <td></td>
            @endif
            <td>{{ $data->designer_id }}</td>
            <td>{{ $data->created_at->format('d-m-Y H:i:s') }}</td>
        </tr>
        @endforeach
    </tbody>
</table>

{{ $datas->appends(Request::except('page'))->links() }}