@extends('admin.layouts.main')
@section('content')
    <?php App::setLocale('id'); ?>
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">List {{ ucfirst($module) }}</h3>
            <div class="box-tools pull-right">
                <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default" style="overflow:hidden">                                        
                    <form action="" class="form-inline" method="get" style="text-align:right">
                        <select name="module" class="form-control input-sm" style="margin-right:2px">
                            <option value="download" {{ Request::get('module') == 'download' ? 'selected' : '' }}>Download</option>
                            <option value="love" {{ Request::get('module') == 'love' ? 'selected' : '' }}>Love</option>
                            <option value="subscribe" {{ Request::get('module') == 'subscribe' ? 'selected' : '' }}>Subscribe</option>
                        </select>
                        <input name="from" class="form-control input-sm datepicker" type="text" placeholder="Start Date" value="{{ Request::get('from') }}" id="start-date" autocomplete="off" style="margin-right:2px" />
                        <input name="to" class="form-control input-sm datepicker" type="text" placeholder="End Date" value="{{ Request::get('to') }}" id="end-date" autocomplete="off" style="margin-right:2px" />                                          
                        <button class="btn btn-primary btn-md" id="buttonSearch"><i class="fa fa-search">&nbsp;</i>Search</button>                        
                    </form>
                </div>                
            </div>
            <div class="horizontal-wrapper" style="display:none" id="formStorySearch">
                
                <div class="btn-horizontal-group bg-default row">
                    <div class="col-sm-10">
                        <input type="text" name="search_keyword" class="form-control" />
                    </div>
                    <div class="col-sm-2">
                        <button class="btn btn-sm btn-info">Search</button>    
                    </div>
                </div>
                
            </div>

            <div id="module{{ $model }}">            
                @if($module == 'love')
                    
                    @include('admin.contents.report.partials.love')

                @elseif($module == 'download')
                    
                    @include('admin.contents.report.partials.download')

                @elseif($module == 'subscribe')
                    
                    @include('admin.contents.report.partials.subscribe')
                    
                @endif
            </div>
            
        </div>
    </div>
@stop

@section('style')
    <!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> -->
@stop

@section('script')
<!-- Holder for Upload File Thumbnail -->
{{ HTML::script('assets/vendor/holder/holder.js') }}

<!-- Jquery Datepicker -->
<!-- <script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script> -->

<!-- page script -->
<script type="text/javascript">
    
    $(function()
    {
        $('#select-sort-option').change(function(event){
            var url  = '{{ URL::route("admin.stories.index") }}' + '?o=' + event.target.value ;
            
            url += '&st=' + $('#start-date').val();
            url += '&en=' + $('#end-date').val();

            window.location = url;

        });

        $('.datepicker').datepicker({ dateFormat : 'dd-mm-yy'});
    });
</script>
@stop