@extends('admin.layouts.main')

@section('style')
	<style>
		.dropdown-menu > li > a:hover {
			color: #333
		}

		/* Sweetalert */
		
		.swal-shown
		{
			height: auto !important;
		}
		
		.custom-swal
		{
			z-index: 10000 !important;
		}
		
		.custom-wide-swal
		{
			width: 640px !important;
			font-size: 13px !important;
		}
		
		/* Custom table row */
		tr.inactive td
		{
			background-color: #E6C8C8;
		}
    </style>
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
@endsection

@section('content')
	<!-- table -->
	<div class="box box-primary">
		<div class="box-header">
			<h3 class="box-title"><i class="fa fa-money"></i> {{ $boxTitle }}</h3>
		</div><!-- /.box-header -->
		<div class="box-body no-padding">
			<div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default form-inline" style="padding-left: 10px;padding-right: 10px;">
					<!-- Add button -->
                    <div class="col-md-6 bg-default" style="padding-bottom:10px;">
                        <a type="submit" class="btn-add btn btn-primary btn-sm" href="{{ route('admin.kost-value.create') }}">
                            <i class="fa fa-plus">&nbsp;</i>Add Benefit
                        </a>
                    </div>  
                    <!-- Search filters -->
                    <div class="col-md-6 bg-default" style="padding-bottom:10px;text-align:right">
                        <form action="" method="get">
                            <input type="text" name="q" class="form-control input-sm" placeholder="Search Benefit Name"
                                autocomplete="off" value="{{ request()->input('q') }}">
                            <button class="btn btn-primary btn-sm" id="buttonSearch"><i
                                    class="fa fa-search">&nbsp;</i>Search</button>
                        </form>
                    </div>
                </div>
			</div>
			<div class="box-body">
				<table id="tableListTag" class="table table-striped">
					<thead>
                        <tr>
                            <th width="30px;">No</th>
                            <th>Benefit Name</th>
                            <th>Description</th>
                            <th>Icon Large</th>
                            <th>Icon Small</th>
                            <th>Created By</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($benefits as $key => $benefit)
                        <tr>
                            <td>{{ $key+1 }}</td>
                            <td>{{ $benefit->name }}</td>
                            <td>{{ $benefit->description }}</td>
                            <td>
								@if($benefit->photo)
									<img src="{{ $benefit->photo->getMediaUrl()['real'] }}" class="img-thumbnail" alt="">
								@else
									No Icon
								@endif
                            </td>
                            <td>
								@if($benefit->photoSmall)
									<img src="{{ $benefit->photoSmall->getMediaUrl()['real'] }}" class="img-thumbnail" alt="">
								@else
									No Icon
								@endif
                            </td>
                            <td>
								@if($benefit->createdBy)
									{{ $benefit->createdBy->name }}
								@else
                                    -
								@endif
                            </td>
							<td class="text-center table-action-column" style="padding-right:15px!important;">
								<div class="btn-group">
									<button type="button" class="btn btn-default btn-sm dropdown-toggle"
									        data-toggle="dropdown">
										<i class="glyphicon glyphicon-cog"></i> Actions
										<span class="caret"></span>
                                    </button>
									<ul class="dropdown-menu pull-right">
										<li>
                                            <a href="{{ route('admin.kost-value.edit', $benefit->id) }}" data-data="{{ $benefit->id }}">
                                                <i class="fa fa-edit"></i> Edit
                                            </a>
                                        </li>
										<li>
                                        <a href="#" class="actions" data-action="remove" data-data="{{ $benefit }}">
                                                <i class="fa fa-minus-square"></i> Remove
                                            </a>
                                        </li>
									</ul>
                                <div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
				</table>
			</div>
        </div><!-- /.box-body -->
        <div class="box-footer text-center">
            <div class="col-md-12 bg-default">
                {{ $benefits->appends(Request::except('page'))->links() }}
            </div>
        </div>
	</div><!-- /.box -->
	<!-- table -->
@stop

@section('script')
	<script src="//cdn.jsdelivr.net/npm/holderjs@2.9.6/holder.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>
	<script src="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.js"></script>
	<!-- page script -->
	<script type="text/javascript">
        function triggerAlert(type, message) {
            Swal.fire({
                type: type,
                customClass: {
                    container: 'custom-swal'
                },
                html: message
            });
        }

        function triggerLoading(message) {
            Swal.fire({
                title: message,
                customClass: {
                    container: 'custom-swal'
                },
                allowEscapeKey: false,
                allowOutsideClick: false,
                onOpen: () => {
                    swal.showLoading();
                }
            });
        }

        function ajaxCall(type, title, html, method, url, payload, input, btnText, btnClass, needRefresh = true) {
            Swal.fire({
                type: type,
                title: title,
                html: html,
                buttonsStyling: btnClass == null,
                confirmButtonText: btnText,
                confirmButtonClass: btnClass,
                showCancelButton: true,
                cancelButtonClass: (btnClass !== null) ? 'btn btn-default' : null,
                customClass: 'custom-swal',
                showLoaderOnConfirm: true,
                input: input,
                preConfirm: (response) => {
                    console.log("This is the payload:", payload);

                    return $.ajax({
                        type: method,
                        url: url,
                        data: payload,
                        dataType: 'json',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: (response) => {
                            if (response.success === false) {
                                Swal.showValidationMessage(response.message);
                            }

                            return;
                        },
                        error: (error) => {
                            Swal.showValidationMessage('Request Failed: ' + error.message);
                        }
                    });
                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                if (!result.dismiss) {
                    if (result.value.success == true) {
                        Swal.fire({
                            type: 'success',
                            title: result.value.title,
                            html: result.value.message,
                            onClose: () => {
                                if (needRefresh) window.location.reload();
                            }
                        });
                    } else {
                        Swal.fire({
                            type: 'error',
                            title: "Failed!",
                            html: result.value.message
                        });
                    }
                }
            });
        }


        $(function () {
            $('.actions').on('click', e => {
                e.preventDefault();

                const action = $(e.currentTarget).data('action');

                if (action === 'remove') {
                    _data = $(e.currentTarget).data('data');

                    console.log(_data);

                    let payload = {id: _data.id};
                    let msg = '<h5>Delete kost benefit will affect all Kos/Apartment using it!<br/><strong>Please do with caution!</strong></h5>';

                    ajaxCall('warning', 'Tag Removal', msg + 'Click [Remove] to continue', 'POST', '/admin/kost-value/remove', payload, null, 'Remove', 'btn btn-danger');
                }
            });
        });
	</script>
@stop
