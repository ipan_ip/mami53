@extends('admin.layouts.main')

@section('content')
    <div style="margin-bottom:15px;">
        <a href="{{ route('admin.kost-value.index') }}">
            <i class="fa fa-backward"></i> kembali <br>
        </a>
    </div>

    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title"><i class="fa fa-money"></i> {{ $boxTitle }}</h3>
        </div>

        <form action="{{ route('admin.kost-value.update', $benefit->id) }}" method="POST" enctype="multipart/form-data" class="form-horizontal form-bordered">
            <div class="box-body no-padding">
                <div class="form-group bg-default">
                    <label for="level-name" class="control-label col-sm-2">Benefit Name</label>
                    <div class="col-sm-10">
                        <input type="text" name="name" id="level-name" class="form-control" value="{{ $benefit->name }}">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="description" class="control-label col-sm-2">Description</label>
                    <div class="col-sm-10">
                        <input type="text" name="description" id="description" class="form-control" value="{{ $benefit->description }}">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="icon-large" class="control-label col-sm-2">Icon Large</label>
                    <div class="col-sm-10">
                        <label for="inputIcon">Upload Icon Large</label> <br>
                        @if($benefit->photo)
                            <img src="{{ $benefit->photo->getMediaUrl()['real'] }}" class="img-thumbnail" id="iconLarge" alt="">
                        @endif
                        <input type="file" name="icon" id="inputIcon" onchange="changeOnLargeIcon(this)" >
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="icon-large" class="control-label col-sm-2">Icon Small</label>
                    <div class="col-sm-10">
                        <label for="inputIconSmall">Upload Icon Small</label> <br>
                        @if($benefit->photoSmall)
                            <img src="{{ $benefit->photoSmall->getMediaUrl()['real'] }}" class="img-thumbnail" id="iconSmall" alt="">
                        @endif
                        <input type="file" name="icon_small" id="inputIconSmall" onchange="changeOnSmallIcon(this)">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <div class="col-sm-10 col-sm-push-2">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection

@section('script')
    <script type="text/javascript">
        function changeOnLargeIcon(){
            $('#iconLarge').remove();
        }
        function changeOnSmallIcon(){
            $('#iconSmall').remove();
        }
	</script>
@stop