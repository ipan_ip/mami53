@extends('admin.layouts.main')

@section('style')
<style>
    .reject-remark-filled i.fa {
        color: #ff0000;
    }
    .table > tbody > tr > td {
        vertical-align: middle;
    }
    .font-large {
        font-size: 1.5em;
    }
    .font-semi-large {
        font-size: 1.15em;
    }
    .font-grey {
        color: #9E9E9E;
    }
    .dropdown-menu > li > a:hover {
        color: #333
    }
    .label-grey {
        background-color: #CFD8DC;
    }

    /* Centering the modal */
    .modal {
        text-align: center;
        padding: 0!important;
    }

    .modal:before {
        content: '';
        display: inline-block;
        height: 100%;
        vertical-align: middle;
        margin-right: -4px;
    }

    .modal-dialog {
        display: inline-block;
        text-align: left;
        vertical-align: middle;
    }
</style>
@endsection

@section('content')
    <div class="box box-default">
        <div class="nox-head text-center" style="margin:20px;">
            <span class="label label-danger font-semi-large"><i class="fa fa-exclamation-circle"></i> Fitur ini dimatikan untuk sementara! (Per tanggal 20 Mei 2019), kecuali untuk SERVER TESTING. Silakan kontak PM atau Developer untuk info lebih lanjut.</span>
        </div>
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <form action="" id="form-filters" class="form-inline" method="get">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control input-md" placeholder="Ketik Nama Kost atau ID" autocomplete="off" value="{{ request()->input('q') }}" style="width:200px;margin-left:20px;">
                            <span class="input-group-btn">
                                <button type="submit" name="search" id="buttonSearch" class="btn btn-primary"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                        <div style="padding-top:5px;margin-right:10px;" class="pull-right">
                        {{ Form::select('class', $statusFilter, request()->input('class'), array('class' => 'pull-right-sort form-group btn btn-primary selector', 'placeholder'=>'Tampilkan Semua Class')) }}
                        </div>
                    </form>
                </div>
            </div>
            <div class="horizontal-wrapper" style="display:none" id="formStorySearch">
                {{ Form::open(array('url'=>$searchUrl , 'method'=> 'POST')) }}
                <div class="btn-horizontal-group bg-default row">
                    <div class="col-sm-10">
                        <input type="text" name="search_keyword" class="form-control"/>
                    </div>
                    <div class="col-sm-2">
                        <button class="btn btn-sm btn-info">Search</button>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
            <!-- table -->
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th style="padding-left:10px!important;"></th>
                        <th>Nama Kost / Kelas</th>
                        <th class="text-center" width="17%">Fac. Kamar</th>
                        <th class="text-center" width="17%">Fac. Kmr. Mandi</th>
                        <th class="text-center" width="17%">Fac. Umum</th>
                        <th class="text-center" width="17%">Fac. Lain</th>
                        <th class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($rowsRoom as $index => $rowRoom)
                        <tr>
                            <td class="text-right" style="padding-left:10px!important;">
                                @if ($rowRoom->is_active == 'true')
                                <i class="fa fa-arrow-circle-up fa-2x text-success" data-toggle="tooltip" title="Kost Aktif"></i>
                                @else
                                <i class="fa fa-arrow-circle-down fa-2x text-danger" data-toggle="tooltip" title="Kost Non Aktif"></i>
                                @endif
                            </td>
                            @if ($rowRoom->is_active != 'true')
                            <td class="font-grey">
                            @else
                            <td>
                            @endif
                                @if($rowRoom->class == 1)
                                    <img src="https://mamikos.com/storage/tag-basic.png" height="18px">
                                @elseif($rowRoom->class == 11)
                                    <img src="https://mamikos.com/storage/tag-basic-plus.png" height="18px">
                                @elseif($rowRoom->class == 2)
                                    <img src="https://mamikos.com/storage/tag-exclusive.png" height="18px">
                                @elseif($rowRoom->class == 21)
                                    <img src="https://mamikos.com/storage/tag-exclusive-plus.png" height="18px">
                                @elseif($rowRoom->class == 22)
                                    <img src="https://mamikos.com/storage/tag-exclusive-max.png" height="18px">
                                @elseif($rowRoom->class == 3)
                                    <img src="https://mamikos.com/storage/tag-residence.png" height="18px">
                                @elseif($rowRoom->class == 31)
                                    <img src="https://mamikos.com/storage/tag-residence-plus.png" height="18px">
                                @elseif($rowRoom->class == 32)
                                    <img src="https://mamikos.com/storage/tag-residence-max.png" height="18px">
                                @else
                                    <span class="label label-default"><i class="fa fa-ban"></i> No Class</span>
                                @endif
                                <br/><strong><span class="font-semi-large" style="padding-bottom:20px!important;">{{ $rowRoom->name }}</span></strong>
                                <br/><small>Last Update: {{ $rowRoom->kost_updated_date }}</small>
                            </td>
                            <td class="text-center">
                            @foreach($rowRoom->tags as $tag)   
                                @if ($tag->type == "fac_room")
                                    @if ($rowRoom->is_active == 'true')
                                    <span class="label label-success" style="margin-bottom:5px!important;">{{ $tag->name }}</span>
                                    @else
                                    <span class="label label-grey" style="margin-bottom:5px!important;">{{ $tag->name }}</span>
                                    @endif
                                @endif
                            @endforeach  
                            </td>
                            <td class="text-center">
                            @foreach($rowRoom->tags as $tag)   
                                @if ($tag->type == "fac_bath")
                                    @if ($rowRoom->is_active == 'true')
                                    <span class="label label-primary" style="margin-bottom:5px!important;">{{ $tag->name }}</span>
                                    @else
                                    <span class="label label-grey" style="margin-bottom:5px!important;">{{ $tag->name }}</span>
                                    @endif
                                @endif
                            @endforeach  
                            </td>
                            <td class="text-center">
                            @foreach($rowRoom->tags as $tag)   
                                @if ($tag->type == "fac_share")
                                    @if ($rowRoom->is_active == 'true')
                                    <span class="label label-warning" style="margin-bottom:5px!important;">{{ $tag->name }}</span>
                                    @else
                                    <span class="label label-grey" style="margin-bottom:5px!important;">{{ $tag->name }}</span>
                                    @endif
                                @endif
                            @endforeach  
                            </td>
                            <td class="text-center">
                            @foreach($rowRoom->tags as $tag)   
                                @if ($tag->type == "fac_park")
                                    @if ($rowRoom->is_active == 'true')
                                    <span class="label label-info" style="margin-bottom:5px!important;">{{ $tag->name }}</span>
                                    @else
                                    <span class="label label-grey" style="margin-bottom:5px!important;">{{ $tag->name }}</span>
                                    @endif
                                @endif
                            @endforeach  
                            </td>
                            <td class="table-action-column" style="padding-right:15px!important;">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-cogs"></i> Actions
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu pull-right">
                                        <li>
                                            <a href="{{ URL::to('admin/room-class/recalculate', $rowRoom->id) }}" class="action-recalculate" data-name="{{ $rowRoom->name }}"><i class="fa fa-repeat"></i> Recalculate Class</a> 
                                        </li>
                                        @if($rowRoom->class != 0)
                                        <li>
                                            <a href="{{ URL::to('admin/room-class/reset', $rowRoom->id) }}" class="action-reset" data-name="{{ $rowRoom->name }}"><i class="fa fa-ban"></i> Reset Class</a> 
                                        </li>
                                        @endif
                                        <li>
                                            <a href="{{ URL::to('admin/room?q=' . $rowRoom->song_id) }}"><i class="fa fa-eye"></i> Lihat Kost</a> 
                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <!-- /.table -->
        </div><!-- /.box-body -->
        <div class="box-body">
            {{ $rowsRoom->appends(Request::except('page'))->links() }}
        </div>
    </div><!-- /.box -->
@endsection
@section('script')
    <!-- Jquery Datepicker -->
    <!-- <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script> -->

    <script type="text/javascript">
        $(function(){
            var token = '{{ csrf_token() }}';

            $('.selector').change(function() {
                $('#form-filters').trigger('submit');
            });

            $('.action-recalculate').click(function(e) {
                var roomName = $(this).data('name');
                var confirm = window.confirm("Class dari kos: \"" + roomName + "\"\nAkan dikalkulasi ulang dan Class baru akan otomatis terpasang.\n\rYakin akan melanjutkan?");
                if (!confirm) {
                    e.preventDefault();
                }
            });

            $('.action-reset').click(function(e) {
                var roomName = $(this).data('name');
                var confirm = window.confirm("Action ini akan manghapus Class dari kos:\n\"" + roomName + "\".\n\rYakin akan melanjutkan?");
                if (!confirm) {
                    e.preventDefault();
                }
            });
        });
    </script>
@endsection
