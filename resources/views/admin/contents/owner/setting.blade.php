@extends('admin.layouts.main')
@section('content')
    <!-- table -->
    <div class="box box-primary">

        <div class="box-header">
            <h3 class="box-title">Setting Owner</h3>
            <div class="box-tools pull-right">
                <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
            </div>
        </div><!-- /.box-header -->
        

        <div class="row">
         <div class="col-md-12">
            <table class="table table-condensed">
              <tr class="danger">
                  <th>No</th>
                  <th>Nama Kost</th>
                  <th>Status Kost Active</th>
                  <th>Notif Sms Update</th>
              </tr>
              
              @foreach ($owners AS $key => $owner) 
                <tr>
                    <td>{{ $key+1 }}</td>
                    <td>{{ $owner->room->name }}</td>
                    <td>
                        @if ($owner->room->is_active == 'true')
                            <label class="label label-primary">Yes</label>
                        @else
                            <label class="label label-danger">No</label> 
                        @endif
                    </td>
                    <td>
                        @if (count($owner->room->setting_notif) > 0)
                            @if ($owner->room->setting_notif[0]->value == "true" AND $owner->room->setting_notif[0]->key == "update_kost")
                              <a class="btn btn-xs btn-success" href="{{ URL::to('admin/owner/setting/update_kost', $owner->designer_id) }}" data-toggle="tooltip" data-placement="right" title="Now is Active">Active</a>
                            @else
                              <a href="{{ URL::to('admin/owner/setting/update_kost', $owner->designer_id) }}" class="btn btn-xs btn-danger" data-toggle="tooltip" data-placement="right" title="Now is Not Active">No</a>
                            @endif
                        @elseif (count($owner->room->setting_notif) == 0)
                            <a class="btn btn-xs btn-success" href="{{ URL::to('admin/owner/setting/update_kost', $owner->designer_id) }}" data-toggle="tooltip" data-placement="right" title="Now is Active">Active</a>
                        @else
                            <a href="">-</a>
                        @endif
                    </td>  
                </tr>
              @endforeach
            </table>
         </div>
        </div>

    </div>
    
    <script type="text/javascript">
        $(function () {
           $('[data-toggle="tooltip"]').tooltip()
        })
    </script>

@endsection