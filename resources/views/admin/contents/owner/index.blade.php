@extends('admin.layouts.main')

@section('style')
<style>
    .reject-remark-filled i.fa {
        color: #ff0000;
    }
    .swal2-popup {
        font-size:1.5rem !important;
    }
</style>
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/jquery-steps@1.1.0/demo/css/jquery.steps.min.css">
@endsection

@section('content')
    {{-- Modals --}}
    @include('admin.contents.booking.partials.view_user_mamipay_info_modal')
    <!-- table -->
    <div class="box box-primary">

        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
            <div class="box-tools pull-right">
                <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
            </div>
        </div><!-- /.box-header -->

        <div class="row">
            <div class="col-md-12">
                <div class="col-md-12">
                    <a href="{{ URL::to('admin/owner/popup') }}" class="col-md-12 btn btn-sm btn-warning" style="margin: 5px 10px 15px 0px;">Create popup owner</a>
                </div>
            </div>
        </div>

        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <form action="" class="form-inline" method="get"
                          style="text-align:right;padding:10px;">
                        <input type="text" name="q" class="form-control input-sm" placeholder="Nama Kost"
                               autocomplete="off" value="{{ request()->input('q') }}">
                        <input type="text" name="owner_name" class="form-control input-sm" placeholder="Owner Name"
                               autocomplete="off" value="{{ request()->input('owner_name') }}">
                        <input type="text" name="phone_owner" class="form-control input-sm" minlength="9" placeholder="No. Telp. Owner"
                               autocomplete="off" value="{{ request()->input('phone_owner') }}">
                        <input type="text" name="from" class="form-control input-sm datepicker"
                               placeholder="Start Date" value="{{ request()->input('from') }}" id="start-date"
                               autocomplete="off" style="margin-right:2px"/>
                        <input type="text" name="to" class="form-control input-sm datepicker" placeholder="End Date"
                               value="{{ request()->input('to') }}" id="end-date" autocomplete="off"
                               style="margin-right:2px"/>
                        {{ Form::select('status_owner', $statusOwner, request()->input('status_owner'), array('class' => 'pull-right-sort form-group btn btn-primary btn-sm', 'placeholder'=>'Semua')) }}

                        {{ Form::select('status', $statusFilter, request()->input('status'), array('class' => 'pull-right-sort form-group btn btn-primary btn-sm', 'placeholder'=>'Listing Status')) }}

                        {{ Form::select('bbk-status', $bbkFilter, request()->input('bbk-status'), array('class' => 'pull-right-sort form-group btn btn-primary btn-sm', 'placeholder'=>'BBK Status')) }}
                        <button class="btn btn-primary btn-md" id="buttonSearch"><i class="fa fa-search">&nbsp;</i>Search
                        </button>
                    </form>
                </div>
            </div>
            <div class="horizontal-wrapper" style="display:none" id="formStorySearch">
                {{ Form::open(array('url'=>$searchUrl , 'method'=> 'POST')) }}
                <div class="btn-horizontal-group bg-default row">
                    <div class="col-sm-10">
                        <input type="text" name="search_keyword" class="form-control"/>
                    </div>
                    <div class="col-sm-2">
                        <button class="btn btn-sm btn-info">Search</button>
                    </div>
                </div>
                {{ Form::close() }}
            </div>

            <div class="col-md-12" style="margin-bottom: 30px;">
                <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                <form class="form-inline">
                    <div class="form-group col-sm-10">
                    <input type="text" id="data_name" name="kos_name" style="width: 100%;" class="form-control input-sm" placeholder="Nama Kost lama"/>
                    </div>
                    <div class="form-group col-sm-2">
                     <input type="submit" id="check-old-name-data" name="submit" style="width: 100%;" class="btn btn-sm btn-success" value="Check">
                    </div>
                </form>
                </div>
                </div>

                <div id="kost_show"></div>
            </div>
            <br/>

            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Room ID</th>
                        <th>Room Name</th>
                        <th>Owner Phone Number</th>
                        <th>Owner ID</th>
                        <th>Account Number</th>
                        <th>Owner Status</th>
                        <th>Status</th>
                        <th>Create / Update</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($rowsOwner as $index => $rowOwner)
                        @if ($rowOwner->room)
                            <tr>
                                <td>{{ $index+1 }}</td>
                                <td>{{ $rowOwner->designer_id }}</td>
                                <td>
                                    @if ($rowOwner->room->incomplete_room)
                                        <span style="content: '';width: 9px;height: 9px;border-radius: 100%;display: -webkit-inline-box;background-color: red;"></span>
                                    @endif
                                    {{ $rowOwner->room->name }} <a href="{{ route('admin.room.edit',$rowOwner->designer_id) }}" target="_blank" rel="noopener"><i class="fa fa-external-link"></i></a>
                                <br/>
                                <a href="{{ URL::to('admin/card', $rowOwner->designer_id) }}" target="_blank" class="btn btn-xs btn-warning">card</a>
                                @if(is_null($rowOwner->room->apartment_project_id))
                                    @if ($rowOwner->user instanceof App\User )
                                        @if (!is_null($rowOwner->user->mamipay_owner))
                                            <a class="btn btn-xs btn-default" href="javascript:openMamipayInfoModal(
                                                '{{ $rowOwner->user->mamipay_owner->bank_account_owner }}',
                                                '{{ $rowOwner->user->mamipay_owner->bank_account_number }}',
                                                '{{ $rowOwner->user->mamipay_owner->bank_name }}',
                                                '{{ $rowOwner->user->mamipay_owner->bank_city ? $rowOwner->user->mamipay_owner->bank_city->city_name : "-" }}'
                                                );">View Mamipay Info</a>
                                        @endif
                                    @endif
                                    @if (!is_null($rowOwner->room->booking_owner_requests->first()))
                                        <form method="GET" target="_blank" action="{{ url('admin/booking/owner/request') }}">
                                            @csrf
                                            <input type="hidden" id="room_name" name="room_name" value="{{ $rowOwner->room->name }}" />
                                            <button type="submit" class="btn btn-xs btn-primary" onclick="this.form.submit();">BBK Data</button>
                                        </form>
                                    @else
                                        <br/>
                                    @endif
                                    @if ($rowOwner->user instanceof App\User )
                                        @if (!is_null($rowOwner->user->mamipay_owner) && $rowOwner->user->mamipay_owner->status == 'approved')
                                        <span class="label label-success">Mamipay</span>
                                        @endif
                                    @endif
                                    @if (!is_null($rowOwner->room->booking_owner_requests->first()) && $rowOwner->room->booking_owner_requests->first()->status == 'approve')
                                    <span class="label label-primary">BBK</span>
                                    @elseif (!is_null($rowOwner->room->booking_owner_requests->first()) && $rowOwner->room->booking_owner_requests->first()->status == 'reject')
                                    <span class="label label-danger">BBK Rejected</span>
                                    @endif
                                @endif
                                </td>
                                <td>{{ $rowOwner->room->owner_phone }}</td>
                                <td>
                                    {{ $rowOwner->user_id }}

                                    @if (!is_null($rowOwner->user) && $rowOwner->user->hostility > 0)
                                        <span class="label label-danger">Hostile</span>
                                    @endif
                                </td>
                                <td>{{ isset($rowOwner->user) ? $rowOwner->user->phone_number : "--" }}</td>
                                <td>{{ $rowOwner->owner_status}}
                                    <br/>
                                    @if (!is_null($rowOwner->room->last_update_from)) <strong>Update from:</strong> {{ $rowOwner->room->last_update_from }} @endif
                                </td>

                                <td>
                                    @if($rowOwner->status == 'verified')
                                        <span class="label label-success">{{ $rowOwner->status }}</span>
                                    @elseif($rowOwner->status == 'unverified')
                                        <span class="label label-danger">{{ $rowOwner->status }}</span>
                                    @else
                                        <span class="label label-info">
                                        @if ($rowOwner->status == 'draft2')
                                        Edited
                                        @else
                                        {{ $rowOwner->status }}
                                        @endif
                                        </span>
                                    @endif

                                    @if($rowOwner->status == 'draft2' && ! is_null($rowOwner->room->verification) && $rowOwner->room->verification->is_ready)
                                        <span class="label label-danger">Ready to Verify</span>
                                    @endif
                                </td>

                                <td>{{ $rowOwner->created_at }} / {{ $rowOwner->updated_at }}</td>
                                <td class="table-action-column">
                                    @if (($rowOwner->owner_status == 'Agen' OR $rowOwner->owner_status == 'Lainnya') AND $rowOwner->status == 'add')
                                    <i data-toggle="tooltip" title="Nunggu verifikasi agen" class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                    @else
                                    <div class="btn-action-group">
                                        @if (isset($rowOwner->user))
                                        <a href="{{ URL::to('admin/owner/setting', $rowOwner->user->id) }}"><i class="fa fa-cog" aria-hidden="true"></i></a>
                                        @endif
                                        <a href="{{ url()->route('admin.room.history-owner', ['id' => $rowOwner->designer_id, 'owner_id' => $rowOwner->id]) }}" target="_blank" rel="noopener" id="buttonSearch"><i class="glyphicon glyphicon-pencil">&nbsp;</i></a>

                                        <a href="{{ route('admin.owner.destroy', $rowOwner->id) }}" onclick="return confirm('Yakin mau hapus saya?')" title="Delete">
                                            <i class="fa fa-trash-o"></i>
                                        </a>
                                        @if($rowOwner->status != 'verified')
                                            @if(is_null($rowOwner->room->apartment_project_id))
                                                @if(
                                                    $rowOwner->status == 'add' &&
                                                    (is_null($rowOwner->room->booking_owner_requests->first()) || (!is_null($rowOwner->room->booking_owner_requests->first()) && $rowOwner->room->booking_owner_requests->first()->status == 'not_active'))
                                                )
                                                <a href="#" title="Verify" data-toggle="modal"
                                                    data-target="#verifyModal2" data-path="{{ route('admin.owner.verify', $rowOwner->id) }}"
                                                    data-name="{{ $rowOwner->room->name }}">
                                                    <i class="fa fa-check"></i>
                                                </a>
                                                @else
                                                    @if ($rowOwner->user instanceof App\User)
                                                        @if(
                                                            !is_null($rowOwner->user->mamipay_owner) &&
                                                            $rowOwner->user->mamipay_owner->status == 'approved' &&
                                                            $rowOwner->status == 'add' &&
                                                            !is_null($rowOwner->room->booking_owner_requests->first()) &&
                                                            $rowOwner->room->booking_owner_requests->first()->status == 'reject'
                                                        )
                                                        <a href="#" title="Verify" data-toggle="modal"
                                                            data-target="#verifyModal" data-path="{{ route('admin.owner.verify', $rowOwner->id) }}"
                                                            data-name="{{ $rowOwner->room->name }}">
                                                            <i class="fa fa-check"></i>
                                                        </a>
                                                        @elseif(
                                                            !is_null($rowOwner->user->mamipay_owner) &&
                                                            $rowOwner->user->mamipay_owner->status == 'approved' &&
                                                            $rowOwner->status == 'add' &&
                                                            !is_null($rowOwner->room->booking_owner_requests->first()) &&
                                                            $rowOwner->room->booking_owner_requests->first()->status == 'approve'
                                                        )
                                                        <a href="#" title="Verify" data-toggle="modal"
                                                            data-target="#verifyModal3" data-path="{{ route('admin.owner.verify', $rowOwner->id) }}"
                                                            data-name="{{ $rowOwner->room->name }}">
                                                            <i class="fa fa-check"></i>
                                                        </a>
                                                        @elseif(
                                                            !is_null($rowOwner->user->mamipay_owner) &&
                                                            $rowOwner->user->mamipay_owner->status == 'approved' &&
                                                            $rowOwner->status == 'add' &&
                                                            !is_null($rowOwner->room->booking_owner_requests->first()) &&
                                                            $rowOwner->room->booking_owner_requests->first()->status == 'waiting'
                                                        )
                                                        <a href="#" title="Verify" data-toggle="modal"
                                                            data-target="#verifyModal4" data-path="{{ route('admin.owner.verify', $rowOwner->id) }}"
                                                            data-name="{{ $rowOwner->room->name }}">
                                                            <i class="fa fa-check"></i>
                                                        </a>
                                                        @else
                                                        <a href="{{ route('admin.owner.verify', $rowOwner->id) }}" title="Verify">
                                                            <i class="fa fa-check"></i>
                                                        </a>
                                                        @endif
                                                    @endif
                                                @endif
                                            @else
                                            <a href="{{ route('admin.owner.verify', $rowOwner->id) }}" title="Verify">
                                                <i class="fa fa-check"></i>
                                            </a>
                                            @endif
                                        @endif

                                        {{--
                                        @if($rowOwner->status != 'unverified')
                                            <a href="{{ route('admin.owner.unverify', $rowOwner->id) }}" title="Unverify">
                                                <i class="fa fa-ban"></i>
                                            </a>
                                        @endif
                                        --}}

                                        <a href="{{ route('admin.owner.reject.edit', $rowOwner->designer_id) }}" title="Alasan ditolak" class="{{ $rowOwner->room->reject_remark != '' ? 'reject-remark-filled' : ''}}">
                                            <i class="fa fa-flag"></i>
                                        </a>

                                        @if ($rowOwner->room->is_active == "true")
                                            <span class="btn-xs btn-primary">True</span>
                                        @else
                                            <span class="btn-xs btn-danger">False</span>
                                        @endif


                                            <a href="javascript:openRequestStep({{$rowOwner->user_id}});" style="display:block; width:auto; color:#FFF; margin-right:0px; margin-top:5px;" class="btn btn-xs btn-success">Add BBK Feature</a>

                                    </div>
                                    @endif
                                </td>
                            </tr>
                        @endif
                    @endforeach
                </tbody>
            </table>

        </div><!-- /.box-body -->
        <div class="box-body no-padding">
            {{ $rowsOwner->appends(Request::except('page'))->links() }}
        </div>
    </div><!-- /.box -->
    <!-- table -->
    <div id="wizard"></div>

    <!-- Verify Modal-->
    <div class="modal fade" id="verifyModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">This listing currently has rejected Booking Langsung/BBK. Are you sure to verify listing?</div>
                <div class="modal-footer">
                    <button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-sm btn-success" href="#"
                        onclick="this.disabled=true;event.preventDefault();document.getElementById('verify-form1').submit();">Verify</button>
                    <form id="verify-form1" method="GET" style="display: none;">
                        @csrf
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Verify Modal 2-->
    <div class="modal fade" id="verifyModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">This listing has not registered as Mamipay or Booking Langsung/BBK. Are you sure to verify listing?</div>
                <div class="modal-footer">
                    <button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-sm btn-success" href="#"
                        onclick="this.disabled=true;event.preventDefault();document.getElementById('verify-form2').submit();">Verify</button>
                    <form id="verify-form2" method="GET" style="display: none;">
                        @csrf
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Verify Modal 3-->
    <div class="modal fade" id="verifyModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">This listing has registered as Booking Langsung/BBK. Are you sure to verify listing?</div>
                <div class="modal-footer">
                    <button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-sm btn-success" href="#"
                        onclick="this.disabled=true;event.preventDefault();document.getElementById('verify-form3').submit();">Verify</button>
                    <form id="verify-form3" method="GET" style="display: none;">
                        @csrf
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Verify Modal 4-->
    <div class="modal fade" id="verifyModal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">This listing has mamipay data but not yet verified as Booking Langsung/BBK. Are you sure to double verify listing & Booking Langsung/BBK feature? (Please verify owner personal data first!)</div>
                <div class="modal-footer">
                    <form method="GET" target="_blank" action="{{ url('admin/booking/owner/request') }}">
                        @csrf
                        <input type="hidden" id="room_name" name="room_name" value="" />
                        <button type="submit" class="btn btn-sm btn-secondary" onclick="this.form.submit();">Check Mamipay Data</button>
                        <button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                        <button class="btn btn-sm btn-success" href="#"
                            onclick="this.disabled=true;event.preventDefault();document.getElementById('verify-form4').submit();">Verify</button>
                    </form>
                    <form id="verify-form4" method="GET" style="display: none;">
                        @csrf
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <script src="https://cdn.jsdelivr.net/npm/lodash@4.17.15"></script>

    <script type="text/javascript">
        $('#verifyModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var path = button.data('path')
            var name = button.data('name')
            var modal = $(this)
            modal.find('.modal-title').text(name)
            modal.find('#verify-form1').attr("action", path);
        })
    </script>

    <script type="text/javascript">
        $('#verifyModal2').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var path = button.data('path')
            var name = button.data('name')
            var modal = $(this)
            modal.find('.modal-title').text(name)
            modal.find('#verify-form2').attr("action", path);
        })
    </script>

    <script type="text/javascript">
        $('#verifyModal3').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var path = button.data('path')
            var name = button.data('name')
            var modal = $(this)
            modal.find('.modal-title').text(name)
            modal.find('#verify-form3').attr("action", path);
        })
    </script>

    <script type="text/javascript">
        $('#verifyModal4').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var path = button.data('path')
            var name = button.data('name')
            var modal = $(this)
            document.getElementById("room_name").value = name
            modal.find('.modal-title').text(name)
            modal.find('#verify-form4').attr("action", path);
        })
    </script>

    <script type="text/javascript">
        function openRequestStep(user_id)
        {
            window.sessionStorage.wantToRequestKost = JSON.stringify([]);
            window.sessionStorage.validPriceKost = JSON.stringify([]);
            function verifyOwner(user_id)
            {
                Swal.getConfirmButton().setAttribute('disabled', '');
                var request = new XMLHttpRequest();

                request.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                        var responseData = JSON.parse(this.responseText);
                        var message = responseData.meta.message;
                        var status = responseData.status;

                        var htmlContent = '';
                        if (status) {
                            htmlContent = '<div style="text-align:center">';
                            htmlContent += '<i class="fa fa-check" style="font-size: 90px;color: #5cb85c;"></i>'
                            htmlContent += '<h3>'+message+'</h3>';
                            htmlContent += '</div>';
                            Swal.getConfirmButton().removeAttribute('disabled');
                        } else {
                            htmlContent = '<div style="text-align:center">';
                            htmlContent += '<i class="fa fa-exclamation" style="font-size: 90px;color: #d9534f;"></i>'
                            htmlContent += '<h3>'+message+'</h3>';
                            htmlContent += '<a href="/admin/mamipay-owner" class="btn btn-primary">Aktifkan di MamiPAY Owner</a>';
                            htmlContent += '</div>';
                        }

                        $(Swal.getContent()).html(htmlContent);
                    }
                };

                request.open('GET', '/admin/booking-request-wizard/validate-user/'+user_id);
                request.send()
            }

            $.fn.setClickedData = function(obj)
            {
                var kostId = parseInt($(obj).val());
                var currentData = JSON.parse(window.sessionStorage.wantToRequestKost);

                var normalizeData = _.remove(currentData, function(n) {
                    return n == kostId;
                });

                if ($(obj).is(':checked')) {
                    currentData.push(kostId);
                }

                window.sessionStorage.wantToRequestKost = JSON.stringify(currentData.sort());
            }

            $.fn.checkValidPrice = function(obj)
            {
                var kostId = parseInt($(obj).val());
                var currentData = JSON.parse(window.sessionStorage.wantToRequestKost);
                var validPrice = JSON.parse(window.sessionStorage.validPriceKost);

                var normalizeData = _.remove(validPrice, function(n) {
                    return n == kostId;
                });

                if ($(obj).is(':checked')) {
                    validPrice.push(kostId);
                }

                window.sessionStorage.validPriceKost = JSON.stringify(validPrice.sort());

                if (window.sessionStorage.validPriceKost  !== window.sessionStorage.wantToRequestKost){
                    Swal.getConfirmButton().setAttribute('disabled','');
                } else {
                    Swal.getConfirmButton().removeAttribute('disabled');
                }
            }

            $.fn.validateSelectedKost = function (limit=5,offset=0)
            {
                Swal.getConfirmButton().setAttribute('disabled', '');
                var currentData = JSON.parse(window.sessionStorage.wantToRequestKost);

                if (currentData.length == 0) {
                    alert('Mohon pilih property terlebih dahulu');
                    return false;
                } else {
                    var request = new XMLHttpRequest();
                    request.onreadystatechange = function() {
                        if (this.readyState == 4 && this.status == 200) {
                            var responseData = JSON.parse(this.responseText);
                            var message = responseData.meta.message;
                            var status = responseData.status;

                            var htmlContent = '';
                            if (status) {
                                htmlContent += '<i class="fa fa-check" style="font-size: 90px;color: #5cb85c;"></i>'
                                htmlContent+='<table class="table" style="text-align:left; font-size:14px">';
                                $.each(responseData.validated_rooms,function(index,value){
                                    htmlContent+='<tr>';
                                    htmlContent+='<td colspan=3 style="background-color:#f4f4f4;">'+value.kost_name+'</td>';
                                    htmlContent+='<td tyle="background-color:#f4f4f4;"><input type="checkbox" value="'+value.kost_id+'" onclick="$.fn.checkValidPrice(this)">Konfirmasi harga benar</td>';
                                    htmlContent+='</tr>';
                                    htmlContent+='<tr>';
                                    htmlContent+='<td>Harian</td>';
                                    htmlContent+='<td>'+value.price.daily+'</td>';
                                    htmlContent+='<td>3-Bulanan</td>';
                                    htmlContent+='<td>'+value.price.quarterly+'</td>';
                                    htmlContent+='</tr>';
                                    htmlContent+='<tr>';
                                    htmlContent+='<td>Mingguan</td>';
                                    htmlContent+='<td>'+value.price.weekly+'</td>';
                                    htmlContent+='<td>6-Bulanan</td>';
                                    htmlContent+='<td>'+value.price.semiannualy+'</td>';
                                    htmlContent+='</tr>';
                                    htmlContent+='<tr>';
                                    htmlContent+='<td>Bulanan</td>';
                                    htmlContent+='<td>'+value.price.monthly+'</td>';
                                    htmlContent+='<td>Tahunan</td>';
                                    htmlContent+='<td>'+value.price.yearly+'</td>';
                                    htmlContent+='</tr>';
                                });
                                htmlContent+='</table>';
                                Swal.update({
                                    'title' : 'Check Harga'
                                });
                            } else {
                                htmlContent += '<i class="fa fa-exclamation" style="font-size: 90px;color: #d9534f;"></i>'
                                htmlContent+='<table class="table" style="text-align:left;">';
                                $.each(responseData.validated_rooms,function(index,value){
                                    htmlContent+='<tr>';
                                    htmlContent+='<td>'+value.kost_name+'</td><td>'+value.kost_status+'</td>';
                                    htmlContent+='</tr>';
                                });
                                htmlContent+='</table>';
                                htmlContent+='<p style="color:red;">'+message+'</p>';
                                Swal.getConfirmButton().setAttribute('disabled','');
                            }

                            var pagination = '';

                            if (responseData.total_item > 1) {
                                var perPage = limit;
                                var pageCount = Math.ceil(responseData.total_item/perPage);
                                var activePage = responseData.active_page;

                                pagination = '<div id="property-pagination"><ul class="pagination">';
                                var offset = 0;
                                for(var x=1; x <= pageCount; x++) {
                                    var active = activePage == x ? ' active' : '';
                                    pagination += '<li class="page-item'+active+'"><a class="page-link" href="javascript:$.fn.validateSelectedKost('+perPage+','+offset+');">'+x+'</a></li>';
                                    offset = x * perPage;
                                }
                                pagination+= '</ul></div>';

                            }

                            htmlContent += pagination;

                            $(Swal.getContent()).html(htmlContent);
                        }
                    };

                    request.open('POST', '/admin/booking-request-wizard/validate-kost?limit='+limit+'&offset='+offset);
                    request.setRequestHeader("Content-type", "application/json");
                    request.send(JSON.stringify({'kost_ids':currentData}));
                }
            }

            $.fn.getKostByOwnerId = function(user_id,limit=5,offset=0)
            {
                var request = new XMLHttpRequest();

                if ($(Swal.getContent()).find('.table')) {
                    $(Swal.getContent()).find('.table').css({'opacity':'0.5'});
                } else {
                    $(Swal.getContent()).html('mengambil data ...');
                }

                Swal.getConfirmButton().setAttribute('disabled', '');

                request.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                        var responseData = JSON.parse(this.responseText);
                        var message = responseData.meta.message;
                        var status = responseData.status;

                        var htmlContent = '';
                        if (status) {
                            var currentData = JSON.parse(window.sessionStorage.wantToRequestKost);

                            htmlContent = '<table class="table" style="text-align:left;">';
                            $.each(responseData.rooms,function(i,v){
                                var disabled = v.is_booking ? ' disabled':'';
                                var checked = currentData.includes(v.id) ? ' checked' : '';

                                htmlContent+='<tr>';
                                htmlContent+='<td style="width:32px"><input'+disabled+checked+' type="checkbox" onclick="$.fn.setClickedData(this)" value="'+v.id+'"/></td>';
                                htmlContent+='<td>'+v.name+'</td>';
                                htmlContent+='<td>'+v.label_info+'</td>';
                                htmlContent+='</tr>';
                            });
                            htmlContent += '</table>';

                            var pagination = '';

                            if (responseData.total_item > 1) {
                                var perPage = limit;
                                var pageCount = Math.ceil(responseData.total_item/perPage);
                                var activePage = responseData.active_page;

                                pagination = '<div id="property-pagination"><ul class="pagination">';
                                var offset = 0;
                                for(var x=1; x <= pageCount; x++) {
                                    var active = activePage == x ? ' active' : '';
                                    pagination += '<li class="page-item'+active+'"><a class="page-link" href="javascript:$.fn.getKostByOwnerId('+user_id+','+perPage+','+offset+');">'+x+'</a></li>';
                                    offset = x * perPage;
                                }
                                pagination+= '</ul></div>';

                            }

                            htmlContent += pagination;

                            htmlContent += '<a href="/admin/room#room" target="_blank" class="btn btn-sm btn-primary">Kost belum ada didalam list</a>';
                            Swal.getConfirmButton().removeAttribute('disabled');
                        }

                        $(Swal.getContent()).html(htmlContent);
                    }
                };

                request.open('GET', '/admin/booking-request-wizard/get-room/'+user_id+'?limit='+limit+'&offset='+offset);
                request.send()
            }

            $.fn.submitBBKRequest = function(user_id)
            {
                var request = new XMLHttpRequest();
                request.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                        var responseData = JSON.parse(this.responseText);
                        var message = responseData.meta.message;
                        var status = responseData.status;

                        var htmlContent = '';
                        if (status) {
                            htmlContent += '<i class="fa fa-check" style="font-size: 90px;color: #5cb85c;"></i>';
                            htmlContent += '<p style="text-align:center;">'+message+'</p>';
                            Swal.getConfirmButton().removeAttribute('disabled');
                        } else {
                            htmlContent += '<i class="fa fa-exclamation" style="font-size: 90px;color: #d9534f;"></i>';
                            htmlContent += '<p style="text-align:center;">'+message+'</p>';
                        }

                        $(Swal.getContent()).html(htmlContent);
                    }
                };

                var currentData = JSON.parse(window.sessionStorage.wantToRequestKost);
                request.open('POST', '/admin/booking-request-wizard/create-request');
                request.setRequestHeader("Content-type", "application/json");
                request.send(JSON.stringify({'kost_ids':currentData,'user_id':user_id}));
            }


            Swal.mixin({
                confirmButtonText: 'Selanjutnya &rarr;',
                showCancelButton: true,
                cancelButtonText: 'Batal',
                width: 800,
                progressSteps: ['1', '2', '3','4']
            }).queue(
                [
                    {
                        title: 'Verifikasi Owner',
                        html: 'verifikasi..',
                        onOpen: () => {
                            verifyOwner(user_id);
                        }
                    },
                    {
                        title: 'Pilih Property',
                        html: 'mengambil data..',
                        onOpen: () => {
                            $.fn.getKostByOwnerId(user_id);
                        },
                        preConfirm: () => {
                            return $.fn.validateSelectedKost();
                        }
                    },
                    {
                        title: 'Verifikasi Property',
                        text: 'verifikasi...',
                        onOpen: () => {
                            Swal.getConfirmButton().setAttribute('disabled','');
                        },
                    },
                    {
                        title: 'Selesai',
                        text: 'mengirim request...',
                        confirmButtonText: 'Selesai',
                        onOpen: () => {
                            Swal.getConfirmButton().setAttribute('disabled','');
                            $.fn.submitBBKRequest(user_id);
                        }
                    }
                ]
            );
        }

        $(function()
        {
            $('#select-sort-option').change(function(event){
                var url  = '{{ URL::route("admin.room.index") }}' + '?o=' + event.target.value ;

                url += '&st=' + $('#start-date').val();
                url += '&en=' + $('#end-date').val();

                window.location = url;

            });

            $('.datepicker').datepicker({ dateFormat : 'dd-mm-yy'});
        });

        $(function(){


        var token = '{{ csrf_token() }}';

        $('.btn-delete').click(function() {
            var id = $(this).data('id');
            var name = $(this).data('name');

            var confirm = window.confirm("Are you sure want to delete " + name +" ?");

            if (confirm) {
                $.post('/{{Request::segment(1)}}/{{Request::segment(2)}}/' + id, {
                    _method: "delete",
                    _token: token,
                }).success(function(response) {
                    console.log(response);
                    window.location.reload();
                });
            }


        });

    });

    $('#check-old-name-data').click(function(e) {
      oldname = $("#data_name").val();
      $("#kost_show").append("Loading ................ ");
      $.ajax({
        url: '/admin/owner/check-old-name?old_name=' + oldname,
        type: 'GET',
        dataType: 'json',
        success: function(data) {
          $("#kost_show").empty();
          if(data.status == false) {

            $("#kost_show").append("<div class='alert alert-danger' style='width: 100%; margin-top: 30px;'>Data tidak ditemukan.");
          } else {
            var project = "<strong>Results</strong>";

            $.each(data.data, function(index, value) {
                project += "<ul>";
                project += "<li>"+value.revisionable_id+" - "+value.old_value+" <span style='color: #ff0000; font-weight:bold;'>ke</span> "+value.new_value+" ("+value.created_at+") ";
                project += "</ul>";
            });

            $("#kost_show").append(project);
          }
        }
      });
    });
    </script>
    @stack('script_stacks')
@endsection