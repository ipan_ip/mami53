@extends('admin.layouts.main')

@section('style')
<style>
    .table>tbody>tr>td {
        vertical-align: middle;
    }

    .font-large {
        font-size: 1.5em;
    }

    .font-semi-large {
        font-size: 1.15em;
    }

    .font-grey {
        color: #9E9E9E;
    }

    .dropdown-menu>li>a:hover {
        color: #333
    }

    .label-grey {
        background-color: #CFD8DC;
    }

    [hidden] {
        display: none !important;
    }
</style>
<link rel="stylesheet" href="//use.fontawesome.com/releases/v5.5.0/css/all.css" crossorigin="anonymous">
@endsection

@section('content')
<!-- table -->
<div class="box box-default">
    <!-- Box Header -->
    <div class="box-header">
        <div class="col-md-12 text-center">
            <h3 class="box-title"><i class="fa fa-trophy"></i> {{ $boxTitle }}</h3>
        </div>
    </div>
    <!-- /.box-header -->

    <div class="box-body no-padding">
        {{-- Top Bar --}}
        <div class="horizontal-wrapper">
            <div class="btn-horizontal-group bg-default form-inline" style="padding-left: 10px;padding-right: 10px;">
                <!-- Add button -->
                <div class="col-md-6 bg-default" style="padding-bottom:10px;">
                    <form action="{{ url('/admin/loyalty/import') }}" class="form-horizontal" method="post"
                        enctype="multipart/form-data">
                        @csrf
                        <label class="btn btn-default">
                            Browse File <input type="file" name="file" hidden>
                        </label>
                        <button class="btn btn-success">
                            Upload&nbsp;<i class="fa fa-paper-plane"></i>
                        </button>
                    </form>
                </div>
                <!-- Search filters -->
                <div class="col-md-6 bg-default text-right inline-form" style="padding-bottom:10px">
                    <form action="" method="get">
                        <input type="text" name="q" class="form-control input-sm" placeholder="Name / Phone Number"
                            autocomplete="off" value="{{ request()->input('q') }}">
                        <button class="btn btn-primary btn-md" id="buttonSearch"><i
                                class="fa fa-search">&nbsp;</i>Search</button>
                    </form>
                </div>
            </div>
        </div>

        {{-- Table --}}
        <table class="table table-striped">
            <thead>
                <tr>
                    <th></th>
                    <th class="text-center">Data Date</th>
                    <th>Owner</th>
                    <th class="text-center">Booking Activation Date</th>
                    <th class="text-center">Total Kost(s)</th>
                    <th class="text-center">Fast Booking Response</th>
                    <th class="text-center">Booking Transactions</th>
                    <th class="text-center">Review From Tenants</th>
                    <th class="text-center">Loyalty Points</th>
                    <th class="text-center">Uploder</th>
                </tr>
            </thead>
            <tbody>
                @if (!count($rowsOwner))
                <tr>
                    <td class="text-center" colspan="11">
                        <div class="callout callout-danger">Tidak ada data untuk ditampilkan!<br>Klik <strong>[Browse File]</strong> lalu klik <strong>[Upload]</strong> untuk menambahkan data baru</div>
                    </td>
                </tr>
                @else
                @foreach($rowsOwner as $index => $owner)
                <tr>
                    <td class="text-center">
                        {{ $index + 1 }}
                    </td>
                    <td class="text-center">
                        {{ date('d/m/Y', strtotime($owner->data_date)) }}
                    </td>
                    <td>
                        <strong class="font-semi-large">{{ $owner->user->name }}</strong>
                        <br><small>{{ $owner->user->phone_number }}</small>
                        <br><small>{{ $owner->user->email }}</small>

                    </td>
                    <td class="text-center">
                        {{ date('d/m/Y', strtotime($owner->booking_activation_date)) }}
                    </td>
                    <td class="text-center">
                        {{ $owner->total_listed_room }}
                    </td>
                    <td class="text-center">
                        {{ $owner->total_booking_response }}
                    </td>
                    <td class="text-center">
                        {{ $owner->total_booking_transaction }}
                    </td>
                    <td class="text-center">
                        {{ $owner->total_review }}
                    </td>
                    <td class="text-center">
                        <strong>{{ $owner->total }}</strong>
                    </td>
                    <td class="text-center">
                        @if ($owner->importer)
                        {{ $owner->importer->name }}
                        <br><small>{{ $owner->importer->role }}</small>
                        @else
                        -
                        @endif
                    </td>
                </tr>
                @endforeach
                @endif
            </tbody>

        </table>
    </div><!-- /.box-body -->
    {{-- pagination --}}
    @if (count($rowsOwner))
    <div class="box-body text-center">
        {{ $rowsOwner->appends(Request::except('page'))->links() }}
    </div>
    @endif
    {{-- /.pagination --}}
</div><!-- /.box -->
<!-- table -->
<div id="wizard"></div>
@endsection
@section('script')
<script type="text/javascript">
    $(function(){
        // 
    });
</script>
@endsection