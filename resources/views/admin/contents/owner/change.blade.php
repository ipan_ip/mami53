@extends('admin.layouts.main')
@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }} {{ $room->name }}</h3>
           
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <form class="form-horizontal" method="post" action="">
              <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Room Name</label>
                <div class="col-sm-10">
                  <input type="text" disabled="true" name="name" value="{{ $room->name }}" class="form-control" id="inputEmail3" placeholder="Phone number">
                </div>
              </div>

              @if (count($room->owners) > 0)
                  <div class="form-group"> 
                     <label for="inputEmail3" class="col-sm-2 control-label">Owner</label>
                     <div class="col-sm-10">
                        <input type="text" class="form-control" name="previous_owner" value="{{ $room->owners[0]->user->phone_number }}" disabled="true">
                        <input type="hidden" value="{{ $room->owners[0]->status }}" name="status_kost">
                     </div>
                  </div>   
              @endif
              
              <div class="form-group"> 
              <label for="inputEmail3" class="col-sm-2 control-label">Status Owner</label>
              <div class="col-sm-10">
                <select class="form-control" name="owner_status">
                  @if ($room->apartment_project_id > 0) 
                    <option value="Pemilik Apartemen">Pemilik Apartemen</option>
                  @else
                    <option value="Pemilik Kos">Pemilik Kos</option>
                  @endif
                </select>
              </div>
              </div>

              <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">New Owner</label>
                <div class="col-sm-10">
                  <input type="text" name="owner_phone" value="" class="form-control" id="inputEmail3" placeholder="Phone number">
                </div>
              </div>
                     
              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <button type="submit" class="btn btn-success">Change owner</button>
                </div>
              </div>
            </form>
            <br/>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
    <!-- table -->

    @include('admin.partials.delete-modal')
@stop
@section('script')
<!-- page script -->
<script type="text/javascript">
    $(function()
    {
        $("#tableListTag").dataTable();
    });
</script>
@stop