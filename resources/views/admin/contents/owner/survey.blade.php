@extends('admin.layouts.main')

@section('style')
<style>
    .reject-remark-filled i.fa {
        color: #ff0000;
    }
</style>
@endsection

@section('content')
    <!-- table -->
    <div class="box box-primary">

        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
            <div class="box-tools pull-right">
                <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
            </div>
        </div><!-- /.box-header -->

        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                   
                </div>
            </div>


            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Owner Phone</th>
                        <th>For</th>
                        <th>Room Total</th>
                        <th>Room Name</th>
                        <th>Content</th>
                        <th>Date</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($survey as $index => $value)
                        <tr>
                            <td>{{ $index+1 }}</td>
                            <td>
                                @if (isset($value->user))
                                    {{ $value->user->id }} - {{ $value->user->phone_number }}
                                @endif
                            </td>
                            <td>
                                @if ($value->for == 'update_room')
                                    <label class="label label-warning">{{ $value->for }}</label>
                                @else 
                                    <label class="label label-danger">{{ $value->for }}</label>
                                @endif
                            </td>
                            <td>{{ $value->room_count }}</td>
                            <td>
                                @if ($value->for == 'update_room')
                                    {{ $value->room->name }}
                                @endif
                            </td>
                            <td>{{ $value->content }}</td>
                            <td>{{ $value->created_at->format('d M Y H:i') }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

        </div><!-- /.box-body -->
        <div class="box-body no-padding">
            {{ $survey->appends(Request::except('page'))->links() }}
        </div>
    </div><!-- /.box -->
    <!-- table -->
@endsection