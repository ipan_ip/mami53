@extends('admin.layouts.main')

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Reject History</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div><!-- /.box-tools -->
        </div><!-- /.box-header -->
        <div class="box-body" style="margin-left: 20px;">
            <div id="history">
            </div>
        </div><!-- /.box-body -->
    </div><!-- /.box -->

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Reject Reason</h3>
        </div><!-- /.box-header -->
        <div class="box-body" style="margin-left: 20px;">
            <div class="form-group" id="formGroup">
                <h4>Alamat Kos</h4>
                <div id="address">
                </div>
                <h4>Foto Bangunan</h4>
                <div id="building_photo">
                </div>
                <h4>Foto Kamar</h4>
                <div id="room_photo">
                </div>
                <h4>Iklan Kos</h4>
                <div id="kost_ads">
                </div>
                <h4>Iklan Apartemen</h4>
                <div id="apartment_ads">
                </div>
                <button id="submitButton" type="button" style="margin-top: 25px;" class="btn btn-success">Reject</button>
            </div>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
@stop


@section('style')
<style>
    /* The container */
    .container {
    display: block;
    position: relative;
    padding-left: 25px;
    margin-bottom: 7px;
    cursor: pointer;
    font-size: 14px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    font-weight:normal;
    margin-left: 0px;
    }

    /* Hide the browser's default checkbox */
    .container input {
    position: absolute;
    opacity: 0;
    cursor: pointer;
    height: 0;
    width: 0;
    }

    /* Create a custom checkbox */
    .checkmark {
    position: absolute;
    top: 0;
    left: 0;
    height: 21px;
    width: 21px;
    background-color: #eee;
    }

    /* On mouse-over, add a grey background color */
    .container:hover input ~ .checkmark {
    background-color: #ccc;
    }

    /* When the checkbox is checked, add a blue background */
    .container input:checked ~ .checkmark {
    background-color: #2196F3;
    }

    /* Create the checkmark/indicator (hidden when not checked) */
    .checkmark:after {
    content: "";
    position: absolute;
    display: none;
    }

    /* Show the checkmark when checked */
    .container input:checked ~ .checkmark:after {
    display: block;
    }

    /* Style the checkmark/indicator */
    .container .checkmark:after {
    left: 9px;
    top: 5px;
    width: 5px;
    height: 10px;
    border: solid white;
    border-width: 0 3px 3px 0;
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(45deg);
    }

    .align-left {
        text-align: left;
    }
</style>
@endsection

@section('script')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $.get('/admin/api/kost/reject-reason')
        .done(function (data) {
            // alert(JSON.stringify(data.data.building_photo));

            var sections = [
                "address",
                "building_photo",
                "room_photo",
                "kost_ads",
                "apartment_ads"
            ];

            sections.forEach(section => {
                data.data[section].forEach(element => {
                    if (element.content != null) {
                        $('#'+section).append(`<label class="container">${element.content}<input type="checkbox" name="reasonCheck" value="${element.id}"><span class="checkmark"></span></label>`)
                    } else {
                        $('#'+section).append(`<label class="container">Lainnya<input type="checkbox" name="otherCheck" value="${element.id}" id="otherCheck" onclick="updateTextArea();"><span class="checkmark"></span></label><textarea disabled maxlength="200" style="width: 500px; margin-left: 25px;" id="${element.id}" rows="3"></textarea>`)
                    }
                });
            });
        })

        $.get('/admin/api/kost/reject/' + '{{ request()->id }}')
        .done(function (data) {
            data.data.history.forEach(element => {
                appendHistory(element);
            });
        })

        function constructReason(reason) {
            var mapObj = {
                "Alamat Kos: ":"<br><strong>Alamat Kos:</strong><br>",
                "Foto Bangunan: ":"<br><strong>Foto Kos:</strong><br>",
                "Foto Kamar: ":"<br><strong>Foto Kamar:</strong><br>",
                "Iklan Kos: ":"<br><strong>Iklan Kos:</strong><br>",
                "Iklan Apartemen: ":"<br><strong>Iklan Apartemen:</strong><br>"
            };
            var temp = reason.replace(/Alamat Kos: |Foto Bangunan: |Foto Kamar: |Iklan Kos: |Iklan Apartemen: /gi, function(matched){
                return mapObj[matched];
            });

            return temp;
        }

        $('#submitButton').click(function (e) {
            document.getElementById("submitButton").disabled = true;

            var reject_reason_ids = [];
            var reject_reason_other = [];
            var isReasonOtherFilled = true;
            $("input:checkbox[name=reasonCheck]:checked").each(function(){
                reject_reason_ids.push($(this).val());
            });
            $("input:checkbox[name=otherCheck]:checked").each(function(){
                if (document.getElementById($(this).val()).value.length > 0) {
                    reject_reason_other.push({
                        "id": $(this).val(),
                        "content": document.getElementById($(this).val()).value
                    });
                } else {
                    isReasonOtherFilled = false;
                }
            });

            if (isReasonOtherFilled == false) {
                Swal.fire({
                    icon: 'error',
                    title: 'Alasan lainnya tidak boleh kosong!'
                })
                document.getElementById("submitButton").disabled = false;
            } else if (reject_reason_ids.length > 0 || reject_reason_other.length > 0) {
                $.get('/admin/api/kost/reject-preview', {reject_reason_ids: reject_reason_ids, reject_reason_other: reject_reason_other})
                .done(function (data) {
                    var reason = constructReason(data.data.reason);

                    Swal.fire({
                        title: 'Send Reject',
                        html: `<div class="align-left"><h5>${reason}</h5></div>`,
                        showCancelButton: true,
                        confirmButtonText: `Send`,
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        width: 600
                    })
                    .then((result) => {
                        if (result.isConfirmed) {
                            sendReject(reject_reason_ids, reject_reason_other);
                        } else if (result.dismiss === Swal.DismissReason.cancel) {
                            document.getElementById("submitButton").disabled = false;
                        }
                    })
                })
                .fail(function (error) {
                    Swal.fire({
                        icon: 'error',
                        title: error.responseJSON.meta.message
                    })
                    document.getElementById("submitButton").disabled = false;
                })
            } else {
                Swal.fire({
                    icon: 'error',
                    title: 'Pilih alasan ditolak terlebih dahulu!'
                })
                document.getElementById("submitButton").disabled = false;
            }
        });

        function sendReject(reject_reason_ids, reject_reason_other) {
            $.ajax({
                type: 'POST',
                url: '/admin/api/kost/reject/' + '{{ request()->id }}',
                data: {
                    reject_reason_ids: reject_reason_ids,
                    reject_reason_other: reject_reason_other,
                },
                dataType: 'json',
                success: (res) => {
                    Swal.fire({
                        icon: 'success',
                        title: 'Reject Kos berhasil'
                    });

                    location.reload();           
                },
                error: (error) => {
                    Swal.fire({
                        icon: 'error',
                        title: error.responseJSON.meta.message
                    })
                    document.getElementById("submitButton").disabled = false;
                }
            });
        }

        function appendHistory(history) {
            $('#history').append(`<div style="font-size: 14px;"><b>${history.admin_name}</b> - <a href="#" id="${history.id}">See History</a> - ${history.date}</div>`)

            $('#'+history.id).click(function (e) {
                var reason = constructReason(history.reject_reason);

                Swal.fire({
                    title: 'Reject History',
                    html: `<div class="align-left"><h5><strong>${history.admin_name}</strong> ${history.date}<br>${reason}</h5></div>`,
                    showCancelButton: false,
                    showConfirmButton: false,
                    showCloseButton: true,
                    width: 600
                })
            }) 
        }
    });
</script>
<script type="text/javascript">
    function updateTextArea() {
        $("input:checkbox[name=otherCheck]:checked").each(function(){
            document.getElementById($(this).val()).disabled = false;
        });
        $("input:checkbox[name=otherCheck]:not(:checked)").each(function(){
            document.getElementById($(this).val()).disabled = true;
        });
    }
</script>
@endsection