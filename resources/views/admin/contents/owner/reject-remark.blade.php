@extends('admin.layouts.main')

@section('content')
	<div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

		<div class="row">
			<div class="col-md-12" style="padding: 0px 40px;">
				@foreach ($reject_reason as $key => $value)
					<p style="margin: 10px;"><strong>{{ isset($value->user) ? $value->user->name : "" }}</strong> - {{ $value->content }} - {{ $value->created_at->format("d M Y H:i") }}</p>
				@endforeach
			</div>
		</div>

		<form action="{{ URL::route('admin.owner.reject.update') }}" method="POST" class="form-horizontal form-bordered" name="rejectRemarkForm">
			<input type="hidden" name="room_id" value="{{ $room->id }}">
			<div class="box-body no-padding">
				<div class="form-group bg-default">
					@foreach ($reject_reason_select as $value)
					<div class="radio col-sm-11 col-sm-push-1">
						<label><input type="radio" name="reject_remark" value="{{ $value }}"> {{ $value }}</label>
					</div>
					@endforeach
					<div class="radio col-sm-11 col-sm-push-1">
						<label><input type="radio" name="reject_remark" value="textarea" checked> Lainnya<div><br></div><textarea maxlength="300" style="width:1000px;" name="reject_remark_text" class="form-control" rows="4"></textarea></label>
					</div>
				</div>

				<div class="form-group bg-default">
					<div class="col-sm-11 col-sm-push-1">
						<button type="submit" onclick="this.disabled=true;this.form.submit();disableInput();" class="btn btn-danger">Unverify</button>
					</div>
				</div>
			</div>
		</form>
    </div>
@endsection

@section('script')
<script>
function disableInput(){
	var radios = document.rejectRemarkForm.reject_remark;

	for (var i=0, iLen=radios.length; i<iLen; i++) {
		radios[i].disabled = true;
	}
	document.rejectRemarkForm.reject_remark_text.disabled = true;
}
</script>
@endsection