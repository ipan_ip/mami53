@extends('admin.layouts.main')
@section('content')
  <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">{{ $boxTitle }}</h3>
    </div>
    {{ Form::open(array('url' => route('admin.call.reply', array($call->id, 'page' => Input::get('page',1), 'from' => Input::get('from'), 'to' => Input::get('to') )), 'method' => 'post', 'class' => 'form-horizontal form-bordered')) }}
      <div class="box-body no-padding">
        <div class="form-group bg-default">
          <label class="col-sm-2 control-label">From</label>
          <div class="col-sm-10">
            <input name="user_phone_number" type="text" class="form-control" value="{{ $call->phone }}" placeholder="User Number" disabled>
            <div id="countTitle" style="font-weight:bold"></div>
          </div>
        </div>

        <div class="form-group bg-default">
          <label class="col-sm-2 control-label">Date</label>
          <div class="col-sm-10">
            <input name="date" type="text" class="form-control" value="{{ $call->created_at->format('d-m-Y, H:i') }}" placeholder="Date" disabled>
            <div id="countTitle" style="font-weight:bold"></div>
          </div>
        </div>

        <div class="form-group bg-default">
          <label class="col-sm-2 control-label">Message/Note</label>
          <div class="col-sm-10">
            <textarea class="form-control" disabled>{{ $call->note }}</textarea>
            <div id="countTitle" style="font-weight:bold"></div>
          </div>
        </div>

        <div class="form-group bg-info divider">
          <div class="col-sm-12 pull-left">
            Recent Reply
          </div>
        </div>

        <div class="form-group bg-default">
          <label for="message" class="col-sm-2 control-label"></label>
          <div class="col-sm-10">
            <textarea rows="4" class="form-control" disabled>{{ $reply->created_at->format('d-m-Y H:i') ."\n\n" . $reply->message }}</textarea>
            <div id="countTitle" style="font-weight:bold"></div>
          </div>
        </div>

        <div class="form-group bg-info divider">
          <div class="col-sm-12 pull-left">
            Your Reply
          </div>
        </div>



        <div class="form-group bg-default {{ ($errors->has('message')) ? 'has-error' : '' }}">
          <label for="message" class="col-sm-2 control-label">Message</label>
          <div class="col-sm-10">
            <textarea name="message" class="form-control"></textarea>
            {{ ($errors->has('message')) ? '<span class="help-block">' . $errors->first('message') . '</span>' : '' }}
            <div id="countTitle" style="font-weight:bold"></div>
          </div>
        </div>

        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-primary">Send</button>
                <a href="{{ route('admin.call.index') }}">
              <button type="button" class="btn btn-default">Cancel</button>
            </a>

          </div>
        </div>
      </div>
    {{ Form::close() }}
  </div>
@stop

@section('script')

<script>

    var $number = $('input[name=user_phone_number');
    // var msg = $('textarea[name=message').val();

    $('form').on('submit',function () {
        var conf = confirm("Are you sure want to send message to " + $number.val() + " ?");
        $number.attr('disabled', false);

        return conf;
    });

</script>

@stop
