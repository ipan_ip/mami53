@extends('admin.layouts.main')
@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>
        {{ Form::open(array('url' => $actionUrl, 'method' => $actionMethod, 'class' => 'form-horizontal form-bordered')) }}
        <div class="box-body no-padding">
            <div class="form-group bg-default">
                {{ Form::label('message', 'Message', array('class' =>' col-sm-2 control-label')) }}
                <div class="col-sm-10">
                    {{ Form::textarea('message', $rowSms->message, array('class'=>'form-control', 'placeholder'=> 'Message', 'rows'=>5)) }}
                    <div id="countTitle" style="font-weight:bold"></div>
                </div>
            </div>
            <div class="form-group bg-default">
                {{ Form::label('city', 'City', array('class' =>' col-sm-2 control-label')) }}
                <div class="col-sm-10">
                      {{ Form::select('city[]',  $cities, $rowSms->city, array('id'=> 'inputCity', 'class'=>'form-control chosen-select', 'data-placeholder'=> 'Select A City', 'multiple'=>'')) }}
                </div>
            </div>

            <div class="form-group bg-info divider">
              <div class="col-sm-12 pull-left">
              </div>
            </div>

            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.sms.index') }}">
                  <button type="button" class="btn btn-default">Cancel</button>
                </a>

              </div>
            </div>
      </div>
    {{ Form::close() }}
  </div>
@stop

@section('style')
    {{ HTML::style('assets/vendor/jquery-chosen/chosen.css') }}
    {{ HTML::style('assets/vendor/jquery-chosen-custom/chosen.css') }}
@stop

@section('script')
    {{ HTML::script('assets/vendor/jquery-chosen/chosen.jquery.js') }}
    <script type="text/javascript">
      $(function()
      {
        var config = {
          '.chosen-select'           : {width: '100%'}
        };
        for (var selector in config) {
          $(selector).chosen(config[selector]);
        }
      });
    </script>
@stop
