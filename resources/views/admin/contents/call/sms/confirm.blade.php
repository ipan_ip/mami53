@extends('admin.layouts.main')
@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
            <div class="box-tools pull-right">
                <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default" style="padding-left : 20px">
                    {{ Form::open(array('url'=>$sendAction,'method'=>$sendMethod)) }}
                    <button class="btn btn-md btn-success pull-right" style="margin-right:20px">
                        <i class="fa fa-paper-plane" data-widget="collapse"></i>&nbsp;&nbsp;Send Message</button>
                    {{ Form::close() }}
                    Found {{ sizeof($rowsDesigner) }} rooms. <br/>
                    Found {{ $numberCount }} unique numbers.
                </div>
            </div>

            <table id="tableListDesigner" class="table table-striped">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Kost</th>
                        <th>City</th>
                        <th>Phone</th>
                        <th>Owner Phone</th>
                        <th>Manager Phone</th>
                    </tr>
                </thead>
                <tbody>
                  @foreach($rowsDesigner as $key => $rowDesigner)
                    <tr>
                        <th>{{ $key + 1 }}</th>
                        <td>{{ $rowDesigner->name }}</td>
                        <td>{{ $rowDesigner->area_city }}</td>
                        <td>{{ $rowDesigner->phone }}</td>
                        <td>{{ $rowDesigner->owner_phone }}</td>
                        <td>{{ $rowDesigner->manager_phone }}</td>
                    </tr>
                  @endforeach
                </tbody>
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Kost</th>
                        <th>City</th>
                        <th>Phone</th>
                        <th>Owner Phone</th>
                        <th>Manager Phone</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@stop

@section('style')
    <!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> -->
@stop

@section('script')
<!-- Holder for Upload File Thumbnail -->
{{ HTML::script('assets/vendor/holder/holder.js') }}

<!-- Jquery Datepicker -->
<!-- <script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script> -->

<!-- page script -->

@stop
