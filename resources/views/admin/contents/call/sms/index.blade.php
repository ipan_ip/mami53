@extends('admin.layouts.main')
@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
            <div class="box-tools pull-right">
                <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                <a href="{{ $createAction }}">
                    <button class="btn btn-add btn-primary btn-sm"><i class="fa fa-plus">&nbsp;</i> New Message</button>
                </a>
                </div>
            </div>
            <div class="horizontal-wrapper" style="display:none" id="formStorySearch">

                <div class="btn-horizontal-group bg-default row">
                    <div class="col-sm-10">
                        <input type="text" name="search_keyword" class="form-control" />
                    </div>
                    <div class="col-sm-2">
                        <button class="btn btn-sm btn-info">Search</button>
                    </div>
                </div>

            </div>
            <table id="tableListDesigner" class="table table-striped">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Message</th>
                        <th>City</th>
                        <th class="table-action-column">Action</th>
                        <th class="table-action-column">Send Button</th>
                    </tr>
                </thead>
                <tbody>
                  @foreach($rowsSms as $key => $rowSms)
                    <tr>
                        <th>{{ $key + 1 }}</th>
                        <td>{{ $rowSms->message }}</td>
                        <td>{{ $rowSms->city_list }}</td>
                        <td class="table-action-column">
                            <a href="{{ URL::route($editRoute, array($rowSms->id)) }}">
                                <i class="fa fa-pencil"></i></a>
                        </td>
                        <td class="table-action-column"><a href="{{ url('admin/sms/confirm/' . $rowSms->id)  }}" class="btn btn-sm btn-primary">Send</a></td>
                    </tr>
                  @endforeach
                </tbody>
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Message</th>
                        <th>City</th>
                        <th class="table-action-column">Action</th>
                        <th class="table-action-column">Send Button</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@stop

@section('style')
    <!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> -->
@stop

@section('script')
<!-- Holder for Upload File Thumbnail -->
{{ HTML::script('assets/vendor/holder/holder.js') }}

<!-- Jquery Datepicker -->
<!-- <script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script> -->

<!-- page script -->

@stop
