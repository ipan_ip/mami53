@extends('admin.layouts.main')
@section('content')
    <?php App::setLocale('id'); ?>
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">List Call</h3>
            <div class="box-tools pull-right">
                <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default" style="overflow:hidden">
                    <form action="" class="form-inline" method="get" style="text-align: right">
                        <input type="text" name="q" class="form-control input-sm"  placeholder="Nama Kost" autocomplete="off" value="{{ Input::get('q') }}">
                        <input name="from" class="form-control input-sm datepicker" type="text" placeholder="Start Date" value="{{ Request::get('from') }}" autocomplete="off" id="start-date" style="margin:2px" />
                        <input name="to" class="form-control input-sm datepicker" type="text" placeholder="End Date" value="{{ Request::get('to') }}" autocomplete="off" id="end-date" style="margin:2px" />
                        <button class="btn btn-primary btn-md" id="buttonSearch"><i class="fa fa-search">&nbsp;</i>Search</button>
                    </form>
                </div>
            </div>
            <div class="horizontal-wrapper" style="display:none" id="formStorySearch">

                <div class="btn-horizontal-group bg-default row">
                    <div class="col-sm-10">
                        <input type="text" name="search_keyword" class="form-control" />
                    </div>
                    <div class="col-sm-2">
                        <button class="btn btn-sm btn-info">Search</button>
                    </div>
                </div>

            </div>
            <table id="tableListDesigner" class="table table-striped">
                <thead>
                    <tr>
                        <th>Designer ID</th>
                        <th>Designer</th>
                        <th>Status</th>
                        <th>Replies</th>
                        <th>Updated</th>
                        <th>Designer Contact</th>
                        <th>Name</th>
                        <th>Address</th>
                        <th>Phone</th>
                        <th>Note</th>
                        <th style="width:80px">Date</th>
                        <th class="table-action-column" width="80px">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($calls as $call)
                    <tr>
                        <td>
                            @if ($designer = $call->room)
                                <a href="{{ $currentPath .'/room/'. $designer->slug }}" target="_blank" rel="noopener">
                            @else
                                <a href="#" target="_blank" rel="noopener">{{ $call->id }}
                            @endif
                                    {{ $call->designer_id }}
                                </a>
                        </td>
                        @if($call->room)
                        <td>{{ $call->room->name }}</td>
                        <td>{{ $call->room->status ? 'Full'  : 'Available' }}</td>
                        <td>{{ sizeof($call->replies) ? 'Sudah' : 'Belum Pehh Jelek' }}</td>
                        <td>{{ $call->room->kost_updated_date }}</td>
                        @else
                        <td></td>
                        <td></td>
                        <td></td>
                        @endif
                        @if($call->room)
                        <td>{{ implode(', ', $call->room->getPhones()) }}</td>
                        @else
                        <td></td>
                        @endif
                        <td>{{ $call->name }}</td>
                        <td>{{ $call->room->address }}</td>
                        <td>{{ $call->phone }}</td>
                        <td>{{ $call->note }}</td>
                        <td>{{ $call->created_at->format('d-m-Y h:i') }}</td>
                        <td>
                            <a href="{{ route('admin.call.reply', array($call->id, 'page' => Input::get('page',1),'from' => Input::get('from'),'to' => Input::get('to'))) }}" class="btn btn-sm btn-primary">Reply</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $calls->appends(Request::except('page'))->links() }}
        </div>
    </div>
@stop

@section('style')
    <!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> -->
@stop

@section('script')
<!-- Holder for Upload File Thumbnail -->
{{ HTML::script('assets/vendor/holder/holder.js') }}

<!-- Jquery Datepicker -->
<!-- <script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script> -->

<!-- page script -->
<script type="text/javascript">

    $(function()
    {
        $('#select-sort-option').change(function(event){
            var url  = '{{ URL::route("admin.room.index") }}' + '?o=' + event.target.value ;

            url += '&st=' + $('#start-date').val();
            url += '&en=' + $('#end-date').val();

            window.location = url;

        });

        $('.datepicker').datepicker({ dateFormat : 'dd-mm-yy'});
    });
</script>
@stop
