@extends('admin.layouts.main')
@section('content')


<div class="box box-primary">
  <div class="box-header">
      <h3 class="box-title">{{ $boxTitle }} @if (isset($review->room->name)) {{ $review->room->name }} @endif </h3>
  </div>

  <div class="row" style="margin: 0px 10px;">
      <div class="col-md-12">
		    <div class="media">
			    <!-- <div class="media-left">
			      <img src="{{ URL::to('assets/vendor/admin-lte/img/avatar3.png') }}" class="media-object" style="width:60px">
			    </div> -->
			    <div class="media-body">
			      <h4 class="media-heading"><strong>{{ $review->user->name }}</strong> 
                       @if ($review->status == 'live')
	                       <span class="label label-success">{{ $review->status }}</span>
	                   @else
	                       <span class="label label-warning">{{ $review->status }}</span>
	                   @endif
			      </h4>
			      <p>Kebersihan: <strong>{{ $review->cleanliness }}</strong></p>
			      <p>Kenyamanan: <strong>{{ $review->comfort }}</strong></p>
			      <p>Keamanan: <strong>{{ $review->safe }}</strong></p>
			      <p>Harga: <strong>{{ $review->price }}</strong></p>
			      <p>Fasilitas ruangan: <strong>{{ $review->room_facility }}</strong></p>
			      <p>Fasilitas Umum: <strong>{{ $review->public_facility }}</strong></p>
			      <p>{{ $review->content }} <a href="{{ URL::to('admin/review', $review->id) }}" class="btn btn-xs btn-primary">edit</a></p>
                  
                  <div class="col-md-1"></div>
                  <div class="col-md-11">
	                  @if (count($reply) > 0)
	                     @foreach ($reply AS $key => $value)
							  <div class="media">
							    <div class="media-body">
							      <h4 class="media-heading">{{ $value->user->name }}
				                       @if ($value->status == '1')
					                       <span class="label label-success">Live</span>
					                   @else
					                       <span class="label label-warning">Waiting</span>
					                   @endif
							      </h4>
							      <p>{{ $value->content }}
                                     
                                       @if ($value->status == '0')
					                       <a href="{{ URL::to('admin/review/reply/confirm', $value->id) }}" class="btn btn-xs btn-primary">verifikasi</a>
					                   @else
					                       <a href="{{ URL::to('admin/review/reply/deleted', $value->id) }}" class="btn btn-xs btn-danger">hapus</a>
					                   @endif

							      </p>
							    </div>
							  </div>
	                     @endforeach  
	                  @else
	                    <span>Belum ada balasan</span>
	                  @endif 

                  </div>	

			    </div>
		    </div>

      </div>	
  </div>

</div>  
  
@stop