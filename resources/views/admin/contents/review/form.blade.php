@extends('admin.layouts.main')
@section('content')
<div class="box box-primary">
  <div class="box-header">
      <h3 class="box-title">{{ $boxTitle }}</h3>
  </div><!-- /.box-header -->
  {{ Form::open(array('route' => $formAction, 'method' => $formMethod,
      'class' => 'form-horizontal form-bordered', 'role' => 'form', 'id' => 'formInsertTag', 'enctype' => 'multipart/form-data')) }}
      <div class="box-body no-padding">
            <!-- select -->
            <div class="form-group bg-default">
                <label for="inputSelectTagType" class="col-sm-2 control-label">Select Type</label>
                <div class="col-sm-10">
                    <select class="form-control" id="inputSelectTagType" name="type">
                        @foreach ($rowsReviewTypes as $rowsReviewType)
                          @if ($data->status == $rowsReviewType)
                          <option value="{{ $rowsReviewType }}" selected="selected">{{ $rowsReviewType }}</option>
                          @else
                          <option value="{{ $rowsReviewType }}">{{ $rowsReviewType }}</option>
                          @endif
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group bg-default">
              <label for="inputName" class="col-sm-2 control-label">Kost Name</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" placeholder="Name"
                  id="inputName" name="name" value="{{ $data->room->name }}" disabled="true">
              </div>
            </div>

            <div class="form-group bg-default">
              <label for="inputIcon" class="col-sm-2 control-label">Rating Kebersihan</label>
              <div class="col-sm-10">
                  <input type="number" class="form-control" value="{{ $data->cleanliness }}" disabled="true">
              </div>
            </div>

            <div class="form-group bg-default">
              <label for="inputIcon" class="col-sm-2 control-label">Rating Kenyamanan</label>
              <div class="col-sm-10">
                  <input type="number" class="form-control" value="{{ $data->comfort }}" disabled="true">
              </div>
            </div>

            <div class="form-group bg-default">
              <label for="inputIcon" class="col-sm-2 control-label">Rating Keamanan</label>
              <div class="col-sm-10">
                  <input type="number" class="form-control" value="{{ $data->safe }}" disabled="true">
              </div>
            </div>

            <div class="form-group bg-default">
              <label for="inputIcon" class="col-sm-2 control-label">Rating Harga</label>
              <div class="col-sm-10">
                  <input type="number" class="form-control" value="{{ $data->price }}" disabled="true">
              </div>
            </div>

            <div class="form-group bg-default">
              <label for="inputIcon" class="col-sm-2 control-label">Rating Fasilitas Ruangan</label>
              <div class="col-sm-10">
                  <input type="number" class="form-control" value="{{ $data->room_facility }}" disabled="true">
              </div>
            </div>

            <div class="form-group bg-default">
              <label for="inputIcon" class="col-sm-2 control-label">Rating Fasilitas Umum</label>
              <div class="col-sm-10">
                  <input type="number" class="form-control" value="{{ $data->public_facility }}" disabled="true">
              </div>
            </div>

            <div class="form-group bg-default">
              <label for="inputIcon" class="col-sm-2 control-label">Oleh</label>
              <div class="col-sm-10">
                  <input type="text" class="form-control" value="{{ $data->user->name }}" disabled="true">
              </div>
            </div>

          <div class="form-group bg-default">
              <label for="inputIcon" class="col-sm-2 control-label">Content</label>
              <div class="col-sm-10">
                  <textarea class="form-control" name="content">{{ $data->content }}</textarea>
              </div>
          </div>

          <div class="form-group bg-default">
              <label for="inputIcon" class="col-sm-2 control-label">Balasan Owner</label>
              <div class="col-sm-10">
                  @if (isset($data->reply_review))
                    @if ($data->reply_review->status == '1')
                       <label class="label label-success">Live</label>
                    @else
                       <label class="label label-danger">Waitting Confirmation</label>
                    @endif
                    {{ $data->reply_review->content }}
                  @else
                    Belum ada balasan oleh pemilik kost
                  @endif
              </div>
          </div>

          <div class="row">
             @if (isset($data->style_review)) 
                 
                 @foreach ($data->style_review AS $review_photo) 

                        @if ($review_photo->photo == null)

                        @else
                         <div class="col-md-3">
                           <img src="{{ $review_photo->photo->getMediaUrl()['medium'] }}" class="img-responsive" style="height: 200px;" alt=""><br/>
                           <a class="btn btn-xs btn-danger" href="{{ url('admin/review/deletephoto', $review_photo->id) }}"><i class="fa fa-trash"></i> Hapus</a>
                           <a href="{{ URL::to('admin/review/photo/rotate', $review_photo->id) }}" class="btn btn-xs btn-success"><i class="fa fa-repeat"></i> Rotate</a>
                         </div>  
                        @endif 

                 @endforeach

             @endif
          </div>
            
            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="{{ URL::route('admin.review.index') }}">
                  <button type="button" class="btn btn-default">Cancel</button>
                </a>
                <button type="reset" class="btn btn-default"
                  onclick="$('#formInsertTag').bootstrapValidator('resetForm', true);">Reset</button>
              </div>
            </div>
      </div>
  {{ Form::close() }}<!-- /.form -->
</div><!-- /.box -->
@stop
@section('script')
<!-- page script -->
<script type="text/javascript">

    @if (Session::get('laravelValidatorJSON') === null)
      myGlobal.laravelValidator = null;
    @else
      myGlobal.laravelValidator = {{ Session::get('laravelValidatorJSON') }};
    @endif


    $(function()
    {
      myGlobal.baseUrl = "{{ url('') . '/' }}";

      $formInsertTag = $('#formInsertTag');

      $( "#inputSelectTagType" ).change(function() {
        $formInsertTag
          .bootstrapValidator('updateStatus', 'name', 'NOT_VALIDATED')
          .bootstrapValidator('validateField', 'name');
      });

      formStoreTagRules = {
          name: {
              message: 'The name is not valid',
              validators: {
                  notEmpty: {
                      message: 'The name is required and cannot be empty'
                  },
                  stringLength: {
                      min: 3,
                      max: 30,
                      message: 'The name must be more than 3 and less than 30 characters long'
                  },
                  // Send { username: 'its value', type: 'username' } to the back-end
                  remote: {
                      message: 'The name is available',
                      url: myGlobal.baseUrl + 'admin/tag/unique',
                      data: function(validator) {
                          return {
                              type : 'tag.name',
                              fields : {
                                name : validator.getFieldElements('name').val(),
                                type : validator.getFieldElements('type').val()
                              }
                          };
                      },
                  }
              }
          },
          type: {
              validators: {
                  notEmpty: {
                      message: 'The type is required and cannot be empty'
                  },
              }
          }
      };

      formUpdateTagRules = {
          name: {
              message: 'The name is not valid',
              validators: {
                  notEmpty: {
                      message: 'The name is required and cannot be empty'
                  },
                  stringLength: {
                      min: 1,
                      max: 30,
                      message: 'The name must be more than 1 and less than 30 characters long'
                  }
              }
          },
          type: {
              validators: {
                  notEmpty: {
                      message: 'The type is required and cannot be empty'
                  },
              }
          }
      };

      @if (Route::currentRouteName() === 'admin.tag.create')
        myGlobal.bootstrapValidatorDefaults.fields = formStoreTagRules;
      @elseif (Route::currentRouteName() === 'admin.tag.edit')
        myGlobal.bootstrapValidatorDefaults.fields = formUpdateTagRules;
      @endif


      $formInsertTag.bootstrapValidator(myGlobal.bootstrapValidatorDefaults);

      updateValidateStatus($formInsertTag.data('bootstrapValidator'), myGlobal.laravelValidator);

    });
</script>
@stop