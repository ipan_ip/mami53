@extends('admin.layouts.main')
@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
            <div class="box-tools pull-right">
                <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                  <a href="{{ URL::to('admin/review/reply') }}" class="btn btn-sm btn-success">Reply Review</a>
                </div>
            </div>

            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <form action="" class="form-inline" method="get" style="text-align:right;padding:10px;">
                       
                          <input type="text" name="q" class="form-control input-sm"  placeholder="Nama kos"  autocomplete="off" value="{{ request()->input('q') }}">

                          <input type="text" name="username" class="form-control input-sm"  placeholder="Nama User"  autocomplete="off" value="{{ request()->input('username') }}">

                          {{ Form::select('status', $statuso, request()->input('status'), array('class' => 'form-control input-sm')) }}


                        <button class="btn btn-primary btn-sm" id="buttonSearch"><i class="fa fa-search">&nbsp;</i>Search</button>
                    </form>
                </div>
            </div>

            <table id="tableListTag" class="table table-striped">
                <thead>
                    <tr>
                        <th width="30px;">No</th>
                        <th>User</th>
                        <th>Kost Name</th>
                        <th>Content</th>
                        <th>Status</th>
                        <th class="table-action-column">Action</th>

                    </tr>
                </thead>
                <tbody>
                   @foreach ($rowsReview as $rowReviewKey => $rowReview)
                    <tr>
                        <td>{{ $rowReviewKey+1 }}</td>
                        <td>@if (isset($rowReview->user->name))
                               {{ $rowReview->user->name }}
                            @else
                               <label class="label label-warning">.....</label>
                            @endif
                        </td>
                        <td>@if (isset($rowReview->room->name))
                               {{ $rowReview->room->name }}
                            @else 
                              <label class="label label-danger">.....</label>
                            @endif
                        </td>
                        <td>{{ $rowReview->content }} <label class="label label-primary">{{ $rowReview->updated_at->format("d M Y H:i") }}</label></td>
                        <td>
                           @if ($rowReview->status == 'live')
                               <span class="label label-success">{{ $rowReview->status }}</span>
                           @else
                               <span class="label label-warning">{{ $rowReview->status }}</span>
                           @endif
                        </td>
                        <td style="width: 150px;">
                                <a href="{{ URL::to('admin/review/thread', $rowReview->id) }}" class="btn btn-xs btn-warning">Show</a>
                                <a href="{{ URL::to('admin/review', $rowReview->id) }}" class="btn btn-xs btn-default">
                                    <i class="fa fa-pencil"></i></a>
                                @if ($rowReview->status == 'live')
                                <a href="{{ URL::to('admin/review/confirm', $rowReview->id) }}?type=rejected" class="btn btn-xs btn-danger" onclick="return preventDoubleEvent(this);"><i class="fa fa-times"></i></a>
                                @else
                                    <a href="{{ URL::to('admin/review/confirm', $rowReview->id) }}?type=live" class="btn btn-xs btn-success" onclick="return preventDoubleEvent(this);"><i class="fa fa-check"></i></a>
                                @endif
                                <a href="{{ URL::to('/admin/review/destroy/'.$rowReview->id) }}" class="btn btn-xs btn-danger" onclick="return confirm('Yakin mau hapus saya?')">
                                    <i class="fa fa-trash-o"></i></a>
                        </td>
                    </tr>
                    @endforeach

                </tbody>
                
            </table>
            <div class="box-body no-padding">
                {{ $rowsReview->appends(Request::except('page'))->links() }}
            </div>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
    <!-- table -->

    @include('admin.partials.delete-modal')
@stop
@section('script')
<!-- page script -->
<script type="text/javascript">
function preventDoubleEvent(btn) {
    btn.className += 'disabled';
}
</script>
@stop
