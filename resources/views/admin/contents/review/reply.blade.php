@extends('admin.layouts.main')
@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
            <div class="box-tools pull-right">
                <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                  <a href="{{ URL::to('admin/review') }}" class="btn btn-sm btn-success">Back</a>
                </div>
            </div>

            <table id="tableListTag" class="table table-striped">
                <thead>
                    <tr>
                        <th width="30px;">No</th>
                        <th>User</th>
                        <th>Reply</th>
                        <th>Status</th>
                        <th>Date</th>
                        <th class="table-action-column">Action</th>

                    </tr>
                </thead>
                <tbody>
                  @foreach ($rowsReviewReply AS $key => $value) 
                     <tr>
                         <td>{{ $key+1 }}</td>
                         <td>{{ $value->user->name }}</td>
                         <td>{{ $value->content }}</td>
                         <td>@if ($value->status == '1')
                              <label class="label label-success">live</label>
                             @else
                              <label class="label label-danger">waiting...</label> 
                             @endif
                         </td>
                         <td>{{ $value->updated_at }}</td>
                         <td>
                          @if (isset($value->review))
                             <a href="{{ URL::to('admin/review/thread', $value->review->id) }}" class="btn btn-xs btn-warning">Show Review</a>
                             @if ($value->status == '1')
                              <a href="{{ URL::to('admin/review/reply/deleted', $value->id) }}" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
                             @else
                              <a href="{{ URL::to('admin/review/reply/confirm', $value->id) }}" class="btn btn-xs btn-success"><i class="fa fa-check"></i></a>
                             @endif
                           @else
                             Review di hapus oleh admin
                           @endif
                         </td>
                     </tr>
                  @endforeach
                </tbody>
                
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
    <!-- table -->

    @include('admin.partials.delete-modal')
@stop
@section('script')
<!-- page script -->
<script type="text/javascript">
    $(function()
    {
        $("#tableListTag").dataTable();
    });
</script>
@stop