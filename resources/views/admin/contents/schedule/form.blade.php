@extends('embed.layouts.main')
@section('content')
<div class="box box-primary">
  <div class="box-header">
      <h3 class="box-title">{{ $boxTitle }}</h3>
  </div><!-- /.box-header -->
  {{ Form::open(array('route' => $formAction, 'method' => $formMethod,
      'class' => 'form-horizontal form-bordered', 'role' => 'form', 'id' => 'formInsertDesigner')) }}
      <div class="box-body no-padding">
            <div class="form-group bg-default">
              <label for="inputAddress" class="col-sm-2 control-label">{{  date('F Y',strtotime($dates_this_month[1][1])) }}</label>
              <div class="col-sm-10">
                <table class="table" style>
                  @foreach($dates_this_month as $dates)
                    <tr>
                      @foreach($dates as $date)
                         @if( $date == null )
                            <td><input type="checkbox"  disabled></td>
                          @else
                            <td>
                              <input type="checkbox" value="excluded" name="{{  date('Y-m-d', strtotime($date) ) }}"
                                {{ in_array( $date , $prev_schedules)?'checked':'' }}> 
                              <b>{{ date('d',strtotime($date)) }}</b>
                            </td>
                          @endif  
                      @endforeach
                    </tr>
                  @endforeach
                </table>
              </div>
              <label for="inputAddress" class="col-sm-2 control-label">{{  date('F Y',strtotime($dates_next_month[1][1])) }}</label>
              <div class="col-sm-10">
                <table class="table" style>
                  @foreach($dates_next_month as $dates)
                    <tr>
                      @foreach($dates as $date)
                          @if( $date == null )
                            <td><input type="checkbox" disabled></td>
                          @else
                            <td><input type="checkbox" value="excluded" name="{{  date('Y-m-d', strtotime($date) ) }}"
                              {{ in_array( $date , $prev_schedules)?'checked':'' }}> 
                            <b>{{ date('d',strtotime($date)) }}</b></td>
                          @endif
                      @endforeach
                    </tr>
                  @endforeach
                </table>
              </div>
            </div>
             <div class="form-group bg-info divider">
              <div class="col-sm-12">
              </div>
            </div>

            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">Submit</button>
                @if (Request::is('admin/*'))
                <a href="{{ $formCancel }}">
                  <button type="button" class="btn btn-default">Cancel</button>
                </a>
                <button type="reset" class="btn btn-default"
                  onclick="$('#formInsertDesigner').bootstrapValidator('resetForm', true);">Reset</button>
                @endif
              </div>
            </div>
      </div>
  {{ Form::close() }}<!-- /.form -->
</div><!-- /.box -->

@stop
@section('style')
{{ HTML::style('assets/vendor/jquery-chosen/chosen.css') }}
{{ HTML::style('assets/vendor/jquery-chosen-custom/chosen.css') }}
{{ HTML::style('assets/vendor/jquery-file-upload/css/jquery.fileupload.css') }}
<style>
  #chosenForm .chosen-choices {
      border: 1px solid #ccc;
      border-radius: 4px;
      min-height: 34px;
      padding: 6px 12px;
  }
  #chosenForm .form-control-feedback {
      /* To make the feedback icon visible */
      z-index: 100;
  }
</style>
@stop
@section('script')
<!-- page script -->
{{ HTML::script('assets/vendor/jquery-chosen/chosen.jquery.js') }}
<script type="text/javascript">
  var config = {
    '.chosen-select'           : {width: '100%'}
  };
  for (var selector in config) {
    $(selector).chosen(config[selector]);
  }
</script>
<!-- Holder for Upload File Thumbnail -->
{{ HTML::script('assets/vendor/holder/holder.js') }}
<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
{{ HTML::script('assets/vendor/jquery-file-upload/js/vendor/jquery.ui.widget.js') }}
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
{{ HTML::script('assets/vendor/jquery-file-upload/js/jquery.iframe-transport.js') }}
<!-- The basic File Upload plugin -->
{{ HTML::script('assets/vendor/jquery-file-upload/js/jquery.fileupload.js') }}
<!-- The File Upload processing plugin -->
{{ HTML::script('assets/vendor/jquery-file-upload/js/jquery.fileupload-process.js') }}
<!-- The File Upload validation plugin -->
{{ HTML::script('assets/vendor/jquery-file-upload/js/jquery.fileupload-validate.js') }}
<!-- File Upload Main -->
{{ HTML::script('assets/vendor/file-upload/index.js') }}
<script>
  $(function () {
    // Change this to the location of your server-side upload handler
    @if (Request::is('admin/*'))
      var url = "{{ url('admin/media') }}";
    @else
      var url = "{{ url('media') }}";
    @endif
    
    myFileUpload($('#wrapper-file-upload'), url, 'user_photo');
  });
  
</script>


<script src="https://maps.googleapis.com/maps/api/js?libraries=places&language=id&key={{ENV('MIX_GOOGLE_MAPS_JAVASCRIPT_API_KEY')}}"></script>
{{ HTML::script('assets/vendor/geocomplete/jquery.geocomplete.js') }}
{{ HTML::script('assets/vendor/google-maps/index.js') }}

<script type="text/javascript">
    var c = { schedule_exclude : [{
                  date : "2012-12-10",
                  time : ["12:00","13:00"]
                },
                {
                  date : "2012-12-10",
                  time : ["12:00","13:00"]
                }]
            };
    console.log(c);
</script>

@stop