@extends('admin.layouts.main')
@section('content')
<div class="box box-default">
        <!-- Box Header -->
        <div class="box-header">
            <div class="col-md-12 text-center">
                <h3 class="box-title"><i class="fa fa-check-square-o"></i> {{ $boxTitle }}</h3>
            </div>
        </div>
        <!-- /.box-header -->

        <div class="box-body no-padding">

            {{-- Top Bar --}}
            <div class="horizontal-wrapper">
            </div>

            {{-- Table --}}
            <table class="table">
                <thead>
                    <tr>
                        <th class="text-center">Name</th>
                        <th class="text-center">Chat Room</th>
                    </tr>
                    @foreach ($chatAdmins as $id => $admin)
                    <tr>
                        <td class="text-center"><b>{{ $admin }}</b></td>
                        <td class="text-center"><b><a href="{{ url('admin/room/chat/'.$id) }}">Admin {{ '('.$admin.')' }}</a></b></td>
                    </tr>
                    @endforeach
                    @foreach ($consultants as $consultant)
                    <tr>
                        <td class="text-center"><b>{{ $consultant->user->name }}</b></td>
                        <td class="text-center"><b><a href="{{ url('admin/consultant/room-chat/'.$consultant->id) }}" target="_blank">Consultant</a></b></td>
                    </tr>
                    @endforeach
                </thead>
                <tbody>
                </tbody>

            </table>
        </div><!-- /.box-body -->
        <div class="box-footer clearfix">
        </div>
    </div><!-- /.box -->
    <!-- table -->
@stop

@section('script')
@stop
