@extends('admin.layouts.main')
@section('content')
  <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">{{ $boxTitle }}</h3>
    </div>
    {{ Form::open(array('url' => route('admin.chat.config.update'), 'method' => 'post',
      'class' => 'form-horizontal form-bordered', 'role' => 'form', 'id' => 'formChatConfig')) }}
      <div class="box-body no-padding">
        @foreach($configs as $config)
          <div class="form-group bg-default">
            <label for="inputName" class="col-sm-2 control-label">CS {{ $adminName[explode('_', $config['name'])[1]] }} On</label>
            <div class="col-sm-10">
              <label for="{{ $config['name'] }}" class="checkbox">
                <input type="checkbox" class="form-control" placeholder="Name"
                id="{{ $config['name'] }}" name="{{ $config['name'] }}" value="1" {{ $config['value'] == '1' ? 'checked="checked"' : '' }}> &nbsp;Online
              </label>
            </div>
          </div>
        @endforeach

        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-primary">Save</button>
          </div>
        </div>
      </div>
    {{ Form::close() }}
  </div>
@stop

@section('script')

<script>

    

</script>

@stop

