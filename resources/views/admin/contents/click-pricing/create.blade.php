@extends('admin.layouts.main')

@section('content')
	<div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

		<form action="{{ URL::route('admin.click-pricing.store') }}" method="POST" class="form-horizontal form-bordered">
			<div class="box-body no-padding">
				<div class="form-group bg-default">
					<label for="area" class="control-label col-sm-2">Area</label>
					<div class="col-sm-10">
						<input type="text" name="area" id="area" class="form-control" value="{{ old('area') }}">
					</div>
				</div>

				<div class="form-group bg-default">
					<label for="lower-limit-price" class="control-label col-sm-2">Batas Bawah</label>
					<div class="col-sm-10">
						<input type="number" name="lower_limit_price" id="lower-limit-price" class="form-control" value="{{ old('lower_limit_price') }}">
						<p class="help-block">Akan ditampilkan untuk keterangan di owner profile</p>
					</div>
				</div>

				<div class="form-group bg-default">
					<label for="higher-limit-price" class="control-label col-sm-2">Batas Atas</label>
					<div class="col-sm-10">
						<input type="number" name="higher_limit_price" id="higher-limit-price" class="form-control" value="{{ old('higher_limit_price') }}">
						<p class="help-block">Akan ditampilkan untuk keterangan di owner profile</p>
					</div>
				</div>

				<div class="form-group bg-default">
					<label for="property-median-price" class="control-label col-sm-2">Batas Tengah Harga Bulanan</label>
					<div class="col-sm-10">
						<input type="number" name="property_median_price" id="property-median-price" class="form-control" value="{{ old('property_median_price') }}">
					</div>
				</div>

				<div class="form-group bg-default">
					<label for="low-price" class="control-label col-sm-2">Harga Bawah</label>
					<div class="col-sm-10">
						<input type="number" name="low_price" id="low-price" class="form-control" value="{{ old('low_price') }}">
					</div>
				</div>
				
				<div class="form-group bg-default">
					<label for="high-price" class="control-label col-sm-2">Harga Atas</label>
					<div class="col-sm-10">
						<input type="number" name="high_price" id="high-price" class="form-control" value="{{ old('high_price') }}">
					</div>
				</div>

				<div class="form-group bg-default">
					<label for="fore" class="control-label col-sm-2">For</label>
					<div class="col-sm-10">
						<select name="for" class="form-control">
							@foreach ($for AS $value)
								<option value="{{ $value }}">{{ $value }}</option>
							@endforeach
						</select>
					</div>
				</div>


				<div class="form-group bg-default">
					<div class="col-sm-10 col-sm-push-2">
						<button type="submit" class="btn btn-primary">Save</button>
					</div>
				</div>
			</div>
		</form>
    </div>
@endsection