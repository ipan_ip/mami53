@extends('admin.layouts.main')
@section('style')
    <link rel="stylesheet" href="/assets/vendor/summernote/summernote.css?v=05042018">
@stop
@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div><!-- /.box-header -->
        <form method="POST" action="{{ $formAction }}" class="form-horizontal form-bordered">
            {{ csrf_field() }}
            
            @if ($formMethod == 'PUT')
                <input type="hidden" name="_method" value="PUT" />
            @endif

            <div class="box-body no-padding">

                <div class="form-group bg-default">
                    <label for="inputName" class="col-sm-2 control-label">Question</label>
                    <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Question"
                        id="inputName" name="question" value="{{ $faq->question }}">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="inputName" class="col-sm-2 control-label">Answer</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" id="inputDescriptionMother" name="answer">{{ $faq->answer }}</textarea>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="inputSelectType" class="col-sm-2 control-label">Is Active</label>
                    <div class="col-sm-10">
                        <select class="form-control" id="inputSelectType" name="is_active">
                            @foreach ($rowsActive as $active)
                                @if ($faq->is_active == $active)
                                    <option value="{{ $active }}" selected="selected">{{ $active }}</option>
                                @else
                                    <option value="{{ $active }}">{{ $active }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div>
        </form>
    </div><!-- /.box -->
@stop
@section('script')

<script src="/assets/vendor/summernote/summernote.min.js"></script>
<script type="text/javascript">
        $('#inputDescriptionMother').summernote({
            height: 300,
            toolbar: [
                ['style', ['style', 'bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['insert', ['link', 'table', 'hr']],
                ['misc', ['codeview']]
            ]
        });

</script>
@stop