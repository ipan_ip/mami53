@extends('admin.layouts.main')
@section('content')
	<!-- table -->
	<div class="box box-primary">
		<div class="box-header">
			<h3 class="box-title">{{ $boxTitle }}</h3>
			<div class="box-tools pull-right">
				<a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
				<a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
			</div>
		</div><!-- /.box-header -->
		<div class="box-body no-padding">
			<div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <a class="btn-add btn btn-primary btn-sm" href="/admin/premium/faq/create">
                        <i class="fa fa-plus">&nbsp;</i>Add New FAQ
                    </a>
                </div>
			</div>
			
			<table class="table table-striped" id="tableList">
				<thead>
					<tr>
						<th width="30px;">No</th>
						<th>Name</th>
						<th>Active</th>
						<th class="table-action-column">Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($faqList AS $key => $faq)
						<tr>
							<td>{{ $key+1 }}</td>
								<td>{{ $faq->question }}</td>
							<td>
								@if ((int) $faq->is_active)
									<span class="label label-success">Yes</span>
								@else
									<span class="label label-danger">No</span>
								@endif
							</td>
							<td>
								<a href="{{ url('admin/premium/faq/'.$faq->id.'/edit') }}" class="btn btn-xs btn-warning">Edit</a>
								<form action="{{ url('/admin/premium/faq/'.$faq->id) }}" style="margin-top:10px" method="POST">
									{{ csrf_field() }}
									{{ method_field('DELETE') }}
									<button class="btn btn-xs btn-danger" type="submit">Delete</button>
								</form>
							</td>
						</tr>
					@endforeach
				</tbody>
				
			</table>
		</div><!-- /.box-body -->
	</div><!-- /.box -->
	<!-- table -->

@stop
@section('script')
<!-- page script -->
<script type="text/javascript">
	$(function()
	{
		$("#tableList").dataTable();
	});
</script>
@stop