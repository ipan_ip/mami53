@extends('admin.layouts.main')

@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <form action="" class="form-inline" method="get" style="text-align:right;padding:10px;">
                        <input type="text" name="q" class="form-control input-sm"  placeholder="Criteria"  autocomplete="off" value="{{ request()->input('q') }}">
                        <button class="btn btn-primary btn-md" id="buttonSearch"><i class="fa fa-search">&nbsp;</i>Search</button>
                    </form>
                </div>
            </div>

            <table  class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Kategori</th>
                        <th>Isi</th>
                        <th>Laporan</th>
                        <th>Tanggal Laporan</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                @foreach($reports as $report)
                    <tr>
                        <td>{{ $report->id }}</td>
                        <td>{{ $report->type }}</td>
                        <td>{{ $report->type == 'thread' ? $report->thread->title : $report->answer->answer }}</td>
                        <td>{{ $report->description }}</td>
                        <td>{{ $report->created_at->format('Y-m-d H:i:s') }}</td>
                        <td>
                            @if ($report->type == 'thread')
                                @if ($report->thread->is_active == 0)
                                    <a href="{{ route('admin.forum-thread.activate', $report->reference_id) }}" title="Aktifasi">
                                        <i class="fa fa-arrow-up"></i>
                                    </a>
                                @else 
                                    <a href="{{ route('admin.forum-thread.deactivate', $report->reference_id) }}" title="Non Aktifkan">
                                        <i class="fa fa-arrow-down"></i>
                                    </a>
                                @endif
                            @elseif ($report->type == 'answer')
                                @if ($report->answer->is_active == 0)
                                    <a href="{{ route('admin.forum-answer.activate', $report->reference_id) }}" title="Aktifasi">
                                        <i class="fa fa-arrow-up"></i>
                                    </a>
                                @else 
                                    <a href="{{ route('admin.forum-answer.deactivate', $report->reference_id) }}" title="Non Aktifkan">
                                        <i class="fa fa-arrow-down"></i>
                                    </a>
                                @endif
                            @endif
                        </td>
                    </tr> 
                @endforeach
                </tbody>
                
            </table>
        </div><!-- /.box-body -->
        <div class="box-body no-padding">
            {{ $reports->appends(Request::except('page'))->links() }}
        </div>
    </div><!-- /.box -->
    <!-- table -->
@endsection