@extends('admin.layouts.main')

@section('content')
<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">{{ $boxTitle }}</h3>
    </div><!-- /.box-header -->
    <form action="{{ route('admin.forum.user.redeem-post', $user->id) }}" method="post" class="form-horizontal form-bordered">
        <div class="box-body no-padding">
            <h3>Jumlah Point Tersedia: <strong>{{ $user->forum_points->sum('point_value') }}</strong></h3>
            <div class="form-group bg-default">
                <label for="inputPoints" class="col-sm-2 control-label">Jumlah Point</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Jumlah Point" id="inputPoints" name="points"  value="{{ old('points') }}">
                </div>
            </div>
            <div class="form-group bg-default">
                <label for="inputDescription" class="col-sm-2 control-label">Description</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Deskripsi" id="inputDescription" name="description"  value="{{ old('description') }}">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </form>
</div><!-- /.box -->
@endsection