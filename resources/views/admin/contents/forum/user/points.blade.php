@extends('admin.layouts.main')

@section('style')
<style>
    .fa-star-official {
        color: #0006FF;
    }
</style>
@endsection

@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    
                </div>
            </div>

            <table id="tableListLanding" class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Keterangan</th>
                        <th>Jumlah Point</th>
                        <th class="table-action-column" width="120px">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($points as $point)
                    <tr>
                        <td>
                            {{ $point->id }} 
                        </td>
                        <td>{{ $point->description }}</td>
                        <td>{{ $point->point_value }}</td>
                        <td align="center">
                            
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div><!-- /.box-body -->

        <div class="box-body no-padding">
            {{ $points->appends(Request::except('page'))->links() }}
        </div>
    </div><!-- /.box -->
    <!-- table -->
@endsection