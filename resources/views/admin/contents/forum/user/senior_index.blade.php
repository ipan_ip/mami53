@extends('admin.layouts.main')

@section('style')
<style>
    .fa-star-official {
        color: #0006FF;
    }
</style>
@endsection

@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <a href="{{ URL::route('admin.forum.user.senior-create') }}">
                        <button type="button" name="button" class="btn-add btn btn-primary btn-sm">
                            <i class="fa fa-plus">&nbsp;</i> Tambah Kakak Official</button>
                    </a>
                </div>
            </div>

            <table id="tableListLanding" class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Universitas</th>
                        <th>Total Point</th>
                        <th class="table-action-column" width="120px">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                    <tr>
                        <td>
                            {{ $user->id }} 
                            @if($user->hasRole('forum_senior_official'))
                                <i class="fa fa-star fa-star-official"></i>
                            @endif
                        </td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->forum_user->university }}</td>
                        <td>{{ $user->forum_points->sum('point_value') }}</td>
                        <td align="center">
                            <a href="{{route('admin.forum.user.redeem', $user->id)}}" title="Redeem">
                                <i class="fa fa-gift"></i>
                            </a>

                            <a href="{{route('admin.forum.user.points', $user->id)}}" title="Points">
                                <i class="fa fa-list-ul"></i>
                            </a>

                            @if(!$user->hasRole('forum_senior_official'))
                                <a href="{{route('admin.forum.user.make-senior-official', $user->id)}}" title="Jadikan Official">
                                    <i class="fa fa-star"></i>
                                </a>
                            @else
                                <a href="{{route('admin.forum.user.make-senior-unofficial', $user->id)}}" title="Jadikan Unofficial">
                                    <i class="fa fa-star-o"></i>
                                </a>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div><!-- /.box-body -->

        <div class="box-body no-padding">
            {{ $users->appends(Request::except('page'))->links() }}
        </div>
    </div><!-- /.box -->
    <!-- table -->
@endsection