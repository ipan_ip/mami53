@extends('admin.layouts.main')

@section('content')
<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">{{ $boxTitle }}</h3>
    </div><!-- /.box-header -->
    <form action="{{ route('admin.forum.user.senior-store') }}" method="post" class="form-horizontal form-bordered">
        <div class="box-body no-padding">
            <div class="form-group bg-default">
                <label for="user-name" class="control-label col-sm-2">User</label>
                <div class="col-sm-10">
                    <input type="text" name="user_name" id="user-name" class="form-control" value="{{ old('user_name') }}" placeholder="Ketikkan email atau nama" data-toggle="dropdown">
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="user-id" class="control-label col-sm-2">User ID</label>
                <div class="col-sm-10">
                    <input type="text" name="user_id" id="user-id" class="form-control" value="{{ old('user_id') }}">
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="nim" class="control-label col-sm-2">NIM</label>
                <div class="col-sm-10">
                    <input type="text" name="nim" id="nim" class="form-control" value="{{ old('nim') }}">
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="university" class="control-label col-sm-2">Universitas</label>
                <div class="col-sm-10">
                    <input type="text" name="university" id="university" class="form-control" value="{{ old('university') }}">
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="major" class="control-label col-sm-2">Jurusan</label>
                <div class="col-sm-10">
                    <input type="text" name="major" id="major" class="form-control" value="{{ old('major') }}">
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="year_start" class="control-label col-sm-2">Tahun Masuk</label>
                <div class="col-sm-10">
                    <select name="year_start" class="form-control">
                        @foreach($yearStartOptions as $option) 
                            <option value="{{ $option }}" {{ old('year_start') == $option ? 'selected="selected"' : '' }}>{{ $option }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="university_city" class="control-label col-sm-2">Kota Universitas</label>
                <div class="col-sm-10">
                    <input type="text" name="university_city" id="university_city" class="form-control" value="{{ old('university_city') }}">
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="category_id" class="control-label col-sm-2">Kategori</label>
                <div class="col-sm-10">
                    <select name="category_id" class="form-control">
                        <option value="">-- Pilih Kategori --</option>
                        @foreach($categories as $category) 
                            <option value="{{ $category->id }}" {{ old('category_id') == $category->id ? 'selected="selected"' : '' }}>{{ $category->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </form>
</div><!-- /.box -->
@endsection

@section('script')
<script>
    function clickSuggestion(idx) {
        key = idx;
        val = $('.suggestion-item-' + idx).text();
        el = $('.suggestion-item-' + idx);

        $('#user-name').val(val);
        $('#user-id').val(key);
    }

    $(function() {
        var userSuggestion = function(e) {
            $.ajax({
                type: 'POST',
                url: '/admin/role/users/suggestion',
                data: {
                    'params': e.target.value
                },
                success: function(data) {
                    $(e.target).next('.dropdown-menu').remove();

                    listOption = '<ul class="dropdown-menu suggestion-list" aria-labelledby="dLabel">';
                    for(idx in data.users) {
                        listOption += '<li><a href="#" class="suggestion-item suggestion-item-' + idx + '" data-key="' + idx + '" onclick="clickSuggestion(' + idx + ')">' + data.users[idx] + '</a></li>';
                    }

                    listOption += '</ul>';

                    $(listOption).insertAfter($(e.target));

                    $('.suggestion-list').dropdown();
                }
                
            });
        }

        var delay = (function(){
          var timer = 0;
          return function(callback, ms){
            clearTimeout (timer);
            timer = setTimeout(callback, ms);
          };
        })();

        $(document).on('keyup', '#user-name', function(e) {
            delay(function(){
                userSuggestion(e)
            }, 1000 );
        });
    });
</script>
@endsection