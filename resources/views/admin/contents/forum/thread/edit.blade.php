@extends('admin.layouts.main')

@section('content')
<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">{{ $boxTitle }}</h3>
    </div><!-- /.box-header -->
    <form action="{{ route('admin.forum-thread.update', $thread->id) }}" method="post" class="form-horizontal form-bordered">
        <input type="hidden" name="_method" value="PUT">

        <div class="box-body no-padding">
            <div class="form-group bg-default">
                <label for="question" class="col-sm-2 control-label">Pertanyaan</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Pertanyaan" id="inputQuestion" name="question"  value="{{ old('question', $thread->title) }}">
                </div>
            </div>
            
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </form>
</div><!-- /.box -->
@endsection