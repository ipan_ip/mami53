@extends('admin.layouts.main')

@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <form action="" class="form-inline" method="get" style="text-align:right;padding:10px;">
                        <input type="text" name="q" class="form-control input-sm"  placeholder="Criteria"  autocomplete="off" value="{{ request()->input('q') }}">
                        <button class="btn btn-primary btn-md" id="buttonSearch"><i class="fa fa-search">&nbsp;</i>Search</button>
                    </form>
                </div>
            </div>

            <table  class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Kategori</th>
                        <th>Pertanyaan</th>
                        <th>Tanggal</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                @foreach($threads as $thread)
                    <tr>
                        <td>{{ $thread->id }}</td>
                        <td>{{ $thread->category->name }}</td>
                        <td>{{ $thread->title }}</td>
                        <td>{{ $thread->created_at->format('Y-m-d H:i:s') }}</td>
                        <td>
                            <a href="{{ route('admin.forum-thread.edit', $thread->id) }}" title="Edit">
                                <i class="fa fa-pencil"></i>
                            </a>

                            @if ($thread->is_active == 0)
                                <a href="{{ route('admin.forum-thread.activate', $thread->id) }}" title="Aktifasi">
                                    <i class="fa fa-arrow-up"></i>
                                </a>
                            @else 
                                <a href="{{ route('admin.forum-thread.deactivate', $thread->id) }}" title="Non Aktifkan">
                                    <i class="fa fa-arrow-down"></i>
                                </a>
                            @endif

                            <a href="{{ route('admin.forum-answer.list', $thread->id) }}" title="Jawaban">
                                <i class="fa fa-list-ul"></i>
                            </a>
                        </td>
                    </tr> 
                @endforeach
                </tbody>
                
            </table>
        </div><!-- /.box-body -->
        <div class="box-body no-padding">
            {{ $threads->appends(Request::except('page'))->links() }}
        </div>
    </div><!-- /.box -->
    <!-- table -->
@endsection