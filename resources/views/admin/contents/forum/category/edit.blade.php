@extends('admin.layouts.main')

@section('content')
<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">{{ $boxTitle }}</h3>
    </div><!-- /.box-header -->
    <form action="{{ route('admin.forum-category.update', $category->id) }}" method="post" class="form-horizontal form-bordered">
        <input type="hidden" name="_method" value="PUT">

        <div class="box-body no-padding">
            <div class="form-group bg-default">
                <label for="inputName" class="col-sm-2 control-label">Nama Kategori</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Nama Kategori" id="inputName" name="name"  value="{{ old('name', $category->name) }}">
                </div>
            </div>
            <div class="form-group bg-default">
                <label for="inputSlug" class="col-sm-2 control-label">Slug</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Slug" id="inpuSlug" name="slug"  value="{{ old('slug', $category->slug) }}">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </form>
</div><!-- /.box -->
@endsection