@extends('admin.layouts.main')

@section('style')
    <link rel="stylesheet" href="/assets/vendor/summernote/summernote.css">
    <link rel="stylesheet" href="{{ mix_url('dist/vendor/leaflet/leaflet.css') }}">

    <style>
        .note-group-select-from-files {
            display: none;
        }
    </style>
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

        <form action="{{ URL::route('admin.list-creator.update-kost', $landingList->id) }}" method="POST" class="form-horizontal form-bordered">

            <div class="box-body no-padding">
                <div class="form-group bg-default">
                    <label for="title" class="control-label col-sm-2">Title</label>
                    <div class="col-sm-10">
                        <input type="text" name="title" id="title" value="{{ old('title', $landingList->keyword) }}" class="form-control" max="190">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="slug" class="control-label col-sm-2">Slug</label>
                    <div class="col-sm-10">
                        <input type="text" name="slug" id="slug" class="form-control" value="{{ old('slug', $landingList->slug) }}" max="190">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="city" class="col-sm-2 control-label">Kota</label>
                    <div class="col-sm-10">
                        <select name="city" id="city" class="form-control chosen-select">
                            @foreach($filterCities as $key => $city)
                                <option value="{{ $key }}" {{ old('city', isset($options['city']) ? $options['city'] : '') == $key ? 'selected="selected"' : '' }}>{{$city}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="inputGeoName" class="col-sm-2 control-label">Koordinat</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" placeholder="Geo Name"
                               id="inputGeoName" name="geo_name">
                    </div>
                    <div class="col-sm-10 col-sm-offset-2">
                        <div style="background: #EEE; width: 100%; height: 400px;">
                            <div id="map-canvas"></div>
                        </div>
                    </div>
                    <div class="col-sm-10 col-sm-offset-2">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="latitude 1"
                               id="inputLatitude1" name="latitude_1" value="{{ old('latitude_1', isset($options['location'][0][1]) ? $options['location'][0][1] : '') }}">
                            <input type="text" class="form-control" placeholder="longitude 1"
                               id="inputLongitude1" name="longitude_1" value="{{ old('longitude_1', isset($options['location'][0][0]) ? $options['location'][0][0] : '') }}">
                       </div>

                       <hr>

                       <div class="input-group">
                            <input type="text" class="form-control" placeholder="latitude 2"
                               id="inputLatitude2" name="latitude_2" value="{{ old('latitude_2', isset($options['location'][1][1]) ? $options['location'][1][1] : '') }}">
                            <input type="text" class="form-control" placeholder="longitude 2"
                               id="inputLongitude2" name="longitude_2" value="{{ old('longitude_2', isset($options['location'][1][0]) ? $options['location'][1][0] : '') }}">
                       </div>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="type" class="col-sm-2 control-label">Urutan</label>
                    <div class="col-sm-10">
                        <select name="sorting" id="type" class="form-control chosen-select">
                            @foreach($sortingOptions as $key => $sortingOption)
                             <option value="{{ $key }}" {{ old('sorting', $options['sorting']) == $key ? 'selected="selected"' : '' }}>{{ $sortingOption }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="type" class="col-sm-2 control-label">Filter yang diaktifkan</label>
                    <div class="col-sm-10">
                            @foreach($filterOptions as $key => $filterList)
                             <input name="active_filter[]" type="checkbox" value="{{ $key }}" {{ old('active_filter', in_array($key, $options['active_filter'])) == $key ? 'checked="checked"' : '' }}>{{ $filterList }}</input><br>
                            @endforeach
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="type" class="col-sm-2 control-label">Description</label>
                    <div class="col-sm-10">
                        <textarea name="description" id="content-open" class="form-control" rows="10">{{ old('description', $landingList->description) }}</textarea>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <div class="col-sm-10 col-sm-push-2">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('script')
<script src="/assets/vendor/summernote/summernote.min.js"></script>
<script src="/assets/vendor/summernote/plugin/summernote-image-attributes.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&language=id&key={{ENV('MIX_GOOGLE_MAPS_JAVASCRIPT_API_KEY')}}"></script>
<script src="{{ mix_url('dist/vendor/leaflet/leaflet.js') }}"></script>

<script>
    function addMarkerListener(map, marker, pos) {
        marker.on('dragend', function(evt) {
            var latlng = evt.target.getLatLng();

            var lat = latlng.lat;
            var lng = latlng.lng;

            $('#inputLatitude' + pos).val(lat);
            $('#inputLongitude' + pos).val(lng);
        });
    }

    function addPlaceChangedListener(autocomplete, map, marker) {
        autocomplete.addListener('place_changed', function() {
            marker[0].setOpacity(0);
            marker[1].setOpacity(0);

            var place = autocomplete.getPlace();
            if (!place.geometry) {
                // User entered the name of a Place that was not suggested and
                // pressed the Enter key, or the Place Details request failed.
                window.alert("No details available for input: '" + place.name + "'");
                return;
            }

            map.setView({
                lat: place.geometry.location.lat(),
                lng: place.geometry.location.lng()
            }, 12);

            bottomLeft = {
                lat: place.geometry.location.lat() - 0.05,
                lng: place.geometry.location.lng() - 0.05
            };

            topRight = {
                lat: place.geometry.location.lat() + 0.05,
                lng: place.geometry.location.lng() + 0.05
            };

            marker[0].setLatLng(bottomLeft);
            marker[1].setLatLng(topRight);
            marker[0].setOpacity(1);
            marker[1].setOpacity(1);


            $('#inputLatitude1').val(bottomLeft.lat);
            $('#inputLongitude1').val(bottomLeft.lng);
            $('#inputLatitude2').val(topRight.lat);
            $('#inputLongitude2').val(topRight.lng);
        });
    }

    @if (isset($options['location'][0][1]) && isset($options['location'][0][0]) 
        && isset($options['location'][1][1]) && isset($options['location'][1][0]))
        var centerPos = {
            lat: parseFloat('{{ ($options['location'][0][1] + $options['location'][1][1]) / 2 }}'), 
            lng: parseFloat('{{ ($options['location'][1][0] + $options['location'][1][0]) / 2 }}')
        };
    @else
        var centerPos = {lat: -7.7858485, lng: 110.3680087};
    @endif

    console.log(centerPos);

    var map = L.map('map-canvas', {
        // Set latitude and longitude of the map center (required)
        center: centerPos, 
        // Set the initial zoom level, values 0-18, where 0 is most zoomed-out (required)
        zoom: 12
    });

    var tiles = new L.tileLayer("{{ config('services.osm.host') }}/hot/{z}/{x}/{y}.png").addTo(map);

    var marker = [];
    var markerImage = [];
    var position = [];

    markerImage[0] = '{{ asset('assets/icons/kiribawah.png') }}';
    
    @if (isset($options['location'][0][1]) && isset($options['location'][0][0]))
        position[0] = {
            'lat': parseFloat('{{ $options['location'][0][1] }}'),
            'lng': parseFloat('{{ $options['location'][0][0] }}')
        };
    @else 
        position[0] = {
            'lat': centerPos.lat - 0.05,
            'lng': centerPos.lng - 0.05
        };
    @endif

    marker[0] = new L.Marker(position[0], {
        draggable: true,
        icon: new L.icon({
            iconUrl: markerImage[0],
            iconSize: [60, 57],
            iconAnchor: [30, 57]
        })
    }).addTo(map);

    addMarkerListener(map, marker[0], 1);

    // marker kanan bawah
    markerImage[1] = '{{ asset('assets/icons/kananatas.png') }}';
    
    @if (isset($options['location'][1][1]) && isset($options['location'][1][0]))
        position[1] = {
            'lat': parseFloat('{{ $options['location'][1][1] }}'),
            'lng': parseFloat('{{ $options['location'][1][0] }}')
        };
    @else 
        position[1] = {
            'lat': centerPos.lat + 0.05,
            'lng': centerPos.lng + 0.05
        };
    @endif

    marker[1] = new L.Marker(position[1], {
        draggable: true,
        icon: new L.icon({
            iconUrl: markerImage[1],
            iconSize: [60, 57],
            iconAnchor: [30, 57]
        })
    }).addTo(map);

    addMarkerListener(map, marker[1], 2);


    var autocomplete = new google.maps.places.Autocomplete(document.getElementById('inputGeoName'));
    autocomplete.setFields(['address_components', 'geometry']);

    addPlaceChangedListener(autocomplete, map, marker);

    $(document).ready(function() {

        $('#content-open, #content-hidden').summernote({
            height: 300,
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['insert', ['picture', 'link', 'video', 'table', 'hr']],
                ['misc', ['codeview']]
            ],
            popover: {
                image: [
                    ['custom', ['imageAttributes']],
                    ['remove', ['removeMedia']]
                ],
            },
            imageAttributes:{
                icon:'<i class="note-icon-pencil"/>',
                removeEmpty:false, // true = remove attributes | false = leave empty if present
                disableUpload: true // true = don't display Upload Options | Display Upload Options
            }
        });
    });

    
</script>
@endsection