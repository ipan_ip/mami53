@extends('admin.layouts.main')

@section('style')
    <link rel="stylesheet" href="/assets/vendor/summernote/summernote.css">

    <style>
        .note-group-select-from-files {
            display: none;
        }
    </style>
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

        <form action="{{ URL::route('admin.list-creator.store-job') }}" method="POST" class="form-horizontal form-bordered">
            <div class="box-body no-padding">
                <div class="form-group bg-default">
                    <label for="title" class="control-label col-sm-2">Title</label>
                    <div class="col-sm-10">
                        <input type="text" name="title" id="title" value="{{ old('title') }}" class="form-control" max="190">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="slug" class="control-label col-sm-2">Slug</label>
                    <div class="col-sm-10">
                        <input type="text" name="slug" id="slug" class="form-control" value="{{ old('slug') }}" max="190">
                    </div>
                </div>
    
                <div class="form-group bg-default">
                    <label for="city" class="col-sm-2 control-label">Kota</label>
                    <div class="col-sm-10">
                        <select name="city" id="city" class="form-control chosen-select">
                            @foreach($filterCities as $key => $city)
                                <option value="{{ $key }}"  {{ old('city') == $key ? 'selected="selected"' : '' }}>{{$city}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="type" class="col-sm-2 control-label">Status Pekerjaan</label>
                    <div class="col-sm-10">
                        <select name="job_status" id="type" class="form-control chosen-select">
                            <option value="all" {{ old('job_status', 'all') == 'all' ? 'selected="selected"' : '' }}>All</option>
                            @foreach($type as $key => $jobStatus)
                             <option value="{{ $jobStatus }}" {{ old('job_status') == $jobStatus ? 'selected="selected"' : '' }}>{{ $jobStatus }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="type" class="col-sm-2 control-label">Pendidikan Terakhir</label>
                    <div class="col-sm-10">
                        <select name="last_education" id="type" class="form-control chosen-select">
                            @foreach($educationOptions as $key => $education)
                             <option value="{{ $key }}" {{ old('last_education') == $key ? 'selected="selected"' : '' }}>{{ $education }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="inputSpesialisasiOption" class="col-sm-2 control-label">Spesialisasi</label>
                    <div class="col-sm-10">
                        <select name="spesialisasi" id="inputSpesialisasiOption" class="form-control chosen-select">
                            <option selected value="all">All</option>
                            @foreach($vacancySpesialisasi as $spesialisasi)
                             <option value="{{ $spesialisasi->id }}" {{ old('spesialisasi', $spesialisasi->id) == $spesialisasi->id ? : '' }} class="{{ $spesialisasi->group == 'general' ? 'opt-general' : 'opt-niche' }}">{{ $spesialisasi->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="partner-id" class="control-label col-sm-2">Aggregator Partner ID</label>
                    <div class="col-sm-10">
                        <select name="partner_id" id="partner-id" class="form-control chosen-select">
                            <option selected value="">Null</option>
                            @foreach($aggregatorPartner as $aggregator)
                             <option value="{{ $aggregator->id }}" {{ old('partner_id', $aggregator->id) == $aggregator->id ? : '' }}>{{ $aggregator->partner_name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="type" class="col-sm-2 control-label">Urutan</label>
                    <div class="col-sm-10">
                        <select name="sorting" id="type" class="form-control chosen-select">
                            @foreach($sortingOptions as $key => $sortingOption)
                             <option value="{{ $key }}" {{ old('sorting') == $key ? 'selected="selected"' : '' }}>{{ $sortingOption }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="type" class="col-sm-2 control-label">Filter yang diaktifkan</label>
                    <div class="col-sm-10">
                            @foreach($filterOptions as $key => $filterList)
                             <input name="active_filter[]" type="checkbox" value="{{ $key }}" {{ old('active_filter') == $key ? 'selected="selected"' : '' }}>{{ $filterList }}</input><br>
                            @endforeach
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="niche" class="col-sm-2 control-label">Landing Niche</label>
                    <div class="col-sm-10">
                        <input type="checkbox" name="is_niche" id="niche" value="1" {{ ( old('is_niche')) ? ' checked' : '' }}>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="type" class="col-sm-2 control-label">Description</label>
                    <div class="col-sm-10">
                        <textarea name="description" id="content-open" class="form-control" rows="10">{{ old('description') }}</textarea>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <div class="col-sm-10 col-sm-push-2">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('script')
<script src="/assets/vendor/summernote/summernote.min.js"></script>
<script src="/assets/vendor/summernote/plugin/summernote-image-attributes.js"></script>

<script>
    function slugify(text)
    {
      return text.toString().toLowerCase()
        .replace(/\s+/g, '-')           // Replace spaces with -
        .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
        .replace(/\-\-+/g, '-')         // Replace multiple - with single -
        .replace(/^-+/, '')             // Trim - from start of text
        .replace(/-+$/, '');            // Trim - from end of text
    }
    
    $(document).ready(function() {
        slug_state = 'unchanged';

        $('#title').on('keyup', function(e) {
            if(slug_state == 'unchanged') {
                slug = slugify($(this).val());

                $('#slug').val(slug);
            }
        });

        $('#slug').on('keyup', function(e) {
            slug_state = 'changed';
        });
        
        $('#content-open, #content-hidden').summernote({
            height: 300,
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['insert', ['picture', 'link', 'video', 'table', 'hr']],
                ['misc', ['codeview']]
            ],
            popover: {
                image: [
                    ['custom', ['imageAttributes']],
                    ['remove', ['removeMedia']]
                ],
            },
            imageAttributes:{
                icon:'<i class="note-icon-pencil"/>',
                removeEmpty:false, // true = remove attributes | false = leave empty if present
                disableUpload: true // true = don't display Upload Options | Display Upload Options
            }
        });

        $('#niche').on('ifChecked', function(e) {
            $('#inputSpesialisasiOption option.opt-general').hide();
            $('#inputSpesialisasiOption option.opt-niche').show();

            $('#inputSpesialisasiOption').val('');
        });

        $('#niche').on('ifUnchecked', function(e) {
            $('#inputSpesialisasiOption option.opt-general').show();
            $('#inputSpesialisasiOption option.opt-niche').hide();

            $('#inputSpesialisasiOption').val('');
        });

        if($('#niche').is(':checked')) {
            $('#inputSpesialisasiOption option.opt-general').hide();
            $('#inputSpesialisasiOption option.opt-niche').show();
        } else {
            $('#inputSpesialisasiOption option.opt-general').show();
            $('#inputSpesialisasiOption option.opt-niche').hide();
        }
    });

    
</script>
@endsection