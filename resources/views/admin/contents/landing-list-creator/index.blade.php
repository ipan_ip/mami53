@extends('admin.layouts.main')

@section('style')
    <style>
        tr.redirected td {
            color: #ff0000;
        }

        .text-redirected {
            color: #ff0000;
        }
    </style>
@endsection

@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <a href="{{ URL::route('admin.list-creator.create-job') }}">
                        <button type="button" name="button" class="btn-add btn btn-primary btn-sm">
                            <i class="fa fa-plus">&nbsp;</i> Add Landing List Job </button>
                    </a>
                    <a href="{{ URL::route('admin.list-creator.create-kost') }}">
                        <button type="button" name="button" class="btn-add btn btn-primary btn-sm">
                            <i class="fa fa-plus">&nbsp;</i> Add Landing List Kost </button>
                    </a>
                    <a href="{{ URL::route('admin.list-creator.create-apartment') }}">
                        <button type="button" name="button" class="btn-add btn btn-primary btn-sm">
                            <i class="fa fa-plus">&nbsp;</i> Add Landing List Apartment </button>
                    </a>
                    {{--<a href="{{ URL::route('admin.list-creator.create-company') }}">--}}
                        {{--<button type="button" name="button" class="btn-add btn btn-primary btn-sm">--}}
                            {{--<i class="fa fa-plus">&nbsp;</i> Add Landing List Company </button>--}}
                    {{--</a>--}}
                    <a href="{{ URL::route('admin.list-creator.create-specific') }}">
                        <button type="button" name="button" class="btn-add btn btn-primary btn-sm">
                            <i class="fa fa-plus">&nbsp;</i> Add Landing Description </button>
                    </a>
                    {{ Form::open(array('method'=>'get','class'=>'form-inline','style'=>'text-align:right;padding:10px;')) }}
                        <input type="text" name="q" class="form-control input-sm"  placeholder="Keyword"  autocomplete="off" value="{{ app('request')->input('q') }}">
                        <button class="btn btn-primary btn-md" id="buttonSearch"><i class="fa fa-search">&nbsp;</i>Search</button>
                    {{ Form::close() }}
                </div>
            </div>

            <table id="tableListLanding" class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Title</th>
                        <th>Type</th>
                        <th class="table-action-column" width="120px">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($landingLists as $landingList)
                    <tr @if ($landingList->redirect_id > 0 || !is_null($landingList->redirect_id)) style="color: #ff0000;" @endif>
                        <td>{{ $landingList->id }}</td>
                        <td>{{ $landingList->keyword }}</td>
                        <td>{{ $landingList->type }}</td>
                        <td align="center">
                            <a href="/admin/list-creator/redirect/{{ $landingList->id }}"><i class="fa fa-external-link"></i></a>
                            @if($landingList->type == 'job')
                                <a href="{{route('admin.list-creator.edit', $landingList->id)}}" title="edit">
                                    <i class="fa fa-pencil"></i>
                                </a>
                                <a href="https://mamikos.com/loker/{{ isset($landingList->option_value->is_niche) && $landingList->option_value->is_niche == 1 ? 'list/' : '1/list/' }}{{ $landingList->slug }}"
                                    target="_blank">
                                    <i class="fa fa-eye"></i>
                                </a>
                            @elseif ($landingList->type == 'specific')
                                <a href="{{route('admin.list-creator.edit-specific', $landingList->id)}}" title="edit">
                                    <i class="fa fa-pencil"></i>
                                </a>
                                <a href="https://mamikos.com/{{ $landingList->slug }}"
                                    target="_blank">
                                    <i class="fa fa-eye"></i>
                                </a>
                            @elseif ($landingList->type == 'kost')
                                <a href="{{route('admin.list-creator.edit-kost', $landingList->id)}}" title="edit">
                                    <i class="fa fa-pencil"></i>
                                </a>
                                <a href="https://mamikos.com/kost/infokost/{{ $landingList->slug }}"
                                    target="_blank">
                                    <i class="fa fa-eye"></i>
                                </a>
                            @elseif ($landingList->type == 'apartment')
                                <a href="{{route('admin.list-creator.edit-apartment', $landingList->id)}}" title="edit">
                                    <i class="fa fa-pencil"></i>
                                </a>
                                <a href="https://mamikos.com/apartemen/disewakan/{{ $landingList->slug }}"
                                    target="_blank">
                                    <i class="fa fa-eye"></i>
                                </a>
                            @endif

                            @if ($landingList->type != 'specific')
                                <a href="{{ route('admin.list-creator.delete', $landingList->id) }}">
                                    <i class="fa fa-trash-o"></i>
                                </a>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="box-body no-padding">
                {{ $landingLists->appends(Request::except('page'))->links() }}
            </div>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
    <!-- table -->
@endsection