@extends('admin.layouts.main')

@section('style')
    <link rel="stylesheet" href="/assets/vendor/summernote/summernote.css">

    <style>
        .note-group-select-from-files {
            display: none;
        }
    </style>
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

        <form action="{{ URL::route('admin.list-creator.update-specific', $landingList->id) }}" method="POST" class="form-horizontal form-bordered">
            <div class="box-body no-padding">
                <div class="form-group bg-default">
                    <label for="title" class="control-label col-sm-2">Title</label>
                    <div class="col-sm-10">
                        <input type="text" name="title" id="title" value="{{ old('title', $landingList->keyword) }}" class="form-control" max="190">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="slug" class="control-label col-sm-2">Slug</label>
                    <div class="col-sm-10">
                        <input type="text" name="slug" id="slug" class="form-control" value="{{ old('slug', $landingList->slug) }}" max="190">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="type" class="col-sm-2 control-label">Description</label>
                    <div class="col-sm-10">
                        <textarea name="description" id="description" class="form-control" rows="10">{{ old('description', $landingList->description) }}</textarea>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <div class="col-sm-10 col-sm-push-2">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('script')
<script src="/assets/vendor/summernote/summernote.min.js"></script>
<script src="/assets/vendor/summernote/plugin/summernote-image-attributes.js"></script>

<script>

    $(document).ready(function() {

        $('#description').summernote({
            height: 300,
            toolbar: [
                ['style', ['style', 'bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['insert', ['picture', 'link', 'video', 'table', 'hr']],
                ['misc', ['codeview']]
            ],
            popover: {
                image: [
                    ['custom', ['imageAttributes']],
                    ['remove', ['removeMedia']]
                ],
            },
            imageAttributes:{
                icon:'<i class="note-icon-pencil"/>',
                removeEmpty:false, // true = remove attributes | false = leave empty if present
                disableUpload: true // true = don't display Upload Options | Display Upload Options
            }
        });
    });

    
</script>
@endsection