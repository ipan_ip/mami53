@extends('embed.layouts.main')
@section('content')
	<div class="box box-primary">
		<div class="box-header">
			<h3 class="box-title">Add Designer's People</h3>
		</div>
		<div class="box-body no-padding">
		
		{{ Form::open(array('route' => $formAction, 'method' => $formMethod,
      		'class' => 'form-horizontal form-bordered', 'role' => 'form', 'id' => 'formInsertDesigner')) }}	
		<div>
			<div class="form-group bg-default">
				<label class="col-sm-2 control-label">Name</label>
				<input type="text" name="name" class="form-control" placeholder="Name" value="{{ @$people->name }}">
			</div>
			<div class="form-group bg-default">
				<label class="col-sm-2 control-label">Phone</label>
				<input type="text" name="phone" class="form-control" placeholder="Phone" value="{{ @$people->phone }}">
			</div>
			<div class="form-group bg-default">
				<label class="col-sm-2 control-label">Gender</label>
				<select name="gender" class="form-control">
					<option value="male"		{{ @($people->gender=='male')?'selected':'' }}	>	Male</option>
					<option value="female"	{{ @($people->gender=='female')?'selected':'' }}	>	Female</option>
				</select>
			</div>
			<input type="submit" class="btn btn-info" value="Submit" />
			<br/>
		</div>
	
	</div>
@stop	