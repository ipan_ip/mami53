@extends('embed.layouts.main')
@section('content')
	<div class="box box-primary">
		<div class="box-header">
			<h3 class="box-title">Designer's People</h3>
		</div>
		<div class="box-body no-padding">
            &nbsp; <a class="btn btn-primary btn-sm" href="{{ URL::Route('people.create') }}">+ Add People</a><br/>
            <a class="btn btn-primary btn-sm" href="{{ URL::Route('people.message') }}"> Send Message</a><br/>
			<table id="tableListStyle" class="table table-striped">
                <thead>
                    <tr>
                        <th width="30px;">No</th>
                        <th>Name</th>
                        <th>Gender</th>
                        <th>Phone</th>
                        <th>Status</th>
                        <th class="table-action-column" width="100px">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($people as $peopleKey => $rowPeople)
                    <tr>
                        <td>{{ $peopleKey+1 }}</td>
                        <td>{{ $rowPeople->name }}</td>
                        <td>{{ ucfirst($rowPeople->gender) }}</td>
                        <td>{{ $rowPeople->phone }}</td>
                        <td>{{ $rowPeople->status }}</td>
                        <td class="table-action-column">
                            <div class="btn-action-group">
                                <a href="{{ URL::Route('people.edit',$rowPeople->id) }}">
                                    <i class="fa fa-pencil"></i></a>
                                <a href="#" >
                                    <i class="fa fa-trash-o"></i></a>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th width="30px;">No</th>
                        <th>Name</th>
                        <th>Gender</th>
                        <th>Phone</th>
                        <th>Status</th>
                        <th class="table-action-column" width="100px">Action</th>
                    </tr>
                </tfoot>
            </table>
		</div>
	
	</div>
@stop	