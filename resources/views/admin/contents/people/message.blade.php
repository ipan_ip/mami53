@extends('embed.layouts.main')
@section('content')

	<div class="box box-primary">
		<div class="box-header">
			<h3 class="box-title">Add Designer's People</h3>
		</div>
		<div class="box-body no-padding">
		
		{{ Form::open(array('route' => $formAction, 'method' => $formMethod,
      		'class' => 'form-horizontal form-bordered', 'role' => 'form', 'id' => 'formInsertDesigner')) }}	
		<div>
			<div class="form-group bg-default">
				<label class="col-sm-2 control-label">Message</label>
				<input type="text" name="message" class="form-control" placeholder="Write message in 80 character or less" value="{{ @$people->name }}" maxlength="80">
			</div>
			<input type="submit" class="btn btn-info" value="Send" />
			<br/>
		</div>
	
	</div>
@stop	