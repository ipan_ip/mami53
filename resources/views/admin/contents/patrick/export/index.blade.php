@extends('admin.layouts.main')

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>


        <form action="{{ URL::to('admin/patrick/export/add') }}" method="POST" id="formInsertDesigner" class="form-horizontal form-bordered" role="form" enctype="multipart/form-data">  
        {{ csrf_field() }}
        
        <div class="box-body no-padding">
            <div class="form-group bg-default">
                <label for="inputName" class="col-sm-2 control-label">Upload CSV file</label>
                <div class="col-sm-10">
                    <input type="file" class="form-control" placeholder="CSV File"
                           id="inputName" name="file_csv" >
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputName" class="col-sm-2 control-label">Agent name</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Agent name"
                           id="inputName" name="agent_name" >
                </div>
            </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">Send Now</button>
            </div>
        </div>
    </div>
    </form><!-- /.form -->
    </div><!-- /.box -->
@stop