@extends('admin.layouts.main')

@section('style')
    <style>
        tr.redirected td {
            color: #ff0000;
        }

        .text-redirected {
            color: #ff0000;
        }
    </style>
@endsection

@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
               
            </div>

            <table id="tableListLanding" class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Company Name</th>
                        <th>Is live</th>
                        <th class="table-action-column" width="120px">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($company as $key => $value)
                    <tr>
                        <td>{{ $value->id }}</td>
                        <td>{{ $value->name }}</td>
                        <td>{{ $value->is_live }}</td>
                        <td><label class="label @if($value->is_live == 1) label-primary @else label-danger @endif ">{{ $value->status }}</label></td>
                        <td>
                            @if ($value->is_live == 1)
                              
                            @else 
                              <a href="{{ URL::to('admin/patrick/vacancy/company/edit',$value->id) }}" title="Edit" class="btn btn-xs btn-danger"><i class="fa fa-pencil"></i></a> 
                            @endif  
                            <a href="{{ $value->detail_url }}" target="_blank" rel="noopener" class="btn btn-xs btn-primary">Detail</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="box-body no-padding">
                {{ $company->appends(Request::except('page'))->links() }}
            </div>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
    <!-- table -->
@endsection
