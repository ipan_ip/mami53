@extends('admin.layouts.main')

@section('style')
    <style>
        tr.redirected td {
            color: #ff0000;
        }

        .text-redirected {
            color: #ff0000;
        }
    </style>
@endsection

@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header" style="margin-top: 15px;">
            <h3 class="box-title">{{ $boxTitle }}</h3>
            <a href="{{ URL::to('admin/patrick/vacancy/company') }}" class="btn btn-warning">Company profile</a>
            <a href="{{ URL::to('admin/patrick/vacancy/import') }}" class="btn btn-default">Import</a>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
               
            </div>

            <table id="tableListLanding" class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Jobs Name</th>
                        <th>Position</th>
                        <th>Type</th>
                        <th>Salary</th>
                        <th>Status</th>
                        <th class="table-action-column" width="120px">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($vacancys as $rowLandingKey => $value)
                    <tr class="{{ !is_null($value->redirect_id) ? 'redirected' : '' }}">
                        <td>{{ $value->id }}</td>
                        <td>{{ $value->name }}</td>
                        <td>{{ $value->position }}</td>
                        <td>{{ $value->type }}</td>
                        <td>{{ $value->salary }}/{{ $value->salary_time }}</td>
                        <td><label class="label @if($value->status == 'verified') label-primary @elseif ($value->status == 'waiting') label-warning @else label-danger @endif ">{{ $value->status }}</label></td>
                        <td>
                            @if ($value->is_active == 1)
                              
                            @else 
                              <a href="{{ URL::to('admin/patrick/vacancy/del', $value->id) }}" onclick="return confirm('Yakin mau hapus saya?')" class="btn btn-xs btn-warning"><i class="fa fa-trash-o"></i></a>
                              <a href="{{ URL::to('admin/patrick/vacancy/edit',$value->id) }}" title="Edit" class="btn btn-xs btn-danger"><i class="fa fa-pencil"></i></a> 
                            @endif  
                            <a href="{{ $value->url }}" target="_blank" rel="noopener" class="btn btn-xs btn-primary">Detail</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="box-body no-padding">
                {{ $vacancys->appends(Request::except('page'))->links() }}
            </div>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
    <!-- table -->
@endsection
