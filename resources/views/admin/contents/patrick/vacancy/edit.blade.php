@extends('admin.layouts.main')

@section('style')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/dropzone.css">
    {{ HTML::style('assets/vendor/jquery-chosen/chosen.css') }}
    {{ HTML::style('assets/vendor/jquery-chosen-custom/chosen.css') }}
    <link rel="stylesheet" href="/assets/vendor/summernote/summernote.css?v=05042018">

    <style>
        #chosenForm .chosen-choices {
            border: 1px solid #ccc;
            border-radius: 4px;
            min-height: 34px;
            padding: 6px 12px;
        }
        #chosenForm .form-control-feedback {
            /* To make the feedback icon visible */
            z-index: 100;
        }

        #map-canvas-1,
        #map-canvas-2 {
            height: 300px;
            width: 100%;
        }
    </style>
@endsection

@section('content')
<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">{{ $boxTitle }}</h3>
    </div><!-- /.box-header -->
    <form action="{{ URL::To('admin/patrick/vacancy/edit', $data->id) }}" method="post" class="form-horizontal form-bordered">

        <div class="box-body no-padding">

            <div class="form-group bg-default">
                <label for="inputUrlSlug" class="col-sm-2 control-label">Company</label>
                <div class="col-sm-10">
                    <div class="col-sm-11">
                        <input type="text" name="company_profile" id="inputName" value="{{ old('company_profile') }}" class="form-control">
                    </div>
                    <div class="col-sm-1">
                        <input type="button" name="search" class="btn btn-success" value="Search" id="inputNameCheck">
                    </div>
                </div>


                 <div style="margin-left: 10px; margin-top: 30px; width: 100%;">
                    <div id="hasil"></div>
                </div>  

            </div>

            <div class="form-group bg-default">
                <label for="inputUrlSlug" class="col-sm-2 control-label">Place Name</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Place Name" id="inputSlugUrl" name="place_name"  value="{{ $data->place_name }}">
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputUrlSlug" class="col-sm-2 control-label">Slug</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Place Name" id="inputSlugUrl" name="slug"  value="{{ $data->slug }}">
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputAddress" class="col-sm-2 control-label">Penempatan Kerja</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Penempatan" id="inputAddress" name="workplace" value="">
                </div>
            </div>

            <div class="form-group bg-default">
            <label for="inputIndexed" class="col-sm-2 control-label">Harus sesuai penempatan kerja</label>
            <div class="col-sm-10">
                <div class="checkbox">
                        <input type="checkbox" name="workplace_status">
                        Yes
                </div>
            </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputUrlSlug" class="col-sm-2 control-label">Address</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Place Name" id="inputSlugUrl" name="address"  value="{{ $data->address }}">
                </div>
            </div>

            <div class="form-group bg-default">
                    <label for="inputGeoName" class="col-sm-2 control-label">Koordinat</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" placeholder="Geo Name"
                               id="inputGeoName" name="geo_name">
                    </div>
                    <div class="col-sm-10 col-sm-offset-2">
                        <div style="background: #EEE; width: 100%; height: 300px;">
                            <div id="map-canvas"></div>
                        </div>
                    </div>
                    <div class="col-sm-10 col-sm-offset-2">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="latitude"
                               id="latitude" name="latitude" value="{{ $data->latitude }}">
                            <input type="text" class="form-control" placeholder="longitude"
                               id="longitude" name="longitude" value="{{ $data->longitude }}">
                       </div>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="subdistrict" class="control-label col-sm-2">Subdistrik</label>
                    <div class="col-sm-10">
                        <input type="text" name="subdistrict" id="subdistrict" class="form-control" value="{{ $data->subdistrict }}">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="area_city" class="control-label col-sm-2">Kota / Daerah</label>
                    <div class="col-sm-10">
                        <input type="text" name="city" id="area_city" class="form-control" value="{{ $data->city }}">
                    </div>
                </div>

            <div class="form-group bg-default">
                <label for="inputUrlSlug" class="col-sm-2 control-label">Jobs Name</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Jobs Name" id="inputSlugUrl" name="jobs_name"  value="{{ $data->name }}">
                </div>
            </div>  

<!--             <div class="form-group bg-default">
                <label for="inputUrlSlug" class="col-sm-2 control-label">Position</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Position" id="inputSlugUrl" name="position"  value="{{ $data->position }}">
                </div>
            </div> -->

            <div class="form-group bg-default">
                <label for="inputUrlSlug" class="col-sm-2 control-label">Salary</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Salary" id="inputSlugUrl" name="salary"  value="{{ $data->salary }}">
                </div>
            </div>  

            <div class="form-group bg-default">
                <label for="inputUrlSlug" class="col-sm-2 control-label">Salary Max</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Salary Max" id="inputSlugUrl" name="max_salary"  value="{{ $data->max_salary }}">
                </div>
            </div>  

            <div class="form-group bg-default">
                <label for="inputUrlSlug" class="col-sm-2 control-label">Salary Time</label>
                <div class="col-sm-10">
                  <select class="form-control" name="salary_time">   
                    @foreach ($times AS $key => $value)
                      @if ($value == $data['salary_time'])
                        <option value="{{ $value }}" selected="true">{{ $value }}</option>
                      @else
                        <option value="{{ $value }}">{{ $value }}</option> 
                      @endif
                    @endforeach
                  </select>  
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputUrlSlug" class="col-sm-2 control-label">Type</label>
                <div class="col-sm-10">
                  <select class="form-control" name="type">   
                    @foreach ($type AS $key => $value)
                      @if ($value == $data['type'])
                        <option value="{{ $value }}" selected="true">{{ $value }}</option>
                      @else
                        <option value="{{ $value }}">{{ $value }}</option> 
                      @endif
                    @endforeach
                  </select>  
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputUrlSlug" class="col-sm-2 control-label">Education</label>
                <div class="col-sm-10">
                  <select class="form-control" name="education">   
                    @foreach ($educations AS $key => $value)
                        <option value="{{ $key }}" selected="true">{{ $value }}</option>
                    @endforeach
                  </select>  
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="expired_date" class="col-sm-2 control-label">Expired Date</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Expired Date (YYYY-MM-DD)" id="expired_date" name="expired_date"  value="{{ old('expired_date', $data->expired_date) }}">
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputDescription" class="col-sm-2 control-label">Deskripsi</label>
                <div class="col-sm-10">
                    <textarea name="description" id="inputDescription" class="form-control">{{ $data->description }}</textarea>
                </div>
            </div> 

            <div class="form-group bg-info divider">
              <div class="col-sm-12 pull-left">
                User
              </div>
            </div> 

            <div class="form-group bg-default">
                <label for="inputUrlSlug" class="col-sm-2 control-label">User Name</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="User Name" id="inputSlugUrl" name="user_name"  value="{{ $data->user_name }}">
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputUrlSlug" class="col-sm-2 control-label">Phone</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" placeholder="Place Name" id="inputOwnerPhone" name="user_phone"  value="{{ $data->user_phone }}">
                    <div id="kost_show"></div>
                </div>
                <div class="col-sm-2">
                    <input type="button" id="checknohpowner" class="btn btn-danger" value="Cek No HP LIVE" />
                </div>
            </div>  

            <div class="form-group bg-default">
                <label for="inputUrlSlug" class="col-sm-2 control-label">Email</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Place Name" id="inputSlugUrl" name="user_email"  value="{{ $data->user_email }}">
                </div>
            </div>  
            <input type="hidden" name="detail_url" value="{{ $data->url }}">

            <div class="form-group bg-info divider">
              <div class="col-sm-12 pull-left">
                Agent
              </div>
            </div> 

            <div class="form-group bg-default">
                <label for="agen" class="col-sm-2 control-label">Agent Name</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Agent Name" id="agen" name="agent_name"  value="{{ $data->agent_name }}">
                </div>
            </div> 

            <div class="form-group bg-info divider">
                <div class="col-sm-12">
                    Global Setting
                </div>
            </div>   

            <div class="form-group bg-default">
            <label for="inputIndexed" class="col-sm-2 control-label">Live Now</label>
            <div class="col-sm-10">
                <div class="checkbox">
                        <input type="checkbox" name="live">
                        Yes
                </div>
            </div>
            </div>

            <div class="form-group bg-info divider">
              <div class="col-sm-12 pull-left">
                Additional
              </div>
            </div> 

            <!--
            <div class="form-group bg-default">
                <label for="inputGroup" class="col-sm-2 control-label">Group</label>
                <div class="col-sm-10">
                    <select class="form-control" name="group" id="inputGroup">
                        @foreach ($groupOptions as $group)
                            <option value="{{ $group }}" {{ old('group', $data->group) == $group ? 'selected="selected"' : '' }}>{{ $group }}</option>
                        @endforeach
                    </select>
                </div>
            </div>-->

            <div class="form-group bg-default">
                <label for="verificator" class="col-sm-2 control-label">Spesialisasi</label>
                <div class="col-sm-10">
                    <select class="form-control" name="spesialisasi" id="inputSpesialisasi">
                        @foreach ($spesialisasi as $key => $value)
                            <option value="{{ $value->id }}" {{ old('spesialisasi') == $value->id ? 'selected="selected"' : '' }}>{{ $value->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="verificator" class="col-sm-2 control-label">Industry</label>
                <div class="col-sm-10">
                    <select class="form-control" name="industry">
                        @foreach ($industry AS $key => $index)
                            <option value="{{ $index->id }}">{{ $index->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
               
               <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </form>
</div><!-- /.box -->
@endsection



@section('script')

{{ HTML::script('assets/vendor/jquery-chosen/chosen.jquery.js') }}
<script src="/assets/vendor/summernote/summernote.min.js"></script>

<script>
    var config = {
        '.chosen-select'           : {width: '100%'}
    };

    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>


<script src="https://maps.googleapis.com/maps/api/js?libraries=places&language=id&key={{ENV('MIX_GOOGLE_MAPS_JAVASCRIPT_API_KEY')}}"></script>

<script type="text/javascript">
    
    <?php
        if (is_null($data->latitude) OR is_null($data->longitude)) {
    ?>
        centerPos = {lat: -7.7858485, lng: 110.3680087};
    <?php          
        } else {
    ?>
        centerPos = {lat: <?php echo $data->latitude; ?>, lng: <?php echo $data->longitude; ?>};
    <?php 
        }
    ?>
        var map = new google.maps.Map(document.getElementById("map-canvas"),
            {
                center: centerPos,
                zoom:12
            }
        );

        var marker = new google.maps.Marker({
            position: centerPos,
            map: map,
            draggable: true,

        });

        var autocomplete = new google.maps.places.Autocomplete(document.getElementById('inputGeoName'));
        autocomplete.bindTo('bounds', map);

        var geocoder = new google.maps.Geocoder();

        google.maps.event.addListener(marker, 'dragend', function(evt) {
            var lat = evt.latLng.lat();
            var lng = evt.latLng.lng();

            var latlng = new google.maps.LatLng(lat, lng);

            geocoder.geocode({'latLng': latlng}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[1]) {

                        map.setCenter(latlng);
                        marker.setPosition(latlng);

                        $('#inputGeoName').val(results[1].formatted_address);
                        $('#latitude').val(lat);
                        $('#longitude').val(lng);

                        setDistrictAndCity(results[0]);
                    } else {
                        alert('No results found');
                    }
                } else {
                    alert('Geocoder failed due to: ' + status);
                }
            });
        });


        autocomplete.addListener('place_changed', function() {
            marker.setVisible(false);
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                // User entered the name of a Place that was not suggested and
                // pressed the Enter key, or the Place Details request failed.
                window.alert("No details available for input: '" + place.name + "'");
                return;
            }

            map.setCenter(place.geometry.location);
            marker.setPosition(place.geometry.location);
            marker.setVisible(true);

            $('#latitude').val(place.geometry.location.lat);
            $('#longitude').val(place.geometry.location.lng);

            setDistrictAndCity(place);
        });

        function setDistrictAndCity(response) {
            subDistrict = '';
            city = '';
            response.address_components.forEach(function(component) {
                // console.log(component.types);
                if ($.inArray("administrative_area_level_3", component.types) != -1) {
                    subDistrict = component.short_name;
                }

                if ($.inArray("administrative_area_level_2", component.types) != -1) {
                    city = component.short_name;      
                }

            });

            $('#subdistrict').val(subDistrict);
            $('#area_city').val(city);
        }

            var geocoder;
            geocoder = new google.maps.Geocoder();
    
    <?php
        if (is_null($data->latitude) OR is_null($data->longitude)) {
    ?>
        var latlng = new google.maps.LatLng(-7.7858485, 110.3680087);
    <?php          
        } else {
    ?>
        var latlng = new google.maps.LatLng(<?php echo $data->latitude; ?>, <?php echo $data->longitude; ?>);
    <?php 
        }
    ?>
            geocoder.geocode(
                {'latLng': latlng}, 
                function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            var add= results[0].formatted_address ;
                            var  value=add.split(",");
                            console.log(value)
                            count=value.length;
                            country=value[count-1];
                            state=value[count-2];
                            city=value[count-3];
                            subDistrict=value[count-4];
                            $('#subdistrict').val(subDistrict);
                            $('#area_city').val(city);
                        } else  {
                            x.innerHTML = "address not found";
                        }
                    } else {
                        x.innerHTML = "Geocoder failed due to: " + status;
                    }
                }
            );

    $('#inputDescription').summernote({
        height: 300,
        toolbar: [
            ['style', ['style', 'bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['insert', ['link', 'table', 'hr']],
            ['misc', ['codeview']]
        ]
    });

    $('#checknohpowner').click(function(e) {
      phone = $("#inputOwnerPhone").val();
      $("#kost_show").append("Loading ................ ");
      $.ajax({
        url: '/admin/patrick/vacancy/check-from-no-hp?phone=' + phone,
        type: 'GET',
        dataType: 'json',
        success: function(data) {
          $("#kost_show").empty();  
          if(data.status == false) {
            
            $("#kost_show").append("<div class='alert alert-danger' style='width: 100%;'>No hp diatas belum punya lowongan.");
          } else {
            var project = "<strong>List</strong>";

            $.each(data.data, function(index, value) {
                project += "<ul>";
                project += "<li>"+value.id+" - "+value.name+"</li>";
                project += "</ul>";
            });

            $("#kost_show").append(project);
          }
        }
      });
    });


    $('#inputNameCheck').click(function(e) {
      theName = $("#inputName").val();
      $("#hasil").append("Loading ................ ");
      $.ajax({
        url: '/admin/jobs/i/company?name=' + theName,
        type: 'GET',
        dataType: 'json',
        success: function(data) {
          $("#hasil").empty();  
          if(data.status == false) {
            
            $("#hasil").append("Tambah profil perusahaan <a href='/admin/jobs/company/new' target='_blank'>Tambah</a>");
          } else {
            var project = "<strong>Hasil pencarian</strong>";
            project += "<ul>";
            $.each(data.data, function(index, value) {

                if (value.is_active == 1) {
                    project += "<li>"+value.id+" - "+value.name+". <a href='/admin/jobs/company/edit/"+value.id+"' target='_blank'>view</a> - <label style='cursor: pointer; text-decoration: underline;' id='choosein' str-id="+value.id+">pilih ini</label></li>";
                } else {
                    project += "<li style='color: #ff0000;'>"+value.id+" - "+value.name+". <a href='/admin/jobs/company/edit/"+value.id+"' target='_blank'>view</a> - <label style='cursor: pointer; text-decoration: underline;' id='choosein' str-id="+value.id+">pilih ini</label></li>";
                }

            });
            project += "Tidak ada dalam pilihan ? Tambah project baru <strong><a href='/admin/jobs/company/new' target='_blank'>Tambah</a></strong><br/>";
            project += "</ul>";

            $("#hasil").append(project);
          }
        }
      });
    });

    $(document).on('click', '#choosein', function(e) {
      var id = $(this).attr("str-id");
      document.getElementById('inputName').value = id;
    });

    
</script>
@endsection
