@extends('admin.layouts.main')

@section('content')
    <div class="box box-primary">
     
        <form action="{{ URL::to('admin/patrick/house_property/live', $data->id) }}" method="POST" id="formInsertDesigner" class="form-horizontal form-bordered" role="form">  
        {{ csrf_field() }}
        
        <div class="box-body no-padding">
            <div class="form-group bg-default">
                <label for="inputName" class="col-sm-2 control-label">Title</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Title"
                           id="inputName" name="name"  value="{{ $data->name }}">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Type house</label>
                <div class="col-md-10">
                    <select class="form-control" name="type">
                        <option value="villa">Villa</option>
                        <option value="rented_house">Kontrakan</option>
                    </select>
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputSize" class="col-sm-2 control-label">Size</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Size"
                           id="inputSize" name="size"  value="{{ $data->size }}">
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputPriceDaily" class="col-sm-2 control-label">Price Daily</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Price Daily"
                           id="inputPriceDaily" name="price_daily" value="{{ $data->price_daily }}" >
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputPriceWeekly" class="col-sm-2 control-label">Price Weekly</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Price Weekly"
                           id="inputPriceWeekly" name="price_weekly" value="{{ $data->price_weekly }}" >
                    <p style="margin-top: 10px; color: #ff0000;"><strong>Harus angka</strong></p>
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputPriceMonthly" class="col-sm-2 control-label">Price Monthly *</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Price Monthly"
                           id="inputPriceMonthly" name="price_monthly" value="{{ $data->price_monthly }}" >
                    <p style="margin-top: 10px; color: #ff0000;"><strong>Harus angka</strong></p>
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputPriceYearly" class="col-sm-2 control-label">Price Yearly</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Price Yearly"
                           id="inputPriceYearly" name="price_yearly" value="{{ $data->price_yearly }}">
                           <p style="margin-top: 10px; color: #ff0000;"><strong>Harus angka</strong></p>
                    <div id="countTitle" style="font-weight:bold"></div>
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputFloor" class="col-sm-2 control-label">Floor</label>
                <div class="col-sm-10">
                    <input type="number" class="form-control" placeholder="Floor"
                           id="inputFloor" name="floor" value="{{ $data->floor }}">
                           <p style="margin-top: 10px; color: #ff0000;"><strong>Harus angka</strong></p>
                    <div id="countTitle" style="font-weight:bold"></div>
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputFloor" class="col-sm-2 control-label">Building year</label>
                <div class="col-sm-10">
                    <input type="number" class="form-control" placeholder="Building year"
                           id="inputFloor" name="building_year" value="">
                           <p style="margin-top: 10px; color: #ff0000;"><strong>Harus angka</strong></p>
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputFloor" class="col-sm-2 control-label">Is available</label>
                <div class="col-sm-10">
                    <select class="form-control" name="is_available">
                        <option value="1">Yes</option>
                        <option value="0">No</option>
                    </select>
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputIndexed" class="col-sm-2 control-label">Indexed</label>
                <div class="col-sm-10">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" checked="checked" name="indexed" value="1">
                            Indexed
                        </label>
                    </div>
                </div>
            </div>

            <div class="form-group bg-info divider">
                <div class="col-sm-12">
                    Location
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputAddress" class="col-sm-2 control-label">Address</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Address"
                           id="inputAddress" name="address" value="{{ $data->address }}" >
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputGeoName" class="col-sm-2 control-label">Input Coordinate</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Geo Name"
                           id="inputGeoName" value="" name="geo_name">
                </div>
                <div class="col-sm-10 col-sm-offset-2">
                    <div style="background: #EEE; width: 100%; height: 300px;">
                        <div id="map-canvas"></div>
                    </div>
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputLongitude" class="col-sm-2 control-label">Longitude *</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Longitude"
                           id="inputLongitude" name="longitude" value="{{ $data->longitude }}">
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputLatitude" class="col-sm-2 control-label">Latitude *</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Latitude"
                           id="inputLatitude" name="latitude" value="{{ $data->latitude }}">
                </div>
            </div>

            <div class="form-group bg-default">
                <label class="col-sm-2 control-label" for="inputSubdistrict">Subdistrict *</label>
                <div class="col-sm-10">
                    <input id="inputSubdistrict" class="form-control"  name="area_subdistrict" placeholder="Subdistrict" value="{{ old('area_subdistrict') }}">
                </div>
            </div>

            <div class="form-group bg-default">
                <label class="col-sm-2 control-label" for="inputAreaCity">City/Regency</label>
                <div class="col-sm-10">
                    <input id="inputAreaCity" class="form-control" rows="3" name="area_city" placeholder="Kota" value="{{ old('area_city') }}">
                </div>
            </div>

            <div class="form-group bg-default">
                <label class="col-sm-2 control-label" for="inputGuide" >Bed room total</label>
                <div class="col-sm-10">
                    <input id="inputGuide" type="number" class="form-control" name="bed_room" placeholder="Bed room total" value="{{ $data->bed_room }}">
                </div>
            </div>

            <div class="form-group bg-default">
                <label class="col-sm-2 control-label" for="inputGuide" >Bed total</label>
                <div class="col-sm-10">
                    <input id="inputGuide" type="number" class="form-control" name="bed_total" placeholder="Bed total" value="{{ $data->bed_total }}">
                </div>
            </div>

            <div class="form-group bg-default">
                <label class="col-sm-2 control-label" for="inputGuide" >Guest max</label>
                <div class="col-sm-10">
                    <input id="inputGuide" type="number" class="form-control" name="guest_max" placeholder="Guest max" value="{{ $data->guest_max }}">
                </div>
            </div>

            <div class="form-group bg-default">
                <label class="col-sm-2 control-label" for="inputGuide" >Bathroom total</label>
                <div class="col-sm-10">
                    <input id="inputGuide" type="number" class="form-control" name="bath_total" placeholder="Bathroom total" value="{{ $data->bath_total }}">
                </div>
            </div>

            <div class="form-group bg-default">
                <label class="col-sm-2 control-label" for="inputGuide" >Parking</label>
                <div class="col-sm-10">
                    <select name="parking" class="form-control">
                        <option value="mobil,motor">Mobil & Motor</option>
                        <option value="mobil">Mobil</option>
                        <option value="motor">Motor</option>
                    </select>
                </div>
            </div>

            <div class="form-group bg-info divider">
                <div class="col-sm-12">
                    Information
                </div>
            </div>

            <!-- select -->
            <div class="form-group bg-default">
                <label for="inputConcern" class="col-sm-2 control-label">Tags</label>
                <div class="col-sm-10">
                    <select class="form-control chosen-select" multiple id="inputConcern" name="concern_ids[]" tabindex="2">
                        @foreach ($tag as $rowConcern)
                            <option value="{{ $rowConcern->id }}"
                                    {{ !is_null(old('concern_ids')) && in_array($rowConcern->id, old('concern_ids')) ? 'selected="selected"' : '' }}>{{ $rowConcern->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>

        <div class="form-group bg-default">
            <label for="inputFacRoomOther" class="col-sm-2 control-label">Biaya lainnya</label>
            <div class="col-sm-10">
                <textarea class="form-control" id="inputFacRoomOther" name="other_cost_information"></textarea>
            </div>
        </div>

        <div class="form-group bg-default">
            <label for="inputFacBathOther" class="col-sm-2 control-label">Informasi lainnya</label>
            <div class="col-sm-10">
                <textarea class="form-control" rows="3" id="inputFacBathOther" name="other_information"></textarea>
            </div>
        </div>

        <div class="form-group bg-default">
            <label for="inputDescription" class="col-sm-2 control-label">Description</label>
            <div class="col-sm-10">
                <textarea class="form-control" rows="3" id="inputDescription" name="description" maxlength="1000" onkeyup="countCharDesc(this,1000)">{{ trim($data->description) }}</textarea>
            </div>
        </div>

        <div class="form-group bg-info divider">
            <div class="col-sm-12">
                Contact
            </div>
        </div>

        <div class="form-group bg-default">
            <label class="col-sm-2 control-label" for="inputOwnerName">Nama Pemilik *</label>
            <div class="col-sm-10">
                <input id="inputOwnerName" class="form-control" name="owner_name" placeholder="Nama Pemilik">
            </div>
        </div>

        <div class="form-group bg-default">
            <label class="col-sm-2 control-label" for="inputOwnerPhone">No. HP Pemilik *</label>
            <div class="col-sm-8">
                <input id="inputOwnerPhone" class="form-control" rows="3" name="owner_phone" placeholder="No. HP Pemilik" value="{{ $data->phone }}">
            </div>
        </div>

        <div class="form-group bg-default">
            <label class="col-sm-2 control-label" for="inputManagerName">Nama Manajer</label>
            <div class="col-sm-10">
                <input id="inputManagerName" class="form-control" name="manager_name" placeholder="Nama Manajer">
            </div>
        </div>

        <div class="form-group bg-default">
            <label class="col-sm-2 control-label" for="inputManagerPhone">No. HP Manajer</label>
            <div class="col-sm-10">
                <input id="inputManagerPhone" class="form-control" name="manager_phone" placeholder="No. HP Manajer">
            </div>
        </div>

        <div class="form-group bg-default">
            <label class="col-sm-2 control-label" for="inputAgentName">Nama Agen *</label>
            <div class="col-sm-10">
                <input id="inputAgentName" class="form-control" name="agent_name" placeholder="Nama Agen">
            </div>
        </div>

        <div class="form-group bg-default" for="inputAgentStatus">
            <label class="col-sm-2 control-label">Status Agent *</label>
            <div class="col-sm-10">
                <select class="form-control" name="agent_status" id="inputAgentStatus">
                        <option value="scrap">Scrap</option>
                </select>
            </div>
        </div>

        <div class="form-group bg-info divider">
            <div class="col-sm-12">
                Global Setting
            </div>
        </div>

        <div class="form-group bg-default" for="inputAgentStatus">
            <label class="col-sm-2 control-label">Images</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="images" id="inputImages" value="{{ $data->cover }}" />
                <p style="margin-top: 10px;">Format image: <strong style="color: #ff0000">(http/https)://........(jpg/png/jpeg/bmp)</strong></p>
            </div>
           <!--  <div class="col-sm-2"><input type="button" class="btn btn-danger" id="changedummyimages" name="" value="Set No images"></div> -->
        </div>

        <div class="form-group bg-default">
            <label for="inputIndexed" class="col-sm-2 control-label">Download images</label>
            <div class="col-sm-10">
                <div class="checkbox">
                        <input type="checkbox" name="download">
                        download now
                </div>
            </div>
        </div>

        <div class="form-group bg-default">
            <label for="inputIndexed" class="col-sm-2 control-label">Live Now</label>
            <div class="col-sm-10">
                <div class="checkbox">
                        <input type="checkbox" name="live">
                        Yes
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-12">
                <button type="submit" class="btn btn-primary" style="width: 100%;">Save</button>
            </div>
        </div>
    </div>
    </form><!-- /.form -->
    </div><!-- /.box -->
@stop

@section('style')
    {{ HTML::style('assets/vendor/jquery-chosen/chosen.css') }}
    {{ HTML::style('assets/vendor/jquery-chosen-custom/chosen.css') }}

    <style>
        #chosenForm .chosen-choices {
            border: 1px solid #ccc;
            border-radius: 4px;
            min-height: 34px;
            padding: 6px 12px;
        }
        #chosenForm .form-control-feedback {
            /* To make the feedback icon visible */
            z-index: 100;
        }
    </style>
@stop

@section('script')
<link rel="stylesheet" href="/assets/vendor/summernote/summernote.css?v=05042018">
            <!-- page script -->
    {{ HTML::script('assets/vendor/jquery-chosen/chosen.jquery.js') }}
    <script type="text/javascript">
        var config = {
            '.chosen-select'           : {width: '100%'}
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }
    </script>

<script src="/assets/vendor/summernote/summernote.min.js"></script>

<script type="text/javascript">
        $('#inputDescription').summernote({
            height: 300,
            toolbar: [
                ['style', ['style', 'bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['insert', ['link', 'table', 'hr']],
                ['misc', ['codeview']]
            ]
        });

</script>

    <script> //count character
        function countCharTitle(val,maxlength) //title card
        {
            var len = val.value.length;
            if (len > maxlength) {
                val.value = val.value.substring(0, maxlength);
            } else {
                $('#countTitle').text(maxlength - len);
            }
        };
    </script>
    <!-- Google Map JS -->

    <script src="https://maps.googleapis.com/maps/api/js?libraries=places&language=id&key={{ENV('MIX_GOOGLE_MAPS_JAVASCRIPT_API_KEY')}}"></script>

    <script type="text/javascript">
        centerPos = {
            lat: <?php echo $data->latitude; ?>, 
            lng: <?php echo $data->longitude; ?>
        }

        var map = new google.maps.Map(document.getElementById("map-canvas"),
            {
                center: centerPos,
                zoom:12
            }
        );

        var marker = new google.maps.Marker({
            position: centerPos,
            map: map,
            draggable: true,

        });

        var autocomplete = new google.maps.places.Autocomplete(document.getElementById('inputGeoName'));
        autocomplete.bindTo('bounds', map);

        var geocoder = new google.maps.Geocoder();

        google.maps.event.addListener(marker, 'dragend', function(evt) {
            var lat = evt.latLng.lat();
            var lng = evt.latLng.lng();

            var latlng = new google.maps.LatLng(lat, lng);

            geocoder.geocode({'latLng': latlng}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[1]) {

                        map.setCenter(latlng);
                        marker.setPosition(latlng);

                        $('#inputGeoName').val(results[1].formatted_address);
                        $('#inputLatitude').val(lat);
                        $('#inputLongitude').val(lng);

                        setDistrictAndCity(results[0]);
                    } else {
                        alert('No results found');
                    }
                } else {
                    alert('Geocoder failed due to: ' + status);
                }
            });
        });


        autocomplete.addListener('place_changed', function() {
            marker.setVisible(false);
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                // User entered the name of a Place that was not suggested and
                // pressed the Enter key, or the Place Details request failed.
                window.alert("No details available for input: '" + place.name + "'");
                return;
            }

            map.setCenter(place.geometry.location);
            marker.setPosition(place.geometry.location);
            marker.setVisible(true);

            $('#inputLatitude').val(place.geometry.location.lat);
            $('#inputLongitude').val(place.geometry.location.lng);

            setDistrictAndCity(place);
        });

        function setDistrictAndCity(response) {
            subDistrict = '';
            city = '';
            response.address_components.forEach(function(component) {
                // console.log(component.types);
                if ($.inArray("administrative_area_level_3", component.types) != -1) {
                    subDistrict = component.short_name;
                }

                if ($.inArray("administrative_area_level_2", component.types) != -1) {
                    city = component.short_name;      
                }

            });

            $('#inputSubdistrict').val(subDistrict);
            $('#inputAreaCity').val(city);
        }

    $('#changedummyimages').click(function(e) {
        document.getElementById('inputImages').value = 'http://songturu.mamikos.com/uploads/file/noimages.png';
        //document.getElementById('')
    });

    </script>

@stop
