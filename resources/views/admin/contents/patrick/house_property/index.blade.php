@extends('admin.layouts.main')

@section('style')
    <style>
        tr.redirected td {
            color: #ff0000;
        }

        .text-redirected {
            color: #ff0000;
        }
    </style>
@endsection

@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <form class="form-horizontal" style="margin-top: 20px;" role="form" method="POST" action="/admin/patrick/house_property/import" enctype="multipart/form-data">
                    <div class="form-group">
                        <div class="col-md-3">
                            <input type="file" class="form-control" name="json_file" id="inputType" placeholder="Type">
                        </div>
                        <div class="col-md-3">
                            <input type="submit" class="btn btn-default" value="Import !!!">
                        </div>
                    </div>
                </form>
            </div>

            <table id="tableListLanding" class="table table-striped">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Website</th>
                        <th>Name</th>
                        <th>Address</th>
                        <th>Status</th>
                        <th class="table-action-column" width="120px">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $key => $value)
                    <tr>
                        <td>{{ $value->id }}</td>
                        <td>{{ getDomainFromSlug($value->web_url) }}</td>
                        <td>{{ $value->name }}</td>
                        <td>{{ $value->address }}</td>
                        <td><label class="label @if($value->status == 'verified') label-primary @elseif ($value->status == 'waiting') label-warning @else label-danger @endif ">{{ $value->status }}</label></td>
                        <td>
                            @if ($value->status == "waiting")
                              <a href="{{ URL::to('admin/patrick/house_property/edit',$value->id) }}" title="Edit" class="btn btn-xs btn-danger"><i class="fa fa-pencil"></i></a> 
                            @endif  
                            <a href="{{ $value->web_url }}" target="_blank" rel="noopener" class="btn btn-xs btn-primary">Detail</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="box-body no-padding">
                {{ $data->appends(Request::except('page'))->links() }}
            </div>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
    <!-- table -->
@endsection
