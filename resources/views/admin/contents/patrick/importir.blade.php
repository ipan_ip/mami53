@extends('admin.layouts.main')

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>


        <form action="{{ URL::to('admin/patrick/importir') }}" method="POST" id="formInsertDesigner" class="form-horizontal form-bordered" role="form" enctype="multipart/form-data">  
        {{ csrf_field() }}
        
        <div class="box-body no-padding">

            <div class="form-group bg-default">
                <label for="inputName" class="col-sm-2 control-label">Username yg mau di check</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Masukkan username"
                           id="inputName" name="named" >
                </br>           
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputName" class="col-sm-2 control-label">Import CSV file</label>
                <div class="col-sm-10">
                    <input type="file" class="form-control" placeholder="CSV File"
                           id="inputName" name="file_csv" >
                </br>           
                </div>
            </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">Check Now</button>
            </div>
        </div>
    </div>
    </form><!-- /.form -->
    </div><!-- /.box -->
@stop