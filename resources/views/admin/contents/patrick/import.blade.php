@extends('admin.layouts.main')

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>


        <form action="{{ URL::to('admin/patrick/import') }}" method="POST" id="formInsertDesigner" class="form-horizontal form-bordered" role="form" enctype="multipart/form-data">  
        {{ csrf_field() }}
        
        <div class="box-body no-padding">
            <div class="form-group bg-default">
                <label for="inputName" class="col-sm-2 control-label">Import CSV file</label>
                <div class="col-sm-10">
                    <input type="file" class="form-control" placeholder="CSV File"
                           id="inputName" name="file_csv" >
                </br>           
                <span>Download template: <a href="http://songturu.mamikos.com/uploads/file/template.csv">click here</a></span>
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputName" class="col-sm-2 control-label">Scrap Koran</label>
                <div class="col-sm-10">
                    <div class="checkbox">
                        <input type="checkbox" name="scrap_koran" id="sc">
                        Yes
                    </div>
                    <br/>
                    <span>Scrap koran akan masuk ke live, jika import hasil scrap nggak usah dicentang</span>
                </div>
            </div>
            

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">Import Now</button>
            </div>
        </div>
    </div>
    </form><!-- /.form -->
    </div><!-- /.box -->
@stop