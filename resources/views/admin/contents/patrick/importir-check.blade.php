@extends('admin.layouts.main')
@section('content')
    <!-- table ALTER TABLE dummy_data ADD data_from VARCHAR(10) AFTER is_live -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
            <div class="box-tools pull-right">
                <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
            </div>
        </div><!-- /.box-header -->
        
        <div class="box-body no-padding">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th width="30px;">No</th>
                        <th width="220px">Kost Name</th>
                        <th width="100px">Phone</th>
                        <th width="100px">Check status</th>
                        <th class="table-action-column">Action</th>
                    </tr>
                </thead>
                <tbody>

                    @if (is_null($data))
                      <tr><td colspan="4">Data tidak ditemukan</td></tr>
                    @else
                        @foreach ($data AS $key => $value)
                        <tr class="marktodelete{{ $value['_id'] }}">
                            <td>
                                {{ $key+1 }}
                            </td>
                            <td>
                                {{ $value['name'] }}                                 
                                <div id="hasil{{ $value['_id'] }}"></div>
                            </td>
                            <td>{{ $value['phone'] }}</td>
                            <td>
                                @if ($value['total_kost'] < 1)
                                    <label class="label label-primary"> </label>
                                @else
                                    <label class="label label-danger"> </label>
                                @endif    
                            </td>
                            <td>
                               
                                    <input type="buttom" id="marker_to_delete" idne="{{ $value['_id'] }}" user-io="{{ $primer->name }}" class="btn btn-xs btn-danger marker_to_delete{{ $value['_id'] }}"  value="Mark to delete" />
                                    <input type="button" id="show_" class="btn btn-xs btn-success" idne="{{ $value['_id'] }}" phone-number="{{ $value['phone'] }}" value="Detail">
                                <a href="" class="btn btn-xs btn-warning">Live now</a>
                            </td>
                        </tr>
                      @endforeach
                    @endif
                 
                </tbody>
                
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
    <!-- table -->

     <div class="box-body no-padding">
            <!-- $data->appends(Request::except('page'))->links() -->
     </div>

@stop
@section('script')

<script type="text/javascript">
      $(document).on('click', '#marker_to_delete', function(e) {
          /*theIdne = $("#idne").val();
          names = $("#user").val();*/
          theIdne = $(this).attr("idne");
          names   = $(this).attr("user-io");
         $.ajax({
            url: '/admin/patrick/marktodelete/' + theIdne+"?user="+names,
            type: 'GET',
            dataType: 'json',
            success: function(data) {
              $("#hasil"+theIdne).empty();  
              if(data.status == false) {
                
              } else {
                $(".marktodelete"+theIdne).hide();
              }
            }
          });
    });

    $(document).on('click', '#show_', function(e) {
      phone = $(this).attr("phone-number");
      idne = $(this).attr("idne");
      $("#hasil"+idne).append("Loading ................ ");
      $.ajax({
        url: '/admin/patrick/check-from-no-hp?phone=' + phone,
        type: 'GET',
        dataType: 'json',
        success: function(data) {
          $("#hasil"+idne).empty();  
          console.log(data.status);
          console.log(idne);
          if(data.status == false) {
            
            $("#hasil"+idne).append("<div class='alert alert-danger' style='width: 100%;'>Tidak ada kost yang mirip.</div>");
          } else {
            var project = "<strong>Hasil pencarian</strong>";
            console.log(data.data);
            $.each(data.data, function(index, value) {
                project += "<ul>";
                project += "<li>"+value._id+" - "+value.name+". <a href='/admin/room/"+value._id+"/edit' target='_blank'>view</a></li>";
                project += "</ul>";
            });

            $("#hasil"+idne).append(project);
          }
        }
      });
    });
</script>

@stop