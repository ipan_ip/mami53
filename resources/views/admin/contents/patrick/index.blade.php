@extends('admin.layouts.main')
@section('content')
    <!-- table ALTER TABLE dummy_data ADD data_from VARCHAR(10) AFTER is_live -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
            <div class="box-tools pull-right">
                <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
            </div>
        </div><!-- /.box-header -->
        
        <div class="row">
            <div class="col-md-12" style="margin-left: 10px; margin-bottom: 10px;">
                <a href="{{ URL::to('admin/patrick/google') }}" target="_blank" rel="noopener" class="btn btn-xs btn-success">Search in google</a>
            </div>
        </div>
        
        <div class="box-body no-padding">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th width="10px;">No</th>
                        <th width="220px">Name</th>
                        <th width="180px">Address</th>
                        <th width="15px">is Live</th>
                        <th width="15px">Is Checking</th>
                        <th style="width: 170px;" class="table-action-column">Action</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach ($data AS $key => $value)
                        <tr>
                            <td>{{ $key+1 }}</td>
                            <td>{{ $value->name }}</td>
                            <td>{{ substr($value->address, 0, 30) }}</td>
                            <td>
                                @if ($value->is_live == 1)
                                    <label class="label label-primary">live</label>
                                @else
                                    <label class="label label-danger">waiting</label>
                                @endif    
                            </td>
                            <td>@if (is_null($value->checking) AND $value->is_live == 1)
                                    <label class="label label-success">yes</label>
                                @elseif ($value->checking == 'yes')
                                    <label class="label label-success">yes</label>
                                @else
                                    <label class="label label-danger">no</label>
                                @endif            
                            </td>
                            <td>
                                <a href="{{ $value->slug }}" target="_blank" rel="noopener" class="btn btn-xs btn-primary">{{ getDomainFromSlug($value->slug) }}</a>
                                <a href="{{ URL::to('admin/patrick/check', $value->id) }}" class="btn btn-xs btn-danger">Check</a>
                                <a href="{{ URL::to('admin/patrick/edit', $value->id) }}" class="btn btn-xs btn-warning" target="_blank" rel="noopener">Edit</a>
                                @if ($value->designer_id > 0)
                                    <a href="{{ URL::to('admin/room', $value->designer_id) }}/edit?ref=patrick" target="_blank" rel="noopener" class="btn btn-xs btn-success">Duplicate</a>
                                @else
                                    <a href="#" disabled="true" class="btn btn-xs btn-default">Duplicate</a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                 
                </tbody>
                
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
    <!-- table -->

     <div class="box-body no-padding">
            {{ $data->appends(Request::except('page'))->links() }}
     </div>

@stop
@section('script')

@stop