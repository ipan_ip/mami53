@extends('admin.layouts.main')
@section('content')
    <!-- table ALTER TABLE dummy_data ADD data_from VARCHAR(10) AFTER is_live -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
            <div class="box-tools pull-right">
                <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
            </div>
        </div><!-- /.box-header -->

        <div class="row" style="margin-bottom: 10px;">
            <div class="col-md-6">
                <div class="col-md-5">
                    <input type="text" id="pages" placeholder="Page ..." class="form-control">
                </div>
                <div class="col-md-7">                
                    <input type="sumbit" id="goestopage" class="btn btn-success" value="To .. Page" />
                </div>
            </div>
        </div>
        
        <div class="box-body no-padding">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th width="30px;">No</th>
                        <th width="220px">Name</th>
                        <th width="15px">is Live</th>
                        <th width="15px">Is Checking</th>
                        <th class="table-action-column">Action</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach ($data AS $key => $value)
                        <tr>
                            <td>{{ $key+1 }}</td>
                            <td>{{ $value->name }}</td>
                            <td>
                                @if ($value->is_live == 1)
                                    <label class="label label-primary">live</label>
                                @else
                                    <label class="label label-danger">waiting</label>
                                @endif    
                            </td>
                            <td>@if (is_null($value->checking) AND $value->is_live == 1)
                                    <label class="label label-success">yes</label>
                                @elseif ($value->checking == 'yes')
                                    <label class="label label-success">yes</label>
                                @else
                                    <label class="label label-danger">no</label>
                                @endif            
                            </td>
                            <td>
                                <a href="{{ $value->slug }}" target="_blank" rel="noopener" class="btn btn-xs btn-primary">{{ getDomainFromSlug($value->slug) }}</a>
                                <a href="{{ URL::to('admin/patrick/apartemen/edit', $value->id) }}" target="_blank" rel="noopener" class="btn btn-xs btn-warning">Edit</a>
                            </td>
                        </tr>
                    @endforeach
                 
                </tbody>
                
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
    <!-- table -->

     <div class="box-body no-padding">
            {{ $data->appends(Request::except('page'))->links() }}
     </div>

@stop
@section('script')
<script type="text/javascript">
    $('#goestopage').click(function(e) {
        var pages = $('#pages').val();
        window.location = '/admin/patrick/apartemen/list?page='+pages;
    });
</script>
@stop