@extends('admin.layouts.main')

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

        <form action="{{ URL::to('admin/patrick/apartemen/search') }}" method="GET" id="formInsertDesigner" class="form-horizontal form-bordered" role="form" enctype="multipart/form-data">  
        {{ csrf_field() }}

        <div class="form-group bg-default">
            <label for="radius" class="col-sm-2 control-label">Nama apartemen</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="word" value="{{ $word }}">
            </div>
        </div>

        <div class="form-group bg-default">
            <label for="inputGeoName" class="col-sm-2 control-label">Masukkan nama daerah</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" @if (isset($request['geo_name'])) value="{{ $request['geo_name'] }}" @endif placeholder="Ketik daerah mana yang ingin kamu cari."
                       id="inputGeoName" name="geo_name">
            </div>
            <div class="col-sm-10 col-sm-offset-2">
                <div style="background: #EEE; width: 100%; height: 300px;">
                    <div id="map-canvas"></div>
                </div>
            </div>
        </div>
        
        <!-- <div class="box-body no-padding">
            <div class="form-group bg-default">
                <label for="inputLatitude" class="col-sm-2 control-label">Latitude</label>
                <div class="col-sm-10"> -->
                    <input type="hidden" class="form-control" placeholder="Latitude" id="inputLatitude" @if (isset($request['latitude'])) value="{{ $request['latitude'] }}" @endif name="latitude" required>
               <!--  </div>
            </div> -->

            <!-- <div class="form-group bg-default">
                <label for="inputLongitude" class="col-sm-2 control-label">Longitude</label>
                <div class="col-sm-10"> -->
                    <input type="hidden" class="form-control" placeholder="Longitude" @if (isset($request['longitude'])) value="{{ $request['longitude'] }}" @endif id="inputLongitude" name="longitude" required>
                <!-- </div>
            </div> -->


            <!-- <div class="form-group bg-default">
                <label for="radius" class="col-sm-2 control-label">Radius</label>
                <div class="col-sm-10"> -->
                    <input type="hidden" value="50000" class="form-control" id="radius" name="radius">
                <!-- </div> 
            </div>-->

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">Searching ....</button>
            </div>
        </div>
    </div>
    </form><!-- /.form -->
    @if (!is_null($data))

            <!-- table ALTER TABLE dummy_data ADD data_from VARCHAR(10) AFTER is_live -->
            <div class="box box-primary">
                <div class="col-md-12">
                    <div class="box-header">
                    <h3 class="box-title">Result From google</h3>
                </div>
                
                <div class="box-body no-padding">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th width="30px;">No</th>
                                <th width="250px">Name</th>
                                <th width="250px">Address</th>
                                <th class="table-action-column">Action</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($data['results'] AS $key => $value)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $value['name'] }}</td>
                                    <td>{{ $value['vicinity'] }}</td>
                                    <td>
                                        <a href="{{ URL::to('admin/patrick/apartemen/project/add') }}?name={{ $value['name'] }}&latitude={{ $value['geometry']['location']['lat'] }}&longitude={{ $value['geometry']['location']['lng'] }}&address={{ $value['vicinity'] }}" target="_blank" rel="noopener" class="btn btn-xs btn-primary">Edit</a>
                                    </td>
                                </tr>
                            @endforeach
                         
                        </tbody>
                        
                    </table>
                </div><!-- /.box-body -->
                </div>    
            </div><!-- /.box -->
            <!-- table -->

            @if (!is_null($next))

                <div class="col-md-12" style="margin-top: 20px;">
                    <a class="btn btn-primary" href="{{ URL::to('admin/patrick/fromgoogle') }}?pagetoken={{ $next }}&latitude={{ $request['latitude'] }}&longitude={{ $request['longitude'] }}&radius={{ $request['radius'] }}">Next Page</a>
                </div>    
            @endif

    @endif

    </div><!-- /.box -->

@stop

@section('script')
    <script src="https://maps.googleapis.com/maps/api/js?libraries=places&language=id&key={{ENV('MIX_GOOGLE_MAPS_JAVASCRIPT_API_KEY')}}"></script>

    <script type="text/javascript">
        centerPos = {
            lat: {{ isset($request['latitude']) ? $request['latitude'] : '-7.7858485' }}, 
            lng: {{ isset($request['longitude']) ? $request['longitude'] :  '110.3680087'}}
        };

        var map = new google.maps.Map(document.getElementById("map-canvas"),
            {
                center: centerPos,
                zoom:12
            }
        );

        var marker = new google.maps.Marker({
            position: centerPos,
            map: map,
            draggable: true,

        });

        var autocomplete = new google.maps.places.Autocomplete(document.getElementById('inputGeoName'));
        autocomplete.bindTo('bounds', map);

        var geocoder = new google.maps.Geocoder();

        google.maps.event.addListener(marker, 'dragend', function(evt) {
            var lat = evt.latLng.lat();
            var lng = evt.latLng.lng();

            var latlng = new google.maps.LatLng(lat, lng);

            geocoder.geocode({'latLng': latlng}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[1]) {

                        map.setCenter(latlng);
                        marker.setPosition(latlng);

                        $('#inputGeoName').val(results[1].formatted_address);
                        $('#inputLatitude').val(lat);
                        $('#inputLongitude').val(lng);

                        setDistrictAndCity(results[0]);
                    } else {
                        alert('No results found');
                    }
                } else {
                    alert('Geocoder failed due to: ' + status);
                }
            });
        });

                autocomplete.addListener('place_changed', function() {
            marker.setVisible(false);
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                // User entered the name of a Place that was not suggested and
                // pressed the Enter key, or the Place Details request failed.
                window.alert("No details available for input: '" + place.name + "'");
                return;
            }

            map.setCenter(place.geometry.location);
            marker.setPosition(place.geometry.location);
            marker.setVisible(true);

            $('#inputLatitude').val(place.geometry.location.lat);
            $('#inputLongitude').val(place.geometry.location.lng);

            setDistrictAndCity(place);
        });

        function setDistrictAndCity(response) {
            subDistrict = '';
            city = '';
            response.address_components.forEach(function(component) {
                // console.log(component.types);
                if ($.inArray("administrative_area_level_3", component.types) != -1) {
                    subDistrict = component.short_name;
                }

                if ($.inArray("administrative_area_level_2", component.types) != -1) {
                    city = component.short_name;      
                }

            });

            $('#inputSubdistrict').val(subDistrict);
            $('#inputAreaCity').val(city);
        }

</script>

@stop