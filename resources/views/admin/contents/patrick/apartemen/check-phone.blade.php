@extends('admin.layouts.main')

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>


        <form action="" method="GET" id="formInsertDesigner" class="form-horizontal form-bordered" >  
        {{ csrf_field() }}
        
        <div class="box-body no-padding">
            <div class="form-group bg-default">
                <label for="inputName" class="col-sm-2 control-label">Input phone number</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="phone" value="{{ $phone }}" >
                </div>
            </div>

            <div class="form-group bg-default">
                <div class="col-sm-12"><input type="submit" name="submit" class="col-sm-12 btn btn-primary" value="Check" /></div>
            </div>
        </div>
    </form><!-- /.form -->

    @if (!is_null($data)) 
        <div class="box-body no-padding">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th width="30px;">No</th>
                        <th width="220px">Name</th>
                        <th width="15px">is Live</th>
                        <th width="15px">Is Checking</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach ($data AS $key => $value)
                        <tr>
                            <td>{{ $key+1 }}</td>
                            <td>{{ $value->name }}</td>
                            <td>
                                @if ($value->is_live == 1)
                                    <label class="label label-primary">live</label>
                                @else
                                    <label class="label label-danger">waiting</label>
                                @endif    
                            </td>
                            <td>@if (is_null($value->checking) AND $value->is_live == 1)
                                    <label class="label label-success">yes</label>
                                @elseif ($value->checking == 'yes')
                                    <label class="label label-success">yes</label>
                                @else
                                    <label class="label label-danger">no</label>
                                @endif            
                            </td>
                            
                        </tr>
                    @endforeach
                 
                </tbody>
                
            </table>
        </div><!-- /.box-body -->

    @endif
    </div><!-- /.box -->
@stop