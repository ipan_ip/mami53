@extends('admin.layouts.main')

@section('style')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/dropzone.css">
    {{ HTML::style('assets/vendor/jquery-chosen/chosen.css') }}
    
    <style>
        .chosen-choices {
            border: 1px solid #ccc;
            border-radius: 4px;
            min-height: 34px;
            padding: 6px 12px;
        }
    </style>
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

        <form action="{{ URL::to('admin/patrick/apartemen/live') }}" method="POST" class="form-horizontal form-bordered">
            
            <div class="box-body no-padding">

                <div class="row">
                    <div class="col-md-6">
                <div class="form-group bg-default">
                    <label for="project_id" class="control-label col-sm-3">Aprt Project Id</label>
                    <input type="hidden" name="id_dummy" value="{{ $data->id }}">
                    <div class="col-sm-7">
                        <input type="text" name="project_id" id="inputName" value="{{ old('project_id') }}" class="form-control">
                    </div>
                    <div class="col-sm-1">
                        <input type="button" name="search" class="btn btn-success" value="Search" id="inputNameCheck">
                    </div>
                </div>

                 <div style="margin-left: 10px; margin-top: 30px; width: 100%;">
                    <div id="hasil"></div>
                </div>    

                <div class="form-group bg-default">
                    <label for="unit_name" class="control-label col-sm-2">Nama</label>
                    <div class="col-sm-10">
                        <input type="text" name="unit_name" id="unit_name" value="{{ $data->name }}" class="form-control">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="unit_type" class="control-label col-sm-2">Tipe Unit</label>
                    <div class="col-sm-10">
                        <select name="unit_type" class="form-control" id="unit_type">
                            @foreach($typeOptions as $type)
                                <option value="{{ $type }}">{{ $type }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="unit_type_name" class="control-label col-sm-2">Tipe Unit (custom)</label>
                    <div class="col-sm-10">
                        <input type="text" name="unit_type_name" id="unit_type_name" value="{{ old('unit_type_name') }}" class="form-control">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="unit_number" class="control-label col-sm-2">Nomor Unit</label>
                    <div class="col-sm-10">
                        <input type="text" name="unit_number" id="unit_number" value="{{ old('unit_number') }}" class="form-control">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="floor" class="control-label col-sm-2">Lantai</label>
                    <div class="col-sm-10">
                        <input type="text" name="floor" id="floor" value="{{ $data->floor }}" class="form-control">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="unit_size" class="control-label col-sm-2">Ukuran Unit</label>
                    <div class="col-sm-10">
                        <input type="text" name="unit_size" id="unit_size" value="{{ $data->size }}" class="form-control">
                        <p class="help-block">Dalam m<sup>2</sup></p>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="min_payment" class="control-label col-sm-2">Minimal Pembayaran</label>
                    <div class="col-sm-10">
                        <select name="min_payment" class="form-control">
                            <option value="" >- Pilih Minimal Pembayaran -</option>
                            @foreach($minPaymentOptions as $key => $value)
                                <option value="{{ $key }}">{{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                    </div>
                    <div class="col-md-6" style="padding: 0px;"><div style="margin: 0px 10px;">{!! $data->description !!}</div></div>
                </div>

                <div class="form-group bg-default">
                    <label for="price_type" class="control-label col-sm-2">Pilih tipe harga</label>
                    <div class="col-sm-10">
                        <select name="price_type" class="form-control" id="price_type">
                            <option value="idr">Rupiah / Idr</option>
                            <option value="usd">Dollar / Usd</option>
                        </select>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="price_monthly" class="control-label col-sm-2">Harga Bulanan</label>
                    <div class="col-sm-10">
                        <input type="text" name="price_monthly" id="price_monthly" value="{{ $data->price }}" class="form-control">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="maintenance_price" class="control-label col-sm-2">Biaya Maintenance / Bulan</label>
                    <div class="col-sm-10">
                        <input type="number" name="maintenance_price" id="maintenance_price" value="{{ old('maintenance_price') }}" class="form-control">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="parking_price" class="control-label col-sm-2">Biaya Parkir</label>
                    <div class="col-sm-10">
                        <input type="number" name="parking_price" id="parking_price" value="{{ old('parking_price') }}" class="form-control">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="facilities" class="control-label col-sm-2">Fasilitas</label>
                    <div class="col-sm-10">
                        <select class="form-control chosen-select" multiple id="facilities" name="facilities[]">
                            @foreach ($facilityOptions as $key => $value)
                                <option value="{{ $key }}" {{ !is_null(old('facilities')) && in_array($key, old('facilities')) ? 'selected="selected"' : '' }}>{{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="owner_name" class="control-label col-sm-2">Nama Pemilik</label>
                    <div class="col-sm-10">
                        <input type="text" name="owner_name" id="owner_name" value="{{ old('owner_name') }}" class="form-control">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="owner_phone" class="control-label col-sm-2">Nomor Telepon Pemilik</label>
                    <div class="col-sm-8">
                        <input type="text" name="owner_phone" id="owner_phone" value="{{ $data->phone_number }}" class="form-control">
                    </div>
                    <div class="col-md-2"><input type="button" class="btn btn-danger" id="checkPhoneNumber" value="Check Phone Number" /></div>
                </div>

                <div class="form-group bg-default">
                    <label for="input_as" class="control-label col-sm-2">Input Sebagai</label>
                    <div class="col-sm-10">
                        <select name="input_as" class="form-control">
                            @foreach($inputAsOptions as $value)
                                <option value="{{ $value }}">{{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="input_name" class="control-label col-sm-2">Nama Penginput</label>
                    <div class="col-sm-10">
                        <input type="text" name="input_name" id="input_name" value="{{ old('input_name') }}" class="form-control">
                    </div>
                </div>

        <div class="form-group bg-info divider">
            <div class="col-sm-12">
                Global Setting
            </div>
        </div>

        <div class="form-group bg-default" for="inputAgentStatus">
            <label class="col-sm-2 control-label">Description</label>
            <div class="col-sm-10">
                <textarea name="description" class="form-control" placeholder="Description"></textarea>
            </div>
        </div>

        <div class="form-group bg-default" for="inputAgentStatus">
            <label class="col-sm-2 control-label">Images</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="inputImages" name="images" value="{{ $data->image }}" />
                <p style="margin-top: 10px;">Format image: <strong style="color: #ff0000">(http/https)://........(jpg/png/jpeg/bmp)</strong></p>
            </div>
        </div>

        <div class="form-group bg-default">
            <label for="inputIndexed" class="col-sm-2 control-label">Download images</label>
            <div class="col-sm-10">
                <div class="checkbox">
                        <input type="checkbox" name="download">
                        download now
                </div>
            </div>
        </div>

        <div class="form-group bg-default">
            <label for="inputIndexed" class="col-sm-2 control-label">Live Now</label>
            <div class="col-sm-10">
                <div class="checkbox">
                        <input type="checkbox" name="live">
                        Yes
                </div>
            </div>
        </div>

        <div class="form-group bg-default">
            <label for="inputIndexed" class="col-sm-2 control-label">Furnished</label>
            <div class="col-sm-10">
                <div class="checkbox">
                        <input type="checkbox" name="furnished">
                        Yes
                </div>
            </div>
        </div>

        <div class="form-group bg-info divider" style="background: #ff6060;">
            <div class="col-sm-12" style="color: #ffffff;">
                May be important
            </div>
        </div>

        <div class="form-group bg-default">
            <label for="inputIndexed" class="col-sm-2 control-label">Duplicate</label>
            <div class="col-sm-10">
                <div class="checkbox">
                        <input type="checkbox" name="is_duplicate">
                        Yes
                </div>
                <p style="margin-top: 10px;">*Jika yes maka setelah klik save akan langsung ke halaman ke 2 (halaman duplicate).</p>
            </div>
        </div>

                <div class="form-group bg-default">
                    <div class="col-sm-10 col-sm-push-2">
                        <button type="submit" class="btn btn-primary" name="save_live">Save to Live</button>
                        <button type="submit" name="save_temp" class="btn btn-success" value="save_temp">Save Temp</button>
                    </div>
                </div>
            </div>
        </form>

        
    </div>

@endsection

@section('script')
{{ HTML::script('assets/vendor/jquery-chosen/chosen.jquery.js') }}
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/dropzone.js"></script>

<script>
    var config = {
        '.chosen-select'           : {width: '100%'}
    };
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }

    $(function() {
        $('#unit_type').on('change', function() {
            if($(this).val() == 'Lainnya') {
                $('#unit_type_name').removeAttr('disabled');
            } else {
                $('#unit_type_name').attr('disabled', 'disabled');
            }
        });

        $('#unit_type').trigger('change');

        Dropzone.autoDiscover = false;

        Dropzone.options.coverphoto = {
            paramName : "media",
            maxFilesize : 6,
            maxFiles: 1,
            accepFiles: '.jpg, .jpeg, .png, .bmp',
            dictDefaultMessage : 'Upload photo for cover',
            addRemoveLinks: true,
            success: function(file, response) {
                if(response.media.id != 0) {
                    $('#photo-cover-wrapper').append('<input type="hidden" name="photo_cover" value="' +response.media.id+ '">');
                }
            },
            init: function() {
                this.on('removedfile', function(file) {
                    $('#photo-cover-wrapper').html('');
                });
            }
        };

        Dropzone.options.bedroomphoto = {
            paramName : "media",
            maxFilesize : 6,
            accepFiles: '.jpg, .jpeg, .png, .bmp',
            dictDefaultMessage : 'Upload bedroom photos',
            addRemoveLinks: true,
            success : function (file, response) {
                if(response.media.id != 0){
                    $('#photo-bedroom-wrapper').append('<input type="hidden" name="photo_bedroom[]" value="' +response.media.id+ '" data-filename="' + file.name + '">');
                }
            },
            init: function() {
                this.on('removedfile', function(file) {
                    $('input[name="photo_bedroom[]"]').each(function(id, el) {
                        if($(el).data('filename') == file.name) {
                            $(el).remove();
                        }
                    });
                });
            }
        };

        $('#bedroomphoto').dropzone();

        Dropzone.options.bathroomphoto = {
            paramName : "media",
            maxFilesize : 6,
            accepFiles: '.jpg, .jpeg, .png, .bmp',
            dictDefaultMessage : 'Upload bedroom photos',
            addRemoveLinks: true,
            success : function (file, response) {
                if(response.media.id != 0){
                    $('#photo-bathroom-wrapper').append('<input type="hidden" name="photo_bathroom[]" value="' +response.media.id+ '" data-filename="' + file.name + '">');
                }
            },
            init: function() {
                this.on('removedfile', function(file) {
                    $('input[name="photo_bathroom[]"]').each(function(id, el) {
                        if($(el).data('filename') == file.name) {
                            $(el).remove();
                        }
                    });
                });
            }
        };

        $('#bathroomphoto').dropzone();

        $('#coverphoto').dropzone();


    });

    $('#inputNameCheck').click(function(e) {
      theName = $("#inputName").val();
      $("#hasil").append("Loading ................ ");
      $.ajax({
        url: '/admin/patrick/apartemen/check-name?name=' + theName,
        type: 'GET',
        dataType: 'json',
        success: function(data) {
          $("#hasil").empty();  
          if(data.status == false) {
            
            $("#hasil").append("Apartemen project tidak tersedia. Tambah Apartemen Project <a href='/admin/apartment/project/create' target='_blank'>Tambah</a><br/>Cari project lewat google ? Cari <strong><a href='/admin/patrick/apartemen/google?word=apartemen "+theName+"' target='_blank'>apartemen "+theName+"</a></strong>");
          } else {
            var project = "<strong>Hasil pencarian</strong>";
            project += "<ul>";
            $.each(data.data, function(index, value) {

                if (value.is_active == 1) {
                    project += "<li>"+value.id+" - "+value.name+". <a href='/admin/apartment/project?q="+value.name+"' target='_blank'>view</a> - <label style='cursor: pointer; text-decoration: underline;' id='choosein' str-id="+value.id+">pilih ini</label></li>";
                } else {
                    project += "<li style='color: #ff0000;'>"+value.id+" - "+value.name+". <a href='/admin/apartment/project?q="+value.name+"' target='_blank'>view</a> - <label style='cursor: pointer; text-decoration: underline;' id='choosein' str-id="+value.id+">pilih ini</label></li>";
                }

            });
            project += "Tidak ada dalam pilihan ? Tambah project baru <strong><a href='/admin/apartment/project/create' target='_blank'>Tambah</a></strong><br/>";
            project += "Cari project lewat google ? Cari <strong><a href='/admin/patrick/apartemen/google?word=apartemen "+theName+"' target='_blank'>apartemen "+theName+"</a></strong>";
            project += "</ul>";

            $("#hasil").append(project);
          }
        }
      });
    });

    $(document).on('click', '#choosein', function(e) {
      var id = $(this).attr("str-id");
      document.getElementById('inputName').value = id;
    });

    $('#changedummyimages').click(function(e) {
        document.getElementById('inputImages').value = 'http://songturu.mamikos.com/uploads/file/noimages.png';
    });

    $('#checkPhoneNumber').click(function(e) {
        var phone = $('#owner_phone').val();
        window.open('/admin/patrick/apartemen/check-phone?phone='+phone, '_blank');
    });

</script>
@endsection