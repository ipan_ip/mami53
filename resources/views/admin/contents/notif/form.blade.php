@extends('admin.layouts.main')
@section('content')
<div class="box box-primary">
  <div class="box-header">
      <h3 class="box-title">{{ $boxTitle }}</h3>
  </div><!-- /.box-header -->
  {{ Form::open(array('route' => $formAction, 'method' => $formMethod,
      'class' => 'form-horizontal form-bordered', 'role' => 'form', 'id' => 'formInsertArea')) }}
      <div class="box-body no-padding">
            <!-- select -->
            <div class="form-group bg-default">
                <label for="inputSelectTarget" class="col-sm-2 control-label">Select Target</label>
                <div class="col-sm-10">
                    <select class="form-control chosen-select" id="inputSelectTarget" name="target" tabindex="2" data-placeholder="Select a Target">
                          <option>Select a Target</option>
                        @foreach ($rowsTarget as $rowTarget)
                          <option value="{{ $rowTarget->id }}"
                            {{ $rowTarget->selected }}>{{ $rowTarget->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div id="formSelectTargetChild"></div>
            <!-- select -->
            <div class="form-group bg-default">
                <label for="inputSelectUserCategory" class="col-sm-2 control-label">Select User Category</label>
                <div class="col-sm-10">
                    <select class="form-control chosen-select" id="inputSelectUserCategory" name="user_category" tabindex="2" data-placeholder="Select a User Category">
                          <option>Select a User Category</option>
                        @foreach ($rowsUserCategory as $rowUserCategory)
                          <option value="{{ $rowUserCategory->id }}"
                            {{ $rowUserCategory->selected }}>{{ $rowUserCategory->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div id="formSelectUserCategoryChild"></div>
            <div class="form-group bg-default">
              <label for="inputTitle" class="col-sm-2 control-label">Title</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" placeholder="Title"
                  id="inputTitle" name="title" value="{{ $rowNotif->title }}">
              </div>
            </div>
            <div class="form-group bg-default">
              <label for="inputTitle" class="col-sm-2 control-label">Message</label>
              <div class="col-sm-10">
                <textarea class="form-control" placeholder="Message"
                  id="inputMessage" name="message" value="{{ $rowNotif->message }}"></textarea>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="{{ URL::route('admin.notif.index') }}">
                  <button type="button" class="btn btn-default">Cancel</button>
                </a>
                <button type="reset" class="btn btn-default"
                  onclick="$('#formInsertArea').bootstrapValidator('resetForm', true);">Reset</button>
              </div>
            </div>
      </div>
  {{ Form::close() }}<!-- /.form -->
</div><!-- /.box -->
@stop
@section('style')
{{ HTML::style('assets/vendor/jquery-chosen/chosen.css') }}
{{ HTML::style('assets/vendor/jquery-chosen-custom/chosen.css') }}
@stop
@section('script')
{{ HTML::script('assets/vendor/jquery-chosen/chosen.jquery.js') }}
<script type="text/javascript">
  function updateSelectChosen () {
    // Update Select Chosen
    var config = {
      '.chosen-select'           : {width: '100%'}
    }
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    }
  }
  $(function()
  {
    updateSelectChosen();
  });
</script>
{{ HTML::script('assets/vendor/mustache/mustache.js') }}
<script id="select-chosen-tpl" type="x-tmpl-mustache">
<div id="@{{form_id}}" class="form-group bg-default">
    <label for="@{{input_id}}" class="col-sm-2 control-label">@{{label}}</label>
    <div class="col-sm-10">
        <select class="form-control chosen-select" id="@{{input_id}}" name="@{{name}}" tabindex="2" data-placeholder="Select a Designer">
              <option>@{{placeholder}}</option>
            @{{#row}}
              <option value="@{{ id }}"
                @{{ selected }}>@{{ name }}</option>
            @{{/row}}
        </select>
    </div>
</div>
</script>
<!-- page script -->
<script type="text/javascript">

    @if (Session::get('laravelValidatorJSON') === null)
      myGlobal.laravelValidator = null;
    @else
      myGlobal.laravelValidator = {{ Session::get('laravelValidatorJSON') }};
    @endif

    myGlobal.url = '{{$url}}';

    function parseTemplate (tplId, wrapperTarget, arrReplace) {
      var deferred = new $.Deferred();

      template = $(tplId).html();
      Mustache.render(template);

      renderedContent = Mustache.render(template, arrReplace);

      $(wrapperTarget).html(renderedContent);

      deferred.resolve(true);

      return deferred.promise();
    }

    $(function()
    {
      $inputSelectTarget = $('#inputSelectTarget');
      $inputSelectTarget.change(function ()
      {
        $('#formSelectTargetChild').html('');

        if ($inputSelectTarget.val() === 'designer_detail')
        {


          $.ajax({
            type: "GET",
            url: myGlobal.url + "/admin/api/v1/designer",
          })
            .done(function( response ) {
              var arrReplace = {
                form_id : 'formSelectDesigner',
                input_id : 'inputSelectDesigner',
                label : 'Select Designer',
                placeholder : 'Select a Designer',
                name : 'target_id',
                row : response.designer
              };
              parseTemplate('#select-chosen-tpl', '#formSelectTargetChild', arrReplace)
                .done(function () {
                  updateSelectChosen();
                });
            });
        }
        else if ($inputSelectTarget.val() === 'style_detail')
        {
          $.ajax({
            type: "GET",
            url: myGlobal.url + "/admin/api/v1/style",
          })
            .done(function( response ) {
              var arrReplace = {
                form_id : 'formSelectStyle',
                input_id : 'inputSelectStyle',
                label : 'Select Style',
                placeholder : 'Select a Style',
                name : 'target_id',
                row : response.style
              };
              parseTemplate('#select-chosen-tpl', '#formSelectTargetChild', arrReplace)
                .done(function () {
                  updateSelectChosen();
                });
            });
        }
        else if ($inputSelectTarget.val() === 'event_detail')
        {
          $.ajax({
            type: "GET",
            url: myGlobal.url + "/admin/api/v1/event",
          })
            .done(function( response ) {
              var arrReplace = {
                form_id : 'formSelectEvent',
                input_id : 'inputSelectEvent',
                label : 'Select Event',
                placeholder : 'Select a Event',
                name : 'target_id',
                row : response.event
              };
              parseTemplate('#select-chosen-tpl', '#formSelectTargetChild', arrReplace)
                .done(function () {
                  updateSelectChosen();
                });
            });
        }
      });

      $inputSelectUserCategory = $('#inputSelectUserCategory');
      $inputSelectUserCategory.change(function ()
      {
        if ($inputSelectUserCategory.val() === 'designer_follower')
        {
          $('#formSelectUserCategoryChild').html('');

          $.ajax({
            type: "GET",
            url: myGlobal.url + "/admin/api/v1/designer",
          })
            .done(function( response ) {
              var arrReplace = {
                form_id : 'formSelectDesigner',
                input_id : 'inputSelectDesigner',
                label : 'Select Designer',
                placeholder : 'Select a Designer',
                name : 'user_category_id',
                row : response.designer
              };
              parseTemplate('#select-chosen-tpl', '#formSelectUserCategoryChild', arrReplace)
                .done(function () {
                  updateSelectChosen();
                });
            });
        }
        else if ($inputSelectUserCategory.val() === 'user')
        {
          $('#formSelectUserCategoryChild').html('');

          $.ajax({
            type: "GET",
            url: myGlobal.url + "/admin/api/v1/user",
          })
            .done(function( response ) {
              var arrReplace = {
                form_id : 'formSelectUser',
                input_id : 'inputSelectUser',
                label : 'Select User',
                placeholder : 'Select a User',
                name : 'user_category_id',
                row : response.user
              };
              parseTemplate('#select-chosen-tpl', '#formSelectUserCategoryChild', arrReplace)
                .done(function () {
                  updateSelectChosen();
                });
            });
        }
        else
        {
          $('#formSelectUserCategoryChild').html('');
        }
      });

      $formInsertArea = $('#formInsertArea');

      formStoreTagRules = {
          name: {
              message: 'The name is not valid',
              validators: {
                  notEmpty: {
                      message: 'The name is required and cannot be empty'
                  },
                  stringLength: {
                      min: 3,
                      max: 30,
                      message: 'The name must be more than 3 and less than 30 characters long'
                  }
              }
          },
          region_id: {
              validators: {
                  notEmpty: {
                      message: 'The type is required and cannot be empty'
                  },
              }
          }
      };

      formUpdateTagRules = {
          name: {
              message: 'The name is not valid',
              validators: {
                  notEmpty: {
                      message: 'The name is required and cannot be empty'
                  },
                  stringLength: {
                      min: 3,
                      max: 30,
                      message: 'The name must be more than 3 and less than 30 characters long'
                  }
              }
          },
          region_id: {
              validators: {
                  notEmpty: {
                      message: 'The type is required and cannot be empty'
                  },
              }
          }
      };

      @if (Route::currentRouteName() === 'admin.area.create')
        myGlobal.bootstrapValidatorDefaults.fields = formStoreTagRules;
      @elseif (Route::currentRouteName() === 'admin.area.edit')
        myGlobal.bootstrapValidatorDefaults.fields = formUpdateTagRules;
      @endif


      $formInsertArea.bootstrapValidator(myGlobal.bootstrapValidatorDefaults);

      updateValidateStatus($formInsertArea.data('bootstrapValidator'), myGlobal.laravelValidator);
    });
</script>
@stop
