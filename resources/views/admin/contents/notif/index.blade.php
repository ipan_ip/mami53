@extends('admin.layouts.main')
@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
            <div class="box-tools pull-right">
                <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <a href="{{ URL::route($createAction) }}">
                        <button type="submit" class="btn-add btn btn-primary btn-sm">
                            <i class="fa fa-plus">&nbsp;</i>Add Notif</button>
                    </a>
                </div>
            </div>

            <table id="tableListNotif" class="table table-striped">
                <thead>
                    <tr>
                        <th width="30px;">No</th>
                        <th>Title</th>
                        <th width="30%">Message</th>
                        <th>Scheme</th>
                    @if(Request::is('admin/*'))
                        <th>Author</th>
                        <th>Created At</th>
                    @endif
                        <th width="7%">Action</th>
                        <th width="5%">Send</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($rowsNotif as $rowNotifKey => $rowNotif)
                    <tr>
                        <td>{{ $rowNotifKey+1 }}</td>
                        <td>{{ $rowNotif->title}}</td>
                        <td>{{ $rowNotif->message}}</td>
                        <td>{{ Notif::getSchemeList()[$rowNotif->scheme] }}</td>
                    @if(Request::is('admin/*'))
                        <td>{{ $rowNotif->author }}</td>
                        <td>{{ date('d/m/Y', strtotime($rowNotif->date)) }}</td>
                    @endif
                        <td class="table-action-column">
                            <div class="btn-action-group">
                                <a href="{{ URL::route($editAction, array($rowNotif->id)) }}">
                                    <i class="fa fa-pencil"></i></a>
                                <!-- <a href="@{{ url('admin/notif/duplicate/' . $rowNotif->id ) }}">
                                    <i class="fa fa-copy" title="copy"></i></a> -->
                                <form action="{{ route('admin.notif.destroy', $rowNotif->id) }}" method="POST" onsubmit="return confirm('Do you really want to delete this?');">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button class="btn btn-xs btn-danger">delete</button>
                                </form>
                            </div>
                        </td>
                        <td>
                            <a href="{{ URL::route($sendAction, array($rowNotif->id)) }}">
                                    <button class='btn btn-sm btn-info'>Send</button>
                                </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                     <tr>
                        <th width="30px;">No</th>
                        <th>Title</th>
                        <th>Message</th>
                        <th>Scheme</th>
                    @if(Request::is('admin/*'))
                        <th>Author</th>
                        <th>Created At</th>
                    @endif
                        <th>Action</th>
                        <th>Send</th>
                    </tr>
                </tfoot>
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
    <!-- table -->

    @include('admin.partials.delete-modal-new')
@stop
@section('script')
<!-- page script -->
<script type="text/javascript">
    $(function()
    {
        $("#tableListNotif").dataTable();
    });
</script>
@stop
