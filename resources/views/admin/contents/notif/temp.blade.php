@extends('admin.layouts.main')
@section('content')
<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">{{ $boxTitle }}</h3>
    </div><!-- /.box-header -->
    {{ Form::open(array('url' => $formAction, 'method' => $formMethod,
        'class' => 'form-horizontal form-bordered', 'role' => 'form', 'id' => 'formInsertNotif')) }}
        <div class="box-body no-padding">
            <!-- select -->
            <div><input type="hidden" value="{{ $rowNotif->id }}" name="notif_id" /></div>

            <div class="form-group bg-default">
                <label for="inputSelectUserCategory" class="col-sm-2 control-label">Select User Category</label>
                <div class="col-sm-10">
                    <select class="form-control chosen-select" id="inputSelectUserCategory" name="user_category" tabindex="2" data-placeholder="Select a User Category">
                        @foreach ($rowsUserCategory as $rowUserCategory)
                          <option value="{{ $rowUserCategory->id }}"
                            {{ $rowUserCategory->selected }}>{{ $rowUserCategory->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group bg-default" id="formInputUser" style="display:{{ $showUserList?'block':'none' }}">
                <label for="inputUser" class="col-sm-2 control-label"> User List</label>
                <div class="col-sm-10">
                    <select class="form-control chosen-select" multiple id="inputUser" name="user_ids[]" tabindex="2">
                        @foreach ($rowsUserList as $rowUser)
                          <option value="{{ $rowUser->id }}"
                            {{ $rowUser->selected }}>{{ $rowUser->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group bg-info divider">
                <div class="col-sm-12">
                    Notification Scheme
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputSelectTarget" class="col-sm-2 control-label">Notif Destination</label>
                <div class="col-sm-10">
                    <select class="form-control" id="target" name="scheme" tabindex="2" data-placeholder="Select Notif Destination">
                          <option>Select a Target</option>
                          @foreach($rowsScheme as $scheme)
                            <option value="{{ $scheme->id}}" {{ $rowNotif->scheme == $scheme->id ? 'selected' : ''}}> {{ $scheme->name }}</option>
                          @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group bg-default" id="formInputUrl" style="display:{{ $rowNotif->scheme == 'url' ? 'block' : 'none' }}">
                <label for="inputInputURL" class="col-sm-2 control-label">URL</label>
                <div class="col-sm-10">
                    <input class="form-control" name="url" placeholder="Filter" value="{{ $rowNotif->scheme_param }}">
                </div>
            </div>
            <div class="form-group bg-default" id="formInputSelectDesigner" style="display:{{ $rowNotif->scheme == 'room/{id}' ? 'block' : 'none' }}">
                <label for="inputSelectDesigner" class="col-sm-2 control-label">Room ID</label>
                <div class="col-sm-10">
                    <input class="form-control" name="designer_id" placeholder="Filter" value="{{ $rowNotif->scheme_param }}">
                </div>
            </div>
            <div class="form-group bg-default" id="formInputSchemeParameter" style="display:{{ $schemeType == 'search_kost?params={params}' || $schemeType == 'list_kost?params={params}'  ? 'block' : 'none' }}">
                <label for="inputSelectDesigner" class="col-sm-2 control-label">Filter</label>
                <div class="col-sm-10">
                    <input class="form-control" name="scheme_param_filter" placeholder="Filter" value="{{ $rowNotif->scheme_param }}">
                </div>
            </div>

            <div class="form-group bg-info divider">
                <div class="col-sm-12">
                    Notification Content
                </div>
            </div>
            <div class="form-group bg-default">
                <label for="inputSelectTarget" class="col-sm-2 control-label">Title</label>
                <div class="col-sm-10">
                    <input class="form-control" name="title" placeholder="Title" value="{{ $rowNotif->title }}">
                </div>
            </div>
            <div class="form-group bg-default">
                <label for="inputSelectTarget" class="col-sm-2 control-label">Message</label>
                <div class="col-sm-10">
                    <input class="form-control" name="message" placeholder="Message" value="{{ $rowNotif->message }}">
                </div>
            </div>
            <div class="form-group bg-default">
                <label for="inputSelectTarget" class="col-sm-2 control-label">Image</label>
                <div class="col-sm-10">
                    <!--  Upload Style Photo -->
                  <div id="wrapper-file-upload" class="media">
                    <div class="pull-right">
                      <img src="{{ $rowNotif->photo }}" class="media-object" height="100px">
                    </div>
                    <div class="media-body">
                      <!-- The fileinput-button span is used to style the file input field as button -->
                      <span class="btn btn-primary btn-sm fileinput-button">
                          <i class="glyphicon glyphicon-plus"></i>
                          <span>&nbsp;Add Photo</span>
                          <!-- The file input field used as target for the file upload widget -->
                          <input id="fileupload" type="file" name="media">
                          <input id="photo_id" type="hidden" name="photo_id"
                            value="{{ $rowNotif->photo_id }}">
                      </span>
                      <div class="row" id="form-input-url" style="display:none">
                            <div class="col-lg-10">
                              <input id="input-media-url" class="form-control" type="text" name="input-media-url" placeholder="URL Photo">
                            </div>
                            <div class="col-lg-2">
                              <button class="btn btn-info btn-block" onclick="upload_by_url();return false;">Upload</button>
                            </div>
                      </div>
                      <br>
                      <br>
                      <!-- The global progress bar -->
                      <div id="progress" class="progress">
                          <div class="progress-bar"></div>
                      </div>
                      <div class="col-sm-6">
                        <div id="original-file-info" class="row"></div>
                      </div>
                      <div class="col-sm-6">
                        <div id="uploaded-file-info" class="row">
                          <b>URL: </b> {{ $rowNotif->photo }} <br>
                        </div>
                      </div>
                      <br>
                      <!-- The container for the uploaded files -->
                      <div id="files" class="files"></div>
                    </div>
                  </div>
                  <img class="media-object">
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">Save</button>
          </div>
        </div>
  {{ Form::close() }}<!-- /.form -->
</div><!-- /.box -->
@stop

@section('style')
{{ HTML::style('assets/vendor/jquery-chosen/chosen.css') }}
{{ HTML::style('assets/vendor/jquery-chosen-custom/chosen.css') }}
{{ HTML::style('assets/vendor/jquery-file-upload/css/jquery.fileupload.css') }}
{{ HTML::style('assets/vendor/jcrop/css/jquery.Jcrop.min.css') }}
@stop

@section('script')
{{ HTML::script('assets/vendor/jquery-chosen/chosen.jquery.js') }}
{{ HTML::script('assets/vendor/jcrop/js/jquery.Jcrop.min.js') }}
<script type="text/javascript">
  $(function()
  {
    var config = {
      '.chosen-select'           : {width: '100%'}
    }
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    }
  });
</script>
<!-- Holder for Upload File Thumbnail -->
{{ HTML::script('assets/vendor/holder/holder.js') }}
<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
{{ HTML::script('assets/vendor/jquery-file-upload/js/vendor/jquery.ui.widget.js') }}
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
{{ HTML::script('assets/vendor/jquery-file-upload/js/jquery.iframe-transport.js') }}
<!-- The basic File Upload plugin -->
{{ HTML::script('assets/vendor/jquery-file-upload/js/jquery.fileupload.js') }}
<!-- The File Upload processing plugin -->
{{ HTML::script('assets/vendor/jquery-file-upload/js/jquery.fileupload-process.js') }}
<!-- The File Upload validation plugin -->
{{ HTML::script('assets/vendor/jquery-file-upload/js/jquery.fileupload-validate.js') }}
<!-- File Upload Main -->
{{ HTML::script('assets/vendor/file-upload/reguler.js') }}

<script type="text/javascript">
    $(function(){
        $('#inputSelectUserCategory').change(function(){
            if ($('#inputSelectUserCategory').val() == 'all') {
                $('#formInputUser').hide();
            } else if ($('#inputSelectUserCategory').val() == 'specific_user') {
                $('#formInputUser').show();
            }
        });

        var showSelected = function(){
            var scheme = $('#target').val();
            console.log(scheme);

            $('#formInputSelectDesigner').hide();
            $('#formInputSchemeParameter').hide();
            $('#formInputUrl').hide();

            if (scheme == 'url') {
                $('#formInputUrl').show();
            }
            if (scheme == 'room/{id}') {
                $('#formInputSelectDesigner').show();
            }
            else if(scheme == 'search_kost?params={params}' || scheme == 'list_kost?params={params}' ){
                $('#formInputSchemeParameter').show();
            }
        };

        $('#target').change(showSelected);

    });

    /*Something */

    $(function () {
        // Change this to the location of your server-side upload handler
        var url = "{{ url('api/v1/media') }}";

        // Upload Engine for Style Photos
        try {
            myFileUpload($('#wrapper-file-upload'), url, 'style_photo');
        } catch(e) {
            console.log(e, 'not loged');
        }
    });
</script>
@stop
