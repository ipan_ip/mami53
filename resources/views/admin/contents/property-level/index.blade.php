@extends('admin.layouts.main')
@section('style')
<style>
    .form-filter .form-group {
        margin: 0px 4px;
    }
</style>
@stop
@section('content')
    <?php App::setLocale('id'); ?>
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
            <div class="box-tools pull-right">
                <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default" style="overflow:hidden">
                    <form action="" class="form-inline" method="get" style="text-align: right">
                        <input type="text" name="name" class="form-control input-sm"  placeholder="Nama Level" autocomplete="off" value="{{ Input::get('name') }}">
                        <button class="btn btn-primary btn-md" id="buttonSearch"><i class="fa fa-search">&nbsp;</i>Search</button>
                    </form>
                </div>
                <div class="btn-horizontal-group bg-default" style="overflow:hidden">
                    <a href="{{ route('admin.property.level.create') }}" class="btn btn-danger" style="float: right;"> Create Property Level </a>
                </div>
            </div>
            <table id="tableListDesigner" class="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Kost Level</th>
                        <th>Max Rooms</th>
                        <th>Minimum Charging</th>
                        <th style="width:80px">Date</th>
                        <th class="table-action-column" width="80px">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($propertyLevels as $propertyLevel)
                    <tr>
                        <td>{{ $propertyLevel->name }}</td>
                        <td>{{ $propertyLevel->kost_level ? $propertyLevel->kost_level->name : '-' }}</td>
                        <td>{{ $propertyLevel->max_rooms }}</td>
                        <td>{{ $propertyLevel->minimum_charging }}</td>
                        <td>{{ $propertyLevel->created_at->format('d-m-Y h:i') }}</td>
                        <td>
                            <form action="{{ route('admin.property.level.delete', $propertyLevel->id) }}" method="post">
                                @method('DELETE')
                                @csrf
                                <input class="btn btn-xs btn-danger btn-flat" type="submit" value="Delete"
                                    onClick="return confirm('Apakah Anda yakin mau hapus?')"
                                />
                            </form>
                            <a class="btn btn-xs bg-navy btn-flat" href="{{ route('admin.property.level.edit', $propertyLevel->id) }}">Edit</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $propertyLevels->appends(Request::except('page'))->links() }}
        </div>
    </div>
@stop
