@extends('admin.layouts.main')

@section('content')
    <a class="btn btn-xs bg-navy btn-flat" href="{{ route('admin.property.level.index') }}">Back</a>
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

        <form action="{{ URL::route('admin.property.level.store') }}" method="POST" class="form-horizontal form-bordered">
            @csrf
            <div class="box-body no-padding">
                <div class="form-group bg-default">
                    <label for="property-level-name" class="control-label col-sm-2">Property Level Name</label>
                    <div class="col-sm-10">
                        <input type="text" name="name" id="property-level-name" class="form-control" value="{{ old('name') }}">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="max-rooms" class="control-label col-sm-2">Max Rooms</label>
                    <div class="col-sm-10">
                        <input type="text" name="max_rooms" id="max-rooms" class="form-control" value="{{ old('max_rooms') }}">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="minimum-charging" class="control-label col-sm-2">Minimum Charging</label>
                    <div class="col-sm-10">
                        <input type="text" name="minimum_charging" id="minimum-charging" class="form-control" value="{{ old('minimum_charging') }}">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="kost-level" class="control-label col-sm-2">Kost Level</label>
                    <div class="col-sm-2">
                        <select name="kost_level_id" id="kost-level" class="form-control">
                            <option value="" disabled>Choose kos level</option>
                            @foreach($kostLevels as $level)
                                <option value="{{ $level->id }}">{{ $level->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <div class="col-sm-10 col-sm-push-2">
                        <button type="submit" class="btn btn-primary">Create</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
