@extends('admin.layouts.main')
@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
            <div class="box-tools pull-right">
                <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <form action="" class="form-inline" method="get" style="text-align:right;padding:10px;">
                        <input type="text" name="q" class="form-control input-sm"  placeholder="Title/ Contents/ Kost Name"  autocomplete="off" value="{{ request()->input('q') }}">
                        <button class="btn btn-primary btn-md" id="buttonSearch"><i class="fa fa-search">&nbsp;</i>Search</button>
                    </form>
                </div>
            </div>

            <table id="tableListTag" class="table table-striped">
                <thead>
                    <tr>
                        <th width="30px;">No</th>
                        <th>Kost Name</th>
                        <th>Title</th>
                        <th>Content</th>
                        <th>Time</th>
                        <th>Verification admin</th>
                        <th class="table-action-column">Action</th>
                    </tr>
                </thead>
                <tbody>
                   @foreach ($promotion AS $key => $value)
                       <tr>
                           <td>{{ $key+1 }}</td>
                           <td>@if (isset($value->room->name)) {{ $value->room->name }} @endif</td>
                           <td>{{ $value->title }}</td>
                           <td>{{ substr($value->content, 0, 50) }}</td> 
                           <td>{{ date('d M Y', strtotime($value->start_date)) }} <strong>s/d</strong> {{ date('d M Y', strtotime($value->finish_date)) }}</td>
                         
                           <td>@if ($value->verification == 'true')
                                   <label class="label label-success">Yes</label>
                               @else
                                   <label class="label label-danger">No</label>
                               @endif    
                           </td>
                           <td style="width: 150px;">
                             
                                <a href="{{ URL::route('admin.promo.show', $value->id) }}" class="btn btn-xs btn-warning">Show or Edit</a>
                                
                                @if ($value->verification == 'true')
                                    <a href="{{ URL::to('admin/promo/verify', $value->id) }}?is_confirm=false" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
                                @else
                                    <a href="{{ URL::to('admin/promo/verify', $value->id) }}?is_confirm=true" class="btn btn-xs btn-primary"><i class="fa fa-check"></i></a>
                                @endif
                                <a href="{{ URL::to('admin/promo/delete', $value->id) }}" onclick="return confirm('Yakin mau hapus saya?')" class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i></a>
                            </td>
                       </tr>
                   @endforeach
                </tbody>
                
            </table>
        </div><!-- /.box-body -->
        <div class="box-body no-padding">
            {{ $promotion->appends(Request::except('page'))->links() }}
        </div>
    </div><!-- /.box -->
    <!-- table -->

@stop