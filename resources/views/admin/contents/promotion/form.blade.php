@extends('admin.layouts.main')
@section('content')
<div class="box box-primary">
  <div class="box-header">
      <h3 class="box-title">{{ $boxTitle }}</h3>
  </div><!-- /.box-header -->
  {{ Form::open(array('route' => $formAction, 'method' => $formMethod,
      'class' => 'form-horizontal form-bordered', 'role' => 'form', 'id' => 'formInsertPaket', 'enctype' => 'multipart/form-data')) }}
      <div class="box-body no-padding">
           
            <div class="form-group bg-default">
              <label for="inputName" class="col-sm-2 control-label">Kost Name</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" placeholder="Name Paket"
                  id="inputName" name="name" value="{{ $promo->room->name }}" disabled="true">
                  <input type="hidden" class="form-control" placeholder="Name Paket"
                  id="inputName" name="user_id" value="{{ $promo->id }}" >
              </div>
            </div>

            <div class="form-group bg-default">
              <label for="inputName" class="col-sm-2 control-label">Title</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" placeholder="Name"
                  id="inputName" name="name" value="{{ $promo->title }}" disabled="true">
              </div>
            </div>

            <div class="form-group bg-default">
              <label for="inputName" class="col-sm-2 control-label">From date</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" placeholder="Name Paket"
                  id="inputName" name="name" value="{{ $promo->start_date }}" disabled="true">
              </div>
            </div>

            <div class="form-group bg-default">
              <label for="inputName" class="col-sm-2 control-label">To Date</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" placeholder="Name Paket"
                  id="inputName" name="name" value="{{ $promo->finish_date }}" disabled="true">
              </div>
            </div>

            <div class="form-group bg-default">
              <label for="inputName" class="col-sm-2 control-label">Content</label>
              <div class="col-sm-10">
                <textarea class="form-control" name="content">{{ $promo->content }}</textarea>
              </div>
            </div>

            <!-- select -->
            <div class="form-group bg-default">
                <label for="inputSelectType" class="col-sm-2 control-label">Confirmation Now</label>
                <div class="col-sm-10">
                    <select class="form-control" id="inputSelectType" name="is_confirm">
                        @foreach ($rowsActive as $active)
                          @if ($promo->verification == $active)
                          <option value="{{ $active }}" selected="selected">{{ $active }}</option>
                          @else
                          <option value="{{ $active }}">{{ $active }}</option>
                          @endif
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="{{ URL::route('admin.promo.index') }}">
                  <button type="button" class="btn btn-default">Cancel</button>
                </a>
                <button type="reset" class="btn btn-default"
                  onclick="$('#formInsertPaket').bootstrapValidator('resetForm', true);">Reset</button>
              </div>
            </div>
      </div>
  {{ Form::close() }}<!-- /.form -->
</div><!-- /.box -->
@stop
@section('script')
<!-- page script -->
<script type="text/javascript">

    @if (Session::get('laravelValidatorJSON') === null)
      myGlobal.laravelValidator = null;
    @else
      myGlobal.laravelValidator = {{ Session::get('laravelValidatorJSON') }};
    @endif


    $(function()
    {
      myGlobal.baseUrl = "{{ url('') . '/' }}";

      $formInsertPaket = $('#formInsertPaket');

      $( "#inputSelectType" ).change(function() {
        $formInsertPaket
          .bootstrapValidator('updateStatus', 'name', 'NOT_VALIDATED')
          .bootstrapValidator('validateField', 'name');
      });

      formStoreTagRules = {
          name: {
              message: 'The name is not valid',
              validators: {
                  notEmpty: {
                      message: 'The name is required and cannot be empty'
                  },
                  stringLength: {
                      min: 3,
                      max: 30,
                      message: 'The name must be more than 3 and less than 30 characters long'
                  },
                  // Send { username: 'its value', type: 'username' } to the back-end
                  remote: {
                      message: 'The name is available',
                      url: myGlobal.baseUrl + 'admin/tag/unique',
                      data: function(validator) {
                          return {
                              type : 'tag.name',
                              fields : {
                                name : validator.getFieldElements('name').val(),
                                type : validator.getFieldElements('type').val()
                              }
                          };
                      },
                  }
              }
          },
          type: {
              validators: {
                  notEmpty: {
                      message: 'The type is required and cannot be empty'
                  },
              }
          }
      };

      formUpdateTagRules = {
          name: {
              message: 'The name is not valid',
              validators: {
                  notEmpty: {
                      message: 'The name is required and cannot be empty'
                  },
                  stringLength: {
                      min: 1,
                      max: 30,
                      message: 'The name must be more than 1 and less than 30 characters long'
                  }
              }
          },
          type: {
              validators: {
                  notEmpty: {
                      message: 'The type is required and cannot be empty'
                  },
              }
          }
      };

      @if (Route::currentRouteName() === 'admin.tag.create')
        myGlobal.bootstrapValidatorDefaults.fields = formStoreTagRules;
      @elseif (Route::currentRouteName() === 'admin.tag.edit')
        myGlobal.bootstrapValidatorDefaults.fields = formUpdateTagRules;
      @endif


      $formInsertPaket.bootstrapValidator(myGlobal.bootstrapValidatorDefaults);

      updateValidateStatus($formInsertPaket.data('bootstrapValidator'), myGlobal.laravelValidator);

    });
</script>
@stop