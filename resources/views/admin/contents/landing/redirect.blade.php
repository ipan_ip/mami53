@extends('admin.layouts.main')

@section('style')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/dropzone.css">
    {{ HTML::style('assets/vendor/jquery-chosen/chosen.css') }}
    <link rel="stylesheet" href="/assets/vendor/summernote/summernote.css?v=05042018">
    <link rel="stylesheet" href="{{ mix_url('dist/vendor/leaflet/leaflet.css') }}">

    <style>
        .chosen-choices {
            border: 1px solid #ccc;
            border-radius: 4px;
            min-height: 34px;
            padding: 6px 12px;
        }
        .note-group-select-from-files {
            display: none;
        }
    </style>
@endsection

@section('content')
    <!-- table -->
    <div class="box box-primary" style="padding: 10px;">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div><!-- /.box-header -->
		<form action="/admin/landing/redirect/{{ $landingId }}" method="post">
		    <div class="form-group">
		        <label for="landing_destination">ID Landing Tujuan</label>
                <select class="form-control chosen-select" id="landing_destination" name="landing_destination">
                    <option value="">Pilih Landing Tujuan</option>
                    @foreach($listLanding as $list)
                        <option value="{{ $list->id }}">{{ $list->id ." | ". $list->heading_1 }}</option>
                    @endforeach
                </select>
		    </div>
		    <button type="submit" class="btn btn-default">Submit</button>
		</form>
    </div><!-- /.box -->
    <!-- table -->
@endsection

@section('script')
    {{ HTML::script('assets/vendor/jquery-chosen/chosen.jquery.js') }}
    <script type="text/javascript">
        $(function() {
            var config = {
                '.chosen-select'  : {width: '100%', height: '100%'}
            };
            for (var selector in config) {
                $(selector).chosen(config[selector]);
            }
        });
    </script>
@endsection
