@extends('admin.layouts.main')

@section('content')
<div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">{{ $isStateCreate ? 'Create Kost' : 'Edit Kost - ' . $room->name }}</h3>
    </div>
    <div class="form-horizontal form-bordered">
    <div class="box-body no-padding">
        <form id="kostForm" method="POST" action="{{ $isStateCreate ? route('admin.kost.store') : route('admin.kost.update', $room->id) }}">
        @if(! $isStateCreate) @method('PUT') @endif
        @csrf
            <div class="form-group bg-info divider">
                <div class="col-sm-12 pull-left">
                    Data Kos
                </div>
            </div>
            @if ($isStateCreate)
            <div class="form-group bg-default">
                <label class="col-sm-2 control-label"><span class="label label-success">new</span>&nbsp;&nbsp;*Nama Property</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Nama Property" name="property_name" value="{{ old('property_name') }}" required>
                    <small class="help-block">Format Nama Properti tidak boleh berisi karakter: . , ( ) / - " '</small>
                </div>
            </div>
            <div class="form-group bg-default">
                <label class="col-sm-2 control-label"><span class="label label-success">new</span>&nbsp;&nbsp;Tipe Kamar
                    <button type="button" class="btn btn-success" id="allow-multiple-type" style="font-size: 10px;" >&#9989;</button>                                                    
                </label>
                <div class="col-sm-10">
                    <input type="hidden" id="has-multiple-type" value="1">
                    <input type="text" class="form-control" placeholder="Tipe Kamar" name="unit_type" value="{{ old('unit_type') }}">
                </div>
            </div>
            @elseif (! is_null($room->owners->first()))
            <div class="form-group bg-default">
                <label class="col-sm-2 control-label"><span class="label label-success">new</span>&nbsp;&nbsp;Nama Properti</label>
                <div class="col-sm-10">
                    {{-- @if (is_null($room->property)) --}}
                    <select class="form-control" name="property_id" tabindex="2" id="propertySelect">
                        <option value=0 selected>-</option>
                        @foreach ($properties as $property)
                            <option value="{{ $property->id }}" @if(!is_null($room->property) && $room->property->id == $property->id) selected @endif>{{ $property->name }}</option>
                        @endforeach
                    </select>
                    <a class="btn btn-primary btn-sm margin-top" data-toggle="modal" data-target="#addProperty">+ Tambah Properti</a>
                    <a class="btn btn-info btn-sm margin-top" data-toggle="modal" href="#" data-target="#editProperty">Edit Properti</a>
                    {{-- @else
                    <input type="hidden" name="property_id" value="{{ $room->property->id }}">
                    <input disabled type="text" class="form-control" placeholder="Nama Property" name="property_name" value="{{ $room->property->name}}">
                    @endif --}}
                </div>
            </div>
            <div class="form-group bg-default">
                <label class="col-sm-2 control-label">
                    <span class="label label-success">new</span>&nbsp;&nbsp;Tipe Kamar
                    @if(!is_null($selectedProperty) && $selectedProperty->has_multiple_type):
                        <button type="button" class="btn btn-success" id="allow-multiple-type" style="font-size: 10px;" disabled>&#9989;</button>
                    @else
                        <button type="button" class="btn btn-danger" id="allow-multiple-type" style="font-size: 10px;" >&#10005;</button>                                        
                    @endif    
                    </label>
                    <div class="col-sm-10">
                    <input type="hidden" id="has-multiple-type" name="has_multiple_type" value="0">
                    <input id="unit-type" type="text" class="form-control" placeholder="Tipe Kamar" name="unit_type" value="{{ $room->unit_type }}" {{ !is_null($selectedProperty) && $selectedProperty->has_multiple_type ? '' : 'disabled' }}>
                    <p class="text-warning" id="alert-unit-type"></p>
                </div>
            </div>
            @endif
            <div class="form-group bg-default">
                <label class="col-sm-2 control-label">*Nama Kos</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Nama Kos" name="name" value="{{ $isStateCreate ? old('name') : $room->name }}" required>
                </div>
            </div>
            <div class="form-group bg-default">
                <label class="col-sm-2 control-label">Mamirooms</label>
                <div class="col-sm-10">
                    <div class="checkbox">
                        <label>
                            @if ($isStateCreate)
                                <input type="checkbox" name="mamirooms" {{ old('mamirooms') == '1' ? 'checked' : '' }} value="1">
                            @else
                                <input type="checkbox" name="mamirooms" {{ $room->is_mamirooms ? 'checked' : '' }} value="1">
                            @endif
                            Mamirooms
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group bg-default">
                <label class="col-sm-2 control-label">Testing</label>
                <div class="col-sm-10">
                    <div class="checkbox">
                        <label>
                            @if ($isStateCreate)
                                <input type="checkbox" name="testing" {{ old('testing') == '1' ? 'checked' : '' }} value="1">
                            @else
                                <input type="checkbox" name="testing" {{ $room->is_testing ? 'checked' : '' }} value="1">
                            @endif
                            Testing
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group bg-default">
                <label class="col-sm-2 control-label">Indexed</label>
                <div class="col-sm-10">
                    <div class="checkbox">
                        <label>
                            @if ($isStateCreate)
                                <input type="checkbox" name="indexed" {{ old('indexed') == '1' ? 'checked' : '' }} value="1">
                            @else
                                <input type="checkbox" name="indexed" {{ $room->is_indexed ? 'checked' : '' }} value="1">
                            @endif
                            Indexed
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group bg-default">
                <label class="col-sm-2 control-label">*Jenis Kos</label>
                <div class="col-sm-10">
                <select class="form-control" id="inputSelectType1" name="gender" tabindex="2" data-placeholder="Room Type">
                    @if ($isStateCreate)
                    <option value=0 @if(old('gender') == 0) selected @endif>Campur</option>
                    <option value=1 @if(old('gender') == 1) selected @endif>Putra</option>
                    <option value=2 @if(old('gender') == 2) selected @endif>Putri</option>
                    @else
                    <option value=0 @if($room->gender == 0) selected @endif>Campur</option>
                    <option value=1 @if($room->gender == 1) selected @endif>Putra</option>
                    <option value=2 @if($room->gender == 2) selected @endif>Putri</option>
                    @endif
                </select>
                </div>
            </div>
            <div class="form-group bg-default">
                @if ($isStateCreate)
                <label class="col-sm-2 control-label"><span class="label label-success">new</span>&nbsp;&nbsp;*Kapan Kos Dibangun</label>
                @else 
                <label class="col-sm-2 control-label"><span class="label label-success">new</span>&nbsp;&nbsp;Kapan Kos Dibangun</label>
                @endif
                <div class="col-sm-10">
                    <select class="form-control" name="building_year" tabindex="2">
                        <option value=0 selected >-</option>
                        @if ($isStateCreate)
                        @for ($i = date('Y'); $i >= 1950; $i--)
                            <option value="{{ $i }}" @if(old('building_year') == $i) selected @endif>{{ $i }}</option>
                        @endfor
                        @else
                        @for ($i = date('Y'); $i >= 1950; $i--)
                            <option value="{{ $i }}" @if($room->building_year == $i) selected @endif>{{ $i }}</option>
                        @endfor
                        @endif
                    </select>
                </div>
            </div>
            <div class="form-group bg-default">
                @if ($isStateCreate)
                <label class="col-sm-2 control-label">*Deskripsi Kos</label>
                <div class="col-sm-10">
                    <textarea class="form-control" name="description" rows="3" required>{{ old('description') }}</textarea>
                </div>
                @else 
                <label class="col-sm-2 control-label">Deskripsi Kos</label>
                <div class="col-sm-10">
                    <textarea class="form-control" name="description" rows="3">{{ $room->description }}</textarea>
                </div>
                @endif
            </div>
            <div class="form-group bg-default">
                <label class="col-sm-2 control-label">Catatan Lainnya</label>
                <div class="col-sm-10">
                    <textarea class="form-control" name="remark" rows="3">{{ $isStateCreate ? old('remark') : $room->remark }}</textarea>
                </div>
            </div>

            <div class="form-group bg-info divider">
                <div class="col-sm-12">
                    Data Kamar
                </div>
            </div>
            <div class="form-group bg-default">
                <label class="col-sm-2 control-label">*Ukuran Kamar</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="ex. 4x4 / 4.5x5 / 3.4x4.2" name="size" value="{{ $isStateCreate ? old('size') : $room->size }}" onchange="this.value = this.value.replace(/,/g, '.')" required>
                </div>
            </div>
            <div class="form-group bg-default">
                <label class="col-sm-2 control-label" for="inputRoomCount">*Jumlah Kamar</label>
                <div class="col-sm-10">
                    @if($isStateCreate)
                    <input class="form-control" type="number" id="inputRoomCount" name="room_count" placeholder="Total kamar yang ada" value="{{ old('room_count') }}" required>
                    @else
                    <input type="hidden" name="room_count" value="{{ $isStateCreate ? old('room_count') : $room->room_count}}" />
                    <input disabled class="form-control" type="number" id="inputRoomCount" name="room_count" placeholder="Total kamar yang ada" value="{{ $isStateCreate ? old('room_count') : $room->room_count }}">
                    @endif
                </div>
            </div>
            <div class="form-group bg-default">
                <label class="col-sm-2 control-label" for="inputRoomAvailable">*Jumlah Kamar Tersedia</label>
                <div class="col-sm-10">
                    @if($isStateCreate)
                    <input class="form-control" type="number" id="inputRoomAvailable" name="room_available" placeholder="Jumlah kamar kosong" value="{{ old('room_available') }}" required>
                    @else
                    <input type="hidden" name="room_available" value="{{ $isStateCreate ? old('room_available') : $room->room_available }}" />
                    <input disabled class="form-control" type="number" id="inputRoomAvailable" name="room_available" placeholder="Jumlah kamar kosong" value="{{ $isStateCreate ? old('room_available') : $room->room_available }}">
                    @endif
                </div>
            </div>
            @if(! $isStateCreate)
            <div class="form-group bg-default">
                <label class="col-sm-2 control-label">Ketersediaan kamar</label>
                <div class="col-sm-10">
                    <a type="button" target="_blank" rel="noopener noreferrer" class="btn btn-sm btn-primary" href="{{ url('admin/room/' . $room->id . '/room-unit') }}">Update Data Kamar</a>
                </div>
            </div>
            <div class="form-group bg-default">
                <label class="col-sm-2 control-label"><span class="label label-success">new</span>&nbsp;&nbsp;*Atur Foto Kos dan Kamar</label>
                <div class="col-sm-10">
                    <a type="button" target="_blank" rel="noopener noreferrer" class="btn btn-sm btn-primary" href="{{ url('admin/card/' . $room->id ) }}">Atur Foto</a>
                </div>
            </div>
            @endif

            <div class="form-group bg-info divider">
                <div class="col-sm-12">
                    Data Alamat
                </div>
            </div>
            <div class="form-group bg-default">
                <label for="inputAddress" class="col-sm-2 control-label">*Alamat</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control"
                           id="inputAddress" name="address" value="{{ $isStateCreate ? old('address') : $room->address }}" required>
                </div>
            </div>
            <div class="form-group bg-default">
                <label for="inputGeoName" class="col-sm-2 control-label">*Titik Koordinat</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Cari Lokasi" id="inputGeoName" name="geo_name">
                </div>
                <div class="col-sm-10 col-sm-offset-2">
                    <div style="background: #EEE; width: 100%; height: 300px;">
                        <div id="map-canvas"></div>
                    </div>
                </div>
                <div class="col-sm-10 col-sm-offset-2">
                    <div class="input-group">
                        <div class="input-group-addon">Coordinate (latitude, longitude)</div>
                        <input disabled type="text" class="form-control" placeholder="Latitude, Longitude" id="inputCoordinate" name="coordinate" value="{{ $isStateCreate ? (is_null(old('latitude')) ? -7.7858485 : old('latitude')) . ',' . (is_null(old('longitude')) ? 110.3680087 : old('longitude')) : $room->latitude . ',' . $room->longitude }}">
                        <input type="hidden" id="inputLatitude" name="latitude" value="{{ $isStateCreate ? (is_null(old('latitude')) ? -7.7858485 : old('latitude')) : $room->latitude }}">
                        <input type="hidden" id="inputLongitude" name="longitude" value="{{ $isStateCreate ? (is_null(old('longitude')) ? 110.3680087 : old('longitude')) : $room->longitude }}">
                    </div>
                </div>
            </div>
            <div class="form-group bg-default">
                <label class="col-sm-2 control-label"><span class="label label-success">new</span>&nbsp;&nbsp;*Provinsi</label>
                <div class="col-sm-10">
                    <input
                        type="text"
                        id="inputAreaProvince"
                        class="form-control"
                        name="province"
                        value="{{ $isStateCreate ? old('province') : (is_null($room->address_note) ? '' : (is_null($room->address_note->province) ? '' : $room->address_note->province->name)) }}"
                        list="province-list"
                        placeholder="Pilih Provinsi/Geser Pin di Map"
                        autocomplete="off"
                        required
                    />
                    <datalist id="province-list">
                        @foreach ($provinceList as $province)
                            <option value="{{ $province->name }}" />
                        @endforeach
                    </datalist>
                </div>
            </div>
            <div class="form-group bg-default">
                <label class="col-sm-2 control-label" for="inputAreaCity">*Kabupaten/Kota</label>
                <div class="col-sm-10">
                    <input
                        type="text"
                        id="inputAreaCity"
                        class="form-control input-city-regency"
                        rows="3"
                        name="area_city"
                        value="{{ $isStateCreate ? old('area_city') : $room->area_city }}"
                        data-toggle="dropdown"
                        list="city-list"
                        placeholder="Pilih Kota/Kabupaten"
                        autocomplete="off"
                        required
                    />
                    <datalist id="city-list">
                    </datalist>
                </div>
            </div>
            <div class="form-group bg-default">
                <label class="col-sm-2 control-label" for="inputSubdistrict">*Kecamatan</label>
                <div class="col-sm-10">
                    <input
                        id="inputSubdistrict"
                        class="form-control"
                        name="area_subdistrict"
                        value="{{ $isStateCreate ? old('area_subdistrict') : $room->area_subdistrict }}"
                        list="subdistrict-list"
                        placeholder="Pilih Kecamatan"
                        autocomplete="off"
                        required
                    />
                    <datalist id="subdistrict-list">
                    </datalist>
                </div>
            </div>
            <div class="form-group bg-default">
                <label class="col-sm-2 control-label">Area Big</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" rows="3" name="area_big" value="{{ $isStateCreate ? old('area_big') : $room->area_big }}">
                </div>
            </div>
            <div class="form-group bg-default">
                <label class="col-sm-2 control-label" for="inputGuide" >Penunjuk Arah / Catatan Alamat</label>
                <div class="col-sm-10">
                    <textarea id="inputGuide" rows="3" class="form-control" name="guide">{{ $isStateCreate ? old('guide') : $room->guide }}</textarea>
                </div>
            </div>

            <div class="form-group bg-info divider">
                <div class="col-sm-12">
                    Data Harga Sewa
                </div>
            </div>
            <div class="form-group bg-default">
                <label class="col-sm-2 control-label"><span class="label label-success">new</span>&nbsp;&nbsp;Minimum Durasi Sewa</label>
                <div class="col-sm-10">
                <select class="form-control" name="min_payment" tabindex="2" data-placeholder="Minimum durasi sewa" >
                    <option value=0 selected>-</option>
                    @if ($isStateCreate)
                    @foreach ($minPayment as $facility)
                        <option value="{{ $facility->id }}" @if(old('min_payment') == $facility->id) selected @endif>{{ $facility->name }}</option>
                    @endforeach
                    @else
                    @foreach ($minPayment as $facility)
                        <option value="{{ $facility->id }}" @if(in_array($facility->id, $selectedTags)) selected @endif>{{ $facility->name }}</option>
                    @endforeach
                    @endif
                </select>
                </div>
            </div>
            <div class="form-group bg-default">
                <label for="inputPriceDaily" class="col-sm-2 control-label">Harga Per Hari</label>
                <div class="col-sm-10">
                    <input type="number" class="form-control" placeholder="0"
                           id="inputPriceDaily" name="price_daily" value="{{ $isStateCreate ? old('price_daily') : $room->price_daily }}" >
                </div>
            </div>
            <div class="form-group bg-default">
                <label for="inputPriceWeekly" class="col-sm-2 control-label">Harga Per Minggu</label>
                <div class="col-sm-10">
                    <input type="number" class="form-control" placeholder="0"
                           id="inputPriceWeekly" name="price_weekly" value="{{ $isStateCreate ? old('price_weekly') : $room->price_weekly }}" >
                </div>
            </div>
            <div class="form-group bg-default">
                <label for="inputPriceMonthly" class="col-sm-2 control-label">*Harga Per Bulan</label>
                <div class="col-sm-10">
                    <input type="number" class="form-control" placeholder="0"
                           id="inputPriceMonthly" name="price_monthly" value="{{ $isStateCreate ? old('price_monthly') : $room->price_monthly }}" required>
                </div>
            </div>
            <div class="form-group bg-default">
                <label for="inputPriceYearly" class="col-sm-2 control-label">Harga Per Tahun</label>
                <div class="col-sm-10">
                    <input type="number" class="form-control" placeholder="0"
                           id="inputPriceYearly" name="price_yearly" value="{{ $isStateCreate ? old('price_yearly') : $room->price_yearly }}" >
                </div>
            </div>
            <div class="form-group bg-default">
                <label class="col-sm-2 control-label">Harga Per 3 Bulan</label>
                <div class="col-sm-10">
                    <input type="number" class="form-control" placeholder="0"
                    name="price_quarterly" value="{{ $isStateCreate ? old('price_quarterly') : $room->price_quarterly }}" >
                </div>
            </div>
            <div class="form-group bg-default">
                <label class="col-sm-2 control-label">Harga Per 6 Bulan</label>
                <div class="col-sm-10">
                    <input type="number" class="form-control" placeholder="0"
                    name="price_semiannually" value="{{ $isStateCreate ? old('price_semiannually') : $room->price_semiannually }}" >
                </div>
            </div>

            <div class="form-group bg-info divider">
                <div class="col-sm-12">
                    Data Biaya Tambahan
                </div>
            </div>
            <div class="form-group bg-default">
                <label class="col-sm-2 control-label">
                    <span class="label label-success">new</span>
                    &nbsp;&nbsp;
                    <input type="hidden" name="additional_cost_active" value="0">
                    @if ($isStateCreate)
                    <input title="Centang untuk Aktifkan / Hapus Centang untuk Non-aktifkan" type="checkbox" value="1" class="form-check-input" name="additional_cost_active" @if(old('additional_cost_active') == 1) checked @endif>
                    @else
                    <input title="Centang untuk Aktifkan / Hapus Centang untuk Non-aktifkan" type="checkbox" value="1" class="form-check-input" name="additional_cost_active" @if(isset($additionalPrice['additional_cost'][0]) && $additionalPrice['additional_cost'][0]['is_active']) checked @endif>
                    @endif
                    Biaya Tambahan Lainnya Perbulan
                </label>
                <div class="container1 col-sm-10">
                    <button class="add_form_field btn btn-primary btn-sm">+ Tambah Biaya</button>
                    @if ($isStateCreate)
                    @foreach ((array)old('additional_cost') as $key => $value)
                        <div class="row margin-top">
                            <div class="col-sm-5">
                                <input placeholder="Nama Biaya" type="text" class="form-control" value="{{ $value['name'] }}" name="{{ 'additional_cost['.$key.'][name]' }}">
                            </div>
                        <div class="col-sm-5">
                            <input placeholder="Jumlah Biaya" type="number" class="form-control" value="{{ $value['price'] }}" name="{{ 'additional_cost['.$key.'][price]' }}">
                        </div>
                        <a href="#" class="delete">x</a></div>
                    @endforeach
                    @else
                    @foreach ($additionalPrice['additional_cost'] as $key => $value)
                        <div class="row margin-top">
                            <div class="col-sm-5">
                                <input placeholder="Nama Biaya" type="text" class="form-control" value="{{ $value['name'] }}" name="{{ 'additional_cost['.$key.'][name]' }}">
                            </div>
                        <div class="col-sm-5">
                            <input placeholder="Jumlah Biaya" type="number" class="form-control" value="{{ $value['price'] }}" name="{{ 'additional_cost['.$key.'][price]' }}">
                        </div>
                        <input type="hidden" value="{{ $value['id'] }}" name="{{ 'additional_cost['.$key.'][id]' }}">
                        <a href="#" class="delete">x</a></div>
                    @endforeach
                    @endif
                </div>
            </div>
            <div class="form-group bg-default">
                <label class="col-sm-2 control-label">
                    <span class="label label-success">new</span>
                    &nbsp;&nbsp;
                    <input type="hidden" name="fine_active" value="0">
                    @if ($isStateCreate)
                    <input title="Centang untuk Aktifkan / Hapus Centang untuk Non-aktifkan" type="checkbox" value="1" class="form-check-input" name="fine_active" @if(old('fine_active') == 1) checked @endif>
                    @else
                    <input title="Centang untuk Aktifkan / Hapus Centang untuk Non-aktifkan" type="checkbox" value="1" class="form-check-input" name="fine_active" @if(isset($additionalPrice['fine']['is_active'])) checked @endif>
                    @endif
                    Biaya Denda
                </label>
                <div class="col-sm-10">
                    <input placeholder="Jumlah Denda" type="number" class="form-control" name="fine_price" value="{{ $isStateCreate ? old('fine_price') : $additionalPrice['fine']['price'] }}" >
                    <small class="help-block">Dibebankan bila penyewa telat bayar setelah</small>
                </div>
                <label class="col-sm-2 control-label"></label>
                <div class="col-sm-5">
                    <input placeholder="0" type="number" class="form-control" name="fine_length" value="{{ $isStateCreate ? old('fine_length') : $additionalPrice['fine']['length'] }}" >
                </div>
                <div class="col-sm-5">
                    <select class="form-control" name="fine_type" tabindex="2" >
                        @if ($isStateCreate)
                        <option value="day" @if(old('fine_type') == 'day') selected @endif>Hari</option>
                        <option value="week" @if(old('fine_type') == 'week') selected @endif>Minggu</option>
                        <option value="month" @if(old('fine_type') == 'month') selected @endif>Bulan</option>
                        @else
                        <option value="day" @if($additionalPrice['fine']['duration_type'] == 'day') selected @endif>Hari</option>
                        <option value="week" @if($additionalPrice['fine']['duration_type'] == 'week') selected @endif>Minggu</option>
                        <option value="month" @if($additionalPrice['fine']['duration_type'] == 'month') selected @endif>Bulan</option>
                        @endif
                    </select>
                </div>
            </div>
            <div class="form-group bg-default">
                <label class="col-sm-2 control-label">
                    <span class="label label-success">new</span>
                    &nbsp;&nbsp;
                    <input type="hidden" name="deposit_active" value="0">
                    @if ($isStateCreate)
                    <input title="Centang untuk Aktifkan / Hapus Centang untuk Non-aktifkan" type="checkbox" value="1" class="form-check-input" name="deposit_active" @if(old('deposit_active') == 1) checked @endif>
                    @else
                    <input title="Centang untuk Aktifkan / Hapus Centang untuk Non-aktifkan" type="checkbox" value="1" class="form-check-input" name="deposit_active" @if($additionalPrice['deposit_fee']['is_active']) checked @endif>
                    @endif
                    Biaya Deposit
                </label>
                <div class="col-sm-10">
                    <input type="number" placeholder="0" class="form-control" name="deposit" value="{{ $isStateCreate ? old('deposit') : $additionalPrice['deposit_fee']['price'] }}">
                </div>
            </div>
            <div class="form-group bg-default">
                <label class="col-sm-2 control-label">
                    <span class="label label-success">new</span>
                    &nbsp;&nbsp;
                    <input type="hidden" name="dp_active" value="0">
                    @if ($isStateCreate)
                    <input title="Centang untuk Aktifkan / Hapus Centang untuk Non-aktifkan" type="checkbox" value="1" class="form-check-input" name="dp_active" @if(old('dp_active') == 1) checked @endif>
                    @else
                    <input title="Centang untuk Aktifkan / Hapus Centang untuk Non-aktifkan" type="checkbox" value="1" class="form-check-input" name="dp_active" @if($additionalPrice['down_payment']['is_active']) checked @endif>
                    @endif
                    Uang Muka (DP)
                </label>
                <div class="col-sm-10">
                    <select class="form-control" name="dp" tabindex="2" >
                        @if ($isStateCreate)
                        <option value=0 selected>0</option>
                        <option value=10 @if(old('dp') == 10) selected @endif>10%</option>
                        <option value=20 @if(old('dp') == 20) selected @endif>20%</option>
                        <option value=30 @if(old('dp') == 30) selected @endif>30%</option>
                        <option value=40 @if(old('dp') == 40) selected @endif>40%</option>
                        <option value=50 @if(old('dp') == 50) selected @endif>50%</option>
                        @else
                        <option value=0 selected>0</option>
                        <option value=10 @if($additionalPrice['down_payment']['percentage'] == 10) selected @endif>10%</option>
                        <option value=20 @if($additionalPrice['down_payment']['percentage'] == 20) selected @endif>20%</option>
                        <option value=30 @if($additionalPrice['down_payment']['percentage'] == 30) selected @endif>30%</option>
                        <option value=40 @if($additionalPrice['down_payment']['percentage'] == 40) selected @endif>40%</option>
                        <option value=50 @if($additionalPrice['down_payment']['percentage'] == 50) selected @endif>50%</option>
                        @endif
                    </select>
                </div>
            </div>
            <div class="form-group bg-default">
                <label class="col-sm-2 control-label">Keterangan Harga Lain</label>
                <div class="col-sm-10">
                    <textarea type="text" rows="3" class="form-control" name="price_remark">{{ $isStateCreate ? old('price_remark') : $room->price_remark }}</textarea>
                </div>
            </div>

            <div class="form-group bg-info divider">
                <div class="col-sm-12">
                    Data Kontak
                </div>
            </div>
            <div class="form-group bg-default">
                <label class="col-sm-2 control-label" for="inputOwnerName">*Nama Pemilik</label>
                <div class="col-sm-10">
                    <input id="inputOwnerName" class="form-control" name="owner_name" placeholder="Nama Pemilik" value="{{ $isStateCreate ? old('owner_name') : $room->owner_name }}">
                </div>
            </div>
            <div class="form-group bg-default">
                <label class="col-sm-2 control-label">*No. HP Pemilik</label>
                <div class="col-sm-10">
                    <input type="number" id="owner_phone" class="form-control" rows="3" name="owner_phone" placeholder="No. HP Pemilik" value="{{ $isStateCreate ? old('owner_phone') : $room->owner_phone }}">
                    @if (! $isStateCreate)
                        <input type="hidden" id="id_id" value="{{ $room->id }}">
                        <input type="hidden" id="kos_name" value="{{ $room->name }}">
                        <input type="hidden" id="user_login" value="{{ Auth::user()->email }}">
                    @endif
                </div>
            </div>
            <div class="form-group bg-default">
                <label class="col-sm-2 control-label" for="inputManagerName">Nama Manajer</label>
                <div class="col-sm-10">
                    <input id="inputManagerName" class="form-control" name="manager_name" placeholder="Nama Manajer" value="{{ $isStateCreate ? old('manager_name') : $room->manager_name }}">
                </div>
            </div>
            <div class="form-group bg-default">
                <label class="col-sm-2 control-label">No. HP Manajer</label>
                <div class="col-sm-10">
                    <input type="number" id="manager_phone" class="form-control" name="manager_phone" placeholder="No. HP Manajer" value="{{ $isStateCreate ? old('manager_phone') : $room->manager_phone }}">
                </div>
            </div>
            @if($isStateCreate)
            <div class="form-group bg-default">
                <label class="col-sm-2 control-label" for="inputAgentName">*Nama Agen</label>
                <div class="col-sm-10">
                    <input id="inputAgentName" class="form-control" name="agent_name" placeholder="Nama Agen" value="{{ old('agent_name') }}" required>
                </div>
            </div>
            @else
            <div class="form-group bg-default">
                <label class="col-sm-2 control-label">No. Telepon Kantor</label>
                <div class="col-sm-10">
                    <input class="form-control" rows="3" name="office_phone" placeholder="No. Telepon Kantor" disabled="disabled" value="{{ $room->office_phone }}">
                    <small class="help-block">Lebih dari satu, pisahkan dengan koma</small>
                </div>
            </div>
            <div class="form-group bg-default">
                <label class="col-sm-2 control-label" for="inputAgentName">Nama Agen</label>
                <div class="col-sm-10">
                    <input id="inputAgentName" class="form-control" name="agent_name" placeholder="Nama Agen" value="{{ $room->agent_name }}">
                </div>
            </div>
            <div class="form-group bg-default">
                <label class="col-sm-2 control-label">Nomor Agen</label>
                <div class="col-sm-10">
                    <input class="form-control" name="agent_phone" placeholder="Nomor Agen" value="{{ $agent_data[0] }}" @if (!$show_agent_input) disabled="true" @endif>
                </div>
            </div>
            <div class="form-group bg-default">
                <label class="col-sm-2 control-label">Email Agen</label>
                <div class="col-sm-10">
                    <input class="form-control" name="agent_email" placeholder="Email Agen" value="{{ $room->agent_email }}">
                </div>
            </div>
            @endif
            <div class="form-group bg-default">
                <label class="col-sm-2 control-label" for="inputVerificator">Verifikator</label>
                <div class="col-sm-10">
                    <input id="inputVerificator" class="form-control" name="verificator" placeholder="Nama Verifikator" value="{{ $isStateCreate ? old('verificator') : $room->verificator }}">
                </div>
            </div>
            <div class="form-group bg-default" for="inputAgentStatus">
                <label class="col-sm-2 control-label">Status Agent</label>
                <div class="col-sm-10">
                    <select class="form-control" name="agent_status" id="inputAgentStatus">
                        @if ($isStateCreate)
                        @foreach ($agentStatus as $agent)
                            <option value="{{ $agent }}" {{ old('agent_status') == $agent ? 'selected' : '' }} >{{ $agent }}</option>
                        @endforeach
                        @else
                        @foreach ($agentStatus as $agent)
                            <option value="{{ $agent }}" {{ $room->agent_status == $agent ? 'selected' : '' }} >{{ $agent }}</option>
                        @endforeach
                        @endif
                    </select>
                </div>
            </div>
            @if (! $isStateCreate) 
            <div class="form-group bg-default">
                <label class="col-sm-2 control-label">Catatan Agen</label>
                <div class="col-sm-10">
                  <textarea class="form-control" name="agent_note">@if (count($room->listing_reason) > 0) {{ $room->listing_reason[0]->content }} @endif </textarea>
                </div>
            </div>
            @endif
            <div class="form-group bg-info divider">
                <div class="col-sm-12">
                    Data Lainnya
                </div>
            </div>
            <div class="form-group bg-default">
                <label for="inputFacRoomOther" class="col-sm-2 control-label">Fasilitas Kamar Lainnya</label>
                <div class="col-sm-10">
                    <input class="form-control" id="inputFacRoomOther" name="fac_room_other" placeholder="Fasilitas" value="{{ $isStateCreate ? old('fac_room_other') : $room->fac_room_other }}">
                </div>
            </div>
            <div class="form-group bg-default">
                <label for="inputFacBathOther" class="col-sm-2 control-label">Fasilitas Kamar Mandi Lainnya</label>
                <div class="col-sm-10">
                    <input class="form-control" rows="3" id="inputFacBathOther" name="fac_bath_other" placeholder="Fasilitas" value="{{ $isStateCreate ? old('fac_bath_other') : $room->fac_bath_other }}">
                </div>
            </div>
            <div class="form-group bg-default">
                <label for="inputFacShareOther" class="col-sm-2 control-label">Fasilitas Bersama Lainnya</label>
                <div class="col-sm-10">
                    <input class="form-control" id="inputFacShareOther" name="fac_share_other" placeholder="fasilitas" value="{{ $isStateCreate ? old('fac_share_other') : $room->fac_share_other }}">
                </div>
            </div>
            <div class="form-group bg-default">
                <label for="inputFacNearOther" class="col-sm-2 control-label">Fasilitas Dekat Kos Lainnya</label>
                <div class="col-sm-10">
                    <input class="form-control" id="inputFacNearOther" name="fac_near_other" placeholder="fasilitas" value="{{ $isStateCreate ? old('fac_near_other') : $room->fac_near_other }}">
                </div>
            </div>
            @if (! $isStateCreate)   
            <div class="form-group bg-default">
                <label for="inputAdminRemark" class="col-sm-2 control-label">Admin Notes</label>
                <div class="col-sm-10">
                    <input class="form-control" rows="3" id="inputAdminRemark" name="admin_remark" placeholder="Catatan Admin" value="{{ $isStateCreate ? old('admin_remark') : $room->admin_remark }}">
                </div>
            </div>
            <div class="form-group bg-default">
                <label for="inputDescription" class="col-sm-2 control-label">Booking</label>
                <div class="col-sm-10">
                    <select class="form-control" disabled>
                    @if ($room->is_booking == 1)
                        <option value="1">Yes</option>
                    @else
                        <option value="0">No</option>
                    @endif
                    </select>
                    <div id="countDesc" style="font-weight:bold"></div>
                </div>
            </div>
            <div class="form-group bg-default">
                <label class="col-sm-2 control-label">Youtube Id</label>
                <div class="col-sm-10">
                    <input class="form-control" rows="3" name="youtube_id" placeholder="Youtube Id" value="{{ $room->youtube_id }}">

                    ex : https://www.youtube.com/watch?v=<span style="color: #ff0000">Youtube_ID</span>
                </div>
            </div>
            @endif

            <div class="form-group bg-info divider">
                <div class="col-sm-12">
                    Data Fasilitas
                </div>
            </div>
            <div class="form-group bg-default">
                @if ($isStateCreate)
                <label for="inputFacilityShare" class="col-sm-2 control-label"><span class="label label-success">new</span>&nbsp;&nbsp;*Fasilitas Umum</label>
                @else
                <label for="inputFacilityShare" class="col-sm-2 control-label"><span class="label label-success">new</span>&nbsp;&nbsp;Fasilitas Umum</label>
                @endif
                <div class="col-sm-10">
                    <select class="form-control chosen-select" multiple id="inputFacilityShare" name="facility_share_ids[]" tabindex="2">
                        @if ($isStateCreate)
                        @foreach ($facilityShare as $facility)
                            <option value="{{ $facility->id }}"
                                    {{ !is_null(old('facility_share_ids')) && in_array($facility->id, old('facility_share_ids')) ? 'selected' : '' }}>{{ $facility->name }}</option>
                        @endforeach
                        @else
                        @foreach ($facilityShare as $facility)
                            <option value="{{ $facility->id }}"
                                    {{ in_array($facility->id, $selectedTags) ? 'selected' : '' }}>{{ $facility->name }}</option>
                        @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div class="form-group bg-default">
                <label for="inputFacilityRoom" class="col-sm-2 control-label"><span class="label label-success">new</span>&nbsp;&nbsp;*Fasilitas Kamar</label>
                <div class="col-sm-10">
                    <select class="form-control chosen-select" multiple id="inputFacilityRoom" name="facility_room_ids[]" tabindex="2">
                        @if ($isStateCreate)
                        @foreach ($facilityRoom as $facility)
                            <option value="{{ $facility->id }}"
                                    {{ !is_null(old('facility_room_ids')) && in_array($facility->id, old('facility_room_ids')) ? 'selected' : '' }}>{{ $facility->name }}</option>
                        @endforeach
                        @else
                        @foreach ($facilityRoom as $facility)
                            <option value="{{ $facility->id }}"
                                    {{ in_array($facility->id, $selectedTags) ? 'selected' : '' }}>{{ $facility->name }}</option>
                        @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div class="form-group bg-default">
                @if ($isStateCreate)
                <label for="inputFacilityBath" class="col-sm-2 control-label"><span class="label label-success">new</span>&nbsp;&nbsp;*Fasilitas Kamar Mandi</label>
                @else
                <label for="inputFacilityBath" class="col-sm-2 control-label"><span class="label label-success">new</span>&nbsp;&nbsp;Fasilitas Kamar Mandi</label>
                @endif 
                <div class="col-sm-10">
                    <select class="form-control chosen-select" multiple id="inputFacilityBath" name="facility_bath_ids[]" tabindex="2">
                        @if ($isStateCreate)
                        @foreach ($facilityBath as $facility)
                            <option value="{{ $facility->id }}"
                                    {{ !is_null(old('facility_bath_ids')) && in_array($facility->id, old('facility_bath_ids')) ? 'selected' : '' }}>{{ $facility->name }}</option>
                        @endforeach
                        @else
                        @foreach ($facilityBath as $facility)
                            <option value="{{ $facility->id }}"
                                    {{ in_array($facility->id, $selectedTags) ? 'selected' : '' }}>{{ $facility->name }}</option>
                        @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div class="form-group bg-default">
                <label for="inputFacilityPark" class="col-sm-2 control-label"><span class="label label-success">new</span>&nbsp;&nbsp;Fasilitas Parkir</label>
                <div class="col-sm-10">
                    <select class="form-control chosen-select" multiple id="inputFacilityPark" name="facility_park_ids[]" tabindex="2">
                        @if ($isStateCreate)
                        @foreach ($facilityPark as $facility)
                            <option value="{{ $facility->id }}"
                                    {{ !is_null(old('facility_park_ids')) && in_array($facility->id, old('facility_park_ids')) ? 'selected' : '' }}>{{ $facility->name }}</option>
                        @endforeach
                        @else
                        @foreach ($facilityPark as $facility)
                            <option value="{{ $facility->id }}"
                                    {{ in_array($facility->id, $selectedTags) ? 'selected' : '' }}>{{ $facility->name }}</option>
                        @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div class="form-group bg-default">
                <label for="inputFacilityOther" class="col-sm-2 control-label"><span class="label label-success">new</span>&nbsp;&nbsp;Fasilitas Lainnya</label>
                <div class="col-sm-10">
                    <select class="form-control chosen-select" multiple id="inputFacilityOther" name="facility_other_ids[]" tabindex="2">
                        @if ($isStateCreate)
                        @foreach ($facilityOther as $facility)
                            <option value="{{ $facility->id }}"
                                    {{ !is_null(old('facility_other_ids')) && in_array($facility->id, old('facility_other_ids')) ? 'selected' : '' }}>{{ $facility->name }}</option>
                        @endforeach
                        @else
                        @foreach ($facilityOther as $facility)
                            <option value="{{ $facility->id }}"
                                    {{ in_array($facility->id, $selectedTags) ? 'selected' : '' }}>{{ $facility->name }}</option>
                        @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div class="form-group bg-default">
                <label for="inputKosRule" class="col-sm-2 control-label"><span class="label label-success">new</span>&nbsp;&nbsp;Peraturan Kos</label>
                <div class="col-sm-10">
                    <select class="form-control chosen-select" multiple id="inputKosRule" name="kos_rule_ids[]" tabindex="2">
                        @if ($isStateCreate)
                        @foreach ($kosRule as $facility)
                            <option value="{{ $facility->id }}"
                                    {{ !is_null(old('kos_rule_ids')) && in_array($facility->id, old('kos_rule_ids')) ? 'selected' : '' }}>{{ $facility->name }}</option>
                        @endforeach
                        @else
                        @foreach ($kosRule as $facility)
                            <option value="{{ $facility->id }}"
                                    {{ in_array($facility->id, $selectedTags) ? 'selected' : '' }}>{{ $facility->name }}</option>
                        @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div id="wrapper">
                @if (! $isStateCreate)
                @foreach($rulePhotos as $photo)
                <input type="hidden" name="kos_rule_photos[][id]" value="{{ $photo['id'] }}">
                @endforeach
                @endif
            </div>
        </form>
        <div class="form-group bg-default">
            <label for="addPhotosFormRule" class="col-sm-2 control-label"><span class="label label-success">new</span>&nbsp;&nbsp;Foto Peraturan Kos</label>
            <div class="col-sm-6">
                @include('admin.contents.stories.partial.upload', ['url' => 'admin/media', 'mediaName' => 'Rule', 'mediaType' => 'term_photo', 'watermarking' => 'true'])
            </div>
            <div class="col-sm-4">
                @if (! $isStateCreate)
                @foreach($rulePhotos as $photo)
                    <div class="col-md-3">
                        {!! link_to(url('admin/media/term',$photo['id']),'Delete', array('class' => 'fa fa-times-circle')) !!}
                        <a href="{{ $photo['url']['large'] }}" data-fancybox>
                            <img class="img-responsive" src="{{ $photo['url']['small'] }}" alt="">
                        </a>
                    </div>
                @endforeach
                @endif
            </div>
        </div>
        <div class="form-group bg-default">
            <label for="addPhoto360" class="col-sm-2 control-label">Foto 360</label>
            <div class="col-sm-6">
                @include('admin.contents.stories.partial.upload', ['url' => 'admin/media', 'mediaName' => 'Round', 'mediaType' => 'round_style_photo', 'watermarking' => 'true'])
            </div>
            <div class="col-sm-4">
                @if (! $isStateCreate && ! is_null($photo360))
                    <div class="col-md-3">
                        <a href="{{ route('admin.media.round.destroy', [$room->id, $photo360['id']]) }}" class="fa fa-times-circle">Delete</a>
                        <a href="{{ $photo360['url']['large'] }}" data-fancybox>
                            <img class="img-responsive" src="{{ $photo360['url']['small'] }}" alt="">
                        </a>
                    </div>
                @endif
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button form="kostForm" type="submit" class="btn btn-primary">Save</button>
                @if (! $isStateCreate)
                <input type="button" class="btn btn-danger" id="callPengelola" value="Telp Pengelola" />
                <input type="button" class="btn btn-warning" id="callOwner" value="Telp Pemilik"/>
                @endif
            </div>
        </div>
    </div>
    </div>
</div>
    
<div class="modal fade" id="addProperty" tabindex="-1" role="dialog" aria-labelledby="addPropertyLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form method="POST" action="{{ url('admin/kost/property') }}">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="addUnitLabelTitle">Tambah Properti</h5>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-form-label">Nama Properti :</label>
                        <input id="input-nama-properti" type="text" placeholder="Nama Properti Baru" class="form-control" name="property_name" required>
                        <small class="help-block">Format Nama Properti tidak boleh berisi karakter: . , ( ) / - " '</small>
                        <p class="text-warning" id="alert-nama-properti"></p>
                    </div>
                    <div class="form-check">
                        <input type="hidden" name="has_multiple_type" value="0">
                        <input type="checkbox" class="form-check-input" value="1" name="has_multiple_type" id="hasMultipleTypeCheck1">
                        <label class="form-check-label" for="hasMultipleTypeCheck1">Apakah Properti ini Mempunyai Lebih Dari 1 Tipe Kamar?</label>
                    </div>
                    <input type="hidden" class="form-control" name="user_id" value="{{ $isStateCreate || is_null($room->owners->first()) ? '' : $room->owners->first()->user_id }}">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" id="submit-tambah-properti" disabled>Add</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="editProperty" tabindex="-1" role="dialog" aria-labelledby="editPropertyLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form id="edit-form" method="POST">
                @method('PUT')
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" name="editPropertyTitle" id="editPropertyTitle">Edit Properti</h5>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-form-label">Nama Properti :</label>
                        <input id="input-edit-properti" type="text" placeholder="Nama Properti Baru" class="form-control" name="property_name" id="property_name" required>
                        <small class="help-block">Format Nama Properti tidak boleh berisi karakter: . , ( ) / - " '</small>
                        <p class="text-warning" id="alert-edit-properti"></p>
                    </div>
                    <div class="form-check">
                        <input type="hidden" name="has_multiple_type" value="0">
                        <input type="checkbox" class="form-check-input" value="1" name="has_multiple_type" id="hasMultipleTypeCheck2">
                        <label class="form-check-label" for="hasMultipleTypeCheck2">Apakah Properti ini Mempunyai Lebih Dari 1 Tipe Kamar?</label>
                    </div>
                    <input type="hidden" class="form-control" name="user_id" value="{{ $isStateCreate || is_null($room->owners->first()) ? '' : $room->owners->first()->user_id }}">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button id="submit-edit-properti" type="submit" class="btn btn-primary" disabled>Update</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>        
    var propertiReq = null;
    var unitTypeReq = null;
    var detailProperiReq = null;

    $(function()
    {       
        $(window).keydown(function(event){
            if(event.keyCode == 13) {
                event.preventDefault();
                return false;
            }
        });

        $("#allow-multiple-type").on('click', function(){
            var hasMultipleType =  $("#has-multiple-type");
            if(hasMultipleType.val() == 1){
                hasMultipleType.val(0);
                $(this).html('&#10005;').removeClass('btn btn-success').addClass('btn btn-danger').attr('style',  'font-size: 10px;');
                $("input[name='unit_type']").prop('disabled', true);
            }else{
                hasMultipleType.val(1);
                $(this).html('&#9989;').removeClass('btn btn-danger').addClass('btn btn-success').attr('style',  'font-size: 10px;');
                $("input[name='unit_type']").prop('disabled', false);
            }
        });

        $("#input-nama-properti").keyup(function(){
            var url = `{!! $checkPropertyNameUrl !!}`;

            $("#submit-tambah-properti").prop('disabled', true);
            $('#alert-nama-properti').attr('class','text-warning').text('Sedang melakukan pengecekkan Nama Properti');
            $(this).closest('.form-group').removeClass('has-feedback has-error').removeClass('has-feedback has-success').addClass('has-feedback has-warning');
            
            stopCheckingPropertyName();

            if(!$(this).val().trim()){
                $(this).closest('.form-group').removeClass('has-feedback has-success').removeClass('has-feedback has-warning').addClass('has-feedback has-error');
                $('#alert-nama-properti').attr('class','text-danger').text("Nama Properti tidak boleh kosong");
                return;
            }

            propertiReq = $.ajax({
                    type: 'GET',
                    url: url,
                    data: {
                        name: $(this).val(),
                    },
                    dataType: 'json',
                    success: (res) => {
                            if (res.success) {                        
                                $(this).closest('.form-group').removeClass('has-feedback has-error').removeClass('has-feedback has-warning').addClass('has-feedback has-success');
                                $('#alert-nama-properti').attr('class','').text('');
                                $("#submit-tambah-properti").prop('disabled', false);
                            }else{
                                $(this).closest('.form-group').removeClass('has-feedback has-success').removeClass('has-feedback has-warning').addClass('has-feedback has-error');
                                $('#alert-nama-properti').attr('class','text-danger').text(res.messages);
                                $("#submit-tambah-properti").prop('disabled', true);
                            }                                
                        },
                    error: (error) => {
                    }
                });                 
        })

        $("#input-edit-properti").keyup(function(){
            var url = `{!! $checkPropertyNameUrl !!}`;
            var idProperty = $('#edit-form').attr('action').split('/').pop();

            $("#submit-edit-properti").prop('disabled', true);
            $('#alert-edit-properti').attr('class','text-warning').text('Sedang melakukan pengecekkan Nama Properti');
            $(this).closest('.form-group').removeClass('has-feedback has-error').removeClass('has-feedback has-success').addClass('has-feedback has-warning');
            
            stopCheckingPropertyName();

            if(!$(this).val().trim()){
                $(this).closest('.form-group').removeClass('has-feedback has-success').removeClass('has-feedback has-warning').addClass('has-feedback has-error');
                $('#alert-edit-properti').attr('class','text-danger').text("Nama Properti tidak boleh kosong");
                return;
            }

            propertiReq = $.ajax({
                    type: 'GET',
                    url: url,
                    data: {
                        name: $(this).val(),
                        property_id: idProperty,
                    },
                    dataType: 'json',
                    success: (res) => {
                            if (res.success) {                        
                                $(this).closest('.form-group').removeClass('has-feedback has-error').removeClass('has-feedback has-warning').addClass('has-feedback has-success');
                                $('#alert-edit-properti').attr('class','').text('');
                                $("#submit-edit-properti").prop('disabled', false);
                            }else{
                                $(this).closest('.form-group').removeClass('has-feedback has-success').removeClass('has-feedback has-warning').addClass('has-feedback has-error');
                                $('#alert-edit-properti').attr('class','text-danger').text(res.messages);
                                $("#submit-edit-properti").prop('disabled', true);
                            }                                
                        },
                    error: (error) => {
                    }
                });                 
        })

        $("#unit-type").keyup(function(){
            var url = `{!! $checkUnitTypeyUrl !!}`;
            var idProperty = $('#propertySelect').val();
            $('#alert-unit-type').attr('class','text-warning').text('Sedang melakukan pengecekkan Tipe Kamar');
            $(this).closest('.form-group').removeClass('has-feedback has-error').removeClass('has-feedback has-success').addClass('has-feedback has-warning');

            stopCheckingUnitType();
            if(!idProperty || idProperty==0){
                $(this).closest('.form-group').removeClass('has-feedback has-success').removeClass('has-feedback has-warning').addClass('has-feedback has-error');
                $('#alert-unit-type').attr('class','text-danger').text("Pilih Nama Properti Terlebih dahulu");
                return;
            }

            if(!$(this).val().trim()){
                $(this).closest('.form-group').removeClass('has-feedback has-success').removeClass('has-feedback has-warning').addClass('has-feedback has-error');
                $('#alert-unit-type').attr('class','text-danger').text("Tipe Kamar tidak boleh kosong");
                return;
            }

            unitTypeReq = $.ajax({
                    type: 'GET',
                    url: url,
                    data: {
                        name: $(this).val(),
                        id_property: idProperty,
                    },
                    dataType: 'json',
                    success: (res) => {
                            if (res.success) {                        
                                $(this).closest('.form-group').removeClass('has-feedback has-error').removeClass('has-feedback has-warning').addClass('has-feedback has-success');
                                $('#alert-unit-type').attr('class','').text('');
                            }else{
                                $(this).closest('.form-group').removeClass('has-feedback has-success').removeClass('has-feedback has-warning').addClass('has-feedback has-error');
                                $('#alert-unit-type').attr('class','text-danger').text(res.messages);
                            }                                
                        },
                    error: (error) => {
                    }
                });                 
        });


        $("select[name='property_id']" ).change(function() {

            stopRegDetailProperty();
            $("#allow-multiple-type").html('&#10005;').removeClass('btn btn-success').addClass('btn btn-danger').attr('style',  'font-size: 10px;').prop('disabled', true);
            $("#unit-type").prop('disabled', true).attr("placeholder", "Loading ....").val('');
            $("#has-multiple-type").val(0);

            var urlPropertyDetail = `{!! url("/admin/room/agent/property/") !!}` + '/' + $(this).val();


            if($(this).val()==0)
            {
                $("#allow-multiple-type").html('&#10005;').removeClass('btn btn-success').addClass('btn btn-danger').attr('style',  'font-size: 10px;').prop('disabled', false);
                $("#has-multiple-type").val(0);
                $("#unit-type").prop('disabled', true).attr("placeholder", "Tipe A, Tipe B, Tipe C");
            } 

            detailProperiReq = $.ajax({
                    type: 'GET',
                    url: urlPropertyDetail,
                    dataType: 'json',
                    success: (res) => {
                        if(res==null){
                            $("#allow-multiple-type").html('&#10005;').removeClass('btn btn-success').addClass('btn btn-danger').attr('style',  'font-size: 10px;').prop('disabled', true);
                            $("#has-multiple-type").val(0);
                            $("#unit-type").prop('disabled', true).attr("placeholder", "Pilih Nama Properti Yang Valid");
                            return;
                        }
                    
                        if(res.has_multiple_type==1){
                            $("#allow-multiple-type").html('&#9989;').removeClass('btn btn-danger').addClass('btn btn-success').attr('style',  'font-size: 10px;').prop('disabled',true);
                            $("#has-multiple-type").val(1);
                            $("#unit-type").prop('disabled', false).attr("placeholder", "Tipe A, Tipe B, Tipe C");
                        
                            }else{
                                $("#allow-multiple-type").html('&#10005;').removeClass('btn btn-success').addClass('btn btn-danger').attr('style',  'font-size: 10px;').prop('disabled', false);
                                $("#has-multiple-type").val(0);
                                $("#unit-type").prop('disabled', true).attr("placeholder", "Tipe A, Tipe B, Tipe C");
                            }                               
                        },
                    error: (error) => {
                    }
                });     
        });

        function stopCheckingPropertyName()
        {
            if(propertiReq != null){
                propertiReq.abort();
            }
        }
        
        function stopCheckingUnitType()
        {
            if(unitTypeReq != null){
                unitTypeReq.abort();
            }
        }

        function stopRegDetailProperty()
	    {
	        if(detailProperiReq != null){
	            detailProperiReq.abort();
	        }
	    }
    });

</script> 

@endsection

@section('style')
    {{ HTML::style('assets/vendor/jquery-chosen/chosen.css') }}
    {{ HTML::style('assets/vendor/jquery-chosen-custom/chosen.css') }}
    <link rel="stylesheet" href="/assets/vendor/summernote/summernote.css?v=05042018">

    <style>
        #chosenForm .chosen-choices {
            border: 1px solid #ccc;
            border-radius: 4px;
            min-height: 34px;
            padding: 6px 12px;
        }
        #chosenForm .form-control-feedback {
            /* To make the feedback icon visible */
            z-index: 100;
        }

        .margin-top {
            margin-top:15px;
        }
    </style>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/dropzone.css">
    <link rel="stylesheet" href="{{ mix_url('dist/vendor/leaflet/leaflet.css') }}">
    <link rel="stylesheet" href="//cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
@stop

@section('script')
    <script src="//cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?libraries=places&language=id&key={{ENV('MIX_GOOGLE_MAPS_JAVASCRIPT_API_KEY')}}"></script>
    <script src="{{ mix_url('dist/vendor/leaflet/leaflet.js') }}"></script>
    
    <script type="text/javascript">
        $('#editProperty').on('show.bs.modal', function () {
            var id = document.getElementById("propertySelect").value
            var modal = $(this)

            @if (! $isStateCreate && ! is_null($properties)) {
                var properties = {!! json_encode($properties->toArray(), JSON_HEX_TAG) !!};
                for (i = 0; i < properties.length; i++) {
                    if (id == properties[i]['id']) {
                        modal.find('.modal-title').text("Edit Properti - " + properties[i]['name'])
                        modal.find('.modal-body #property_name').val(properties[i]['name'])
                        if (1 == properties[i]['has_multiple_type']) {
                            document.getElementById("hasMultipleTypeCheck2").checked = true
                            document.getElementById("hasMultipleTypeCheck2").disabled = true
                        } else {
                            document.getElementById("hasMultipleTypeCheck2").checked = false
                            document.getElementById("hasMultipleTypeCheck2").disabled = false
                        }
                        var path = "{{ url('admin/kost/property') }}" + "/" + id
                        modal.find('#edit-form').attr("action", path);
                        break
                    }
                }
            }
            @endif
        })
    </script> 

    <script type="text/javascript">
        centerPos = {
            lat: parseFloat('{{ $isStateCreate ? (!is_null(old("latitude")) ? old("latitude") : "-7.7858485") : (!is_null($room->latitude) ? $room->latitude : "-7.7858485") }}'),
            lng: parseFloat('{{ $isStateCreate ? (!is_null(old("longitude")) ? old("longitude") : "110.3680087") : (!is_null($room->longitude) ? $room->longitude : "110.3680087") }}')
        };
    
        var map = L.map('map-canvas', {
            // Set latitude and longitude of the map center (required)
            center: centerPos,
            // Set the initial zoom level, values 0-18, where 0 is most zoomed-out (required)
            zoom: 12
        });
    
        var tiles = new L.tileLayer("{{ config('services.osm.host') }}/hot/{z}/{x}/{y}.png").addTo(map);
        markerImage = '{{ asset("assets/icons/kiribawah.png") }}';
    
        marker = new L.Marker(
        centerPos,
        {
            draggable: true
        }
        ).addTo(map);
    
        var autocomplete = new google.maps.places.Autocomplete(document.getElementById('inputGeoName'));
        autocomplete.setFields(['address_components', 'geometry']);
    
        var geocoder = new google.maps.Geocoder();
    
        marker.on('dragend', function(evt) {
        if (window.confirm("If you move the pin, the address data will be refreshed")) {
            geocoder.geocode(
            {'latLng': evt.target.getLatLng()},
            function(results, status) {
                geoCodeResultHandler(results, status);
            }
            );
            return;
        }
        marker.setOpacity(0);
        map.setView({
            lat: <?php echo $isStateCreate ? (is_null(old('latitude')) ? -7.7858485 : old('latitude')) : $room->latitude; ?>,
            lng: <?php echo $isStateCreate ? (is_null(old('longitude')) ? 110.3680087 : old('longitude')) : $room->longitude; ?>
        });
        marker.setLatLng({
            lat: <?php echo $isStateCreate ? (is_null(old('latitude')) ? -7.7858485 : old('latitude')) : $room->latitude; ?>,
            lng: <?php echo $isStateCreate ? (is_null(old('longitude')) ? 110.3680087 : old('longitude')) : $room->longitude; ?>
        });
        marker.setOpacity(1);
        return;
        });
    
        autocomplete.addListener('place_changed', function() {
        var place = autocomplete.getPlace();
        if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place.name + "'");
            return;
        }
    
        marker.setOpacity(0);
        if (window.confirm("If you move the pin, the address data will be refreshed")) {
            map.setView({
            lat: place.geometry.location.lat(),
            lng: place.geometry.location.lng()
            });
            marker.setLatLng({
            lat: place.geometry.location.lat(),
            lng: place.geometry.location.lng()
            });
            marker.setOpacity(1);
            populateAddressInputForm(place);
            return;
        } else {
            marker.setOpacity(0);
            map.setView({
                lat: <?php echo $isStateCreate ? (is_null(old('latitude')) ? -7.7858485 : old('latitude')) : $room->latitude; ?>,
                lng: <?php echo $isStateCreate ? (is_null(old('longitude')) ? 110.3680087 : old('longitude')) : $room->longitude; ?>
            });
            marker.setLatLng({
                lat: <?php echo $isStateCreate ? (is_null(old('latitude')) ? -7.7858485 : old('latitude')) : $room->latitude; ?>,
                lng: <?php echo $isStateCreate ? (is_null(old('longitude')) ? 110.3680087 : old('longitude')) : $room->longitude; ?>
            });
            marker.setOpacity(1);
            $('#inputGeoName').val('');
            return;
        }
        });
    
        @if (
            ! $isStateCreate &&
            $room->latitude !== null &&
            $room->longitude !== null &&
            (
                empty($room->area_subdistrict) ||
                empty($room->area_city) ||
                empty($room->area_big)
            )
        )
        geocoder.geocode(
            {
            'latLng': new google.maps.LatLng(<?php echo $room->latitude; ?>, <?php echo $room->longitude; ?>)
            },
            geoCodeResultHandler
        );
        @endif
    
        $('#inputGeoName').keypress(function(e){
        if ( e.which == 13 ) e.preventDefault();
        });
    
        function geoCodeResultHandler(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            if (results[0] != undefined) {
            return populateAddressInputForm(results[0])
            }
            x.innerHTML = "address not found";
            return;
        }
        x.innerHTML = "Geocoder failed due to: " + status;
        return;
        }
    
        function populateAddressInputForm(geoplace) {
        var lat = geoplace.geometry.location.lat();
        var lng = geoplace.geometry.location.lng();
    
        if (geoplace.formatted_address == undefined) {
            $('#inputGeoName').val(geoplace.address_components.flatMap(
            function (address) {
                return address.long_name;
            }
            ).join(' '));
        } else {
            $('#inputGeoName').val(geoplace.formatted_address);
        }
        $('#inputLatitude').val(lat);
        $('#inputLongitude').val(lng);
        $('#inputCoordinate').val(lat + ',' + lng);
    
        setDistrictAndCity(geoplace);
        return;
        }
    
        function setDistrictAndCity(geoplace) {
            geoplace.address_components.forEach(function(component) {
                if ($.inArray("administrative_area_level_1", component.types) != -1) {
                    var provinceName = sanitizeRegionMapping(component.long_name);
                    var originProvinceName = $('#inputAreaProvince').val();
                    $('#inputAreaProvince').val(provinceName);

                    if (originProvinceName != provinceName) {
                        $('#inputAreaProvince').prop("disabled", true);
                        $.get('/admin/owner/data/kos/address/city', {province_name: provinceName})
                        .done(function (data, textStatus) {
                            $('#city-list').children().remove();
                            data.data.forEach(element => $('#city-list').append(`<option value="${element.name}"`))
                        }).always(function () {
                            $('#inputAreaProvince').prop( "disabled", false );
                        });
                    }
                }

                if ($.inArray("administrative_area_level_2", component.types) != -1) {
                    var cityName = sanitizeRegionMapping(component.long_name);
                    var originCityName = $('#inputAreaCity').val();
                    $('#inputAreaCity').val(cityName);

                    if (originCityName != cityName) {
                        $('#inputAreaCity').prop( "disabled", true );
                        $.get('/admin/owner/data/kos/address/subdistrict', {city: cityName})
                        .done(function (data) {
                            $('#subdistrict-list').children().remove();
                            data.data.forEach(element => $('#subdistrict-list').append(`<option value="${element.name}"`))
                        })
                        .always(function () {
                            $('#inputAreaCity').prop( "disabled", false );
                        });
                    }
                }

                if ($.inArray("administrative_area_level_3", component.types) != -1) {
                    $('#inputSubdistrict').val(sanitizeRegionMapping(component.long_name));
                }
            });
        }
    
        function sanitizeRegionMapping(label) {
        return label
            .replace('Daerah Khusus Ibukota', 'DKI')
            .replace('Daerah Istimewa', 'DI')
            .replace(/Kabupatén|Kab\.|City/i, '')
            .replace(/Kec\.|Sub-District/i, '')
            .replace('Jkt', 'Jakarta')
            .replace('Tim.', 'Timur')
            .replace('Sel.', 'Selatan')
            .replace('Bar.', 'Barat')
            .replace('Tj.', 'Tanjung')
            .replace('Tlk.', 'Teluk')
            .replace('Bks', 'Bekasi')
            .replace('Gn.', 'Gunung')
            .replace('Kb.', 'Kebon')
            .replace('Cemp.', 'Cempaka')
            .replace('Tanahabang', 'Tanah Abang')
            .replace('Kby.', 'Kebayoran')
            .replace('Prpt.', 'Prapatan')
            .replace('Ps.', 'Pasar')
            .replace('Setia Budi', 'Setiabudi')
            .replace('Durensawit', 'Duren Sawit')
            .replace('Kramat Jati', 'Kramatjati')
            .replace('Pondokgede', 'Pondok Gede')
            .replace('Klp.', 'Kelapa')
            .replace('Barar', 'Barat')
            .replace('SBY', 'Surabaya')
            .replace('South Jakarta City', 'Jakarta Selatan')
            .replace('South Sulawesi', 'Sulawesi Selatan')
            .replace('South Sumatra', 'Sumatra Selatan')
            .replace('South Tangerang City', 'Tangerang Selatan')
            .replace('Pd.', 'Pondok')
            .replace('West Java', 'Jawa Barat')
            .replace('Slem.', 'Sleman')
            .trim();
        }
    
        $('#cards').on('click', '.btn-delete',function () {
            $(this).closest('.card-item').remove();
        });
    </script>
    <script>
    function clickSuggestion(idx) {
            key = idx;
            val = $('.suggestion-item-' + idx).text();
            el = $('.suggestion-item-' + idx);

            $(el).parent().parent().prev('.input-city-regency').val(val);
        };

        var callSuggestionCity = function(e) {
                $.ajax({
                    type: 'POST',
                    url: '/admin/area-big-mapper/suggestion-city-regency',
                    data: {
                        'params': e.target.value
                    },
                    success: function(data) {
                        $(e.target).next('.dropdown-menu').remove();

                        listOption = '<ul class="dropdown-menu suggestion-list" aria-labelledby="dLabel">'
                        for(idx in data.landings) {
                            listOption += '<li><a href="#" class="suggestion-item suggestion-item-' + idx + '" data-key="' + idx + '" onclick="clickSuggestion(' + idx + ')">' + data.landings[idx] + '</a></li>';
                        }

                        listOption += '</ul>';

                        $(listOption).insertAfter($(e.target));

                    $('.suggestion-list').dropdown();
                    }

                });
            };

        var delay = (function(){
            var timer = 0;
            return function(callback, ms){
                clearTimeout (timer);
                timer = setTimeout(callback, ms);
            };
            })();

        $(document).on('keyup', '.input-city-regency', function(e) {
                delay(function(){
                callSuggestionCity(e)
                }, 1000 );
            });
    </script>

    <script>
        $(document).on("wheel", "input[type=number]", function (e) {
            $(this).blur();
        });
    </script>

    <script src="/assets/vendor/jquery-chosen/chosen.jquery.js"></script>
    <script type="text/javascript">
        var config = {
            '.chosen-select'           : {width: '100%'}
        };
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }
    </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            var max_fields = 10;
            var wrapper = $(".container1");
            var add_button = $(".add_form_field");
        
            var x = '{{ $isStateCreate ? count((array)old('additional_cost')) : count($additionalPrice['additional_cost']) }}';
            $(add_button).click(function(e) {
                e.preventDefault();
                if (x < max_fields) {
                    x++;
                    $(wrapper).append('<div class="row margin-top"><div class="col-sm-5"><input placeholder="Nama Biaya" type="text" class="form-control" name="additional_cost[' +x+ '][name]"></div><div class="col-sm-5"><input placeholder="Jumlah Biaya" type="number" class="form-control" name="additional_cost[' +x+ '][price]"></div><a href="#" class="delete">x</a></div>'); //add input box
                } else {
                    alert('You Reached the limits')
                }
            });
        
            $(wrapper).on("click", ".delete", function(e) {
                e.preventDefault();
                $(this).parent('div').remove();
                x--;
            })

            // address component selection
            $('#inputAreaProvince').change(function (evt) {
                if (evt.target.value.length > 0) {
                    $('#inputAreaProvince').prop( "disabled", true );
                    $('#inputAreaCity').prop( "disabled", true );
                    $('#inputSubdistrict').prop( "disabled", true );
                    $.get('/admin/owner/data/kos/address/city', {province_name: evt.target.value})
                    .done(function (data) {
                        $('#city-list').children().remove();
                        $('#inputAreaCity').val('');
                        $('#inputSubdistrict').val('');
                        data.data.forEach(element => $('#city-list').append(`<option value="${element.name}"`))
                    })
                    .fail(function (jqXHR, textStatus, errorThrown) {
                        alert('Gagal mengambil daftar Kota')
                        console.log({jqXHR, textStatus, errorThrown});
                    })
                    .always(function () {
                        $('#inputAreaProvince').prop( "disabled", false );
                        $('#inputAreaCity').prop( "disabled", false );
                        $('#inputSubdistrict').prop( "disabled", false );
                    });
                }
            });

            $('#inputAreaCity').change(function (evt) {
                if (evt.target.value.length > 0) {
                    $('#inputAreaProvince').prop( "disabled", true );
                    $('#inputAreaCity').prop( "disabled", true );
                    $('#inputSubdistrict').prop( "disabled", true );
                    $.get('/admin/owner/data/kos/address/subdistrict', {city: evt.target.value})
                    .done(function (data) {
                        $('#subdistrict-list').children().remove();
                        $('#inputSubdistrict').val('');
                        data.data.forEach(element => $('#subdistrict-list').append(`<option value="${element.name}"`))
                    })
                    .fail(function (jqXHR, textStatus, errorThrown) {
                        alert('Gagal mengambil data kecamatgan')
                        console.log({jqXHR, textStatus, errorThrown});
                    })
                    .always(function () {
                        $('#inputAreaProvince').prop( "disabled", false );
                        $('#inputAreaCity').prop( "disabled", false );
                        $('#inputSubdistrict').prop( "disabled", false );
                    });
                }
            }); 
        });
    </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/dropzone.js"></script>
    <script>
        Dropzone.autoDiscover = false;

        $('#addPhotosFormRule').dropzone({
            paramName : "media",
            maxFilesize : 6,
            maxFiles: '{{ $isStateCreate ? 5 : (5 - count($rulePhotos)) }}',
            acceptedFiles: '.jpg, .jpeg, .png, .bmp',
            dictDefaultMessage : 'Unggah foto peraturan kos',
            addRemoveLinks: true,
            init: function () {
                this.on("removedfile", function (file) {
                    //console.log(file.id)
                    $.get("/admin/media/term/"+file.id, function(data, status) {
                        console.log("Sukses hapus");
                    });
                });
            },
            success : function (file, response) {
                $('#wrapper').append('<input type="hidden" name="kos_rule_photos[][id]" value="' +response.media.id+ '">');
                file["id"] = response.media.id;
            }
        });
    </script>

    <script>
        $('#addPhotosFormRound').dropzone({
            paramName : "media",
            maxFilesize : 8,
            maxFiles: '{{ $isStateCreate ? 1 : (is_null($photo360) ? 1 : 0) }}',
            acceptedFiles: '.jpg, .jpeg, .png, .bmp',
            dictDefaultMessage : 'Upload photo for 360 photos',
            addRemoveLinks: true,
            init: function () {
                this.on("removedfile", function (file) {
                    //console.log(file.id)
                    $.get("/admin/media/"+file.id+"/delete", function(data, status) {
                        console.log("Sukses hapus");
                    });
                });
            },
            success : function (file, response) {
                $('#wrapper input[name="photo_round_id"]').remove();
                $('#wrapper').append('<input type="hidden" name="photo_round_id" value="' +response.media.id+ '">');
                file["id"] = response.media.id;
            }
        });
    </script>

    <script>
        $('#callOwner').click(function(e) {
            phone = $("#owner_phone").val();
            id_id = $("#id_id").val();
            email = $("#user_login").val();
            kos_name = $("#kos_name").val();

            $.ajax({
                url: '/admin/squarepants/notify?phone='+phone+"&email="+email+"&id="+id_id+"&kos_name="+kos_name,
                type: 'GET',
                dataType: 'json',
                success: function(data) {
                if(data.status == false) {
                    alert("Gagal ngirim nomer ke hp");
                } else {
                    alert("Berhasil ngirim nomer ke hp");
                }
                }
            });
        });

        $('#callPengelola').click(function(e) {
            phone = $("#manager_phone").val();
            id_id = $("#id_id").val();
            email = $("#user_login").val();
            kos_name = $("#kos_name").val();

            $.ajax({
                url: '/admin/squarepants/notify?phone='+phone+"&email="+email+"&id="+id_id+"&kos_name="+kos_name,
                type: 'GET',
                dataType: 'json',
                success: function(data) {
                if(data.status == false) {
                    alert("Gagal ngirim nomer ke hp");
                } else {
                    alert("Berhasil ngirim nomer ke hp");
                }
                }
            });
        });
    </script>
@stop
