@extends('admin.layouts.main')
@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <form action="" class="form-inline" method="get" style="text-align:right;padding:10px;">
                        
                        <input type="text" name="q" class="form-control input-sm"  placeholder="No. HP"  autocomplete="off" value="{{ request()->input('q') }}">
                        
                        <button class="btn btn-primary btn-md" id="buttonSearch"><i class="fa fa-search">&nbsp;</i>Search</button>
                    </form>
                </div>
            </div>

            <table id="tableListTag" class="table table-striped">
                <thead>
                    <tr>
                        <th>No. HP</th>
                        <th>Nama Penginput</th>
                        <th>Nama Kost</th>
                        <th>Tanggal Input</th>
                        <th>Status</th>
                        <th class="table-action-column">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($rewards as $reward)
                        @if(!$reward->room)
                            @php 
                                continue; 
                            @endphp
                        @endif
                        <tr>
                            <td>{{ $reward->phone_number }}</td>
                            <td>{{ $reward->name }}</td>
                            <td>{{ $reward->room->name }}</td>
                            <td>{{ \Carbon\Carbon::parse($reward->created_at)->format('Y-m-d H:i:s') }}</td>
                            <td>{{ $reward->reward_sent == 0 ? 'Belum Terkirim' : 'Sudah Terkirim' }}</td>
                            <td class="table-action-column">
                                @if($reward->reward_sent == 0)
                                    <a href="{{ route('admin.reward.sent', $reward->id) }}" title="Tandai sudah terkirim">
                                        <i class="fa fa-share"></i>
                                    </a>
                                @else
                                    <a href="{{ route('admin.reward.unsent', $reward->id) }}" title="Tandai belum terkirim">
                                        <i class="fa fa-minus-square"></i>
                                    </a>
                                @endif
                            </td>
                        </tr>
                    @endforeach

                </tbody>
                
            </table>
        </div><!-- /.box-body -->

        <div class="box-body no-padding">
            {{ $rewards->appends(Request::except('page'))->links() }}
        </div>
    </div><!-- /.box -->
    <!-- table -->

@endsection