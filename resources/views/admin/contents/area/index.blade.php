@extends('admin.layouts.main')
@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
            <div class="box-tools pull-right">
                <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <a href="{{ URL::route('admin.area.create') }}">
                        <button type="submit" class="btn-add btn btn-primary btn-sm">
                            <i class="fa fa-plus">&nbsp;</i>Add Area</button>
                    </a>
                </div>
            </div>

            <table id="tableListArea" class="table table-striped">
                <thead>
                    <tr>
                        <th width="30px;">No</th>
                        <th>{{ Lang::get('hairclick.page.geography.area.name') }}</th>
                        <th>{{ Lang::get('hairclick.page.geography.area.region') }}</th>
                        <th class="table-action-column" width="100px">{{ Lang::get('hairclick.page.geography.area.action') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($rowsArea as $rowAreaKey => $rowArea)
                    <tr>
                        <td>{{ $rowAreaKey+1 }}</td>
                        <td>{{ $rowArea->name }}</td>
                        <td>{{ $rowArea->region->name }}</td>
                        <td class="table-action-column">
                            <div class="btn-action-group">
                                <a href="{{ URL::route('admin.area.edit', $rowArea->id) }}">
                                    <i class="fa fa-pencil"></i></a>
                                <a href="#" ng-click="showConfirmDelete('Area', {{ $rowArea->id }}, '{{ $rowArea->name }}');">
                                    <i class="fa fa-trash-o"></i></a>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>No</th>
                        <th>{{ Lang::get('hairclick.page.geography.area.name') }}</th>
                        <th>{{ Lang::get('hairclick.page.geography.area.region') }}</th>
                        <th class="table-action-column">{{ Lang::get('hairclick.page.geography.area.action') }}</th>
                    </tr>
                </tfoot>
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
    <!-- table -->

    @include('admin.partials.delete-modal')
@stop
@section('script')
<!-- page script -->
<script type="text/javascript">
    $(function()
    {
        $("#tableListArea").dataTable();
    });
</script>
@stop