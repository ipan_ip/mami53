@extends('admin.layouts.main')

@section('style')
<style>
    .table>tbody>tr>td {
        vertical-align: middle;
    }

    .font-large {
        font-size: 1.5em;
    }

    .font-semi-large {
        font-size: 1.2em;
    }

    .font-grey {
        color: #9E9E9E;
    }

    .dropdown-menu>li>a:hover {
        color: #333
    }

    .label-grey {
        background-color: #CFD8DC;
    }

    [hidden] {
        display: none !important;
    }

    /* Select2 tweak */

    .input-group .select2-container,
    .form-group .select2-container {
        position: relative !important;
        z-index: 2;
        float: left !important;
        width: 100% !important;
        margin-bottom: 0 !important;
        display: table !important;
        table-layout: fixed !important;
    }

    /* Sweetalert */

    .swal2-container {
        z-index: 99999 !important;
    }

    .swal-shown {
        height: auto !important;
    }

    .custom-swal {
        z-index: 10000 !important;
    }

    .custom-wide-swal {
        width: 480px !important;
        font-size: 12px !important;
    }

    /* Custom table row */
    tr.inactive td {
        background-color: #E6C8C8;
    }

    /* Modal tweak */

    .modal-open {
        overflow: hidden;
        position: fixed;
        width: 100%;
    }


    /* Centering the modal */

    .modal {
        text-align: center;
        padding: 0 !important;
    }

    .modal:before {
        content: '';
        display: inline-block;
        height: 100%;
        vertical-align: middle;
        margin-right: -4px;
    }

    .modal-dialog {
        display: inline-block;
        text-align: left;
        vertical-align: middle;
    }

    /* Grid */
    .grid {
        position: relative;
    }

    .item {
        display: block;
        position: absolute;
        width: 100px;
        height: 100px;
        margin: 5px;
        z-index: 1;
        background: #000;
        color: #fff;
    }

    .item.muuri-item-dragging {
        z-index: 3;
    }

    .item.muuri-item-releasing {
        z-index: 2;
    }

    .item.muuri-item-hidden {
        z-index: 0;
    }

    .item-content {
        position: relative;
        width: 100%;
        height: 100%;
    }

    /* Blink effect */
    .blink {
        animation: blink 1s infinite;
    }

    @keyframes blink {
        0% {
            opacity: 1;
        }

        75% {
            opacity: 1;
        }

        76% {
            opacity: 0;
        }

        100% {
            opacity: 0;
        }
    }

    .bootstrap-datetimepicker-widget {
        z-index: 9999 !important;
    }
</style>
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.css">
<link rel="stylesheet" href="//gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css">
<link rel="stylesheet" href="//unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.css">
<link rel="stylesheet"
    href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" />
<link rel="stylesheet" href="//cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
@endsection

@section('content')
<!-- table -->
<div class="box box-default">
    <!-- Box Header -->
    <div class="box-header">
        @if(!empty($error))
        <div class="alert alert-danger"> {{ $error }}</div>
        @endif
        <div class="col-md-6 text-center">
            <h3 class="box-title"><i class="fa fa-percent"></i> {{ $boxTitle }}</h3>
        </div>
        <!-- Add button -->
        <div class="col-md-6">
            <div class="form-horizontal pull-right" style="padding-top: 10px;">
                <button class="btn btn-success actions" style="margin-right: 10px;" onclick="actionButton('create')">
                    <i class="fa fa-plus"></i>&nbsp;&nbsp;Add New Data
                </button>
            </div>
        </div>
    </div>
    <!-- /.box-header -->
    <!-- box-body -->
    <div class="box-body">
        <!-- Table -->
        @include('admin.contents.flash-sale.partials.table')
        <!-- table -->
    </div>
</div><!-- /.box -->

@include('admin.contents.flash-sale.partials.modal')

@endsection

@section('script')
<script src="//cdn.jsdelivr.net/npm/holderjs@2.9.6/holder.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.js"></script>
<script src="//gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.js"></script>
<script src="//unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment-with-locales.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js">
</script>
<script src="//cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
<script type="text/javascript">
    // Helper variable
    let _editing, _data, _modalTitle;

    function triggerAlert(type, message) {
        Swal.fire({
            type: type,
            customClass: {
                container: 'custom-swal'
            },
            html: message
        });
    }

    function triggerLoading(message) {
        Swal.fire({
            title: message,
            customClass: {
                container: 'custom-swal'
            },
            allowEscapeKey: false,
            allowOutsideClick: false,
            onOpen: () => {
                swal.showLoading();
            }
        });
    }

    function ajaxLoadData(params) {
        let url = '/admin/flash-sale/data';

        $.get(url + '?' + $.param(params.data)).then(function (res) {
            params.success(res)
        })
    }

    function queryParams(params) {
        let newParams = {};

        newParams.search = params.searchText;
        newParams.limit = params.pageSize;
        newParams.page = params.pageNumber;
        
        return newParams
    }

    function bannerFormatter(value, row) {
        if (value !== null) {
            return '<a href="' + value.real + '" data-fancybox data-caption="' + row.name + '">'+
                        '<img width="180" class="img-thumbnail" src="' + value.medium + '" alt="' + row.name + '" data-src="holder.js/180x100?text=Banner \n Unavailable">'
                    + '</a>' ;
        } else {
            return '<span class="label label-grey">Banner Not Set</span>';
        }
    }

    function nameFormatter(value, row) {
        let html = '<span class="font-semi-large"><strong>' + value + '</strong></span>';

        if (row.is_running) {
            html += ' <span class="label label-success blink" style="vertical-align:text-top!important;">CURRENTLY RUNNING..</span>';
        }

        html += '<br/>' +
            '<small>Total </small> <strong>' + row.areas_count + '</strong> Area(s)' +
            '<small> and</small> <strong>' + row.landings_count + '</strong> Landing(s)<br/>' +
            '<small>Created At</small> <strong>' + row.created_at + '</strong><br/>' +
            '<small>Created By</small> <strong>' + row.created_by + '</strong>';

        return html;
    }

    function statusFormatter(value) {
        if (!value) {
            return "<span class='label label-default'>INACTIVE</span>";
        } else {
            return "<span class='label label-success'>ACTIVATED</span>";
        }
    }

    function startFormatter(value, row, index) {
        if (value === 'Not Set')
            return '<span class="font-grey">' + value + '</span>';
        else
            return '<span class="font-semi-large">' + value + '</span><br/>' + row.start_time;
    }

    function endFormatter(value, row, index) {
        if (value === 'Not Set')
            return '<span class="font-grey">' + value + '</span>';
        else
            return '<span class="font-semi-large">' + value + '</span><br/>' + row.end_time;
    }

    // Action button compilator
    function actionFormatter(value, row, index) {
        let html = '<div class="btn-group">'+
                    '<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" data-boundary="window">'+
                        '<i class="fa fa-caret-down"></i> Actions'+
                    '</button>'+
                    '<ul class="dropdown-menu pull-right action-button">';

        html += "<li><a href='#' onclick='actionButton(\"history\", " + JSON.stringify(row) + ")'><i class='fa fa-history'></i> Show History</a></li>";

        html += "<li><a href='#' onclick='actionButton(\"update\", " + JSON.stringify(row) + ")'><i class='fa fa-edit'></i> View/Update Area</a></li>";

        if (!row.is_active) {
            html += "<li><a href='#' onclick='actionButton(\"activate\", " + JSON.stringify(row) + ")'><i class='fa fa-arrow-up'></i> Activate</a></li>";
        } else {
            html += "<li><a href='#' onclick='actionButton(\"deactivate\", " + JSON.stringify(row) + ")'><i class='fa fa-arrow-down'></i> Deactivate</a></li>";
        }

        if (row.is_active && row.is_running) {
            html += "<li role=\"presentation\" class=\"divider\"></li>"+
            "<li><a href='#' onclick='actionButton(\"recalculate\", " + JSON.stringify(row) + ")'><i class='fa fa-refresh'></i> Recalculate Scores</a></li>";
        }

        html += "<li role=\"presentation\" class=\"divider\"></li>"+
            "<li><a href='#' onclick='actionButton(\"delete\", " + JSON.stringify(row) + ")'><i class='fa fa-trash'></i> Remove</a></li>";

        html += '</ul>'+
            '</div>';

        return html;
    }

    // Action buttons click event
    function actionButton(action, data) {

        // Creating data
        if (action === 'create') {
            _editing = false;
            _modalTitle = "New Promo Ngebut Data";

            Swal.fire({
                title: 'Enter The Codename',
                html: "<div class='callout callout-warning text-left'>" +
                    "<li>Codename is unique characters which is used to identify each Promo</li>" +
                    "<li>The codename should not being used by another Promo data</li>" +
                    "<li>Only alphanumeric allowed! Eg. <strong>JAKARTA0805</strong></li>" +
                    "</div>",
                input: 'text',
                confirmButtonText: 'Check Codename',
                showCancelButton: true,
                customClass: 'custom-wide-swal',
                showLoaderOnConfirm: true,
                preConfirm: (val) => {
                    if (val === '') {
                        Swal.showValidationMessage("Type the codename first!");
                        return;
                    }

                    if (val.length > 20) {
                        Swal.showValidationMessage("Maximum 20 characters!");
                        return;
                    }

                    if (val) {
                        return $.ajax({
                            type: 'POST',
                            url: "/admin/flash-sale/verify",
                            dataType: 'json',
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            data: {
                                'name': val
                            },
                            success: (response) => {
                                if (response.status === true) {
                                    return response;
                                } else {
                                    _this.error;
                                }
                            },
                            error: (error) => {
                                Swal.showValidationMessage(error.responseJSON.meta.message);
                                Swal.hideLoading();
                            }
                        });
                    }
                },
                allowOutsideClick: () => !Swal.isLoading()

            }).then((result) => {
                if (!result.dismiss) {
                    if (result.value.status === true) {
                        triggerLoading('Creating new Promo data..');

                        _processedName = result.value.data.name;

                        // Store the data
                        return $.ajax({
                            type: 'POST',
                            url: "/admin/flash-sale",
                            dataType: 'json',
                            headers: {
                                'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr('content')
                            },
                            data: {
                                'name': _processedName
                            },
                            success: (response) => {
                                Swal.fire({
                                    type: 'success',
                                    title: 'Promo "' + result.value.data.name + '" successfully created!',
                                    html: '<h5>You may now add some Areas by using button<br/><br />' +
                                        '<span class="label label-default"><i class="fa fa-thumb-tack"></i> View/Update Areas</span></h5>',
                                    onClose: () => {
                                        $('#table').bootstrapTable('refreshOptions', {});
                                    }
                                });
                            },
                            error: (error) => {
                                Swal.showValidationMessage(error.responseJSON.meta.message);
                                Swal.hideLoading();
                            }
                        });
                    }
                }
            });
        }

        // Show history
        else if (action === 'history') {
            _data = data;
            _modalTitle = "Promo <strong>" + _data.name + "</strong> History";
            
            $('#modal').modal('show');
        }

        // View / update areas
        else if (action === 'update') {
            _editing = true;
            _data = data;
            _modalTitle = "Promo <strong>" + _data.name + "</strong>";

            window.location.replace('/admin/flash-sale/update/' + _data.name.toLowerCase());
        }

        // Activate Data
        else if (action === 'activate') {
            let title, html, btnText, url, type, payload;

            _data = data;

            if (_data.areas_count < 1) {
                triggerAlert('error', "<h4>Please add some areas first!</h4>" +
                    "<h5>You may do it by clicking button<br/><br />" +
                    "<span class='label label-default'><i class='fa fa-thumb-tack'></i> View/Update Areas</span></h5>");
                return;
            }

            type = 'question';
            title = 'Activate Promo ' + data.name + '?';
            html = null;
            btnText = 'Activate';
            url = "/admin/flash-sale/activate";
            payload = {
                id: _data.id
            };

            Swal.fire({
                title: 'Please set scheduled period first',
                html: '<br/><br/><div class="row">'+
                            '<div class="col-md-6">'+
                                '<div class="form-group">'+
                                    '<label>Start Time</label>'+
                                    '<input type="text" class="form-control" id="start"/>'+
                                '</div>'+
                            '</div>'+
                            '<div class="col-md-6">'+
                                '<div class="form-group">'+
                                    '<label>End Time</label>'+
                                    '<input type="text" class="form-control" id="end"/>'+
                                '</div>'+
                            '</div>'+
                        '</div>',
                customClass: 'custom-wide-swal',
                showLoaderOnConfirm: true,
                showCancelButton: true,
                confirmButtonText: 'Set',
                onOpen: function() {
                    $('#start').datetimepicker({
                        locale: 'id',
                        format: 'DD/MM/YYYY H:mm:ss',
                        minDate: moment().add('d', 1).toDate(),
                        widgetPositioning: {
                            horizontal: 'auto',
                            vertical: 'top'
                        }
                    });
                    $('#end').datetimepicker({
                        locale: 'id',
                        format: 'DD/MM/YYYY H:mm:ss',
                        minDate: moment().add('d', 2).toDate(),
                        useCurrent: false,
                        widgetPositioning: {
                            horizontal: 'auto',
                            vertical: 'top'
                        }
                    });

                    $("#start").on("dp.change", function (e) {
                        $('#end').data("DateTimePicker").minDate(e.date);
                    });

                    $("#end").on("dp.change", function (e) {
                        $('#start').data("DateTimePicker").maxDate(e.date);
                    });
                },
                preConfirm: () => {
                    if ($('#end').val() === '') {
                        return Swal.showValidationMessage('End time is required!');
                    }

                    return $.ajax({
                        type: 'POST',
                        url: '/admin/flash-sale/activate',
                        dataType: 'json',
                        headers: {
                            'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr("content")
                        },
                        data: {
                            'id': _data.id,
                            'start_time': $('#start').val(),
                            'end_time': $('#end').val()
                        },
                        success: (response) => {
                            return response;
                        },
                        error: (error) => {
                            triggerAlert('error', '<h5>' + error.responseJSON.meta.message + '</h5>');
                        }
                    });
                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                if (!result.dismiss) {
                    if (result.value.status) {
                        Swal.fire({
                            type: 'success',
                            title: result.value.title,
                            text: result.value.message,
                            onClose: () => {
                                $('#table').bootstrapTable('refreshOptions', {});
                            }
                        });
                    } else {
                        triggerAlert('error', result.value.meta.message);
                    }
                }
            })
        }

        // Deactivate Data
        else if (action === 'deactivate') {
            let title, html, btnText, url, type, payload;

            _data = data;

            if (_data.is_running === true) {
                title = 'Promo "' + data.name + '" is currently running!';
                html = 'Start time and end time data will be removed.' +
                    '<br/>Are you sure to deactivate it?';
            } else {
                if (_data.start_time !== null) {
                    title = 'Are you sure to deactivate Promo "' + data.name + '"?';
                    html = 'Start time and end time data will be removed';
                } else {
                    title = 'Are you sure to deactivate Promo "' + data.name + '"?';
                    html = '';
                }
            }

            type = 'warning';
            btnText = 'Deactivate';
            url = "/admin/flash-sale/deactivate";
            payload = {
                id: _data.id
            };

            Swal.fire({
                title: title,
                html: html,
                type: 'question',
                showCancelButton: true,
                cancelButtonClass: 'btn btn-default',
                showLoaderOnConfirm: true,
                confirmButtonText: btnText,
                preConfirm: (val) => {
                    return $.ajax({
                        type: 'POST',
                        url: url,
                        dataType: 'json',
                        headers: {
                            'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr("content")
                        },
                        data: payload,
                        success: (response) => {
                            return response;
                        },
                        error: (error) => {
                            triggerAlert('error', '<h5>' + error.responseJSON.meta.message + '</h5>');
                        }
                    });
                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                var res = result.value;

                if (res.status === true) {
                    Swal.fire({
                        type: 'success',
                        title: "Promo data successfully deactivated",
                        text: "Table will be refreshed after you clicked OK",
                        onClose: () => {
                            $('#table').bootstrapTable('refreshOptions', {});
                        }
                    });
                } else {
                    triggerAlert('error', res.meta.message);
                }
            })
        }

        // Recalculate all implied Kos scores
        else if (action == 'recalculate') {
            _data = data;

            Swal.fire({
                title: 'Recalculate Kos Scores For Flash Sale ' + _data.name + '?',
                html: "<h5>This will recalculate sorting score of all related Kos.<br/><br />Are you sure to continue?</h5>",
                type: 'warning',
                confirmButtonColor: '#f56954',
                showCancelButton: true,
                confirmButtonText: 'Yes, continue!',
                preConfirm: (val) => {
                    // Show loader
                    triggerLoading('Processing..');

                    return $.ajax({
                        type: 'POST',
                        url: "/admin/flash-sale/recalculate",
                        dataType: 'json',
                        headers: {
                            'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr("content")
                        },
                        data: {
                            'id': _data.id,
                        },
                        done: () => {
                            if (Swal.isLoading) Swal.close();
                        },
                        success: (response) => {
                            return response;
                        },
                        error: (error) => {
                            triggerAlert('error', '<h5>' + error.responseJSON.meta.message + '</h5>');
                        }
                    });
                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                var res = result.value;

                if (res.status === true) {
                    Swal.fire({
                        type: 'success',
                        title: "Calculation is now under process..",
                        html: "<h5><strong>Please be notice!</strong><br>" +
                            "All Kos LPL scores would be affected in up to 24 hours<br>" +
                            "<small>Depends on how many Kos registered under this Flash Sale promo</small></h5>",
                        onClose: () => {
                            $('#table').bootstrapTable('refreshOptions', {});
                        }
                    });
                } else {
                    triggerAlert('error', res.meta.message);
                }
            })
        }

        // Deleting data
        else if (action == 'delete') {
            _data = data;

            Swal.fire({
                title: 'Remove Promo ' + _data.name + '?',
                html: "<h5>This will also remove all existing related area and landing datas.<br/><br />Are you sure to remove it?</h5>",
                type: 'warning',
                confirmButtonColor: '#f56954',
                showCancelButton: true,
                showLoaderOnConfirm: true,
                confirmButtonText: 'Yes, remove it!',
                preConfirm: (val) => {
                    return $.ajax({
                        type: 'POST',
                        url: "/admin/flash-sale/remove",
                        dataType: 'json',
                        headers: {
                            'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr("content")
                        },
                        data: {
                            'id': _data.id,
                        },
                        success: (response) => {
                            return response;
                        },
                        error: (error) => {
                            triggerAlert('error', '<h5>' + error.responseJSON.meta.message + '</h5>');
                        }
                    });
                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                var res = result.value;

                if (res.status === true) {
                    Swal.fire({
                        type: 'success',
                        title: "Promo data successfully removed",
                        text: "Table will be refreshed after you clicked OK",
                        onClose: () => {
                            $('#table').bootstrapTable('refreshOptions', {});
                        }
                    });
                } else {
                    triggerAlert('error', res.meta.message);
                }
            })
        }
    }

    $(function()
    {
        $('#table').on('load-success.bs.table', (data) => {
            Holder.addTheme("img-thumbnail", {
                background: '#EEE',
                foreground: '#AAA',
                size: 10,
                fontweight: 'normal'
            });
        })

        // Action button tweak
        var dropdownMenu;                                     
        $(window).on('show.bs.dropdown', function(e) {        
            dropdownMenu = $(e.target).find('.action-button');
            $('body').append(dropdownMenu.detach());          
            dropdownMenu.css('display', 'block');             
            dropdownMenu.position({                           
                'my': 'left top',                            
                'at': 'left bottom',                         
                'of': $(e.relatedTarget)                      
            })                                                
        });                                                   
        $(window).on('hide.bs.dropdown', function(e) {        
            $(e.target).append(dropdownMenu.detach());        
            dropdownMenu.hide();                              
        });             

        // :: Modal Events ::
        let modal = $('#modal');
        modal
            .on("show.bs.modal", (e) => {
                triggerLoading('Fetching history data..');

                modal.data('bs.modal').options.keyboard = false;
                modal.data('bs.modal').options.backdrop = 'static';

                const modalTitle = $('#modal-title');
                const modalContainer = $('#modal-container');

                modalTitle.html(_modalTitle);
                modalContainer.empty();

                function compileTable(data) {
                    let html, icon;

                    data.forEach(item => {
                        if (item.event.type == 'remove') {
                            html += '<tr class="bg-warning">';
                            icon = '<i class="fa fa-exclamation-circle">';
                        } else if (item.event.type == 'update') {
                            html += '<tr">';
                            icon = '<i class="fa fa-edit">';
                        } else {
                            html += '<tr>';
                            icon = '';
                        }

                        html += '<td class="text-center">' + item.date + '</td><td class="text-center">' + icon + '</td><td>' + item.event.message + '</td></tr>';
                    });

                    modalContainer.html(html);
                }

                $.ajax({
                    type: 'GET',
                    url: '/admin/flash-sale/history?id=' + _data.id,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: response => {
                        compileTable(response.data)
                    },
                    error: error => {
                        $('#modal').modal('hide');
                        triggerAlert('error', '<h3>Oops!</h3><h5>Something wrong happened.<br/>Please try again in 10 minutes.</h5>');
                    },
                    complete: function () {
                        if (Swal.isLoading()) Swal.close();
                    }
                });
            })
    });
</script>
@stop