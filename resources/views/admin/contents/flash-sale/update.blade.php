@extends('admin.layouts.main')

@section('style')
<style>
    .table>tbody>tr>td {
        vertical-align: middle;
    }

    .font-large {
        font-size: 1.5em;
    }

    .font-semi-large {
        font-size: 1.2em;
    }

    .font-grey {
        color: #9E9E9E;
    }

    .dropdown-menu>li>a:hover {
        color: #333
    }

    .label-grey {
        background-color: #CFD8DC;
    }

    [hidden] {
        display: none !important;
    }

    /* Select2 tweak */

    .input-group .select2-container,
    .form-group .select2-container {
        position: relative !important;
        z-index: 2;
        float: left !important;
        width: 100% !important;
        margin-bottom: 0 !important;
        display: table !important;
        table-layout: fixed !important;
    }

    /* Sweetalert */

    .swal2-container {
        z-index: 99999 !important;
    }

    .swal-shown {
        height: auto !important;
    }

    .custom-swal {
        z-index: 10000 !important;
    }

    .custom-wide-swal {
        width: 480px !important;
        font-size: 12px !important;
    }

    /* Custom table row */
    tr.inactive td {
        background-color: #E6C8C8;
    }

    /* Modal tweak */

    .modal-open {
        overflow: hidden;
        position: fixed;
        width: 100%;
    }


    /* Centering the modal */

    .modal {
        text-align: center;
        padding: 0 !important;
    }

    .modal:before {
        content: '';
        display: inline-block;
        height: 100%;
        vertical-align: middle;
        margin-right: -4px;
    }

    .modal-dialog {
        display: inline-block;
        text-align: left;
        vertical-align: middle;
    }

    /* Grid */
    .grid {
        position: relative;
    }

    .item {
        display: block;
        position: absolute;
        width: 100px;
        height: 100px;
        margin: 5px;
        z-index: 1;
        background: #000;
        color: #fff;
    }

    .item.muuri-item-dragging {
        z-index: 3;
    }

    .item.muuri-item-releasing {
        z-index: 2;
    }

    .item.muuri-item-hidden {
        z-index: 0;
    }

    .item-content {
        position: relative;
        width: 100%;
        height: 100%;
    }

    /* Blink effect */
    .blink {
        animation: blink 1s infinite;
    }

    @keyframes blink {
        0% {
            opacity: 1;
        }

        75% {
            opacity: 1;
        }

        76% {
            opacity: 0;
        }

        100% {
            opacity: 0;
        }
    }
</style>
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" />
<link href="//cdn.jsdelivr.net/npm/select2-bootstrap-theme@0.1.0-beta.10/dist/select2-bootstrap.min.css"
    rel="stylesheet" />
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.css">
@endsection

@section('content')
<div class="box box-warning">
    <div class="box-header">
        <div class="col-md-6 text-center">
            <h3 class="box-title"><i class="fa fa-edit"></i> {{ $boxTitle }}</h3>
        </div>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-md-7">
                <label>Areas & Landings</label><br />
                <div class="panel panel-default">
                    <!-- HIDDEN DYNAMIC ELEMENT TO CLONE -->
                    <div class="form-group dynamic-element" style="display:none">
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label>Area</label>
                                <input type="text" name="areas[]" class="form-control name-input" value=""
                                    placeholder="Enter area name">
                                <input type="hidden" class="name-input-id" value="">
                            </div>
                            <div class="col-md-12 form-group">
                                <label>Landings</label>
                                <select class="form-control landing-selector" name="landings[]"
                                    multiple="multiple"></select>
                            </div>
                            <div class="col-md-12 form-group">
                                <button type="button" class="btn btn-danger btn-xs delete">
                                    <i class="fa fa-trash"></i> Remove
                                </button>
                            </div>
                        </div>
                    </div>
                    <!-- END OF HIDDEN ELEMENT -->
                    <div class="panel-body" id="dynamic-div"></div>
                    <div class="panel-footer">
                        <button type="button" class="btn btn-success" id="add">
                            <i class="fa fa-plus"></i>&nbsp;&nbsp;Add More Area
                        </button>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="row">
                    <div class="form-group col-md-12">
                        <label>Banner Image</label><br />
                        <form id="imageSection" action="/admin/media" method="POST" class="dropzone">
                            {{ csrf_field() }}
                            <input type="hidden" name="media_type" value="flash_sale_photo">
                        </form>
                        <small class="text-muted">
                            <h4>IMPORTANT!</h4>
                            <h5>Make sure the photo <strong>does not exceed 2 MB</strong> on size for best display
                                results!</h5>
                        </small>
                        <input type="hidden" id="photoId" name="photo_id" value="" data-filename="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="box-footer">
        <div class="text-right">
            <button type="button" class="btn btn-default" id="cancel" style="margin-right: 20px;">
                <i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Back
            </button>
            <button type="button" class="btn btn-primary" id="history">
                <i class="fa fa-history"></i>&nbsp;&nbsp;Show History
            </button>
            <button type="button" class="btn btn-success" id="submit">
                Update Data&nbsp;&nbsp;<i class="fa fa-floppy-o"></i>
            </button>
        </div>
    </div>
</div>

@include('admin.contents.flash-sale.partials.modal')

@endsection

@section('script')
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/dropzone@5.5.1/dist/min/dropzone.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.js"></script>
<script type="text/javascript">
    Dropzone.autoDiscover = false;

    function triggerAlert(type, message) {
        Swal.fire({
            type: type,
            customClass: {
                container: 'custom-swal'
            },
            html: message
        });
    }

    function triggerLoading(message) {
        Swal.fire({
            title: message,
            customClass: {
                container: 'custom-swal'
            },
            allowEscapeKey: false,
            allowOutsideClick: false,
            onOpen: () => {
                swal.showLoading();
            }
        });
    }

    // Ajax caller
    function ajaxCall(type, title, html, method, url, payload, input, btnText, btnClass, needRefresh = true) {
        Swal.fire({
            type: type,
            title: title,
            html: html,
            buttonsStyling: btnClass == null,
            confirmButtonText: btnText,
            confirmButtonClass: btnClass,
            showCancelButton: true,
            cancelButtonClass: (btnClass !== null) ? 'btn btn-default' : null,
            customClass: 'custom-swal',
            showLoaderOnConfirm: true,
            input: input,
            preConfirm: (response) => {
                if (input !== null) {
                    let number = response;
                    if (!response) {
                        return Swal.showValidationMessage('Please provide a valid phone number!');
                    }

                    payload = {
                        "template_id": payload.id,
                        "number": response
                    };
                }

                return $.ajax({
                    type: method,
                    url: url,
                    data: payload,
                    dataType: 'json',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: (response) => {
                        if (response.success === false) {
                            Swal.close();
                            triggerAlert('error', response.meta.message);
                        }


                    },
                    error: (error) => {
                        Swal.close();
                        triggerAlert('error', JSON.stringify(error));
                    }
                });
            },
            allowOutsideClick: () => !Swal.isLoading()
        }).then((result) => {
            if (!result.dismiss != 'cancel') {
                if (result.value.status === true) {
                    // check if slide page modal is opened
                    if ($('#page-modal').is(':visible')) {
                        $('#page-modal').modal('toggle');
                    }

                    Swal.fire({
                        type: 'success',
                        title: result.value.title,
                        html: result.value.message,
                        onClose: () => {
                            if (needRefresh) {
                                $('#table').bootstrapTable('refresh');
                            }
                        }
                    });
                } else {
                    Swal.fire({
                        type: 'error',
                        title: "Failed!",
                        html: result.value.message
                    });
                }
            }
        });
    }
    
    // DOM
    $(function () {
        let _data = @json($data);

        function attach_delete(){
            $('.delete').off();
            $('.delete').click(function(){
                $(this).closest('.dynamic-element').remove();
            });
        }

        function initiateLandingSelector(element) {
            element.select2({
                theme: "bootstrap",
                placeholder: 'Type landing name to search..',
                language: {
                    searching: function() {
                        return "Fetching landing datas...";
                    },
                    inputTooShort: function() {
                        return 'Type landing name to search..';
                    },
                    noResults: function(){
                        return "Landing data not found!";
                    }
                },
                ajax: {
                    url: '/admin/flash-sale/landings',
                    data: function (params) {
                        return {
                            search: params.term,
                            page: params.page || 1
                        }
                    },
                    processResults: function (data, params) {
                        var page = params.page || 1;
                        if (data.data.length < 1) var total_count = 0;
                        else var total_count = data.data[0].total_count;
                        return {
                            results: $.map(data.data, item => { 
                                return {
                                    id: item.id, 
                                    text: item.heading_1
                                }
                            }),
                            pagination: {
                                more: (page * 10) <= total_count
                            }
                        };
                    },
                    cache: true
                },
                minimumInputLength: 3,
                closeOnSelect: false,
                multiple:true
            });
        }
        
        function generateFields() {
            $('.dynamic-element').first().clone().appendTo('#dynamic-div').show();
            attach_delete();

            let targetSelector = $('#dynamic-div').children().last().find('.landing-selector');
            initiateLandingSelector(targetSelector);
        }

        function generateFieldsWithData(data) {
            $('.dynamic-element').first().clone().appendTo('#dynamic-div').show();
            attach_delete();
            
            let targetInput = $('#dynamic-div').children().last().find('.name-input');
            targetInput.val(data.name);
            let targetInputId = $('#dynamic-div').children().last().find('.name-input-id');
            targetInputId.val(data.id);

            let targetSelector = $('#dynamic-div').children().last().find('.landing-selector');
            initiateLandingSelector(targetSelector);

            // Assign selector values
            $.each(data.landings, (index, data) => {
                var option = new Option(data.name, data.id, true, true);
                targetSelector.append(option).trigger('change');
            });
        }

        function getFormData() {
            var formData = new Object;

            formData.areas = $("input[name='areas[]']").map(function(index){
                let val = $(this).val();
                let id = $(this).next(".name-input-id").val();

                if (index > 0 && val && val.length !== 0) {
                    return {
                        'id': id,
                        'name': val.toLowerCase()
                    }
                }
            }).get();

            formData.landings = $("select[name='landings[]']").map(function(index){
                let val = $(this).val();
                if (index > 0 && val && val.length !== 0) {
                    return {
                        'landing_ids': val
                    }
                }
            }).get();

            formData.photo_id = $('#photoId').val();
            formData.id = _data.id
            
            return formData;
        }

        function validate(data) {
            if (data.areas.length < 1 || data.landings.length < 1) {
                return "Please fill Area and Landings fields!";
            }
            
            if (data.areas.length !== data.landings.length) {
                return "Please complete Area and Landings forms!";
            }

            if (!data.photo_id || data.photo_id === 0) {
                return "Banner image is required!";
            }
            
            return true;
        }

        if (Dropzone.instances.length === 0) {
            $('#imageSection').dropzone({
                paramName: "media",
                url: '/admin/media',
                maxFilesize: 2,
                maxFiles: 1,
                acceptedFiles: 'image/*',
                dictDefaultMessage: 'Click to upload banner image',
                dictCancelUpload: 'Cancel',
                dictRemoveFile: 'Remove',
                addRemoveLinks: true,
                thumbnailWidth: null,
                thumbnailHeight: null,
                success: function (file, response) {
                    var photoInput = $('#photoId');
                    photoInput.val(response.media.id);
                    photoInput.attr('data-filename', file.name);
                },
                init: function () {
                    // generate existing photo
                    if (_data.photo_id && _data.photo_id != '') {
                        $('#photoId').val(_data.photo_id);

                        let mockFile = {
                            name: _data.name + '.jpg', 
                            size: 123
                        };
                        
                        if (_data.banner && _data.banner != '') {
                            this.emit("addedfile", mockFile);
                            let url = _data.banner.large;
                            url.substr(url.indexOf('/', 7) + 1);
                            this.createThumbnailFromUrl(mockFile, url);
                            $('.dz-image').last().find('img').attr({"width": "100%", "height": "auto"});
                            // this.emit('thumbnail', mockFile, url);
                        }
                    }

                    this.on("processing", function (file) {
                        triggerLoading('Processing banner image..');
                    });
                    this.on("thumbnail", function (file, dataUrl) {
                        $('.dz-image').last().find('img').attr({"width": "100%", "height": "auto"});
                    });
                    this.on("success", function (file) {
                        $('.dz-image').css({"width": "100%", "height": "auto"});
                        if (Swal.isLoading()) Swal.close();
                    })
                    this.on("maxfilesexceeded", function (file) {
                        this.removeAllFiles();
                        this.addFile(file);
                        triggerAlert('error', '<h5>Only 1 image file allowed!</h5>');
                    });
                    this.on("removedfile", function (file) {
                        $('#photoId').val('');
                    });
                    this.on("error", function(file, message) {
                        this.removeFile(file);
                        triggerAlert('error', '<h5>' + message + '</h5>');
                    });
                }
            });
        }

        $('#add').click((e) => {
            e.preventDefault();

            generateFields();
        });

        $('#cancel').click((e) => {
            e.preventDefault();
            
            Swal.fire({
                type: 'question',
                title: 'Cancel updating data?',
                html: '<h5>All unsaved data will be lost</h5>',
                showCancelButton: true,
                cancelButtonText: 'No, stay here',
                confirmButtonText: 'Yes, cancel it',
                showLoaderOnConfirm: true,
                preConfirm: () => {
                    window.location.replace('/admin/flash-sale/');
                }
            })
        });

        $('#history').click((e) => {
            _modalTitle = "Promo <strong>" + _data.name + "</strong> History";
            $('#modal').modal('show');
        })

        $('#submit').click((e) => {
            e.preventDefault();
            
            let dataObject = getFormData();
            validation = validate(dataObject);

            if (validation !== true) {
                triggerAlert('error', '<h5>' + validation + '</h5>');
            } else {
                Swal.fire({
                    type: 'question',
                    customClass: {
                        container: 'custom-swal'
                    },
                    title: 'Save updated data?',
                    showCancelButton: true,
                    confirmButtonText: 'Yes, save it',
                    showLoaderOnConfirm: true,
                    preConfirm: () => {
                        return $.ajax({
                            type: 'POST',
                            url: "/admin/flash-sale",
                            dataType: 'json',
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            data: dataObject
                        });
                    },
                    allowOutsideClick: () => !Swal.isLoading()
                }).then((result) => {
                    if (!result.dismiss) {
                        const response = result.value;

                        if (response.status !== true) {
                            triggerAlert('error', '<h5>' + response.responseJSON.meta.message + '</h5>');
                        } else {
                            Swal.fire({
                                type: 'success',
                                customClass: {
                                    container: 'custom-swal'
                                },
                                title: "Promo data successfully updated!",
                                onClose: () => {
                                    window.location.replace("{{ url('admin/flash-sale') }}");
                                }
                            });
                        }

                    }
                });
            }
        });

        // Initial area data
        if (_data.areas_count === 0) {
            generateFields();
        } else {
            $.each(_data.areas, (index, area) => {
                generateFieldsWithData(area);
            })
        }

        // :: Modal Events ::
        let modal = $('#modal');
        modal
            .on("show.bs.modal", (e) => {
                triggerLoading('Fetching history data..');

                modal.data('bs.modal').options.keyboard = false;
                modal.data('bs.modal').options.backdrop = 'static';

                const modalTitle = $('#modal-title');
                const modalContainer = $('#modal-container');

                modalTitle.html(_modalTitle);
                modalContainer.empty();

                function compileTable(data) {
                    let html, icon;

                    data.forEach(item => {
                        if (item.event.type == 'remove') {
                            html += '<tr class="bg-warning">';
                            icon = '<i class="fa fa-exclamation-circle">';
                        } else if (item.event.type == 'update') {
                            html += '<tr">';
                            icon = '<i class="fa fa-edit">';
                        } else {
                            html += '<tr>';
                            icon = '';
                        }

                        html += '<td class="text-center">' + item.date + '</td><td class="text-center">' + icon + '</td><td>' + item.event.message + '</td></tr>';
                    });

                    modalContainer.html(html);
                }

                $.ajax({
                    type: 'GET',
                    url: '/admin/flash-sale/history?id=' + _data.id,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: response => {
                        compileTable(response.data)
                    },
                    error: error => {
                        $('#modal').modal('hide');
                        triggerAlert('error', '<h3>Oops!</h3><h5>Something wrong happened.<br/>Please try again in 10 minutes.</h5>');
                    },
                    complete: function () {
                        if (Swal.isLoading()) Swal.close();
                    }
                });
            })
    });
</script>
@stop