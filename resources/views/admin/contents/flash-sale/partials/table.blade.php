<table id="table" class="table table-responsive" data-toggle="table" data-show-refresh="true" data-search="true"
    data-show-search-button="false" data-side-pagination="server" data-pagination="true" data-ajax="ajaxLoadData"
    data-query-params="queryParams" data-query-params-type="">
    <thead>
        <tr>
            <th class="text-center" data-field="banner" data-formatter="bannerFormatter"></th>
            <th data-field="name" data-formatter="nameFormatter">Promo Name</th>
            <th class="text-center" data-field="start_date" data-formatter="startFormatter">Starts</th>
            <th class="text-center" data-field="end_date" data-formatter="endFormatter">Ends</th>
            <th class="text-center" data-field="is_active" data-formatter="statusFormatter">Status</th>
            <th class="text-center" data-field="updated_at">Last Updated</th>
            <th class="text-center" data-field="actions" data-formatter="actionFormatter">Actions</th>
        </tr>
    </thead>
</table>
