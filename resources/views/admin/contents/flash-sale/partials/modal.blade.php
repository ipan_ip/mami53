<div class="modal" id="modal" role="dialog" aria-labelledby="modalLabel">
    <div class="modal-dialog modal-lg" role="modal">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top:2px;"><i
                        class="fa fa-lg fa-times-circle"></i></button>
                <h4 class="modal-title"><i class="fa fa-history"></i>&nbsp;&nbsp;<span id="modal-title"></span></h4>
            </div>
            <div class="modal-body">
                <table class="table table-responsive table-bordered">
                    <thead>
                        <th class="text-center" style="width: 25%;">Date</th>
                        <th></th>
                        <th class="text-center">Event</th>
                    </thead>
                    <tbody id="modal-container"></tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>