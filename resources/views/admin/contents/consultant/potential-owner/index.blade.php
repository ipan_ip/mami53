@extends('admin.layouts.main')

@section('style')
	<style>
		.modal {
			background: none;
		}

		.simple-list {
			list-style-type: disc;
		}

		.swal2-container .swal2-popup {
      font-size: 1.6rem;
    }

    .swal2-container .swal2-icon.swal2-question::before, .swal2-container .swal2-icon.swal2-warning::before {
      content: '';
    }

		.input-file-container {

		}

		.input-file-container .add-csv-button {

		}

		.input-file-container .add-csv-filename {
			margin-top: 10px;
			padding: 5px 7px;
			border: 1px solid #bbb;
			word-break: break-word;
			display: none;
			color: #666;
		}

		#import-data {
			display: none;
		}

		.error-message {
			font-size: 14px;
			padding: 5px 7px;
			margin: 20px 0;
			border: 1px solid #ebccd1;
			display: none;
			color: #a94442;
			background-color: #f2dede;
			border-radius: 3px;
		}

		.error-message h3 {
			font-size: 24px;
			margin: 0;
			cursor: pointer;
		}

		.error-message ul {
			padding-top: 10px;
		}

		.error-message ul li + li {
			padding-top: 5px;
		}

		.error-message.collapsed ul {
			display: none;
		}

		.error-message.collapsed h3 .fa-caret-down {
			transform: rotateX(180deg);
		}
		/*-- Search Styles --*/
		.search-wrapper {
			max-width: 600px;
			line-height: 2.5;
			padding-right: 20px;
		}
		.search-wrapper input {
			min-width: 175px;
			margin: 6px;
			padding: 1px 4px;
		}
		.search-wrapper .dropdown {
			width: 175px;
			display: inline-block;
			text-align: left;
			margin: 6px;
		}
		.search-wrapper .dropdown button{
			width: 100%;
			color: white;
			background: #333333;
		}
		.search-wrapper .dropdown span.caret {
			float: right;
			margin-top: 8px;
		}
		.btn-search {
			width: 100%;
			margin-top: 62px;
		}
		#clear-bbk{
			display: none;
			color: white;
			background: #333333;
		}
		#clear-bbk.active{
			display: block;
		}
		#clear-follow{
			display: none;
			color: white;
			background: #333333;
		}
		#clear-follow.active{
			display: block;
		}
		.badge {
			padding: .25em .4em;
			font-size: 75%;
			border-radius: 1rem;
			margin-left: 1rem;
		}
		.badge-danger {
			color: #fff;
			background-color: #dc3545;
		}
	</style>

	<link rel="stylesheet" href="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.css">
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
@endsection

@section('content')
	<div class="box box-default">
		<!-- Box Header -->
		<div class="box-header" style="padding-top: 10px;">
			<div class="col-md-6 text-center">
				<h3 class="box-title" style="padding-left: 0;"><i class="fa fa-users"></i>&nbsp;Potential Owner</h3>
			</div>

			@permission(['access-potential-owner'])
			<!-- disable this for now
				<div class="col-md-6">
					<div style="text-align: right;">
						<a class="btn btn-primary" style="margin-top: 5px; color: #fff;" href="/admin/consultant/potential-owner/add"><i class="fa fa-plus"></i>&nbsp;Add Potential Owner</a>
						{{--
						<br>
						<a class="btn btn-primary" style="margin-top: 15px; color: #fff;" data-toggle="modal" data-target="#modal-import"><i class="fa fa-file-text-o"></i>&nbsp;Import Owner Data</a>
						--}}
					</div>
				</div>
			-->
			@endpermission

			<div class="col-md-12">
				<div class="error-message">
					<h3>Failed data <i class="fa fa-caret-down"></i></h3>
					<ul class="simple-list" style="padding-left: 17px">
					</ul>
				</div>
			</div>
		</div>
		<!-- /.box-header -->

    <div class="box-body no-padding no-overflow">
      <!-- Content -->
      <div class="horizontal-wrapper" style="margin-top: 15px;">
				<div class="search-wrapper pull-right">
					<div class="row align-items-end">
						<div class="col-md-8 col-12 text-center">
							<div class="search-input" id="search">
								<input type="text" name="name" id="search-name" placeholder="Owner Name">
								<input type="tel" name="phone_number" id="search-phone" placeholder="Owner Phone Number">
								<div class="dropdown">
									<button
										name="bbk_status"
										class="btn dropdown-toggle followup-status-options-button"
										id="search-bbk-status"
										data-toggle="dropdown"
										role="button"
										aria-haspopup="true"
										aria-expanded="false"
										>Owner BBK Status<span class="caret"></span></button>
									<ul class="dropdown-menu bbk-status-options">
										<li>
											<a value="bbk">BBK</a>
										<li>
										<li>
											<a value="waiting"">Waiting</a>
										<li>
										<li>
											<a value="non-bbk">Non BBK</a>
										<li>
										<li id="clear-bbk">
											<a value="">None</a>
										<li>
									</ul>
								</div>
								<div class="dropdown">
									<button
										name="followup_status"
										class="btn dropdown-toggle followup-status-options-button"
										id="search-followup-status"
										data-toggle="dropdown"
										role="button"
										aria-haspopup="true"
										aria-expanded="false"
										>Owner Follow Up Status<span class="caret"></span></button>
									<ul class="dropdown-menu followup-status-options">
										<li>
											<a value="new">New</a>
										<li>
										<li>
											<a value="ongoing">Ongoing</a>
										<li>
										<li>
											<a value="done">Done</a>
										<li>
										<li id="clear-followup">
											<a value="">None</a>
										<li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-12 p-0">
							<button class="btn btn-primary btn-search" id="submit-search">Search</button>
						</div>
					</div>
				</div>
        <div style="padding: 15px;" id="potential-owner-list">
          <table>
            <thead>
              <tr>
                <th data-field="name" data-searchable="true">Owner Name</th>
                <th data-field="phone_number" data-formatter="phoneNumberFormatter">Owner Phone Number</th>
                <th data-field="total_property" data-searchable="false">Total Listing</th>
				<th data-field="created_at" data-formatter="dateFormatterCreatedAt" data-searchable="false">Created At</th>
                <th data-field="updated_at" data-formatter="dateFormatter" data-searchable="false">Updated At</th>
                <th data-field="updated_by" data-searchable="false">Updated By</th>
                <th data-field="bbk_status" data-searchable="false">Owner BBK Status</th>
                <th data-field="followup_status" data-searchable="false">Owner Followup Status</th>
                <th data-field="id" data-formatter="actionFormatter" data-searchable="false">Action</th>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
	</div>

	<!-- Modal -->
	<div class="modal fade" id="modal-import">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
          <h4 class="modal-title">Import Potential Owner Data</h4>
				</div>
				<div class="modal-body">
					<div class="input-file-container">
						<button class="btn btn-default add-csv-button" style="width: 100%"><i class="fa fa-save"></i>&nbsp;Browse</button>
						<p class="add-csv-filename"></p>
						<input type="file" accept=".csv, .txt" id="import-data" name="import-data">
					</div>
					<ul class="simple-list" style="color: #777; font-size: 12px; padding-left: 17px; margin: 20px 0;">
						<li>Only support .csv or .txt file</li>
						<li>Maximum filesize is 1MB</li>
					</ul>
				</div>
				<div class="modal-footer">
					<button class="btn btn-success btn-import-save" disabled>
						Save
					</button>
				</div>
			</div>
		</div>
	</div>
	<!-- END Modal -->
@endsection

@section('script')
	<script src="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.27.0/moment.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
	<script>
		function actionFormatter(value, row) {
			let action = `
			<a class="btn btn-default" href="/admin/consultant/potential-owner/detail/${value}" title="see potential owner detail"><i class="fa fa-eye"></i></a>
			@permission(['access-potential-owner']) <a class="btn btn-warning" href="/admin/consultant/potential-owner/edit/${value}" title="edit potential owner"><i class="fa fa-edit"></i></a>
			<button class="btn btn-danger delete-potential-owner" title="delete potential owner" data-delete-potential-owner="${value}"><i class="fa fa-trash"></i></button> @endpermission
			`;

			return action;
		}

		/**
		 * Formatter for created at
		 * 
		 * @return string
		 */
		function dateFormatterCreatedAt(date) {
			return moment(date).format('DD-MM-Y  HH:mm:ss')
		}

		function dateFormatter(value) {
			return moment(value).format('DD-MM-Y  HH:mm:ss')
		}

		function phoneNumberFormatter(value, row) {
			if (row.is_hostile) {
				return value + "<span class='badge badge-danger'>hostile</span>"
			}
			return value
		}
		
    $(document).ready(function() {
			$('#potential-owner-list table').bootstrapTable({
				pagination: true,
				search: false,
				showSearchButton: true,
				searchOnEnterKey: true,
				searchAlign: 'left',
				pageSize: 15,
				pageList: [],
				sidePagination: 'server',
				url: "{{ url('admin/consultant/potential-owner/data') }}",
			})

			var $save = $('.btn-import-save');
			var $importData = $('#import-data');

			var $csvButton = $('.add-csv-button');
			var $csvFilename = $('.add-csv-filename');

			var $errorMessage = $('.error-message');
			var $errorList = $errorMessage.find('ul');

			$importData.val('');

			$csvButton.on('click', function() {
				$importData.click();
			});

			$importData.on('change', function() {
				if ($(this)[0].files[0].size <= 1000000) {
					$save.prop('disabled', false);
					$csvFilename.text($importData[0].files[0].name).show();
				}
			});

			$save.on('click', function() {
				if ($importData) {
					$('body').css('pointer-events', 'none');
					$save.prop('disabled', true).text('importing data...');

					var data = new FormData();
					data.append('file', new Blob([$importData[0].files[0]], { type: 'text/csv' }));

					axios.post(`/admin/consultant/potential-owner/bulk`, data).then(response => {
						$('#modal-import').modal('hide');

						if (response.data.status) {
							$errorList.html('');

							if (response.data.data.failed_count > 0) {

								// some data failed or all data failed
								if (response.data.data.success_count > 0) {
									Swal.fire({
										title: 'Only some of the data imported',
										icon: 'warning',
										text: `${response.data.data.success_count} data was successfuly imported from ${response.data.data.success_count + response.data.data.failed_count} data on your csv file. Please check the top page to check what\'s wrong with your csv data.`,
										confirmButtonText: 'Ok'
									});

									$('#potential-owner-list table').bootstrapTable('refresh');
								} else {
									Swal.fire({
										title: 'Failed importing csv file',
										icon: 'error',
										text: 'Check error message at the top to see what\'s wrong with the data on your csv file.',
										confirmButtonText: 'Ok'
									});
								}

								$errorMessage.show();

								let failedData = response.data.data.failed;

								for (const message of failedData) {
									$errorList.append(`<li>${Object.keys(message)} : ${Object.values(message)}</li>`)
								}
							}

							// all data success
							if (response.data.data.failed_count == 0) {
								$('#potential-owner-list table').bootstrapTable('refresh');

								Swal.fire({
									title: 'Csv file imported successfully!',
								  icon: 'success',
								  text: 'Potential owner table will be updated',
									confirmButtonText: 'Ok'
								});

								$errorMessage.hide();
								$errorList.html('');
							}
						} else {
							Swal.fire({
                icon: 'error',
                text: response.data.meta.message,
          			confirmButtonText: 'Ok'
							});
						}
					}).catch(error => {
						$('#modal-import').modal('hide');
						Swal.fire({
							icon: 'error',
							text: 'Something wrong with your csv file',
          		confirmButtonText: 'Ok'
						});
					}).finally(() => {
						$importData.val('');
						$csvFilename.text('').hide();
						$('body').css('pointer-events', 'auto');
						$save.text('Save').prop('disabled', false);
					});
				}
			});

			// Search Function

			// dropdown followup status logic
			var followup_status_selected = '';

			$('.dropdown-menu.followup-status-options a').on('click', function(){
				$(this).parent().parent().prev().html($(this).html() + '<span class="caret"></span>');

				followup_status_selected = $(this).attr('value');

				followup_status_selected!='' ? $('#clear-followup').addClass('active') : $('#clear-followup').removeClass('active');
			})

			// dropdown BBK status status logic
			var bbk_status_selected = '';

			$('.dropdown-menu.bbk-status-options a').on('click', function(){
				$(this).parent().parent().prev().html($(this).html() + '<span class="caret"></span>');

				bbk_status_selected = $(this).attr('value');

				bbk_status_selected!='' ? $('#clear-bbk').addClass('active') : $('#clear-bbk').removeClass('active');
			})


				// submit search
			$('#submit-search').on('click', function(event) {

				event.preventDefault();

				let $name = $('#search-name').val();
				let $phone = $('#search-phone').val();
				let $bbk_status = bbk_status_selected;
				let $followup_status = followup_status_selected;


				if( $name!='' || $phone!='' || $bbk_status!='' || $followup_status!='') {
					let searchData= [];

					if($name!='') {
						const searchName = `name=${$name}`;
						searchData.push(searchName);
					};
					if($phone!='') {
						const searchPhone = `phone_number=${$phone}`;
						searchData.push(searchPhone);
					};
					if($bbk_status!='') {
						const searchBbk = `bbk_status=${$bbk_status}`;
						searchData.push(searchBbk);
					};
					if($followup_status!='') {
						const searchFollowup = `followup_status=${$followup_status}`;
						searchData.push(searchFollowup);
					};

					let params = searchData.join('&');

					$('#potential-owner-list table').bootstrapTable('destroy');

					$('#potential-owner-list table').bootstrapTable({
						pagination: true,
						search: false,
						showSearchButton: true,
						searchOnEnterKey: true,
						searchAlign: 'left',
						pageSize: 15,
						pageList: [],
						sidePagination: 'server',
						url: "{{ url('admin/consultant/potential-owner/data?') }}" + params
					})
				} else {
					$('#potential-owner-list table').bootstrapTable('destroy');

					$('#potential-owner-list table').bootstrapTable({
						pagination: true,
						search: false,
						showSearchButton: true,
						searchOnEnterKey: true,
						searchAlign: 'left',
						pageSize: 15,
						pageList: [],
						sidePagination: 'server',
						url: "{{ url('admin/consultant/potential-owner/data') }}",
					})
				}
			})

			// delete potential owner
			$('#potential-owner-list table').on('click', '.delete-potential-owner', function() {
				const dataId = $(this).data('delete-potential-owner');

				Swal.fire({
					title: 'Delete potential owner',
					icon: 'question',
					text: 'Do you really want to delete this potential owner?',
          showCancelButton: true,
          cancelButtonText: 'Cancel',
					confirmButtonText: 'Yes, delete it',
					confirmButtonColor: '#f4543c',
					showLoaderOnConfirm: true,
					preConfirm: function() {
						return axios.delete(`/admin/consultant/potential-owner/${dataId}`)

						.then(response => {
							if (!response.data.status) {
								throw new Error(response.meta.message);
							}

							Swal.fire({
								title: 'Done',
								text: 'Potential owner deleted',
								icon: 'success',
								confirmButtonText: 'Ok'
							});

							$('#potential-owner-list table').bootstrapTable('refresh');

							return response;
						})
						.catch(error => {
							alert(error);
						});
					},
					allowOutsideClick: () => !Swal.isLoading()
				});
			});


			$errorMessage.find('h3').on('click', function() {
				$errorMessage.toggleClass('collapsed');
			});
    });
	</script>
@endsection