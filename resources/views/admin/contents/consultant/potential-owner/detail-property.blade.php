@extends('admin.layouts.main')

@section('style')
	<style>
		.custom-table {
			width: 100%;
			border-collapse: collapse;
			border: 1px solid #ddd;
		}

		.custom-table td {
			padding: 7px 10px;
			border: 1px solid #ddd;
			font-size: 16px;
		}

		.custom-table tr td:first-child {
			font-weight: bold;
			width: 250px;
		}

		.box.closed .box-body {
			display: none;
		}

		.box.closed .box-header .fa-caret-down {
			transform: rotateX(180deg);
		}

		.form-group > label span {
			color: #f00;
		}

		.swal2-container .swal2-popup {
			font-size: 1.6rem;
		}

		.swal2-container .swal2-icon.swal2-warning::before,
		.swal2-container .swal2-icon.swal2-error::before,
		.swal2-container .swal2-icon.swal2-question::before {
			content: '';
		}
	</style>

	<link rel="stylesheet" href="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.css">
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.1/min/dropzone.min.css">
@endsection

@section('content')
	<div class="box box-default">
		<!-- Box Header -->
		<div class="box-header" style="padding-top: 10px;">
			<div class="col-md-6 text-center">
			<h3 class="box-title" style="padding-left: 0;"><i class="fa fa-users"></i>&nbsp;Detail Potential Property</h3>
			</div>
			<div class="col-md-6">
				<a
					class="btn btn-warning pull-right"
					style="margin-top: 5px; color: #fff;"
					data-toggle="modal"
					data-target="#modal-edit-potential-property"
				><i class="fa fa-pencil"></i> Edit This Potential Property</a>
			</div>
		</div>
		<!-- /.box-header -->

    <div class="box-body no-padding no-overflow">
      <!-- Content -->
      <div class="horizontal-wrapper" style="margin-top: 15px;">
        <div style="padding: 15px;">
          <table class="custom-table">
						<tbody>
							<tr>
								<td>Owner Name</td>
								<td>{{ !is_null($potentialProperty->potential_owner) && $potentialProperty->potential_owner->name != '' ? $potentialProperty->potential_owner->name : '-' }}</td>
							</tr>
							<tr>
								<td>Owner phone number</td>
								<td>{{ !is_null($potentialProperty->potential_owner) && $potentialProperty->potential_owner->phone_number!= '' ? $potentialProperty->potential_owner->phone_number : '-' }}</td>
							</tr>
							<tr>
								<td>Property name</td>
								<td>{{ $potentialProperty->name }}</td>
							</tr>
							<tr>
								<td>Address</td>
								<td>{{ $potentialProperty->address }}</td>
							</tr>
							<tr>
								<td>Area</td>
								<td>{{ $potentialProperty->area_city }}</td>
							</tr>
							<tr>
								<td>Province</td>
								<td>{{ $potentialProperty->province }}</td>
							</tr>
							<tr>
								<td>Total room</td>
								<td>{{ $potentialProperty->total_room }}</td>
							</tr>
							<tr>
								<td>Property URL</td>
								<td><a href="{{ $potentialProperty->getKostUrl() }}">{{ $potentialProperty->getKostUrl() }}</a></td>
							</tr>
							<tr>
								<td>Product Offer</td>
								<td id="product-offer-display"></td>
							</tr>
							<tr>
								<td>Product GP Total Room</td>
								<td>{{ $potentialProperty->potential_gp_total_room }}</td>
							</tr>
							<tr>
								<td>Product GP Room List</td>
								<td>{{ $potentialProperty->potential_gp_room_list }}</td>
							</tr>
							<tr>
								<td>Priority</td>
								<td>{{ $potentialProperty->is_priority == 1 ? 'High' : 'Low' }}</td>
							</tr>
							<tr>
								<td>Notes</td>
								<td>{{ !is_null($potentialProperty->remark) && $potentialProperty->remark != '' ? $potentialProperty->remark : '-' }}</td>
							</tr>
							<tr>
								<td>Uploaded photo</td>
								<td>
									<img src="{{ $medium }}">
								</td>
							</tr>
							<tr>
								<td>Created at</td>
								<td class="date-format">{{ $potentialProperty->created_at }}</td>
							</tr>
							<tr>
								<td>Updated at</td>
								<td class="date-format">{{ $potentialProperty->updated_at }}</td>
							</tr>
							<tr>
								<td>Created By</td>
								<td>{{ $potentialProperty->first_created_by->name }}</td>
							</tr>
							<tr>
								<td>Updated By</td>
								<td>{{ !is_null($potentialProperty->last_updated_by) ? $potentialProperty->last_updated_by->name : '' }}</td>
							</tr>
							<tr>
								<td>BBK Status</td>
								<td>{{ $potentialProperty->bbk_status }}</td>
							</tr>
							<tr>
								<td>Follow Up Status</td>
								<td>{{ $potentialProperty->followup_status }}</td>
							</tr>
						</tbody>
          </table>

					<div>
						<hr>
						<a class="btn btn-default" href="/admin/consultant/potential-owner/detail/{{ $potentialProperty->potential_owner->id }}"><i class="fa fa-arrow-left"></i>&nbsp;Back to potential owner detail</a>
					</div>
        </div>
      </div>
    </div>
	</div>

	<!-- modal edit -->
	<div class="modal fade" id="modal-edit-potential-property">
		<div class="modal-dialog">
			<div class="modal-content">

				<form class="form-edit-potential-property">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">×</span>
						</button>
						<h4 class="modal-title">Edit Potential Listing</h4>
					</div>

					<div class="modal-body">
						<div class="form-group">
							<label for="edit-property-name">Property Name<span>*</span></label>
							<div class="row">
								<div class="col-sm-12">
									<input name="" id="edit-property-name" class="form-control" type="text" required value="{{ $potentialProperty->name }}" disabled>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label>Product offer<span>*</span></label>
							<div class="btn-group" data-toggle="buttons" role="group" id="edit-product-offer-selector">
								<label class="btn active">
									<input type="radio" class="edit-product-offer" name="edit-product-offer" id="edit-product-offer-gp1" value="gp1" checked> GP 1
								</label>
								<label class="btn">
									<input type="radio" class="edit-product-offer" name="edit-product-offer" id="edit-product-offer-gp2" value="gp2"> GP 2
								</label>
								<label class="btn">
									<input type="radio" class="edit-product-offer" name="edit-product-offer" id="edit-product-offer-gp3" value="gp3"> GP 3
								</label>
							</div>
						</div>

						<div class="form-group">
							<label for="edit-priority">Priority</label>
							<div class="row">
								<div class="col-sm-12">
									<select name="priority" id="edit-priority">
										<option value="high">High</option>
										<option value="low">Low</option>
									</select>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="edit-notes">Notes</label>
							<div class="row">
								<div class="col-sm-12">
									<textarea class="form-control" id="edit-notes" maxlength="250">{{ $potentialProperty->remark }}</textarea>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="edit-followup-status">Follow Up Status</label>
							<div class="btn-group" data-toggle="buttons" role="group" id="edit-followup-status-selector">
								<label class="btn active">
									<input type="radio" name="edit-followup-status" id="edit-followup-status-new" value="new" checked> New
								</label>
								<label class="btn">
									<input type="radio" name="edit-followup-status" id="edit-followup-status-in-progress" value="in-progress"> In Progress
								</label>
								<label class="btn active">
									<input type="radio" name="edit-followup-status" id="edit-followup-status-paid" value="paid"> Paid
								</label>
								<label class="btn active">
									<input type="radio" name="edit-followup-status" id="edit-followup-status-approved" value="approved"> Approved
								</label>
								<label class="btn active">
									<input type="radio" name="edit-followup-status" id="edit-followup-status-rejected" value="rejected"> Rejected
								</label>
							</div>
						</div>

					</div>

					<div class="modal-footer">
						<button class="btn btn-success submit-edit-potential-property">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- END modal edit -->
@endsection

@section('script')
	<script src="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.27.0/moment.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.1/min/dropzone.min.js"></script>

	<script>
		$(document).ready(function() {
			Dropzone.autoDiscover = false;

			let imageId = null;

			@if(is_null($potentialProperty->media_id))
				let originalMediaId = null;
			@else
				let originalMediaId = {{ $potentialProperty->media_id }}
			@endif

			let $offered_product_checked = $('input.edit-product-offer:checked');
			let $priority = $('#edit-priority');
			let $remark = $('#edit-notes');

			$priority.val({{ $potentialProperty->is_priority }} === 1 ? 'high' : 'low');

			const offeredProductString = `{{ $potentialProperty->offered_product }}`;
			$('#product-offer-display').text(offeredProductString.toUpperCase());

			const offeredProductArray = offeredProductString.split(', ');
			for (let item of offeredProductArray) {
				$(`input.edit-product-offer[value="${item}"]`).iCheck('check');
			}

			$(`input[name="edit-followup-status"][value="{{ $potentialProperty->followup_status }}"]`).iCheck('check');

			function checkingChecked() {
				if ($('input.edit-product-offer:checked').length) {
					$('input.edit-product-offer').attr('required', false);
				} else {
					$('input.edit-product-offer').attr('required', true);
				}
			}

			checkingChecked();

			$('input.edit-product-offer').on('ifToggled', function() {
				checkingChecked();
			});

			$('.date-format').each(function() {
				const date = $(this).text();
				$(this).text(moment(date).format('DD-MM-Y HH:mm:ss'));
			});

			// edit potential property
			$('.form-edit-potential-property').submit(function() {
				$('#modal-edit-potential-property').modal('hide');

				Swal.fire({
					title: 'Editing Potential Property..',
					onBeforeOpen: () => {
						Swal.showLoading();
					},
					allowOutsideClick: false
				});

				axios.patch(`/admin/consultant/potential-owner/{{ $potentialProperty->id }}/property`, {
					offered_product: [$('input.edit-product-offer:checked').val()],
					remark: $remark.val(),
					is_priority: $priority.val() === 'high' ? 1 : 0,
					followup_status: $('input[name="edit-followup-status"]:checked').val()
				})
				.then(response => {
					if (response.data.status) {
						Swal.fire({
							title: 'Done',
							text: 'Potential Property Edited. Now reloading this page.',
							icon: 'success',
							showConfirmButton: false,
							allowOutsideClick: false
						});

						location.reload();
					}
				})
				.catch(error => {
					Swal.fire({
						title: 'Error',
						text: error.response.data.meta.message,
						icon: 'error',
						confirmButtonText: 'Ok'
					})
				});

				return false;
			});
		});
	</script>
@endsection