@extends('admin.layouts.main')

@section('style')
	<style>
		.one-input {

		}

		.one-input + .one-input {
			padding-top: 20px;
		}

		.one-input p {

		}

		.one-input p span {
			color: red;
		}

		.one-input input {
			width: 100%;
		}

		.one-input select {
			width: 100%;
		}

		.one-input textarea {
			width: 100%;
			resize: vertical;
			min-height: 75px;
    	max-height: 250px;
			padding: 5px 10px;
		}

		.swal2-container .swal2-popup {
			font-size: 1.6rem;
		}

		.swal2-container .swal2-icon.swal2-warning::before {
			content: '';
		}

		.error-list {
			list-style-type: none;
			color: #a94442;
			padding-left: 0;
		}

		.error-list li + li {
			padding-top: 5px;
		}

		.datepicker table tr td {
			color: #222;
		}
		.datepicker table tr td.disabled, .datepicker table tr td.disabled:hover {
			color: #ccc;
		}
	</style>

	<link rel="stylesheet" href="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.css">
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker3.min.css">
	<link rel="stylesheet" href="/css/admin-consultant.css">
@endsection

@section('content')
	<div class="box box-default">
		<!-- Box Header -->
		<div class="box-header" style="padding-top: 10px;">
			<div class="col-md-12 text-center">
				<h3 class="box-title" style="padding-left: 0;"><i class="fa fa-users"></i>&nbsp;Edit Potential Owner</h3>
			</div>
		</div>
		<!-- /.box-header -->

    <div class="box-body no-padding no-overflow">
      <!-- Content -->
      <div class="horizontal-wrapper" style="margin-top: 15px;">
        <div style="padding: 15px;">

					<form id="submit-edit">
						<div class="row">
							<div class="col-sm-6">
								<div class="one-input">
									<p>Owner Name<span>*</span></p>
									<input type="text" id="name" placeholder="" value="{{ $owner->name }}" required>
								</div>

								<div class="one-input">
									<p>Owner Phone<span>*</span></p>
									<input type="text" id="phone" placeholder="" value="{{ $owner->phone_number }}" required>
								</div>

								<div class="one-input">
									<p>Owner Email</p>
									<input type="email" id="email" placeholder="-" value="{{ $owner->email }}">
								</div>

								<div class="one-input">
									<p>Total Property</p>
									<input type="text" id="total-property" placeholder="" value="{{ $owner->total_property }}" disabled>
								</div>

								<div class="one-input">
									<p>Visit Date</p>
									<input type="text" id="visit-date" placeholder="" value="">
								</div>

								<div class="one-input">
									<p>Notes</p>
									<textarea id="notes" maxlength="250" placeholder="-">{{ $owner->remark }}</textarea>
								</div>
								<div class="one-input">
									<p>Owner BBK Status: </p>
									<input type="text" value="{{ $owner->bbk_status }}" disabled>
								</div>
								<div class="one-input">
									<p>Owner Follow Up Status: </p>
									<input type="text" value="{{ $owner->followup_status }}" disabled>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-6">
								<hr>
								<a href="/admin/consultant/potential-owner/detail/{{ $owner->id }}" class="btn btn-default"><i class="fa fa-arrow-left"></i>&nbsp;back</a>
								<button type="submit"  class="btn btn-success pull-right" style="color: #fff;"><i class="fa fa-floppy-o"></i>&nbsp;Edit</button>
							</div>
						</div>
					</form>

        </div>
      </div>
    </div>
	</div>
@endsection

@section('script')
	<script src="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.27.0/moment.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>

	<script>
    $(document).ready(function() {
			$('#visit-date').datepicker({
				format: "dd-mm-yyyy",
				autoclose: true,
				maxViewMode: 2,
				todayHighlight: false,
				startDate: new Date()
			});

			const getDate = {{ $owner->date_to_visit }};
			if (getDate !== '0000-00-00') {
				$('#visit-date').val(getDate);
			}

			$('#submit-edit').submit(function(e) {

				Swal.fire({
					title: 'Adding edited potential owner...',
					onBeforeOpen: () => {
						Swal.showLoading();
					},
					allowOutsideClick: false
				});

				$('#submit-edit').prop('disabled', true);

				const filledDate = !!($('#visit-date').val()) ? moment($('#visit-date').val(), 'DD-MM-YYYY').format('Y-MM-DD') : '' ;

				axios.post('/admin/consultant/potential-owner/update/{{ $owner->id }}', {
					name: $('#name').val(),
					phone_number: $('#phone').val(),
					email: $('#email').val(),
					date_to_visit: filledDate,
					remark: $('#notes').val()
				})
				.then(response => {
					if (!response.data.status) {
						let errorMessageArray = response.data.message;

						let errorList = '';

						for (const message in errorMessageArray) {
							errorList += `<li>${errorMessageArray[message]}</li>`;
						}

						throw new Error(errorList);
					}

					Swal.fire({
						title: 'Done',
						text: 'You will be redirected to potential owner page',
						icon: 'success',
						showConfirmButton: false,
						allowOutsideClick: false
					});
					window.location.href = '{{ url("/admin/consultant/potential-owner") }}'
				})
				.catch(error => {
					$('#submit-add').prop('disabled', false);
					Swal.fire({
						title: 'Error',
						html: `<ul class="error-list">${error.message}</ul>`,
						icon: 'error',
						confirmButtonText: 'Ok'
					});
				});

				return false;
			});
    });
	</script>
@endsection