@extends('admin.layouts.main')

@section('style')
	<style>
		.custom-table {
			width: 100%;
			border-collapse: collapse;
			border: 1px solid #ddd;
		}

		.custom-table td {
			padding: 7px 10px;
			border: 1px solid #ddd;
			font-size: 16px;
		}

		.custom-table tr td:first-child {
			font-weight: bold;
			width: 250px;
		}

		.box.closed .box-body {
			display: none;
		}

		.box.closed .box-header .fa-caret-down {
			transform: rotateX(180deg);
		}

		.box-header {
			padding: 10px 0;
			background-color: rgba(0,0,0,.03);
			cursor: pointer;
		}

		.box-header h3 {
			cursor: pointer;
		}

    .modal {
      background: rgba(255, 255, 255, .4);
    }

		.form-group > label span {
			color: #f00;
		}

		input[type="file"] {
			border: 0;
			padding-left: 0;
		}

		textarea {
			width: 100%;
			resize: vertical;
			min-height: 75px;
    	max-height: 250px;
			padding: 5px 10px;
		}

		h3 {
			cursor: pointer;
		}

		.swal2-container .swal2-popup {
			font-size: 1.6rem;
		}

		.swal2-container .swal2-icon.swal2-warning::before,
		.swal2-container .swal2-icon.swal2-question::before {
			content: '';
		}

    td .btn + .btn {
      margin-left: 4px;
    }

		#potential-property .bootstrap-table .fixed-table-container .fixed-table-body {
			overflow: unset;
		}

		#modal-edit-follow-up-status .modal-body {
			padding: 24px 13rem;
		}

		#modal-edit-follow-up-status .dropdown {
			margin-bottom: 16px;
		}

		#modal-edit-follow-up-status .dropdown span.caret {
			float: right;
			margin-top: 8px;
		}

		.dropdown .dropdown-menu a, .edit-follow-up-status:hover, .edit-potential-property:hover {
			cursor: pointer;
		}

		.followup-status-options-button {
			width: 100%;
			border: 1px solid black;
			border-radius: 2px;
			color: black;
			text-align: left;
			background: transparent;
		}
		.dropdown-menu {
			left: unset !important;
			right: 19px;
		}
	</style>

	<link rel="stylesheet" href="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.css">
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.1/min/dropzone.min.css">
@endsection

@section('content')
	<div class="box box-default">
		<!-- Box Header -->
		<div class="box-header">
			<div class="col-md-12">
				<h3 class="box-title" style="padding-left: 0;"><i class="fa fa-users"></i>&nbsp;Detail Potential Owner&nbsp;<i class="fa fa-caret-down"></i></h3>

				@permission(['access-potential-owner'])
					<a class="btn btn-warning pull-right" style="margin-top: 5px; color: #fff;" href="/admin/consultant/potential-owner/edit/{{ $owner->id }}"><i class="fa fa-pencil"></i> Edit This Potential Owner</a>
					<a class="btn btn-default pull-right" style="margin-top: 5px; margin-right: 10px;" href="/admin/property-package/create/{{ $owner->phone_number }}"><i class="fa fa-plus"></i> Add New Package</a>
				@endpermission
			</div>
		</div>
		<!-- /.box-header -->

    <div class="box-body no-padding no-overflow">
      <!-- Content -->
      <div class="horizontal-wrapper">
        <div style="padding: 15px;">
          	<table class="custom-table">
				<tbody>
					<tr>
						<td>Owner Name</td>
						<td>{{ $owner->name }}</td>
					</tr>
					<tr>
						<td>Owner Phone Number</td>
						<td>{{ $owner->phone_number }}</td>
					</tr>
					<tr>
						<td>Owner Email</td>
						<td>{{ isset($owner->email) ? $owner->email : '-' }}</td>
					</tr>
					<tr>
						<td>Total Property</td>
						<td>{{ $owner->total_property }}</td>
					</tr>
					<tr>
						<td>Notes</td>
						<td>{{ !is_null($owner->remark) && $owner->remark != '' ? $owner->remark : '-' }}</td>
					</tr>
					<tr>
						<td>Visit Date</td>
						<td class="date-format">{{ $owner->date_to_visit }}</td>
					</tr>
					<tr>
						<td>Created at</td>
						<td class="date-format">{{ $owner->created_at }}</td>
					</tr>
					<tr>
						<td>Updated at</td>
						<td class="date-format">{{ $owner->updated_at }}</td>
					</tr>
					<tr>
						<td>Created by</td>
						<td>{{ $owner->created_by_user->name }}</td>
					</tr>
					<tr>
						<td>Updated by</td>
						<td>{{ isset($owner->last_updated_by) ? $owner->last_updated_by->name : '-' }}</td>
					</tr>
					<tr>
						<td>Owner BBK Status</td>
						<td>{{ $owner->bbk_status }}</td>
					</tr>
					<tr>
						<td>Owner Follow Up Status</td>
						<td>{{ $owner->followup_status }}</td>
					</tr>
					<tr>
						<td>Hostile Status</td>
						<td>{{ !is_null($owner->user) ? $owner->user->hostile_status : '-' }}</td>
					</tr>
				</tbody>
			</table>

			<div>
				<hr>
				<a class="btn btn-default" href="/admin/consultant/potential-owner"><i class="fa fa-arrow-left"></i>&nbsp;Back to potential owner list</a>
			</div>
        </div>
      </div>
    </div>
	</div>

	<div class="box box-default">
		<!-- Box Header -->
		<div class="box-header">
			<div class="col-md-12">
				<h3 class="box-title" style="padding-left: 0;"><i class="fa fa-home"></i>&nbsp;Potential Listing&nbsp;<i class="fa fa-caret-down"></i></h3>
				<!-- Hide this button for now
				<button
					type="button"
					class="btn btn-primary pull-right trigger-modal-add-potential-property"
					style="margin-top: 5px; color: #fff;"
				><i class="fa fa-plus"></i>&nbsp;Add Potential Listing</a>
				-->
			</div>
		</div>
		<!-- /.box-header -->

		<div class="box-body no-padding no-overflow">
			<div class="horizontal-wrapper">
        <div style="padding: 15px;" id="potential-property">
					<table
						data-toggle="table"
						data-url="{{ '/admin/consultant/potential-owner/'.$owner->id.'/property/data' }}"
						data-data-field="properties"
					>
						<thead>
							<tr>
								<th data-field="name">Property Name</th>
								<th data-field="address">Address</th>
								<th data-field="area_city">Area</th>
								<th data-field="province">Province</th>
								<th data-field="total_room">Total room</th>
								<th data-field="potential_gp_total_room">Potential GP Total Rooms</th>
								<th data-field="room" data-formatter="kostLevelFormatter">Kost Level</th>
								<th data-field="offered_product" data-formatter="productOfferFormatter">Product offer</th>
								<th data-field="is_priority" data-formatter="priorityFormatter">Priority</th>
								<th data-field="bbk_status">Property BBK Status</th>
								<th data-field="followup_status">Follow Up Status</th>
								<th data-field="remark">Notes</th>
								<th data-field="id" data-formatter="actionFormatter">Action</th>
							</tr>
						</thead>
					</table>

					<div>
						<hr>
						<a class="btn btn-default" href="/admin/consultant/potential-owner"><i class="fa fa-arrow-left"></i>&nbsp;Back to potential owner list</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- modal add -->
	<div class="modal fade" id="modal-add-potential-property">
		<div class="modal-dialog">
			<div class="modal-content">

				<form class="form-add-potential-property">
					<div class="modal-header">
						<!-- Hide this button for now
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">×</span>
						</button>
						<h4 class="modal-title">Add Potential Property</h4>
						-->
					</div>

					<div class="modal-body">
						<div class="form-group">
							<label for="property-name">Property Name<span>*</span></label>
							<div class="row">
								<div class="col-sm-12">
									<input name="" id="property-name" class="form-control" type="text" required>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="address">Address<span>*</span></label>
							<div class="row">
								<div class="col-sm-12">
									<input name="" id="address" class="form-control" type="text" required>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="area">Area<span>*</span></label>
							<div class="row">
								<div class="col-sm-12">
									<input name="" id="area" class="form-control" type="text" required>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="province">Province<span>*</span></label>
							<div class="row">
								<div class="col-sm-12">
									<input name="" id="province" class="form-control" type="text" required>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="kost-url">Kost Detail URL</label>
							<div class="row">
								<div class="col-sm-12">
									<input name="" id="kost-url" class="form-control" type="url" required>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="total-room">Total Room</label>
							<div class="row">
								<div class="col-sm-12">
									<input name="" id="total-room" class="form-control" type="numeric" required>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label>Product Offer<span>*</span></label>
							<div class="btn-group" data-toggle="buttons" role="group" id="product-offer-selector">
								<label class="btn active">
									<input type="radio" class="product-offer" name="edit-product-offer" id="edit-product-offer-gp1" value="gp1" checked> GP 1
								</label>
								<label class="btn">
									<input type="radio" class="product-offer" name="edit-product-offer" id="edit-product-offer-gp2" value="gp2"> GP 2
								</label>
								<label class="btn">
									<input type="radio" class="product-offer" name="edit-product-offer" id="edit-product-offer-gp3" value="gp3"> GP 3
								</label>
							</div>
						</div>

						<div class="form-group">
							<label for="potential-gp-total-room">Potential GP Total Room</label>
							<div class="row">
								<div class="col-sm-12">
									<input name="" id="potential-gp-total-room" class="form-control" type="numeric" required>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="potential-gp-room-list">Potential GP Room List<span>*</span></label>
							<div class="row">
								<div class="col-sm-12">
									<input name="" id="potential-gp-room-list" class="form-control" type="text" required>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="edit-priority">Priority</label>
							<div class="row">
								<div class="col-sm-12">
									<select name="priority" id="priority">
										<option value="high">High</option>
										<option value="low">Low</option>
									</select>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="notes">Notes</label>
							<div class="row">
								<div class="col-sm-12">
									<textarea class="form-control" id="notes" maxlength="250"></textarea>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="upload-photo">Upload photo</label>
							<div class="row">
								<div class="col-sm-12">
									<div id="upload-photo" class="dropzone">
									</div>
								</div>
							</div>
						</div>

						<div class="form-group">

							<label for="followup-status">Follow Up Status</label>
							<div class="btn-group" data-toggle="buttons" role="group" id="followup-status-selector">
								<label class="btn active">
									<input type="radio" name="followup-status" id="followup-status-new" value="new" checked> New
								</label>
								<label class="btn">
									<input type="radio" name="followup-status" id="followup-status-in-progress" value="in-progress"> In Progress
								</label>
							</div>
						</div>
					</div>

					<div class="modal-footer">
						<button class="btn btn-success submit-add-potential-property">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- END modal add -->

	<!-- modal edit follow up status-->
	<div class="modal fade" id="modal-edit-follow-up-status">
		<div class="modal-dialog">
			<div class="modal-content">

				<form class="form-edit-follow-up-status">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">×</span>
						</button>
						<h4 class="modal-title">Edit Follow Up Status</h4>
					</div>

					<div class="modal-body">
						<label for="dropdown">Please Select Follow Up Status</label>
						<div class="dropdown">
							<a
								class="btn dropdown-toggle followup-status-options-button"
								id="followup-status-selected"
								data-toggle="dropdown"
								role="button"
								aria-haspopup="true"
								aria-expanded="false"
								>Follow Up Status
								<span class="caret"></span></a>
							<ul class="dropdown-menu followup-status-options">
								<li>
									<a value="new">New</a>
								<li>
								<li>
									<a value="in-progress">In Progress</a>
								<li>
								<li>
									<a value="paid">Paid</a>
								<li>
								<li>
									<a value="approved">Approved</a>
								<li>
								<li>
									<a value="rejected">Rejected</a>
								<li>
							</ul>
						</div>
						<textarea class="rejected-textarea hidden" id="rejected-reason" rows="4" cols="50" name="comment" form="usrform" placeholder="The reason of rejected ..."></textarea>
					</div>

					<div class="modal-footer">
						<button class="btn btn-success submit-edit-follow-up-status">Ubah</button>
					</div>
				</form>

			</div>
		</div>
	</div>

	<!-- modal edit potential property-->
	<div class="modal fade" id="modal-edit-potential-property">
		<div class="modal-dialog">
			<div class="modal-content">

				<form class="form-edit-potential-property">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">×</span>
						</button>
						<h4 class="modal-title">Edit Potential Listing</h4>
					</div>

					<div class="modal-body">
						<div class="form-group">
							<label for="edit-property-name">Property Name<span>*</span></label>
							<div class="row">
								<div class="col-sm-12">
									<input name="" id="edit-property-name" class="form-control" type="text" disabled>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="edit-address">Address<span>*</span></label>
							<div class="row">
								<div class="col-sm-12">
									<input name="" id="edit-address" class="form-control" type="text" disabled>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="edit-area">Area<span>*</span></label>
							<div class="row">
								<div class="col-sm-12">
									<input name="" id="edit-area" class="form-control" type="text" disabled>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="edit-province">Province<span>*</span></label>
							<div class="row">
								<div class="col-sm-12">
									<input name="" id="edit-province" class="form-control" type="text" disabled>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="edit-total-room">Total room</label>
							<div class="row">
								<div class="col-sm-12">
									<input name="" id="edit-total-room" class="form-control" type="text" disabled>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label>Product Offer<span>*</span></label>
							<div class="btn-group" data-toggle="buttons" role="group" id="edit-product-offer-selector">
								<label class="btn active">
									<input type="radio" class="edit-product-offer" name="edit-product-offer" id="edit-product-offer-gp1" value="gp1" checked> GP 1
								</label>
								<label class="btn">
									<input type="radio" class="edit-product-offer" name="edit-product-offer" id="edit-product-offer-gp2" value="gp2"> GP 2
								</label>
								<label class="btn">
									<input type="radio" class="edit-product-offer" name="edit-product-offer" id="edit-product-offer-gp3" value="gp3"> GP 3
								</label>
							</div>
						</div>

						<div class="form-group">
							<label for="edit-priority">Priority</label>
							<div class="row">
								<div class="col-sm-12">
									<select name="priority" id="edit-priority">
										<option value="high">High</option>
										<option value="low">Low</option>
									</select>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="edit-notes">Notes</label>
							<div class="row">
								<div class="col-sm-12">
									<textarea class="form-control" id="edit-notes" maxlength="250" placeholder="-"></textarea>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="edit-followup-status">Follow Up Status</label>
							<div class="btn-group" data-toggle="buttons" role="group" id="edit-followup-status-selector">
								<label class="btn active">
									<input type="radio" name="edit-followup-status" class="followup-status-option" id="edit-followup-status-new" value="new"> New
								</label>
								<label class="btn">
									<input type="radio" name="edit-followup-status" class="followup-status-option" id="edit-followup-status-in-progress" value="in-progress"> In Progress
								</label>
								<label class="btn">
									<input type="radio" name="edit-followup-status" class="followup-status-option" id="edit-followup-status-paid" value="paid">Paid
								</label>
								<label class="btn">
									<input type="radio" name="edit-followup-status" class="followup-status-option" id="edit-followup-status-approved" value="approved">Approved
								</label>
								<label class="btn">
									<input type="radio" name="edit-followup-status" class="followup-status-option" id="edit-followup-status-rejected" value="rejected">Rejected
								</label>
							</div>
						</div>
					</div>

					<div class="modal-footer">
						<button class="btn btn-success submit-edit-potential-property">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- END modal edit -->
@endsection

@section('script')
	<script src="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.27.0/moment.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.1/min/dropzone.min.js"></script>

	<script>
		function actionFormatter(value, row, index) {
			let action = `
			<div class="dropdown">
				<a
					class="btn btn-default dropdown-toggle"
					data-toggle="dropdown"
					role="button"
					aria-haspopup="true"
					aria-expanded="false"
					>
					Action <span class="caret"></span></a>
				<ul class="dropdown-menu">
					<li>
						<a
							class="edit-follow-up-status"
							title="edit follow up status"
							data-edit-potential-property-id="${value}"
							data-edit-potential-property-index="${index}"
						>
						Edit Follow Up Status</a>
					<li>
					<li>
						<a href="${row['kost_url']}" title="see potential property detail" target="_blank">Kost Detail Page</a>
					</li>
					<li>
						<a href="/admin/consultant/potential-owner/property/${value}">More Detail</a>
					<li>
						<a
						class="edit-potential-property"
						title="edit potential property"
						data-edit-potential-property-id="${value}"
						data-edit-potential-property-index="${index}"
						>Edit Potential Listing</a>
					</li>
					<li>
						@permission(['delete-potential-property']) <button class="delete-potential-property" title="delete potential property" data-delete-potential-property="${value}">Delete Potential Listing</button> @endpermission
					</li>
				</ul>
			</div>
			`;

			return action;
		}

		function productOfferFormatter(value) {
			return value.join(', ').toUpperCase();
		}

		function priorityFormatter(value) {
			return (value === 1 ? 'High' : 'Low' );
		}

		/**
		 * Kost level formatter
		 * 
		 * @return string
		 */
		function kostLevelFormatter(value) {
			try {
				if (value.level[0].name != null)
					return value.level[0].name;
				else
					return '-';
			} catch (e) {
				return '-';
			}
			
		}

		$(document).ready(function() {
			let editedPropertyId = '';
			let imageId = null;
			let addTriggered = false;
			let editTriggered = false;

			Dropzone.autoDiscover = false;

			$('.date-format').each(function() {
				const date = $(this).text();
				if (date === '0000-00-00') {
					$(this).text('-');
				} else {
					$(this).text(moment(date).format('DD-MM-Y HH:mm:ss'));
				}
			});

			// toggle accordion
			$('.box-header').on('click', function() {
				$(this).closest('.box').toggleClass('closed');
			});

			// stop propagation on accordion
			$('.box-header .btn').on('click', function(e) {
				e.stopPropagation();
			});

			// open modal add potential property
			$('.trigger-modal-add-potential-property').on('click', function(e) {
				e.preventDefault();
				$('#modal-add-potential-property').modal('show');
			});

			// input only number
			$('#total-room, #edit-total-room').on('input blur paste', function(){
				$(this).val($(this).val().replace(/\D/g, ''))
			});

			// delete potential property
			$('#potential-property table').on('click', '.delete-potential-property', function() {
				const propertyId = $(this).data('delete-potential-property');
				Swal.fire({
					title: 'Delete potential listing',
					icon: 'question',
					text: 'Do you really want to delete this property?',
					showCancelButton: true,
          cancelButtonText: 'Cancel',
					confirmButtonText: 'Yes, delete it',
					confirmButtonColor: '#f4543c',
					showLoaderOnConfirm: true,
					preConfirm: function() {
						axios.delete(`/admin/consultant/potential-owner/${propertyId}/property`)
						.then(response => {
							if (response.data.status) {
								Swal.fire({
									title: 'Done',
									text: 'Potential listing deleted. Now reloading this page.',
									icon: 'success',
									showConfirmButton: false,
									allowOutsideClick: false
								});

								location.reload();
							}
						})
						.catch(error => {
							Swal.fire({
								title: 'Error',
								text: error.data.meta.message,
								icon: 'error',
								confirmButton: 'Ok'
							});
						});
					},
					allowOutsideClick: () => !Swal.isLoading()
				})
			});



			{{--
			// 				d8888 8888888b.  8888888b.
			//       d88888 888  "Y88b 888  "Y88b
			//      d88P888 888    888 888    888
			//     d88P 888 888    888 888    888
			//    d88P  888 888    888 888    888
			//   d88P   888 888    888 888    888
			//  d8888888888 888  .d88P 888  .d88P
			// d88P     888 8888888P"  8888888P"
			--}}

			var addDropzone = new Dropzone("#upload-photo", {
				paramName: 'media',
				url: '/admin/media',
				maxFilesize: 5,
				maxFiles: 1,
				acceptedFiles: 'image/*',
				dictDefaultMessage: 'Click to upload a photo',
				dictCancelUpload: 'Cancel',
				dictRemoveFile: 'Delete',
				addRemoveLinks: true,
				thumbnailWidth: null,
				thumbnailHeight: null,
				params: {
					_token: '{{ csrf_token() }}',
					media_type: 'consultant_photo',
					need_watermark: 'false'
				},
				processing: function() {
					$('.submit-add-potential-property').prop('disabled', true);
				},
				success: function(file, response) {
					$('.submit-add-potential-property').prop('disabled', false);
					file.id = response.media.id;
					imageId = response.media.id;
				},
				init: function() {
					this.on('addedfile', function(file) {
						if (this.files.length > 1) {
							this.removeFile(this.files[0]);
						}
					});

					this.on("removedfile", function (file) {
						$('.submit-add-potential-property').prop('disabled', false);
						if (file.id) {
							$.get("/admin/media/"+file.id+"/delete", function(data, status) {
								console.log("Sukses hapus");
							});
						}
					});
				}
			});

			function checkingAddChecked() {
				if ($('input.product-offer:checked').length) {
					$('input.product-offer').attr('required', false);
				} else {
					$('input.product-offer').attr('required', true);
				}
			}

			checkingAddChecked();

			$('input.product-offer').on('ifToggled', function() {
				checkingAddChecked();
			});


			// reset input value on close add potential property modal
			$('#modal-add-potential-property').on('hidden.bs.modal', function() {
				let $name = $('#property-name');
				let $address = $('#address');
				let $area_city = $('#area');
				let $province = $('#province');
				let $total_room = $('#total-room');
				let $priority = $('#priority');
				let $remark = $('#notes');

				$name.val('');
				$address.val('');
				$area_city.val('');
				$province.val('');
				$total_room.val('');
				$priority.val('high');
				$('input.product-offer').iCheck('uncheck');
				$remark.val('');

				if (!addTriggered) {
					addDropzone.removeAllFiles(true);
				}

				imageId = null;
			});


			// add potential property
			$('.form-add-potential-property').submit(function() {
				addTriggered = true;
				$('#modal-add-potential-property').modal('hide');

				let name = $('#property-name').val();
				let address = $('#address').val();
				let area_city = $('#area').val();
				let province = $('#province').val();
				let total_room = $('#total-room').val();
				let potential_gp_total_room = $('#potential-gp-total-room').val();
				let potential_gp_room_list = $('#potential-gp-room-list').val();
				let priority = $('#priority').val();
				let offered_product_checked = $('input.product-offer:checked').val();
				let remark = $('#notes').val();
				let followup_status = $('input[name="followup-status"]:checked').val();

				Swal.fire({
					title: 'Adding new potential owner...',
					onBeforeOpen: () => {
						Swal.showLoading();
					},
					allowOutsideClick: false
				});

				axios.post(`/admin/consultant/potential-owner/{{ $owner->id }}/property`, {
					name: name,
					address: address,
					area_city: area_city,
					province: province,
					total_room: total_room,
					potential_gp_total_room: potential_gp_total_room,
					potential_gp_room_list: potential_gp_room_list,
					is_priority: priority === 'high' ? true : false,
					offered_product: [offered_product_checked],
					remark: remark,
					media_id: imageId,
					followup_status: followup_status
				})
				.then(response => {
					if (response.data.status) {
						Swal.fire({
							title: 'Done',
							text: 'Added new potential listing',
							icon: 'success',
							showConfirmButton: false,
							allowOutsideClick: false
						});

						addTriggered = false;

						if (addDropzone.files.length) {
							addDropzone.files[0].previewElement.remove();
							addDropzone.files[0].id = '';
						}

						location.reload();
					}
				})
				.catch(error => {
					console.log(error);
					Swal.fire({
						title: 'Error',
						text: error.data.meta.message,
						icon: 'error',
						confirmButton: 'Ok'
					});
				});

				return false;
			});



			{{--
			// 8888888888 8888888b. 8888888 88888888888
			// 888        888  "Y88b  888       888
			// 888        888    888  888       888
			// 8888888    888    888  888       888
			// 888        888    888  888       888
			// 888        888    888  888       888
			// 888        888  .d88P  888       888
			// 8888888888 8888888P" 8888888     888
			--}}

			let originalMediaId = '';

			function checkingEditChecked() {
				if ($('input.edit-product-offer:checked').length) {
					$('input.edit-product-offer').attr('required', false);
				} else {
					$('input.edit-product-offer').attr('required', true);
				}
			}

			checkingEditChecked();

			$('input.edit-product-offer').on('ifToggled', function() {
				checkingEditChecked();
			});

			// open modal edit follow up status
			$('#potential-property table').on('click', '.edit-follow-up-status',
			function() {
				$('#modal-edit-follow-up-status').modal('show');

				const dataPotentialProperty = $('#potential-property table').bootstrapTable('getData')[$(this).data('edit-potential-property-index')];

				editedPropertyId = dataPotentialProperty.id;

				let $followup_status = $('#followup-status-selected');
				let $followup_status_current = dataPotentialProperty.followup_status;

				//remove '-' and convert existed followup status data to Capital
				let $followup_status_current_uppercase = $followup_status_current
				.replace(/-/g, ' ')
				.replace(/^./, function(x){return x.toUpperCase()})

				$followup_status.html(`${ $followup_status_current_uppercase }` + '<span class="caret"></span>')
			})

			// reset modal edit follow up status value when modal closed
			$('#modal-edit-follow-up-status').on('hidden.bs.modal', function() {
				let $followup_status = $('#followup-status-selected');
				let $rejected_reason = $('#rejected-reason')

				$followup_status.html('Follow Up status <span class="caret"></span>');
				$rejected_reason.addClass('hidden').val('');

				editedPropertyId = '';
			})

			// variable for edit followup status
			var followup_status_selected = '';

			// dropdown followup status logic
			$('.dropdown-menu.followup-status-options a').on('click', function(){
				$(this).parent().parent().prev().html($(this).html() + '<span class="caret"></span>');

				followup_status_selected = $(this).attr('value');

				// show or hide rejected reason
				followup_status_selected === 'rejected' ? $('.rejected-textarea').removeClass('hidden') : $('.rejected-textarea').addClass('hidden');
			})

			// submit edit followup status
			$('.form-edit-follow-up-status').submit(function() {
				editTriggered = true;
				$('#modal-edit-follow-up-status').modal('hide');

				let followup_status = followup_status_selected;
				let rejected_reason_value = $('#rejected-reason').val();

				Swal.fire({
					title: 'Editing Follow Up Status...',
					onBeforeOpen: () => {
						Swal.showLoading();
					},
					allowOutsideClick: false
				})

				axios.patch(`/admin/consultant/potential-owner/${editedPropertyId}/property`, {
					followup_status: followup_status,
					remark: rejected_reason_value
				})

				.then(response => {
					if (response.data.status) {
						Swal.fire({
							title: 'Done',
							text: 'Follow Up Status Edited. Now reloading this page.',
							icon: 'success',
							showConfirmButton: false,
							allowOutsideClick: false
						});

						editTriggered = false;

						location.reload();
					}
				})
				.catch(error => {
					Swal.fire({
						title: 'Error',
						text: error,
						icon: 'error',
						confirmButton: 'Ok'
					});
				});

				return false;
			})


			// open modal edit potential property
			$('#potential-property table').on('click', '.edit-potential-property', function() {
				const dataPotentialProperty = $('#potential-property table').bootstrapTable('getData')[$(this).data('edit-potential-property-index')];

				let $name = $('#edit-property-name');
				let $address = $('#edit-address');
				let $area_city = $('#edit-area');
				let $province = $('#edit-province');
				let $total_room = $('#edit-total-room');
				let $priority = $('#edit-priority');
				let $remark = $('#edit-notes');

				$name.val(dataPotentialProperty.name);
				$address.val(dataPotentialProperty.address);
				$area_city.val(dataPotentialProperty.area_city);
				$province.val(dataPotentialProperty.province);
				$total_room.val(dataPotentialProperty.total_room);
				$priority.val(dataPotentialProperty.is_priority === 1 ? 'high' : 'low');

				for (let item of dataPotentialProperty.offered_product) {
					$(`input.edit-product-offer[value="${item}"]`).iCheck('check');
				}

				// followup status
				let $followup_status = dataPotentialProperty.followup_status;

				$(`input.followup-status-option[value="${$followup_status}"]`).iCheck('check');
				$followup_status === 'rejected' ? $('.rejected-textarea').removeClass('hidden') : $('.rejected-textarea').addClass('hidden');

				$remark.val(dataPotentialProperty.remark);

				editedPropertyId = dataPotentialProperty.id;

				originalMediaId = dataPotentialProperty.media_id !== 0 ? dataPotentialProperty.media_id : '';

				$('#modal-edit-potential-property').modal('show');
			});

			////////////////***Optional code for rejected reason on edit potential property****///////////////////
			//hide or show rejected reason on edit potential property
			// $('.followup-status-option').change(function() {
			// 		let radio_followup_status_selected = $(this).val();

			// 		radio_followup_status_selected === 'rejected' ? $('.rejected-textarea').removeClass('hidden') : $('.rejected-textarea').addClass('hidden');
			// 	}
			// })

			// reset input value on close edit potential property modal
			$('#modal-edit-potential-property').on('hidden.bs.modal', function() {
				let $name = $('#edit-property-name');
				let $address = $('#edit-address');
				let $area_city = $('#edit-area');
				let $province = $('#edit-province');
				let $total_room = $('#edit-total-room');
				let $priority = $('#edit-priority');
				let $remark = $('#edit-notes');

				$name.val('');
				$address.val('');
				$area_city.val('');
				$province.val('');
				$total_room.val('');
				$priority.val('high');
				$('input.edit-product-offer').iCheck('uncheck');
				$remark.val('');

				editedPropertyId = '';
				imageId = null;
				originalMediaId = '';
			});

			// edit potential property
			$('.form-edit-potential-property').submit(function() {
				editTriggered = true;
				$('#modal-edit-potential-property').modal('hide');

				let offered_product_checked = $('input.edit-product-offer:checked').val();
				let priority = $('#edit-priority').val();
				let remark = $('#edit-notes').val();
				let followup_status = $('input[name="edit-followup-status"]:checked').val();

				Swal.fire({
					title: 'Editing potential listing...',
					onBeforeOpen: () => {
						Swal.showLoading();
					},
					allowOutsideClick: false
				});

				axios.patch(`/admin/consultant/potential-owner/${editedPropertyId}/property`, {
					// previously it's an array. this to enable backward compatibility.
					offered_product: [offered_product_checked],
					remark: remark,
					is_priority: priority === 'high' ? true : false,
					followup_status: followup_status
				})
				.then(response => {
					if (response.data.status) {
						Swal.fire({
							title: 'Done',
							text: 'Potential listing edited. Now reloading this page.',
							icon: 'success',
							showConfirmButton: false,
							allowOutsideClick: false
						});

						editTriggered = false;

						location.reload();
					}
				})
				.catch(error => {
					Swal.fire({
						title: 'Error',
						text: error,
						icon: 'error',
						confirmButton: 'Ok'
					});
				});

				return false;
			});
		});
	</script>
@endsection