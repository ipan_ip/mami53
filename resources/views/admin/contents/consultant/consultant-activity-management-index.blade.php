@extends('admin.layouts.main')

@section('style')
  <style>
    div.required-field label::after {
      content: "*";
      color: red;
    }

    .swal2-container .swal2-popup {
      font-size: 1.6rem;
    }

    .swal2-container .swal2-icon.swal2-warning::before {
      content: '';
    }


    td .btn + .btn {
      margin-left: 4px;
    }
  </style>

  <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
  <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" />
  <link href="//cdn.jsdelivr.net/npm/select2-bootstrap-theme@0.1.0-beta.10/dist/select2-bootstrap.min.css"
    rel="stylesheet" />
  <link rel="stylesheet" href="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.css">
@endsection

@section('content')
  <div class="box box-default">
    <!-- Box Header -->
    <div class="box-header" style="padding-top: 10px;">
      <div class="col-md-6 text-center">
        <h3 class="box-title" style="padding-left: 0;"><i class="fa fa-list"></i>&nbsp;Activity Management</h3>
      </div>
      <div class="col-md-6">
        @permission('access-activity-management')
          <a href="{{ url('admin/consultant/activity-management/create/funnel') }}" class="btn btn-primary pull-right" style="color: #fff; margin-top: 5px;" ><i class="fa fa-plus"></i> Create New Funnel</a>
        @endpermission
      </div>
    </div>
    <!-- /.box-header -->

    <div class="box-body no-padding">
      <!-- Content -->
      <div class="horizontal-wrapper" style="margin-top: 15px;">
        <div class="btn-horizontal-group bg-default form-inline" style="padding: 15px;">

          <table
            id="funnel-list"
            data-toggle="table"
            data-side-pagination="server"
            data-pagination="true"
            data-url="{{ url('admin/consultant/activity-management/funnel') }}"
            data-data-field="funnels"
          >
            <thead>
              <tr>
                <th data-field="funnel_name">
                  Funnel Name
                </th>
                <th data-field="role">
                  Roles
                </th>
                <th data-field="data_type" data-formatter="dataTypeFormatter">
                  Data Types
                </th>
                <th data-field="staging">
                  Staging
                </th>
                <th data-field="updated_at">
                  Updated at
                </th>
                <th data-field="id" data-formatter="actionFormatter">
                  Action
                </th>
              </tr>
            </thead>
          </table>

        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
  <script src="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
  <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

  <script>
    function actionFormatter(value, row) {
      let action = `<a class="btn btn-default" href="/admin/consultant/activity-management/funnel/${value}"><i class="fa fa-eye"></i></a>`;

      @permission('access-activity-management')
        action += `<a class="btn btn-warning" href="/admin/consultant/activity-management/edit/funnel/${value}"><i class="fa fa-edit"></i></a>`
      @endpermission

      @permission('access-delete-activity-management')
        action += `<a class="btn btn-danger delete-funnel" data-funnel-id="${value}"><i class="fa fa-trash"></i></a>`
      @endpermission

      return action;
    }

    function dataTypeFormatter(value) {
      switch (value) {
        case 'consultant_tools_potential_tenant':
          return 'DBET';
        case 'designer':
          return 'Property';
        case 'mamipay_contract':
          return 'Contract';
        case 'consultant_potential_property':
          return 'Potential Property';
        case 'consultant_potential_owner':
          return 'Potential Owner';
        defaul:
          return value;
      }
    }

    $(function () {
      $('[data-toggle="tooltip"]').tooltip();

      $('#funnel-list').on('click', '.delete-funnel', function(e) {
        e.preventDefault();
        const funnelId = parseInt($(this).data('funnel-id'));

        Swal.fire({
          title: 'Are you sure?',
          text: 'This funnel will be deleted from consultant tools',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Delete',
          confirmButtonColor: '#c9302c',
          cancelButtonText: 'Cancel',
          preConfirm: function() {
            return axios.post(`/admin/consultant/activity-management/delete/funnel/${funnelId}`)
            .then(response => {
              if (!response.data.status) {
                throw new Error(response.data.meta.message)
              }
              return response;
            })
            .catch(error => {
              Swal.showValidationMessage(
                `Request failed: ${error}`
              )
            })
          },
          allowOutsideClick: () => !Swal.isLoading()
        }).then((result) => {
          if (result.value) {
            Swal.fire({
              icon: 'success',
              text: 'Funnel deleted'
            });

            $('#funnel-list').bootstrapTable('refresh');
          }
        });
      });


    });
  </script>
@endsection