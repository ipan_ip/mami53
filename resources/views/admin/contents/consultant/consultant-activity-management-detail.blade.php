@extends('admin.layouts.main')

@section('style')
  <style>
    div.required-field label::after {
      content: "*";
      color: red;
    }

    .funnel-title {
      border: 1px solid #ccc;
      padding: 15px;
      margin-top: 10px;
    }

    .funnel-title div {
      font-size: 16px;
    }

    .funnel-title div span {
      display: block;
      font-size: 14px;
    }

    .funnel-title div + div {
      padding-top: 10px;
    }

    .no-overflow .bootstrap-table .fixed-table-container .fixed-table-body {
      overflow-x: visible;
      overflow-y: visible;
    }

    .modal {
      background: rgba(255, 255, 255, .4);
    }

    textarea {
      resize: vertical;
    }

    .swal2-container .swal2-popup {
      font-size: 1.6rem;
    }

    .swal2-container .swal2-icon.swal2-warning::before {
      content: '';
    }
  </style>

  <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
  <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" />
  <link href="//cdn.jsdelivr.net/npm/select2-bootstrap-theme@0.1.0-beta.10/dist/select2-bootstrap.min.css"
    rel="stylesheet" />
  <link rel="stylesheet" href="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.css">
@endsection

@section('content')
  <div class="box box-default">
    <!-- Box Header -->
    <div class="box-header" style="padding-top: 10px;">
      <div class="col-md-6 text-center">
        <h3 class="box-title" style="padding-left: 0;"><i class="fa fa-list"></i>&nbsp;Detail Funnel</h3>
      </div>
      <div class="col-md-6">
        <a class="btn btn-primary pull-right" style="margin-top: 5px; color: #fff;" href="{{ url("admin/consultant/activity-management/create/staging/$funnel_id") }}"><i class="fa fa-plus"></i> Add Staging</a>
      </div>

      <div class="col-sm-12">
        <div class="row">
          <div class="col-xs-6">
            <div class="funnel-title">
              <div>
                <span>Funnel Name : </span>
                <strong>{{ $funnel_name }}</strong>
              </div>
              <div>
                <span>Consultant Roles : </span>

                <strong>{{ $consultant_role }}</strong>
              </div>
              <div>
                <span>Data Type : </span>
                @if ($data_type === "consultant_tools_potential_tenant")
                  <strong>DBET</strong>
                @elseif ($data_type === "designer")
                  <strong>Property</strong>
                @elseif ($data_type === "mamipay_contract")
                  <strong>Contract</strong>
                @elseif ($data_type === "consultant_potential_property")
                  <strong>Potential Property</strong>
                @elseif ($data_type === "consultant_potential_owner")
                  <strong>Potential Owner</strong>
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /.box-header -->

    <div class="box-body no-padding no-overflow">
      <!-- Content -->
      <div class="horizontal-wrapper" style="margin-top: 15px;">
        <div style="padding: 15px;">

          <table
            id="staging-list"
            data-toggle="table"
            data-side-pagination="server"
            data-pagination="true"
            data-url="{{ url("admin/consultant/activity-management/staging/$funnel_id") }}"
            data-data-field="stages"
          >
            <thead>
              <tr>
                <th data-field="stage_phase">
                  Stage Phase
                </th>
                <th data-field="stage_name">
                  Stage Name
                </th>
                <th data-field="description">
                  Description
                </th>
                <th data-field="input_form">
                  Input Form
                </th>
                <th data-field="id" data-formatter="actionDropdown">
                  Action
                </th>
              </tr>
            </thead>
          </table>

        </div>
      </div>
    </div>
  </div>

  <!-- Modal edit -->
  <div class="modal fade" id="modal-form">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
          <h4 class="modal-title">Edit Stage</h4>
        </div>

          <div class="modal-body">
            <div class="form-group">
              <label for="stagePhase">Stage Phase</label>
              <div class="row">
                <div class="col-sm-12">
                  <input name="stage_phase" id="stagePhase" class="form-control" type="number" min="1" max="10" required>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label for="stageName">Stage Name</label>
              <div class="row">
                <div class="col-sm-12">
                  <input type="text" class="form-control" id="stageName" name="stage_name" placeholder="ex: Visit Kos" required value="">
                </div>
              </div>
            </div>

            <div class="form-group">
              <label for="stageDescription">Stage Description</label>
              <div class="row">
                <div class="col-sm-12">
                  <textarea class="form-control" id="stageDescription" name="stage_description" placeholder="ex: Menemui pemilik kost untuk ditawarkan BBK" required value=""></textarea>
                </div>
              </div>
            </div>
          </div>

          <div class="modal-footer">
            <button class="btn btn-success" type="button" data-click="edit-staging">Edit Staging</button>
          </div>

      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->
@endsection

@section('script')
  <script src="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
  <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

  <script>
    function actionDropdown(value, row, index) {
      let dropdown = `
        <div class="btn-group">
          <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-cog"></i>&nbsp;Actions&nbsp;<i class="fa fa-caret-down"></i>
          </button>
          <ul class="dropdown-menu pull-right">
            <li>
              <a href="/admin/consultant/activity-management/staging/detail/${value}"><i class="fa fa-eye"></i>See Detail Stage</a>
            </li>`;

      @permission('access-activity-management')
        dropdown += `<li>
              <a class="button-edit" data-index="${index}" data-id="${row.id}" data-toggle="modal" data-target="#modal-form"><i class="fa fa-edit"></i>Edit</a>
            </li>`;
      @endpermission

      @permission('access-delete-activity-management')
        dropdown += `<li>
              <a class="delete-stage" data-stage-id="${value}" href=""><i class="fa fa-trash"></i>Delete Stage</a>
            </li>`;
      @endpermission

      dropdown += `</ul>
        </div>`;

      return dropdown;
    }

    $(document).ready(function() {
      let $stage_phase = $('#stagePhase');
      let $stage_name = $('#stageName');
      let $stage_description = $('#stageDescription');

      let $table = $('#staging-list');

      let index = null;

      let rowId = null;

      $stage_phase.val('');
      $stage_name.val('');
      $stage_description.val('');


      $table.on('click', '.button-edit', function() {
        index = $(this).data('index');
        let getData = $table.bootstrapTable('getData');
        let getDataRow = getData[index];

        rowId = $(this).data('id');

        $stage_phase.val(getDataRow.stage_phase);
        $stage_name.val(getDataRow.stage_name);
        $stage_description.val(getDataRow.description);
      });


      $('[data-click="edit-staging"]').on('click', function() {

        let phaseVal = $stage_phase.val();
        let nameVal = $stage_name.val();
        let descriptionVal = $stage_description.val();

        if (phaseVal < 0 || phaseVal > 10 || isNaN(parseInt(phaseVal)) || !(nameVal.length) ||!(descriptionVal.length)) {
          return falseindex;
        } else {
          $('#modal-form').modal('hide');

          Swal.fire({
            title: 'Edit this Stage?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Save',
            cancelButtonText: 'Cancel',
            preConfirm: function() {
              return axios.post(`/admin/consultant/activity-management/update/staging/${rowId}`, {
                stage: phaseVal,
                name: nameVal,
                detail: descriptionVal
              })
              .then(response => {
                if (!response.data.status) {
                  throw new Error(response.data.meta.message)
                }
                return response;
              })
              .catch(error => {
                Swal.showValidationMessage(
                  `Request failed: ${error}`
                )
              })
            },
            allowOutsideClick: () => !Swal.isLoading()
          })
          .then((result) => {
            if (result.value) {
              $table.bootstrapTable('refresh');

              Swal.fire({
                icon: 'success',
                text: 'Edited Stages Saved'
              });

              $stage_phase.val('');
              $stage_name.val('');
              $stage_description.val('');
            }
          });

        }

      });

      $('#staging-list').on('click', '.delete-stage', function(e) {
        e.preventDefault();
        const stageId = parseInt($(this).data('stage-id'));

        Swal.fire({
          title: 'Are you sure?',
          text: 'This stage will be deleted from current funnel',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Delete',
          confirmButtonColor: '#c9302c',
          cancelButtonText: 'Cancel',
          preConfirm: function() {
            return axios.post(`/admin/consultant/activity-management/delete/staging/${stageId}`)
            .then(response => {
              if (!response.data.status) {
                throw new Error(response.data.meta.message)
              }
              return response;
            })
            .catch(error => {
              Swal.showValidationMessage(
                `Request failed: ${error}`
              )
            })
          },
          allowOutsideClick: () => !Swal.isLoading()
        }).then((result) => {
          if (result.value) {
            Swal.fire({
              icon: 'success',
              text: 'Stage deleted'
            });

            $('#staging-list').bootstrapTable('refresh');
          }
        });
      });

    });
  </script>
@endsection