@extends('admin.layouts.main')

@section('style')
    <style>
        div.required-field label::after {
            content: "*";
            color: red;
        }
    </style>

    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet"/>
    <link href="//cdn.jsdelivr.net/npm/select2-bootstrap-theme@0.1.0-beta.10/dist/select2-bootstrap.min.css"
          rel="stylesheet"/>
    <link rel="stylesheet" href="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.css">
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
@endsection

@section('content')
    <div class="box box-default">
        <!-- Box Header -->
        <div class="box-header">
            <div class="col-md-12 text-center">
                <h3 class="box-title"><i class="fa fa-tasks"></i> {{ !isset($edit)?'Create New Staging':'Edit Staging' }}
                </h3>
            </div>
        </div>
        <!-- /.box-header -->

        <div class="box-body">
            <!-- Content -->
        </div>
    </div>
@endsection

@section('script')
    <script src="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
@endsection