@extends('admin.layouts.main')

@section('style')
  <style>
    div.required-field label::after {
      content: "*";
      color: red;
    }

    .funnel-title {
      border: 1px solid #ccc;
      padding: 15px;
      margin-top: 10px;
    }

    .funnel-title div {
      font-size: 16px;
    }

    .funnel-title div span {
      display: block;
      font-size: 14px;
    }

    .funnel-title div + div {
      padding-top: 10px;
    }

    .form-horizontal {
      margin-top: 20px;
    }

    .modal {
      background: rgba(255, 255, 255, .4);
    }

    textarea {
      resize: vertical;
    }

    .swal2-container .swal2-popup {
      font-size: 1.6rem;
    }

    .swal2-container .swal2-icon.swal2-warning::before {
      content: '';
    }

    td .btn + .btn {
      margin-left: 4px;
    }
  </style>

  <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
  <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" />
  <link href="//cdn.jsdelivr.net/npm/select2-bootstrap-theme@0.1.0-beta.10/dist/select2-bootstrap.min.css"
    rel="stylesheet" />
  <link rel="stylesheet" href="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.css">
  <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
@endsection

@section('content')
  <div class="box box-default">
    <!-- Box Header -->
    <div class="box-header" style="padding-top: 10px;">
      <div class="col-md-6 text-center">
        <h3 class="box-title" style="padding-left: 0;"><i class="fa fa-list"></i>&nbsp;Detail Staging</h3>
      </div>
      <div class="col-md-6">
        @permission('access-activity-management')
          <a class="btn btn-primary pull-right" style="margin-top: 5px; color: #fff;" href="{{ url("admin/consultant/activity-management/create/forms/$stage_id") }}"><i class="fa fa-plus"></i> Add Form Field</a>
        @endpermission
      </div>

      <div class="col-sm-12">
        <div class="row">
          <div class="col-xs-6">
            <div class="funnel-title">
              <div>
                <span>Funnel Name : </span>
                <strong>{{ $funnel_name }}</strong>
              </div>
              <div>
                <span>Consultant Roles : </span>
                <strong>{{ $consultant_role }}</strong>
              </div>
              <div>
                <span>Data Type : </span>
                @if ($data_type === "consultant_tools_potential_tenant")
                  <strong>DBET</strong>

                @elseif ($data_type === "designer")
                  <strong>Property</strong>

                @elseif ($data_type === "mamipay_contract")
                  <strong>Contract</strong>
                @elseif ($data_type === "consultant_potential_property")
                  <strong>Potential Property</strong>
                @elseif ($data_type === "consultant_potential_owner")
                  <strong>Potential Owner</strong>
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-12">
        <div class="row">
          <form class="form-horizontal">
            <div class="form-group form-group-sm">
              <label class="col-sm-12" for="stageName">Stage Name</label>
              <div class="col-sm-6">
                <select class="form-control" id="stageName">
                  @foreach($all_stage as $stage)
                    <option value="{{ $stage['id'] }}" {{ ($stage_id===$stage['id']) ? 'selected' : '' }}>{{ $stage['name'] }}</option>
                  @endforeach
                </select>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!-- /.box-header -->

    <div class="box-body no-padding no-overflow">
      <!-- Content -->
      <div class="horizontal-wrapper" style="margin-top: 15px;">
        <div style="padding: 15px;">

          <table
            id="form-list"
            data-toggle="table"
            data-side-pagination="server"
            data-pagination="true"
            data-url="{{ url("admin/consultant/activity-management/forms/$stage_id") }}"
            data-data-field="forms"
          >
            <thead>
              <tr>
              <th data-field="order">
                  Order Form
                </th>
                <th data-field="label">
                  Field Label
                </th>
                <th data-field="type">
                  Input Type
                </th>
                <th data-field="placeholder">
                  Input Placeholder
                </th>
                <th data-field="values">
                  Input Value
                </th>
                <th data-field="id" data-formatter="actionFormatter">
                  Action
                </th>
              </tr>
            </thead>
          </table>

        </div>
      </div>
    </div>
  </div>

  <!-- Modal Created -->
  <div class="modal fade" id="modal-form">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
          <h4 class="modal-title">Edit Form Input</h4>
        </div>

          <div class="modal-body">
            <div class="form-group">
              <label for="orderForm">Order Form</label>
              <div class="row">
                <div class="col-sm-12">
                  <input name="order_form" id="orderForm" class="form-control" type="number" min="1" max="10" placeholder="1-10" required>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label for="fieldLabel">Field Label</label>
              <div class="row">
                <div class="col-sm-12">
                  <input type="text" class="form-control" id="fieldLabel" name="field_label" placeholder="ex: Jam Berkunjung" required value="">
                </div>
              </div>
            </div>

            <div class="form-group">
              <label for="inputType">Input Type</label>
              <div class="row">
                <div class="col-sm-12">
                  <select name="input_type" id="inputType" required>
										<option value="" disabled>Choose Input Type</option>
										<option value="time">Hours Minutes</option>
										<option value="date">Date</option>
										<option value="text">Text</option>
										<option value="text_area">Textarea</option>
										<option value="number">Number</option>
										<option value="radio">Radio</option>
										<option value="select">Select Dropdown</option>
										<option value="file">File</option>
										<option value="checkbox">Checkbox</option>
									</select>
                </div>
              </div>
            </div>

						<div class="form-group input-placeholder-container">
              <label for="inputPlaceholder">Input Placeholder</label>
              <div class="row">
                <div class="col-sm-12">
                  <input type="text" class="form-control" id="inputPlaceholder" name="input_placeholder" placeholder="" required value="">
                </div>
              </div>
            </div>

						<div class="form-group input-value-container" style="display: none;">
							<label for="inputValue">Input Value</label>
							<div class="row">
								<div class="col-sm-12">
									<p class="help-block" style="padding-bottom: 2px; margin: 0;">Separate your input value using comma (,)</p>
									<input type="text" class="form-control" id="inputValue" name="input_value" placeholder="Ex: ya; tidak" value="">
								</div>
							</div>
						</div>
          </div>

          <div class="modal-footer">
            <button class="btn btn-success" type="button" data-click="edit-form-input">Edit Form Input</button>
          </div>

      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->
@endsection

@section('script')
  <script src="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
  <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

  <script>
    function actionFormatter(value, row, index) {
      let dropdown = ``;

      @permission('access-activity-management')
        dropdown += `<a class="btn btn-warning button-edit" data-toggle="modal" data-target="#modal-form" data-index="${index}" data-id="${row.id}"><i class="fa fa-edit"></i></a>`;
      @endpermission

      @permission('access-delete-activity-management')
        dropdown += `<a class="btn btn-danger delete-form" href="" data-form-id="${value}"><i class="fa fa-trash"></i></a>`;
      @endpermission

      return dropdown;
    }

    $(document).ready(function () {
      $('[data-toggle="tooltip"]').tooltip();

      let $order_form = $('#orderForm');
      let $field_label = $('#fieldLabel');
      let $input_type = $('#inputType');
      let $input_value = $('#inputValue');
      let $input_placeholder = $('#inputPlaceholder');

      let $table = $('#form-list');

      let index = null;

      let rowId = null;

      let indexIncrement = 0;

      $order_form.val('');
      $field_label.val('');
      $input_type.val('');
      $input_value.val('');
      $input_placeholder.val('');


      function trimAndSeparate(input) {
        const values = input.split(',');
        for (let i = 0; i < values.length; i++) {
          values[i] = values[i].trim();
        }
        return values;
      }


      $table.on('click', '.button-edit', function() {
        index = $(this).data('index');
        console.log('test', $(this));
        let getData = $table.bootstrapTable('getData');
        let getDataRow = getData[index];

        rowId = $(this).data('id');

        $order_form.val(getDataRow.order);
        $field_label.val(getDataRow.label);
        $input_type.val(getDataRow.type);
        $input_placeholder.val(getDataRow.placeholder);
        $input_value.val(getDataRow.values);

        if (
            $input_type.val() === 'radio' ||
            $input_type.val() === 'select' ||
            $input_type.val() === 'checkbox'
          ) {
          $('.input-value-container').show();
        }
      });

			$('#inputType').on('input', function() {
				if (
					$(this).val() === 'radio' ||
					$(this).val() === 'select' ||
					$(this).val() === 'checkbox'
				) {
					$('.input-value-container').show();
				} else {
					$('.input-value-container').hide();
				}
			});

      $('[data-click="edit-form-input"]').on('click', function() {

        let orderVal = $order_form.val();
        let labelVal = $field_label.val();
        let typeVal = $input_type.val();
        let valueVal = $input_value.val();
        let placeholderVal = $input_placeholder.val();

        if (orderVal < 0 || orderVal > 10 || isNaN(parseInt(orderVal)) || !(labelVal.length) ||!(typeVal.length)) {
          return falseindex;
        } else {
          $('#modal-form').modal('hide');

          Swal.fire({
            title: 'Edit this form input?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Save',
            cancelButtonText: 'Cancel',
            preConfirm: function() {
              return axios.patch(`/admin/consultant/activity-management/forms/{{ $stage_id }}`, {
                id: rowId,
                order: orderVal,
                label: labelVal,
                type: typeVal,
                values: trimAndSeparate(valueVal),
                placeholder: placeholderVal
              })
              .then(response => {
                if (!response.data.status) {
                  throw new Error(response.data.meta.message)
                }
                return response;
              })
              .catch(error => {
                Swal.showValidationMessage(
                  `Request failed: ${error}`
                )
              })
            },
            allowOutsideClick: () => !Swal.isLoading()
          })
          .then((result) => {
            if (result.value) {
              $table.bootstrapTable('refresh');

              Swal.fire({
                icon: 'success',
                text: 'Edited Form Input Saved'
              });

              $order_form.val('');
              $field_label.val('');
              $input_type.val('');
              $input_value.val('');
              $input_placeholder.val('');
            }
          });
        }

      });

      $('#form-list').on('click', '.delete-form', function(e) {
        e.preventDefault();
        const formId = parseInt($(this).data('form-id'));

        Swal.fire({
          title: 'Are you sure?',
          text: 'This form will be deleted from current staging',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Delete',
          confirmButtonColor: '#c9302c',
          cancelButtonText: 'Cancel',
          preConfirm: function() {
            return axios.post(`/admin/consultant/activity-management/delete/forms/{{ $stage_id }}/${formId}`)
            .then(response => {
              if (!response.data.status) {
                throw new Error(response.data.meta.message)
              }
              return response;
            })
            .catch(error => {
              Swal.showValidationMessage(
                `Request failed: ${error}`
              )
            })
          },
          allowOutsideClick: () => !Swal.isLoading()
        }).then((result) => {
          if (result.value) {
            Swal.fire({
              icon: 'success',
              text: 'Form deleted'
            });

            $('#form-list').bootstrapTable('refresh');
          }
        });
      });

      $('#stageName').on('input', function() {
        $(this).prop('disabled', true);
        window.location.href = "{{ url("/admin/consultant/activity-management/staging/detail") }}" + `/${$(this).val()}`;
      });

    });
  </script>
@endsection