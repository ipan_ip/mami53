@extends('admin.layouts.main')

@section('style')
    <style>
        /* Select2 tweak */
        .input-group .select2-container,
        .form-group .select2-container {
            position: relative !important;
            z-index: 2;
            float: left !important;
            width: 100% !important;
            margin-bottom: 0 !important;
            display: table !important;
            table-layout: fixed !important;
        }

        div.required-field label::after {
            content: "*";
            color: red;
        }
    </style>

    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet"/>
    <link href="//cdn.jsdelivr.net/npm/select2-bootstrap-theme@0.1.0-beta.10/dist/select2-bootstrap.min.css"
          rel="stylesheet"/>
    <link rel="stylesheet" href="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.css">
@endsection

@section('content')
    <div class="box box-default">
        <!-- Box Header -->
        <div class="box-header">
            <div class="col-md-12 text-center">
                <h3 class="box-title"><i class="fa fa-check-square-o"></i> {{ $contentHeader }}</h3>
            </div>
        </div>
        <!-- /.box-header -->

        <div class="box-body no-padding">

            {{-- Top Bar --}}
            <div class="horizontal-wrapper">

                <div class="btn-horizontal-group bg-default form-inline"
                     style="padding-left: 10px;padding-right: 10px;">
                    {{-- Table --}}
                    <table class="table" id="potentialTenantTable"
                           data-toggle="table"
                           data-url="{{ url("admin/consultant/potential-tenant/data") }}"
                           data-side-pagination="server"
                           data-pagination="true"
                           data-search="true"
                           data-search-on-enter-key="true"
                    >
                        <thead>
                        <tr>
                            <th data-field="name">Name</th>
                            <th data-field="email">Email</th>
                            <th data-field="phone_number">Phone Number</th>
                            <th data-field="gender">Gender</th>
                            <th data-field="job_information">Job Information</th>
                            <th data-field="property_name" data-formatter="propertyFormatter">Property</th>
                            <th data-field="price">Price</th>
                            <th data-field="due_date">Due Date</th>
                            <th data-field="registered" data-formatter="iconFormatter">Registered</th>
                            <th data-field="active_contract" data-formatter="iconFormatter">Due Date</th>
                        </tr>
                        </thead>
                    </table>
                    <!-- table -->

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
            @endsection

            @section('script')
                <script src="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.js"></script>

                <script>
                    function propertyFormatter(value, row) {
                        return row.property_name+'<br><b>Area City:</b> '+row.area_city+'<br><b>Owner Phone:</b> '+row.owner_phone
                    }

                    function iconFormatter(value, row) {
                        if(value == true || value == 1)
                            return '<span class="label label-success"><i class="fa fa-check"></i></span>';
                        else
                            return '<span class="label label-danger"><i class="fa fa-times"></i></span>';
                    }
                </script>
@endsection
