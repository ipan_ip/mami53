@extends('admin.layouts.main')

@section('style')
  <style>
    div.required-field label::after {
      content: "*";
      color: red;
    }

    .modal {
      background: rgba(255, 255, 255, .4);
    }

    textarea {
      resize: vertical;
    }

    .no-overflow .bootstrap-table .fixed-table-container .fixed-table-body {
      overflow-x: visible;
      overflow-y: visible;
    }

    .swal2-container .swal2-popup {
      font-size: 1.6rem;
    }

    .swal2-container .swal2-icon.swal2-warning::before {
      content: '';
    }
  </style>

  <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
  <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" />
  <link href="//cdn.jsdelivr.net/npm/select2-bootstrap-theme@0.1.0-beta.10/dist/select2-bootstrap.min.css"
    rel="stylesheet" />
  <link rel="stylesheet" href="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.css">
  <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
@endsection

@section('content')
  <div class="box box-default">
    <!-- Box Header -->
    <div class="box-header" style="padding-top: 10px;">
      <div class="col-md-6 text-center">
        <h3 class="box-title">
          <i class="fa fa-tasks"></i> {{ !isset($edit)?'Create New Form':'Edit Form' }}
        </h3>
      </div>

      <div class="col-md-6">
        <button
          class="btn btn-primary pull-right"
          style="margin-top: 5px;"
          data-toggle="modal"
          data-target="#modal-form"
          data-add-edit="add"
        ><i class="fa fa-plus"></i> Add Form</button>
      </div>
    </div>
    <!-- /.box-header -->

    <div class="box-body no-padding no-overflow">
      <!-- Content -->
      <div class="horizontal-wrapper" style="margin-top: 15px;">
        <div style="padding: 15px;">

          <table
            id="table-new-form-input"
            data-toggle="table"
          >
            <thead>
              <tr>
                <th data-field="order">
                  Order Form
                </th>
                <th data-field="label">
                  Field Label
                </th>
                <th data-field="type">
                  Input Type
                </th>
                <th data-field="placeholder">
                  Input Placeholder
                </th>
                <th data-field="values">
                  Input Value
                </th>
                <th data-field="id" data-formatter="actionFormatter">
                  Action
                </th>
              </tr>
            </thead>
          </table>

          <div data-click-show="save-changes" style="margin-top: 30px; display: none;">
            <button class="btn btn-success btn-lg btn-block save-all-form-input">Save Changes</button>
          </div>

        </div>
      </div>
    </div>
  </div>

  <!-- Modal Created -->
  <div class="modal fade" id="modal-form">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
          <h4 class="modal-title"></h4>
        </div>

          <div class="modal-body">
            <div class="form-group">
              <label for="orderForm">Order Form</label>
              <div class="row">
                <div class="col-sm-12">
                  <input name="order_form" id="orderForm" class="form-control" type="number" min="1" max="10" placeholder="1-10" required>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label for="fieldLabel">Field Label</label>
              <div class="row">
                <div class="col-sm-12">
                  <input type="text" class="form-control" id="fieldLabel" name="field_label" placeholder="ex: Jam Berkunjung" required value="">
                </div>
              </div>
            </div>

            <div class="form-group">
              <label for="inputType">Input Type</label>
              <div class="row">
                <div class="col-sm-12">
                  <select name="input_type" id="inputType" required>
										<option value="" disabled>Choose Input Type</option>
										<option value="time">Hours Minutes</option>
										<option value="date">Date</option>
										<option value="text">Text</option>
										<option value="text_area">Textarea</option>
										<option value="number">Number</option>
										<option value="radio">Radio</option>
										<option value="select">Select Dropdown</option>
										<option value="file">File</option>
										<option value="checkbox">Checkbox</option>
									</select>
                </div>
              </div>
            </div>

						<div class="form-group input-placeholder-container">
              <label for="inputPlaceholder">Input Placeholder</label>
              <div class="row">
                <div class="col-sm-12">
                  <input type="text" class="form-control" id="inputPlaceholder" name="input_placeholder" placeholder="" required value="">
                </div>
              </div>
            </div>

						<div class="form-group input-value-container" style="display: none;">
							<label for="inputValue">Input Value</label>
							<div class="row">
								<div class="col-sm-12">
									<p class="help-block" style="padding-bottom: 2px; margin: 0;">Separate your input value using comma (,)</p>
									<input type="text" class="form-control" id="inputValue" name="input_value" placeholder="Ex: ya; tidak" value="">
								</div>
							</div>
						</div>
          </div>

          <div class="modal-footer">
            <button class="btn btn-success" type="button" data-click="add-new-form-input" style="display: none;">Add New Form Input</button>
            <button class="btn btn-success" type="button" data-click="edit-form-input" style="display: none;">Edit Form Input</button>
          </div>

      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

@endsection

@section('script')
  <script src="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
  <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

  <script>
    function actionFormatter(value, row, index) {
      return `<button class="btn btn-warning button-edit" data-index="${index}" data-row-index="${row.index}" data-toggle="modal" data-target="#modal-form" data-add-edit="edit"><i class="fa fa-edit"></i></button>
              <button class="btn btn-danger button-delete" data-index="${index}" data-row-index="${row.index}"><i class="fa fa-trash"></i></button>`
    }

    $(document).ready(function() {

      let $order_form = $('#orderForm');
      let $field_label = $('#fieldLabel');
      let $input_type = $('#inputType');
      let $input_value = $('#inputValue');
      let $input_placeholder = $('#inputPlaceholder');

      let $addButton = $('[data-click="add-new-form-input"]');
      let $editButton = $('[data-click="edit-form-input"]');

      let $table = $('#table-new-form-input');

      let index = null;

      let indexIncrement = 0;

      $order_form.val('');
      $field_label.val('');
      $input_type.val('');
      $input_value.val('');
      $input_placeholder.val('');

      function trimAndSeparate(input) {
        const values = input.split(',');
        for (let i = 0; i < values.length; i++) {
          values[i] = values[i].trim();
        }
        return values;
      }


      $('#modal-form').on('show.bs.modal', function(e) {
        let button = $(e.relatedTarget);
        let recipent = button.data('add-edit');

        if (recipent === 'add') {
          $addButton.show();
          $(this).find('.modal-title').text('Add New Form Input');
        }

        if (recipent === 'edit') {
          $editButton.show();
          $(this).find('.modal-title').text('Edit Form Input');
        }
      });


      // hide all button modal form
      $('#modal-form').on('hide.bs.modal', function(e) {
        $addButton.hide();
        $editButton.hide();
        index = null;

        $order_form.val('');
        $field_label.val('');
        $input_type.val('');
        $input_value.val('');
        $input_placeholder.val('');
      });


      $table.on('click', '.button-edit', function() {
        index = $(this).data('index');
        let getData = $table.bootstrapTable('getData');
        let getDataRow = getData[index];

        $order_form.val(getDataRow.order);
        $field_label.val(getDataRow.label);
        $input_type.val(getDataRow.type);
        $input_placeholder.val(getDataRow.placeholder);
        $input_value.val(getDataRow.values);

        if (
            $input_type.val() === 'radio' ||
            $input_type.val() === 'select' ||
            $input_type.val() === 'checkbox'
          ) {
          $('.input-value-container').show();
        }
      });


      $table.on('click', '.button-delete', function() {
        let rowIndex = $(this).data('row-index');

        $table.bootstrapTable('remove', {
          field: 'index',
          values: [rowIndex]
        })
      });

			$('#inputType').on('input', function() {
				if (
					$(this).val() === 'radio' ||
					$(this).val() === 'select' ||
					$(this).val() === 'checkbox'
				) {
					$('.input-value-container').show();
				} else {
					$('.input-value-container').hide();
				}
			});


      $('[data-click="add-new-form-input"]').on('click', function() {

        let orderVal = $order_form.val();
        let labelVal = $field_label.val();
        let typeVal = $input_type.val();
        let valueVal = $input_value.val();
        let placeholderVal = $input_placeholder.val();

        if (orderVal < 0 || orderVal > 10 || isNaN(parseInt(orderVal)) || !(labelVal.length) ||!(typeVal.length)) {
          return false;
        } else {

          $table.bootstrapTable('insertRow', {
            index: 0,
            row: {
              index: indexIncrement,
              order: orderVal,
              label: labelVal,
              type: typeVal,
							values: trimAndSeparate(valueVal),
							placeholder: placeholderVal
            }
          });

          $order_form.val('');
          $field_label.val('');
          $input_type.val('');
          $input_value.val('');
          $input_placeholder.val('');

          indexIncrement++;

          $('#modal-form').modal('hide');

          $('[data-click-show="save-changes"]').fadeIn();
        }

      });

      $('[data-click="edit-form-input"]').on('click', function() {

        let orderVal = $order_form.val();
        let labelVal = $field_label.val();
        let typeVal = $input_type.val();
        let valueVal = $input_value.val();
        let placeholderVal = $input_placeholder.val();

        if (orderVal < 0 || orderVal > 10 || isNaN(parseInt(orderVal)) || !(labelVal.length) ||!(typeVal.length)) {
          return falseindex;
        } else {

          $table.bootstrapTable('updateRow', {
            index: index,
            row: {
              order: orderVal,
              label: labelVal,
              type: typeVal,
							values: trimAndSeparate(valueVal),
							placeholder: placeholderVal
            }
          });

          $order_form.val('');
          $field_label.val('');
          $input_type.val('');
          $input_value.val('');
          $input_placeholder.val('');

          $('#modal-form').modal('hide');

          $('[data-click-show="save-changes"]').fadeIn();
        }

      });

      $('.save-all-form-input').on('click', function() {
        Swal.fire({
          title: 'Save New Form Input?',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Save',
          cancelButtonText: 'Cancel',
          preConfirm: function() {
            return axios.post('/admin/consultant/activity-management/forms/{{ $stage_id }}', {
              fields: $table.bootstrapTable('getData')
            })
            .then(response => {
              if (!response.data.status) {
                throw new Error(response.data.meta.message)
              }
              return response;
            })
            .catch(error => {
              Swal.showValidationMessage(
                `Request failed: ${error}`
              )
            })
          },
          allowOutsideClick: () => !Swal.isLoading()
        })
        .then((result) => {
          if (result.value) {

            Swal.fire({
              icon: 'success',
              text: 'New Form Input Saved',
              showConfirmButton: false,
              allowOutsideClick: false
            });

            window.location.href = '{{ url("/admin/consultant/activity-management/staging/detail/$stage_id") }}'
          }
        });
      });

    });
  </script>
@endsection