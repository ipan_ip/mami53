@extends('admin.layouts.main')

@section('style')
  <style>
    div.required-field label::after {
      content: "*";
      color: red;
    }

    .form-group {
      margin-bottom: 20px;
    }
  </style>

  <link href="//cdn.jsdelivr.net/npm/select2-bootstrap-theme@0.1.0-beta.10/dist/select2-bootstrap.min.css"
    rel="stylesheet" />
@endsection

@section('content')
  <div class="box box-default">
    <!-- Box Header -->
    <div class="box-header" style="padding-top: 10px;">
      <div class="col-md-12 text-center">
        <h3 class="box-title" style="padding: 0;"><i class="fa fa-tasks"></i> {{ !isset($edit)?'Create New Funnel':'Edit Funnel' }}
        </h3>
      </div>
    </div>
    <!-- /.box-header -->

    <div class="box-body" style="padding: 30px 15px; 10px">
      <!-- Content -->
      <form action="{{ !isset($edit) ? url("admin/consultant/activity-management/store/funnel") : url("admin/consultant/activity-management/update/funnel/$funnel->id") }}" method="post">
        <div class="row">
          <div class="col-sm-12">

            <div class="form-group">
              <label for="name">Funnel Name</label>
              <div class="row">
                <div class="col-sm-4">
                  <input type="text" class="form-control" id="name" name="name" placeholder="ex: Visit Kos" required value="{{ !isset($edit) ? '' : $funnel->name }}">
                </div>
              </div>
            </div>

            <div class="form-group">
              <label for="relatedTable">Data Type</label>
              <div class="row">
                <div class="col-sm-4">
                  <select class="form-control" id="relatedTable" name="relatedTable" required>
                    <option value="" disabled {{ isset($edit) ? '' : 'selected' }}>Choose data type</option>
                    <option value="consultant_tools_potential_tenant" {{ (isset($edit) && $funnel->related_table === 'consultant_tools_potential_tenant') ? 'selected' : '' }}>DBET</option>
                    <option value="designer" {{ (isset($edit) && $funnel->related_table === 'designer') ? 'selected' : '' }}>Property</option>
                    <option value="mamipay_contract" {{ (isset($edit) && $funnel->related_table === 'mamipay_contract') ? 'selected' : '' }}>Contract</option>
                    <option value="consultant_potential_property" {{ (isset($edit) && $funnel->related_table === 'consultant_potential_property') ? 'selected' : '' }}>Potential Property</option>
                    <option value="consultant_potential_owner" {{ (isset($edit) && $funnel->related_table === 'consultant_potential_owner') ? 'selected' : '' }}>Potential Owner</option>
                  </select>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label for="role">Consultant Role</label>
              <div class="row">
                <div class="col-sm-4">
                  <select class="form-control" id="role" name="role" required>
                    <option value="" disabled {{ isset($edit) ? '' : 'selected' }}>Choose role</option>
                    <option value="admin" {{ (isset($edit) && $funnel->consultant_role === 'admin') ? 'selected' : '' }}>Admin</option>
                    <option value="supply" {{ (isset($edit) && $funnel->consultant_role === 'supply') ? 'selected' : '' }}>Supply</option>
                    <option value="demand" {{ (isset($edit) && $funnel->consultant_role === 'demand') ? 'selected' : '' }}>Demand</option>
                  </select>
                </div>
              </div>
            </div>

            <div style="padding-top: 15px;">
              <button class="btn btn-success" type="submit">{{ !isset($edit) ? 'Save New Funnel' : 'Edit Funnel' }}</button>
            </div>

          </div>
        </div>
      </form>
    </div>
  </div>
@endsection

@section('script')
  <script src="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.js"></script>
@endsection