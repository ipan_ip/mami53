@extends('admin.layouts.main')

@section('style')
    <style>
        /* Select2 tweak */
        .input-group .select2-container,
        .form-group .select2-container {
            position: relative !important;
            z-index: 2;
            float: left !important;
            width: 100% !important;
            margin-bottom: 0 !important;
            display: table !important;
            table-layout: fixed !important;
        }

        div.required-field label::after {
            content: "*";
            color: red;
        }
    </style>

    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet"/>
    <link href="//cdn.jsdelivr.net/npm/select2-bootstrap-theme@0.1.0-beta.10/dist/select2-bootstrap.min.css"
          rel="stylesheet"/>
    <link rel="stylesheet" href="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.css">
@endsection

@section('content')
    <div class="box box-default">
        <!-- Box Header -->
        <div class="box-header">
            <div class="col-md-6 text-center">
                <h3 class="box-title"><i class="fa fa-check-square-o"></i> {{ $contentHeader }}</h3>
            </div>

            @permission('access-mapping-system')
                <div class="col-md-6">
                    <button class="btn btn-primary pull-right" style="margin-top: 5px;" data-toggle="modal" data-target="#addModal"><i class="fa fa-plus"></i> Add New Mapping</button>
                </div>
            @endpermission
        </div>
        <!-- /.box-header -->

        <div class="box-body">
            {{-- Custom Search Form --}}
            <form id="searchForm" class="form-inline" autocomplete="off" style="margin-bottom: 10px;">
                <div class="form-group col-2 col-sm-2">
                    <label for="">Search By:</label>
                    <select name="searchBy" class="form-control" id="searchBy">
                        <option value="consultant" selected>Consultant name</option>
                        <option value="property">Property name</option>
                        <option value="area">Area city</option>
                    </select>
                </div>
                <input type="text" id="search" name="search" class="form-control col-2 col-sm-2"
                       id="search" placeholder="Search..">
                <button id="btnSearch" type="submit" class="btn btn-primary col-1 col-sm-1" style="margin-left: 10px"><i
                            class="fa fa-search"></i> Search
                </button>
            </form>

            {{-- Top Bar --}}
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default form-inline"
                     style="padding-left: 10px;padding-right: 10px;">
                    {{-- Table --}}
                    <table class="table" id="consultantMappingTable"
                           data-toggle="table"
                           data-url="{{ url("admin/consultant/room-mapping/data") }}"
                           data-side-pagination="server"
                           data-pagination="true"
                           data-query-params="searchQuery"
                    >
                        <thead>
                        <tr>
                            <th data-field="consultant">Consultant</th>
                            <th data-field="area">Area</th>
                            <th data-field="property_mapped">Property Mapped</th>
                            <th data-field="consultant_id" data-formatter="actionFormatter">Action</th>
                        </tr>
                        </thead>
                    </table>
                    <!-- table -->
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

            <!-- Modal -->
            <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Add New Mapping</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form id="frmAdd" action="{{ url('admin/consultant/room-mapping/add') }}">
                                <div class="form-group row">
                                    <label for="addConsultant" class="col-sm-2 col-form-label">Consultant</label>
                                    <div class="col-sm-10">
                                        <select name="consultant" id="addConsultant" class="select2">
                                            @foreach($consultants as $consultant)
                                                <option value="{{ $consultant->id }}">{{ $consultant->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="addAreaCity" class="col-sm-2 col-form-label">Area City</label>
                                    <div class="col-sm-10">
                                        <select name="areaCity" id="addAreaCity" class="select2">
                                            @foreach($cities as $city)
                                                <option value="{{ $city->area_city }}">{{ $city->area_city }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" id="btnAdd" class="btn btn-primary">Add</button>
                        </div>
                    </div>
                </div>
            </div>
            @endsection

            @section('script')
                <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
                <script src="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.js"></script>
                <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
                <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
                <script>
                    $(function () {
                        // Initiate table once page has rendered
                        $('#consultantMappingTable').bootstrapTable({

            }).on('post-body.bs.table', function () {
                // need to re-render tooltip when table is modified/sorted
                $('[data-toggle="tooltip"]').tooltip()
            });

                        $('[data-toggle="tooltip"]').tooltip()

                        $('.select2').select2({
                            dropdownParent: $('#addModal')
                        });

                        $('#btnAdd').on('click',function () {
                            $('#frmAdd').submit();
                        })
                    });

        function actionFormatter(value, row) {
            let menu = `      <a class="btn btn-primary" href="/admin/consultant/room-mapping/detail/${value}/${row.area}"  data-toggle="tooltip" data-placement="top" title="Detail"><i class="fa fa-eye"></i></a> `;

            @permission('access-mapping-system')
                menu = menu + `      <a class="btn btn-warning" href="/admin/consultant/room-mapping/detail/edit/${value}/${row.area}" data-toggle="tooltip" data-placement="top" title="Edit Mapping"><i class="fa fa-pencil-square-o"></i></a> ` +
                `      <a class="btn btn-warning" href="#" data-toggle="tooltip" data-placement="top" title="Edit Consultant" onclick="reassignConsultant(${value}, '${row.area}')"><i class="fa fa-user"></i></a> ` +
                `      <a class="btn btn-danger" href="#" data-toggle="tooltip" data-placement="top" title="Delete Mapping" onclick="deleteMappingByArea(${value},'${row.area}')"><i class="fa fa-trash"></i></a>`
            @endpermission

            return menu;
        }

        function searchQuery(params){
            params.search = $('#search').val();
            params.searchBy = $('#searchBy option:selected').val();

            return params;
        }

        $('#searchForm').submit(function(e){
            e.preventDefault();

            $('#consultantMappingTable').bootstrapTable('refresh')
        })

        function reassignConsultant(value, area_city) {
            Swal.fire({
                title: 'Enter consultant name',
                html: `<input class="form-control" type="text" id="consultant-name" data-consultant-id="${value}" data-area-city="${area_city}" data-toggle="dropdown">`,
                showCancelButton: true,
                showLoaderOnConfirm: true,
                preConfirm: (login) => {
                    return axios.post('/admin/consultant/room-mapping/reassign', {
                        old_consultant_id: Number($('#consultant-name').attr('data-consultant-id')),
                        new_consultant_id: Number($('#consultant-name').attr('data-new-consultant-id')),
                        area_city: $('#consultant-name').attr('data-area-city')
                    })
                        .then(response => {
                            if (!response.status) {
                                throw new Error(response.meta.message);
                            }

                            return response;
                        })
                        .catch(error => {
                            Swal.showValidationMessage(
                                `Request failed: ${error}`
                            )
                        })
                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                if (result.value) {
                    Swal.fire({
                        icon: 'success',
                        text: 'Mapping has been reassigned'
                    });

                    window.location.reload();
                }
            })
        }

        var currentlyQuerying;

        $(document).on('keyup', '#consultant-name', function () {
            clearTimeout(currentlyQuerying); // If the user continue typing, cancel our query.
            currentlyQuerying = setTimeout(queryConsultant, 700); // Wait for 2 seconds then query the consultant name
        });

        $(document).on('keydown', '#consultant-name', function () {
            clearTimeout(currentlyQuerying); // If the user continue typing, cancel our query.
        });

        function queryConsultant() {
            let consultantName = $('#consultant-name').val();

            $.ajax({
                type: 'GET',
                url: `/admin/consultant/room-mapping/consultant?search=${consultantName}`,
                success: function(data) {
                    $('#consultant-name').next('.dropdown-menu').remove();

                    listOption = '<ul class="dropdown-menu suggestion-list" aria-labelledby="dLabel">'
                    for(consultant of data.data) {
                        listOption += '<li><a href="#" class="suggestion-item suggestion-item-' + consultant.id + '" data-key="' + consultant.id + '" onclick="clickSuggestion(' + consultant.id + ')">' + consultant.name + '</a></li>';
                    }

                    listOption += '</ul>';

                    $(listOption).insertAfter($('#consultant-name'));

                    $('.suggestion-list').dropdown();
                }
            });
        }

        function clickSuggestion(id) {
            val = $('.suggestion-item-' + id).text();

            $('#consultant-name').val(val);
            $('#consultant-name').attr('data-new-consultant-id', id);
        }

        function deleteMappingByArea(consultant,area){
            Swal.fire({
                title: 'Are you sure?',
                text: "You will delete this mapping!",
                icon: 'warning',
                showCancelButton: true,
                showLoaderOnConfirm: true,
                preConfirm: function() {
                    return axios.post('/admin/consultant/room-mapping/area', {
                        consultant: consultant,
                        area_city: area
                    })
                        .then(response => {
                            if (!response.status) {
                                throw new Error(response.meta.message);
                            }

                            return response;
                        })
                        .catch(error => {
                            Swal.showValidationMessage(
                                `Request failed: ${error}`
                            )
                        })
                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                if (result.value) {
                    Swal.fire({
                        icon: 'success',
                        text: 'Mapping successfully deleted'
                    });

                    $('#consultantMappingTable').bootstrapTable('refresh')
                }
            })
        }
    </script>
@endsection
