@extends('admin.layouts.main')

@section('style')
    <link rel="stylesheet" href="/css/admin-consultant.css">

    <style>
        /* Select2 tweak */
        .input-group .select2-container,
        .form-group .select2-container {
            position: relative !important;
            z-index: 2;
            float: left !important;
            width: 100% !important;
            margin-bottom: 0 !important;
            display: table !important;
            table-layout: fixed !important;
        }
        div.required-field label::after {
            content: "*";
            color: red;
        }

        .mapping-error-message {
            display: none;
            border: 1px solid #fa6155;
            background-color: #fa6155;
            color: #fff;
            border-radius: 3px;
            padding: 6px;
            text-align: center;
            margin-bottom: 10px;
            font-size: 14px;
        }

        .icheckbox_minimal {
            vertical-align: text-bottom;
        }

        .icheckbox_minimal input[type="checkbox"] {
            margin-left: 0;
        }
    </style>

    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" />
    <link href="//cdn.jsdelivr.net/npm/select2-bootstrap-theme@0.1.0-beta.10/dist/select2-bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="/css/admin-consultant.css">
@endsection

@section('content')
    <div class="box box-default">
        <!-- Box Header -->
        <div class="box-header">
            <div class="col-md-12 text-center">
                <h3 class="box-title"><i class="fa fa-check-square-o"></i> {{ $contentHeader }}</h3>
            </div>
        </div>
        <!-- /.box-header -->

        <div class="box-body no-padding">

            {{-- Top Bar --}}
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default form-inline">
                    <!-- Add button -->
                    <div class="row">
                        <div class="col-md-6" style="padding-bottom:10px;">
                            <div style="padding-left: 10px;">
                                <a href="#" class="btn btn-success btn pull-left actions" data-toggle="modal" data-target="#modal-default">
                                    <i class="fa fa-plus">&nbsp;</i>Add Mapping
                                </a>
                            </div>
                        </div>
                        <!-- Search filters -->
                        <div class="col-md-6" style="padding-bottom:10px;text-align:right">
                            <div style="padding-right: 10px;">
                                <form action="{{ url('/admin/consultant/mapping') }}" method="get">
                                    <input type="text" name="name" class="form-control input-sm" placeholder="Name" autocomplete="off" value="{{ request()->input('name') }}">
                                    <input type="text" name="city" class="form-control input-sm" placeholder="City" autocomplete="off" value="{{ request()->input('city') }}">
                                    <button type="submit" class="btn btn-primary btn-md" id="buttonSearch"><i class="fa fa-search">&nbsp;</i>Search</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {{-- Table --}}
            <div style="padding: 0 10px;">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Consultant</th>
                            <th>Area</th>
                            <th class="text-center">Mamirooms</th>
                            <th class="text-center">Non-Mamirooms</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($mappings as $mapping)
                        <tr>
                            <td>{{ $mapping->consultant->name }}</td>
                            <td>{{ $mapping->area_city }}</td>
                            @if($mapping->is_mamirooms)
                                <td class="text-center">
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                </td>
                                <td></td>
                            @endif
                            @if(!$mapping->is_mamirooms)
                                <td></td>
                                <td class="text-center">
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                </td>
                            @endif
                            <td class="table-action-column text-center" style="padding-right:15px!important;">
                                <div class="btn-group">
                                    <button type="button" id="updateMapping" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                        <i class="glyphicon glyphicon-cog"></i> Actions <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu pull-right">
                                        <li>
                                            <a href="#" class="actions edit-update-data" data-toggle="modal" data-target="#modal-update{{ $mapping->id }}" data-id="{{ $mapping->id }}"><i class="glyphicon glyphicon-edit"></i> Edit Profil</a>
                                        </li>
                                        <li>
                                            <a href="#" class="actions btn-delete-mapping" data-id = "{{ $mapping->id }}"><i class="glyphicon glyphicon-trash"></i> Hapus Profil</a>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- table -->

        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
            {{ $mappings->appends(Request::except('page'))->links() }}
        </div>
    </div>
    <!-- /.box -->

    <!-- Modal Created -->
    <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 800px;">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add New Mapping</h4>
                </div>
                <form class="form-horizontal" method="POST" action="{{ url('/admin/consultant/mapping/store')}}" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="box-body">
                            <div class="form-group required-field">
                                <label for="inputEmail3" class="col-sm-3 control-label">City</label>
                                <div class="col-sm-9">
                                    <select name="city[]" id="city" class="form-control select2-single" multiple="multiple">
                                        @foreach($cities as $city)
                                            <option value="{{ $city->area_city }}">{{ $city->area_city }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group required-field">
                                <label for="inputEmail3" class="col-sm-3 control-label">Search Existing Consultant</label>
                                <div class="col-sm-9">
                                    <select id="single" class="form-control select2-single">
                                    </select>
                                    <input type="hidden" id="consultantId" name="consultant_id" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="required-field">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Mamirooms</label>
                                </div>
                                <div class="col-sm-9">
                                    <div class="checkbox" id="checkbox-mamirooms">
                                        <label for="is_mamirooms" style="padding-left: 0;">
                                            <input id="is_mamirooms" type="checkbox" name="is_mamirooms[]" value="1" required>&nbsp;Mamirooms
                                        </label>
                                        <label for="is_nonmamirooms" style="padding-left: 0; margin-left: 20px;">
                                            <input id="is_nonmamirooms" type="checkbox" name="is_mamirooms[]" value="0" required>&nbsp;Non-Mamirooms
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" id="create-mapping-submit-button" disabled>Save changes</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!-- Modal Edit -->
    @foreach ($mappings as $mapping)
        <div class="modal fade update" id="modal-update{{ $mapping->id }}">
            <div class="modal-dialog">
                <div class="modal-content" style="width: 800px;">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Update Data Mapping</h4>
                    </div>
                    <form class="form-horizontal" method="POST" action="{{ url('/admin/consultant/mapping/update/'. $mapping->id) }}" enctype="multipart/form-data">
                        <div class="modal-body">
                            <div class="box-body">
                                <div class="form-group required-field">
                                    <label for="inputEmail3" class="col-sm-2 control-label">City</label>
                                    <div class="col-sm-10">
                                        <select name="city" id="cityUpdate{{ $mapping->id }}" class="form-control select2-single city-update">
                                            @foreach($cities as $city)
                                                <option value="{{ $city->area_city }}" {{ $city->area_city === $mapping->area_city ? 'selected' : '' }}>{{ $city->area_city }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Consultant</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="consultant" id="inputName" class="form-control" value="{{ $mapping->consultant->name }}" disabled>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Mamirooms</label>
                                    <div class="col-sm-10">
                                        <div class="checkbox">
                                            <input type="checkbox" name="is_mamirooms" {{ ($mapping->is_mamirooms) ? "checked" : '' }}><p></p>
                                            <span>check the box if you want to be set as a <b>mamirooms</b> consultant, otherwise the default is <b>non-mamiroooms</b></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    @endforeach
    <!-- /.modal -->
@endsection

@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script>
    $(function() {
        function validateCreate() {
            console.log('test');
            if (
                $('#city').select2('data').length &&
                $('#single').select2('data').length &&
                $('#checkbox-mamirooms .icheckbox_minimal.checked').length
            ) {
                $('#create-mapping-submit-button').prop('disabled', false);
            } else {
                $('#create-mapping-submit-button').prop('disabled', true);
            }
        }

        // Create
        $('#city').select2({
            theme: "bootstrap",
            maximumSelectionLength: 10,
            placeholder: "City",
            allowClear: true,
            selectOnClose: false,
            closeOnSelect: false,
            tags: false,
            tokenSeparators: [','],
            dropdownParent: $("#modal-default")
        })

        $('#single').select2({
            theme: "bootstrap",
            placeholder: "Name",
            ajax: {
                url: '/admin/users/consultant/list',
                data: function (params) {
                    return {
                        search: params.term,
                        page: params.page || 1
                    }
                },
                processResults: function (data, params) {
                    var page = params.page || 1;
                    if (data.data.length < 1) var total_count = 0;
                    else var total_count = data.data[0].total_count;
                    return {
                        results: $.map(data.data, item => {
                            return {
                                id: item.id,
                                text: item.name + ' (' + item.email + ')',
                                totalMapping: item.mapping_count,
                                roles: item.roles.map(role => role.role)
                            }
                        }),
                        pagination: {
                            more: (page * 10) <= total_count
                        }
                    };
                },
                cache: true
            },
            dropdownParent: $("#modal-default")
        });

        // Dropdown listeners
        $("#city").on('select2:select', validateCreate);

        $("#single").on('select2:select', e => {
            var selected = e.params.data;
            if (selected.id != '') {
                $('#consultantId').val(selected.id);
            }

            validateCreate();
        });

        // Update
        $('.edit-update-data').on('click', function() {
            const dataId = $(this).data('id');
            const dataSelected = $(this).data('selected');
            $(`#cityUpdate${dataId}`).select2({
                theme: "bootstrap",
                placeholder: "City",
                dropdownParent: $(`#modal-update${dataId}`)
            });
        })

        //delete
        $('.btn-delete-mapping').on('click',function(e){
            let id = $(this).data('id');

            axios.delete('/admin/consultant/mapping/delete/'+id);

            location.reload(true);
        });

        $('.checkbox input').on('ifToggled', function() {
            setTimeout(() => {
                if ($(this).parents('.checkbox').find('.icheckbox_minimal.checked').length > 0) {
                    $(this).parents('.checkbox').find('input').attr('required', false);
                } else {
                    $(this).parents('.checkbox').find('input').attr('required', true);
                }

                validateCreate();
            }, 50);
        });


    });
</script>
@endsection
