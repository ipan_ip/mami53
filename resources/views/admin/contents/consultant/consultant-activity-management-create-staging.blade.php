@extends('admin.layouts.main')

@section('style')
  <style>
    div.required-field label::after {
      content: "*";
      color: red;
    }

    .modal {
      background: rgba(255, 255, 255, .4);
    }

    textarea {
      resize: vertical;
    }

    .no-overflow .bootstrap-table .fixed-table-container .fixed-table-body {
      overflow-x: visible;
      overflow-y: visible;
    }

    .swal2-container .swal2-popup {
      font-size: 1.6rem;
    }

    .swal2-container .swal2-icon.swal2-warning::before {
      content: '';
    }
  </style>

  <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
  <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" />
  <link href="//cdn.jsdelivr.net/npm/select2-bootstrap-theme@0.1.0-beta.10/dist/select2-bootstrap.min.css"
    rel="stylesheet" />
  <link rel="stylesheet" href="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.css">
  <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
@endsection

@section('content')
  <div class="box box-default">
    <!-- Box Header -->
    <div class="box-header" style="padding-top: 10px;">
      <div class="col-md-6 text-center">
        <h3 class="box-title">
          <i class="fa fa-tasks"></i> {{ !isset($edit)?'Create New Staging':'Edit Staging' }}
        </h3>
      </div>

      <div class="col-md-6">
        <button
          class="btn btn-primary pull-right"
          style="margin-top: 5px;"
          data-toggle="modal"
          data-target="#modal-form"
          data-add-edit="add"
        ><i class="fa fa-plus"></i> Add Staging</button>
      </div>
    </div>
    <!-- /.box-header -->

    <div class="box-body no-padding no-overflow">
      <!-- Content -->
      <div class="horizontal-wrapper" style="margin-top: 15px;">
        <div style="padding: 15px;">

          <table
            id="table-new-staging"
            data-toggle="table"
          >
            <thead>
              <tr>
                <th data-field="stage">
                  Stage Phase
                </th>
                <th data-field="name">
                  Stage Name
                </th>
                <th data-field="detail">
                  Description
                </th>
                <th data-field="id" data-formatter="actionFormatter">
                  Action
                </th>
              </tr>
            </thead>
          </table>

          <div data-click-show="save-changes" style="margin-top: 30px; display: none;">
            <button class="btn btn-success btn-lg btn-block save-all-stage">Save Changes</button>
          </div>

        </div>
      </div>
    </div>
  </div>

  <!-- Modal Created -->
  <div class="modal fade" id="modal-form">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
          <h4 class="modal-title"></h4>
        </div>

          <div class="modal-body">
            <div class="form-group">
              <label for="stagePhase">Stage Phase</label>
              <div class="row">
                <div class="col-sm-12">
                  <input name="stage_phase" id="stagePhase" class="form-control" type="number" min="1" max="10" required>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label for="stageName">Stage Name</label>
              <div class="row">
                <div class="col-sm-12">
                  <input type="text" class="form-control" id="stageName" name="stage_name" placeholder="ex: Visit Kos" required value="">
                </div>
              </div>
            </div>

            <div class="form-group">
              <label for="stageDescription">Stage Description</label>
              <div class="row">
                <div class="col-sm-12">
                  <textarea class="form-control" id="stageDescription" name="stage_description" placeholder="ex: Menemui pemilik kost untuk ditawarkan BBK" required value=""></textarea>
                </div>
              </div>
            </div>
          </div>

          <div class="modal-footer">
            <button class="btn btn-success" type="button" data-click="add-new-staging" style="display: none;">Add New Staging</button>
            <button class="btn btn-success" type="button" data-click="edit-staging" style="display: none;">Edit Staging</button>
          </div>

      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

@endsection

@section('script')
  <script src="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
  <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

  <script>
    function actionFormatter(value, row, index) {
      return `<button class="btn btn-warning button-edit" data-index="${index}" data-row-index="${row.index}" data-toggle="modal" data-target="#modal-form" data-add-edit="edit"><i class="fa fa-edit"></i></button>
              <button class="btn btn-danger button-delete" data-index="${index}" data-row-index="${row.index}"><i class="fa fa-trash"></i></button>`
    }

    $(document).ready(function() {

      let $stage_phase = $('#stagePhase');
      let $stage_name = $('#stageName');
      let $stage_description = $('#stageDescription');

      let $addButton = $('[data-click="add-new-staging"]');
      let $editButton = $('[data-click="edit-staging"]');

      let $table = $('#table-new-staging');

      let index = null;

      let indexIncrement = 0;

      $stage_phase.val('');
      $stage_name.val('');
      $stage_description.val('');


      $('#modal-form').on('show.bs.modal', function(e) {
        let button = $(e.relatedTarget);
        let recipent = button.data('add-edit');

        if (recipent === 'add') {
          $addButton.show();
          $(this).find('.modal-title').text('Add New Stage');
        }

        if (recipent === 'edit') {
          $editButton.show();
          $(this).find('.modal-title').text('Edit Stage');
        }
      });


      // hide all button modal form
      $('#modal-form').on('hide.bs.modal', function(e) {
        $addButton.hide();
        $editButton.hide();
        index = null;

        $stage_phase.val('');
        $stage_name.val('');
        $stage_description.val('');
      });


      $table.on('click', '.button-edit', function() {
        index = $(this).data('index');
        let getData = $table.bootstrapTable('getData');
        let getDataRow = getData[index];

        $stage_phase.val(getDataRow.stage);
        $stage_name.val(getDataRow.name);
        $stage_description.val(getDataRow.detail);
      });


      $table.on('click', '.button-delete', function() {
        let rowIndex = $(this).data('row-index');

        $table.bootstrapTable('remove', {
          field: 'index',
          values: [rowIndex]
        })
      });


      $('[data-click="add-new-staging"]').on('click', function() {

        let phaseVal = $stage_phase.val();
        let nameVal = $stage_name.val();
        let descriptionVal = $stage_description.val();

        if (phaseVal < 0 || phaseVal > 10 || isNaN(parseInt(phaseVal)) || !(nameVal.length) ||!(descriptionVal.length)) {
          return false;
        } else {

          $table.bootstrapTable('insertRow', {
            index: 0,
            row: {
              index: indexIncrement,
              stage: phaseVal,
              name: nameVal,
              detail: descriptionVal
            }
          });

          $stage_phase.val('');
          $stage_name.val('');
          $stage_description.val('');

          indexIncrement++;

          $('#modal-form').modal('hide');

          $('[data-click-show="save-changes"]').fadeIn();
        }

      });

      $('[data-click="edit-staging"]').on('click', function() {

        let phaseVal = $stage_phase.val();
        let nameVal = $stage_name.val();
        let descriptionVal = $stage_description.val();

        if (phaseVal < 0 || phaseVal > 10 || isNaN(parseInt(phaseVal)) || !(nameVal.length) ||!(descriptionVal.length)) {
          return falseindex;
        } else {

          $table.bootstrapTable('updateRow', {
            index: index,
            row: {
              stage: phaseVal,
              name: nameVal,
              detail: descriptionVal
            }
          });

          $stage_phase.val('');
          $stage_name.val('');
          $stage_description.val('');

          $('#modal-form').modal('hide');

          $('[data-click-show="save-changes"]').fadeIn();
        }

      });

      $('.save-all-stage').on('click', function() {
        Swal.fire({
          title: 'Save New Stages?',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Save',
          cancelButtonText: 'Cancel',
          preConfirm: function() {
            return axios.post('/admin/consultant/activity-management/store/staging/{{ $funnel_id }}', {
              stages: $table.bootstrapTable('getData')
            })
            .then(response => {
              if (!response.data.status) {
                throw new Error(response.data.meta.message)
              }
              return response;
            })
            .catch(error => {
              Swal.showValidationMessage(
                `Request failed: ${error}`
              )
            })
          },
          allowOutsideClick: () => !Swal.isLoading()
        })
        .then((result) => {
          if (result.value) {

            Swal.fire({
              icon: 'success',
              text: 'New Stages Saved',
              showConfirmButton: false,
              allowOutsideClick: false
            });

            window.location.href = '{{ url("/admin/consultant/activity-management/funnel/$funnel_id") }}'
          }
        });
      });

    });
  </script>
@endsection