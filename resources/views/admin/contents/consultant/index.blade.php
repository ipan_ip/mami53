@extends('admin.layouts.main')

@section('style')
    <style>
        /* Select2 tweak */
        .input-group .select2-container,
        .form-group .select2-container {
            position: relative !important;
            z-index: 2;
            float: left !important;
            width: 100% !important;
            margin-bottom: 0 !important;
            display: table !important;
            table-layout: fixed !important;
        }
        div.required-field label::after {
            content: "*";
            color: red;
        }
        .admin-bse-checkbox:disabled {
            opacity: 0.3;
        }

        .no-admin-bse {
            color: #777;
            font-style: italic;
            display: none;
        }

        .no-admin-bse p {
            padding: 8px 0;
        }
    </style>

    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" />
    <link href="//cdn.jsdelivr.net/npm/select2-bootstrap-theme@0.1.0-beta.10/dist/select2-bootstrap.min.css" rel="stylesheet" />
@endsection

@section('content')
    <div class="box box-default">
        <!-- Box Header -->
        <div class="box-header">
            <div class="col-md-12 text-center">
                <h3 class="box-title"><i class="fa fa-check-square-o"></i> {{ $contentHeader }}</h3>
            </div>
        </div>
        <!-- /.box-header -->

        <div class="box-body no-padding">

            {{-- Top Bar --}}
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default form-inline" style="padding-left: 10px;padding-right: 10px;">
                    <!-- Add button -->
                    <div class="col-md-6 bg-default" style="padding-bottom:10px;">
                        <a href="#" class="btn btn-success btn pull-left actions" data-toggle="modal" data-target="#modal-default">
                            <i class="fa fa-plus">&nbsp;</i>Add Consultant
                        </a>
                    </div>
                    <!-- Search filters -->
                    <div class="col-md-6 bg-default" style="padding-bottom:10px;text-align:right">
                        <form action="{{ url('/admin/consultant') }}" method="get">
                            <input type="text" name="key" class="form-control input-sm" placeholder="Name"
                                autocomplete="off" value="{{ request()->input('key') }}">
                            <button type="submit" class="btn btn-primary btn-md" id="buttonSearch"><i
                                    class="fa fa-search">&nbsp;</i>Search</button>
                        </form>
                    </div>
                </div>
            </div>

            {{-- Table --}}
            <table class="table">
                <thead>
                    <tr>
                        <th></th>
                        <th class="text-center">ID</th>
                        <th class="text-center">User ID</th>
                        <th>Name</th>
                        <th class="text-center">Room Chat</th>
                        <th class="text-center">City</th>
                        <th class="text-center">Roles</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($consultants as $consultant)
                    <tr>
                        <td class="text-center">
                            <a href="#" data-image="{{ $consultant->photo_url['large'] }}" data-name="{{ $consultant->name }}" data-email="{{ $consultant->email }}" data-toggle="lightbox" data-max-width="400">
                                <img src="{{ $consultant->photo_url['small'] }}" class="img-circle" width="80" height="70">
                            </a>
                        </td>
                        <td class="text-center">{{ $consultant->id }}</td>
                        <td class="text-center">{{ $consultant->user->id }}</td>
                        <td>
                            <strong class="font-semi-large">{{ $consultant->name }}</strong>
                            <br><small>{{ $consultant->user->phone_number }}</small>
                            <br><small>{{ $consultant->email }}</small>
                        </td>
                        <td class="text-center"><a href="{{ url('/admin/consultant/room-chat/'.$consultant->id) }}" target="_blank"><i class='fa fa-commenting-o'></i> Open Room</a></td>
                        <td class="text-center">
                            @foreach($consultant->mapping as $mapping)
                                <li>{{ $mapping->area_city }}</li>
                            @endforeach
                        </td>
                        <td class="text-center">
                            @foreach($consultant->roles as $role)
                                <li>{{ $role->role }}</li>
                            @endforeach
                        </td>
                        <td class="table-action-column" style="padding-right:15px!important;">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                    <i class="glyphicon glyphicon-cog"></i> Actions <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a href="#" class="actions trigger-update-modal" data-toggle="modal" data-target="#modal-update{{ $consultant->id }}" data-id="{{ $consultant->id }}" data-mapping="{{ $consultant->mapping->count() }}"><i class="glyphicon glyphicon-edit"></i> Edit Profil</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/admin/consultant/delete/'.$consultant->id) }}" class="actions" onclick="return confirm('are you sure?')"><i class="glyphicon glyphicon-trash"></i> Hapus Profil</a>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>

            </table>
        </div><!-- /.box-body -->
        <div class="box-footer clearfix">
            {{ $consultants->appends(Request::except('page'))->links() }}
        </div>
    </div><!-- /.box -->
    <!-- table -->

    <!-- Modal Created -->
    <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 800px;height: 550px;">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add New Consultant</h4>
                </div>
                <form class="form-horizontal" method="POST" action="{{ url('/admin/consultant/store')}}" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Search Existing User</label>
                                <div class="col-sm-10">
                                    <select id="single" class="form-control select2-single">
                                    </select>
                                    <input type="hidden" id="userId" name="user_id" value="" required>
                                    <p></p>
                                    <p class="help-block">Please search existing user first, before insert form input in below.</p>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group required-field">
                                <label for="inputEmail3" class="col-sm-2 control-label">Name</label>
                                <div class="col-sm-10">
                                    <input type="text" name="name" class="form-control" id="inputName" placeholder="Name" required readonly>
                                </div>
                            </div>
                            <div class="form-group required-field">
                                <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                                <div class="col-sm-10">
                                    <input type="text" name="email" class="form-control" id="inputEmail" placeholder="Email" required readonly>
                                </div>
                            </div>
                            <div class="form-group required-field">
                                <label for="inputEmail3" class="col-sm-2 control-label">Phone Number</label>
                                <div class="col-sm-10">
                                    <input type="text" name="phone_number" class="form-control" id="inputPhoneNumber" placeholder="Phone Number" required readonly>
                                </div>
                            </div>
                            <div class="form-group required-field">
                                <label for="inputRole" class="col-sm-2 control-label">Role</label>
                                <div class="col-sm-10">
                                    <label style="padding-right: 8px;">
                                        <input
                                            type="checkbox"
                                            class="form-control"
                                            id="add-admin"
                                            name="roles[]"
                                            value="admin"
                                            onclick="checkAddRequired()"
                                            required
                                        >Admin
                                    </label>

                                    <label style="padding-right: 8px;">
                                        <input
                                            type="checkbox"
                                            class="form-control"
                                            id="add-demand"
                                            name="roles[]"
                                            value="demand"
                                            onclick="checkAddRequired()"
                                            required
                                        >Demand
                                    </label>

                                    <label style="padding-right: 8px;">
                                        <input
                                            type="checkbox"
                                            class="form-control"
                                            id="add-supply"
                                            name="roles[]"
                                            value="supply"
                                            onclick="checkAddRequired()"
                                            required
                                        >Supply
                                    </label>

                                    <label style="padding-right: 8px;">
                                        <input
                                            type="checkbox"
                                            class="form-control"
                                            id="add-admin_chat"
                                            name="roles[]"
                                            value="admin_chat"
                                            onclick="checkAddRequired()"
                                            required
                                        >Admin Chat
                                    </label>

                                    <label>
                                        <input
                                            type="checkbox"
                                            class="form-control"
                                            id="add-admin_bse"
                                            name="roles[]"
                                            value="admin_bse"
                                            onclick="checkAddRequired()"
                                            required
                                        >Admin BSE
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Photo</label>
                                <div class="col-sm-10">
                                    <input type="file" class="form-control" id="photoId" name="photo" value="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" id="submit-add" class="btn btn-primary" disabled>Save changes</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!-- Modal Edit -->
    @foreach ($consultants as $consultant)
        <div class="modal fade" id="modal-update{{ $consultant->id }}">
            <div class="modal-dialog">
                <div class="modal-content" style="width: 800px;">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Form Update Consultant : {{ $consultant->user->name }}</h4>
                    </div>
                    <form class="form-horizontal" method="POST" action="{{ url('/admin/consultant/update/'.$consultant->id) }}" enctype="multipart/form-data">
                        <div class="modal-body">
                            <div class="box-body">
                                <div class="form-group required-field">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Name</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="name" class="form-control" id="inputName" value="{{ $consultant->name }}">
                                    </div>
                                </div>
                                <div class="form-group required-field">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="email" class="form-control" id="inputEmail" value="{{ $consultant->user->email }}" readonly>
                                    </div>
                                </div>
                                <div class="form-group required-field">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Phone Number</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="phone_number" class="form-control" id="inputPhoneNumber" value="{{ $consultant->user->phone_number }}" readonly>
                                    </div>
                                </div>
                                <div class="form-group required-field">
                                    <label for="inputRole" class="col-sm-2 control-label">Role</label>
                                    <div class="col-sm-10">
                                        <label style="padding-right: 8px;">
                                            <input
                                                type="checkbox"
                                                class="form-control"
                                                id="roles-admin-{{ $consultant->id }}"
                                                name="roles[]"
                                                value="admin"
                                                onclick="checkRequired({{ $consultant->id }})"
                                                {{ $consultant->roles->where('role', 'admin')->isNotEmpty() ? 'checked' : '' }}
                                                {{ $consultant->roles->isEmpty() ? 'required' : '' }}
                                            >Admin
                                        </label>

                                        <label style="padding-right: 8px;">
                                            <input
                                                type="checkbox"
                                                class="form-control"
                                                id="roles-demand-{{ $consultant->id }}"
                                                name="roles[]"
                                                value="demand"
                                                onclick="checkRequired({{ $consultant->id }})"
                                                {{ $consultant->roles->where('role', 'demand')->isNotEmpty() ? 'checked' : '' }}
                                                {{ $consultant->roles->isEmpty() ? 'required' : '' }}
                                            >Demand
                                        </label>

                                        <label style="padding-right: 8px;">
                                            <input
                                                type="checkbox"
                                                class="form-control"
                                                id="roles-supply-{{ $consultant->id }}"
                                                name="roles[]"
                                                value="supply"
                                                onclick="checkRequired({{ $consultant->id }})"
                                                {{ $consultant->roles->where('role', 'supply')->isNotEmpty() ? 'checked' : '' }}
                                                {{ $consultant->roles->isEmpty() ? 'required' : '' }}
                                            >Supply
                                        </label>

                                        <label style="padding-right: 8px;">
                                            <input
                                                type="checkbox"
                                                class="form-control"
                                                id="roles-admin_chat-{{ $consultant->id }}"
                                                name="roles[]"
                                                value="admin_chat"
                                                onclick="checkRequired({{ $consultant->id }})"
                                                {{ $consultant->roles->where('role', 'admin_chat')->isNotEmpty() ? 'checked' : '' }}
                                                {{ $consultant->roles->isEmpty() ? 'required' : '' }}
                                            >Admin Chat
                                        </label>

                                        <label>
                                            <input
                                                type="checkbox"
                                                class="form-control admin-bse-checkbox"
                                                id="roles-admin_bse-{{ $consultant->id }}"
                                                name="roles[]"
                                                value="admin_bse"
                                                onclick="checkRequired({{ $consultant->id }})"
                                                {{ $consultant->roles->where('role', 'admin_bse')->isNotEmpty() ? 'checked' : '' }}
                                                {{ $consultant->roles->isEmpty() ? 'required' : '' }}
                                            >Admin BSE
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <img src="{{ $consultant->photo_url['medium'] }}" class="img-circle" width="280" height="260">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Photo</label>
                                    <div class="col-sm-10">
                                        <input type="file" class="form-control" id="photoId" name="photo" value="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    @endforeach
    <!-- /.modal -->

@endsection

@section('script')
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>
<script>
let roleFilled = false;
let userIdFilled = false;

function checkAddRequired() {
    let adminChecked = $('#add-admin').is(':checked');
    let demandChecked = $('#add-demand').is(':checked');
    let supplyChecked = $('#add-supply').is(':checked');
    let adminChatChecked = $('#add-admin_chat').is(':checked');
    let adminBseChecked = $('#add-admin_bse').is(':checked');

    let isOneChecked = (adminChecked || demandChecked || supplyChecked || adminChatChecked || adminBseChecked);
    if (isOneChecked) {
        $('#add-admin').removeAttr('required');
        $('#add-demand').removeAttr('required');
        $('#add-supply').removeAttr('required');
        $('#add-admin_chat').removeAttr('required');
        $('#add-admin_bse').removeAttr('required');

        roleFilled = true;
        adjustSubmitButton();
    }
    else {
        $('#add-admin').attr('required', true);
        $('#add-demand').attr('required', true);
        $('#add-supply').attr('required', true);
        $('#add-admin_chat').attr('required', true);
        $('#add-admin_bse').attr('required', true);

        roleFilled = false;
        adjustSubmitButton();
    }
}

function checkRequired(id) {
    let adminChecked = $(`#roles-admin-${id}`).is(':checked');
    let demandChecked = $(`#roles-demand-${id}`).is(':checked');
    let supplyChecked = $(`#roles-supply-${id}`).is(':checked');
    let adminChatChecked = $(`#roles-admin_chat-${id}`).is(':checked');
    let adminBseChecked = $(`#roles-admin_bse-${id}`).is(':checked');

    let isOneChecked = (adminChecked || demandChecked || supplyChecked || adminChatChecked || adminBseChecked);
    if (isOneChecked) {
        $(`#roles-admin-${id}`).removeAttr('required');
        $(`#roles-demand-${id}`).removeAttr('required');
        $(`#roles-supply-${id}`).removeAttr('required');
        $(`#roles-admin_chat-${id}`).removeAttr('required');
        $('#roles-admin_bse-${id}').removeAttr('required');
    }
    else {
        $(`#roles-admin-${id}`).attr('required', true);
        $(`#roles-demand-${id}`).attr('required', true);
        $(`#roles-supply-${id}`).attr('required', true);
        $(`#roles-admin_chat-${id}`).attr('required', true);
        $('#roles-admin_bse-${id}').attr('required', true);
    }


}

function adjustSubmitButton()
{
    if (roleFilled && userIdFilled) {
        $('#submit-add').removeAttr('disabled');
    } else {
        $('#submit-add').attr('disabled', true);
    }
}

$(function() {
    {{-- Kill iCheck due to inability to check checkbox value --}}
    $('#add-admin').iCheck('destroy');
    $('#add-demand').iCheck('destroy');
    $('#add-supply').iCheck('destroy');
    $('#add-admin_chat').iCheck('destroy');
    $('#add-admin_bse').iCheck('destroy');
    @foreach ($consultants as $consultant)
        $('#roles-admin-{{ $consultant->id }}').iCheck('destroy');
        $('#roles-demand-{{ $consultant->id }}').iCheck('destroy');
        $('#roles-supply-{{ $consultant->id }}').iCheck('destroy');
        $('#roles-admin_chat-{{ $consultant->id }}').iCheck('destroy');
        $('#roles-admin_bse-{{ $consultant->id }}').iCheck('destroy');
    @endforeach

    $('#single').select2({
        theme: "bootstrap",
        placeholder: "Name",
        ajax: {
            url: '/admin/users/consultant',
            data: function (params) {
                return {
                    search: params.term,
                    page: params.page || 1
                }
            },
            processResults: function (data, params) {
                var page = params.page || 1;
                if (data.data.length < 1) var total_count = 0;
                else var total_count = data.data[0].total_count;
                return {
                    results: $.map(data.data, item => {
                        return {
                            id: item.id,
                            text: item.name + ' (' + item.email + ')'
                        }
                    }),
                    pagination: {
                        more: (page * 10) <= total_count
                    }
                };
            },
            cache: true
        },
        dropdownParent: $("#modal-default")
    });

    // Dropdown listeners
    $("#single").on('select2:select', e => {
        var selected = e.params.data.id;
        if (selected != '') {
            applyUser(selected);
        }
    });

    // Function to fetch single user details
    function applyUser(id) {
        $.ajax({
            type: 'POST',
            url: "/admin/users/user",
            data: {
                id: id
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: (data) => {
                $('#userId').val(data.id);
                $('#inputName').val(data.name);
                $('#inputEmail').val(data.email);
                $('#inputPhoneNumber').val(data.phone_number);
                $('#inputAddress').val(data.address);

                userIdFilled = true;
                adjustSubmitButton();
            }
        });
    }
});
</script>


@endsection
