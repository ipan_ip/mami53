@extends('admin.layouts.main')

@section('style')
    <style>
        /* Select2 tweak */
        .input-group .select2-container,
        .form-group .select2-container {
            position: relative !important;
            z-index: 2;
            float: left !important;
            width: 100% !important;
            margin-bottom: 0 !important;
            display: table !important;
            table-layout: fixed !important;
        }

        div.required-field label::after {
            content: "*";
            color: red;
        }
    </style>

    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet"/>
    <link href="//cdn.jsdelivr.net/npm/select2-bootstrap-theme@0.1.0-beta.10/dist/select2-bootstrap.min.css"
          rel="stylesheet"/>
    <link rel="stylesheet" href="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.css">
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
@endsection

@section('content')
    <div class="box box-default">
        <!-- Box Header -->
        <div class="box-header">
            <div class="col-md-12 text-center">
                <h3 class="box-title"><i class="fa fa-check-square-o"></i> Add New Mapping</h3>
            </div>
        </div>
        <!-- /.box-header -->

        <div class="box-body">
            {{-- Custom Search Form --}}
            <div id="searchForm" style="margin-left: 10px">
                <label for="searchForm">Filter: </label>
                <form id="searchForm" class="form-inline" autocomplete="off" style="margin-bottom: 10px;">
                    <div class="form-group">
                        <select name="searchBy" class="form-control" id="searchBy">
                            <option value="" selected>All Property Type</option>
                            <option value="booking">Booking Langsung</option>
                            <option value="free_listing">Free Listing</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <select name="level" class="form-control" id="levelId">
                            <option value="" selected>All Level</option>
                            @foreach ($kostLevels as $kostLevel)
                                <option value="{{ $kostLevel->id }}">{{ $kostLevel->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <input type="text" id="search" name="search" class="form-control"
                            id="search" placeholder="Search..">
                    </div>
                    
                    <button id="btnSearch" type="submit" class="btn btn-primary"><i
                                class="fa fa-search"></i> Search
                    </button>
                </form>
            </div>
            {{-- Top Bar --}}
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default"
                     style="padding-left: 10px;padding-right: 10px;">
                    {{-- Table --}}
                    <table class="table" id="consultantMappingTable"
                           data-toggle="table"
                           data-url="{{ url("admin/consultant/room-mapping/add/data/".$consultant->id."/".$areaCity) }}"
                           data-side-pagination="server"
                           data-pagination="true"
                           data-query-params="searchQuery"
                           data-checkbox-header="true"
                           data-click-to-select="true"
                           data-page-list="[15, 25,50]"
                    >
                        <thead>
                        <tr>
                            <th data-field="state" data-checkbox="true"></th>
                            <th data-field="id">Property ID</th>
                            <th data-field="name">Property Name</th>
                            <th data-field="area_city">City</th>
                            <th data-field="type">Property Type</th>
                            <th data-field="kost_level">Level</th>
                        </tr>
                        </thead>
                    </table>
                    <!-- table -->
                    <div class="col-md-12">
                        <button class="btn btn-primary pull-right" id="btnSave">Save Selected</button>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
            @endsection

            @section('script')
                <script src="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.js"></script>
                <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
                <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
                <script>
                    $(function () {
                        // Initiate table once page has rendered
                        $('#consultantMappingTable').bootstrapTable({}).on('post-body.bs.table', function () {
                            // need to re-render tooltip when table is modified/sorted
                            $('[data-toggle="tooltip"]').tooltip()
                        });
                    });

                    function searchQuery(params) {
                        params.search = $('#search').val();
                        params.property_type = $('#searchBy option:selected').val();
                        params.level_id = $('#levelId option:selected').val();
                        return params;
                    }

                    $('#searchForm').submit(function (e) {
                        e.preventDefault();

                        $('#consultantMappingTable').bootstrapTable('refresh')
                    })

                    $('#btnSave').on('click', function () {
                        swal.fire({
                            title: 'Are you sure?',
                            text: "You will assign selected properties to consultant {{ $consultant->name }}",
                            icon: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Yes',
                            showLoaderOnConfirm: true,
                            allowOutsideClick: false,
                            preConfirm: function() {
                                let propertyIds = [];
                                $('#consultantMappingTable').bootstrapTable('getSelections').map((value, index) => {
                                    propertyIds.push(value.id);
                                });

                                return axios.post("{{ url('admin/consultant/room-mapping/add/'.$consultant->id) }}", {
                                    rooms: propertyIds
                                }).then(function (response) {
                                    if (!response.status) {
                                        throw new Error(response.meta.message);
                                    }

                                    return response;
                                }).catch(error => {
                                    Swal.showValidationMessage(
                                        `Request failed: ${error}`
                                    )
                                });
                            },

                        }).then((result) => {
                            if (result.value) {
                                Swal.fire({
                                    icon: 'success',
                                    text: 'Mapping successfully added'
                                });

                                $('#consultantMappingTable').bootstrapTable('refresh')
                            }

                        })
                    })
                </script>
@endsection
