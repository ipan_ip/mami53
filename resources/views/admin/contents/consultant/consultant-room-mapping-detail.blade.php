@extends('admin.layouts.main')

@section('style')
    <style>
        /* Select2 tweak */
        .input-group .select2-container,
        .form-group .select2-container {
            position: relative !important;
            z-index: 2;
            float: left !important;
            width: 100% !important;
            margin-bottom: 0 !important;
            display: table !important;
            table-layout: fixed !important;
        }

        div.required-field label::after {
            content: "*";
            color: red;
        }
    </style>

    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet"/>
    <link href="//cdn.jsdelivr.net/npm/select2-bootstrap-theme@0.1.0-beta.10/dist/select2-bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.css">
@endsection

@section('content')
    <div class="box box-default">
        <!-- Box Header -->
        <div class="box-header">
            <div class="col-md-12 text-center">
                <h3 class="box-title"><i class="fa fa-check-square-o"></i> {{ $contentHeader }}</h3>
            </div>
        </div>
        <!-- /.box-header -->

        <div class="box-body">
            {{-- Custom Search Form --}}
            <div class="row">
                <div class="col-md-6">
                    <form id="searchForm" class="form-inline" autocomplete="off" style="margin-left:30px">
                        <input type="text" id="search" name="search" class="form-control col-2 col-sm-2"
                               id="search" placeholder="Search Property">
                        <button id="btnSearch" type="submit" class="btn btn-primary col-1 col-sm-1" style="margin-left: 10px"><i
                                    class="fa fa-search"></i> Search
                        </button>
                    </form>

                    <select id="selectpicker" class="selectpicker" data-style="col-md-offset-1" data-live-search="true" data-live-search-placeholder="Consultant name" data-none-results-text="Consultant does not exist">
                        <option value="{{ $consultantId }}">{{ $consultantName }}</option>
                    </select>

                    @permission('access-mapping-system')
                        <button id="editButton" class="btn btn-success" onClick="editMapping({{ $consultantId }}, '{{ $areaCity }}')" style="margin-left: 30px;">Edit Consultant</button>
                    @endpermission
                </div>

                @permission('access-mapping-system')
                    <div class="col-md-6">
                        <a href="{{ url('admin/consultant/room-mapping/detail/edit/'.$consultantId.'/'.$areaCity) }}" class="btn btn-success pull-right">Edit Mapping</a>
                    </div>
                @endpermission

            </div>

            {{-- Top Bar --}}
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default form-inline" style="padding-left: 10px;padding-right: 10px;">
                    {{-- Table --}}
                    <table class="table" id="consultantMappingTable"
                        data-toggle="table"
                        data-url="{{ url("admin/consultant/room-mapping/detail/data/$consultantId/$areaCity") }}"
                        data-side-pagination="server"
                        data-pagination="true"
                        data-page-list="[15, 25, 50, 100]"
                        data-page-size="15"
                        data-query-params="searchQuery"
                        data-total-field="count"
                        data-data-field="data"
                    >
                        <thead>
                        <tr>
                            <th data-field="id" id="id-column">Property ID</th>
                            <th data-field="name">Property Name</th>
                            <th data-field="area_city">City</th>
                            <th data-field="is_booking" data-formatter="getPropertyTypeFormatter">Property Type</th>
                            <th data-field="kost_level">Level</th>
                        </tr>
                        </thead>
                    </table>
                    <!-- table -->
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
@endsection

@section('script')
    <script src="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script>
        $('#selectpicker').selectpicker();

        function loadConsultant(name = '') {
            $('#selectpicker').attr('data-none-results-text', 'Loading...');
            $('#selectpicker').selectpicker('refresh');

            axios.get(`/admin/consultant/room-mapping/consultant?search=${name}`).then(res => {
                let data = res.data.data;

                for (let i = 0; i < data.length; i++) {
                    if (!$('#selectpicker').find(`[value=${data[i].id}]`).length) {
                        if (name !== '') {
                            $('#selectpicker').prepend(`<option value=${data[i].id}>${data[i].name}</option>`);
                        }
                        else {
                            $('#selectpicker').append(`<option value=${data[i].id}>${data[i].name}</option>`);
                        }
                    }
                }

                $('#selectpicker').selectpicker('refresh');
            });
        }

        loadConsultant();

        $('[type=search]').keyup(e => {
            loadConsultant(e.target.value);
        });

        $('#selectpicker').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
            window.location.replace(`/admin/consultant/room-mapping/detail/${$('#selectpicker').selectpicker('val')}/{{ $areaCity }}`);
        });

        $(function () {
            // Initiate table once page has rendered
            $('#consultantMappingTable').bootstrapTable({
                pagination: true,
                queryParamsType: 'limit',
            }).on('post-body.bs.table', function () {
                // need to re-render tooltip when table is modified/sorted
                $('[data-toggle="tooltip"]').tooltip()
            });

            $('[data-toggle="tooltip"]').tooltip();
        });

        function searchQuery(params){
            params.search = $('#search').val();

            return params;
        }

        $('#searchForm').submit(function(e){
            e.preventDefault();

            $('#consultantMappingTable').bootstrapTable('refresh')
        })

        function getPropertyTypeFormatter(value, row) {
            return (value ? 'Booking Langsung' : 'Freelisting');
        }

        function editMapping(value, area_city) {
            Swal.fire({
                title: 'Enter consultant name',
                html: `<input class="form-control" type="text" id="consultant-name" data-consultant-id="${value}" data-area-city="${area_city}" data-toggle="dropdown">`,
                showCancelButton: true,
                showLoaderOnConfirm: true,
                preConfirm: (login) => {
                    return axios.post('/admin/consultant/room-mapping/reassign', {
                        old_consultant_id: Number($('#consultant-name').attr('data-consultant-id')),
                        new_consultant_id: Number($('#consultant-name').attr('data-new-consultant-id')),
                        area_city: $('#consultant-name').attr('data-area-city')
                    })
                        .then(response => {
                            if (!response.status) {
                                throw new Error(response.meta.message);
                            }

                            return response;
                        })
                        .catch(error => {
                            Swal.showValidationMessage(
                                `Request failed: ${error}`
                            )
                        })
                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                if (result.value) {
                    Swal.fire({
                        icon: 'success',
                        text: 'Mapping has been reassigned'
                    });

                    window.location.reload();
                }
            })
        }

        var currentlyQuerying;

        $(document).on('keyup', '#consultant-name', function () {
            clearTimeout(currentlyQuerying); // If the user continue typing, cancel our query.
            currentlyQuerying = setTimeout(queryConsultant, 700); // Wait for 2 seconds then query the consultant name
        });

        $(document).on('keydown', '#consultant-name', function () {
            clearTimeout(currentlyQuerying); // If the user continue typing, cancel our query.
        });

        function queryConsultant() {
            let consultantName = $('#consultant-name').val();

            $.ajax({
                type: 'GET',
                url: `/admin/consultant/room-mapping/consultant?search=${consultantName}`,
                success: function(data) {
                    $('#consultant-name').next('.dropdown-menu').remove();

                    listOption = '<ul class="dropdown-menu suggestion-list" aria-labelledby="dLabel">'
                    for(consultant of data.data) {
                        listOption += '<li><a href="#" class="suggestion-item suggestion-item-' + consultant.id + '" data-key="' + consultant.id + '" onclick="clickSuggestion(' + consultant.id + ')">' + consultant.name + '</a></li>';
                    }

                    listOption += '</ul>';

                    $(listOption).insertAfter($('#consultant-name'));

                    $('.suggestion-list').dropdown();
                }
            });
        }

        function clickSuggestion(id) {
            val = $('.suggestion-item-' + id).text();

            $('#consultant-name').val(val);
            $('#consultant-name').attr('data-new-consultant-id', id);
        }
    </script>
@endsection
