@extends('admin.layouts.main')

@section('style')
	<link rel="stylesheet" href="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.css">
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker3.min.css">
	<link rel="stylesheet" href="/css/admin-consultant.css">

	<style>
		.datepicker table tr td {
			color: #222;
		}
		.datepicker table tr td.disabled, .datepicker table tr td.disabled:hover {
			color: #ccc;
		}
	</style>
@endsection

@section('content')
	<div class="box box-default">
		<!-- Box Header -->
		<div class="box-header" style="padding-top: 10px;">
			<div class="col-md-6 text-center">
				<h3 class="box-title" style="padding-left: 0;"><i class="fa fa-line-chart"></i>&nbsp;Sales Motion</h3>
			</div>
			@permission('access-sales-motion')
			<div class="col-md-6">
				<a class="btn btn-primary pull-right" style="margin-top: 5px; color: #fff;" href="/admin/consultant/sales-motion/add"><i class="fa fa-plus"></i> Create Sales Motion</a>
			</div>
			@endpermission
		</div>
		<!-- /.box-header -->

    <div class="box-body no-padding no-overflow">
      <!-- Content -->
      <div class="horizontal-wrapper" style="margin-top: 15px;">
        <div id="sales-motion-list" style="padding: 15px;">
          <table>
            <thead>
              <tr>
                <th data-field="task_name">Task Name</th>
                <th data-field="sales_type" data-searchable="false">Sales Type</th>
								<th data-field="start_date" data-formatter="dateFormatterOnly" data-searchable="false">Start Date</th>
								<th data-field="due_date" data-formatter="dateFormatterOnly" data-searchable="false">Due Date</th>
								<th data-field="updated_at" data-formatter="dateFormatter" data-searchable="false">Updated at</th>
								<th data-field="updated_by" data-searchable="false">Updated by</th>
								<th data-field="is_active" data-formatter="isActiveFormatter" data-searchable="false">Status</th>
								<th data-field="id" data-formatter="actionFormatter" data-searchable="false">Action</th>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
	</div>

	<!-- Modal -->
	<div class="modal fade" id="modal-update-due-date">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">Update Due Date to activate</div>
				<div class="modal-body">
					<div class="one-input">
						<p>Due Date<span>*</span></p>
						<input type="text" id="due-date" value="">
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-primary pull-right" id="activate-button" style="margin-top: 5px; color: #fff;" disabled>Update</button>
				</div>
			</div>
		</div>
	</div>
	<!-- END Modal -->
@endsection

@section('script')
	<script src="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.27.0/moment.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>

	<script>
		function actionFormatter(value, row) {
			let action = `
				<a class="btn btn-default" href="/admin/consultant/sales-motion/detail/${value}"><i class="fa fa-eye"></i></a>
				@permission('access-sales-motion')<a class="btn btn-warning" href="/admin/consultant/sales-motion/edit/${value}"><i class="fa fa-edit"></i></a>@endpermission
			`;

			@permission('access-sales-motion')
			if (row.is_active) {
				action += `<button
					class="btn btn-default"
					style="color: #c9302c"
					data-toggle-active="deactivate"
					data-id="${value}">Deactivate</button>`;
			} else {
				action += `<button
					class="btn btn-default"
					style="color: #449d44"
					data-toggle-active="activate"
					data-id="${value}"
          data-toggle="modal"
          data-target="#modal-form">Activate</button>`;
			}
			@endpermission

			return action;
		}

		function dateFormatterOnly(value) {
			return moment(value).format('DD-MM-Y');
		}

		function dateFormatter(value) {
			return moment(value).format('DD-MM-Y HH:mm:ss')
		}

		function isActiveFormatter(value) {
			if (value) {
				return 'Active'
			}
			return 'Non-active'
		}

    $(document).ready(function() {
			let salesMotionId = '';

			$('#sales-motion-list table').bootstrapTable({
				pagination: true,
				search: true,
				showSearchButton: true,
				searchOnEnterKey: true,
				searchAlign: 'left',
				pageSize: 15,
				pageList: [],
				sidePagination: 'server',
				url: "{{ url('admin/consultant/sales-motion/data') }}",
				dataField: 'sales_motions',
				queryParams: function(params) {
					params.search = '';
					params.q = $('#sales-motion-list .search-input').val();
					return params;
				}
			});

			$('#due-date').datepicker({
				format: "dd-mm-yyyy",
				autoclose: true,
				todayHighlight: false,
				startDate: new Date()
			});

			$('#due-date').on('change', function() {
				if ($(this).val()) {
					$('#activate-button').prop('disabled', false);
				} else {
					$('#activate-button').prop('disabled', true);
				}
			});


			$('#sales-motion-list table').on('click', '[data-toggle-active]', function() {
				salesMotionId = $(this).data('id');

				if ($(this).data('toggle-active') === 'deactivate') {
					Swal.fire({
						title: 'Are you sure?',
						icon: 'warning',
						text: 'Non-active task will not be shown in consultant tools',
						confirmButtonText: 'Yes',
						confirmButtonColor: '#449d44',
						showCancelButton: true,
						cancelButtonText: 'No',
						showLoaderOnConfirm: true,
						reverseButtons: true,
						allowOutsideClick: () => !Swal.isLoading(),
						preConfirm: function() {
							axios.post(`/admin/consultant/sales-motion/deactivate/${salesMotionId}`)
							.then(response => {
								if (response.data.status) {
									Swal.fire({
										title: 'Done',
										text: 'Sales motion deactivated',
										icon: 'success',
										confirmButtonText: 'Ok'
									});

									salesMotionId = '';

									$('#sales-motion-list table').bootstrapTable('refresh');
								}
							})
							.catch(error => {
								alert(error.message);
							});
						}
					})
				}

				if ($(this).data('toggle-active') === 'activate') {
					$('#modal-update-due-date').modal('show');
				}

			});

			$('#activate-button').on('click', function() {
				$('#modal-update-due-date').modal('hide');

				Swal.fire({
					title: 'Adding new potential owner...',
					onBeforeOpen: () => {
						Swal.showLoading();
					},
					allowOutsideClick: false
				});

				axios.post(`/admin/consultant/sales-motion/activate/${salesMotionId}`, {
					end_date: moment($('#due-date').val(), 'DD-MM-YYYY').format('Y-MM-DD')
				})
				.then(response => {
					if (response.data.status) {
						Swal.fire({
							title: 'Done',
							text: 'Sales motion activated',
							icon: 'success',
							confirmButtonText: 'Ok'
						});

						salesMotionId = '';

						$('#sales-motion-list table').bootstrapTable('refresh');
					}
				})
				.catch(error => {
					alert(error.message);
				});
			});

    });
	</script>
@endsection