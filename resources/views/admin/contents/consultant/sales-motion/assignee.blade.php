@extends('admin.layouts.main')

@section('style')
	<style>
		.table-header {
			margin-bottom: 10px;
		}

		.total-consultant {
			text-align: right;
			margin-top:
		}
	</style>

	<link rel="stylesheet" href="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.css">
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
	<link rel="stylesheet" href="/css/admin-consultant.css">
@endsection

@section('content')
	<div class="box box-default">
		<!-- Box Header -->
		<div class="box-header" style="padding-top: 10px;">
			<div class="col-md-6 text-center">
				<h3 class="box-title" style="padding-left: 0;"><i class="fa fa-list"></i>&nbsp;Update Assignee</h3>
			</div>
		</div>
		<!-- /.box-header -->

    <div class="box-body no-padding no-overflow">
      <!-- Content -->
      <div class="horizontal-wrapper" style="margin-top: 15px;">
        <div id="consultant-assignee" style="padding: 15px;">
					<div class="table-header">
						<div class="row">
							<div class="col-md-6">
								<label for="roles">
									Consultant Roles:
									<select name="roles" id="roles">
										<option value="">All</option>
										<option value="admin">Admin</option>
										<option value="supply">Supply</option>
										<option value="demand">Demand</option>
										<option value="admin_chat">Admin Chat</option>
									</select>
								</label>
							</div>
							<div class="col-md-6">
								<div class="total-consultant">
									<span class="checked-consultant"></span> out of <span class="total-consultant"></span> consultant are chosen
								</div>
							</div>
						</div>
					</div>

          <table>
            <thead>
              <tr>
								<th data-checkbox="true"></th>
                <th data-field="name">Consultant Mapped</th>
                <th data-field="role" data-formatter="roleFormatter">Roles</th>
              </tr>
            </thead>
          </table>

					<div class="row">
						<div class="col-sm-12">
							<hr>

							<button type="button" class="btn btn-success pull-right" id="submit-assignee" style="color: #fff;" disabled><i class="fa fa-save"></i>&nbsp;Submit</button>

							<button type="button" class="btn btn-warning pull-right" id="assign-all" style="margin-right: 16px;"><i class="fa fa-address-book"></i>&nbsp;Select by Role</button>
						</div>
					</div>
        </div>
      </div>
    </div>
	</div>

	<!-- Modal -->
	<div class="modal fade">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header"></div>
				<div class="modal-body"></div>
				<div class="modal-footer"></div>
			</div>
		</div>
	</div>
	<!-- END Modal -->
@endsection

@section('script')
	<script src="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

	<script>
		var checkedConsultants = [];
		var firstRoleLoad = true;

		axios.get('/admin/consultant/sales-motion/{{ $salesMotionId }}/assigned/consultant')
		.then(response => {
			checkedConsultants = response.data.assignee_ids;
			afterGetAssigned();
		})
		.catch(error => {
			alert(error);
		})

		let roleChosen = 'Admin';

		function changeChosenRole(event)
		{
			switch (event.target.value) {
				case 'admin':
					roleChosen = 'Admin';
					break;
				case 'supply':
					roleChosen = 'Supply';
					break;
				case 'demand':
					roleChosen = 'Demand';
					break;
			}

			$('button.swal2-confirm.swal2-styled').first().html(`Assign ke semua konsultan ${roleChosen}`);
		}

		$('#assign-all').click(function () {
			Swal.fire({
				title: 'Pilih role untuk diassign',
				// text: "Anda akan mengassign sales motion ini ke semua konsultan.",
				icon: 'warning',
				html: "<form id=\"assign-all-form\" method=\"POST\" action=\"{{ route('admin.consultant.sales-motion.assign-all', ['salesMotion' => $salesMotionId]) }}\"><select name=\"role\" onchange=\"changeChosenRole(event)\"><option value=\"admin\">Admin</option><option value=\"supply\">Supply</option><option value=\"demand\">Demand</option></select></form>",
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				cancelButtonText: 'Batalkan',
				confirmButtonText: `Assign ke semua konsultan ${roleChosen}`,
			}).then((result) => {
				if (result.isConfirmed) {
					$('#assign-all-form').submit();

					Swal.fire({
						title: 'Sedang di proses!',
						html: 'Anda akan otomatis di redirect setelah proses selesai.',
						timer: 5000,
						timerProgressBar: true,
						willOpen: () => {
							Swal.showLoading()
							timerInterval = setInterval(() => {
							const content = Swal.getContent()
							if (content) {
								const b = content.querySelector('b')
								if (b) {
								b.textContent = Swal.getTimerLeft()
								}
							}
							}, 100)
						},
						onClose: () => {
							clearInterval(timerInterval)
						}
					});
				}
			})
		})

		function checkExistingId(id) {
			return checkedConsultants.findIndex(value => value === id);
		}

		function roleFormatter(value, row) {
			return value.join(', ');
		}

		function newUrl(value) {
			switch (value) {
				case 'admin':
					return '/admin/consultant/sales-motion/{{ $salesMotionId }}/consultant?role=admin';
					break;
				case 'supply':
					return '/admin/consultant/sales-motion/{{ $salesMotionId }}/consultant?role=supply';
					break;
				case 'demand':
					return '/admin/consultant/sales-motion/{{ $salesMotionId }}/consultant?role=demand';
					break;
				case 'admin_chat':
					return '/admin/consultant/sales-motion/{{ $salesMotionId }}/consultant?role=admin_chat';
					break;
				default:
					return '/admin/consultant/sales-motion/{{ $salesMotionId }}/consultant';
					break;
			}
		}

		function afterGetAssigned() {
			$(document).ready(function() {
				let $spanChecked = $('span.checked-consultant');
				let $spanTotal = $('span.total-consultant');
				let $table = $('#consultant-assignee table');
				let $submitButton = $('#submit-assignee');

				$('#roles').val('');

				function toggleSubmitButton() {
					if (checkedConsultants.length) {
						$submitButton.prop('disabled', false);
					} else {
						$submitButton.prop('disabled', true);
					}
				};

				toggleSubmitButton();

				$table.bootstrapTable({
					checkboxHeader: false,
					pagination: true,
					clickToSelect: true,
					pageSize: 15,
					pageList: [],
					check: 1,
					sidePagination: 'server',
					url: '/admin/consultant/sales-motion/{{ $salesMotionId }}/consultant',
					dataField: 'consultants',

					onLoadSuccess: function(data) {
						let consultants = data.consultants;
						$spanChecked.text(checkedConsultants.length);
						$spanTotal.text(data.total);

						if (firstRoleLoad) {
							$table.bootstrapTable('selectPage', 1);
							firstRoleLoad = false;
						}

						if (checkedConsultants.length > 0) {
							for (const id of checkedConsultants) {
								const foundIndex = consultants.findIndex(value => value.id === id);
								if (foundIndex !== -1) {
									$table.bootstrapTable('check', foundIndex);
								}
							}
						}
					},

					onCheck: function(row) {
						if (checkExistingId(row.id) === -1) {
							checkedConsultants.push(row.id);
						}
						$spanChecked.text(checkedConsultants.length);

						toggleSubmitButton();
					},

					onUncheck: function(row) {
						if (checkExistingId(row.id) !== -1) {
							checkedConsultants.splice(checkExistingId(row.id), 1);
							$spanChecked.text(checkedConsultants.length);
						} else {
							alert('I don\'t know what you did, but you shouldn\'t see this alert');
						}

						toggleSubmitButton();
					}
				});

				// EVENT: change roles on event input dropdown
				$('#roles').on('input', function(e) {
					$table.bootstrapTable('refresh', {
						url: newUrl($(this).val())
					});
					firstRoleLoad = true;
				});

				// EVENT: click submit button
				$('#submit-assignee').on('click', function() {
					Swal.fire({
						title: 'Adding assigned consultant...',
						onBeforeOpen: () => {
							Swal.showLoading();
						},
						allowOutsideClick: false
					});

					axios.put('/admin/consultant/sales-motion/{{ $salesMotionId }}/sync/assignee', {
						consultants: checkedConsultants
					})
					.then(response => {
						if (response.data.status) {
							Swal.fire({
								title: 'Done',
								text: 'Updated assigned consultants. \nYou will be redirected to sales motion detail.',
								icon: 'success',
								showConfirmButton: false,
								allowOutsideClick: false,
								timerProgressBar: true,
								timer: 1500
							})
							.then(() => {
								window.location.href = `/admin/consultant/sales-motion/detail/{{ $salesMotionId }}`;
							});
						}
					})
					.catch(error => {
						Swal.hide();
						alert(error);
					})
				});
			});
		}
	</script>
@endsection