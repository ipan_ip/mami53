@extends('admin.layouts.main')

@section('style')
	<style>
		.detail-list {
      border: 1px solid #ccc;
      padding: 15px;
    }

    .detail-list div {
      font-size: 16px;
    }

    .detail-list div p {
      margin-bottom: 0;
    }

		.detail-list div strong {
			font-size: 16px;
			padding-bottom: 10px;
		}

    .detail-list div + div {
      padding-top: 20px;
    }
	</style>

	<link rel="stylesheet" href="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.css">
@endsection

@section('content')
	<div class="box box-default">
		<!-- Box Header -->
		<div class="box-header" style="padding-top: 10px;">
			<div class="col-sm-6 text-center">
				<h4 class="box-title" style="padding-left: 0;">Detail Sales Motion and Assignee</h4>
			</div>
			<div class="col-sm-6">

			</div>
		</div>
		<!-- /.box-header -->

    <div class="box-body no-padding no-overflow">
      <!-- Content -->
      <div class="horizontal-wrapper" style="border-top: 0;">
        <div style="padding: 15px;">
          <div class="row">
						<div class="col-sm-6">
							<h3 class="box-title" style="padding-left: 0;">Sales Motion @permission('access-sales-motion')<a class="btn btn-default" href="{{ "/admin/consultant/sales-motion/edit/$salesMotionId/" }}"><i class="fa fa-edit"></i></a>@endpermission</h3>
							<div class="detail-list">
								<div>
									<strong>Task Name</strong>
									<p id="task-name"></p>
								</div>

								<div>
									<strong>Sales Motion Type</strong>
									<p id="type"></p>
								</div>

								<div>
									<strong>Created by Division</strong>
									<p id="division"></p>
								</div>

								<div>
									<strong>Objective</strong>
									<p id="objective"></p>
								</div>

								<div>
									<strong>Terms and Condition</strong>
									<p id="tnc" style="white-space: pre-line; background-color: #eee; padding: 8px;"></p>
								</div>

								<div>
									<strong>Requirements</strong>
									<p id="requirements"></p>
								</div>

								<div>
									<strong>Benefit</strong>
									<p id="benefit"></p>
								</div>

								<div>
									<strong>Max Participants</strong>
									<p id="participants"></p>
								</div>

								<div>
									<strong>Link</strong>
									<p id="link"></p>
								</div>

								<div>
									<strong>Attachment</strong>
									<div id="attachment"></div>
								</div>

								<div>
									<strong>Start Date</strong>
									<p id="start-date"></p>
								</div>

								<div>
									<strong>Due Date</strong>
									<p id="due-date"></p>
								</div>

								<div>
									<strong>Created at</strong>
									<p id="created-at"></p>
								</div>

								<div>
									<strong>Updated at</strong>
									<p id="updated-at"></p>
								</div>

								<div>
									<strong>Created by</strong>
									<p id="created-by"></p>
								</div>

								<div>
									<strong>Updated by</strong>
									<p id="updated-by"></p>
								</div>

								<div>
									<strong>Status</strong>
									<p id="status"></p>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<h3 class="box-title" style="padding-left: 0;">Detail Assignee @permission('access-sales-motion')<a class="btn btn-default" href="{{ "/admin/consultant/sales-motion/$salesMotionId/assignee" }}"><i class="fa fa-edit"></i></a>@endpermission</h3>
							<table id="assignee">
								<thead>
									<tr>
										<th data-field="consultant">Consultant Mapped</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
        </div>
      </div>
    </div>
	</div>
@endsection

@section('script')
	<script src="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

	<script>
    $(document).ready(function() {
			var $taskName = $('#task-name');
			var $type = $('#type');
			var $division = $('#division');
			var $objective = $('#objective');
			var $tnc = $('#tnc');
			var $requirements = $('#requirements');
			var $benefit = $('#benefit');
			var $participants = $('#participants');
			var $link = $('#link');
			var $attachment = $('#attachment');
			var $startDate = $('#start-date');
			var $dueDate = $('#due-date');
			var $createdAt = $('#created-at');
			var $updatedAt = $('#updated-at');
			var $createdBy = $('#created-by');
			var $updatedBy = $('#updated-by');
			var $status = $('#status');
			var $assigneeTable = $('#assignee');

			function nullChecker(value) {
				return value ? value : '-';
			}

			axios.get(`/admin/consultant/sales-motion/detail/{{ $salesMotionId }}/data`)
			.then(response => {
				if (response.data.status) {
					$taskName.text(nullChecker(response.data.sales_motion.name));
					$type.text(nullChecker(response.data.sales_motion.type));
					$division.text(nullChecker(response.data.sales_motion.created_by_division));
					$objective.text(nullChecker(response.data.sales_motion.objective));
					$tnc.text(nullChecker(response.data.sales_motion.terms_and_condition));
					$requirements.text(nullChecker(response.data.sales_motion.requirement));
					$benefit.text(nullChecker(response.data.sales_motion.benefit));
					$participants.text(nullChecker(response.data.sales_motion.max_participant));
					$link.text(nullChecker(response.data.sales_motion.link));
					$startDate.text(nullChecker(response.data.sales_motion.start_date));
					$dueDate.text(nullChecker(response.data.sales_motion.due_date));
					$createdAt.text(nullChecker(response.data.sales_motion.created_at));
					$updatedAt.text(nullChecker(response.data.sales_motion.updated_at));
					$createdBy.text(nullChecker(response.data.sales_motion.created_by));
					$updatedBy.text(nullChecker(response.data.sales_motion.updated_by));

					$status.text(response.data.sales_motion.is_active ? 'Active' : 'Not Active');

					if (response.data.sales_motion.photos.small) {
						$attachment.html(`<img src="${response.data.sales_motion.photos.small}" alt="">`);
					} else {
						$attachment.html(`<span>-</span>`);
					}
				}
			})
			.catch(error => {
				alert(error);
			});

			$assigneeTable.bootstrapTable({
				pagination: true,
				pageSize: 15,
				pageList: [],
				sidePagination: 'server',
				url: '/admin/consultant/sales-motion/detail/{{ $salesMotionId }}/data',
				dataField: 'assignees'
			});
    });
	</script>
@endsection