@extends('admin.layouts.main')

@section('style')

	<link rel="stylesheet" href="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.1/min/dropzone.min.css">
	<link rel="stylesheet" href="/css/admin-consultant.css">

	<style>
		.datepicker table tr td {
			color: #222;
		}
		.datepicker table tr td.disabled, .datepicker table tr td.disabled:hover {
			color: #ccc;
		}
	</style>
@endsection

@section('content')
	<div class="box box-default">
		<!-- Box Header -->
		<div class="box-header" style="padding-top: 10px;">
			<div class="col-md-6 text-center">
				<h3 class="box-title" style="padding-left: 0;"><i class="fa fa-list"></i>&nbsp;Create New Sales Motion</h3>
			</div>
		</div>
		<!-- /.box-header -->

    <div class="box-body no-padding no-overflow">
      <!-- Content -->
      <div class="horizontal-wrapper" style="margin-top: 15px;">
				<form id="form-add-new-detail">

					<div style="padding: 15px;">
						<div class="row">
							<div class="col-sm-6">

								<div class="one-input">
									<p>Task Name<span>*</span></p>
									<input id="task-name" type="text" placeholder="" required>
								</div>

								<div class="one-input">
									<p>Sales Motion Type<span>*</span></p>
									<select name="" id="type" required>
										<option value="">Choose Type</option>
										<option value="promotion">Promotion</option>
										<option value="campaign">Campaign</option>
										<option value="product">Product</option>
										<option value="feature">Feature</option>
									</select>
								</div>

								<div class="one-input">
									<p>Created by Division<span>*</span></p>
									<select name="" id="division" required>
										<option value="">Choose Division</option>
										<option value="marketing">Marketing</option>
										<option value="commercial">Commercial</option>
									</select>
								</div>

								<div class="one-input">
									<p>Start Date<span>*</span></p>
									<input type="text" id="start-date" required>
								</div>

								<div class="one-input">
									<p>Due Date<span>*</span></p>
									<input type="text" id="due-date" required>
								</div>

								<div class="one-input">
									<p>Objective</p>
									<input type="text" id="objective" placeholder="">
								</div>

								<div class="one-input">
									<p>Terms and Condition</p>
									<textarea id="tnc" maxlength="250"></textarea>
								</div>

								<div class="one-input">
									<p>Requirement</p>
									<input type="text" id="requirement">
								</div>

								<div class="one-input">
									<p>Benefit</p>
									<input type="text" id="benefit">
								</div>

							</div>
							<div class="col-sm-6">

								<div class="one-input">
									<p>Max Participant</p>
									<input type="text" id="participants">
								</div>

								<div class="one-input">
									<p>Url Link</p>
									<input type="text" id="url-link" placeholder="Insert Document link here">
								</div>

								<div class="one-input">
									<p>Attachment</p>
									<div id="attachment" class="dropzone">
										<input type="hidden" name="_token" value="{{ csrf_token() }}">
										<input type="hidden" name="media_type" value="consultant_photo">
										<input type="hidden" name="need_watermark" value="false">
									</div>
								</div>

							</div>
							<div class="col-sm-12">
								<hr>
								<a href="/admin/consultant/sales-motion" class="btn btn-default"><i class="fa fa-arrow-left"></i>&nbsp;back to sales motion list</a>
								<button type="submit" class="btn btn-success pull-right" style="color: #fff;"><i class="fa fa-plus"></i>&nbsp;Submit</button>
							</div>
						</div>
					</div>

				</form>
      </div>
    </div>
	</div>

	<!-- Modal -->
	<div class="modal fade">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header"></div>
				<div class="modal-body"></div>
				<div class="modal-footer"></div>
			</div>
		</div>
	</div>
	<!-- END Modal -->
@endsection

@section('script')
	<script src="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.27.0/moment.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.1/min/dropzone.min.js"></script>

	<script>
    $(document).ready(function() {
			Dropzone.autoDiscover = false;

			let $taskName = $('#task-name');
			let $type = $('#type');
			let $division = $('#division');
			let $startDate = $('#start-date');
			let $dueDate = $('#due-date');
			let $objective = $('#objective');
			let $tnc = $('#tnc');
			let $requirement = $('#requirement');
			let $benefit = $('#benefit');
			let $participants = $('#participants');
			let $urlLink = $('#url-link');
			let imageId = null;

			// input only number
			$participants.on('input blur paste', function(){
				$(this).val($(this).val().replace(/\D/g, ''));
			});

			$('#start-date').datepicker({
				format: "dd-mm-yyyy",
				autoclose: true,
				maxViewMode: 2,
				todayHighlight: false,
				startDate: new Date()
			});

			$('#due-date').datepicker({
				format: "dd-mm-yyyy",
				autoclose: true,
				maxViewMode: 2,
				todayHighlight: false,
				startDate: moment().add(1, 'days').format()
			});

			$startDate.on('changeDate', function (e) {
				const startDate = moment($startDate.val(), 'DD-MM-YYYY');
				const dueDate = moment($dueDate.val(), 'DD-MM-YYYY');

				$dueDate.datepicker('setStartDate', moment($startDate.val(), 'DD-MM-YYYY').add(1, 'days').format('DD-MM-YYYY'));
				if (dueDate.isBefore(startDate) || startDate == dueDate) {
					$('#due-date').val('');
				}
			});

			let addDropzone = new Dropzone("#attachment", {
				paramName: 'media',
				url: '/admin/media',
				maxFilesize: 5,
				maxFiles: 1,
				acceptedFiles: 'image/*',
				dictDefaultMessage: 'Click to upload a photo',
				dictCancelUpload: 'Cancel',
				dictRemoveFile: 'Delete',
				addRemoveLinks: true,
				thumbnailWidth: null,
				thumbnailHeight: null,
				params: {
					_token: '{{ csrf_token() }}',
					media_type: 'consultant_photo',
					need_watermark: 'false'
				},
				success: function(file, response) {
					file.id = response.media.id;
					imageId = response.media.id;
				},
				init: function() {
					this.on('addedfile', function(file) {
						if (this.files.length > 1) {
							this.removeFile(this.files[0]);
						}
					});

					this.on("removedfile", function (file) {
						if (file.id) {
							$.get("/admin/media/"+file.id+"/delete", function(data, status) {
								imageId = '';
							});
						}
					});
				}
			});


			$('#form-add-new-detail').submit(function() {
				Swal.fire({
					title: 'Adding new sales motion...',
					onBeforeOpen: () => {
						Swal.showLoading();
					},
					allowOutsideClick: false
				});

				axios.post('/admin/consultant/sales-motion/store', {
					name: $taskName.val(),
					type: $type.val(),
					created_by_division: $division.val(),
					start_date: moment($startDate.val(), 'DD-MM-YYYY').format('Y-MM-DD'),
					end_date: moment($dueDate.val(), 'DD-MM-YYYY').format('Y-MM-DD'),
					objective: $objective.val(),
					terms_and_condition: $tnc.val(),
					requirement: $requirement.val(),
					benefit: $benefit.val(),
					max_participant: $participants.val(),
					url: $urlLink.val(),
					media_id: imageId
				})
				.then(response => {
					if (response.data.status) {
						Swal.fire({
							title: 'Done',
							text: 'Added new sales motion. \nYou will be redirected to consultant assignee page.',
							icon: 'success',
							showConfirmButton: false,
							allowOutsideClick: false
						});

						window.location.href = `/admin/consultant/sales-motion/${response.data.sales_motion_id}/assignee`;
					}
				})
				.catch(error => {
					alert(error);
				});

				return false;
			});
    });
	</script>
@endsection