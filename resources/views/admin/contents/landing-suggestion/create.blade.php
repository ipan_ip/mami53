@extends('admin.layouts.main')

@section('style')
    <link rel="stylesheet" href="{{ mix_url('dist/vendor/leaflet/leaflet.css') }}">

    <style>
        #map-canvas {
            height: 470px;
            width: 560px;
        }

        .marker-label {
            background: #fff;
            font-weight: bold;
            padding: 2px;
        }
    </style>
@endsection

@section('content')
<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">{{ $boxTitle }}</h3>
    </div><!-- /.box-header -->
    <form action="{{ route('admin.landing-suggestion.store') }}" method="post" class="form-horizontal form-bordered">
        <div class="box-body no-padding">
            <div class="form-group bg-default">
                <label for="inputName" class="col-sm-2 control-label">Nama</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Nama" id="inputName" name="name"  value="{{ old('name') }}">
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputGeoName" class="col-sm-2 control-label">Koordinat</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Geo Name"
                           id="inputGeoName" name="geo_name">
                </div>
                <div class="col-sm-10 col-sm-offset-2">
                    <div style="background: #EEE; width:560px; height:470px;">
                        <div id="map-canvas"></div>
                    </div>
                </div>
                <div class="col-sm-10 col-sm-offset-2">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="latitude 1"
                           id="inputLatitude1" name="latitude_1" value="{{ old('latitude_1') }}">
                        <input type="text" class="form-control" placeholder="longitude 1"
                           id="inputLongitude1" name="longitude_1" value="{{ old('longitude_1') }}">
                   </div>

                   <hr>

                   <div class="input-group">
                        <input type="text" class="form-control" placeholder="latitude 2"
                           id="inputLatitude2" name="latitude_2" value="{{ old('latitude_2') }}">
                        <input type="text" class="form-control" placeholder="longitude 2"
                           id="inputLongitude2" name="longitude_2" value="{{ old('longitude_2') }}">
                   </div>
                </div>
            </div>


            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Manage Landing Kost Suggestion</h3>
                </div><!-- /.box-header -->

                <div class="box-body no-padding landing-wrapper landing-wrapper-0">
                    <div class="form-group bg-default">
                        <label for="input-alias-0" class="col-sm-2 control-label">Alias</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" placeholder="Alias" id="input-alias-0" name="aliases[]" value="{{old('aliases')[0]}}">
                        </div>
                    </div>
                    <div class="form-group bg-default">
                        <label for="input-landing-0" class="col-sm-2 control-label">Landing Name</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control input-landing-name" placeholder="Landing Name" id="input-landing-0" name="landing_name[]"  value="{{ old('landing_name')[0] }}" data-toggle="dropdown">
                            <input type="hidden" class="input-landing-id" name="landing_ids[]" value="{{ old('landing_ids')[0] }}" id="landing-id-0">
                        </div>
                    </div>
                    <div class="form-group bg-default">
                        <div class="col-sm-10 col-sm-offset-2">
                            <a href="#" class="btn btn-danger btn-remove-landing" data-id="0">Hapus</a>
                        </div>
                    </div>
                </div>

                <div class="form-group" id="landing-btn">
                    <div class="col-sm-offset-2 col-sm-10">
                        <a href="#" class="btn btn-warning" id="landing-adder">Add Landing</a>
                    </div>
                </div>
                
            </div>

            
            
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </form>
</div><!-- /.box -->
@endsection



@section('script')
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&language=id&key={{ENV('MIX_GOOGLE_MAPS_JAVASCRIPT_API_KEY')}}"></script>
<script src="{{ mix_url('dist/vendor/leaflet/leaflet.js') }}"></script>

<script type="text/javascript">
    function addMarkerListener(map, marker, pos) {
        marker.on('dragend', function(evt) {
            var latlng = evt.target.getLatLng();

            var lat = latlng.lat;
            var lng = latlng.lng;

            $('#inputLatitude' + pos).val(lat);
            $('#inputLongitude' + pos).val(lng);
        });
    }

    function addPlaceChangedListener(autocomplete, map, marker) {
        autocomplete.addListener('place_changed', function() {
            marker[0].setOpacity(0);
            marker[1].setOpacity(0);

            var place = autocomplete.getPlace();
            if (!place.geometry) {
                // User entered the name of a Place that was not suggested and
                // pressed the Enter key, or the Place Details request failed.
                window.alert("No details available for input: '" + place.name + "'");
                return;
            }

            map.setView({
                lat: place.geometry.location.lat(),
                lng: place.geometry.location.lng()
            }, 12);

            bottomLeft = {
                lat: place.geometry.location.lat() - 0.05,
                lng: place.geometry.location.lng() - 0.05
            };

            topRight = {
                lat: place.geometry.location.lat() + 0.05,
                lng: place.geometry.location.lng() + 0.05
            };

            marker[0].setLatLng(bottomLeft);
            marker[1].setLatLng(topRight);
            marker[0].setOpacity(1);
            marker[1].setOpacity(1);


            $('#inputLatitude1').val(bottomLeft.lat);
            $('#inputLongitude1').val(bottomLeft.lng);
            $('#inputLatitude2').val(topRight.lat);
            $('#inputLongitude2').val(topRight.lng);
        });
    }

    centerPos = {lat: -7.7858485, lng: 110.3680087}

    var map = L.map('map-canvas', {
        // Set latitude and longitude of the map center (required)
        center: centerPos, 
        // Set the initial zoom level, values 0-18, where 0 is most zoomed-out (required)
        zoom: 12
    });

    var tiles = new L.tileLayer("{{ config('services.osm.host') }}/hot/{z}/{x}/{y}.png").addTo(map);

    var marker = [];
    var markerImage = [];
    var position = [];

    // marker kiri bawah
    markerImage[0] = '{{ asset('assets/icons/kiribawah.png') }}';
    
    position[0] = {
        'lat': centerPos.lat - 0.05,
        'lng': centerPos.lng - 0.05
    }

    marker[0] = new L.Marker(position[0], {
        draggable: true,
        icon: new L.icon({
            iconUrl: markerImage[0],
            iconSize: [60, 57],
            iconAnchor: [30, 57]
        })
    }).addTo(map);

    addMarkerListener(map, marker[0], '1');

    // marker kanan atas
    markerImage[1] = '{{ asset('assets/icons/kananatas.png') }}';
    
    position[1] = {
        'lat': centerPos.lat + 0.05,
        'lng': centerPos.lng + 0.05
    };

    marker[1] = new L.Marker(position[1], {
        draggable: true,
        icon: new L.icon({
            iconUrl: markerImage[1],
            iconSize: [60, 57],
            iconAnchor: [30, 57]
        })
    }).addTo(map);

    addMarkerListener(map, marker[1], '2');

    var autocomplete = new google.maps.places.Autocomplete(document.getElementById('inputGeoName'));
    autocomplete.setFields(['address_components', 'geometry']);

    addPlaceChangedListener(autocomplete, map, marker);
</script>

<!-- script for landing autocomplete -->
<script>
    landing_key = 1;

    function clickSuggestion(idx) {
        key = idx;
        val = $('.suggestion-item-' + idx).text();
        el = $('.suggestion-item-' + idx);

        $(el).parent().parent().prev('.input-landing-name').val(val);
        $(el).parent().parent().next('.input-landing-id').val(key);
    }

    $(function() {
        $('#landing-adder').on('click', function(e) {
            e.preventDefault();

            html = '<div class="box-body no-padding landing-wrapper landing-wrapper-' + landing_key + '">' +
                        '<div class="form-group bg-default">' +
                            '<label for="input-alias-' + landing_key + '" class="col-sm-2 control-label">Alias</label>' +
                            '<div class="col-sm-10">' +
                                '<input type="text" class="form-control" placeholder="Alias" id="input-alias-' + landing_key + '" name="aliases[]">' +
                            '</div>' +
                        '</div>' +
                        '<div class="form-group bg-default">' +
                            '<label for="input-landing-' + landing_key + '" class="col-sm-2 control-label">Landing Name</label>' +
                            '<div class="col-sm-10">' +
                                '<input type="text" class="form-control input-landing-name" placeholder="Landing Name" id="input-landing-' + landing_key + '" name="landing_name[]" data-toggle="dropdown">' +
                                '<input type="hidden" class="input-landing-id" name="landing_ids[]" id="landing-id-' + landing_key +  '">' +
                            '</div>' +
                        '</div>' +
                        '<div class="form-group bg-default">' +
                            '<div class="col-sm-10 col-sm-offset-2">' +
                                '<a href="#" class="btn btn-danger btn-remove-landing" data-id="' + landing_key + '">Hapus</a>' +
                            '</div>' +
                        '</div>' +
                    '</div>';

            $(html).insertBefore('#landing-btn');

            landing_key++;
        });

        var callLandingSuggestion = function(e) {
            $.ajax({
                type: 'POST',
                url: '/admin/area-big-mapper/landing-suggestion',
                data: {
                    'params': e.target.value
                },
                success: function(data) {
                    $('.suggestion-list').remove();
                    // $(e.target).next('.dropdown-menu').remove();

                    listOption = '<ul class="dropdown-menu suggestion-list" aria-labelledby="dLabel">'
                    for(idx in data.landings) {
                        listOption += '<li><a href="#" class="suggestion-item suggestion-item-' + idx + '" data-key="' + idx + '" onclick="clickSuggestion(' + idx + ')">' + data.landings[idx] + '</a></li>';
                    }

                    listOption += '</ul>';

                    $(listOption).insertAfter($(e.target));

                    $('.suggestion-list').dropdown();
                }
                
            })
        }


        var delay = (function(){
          var timer = 0;
          return function(callback, ms){
            clearTimeout (timer);
            timer = setTimeout(callback, ms);
          };
        })();

        $(document).on('keyup', '.input-landing-name', function(e) {
            delay(function(){
                callLandingSuggestion(e)
            }, 1000 );
        });

        $(document).on('click', '.btn-remove-landing', function(e) {
            e.preventDefault();

            id = $(this).data('id');

            $('.landing-wrapper-' + id).remove();
        });
    });
</script>
@endsection
