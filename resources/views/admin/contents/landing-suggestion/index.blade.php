@extends('admin.layouts.main')

@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <a href="{{ URL::route('admin.landing-suggestion.create') }}">
                        <button type="button" name="button" class="btn-add btn btn-primary btn-sm">
                            <i class="fa fa-plus">&nbsp;</i> Add Landing Suggestion </button>
                    </a>

                    <form action="" class="form-inline" method="get" style="text-align:right;padding:10px;">
                        <input type="text" name="q" class="form-control input-sm"  placeholder="Criteria"  autocomplete="off" value="{{ request()->input('q') }}">
                        <button class="btn btn-primary btn-md" id="buttonSearch"><i class="fa fa-search">&nbsp;</i>Search</button>
                    </form>
                </div>
            </div>

            <table  class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nama</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                @foreach($landingSuggestionBlocks as $block)
                    <tr>
                        <td>{{ $block->id }}</td>
                        <td>{{ $block->name }}</td>
                        <td>
                            <a href="{{ route('admin.landing-suggestion.edit', $block->id) }}" title="Edit">
                                <i class="fa fa-pencil"></i>
                            </a>

                            <a href="{{ route('admin.landing-suggestion.delete', $block->id) }}" title="Delete">
                                <i class="fa fa-trash-o"></i>
                            </a>
                        </td>
                    </tr> 
                @endforeach
                </tbody>
                
            </table>
        </div><!-- /.box-body -->
        <div class="box-body no-padding">
            {{ $landingSuggestionBlocks->appends(Request::except('page'))->links() }}
        </div>
    </div><!-- /.box -->
    <!-- table -->
@endsection