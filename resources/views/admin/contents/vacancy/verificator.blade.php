@extends('admin.layouts.main')

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>


        <form action="{{ URL::to('admin/jobs/verify',$data->id) }}" method="GET" id="formInsertDesigner" class="form-horizontal form-bordered" role="form" enctype="multipart/form-data">  
        {{ csrf_field() }}

        <input type="hidden" name="for" value="verify">
        
        <div class="box-body no-padding">
            <div class="form-group bg-default">
                <label for="inputName" class="col-sm-2 control-label">Input Admin</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Input name"
                           id="inputName" value="{{ $data->agent_name }}" name="input_by" >
                           <br/>
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputName" class="col-sm-2 control-label">Verificator Name</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Input name"
                           id="inputName" name="verificator" >
                           <br/>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary">Verify Now</button>
                </div>
            </div>
    </form><!-- /.form -->
    </div><!-- /.box -->
@stop