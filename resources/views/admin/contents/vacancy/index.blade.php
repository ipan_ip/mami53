@extends('admin.layouts.main')

@section('style')
    <style>
        tr.group-niche td,
        .text-niche {
            color: #c58112;
        }

    </style>
@endsection

@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default" style="margin-left: 10px;">
                    <a href="{{ URL::to('admin/jobs/apply/list') }}" class="btn btn-xs btn-danger">Jobs Apply</a>
                    <a href="{{ URL::to('admin/jobs/create') }}" class="btn btn-xs btn-warning">Add New</a>
                    <a href="{{ URL::to('admin/jobs/i/import') }}" class="btn btn-xs btn-default">Import CSV</a>
                    <a href="{{ URL::to('admin/jobs/company') }}" class="btn btn-primary">Company Profile</a>
                    <a href="{{ URL::to('admin/spesialisasi') }}" class="btn btn-xs btn-primary">Spesialisasi</a>
                    <a href="{{ URL::to('admin/industry') }}" class="btn btn-xs btn-success">Industri</a>
                    
                    {{ Form::open(array('method'=>'get','class'=>'form-inline','style'=>'text-align:right;padding:10px;')) }}
                        <input type="text" name="q" class="form-control input-sm"  placeholder="Vacancy name"  autocomplete="off" value="{{ Request()->input('q') }}">
                        <input type="text" name="place_name" class="form-control input-sm" placeholder="Company" value="{{ Request()->input('place_name') }}">
                        <input type="text" name="position" class="form-control input-sm" placeholder="Position" value="{{ Request()->input('position') }}">
                        {{ Form::select('status', $status, request()->input('status'), array('class' => 'pull-right-sort form-group btn btn-primary btn-sm')) }}

                        {{ Form::select('job_type', $jobTypes, request()->input('job_type'), array('class' => 'pull-right-sort form-group btn btn-primary btn-sm')) }}

                        {{ Form::select('data_input', $data_input, request()->input('data_input'), array('class' => 'pull-right-sort form-group btn btn-primary btn-sm')) }}

                        <button class="btn btn-primary btn-md" id="buttonSearch"><i class="fa fa-search">&nbsp;</i>Search</button>
                    {{ Form::close() }}
                </div>
            </div>

            <p>
                Baris yang berwarna <strong class="text-niche">orange</strong> adalah loker niche
            </p>

            <table id="tableListLanding" class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Jobs Name</th>
                        <th>Type</th>
                        <th>Salary</th>
                        <th>Apply</th>
                        <th>Agent</th>
                        <th>Status</th>
                        <th>Owner Ver.</th>
                        <th>Input date</th>
                        <th class="table-action-column" width="120px">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($vacancys as $rowLandingKey => $value)
                    <tr class="group-{{ $value->group }}">
                        <td>{{ $value->id }}</td>
                        <td>{{ $value->name }} <label class="label label-warning">{{ $value->data_input }}</label></td>
                        <td>{{ $value->type }}</td>
                        <td>{{ number_format($value->salary,0,",",".") }}/{{ $value->salary_time }}</td>
                        <td><a href="{{ URL::to('admin/jobs/apply', $value->id) }}">{{ count($value->vacancy_apply) }} User</a></td>
                        <td>{{ $value->agent_name }} <label class="label label-default">{{ $value->verificator }}</label></td>
                        <td><label class="label @if($value->status == 'verified') label-primary @elseif ($value->status == 'waiting') label-warning @else label-danger @endif ">{{ $value->status }}</label></td>
                        <td>@if ($value->deleted_owner == 1)
                                <label class="label label-default">deleted</label>
                            @endif
                        </td>
                        <td>{{ $value->created_at->format('d M Y H:i') }}</td>
                        <td>
                            <a href="/admin/jobs/redirect/{{ $value->id }}"><i class="fa fa-external-link"></i></a>
                            @if ($value->status == 'waiting')
                                @if ($value->data_input == 'scrap koran')
                                    <a href="{{ URL::to('admin/jobs/verify',$value->id) }}?for=input_verificator" title="Verify"><i class="fa fa-arrow-up"></i></a>
                                @else
                                    <a href="{{ URL::to('admin/jobs/verify',$value->id) }}?for=verify" title="Verify"><i class="fa fa-arrow-up"></i></a>
                                @endif 
                            @else 
                              <a href="{{ URL::to('admin/jobs/verify',$value->id) }}?for=reject" title="Rejected"><i class="fa fa-arrow-down"></i></a> 
                            @endif
                            <a href="{{ URL::to('admin/jobs/verifyslug', $value->id) }}"><i class="fa fa-fighter-jet"></i></a>  
                            <a href="{{ route('admin.jobs.edit', $value->id) }}" title="Edit"><i class="fa fa-pencil"></i></a> 
                            <a href="{{ URL::to('admin/jobs/delete', $value->id) }}" onclick="return confirm('Yakin mau hapus saya?')" title="Delete"><i class="fa fa-trash-o"></i></a> 
                            <!-- <a href="{{-- URL::to('lowongan', $value->slug) --}}" target="_blank" rel="noopener" title="View"><i class="fa fa-eye"></i></a> -->
                            <a href="{{ url($value->share_slug) }}" target="_blank" rel="noopener" title="View"><i class="fa fa-eye"></i></a>
                            @if ($value->status != 'waiting' && (is_null($value->user_id) || $value->user_id == 4))
                                <a href="{{ URL::to('admin/jobs/create-account', $value->id) }}" title="Create Account"><i class="fa fa-user"></i></a>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="box-body no-padding">
                {{ $vacancys->appends(Request::except('page'))->links() }}
            </div>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
    <!-- table -->
@endsection
