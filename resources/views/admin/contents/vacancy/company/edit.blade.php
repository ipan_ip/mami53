@extends('admin.layouts.main')

@section('style')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/dropzone.css">
    {{ HTML::style('assets/vendor/jquery-chosen/chosen.css') }}
    {{ HTML::style('assets/vendor/jquery-chosen-custom/chosen.css') }}
    <link rel="stylesheet" href="/assets/vendor/summernote/summernote.css?v=05042018">
    <link rel="stylesheet" href="{{ mix_url('dist/vendor/leaflet/leaflet.css') }}">

    <style>
        #chosenForm .chosen-choices {
            border: 1px solid #ccc;
            border-radius: 4px;
            min-height: 34px;
            padding: 6px 12px;
        }
        #chosenForm .form-control-feedback {
            /* To make the feedback icon visible */
            z-index: 100;
        }

        #map-canvas-1,
        #map-canvas-2 {
            height: 300px;
            width: 100%;
        }
    </style>
@endsection

@section('content')
<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">{{ $boxTitle }}</h3>
    </div><!-- /.box-header -->
    <form action="{{ URL::To('admin/jobs/company/edit', $data->id) }}" method="post" class="form-horizontal form-bordered">

        <div class="box-body no-padding">
            <div class="form-group bg-default">
                <label for="inputUrlSlug" class="col-sm-2 control-label">Company Name</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Company Name" id="inputSlugUrl" name="name"  value="{{ $data->name }}">
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputUrlSlug" class="col-sm-2 control-label">Industry : </label>
                <div class="col-sm-10">
                    <select name="industry" class="form-control">
                        <option value="">Pilih Industry</option>
                        @if (!is_null($industry))
                        @foreach ($industry AS $key => $value)
                            <option value="{{ $value['key'] }}" @if ($value['key'] == $data->industry_id) selected="true" @endif >{{ $value['value'] }}</option>
                        @endforeach
                        @endif
                    </select>
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputAddress" class="col-sm-2 control-label">Address</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Address" id="inputAddress" name="address" value="{{ $data->address }}">
                </div>
            </div>

            <div class="form-group bg-default">
            <label for="inputIndexed" class="col-sm-2 control-label">Website</label>
                <div class="col-sm-10">
                    <input type="text" value="{{ $data->website }}" name="website" class="form-control">
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputUrlSlug" class="col-sm-2 control-label">Size</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Place Name" id="inputSlugUrl" name="size"  value="{{ $data->size }}">
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputGeoName" class="col-sm-2 control-label">Koordinat</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Geo Name"
                            id="inputGeoName" name="geo_name">
                </div>
                <div class="col-sm-10 col-sm-offset-2">
                    <div style="background: #EEE; width: 100%; height: 300px;">
                        <div id="map-canvas"></div>
                    </div>
                </div>
                <div class="col-sm-10 col-sm-offset-2">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="latitude"
                            id="latitude" name="latitude" value="{{ $data->latitude }}">
                        <input type="text" class="form-control" placeholder="longitude"
                            id="longitude" name="longitude" value="{{ $data->longitude }}">
                    </div>
                </div>
            </div>


            <div class="form-group bg-default">
                <label for="subdistrict" class="control-label col-sm-2">Subdistrik</label>
                <div class="col-sm-10">
                    <input type="text" name="subdistrict" id="subdistrict" class="form-control" value="{{ $data->subdistrict }}">
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="area_city" class="control-label col-sm-2">Kota / Daerah</label>
                <div class="col-sm-10">
                    <input type="text" name="city" id="area_city" class="form-control" value="{{ $data->city }}">
                </div>
            </div>


            <div class="form-group bg-default">
                <label for="inputUrlSlug" class="col-sm-2 control-label">Dresscode</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Dresscode" id="inputSlugUrl" name="dresscode"  value="{{ $data->dresscode }}">
                </div>
            </div>  

<!--             <div class="form-group bg-default">
                <label for="inputUrlSlug" class="col-sm-2 control-label">Position</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Position" id="inputSlugUrl" name="position"  value="{{ $data->position }}">
                </div>
            </div> -->

            <div class="form-group bg-default">
                <label for="inputUrlSlug" class="col-sm-2 control-label">Language</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="language" id="inputSlugUrl" name="language"  value="{{ $data->language }}">
                </div>
            </div>  

            <div class="form-group bg-default">
                <label for="inputUrlSlug" class="col-sm-2 control-label">Time</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Time" id="inputSlugUrl" name="time"  value="{{ $data->time }}">
                </div>
            </div>  


            <div class="form-group bg-default">
                <label for="inputDescription" class="col-sm-2 control-label">Deskripsi</label>
                <div class="col-sm-10">
                    <textarea name="description" id="inputDescription" class="form-control">{{ $data->description }}</textarea>
                </div>
            </div> 

            <div class="form-group bg-default">
                <label for="inputUrlSlug" class="col-sm-2 control-label">Subsidy</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Subsidy" id="inputSlugUrl" name="subsidy"  value="{{ $data->subsidy }}">
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputUrlSlug" class="col-sm-2 control-label">Facilities</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" placeholder="Facilities" id="inputOwnerPhone" name="facilities"  value="{{ $data->facilities }}">
                </div>
            </div>  

            <div class="form-group bg-default">
                <label for="inputUrlSlug" class="col-sm-2 control-label">Email</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Email" id="inputSlugUrl" name="email"  value="{{ $data->email }}">
                </div>
            </div> 

            <div class="form-group bg-default">
                <label for="agen" class="col-sm-2 control-label">Phone</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Phone" id="agen" name="phone"  value="{{ $data->phone }}">
                </div>
            </div> 

            <div class="form-group bg-default">
                <label for="logo" class="control-label col-sm-2">Logo</label>
                <div class="col-sm-10">
                    <div id="logo"
                          action="{{ url('admin/media') }}"
                          method="POST"
                          class="dropzone"
                          uploadMultiple="no">
                        {{ csrf_field() }}
                    </div>
                </div>
            </div>

            <div id="logo-wrapper">
                @if(!is_null($data->photo_id))
                    <input type="hidden" name="photo_id" value="{{ $data->photo_id }}">
                @endif
            </div>

            <div class="form-group bg-default">
                <div class="col-sm-10 col-sm-push-2">
                    @if(!is_null($data->photo))
                        <img src="{{ $data->photo->getMediaUrl()['small'] }}" class="logo-preview" alt="logo-preview">
                    @endif
                </div>
            </div>


            <div class="form-group bg-info divider">
                <div class="col-sm-12">
                    Global Setting
                </div>
            </div>   

            <div class="form-group bg-default">
            <label for="inputIndexed" class="col-sm-2 control-label">Live Now</label>
            <div class="col-sm-10">
                <div class="checkbox">
                    <input type="checkbox" @if ($data->is_active == 1) checked="true" @endif name="live"> Yes
                </div>
            </div>
            </div>

             <div class="form-group bg-info divider">
                <div class="col-sm-12">
                    Agent
                </div>
            </div>   

            <div class="form-group bg-default">
            <label for="inputIndexed" class="col-sm-2 control-label">Agent name</label>
            <div class="col-sm-10">
                <div class="checkbox">
                    <input type="text" name="agent_name" placeholder="Agent name" value="{{ $data->agent_name }}" class="form-control" />
                </div>
            </div>

            </div>
               <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </form>
</div><!-- /.box -->
@endsection



@section('script')

{{ HTML::script('assets/vendor/jquery-chosen/chosen.jquery.js') }}
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/dropzone.js"></script>
<script src="/assets/vendor/summernote/summernote.min.js"></script>

<script>
    var config = {
        '.chosen-select'           : {width: '100%'}
    };

    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>


<script src="https://maps.googleapis.com/maps/api/js?libraries=places&language=id&key={{ENV('MIX_GOOGLE_MAPS_JAVASCRIPT_API_KEY')}}"></script>
<script src="{{ mix_url('dist/vendor/leaflet/leaflet.js') }}"></script>

<script type="text/javascript">
    
    <?php
        if (is_null($data->latitude) OR is_null($data->longitude)) {
    ?>
        centerPos = {lat: -7.7858485, lng: 110.3680087};
    <?php          
        } else {
    ?>
        centerPos = {lat: <?php echo $data->latitude; ?>, lng: <?php echo $data->longitude; ?>};
    <?php 
        }
    ?>

    var map = L.map('map-canvas', {
        // Set latitude and longitude of the map center (required)
        center: centerPos, 
        // Set the initial zoom level, values 0-18, where 0 is most zoomed-out (required)
        zoom: 12
    });

    var tiles = new L.tileLayer("{{ config('services.osm.host') }}/hot/{z}/{x}/{y}.png").addTo(map);

    var marker = new L.Marker(centerPos, {
        draggable: true
    }).addTo(map);

    var autocomplete = new google.maps.places.Autocomplete(document.getElementById('inputGeoName'));
    autocomplete.setFields(['address_components', 'geometry']);

    var geocoder = new google.maps.Geocoder();

    marker.on('dragend', function(evt) {
        var latlng = evt.target.getLatLng();

        var lat = latlng.lat;
        var lng = latlng.lng;

        geocoder.geocode({'latLng': latlng}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[1]) {

                    map.setView(latlng);

                    $('#inputGeoName').val(results[1].formatted_address);
                    $('#latitude').val(lat);
                    $('#longitude').val(lng);

                    setDistrictAndCity(results[0]);
                } else {
                    alert('No results found');
                }
            } else {
                alert('Geocoder failed due to: ' + status);
            }
        });
    });


    autocomplete.addListener('place_changed', function() {
        marker.setOpacity(0);
        var place = autocomplete.getPlace();
        if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place.name + "'");
            return;
        }

        latlng = {
            lat: place.geometry.location.lat(),
            lng: place.geometry.location.lng()
        };

        map.setView(latlng);
        marker.setLatLng(latlng);
        marker.setOpacity(1);

        $('#latitude').val(place.geometry.location.lat);
        $('#longitude').val(place.geometry.location.lng);

        setDistrictAndCity(place);
    });

    function setDistrictAndCity(response) {
        subDistrict = '';
        city = '';
        response.address_components.forEach(function(component) {
            // console.log(component.types);
            if ($.inArray("administrative_area_level_3", component.types) != -1) {
                subDistrict = component.short_name;
            }

            if ($.inArray("administrative_area_level_2", component.types) != -1) {
                city = component.short_name;      
            }

        });

        $('#subdistrict').val(subDistrict);
        $('#area_city').val(city);
    }

    var geocoder;
    geocoder = new google.maps.Geocoder();
    
    <?php
        if (is_null($data->latitude) OR is_null($data->longitude)) {
    ?>
        var latlng = new google.maps.LatLng(-7.7858485, 110.3680087);
    <?php          
        } else {
    ?>
        var latlng = new google.maps.LatLng(<?php echo $data->latitude; ?>, <?php echo $data->longitude; ?>);
    <?php 
        }
    ?>


            Dropzone.autoDiscover = false;
        Dropzone.options.logo = {
            paramName : "media",
            maxFilesize : 6,
            maxFiles: 1,
            accepFiles: '.jpg, .jpeg, .png, .bmp',
            dictDefaultMessage : 'Upload Company Logo',
            addRemoveLinks: true,
            success : function (file, response) {
                if($('#logo-wrapper input[name=photo_id]').size() > 0) {
                    $('.logo-preview').remove();
                    $('#logo-wrapper input[name=photo_id]').val(response.media.id);
                } else {
                    $('#logo-wrapper').append('<input type="hidden" name="photo_id" value="' +response.media.id+ '">');
                }
            },
            init: function() {
                this.on("removedfile", function(file) {
                    console.log('remove');
                    $('#logo-wrapper input[name=photo_id]').remove();
                });

                this.on('sending', function(file, xhr, data) {
                    data.append('media_type', 'company_photo');
                    data.append('watermarking', false);
                })
            }
        };

        $('#logo').dropzone();

    $('#inputDescription').summernote({
        height: 300,
        toolbar: [
            ['style', ['style', 'bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['insert', ['link', 'table', 'hr']],
            ['misc', ['codeview']]
        ]
    });

    $('#checknohpowner').click(function(e) {
      phone = $("#inputOwnerPhone").val();
      $("#kost_show").append("Loading ................ ");
      $.ajax({
        url: '/admin/patrick/vacancy/check-from-no-hp?phone=' + phone,
        type: 'GET',
        dataType: 'json',
        success: function(data) {
          $("#kost_show").empty();  
          if(data.status == false) {
            
            $("#kost_show").append("<div class='alert alert-danger' style='width: 100%;'>No hp diatas belum punya lowongan.");
          } else {
            var project = "<strong>List</strong>";

            $.each(data.data, function(index, value) {
                project += "<ul>";
                project += "<li>"+value.id+" - "+value.name+"</li>";
                project += "</ul>";
            });

            $("#kost_show").append(project);
          }
        }
      });
    });

</script>
@endsection
