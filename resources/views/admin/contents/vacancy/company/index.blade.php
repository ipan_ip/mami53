@extends('admin.layouts.main')

@section('style')
    <style>
        tr.redirected td {
            color: #ff0000;
        }

        .text-redirected {
            color: #ff0000;
        }
    </style>
@endsection

@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
            <a href="{{ URL::to('admin/jobs/company/new') }}" class="btn btn-danger">Add new</a>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
               
            </div>

            <table id="tableListLanding" class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Company Name</th>
                        <th>Is live</th>
                        <th class="table-action-column" width="120px">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($company as $key => $value)
                    <tr>
                        <td>{{ $value->id }}</td>
                        <td>{{ $value->name }}</td>
                        <td>{{ $value->is_active }}</td>
                        <td><label class="label @if($value->is_active == 1) label-primary @else label-danger @endif ">{{ $value->status }}</label></td>
                        <td>
                            
                            <a href="{{ URL::to('admin/jobs/company/edit',$value->id) }}" title="Edit" class="btn btn-xs btn-success"><i class="fa fa-pencil"></i></a>
                            <a href="{{ URL::to('admin/jobs/company/delete',$value->id) }}" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i></a> 
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="box-body no-padding">
                {{ $company->appends(Request::except('page'))->links() }}
            </div>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
    <!-- table -->
@endsection
