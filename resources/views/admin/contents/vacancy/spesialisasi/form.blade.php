@extends('admin.layouts.main')

@section('style')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/dropzone.css">
    {{ HTML::style('assets/vendor/jquery-chosen/chosen.css') }}
    {{ HTML::style('assets/vendor/jquery-chosen-custom/chosen.css') }}
    <link rel="stylesheet" href="/assets/vendor/summernote/summernote.css">

    <style>
        #chosenForm .chosen-choices {
            border: 1px solid #ccc;
            border-radius: 4px;
            min-height: 34px;
            padding: 6px 12px;
        }
        #chosenForm .form-control-feedback {
            /* To make the feedback icon visible */
            z-index: 100;
        }

        #map-canvas-1,
        #map-canvas-2 {
            height: 300px;
            width: 100%;
        }
    </style>
@endsection

@section('content')
<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">{{ $boxTitle }}</h3>
    </div><!-- /.box-header -->
        {{ Form::open(array('route' => $formAction, 'method' => $formMethod,
            'class' => 'form-horizontal form-bordered', 'role' => 'form', 'id' => 'formInsertBank', 'enctype' => 'multipart/form-data')) }}

        {{ csrf_field() }}

        <div class="box-body no-padding">
            <div class="form-group bg-default">
                <label for="inputType" class="col-sm-2 control-label">Type</label>
                <div class="col-sm-10">
                    <select name="type" class="form-control chosen-select" id="inputType">
                        @foreach($vacancyTypes as $key => $vacancyType)
                            <option value="{{ $key }}" {{ old('type', $data->vacancy_spesialisasi_type_id) == $key ? 'selected="selected"' : '' }}>{{ $vacancyType }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputGroup" class="col-sm-2 control-label">Group</label>
                <div class="col-sm-10">
                    <select name="group" class="form-control chosen-select" id="inputGroup">
                        @foreach($groups as $key => $group)
                         <option value="{{ $key }}" {{ old('group', $data->group) == $key ? 'selected="selected"' : '' }}>{{ $group }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputName" class="col-sm-2 control-label">Name</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Name" id="inputName" name="spesialisasi_name"  value="{{ $data->name }}">
                </div>
            </div>

            <div class="form-group bg-default">
              <label for="inputActive" class="col-sm-2 control-label">Active</label>
              <div class="col-sm-10">
                <div class="checkbox">
                  <label>
                    <input type="checkbox" @if($data->is_active == 1) checked="checked" @endif name="is_active">
                    Active now
                  </label>
                </div>
              </div>
              </div>
           
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    {{ Form::close() }}
</div><!-- /.box -->
@endsection

@section('script')
<script src="/assets/vendor/jquery-chosen/chosen.jquery.js"></script>

<script>
    $(function() {
        var config = {
            '.chosen-select'           : {width: '100%'}
        };
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }

        if($('#inputGroup').val() == 'general') {
            $('#inputSpeciality optgroup.opt-general').show();
            $('#inputSpeciality optgroup.opt-niche').hide();
            console.log('yes');
        } else {
            $('#inputSpeciality optgroup.opt-niche').show();
            $('#inputSpeciality optgroup.opt-general').hide();
            console.log('no');
        }

        $('#inputGroup').on('change', function(e) {
            if($(this).val() == 'general') {
                $('#inputSpeciality optgroup.opt-general').show();
                $('#inputSpeciality optgroup.opt-niche').hide();
                console.log('yes2');
            } else {
                $('#inputSpeciality optgroup.opt-niche').show();
                $('#inputSpeciality optgroup.opt-general').hide();
                console.log('no2');
            }

            $('#inputSpeciality').val('');
        });
    });
</script>
@endsection