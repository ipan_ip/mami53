@extends('admin.layouts.main')

@section('style')
    <style>
        tr.redirected td {
            color: #ff0000;
        }

        .text-redirected {
            color: #ff0000;
        }
    </style>
@endsection

@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default" style="margin-left: 10px;">
                    <a href="{{ URL::to('admin/jobs/apply/list') }}" class="btn btn-xs btn-danger">Jobs Apply</a>
                    <a href="{{ URL::to('admin/jobs/create') }}" class="btn btn-xs btn-warning">Add New</a>
                    <a href="{{ URL::to('admin/jobs/i/import') }}" class="btn btn-xs btn-default">Import CSV</a>
                    <a href="{{ URL::to('admin/vacancy-speciality-types') }}" class="btn btn-xs btn-info">Vacancy Types</a>
                    <a href="{{ URL::to('admin/spesialisasi/create') }}" class="btn btn-xs btn-primary">Add New Spesialisasi</a>
                    <a href="{{ URL::to('admin/jobs/industry') }}" class="btn btn-xs btn-success">Industri</a>
                    
                    {{ Form::open(array('method'=>'get','class'=>'form-inline','style'=>'text-align:right;padding:10px;')) }}
                        <input type="text" name="q" class="form-control input-sm"  placeholder="Keyword"  autocomplete="off" value="{{ Input::old('q') }}">
                        <button class="btn btn-primary btn-md" id="buttonSearch"><i class="fa fa-search">&nbsp;</i>Search</button>
                    {{ Form::close() }}
                </div>
            </div>

            <table id="tableListLanding" class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Type</th>
                        <th>Name</th>
                        <th>Group</th>
                        <th>Is active</th>
                        <th class="table-action-column" width="120px">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($spesialisasi AS $key => $value)
                        <tr>
                            <td>{{ $key+1 }}</td>
                            <td>{{ $value->vacancy_spesialisasi_type ? $value->vacancy_spesialisasi_type->name : '' }}</td>
                            <td>{{ $value->name }}</td>
                            <td>{{ $value->group }}</td>
                            <td>
                                @if($value->is_active == 1)
                                    <label class="label label-success">Yes</label>
                                @else
                                    <label class="label label-danger">No</label>
                                @endif
                            </td>
                            <td>
                                <a href="{{ URL::to('admin/spesialisasi/destroy', $value->id) }}" class="btn btn-xs btn-danger">delete</a>
                                <a href="{{ URL::to('admin/spesialisasi/'.$value->id.'/edit') }}" class="btn btn-xs btn-primary">edit</a>
                            </td>
                        </tr>
                    @endforeach              
                </tbody>
            </table>
            <div class="box-body no-padding">
                {{ $spesialisasi->appends(Request::except('page'))->links() }}
            </div>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
    <!-- table -->
@endsection
