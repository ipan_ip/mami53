@extends('admin.layouts.main')
@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
           
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <form class="form-horizontal" method="post" action="">
              <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Phone Number</label>
                <div class="col-sm-10">
                  <input type="text" name="user_phone" value="{{ $user_phone }}" class="form-control" id="inputEmail3" placeholder="Phone number">
                </div>
              </div>
              <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                <div class="col-sm-10">
                  <input type="text" name="user_email" value="{{ $user_email }}" class="form-control" id="inputEmail3" placeholder="User Email">
                </div>
              </div>
              <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">Link</label>
                <div class="col-sm-10">
                  <input type="text" disabled="true" value="{{ 'https://mamikos.com/p/'.$link }}" class="form-control" id="inputPassword3" placeholder="">
                  <input type="hidden" name="link" value="{{ $link }}">
                </div>
              </div>
             
             <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" checked="true" disabled="true" value="claim"> Claim
                    </label>
                  </div>
                </div>
              </div>   

              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" checked="true" disabled="true" value="sms"> Notif Sms
                    </label>
                  </div>
                </div>
              </div> 

              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" checked="true" disabled="true" value="sms"> Notif Email
                    </label>
                  </div>
                </div>
              </div>              

              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" checked="true" disabled="true" value="owner"> Is Owner
                    </label>
                  </div>
                </div>
              </div>              
              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <button type="submit" class="btn btn-success">Create owner</button>
                </div>
              </div>
            </form>
            <br/>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
    <!-- table -->

    
@stop
@section('script')
<!-- page script -->
<script type="text/javascript">
    $(function()
    {
        $("#tableListTag").dataTable();
    });
</script>
@stop