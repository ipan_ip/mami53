@extends('admin.layouts.main')

@section('style')
    <link rel="stylesheet" href="/assets/vendor/summernote/summernote.css?v=05042018">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/dropzone.css">
    <link rel="stylesheet" href="{{ mix_url('dist/vendor/leaflet/leaflet.css') }}">

    <style>
        #map-canvas {
            height: 300px;
            width: 100%;
        }
    </style>
@endsection

@section('content')
<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">{{ $boxTitle }}</h3>
    </div><!-- /.box-header -->
    <form action="{{ route('admin.jobs.store') }}" method="post" class="form-horizontal form-bordered">
        <div class="box-body no-padding">
            <div class="form-group bg-default">
                <label for="inputPlaceName" class="col-sm-2 control-label">Nama Tempat Usaha</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Nama Tempat Usaha" id="inputSlugUrl" name="place_name" value="{{ old('place_name') }}">
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputAddress" class="col-sm-2 control-label">Alamat</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Alamat" id="inputAddress" name="address" value="{{ old('address') }}">
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputAddress" class="col-sm-2 control-label">Penempatan Kerja</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Alamat" id="inputAddress" name="workplace" value="{{ old('workplace') }}">
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputIndexed" class="col-sm-2 control-label">Penempatan Kerja</label>
                <div class="col-sm-10">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" checked="checked" name="workplace_status" value="1">
                            Kandidate harus bersedia di tempatkan disini.
                        </label>
                    </div>
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputGeoName" class="col-sm-2 control-label">Koordinat</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Geo Name"
                           id="inputGeoName" name="geo_name">
                </div>
                <div class="col-sm-10 col-sm-offset-2">
                    <div style="background: #EEE; width: 100%; height: 300px;">
                        <div id="map-canvas"></div>
                    </div>
                </div>
                <div class="col-sm-10 col-sm-offset-2">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="latitude"
                           id="inputLatitude" name="latitude" value="{{ old('latitude') }}">
                        <input type="text" class="form-control" placeholder="longitude"
                           id="inputLongitude" name="longitude" value="{{ old('longitude') }}">
                   </div>
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputCity" class="col-sm-2 control-label">Kota</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Kota" id="inputCity" name="city" placeholder="Kota" value="{{ old('city') }}">
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputSubdistrict" class="col-sm-2 control-label">Subdistrict</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Subdistrict" id="inputSubdistrict" name="subdistrict" value="{{ old('subdistrict') }}">
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputJobsName" class="col-sm-2 control-label">Nama Pekerjaan</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Nama Pekerjaan" id="inputJobsName" name="jobs_name" value="{{ old('jobs_name') }}">
                </div>
            </div>  

            <div class="form-group bg-default">
                <label for="inputPosition" class="col-sm-2 control-label">Posisi</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Posisi" id="inputPosi" name="position" value="{{ old('position') }}">
                </div>
            </div> 

            <div class="form-group bg-default">
                <label for="inputDescription" class="col-sm-2 control-label">Deskripsi</label>
                <div class="col-sm-10">
                    <textarea name="description" id="inputDescription" class="form-control">{{old('description')}}</textarea>
                </div>
            </div> 

            <div class="form-group bg-default">
                <label for="inputSalary" class="col-sm-2 control-label">Gaji</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" placeholder="Min Gaji" id="inputSalary" name="salary" value="{{ old('salary') }}">
                </div>
                <div class="col-sm-5">
                    <input type="text" class="form-control" placeholder="Max Gaji" id="inputSalary" name="max_salary" value="{{ old('max_salary') }}">
                </div>
            </div>  

            <div class="form-group bg-default">
                <label for="inputSalaryTime" class="col-sm-2 control-label">Satuan Gaji</label>
                <div class="col-sm-10">
                  <select class="form-control" name="salary_time" id="inputSalaryTime">   
                    @foreach ($times AS $key => $value)
                        <option value="{{ $value }}" {{ old('salary_time') == $value ? 'selected="selected"' : '' }}>{{ $value }}</option>
                    @endforeach
                  </select>  
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputType" class="col-sm-2 control-label">Status Pekerjaan</label>
                <div class="col-sm-10">
                  <select class="form-control" name="type" id="inputType">   
                    @foreach ($type AS $key => $value)
                        <option value="{{ $value }}" {{ old('type') == $value ? 'selected="selected"' : '' }}>{{ $value }}</option>
                    @endforeach
                  </select>  
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputLastEducation" class="col-sm-2 control-label">Pendidikan Terakhir</label>
                <div class="col-sm-10">
                  <select class="form-control" name="last_education" id="inputLastEducation">   
                    @foreach ($educationOptions AS $key => $education)
                        <option value="{{ $key }}" {{ old('last_education') == $key ? 'selected="selected"' : '' }}>{{ $education }}</option>
                    @endforeach
                  </select>  
                </div>
            </div>

            <div class="form-group bg-info divider">
              <div class="col-sm-12 pull-left">
                User
              </div>
            </div> 

            <div class="form-group bg-default">
                <label for="inputUserName" class="col-sm-2 control-label">Nama Penanggung Jawab</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Nama Penanggung Jawab" id="inputUserName" name="user_name" value="{{ old('user_name') }}">
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputUserPhone" class="col-sm-2 control-label">No. Telp. Penanggung Jawab</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="No. Telp. Penanggung Jawab" id="inputUserPhone" name="user_phone" value="{{ old('user_phone') }}">
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputUserEmail" class="col-sm-2 control-label">Email Penanggung Jawab</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Email Penanggung Jawab" id="inputUserEmail" name="user_email" value="{{ old('user_email') }}">
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputDetailUrl" class="col-sm-2 control-label">URL Sumber Data</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="URL Sumber Data" id="inputDetailUrl" name="detail_url" value="{{ old('detail_url') }}">
                </div>
            </div>

             <div class="form-group bg-info divider">
              <div class="col-sm-12 pull-left">
                Agent
              </div>
            </div> 

            <div class="form-group bg-default">
                <label for="agen" class="col-sm-2 control-label">Agent Name</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Agent Name" id="agen" name="agent_name"  value="{{ old('agent_name') }}">
                </div>
            </div> 

            <div class="form-group bg-default">
                <label for="verificator" class="col-sm-2 control-label">Verificator</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Verificator Name" id="verificator" name="verificator"  value="{{ old('verificator') }}">
                </div>
            </div>

            <!-- <div class="form-group bg-info divider">
              <div class="col-sm-12 pull-left">
                Penginput
              </div>
            </div>  -->

<!--             <div class="form-group bg-default">
                <label for="inputAgentName" class="col-sm-2 control-label">Nama Penginput</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Nama Penginput" id="inputAgentName" name="agent_name" value="{{ old('agent_name') }}">
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputAgentType" class="col-sm-2 control-label">Tipe Penginput</label>
                <div class="col-sm-10">
                  <select class="form-control" name="agent_type" id="inputAgentType">   
                    @foreach ($agentType AS $value)
                        <option value="{{ $value }}" {{ old('agent_type') == $value ? 'selected="selected"' : '' }}>{{ $value }}</option>
                    @endforeach
                  </select>  
                </div>
            </div> -->
            <div class="form-group bg-info divider">
              <div class="col-sm-12 pull-left">
                Additional
              </div>
            </div> 

            <!--
            <div class="form-group bg-default">
                <label for="inputGroup" class="col-sm-2 control-label">Group</label>
                <div class="col-sm-10">
                    <select class="form-control" name="group" id="inputGroup">
                            <option value="niche">Niche</option>
                    </select>
                </div>
            </div>
            -->

            <div class="form-group bg-default">
                <label for="verificator" class="col-sm-2 control-label">Spesialisasi</label>
                <div class="col-sm-10">
                    <select class="form-control" name="spesialisasi" id="inputSpesialisasi">
                        @foreach ($spesialisasi as $key => $value)
                            <option value="{{ $value->id }}" {{ old('spesialisasi') == $value->id ? 'selected="selected"' : '' }}>{{ $value->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="verificator" class="col-sm-2 control-label">Industry</label>
                <div class="col-sm-10">
                    <select class="form-control" name="industry">
                        @foreach ($industry as $key => $index)
                            <option value="{{ $index->id }}">{{ $index->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="expired_date" class="col-sm-2 control-label">Expired Date</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Expired Date (YYYY-MM-DD)" id="expired_date" name="expired_date"  value="{{ old('expired_date') }}">
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="logo" class="control-label col-sm-2">Logo</label>
                <div class="col-sm-10">
                    <div id="logo"
                          action="{{ url('admin/media') }}"
                          method="POST"
                          class="dropzone"
                          uploadMultiple="no">
                        {{ csrf_field() }}
                    </div>
                </div>
            </div>

            <div class="form-group bg-default">
            <label for="inputIndexed" class="col-sm-2 control-label">Live Now</label>
            <div class="col-sm-10">
                <div class="checkbox">
                        <input type="checkbox" name="live">
                        Yes
                </div>
            </div>
            </div>

            <div id="logo-wrapper"></div>
           
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <!-- <button type="submit" class="btn btn-info" id="btn-duplicate" name="duplicate" value="1">Save &amp; Duplicate</button> -->
                </div>
            </div>
        </div>
    </form>
</div><!-- /.box -->
@endsection



@section('script')

{{ HTML::script('assets/vendor/jquery-chosen/chosen.jquery.js') }}
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/dropzone.js"></script>
<script src="/assets/vendor/summernote/summernote.min.js"></script>

<script>
    var config = {
        '.chosen-select'           : {width: '100%'}
    };

    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>


<script src="https://maps.googleapis.com/maps/api/js?libraries=places&language=id&key={{ENV('MIX_GOOGLE_MAPS_JAVASCRIPT_API_KEY')}}"></script>
<script src="{{ mix_url('dist/vendor/leaflet/leaflet.js') }}"></script>

<script>
    var geocoder = new google.maps.Geocoder();

    function addMarkerListener(map, marker) {
        marker.on('dragend', function(evt) {
            var latlng = evt.target.getLatLng();

            var lat = latlng.lat;
            var lng = latlng.lng;

            geocoder.geocode({'latLng': latlng}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[1]) {
                        map.setView(latlng);

                        $('#inputGeoName').val(results[1].formatted_address);
                        $('#inputLatitude').val(lat);
                        $('#inputLongitude').val(lng);

                        for(idx in results[1].address_components) {

                            if(results[1].address_components[idx].types[0] == 'administrative_area_level_2') {
                                $('#inputCity').val(results[1].address_components[idx].short_name);
                            }

                            if(results[1].address_components[idx].types[0] == 'administrative_area_level_3') {
                                $('#inputSubdistrict').val(results[1].address_components[idx].short_name);
                            }                            
                        }
                    } else {
                        alert('No results found');
                    }
                } else {
                    alert('Geocoder failed due to: ' + status);
                }
            });
        });
    }

    function addPlaceChangedListener(autocomplete, map, marker) {
        autocomplete.addListener('place_changed', function() {
            marker.setOpacity(0);
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                // User entered the name of a Place that was not suggested and
                // pressed the Enter key, or the Place Details request failed.
                window.alert("No details available for input: '" + place.name + "'");
                return;
            }

            latlng = {
                lat: place.geometry.location.lat(),
                lng: place.geometry.location.lng()
            };

            map.setView(latlng);
            marker.setLatLng(latlng);
            marker.setOpacity(1);

            $('#inputLatitude').val(place.geometry.location.lat);
            $('#inputLongitude').val(place.geometry.location.lng);

            for(idx in place.address_components) {
                if(place.address_components[idx].types[0] == 'administrative_area_level_2') {
                    $('#inputCity').val(place.address_components[idx].short_name);
                }

                if(place.address_components[idx].types[0] == 'administrative_area_level_3') {
                    $('#inputSubdistrict').val(place.address_components[idx].short_name);
                }                            
            }
        });
    }

    var centerPos = {lat: -7.7858485, lng: 110.3680087};
        
    var map, marker, autocomplete;

    map = L.map('map-canvas', {
        // Set latitude and longitude of the map center (required)
        center: centerPos, 
        // Set the initial zoom level, values 0-18, where 0 is most zoomed-out (required)
        zoom: 12
    });

    var tiles = new L.tileLayer("{{ config('services.osm.host') }}/hot/{z}/{x}/{y}.png").addTo(map);

    marker = new L.Marker(centerPos, {
        draggable: true
    }).addTo(map);

    addMarkerListener(map, marker);

    autocomplete = new google.maps.places.Autocomplete(document.getElementById('inputGeoName'));
    autocomplete.setFields(['address_components', 'geometry']);

    addPlaceChangedListener(autocomplete, map, marker);
</script>

<script type="text/javascript">
    // var geocoder = new google.maps.Geocoder();

    // function addMarkerListener(map, marker) {
    //     google.maps.event.addListener(marker, 'dragend', function(evt) {
    //         var lat = evt.latLng.lat();
    //         var lng = evt.latLng.lng();

    //         var latlng = new google.maps.LatLng(lat, lng);

    //         geocoder.geocode({'latLng': latlng}, function(results, status) {
    //             if (status == google.maps.GeocoderStatus.OK) {
    //                 if (results[1]) {
    //                     map.setCenter(latlng);
    //                     marker.setPosition(latlng);

    //                     $('#inputGeoName').val(results[1].formatted_address);
    //                     $('#inputLatitude').val(lat);
    //                     $('#inputLongitude').val(lng);

    //                     for(idx in results[1].address_components) {

    //                         if(results[1].address_components[idx].types[0] == 'administrative_area_level_2') {
    //                             $('#inputCity').val(results[1].address_components[idx].short_name)
    //                         }

    //                         if(results[1].address_components[idx].types[0] == 'administrative_area_level_3') {
    //                             $('#inputSubdistrict').val(results[1].address_components[idx].short_name)
    //                         }                            
    //                     }
    //                 } else {
    //                     alert('No results found');
    //                 }
    //             } else {
    //                 alert('Geocoder failed due to: ' + status);
    //             }
    //         });
    //     });
    // }

    // function addPlaceChangedListener(autocomplete, map, marker) {
    //     autocomplete.addListener('place_changed', function() {
    //         marker.setVisible(false);
    //         var place = autocomplete.getPlace();
    //         if (!place.geometry) {
    //             // User entered the name of a Place that was not suggested and
    //             // pressed the Enter key, or the Place Details request failed.
    //             window.alert("No details available for input: '" + place.name + "'");
    //             return;
    //         }

    //         map.setCenter(place.geometry.location);
    //         marker.setPosition(place.geometry.location);
    //         marker.setVisible(true);

    //         $('#inputLatitude').val(place.geometry.location.lat);
    //         $('#inputLongitude').val(place.geometry.location.lng);

    //         for(idx in place.address_components) {
    //             if(place.address_components[idx].types[0] == 'administrative_area_level_2') {
    //                 $('#inputCity').val(place.address_components[idx].short_name)
    //             }

    //             if(place.address_components[idx].types[0] == 'administrative_area_level_3') {
    //                 $('#inputSubdistrict').val(place.address_components[idx].short_name)
    //             }                            
    //         }
    //     });
    // }

    $(function () {
        // var centerPos = {lat: -7.7858485, lng: 110.3680087};
        
        // var map, marker, autocomplete;

        // map = new google.maps.Map(document.getElementById("map-canvas"),
        //     {
        //         center: centerPos,
        //         zoom:12
        //     }
        // );

        // marker = new google.maps.Marker({
        //     position: centerPos,
        //     map: map,
        //     draggable: true,
        // });

        // addMarkerListener(map, marker);

        // autocomplete = new google.maps.places.Autocomplete(document.getElementById('inputGeoName'));
        // autocomplete.bindTo('bounds', map);

        // addPlaceChangedListener(autocomplete, map, marker);

        $('#inputDescription').summernote({
            height: 300,
            toolbar: [
                ['style', ['style', 'bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['insert', ['link', 'table', 'hr']],
                ['misc', ['codeview']]
            ]
        });

        // nicheType = ['freelance', 'freelancer', 'part-time', 'internship'];
        // generalType = ['full-time'];
        // currentType = nicheType.indexOf($('#inputType').val()) >= 0 ? 'niche' : 'general';

        Dropzone.autoDiscover = false;
        Dropzone.options.logo = {
            paramName : "media",
            maxFilesize : 6,
            maxFiles: 1,
            accepFiles: '.jpg, .jpeg, .png, .bmp',
            dictDefaultMessage : 'Upload Company Logo',
            addRemoveLinks: true,
            success : function (file, response) {
                if($('#logo-wrapper input[name=photo_id]').size() > 0) {
                    $('.logo-preview').remove();
                    $('#logo-wrapper input[name=photo_id]').val(response.media.id);
                } else {
                    $('#logo-wrapper').append('<input type="hidden" name="photo_id" value="' +response.media.id+ '">');
                }
            },
            init: function() {
                this.on("removedfile", function(file) {
                    console.log('remove');
                    $('#logo-wrapper input[name=photo_id]').remove();
                });

                this.on('sending', function(file, xhr, data) {
                    data.append('media_type', 'company_photo');
                    data.append('watermarking', false);
                })
            }
        };

        $('#logo').dropzone();

    });
</script>
@endsection
