@extends('admin.layouts.main')

@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <a href="{{ URL::route('admin.vacancy-speciality-types.create') }}">
                        <button type="submit" class="btn-add btn btn-primary btn-sm">
                            <i class="fa fa-plus">&nbsp;</i>Tambah</button>
                    </a>
                    <form action="" class="form-inline" method="get" style="text-align:right;padding:10px;">
                        <input type="text" name="q" class="form-control input-sm"  placeholder="Name"  autocomplete="off" value="{{ request()->input('q') }}">
                        <button class="btn btn-primary btn-md" id="buttonSearch"><i class="fa fa-search">&nbsp;</i>Search</button>
                    </form>
                </div>
            </div>

            <table  class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                 @foreach($vacancyTypes as $vacancyType)
                     <tr>
                         <td>{{ $vacancyType->id }}</td>
                         <td>{{ $vacancyType->name }}</td>
                         <td>
                            <a href="{{route('admin.vacancy-speciality-types.edit', $vacancyType->id)}}" title="edit">
                                <i class="fa fa-pencil"></i>
                            </a>
                         </td>
                     </tr> 
                 @endforeach
                </tbody>
                
            </table>
        </div><!-- /.box-body -->

        <div class="box-body no-padding">
            {{ $vacancyTypes->appends(Request::except('page'))->links() }}
        </div>
    </div><!-- /.box -->
    <!-- table -->
@endsection