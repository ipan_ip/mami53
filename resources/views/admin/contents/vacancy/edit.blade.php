@extends('admin.layouts.main')

@section('style')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/dropzone.css">
    {{ HTML::style('assets/vendor/jquery-chosen/chosen.css') }}
    {{ HTML::style('assets/vendor/jquery-chosen-custom/chosen.css') }}
    <link rel="stylesheet" href="/assets/vendor/summernote/summernote.css?v=05042018">
    <link rel="stylesheet" href="{{ mix_url('dist/vendor/leaflet/leaflet.css') }}">

    <style>
        #chosenForm .chosen-choices {
            border: 1px solid #ccc;
            border-radius: 4px;
            min-height: 34px;
            padding: 6px 12px;
        }
        #chosenForm .form-control-feedback {
            /* To make the feedback icon visible */
            z-index: 100;
        }

        #map-canvas-1,
        #map-canvas-2 {
            height: 300px;
            width: 100%;
        }
    </style>
@endsection

@section('content')
<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">{{ $boxTitle }}</h3>
    </div><!-- /.box-header -->
    <form action="{{ route('admin.jobs.update', $data->id) }}" method="post" class="form-horizontal form-bordered">
        <input type="hidden" name="_method" value="PUT">

        <div class="box-body no-padding">
            <div class="form-group bg-default">
                <label for="inputUrlSlug" class="col-sm-2 control-label">Company</label>
                <div class="col-sm-10">
                    <div class="col-sm-11">
                        <input type="text" name="company_profile" id="inputName" value="{{ $data->company_profile_id }}" class="form-control">
                    </div>
                    <div class="col-sm-1">
                        <input type="button" name="search" class="btn btn-success" value="Search" id="inputNameCheck">
                    </div>
                </div>


                 <div style="margin-left: 10px; margin-top: 30px; width: 100%;">
                    <div id="hasil"></div>
                </div>  

            </div>

            <div class="form-group bg-default">
                <label for="inputUrlSlug" class="col-sm-2 control-label">Place Name</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Place Name" id="inputSlugUrl" name="place_name"  value="{{ $data->place_name }}">
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputUrlSlug" class="col-sm-2 control-label">Slug</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Place Name" id="inputSlugUrl" name="slug"  value="{{ $data->slug }}">
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputUrlSlug" class="col-sm-2 control-label">Address</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Place Name" id="inputSlugUrl" name="address"  value="{{ $data->address }}">
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputAddress" class="col-sm-2 control-label">Penempatan Kerja</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Alamat" id="inputAddress" name="workplace" value="{{ $data->workplace }}">
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputIndexed" class="col-sm-2 control-label">Penempatan Kerja</label>
                <div class="col-sm-10">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" @if ($data->workplace_status == 1) checked="checked" @endif name="workplace_status" value="{{ $data->workplace_status }}">
                            Kandidate harus bersedia di tempatkan disini.
                        </label>
                    </div>
                </div>
            </div>

              <div class="form-group bg-default">
              <label for="inputName" class="col-sm-2 control-label">Indexed</label>
              <div class="col-sm-10">
                <div class="checkbox">
                  <label>
                    <input type="checkbox" @if($data->is_indexed == 1) checked="checked" @endif name="indexed">
                    Indexed
                  </label>
                </div>
              </div>
              </div>

            <div class="form-group bg-default">
                <label for="inputGeoName" class="col-sm-2 control-label">Koordinat</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Geo Name"
                           id="inputGeoName" name="geo_name">
                </div>
                <div class="col-sm-10 col-sm-offset-2">
                    <div style="background: #EEE; width: 100%; height: 300px;">
                        <div id="map-canvas"></div>
                    </div>
                </div>
                <div class="col-sm-10 col-sm-offset-2">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="latitude"
                           id="latitude" name="latitude" value="{{ $data->latitude }}">
                        <input type="text" class="form-control" placeholder="longitude"
                           id="longitude" name="longitude" value="{{ $data->longitude }}">
                   </div>
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputUrlSlug" class="col-sm-2 control-label">City</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="" id="area_city" name="city"  value="{{ $data->city }}">
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputUrlSlug" class="col-sm-2 control-label">Subdistrict</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Place Name" id="subdistrict" name="subdistrict"  value="{{ $data->subdistrict }}">
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputUrlSlug" class="col-sm-2 control-label">Jobs Name</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Jobs Name" id="inputSlugUrl" name="jobs_name"  value="{{ $data->name }}">
                </div>
            </div>  

            <div class="form-group bg-default">
                <label for="inputDescription" class="col-sm-2 control-label">Deskripsi</label>
                <div class="col-sm-10">
                    <textarea name="description" id="inputDescription" class="form-control">{{ $data->description }}</textarea>
                </div>
            </div> 

            <div class="form-group bg-default">
                <label for="inputUrlSlug" class="col-sm-2 control-label">Salary</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" placeholder="Salary" id="inputSlugUrl" name="salary"  value="{{ $data->salary }}">
                </div>
                <div class="col-sm-5">
                    <input type="text" class="form-control" placeholder="Max Gaji" id="inputSalary" name="max_salary" value="{{ $data->max_salary }}">
                </div>
            </div>  

            <div class="form-group bg-default">
                <label for="inputUrlSlug" class="col-sm-2 control-label">Salary Time</label>
                <div class="col-sm-10">
                  <select class="form-control" name="salary_time">   
                    @foreach ($times AS $key => $value)
                      @if ($value == $data['salary_time'])
                        <option value="{{ $value }}" selected="true">{{ $value }}</option>
                      @else
                        <option value="{{ $value }}">{{ $value }}</option> 
                      @endif
                    @endforeach
                  </select>  
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputUrlSlug" class="col-sm-2 control-label">Type</label>
                <div class="col-sm-10">
                  <select class="form-control" name="type" id="inputType">   
                    @foreach ($type AS $key => $value)
                      @if ($value == $data['type'])
                        <option value="{{ $value }}" selected="true">{{ $value }}</option>
                      @else
                        <option value="{{ $value }}">{{ $value }}</option> 
                      @endif
                    @endforeach
                  </select>  
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputLastEducation" class="col-sm-2 control-label">Pendidikan Terakhir</label>
                <div class="col-sm-10">
                  <select class="form-control" name="last_education" id="inputLastEducation">   
                    @foreach ($educationOptions AS $key => $education)
                        <option value="{{ $key }}" {{ old('last_education', $data->last_education) == $key ? 'selected="selected"' : '' }}>{{ $education }}</option>
                    @endforeach
                  </select>  
                </div>
            </div>

            

            <div class="form-group bg-info divider">
              <div class="col-sm-12 pull-left">
                User
              </div>
            </div> 

            <div class="form-group bg-default">
                <label for="inputUrlSlug" class="col-sm-2 control-label">User Name</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="User Name" id="inputSlugUrl" name="user_name"  value="{{ $data->user_name }}">
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputUrlSlug" class="col-sm-2 control-label">Phone</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Place Name" id="inputSlugUrl" name="user_phone"  value="{{ $data->user_phone }}">
                </div>
            </div>  

            <div class="form-group bg-default">
                <label for="inputUrlSlug" class="col-sm-2 control-label">Email</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Place Name" id="inputSlugUrl" name="user_email"  value="{{ $data->user_email }}">
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="inputDetailUrl" class="col-sm-2 control-label">URL Sumber Data</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="URL Sumber Data" id="inputDetailUrl" name="detail_url" value="{{ old('detail_url', $data->detail_url) }}">
                </div>
            </div>

            <div class="form-group bg-info divider">
              <div class="col-sm-12 pull-left">
                Agent
              </div>
            </div> 

            <div class="form-group bg-default">
                <label for="agen" class="col-sm-2 control-label">Agent Name</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Agent Name" id="agen" name="agent_name"  value="{{ $data->agent_name }}">
                </div>
            </div> 

            <div class="form-group bg-default">
                <label for="verificator" class="col-sm-2 control-label">Verificator</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Verificator Name" id="verificator" name="verificator"  value="{{ $data->verificator }}">
                </div>
            </div>   

            <div class="form-group bg-info divider">
                <div class="col-sm-12 pull-left">
                    Additional
                </div>
            </div> 

            <!--
            <div class="form-group bg-default">
                <label for="inputGroup" class="col-sm-2 control-label">Group</label>
                <div class="col-sm-10">
                    <select class="form-control" name="group" id="inputGroup">
                        <option value="niche">Niche</option>
                    </select>
                </div>
            </div>
            -->

            <div class="form-group bg-default">
                <label for="verificator" class="col-sm-2 control-label">Spesialisasi</label>
                <div class="col-sm-10">
                    <select class="form-control" name="spesialisasi" id="inputSpesialisasi">
                        <option value="" {{ is_null(old('spesialisasi', $data->spesialisasi_id)) ? 'selected="selected"' : '' }}>-- Pilih Spesialisasi --</option>
                        @foreach ($spesialisasi as $groupName => $spesial)
                            <optgroup label="{{ $groupName }}" class="opt-{{ $groupName }}">
                                @foreach ($spesial AS $key => $index)
                                    <option value="{{ $index->id }}" {{ old('spesialisasi', $data->spesialisasi_id) == $index->id ? 'selected="selected"' : '' }}>{{ $index->name }}</option>
                                @endforeach
                            </optgroup>
                        @endforeach
                    </select>

                    
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="verificator" class="col-sm-2 control-label">Industry</label>
                <div class="col-sm-10">
                    <select class="form-control" name="industry">
                        @foreach ($industry AS $key => $index)
                            @if ($index->id == $data->industry_id)
                                <option value="{{ $index->id }}" selected="true">{{ $index->name }}</option>
                            @else
                                <option value="{{ $index->id }}">{{ $index->name }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="expired_date" class="col-sm-2 control-label">Expired Date</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Expired Date (YYYY-MM-DD)" id="expired_date" name="expired_date"  value="{{ old('expired_date', $data->expired_date) }}">
                </div>
            </div>

            <div class="form-group bg-default">
                <label for="logo" class="control-label col-sm-2">Logo</label>
                <div class="col-sm-10">
                    <div id="logo"
                          action="{{ url('admin/media') }}"
                          method="POST"
                          class="dropzone"
                          uploadMultiple="no">
                        {{ csrf_field() }}
                    </div>
                </div>
            </div>

            <div id="logo-wrapper">
                @if(!is_null($data->photo_id))
                    <input type="hidden" name="photo_id" value="{{ $data->photo_id }}">
                @endif
            </div>

            <div class="form-group bg-default">
                <div class="col-sm-10 col-sm-push-2">
                    @if(!is_null($data->photo))
                        <img src="{{ $data->photo->getMediaUrl()['small'] }}" class="logo-preview" alt="logo-preview">
                    @endif
                </div>
            </div>

            <div class="form-group bg-default">
              <label for="inputName" class="col-sm-2 control-label">Live</label>
              <div class="col-sm-10">
                <div class="checkbox">
                  <label>
                    <input type="checkbox" @if($data->is_active == 1 AND $data->status == 'verified') checked="checked" @endif name="live">
                    Live
                  </label>
                </div>
              </div>
            </div>
           
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <!-- <button type="submit" class="btn btn-info" id="btn-duplicate" name="duplicate" value="1">Save &amp; Duplicate</button> -->
                </div>
            </div>
        </div>
    </form>
</div><!-- /.box -->
@endsection



@section('script')

{{ HTML::script('assets/vendor/jquery-chosen/chosen.jquery.js') }}
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/dropzone.js"></script>
<script src="/assets/vendor/summernote/summernote.min.js"></script>

<script>
    var config = {
        '.chosen-select'           : {width: '100%'}
    };

    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>


<script src="https://maps.googleapis.com/maps/api/js?libraries=places&language=id&key={{ENV('MIX_GOOGLE_MAPS_JAVASCRIPT_API_KEY')}}"></script>
<script src="{{ mix_url('dist/vendor/leaflet/leaflet.js') }}"></script>

<script>
    <?php
        if (is_null($data->latitude) OR is_null($data->longitude)) {
    ?>
        centerPos = {lat: -7.7858485, lng: 110.3680087};
    <?php          
        } else {
    ?>
        centerPos = {lat: <?php echo $data->latitude; ?>, lng: <?php echo $data->longitude; ?>};
    <?php 
        }
    ?>

    var map = L.map('map-canvas', {
        // Set latitude and longitude of the map center (required)
        center: centerPos, 
        // Set the initial zoom level, values 0-18, where 0 is most zoomed-out (required)
        zoom: 12
    });

    var tiles = new L.tileLayer("{{ config('services.osm.host') }}/hot/{z}/{x}/{y}.png").addTo(map);

    var marker = new L.Marker(centerPos, {
        draggable: true
    }).addTo(map);

    var autocomplete = new google.maps.places.Autocomplete(document.getElementById('inputGeoName'));
    autocomplete.setFields(['address_components', 'geometry']);

    var geocoder = new google.maps.Geocoder();

    marker.on('dragend', function(evt) {
        var latlng = evt.target.getLatLng();

        var lat = latlng.lat;
        var lng = latlng.lng;

        geocoder.geocode({'latLng': latlng}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[1]) {

                    map.setView(latlng);

                    $('#inputGeoName').val(results[1].formatted_address);
                    $('#latitude').val(lat);
                    $('#longitude').val(lng);

                    setDistrictAndCity(results[0]);
                } else {
                    alert('No results found');
                }
            } else {
                alert('Geocoder failed due to: ' + status);
            }
        });
    });


    autocomplete.addListener('place_changed', function() {
        marker.setOpacity(0);
        var place = autocomplete.getPlace();
        if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place.name + "'");
            return;
        }

        latlng = {
            lat: place.geometry.location.lat(),
            lng: place.geometry.location.lng()
        };

        map.setView(latlng);
        marker.setLatLng(latlng);
        marker.setOpacity(1);

        $('#latitude').val(place.geometry.location.lat);
        $('#longitude').val(place.geometry.location.lng);

        setDistrictAndCity(place);
    });

    function setDistrictAndCity(response) {
        subDistrict = '';
        city = '';
        response.address_components.forEach(function(component) {
            // console.log(component.types);
            if ($.inArray("administrative_area_level_3", component.types) != -1) {
                subDistrict = component.short_name;
            }

            if ($.inArray("administrative_area_level_2", component.types) != -1) {
                city = component.short_name;      
            }

        });

        $('#subdistrict').val(subDistrict);
        $('#area_city').val(city);
    }

    var geocoder;
    geocoder = new google.maps.Geocoder();
    
    <?php
        if (is_null($data->latitude) OR is_null($data->longitude)) {
    ?>
        var latlng = new google.maps.LatLng(-7.7858485, 110.3680087);
    <?php          
        } else {
    ?>
        var latlng = new google.maps.LatLng(<?php echo $data->latitude; ?>, <?php echo $data->longitude; ?>);
    <?php 
        }
    ?>

</script>

<script type="text/javascript">

    <?php
        if (is_null($data->latitude) OR is_null($data->longitude)) {
    ?>
        //centerPos = {lat: -7.7858485, lng: 110.3680087};
    <?php          
        } else {
    ?>
        //centerPos = {lat: <?php echo $data->latitude; ?>, lng: <?php echo $data->longitude; ?>};
    <?php 
        }
    ?>
        // var map = new google.maps.Map(document.getElementById("map-canvas"),
        //     {
        //         center: centerPos,
        //         zoom:12
        //     }
        // );

        // var marker = new google.maps.Marker({
        //     position: centerPos,
        //     map: map,
        //     draggable: true,

        // });

        // var autocomplete = new google.maps.places.Autocomplete(document.getElementById('inputGeoName'));
        // autocomplete.bindTo('bounds', map);

        // var geocoder = new google.maps.Geocoder();

        // google.maps.event.addListener(marker, 'dragend', function(evt) {
        //     var lat = evt.latLng.lat();
        //     var lng = evt.latLng.lng();

        //     var latlng = new google.maps.LatLng(lat, lng);

        //     geocoder.geocode({'latLng': latlng}, function(results, status) {
        //         if (status == google.maps.GeocoderStatus.OK) {
        //             if (results[1]) {

        //                 map.setCenter(latlng);
        //                 marker.setPosition(latlng);

        //                 $('#inputGeoName').val(results[1].formatted_address);
        //                 $('#latitude').val(lat);
        //                 $('#longitude').val(lng);

        //                 setDistrictAndCity(results[0]);
        //             } else {
        //                 alert('No results found');
        //             }
        //         } else {
        //             alert('Geocoder failed due to: ' + status);
        //         }
        //     });
        // });


        // autocomplete.addListener('place_changed', function() {
        //     marker.setVisible(false);
        //     var place = autocomplete.getPlace();
        //     if (!place.geometry) {
        //         // User entered the name of a Place that was not suggested and
        //         // pressed the Enter key, or the Place Details request failed.
        //         window.alert("No details available for input: '" + place.name + "'");
        //         return;
        //     }

        //     map.setCenter(place.geometry.location);
        //     marker.setPosition(place.geometry.location);
        //     marker.setVisible(true);

        //     $('#latitude').val(place.geometry.location.lat);
        //     $('#longitude').val(place.geometry.location.lng);

        //     setDistrictAndCity(place);
        // });

        // function setDistrictAndCity(response) {
        //     subDistrict = '';
        //     city = '';
        //     response.address_components.forEach(function(component) {
        //         // console.log(component.types);
        //         if ($.inArray("administrative_area_level_3", component.types) != -1) {
        //             subDistrict = component.short_name;
        //         }

        //         if ($.inArray("administrative_area_level_2", component.types) != -1) {
        //             city = component.short_name;      
        //         }

        //     });

        //     $('#subdistrict').val(subDistrict);
        //     $('#area_city').val(city);
        // }

        //     var geocoder;
        //     geocoder = new google.maps.Geocoder();
    
    <?php
        if (is_null($data->latitude) OR is_null($data->longitude)) {
    ?>
        // var latlng = new google.maps.LatLng(-7.7858485, 110.3680087);
    <?php          
        } else {
    ?>
        // var latlng = new google.maps.LatLng(<?php echo $data->latitude; ?>, <?php echo $data->longitude; ?>);
    <?php 
        }
    ?>

  

$('#inputDescription').summernote({
    height: 300,
    toolbar: [
        ['style', ['style', 'bold', 'italic', 'underline', 'clear']],
        ['font', ['strikethrough', 'superscript', 'subscript']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['insert', ['link', 'table', 'hr']],
        ['misc', ['codeview']]
    ]
});

    $(function() {
        // nicheType = ['freelancer', 'freelance', 'part-time', 'internship'];
        // generalType = ['full-time'];
        // currentType = nicheType.indexOf($('#inputType').val()) >= 0 ? 'niche' : 'general';
        Dropzone.autoDiscover = false;
        Dropzone.options.logo = {
            paramName : "media",
            maxFilesize : 6,
            maxFiles: 1,
            accepFiles: '.jpg, .jpeg, .png, .bmp',
            dictDefaultMessage : 'Upload Company Logo',
            addRemoveLinks: true,
            success : function (file, response) {
                if($('#logo-wrapper input[name=photo_id]').size() > 0) {
                    $('.logo-preview').remove();
                    $('#logo-wrapper input[name=photo_id]').val(response.media.id);
                } else {
                    $('#logo-wrapper').append('<input type="hidden" name="photo_id" value="' +response.media.id+ '">');
                }
            },
            init: function() {
                this.on("removedfile", function(file) {
                    console.log('remove');
                    $('#logo-wrapper input[name=photo_id]').remove();
                });

                this.on('sending', function(file, xhr, data) {
                    data.append('media_type', 'company_photo');
                    data.append('watermarking', false);
                })
            }
        };

        $('#logo').dropzone();

        currentType = $('#inputGroup').val();
        
        if(currentType == 'niche') {
            $('#inputSpesialisasi optgroup.opt-niche').show();
            $('#inputSpesialisasi optgroup.opt-general').hide();
            // $('#btn-duplicate').show();
        } else {
            $('#inputSpesialisasi optgroup.opt-niche').hide();
            $('#inputSpesialisasi optgroup.opt-general').show();
            // $('#btn-duplicate').hide();
        }

        // $('#inputType').on('change', function(e) {
        $('#inputGroup').on('change', function(e) {
            // currentTypeTemp = nicheType.indexOf($(this).val()) >= 0 ? 'niche' : 'general';

            // if(currentType == currentTypeTemp) {
            //     return;
            // }

            // currentType = currentTypeTemp;

            currentType = $('#inputGroup').val();


            if(currentType == 'niche') {
                $('#inputSpesialisasi optgroup.opt-niche').show();
                $('#inputSpesialisasi optgroup.opt-general').hide();
                // $('#btn-duplicate').show();
            } else {
                $('#inputSpesialisasi optgroup.opt-niche').hide();
                $('#inputSpesialisasi optgroup.opt-general').show();
                // $('#btn-duplicate').hide();
            }

            $('#inputSpesialisasi').val('');
        });

    });



    $('#inputNameCheck').click(function(e) {
      theName = $("#inputName").val();
      $("#hasil").append("Loading ................ ");
      $.ajax({
        url: '/admin/jobs/i/company?name=' + theName,
        type: 'GET',
        dataType: 'json',
        success: function(data) {
          $("#hasil").empty();  
          if(data.status == false) {
            
            $("#hasil").append("Tambah profil perusahaan <a href='/admin/jobs/company/new' target='_blank'>Tambah</a>");
          } else {
            var project = "<strong>Hasil pencarian</strong>";
            project += "<ul>";
            $.each(data.data, function(index, value) {

                if (value.is_active == 1) {
                    project += "<li>"+value.id+" - "+value.name+". <a href='/admin/jobs/company/edit/"+value.id+"' target='_blank'>view</a> - <label style='cursor: pointer; text-decoration: underline;' id='choosein' str-id="+value.id+">pilih ini</label></li>";
                } else {
                    project += "<li style='color: #ff0000;'>"+value.id+" - "+value.name+". <a href='/admin/jobs/company/edit/"+value.id+"' target='_blank'>view</a> - <label style='cursor: pointer; text-decoration: underline;' id='choosein' str-id="+value.id+">pilih ini</label></li>";
                }

            });
            project += "Tidak ada dalam pilihan ? Tambah project baru <strong><a href='/admin/jobs/company/new' target='_blank'>Tambah</a></strong><br/>";
            project += "</ul>";

            $("#hasil").append(project);
          }
        }
      });
    });

    $(document).on('click', '#choosein', function(e) {
      var id = $(this).attr("str-id");
      document.getElementById('inputName').value = id;
    });
</script>
@endsection
