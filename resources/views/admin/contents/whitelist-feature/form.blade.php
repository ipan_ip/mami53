@extends('admin.layouts.main')
@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>
        <div class="box-body">
            <div>Make sure you write same <strong>Feature Name</strong> as implemented on the code</div>
            <div>You can search for owner's user id on “Kost Owner” admin menu (column “Owner ID”). For other user, you can find from redash or database.</div>
        </div>

        {{ Form::open(array('route' => $formAction, 'method' => $formMethod, 'class' => 'form-horizontal form-bordered', 'role' => 'form', 'id' => 'formInsertFeature', 'enctype' => 'multipart/form-data')) }}
            <div class="box-body no-padding">
                <div class="form-group bg-default">
                    <label for="inputName" class="col-sm-2 control-label">Feature Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" placeholder="feature_name" id="inputName" name="name" value="{{ $whitelistFeature->name }}">
                    </div>
                </div>
                <div class="form-group bg-default">
                    <label for="inputName" class="col-sm-2 control-label">User ID</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" placeholder="user_id" id="inputUserId" name="user_id" value="{{ $whitelistFeature->user_id }}">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <a href="{{ URL::route('admin.whitelist-features.index') }}">
                            <button type="button" class="btn btn-default">Cancel</button>
                        </a>
                        <button type="reset" class="btn btn-default" onclick="$('#formInsertFeature').bootstrapValidator('resetForm', true);">Reset</button>
                    </div>
                </div>
        </div>
        {{ Form::close() }}
    </div>
@stop
