@extends('admin.layouts.main')
@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
            <div class="box-tools pull-right">
                <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <a href="{{ URL::route('admin.whitelist-features.create') }}">
                        <button type="submit" class="btn-add btn btn-primary btn-sm">
                            <i class="fa fa-plus"></i> Create Whitelist Feature
                        </button>
                    </a>
                </div>
            </div>

            <table id="tableList" class="table table-striped">
                <thead>
                    <tr>
                        <th width="30px;">No</th>
                        <th>Feature Name</th>
                        <th>User ID</th>
                        <th class="table-action-column">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($whitelistFeatures as $key => $whitelistFeature)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ $whitelistFeature->name }}</td>
                            <td>{{ $whitelistFeature->user_id }}</td>
                            <td>
                                <a href="{{ URL::route('admin.whitelist-features.edit', $whitelistFeature->id) }}" class="btn btn-xs btn-warning">Edit</a>
                                <form action="{{ url('/admin/whitelist-features/'.$whitelistFeature->id) }}" method="POST">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    <button class="btn btn-xs btn-danger" type="submit">Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>No</th>
                        <th>Feature Name</th>
                        <th>User ID</th>
                        <th class="table-action-column">Action</th>
                    </tr>
                </tfoot>
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
    <!-- table -->

    @include('admin.partials.delete-modal')
@stop
@section('script')
<!-- page script -->
<script type="text/javascript">
    $(function()
    {
        $("#tableList").dataTable();
    });
</script>
@stop
