@extends('admin.layouts.main')

@section('content')


<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">{{ $boxTitle }}</h3>
    </div><!-- /.box-header -->
    <form class="form-horizontal form-bordered" id="formInsertDesigner" action="{{ $action }}" method="POST">
    @if (isset($method))
        <input type="hidden" name="_method" value="{{ $method }}" />
    @endif
    {{ csrf_field() }}
    <div class="box-body no-padding">
        <div class="form-group bg-default">
            <label for="inputUrlSlug" class="col-sm-2 control-label">URL Slug</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" placeholder="URL Slug" id="inputSlugUrl" name="slug"  value="{{ $rowLanding->slug }}">
            </div>
        </div>
        <div class="form-group bg-default">
            <label for="inputType" class="col-sm-2 control-label">Property Type</label>
            <div class="col-sm-10">
                <select class="form-control chosen-select" id="inputPropertyType" name="property_type" tabindex="2">
                    <option value="rented_house"  {{ $rowLanding->property_type=='rented_house'?'selected':'' }}>Kontrakan</option>
                    <option value="villa" {{ $rowLanding->property_type=='villa'?'selected':'' }}>Villa</option>
                </select>
            </div>
        </div>
        <div class="form-group bg-default">
            <label for="inputType" class="col-sm-2 control-label">Type</label>
            <div class="col-sm-10">
                <select class="form-control chosen-select" id="inputType" name="type" tabindex="2">
                    <option value="area"  {{ $rowLanding->type=='area'?'selected':'' }}>Area</option>
                    <option value="campus" {{ $rowLanding->type=='campus'?'selected':'' }}>Campus</option>
                </select>
            </div>
        </div>
        <div class="form-group bg-default">
            <label for="inputHeading1" class="col-sm-2 control-label">Heading 1</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" placeholder="Heading 1" id="inputHeading1" name="heading_1"  value="{{ $rowLanding->heading_1 }}">
            </div>
        </div>
        <div class="form-group bg-default">
            <label for="inputHeading1" class="col-sm-2 control-label">Heading 2</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" placeholder="Heading 2" id="inputHeading2" name="heading_2"  value="{{ $rowLanding->heading_2 }}">
            </div>
        </div>

        <div class="form-group bg-default">
            <label for="inputKeyword" class="col-sm-2 control-label">Keyword</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" placeholder="Keyword" id="inputKeyword" name="keyword"  value="{{ $rowLanding->keyword }}">
            </div>
        </div>

        <!-- <div class="form-group bg-default">
            <label for="inputCoordinate1" class="col-sm-2 control-label">Coordinate 1</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" placeholder="Coordinate 1" id="inputCoordinate1" name="coordinate_1"  value="{{ $rowLanding->coordinate_1 }}">
            </div>
        </div>

        <div class="form-group bg-default">
            <label for="inputCoordinate2" class="col-sm-2 control-label">Coordinate 2</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" placeholder="Coordinate 2" id="inputCoordinate2" name="coordinate_2"  value="{{ $rowLanding->coordinate_2 }}">
            </div>
        </div> -->
        <div class="form-group bg-default">
            <label for="inputDescription1" class="col-sm-2 control-label">Description</label>
            <div class="col-sm-10">
                <textarea rows="8" type="text" class="form-control" id="inputDescription" name="description"> {{ $rowLanding->description}}</textarea>
            </div>
            <div id="charCount"></div>
        </div>
        <div class="form-group bg-default">
            <label for="inputPriceMax" class="col-sm-2 control-label"></label>
            <div class="col-sm-10">
                <input type="text" class="form-control" placeholder="Search Location" id="inputGeoName" name="geo_name" />
                <div>
                    <div style="background: #EEE; width: 560px; height: 470px;">
                        <div id="map-canvas"></div>
                    </div>
                </div>
                <div class="col-sm-10">
                    <div class="input-group">
                        <div class="input-group-addon">Kiri Bawah (latitude, longitude)</div>
                        <input type="text" class="form-control" placeholder="Latitude, Longitude"
                            id="inputCoordinateBottomLeft" name="coordinate_1" value="{{ $rowLanding->coordinate_1 }}">
                    </div>
                </div>
                <div class="col-sm-10">
                    <div class="input-group">
                        <div class="input-group-addon">Kanan Atas (latitude, longitude)</div>
                        <input type="text" class="form-control" placeholder="Latitude, Longitude"
                            id="inputCoordinateTopRight" name="coordinate_2" value="{{ $rowLanding->coordinate_2 }}">
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group bg-default">
            <label for="inputPriceMin" class="col-sm-2 control-label">Price Min</label>
            <div class="col-sm-10">
                <input type="number" class="form-control" placeholder="Price Min" id="inputPriceMin" name="price_min"  value="{{ $rowLanding->price_min }}">
            </div>
        </div>

        <div class="form-group bg-default">
            <label for="inputPriceMax" class="col-sm-2 control-label">Price Max</label>
            <div class="col-sm-10">
                <input type="number" class="form-control" placeholder="Price Max" id="inputPriceMax" name="price_max"  value="{{ $rowLanding->price_max }}">
            </div>
        </div>

        <div class="form-group bg-default">
            <label for="inputTag" class="col-sm-2 control-label">Tags</label>
            <div class="col-sm-10">
                <select class="form-control chosen-select" multiple id="inputTag" name="tag_ids[]" tabindex="2">
                    @foreach ($rowsTag as $rowTag)
                        <option value="{{ $rowTag->id }}"
                            {{ $rowTag->selected }}>{{ $rowTag->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group bg-default">
            <label for="inputGender" class="col-sm-2 control-label">Rent Type</label>
            <div class="col-sm-10">
                {!! Form::select('rent_type', [ 
                        0 => 'Harian', 
                        2 => 'Bulanan', 
                        3 => 'Tahunan'
                        ],
                        Input::old('inputRentType', $rowLanding->rent_type), 
                        [
                            'class' => 'form-control chosen-select',
                            'id'    => 'inputRentType'
                        ]
                    ) 
                !!}
            </div>
        </div>

        <div class="form-group bg-default">
            <label for="inputTag" class="col-sm-2 control-label">Tagging Category</label>
            <div class="col-sm-10">
                <select class="form-control tagging" multiple="multiple" name="tagging_ids[]">
                    @foreach ($rowsTagging as $tagging)
                        <option value="{{ $tagging->name }}" {{ in_array($tagging->id, $rowsOldTagging) ? 'selected="selected"' : '' }}>{{ $tagging->name }}</option>
                    @endforeach
                </select>
            </div>

        </div>

        <div class="form-group bg-default">
            <label for="dummy-counter" class="col-sm-2 control-label">Dummy Counter</label>
            <div class="col-sm-10">
                <input type="number" class="form-control" placeholder="Dummy Counter" id="dummy-counter" name="dummy_counter" value="{{ $rowLanding->dummy_counter }}">
                <p class="help-block">
                    Dummy counter digunakan untuk meng-inject jumlah kost pada meta description landing
                </p>
            </div>
        </div>

    </div>
</div>
<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-primary">Save</button>
  </div>
</div>
</form>
</div><!-- /.box -->
@stop

@section('style')
    {{ HTML::style('assets/vendor/jquery-chosen/chosen.css') }}
    {{ HTML::style('assets/vendor/jquery-chosen-custom/chosen.css') }}
    <link rel="stylesheet" href="/assets/vendor/summernote/summernote.css?v=05042018">
    <link rel="stylesheet" href="{{ mix_url('dist/vendor/leaflet/leaflet.css') }}">

    <style>
        #chosenForm .chosen-choices {
            border: 1px solid #ccc;
            border-radius: 4px;
            min-height: 34px;
            padding: 6px 12px;
        }
        #chosenForm .form-control-feedback {
            /* To make the feedback icon visible */
            z-index: 100;
        }
    </style>
@stop

@section('script')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script>
        $(".tagging").select2({
            tags: true,
            tokenSeparators: [',', ' ']
        });
    </script>

    {{ HTML::script('assets/vendor/jquery-chosen/chosen.jquery.js') }}
    <script type="text/javascript">
        $(function() {
                var config = {
                    '.chosen-select'           : {width: '100%'}
                };
                for (var selector in config) {
                    $(selector).chosen(config[selector]);
                }
        });
    </script>

    <!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
    {{ HTML::script('assets/vendor/jquery-file-upload/js/vendor/jquery.ui.widget.js') }}
    {{ HTML::script('assets/vendor/jquery-chosen/chosen.jquery.js') }}

    <script>
    $(function () {
        // Change this to the location of your server-side upload handler
        @if (Request::is('admin/*'))
            var url = "{{ url('admin/media') }}";
        @else
            var url = "{{ url('user/media') }}";
        @endif

        // myFileUpload($('#cardUpload1'), url, 'style_photo');
        // myFileUpload($('#cardUpload2'), url, 'style_photo');
    });

    var config = {
        '.chosen-select'           : {width: '100%'}
    };

    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>


<script src="https://maps.googleapis.com/maps/api/js?libraries=places&language=id&key={{ENV('MIX_GOOGLE_MAPS_JAVASCRIPT_API_KEY')}}"></script>

<script src="/assets/vendor/summernote/summernote.min.js"></script>

<script src="{{ mix_url('dist/vendor/leaflet/leaflet.js') }}"></script>


<script type="text/javascript">
    // leaflet starts here

    function addMarkerListener(map, marker, pos) {
        marker.on('dragend', function(evt) {
            var latlng = evt.target.getLatLng();

            var lat = latlng.lat;
            var lng = latlng.lng;

            $('#inputCoordinate' + pos).val(lat + ',' + lng);
        });
    }

    function addPlaceChangedListener(autocomplete, map, marker) {
        autocomplete.addListener('place_changed', function() {
            marker[0].setOpacity(0);
            marker[1].setOpacity(0);

            var place = autocomplete.getPlace();
            if (!place.geometry) {
                // User entered the name of a Place that was not suggested and
                // pressed the Enter key, or the Place Details request failed.
                window.alert("No details available for input: '" + place.name + "'");
                return;
            }

            // map.setCenter(place.geometry.location);
            map.setView({
                lat: place.geometry.location.lat(),
                lng: place.geometry.location.lng()
            }, 12);

            bottomLeft = {
                lat: place.geometry.location.lat() - 0.05,
                lng: place.geometry.location.lng() - 0.05
            }

            topRight = {
                lat: place.geometry.location.lat() + 0.05,
                lng: place.geometry.location.lng() + 0.05
            }

            marker[0].setLatLng(bottomLeft);
            marker[1].setLatLng(topRight);
            marker[0].setOpacity(1);
            marker[1].setOpacity(1);

            $('#inputCoordinateBottomLeft').val(bottomLeft.lat + ',' + bottomLeft.lng);
            $('#inputCoordinateTopRight').val(topRight.lat + ',' + topRight.lng);
        });
    }

    @if(!is_null($rowLanding->coordinate_1) && !is_null($rowLanding->coordinate_2))
        centerPos = {
            lat: parseFloat('{{ ($rowLanding->latitude_1 + $rowLanding->latitude_2) / 2 }}'), 
            lng: parseFloat('{{ ($rowLanding->longitude_1 + $rowLanding->longitude_2) / 2 }}')
        }
    @else
        centerPos = {lat: -7.7858485, lng: 110.3680087}        
    @endif

    var map = L.map('map-canvas', {
        // Set latitude and longitude of the map center (required)
        center: centerPos, 
        // Set the initial zoom level, values 0-18, where 0 is most zoomed-out (required)
        zoom: 12
    });

    var tiles = new L.tileLayer("{{ config('services.osm.host') }}/hot/{z}/{x}/{y}.png").addTo(map);

    var marker = [];
    var markerImage = [];
    var position = [];

    // marker kiri bawah
    markerImage[0] = '{{ asset('assets/icons/kiribawah.png') }}';
    
    @if(!is_null($rowLanding->latitude_1))
        position[0] = {
            'lat': parseFloat('{{ $rowLanding->latitude_1 }}'),
            'lng': parseFloat('{{ $rowLanding->longitude_1 }}')
        }
    @else
        position[0] = {
            'lat': centerPos.lat - 0.05,
            'lng': centerPos.lng - 0.05
        }
    @endif

    marker[0] = new L.Marker(position[0], {
        draggable: true,
        icon: new L.icon({
            iconUrl: markerImage[0],
            iconSize: [60, 57],
            iconAnchor: [30, 57]
        })
    }).addTo(map);

    addMarkerListener(map, marker[0], 'BottomLeft');

    // marker kanan atas
    markerImage[1] = '{{ asset('assets/icons/kananatas.png') }}';
    
    @if(!is_null($rowLanding->latitude_2))
        position[1] = {
            'lat': parseFloat('{{ $rowLanding->latitude_2 }}'),
            'lng': parseFloat('{{ $rowLanding->longitude_2 }}')
        }
    @else
        position[1] = {
            'lat': centerPos.lat + 0.05,
            'lng': centerPos.lng + 0.05
        }
    @endif

    marker[1] = new L.Marker(position[1], {
        draggable: true,
        icon: new L.icon({
            iconUrl: markerImage[1],
            iconSize: [60, 57],
            iconAnchor: [30, 57]
        })
    }).addTo(map);

    addMarkerListener(map, marker[1], 'TopRight');

    var autocomplete = new google.maps.places.Autocomplete(document.getElementById('inputGeoName'));
    autocomplete.setFields(['address_components', 'geometry']);

    addPlaceChangedListener(autocomplete, map, marker);
</script>

<script type="text/javascript">

    $(function () {
        $('#inputDescription').summernote({
            height: 300,
            toolbar: [
                ['style', ['style', 'bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['insert', ['link', 'table', 'hr']],
                ['misc', ['codeview']]
            ]
        });
    });
</script>
@stop
