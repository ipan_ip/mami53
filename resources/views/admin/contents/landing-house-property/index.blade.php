@extends('admin.layouts.main')

@section('style')
    <style>
        tr.redirected td {
            color: #ff0000;
        }

        .text-redirected {
            color: #ff0000;
        }
    </style>
@endsection

@section('content')
    <?php App::setLocale('id'); ?>
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
            <div class="box-tools pull-right">
                <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    @if(Auth::user()->role == 'administrator')
                        <a href="/admin/house_property/landing/create">
                            <button type="button" name="button" class="btn-add btn btn-primary btn-sm">
                                <i class="fa fa-plus">&nbsp;</i> Add Landing Page </button>
                        </a>
                    @endif

                    <div class="row">
                        <div class="col-md-12">
                            <form class="form-inline" action="" method="GET" style="margin: 10px;">
                                <input type="text" name="name" class="form-control input-sm"  placeholder="Keyword"  autocomplete="off" value="{{ request()->input('name') }}" />
                                <button class="btn btn-primary btn-md" id="buttonSearch"><i class="fa fa-search">&nbsp;</i>Search</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <p class="helper-block">Warna <span class="text-redirected">Merah</span> berarti landing di-redirect ke landing lain</p>

            <table id="tableListLanding" class="table table-striped">
                <thead>
                    <tr>
                        <th width="30px;">No</th>
                        <th>ID</th>
                        <th>Slug</th>
                        <th>Heading 1</th>
                        <th>Heading 2</th>
                        <th>Keyword</th>
                        <th>Price</th>
                        <th>Update</th>
                        <th class="table-action-column" width="120px">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($landing as $rowLandingKey => $rowLanding)
                    <tr class="{{ !is_null($rowLanding->redirect_id) ? 'redirected' : '' }}">
                        <td>{{ $rowLandingKey+1      }}</td>
                        <td>{{ $rowLanding->id }}</td>
                        <td>{{ $rowLanding->slug     }}</td>
                        <td>{{ $rowLanding->heading_1}}</td>
                        <td>{{ $rowLanding->heading_2}}</td>
                        <td>{{ $rowLanding->keyword  }}</td>
                        <td>{{ $rowLanding->price_min}} - {{ $rowLanding->price_max }}</td>
                        <td>{{ date('d M Y H:i:s', strtotime($rowLanding->updated_at)) }}</td>
                        <td class="table-action-column">
                            @permission('access-landing-page-edit')
                            <div class="row" style="margin-bottom: 10px;">
                                <a href="{{ route('admin.house_property.landing.edit', $rowLanding->id) }}" class="btn btn-xs btn-warning" title="Edit Landing">Edit</a>
                                @endpermission

                                <a class="btn btn-xs btn-default" href="{{ $base_url }}{{ $rowLanding->property_type == 'villa' ? 'villa' : 'kontrakan' }}/{{ $rowLanding->slug }}" title="Show Preview" target="_blank" rel="noopener">
                                    View
                                </a>
                            </div>
                            @permission('access-landing-page-delete')
                                @if(Auth::user()->role == 'administrator')
                                    <form action='{{ route("admin.house_property.landing.destroy", $rowLanding->id) }}' method="POST" style="margin-bottom: 10px;">
                                        {{ method_field('DELETE') }}
                                        {{ csrf_field() }}
                                        <button type='submit' class="btn btn-xs btn-danger" value="delete">delete</button>
                                    </form>
                                @endif
                            @endpermission
                            <a class="btn btn-xs btn-success" href="{{ URL::to('admin/house_property/landing/redirect', $rowLanding->id) }}" title="Redirect" target="_blank" rel="noopener">
                                <i class="fa fa-exchange"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="box-body no-padding">
                {{ $landing->appends(Request::except('page'))->links() }}
            </div>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
    <!-- table -->

@stop

@section('style')
    <!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> -->
@stop

@section('script')
<!-- Holder for Upload File Thumbnail -->
{{ HTML::script('assets/vendor/holder/holder.js') }}

<!-- Jquery Datepicker -->
<!-- <script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script> -->

<!-- page script -->
<script type="text/javascript">

    $(function()
    {
        $('#select-sort-option').change(function(event){
            var url  = '{{ URL::route("admin.room.index") }}' + '?o=' + event.target.value ;

            url += '&st=' + $('#start-date').val();
            url += '&en=' + $('#end-date').val();

            window.location = url;

        });

        $('.datepicker').datepicker({ dateFormat : 'dd-mm-yy'});
    });

    $(function()
    {
        Holder.addTheme("thumbnail", {
            background: '#EEE',
            foreground: '#AAA',
            size: 11,
            fontweight: 'normal',
            text: 'Image'
          });
        // $("#tableListLanding").dataTable();
    });
</script>
@stop
