@extends('admin.layouts.main')

@section('style')
<style>
    .form-submit-inbox {
        display: inline-block;
    }

    .btn-submit-inbox {
        background: transparent;
        border: none;
        display: inline-block;
    }
</style>
@endsection

@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <form action="" class="form-inline" method="get" style="text-align:right;padding:10px;">
                        <input type="text" name="q" class="form-control input-sm"  placeholder="Tanggal (YYYY-MM-DD)"  autocomplete="off" value="{{ request()->input('q') }}">
                        <button class="btn btn-primary btn-md" id="buttonSearch"><i class="fa fa-search">&nbsp;</i>Search</button>
                    </form>
                </div>
            </div>

            <table  class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Tanggal</th>
                        <th>Waktu</th>
                        <th>Pesan</th>
                        <th>Pengirim</th>
                        <th>Action</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                @foreach($inboxes as $key => $inbox)
                    <tr>
                        <td>{{ $inbox->id }}</td>
                        <td>{{ $inbox->tgl }}</td>
                        <td>{{ $inbox->waktu }}</td>
                        <td>{{ $inbox->isiPesan }}</td>
                        <td>{{ $inbox->dari }}</td>
                        <td>
                            @if (strlen($inbox->dari) > 9) 
                                <a href="{{ URL::route('admin.room.two.index') }}?q={{ str_replace('+62', '0', $inbox->dari) }}" 
                                    target="_blank" title="Ke Edit Kost">
                                    <i class="fa fa-location-arrow"></i>
                                </a>
                                
                            @endif
                        </td>
                        <td>
                            @if (strlen($inbox->dari) > 9 && !in_array($inbox->id, $replyIds)) 
                                <form action="{{ URL::route('admin.zenziva-inbox.convert') }}" method="post" class="form-submit-inbox">
                                    <input type="hidden" name="zenziva_id" value="{{ $inbox->id }}">
                                    <input type="hidden" name="date" value="{{ $inbox->tgl }}">
                                    <input type="hidden" name="time" value="{{ $inbox->waktu }}">
                                    <input type="hidden" name="message" value="{{ $inbox->isiPesan }}">
                                    <input type="hidden" name="phone_number" value="{{ $inbox->dari }}">
                                    <button type="submit" title="Masukkan ke Database" class="btn btn-default btn-submit-inbox">
                                        <i class="fa fa-download"></i>
                                    </button>
                                </form>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
                
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
    <!-- table -->
@endsection