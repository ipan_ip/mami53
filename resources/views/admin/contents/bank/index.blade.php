@extends('admin.layouts.main')
@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
            <div class="box-tools pull-right">
                <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">

            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <a href="{{ URL::route('admin.bank.create') }}">
                        <button type="submit" class="btn-add btn btn-primary btn-sm">
                            <i class="fa fa-plus">&nbsp;</i>Add Bank Account</button>
                    </a>
                </div>
            </div>

            <table id="tableListTag" class="table table-striped">
                <thead>
                    <tr>
                        <th width="30px;">No</th>
                        <th>Name</th>
                        <th>Account Name</th>
                        <th>Number</th>
                        <th>is Active</th>
                        <th class="table-action-column">Action</th>
                    </tr>
                </thead>
                <tbody>
                 @foreach($rowsBank AS $key => $bank)
                     <tr>
                         <td>{{ $key+1 }}</td> 
                         <td>{{ $bank->name }}</td>
                         <td>{{ $bank->account_name }}</td>
                         <td>{{ $bank->number }}</td>
                         <td>@if ($bank->is_active == '1') <span class="label label-primary">Yes</span>
                         @else <span class="label label-danger">No</span>
                         @endif </td>
                         <td><a href="{{ URL::route('admin.bank.edit', $bank->id) }}"><i class="fa fa-pencil"></i></a>   
                         <a href="{{ URL::to('admin/bank/destroy', $bank->id) }}"><i class="fa fa-trash-o"></i></a></td>
                     </tr> 
                 @endforeach
                </tbody>
                
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
    <!-- table -->

@stop
@section('script')
<!-- page script -->
<script type="text/javascript">
    $(function()
    {
        $("#tableListTag").dataTable();
    });
</script>
@stop