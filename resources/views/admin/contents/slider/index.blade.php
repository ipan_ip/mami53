@extends('admin.layouts.main')

@section('style')
<style>
    .table>tbody>tr>td {
        vertical-align: middle;
    }

    .font-large {
        font-size: 1.5em;
    }

    .font-semi-large {
        font-size: 1.2em;
    }

    .font-grey {
        color: #9E9E9E;
    }

    .dropdown-menu>li>a:hover {
        color: #333
    }

    .label-grey {
        background-color: #CFD8DC;
    }

    [hidden] {
        display: none !important;
    }

    /* Select2 tweak */

    .input-group .select2-container,
    .form-group .select2-container {
        position: relative !important;
        z-index: 2;
        float: left !important;
        width: 100% !important;
        margin-bottom: 0 !important;
        display: table !important;
        table-layout: fixed !important;
    }

    /* Sweetalert */

    .swal2-container {
        z-index: 99999 !important;
    }

    .swal-shown {
        height: auto !important;
    }

    .custom-swal {
        z-index: 10000 !important;
    }

    .custom-wide-swal {
        width: 480px !important;
        font-size: 12px !important;
    }

    /* Custom table row */
    tr.inactive td {
        background-color: #E6C8C8;
    }

    /* Modal tweak */

    .modal-open {
        overflow: hidden;
        position: fixed;
        width: 100%;
    }


    /* Centering the modal */

    .modal {
        text-align: center;
        padding: 0 !important;
    }

    .modal:before {
        content: '';
        display: inline-block;
        height: 100%;
        vertical-align: middle;
        margin-right: -4px;
    }

    .modal-dialog {
        display: inline-block;
        text-align: left;
        vertical-align: middle;
    }

    /* Grid */
    .grid {
        position: relative;
    }

    .item {
        display: block;
        position: absolute;
        width: 100px;
        height: 100px;
        margin: 5px;
        z-index: 1;
        background: #000;
        color: #fff;
    }

    .item.muuri-item-dragging {
        z-index: 3;
    }

    .item.muuri-item-releasing {
        z-index: 2;
    }

    .item.muuri-item-hidden {
        z-index: 0;
    }

    .item-content {
        position: relative;
        width: 100%;
        height: 100%;
    }

    /* Sortable tweak */
    #sortable li {
        cursor: move !important;
    }
</style>
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.css">
<link rel="stylesheet" href="//gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css">
<link rel="stylesheet" href="//unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.css">
@endsection

@section('content')
<!-- table -->
<div class="box box-default">
    <!-- Box Header -->
    <div class="box-header">
        <div class="col-md-6 text-center">
            <h3 class="box-title"><i class="fa fa-sliders"></i> {{ $boxTitle }}</h3>
        </div>
        <!-- Add button -->
        <div class="col-md-6">
            <div class="form-horizontal pull-right" style="padding-top: 10px;">
                @role('super-admin', 'admin-general', 'developer')
                <button class="btn btn-success actions" style="margin-right: 10px;" onclick="actionButton('create')">
                    <i class="fa fa-plus"></i>&nbsp;&nbsp;Add New Slider
                </button>
                @endrole
            </div>
        </div>
    </div>
    <!-- /.box-header -->
    <!-- box-body -->
    <div class="box-body">
        <!-- Table -->
        @include('admin.contents.slider.partials.table')
        <!-- table -->
    </div>
</div><!-- /.box -->

{{-- Modal --}}
@include('admin.contents.slider.partials.modal')

@include('admin.contents.slider.partials.page-modal')

@endsection

@section('script')
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.js"></script>
<script src="//gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.js"></script>
<script src="//unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.js"></script>
<script type="text/javascript">
    Dropzone.autoDiscover = false;

    function triggerAlert(type, message) {
        Swal.fire({
            type: type,
            customClass: {
                container: 'custom-swal'
            },
            html: message
        });
    }

    function triggerLoading(message) {
        Swal.fire({
            title: message,
            customClass: {
                container: 'custom-swal'
            },
            allowEscapeKey: false,
            allowOutsideClick: false,
            onOpen: () => {
                swal.showLoading();
            }
        });
    }

    function ajaxCall(type, title, html, method, url, payload, input, btnText, btnClass, needRefresh = true) {
        Swal.fire({
            type: type,
            title: title,
            html: html,
            buttonsStyling: btnClass == null,
            confirmButtonText: btnText,
            confirmButtonClass: btnClass,
            showCancelButton: true,
            cancelButtonClass: (btnClass !== null) ? 'btn btn-default' : null,
            customClass: 'custom-swal',
            showLoaderOnConfirm: true,
            input: input,
            preConfirm: (response) => {
                if (input !== null) {
                    let number = response;
                    if (!response) {
                        return Swal.showValidationMessage('Please provide a valid phone number!');
                    }

                    payload = {
                        "template_id": payload.id,
                        "number": response
                    };
                }

                return $.ajax({
                    type: method,
                    url: url,
                    data: payload,
                    dataType: 'json',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: (response) => {
                        if (response.success == false) {
                            Swal.close();
                            triggerAlert('error', response.message);
                        }
                            
                        return;
                    },
                    error: (error) => {
                        Swal.close();
                        triggerAlert('error', JSON.stringify(error));
                    }
                });
            },
            allowOutsideClick: () => !Swal.isLoading()
        }).then((result) => {
            if (!result.dismiss != 'cancel') {
                if (result.value.success == true) {
                    // check if slide page modal is opened
                    if ($('#page-modal').is(':visible')) {
                        $('#page-modal').modal('toggle');
                    }

                    Swal.fire({
                        type: 'success',
                        title: result.value.title,
                        html: result.value.message,
                        onClose: () => {
                            if (needRefresh) {
                                $('#table').bootstrapTable('refresh');
                            }
                        }
                    });
                } else {
                    Swal.fire({
                        type: 'error',
                        title: "Failed!",
                        html: result.value.message
                    });
                }
            }
        });
    }

    // Action buttons click event
    function actionButton(action, data) {
        // Setup Notification
        if (action === 'create') {
            _editing = false;
            _modalTitle = "New Slide Data";

            Swal.fire({
                title: 'Enter the slider name',
                html: "<div class='callout callout-warning text-left'>"+
                        "<p class='font-grey'><strong>" + 
                        "<i class='fa fa-exclamation-circle'></i> Attention!" +
                        "<li>The name should be unique (not being used by another slider)</li>" + 
                        "<li>Only alphanumeric and space characters are allowed</li>" + 
                        "</strong></p>"+
                        "</div>",
                input: 'text',
                confirmButtonText: 'Check Name',
                showCancelButton: true,
                customClass: 'custom-wide-swal',
                showLoaderOnConfirm: true,
                preConfirm: (val) => {
                    if (val === '') Swal.showValidationMessage("Type the name first!");

                    if (val) {
                        // just get the "slug"
                        return $.ajax({
                            type: 'POST',
                            url: "/admin/slider/verify",
                            dataType: 'json',
                            headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            data: {
                                'name': val
                            },
                            success: (response) => {
                                if (!response.status === true) {
                                    Swal.showValidationMessage(response.message);
                                }

                                return;
                            },
                            error: (error) => {
                                Swal.showValidationMessage('Request Failed: ' + error.message);
                            }
                        });
                    }
                },
                allowOutsideClick: () => !Swal.isLoading()

            }).then((result) => {
                if (!result.dismiss) {
                    if (result.value.status === true) {
                        triggerLoading('Processing slider..');

                        _processedName = result.value.data.name;
                        _generatedEndpoint = result.value.data.endpoint;

                        // Store the slider
                        return $.ajax({
                            type: 'POST',
                            url: "/admin/slider",
                            dataType: 'json',
                            headers: {
                                'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr('content')
                            },
                            data: {
                                'name': _processedName,
                                'endpoint': _generatedEndpoint
                            },
                            success: (response) => {
                                if (!response.success) {
                                    Swal.showValidationMessage(response.message);
                                }

                                Swal.fire({
                                    type: 'success',
                                    title: "Slider successfully created!",
                                    html: "You may later add slides by clicking [<i class='fa fa-plus'></i> Add Slide Page]",
                                    onClose: () => {
                                        $('#table').bootstrapTable('refreshOptions', {});
                                    }
                                });
                            },
                            error: (error) => {
                                Swal.showValidationMessage('Request Failed: ' + error.message);
                            }
                        });
                    }
                }
            });                    
        }

        // Adding slides action
        else if (action === 'add') {
            _editing = true;
            _data = data;
            _modalTitle = "Slider <strong>" + _data.name + "</strong>";

            $('#modal').modal('show');
        }

        // Preview slide pages action
        else if (action === 'preview') {
            _data = data;
            _modalTitle = "Slides for slider <strong>" + _data.name + "</strong>";

            $('#page-modal').modal('show');
        }

        // Editing action
        else if (action === 'edit') {
            _editing = true;
            _data = data;
        }

        // Activate / Deactivate action
        else if (action === 'activate' || action === 'deactivate') {
            let title, html, btnText, url, type, payload;

            _data = data;

            if (action === 'activate') {
                if (_data.pages_count < 1) {
                    triggerAlert('error', "<h5>Please add some slide pages first!<br/><small>You may do it by clicking button <label label-default label-xs><i class='fa fa-plus'></i> Add Slide Page</label></small></h5>");
                    return;
                }
                
                type = 'question';
                title = 'Activate slider ' + data.name + '?';
                html = null;
                btnText = 'Activate';
                url = "/admin/slider/activate";
                payload = {
                    status: 1,
                    id: _data.id
                };
            } else {
                type = 'warning';
                title = 'Deactivate slider ' + data.name + '??';
                html = 'Warning! This action will likely break current pages which are using it';
                btnText = 'Deactivate';
                url = "/admin/slider/deactivate";
                payload = {
                    status: 0,
                    id: _data.id
                };
            }

            ajaxCall(type, title, html, 'POST', url, payload, null, btnText, null);
        }

        // Removing slider data
        else if (action == 'delete') {
            _data = data;
            
            if (_data.is_active === 1) {
                triggerAlert('error', "<h5>Please deactivate the slider first!</h5>");
                return;
            }

            Swal.fire({
                title: 'Remove slider ' + _data.name + '?',
                type: 'question',
                buttonsStyling: false,
                confirmButtonClass: 'btn btn-danger',
                showCancelButton: true,
                cancelButtonClass: 'btn btn-default',
                showLoaderOnConfirm: true,
                confirmButtonText: 'Yes, remove it!',
                preConfirm: (val) => {
                    return $.ajax({
                        type: 'POST',
                        url: "/admin/slider/remove",
                        dataType: 'json',
                        headers: {
                            'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr("content")
                        },
                        data: {
                            'id': _data.id,
                        },
                        success: (response) => {
                            return response;
                        },
                        error: (error) => {
                            triggerAlert('error', error.message);
                        }
                    });
                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                var res = result.value;
                if (res.success) {
                    Swal.fire({
                        type: 'success',
                        title: "Owner testimony data successfully removed",
                        text: "Page will be refreshed after you clicked OK",
                        onClose: () => {
                            $('#table').bootstrapTable('refreshOptions', {});
                        }
                    });
                } else {
                    triggerAlert('error', res.message);
                }
            })
        }

        // Removing slide page data
        else if (action == 'page_delete') {
            _data = data;

            Swal.fire({
                title: 'Remove slide ' + _data.title + '?',
                type: 'question',
                buttonsStyling: false,
                confirmButtonClass: 'btn btn-danger',
                showCancelButton: true,
                cancelButtonClass: 'btn btn-default',
                showLoaderOnConfirm: true,
                confirmButtonText: 'Yes, remove it!',
                preConfirm: (val) => {
                    return $.ajax({
                        type: 'POST',
                        url: "/admin/slider/page/remove",
                        dataType: 'json',
                        headers: {
                            'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr("content")
                        },
                        data: {
                            'id': _data.id,
                        },
                        success: (response) => {
                            return response;
                        },
                        error: (error) => {
                            triggerAlert('error', error.message);
                        }
                    });
                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                var res = result.value;
                if (res.success) {
                    $('#page-modal').modal('toggle');

                    Swal.fire({
                        type: 'success',
                        title: res.title,
                        text: res.message,
                        onClose: () => {
                            $('#table').bootstrapTable('refreshOptions', {});
                        }
                    });
                } else {
                    triggerAlert('error', res.message);
                }
            })
        }

        // Activate / Deactivate slide page
        else if (action === 'page_activate' || action === 'page_deactivate') {
            let title, html, btnText, url, type, payload;

            _data = data;

            if (action === 'page_activate') {
                type = 'question';
                title = 'Activate slide ' + data.title + '?';
                html = null;
                btnText = 'Activate';
                url = "/admin/slider/page/activate";
                payload = {
                    status: 1,
                    id: _data.id
                };
            } else {
                type = 'warning';
                title = 'Deactivate slide ' + data.title + '??';
                html = null;
                btnText = 'Deactivate';
                url = "/admin/slider/page/deactivate";
                payload = {
                    status: 0,
                    id: _data.id
                };
            }

            ajaxCall(type, title, html, 'POST', url, payload, null, btnText, null);
        }

        // Set slide page as cover 
        else if (action === 'page_set_as_cover') {
            _data = data;

            type = 'warning';
            title = 'Set slide "' + data.title + '" as cover?';
            html = 'Other slide which is currently acted as cover will be automatically updated!';
            btnText = 'Yes, set it up!';
            url = "/admin/slider/page/setCover";
            payload = {
                slider_id: _data.slider_id,
                id: _data.id
            };

            ajaxCall(type, title, html, 'POST', url, payload, null, btnText, null);
        }

        else {
            triggerAlert('error', 'Undefined Action!');
        }
    }

    // Load sliders data required by Bootstrap-table
    function ajaxLoadData(params) {
        const url = "{{ url('/') }}/admin/slider/data";
        
        $.get(url + '?' + $.param(params.data)).then(res => {
            if (res.data) {
                params.success(res.data);
            }

            // Enable tooltip
            $('[data-toggle="tooltip"]').tooltip();
        })
    }

    function nameFormatter(value, row) {
        return "<strong>" + value + "</strong>";
    }

    function totalSlideFormatter(value, row) {
        let html = "<div class=\"form-inline\">";

        if (value > 0) {
            html += "<span class='badge badge-default'>" + value + "</span> <a href='#' class=\"btn btn-xs btn-default\" onclick='actionButton(\"preview\", " + JSON.stringify(row) + ")' data-toggle=\"tooltip\" data-placement=\"top\" title=\"Preview Slides\" style=\"margin-right:5px;\"><i class='fa fa-share'></i></a>";
        } else {
            html += "<a href='#' class=\"btn btn-sm btn-default\" onclick='actionButton(\"add\", " + JSON.stringify(row) + ")' data-toggle=\"tooltip\" data-placement=\"top\" title=\"Add Slide Page\" style=\"margin-right:5px;\"><i class='fa fa-plus-square'></i> Add Slide</a>";
        }

        return html;
    }

    function statusFormatter(value) {
        if (value == "NON ACTIVE") {
            return "<span class='label label-danger'>" +
                value +
                "</span>";
        } else {
            return "<span class='label label-success'>" +
                value +
                "</span>";
        }
    }

    // Action button compilator
    function actionFormatter(value, row, index) {
        let $html = "";
        
        $html += "<a href='#' class=\"btn btn-sm btn-default\" onclick='actionButton(\"add\", " + JSON.stringify(row) + ")' data-toggle=\"tooltip\" data-placement=\"top\" title=\"Add Slide Page\" style=\"margin-right:5px;\"><i class='fa fa-plus-square'></i></a>";
        
        if (row.status === "NON ACTIVE") {
            $html += "<a href='#' class=\"btn btn-sm btn-success\" onclick='actionButton(\"activate\", " + JSON.stringify(row) + ")' data-toggle=\"tooltip\" data-placement=\"top\" title=\"Activate Slider\" style=\"margin-right:20px;\"><i class='fa fa-arrow-up'></i></a>";
        } else {
            $html += "<a href='#' class=\"btn btn-sm btn-warning\" onclick='actionButton(\"deactivate\", " + JSON.stringify(row) + ")' data-toggle=\"tooltip\" data-placement=\"top\" title=\"Deactivate Slider\" style=\"margin-right:20px;\"><i class='fa fa-arrow-down'></i></a>";
        }             
                            
        $html += "<a href='#' class=\"btn btn-sm btn-danger\" onclick='actionButton(\"delete\", " + JSON.stringify(row) + ")' data-toggle=\"tooltip\" data-placement=\"top\" title=\"Delete Slider\"><i class='fa fa-trash'></i></a>";

        return $html;
    }

    $(function () {

        // :: Modal Events ::
        let $modal = $('#modal');
        let $pageModal = $('#page-modal');
        let $table = $('#table');

        $table.bootstrapTable({
            columns: [{
                field: 'name',
                title: 'Date Stamp',
                cellInputEnabled: true,
                cellInputType: 'text'
            }]
        });

        $modal
            .on("show.bs.modal", (e) => {
                // triggerLoading('Setting up form..');

                $modal.data('bs.modal').options.keyboard = false;
                $modal.data('bs.modal').options.backdrop = 'static';

                // Form elements
                const modalTitle = $('#modalTitle');
                const imageUploader = $('#imageSection');
                const nameInput = $('#name');
                const contentInput = $('#content');
                const redirectLabelInput = $('#redirectLabel');
                const redirectLinkInput = $('#redirectLink');
                const isCoverCheckbox = $('#isCover');
                const activateCheckbox = $('#activate');
                const photoInput = $('#photoId');
                const submitButton = $('#submit');

                // Initiate components
                modalTitle.html(_modalTitle);

                // initiate Dropzone
                if (Dropzone.instances.length == 0) {
                    imageUploader.dropzone({
                        paramName: "media",
                        url: '/admin/media',
                        maxFilesize: 2,
                        maxFiles: 1,
                        acceptedFiles: 'image/*',
                        dictDefaultMessage: 'Click to upload',
                        dictCancelUpload: 'Cancel',
                        dictRemoveFile: 'Remove',
                        addRemoveLinks: true,
                        thumbnailWidth: 360,
                        thumbnailHeight: 360,
                        success: function (file, response) {
                            photoInput.val(response.media.id);
                            photoInput.attr('data-filename', file.name);
                        },
                        init: function () {
                            // generate existing photo
                            // if (_editing) {
                            //     let mockFile = {name: _data.name + 'jpg', size: 12345};

                            //     if (_data.media_url !== undefined) {
                            //         this.emit('thumbnail', mockFile, _data.media_url.medium);
                            //     }
                            // }

                            this.on("addedfile", function (file) {
                                triggerLoading('Uploading');
                            });

                            this.on("thumbnail", function (file, dataUrl) {
                                $('.dz-image').last().find('img').attr({width: '100%', height: '100%'});
                            });

                            this.on("success", function (file) {
                                $('.dz-image').css({"width": "100%", "height": "auto"});
                                if (Swal.isLoading) Swal.close();
                            })

                            this.on("maxfilesexceeded", function (file) {
                                this.removeAllFiles();
                                this.addFile(file);
                            });

                            this.on("removedfile", function (file) {
                                photoInput.val('');
                            });
                        }
                    });
                }

                function validateFormData() {
                    if (nameInput.val() === '') {
                        triggerAlert('error', "<h5>Heading text is required!</h5>");
                        nameInput.parent().addClass('has-error');
                        nameInput.focus();
                        return false;
                    }

                    if (contentInput.val() === '') {
                        triggerAlert('error', "<h5>Content is required!</h5>");
                        contentInput.parent().addClass('has-error');
                        contentInput.focus();
                        return false;
                    }

                    if (photoInput.val() === '') {
                        triggerAlert('error', "<h5>Slide image is required!</h5>");
                        photoInput.parent().addClass('has-error');
                        return false;
                    }

                    return true;
                }
                
                function getFormData() {
                    let formData = new Object();

                    formData.slider_id = _data.id;
                    formData.title = nameInput.val();
                    formData.content = contentInput.val();
                    formData.redirect_label = redirectLabelInput.val() == "" ? null : redirectLabelInput.val();
                    formData.redirect_link = redirectLinkInput.val() == "" ? null : redirectLinkInput.val();
                    formData.is_cover = isCoverCheckbox.prop('checked') == true ? 1 : 0;
                    formData.is_active = activateCheckbox.prop('checked') == true ? 1 : 0;
                    formData.media_id = photoInput.val();
                    formData.creator_id = {{ Auth::user()->id }};
                    
                    return formData;
                }

                // Submit the form
                submitButton.on('click', e => {
                    e.preventDefault();

                    if (validateFormData()) {
                        // compile dataObject
                        let dataObject = getFormData();

                        Swal.fire({
                            type: 'question',
                            customClass: {
                                container: 'custom-swal'
                            },
                            title: 'Save slide data?',
                            showCancelButton: true,
                            confirmButtonText: 'Save',
                            showLoaderOnConfirm: true,
                            preConfirm: () => {
                                return $.ajax({
                                    type: 'POST',
                                    url: "/admin/slider/page",
                                    dataType: 'json',
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                    },
                                    data: dataObject
                                });
                            },
                            allowOutsideClick: () => !Swal.isLoading()
                        }).then((result) => {
                            if (!result.dismiss) {
                                const response = result.value;

                                if (!response.success) {
                                    triggerAlert('error', '<h5>' + response.message + '</h5>');
                                } else {
                                    $('#modal').modal('toggle');

                                    Swal.fire({
                                        type: 'success',
                                        customClass: {
                                            container: 'custom-swal'
                                        },
                                        title: "Slide data successfully saved!",
                                        onClose: () => {
                                            $('#table').bootstrapTable('refreshOptions', {});
                                        }
                                    });
                                }

                            }
                        })
                    }
                })
            })
            .on("hide.bs.modal", (e) => {
                //
            });

        $pageModal
            .on("show.bs.modal", e => {
                $pageModal.data('bs.modal').options.keyboard = false;
                $pageModal.data('bs.modal').options.backdrop = 'static';

                // Form elements
                const modalTitle = $('#pageModalTitle');
                const gridContainer = $('.list-unstyled');
                const submitButton = $('#submit2');

                // Initialize components
                gridContainer.html('');

                let html = '';
                let pages = _data.pages;
                pages.forEach(page => {

                    if (page.image_url !== null) {
                        imageUrl = page.image_url.medium;
                    } else {
                        imageUrl = '';
                    }

                    html += '<li class="col-md-3" id=' + page.id + '>' +
								'<div class="thumbnail" style="padding: 0">' +
									'<div style="padding:4px">' +
										'<img alt="' + page.title + '" style="width: 100%" src="' + imageUrl + '">' +
									'</div>' +
									'<div class="caption">' +
										'<h2>' + page.title + '</h2>' +
										'<p>' + page.content + '</p>' +
                                        '<p>';

                    if (page.is_cover === 1) {
                        html += '<span class="label label-primary" style="margin-right:10px!important;">Cover</span>';
                    }

                    if (page.is_active === 1) {
                        html += '<span class="label label-success" style="margin-right:10px!important;">Active</span>';
                    } else {
                        html += '<span class="label label-danger" style="margin-right:10px!important;">Inactive</span>';
                    }

                    if (page.redirect_link !== '' && page.redirect_label !== '') {
                        html += '<span class="label label-info" style="margin-right:10px!important;">Has Redirection</span>';
                    }

					html += '</p>' +
                            '</div>' +
                            '<div class="modal-footer" style="text-align: left">' +
                                '<div class="row">' +
                                    '<div class="col-md-12 text-center">';
                    
                    if (page.is_active === 1) {
                        html += "<a href='#' class=\"btn btn-xs btn-warning\" onclick='actionButton(\"page_deactivate\", " + JSON.stringify(page) + ")' style=\"margin-right:5px;\"><i class='fa fa-arrow-down'></i> Deactivate</a>";
                    } else {
                        html += "<a href='#' class=\"btn btn-xs btn-success\" onclick='actionButton(\"page_activate\", " + JSON.stringify(page) + ")' style=\"margin-right:5px;\"><i class='fa fa-arrow-up'></i> Activate</a>";
                    }
                    
                    if (page.is_cover !== 1) {
                        html +=     "<a href='#' class=\"btn btn-xs btn-primary\" onclick='actionButton(\"page_set_as_cover\", " + JSON.stringify(page) + ")' style=\"margin-right:5px;\"><i class='fa fa-tag'></i> Set As Cover</a>";
                    }

                    html += "<a href='#' class=\"btn btn-xs btn-danger\" onclick='actionButton(\"page_delete\", " + JSON.stringify(page) + ")' data-toggle=\"tooltip\" data-placement=\"top\" title=\"Delete Slider\"><i class='fa fa-trash-o'></i></a>" +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</li>';
                });

                gridContainer.html(html);

                // Initiate components
                modalTitle.html(_modalTitle);

                // Sortable handler
                $('#sortable').sortable({
                    cursor: 'move',
                    axis: 'x',
                    update: (e, ui) => {
                        triggerLoading('Saving order...');

                        var order = []; 
                        $('#sortable li').each( function(e) {
                            order.push({
                                id: parseInt($(this).attr('id')),
                                sort: $(this).index() + 1
                            });
                        });
                        
                        order = JSON.stringify(order);
                        $.ajax({
                            type: 'POST',
                            url: "/admin/slider/page/order",
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            contentType: 'application/json',
                            data: order,
                            success: (response) => {
                                if (!response.success) {
                                    triggerAlert('error', response.message);
                                    return;
                                }

                                $('#page-modal').modal('toggle');

                                Swal.fire({
                                    type: 'success',
                                    title: response.title,
                                    text: response.message,
                                    onClose: () => {
                                        $('#table').bootstrapTable('refreshOptions', {});
                                    }
                                });
                            },
                            error: (error) => {
                                triggerAlert('error', error.message);
                            }
                        });
                    }
                });
            })
        
    });
</script>
@endsection