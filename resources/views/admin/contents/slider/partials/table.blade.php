<table 
    id="table" 
    class="table table-responsive" 
    data-toggle="table" 
    data-show-refresh="true" 
    data-search="true"
    data-show-search-button="false" 
    data-side-pagination="server" 
    data-pagination="true" 
    data-ajax="ajaxLoadData">
    <thead>
        <tr>
            <th class="text-center" data-field="id">ID</th>
            <th class="text-center" data-field="name" data-formatter="nameFormatter">Slider Name</th>
            <th class="text-center" data-field="pages_count" data-formatter="totalSlideFormatter">Total Slides</th>
            <th class="text-center" data-field="endpoint">API Endpoint</th>
            <th class="text-center" data-field="status" data-formatter="statusFormatter">Status</th>
            <th class="text-center" data-field="creator_name">Creator</th>
            <th class="text-center" data-field="last_updated">Last Updated</th>
            <th class="text-center" data-field="actions" data-formatter="actionFormatter">Actions</th>
        </tr>
    </thead>
</table>