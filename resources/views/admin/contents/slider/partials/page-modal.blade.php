{{-- Add / Update Modal --}}
<div class="modal" id="page-modal" role="dialog" aria-labelledby="modalLabel">
	<div class="modal-dialog modal-lg" role="modal">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top:2px;"><i
						class="fa fa-lg fa-times-circle"></i></button>
				<h4 class="modal-title"><i class="fa fa-plus"></i>&nbsp;&nbsp;<span id="pageModalTitle"></span></h4>
			</div>
			<div class="modal-body" style="overflow-x: auto !important;">
				<div class="container">
					<div class="row">
						<ul class="thumbnails list-unstyled" id="sortable"></ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>