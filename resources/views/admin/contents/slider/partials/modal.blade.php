{{-- Add / Update Modal --}}
<div class="modal" id="modal" role="dialog" aria-labelledby="modalLabel">
	<div class="modal-dialog modal-lg" role="modal">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top:2px;"><i
						class="fa fa-lg fa-times-circle"></i></button>
				<h4 class="modal-title"><i class="fa fa-plus"></i>&nbsp;&nbsp;<span id="modalTitle"></span></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-7">
						<div class="row">
							<div class="form-group col-md-12 col-md-offset-0">
								<label for="name" class="control-label">Heading</label>
								<input type="text" class="form-control" placeholder="" id="name" name="name">
							</div>
							<div class="form-group col-md-12 col-md-offset-0">
								<label for="email" class="control-label">Content</label>
								<textarea class="form-control" placeholder="" rows="5" id="content"
									name="content"></textarea>
							</div>
							<div class="form-group col-md-12 col-md-offset-0">
								<label for="redirect_label" class="control-label">Redirect Label</label>
								<input type="text" class="form-control" placeholder="" id="redirectLabel"
									name="redirect_label">
							</div>
							<div class="form-group col-md-12 col-md-offset-0">
								<label for="redirect_label" class="control-label">Redirect URL</label>
								<input type="text" class="form-control" placeholder="" id="redirectLink"
									name="redirect_link">
							</div>
							<div class="form-group col-md-6">
								<label for="is_active">Set As Cover Slide?</label><br />
								<input type="checkbox" class="form-control" id="isCover" name="is_cover"
									data-toggle="toggle" data-on="Yes" data-off="No" data-onstyle="success"
									data-offstyle="danger">
							</div>
							<div class="form-group col-md-6">
								<label for="is_active">Enable After Saving?</label><br />
								<input type="checkbox" class="form-control" id="activate" name="is_active"
									data-toggle="toggle" data-on="Yes" data-off="No" data-onstyle="success"
									data-offstyle="danger" checked>
							</div>
						</div>
					</div>
					<div class="col-md-5">
						<div class="row">
							<div class="form-group col-md-12 text-center">
								<label>Slide Image</label><br />
								<form id="imageSection" action="/admin/media" method="POST" class="dropzone">
									{{ csrf_field() }}
									<input type="hidden" name="media_type" value="slide_image">
								</form>
								<small class="text-muted">
									<h5>IMPORTANT!</h5>
									Make sure the photo has <strong>1:1 ratio (square)</strong> and <strong>does not
										exceed 2 MB</strong> on size for best display results!
								</small>
								<input type="hidden" id="photoId" name="photo_id" value="" data-filename="">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="submit">
					<i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save
				</button>
			</div>
		</div>
	</div>
</div>