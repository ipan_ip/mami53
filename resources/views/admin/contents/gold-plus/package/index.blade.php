@extends('admin.layouts.main')

@section('content')
	<!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Gold Plus Package</h3>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <table class="table">
                <thead>
                    <tr>
                        <th class="text-center">ID</th>
                        <th class="text-center">Property Level ID</th>
                        <th class="text-center">Code</th>
                        <th class="text-center">Name</th>
                        <th class="text-center">Description</th>
                        <th class="text-center">Price</th>
                        <th class="text-center">Periodicity</th>
                        <th class="text-center">Unit Type</th>
                        <th class="text-center">Status</th>
                        <th class="text-center">Created At</th>
                        <th class="text-center">Updated At</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                @foreach($packages as $package)
                    <tr>
                        <td align="center" style="vertical-align: middle;">{{ $package->id }}</td>
                        <td align="center" style="vertical-align: middle;">{{ $package->property_level_id }}</td>
                        <td align="center" style="vertical-align: middle;">{{ $package->code }}</td>
                        <td align="center" style="vertical-align: middle;">{{ $package->name }}</td>
                        <td align="left" style="vertical-align: middle;">{{ $package->description }}</td>
                        <td align="center" style="vertical-align: middle;">{{ $package->price }}</td>
                        <td align="center" style="vertical-align: middle;">{{ $package->periodicity }}</td>
                        <td align="center" style="vertical-align: middle;">{{ $package->unit_type }}</td>
                        <td align="center" style="vertical-align: middle;"><span class="label @if($package->active) label-success @else label-danger @endif">@if($package->active) Active @else Inactive @endif</span></td>
                        <td align="center" style="vertical-align: middle;">{{ $package->created_at->format('d F Y H:i:s') }}</td>
                        <td align="center" style="vertical-align: middle;">{{ $package->updated_at->format('d F Y H:i:s') }}</td>

                        <td align="center" class="table-action-column">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-cog"></i> <span class="caret"></span>
                                </button>

                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a href="{{ route('admin.gold-plus.package.edit', $package->id) }}" title="Edit">
                                            <i class="fa fa-pencil"></i> Edit Data
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr> 
                @endforeach
                </tbody>
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
    <!-- table -->
@endsection