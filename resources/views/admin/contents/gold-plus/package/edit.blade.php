@extends('admin.layouts.main')

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Edit Gold Plus Package</h3>
        </div>

        <form action="{{ URL::route('admin.gold-plus.package.update', ['id' => $package->id]) }}" method="POST" id="form-edit" class="form-horizontal form-bordered">
            @csrf
            {{ method_field('PUT') }}
            
            <div class="box-body no-padding">
                <div class="form-group bg-default">
                    <label for="code" class="control-label col-sm-2">Code</label>
                    <div class="col-sm-10">
                        <input type="text" readonly name="code" id="code" class="form-control" value="{{ old('code', $package->code) }}">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="name" class="control-label col-sm-2">Name</label>
                    <div class="col-sm-10">
                        <input type="text" readonly name="name" id="name" class="form-control" value="{{ old('name', $package->name) }}">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="description" class="control-label col-sm-2">Description</label>
                    <div class="col-sm-10">
                        <input type="text" name="description" id="description" class="form-control" value="{{ old('description', $package->description) }}">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="display-name" class="control-label col-sm-2">Price</label>
                    <div class="col-sm-10">
                        <input type="text" name="price" id="price" class="form-control" value="{{ old('price', $package->price) }}">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <div class="col-sm-10 col-sm-push-2">
                        <button type="submit" class="btn btn-primary">Save</button>
                        <a class="btn btn-warning" href="{{ URL::route('admin.gold-plus.package.index') }}">Cancel</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('script')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

    <script>
        $(document).ready(function() {
            $('#form-edit').submit(function(e) {
                e.preventDefault();

                let package = $('#name').val();

                Swal.fire({
                    title: 'Konfirmasi',
                    html: 'Apakah Anda yakin ingin mengubah data paket <strong>' + package + '</strong>?',
                    showCancelButton: true,
                    confirmButtonColor: "#3c8dbc",
                    confirmButtonText: "Ubah",
                    cancelButtonText: "Batal"
                }).then((result) => {
                    if (result.isConfirmed) {
                        $('#form-edit').off().submit();
                    }
                })
            });
        });
    </script>
@endsection