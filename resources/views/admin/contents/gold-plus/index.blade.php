@extends('admin.layouts.main')
@section('content')
<div id="vue-app">
    <div class="box box-default">
        <!-- Box Header -->
        <div class="box-header">
        </div><!-- /.box-header -->
        <!-- box-body -->
        <div class="box-body">
            <table class="table table-responsive table-striped">
                <thead>
                    <tr>
                        <th class="text-center">Nama Owner</th>
                        <th class="text-center">No HP Akun Owner</th>
                        <th class="text-center">GP Level</th>
                        <th class="text-center">Nama Kos</th>
                        <th class="text-center">Kota</th>
                        <th class="text-center">Link Statistics</th>
                        <th class="text-center">Latest Statistics Timestamp</th>
                        <th class="text-center">Action</th>
                    </tr>
                    <tr>
                        <td><input class="form-control input-sm" type="text" v-model="query.owner_name"></td>
                        <td><input class="form-control input-sm" type="text" v-model="query.owner_phone"></td>
                        <td>
                            <select class="form-control input-sm"  v-model="query.gp_level">
                                <option value="">-</option>
                                @foreach (__('api.goldplus.filters') as $goldplusLevelId => $translation)
                                    <option value="{{ $goldplusLevelId }}">{{ $translation['value'] }}</option>
                                @endforeach
                            </select>
                        </td>
                        <td>
                            <div class="input-group">
                                <input style="width: 70%" class="form-control input-sm" type="text" v-model="query.room_name">
                                <select style="width: 30%" class="form-control input-sm" v-model="query.is_testing">
                                    <option value="">-</option>
                                    <option value="1">Testing</option>
                                    <option value="0">Non Testing</option>
                                </select>
                            </div>
                        </td>
                        <td colspan="3"></td>
                        <td class="text-center">
                            {{-- The real form with hidden fields. Because somehow form cannot be a child of tabble/thead/tr --}}
                            {{-- Works by the help of vue bindings to bind two input (this hidden with form and shown field that can be edited by user) --}}
                            <form action="" method="get">
                                <input type="hidden" name="owner_name" v-model="query.owner_name">
                                <input type="hidden" name="owner_phone" v-model="query.owner_phone">
                                <input type="hidden" name="gp_level" v-model="query.gp_level">
                                <input type="hidden" name="room_name" v-model="query.room_name">
                                <input type="hidden" name="is_testing" v-model="query.is_testing">
                                <input type="hidden" name="report_status" v-model="query.report_status">
                                <button class="btn btn-flat btn-block btn-sm btn-info" type="submit">Filter</button>
                            </form>
                        </td>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($rooms as $room)
                        <tr>
                            <td>{{ $room->owners[0]->user->name ?? '-' }}</td>
                            <td>{{ $room->owners[0]->user->phone_number ?? '-' }}</td>
                            <td>{{ $room->level_info['name'] ?? '-'}} </td>
                            <td>
                                <span style="display: block">{{ $room->name }}</span>
                                @if ($room->is_testing)
                                    <span class="badge progress-bar-danger"A>Testing</span>
                                @endif
                            </td>
                            <td>{{ $room->area_city ?? '-' }}</td>
                            <td>{{ $room->statisticUrl ?? '-' }}</td>
                            <td>{{ $room->latestReportTimestamp ?? '-' }}</td>
                            <td>
                                @if (!is_null($room->statisticUrl))
                                    <div v-if="copiedUrl == '{{ $room->statisticUrl }}'" class="text-center">
                                        <span>Copied</span>
                                    </div>
                                    <div v-else>
                                        <a class="btn btn-flat btn-sm btn-block bg-green" href="javascript:void(0)" v-on:click="copyTextToClipboard('{{ $room->statisticUrl }}'); return false;">Copy Link Report</a>
                                    </div>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div><!-- /.box-body -->
        {{ $pagination }}
    </div><!-- /.box -->
</div>
@endsection

@section('script')
<script type="text/javascript" src="/js/vue/vue.2.5.13.js"></script>
<script type="text/javascript" src="/js/vue/vue-resource.1.3.5.js"></script>
<script>
    $(function() {
        // Select the previously choosen filter.
        $('select').each(function (idx, selectObj) {
            var selectedVal = $(selectObj).data('selected');
            if  (selectedVal != undefined) {
                $(selectObj).find(`option[value="${selectedVal}"]`).prop('selected', true);
            }
        });
    })

    new Vue({
        el: '#vue-app',
        delimiters: ['${','}'],
        data: {
            query: {},
            copiedUrl: '',
        },
        created()
        {
            let uri = window.location.href.split('?');
            if (uri.length == 2) {
                let vars = uri[1].split('&');
                let query = {}
                vars.forEach(function(v){
                    [key, value] = v.split('=');
                    if (value && value !== '') {
                        // Use decode uri component and replace + to space to handle value that contain space for select options.
                        query[key] = decodeURIComponent(value.replace(/\+/g, ' '));
                    }
                });
                this.query = query;
            }
            console.log('query: ', this.query);
        },
        mounted: function() {
        },
        methods: {
            // Credits of copy text to clipboard: https://stackoverflow.com/questions/400212/how-do-i-copy-to-the-clipboard-in-javascript
            fallbackCopyTextToClipboard: function(text) {
                var textArea = document.createElement("textarea");
                textArea.value = text;

                // Avoid scrolling to bottom
                textArea.style.top = "0";
                textArea.style.left = "0";
                textArea.style.position = "fixed";

                document.body.appendChild(textArea);
                textArea.focus();
                textArea.select();

                try {
                    var successful = document.execCommand('copy');
                    var msg = successful ? 'successful' : 'unsuccessful';
                    console.log('Fallback: Copying text command was ' + msg);
                    this.copiedUrl = text;
                } catch (err) {
                    console.error('Fallback: Oops, unable to copy', err);
                }

                document.body.removeChild(textArea);
            },
            copyTextToClipboard: function(text) {
                if (!navigator.clipboard) {
                    this.fallbackCopyTextToClipboard(text);
                    return;
                }
                navigator.clipboard.writeText(text).then(function() {
                    console.log('Async: Copying to clipboard was successful!');
                    this.copiedUrl = text;
                }, function(err) {
                    console.error('Async: Could not copy text: ', err);
                });
            },
        },
    });
</script>
@endsection
