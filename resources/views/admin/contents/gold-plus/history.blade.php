@extends('admin.layouts.main')
@section('content')
<div id="vue-app">
    <div class="box box-default">
        <!-- Box Header -->
        <div class="box-header">
            <h3 class="box-title">Gold Plus Report Action Log History</h3>
        </div><!-- /.box-header -->
        <!-- box-body -->
        <div class="box-body">
            <div class="row">
                <div class="col-sm-4">
                    <table class="table table-responsive">
                        <tbody>
                            <tr>
                                <th class="text-right active">Nama Kost</th>
                                <td>{{ $room->name ?? '-' }}</td>
                            </tr>
                            <tr>
                                <th class="text-right active">Nama Owner</th>
                                <td>{{ $room->owner_name ?? '-' }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row" style="margin-top: 20px">
                <div class="col-sm-12">
                    <table class="table table-responsive">
                        <thead>
                            <tr>
                                <th class="text-center">Log</th>
                                <th class="text-center">Actor</th>
                                <th class="text-center">Timestamp</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($logs as $log)
                                <tr>
                                    <td>{{ $log->action_code_message ?? $log->action_code }}</td>
                                    <td>{{ $log->admin->name }}</td>
                                    <td>{{ $log->created_at }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</div>
@endsection

@section('script')
@endsection
