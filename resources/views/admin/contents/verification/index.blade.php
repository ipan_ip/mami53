@extends('admin.layouts.main')

@php
    use App\Entities\Activity\ActivationCodeType;
    use App\Enums\Activity\ActivationCodeDeliveryReportStatus;

    $formatCategoryCaller = function ()
    {
        if (empty($this->for)) {
            return '-';
        }
        $key = 'activation-code-type.description.' . $this->for;
        $description = __($key);
        return $description !== $key ? $description : ActivationCodeType::coerce($this->for) ?? $this->for;
    };

    $formatStatusCaller = function ()
    {
        if (!isset($this->delivery_report->status)) {
            return '-';
        }
        return ActivationCodeDeliveryReportStatus::coerce($this->delivery_report->status)->description ?? $this->delivery_report->status;
    };
@endphp
@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div><!-- /.box-header -->

        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                   <form action="" class="form-inline" method="get" style="text-align:right;padding:10px;">
                        <input type="text" name="phone" class="form-control input-sm"  placeholder="Nomor HP"  autocomplete="off" value="{{ request()->input('phone') }}">
                        <button class="btn btn-primary btn-md" id="buttonSearch"><i class="fa fa-search">&nbsp;</i>Search</button>
                   </form>
                </div>
            </div>

            <table id="tableListArea" class="table table-striped">
                <thead>
                    <tr>
                        <th>No. HP</th>
                        <th>Via</th>
                        <th>Kode Verifikasi</th>
                        <th>Waktu Request</th>
                        <th>Waktu Expired</th>
                        <th>Kategory</th>
                        <th>Network</th>
                        <th>Status</th>
                        <th>Reason</th>
                        <th>Waktu Terkirim</th>
                        <th>Waktu Selesai</th>
                        <th>Waktu Soft Delete</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($verificationCodes as $verificationCode)
                        <tr>
                            <td>{{ $verificationCode->destination ?? $verificationCode->phone_number }}</td>
                            <td>{{ $verificationCode->via->description ?? 'SMS' }}</td>
                            <td>{{ $verificationCode->code }}</td>
                            <td>{{ $verificationCode->created_at }}</td>
                            <td>{{ $verificationCode->expired_at ?? '-' }}</td>
                            <td>{{ $formatCategoryCaller->call($verificationCode) }}</td>
                            <td>{{ $verificationCode->delivery_report->network ?? '-' }}</td>
                            <td>{{ $formatStatusCaller->call($verificationCode) }}</td>
                            <td>{{ $verificationCode->delivery_report->reason ?? '-' }}</td>
                            <td>{{ $verificationCode->delivery_report->send_at ?? '-' }}</td>
                            <td>{{ $verificationCode->delivery_report->done_at ?? '-' }}</td>
                            <td>{{ $verificationCode->deleted_at ?? '-' }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            {{ $verificationCodes->appends(Request::except('page'))->links() }}
        </div>
    </div>

@stop

@section('script')

@endsection