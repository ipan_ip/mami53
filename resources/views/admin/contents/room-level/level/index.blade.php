@extends('admin.layouts.main')

@section('content')
	<div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default form-inline">
                    <div class="col-md-6 bg-default" style="padding-bottom:10px;">
                        <a href="{{ URL::to('admin/kost-level/room-level/create#kost-level') }}" class="btn btn-primary">
                            <i class="fa fa-plus">&nbsp;</i>Add Room Level
                        </a>
                    </div>
                    <div class="col-md-6 bg-default" style="padding-bottom:10px;text-align:right;">
                        <form action="" method="GET">
                            <input type="text" name="q" class="form-control"  placeholder="Level Name"  autocomplete="off" value="{{ request()->input('q') }}">
                            <button type="submit" class="btn btn-primary" id="buttonSearch">
                                <i class="fa fa-search">&nbsp;</i>Search
                            </button>
                        </form>
                    </div>
                </div>
            </div>

            <table  class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Level Name</th>
                        <th>Key</th>
                        <th>Status</th>
                        <th>Charging Name</th>
                        <th>Charging Fee</th>
                        <th>Charging Type</th>
                        <th>Charge for Booking Contract</th>
                        <th>Charge for Consultant Contract</th>
                        <th>Charge for Owner Contract</th>
                        <th>Invoice Type</th>
                        <th>Notes</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($level as $levelItem)
                    <tr>
                        <td>{{ $levelItem->id }}</td>
                        <td>
                            {{ $levelItem->name }}
                            @if ($levelItem->is_regular)
                                &nbsp;<span class="label label-success">Regular</span>
                            @endif
                        </td>
                        <td>
                            {{ $levelItem->key }}
                        </td>
                        <td>@if ($levelItem->is_hidden) Hide @else Show @endif</td>
                        <td>{{ $levelItem->charging_name ?? '' }}</td>
                        <td>{{ $levelItem->charging_fee }}</td>
                        <td>{{ $chargingTypeOptions[$levelItem->charging_type] }}</td>
                        <td>@if($levelItem->charge_for_booking) Yes @else No @endif</td>
                        <td>@if($levelItem->charge_for_consultant) Yes @else No @endif</td>
                        <td>@if($levelItem->charge_for_owner) Yes @else No @endif</td>
                        <td>{{ $chargeInvoiceTypeOptions[$levelItem->charge_invoice_type] }}</td>
                        <td>{{ $levelItem->notes }}</td>
                        <td>
                            <a href="{{ URL::to('admin/kost-level/room-level/' . $levelItem->id . '/edit#kost-level') }}" title="Edit">
                                <i class="fa fa-pencil"></i>
                            </a>
                            <form action="{{ URL::to('admin/kost-level/room-level/' . $levelItem->id) }}" method="POST" style="display: inline;">
                                <input type="hidden" name="_method" value="DELETE">
                                <a href="#" onclick="if(!confirm('Are you sure want to delete level {{ $levelItem->name }}?')) return false; else $(this).parent().submit();" title="Delete">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </form>
                        </td>
                    </tr> 
                @endforeach
                </tbody>
            </table>
        </div>

        <div class="box-body no-padding">
            <div class="col-md-12 bg-default">
                {{ $level->appends(Request::except('page'))->links() }}
            </div>
        </div>
    </div>

    <script>
        
    </script>
@endsection
