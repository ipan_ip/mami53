@extends('admin.layouts.main')

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

        @php
            $levelId = '';
            if (!empty($level)) {
                $levelId = $level->id;
            }
        @endphp
        <form action="{{ URL::to('admin/kost-level/room-level/' . $levelId) }}" method="POST" class="form-horizontal form-bordered">
            @if (!empty($level))
            <input type="hidden" name="_method" value="PUT">
            @endif

            <div class="box-body no-padding">
                <div class="form-group bg-default">
                    <label for="level-name" class="control-label col-sm-2">Level Name</label>
                    <div class="col-sm-10">
                        @php
                            $levelName = '';
                            if(!empty(old('name'))) {
                                $levelName = old('name');
                            } elseif (!empty($level)) {
                                $levelName = $level->name;
                            }
                        @endphp
                        <input type="text" name="name" id="level-name" class="form-control" value="{{ $levelName }}">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="level-key" class="control-label col-sm-2">Key</label>
                    <div class="col-sm-10">
                        @php
                            $levelKey = null;
                            if(!empty(old('key'))) {
                                $levelKey = old('key');
                            } elseif (!empty($level)) {
                                $levelKey = $level->key;
                            }
                        @endphp
                        <input type="text" name="key" id="level-key" class="form-control" value="{{ $levelKey }}">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="level-order" class="control-label col-sm-2">Order</label>
                    <div class="col-sm-2">
                        @php
                            $levelOrder = 0;
                            if(!empty(old('order'))) {
                                $levelOrder = old('order');
                            } elseif (!empty($level)) {
                                $levelOrder = $level->order;
                            }
                        @endphp
                        <select name="order" id="level-order" class="form-control">
                            @foreach ($order as $availOrder)
                            <option value="{{ $availOrder }}" @if ($levelOrder == $availOrder) selected="true" @endif>{{ $availOrder }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                @if ($isRegularAvail || !empty($level))
                <div class="form-group bg-default">
                    <label for="level-regular" class="control-label col-sm-2">Is Regular Level</label>
                    <div class="col-sm-2">
                        @php
                            $levelRegular = '0';
                            if(!empty(old('is_regular'))) {
                                $levelRegular = old('is_regular');
                            } elseif (!empty($level)) {
                                $levelRegular = $level->is_regular;
                            }
                        @endphp
                        <select name="is_regular" id="level-regular" class="form-control">
                            <option value="0" @if ($levelRegular == '0') selected="true" @endif>No</option>
                            <option value="1" @if ($levelRegular == '1') selected="true" @elseif (!$isRegularAvail) disabled @endif>Yes</option>
                        </select>
                    </div>
                </div>
                @else
                <input type="hidden" name="is_regular" value="0">
                @endif

                <div class="form-group bg-default">
                    <label for="level-hidden" class="control-label col-sm-2">Status</label>
                    <div class="col-sm-2">
                        @php
                            $levelHidden = '0';
                            if(!empty(old('is_hidden'))) {
                                $levelHidden = old('is_hidden');
                            } elseif (!empty($level)) {
                                $levelHidden = $level->is_hidden;
                            }
                        @endphp
                        <select name="is_hidden" id="level-hidden" class="form-control">
                            <option value="0" @if ($levelHidden == '0') selected="true" @endif>Show</option>
                            <option value="1" @if ($levelHidden == '1') selected="true" @endif>Hidden</option>
                        </select>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="level-notes" class="control-label col-sm-2">Notes</label>
                    <div class="col-sm-10">
                        @php
                            $levelNotes = '';
                            if(!empty(old('notes'))) {
                                $levelNotes = old('notes');
                            } elseif (!empty($level)) {
                                $levelNotes = $level->notes;
                            }
                        @endphp
                        <input type="text" name="notes" id="level-notes" class="form-control" value="{{ $levelNotes }}">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="level-fee-name" class="control-label col-sm-2">Charging Name</label>
                    <div class="col-sm-2">
                        <input type="text" name="charging_name" id="level-charging-name" class="form-control" value="{{$level->charging_name ?? ''}}">
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="level-fee" class="control-label col-sm-2">Charging Fee</label>
                    <div class="col-sm-2">
                        @php
                            $levelChargingFee = '0';
                            if(!empty(old('charging_fee'))) {
                                $levelChargingFee = old('charging_fee');
                            } elseif (!empty($level)) {
                                $levelChargingFee = $level->charging_fee;
                            }
                        @endphp
                        <input step="any" type="number" min="0" name="charging_fee" id="level-count" class="form-control" value="{{ (float) $levelChargingFee }}">
                    </div>
                </div>
                
                <div class="form-group bg-default">
                    <label for="level-fee-type" class="control-label col-sm-2">Charging Type</label>
                    <div class="col-sm-2">
                        @php
                            $levelCharginType = 'percentage';
                            if(!empty(old('charging_type'))) {
                                $levelCharginType = old('charging_type');
                            } elseif (!empty($level)) {
                                $levelCharginType = $level->charging_type;
                            }
                        @endphp
                        <select name="charging_type" class="form-control">
                            @foreach ($chargingTypeOptions as $key => $value)
                                <option value="{{ $key }}"  @if($levelCharginType == $key) selected="true" @endif>{{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="level-charging-rule" class="control-label col-sm-2">Charging Rules For</label>
                    <div class="col-sm-6">
                        @php
                            $chargeForBooking = false;
                            $chargeForConsultant = false;
                            $chargeForOwner = false;
                            if (!empty(old('charge_for_booking')) || !empty(old('charge_for_consultant')) || !empty(old('charge_for_owner'))) {
                                $chargeForBooking = old('charge_for_booking');
                                $chargeForConsultant = old('charge_for_consultant');
                                $chargeForOwner = old('charge_for_owner');
                            } elseif (!empty($level)) {
                                $chargeForBooking = $level->charge_for_booking;
                                $chargeForConsultant = $level->charge_for_consultant;
                                $chargeForOwner = $level->charge_for_owner;
                            }
                        @endphp
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" name="charge_for_booking" type="checkbox" value=1 @if($chargeForBooking) checked @endif>
                            <label class="form-check-label">Booking Contract</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" name="charge_for_consultant" type="checkbox" value=1 @if($chargeForConsultant) checked @endif>
                            <label class="form-check-label">Consultant Contract</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" name="charge_for_owner" type="checkbox" value=1 @if($chargeForOwner) checked @endif>
                            <label class="form-check-label">Owner Contract</label>
                        </div>
                    </div>
                </div>
                
                <div class="form-group bg-default">
                    <label for="level-invoice-type" class="control-label col-sm-2">Charging Invoice Type</label>
                    <div class="col-sm-2">
                        @php
                            $levelChargeInvoiceType = 'all';
                            if(!empty(old('charge_invoice_type'))) {
                                $levelChargeInvoiceType = old('charge_invoice_type');
                            } elseif (!empty($level)) {
                                $levelChargeInvoiceType = $level->charge_invoice_type;
                            }
                        @endphp
                        <select name="charge_invoice_type" class="form-control">
                            @foreach ($chargeInvoiceTypeOptions as $key => $value)
                                <option value="{{ $key }}" @if($levelChargeInvoiceType == $key) selected="true" @endif>{{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <div class="col-sm-10 col-sm-push-2">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <link rel="stylesheet" href="/assets/vendor/summernote/summernote.css?v=05042018">
    <script src="/assets/vendor/summernote/summernote.min.js"></script>
@endsection
