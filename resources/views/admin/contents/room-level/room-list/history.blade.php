@extends('admin.layouts.main')

@section('content')
	<div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default form-inline">
                    <div class="col-md-12 bg-default" style="padding-bottom:10px;">
                        <a href="{{ route('admin.room-list.index', ['id' => $room->song_id]). '#kost-level' }}" class="btn btn-primary">
                            <i class="fa fa-arrow-left">&nbsp;</i>Back to Room List
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="jumbotron" style="padding:15px;margin-bottom:15px;">
                    @php
                        $userOwner = optional($room->owners->first())->user;
                        $ownerName = '-';
                        if (!is_null($userOwner)) {
                            $ownerName = $userOwner->name;
                        }
                    @endphp
                    <span>Owner: <b>{{ $ownerName }}</b></span><br />
                    <span>Kost: {{ $room->name }}</span><br />
                    <span>Room: {{ $roomUnit->name . '/' . $roomUnit->floor }}</span><br />
                </div>
            </div>

            <table  class="table table-striped">
                <thead>
                    <tr>
                        <th>Log</th>
                        <th>Actor</th>
                        <th>Created</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($levelHistories as $historyItem)
                        <tr>
                            @php
                                if (is_null($historyItem->room_level)) {
                                    continue;
                                }
                                $log = ucfirst($historyItem->action) . ' to ' . $historyItem->room_level->name;
                            @endphp
                            <td>{{ $log }}</td>

                            @php
                                $actor = $historyItem->user;
                                if (!is_null($actor)) {
                                    $actorName = $actor->name;
                                    if ($actorName == $ownerName) {
                                        $actorName = 'Owner';
                                    }
                                } else {
                                    $actorName = 'System';
                                }
                            @endphp
                            <td>{{ $actorName }}</td>

                            <td>{{ $historyItem->created_at }}</td>
                        </tr> 
                    @endforeach
                </tbody>
            </table>
        </div>

        <div class="box-body no-padding">
            <div class="col-md-12 bg-default">
                {{ $levelHistories->appends(Request::except('page'))->links() }}
            </div>
        </div>
    </div>
@endsection
