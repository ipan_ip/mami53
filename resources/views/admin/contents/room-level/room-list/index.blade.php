@extends('admin.layouts.main')

@section('content')
	<div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default form-inline">
                    <div class="col-md-6 bg-default">
                        <a href="#" class="btn btn-primary"
                            data-toggle="modal"
                            data-target="#popup-assign-all">
                            <i class="fa fa-plus-square-o">&nbsp;</i>Assign All
                        </a>
                    </div>
                    <div class="col-md-6 bg-default" style="padding-bottom:10px;text-align:right;">
                        <form action="" method="GET">
                            <input type="text" name="room-name" class="form-control"  placeholder="Room Name"  autocomplete="off" value="{{ request()->input('room-name') }}">
                            <select name="level-id" class="form-control">
                                <option value="0">All Level</option>
                                <option value="-1" @if (request()->input('level-id') == -1) selected="true" @endif>Unassigned</option>
                                @foreach ($roomLevelList as $levelItem)
                                    <option value="{{ $levelItem->id }}" @if (request()->input('level-id') == $levelItem->id) selected="true" @endif>{{ $levelItem->name }}</option>
                                @endforeach
                            </select>
                            <button type="submit" class="btn btn-primary" id="buttonSearch">
                                <i class="fa fa-search">&nbsp;</i>Search
                            </button>
                        </form>
                    </div>
                </div>
            </div>

            <table  class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Room Name</th>
                        <th>Floor</th>
                        <th>Occupied</th>
                        <th>Level</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($roomUnits as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->name }}</td>
                        <td>{{ $item->floor }}</td>
                        <td>@if($item->occupied) Yes @else No @endif</td>
                        @php
                            $level = $item->level;
                            if (empty($level)) {
                                $roomLevelName = 'Unassigned';
                            } else {
                                $roomLevelName = $level->name;
                            }
                        @endphp
                        <td>{{ $roomLevelName }}</td>
                        <td>
                            <a href="{{ route('admin.room-list.edit', ['id' => $room->song_id, 'roomId' => $item->id]). '#kost-level' }}" title="Edit Room Level">
                                <i class="fa fa-pencil"></i>
                            </a>

                            &nbsp;
                            <a href="{{ route('admin.room-list.history', ['id' => $room->song_id, 'roomId' => $item->id]). '#kost-level' }}" title="History">
                                <i class="fa fa-history"></i>
                            </a>
                        </td>
                    </tr> 
                @endforeach
                </tbody>
            </table>
        </div>

        <div class="box-body no-padding">
            <div class="col-md-12 bg-default">
                {{ $roomUnits->appends(Request::except('page'))->links() }}
            </div>
        </div>
    </div>

    {{-- Popup Assign All --}}
    <div class="modal fade" id="popup-assign-all">
        <div class="modal-dialog">
            <form method="POST" action="{{ route('admin.room-list.assignall', [ 'id' => $room->song_id ]) }}" class="modal-content" enctype="multipart/form-data">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Assign All Rooms</h4>
                </div>
                <div class="modal-body">

                    <div class="form-group row">
                        <label for="level-id" class="control-label col-sm-3">Level</label>
                        <div class="col-sm-9">
                            <select name="room_level_id" id="level-id" class="form-control">
                                @foreach ($roomLevelList as $levelItem)
                                <option value="{{ $levelItem->id }}">{{ $levelItem->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
    
@endsection
