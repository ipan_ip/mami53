@extends('admin.layouts.main')

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

        <form action="{{ route('admin.room-list.update', [ 'id' => $room->song_id, 'roomId' => $roomUnit->id ]) }}" method="POST" class="form-horizontal form-bordered">
            <input type="hidden" name="_method" value="PUT">
            
            <div class="box-body no-padding">
                <div class="form-group bg-default">
                    <label for="room-name" class="control-label col-sm-2">Room Name</label>
                    <div class="col-sm-10">
                        <input type="text" id="room-name" class="form-control" value="{{ $roomUnit->name }}" disabled>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <label for="level-id" class="control-label col-sm-2">Level</label>
                    <div class="col-sm-10">
                        <select name="room_level_id" id="level-id" class="form-control">
                            @foreach ($roomLevelList as $levelItem)
                            <option value="{{ $levelItem->id }}" @if (old('room_level_id', optional($roomUnit)->room_level_id) == $levelItem->id) selected="true" @endif>{{ $levelItem->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group bg-default">
                    <div class="col-sm-10 col-sm-push-2">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
