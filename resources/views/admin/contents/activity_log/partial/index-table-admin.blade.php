<table id="tableListDesigner" class="table">
    <thead>
        <tr>
            <th class="text-center" width="10%">ID</th>
            <th class="text-center" width="10%">User</th>
            <th class="text-center" width="10%">IP</th>
            <th class="text-center" width="10%">Action</th>
            <th class="text-center">Description</th>
            <th class="text-center" width="10%">Target Type</th>
            <th class="text-center" width="10%">Target ID</th>
            <th class="text-center" width="10%">Time</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($rowsLog as $index => $activity)
            <tr>
                <td class="text-center" style="padding-left:10px; font-size:14px;">
                    <small>{{ $activity['id'] }}</small>
                </td>

                <td class="text-center" onmouseenter="$('{{ "#tooltip_$index" }}').css('visibility', 'visible');" onmouseleave="$('{{ "#tooltip_$index" }}').css('visibility', 'hidden');">
                    {{ $activity['user_name'] }}
                    <span id="{{ "tooltip_$index" }}" class="tooltiptext">{{ $activity['causer_id'] }}</span>
                </td>
                    
                <td class="text-center" style="font-size: 14px;">
                    @if(empty($activity['ip']) === false)
                        @if (\App\Libraries\RequestHelper::getIsPrivateIp($activity['ip']))
                            <p>
                                <a href="javascript:alert('It\'s private IP.')">{{ $activity['ip'] }}</a>
                            </p>
                        @else
                            <p>
                                <a href="https://iplocation.com/?ip={{ $activity['ip'] }}" target="_blank">{{ $activity['ip'] }}</a>
                            </p>
                        @endif
                    @endif
                </td>

                <td class="text-center" style="font-size: 14px;">
                    <strong>{{ $activity['log_name'] }}</strong>
                </td>

                <td class="text-center" style="font-size: 14px;">
                    <p>{{ $activity['description'] }}</p>
                </td>

                <td class="text-center" style="font-size: 14px;">
                    <p>{{ $activity['subject_type'] }}</p>
                </td>
                
                <td class="text-center" style="font-size: 14px;">
                    <p>{{ $activity['subject_id'] }}</p>
                </td>

                <td style="font-size: 14px;">
                    <p>{{ $activity['created_at'] }}</p>
                </td>
            </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th class="text-center" width="10%">ID</th>
            <th class="text-center" width="10%">User</th>
            <th class="text-center" width="10%">IP</th>
            <th class="text-center" width="10%">Action</th>
            <th class="text-center">Description</th>
            <th class="text-center" width="10%">Target Type</th>
            <th class="text-center" width="10%">Target ID</th>
            <th class="text-center" width="10%">Time</th>
        </tr>
    </tfoot>
</table>
