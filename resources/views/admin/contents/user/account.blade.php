@extends('admin.layouts.main')

@section('style')
<link rel="stylesheet" href="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.css">
<link rel="stylesheet" href="/css/admin-consultant.css">
@endsection

@section('content')
<div class="box box-default">

	<!-- box-header -->
	<div class="box-header" style="padding-top: 10px;">
		<div class="col-md-6">
			<h3 class="box-title" style="padding-left: 0;"><i class="fa fa-user"></i>&nbsp;Users</h3>
		</div>
	</div>
	<!-- /.box-header -->

	<div class="box-body no-padding no-overflow">
		<div class="horizontal-wrapper">
			<div style="padding: 15px;" id="user-account">

				<div class="btn-horizontal-group bg-default" style="margin-bottom: 20px;">
					<form action="" class="form-inline" id="filter-account">
						<input type="text" name="user_id" class="form-control input-sm" placeholder="User ID" autocomplete="off" value="">
						<input type="text" name="name" class="form-control input-sm" placeholder="Name" autocomplete="off" value="">
						<input type="text" name="phone_number" class="form-control input-sm" placeholder="Phone Number" autocomplete="off" value="">
						<input type="email" name="email" class="form-control input-sm" placeholder="Email" autocomplete="off" value="" style="margin-right:10px">
						<button class="btn btn-primary btn-md" id="buttonSearch" disabled="">
							<i class="fa fa-search">&nbsp;</i>Search
						</button>
					</form>
				</div>

				<table>
					<thead>
						<tr>
							<th data-field="id">User ID</th>
							<th data-field="name">Name</th>
							<th data-field="phone_number">Phone Number</th>
							<th data-field="email">Email</th>
							<th data-field="created_at">Created At</th>
							<th data-formatter="jobWorkplaceFormatter">Job & Workplace</th>
							<th data-field="birthday">Birthday Date</th>
							<th data-field="gender">Gender</th>
							<th data-field="is_owner" data-formatter="yesNoFormatter">Is Owner</th>
							<th data-field="is_verify_phone_number" data-formatter="yesNoFormatter">Is Verify Phone</th>
							<th data-field="is_verify_email" data-formatter="yesNoFormatter">Is Verify Email</th>
							<th data-field="identity_card" data-formatter="yesNoFormatter">Is Verify KTP</th>
							<th data-field="is_tester" data-formatter="yesNoFormatter">Is Tester</th>
							<th data-field="register_from">Register With</th>
							@permission('user-account-delete')
								<th data-formatter="actionFormatter">Action</th>
							@endpermission
						</tr>
					</thead>
				</table>

			</div>
		</div>
	</div>

</div>
@endsection

@section('script')
<script src="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

<script>
	function jobWorkplaceFormatter(value, row) {
		if (!row.job && !row.workplace) {
			return `-`;
		} else {
			return `${row.job || '-'}, ${row.workplace || '-'}`;
		}
	};

	function actionFormatter(value, row) {
		if (row.role === 'administrator') {
			return `<button class="btn btn-danger" onclick="deleteAdminAccount(${row.id})">Delete</button>`;
		}
	}

	function deleteAdminAccount(id) {
		Swal.fire({
			title: 'Are you sure?',
			text: "You won't be able to revert this!",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, delete it!'
		}).then((result) => {
			if (result.isConfirmed) {
				let deleteAdmin = axios.post(`/admin/account/${id}`, {'_method': 'DELETE'});

				deleteAdmin.then((response) => {
					if (response.data.success) {
						Swal.fire(
							'Deleted!',
							'Akun admin berhasil dihapus.',
							'success'
						);

						window.location.reload();
					} else {
						Swal.fire(
							'Failed!',
							'Akun non admin tidak bisa dihapus.',
							'error'
						);
					}
				});
			}
		});
	}

	function yesNoFormatter(value, row) {
		if (value == 1 || value === 'true') {
			return 'yes';
		} else {
			return 'no';
		}
	};

	$(function() {
		const $table = $('#user-account table');
		const $inputs = $('#filter-account input');
		const $searchButton = $('#buttonSearch');

		$searchButton.prop('disabled', true);

		$table.bootstrapTable({
			url: '',
			dataField: 'rows',
			pagination: true,
			pageSize: 15,
			pageList: [],
			sidePagination: 'server'
		});


		$inputs.on('input', function() {
			$inputs.each(function() {
				if ($(this).val()) {
					$searchButton.prop('disabled', false);
					return false;
				}
				$searchButton.prop('disabled', true);
			});
		});

		$('#filter-account').on('submit', function(e) {
			e.preventDefault();

			const queryArray = $(this).serializeArray();

			let checker = '';

			queryArray.forEach(function(object) {
				checker += object.value;
			});

			if (!!checker) {
				const queryString = $(this).serialize();

				$table.bootstrapTable('refreshOptions', {
					url: `/admin/account/data?${queryString}`,
					pageNumber: 1
				});
			} else {
				alert('Please filled one of the input field first');
			}
		});

	});
</script>
@endsection
