@extends('admin.layouts.main')

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div>

        <form action="{{ URL::route('admin.account.update-password') }}" method="POST" class="form-horizontal form-bordered">
            <div class="box-body no-padding">
                <div class="form-group bg-default">
                    <label for="password" class="control-label col-sm-2">New Password</label>
                    <div class="col-sm-10">
                        <input type="password" name="password" class="form-control" value="{{ old('password') }}">
                    </div>
                </div>
                <div class="form-group bg-default">
                    <label for="password-confirmation" class="control-label col-sm-2">New Password Confirmation</label>
                    <div class="col-sm-10">
                        <input type="password" name="password_confirmation" class="form-control" value="{{ old('password_confirmation') }}">
                    </div>
                </div>
                

                <div class="form-group bg-default">
                    <div class="col-sm-10 col-sm-push-2">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
