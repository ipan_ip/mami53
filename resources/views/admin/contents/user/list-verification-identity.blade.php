@extends('admin.layouts.main')
@inject('UserMedia', 'App\Entities\User\UserMedia')
@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
            <div class="box-tools pull-right">
                <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <table id="tableListUser" class="table table-striped">
                <form action="" method="GET">
                    <thead>
                        <tr>
                            <td colspan="7">
                                <div class="input-group">
                                    <input type="text" name="q" class="form-control input-md"
                                        placeholder="Ketik User Profile atau Email" autocomplete="off"
                                        value="{{ request()->input('q') }}"
                                        style="width:200px;margin-left:20px;">
                                </div>
                            </td>
                            <td colspan="3">
                                <div style="padding-top:5px;margin-right:10px;" class="pull-right">
                                    <select class="pull-right-sort form-group btn btn-primary selector" name="per_page" width="25px">
                                        @php $selected = 'selected="true"'; @endphp
                                        @for ($i = 10; $i <= 20; $i++)
                                            <option
                                                @if (
                                                        !empty(app('request')->input('per_page'))
                                                        && app('request')->input('per_page') == $i
                                                    )
                                                        {{ $selected}}
                                                @endif
                                                value="{{$i}}">{{$i}}</option>
                                        @endfor
                                    </select> records per page
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th width="30px;">No</th>
                            <th width="30px;">Submit Date
                                <input type="text" name="from_submit_date" class="form-control input-sm datepicker" placeholder="Start" value="{{ request()->input('from_submit_date') }}" autocomplete="off"/>
                                <input type="text" name="to_submit_date" class="form-control input-sm datepicker" placeholder="End" value="{{ request()->input('to_submit_date') }}" autocomplete="off"/>
                            </th>
                            <th width="30px;">Verified Date
                                <input type="text" name="from_verification_date" class="form-control input-sm datepicker" placeholder="Start" value="{{ request()->input('from_verification_date') }}" autocomplete="off"/>
                                <input type="text" name="to_verification_date" class="form-control input-sm datepicker" placeholder="End" value="{{ request()->input('to_verification_date') }}" autocomplete="off"/>
                            </th>
                            <th width="30px;">Owner Data</th>
                            <th width="30px;">Preview</th>
                            <th width="30px;">Identity Type
                                <select class="form-control input-sm" name="identity_type" data-selected="{{ request()->input('identity_type') }}">
                                    <option value="">-</option>
                                    @foreach (__('api.user-verification.options') as $key => $detail)
                                        <option value="{{ $key }}">{{ $detail['subtitle'] ?? $detail['title'] ?? $key }}</option>
                                    @endforeach
                                </select>
                            </th>
                            <th width="30px;">Upload From
                                <select class="form-control input-sm" name="upload_from" data-selected="{{ request()->input('upload_from') }}">
                                    <option value="">-</option>
                                    <option value="booking">Booking</option>
                                    <option value="user_profile">User Profile</option>
                                </select>
                            </th>
                            <th width="30px;">Is Owner
                                <select class="form-control input-sm" name="is_owner" data-selected="{{ request()->input('is_owner') }}">
                                    <option value="">-</option>
                                    <option value="true">TRUE</option>
                                    <option value="false">FALSE</option>
                                </select>
                            </th>
                            <th width="30px;">Status
                                <select class="form-control input-sm" name="status" data-selected="{{ request()->input('status') }}">
                                    <option value="">-</option>
                                    <option value="waiting">Waiting</option>
                                    <option value="verified">Verified</option>
                                    <option value="rejected">Rejected</option>
                                    <option value="cancelled">Unverified</option>
                                </select>
                            </th>
                            <th width="30px;">Action
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-search"></i>
                                </button>
                            </th>
                        </tr>
                    </thead>
                </form>
                <tbody>
                    @foreach ($datas as $index => $data)
                        @if (!$data->user || !$data->user_medias)
                            @continue
                        @endif
                        @php
                            // So far only needed small and large
                            $photoIdentity = ['small' => '', 'large' => ''];
                            $photoSelfieIdentity = ['small' => '', 'large' => ''];
                            $type = "-";
                            $uploadFrom = "-";
                            foreach ($data->user_medias as $photo) {
                                if ($photo->type) {
                                    if ($photo->description == "photo_identity" || $photo->description == "photo_ktp") {
                                        $photoIdentity = $photo->getMediaUrl();
                                    }
                                    if ($photo->description == "selfie_identity" || $photo->description == "selfie_ktp") {
                                        $photoSelfieIdentity = $photo->getMediaUrl();
                                    }

                                    $uploadFrom = $photo->from;
                                    $type = $photo->type;
                                }
                            }

                            // Prettify the upload form and type
                            switch ($uploadFrom) {
                                case 'booking':
                                    $uploadFrom = "Booking";
                                    break;
                                case 'user_profile':
                                    $uploadFrom = "User Profile";
                                    break;
                            }
                            $translatedTypeOptions = __('api.user-verification.options');
                            $type = $translatedTypeOptions[$type]['subtitle'] ?? $translatedTypeOptions[$type]['title'] ?? $type;
                        @endphp
                        <tr>
                            <td>{{ $index+1 }}</td>
                            <td>{{ $data->created_at->format("d M Y H:i") }}</td>
                            <td>
                                @if ($data->identity_card == $data::IDENTITY_CARD_STATUS_VERIFIED)
                                    {{ $data->updated_at->format("d M Y H:i") }}
                                @else
                                    -
                                @endif
                            </td>
                            <td>{{ $data->user->name }} | {{ $data->user->phone_number }} | {{ $data->user->email }}</td>
                            <td>
                                KTP:
                                <a href="javascript:showLargePhoto('{{ $photoIdentity['large'] }}');">
                                    <img onerror="onPhotoError(this);" src="{{ $photoIdentity['small'] }}" alt="">
                                </a>
                                <br>
                                Selfie:
                                <a href="javascript:showLargePhoto('{{ $photoSelfieIdentity['large'] }}');">
                                    <img onerror="onPhotoError(this);" src="{{ $photoSelfieIdentity['small'] }}" alt="">
                                </a>
                            </td>
                            <td>{{ $type }}</td>
                            <td>{{ $uploadFrom }}</td>
                            <td>{{ strtoupper($data->user->is_owner) }}</td>
                            <td>
                                @if ($data->identity_card == $data::IDENTITY_CARD_STATUS_VERIFIED)
                                    <span class="label label-success">Approved <i class="fa fa-check"></i>
                                @elseif ($data->identity_card == $data::IDENTITY_CARD_STATUS_REJECTED)
                                    <span class="label label-danger">Rejected</span>
                                @elseif ($data->identity_card == $data::IDENTITY_CARD_STATUS_CANCELLED)
                                    <span class="label label-primary">Unverified</span>
                                @else
                                    <span class="label label-warning">Waiting</span>
                                @endif
                            </td>                                
                            <td>
                                <a href="#" onclick="openPreviewModal('{{ $photoIdentity['large'] }}', '{{ $photoSelfieIdentity['large'] }}')" title="View">
                                    <i class="fa fa-eye" style="font-size: 20px; "></i>
                                </a>
                                <form action="/admin/account/verification-identity-card/{{ $data->id }}" method="post">
                                    @if ($data->identity_card == $data::IDENTITY_CARD_STATUS_VERIFIED)
                                        <input type="hidden" name="status" value="{{ $data::IDENTITY_CARD_STATUS_CANCELLED }}">
                                        <a href="#" onclick="$(this).parent('form').submit();" title="Unverify">
                                            <i class="fa fa-warning" style="font-size: 20px; color:red; padding-left: 5px"></i>
                                        </a>
                                    @elseif ($data->identity_card == $data::IDENTITY_CARD_STATUS_WAITING)
                                        <input type="hidden" name="status" value="{{ $data::IDENTITY_CARD_STATUS_VERIFIED }}">
                                        <a href="#" onclick="$(this).parent('form').submit();" title="Verify">
                                            <i class="fa fa-check" style="font-size: 20px; color:green; padding-left: 5px"></i>
                                        </a>
                                        <a href="#" onclick="openRejectionForm('/admin/account/verification-identity-card/{{ $data->id }}', '{{ $data::IDENTITY_CARD_STATUS_REJECTED }}')" title="Reject">
                                            <i class="fa fa-close" style="font-size: 20px; color:red; padding-left: 5px"></i>
                                        </a>
                                    @endif
                                </form>
                            </td>
                        </tr>
                    @endforeach

                </tbody>
                <tfoot>
                    <tr>
                        <th width="30px;">No</th>
                        <th width="30px;">Submit Date</th>
                        <th width="30px;">Verified Date</th>
                        <th width="30px;">Owner Data</th>
                        <th width="30px;">Preview</th>
                        <th width="30px;">Identity Type</th>
                        <th width="30px;">Upload From</th>
                        <th width="30px;">Is Owner</th>
                        <th width="30px;">Status</th>
                        <th width="30px;">Action</th>
                    </tr>
                </tfoot>
            </table>
        </div><!-- /.box-body -->
        <div class="box-body no-padding">
            {{ $datas->appends(Request::except('page'))->links() }}
        </div>
    </div><!-- /.box -->
    <!-- table -->
@stop
@section('script')
<!-- page script -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script type="text/javascript">
    const FALLBACK_PHOTO_URL = '/assets/logo/mamikos_header_logo_default.png';

    $(function() {
        // Select the previously choosen filter.
        $('#tableListUser select').each(function (idx, selectObj) {
            var selectedVal = $(selectObj).data('selected');
            if  (selectedVal != undefined) {
                $(selectObj).find(`option[value="${selectedVal}"]`).prop('selected', true);
            }
        });

        // Format the datepicker
        $('.datepicker').datepicker({ dateFormat : 'dd-mm-yy'});
    })

    // Fallbacking photo error to mamikos image
    function onPhotoError(object) {
        $(object).onerror = null;
        $(object).attr('src', FALLBACK_PHOTO_URL);
    }

    function openRejectionForm(actionUrl, status) {
        Swal.fire({
            title: 'Rejection Form',
            html: `
            <form action="${actionUrl}" method="POST" >
                <input type="hidden" name="status" value="${status}">
                <div class="form-group">
                    <label>Reject Reason</label>
                    <select class="form-control input-sm" onChange="onChangeRejectionOption(this); return false;">
                        <option value="Data Tidak Valid"> Data Tidak Valid </option>
                        <option value="Gambar Tidak Jelas"> Gambar Tidak Jelas </option>
                        <option value="Gambar Ditolak"> Gambar Ditolak </option>
                        <option> Lainnya </option>
                    </select>
                    <br>
                    <textarea style="display:none" class="form-control input-sm" rows="5" cols="40" maxlength="200" ></textarea>
                </div>
                <div class="form-group">
                    <input class="btn btn-danger" type="submit" value="Submit Rejection" />
                </div>
            </form>
            `,
            showConfirmButton: false,
        });
    }

    function onChangeRejectionOption(object) {
        // Last item is other / 'lainnya'
        var isLastItemSelected = object.selectedIndex == object.options.length - 1;
        console.log($(object).siblings('textarea'), isLastItemSelected)
        if (isLastItemSelected) {
            $(object).removeAttr('name');
            $(object).siblings('textarea').show();
            $(object).siblings('textarea').attr('name', 'reject_reason');
        } else {
            $(object).attr('name', 'reject_reason');
            $(object).siblings('textarea').hide();
        }
    }

    function showLargePhoto(url) {
        Swal.fire({
            width: '100%',
            html: `
                <img src='${url}' onerror='onPhotoError(this);'>
            `,
        })
    }

    function openPreviewModal(photoIdentityUrl, photoSelfieIdentityUrl) {
        Swal.fire({
            width: '100%',
            html: `
                <div class="box box-primary">
                    <h3 class="text-center">Identity Card</h3>
                    <center>
                        <img src='${photoIdentityUrl}' onerror='onPhotoError(this);'>
                    </center>
                </div>
                <div class="box box-primary">
                    <h3 class="text-center">Selfie with Identity Card</h3>
                    <center>
                        <img src='${photoSelfieIdentityUrl}' onerror='onPhotoError(this);'>
                    </center>
                </div>
            `,
        })
    }
</script>
@stop