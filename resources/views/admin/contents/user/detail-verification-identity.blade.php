@extends('admin.layouts.main')
@section('content')
    <div class="box-body no-padding">
        <section class="content">
            <div class="row">
                <div class="col-md-4">
                    <div class="box box-primary">
                        <div class="box-body box-profile">
                            <h3 class="profile-username text-center">{{ $user->name }}</h3>
                            <ul class="list-group list-group-unbordered">
                                <li class="list-group-item">
                                    <b>Email<div class="pull-right">{{ $user->email }}</div></b> 
                                </li>
                                <li class="list-group-item">
                                    <b>Phone Number<div class="pull-right">{{ $user->phone_number }}</div></b> 
                                </li>
                                <li class="list-group-item">
                                    <b>Upload From<div class="pull-right">{{ $uploadFrom }}</div></b> 
                                </li>
                                <li class="list-group-item">
                                    <b>Identity Type<div class="pull-right">{{ $type }}</div></b> 
                                </li>
                                <li class="list-group-item">
                                    @if ($userRequest->identity_card == "verified")
                                        <b>Status<div class="pull-right"><span class="label label-success" style="font-size:15px">Verified</span></div></b> 
                                    @elseif ($userRequest->identity_card == "waiting")
                                        <b>Status<div class="pull-right"><span class="label label-warning" style="font-size:15px">Waiting</span></div></b> 
                                    @elseif ($userRequest->identity_card == "rejected")
                                        <b>Status<div class="pull-right"><span class="label label-danger" style="font-size:15px">Rejected</span></div></b> 
                                    @else
                                        -
                                    @endif
                                </li>
                                <li class="list-group-item">
                                    @if ($userRequest->identity_card == "waiting")
                                        <a href="{{ url('admin/account/verification-identity-card/'.$id.'/verified') }}" class="btn btn-success">Verify</a>
                                        <a href="{{ url('admin/account/verification-identity-card/'.$id.'/rejected') }}" class="btn btn-danger">Reject</a>
                                        <a href="{{ url('admin/account/verification-identity-card/'.$id.'/cancelled') }}" class="btn btn-primary">Cancel</a>
                                    @endif
                                    <a href="{{ url('admin/account/verification-identity-card') }}" class="btn btn-default">Back</a>

                                </li>
                            </ul>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="box box-primary">
                            <!-- /.box-header -->
                            <div class="box-body">
                                <h3 class="profile-username text-center">Identity Card</h3>
                                @if (empty($photoIdentity))
                                    <center><img class="img-responsive pad" src="{{ asset('assets/logo/mamikos_header_logo_full_inverse.png') }}" alt="Photo"></center>
                                @else
                                    <center><img class="img-responsive pad" src="{{ $photoIdentity }}" alt="Photo"></center>
                                @endif
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <div class="box box-primary">
                            <!-- /.box-header -->
                            <div class="box-body">
                                <h3 class="profile-username text-center">Selfie with Identity Card</h3>
                                @if (empty($photoSelfieIdentity))
                                    <center><img class="img-responsive pad" src="{{ asset('assets/logo/mamikos_header_logo_full_inverse.png') }}" alt="Photo"></center>
                                @else
                                    <center><img class="img-responsive pad" src="{{ $photoSelfieIdentity }}" alt="Photo"></center>
                                @endif
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div><!-- /.box-body -->
@stop