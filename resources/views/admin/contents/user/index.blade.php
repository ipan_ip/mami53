@extends('admin.layouts.main')
@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
            <div class="box-tools pull-right">
                <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                &nbsp;
            </div>

            <table id="tableListUser" class="table table-striped">
                <thead>
                    <tr>
                        <th width="30px;">No</th>
                        <th>{{ Lang::get('hairclick.page.users.photo') }}</th>
                        <th>{{ Lang::get('hairclick.page.users.name') }}</th>
                        <th>{{ Lang::get('hairclick.page.users.gender') }}</th>
                        <th>{{ Lang::get('hairclick.page.users.phone') }}</th>
                        <th>{{ Lang::get('hairclick.page.users.email') }}</th>
                        <th>{{ Lang::get('hairclick.page.users.registered') }}</th>
                        <th>{{ Lang::get('hairclick.page.users.sign_with') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($rowsUser as $rowUserKey => $rowUser)
                    <tr>
                        <td>{{ $rowUserKey+1 }}</td>
                        <td>
                            <?php $tempMedia = Media::getMediaUrl($rowUser['photo_id']); ?>
                            <img width="60" class="img-thumbnail" src="{{ $tempMedia['small'] }}" alt="">
                        </td>
                        <td>{{ $rowUser['name'] }}</td>
                        <td>{{ $rowUser['gender'] }}</td>
                        <td>{{ $rowUser['phone'] }}</td>
                        <td>{{ $rowUser['email'] }}</td>
                        <td>{{ $rowUser['registered'] }}</td>
                        <td>{{ $rowUser['sign_with'] }}</td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th width="30px;">No</th>
                        <th>{{ Lang::get('hairclick.page.users.photo') }}</th>
                        <th>{{ Lang::get('hairclick.page.users.name') }}</th>
                        <th>{{ Lang::get('hairclick.page.users.gender') }}</th>
                        <th>{{ Lang::get('hairclick.page.users.phone') }}</th>
                        <th>{{ Lang::get('hairclick.page.users.email') }}</th>
                        <th>{{ Lang::get('hairclick.page.users.registered') }}</th>
                        <th>{{ Lang::get('hairclick.page.users.sign_with') }}</th>
                    </tr>
                </tfoot>
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
    <!-- table -->
@stop
@section('script')
<!-- page script -->
<script type="text/javascript">
    $(function()
    {
        $("#tableListUser").dataTable();
    });
</script>
@stop