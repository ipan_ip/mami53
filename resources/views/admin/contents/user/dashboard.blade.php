@extends('admin.layouts.main')

@section('style')
<style>
    span.apartment-color,
    tr.apartment-color td {
        color: #0D47A1;
    }

    tr.kost-active td {
        background-color: #C8E6C9;
    }

    .table>tbody>tr>td {
        vertical-align: middle;
    }

    .font-large {
        font-size: 1.5em;
    }

    .font-semi-large {
        font-size: 1.15em;
    }

    .font-grey {
        color: #9E9E9E;
    }

    .dropdown-menu>li>a:hover {
        color: #333
    }

    .label-grey {
        background-color: #CFD8DC;
    }

    /* Sweetalert */

    .swal-wide {
        width: 850px !important;
        /* font-size: 14px!important; */
    }

    .swal-shown {
        height: auto !important;
    }

    .custom-swal {
        z-index: 10000 !important;
    }

    .custom-wide-swal {
        width: 850px !important;
    }

    /* Tooltip container */
    .tooltip {
        visibility: visible;
        position: relative;
        display: inline-block;
    }

    /* Tooltip text */
    .tooltiptext {
        visibility: hidden;
        width: 120px;
        background-color: black;
        color: #fff;
        text-align: center;
        padding: 5px 0;
        border-radius: 6px;

        /* Position the tooltip text - see examples below! */
        position: absolute;
        z-index: 1;
        margin-top: 15px;
        margin-left: 5px;
    }
</style>
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.css">
<link rel="stylesheet" href="//cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
@endsection

@section('content')
<!-- table -->
<div class="box box-primary">
    <div class="box-body no-padding">
        <div class="horizontal-wrapper">
            <h3 style="margin-left: 25px;">Hello, {{ Auth::user()->name }}</h3>
        </div>
        <div class="horizontal-wrapper">
            <div class="btn-horizontal-group bg-default">
                <form action="" class="form-inline w-100" method="get" style="text-align:right;padding:10px;">
                    <input type="text" name="keyword" id="keyword" class="form-control input-sm" placeholder="Action, description, ..."
                        autocomplete="off" value="{{ request()->input('keyword') }}" style="width: 100%;">

                    <input type="text" name="from" class="form-control input-sm datepicker" placeholder="Start Date"
                        value="{{ request()->input('from') }}" id="start-date" autocomplete="off"
                        style="margin-right:2px" />

                    <input type="text" name="to" class="form-control input-sm datepicker" placeholder="End Date"
                        value="{{ request()->input('to') }}" id="end-date" autocomplete="off"
                        style="margin-right:2px" />

                    {{ Form::select('sort', [
                        0 => 'Last Created',
                        1 => 'First Created',
                    ], request()->input('sort'), array('class' => 'pull-right-sort form-group btn btn-default')) }}

                    <button class="btn btn-primary btn-sm" id="buttonSearch"><i
                            class="fa fa-search">&nbsp;</i>Search</button>
                </form>
            </div>
        </div>

        <!-- The table is going here -->
        @include('admin.contents.activity_log.partial.index-table-admin')
        <!-- End of index table -->

    </div><!-- /.box-body -->
    <div class="box-body no-padding">
        {{ $activityLogs->appends(Request::except('page'))->links() }}
    </div>
</div><!-- /.box -->
<!-- table -->
@stop

@section('script')
<script src="//cdn.jsdelivr.net/npm/holderjs@2.9.6/holder.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.min.js"></script>
<script src="//cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
<!-- page script -->
<script type="text/javascript">
    $(function () {
        $('#select-sort-option').change(function (event) {
            var url = '{{ URL::route("admin.dashboard.home") }}' + '?sort=' + event.target.value;

            url += '&st=' + $('#start-date').val();
            url += '&en=' + $('#end-date').val();

            window.location = url;

        });

        $('.datepicker').datepicker({
            dateFormat: 'dd-mm-yy'
        });
    });
</script>
@stop
