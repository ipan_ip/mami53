@extends('admin.layouts.main')
@section('content')
<div class="box box-primary">
  <div class="box-header">
      <h3 class="box-title">{{ $boxTitle }}</h3>
  </div><!-- /.box-header -->
  {{ Form::open(array('route' => $formAction, 'method' => $formMethod,
      'class' => 'form-horizontal form-bordered', 'role' => 'form', 'id' => 'formInsertRegion')) }}
      <div class="box-body no-padding">
            <div class="form-group bg-default">
              <label for="inputName" class="col-sm-2 control-label">Name</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" placeholder="Name"
                  id="inputName" name="name" value="{{ $rowRegion->name }}">
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="{{ URL::route('admin.region.index') }}">
                  <button type="button" class="btn btn-default">Cancel</button>
                </a>
                <button type="reset" class="btn btn-default"
                  onclick="$('#formInsertRegion').bootstrapValidator('resetForm', true);">Reset</button>
              </div>
            </div>
      </div>
  {{ Form::close() }}<!-- /.form -->
</div><!-- /.box -->
@stop
@section('script')
<!-- page script -->
<script type="text/javascript">

    @if (Session::get('laravelValidatorJSON') === null)
      myGlobal.laravelValidator = null;
    @else
      myGlobal.laravelValidator = {{ Session::get('laravelValidatorJSON') }};
    @endif


    $(function()
    {
      $formInsertRegion = $('#formInsertRegion');

      formStoreRegionRules = {
          name: {
              message: 'The name is not valid',
              validators: {
                  notEmpty: {
                      message: 'The name is required and cannot be empty'
                  },
                  stringLength: {
                      min: 3,
                      max: 30,
                      message: 'The name must be more than 3 and less than 30 characters long'
                  }
              }
          },
      };

      formUpdateRegionRules = {
          name: {
              message: 'The name is not valid',
              validators: {
                  notEmpty: {
                      message: 'The name is required and cannot be empty'
                  },
                  stringLength: {
                      min: 3,
                      max: 30,
                      message: 'The name must be more than 3 and less than 30 characters long'
                  }
              }
          },
      };

      @if (Route::currentRouteName() === 'admin.region.create')
        myGlobal.bootstrapValidatorDefaults.fields = formStoreRegionRules;
      @elseif (Route::currentRouteName() === 'admin.region.edit')
        myGlobal.bootstrapValidatorDefaults.fields = formUpdateRegionRules;
      @endif


      $formInsertRegion.bootstrapValidator(myGlobal.bootstrapValidatorDefaults);

      updateValidateStatus($formInsertRegion.data('bootstrapValidator'), myGlobal.laravelValidator);
    });
</script>
@stop