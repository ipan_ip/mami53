@extends('admin.layouts.main')

@section('style')
    <style>
        tr.redirected td {
            color: #ff0000;
        }

        .text-redirected {
            color: #ff0000;
        }
    </style>
@endsection

@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <a href="{{ URL::route('admin.jobs-landing.create') }}">
                        <button type="button" name="button" class="btn-add btn btn-primary btn-sm">
                            <i class="fa fa-plus">&nbsp;</i> Add </button>
                    </a>
                    {{ Form::open(array('method'=>'get','class'=>'form-inline','style'=>'text-align:right;padding:10px;')) }}
                        <input type="text" name="q" class="form-control input-sm"  placeholder="Keyword"  autocomplete="off" value="{{ Input::old('q') }}">
                        <button class="btn btn-primary btn-md" id="buttonSearch"><i class="fa fa-search">&nbsp;</i>Search</button>
                    {{ Form::close() }}
                </div>
            </div>

            <table id="tableListLanding" class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Landing Kost ID</th>
                        <th>Landing Apartment ID</th>
                        <th>Landing Job ID</th>
                        <th class="table-action-column" width="120px">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($rowsLanding as $rowLandingKey => $rowLanding)
                    <tr class="{{ !is_null($rowLanding->redirect_id) ? 'redirected' : '' }}">
                        <td>{{ $rowLanding->id }}</td>
                        <td>{{ $rowLanding->landing_kost_id}}</td>
                        <td>{{ $rowLanding->landing_apartment_id}}</td>
                        <td>{{ $rowLanding->landing_job_id}}</td>
                        <td class="table-action-column">
                            <div class="btn-action-group">
                                <a href="{{ route("admin.landing-apartment.edit", $rowLanding->id) }}" title="Edit Landing">
                                    <i class="fa fa-pencil"></i></a>
                                <a href="{{ route("admin.landing-apartment.destroy", $rowLanding->id) }}" class="btn-delete" title="Delete Landing">
                                    <i class="fa fa-trash-o"></i></a>
                                <a href="{{ url('https://mamikos.com/daftar/' . $rowLanding->slug) }}" title="Show Preview" target="_blank">
                                    <i class="fa fa-eye"></i></a>

                                <a href="{{ url('/admin/landing-apartment/redirect/' . $rowLanding->id) }}" title="Redirect" >
                                    <i class="fa fa-exchange"></i>
                                </a>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="box-body no-padding">
                {{ $rowsLanding->appends(Request::except('page'))->links() }}
            </div>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
    <!-- table -->
@endsection