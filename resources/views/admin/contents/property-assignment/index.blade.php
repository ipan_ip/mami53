@extends('admin.layouts.main')
@section('style')
<style>
    .form-filter .form-group {
        margin: 0px 4px;
    }
</style>
@stop
@section('content')
    <?php App::setLocale('id'); ?>
    <!-- table -->
    <a class="btn btn-xs bg-navy btn-flat" href="{{ route('admin.property.index') }}">Back</a>
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle . ' ' . $property->name }}</h3>
            <div class="box-tools pull-right">
                <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <table id="tableListDesigner" class="table table-striped">
                <thead>
                    <tr>
                        <th>Kost Name</th>
                        <th>Available Room</th>
                        <th>Area</th>
                        <th style="width:80px">Created Date</th>
                        <th class="table-action-column" width="80px">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($kosts as $kost)
                    <tr>
                        <td>{{ $kost->name }}</td>
                        <td>{{ $kost->available_room }}</td>
                        <td>{{ $kost->area_formatted }}</td>
                        <td>{{ $kost->created_at->format('d-m-Y h:i') }}</td>
                        <td>
                            <a class="btn btn-xs bg-navy btn-flat" href="{{ route('admin.property.assign.toggle', [$property->id, $kost->id]) }}">
                                @if($kost->property_id === null)
                                    Assign
                                @else
                                    Remove
                                @endif
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $kosts->appends(Request::except('page'))->links() }}
        </div>
    </div>
@stop
