@extends('admin.layouts.main')
@section('content')
<div class="box box-primary">
  <div class="box-header">
      <h3 class="box-title">{{ $boxTitle }}</h3>
  </div><!-- /.box-header -->
  {{ Form::open(array('route' => $formAction, 'method' => $formMethod,
      'class' => 'form-horizontal form-bordered', 'role' => 'form', 'id' => 'formInsertPaket', 'enctype' => 'multipart/form-data')) }}
      <div class="box-body no-padding">
           
            <div class="form-group bg-default">
              <label for="inputName" class="col-sm-2 control-label">Package Name</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" placeholder="Name Paket"
                  id="inputName" name="name" value="{{ $paket->name }}">
              </div>
            </div>

            <div class="form-group bg-default">
              <label for="inputParentName" class="col-sm-2 control-label">Price</label>
              <div class="col-sm-10">
                <input type="number" class="form-control" placeholder="Price"
                  id="inputParentName" name="price" value="{{ $paket->price }}">
              </div>
            </div>

            <div class="form-group bg-default">
              <label for="inputlatitude1" class="col-sm-2 control-label">Sale Price</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" placeholder="paket Number"
                  id="inputlatitude1" name="sale_price" value="{{ $paket->sale_price }}">
              </div>
            </div>

            <div class="form-group bg-default">
              <label for="inputlatitude1" class="col-sm-2 control-label">Special Price</label>
              <div class="col-sm-10">
                <input type="number" class="form-control" placeholder="Special Price"
                  id="inputlatitude1" name="special_price" value="{{ $paket->special_price }}">
                  <p>(sale price + special price)</p>
              </div>
            </div>
            
            <div class="form-group bg-default">
              <label for="kdjfkdjfkdj" class="col-sm-2 control-label">Start Date Package</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" placeholder="Start date :: ex : 2017-01-02 / tahun-bulan-tanggal"
                  id="kdjfkdjfkdj" name="start_date" value="{{ $paket->start_date }}">
              </div>
            </div>

            <div class="form-group bg-default">
              <label for="inputlatitude1" class="col-sm-2 control-label">Sale Limit Date</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" placeholder="Sale limit date ex : 2017-01-02 / tahun-bulan-tanggal"
                  id="inputlatitude1" name="sale_limit_date" value="{{ $paket->sale_limit_date }}">
              </div>
            </div>

            <div class="form-group bg-default">
              <label for="inputlatitude1" class="col-sm-2 control-label">Bonus</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" placeholder="Bonus"
                  id="inputlatitude1" name="bonus" value="{{ $paket->bonus }}">
              </div>
            </div>

            <div class="form-group bg-default">
              <label for="fitur" class="col-sm-2 control-label">Fitur</label>
              <div class="col-sm-10">
                <textarea class="form-control" name="feature">{{ $paket->feature }}</textarea>
                <p>List fitur dipisah dengan titik (.)</p>
              </div>
            </div>

            <div class="form-group bg-default">
              <label for="inputlatitude1" class="col-sm-2 control-label">Days Count</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" placeholder="Days Count"
                  id="inputlatitude1" name="total_day" value="{{ $paket->total_day }}">
              </div>
            </div>

            <div class="form-group bg-default">
              <label for="inputlatitude1" class="col-sm-2 control-label">Views / Saldo Iklan </label>
              <div class="col-sm-10">
                <input type="text" class="form-control" placeholder="Views Total"
                  id="inputlatitude1" name="view" value="{{ $paket->view }}">
              </div>
            </div>

            <div class="form-group bg-default">
              <label for="inputlatitude1" class="col-sm-2 control-label">For</label>
              <div class="col-sm-10">
                <select class="form-control" id="inputSelectType" name="for">
                        @foreach ($rowFor as $for)
                          @if ($paket->for == $for)
                          <option value="{{ $for }}" selected="selected">{{ $for }}</option>
                          @else
                          <option value="{{ $for }}">{{ $for }}</option>
                          @endif
                        @endforeach
                    </select>
              </div>
            </div>

             <!-- select -->
            <div class="form-group bg-default">
                <label for="inputSelectType" class="col-sm-2 control-label">Is Active</label>
                <div class="col-sm-10">
                    <select class="form-control" id="inputSelectType" name="is_active">
                        @foreach ($rowsActive as $active)
                          @if ($paket->is_active == $active)
                          <option value="{{ $active }}" selected="selected">{{ $active }}</option>
                          @else
                          <option value="{{ $active }}">{{ $active }}</option>
                          @endif
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="{{ URL::route('admin.paket.index') }}">
                  <button type="button" class="btn btn-default">Cancel</button>
                </a>
                <button type="reset" class="btn btn-default"
                  onclick="$('#formInsertPaket').bootstrapValidator('resetForm', true);">Reset</button>
              </div>
            </div>
      </div>
  {{ Form::close() }}<!-- /.form -->
</div><!-- /.box -->
@stop
@section('script')
<!-- page script -->
<script type="text/javascript">

    @if (Session::get('laravelValidatorJSON') === null)
      myGlobal.laravelValidator = null;
    @else
      myGlobal.laravelValidator = {{ Session::get('laravelValidatorJSON') }};
    @endif


    $(function()
    {
      myGlobal.baseUrl = "{{ url('') . '/' }}";

      $formInsertPaket = $('#formInsertPaket');

      $( "#inputSelectType" ).change(function() {
        $formInsertPaket
          .bootstrapValidator('updateStatus', 'name', 'NOT_VALIDATED')
          .bootstrapValidator('validateField', 'name');
      });

      formStoreTagRules = {
          name: {
              message: 'The name is not valid',
              validators: {
                  notEmpty: {
                      message: 'The name is required and cannot be empty'
                  },
                  stringLength: {
                      min: 3,
                      max: 30,
                      message: 'The name must be more than 3 and less than 30 characters long'
                  },
                  // Send { username: 'its value', type: 'username' } to the back-end
                  remote: {
                      message: 'The name is available',
                      url: myGlobal.baseUrl + 'admin/tag/unique',
                      data: function(validator) {
                          return {
                              type : 'tag.name',
                              fields : {
                                name : validator.getFieldElements('name').val(),
                                type : validator.getFieldElements('type').val()
                              }
                          };
                      },
                  }
              }
          },
          type: {
              validators: {
                  notEmpty: {
                      message: 'The type is required and cannot be empty'
                  },
              }
          }
      };

      formUpdateTagRules = {
          name: {
              message: 'The name is not valid',
              validators: {
                  notEmpty: {
                      message: 'The name is required and cannot be empty'
                  },
                  stringLength: {
                      min: 1,
                      max: 30,
                      message: 'The name must be more than 1 and less than 30 characters long'
                  }
              }
          },
          type: {
              validators: {
                  notEmpty: {
                      message: 'The type is required and cannot be empty'
                  },
              }
          }
      };

      @if (Route::currentRouteName() === 'admin.tag.create')
        myGlobal.bootstrapValidatorDefaults.fields = formStoreTagRules;
      @elseif (Route::currentRouteName() === 'admin.tag.edit')
        myGlobal.bootstrapValidatorDefaults.fields = formUpdateTagRules;
      @endif


      $formInsertPaket.bootstrapValidator(myGlobal.bootstrapValidatorDefaults);

      updateValidateStatus($formInsertPaket.data('bootstrapValidator'), myGlobal.laravelValidator);

    });
</script>
@stop