@extends('admin.layouts.main')
@section('content')
    <!-- table -->
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $boxTitle }}</h3>
            <div class="box-tools pull-right">
                <a href="#"><i class="fa fa-minus" data-widget="collapse"></i></a>
                <a href="#"><i class="fa fa-times" data-widget="remove"></i></a>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">

            <div class="horizontal-wrapper">
                <div class="btn-horizontal-group bg-default">
                    <a href="{{ URL::route('admin.paket.create') }}">
                        <button type="submit" class="btn-add btn btn-primary btn-sm">
                            <i class="fa fa-plus">&nbsp;</i>Tambah Paket Premium</button>
                    </a>
                    <form action="" class="form-inline" method="get" style="text-align:right;padding:10px;">
                        <input type="text" name="q" class="form-control input-sm"  placeholder="Nama paket"  autocomplete="off" value="{{ request()->input('q') }}" />
                        {{ Form::select('type', $packageType, request()->input('type'), array('class' => 'pull-right-sort form-group btn btn-primary btn-sm')) }}     
                        <button class="btn btn-primary btn-sm" id="buttonSearch"><i class="fa fa-search">&nbsp;</i>Search</button>
                  </form>
                </div>
            </div>

            <table class="table table-striped">
                <thead>
                    <tr>
                        <th width="30px;">No</th>
                        <th>Name</th>
                        <th>Price</th>
                        <th>Sale Price</th>
                        <th>Start - End Date</th>
                        <th>Days Count</th>
                        <th>is Active</th>
                        <th>Views</th>
                        <th>For</th>
                        <th class="table-action-column">Action</th>
                    </tr>
                </thead>
                <tbody>
                   @foreach ($PremiumPackage AS $key => $package)
                       <tr>
                           <td>{{ $key+1 }}</td>
                           <td>@if ($package->special_price > 0)
                             <i class="fa fa-star" title="Special price" aria-hidden="true"></i>
                           @endif
                           {{ $package->name }}</td>
                           <td>Rp {{ number_format($package->price,0,",",".") }}</td>
                           <td>Rp {{ number_format($package->sale_price,0,",",".") }}</td>
                           <td>{{ date('d M Y', strtotime($package->start_date)) }} <strong> - </strong> {{ date('d M Y', strtotime($package->sale_limit_date)) }}</td>
                           <td>{{ $package->total_day }}</td>
                           <td>@if ($package->is_active == '1')
                                  <span class="label label-success">Yes</span>
                               @else
                                  <span class="label label-danger">No</span>
                               @endif
                           </td>
                           <td>{{ $package->view }}</td>
                           <td>
                             @if ($package->for == 'balance')
                                  <span class="label label-info">Balance</span>
                             @elseif (in_array($package->for, ['package', 'apartment', 'kos'])) 
                                  <span class="label label-danger">Package</span>
                             @else
                                  <span class="label label-warning">Trial</span>     
                             @endif
                           </td>
                           <td><a href="{{ URL::route('admin.paket.edit', $package->id) }}"><i class="fa fa-pencil"></i></a>

                           @if (in_array($package->for, ['package', 'apartment', 'kos']))
                                
                               @if (date('Y-m-d', strtotime('-5 days', strtotime($package->sale_limit_date))) < date('Y-m-d') AND date('Y-m-d') <= $package->sale_limit_date)
                                  <span class="label label-warning">mau abs waktunya</span>
                               @elseif ($package->sale_limit_date < date('Y-m-d'))
                                  <span class="label label-danger">udh abs waktunya</span>
                               @else 
                                  <span class="label label-danger"></span>
                               @endif

                           @endif

                           @if ($package->is_recommendation == 1 AND $package->sale_limit_date >= date('Y-m-d'))
                                <a href="{{ URL::to('admin/paket/recommendation', $package->id) }}"><i style="color: #ff0000;" title="Is recommendation" class="fa fa-star"></i></a>
                           @elseif ($package->is_recommendation == 0 AND $package->sale_limit_date >= date('Y-m-d') AND $package->for != 'trial')
                                <a href="{{ URL::to('admin/paket/recommendation', $package->id) }}"><i style="color: #cccccc;" title="Is recommendation ?" class="fa fa-star"></i></a>
                           @endif
                               <!-- <a href="{{ URL::to('admin/paket/destroy', $package->id) }}"><i class="fa fa-trash-o"></i></a>
                            -->
                            
                         </td>
                       </tr>
                   @endforeach
                </tbody>
                
            </table>
            <div class="box-body no-padding">
                {{ $PremiumPackage->appends(Request::except('page'))->links() }}
            </div>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
    <!-- table -->

@stop