@extends('web.layout.ownerLayout')
@section('meta')
  <!-- Meta
  ================================================== -->
  <base href="/">
  <title>Pencarian Iklan Akun Pemilik</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

  @include('web.@meta.favicon')

  <meta name="description" content="Pencarian Iklan Akun Pemilik" />
  <meta property="fb:app_id" content="607562576051242"/>
  <meta name="keywords" content="Pemilik, Owner">
  <meta name="og:site_name" content="Mamikos"/>
  <meta property="og:description" content="Pencarian Iklan Akun Pemilik" />
  <meta property="og:type" content="website" />
  <meta property="og:url" content="<?php echo Request::url() ?>" />
  <meta property="og:image" content="/assets/share-image-default.jpg" />
  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:site" content="@mamikosapp">
  <meta name="twitter:title" content="Pencarian Iklan Akun Pemilik">
  <meta name="twitter:description" content="Pencarian Iklan Akun Pemilik">
  <meta name="twitter:creator" content="@mamikosapp">
  <meta name="twitter:image" content="/assets/share-image-default.jpg">
  <meta name="twitter:domain" content="mamikos.com">
@endsection

@section('style-source')
<style>
  #modalAddListing .modal-body .form-control {
    min-height: 45px;
    font-size: 18px;
  }
</style>
@endsection

@section('body-start')
<body ng-app="myApp">
  @include('web.part.external.gtm-body')
@endsection

@section('body-content')
      <!--Breadcrumb-->
      <div class="col-xs-12 breadcrumbdiv p0 breadcrumb-recfav" ng-class="{'bannerOn':bannerdisplay.value==true&&windowsWidth<993,'bannerOff':bannerdisplay.value==false}" >
      </div>

      <div id="ownerSearch" class="container-fluid col-xs-12 hidden-xs hidden-sm p0">
        <form v-on:submit.prevent>
          <input class="col-xs-4 owner-input form-control" type="text" maxlength="70" placeholder="Masukkan Nama Kost / Apartemen" v-model="ownerInput">
          <button type="submit" class="col-xs-2 owner-search" v-on:click="ownerSearch(ownerInput)">
            <h4><i class="fa fa-search" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Cari Iklan</h4>
          </button>
        </form>
        <label class="col-xs-1 owner-or">
          <h5 style="font-weight:bold">Atau</h5>
        </label>
        <button type="submit" class="col-xs-3 owner-new" data-toggle="modal" data-target="#modalAddListing">
          <h4><i class="fa fa-plus"></i>&nbsp;&nbsp;&nbsp;Tambah Iklan Baru</h4>
        </button>
      </div>
      
    <div id="ownerSearchMobile" class="container-fluid col-xs-12 visible-xs visible-sm p0">
      <form v-on:submit.prevent="ownerSearchMobile(ownerInputMobile)">
            <input class="col-xs-12 owner-input-mobile form-control" type="text" maxlength="70" placeholder="Masukkan Nama Kost / Apartemen" v-model="ownerInputMobile">
          <button type="submit" class="col-xs-5 owner-search-mobile" v-on:click="ownerSearchMobile(ownerInputMobile)">
            <h5><i class="fa fa-search" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Cari Iklan</h5>
          </button>
          </form>
          <label class="col-xs-1 owner-or-mobile">
          <h6 style="font-weight:bold">Atau</h6>
        </label>
        <button type="button" class="col-xs-6 owner-new-mobile" data-toggle="modal" data-target="#modalAddListing">
          <h5><i class="fa fa-plus"></i>&nbsp;&nbsp;&nbsp;Tambah Iklan Baru</h5>
        </button>
    </div>
    
      <!-- Listing XS -->
        <!-- Tab panes -->
        <div id="ownerListMobile" class="tab-content visible-xs visible-sm" style="display:none;">
          <div role="tabpanel" class="tab-pane active" id="recommendation" v-for="(room, index) in data.rooms">
            <!-- Room Repeat -->
            <div class="col-xs-12 p0 room-recfav-div">
              <a v-bind:href="room.share_url" v-show="room.available_room == 0">
                <div class="col-xs-5 photo-recfav full" v-bind:style="{backgroundImage:'url('+room.photo_url.medium+')'}">
                  <div class="label-full" style="font-size:14px;">
                    PENUH
                  </div>
                </div>
              </a>
              <a v-bind:href="room.share_url" v-show="room.available_room == 1">
                <div class="col-xs-5 photo-recfav" v-bind:style="{backgroundImage:'url('+room.photo_url.medium+')'}">
                  <div class="label-one-left" style="font-size:14px;">
                    TINGGAL 1 KAMAR
                  </div>
                </div>
              </a>
              <a v-bind:href="room.share_url" v-show="room.available_room > 1">
                <div class="col-xs-5 photo-recfav" v-bind:style="{backgroundImage:'url('+room.photo_url.medium+')'}">
                  <div class="label-more-left" style="font-size:14px;">
                    ADA @{{room.available_room}} KAMAR
                  </div>
                </div>
              </a>
              <div class="col-xs-7 room-recfav-info" style="font-size:16px">
                <div v-text="room.price_title_time"></div>
                <div class="title-recfav"><a v-bind:href="room.share_url" style="color:#333;font-weight:normal;">@{{room['room-title']}}</a></div>
                <div class="labelMix label-gender-recfav" v-show="room.gender == 0">Campur</div>
                <div class="labelMale label-gender-recfav" v-show="room.gender == 1">Putra</div>
                <div class="labelFemale label-gender-recfav" v-show="room.gender == 2">Putri</div>
              </div>
            </div>
          </div>
      </div>
        <!-- End of Listing XS -->

        <!-- Listing LG -->
        <div  class="col-sm-12 hidden-xs hidden-sm p0">
        <div id="ownerList" class="tab-content hidden-xs hidden-sm" style="display:none;">
          <!-- Recommendation LG -->
          <div role="tabpanel" class="tab-pane active" id="recommendationLg">
            <div class="col-sm-5" v-for="(room, index) in data.rooms">
              <!-- Room Repeat -->
                <div class="col-xs-12 p0 room-recfav-div">
                  <a v-bind:href="room.share_url" v-show="room.available_room == 0">
                    <div id="roomPhoto" class="col-xs-5 photo-recfav full" v-bind:style="{backgroundImage:'url('+room.photo_url.medium+')',height:'120px'}">
                      <div id="roomFull" class="label-full">
                        PENUH
                      </div>
                    </div>
                  </a>
                  <a v-bind:href="room.share_url" v-show="room.available_room == 1">
                    <div class="col-xs-5 p0 photo-recfav" v-bind:style="{backgroundImage:'url('+room.photo_url.medium+')',height:'120px'}">
                      <div id="roomOne" class="label-one-left">
                        TINGGAL 1 KAMAR
                      </div>
                    </div>
                  </a>
                  <a v-bind:href="room.share_url" v-show="room.available_room > 1">
                    <div class="col-xs-5 p0 photo-recfav" v-bind:style="{backgroundImage:'url('+room.photo_url.medium+')',height:'120px'}">
                      <div id="roomMore" class="label-more-left">
                        ADA @{{room.available_room}} KAMAR
                      </div>
                    </div>
                  </a>
                  <div class="col-xs-7 room-recfav-info">
                    <div v-text="room.price_title_time"></div>
                    <div class="title-recfav"><a v-bind:href="room.share_url" style="color:#333;">@{{room['room-title']}}</a></div>
                    <div class="labelMix label-gender-recfav" v-show="room.gender == 0">Campur</div>
                    <div class="labelMale label-gender-recfav" v-show="room.gender == 1">Putra</div>
                    <div class="labelFemale label-gender-recfav" v-show="room.gender == 2">Putri</div>
                  </div>
                </div>
            </div>
          </div>
          </div>
      </div>
        <div class="col-xs-12" style="height:50px;">
        </div>
      </div>
    </div>

    <!-- Modal Add Listing -->
    <div id="modalAddListing" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title"><b>Saya ingin menambahkan data:</b></h4>
          </div>
          <div class="modal-body">
            <select class="form-control" v-model="listingType">
              <option value="Kost">Kost</option>
              <option value="Apartment">Apartemen</option>
              <option value="Vacancy">Lowongan Kerja</option>
            </select>
          </div>
          <div class="modal-footer">
            <form action="/input-properti" method="POST" v-if="listingType == 'Kost'">
              {{ csrf_field() }}
              <input type="hidden" name="property_type" value="Kost">
              <input type="hidden" name="input_as" value="Pemilik Kos">
              <input type="hidden" name="phone_number" value="{{ $user->phone_number }}">
              <button type="submit" class="btn btn-mamiorange" style="font-size: larger;">Tambahkan Data</button>
              <button type="button" class="btn btn-default" data-dismiss="modal" style="font-size: larger;">Batal</button>
            </form>
            <form action="/input-properti" method="POST" v-else-if="listingType == 'Apartment'">
              {{ csrf_field() }}
              <input type="hidden" name="property_type" value="Apartment">
              <input type="hidden" name="input_as" value="Pemilik Apartemen">
              <input type="hidden" name="phone_number" value="{{ $user->phone_number }}">
              <button type="submit" class="btn btn-mamiorange" style="font-size: larger;">Tambahkan Data</button>
              <button type="button" class="btn btn-default" data-dismiss="modal" style="font-size: larger;">Batal</button>
            </form>
            <form v-if="listingType == 'Vacancy'" @submit.prevent="openInputVacancy">
              <button type="submit" class="btn btn-mamiorange" style="font-size: larger;">Tambahkan Data</button>
              <button type="button" class="btn btn-default" data-dismiss="modal" style="font-size: larger;">Batal</button>
            </form>
          </div>
        </div>
      </div>
    </div>

      

    <!-- Modal Overlay for Loading -->
    <div id="overlay" class="modal fade" role="dialog">
      <div class="modal-dialog modal-sm">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-body overlay-loading">
            <h4>Mohon tunggu...</h4>
            <i class="fa fa-circle-o-notch fa-spin loading"></i>
          </div>
        </div>
      </div>
    </div>

@endsection


@section('script')
<script src="{{ mix_url('dist/js/controller/ownersearchctrl.js') }}"></script>
@endsection

