<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Tutorial Akun Pemilik - mamikos.com</title>
	<link rel="stylesheet" href="{{ mix_url('dist/css/vendor.css' ) }}">
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				@if($type == 'mobile')
					<img src="/assets/tutorial-akun-pemilik-mobile-web-min.jpg">
				@elseif($type == 'desktop')
					<img src="/assets/tutorial-akun-pemilik-desktop-web-min.jpg">
				@elseif($type == 'app')
					<img src="/assets/tutorial-akun-pemilik-app-min.jpg">
				@endif
			</div>
		</div>
	</div>
</body>
</html>