@extends('web.layout.ownerLayout')
@section('meta')
  <!-- Meta
  ================================================== -->
  <base href="/">
  <title>{{$meta['title']}}</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

  @include('web.@meta.favicon')

  <meta name="description" content="{{$meta['description']}}" />
  <meta property="fb:app_id" content="607562576051242"/>
  <meta name="keywords" content="{{$meta['keywords']}}">

  <meta name="og:site_name" content="Mamikos"/>
  <meta property="og:description" content="{{$meta['description']}}" />
  <meta property="og:type" content="website" />
  <meta property="og:url" content="<?php echo Request::url() ?>" />
  <meta property="og:image" content="{{$meta['image_url']}}" />
  
  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:site" content="@mamikosapp">
  <meta name="twitter:title" content="{{$meta['title']}}">
  <meta name="twitter:description" content="{{$meta['description']}}">
  <meta name="twitter:creator" content="@mamikosapp">
  <meta name="twitter:image" content="{{$meta['image_url']}}">
  <meta name="twitter:domain" content="mamikos.com">
@endsection

@section('style')
<link rel="stylesheet" href="{{ mix_url('dist/vendor/photo-sphere-viewer/photo-sphere-viewer.css') }}">

<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/slick-carousel@1.6.0/slick/slick.min.css"/>

<link rel="stylesheet" href="{{ asset('css/premiumpackage.css') }}">
@endsection

@section('body-start')
<body ng-app="myApp">
  @include('web.part.external.gtm-body')
@endsection

@section('body-content')
  <!--Breadcrumb-->
  <div
    class="col-xs-12 breadcrumbdiv p0 breadcrumb-recfav"
    ng-class="{'bannerOn':bannerdisplay.value==true&&windowsWidth<993,'bannerOff':bannerdisplay.value==false}"
  >
  </div>

  <div class="clearfix"></div>

  <h1 class="pkg-title">Upgrade Premium Membership</h1>
  <p class="pkg-sub-desc">Buat iklan kostmu lebih menarik bagi calon penyewa dengan fitur-fitur premium</p>


  <div class="package-promo-container">
    <div class="package-promo material-card-2">
      <div class="package-promo-image package-promo-image-1"></div>
      <div class="package-promo-text">
        <p class="package-promo-title">Top Listing</p>
        <p class="package-promo-caption">Jual kamar kosong lebih cepat dengan fitur Top Listing. Kostmu akan ditampilkan di list atas untuk area terkait, sehingga promosi kostmu jadi lebih efektif dan efisien.</p>
      </div>
    </div>
    <div class="package-promo material-card-2">
      <div class="package-promo-image package-promo-image-2"></div>
      <div class="package-promo-text">
        <p class="package-promo-title">Statistik Iklan Kost</p>
        <p class="package-promo-caption">Pantau berapa kali kostmu dilihat dan disukai calon penyewa, mengetahui list chat calon penyewa dan kamu bisa balas chat untuk mengontak calon penyewa.</p>
      </div>
    </div>
    <div class="package-promo material-card-2">
      <div class="package-promo-image package-promo-image-4"></div>
      <div class="package-promo-text">
        <p class="package-promo-title">Gratis Foto &amp; Video</p>
        <p class="package-promo-caption">Kostmu bisa dapatkan fasilitas foto dan video dari fotografer profesional Mamikos secara GRATIS dan akan membuat iklan kostmu terlihat lebih menarik.</p>
      </div>
    </div>
  </div>



@php
$membershipStatus = $ownerDetail && $ownerDetail['membership']['status'] || ''
@endphp

  <div class="col-md-12" id="pkg-option">
    @if (session('membership-status'))
      <div class="alert alert-info">
          {{ session('membership-status') }}
      </div>
    @endif

    <h1 class="pkg-title">Pilih Paketnya, Bonus Iklan Teratas</h1>
    <p class="pkg-sub-desc">Jadi Member Premium sekarang! Bebas biaya tambahan, gratis bonus Iklan Teratas</p>

    <div class="package-option-container">
      @if(!is_null($trial))
        <div class="package-option-item">

          <div class="package-header material-card-2">
            <span class="package-title-trial">{{ $trial['name'] }}</span>
          </div>

          <div class="package-body material-card-2">

            <div class="package-price">
              <div
                class="
                package-price-main
                package-price-normal
                package-price-trial"
              >
                FREE
              </div>
            </div>

            <div class="package-buy">
              <div class="package-duration">
               {{ $trial['active_count'] }}
              </div>
              
              <form id="trialForm">
                @csrf
                {{-- <input type="hidden" value="{{ $trial['id'] }}"> --}}
                <button
                  type="submit"
                  class="
                    btn btn-buy-pkg
                    package-buy-button
                    track-owner-upgrade-package"
                  {{-- {{ $ownerDetail['membership']['status'] === 'Proses Verifikasi' ? 'disabled' : '' }}  commented for furher changes --}} 
                >
                  {{ $trial['price'] == 'Rp 0' ? 'COBA' : 'BELI'}}
                </button>
              </form>

            </div>

            <div class="package-features">
              @foreach($trial['feature'] as $feature)
                <div class="package-feature-item">{{$feature}}</div>
              @endforeach
            </div>

          </div>
        </div>
      @endif

      @foreach($packages as $package)

        <div class="package-option-item">

          <div class="package-header material-card-2">
            <span class="package-title">{{ $package['name'] }}</span>
          </div>

          <div class="package-body material-card-2">

            <div class="package-price">
              <div class="package-price-discount">
                <span class="package-price-discount-percentage">
                  {{ $package['diskon'] }}
                </span>
                <span class="package-price-discount-original">
                  {{ $package['price'] }}
                </span>
              </div>
              <div class="package-price-main">
                <span class="package-price-superscript">Rp.</span>
                <span class="package-price-normal">{{
                    substr(
                      $package['sale_price'],
                      3,
                      strlen($package['sale_price'])-7
                    )
                  }}</span>
                  {{-- get string that starts from 3rd char get all of it except for unused chars --}}
                <span class="package-price-subscript">.000</span>
              </div>
            </div>

            <div class="package-buy">
              <div class="package-duration">
               {{ $package['active_count'] }}
              </div>
              <button 
              id="buyPremiumButton"
              class="btn btn-buy-pkg
              package-buy-button
              track-owner-upgrade-package" 
              {{-- {{ $ownerDetail['membership']['status'] === 'Proses Verifikasi' ? 'disabled' : '' }} commented for furher changes --}}
              onclick="buyPackage({{ json_encode($package) }})"
              >
                {{ $package['price'] == 'Rp 0' ? 'COBA' : 'BELI'}}
              </button>
            </div>

            <div class="package-features">
              @foreach($package['feature'] as $feature)
                <div class="package-feature-item">{{$feature}}</div>
              @endforeach
            </div>

          </div>
        </div>

      @endforeach
    </div>


  </div>

  <div class="clearfix"></div>




  <div class="col-md-10 col-md-push-1">
    <h1 class="pkg-title pkg-title-wo-underline">
      Tampilan Terbaik untuk Kostmu
    </h1>

    <h2 class="pkg-title-special">
      <img src="/assets/icons/bintang1.png">
      <span>Paket Premium Mulai dari 6 Bulan</span>
      <img src="/assets/icons/bintang2.png">
    </h2>

    <p class="pkg-sub-desc">
      Buat kostmu semakin menarik dengan fitur spesial Member Premium Mamikos, mudah dan praktis.
    </p>
    <p class="pkg-sub-desc">
      <strong>Hanya berlaku untuk membership premium 6 Bulan atau lebih</strong>
    </p>

    <div class="row pkg-special-features">
      <div class="col-md-3 pkg-special-feature">
        <div class="pkg-special-feature-img-container">
          <img src="/assets/icons/ic_360_green.png">
        </div>
        <div class="pkg-special-feature-info-container">
          <h3 class="pkg-special-feature-title">Foto 360 <strong>*</strong></h3>
          <p class="pkg-special-feature-desc">
            Dapatkan bonus pembuatan foto kost 360
          </p>
          <p>
            <em>* Untuk area tertentu</em>
          </p>
        </div>
      </div>
      <div class="col-md-3 pkg-special-feature">
        <div class="pkg-special-feature-img-container">
          <img src="/assets/icons/ic_video_green.png">
        </div>
        <div class="pkg-special-feature-info-container">
          <h3 class="pkg-special-feature-title">Video Kost Eksklusif</h3>
          <p class="pkg-special-feature-desc">
            Dapatkan bonus pembuatan video kost eksklusif
          </p>
        </div>
      </div>
      <div class="col-md-3 pkg-special-feature">
        <div class="pkg-special-feature-img-container">
          <img src="/assets/icons/ic_ulasan_green.png">
        </div>
        <div class="pkg-special-feature-info-container">
          <h3 class="pkg-special-feature-title">Ulasan Eksklusif</h3>
          <p class="pkg-special-feature-desc">
            Dapatkan bonus pembuatan 1 artikel ulasan kost eksklusif
          </p>
        </div>
      </div>
    </div>

    <div class="row pkg-media-example">
      <div class="col-md-6">
        <div class="pkg-round-viewer" id="round-viewer"></div>
        <strong>Contoh Foto 360</strong>
      </div>
      <div class="col-md-6">
        <iframe width="560" height="315" src="https://www.youtube.com/embed/xt9UepOgyxE" frameborder="0" allowfullscreen></iframe>
        <strong>Contoh Video Kost</strong>
      </div>
    </div>

    <div class="pkg-cta-btn-group">
      <a  href="/promosi-kost/premium-package#pkg-option" class="btn btn-cta-pkg">Beli Paket Sekarang</a>
    </div>
  </div>

  <div id="modalRequest" class="modal fade pkg-modal-request">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form id="requestForm" class="modal-request" v-on:submit.prevent>
          <div class="modal-body">
            <div class="request-top">
              <input type="hidden" name="request-item[]" required value="Penjelasan Tentang Premium Membership" v-model="requestItem">
              <p class="col-xs-12">Tim MamiKos akan menghubungi Anda untuk menindaklanjuti permintaan Anda, silakan tinggalkan kontak Anda di bawah :</p>
            </div>
            <div class="request-bottom">
              <input class="col-xs-12 form-control" type="text" minlength="2" maxlength="30" name="name" title="tuliskan nama lengkap anda" placeholder="Tuliskan Nama Anda Di Sini" required v-model="name">
              <br>
              <input class="col-xs-12 form-control" type="text" type="text" rangelength="[9,15]" number="true" name="phone" title="tuliskan no. hp yang valid" placeholder="Tuliskan No. HP" required v-model="phone">
                <input class="col-xs-12 form-control" type="email" name="email" title="tuliskan email yang valid" placeholder="Tuliskan Email" required v-model="email">
              <div class="clearfix"></div>
              <input type="hidden" name="status" value="Pemilik" required>
              <input class="col-xs-12 form-control" type="text" minlength="6" maxlength="50" name="kost-name" title="tuliskan nama kost anda secara lengkap" placeholder="Tuliskan Nama Kost Anda Di Sini..." required v-model="kostName">
              <br>
              <label class="col-xs-12 p0" for="kostUrl"><strong>Apabila KOST Anda sudah ada di MamiKos masukkan linknya:</strong></label>
              <input id="kostUrl" class="col-xs-12 form-control" type="url" rangelength="[15,250]" name="kost-url" title="isikan url kost yang valid" placeholder="Masukkan Link Kost Anda Di Sini..." v-model="kostUrl">
            </div>
          </div>
          <div class="modal-footer">
            <button class="col-xs-12 btn request-btn" type="submit" v-on:click="submitRequest()">SUBMIT</button>
          </div>
        </form>
      </div>
    </div>
  </div>

@endsection

@section('script')

  <script src="{{ mix_url('dist/vendor/photo-sphere-viewer/photo-sphere-viewer.js') }}"></script>
  <script src="//cdn.jsdelivr.net/npm/slick-carousel@1.6.0/slick/slick.min.js"></script>
  @include('web.@script.axios-header')

  <script>
    var moengageEventTrack = function(id) {
      tracker('moe', ['[Owner] Buy Package', {
        package_type: 'premium',
        package_id: id,
        balance_id: null,
        bank_id: null,
        membership_expired: null,
        owner_balance: null,
        buy_result: true,
        expired_date: null,
        total_balance: null,
        is_promoted: null
       }]);
    };

    var owner_premium_status = '{{ $ownerDetail['membership']['status'] }}';

    var package_end_date = function endDate(date, days) {
      var endDate = new Date(Number(date));
      endDate.setDate(date.getDate() + days);
      return endDate;
    };

    var moengageEventTrackers = function(package) {
      tracker('moe', ['[Owner] Premium Package Selected', {
        owner_id: '{{ $ownerDetail['user']['user_id'] }}',
        package_id: package.id,
        package_price: package.price_int,
        package_start_date: new Date(),
        package_end_date: package_end_date(new Date(), package.days_count),
        owner_premium_status: owner_premium_status === "Premium",
        owner_premium_package_id: '{{ $ownerDetail['membership']['active_package'] }}',
        interface: getInterFace()
      }])
    };
      
    function getInterFace() {
      var ownerInterface = "";
      var userDevices = [
        {
          device: 'webOs',
          interface: 'mobile'
        },
        {
          device: 'Android',
          interface: 'mobile-android'
        },
        {
          device: 'iPhone',
          interface: 'mobile-ios'
        },
        {
          device: 'iPad',
          interface: 'mobile-ios'
        }
      ];
      if (window.innerWidth <= 800) {
        var found = false;
        $.each(userDevices, function(index, value) {
          if (navigator.userAgent.indexOf(value.device) !== -1) {
            ownerInterface = value.interface;
            found = true;
          }
        })
      } else {
        ownerInterface = "desktop";
      }
      return ownerInterface;
    };


    function buyPackage(package) {
      var membershipStatus = '{{ $ownerDetail['membership']['status'] }}';
      var changePackage = '{{ $changePackage }}';

      if (membershipStatus === "Premium" || membershipStatus === "Upgrade ke Premium" ||  membershipStatus === "Konfirmasi Pembayaran" ) {
        window.location.href = `/ownerpage/premium/purchase/confirmation?package=${package.id}`;
      } else {
         window.location.replace('/login?page=login');
      }
    };

    $( "#trialForm" ).submit(function( event ) {
      var membershipStatus = '{{ $ownerDetail['membership']['status'] }}';
      var changePackage = '{{ $changePackage }}';
      moengageEventTrack(null);
      var oxWebUrl= '{{ config('owner.dashboard_url') }}';

      if ((membershipStatus == 'Premium' || membershipStatus == 'Upgrade ke Premium') || changePackage) {
        if  (changePackage) {
          window.location.href = `/ownerpage/premium/purchase/confirmation?package=${package.id}`;
        } else {
          axios.post('/owner/premium/trial')
          .then(function (res) {
            window.location.replace(oxWebUrl);
          })
          .catch(function (error) {  
            bugsnagClient.notify(error);
            alert('Terjadi galat. Sialakan coba lagi.');
          })
        }
      } else {
        window.location.replace('/login?page=login')
      }
    });



    $(function() {
      var PSV = new PhotoSphereViewer({
        panorama: '/assets/sphere-sample.jpg',
        container: document.getElementById('round-viewer'),
        time_anim: 3000,
        loading_txt:'Memuat Foto...',
        navbar: true,
        navbar_style: {
          backgroundColor: 'rgba(58, 67, 77, 0.7)'
        },
      });

      $('.pkg-features-container').slick({
        dots: true,
        nextArrow: '<button type="button" class="slick-next"><i class="fa fa-arrow-right"></i></button>',
        prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-arrow-left"></i></button>',
        mobileFirst: true,
        responsive : [
          {
            breakpoint: 768,
            settings: {
              slidesToShow: 4,
              slidesToScroll: 4,
              dots: false
            }
          }
        ]
      });
    });

    Vue.config.productionTip = false;

    var requestOwner = new Vue({
      el: '#modalRequest',
      data: {
        requestItem: ['Penjelasan Tentang Premium Membership'],
        name: '',
        phone: '',
        email: '',
        status: 'Pemilik',
        kostName: '',
        kostUrl: ''
      },
      methods: {
        submitRequest: function () {
        if (this.requestItem.length > 0) {
          var validator = $( "#requestForm" ).validate();
          var valid = validator.form();

          if (valid == true) {
            $( ".request-loading" ).show();
            $.ajax({
                  type : "POST",
                  url : "/garuda/premium/request-direction",
                  headers : {"Content-Type":"application/json", "X-GIT-Time": "1406090202", "Authorization": "GIT WEB:WEB"},
                  data : JSON.stringify({
                          "request-item" : this.requestItem,
                        "name" : this.name,
                        "phone" : this.phone,
                        "email" : this.email,
                        "status" : this.status,
                        "kost-name" : this.kostName,
                        "kost-url" : this.kostUrl
                          }),
                  crossDomain: true,
                  dataType: "json",
                  beforeSend: function() {
                    $('.pkg-modal-request .request-btn').attr('disabled', 'disabled');
                  },
                  success : function (requestResponse) {
                    $( "#modalRequest" ).modal( "hide" );
                    $( "#modalSuccess" ).modal( "show" );
                  }
              });
            }

          }
          else {
            alert('Pilihan permintaan Anda belum diisi');
          }
        },
      }
    });
    // track non-user
    if(authCheck && !authCheck.all){
      tracker('moe', ['[Owner] Premium Package List Visited', {
        interface: window.innerWidth < 768 ? 'mobile' : 'desktop',
      }]);
    }
    // track user/non-user which comes from notification
    if(document.location.href.indexOf('redirection_source=notification') !== -1){
      var id = 0;
      if(authData && authData.all && authData.all.id ){
        id = authData.all.id;
      }
      tracker('moe', ['[Owner] Premium Package List Visited', {
        owner_id: id,
        interface: window.innerWidth < 768 ? 'mobile' : 'desktop',
        redirection_source: 'notification'
      }]);
    }
  </script>

	@include('web.@script.aws-kds-logged-user-tracker')
@endsection
