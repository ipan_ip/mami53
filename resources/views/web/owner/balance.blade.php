@extends('web.layout.ownerLayout')
@section('meta')
  <!-- Meta
  ================================================== -->
  <base href="/">
  <title>Tambah Saldo Iklan Pemilik Kost</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  
  @include('web.@meta.favicon')

  <meta name="description" content="Tambah Saldo Iklan Pemilik Kost" />
  <meta property="fb:app_id" content="607562576051242"/>
  <meta name="keywords" content="Pemilik, Owner">

  <meta name="og:site_name" content="Mamikos"/>
  <meta property="og:description" content="Tambah Saldo Iklan Pemilik Kost" />
  <meta property="og:type" content="website" />
  <meta property="og:url" content="<?php echo Request::url() ?>" />
  <meta property="og:image" content="/assets/share-image-default.jpg" />
  
  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:site" content="@mamikosapp">
  <meta name="twitter:title" content="Halaman Pemilik Kost">
  <meta name="twitter:description" content="Tambah Saldo Iklan Pemilik Kost">
  <meta name="twitter:creator" content="@mamikosapp">
  <meta name="twitter:image" content="/assets/share-image-default.jpg">
  <meta name="twitter:domain" content="mamikos.com">
@endsection

@section('style')
<style>
.balance-radio {
  display: inline-block !important;
  width: 32.8%;
  padding: 0 10px;
}
.balance-radio > input {
  visibility: hidden;
  position: absolute;
}
.balance-radio > input + div {
  user-select: none;
  cursor: pointer;
  padding: 8px 0 1px 0;
  outline: 2px solid #e0e0e0;
  overflow: hidden;
  text-align: left;
  padding-left: 15px;
  padding-top: 23px;
  padding-bottom: 15px;
  font-size: 16px;
  position: relative;
}
.balance-radio > input + div:hover {
  background-color: #eeeeee;
  transition: all 0.3s ease;
}
.balance-radio > input:checked + div  {
  /* (RADIO CHECKED) DIV STYLES */
  background-color: #e0e0e0;
}

.corner-ribbon{
  width: 82px;
  background: #1BAA56;
  position: absolute;
  text-align: center;
  line-height: 20px;
  font-size: 11px;
  letter-spacing: 1px;
  color: #f0f0f0;
  top: 0;
  right: 0;
}
.normal {
  font-weight: normal;
}
.price {
  color: #888;
}
.subtitle {
  background-color: #f0f0f0;
  display: inline-block;
  padding: 6px 18px;
  margin-bottom: 20px;
}

.new-premium  {
  width: 96%;
  margin-left: 8px;
  padding: 12px;
  border: 0;
  background-color: #E5EDF8;
  align-items: flex-start;
  display: flex;
  text-align: left;
}

.new-premium .alert-icon {
  width: 21px;
  margin-right: 2rem;
  flex-basis: auto;
  flex-grow: 0;
  flex-shrink: 0;
}

.new-premium .content {
   display: flex;
  flex-direction: column;
  flex-basis: auto;
  flex-grow: 1;
  flex-shrink: 1;
  text-align: left;
}

.new-premium .content .title {
  color: #142E63;
  font-weight: bold;
  font-size: 16px;
  padding-left: 0
}

.new-premium .close {
  align-items: center;
  display: inline-flex;
  justify-content: center;
}


.centered-container {
  max-width: 652px;
  text-align: left;
  margin: auto;
  padding: 0 22px;
}

@media (max-width: 767px) {
  .balance-radio {
    width: 49.4% !important;
    padding: 0px 4px;
  }

  .new-premium {
    width: 98%;
    margin-left: 2px;
  }

  .centered-container {
    padding: 0px 11px;
  }
}
.separate-left {
  margin-right: 10px;
}
</style>
@endsection

@section('body-start')
<body ng-app="myApp">
  @include('web.part.external.gtm-body')
@endsection

@section('body-content')
	<!--Breadcrumb-->
  <div class="col-xs-12 breadcrumbdiv p0 breadcrumb-recfav" ng-class="{'bannerOn':bannerdisplay.value==true&&windowsWidth<993,'bannerOff':bannerdisplay.value==false}" >
  </div>

  <div class="col-xs-12">
    @if (session('membership-status'))
      <div class="alert alert-info">
          {{ session('membership-status') }}
      </div>
    @endif
    {{-- {{ dd($balancePackages) }} --}}
    <div class="row">
      <div class="col-md-12 col-xs-12">
        @if($balancePackages['packages'])
          <form id="balance-form">
            {{ csrf_field() }}
            <input type="hidden" name="is_cancel" value="{{ $changePackage }}">

            <div class="row">
              <div class="row text-center">
                <div class="col-xs-12 col-md-8 col-md-offset-2">
                  <div class="form-group">
                    <label>Beli Saldo Iklan</label>
                    @if ( $userScenario['is_active'] !== true || $userScenario['experiment_value'] == 'control')
                      <p class="subtitle">Membeli saldo tidak sama dengan menambah masa aktif paket premium anda</p>
                    @else
                      <div class="centered-container">
                        <div id="new-premium" class="alert alert-info new-premium">
                          <img src="/general/img/icons/ic_alert_blue.svg" class="alert-icon"/>
                          <div class="content">
                            <span class="title">Yang Baru di Premium</span>
                            <span class="caption">Sekarang, Anda cukup membeli saldo untuk mengaktifkan Premium Anda.</span>
                          </div>
                          <img src="/assets/icons/svg/ic_close_blue.svg" class="close"/>
                        </div>
                      </div>
                    @endif
                    {{-- <select name="premium_package_id" class="form-control balance-selector">
                      @foreach($balancePackages['packages'] as $balance)
                        <option
                          value="{{$balance['id']}}"
                          data-price="{{$balance['price']}}">
                          Rp. {{number_format($balance['views'], 0, ',', '.')}}
                        </option>
                      @endforeach
                    </select> --}}

                    <div class="row centered-container">
                      @foreach($balancePackages['packages'] as $balance)
                        <label class="balance-radio">
                          <input type="radio" name="premium_package_id" value="{{ $balance['id'] }}" />
                          <div class="balance-radio-selector">
                            @if($balance['diskon'] != null)
                            <div class="corner-ribbon">{{ $balance['diskon'] }}</div>
                            @endif
                            <p class="normal">{{ $balance['name'] }}</p>
                            <p class="price">
                              <strong>{{ $balance['price'] }}</strong>
                            </p>
                          </div>
                        </label>
                      @endforeach
                    </div>

                  </div>
                </div>
              </div>
              {{-- <div class="col-xs-12 col-md-3">
                <div class="form-group">
                  <label>Harga</label>
                  <div class="balance-price"></div>
                </div>
              </div> --}}

              <div class="row text-center">
                <div class="col-xs-12 col-sm-4 col-sm-offset-4">
                  <div class="form-group">
                    <button
                      type="button"
                      class="btn btn-balance-purchase separate-left"
                      onclick="history.back()"
                      >

                      Batal
                    </button>
                    <button
                      type="{{ $userScenario['is_active'] !== true || $userScenario['experiment_value'] == 'control' ? 'submit' : 'button' }}"
                      id="balanceSubmitButton"
                      disabled
                      class="btn btn-balance-purchase track-owner-increase-saldo"
                      {{$balanceRequest != null && $balanceRequest->status == 0 ? ($changePackage ? '' : 'disabled="disabled"') : ''}}
                    >
                      OK
                    </button>
                  </div>
                </div>
              </div>

            </div>
          </form>
        @endif
      </div>
    </div>
  </div>
@endsection


@section('script')
@include('web.@script.axios-header')

<script>
  function getInterFace() {
    var ownerInterface = "";
    var userDevices = [
      {
        device: 'webOs',
        interface: 'mobile'
      },
      {
        device: 'Android',
        interface: 'mobile-android'
      },
      {
        device: 'iPhone',
        interface: 'mobile-ios'
      },
      {
        device: 'iPad',
        interface: 'mobile-ios'
      }
    ];
    if (window.innerWidth <= 800) {
      var found = false;
      $.each(userDevices, function(index, value) {
        if (navigator.userAgent.indexOf(value.device) !== -1) {
          ownerInterface = value.interface;
          found = true;
        }
      })
    } else {
      ownerInterface = "desktop";
    }
    return ownerInterface;
  };
  var ownerPackageId = '{{ $ownerDetail['membership']['active_package'] }}';
  var ownerPremiumEndDate = '{{ date_format(date_create($ownerDetail['membership']['active']), "Y-m-d") }}';
  var ownerBalance = '{{ $ownerDetail['membership']['views'] }}';
  var ownerId = '{{ $ownerDetail['user']['user_id'] }}';
  
  var visitedTrackerData = {
    owner_id: parseInt(ownerId),
    owner_premium_package_id: parseInt(ownerPackageId),
    owner_premium_end_date: ownerPremiumEndDate,
    owner_balance: parseInt(ownerBalance),
    redirection_source: window.document.referrer,
    interface: getInterFace()
  };
  tracker('moe', ['[Owner] Premium Balance List Visited', visitedTrackerData]);
  tracker('logger', ['[Owner] Premium Balance List Visited', visitedTrackerData]);

  $(function() {
    $('#balanceSubmitButton').on('click', function() {
      var selectedId = $('input[name=premium_package_id]:checked', '#balance-form').val();
      var isActiveABTest = '{{ $userScenario['is_active'] == true && $userScenario['experiment_id'] > 0 }}';
      var isOnBScenario = '{{ $userScenario['experiment_value'] !== 'control' }}';
      var selectedTrackerData = {
        owner_id: parseInt(ownerId),
        owner_premium_package_id: parseInt(ownerPackageId),
        owner_premium_end_date: ownerPremiumEndDate,
        owner_balance: parseInt(ownerBalance),
        interface: getInterFace()
      };

      tracker('moe', ['[Owner] Premium Balance Selected', selectedTrackerData]);tracker('logger', ['[Owner] Premium Balance Selected', selectedTrackerData]);

       if (isActiveABTest && isOnBScenario) {
        axios.post('/owner/premium/purchase', { 'package_id': selectedId })
            .then(function (res) {
              if (res.data.status) {
                window.location.href = `/ownerpage/premium/balance/confirmation?package=${selectedId}`;
              } else {
                alert(`${res.data.message}`);
              }
            }).catch(function (error) {
              bugsnagClient.notify(error);
              alert('Terjadi galat. Silakan coba lagi.');
            });
       } else {
          window.location.href = `/ownerpage/premium/purchase/confirmation?package=${selectedId}`;
       };
    });

    $('.balance-selector').on('change', function(e) {
      price = $(this).find('option:selected').data('price');

      $('.balance-price').text(price);
    });

    $('.balance-selector').trigger('change');

    @if($balanceRequest != null && $balanceRequest->status == 0)
      @if(!$changePackage)
        $('#balance-form').on('submit', function(e) {
          e.preventDefault();
        });
      @endif
    @endif

    $("input[name='premium_package_id']").change(function() {
        $("#balanceSubmitButton").removeAttr("disabled");
    });


    $('.close').on('click', function(e) {
      document.getElementById("new-premium").style.display = "none";
    })

  });
</script>
@endsection