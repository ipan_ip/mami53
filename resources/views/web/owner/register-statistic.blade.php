@extends('web.layout.ownerLayout')
@section('meta')
    <!-- Meta
  ================================================== -->
  <base href="/">
  <title>Registrasi Pemilik Kost</title>
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <meta name="application-name" content="Mamikos">
  <meta name="theme-color" content="#1BAA56">
  <link rel="manifest" href="/json/manifest.json">

  @include('web.@meta.link-prebrowsing')

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

  @include('web.@meta.favicon')

  <meta name="description" content="Halaman Registrasi Pemilik Kost" />
  <meta property="fb:app_id" content="607562576051242"/>
  <meta name="keywords" content="Pemilik, Owner">
  <meta name="og:site_name" content="Mamikos"/>

  <meta property="og:description" content="Halaman Registrasi Pemilik Kost" />
  <meta property="og:type" content="website" />
  <meta property="og:url" content="<?php echo Request::url() ?>" />
  <meta property="og:image" content="/assets/share-image-default.jpg" />
  
  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:site" content="@mamikosapp">
  <meta name="twitter:title" content="Halaman Registrasi Pemilik Kost">
  <meta name="twitter:description" content="Halaman Registrasi Pemilik Kost">
  <meta name="twitter:creator" content="@mamikosapp">
  <meta name="twitter:image" content="/assets/share-image-default.jpg">
  <meta name="twitter:domain" content="mamikos.com">
@endsection

@section('style-source')
  <link href="{{ mix_url('dist/css/appmamikos.css') }}" rel="stylesheet">
  <style>

    @media (min-width: 768px) {
      .register-statistic-container {
        margin-top: 60px;
      }
    }

    .register-statistic-tablist {
      background: #fff;
      border-bottom: none;
      display: block;
      margin-bottom: 3px;;
    }

    .nav-tabs.register-statistic-tablist > li {
      background: #fff;
      padding: 10px 0;
      text-align: center;
      width: 25%;
    }

    .nav-tabs.register-statistic-tablist > li.active {
      background: #fff;
    }

    .nav-tabs.register-statistic-tablist > li > a {
      background: #fff;
      padding: 0;
    }

    .nav-tabs.register-statistic-tablist > li.active >  a,
    .nav-tabs.register-statistic-tablist > li.active >  a:hover,
    .nav-tabs.register-statistic-tablist > li.active >  a:active,
    .nav-tabs.register-statistic-tablist > li.active >  a:focus {
      background: #fff;
      padding: 0;
    }

    label.error {
      color: yellow;
      font-weight: normal;
      font-size: 16px;
    }

    .table-survey.fake tbody tr td:last-child {
      color: transparent;
      text-shadow: 0 0 8px rgba(0,0,0,0.5);
    }

    .stats-list.fake > div {
      color: transparent;
      text-shadow: 0 0 8px rgba(0,0,0,0.5);
    }

    /*#register-modal-cta .modal-dialog {
      top: 50%;
      transform: translateY(-50%);
    }*/

    #register-modal-cta .modal-dialog .modal-content {
      border: 2px solid #1BAA56;
      border-radius: 10px;
    }
  </style>
@endsection


@section('body-start')
<body>
  @include('web.part.external.gtm-body')
@endsection

@section('body-content')

  <div class="container register-statistic-container">
    <div class="row">

      <div id="statistics" class="container">
        <div class="row">
          <div class="col-md-12">
            <ul class="nav nav-tabs register-statistic-tablist" role="tablist">
              <li role="presentation" class="active">
                <a href="#surveys" role="tab" aria-controls="surveys" data-toggle="tab">SURVEY</a>
              </li>
              <li role="presentation">
                <a href="#call" role="tab" aria-controls="call" data-toggle="tab">TELEPON</a>
              </li>
              <li role="presentation">
                <a href="#called" role="tab" aria-controls="called" data-toggle="tab">CHAT</a>
              </li>
              <li role="presentation">
                <a href="#review" role="tab" aria-controls="review" data-toggle="tab">REVIEW</a>
              </li>
            </ul>
          </div>
        </div>

        <div class="tab-content">
          <div role="tabpanel" class="tab-pane fade in active" id="surveys">
            <table class="table table-striped table-survey fake">
              <tbody>
                <tr>
                  <td>Tanggal Survey</td>
                  <td>{{date('Y-m-d', time())}}</td>
                </tr>
                <tr>
                  <td>Nama User</td>
                  <td>User Name Survey</td>
                </tr>
                <tr>
                  <td>Lama Sewa</td>
                  <td>{{ rand(1,12) }} bulan</td>
                </tr>
                <tr>
                  <td>Jenis Kelamin</td>
                  <td>Pria</td>
                </tr>
                <tr>
                  <td>Usia</td>
                  <td>{{ rand(17, 35) }}</td>
                </tr>
                <tr>
                  <td>Pekerjaan</td>
                  <td>Mahasiswa</td>
                </tr>
                <tr class="survey-action">
                  <td colspan="2">
                    <button class="btn btn-send-chat track-owner-history-survey-reply">Kirim Chat</button>
                  </td>
                </tr>

                <tr>
                  <td>Tanggal Survey</td>
                  <td>{{date('Y-m-d', time())}}</td>
                </tr>
                <tr>
                  <td>Nama User</td>
                  <td>User Name Survey</td>
                </tr>
                <tr>
                  <td>Lama Sewa</td>
                  <td>{{ rand(1,12) }} bulan</td>
                </tr>
                <tr>
                  <td>Jenis Kelamin</td>
                  <td>Pria</td>
                </tr>
                <tr>
                  <td>Usia</td>
                  <td>{{ rand(17, 35) }}</td>
                </tr>
                <tr>
                  <td>Pekerjaan</td>
                  <td>Mahasiswa</td>
                </tr>
                <tr class="survey-action">
                  <td colspan="2">
                    <button class="btn btn-send-chat track-owner-history-survey-reply">Kirim Chat</button>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div role="tabpanel" class="tab-pane fade" id="call">
            <div class="row stats-list fake">
              <div class="col-md-1 col-xs-1 stats-index">
                1
              </div>
              <div class="col-md-2 col-xs-11 stats-date">
                {{date('Y-m-d', time())}}
              </div>
              <div class="col-md-2 col-md-offset-0 col-xs-11 col-xs-offset-1 stats-name">
                Call User Name
              </div>
              <div class="col-md-2 col-md-offset-0 col-xs-11 col-xs-offset-1 stats-phone">
                08145551113
              </div>
              <div class="col-md-3 col-md-offset-0 col-xs-11 col-xs-offset-1 stats-email">
                email@email.com
              </div>
              <div class="col-md-2 col-xs-12 stats-btn">
                <button class="report-btn btn-default track-owner-history-phone-chat">Kirim Chat</button>
                <button class="report-btn btn-default track-owner-history-phone-call">Telepon</button>
              </div>
            </div>

            <div class="row stats-list fake">
              <div class="col-md-1 col-xs-1 stats-index">
                1
              </div>
              <div class="col-md-2 col-xs-11 stats-date">
                {{date('Y-m-d', time())}}
              </div>
              <div class="col-md-2 col-md-offset-0 col-xs-11 col-xs-offset-1 stats-name">
                Call User Name
              </div>
              <div class="col-md-2 col-md-offset-0 col-xs-11 col-xs-offset-1 stats-phone">
                08145551113
              </div>
              <div class="col-md-3 col-md-offset-0 col-xs-11 col-xs-offset-1 stats-email">
                email@email.com
              </div>
              <div class="col-md-2 col-xs-12 stats-btn">
                <button class="report-btn btn-default track-owner-history-phone-chat">Kirim Chat</button>
                <button class="report-btn btn-default track-owner-history-phone-call">Telepon</button>
              </div>
            </div>

            <div class="row stats-list fake">
              <div class="col-md-1 col-xs-1 stats-index">
                1
              </div>
              <div class="col-md-2 col-xs-11 stats-date">
                {{date('Y-m-d', time())}}
              </div>
              <div class="col-md-2 col-md-offset-0 col-xs-11 col-xs-offset-1 stats-name">
                Call User Name
              </div>
              <div class="col-md-2 col-md-offset-0 col-xs-11 col-xs-offset-1 stats-phone">
                08145551113
              </div>
              <div class="col-md-3 col-md-offset-0 col-xs-11 col-xs-offset-1 stats-email">
                email@email.com
              </div>
              <div class="col-md-2 col-xs-12 stats-btn">
                <button class="report-btn btn-default track-owner-history-phone-chat">Kirim Chat</button>
                <button class="report-btn btn-default track-owner-history-phone-call">Telepon</button>
              </div>
            </div>
          </div>
          <div role="tabpanel" class="tab-pane fade" id="called">
            <div class="row stats-list fake">
              <div class="col-md-1 col-xs-1 stats-index">

              </div>
              <div class="col-md-2 col-xs-11 stats-date">
                {{date('Y-m-d', time())}}
              </div>
              <div class="col-md-3 col-md-offset-0 col-xs-11 col-xs-offset-1 stats-name">
                Chat User Name
              </div>
              <div class="col-md-3 col-md-offset-0 col-xs-11 col-xs-offset-1 stats-phone">
                08145551113
              </div>
              <div class="col-md-3 col-xs-12 stats-btn">
                <button class="report-btn btn-default track-owner-history-chat-reply">Balas Chat</button>
              </div>
            </div>

            <div class="row stats-list fake">
              <div class="col-md-1 col-xs-1 stats-index">

              </div>
              <div class="col-md-2 col-xs-11 stats-date">
                {{date('Y-m-d', time())}}
              </div>
              <div class="col-md-3 col-md-offset-0 col-xs-11 col-xs-offset-1 stats-name">
                Chat User Name
              </div>
              <div class="col-md-3 col-md-offset-0 col-xs-11 col-xs-offset-1 stats-phone">
                08145551113
              </div>
              <div class="col-md-3 col-xs-12 stats-btn">
                <button class="report-btn btn-default track-owner-history-chat-reply">Balas Chat</button>
              </div>
            </div>

            <div class="row stats-list fake">
              <div class="col-md-1 col-xs-1 stats-index">

              </div>
              <div class="col-md-2 col-xs-11 stats-date">
                {{date('Y-m-d', time())}}
              </div>
              <div class="col-md-3 col-md-offset-0 col-xs-11 col-xs-offset-1 stats-name">
                Chat User Name
              </div>
              <div class="col-md-3 col-md-offset-0 col-xs-11 col-xs-offset-1 stats-phone">
                08145551113
              </div>
              <div class="col-md-3 col-xs-12 stats-btn">
                <button class="report-btn btn-default track-owner-history-chat-reply">Balas Chat</button>
              </div>
            </div>
          </div>
          <div role="tabpanel" class="tab-pane fade" id="review">
            <div class="row stats-list fake">
              <div class="col-md-12 col-xs-12 stats-name">
                Review User Name
              </div>
              <div class="col-md-12 col-xs-12 stats-date">
                {{date('Y-m-d', time())}}
              </div>
              <div class="col-md-6 col-xs-12 review-point">
                <div class="container-fluid whitesmoke">
                  <div class="col-md-3 col-xs-4">
                    Kebersihan
                  </div>
                  <div class="col-md-3 col-xs-2 p0 review-star">
                    <i class="fa fa-star green"></i>
                    <i class="fa fa-star green"></i>
                    <i class="fa fa-star green"></i>
                  </div>
                  <div class="col-md-3 col-xs-4">
                    Kenyamanan
                  </div>
                  <div class="col-md-3 col-xs-2 p0 review-star">
                    <i class="fa fa-star green"></i>
                    <i class="fa fa-star green"></i>
                    <i class="fa fa-star green"></i>
                    <i class="fa fa-star green"></i>
                  </div>
                </div>
                <div class="container-fluid">
                  <div class="col-md-3 col-xs-4">
                    Keamanan
                  </div>
                  <div class="col-md-3 col-xs-2 p0 review-star">
                    <i class="fa fa-star green"></i>
                    <i class="fa fa-star green"></i>
                  </div>
                  <div class="col-md-3 col-xs-4">
                    Harga
                  </div>
                  <div class="col-md-3 col-xs-2 p0 review-star">
                    <i class="fa fa-star green"></i>
                    <i class="fa fa-star green"></i>
                    <i class="fa fa-star green"></i>
                    <i class="fa fa-star green"></i>
                  </div>
                </div>
                <div class="container-fluid whitesmoke">
                  <div class="col-md-3 col-xs-4">
                    Fasilitas Kamar
                  </div>
                  <div class="col-md-3 col-xs-2 p0 review-star">
                    <i class="fa fa-star green"></i>
                    <i class="fa fa-star green"></i>
                    <i class="fa fa-star green"></i>
                  </div>
                  <div class="col-md-3 col-xs-4">
                    Fasilitas Umum
                  </div>
                  <div class="col-md-3 col-xs-2 p0 review-star">
                    <i class="fa fa-star green"></i>
                    <i class="fa fa-star green"></i>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-xs-12 review-content">
                This is review content
              </div>
            </div>

            <div class="row stats-list fake">
              <div class="col-md-12 col-xs-12 stats-name">
                Review User Name
              </div>
              <div class="col-md-12 col-xs-12 stats-date">
                {{date('Y-m-d', time())}}
              </div>
              <div class="col-md-6 col-xs-12 review-point">
                <div class="container-fluid whitesmoke">
                  <div class="col-md-3 col-xs-4">
                    Kebersihan
                  </div>
                  <div class="col-md-3 col-xs-2 p0 review-star">
                    <i class="fa fa-star green"></i>
                    <i class="fa fa-star green"></i>
                    <i class="fa fa-star green"></i>
                  </div>
                  <div class="col-md-3 col-xs-4">
                    Kenyamanan
                  </div>
                  <div class="col-md-3 col-xs-2 p0 review-star">
                    <i class="fa fa-star green"></i>
                    <i class="fa fa-star green"></i>
                    <i class="fa fa-star green"></i>
                    <i class="fa fa-star green"></i>
                  </div>
                </div>
                <div class="container-fluid">
                  <div class="col-md-3 col-xs-4">
                    Keamanan
                  </div>
                  <div class="col-md-3 col-xs-2 p0 review-star">
                    <i class="fa fa-star green"></i>
                    <i class="fa fa-star green"></i>
                  </div>
                  <div class="col-md-3 col-xs-4">
                    Harga
                  </div>
                  <div class="col-md-3 col-xs-2 p0 review-star">
                    <i class="fa fa-star green"></i>
                    <i class="fa fa-star green"></i>
                    <i class="fa fa-star green"></i>
                    <i class="fa fa-star green"></i>
                  </div>
                </div>
                <div class="container-fluid whitesmoke">
                  <div class="col-md-3 col-xs-4">
                    Fasilitas Kamar
                  </div>
                  <div class="col-md-3 col-xs-2 p0 review-star">
                    <i class="fa fa-star green"></i>
                    <i class="fa fa-star green"></i>
                    <i class="fa fa-star green"></i>
                  </div>
                  <div class="col-md-3 col-xs-4">
                    Fasilitas Umum
                  </div>
                  <div class="col-md-3 col-xs-2 p0 review-star">
                    <i class="fa fa-star green"></i>
                    <i class="fa fa-star green"></i>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-xs-12 review-content">
                This is review content
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


  <div class="modal fade" id="register-modal-cta" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body" style="text-align:center;">
          <p>
            {{ $dialogOptions['text'] }}
          </p>

          <button class="btn btn-mamigreen btn-lg btn-register-modal-cta {{ $dialogOptions['class'] }}">{{ $dialogOptions['button'] }}</button>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
  <script>
  $(function() {
    function reposition() {
      var modal = $(this),
        dialog = modal.find('.modal-dialog');
      modal.css('display', 'block');

      // Dividing by two centers the modal exactly, but dividing by three
      // or four works better for larger screens.
      dialog.css(
        'margin-top',
        Math.max(0, ($(window).height() - dialog.height()) / 2)
      );
      dialog.css(
        'margin-left',
        Math.max(0, ($(window).width() - dialog.width()) / 2)
      );
    }
    // Reposition when a modal is shown
    $('.modal').on('show.bs.modal', reposition);
    // Reposition when the window is resized
    $(window).on('resize', function() {
      $('.modal:visible').each(reposition);
    });

    $('#register-modal-cta').modal({
      backdrop: 'static',
      keyboard: false,
      show: true
    });

    $('.btn-register-modal-cta').on('click', function(e) {
      e.preventDefault();

      window.open('/login?page=login', '_self');
    });

    $('a[data-toggle="tab"][aria-controls="{{ $tab }}"]').trigger('click');
  });
  </script>
@endsection