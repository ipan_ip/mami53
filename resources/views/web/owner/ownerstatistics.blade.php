@extends('web.layout.ownerLayout')
@section('meta')
  <!-- Meta
  ================================================== -->
  <base href="/">
  <title>Halaman Pemilik Kost</title>
  <meta name="csrf-token" content="{{ csrf_token() }}">

<meta name="application-name" content="Mamikos">
<meta name="theme-color" content="#1BAA56">
<link rel="manifest" href="/json/manifest.json">

@include('web.@meta.link-prebrowsing')

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  
  @include('web.@meta.favicon')
  
  <meta name="description" content="Halaman Pemilik Kost" />
  <meta property="fb:app_id" content="607562576051242"/>
  <meta name="keywords" content="Pemilik, Owner">
  <meta name="og:site_name" content="Mamikos"/>
  <meta property="og:description" content="Halaman Pemilik Kost" />
  <meta property="og:type" content="website" />
  <meta property="og:url" content="<?php echo Request::url() ?>" />
  <meta property="og:image" content="/assets/share-image-default.jpg" />
  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:site" content="@mamikosapp">
  <meta name="twitter:title" content="Halaman Pemilik Kost">
  <meta name="twitter:description" content="Halaman Pemilik Kost">
  <meta name="twitter:creator" content="@mamikosapp">
  <meta name="twitter:image" content="/assets/share-image-default.jpg">
  <meta name="twitter:domain" content="mamikos.com">
@endsection

@section('style-source')

@endsection

@section('style')
<style>
  .stats-list.is-new {
    background: #d0f6fc;
  }

  .tab-flag-new {
    background: #ff0000;
    border-radius: 3px;
    color: #fff;
    display: inline-block;
    padding: 0 5px;
    margin-left: 5px;
  }

  @media (max-width: 480px) {
    .tab-flag-new {
      display: block;
      font-size: 12px;
      position: absolute;
      right: 0;
      top: 0;
    }
  }

  .btn-prev {
    margin-right: 15px;
  }

  .pagination-button {
    margin-top: 15px;
  }

  .tablist-menu {
    display: -webkit-box;
    display: -moz-box;
    display: -ms-flexbox;
    display: -webkit-flex;
    display: flex;
    -webkit-justify-content: center;
    -moz-justify-content: center;
    -ms-justify-content: center;
    justify-content: center;
  }

  .reply-box-reviewer {
    height: 120px;
    overflow-y: auto;
  }

  #modalShowReply{
    overflow-y: auto;
  }

  .--green{
    color: #1baa56;
  }

  .--grey{
    color: #dbdbdb;
  } 

.half{
  position:relative;
}

.half::before{
  position: relative;
  z-index: 9;
  display: block;
  width: 47%;
  overflow: hidden;
}

.half::after{
  position: absolute;
  top: 0;
  left: 0;
  z-index: 8;
  color: #bdc3c7;
  content: '\e006';
}
</style>
@endsection

@section('body-start')
<body ng-app="myApp">
  @include('web.part.external.gtm-body')
@endsection

@section('body-content')
<!--Breadcrumb-->
<div class="col-xs-12 breadcrumbdiv p0 breadcrumb-recfav" ng-class="{'bannerOn':bannerdisplay.value==true&&windowsWidth<993,'bannerOff':bannerdisplay.value==false}" >
</div>

<div class="clearfix"></div>

<div id="statistics" class="container">
  <div class="tablist-menu" class="row" role="tablist">
    <div
      id="surveys"
      class="text-center track-owner-history-survey"
      role="tab"
      :class="[hasTelp ? 'col-xs-3' : 'col-xs-4', tab.hash == 'survey' ? active : inactive]"
      @click="selectTab('survey')"
      v-cloak
    >
      <span class="track-owner-history-survey"><i class="fa fa-map-marker hidden-sm hidden-xs" aria-hidden="true"></i> SURVEY</span>
      <span v-show="stats.data.survey != undefined && stats.data.survey.length > 0 && stats.data.survey[0].is_new" class="tab-flag-new">BARU</span>
    </div>
    <div
      id="call"
      class="text-center track-owner-history-phone"
      role="tab"
      :class="[hasTelp ? 'col-xs-3' : 'col-xs-4', tab.hash == 'telepon' ? active : inactive]"
      @click="selectTab('telepon')"
      v-if="hasTelp"
      v-cloak
    >
      <span class="track-owner-history-phone"><i class="fa fa-phone hidden-sm hidden-xs" aria-hidden="true"></i> TELEPON</span>
    </div>
    <div
      id="called"
      class="text-center track-owner-history-chat"
      role="tab"
      :class="[hasTelp ? 'col-xs-3' : 'col-xs-4', tab.hash == 'chat' ? active : inactive]"
      @click="selectTab('chat')"
      v-cloak
    >
      <span class="track-owner-history-chat"><i class="fa fa-comment hidden-sm hidden-xs" aria-hidden="true"></i> CHAT</span>
      <span v-show="stats.data.chat != undefined && stats.data.chat.length > 0 && stats.data.chat[0].is_new" class="tab-flag-new">BARU</span>
    </div>
    @if(!$is_apartment)
    <div
      id="review"
      class="text-center track-owner-history-review"
      role="tab"
      :class="[hasTelp ? 'col-xs-3' : 'col-xs-4', tab.hash == 'review' ? active : inactive]"
      @click="selectTab('review')"
      v-cloak
    >
      <span class="track-owner-history-review"><i class="fa fa-star hidden-sm hidden-xs" aria-hidden="true"></i> REVIEW</span>
    </div>
    @endif
  </div>
  @if($enable_chat)
  <div class="col-md-12 col-xs-12 stats-page" v-show="stats.loading == false && stats.totalPage > 0" v-cloak>
    <p><b>Halaman @{{stats.page}} dari @{{stats.totalPage}}</b></p>
  </div>
  <div class="clearfix"></div>
  @endif
  <div class="stats-loading" v-show="stats.loading == true" v-cloak>
    <p class="hidden-lg hidden-md">Memuat data statistik {{ $room_name }}...</p>
    <p class="hidden-sm hidden-xs">Memuat data statistik...</p>
    <i class="fa fa-circle-o-notch fa-spin loading-spinner-green"></i>
  </div>
  <br>
  <div class="stats-null" v-show="stats.null == true" v-cloak>
    <p>Tidak ada data di List.</p>
    <p>Untuk member non premium hanya ditampilkan list 2 hari terakhir saja.</p>
    <img src="/assets/history_zero.png">
  </div>


  @if(!$enable_chat)
  <!-- Survey List No Premium -->
  <div class="row stats-list" :class="{'is-new': survey.is_new}" v-show="tab.hash == 'survey' && stats.loading == false" v-for="(survey, index) in stats.data.survey" v-cloak>
    <div class="col-md-1 col-xs-1 stats-index">
      @{{index + 1}}.
    </div>
    <div class="col-md-2 col-xs-11 stats-date">
      @{{survey.time}}
    </div>
    <div class="col-md-3 col-md-offset-0 col-xs-11 col-xs-offset-1 stats-name">
      @{{survey.name}}
    </div>
    <div class="col-md-3 col-md-offset-0 col-xs-11 col-xs-offset-1 stats-phone">
      @{{survey.phone_number}}
      <span class="is-no-premium" data-toggle="modal" data-target="#modalShowNumber">@{{survey.button_click}}</span>
    </div>
    <div class="col-md-3 col-md-offset-0 col-xs-11 col-xs-offset-1 stats-btn">
      <button class="report-btn btn-default track-owner-history-survey-reply" data-toggle="modal" data-target="#modalPremium">Lihat Detail</button>
    </div>
  </div>
  @else


  <!-- Survey List Premium -->
  <div class="row" v-show="tab.hash == 'survey' && stats.loading == false" v-cloak>
    <div class="col-md-6 col-xs-12" v-for="(survey, index) in stats.data.survey" v-show="stats.loading == false">
      <table class="table table-striped table-survey">
        <tbody>
          <tr>
            <td>Tanggal Survey</td>
            <td>@{{survey.time}}</td>
          </tr>
          <tr>
            <td>Nama User</td>
            <td>@{{survey.name}}</td>
          </tr>
          <tr>
            <td>Lama Sewa</td>
            <td>@{{survey.kost_time}}</td>
          </tr>
          <tr>
            <td>Jenis Kelamin</td>
            <td>@{{survey.gender}}</td>
          </tr>
          <tr>
            <td>Usia</td>
            <td>@{{survey.birthday}}</td>
          </tr>
          <tr>
            <td>Pekerjaan</td>
            <td>
              @{{survey.jobs}}
            </td>
          </tr>
          <tr>
            <td>No. Handphone</td>
            <td>
              @{{survey.phone_number}}
            </td>
          </tr>
          <tr class="survey-action">
            <td colspan="2">
              <button class="btn btn-send-chat track-owner-history-survey-reply" @click="surveyChat(survey.group_channel_url, survey.name, survey.user_id, survey.name_social, survey.admin_id)">Kirim Chat</button>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
  @endif


  <!-- Telepon List -->
  <div class="row stats-list" v-show="tab.hash == 'telepon' && stats.loading == false" v-for="(call, index) in stats.data.call" v-cloak>
    <div class="col-md-1 col-xs-1 stats-index">
      @{{index + 1}}.
    </div>
    <div class="col-md-2 col-xs-11 stats-date">
      @{{call.time}}
    </div>
    <div class="col-md-2 col-md-offset-0 col-xs-11 col-xs-offset-1 stats-name">
      @{{call.name}}
    </div>
    <div class="col-md-2 col-md-offset-0 col-xs-11 col-xs-offset-1 stats-phone">
      @{{call.phone_number}}
      @if(!$enable_chat)
      <span class="is-no-premium" data-toggle="modal" data-target="#modalShowNumber">@{{call.button_click}}</span>
      @endif
    </div>
    <div class="col-md-3 col-md-offset-0 col-xs-11 col-xs-offset-1 stats-email">
      @{{call.email}}
    </div>
    <div class="col-md-2 col-md-offset-0 col-xs-11 col-xs-offset-1 stats-btn">
      @if(!$enable_chat)
      <!-- Telepon List No Premium -->
      <button class="report-btn btn-default track-owner-history-phone-chat" data-toggle="modal" data-target="#modalPremium">Kirim Chat</button>
      @else
      <!-- Telepon List Premium -->
      <button class="report-btn btn-default track-owner-history-phone-chat" @click="telpChat(call.name, call.user_id, call.admin_id)">Kirim Chat</button>
      <button class="report-btn btn-default track-owner-history-phone-call" @click="telpModal(call.name, call.phone_number)">Telepon</button>
      @endif
    </div>
  </div>


  <!-- Chat List -->
  <div class="row stats-list" :class="{'is-new': chat.is_new}" v-show="tab.hash == 'chat' && stats.loading == false" v-for="(chat, index) in stats.data.chat" v-cloak>
    <div class="col-md-1 col-xs-1 stats-index">
      @{{index + stats.data.startingListNumber }}.
    </div>
    <div class="col-md-2 col-xs-11 tats-date">
      @{{chat.time}}
    </div>
    <div class="col-md-3 col-md-offset-0 col-xs-11 col-xs-offset-1 stats-name">
      @{{chat.name}}
    </div>
    <div class="col-md-3 col-md-offset-0 col-xs-11 col-xs-offset-1 stats-phone">
      @{{chat.phone_number}}
      @if(!$enable_chat)
      <span class="is-no-premium" data-toggle="modal" data-target="#modalShowNumber">@{{chat.button_click}}</span>
      @endif
    </div>
    <div class="col-md-3 col-md-offset-0 col-xs-11 col-xs-offset-1 stats-btn">
      @if(!$enable_chat)
      <!-- Chat List No Premium -->
      <button class="report-btn btn-default track-owner-history-chat-reply" data-toggle="modal" data-target="#modalPremium">Balas Chat</button>
      @else
      <!-- Chat List Premium -->
      <button class="report-btn btn-default track-owner-history-chat-reply" @click="replyChat(chat.name, chat.user_id, chat.admin_id, chat.group_channel_url)">Balas Chat</button>
      @endif
    </div>
  </div>


  <!-- Review List -->
  <div class="row stats-list" v-show="tab.hash == 'review' && stats.loading == false" v-for="(review, index) in stats.data.review" v-cloak>
    <div class="col-md-12 col-xs-12 stats-name">
      @{{review.name}}
    </div>
    <div class="col-md-12 col-xs-12 stats-date">
      @{{review.tanggal}}
    </div>
    <div class="col-md-6 col-xs-12 review-point">
      <div class="container-fluid whitesmoke">
        <div class="col-md-3 col-xs-4">
          Kebersihan
        </div>
        <div class="col-md-3 col-xs-2 p0 review-star">
          <i 
            v-for="star in 5"
            :class="[
              'glyphicon glyphicon-star',
              {
                '--green': review.clean.full >= star,
                '--green half': review.clean.half === star,
                '--grey': star > review.clean.full && star !== review.clean.half
              }
            ]"
            aria-hidden="true">
          </i>
        </div>
        <div class="col-md-3 col-xs-4">
          Kenyamanan
        </div>
        <div class="col-md-3 col-xs-2 p0 review-star">
          <i 
            v-for="star in 5"
            :class="[
              'glyphicon glyphicon-star',
              {
                '--green': review.happy.full >= star,
                '--green half': review.happy.half === star,
                '--grey': star > review.happy.full && star !== review.happy.half
              }
            ]"
            aria-hidden="true">
          </i>
        </div>
      </div>
      <div class="container-fluid">
        <div class="col-md-3 col-xs-4">
          Keamanan
        </div>
        <div class="col-md-3 col-xs-2 p0 review-star">
          <i 
            v-for="star in 5"
            :class="[
              'glyphicon glyphicon-star',
              {
                '--green': review.safe.full >= star,
                '--green half': review.safe.half === star,
                '--grey': star > review.safe.full && star !== review.safe.half
              }
            ]"
            aria-hidden="true">
          </i>
        </div>
        <div class="col-md-3 col-xs-4">
          Harga
        </div>
        <div class="col-md-3 col-xs-2 p0 review-star">
          <i 
            v-for="star in 5"
            :class="[
              'glyphicon glyphicon-star',
              {
                '--green': review.pricing.full >= star,
                '--green half': review.pricing.half === star,
                '--grey': star > review.pricing.full && star !== review.pricing.half
              }
            ]"
            aria-hidden="true">
          </i>
        </div>
      </div>
      <div class="container-fluid whitesmoke">
        <div class="col-md-3 col-xs-4">
          Fasilitas Kamar
        </div>
         <div class="col-md-3 col-xs-2 p0 review-star">
          <i 
            v-for="star in 5"
            :class="[
              'glyphicon glyphicon-star',
              {
                '--green': review.room_facilities.full >= star,
                '--green half': review.room_facilities.half === star,
                '--grey': star > review.room_facilities.full && star !== review.room_facilities.half
              }
            ]"
            aria-hidden="true">
          </i>
        </div>
        <div class="col-md-3 col-xs-4">
          Fasilitas Umum
        </div>
           <div class="col-md-3 col-xs-2 p0 review-star">
          <i 
            v-for="star in 5"
            :class="[
              'glyphicon glyphicon-star',
              {
                '--green': review.public_facilities.full >= star,
                '--green half': review.public_facilities.half === star,
                '--grey': star > review.public_facilities.full && star !== review.public_facilities.half
              }
            ]"
            aria-hidden="true">
          </i>
        </div>
      </div>
    </div>
    <div class="col-md-6 col-xs-12 review-content">
      @{{review.content}}
    </div>
    <div class="col-md-6 col-md-offset-6 col-xs-12">
      <a :href="photo.photo_url.large" class="col-md-4 col-xs-4 p0 swipebox flex-center" :title="caption + review.name" v-for="(photo, index) in review.photo">
        <img class="review-photo" :src="photo.photo_url.small">
      </a>
    </div>

    <div class="col-md-6 col-md-offset-6 col-xs-12">
    <button class="btn btn-mamigreen-inverse btn-reply-review" @click="openReplyModal(review.token)">
      <span v-if="review.reply_count > 0">Lihat Balasan</span>
      <span v-else>Balas Review</span>
    </button>
    </div>

  </div>





  @if($enable_chat)
  <div class="col-md-12 col-xs-12 pagination-button strecth" v-show="stats.loading == false && stats.totalPage > 0" v-cloak>
    <button class="btn btn-mamiorange btn-prev" @click="previousPage(stats.page)" :disabled="stats.page > 1 ? false : true">
      <i class="fa fa-chevron-left" aria-hidden="true"></i> Sebelumnya
    </button>
    <button class="btn btn-mamiorange btn-next" @click="nextPage(stats.page)" :disabled="stats.page < stats.totalPage ? false : true">
      Selanjutnya <i class="fa fa-chevron-right" aria-hidden="true"></i>
    </button>
  </div>
  @endif
  <!-- Modal Show Review Reply  -->
  <div id="modalShowReply" class="modal fade" role="dialog">
    <div class="modal-dialog modal-medium modal-top">

      <div class="modal-content modal-content-review" v-show="!stateReply">
        <div class="modal-header">
            <span class="stats-name">@{{ getreplies.result.name }}</span>
            <span class="stats-date">| @{{ getreplies.result.tanggal }}</span>
        </div>
        <div class="modal-body">
          <div class="reply-box">
            <div class="review-point">
              <div class="container-fluid whitesmoke">
                <div class="col-sm-3 col-xs-6">
                  Kebersihan
                </div>
                <div class="col-sm-3 col-xs-6 p0 review-star">
                  <i 
                    v-for="star in 5"
                    :class="[
                      'glyphicon glyphicon-star',
                      {
                        '--green': getreplies.rating.clean.full >= star,
                        '--green half': getreplies.rating.clean.half === star,
                        '--grey': star > getreplies.rating.clean.full && star !== getreplies.rating.clean.half
                      }
                    ]"
                    aria-hidden="true">
                  </i>
                 </div>
                <div class="col-sm-3 col-xs-6 whitesmoke-inverse">
                  Kenyamanan
                </div>
                <div class="col-sm-3 col-xs-6 p0 review-star whitesmoke-inverse">
                 <i 
                    v-for="star in 5"
                    :class="[
                      'glyphicon glyphicon-star',
                      {
                        '--green': getreplies.rating.happy.full >= star,
                        '--green half': getreplies.rating.happy.half === star,
                        '--grey': star > getreplies.rating.happy.full && star !== getreplies.rating.happy.half
                      }
                    ]"
                    aria-hidden="true">
                  </i>
                </div>
              </div>
              <div class="container-fluid">
                <div class="col-sm-3 col-xs-6 white-inverse">
                  Keamanan
                </div>
                <div class="col-sm-3 col-xs-6 p0 review-star white-inverse">
                  <i 
                    v-for="star in 5"
                    :class="[
                      'glyphicon glyphicon-star',
                      {
                        '--green': getreplies.rating.safe.full >= star,
                        '--green half': getreplies.rating.safe.half === star,
                        '--grey': star > getreplies.rating.safe.full && star !== getreplies.rating.safe.half
                      }
                    ]"
                    aria-hidden="true">
                  </i>
                </div>
                <div class="col-sm-3 col-xs-6">
                  Harga
                </div>
                <div class="col-sm-3 col-xs-6 p0 review-star">
                  <i 
                    v-for="star in 5"
                    :class="[
                      'glyphicon glyphicon-star',
                      {
                        '--green': getreplies.rating.pricing.full >= star,
                        '--green half': getreplies.rating.pricing.half === star,
                        '--grey': star > getreplies.rating.pricing.full && star !== getreplies.rating.pricing.half
                      }
                    ]"
                    aria-hidden="true">
                  </i>
                </div>
              </div>
              <div class="container-fluid whitesmoke">
                <div class="col-sm-3 col-xs-6">
                  Fasilitas Kamar
                </div>
                <div class="col-sm-3 col-xs-6 p0 review-star">
                  <i 
                    v-for="star in 5"
                    :class="[
                      'glyphicon glyphicon-star',
                      {
                        '--green': getreplies.rating.room_facilities.full >= star,
                        '--green half': getreplies.rating.room_facilities.half === star,
                        '--grey': star > getreplies.rating.room_facilities.full && star !== getreplies.rating.room_facilities.half
                      }
                    ]"
                    aria-hidden="true">
                  </i>
                </div>
                <div class="col-sm-3 col-xs-6 whitesmoke-inverse">
                  Fasilitas Umum
                </div>
                <div class="col-sm-3 col-xs-6 p0 review-star whitesmoke-inverse">
                  <i 
                    v-for="star in 5"
                    :class="[
                      'glyphicon glyphicon-star',
                      {
                        '--green': getreplies.rating.public_facilities.full >= star,
                        '--green half': getreplies.rating.public_facilities.half === star,
                        '--grey': star > getreplies.rating.public_facilities.full && star !== getreplies.rating.public_facilities.half
                      }
                    ]"
                    aria-hidden="true">
                  </i>
                </div>
              </div>
            </div>
          </div>
          <div class="reply-box reply-box-reviewer padding-left-5">
            @{{ getreplies.result.content }}
          </div>

          <div class="reply-box padding-left-5" v-for="reply in getreplies.replies">
            <div class="review-reply" v-if="!reply.can_reply">
              <div class="review-heading">
                <span class="review-name">@{{ reply.name }}</span>
                <span class="review-approval" v-if="!reply.is_show">Tunggu disetujui</span>
                <span class="review-date"> | @{{ reply.time }} </span>
              </div>
              <div class="review-content">
                <span class=""> @{{ reply.content }} </span>
              </div>
            </div>
          </div>

          <div v-if="getreplies.result.can_reply">
            <textarea maxlength="140" class="reply-text" placeholder="Tulis balasan review disini..." v-model="replyContent" required></textarea>
            <div class="char-counter">@{{ charLeft }}</div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-mamigreen-inverse" data-dismiss="modal" aria-label="Close">
            <span v-if="getreplies.result.can_reply">Batal</span><span v-else>Tutup</span>
          </button>
          <button type="button" class="btn btn-mamigreen" @click="sendReply()" v-if="getreplies.result.can_reply">
            Balas Review
          </button>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal Show User Phone Number-->
  <div id="modalShowNumber" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title text-center text-green"><b>Perhatian</b></h4>
        </div>
        <div class="modal-body">
          <p>@{{ stats.data.clickable }}</p>
          <img src="/assets/popup-login.png" style="width: 134px;">
        </div>
        <div class="modal-footer">
          <a href="/promosi-kost/premium-package" class="full-width btn btn-default btn-orange track-owner-upgrade">LIHAT PAKET PREMIUM</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal Premium Direction-->
  <div id="modalPremium" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title text-center text-green"><b>Jadilah Member Premium</b></h4>
        </div>
        <div class="modal-body">
          <p>Dapatkan fitur "@{{ stats.data.feature }}"</p>
          <img src="/assets/popup-login.png" style="width: 134px;">
        </div>
        <div class="modal-footer">
          <a href="/promosi-kost/premium-package" class="full-width btn btn-default btn-orange track-owner-upgrade">DAFTAR MENJADI PREMIUM</a>
        </div>
      </div>
    </div>
  </div>

  <!-- MODAL TELP DOWNLOAD APP -->
  <div class="modal fade" id="download-app" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="col-xs-12 modal-header text-uppercase">
            TELEPON USER
        </div>
        <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <p><b>Silakan salin & simpan no. handphone user</b></p>
          <p id="telpUserName"></p>
          <p id="telpUserNumber"></p>
          <button class="btn btn-orange phone-user-btn" data-clipboard-target="#telpUserNumber">SALIN</button>
        </div>
      </div>
    </div>
  </div>

  <!-- MODAL SUBSCRIBE SUKSES-->
  <div class="modal fade"  id="subSuccess" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body" style="text-align:center;">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close" ><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" style="text-align:center; color:green; font-weight:bold;padding-bottom: 20px;">Terima Kasih!</h4>
          <img style="width:150px;" src="/assets/suksesicon.png">
          <p style="color: limegreen;padding-top: 20px;">Link telah dikirim. Silakan download Aplikasi MamiKos</p>
        </div>
        <div class="modal-footer" style="border:0px;">
          <button type="button" class="btn btn-default foot-btn" data-dismiss="modal" >Close</button>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal Overlay for Loading -->
  <div id="overlay" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-body overlay-loading">
          <h4>Mohon tunggu...</h4>
          <i class="fa fa-circle-o-notch fa-spin loading-spinner-green loading"></i>
        </div>
      </div>
    </div>
  </div>

</div>
@endsection

@section('script')
<script src="{{ asset('assets/vendor/clipboard-js/dist/clipboard.min.js') }}"></script>
<script>
var owner = {
  id: {{ Auth::user()->id }},
  name: '{{ $ownerDetail['user']['chat_name'] }}',
  room: '{{ $room_name }}',
  roomId: '{{ $songId }}',
  roomPhoto: '{{ $photo['small'] }}'
};
</script>
<script src="{{ mix_url('dist/js/controller/ownerstatsctrl.js') }}"></script>
<script>
// Send Download App Link From Modal
$('.form-download-app').on('submit', function(e) {
  $form = $(this);

  email_download = $form.find('.email-download').val();
  phone_download = $form.find('.phone-download').val();

  if (email_download == '') {
    return false;
  } else {
    $.ajax({
      type: 'POST',
      url: '/garuda/download/link',
      headers: {
        'Content-Type': 'application/json',
        'X-GIT-Time': '1406090202',
        Authorization: 'GIT WEB:WEB'
      },
      data: JSON.stringify({
        _token: $('meta[name="csrf-token"]').attr('content'),
        user_phone: phone_download,
        user_email: email_download
      }),
      crossDomain: true,
      dataType: 'json',
      success: function(data) {
        $('#download-app').modal('hide');
        $('#modalDownload').modal('hide');
        $('#subSuccess').modal('show');
      }
    });
  }
});
</script>

@if($enable_chat)
  @include('web.@script.sendbird-widget')
@endif

@include('web.@script.aws-kds-logged-user-tracker')

@endsection
