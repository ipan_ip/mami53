@extends('web.owner.ownerpage.ownerPageLayout')

@section('meta-custom')
	<title>Halaman Akun Pemilik - Mamikos</title>
@endsection

@section('style')
	<style>
		#launcher {
			display: none;
		}
	</style>
@endsection

@section('content')

		<div id="app">
			<app></app>
		</div>

@endsection

@section('script')
	@include('web.@script.global-auth')

	@if($enable_chat)
		@include('web.@script.sendbird-widget', ['hide_element' => 'true'])
		@include('web.@script.zendesk-chat-widget')
	@endif

	<script>
		var AB_TEST_URL = "{{config('abtest.base_url').config('abtest.path_url')}}";
		var AB_TEST_API_KEY = "{{config('abtest.api_key')}}";
		var AB_TEST_API_AUTHORIZATION = "{{config('abtest.authorization')}}";
	</script>

	<script type="text/javascript">
		var data = @JZON($package);
	</script>		

	@if (App::environment('frontend'))
		<script src="/dist/js/owner-page.js" async defer></script>
	@else
		<script src="{{ mix_url('dist/js/owner-page.js') }}" async defer></script>
	@endif

	@if ($is_midtrans_production)
		<script src="https://app.midtrans.com/snap/snap.js" data-client-key="{{ $midtrans_token }}"></script>
	@else
		<script src="https://app.sandbox.midtrans.com/snap/snap.js" data-client-key="{{ $midtrans_token }}"></script>
	@endif

	@include('web.@script.aws-kds-logged-user-tracker')
@endsection
