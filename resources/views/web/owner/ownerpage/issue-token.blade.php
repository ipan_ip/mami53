<body>
    <script>
        var oauthToken= @json($oauth);
        var oxOauth2Domain= '{{ config('owner.oauth2_domain') }}';
        document.cookie = "access_token="+oauthToken.access_token+";samesite=strict;path=/;domain="+oxOauth2Domain+";max-age=604800";
        document.cookie = "refresh_token="+oauthToken.refresh_token+";samesite=strict;path=/;domain="+oxOauth2Domain+";max-age=604800";
        window.location.replace('{{$target}}');
    </script>
</body>