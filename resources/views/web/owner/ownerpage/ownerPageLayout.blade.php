<!DOCTYPE html>
<html>
<head>
  @if($_SERVER['HTTP_HOST'] != 'mamikos.com')
  <meta name="robots" content="noindex, nofollow">
  @endif
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <meta name="application-name" content="Mamikos">
  <meta name="theme-color" content="#1BAA56">
  <link rel="manifest" href="/json/manifest.json">

  @include('web.@meta.link-prebrowsing')

  <base href="/">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <meta name="fragment" content="!">
  
  <meta name="author" content="https://plus.google.com/u/1/+Mamikos/posts">
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Expires" content="0" />

  @include('web.@meta.favicon')
  
  @yield('meta-custom')

  <!--  Fonts, Icons and CSS
  ================================================== -->
  @include('web.part.external.global-head')
  <link href="{{ mix_url('dist/css/common.css') }}" rel="stylesheet">

  @yield('style')

</head>
<body>
  @include('web.part.external.gtm-body')

  <!-- Navbar -->

  @yield('content')

  @include ('web.part.modalloginuser')

  <!--  JavaScripts
  ================================================== -->
  <script src="{{ mix_url('dist/js/common.js') }}"></script>
  @include ('web.part.script.bugsnagapikey')

  @yield('script')

  @include ('web.part.script.tokenerrorhandlescript')
  @include ('web.part.script.navbarscript')
  @include ('web.part.script.browseralertscript')
</body>
</html>