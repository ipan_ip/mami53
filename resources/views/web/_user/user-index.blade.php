@extends('web.@layout.globalLayout')

@section('head')
	@if (!empty($_SERVER['HTTP_HOST']))
		@if ($_SERVER['HTTP_HOST'] == 'mamikos.com')
			<meta name="robots" content="index, follow">
		@else
			<meta name="robots" content="noindex, nofollow">
		@endif
	@else
		<meta name="robots" content="noindex, nofollow">
	@endif

	<title>Profil Akun Pengguna - Mamikos</title>
	<meta name="description" content="Ubah atau perbarui data profil Anda di sini.">
	<link rel="canonical" href="https://mamikos.com/user">

	<meta property="og:url" content="https://mamikos.com/user">
	<meta property="og:title" content="Profil Akun Pengguna - Mamikos">
	<meta property="og:image" content="/general/img/pictures/share-image-default.jpg">
	<meta property="og:description" content="Ubah atau perbarui data profil Anda di sini.">

	<meta name="twitter:url" content="https://mamikos.com/user">
	<meta name="twitter:title" content="Profil Akun Pengguna - Mamikos">
	<meta name="twitter:description" content="Ubah atau perbarui data profil Anda di sini.">
	<meta name="twitter:image" content="/general/img/pictures/share-image-default.jpg">

	<meta name="keyphrases" content="Info Kost, Cari Kost, Sewa Kost, Kost Bebas, Kost Murah, Kost pasutri, Aplikasi Kost, Aplikasi Pencarian Kost, Aplikasi Info Kost, APlikasi Cari Kost, Kost, Mamikost, Mamikosapp, Kamar Kost, Kamar Kos, Kostan, Kos, Rumah Kost, Rumah Kos, Kost Harian">

	<meta name="classification" content="Business, Rent House, Sewa Kost, Property, Rent Room, Info Kost, Information, Kost, Room, Cari Kost, Kost Murah, Kost Bebas, Application, Mobile Application, Kamar Kost, Kamar Kos, Kostan, Kos, Rumah Kost, Rumah Kos, Kost Harian">

	<meta name="keywords" content="mamikost, Info Kost, Cari kost, kost, Kamar Kost, Kamar Kos, Kostan, Kos, Rumah Kost, Rumah Kos, Kost Harian">

	<link rel="preload" href="{{ mix_url('dist/js/_user/app.js') }}" as="script">
@endsection

@section('data')

@endsection


@section('styles')
	@include('web.@style.preloading-style')
@endsection

@section('content')
	@include('web.@style.preloading-content')

	<div id="app">
		<app></app>
	</div>
@endsection


@section('scripts')

	@if (Auth::check() and Auth::user()->is_owner == 'false')
			@include('web.@script.sendbird-widget')
	@endif

	<script src="{{ mix_url('dist/vendor/moment/moment.js') }}"></script>

	<script src="{{ mix_url('dist/js/_user/app.js') }}" async defer></script>

	@include('web.@script.aws-kds-logged-user-tracker')
@endsection
