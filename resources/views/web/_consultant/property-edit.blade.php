@extends('web.input.inputLayout')

@section('meta-custom')
  <title>Form Edit Kost MamiKos - Cari Kost Gampang & Akurat</title>

  <meta name="description" content="Form Edit Kost MamiKos - Cari Kost Gampang & Akurat. Promosikan Kost Mu Gratis di Mamikos.com!">
  <meta property="og:title" content="Form Edit Kost MamiKos - Cari Kost Gampang & Akurat" />
  <meta property="og:description" content="Form Edit Kost MamiKos - Cari Kost Gampang & Akurat. Promosikan Kost Mu Gratis di Mamikos.com!" />
  <meta property="og:image" content="/assets/mamikos_form.png" />
  <meta name="twitter:title" content="Form Edit Kost MamiKos - Cari Kost Gampang & Akurat">
  <meta name="twitter:description" content="Form Edit Kost MamiKos - Cari Kost Gampang & Akurat. Promosikan Kost Mu Gratis di Mamikos.com!">
  <meta name="twitter:image" content="/assets/mamikos_form.png">
	<meta name="property_type" content="Kost" />
  <meta name="property_index" content="{{ $id }}" />

  @include('web.@style.preloading-style')
@endsection

@section('content')
    @include('web.@style.preloading-content')

  <div id="app" class="wrapper">
  	<app></app>
  </div>
@endsection

@section('script')
  @include('web.@script.global-auth')

  <script>
    window.isConsultantOnly = true;
  </script>

  <script src="{{ mix_url('dist/js/input-edit.js') }}"></script>
@endsection
