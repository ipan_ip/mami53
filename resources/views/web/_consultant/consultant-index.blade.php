@extends('web.@layout.globalLayout')

@section('head')
	<meta name="robots" content="noindex, nofollow">

	<title>Consultant Tools Management - Mamikos</title>
	<meta name="description" content="Consultant Tools Management - Mamikos">
	<link rel="canonical" href="https://mamikos.com/consultant/">

	<meta property="og:url" content="https://mamikos.com/consultant/">
	<meta property="og:title" content="Consultant Management Tool - Mamikos">
	<meta property="og:image" content="/general/img/pictures/share-image-default.jpg">
	<meta property="og:description" content="Description for this page.">

	<meta name="twitter:url" content="https://mamikos.com/consultant/">
	<meta name="twitter:title" content="Consultant Management Tool - Mamikos">
	<meta name="twitter:description" content="Description for this page.">
	<meta name="twitter:image" content="/general/img/pictures/share-image-default.jpg">

	<link rel="preload" href="{{ mix_url('dist/js/_consultant/app.js') }}" as="script">

@endsection

@section('data')

@endsection


@section('styles')
	@include('web.@style.preloading-style')
@endsection


@section('content')
	@include('web.@style.preloading-content')

	<div id="app">
		<app></app>
	</div>
@endsection


@section('scripts')
	<script src="{{ mix_url('dist/js/_consultant/app.js') }}" async defer></script>
@endsection
