<script>
window.addEventListener(
	'load',
	function(load) {
		if ('serviceWorker' in navigator ) {
			navigator.serviceWorker.register('/serviceworker.js').then(
				function(registration) {
					console.log(
						'ServiceWorker registration successful with scope: ',
						registration.scope
					);
				},
				function(err) {
					console.log('ServiceWorker registration failed: ', err);
				}
			);
		} else {
			if ('userAgent' in navigator) {
				tracker('ga', ['send', 'event', {
					eventCategory: 'Unsupported UserAgents',
					eventAction: 'serviceWorker',
					eventLabel: window.navigator.userAgent
				}]);
			}
		}
	},
	false
);
</script>

<script>
window.addEventListener(
	'load',
	function(load) {
		if ('serviceWorker' in navigator ) {
			//commenting for temporary experiment purpose
			// window.addEventListener('beforeinstallprompt', function(e) {
			// 	e.preventDefault();
			// });

			window.addEventListener('appinstalled', function(e) {
				tracker('ga', ['send', 'event', {
					eventCategory: 'A2HS',
					eventAction: 'install',
					eventLabel: 'User installed the A2HS'
				}]);
			});
		}
	},
	false
);
</script>
