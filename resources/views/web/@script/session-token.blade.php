<script>
setInterval(function() {
	function reqListener(e) {
		console.log('Session is extended.');
	}

	var dripReq = new XMLHttpRequest();
	dripReq.addEventListener('load', reqListener);
	dripReq.open('GET', '/drip');
	dripReq.send();
}, 60 * 30 * 1000);
</script>

@if (session('flash-message'))
	<script>
	swal({
		title: 'Perhatian',
		text: '{{ session('flash-message') }}',
		type: 'warning',
		confirmButtonColor: '#EC4A0C'
	});
	</script>
@endif
