<script>
  function trackAuthenticatedUser(lastLogin) {
    tracker('logger', [
      '[Web] Open App when Login',
      {
        timestamp: currentUTCTime(),
        name: authData.all.name,
        phone_number: authData.all.phone_number,
        email: authData.all.email,
        last_login: lastLogin,
        is_owner: authCheck.owner,
        device: window.innerWidth < 768 ? 'mobile' : 'desktop'
      }
    ]);
  }

  function initKinesisAuth() {
    var now = new Date();
    var kinesisAuthSession = 
      Cookies && Cookies.get('kinesis-auth-session') 
        ? Cookies.get('kinesis-auth-session')
        : null;
    var kinesisAuthLastLogin = null;

    if (isStorageAvailable('localStorage')) {
      kinesisAuthLastLogin = localStorage.getItem('kinesis-auth-last-login');
    }

    if (authCheck.all) {
      if (kinesisAuthLastLogin) {
        if (!kinesisAuthSession) {
          Cookies.set('kinesis-auth-session', 1, { expires: 1 });

          // {{--  track user every time session expired --}}
          trackAuthenticatedUser(kinesisAuthLastLogin); 
        }
      } else {
        if (!kinesisAuthLastLogin) {
          if (isStorageAvailable('localStorage')) {
            localStorage.setItem('kinesis-auth-last-login', currentUTCTime());
          }

          // {{--  track user's first time login at a time --}}
          trackAuthenticatedUser(currentUTCTime());
        } 

        if (!kinesisAuthSession) {
          Cookies.set('kinesis-auth-session', 1, { expires: 1 });
        }
      }
    } else {
      if (kinesisAuthLastLogin) {
        if (isStorageAvailable('localStorage')) {
          localStorage.removeItem('kinesis-auth-last-login');
        }
      }

      if (kinesisAuthSession) {
        Cookies.remove('kinesis-auth-session');
      }
    }
  }

  document.onreadystatechange = function () {
    if (document.readyState === 'complete') {
      initKinesisAuth();
    }
  }
</script>
