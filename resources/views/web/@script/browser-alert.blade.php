<script>
window.addEventListener(
	'load',
	function(load) {
		if (axios) {
			axios.interceptors.response.use(
				function(response) {
					return response;
				},
				function(error) {
					if (error.response) {
						if (error.response.status == 401) {
							try {
								swal({
									title: '',
									text: 'Session Anda telah habis, coba muat ulang halaman',
									type: 'warning',
									confirmButtonColor: '#ec4a0c'
								});
							} catch (e) {
								alert('Session Anda telah habis, coba muat ulang halaman');
							}
						} else if (error.response.status == 429) {
							try {
								swal({
									title: '',
									text: 'Terlalu banyak request, coba beberapa saat lagi.',
									type: 'warning',
									confirmButtonColor: '#ec4a0c'
								});
							} catch (e) {
								alert('Terlalu banyak request, coba beberapa saat lagi.');
							}
						}
					}
				}
			);
		}
	},
	false
);
</script>

<script>
function isUsingOldIE() {
	var ua = window.navigator.userAgent;
	var msie = ua.indexOf('MSIE ');

	return msie > 0;
}

if (
	!Object.defineProperty ||
	typeof Object.defineProperty !== 'function' ||
	isUsingOldIE()
) {
	if (
		confirm(
			'Maaf, browser yang Anda gunakan kurang optimal untuk menjalankan aplikasi web Mamikos.\nSilakan perbaharui atau gunakan internet browser lainnya untuk kenyamanan Anda dalam menggunakan fitur-fitur kami.'
		)
	) {
		window.open('https://updateoutdatedbrowser.com/');
	}
}
</script>

<noscript>
	<strong>
		Maaf, website Mamikos tidak berfungsi dengan baik tanpa JavaScript yang aktif. Silakan aktifkan untuk melanjutkan.
	</strong>
	<hr/>
	<strong>
		We're sorry but Mamikos doesn't work properly without JavaScript enabled. Please enable it to continue.
	</strong>
</noscript>
