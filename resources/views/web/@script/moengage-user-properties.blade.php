<script>
	function getInterface() {
		return window.innerWidth < 768 ? 'mobile' : 'desktop';
	}

	function getFormattedDateTime(val) {
		var pad = new Date(val);
    var date = String(pad.getUTCDate()).padStart(2, '0');
    var month = String(pad.getUTCMonth() + 1).padStart(2, '0');
    var year = pad.getUTCFullYear();
    var hours = String(pad.getUTCHours()).padStart(2, '0');
    var minutes = String(pad.getUTCMinutes()).padStart(2, '0');
    var seconds = String(pad.getUTCSeconds()).padStart(2, '0');

    pad =
      year +
      '-' +
      month +
      '-' +
      date +
			'T' +
      hours +
      ':' +
      minutes +
      ':' +
      seconds;

    return pad;
	}

	function initMoengageUserAuth() {
		var moengageAuth = null;

		if (isStorageAvailable('localStorage')) {
			moengageAuth = localStorage.getItem('moengage-auth');
		}

		if (authCheck.all && moengageAuth != 1) {
			Moengage.add_unique_user_id(parseInt(authData.all.id));
			Moengage.add_email(authData.all.email);
			Moengage.add_mobile(authData.all.phone_number);
			Moengage.add_user_name(authData.all.name);
			Moengage.add_gender(authData.all.gender);
			Moengage.add_user_attribute('created_at', new Date(authData.all.created_at));
			Moengage.add_user_attribute('updated_at', authData.all.updated_at);
			Moengage.add_user_attribute('is_owner', authCheck.owner);
			Moengage.add_user_attribute('birthday', authData.all.birthday);
			Moengage.add_user_attribute('age', authData.all.age);
			Moengage.add_user_attribute('city', authData.all.city);
			Moengage.add_user_attribute('job', authData.all.job);
			Moengage.add_user_attribute('job_place', authData.all.work_place);
			Moengage.add_user_attribute('last_education', authData.all.education);
			Moengage.add_user_attribute('marital_status', authData.all.marital_status);
			Moengage.add_user_attribute('description', authData.all.description);
			Moengage.add_user_attribute(
				'phone_number_additional',
				authData.all.phone_number_additional
			);

			if (authCheck.user) {
				Moengage.add_user_attribute('property_name', null);
				Moengage.add_user_attribute('property_city', null);
				Moengage.add_user_attribute('property_subdistrict', null);

				var params = {
					current_page: window.location.pathname,
					current_property: null,
					success_status: true,
					fail_reason: null,
					login_platform: (!authData.all.fb_education || authData.all.fb_education == '-') && (!authData.all.fb_location || authData.all.fb_location == '-') ?
						'Google' : 'Facebook',
					id: parseInt(authData.all.id),
					username: authData.all.name,
					phone_number: authData.all.phone_number,
					email_address: authData.all.email,
					is_limitedcontent: sessionStorage.getItem('moengage-detail-limited-content') == 'true' ? true : false,
					created_At: getFormattedDateTime(authData.all.created_at),
					interface: getInterface()
				};

				tracker('moe', ['[User] Login', params]);

				if (isStorageAvailable('localStorage')) {
					localStorage.setItem('moengage-auth-type', 'user');
				}
			} else if (authCheck.owner) {
				Moengage.add_user_attribute('is_promoted', null);
				Moengage.add_user_attribute('is_premium', null);
			}

			if (isStorageAvailable('localStorage')) {
				localStorage.setItem('moengage-auth', 1);
			}
		} else if (!authCheck.all && moengageAuth == 1) {
			var moengageAuthType = null;

			if (isStorageAvailable('localStorage')) {
				moengageAuthType = localStorage.getItem('moengage-auth-type');
			}

			if (moengageAuthType == 'user') {
				var params = {
					current_page: window.location.pathname,
					current_property: null,
					success_status: true,
					interface: getInterface()
				};

				tracker('moe', ['[User] Logout', params]);

				if (isStorageAvailable('localStorage')) {
					localStorage.removeItem('moengage-auth-type');
				}
			} else {
				var params = {
					logout_result: true
				};

				tracker('moe', ['[Owner] Logout', params]);
			}

			Moengage.destroy_session();

			if (isStorageAvailable('localStorage')) {
				localStorage.removeItem('moengage-auth');
			}
		}
	}

	window.addEventListener(
		'load',
		function(load) {
			initMoengageUserAuth();
		},
		false
	);
</script>