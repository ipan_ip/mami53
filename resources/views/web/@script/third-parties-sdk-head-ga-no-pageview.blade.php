@include('web.@script.google-analytics-no-pageview')

@include('web.@script.gtm-head')

@include('web.@script.common-config')

@include('web.@script.moengage-sdk')

@include('web.@script.tracking-wrapper')

{{-- AddThis style//don't remove --}}
<style>
@media (max-width:991px){.addthis_bar{font-family:Lato,sans-serif!important}.addthis_bar .addthis_bar_message{font-size:14px!important;font-family:Lato,sans-serif!important}.addthis_bar .at-cv-button,.addthis_bar .at-cv-input{font-size:12px!important;font-family:Lato,sans-serif!important}}@media (min-width:992px){.addthis_bar.addthis_bar_bottom{display:none!important;visibility:hidden!important}}
</style>
