<script>
	var authCheck = {
		all: false,
		user: false,
		owner: false,
		admin: false
	};
</script>

<script>
	@if (Auth::check())
		authCheck.all = true;

		@if (Auth::user()->is_owner == 'false' && Auth::user()->role != 'administrator')
				authCheck.user = true;
		@elseif (Auth::user()->is_owner == 'false' && Auth::user()->role == 'administrator')
				authCheck.admin = true
		@elseif (Auth::user()->is_owner == 'true')
				authCheck.owner = true;
		@endif

	@endif
</script>

<script>
	var authData = {
		all: null
	};
</script>

<script>
	@if (Auth::check())
		authData = {
			all: @JZON(Auth::user())
		};

		authData.all.photo_user = '{{ (Auth::user()->photo_user) }}';
		authData.all.consultant = @JZON(Auth::user()->consultant);

		@if (isset(Auth::user()->consultant))
			authData.all.roles = @JZON(Auth::user()->consultant->roles);
		@else
			authData.all.roles = null;
		@endif

	@endif
</script>

<script>
	var oxWebUrl= '{{ config('owner.dashboard_url') }}';
	var oxOauth2Domain= '{{ config('owner.oauth2_domain') }}';
</script>
@include('web.@script.moengage-user-properties')
