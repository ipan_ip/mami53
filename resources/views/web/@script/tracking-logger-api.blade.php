<script>
  function currentUTCTime() {
    var today = new Date();
    var date = String(today.getUTCDate()).padStart(2, '0');
    var month = String(today.getUTCMonth() + 1).padStart(2, '0');
    var year = today.getUTCFullYear();
    var hours = String(today.getUTCHours()).padStart(2, '0');
    var minutes = String(today.getUTCMinutes()).padStart(2, '0');
    var seconds = String(today.getUTCSeconds()).padStart(2, '0');
    var milliseconds = String(today.getUTCMilliseconds()).padStart(3, '0');

    today =
      year +
      '-' +
      month +
      '-' +
      date +
      ' ' +
      hours +
      ':' +
      minutes +
      ':' +
      seconds +
      '.' +
      milliseconds;

    return today;
  }

  function sendEventToLogger(params, callback) {
    if (params[0] === 'send' && params[1] === 'pageview') {
      return;
    }

    var recordData = [];
    var userId = authCheck.all ? authData.all.id : null;
    var deviceId =
      Cookies && Cookies.get('USER_DATA')
        ? JSON.parse(Cookies.get('USER_DATA')).deviceUuid
        : null;
    var sessionId =
      localStorage
      && localStorage.getItem('MOE_DATA')
      && JSON.parse(localStorage.getItem('MOE_DATA')).SESSION
        ? JSON.parse(localStorage.getItem('MOE_DATA')).SESSION.sessionKey
        : null;

    var trackingData = JSON.stringify({
      user: {
          user_id: userId,
          device_id: deviceId,
          session_id: sessionId,
          platform: window.innerWidth < 768 ? 'Web Mobile' : 'Web Desktop',
          app_version:
            '{{ \App\Libraries\PackageHelper::getApplicationVersion() }}'
        },
        e: params[0],
        event_time: currentUTCTime(),
        a: typeof params[1] === 'object' ? params[1] : params[2],
        URL: window.location.href
    });

    var apiLoggerUrl = '{{ ENV('MIX_API_LOGGER_URL') }}';

    makeRequest(
      'POST',
      apiLoggerUrl,
      trackingData,
      params,
      callback
    );
  }

  function makeRequest(method, url, body, eventParams, callback) {
    try {
      var request = new XMLHttpRequest();

      request.open(method, url);
      request.setRequestHeader('Content-Type', 'application/json');
      request.setRequestHeader('x-api-key', '6QizyeWpdD3LXAs9f4CwE1zPrTY7EbNr4DVEGp82');

      request.onerror = function() {
        var error = {
          response: request.responseText || '',
          status: request.statusText || ''
        };
        console.error('onerror', error);
        sendErrorToBugsnag('tracker_logger', error, eventParams);
      };

      request.onreadystatechange = function() {
        if (request.status !== 200) {
          var error = {
            response: request.responseText || '',
            status: request.statusText || ''
          };
          console.error('onreadystatechange', error);
          sendErrorToBugsnag('tracker_logger', error, eventParams);
        }

        if (typeof callback === 'function') {
          callback(request)
        }
      };

      request.send(body);
    } catch (error) {
      sendErrorToBugsnag('tracker_logger', error);
    }
  }

  function sendErrorToBugsnag(source, error, params) {
    bugsnagClient.notify(
      JSON.stringify({
        source: source,
        error: error,
        event_params: params
      })
    );
  }
</script>
