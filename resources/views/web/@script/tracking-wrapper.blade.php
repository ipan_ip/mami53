  {{--
  moengage params: 
    - eventName
    - eventAttribute
  ref: https://docs.moengage.com/docs/tracking-website-events
  
  google analytic params:
    - command, 
    - fields
    - fieldsObject
  ref: https://developers.google.com/analytics/devguides/collection/analyticsjs/command-queue-reference

  How to call tracker:
  tracker('type', [params1, params2, .., paramsN]);
  
  Please check tracker documentation for passing params. 

  e.g:
  - Moengage: 
    tracker('moe', [
				'[User] Booking - Click Booking',
				{
					property_type: this.detailTypeName ? this.detailTypeName : 'kost',
					property_name: _get(this.detail, 'room_title', null),
				}
      ]);
  - Google Analytics:
      tracker('ga', [
				'send',
				'event',
				{
					eventCategory: 'Search v2',
					eventAction: 'suggestions-request',
					eventLabel: keyword
				}
			]);
--}}

@include('web.@script.tracking-logger-api')

<script>
  function tracker(type, params, callback) {
    var trackers = {
      moe: function() {
        // {{-- Send Event Tracking to Moe & Logger API --}}
        Moengage.track_event.apply(Moengage, params);
        if (typeof sendEventToLogger === 'function') {
          sendEventToLogger(params);
        }
        // {{-- Dump Event Tracking Name to GA for Logs --}}
        ga('send', {
          hitType: 'event',
          eventCategory: 'Event Tracking Log',
          eventAction: 'Record',
          eventLabel: params[0]
        });
      },
      ga: function() {
        // {{-- Send Event Tracking to GA & Logger API --}}
        ga.apply(window, params);
      },
      logger: function() {
        // {{-- Send Event Tracking to Logger API only --}}
        if (typeof sendEventToLogger === 'function') {
          sendEventToLogger(params, callback);
        }
      }
    };
    trackers[type]();
  };
</script>