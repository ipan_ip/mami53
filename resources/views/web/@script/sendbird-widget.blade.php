<style>
	@media (max-width: 767px) {
		#sb_widget {
			right: 999999px;
		}
	}
	#sb_widget > .widget {
		visibility: hidden;
	}
</style>

@if(!isset($hide_element))
	<div id="sb_widget"></div>
@endif

<script src="{{ mix_url('dist/vendor/sendbird-chat/SendBird.min.js') }}"></script>
<script src="{{ mix_url('dist/vendor/sendbird-chat/widget.SendBird.js') }}"></script>




<script>
	@if(isset($chat_name) && !is_null($chat_name))
		var ownerChatName = '{{ $chat_name }}';
	@else
		var ownerChatName = null;
	@endif

	@if(isset($hide_element))
		var isWidgetReady = false;
	@else
		var isWidgetReady = true;
	@endif
</script>

<script>
var sendbirdAppId = '{{ config('sendbird.app_id') }}';
var sendbirdUserId = authData.all.id.toString();
var sendbirdUserName = ownerChatName ? ownerChatName : authData.all.name;
var sendbirdCAT = authData.all.chat_access_token;
var sb = new SendBird({ appId: sendbirdAppId });

var connectSendBird = function(callback){
	if (sendbirdCAT) {
		sb.connect(sendbirdUserId, authData.all.chat_access_token, function(
			user,
			error
		) {
			if (error) {
				console.error(error);
				return;
			}
			callback && callback();
		});
	} else {
		sb.connect(sendbirdUserId, function(user, error) {
			if (error) {
				console.error(error);
				return;
			}
			callback && callback()
		});
	}
};

var startSendBirdWidget = function(callback){
	connectSendBird();

	sbWidget.startWithConnect(
		sendbirdAppId,
		sendbirdUserId,
		sendbirdUserName,
		function() {
			// handle auto open chat widget from query string url
			try {
				var queryString = new URLSearchParams(window.location.search);

				if (queryString.get('open-chat') === 'true') {
					document.querySelector('#sb_widget .widget').click();
				}
			} catch (e) {
				console.error(e);
			}

			callback && callback();
		}
	);
};

if (sendbirdUserId !== '0' && isWidgetReady) {
	startSendBirdWidget();
};
</script>
