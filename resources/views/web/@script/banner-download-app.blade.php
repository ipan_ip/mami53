<style>
	#banner-download-app #banner-download {
		width: 240px;
		margin: 0 auto;
		background-color: #60ac56;
		color: #fff;
		opacity: 0;
		display: none;
		border-radius: 100px;
		margin-bottom: 11.3rem;
		padding: 0rem 2.9rem;
		line-height: 3.9rem;
		cursor: pointer;
	}

	#banner-download-app .banner-text {
		width: 88%;
		padding-left: 1.4rem;
		font-weight: 600;
	}

	#banner-download-app #area-close {
		border-left: 1px solid #fff;
		padding-left: 2rem;
		font-weight: 600;
	}

	#banner-download-app #area-close > a.close-button {
		color: #fff;
	}

	#banner-download-app {
		display: none;
		width: 100%;
		position: fixed;
		bottom: 0;
		z-index: 999999;
	}

	@media (max-width: 768px) {
		#banner-download-app {
			display: block;
		}
	}
</style>

<div id="banner-download-app">
	<div id="banner-download">
		<div id="download-apps" class="banner-text">
			Download Aplikasi
		</div>
		<div id="area-close" class="banner-close">
			<a class="close-button">X</a>
		</div>
	</div>
</div>

<script>
var bannerDownload = document.getElementById('download-apps');
var bannerDownloadClose = document.getElementById('area-close');
var bannerWrapper = document.getElementById('banner-download');

window.addEventListener('scroll', function (e) {
	try {
		checkLastScroll();
	} catch (err) {}
});

function checkLastScroll() {
	if (bannerWrapper.style.display != 'flex') {
		var banner = localStorage.getItem('banner-download-app');
		if (banner != 'no') {
			var heightBody = document.body.scrollHeight;
			var heightPercentScroll = (getEndYScroll() / heightBody) * 100;
			if (heightPercentScroll > 60) {
				bannerWrapper.style.transition = "opacity 2s";
				bannerWrapper.style.display = "flex";
				setTimeout(function() {
				bannerWrapper.style.opacity = "1";
				}, 1000);
			}
		}
	}
}

function getEndYScroll() {
	var supportPageOffset = window.pageXOffset !== undefined;
	var isCSS1Compat = ((document.compatMode || "") === "CSS1Compat");
	var y = supportPageOffset ? window.pageYOffset : isCSS1Compat ? document.documentElement.scrollTop : document.body.scrollTop;
	return y;
}

bannerDownloadClose.addEventListener('click', function () {
	actionClose();
});

function actionClose() {
	bannerWrapper.style.display = "none";
	localStorage.setItem('banner-download-app', 'no');
}

bannerDownload.addEventListener('click', function () {
	window.open('/app?utm_campaign=DownloadAppFloatingbanner&utm_source=DownloadAppFloatingbanner&utm_medium=DownloadAppFloatingbanner&utm_term=DownloadAppFloatingbanner');
	actionClose();
});

</script>
