{{-- Object Assign polyfill --}}
<script>
"function"!==typeof Object.assign&&Object.defineProperty(Object,"assign",{value:function(b,f){if(null===b||void 0===b)throw new TypeError("Cannot convert undefined or null to object");for(var e=Object(b),c=1;c<arguments.length;c++){var a=arguments[c];if(null!==a&&void 0!==a)for(var d in a)Object.prototype.hasOwnProperty.call(a,d)&&(e[d]=a[d])}return e},writable:!0,configurable:!0});
</script>

{{-- classList polyfill --}}
<script>
"classList"in document.documentElement||!Object.defineProperty||"undefined"==typeof HTMLElement||Object.defineProperty(HTMLElement.prototype,"classList",{get:function(){var e=this;function n(n){return function(t){var s=e.className.split(/\s+/),i=s.indexOf(t);n(s,i,t),e.className=s.join(" ")}}var t={add:n(function(e,n,t){~n||e.push(t)}),remove:n(function(e,n){~n&&e.splice(n,1)}),toggle:n(function(e,n,t){~n?e.splice(n,1):e.push(t)}),contains:function(n){return!!~e.className.split(/\s+/).indexOf(n)},item:function(n){return e.className.split(/\s+/)[n]||null}};return Object.defineProperty(t,"length",{get:function(){return e.className.split(/\s+/).length}}),t}});
</script>

{{-- String includes polyfill --}}
<script>
String.prototype.includes||(String.prototype.includes=function(t,e){"use strict";return"number"!=typeof e&&(e=0),!(e+t.length>this.length)&&-1!==this.indexOf(t,e)});
</script>

{{-- Node child remove polyfill --}}
<script>
[Element.prototype,CharacterData.prototype,DocumentType.prototype].forEach(function(e){e.hasOwnProperty("remove")||Object.defineProperty(e,"remove",{configurable:!0,enumerable:!0,writable:!0,value:function(){this.parentNode.removeChild(this)}})});
</script>

{{-- Array​ for​Each polyfill --}}
<script>
Array.prototype.forEach||(Array.prototype.forEach=function(c){var d,a;if(null==this)throw new TypeError("this is null or not defined");var b=Object(this),e=b.length>>>0;if("function"!==typeof c)throw new TypeError(c+" is not a function");1<arguments.length&&(d=arguments[1]);for(a=0;a<e;){if(a in b){var f=b[a];c.call(d,f,a,b)}a++}});
</script>

{{-- Array​ from polyfill --}}
<script>
Array.from||(Array.from=function(){var r=Object.prototype.toString,n=function(n){return"function"==typeof n||"[object Function]"===r.call(n)},t=Math.pow(2,53)-1,e=function(r){var n=function(r){var n=Number(r);return isNaN(n)?0:0!==n&&isFinite(n)?(n>0?1:-1)*Math.floor(Math.abs(n)):n}(r);return Math.min(Math.max(n,0),t)};return function(r){var t=Object(r);if(null==r)throw new TypeError("Array.from requires an array-like object - not null or undefined");var o,a=arguments.length>1?arguments[1]:void 0;if(void 0!==a){if(!n(a))throw new TypeError("Array.from: when provided, the second argument must be a function");arguments.length>2&&(o=arguments[2])}for(var i,u=e(t.length),f=n(this)?Object(new this(u)):new Array(u),c=0;c<u;)i=t[c],f[c]=a?void 0===o?a(i,c):a.call(o,i,c):i,c+=1;return f.length=u,f}}());
</script>

{{-- String padStart polyfill --}}
<script>
String.prototype.padStart||(String.prototype.padStart=function(t,i){return t>>=0,i=String(void 0!==i?i:" "),this.length>t?String(this):((t-=this.length)>i.length&&(i+=i.repeat(t/i.length)),i.slice(0,t)+String(this))});
</script>

{{-- String Object.entries polyfill --}}
<script>
Object.entries||(Object.entries=function(e){for(var r=Object.keys(e),t=r.length,n=new Array(t);t--;)n[t]=[r[t],e[r[t]]];return n});
</script>