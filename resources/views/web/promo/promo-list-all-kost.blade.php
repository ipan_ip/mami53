@extends('web.@layout.globalLayout')

@section('head')
	@if (!empty($_SERVER['HTTP_HOST']))
		@if ($_SERVER['HTTP_HOST'] == 'mamikos.com')
			<meta name="robots" content="index, follow">
		@else
			<meta name="robots" content="noindex, nofollow">
		@endif
	@else
		<meta name="robots" content="noindex, nofollow">
	@endif

	<title>Info Kost di Indonesia Putra dan Putri Bulanan Murah</title>
	<meta name="twitter:title" content="Info Kost Harian Bulanan Putra dan Putri - Mamikos">
	<meta name="twitter:description" content="info kost harian & bulanan, putra, putri campur bebas 24 jam dengan harga terbaik, gunakan kode promo untuk harga lebih hemat. bisa booking sekarang juga hanya di Mamikos.com">
	<meta name="twitter:image" content="/assets/og/og_kost_v2.jpg">
	<meta property="og:description" content="info kost harian & bulanan, putra, putri campur bebas 24 jam dengan harga terbaik, gunakan kode promo untuk harga lebih hemat. bisa booking sekarang juga hanya di Mamikos.com" />
	<meta property="og:image" content="/assets/og/og_kost_v2.jpg" />
	<meta name="description" content="info kost harian & bulanan, putra, putri campur bebas 24 jam dengan harga terbaik, gunakan kode promo untuk harga lebih hemat. bisa booking sekarang juga hanya di Mamikos.com" />
	<meta name="keywords" content="kost putra, kost putri, kost pasutri, kost campur, kost bebas, kost harian, kost mingguan, kost bulanan, kost tahunan">
	<link rel="canonical" href="https://mamikos.com{{ $_SERVER['REQUEST_URI'] }}">

	<link rel="preload" href="{{ mix_url('dist/js/_promo/all-kost/app.js') }}" as="script">
@endsection


@section('data')
<script type="text/javascript">
	var landingType = 'all-kost';
	var landingTitle = 'Info Kost Harian Bulanan Putra dan Putri';

	var urlParams = {
		cityFilter : '{{ $cityFilter }}',
		genderFilter: '{{ $genderFilter }}'.split(',').map(Number),
		rentTypeFilter: {{ $rentTypeFilter }},
	};

	var cityList = null;
	var cityLabel = urlParams.cityFilter;

	var selectDefault = {
		label : cityLabel
	};

	var cityTitle = '';

	if (cityLabel == 'Semua Kota') {
		selectDefault.value = 'all';
	} else {
		selectDefault.value = cityLabel.replace(' ','-').toLowerCase();
		cityTitle = cityLabel;
	}


	// TITLE GENERATOR
	var genderOption = {
		'0,1,2' : 'Putra Dan Putri',
		'2' : 'Khusus Putri',
		'1' : 'Khusus Putra',
		'0,2' : 'Putri dan Campur',
		'0,1' : 'Putra dan Campur'
	};
	var rentTypeList = ['Harian', 'Mingguan', 'Bulanan', 'Tahunan'];
	var rentType = rentTypeList[urlParams.rentTypeFilter];
	var titleLocation =
		urlParams.cityFilter === 'Semua Kota' || urlParams.cityFilter === 'All'
		? 'Indonesia'
		: urlParams.cityFilter;

	document.title =
		'Info Kosan ' +
		titleLocation + ' ' +
		genderOption[urlParams.genderFilter.toString()] + ' ' +
		rentType +
		' - Mamikos';

	// GET ARTICLE
	var article = '{!! $article !!}';

</script>
@endsection

@section('styles')
	@include('web.@style.preloading-style')
@endsection

@section('content')
	@include('web.@style.preloading-content')
	{{-- Complete Content After JS Rendered --}}
	<div id="app">
		<app></app>
	</div>
@endsection

@section('scripts')
	@include('web.@script.global-auth')
	
	@if (Auth::check() and Auth::user()->is_owner == 'false')
			@include('web.@script.sendbird-widget')
	@endif

	<script src="{{ mix_url('dist/js/_promo/all-kost/app.js') }}" async defer></script>
	@include('web.@script.aws-kds-logged-user-tracker')
@endsection