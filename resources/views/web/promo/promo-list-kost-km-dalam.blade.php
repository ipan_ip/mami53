@extends('web.@layout.globalLayout')

@section('head')
	@if (!empty($_SERVER['HTTP_HOST']))
		@if ($_SERVER['HTTP_HOST'] == 'mamikos.com')
			<meta name="robots" content="index, follow">
		@else
			<meta name="robots" content="noindex, nofollow">
		@endif
	@else
		<meta name="robots" content="noindex, nofollow">
	@endif

	<title>Promo kost kamar mandi dalam, Pesan sekarang lebih hemat - Mamikos</title>

	<meta name="twitter:title" content="Promo kost harian, Pesan sekarang lebih hemat.">
	<meta name="twitter:description" content="Promo kost harian, bebas, pasutri, kamar mandi dalam murah Terlengkap & Termurah Hanya di Sini, Cek Juga Berbagai Promonya.">
	<meta name="twitter:image" content="/assets/og/og_kost_v2.jpg">
	<meta property="og:description" content="Promo kost harian, bebas, pasutri, kamar mandi dalam murah Terlengkap & Termurah Hanya di Sini, Cek Juga Berbagai Promonya." />
	<meta property="og:image" content="/assets/og/og_kost_v2.jpg" />
	<meta name="description" content="Promo kost harian, bebas, pasutri, kamar mandi dalam murah Terlengkap & Termurah Hanya di Sini, Cek Juga Berbagai Promonya." />
	<meta name="keywords" content="kost bebas, kost kamar mandi dalam, promo kost, kost harian bebas, kost bulanan bebas, kost bebas putra, kost bebas putri">
	<link rel="canonical" href="https://mamikos.com{{ $_SERVER['REQUEST_URI'] }}">
	<link rel="preload" href="{{ mix_url('dist/js/_promo/app.js') }}" as="script">

	<link rel="preload" href="{{ mix_url('dist/js/_promo/app.js') }}" as="script">
@endsection

@section('data')
	@yield('data-default')
	<script type="text/javascript">
		var landingType = 'kost-km-dalam';
		var landingTitle = 'Promo Kost Kamar Mandi Dalam, Pesan Sekarang Lebih Hemat';
		var promoSlug = '{{ $city }}';
		var cityList = null;
		var cityValue = promoSlug.replace(' ','-').toLowerCase();

		var selectDefault = {
			value : cityValue
		};

		var cityTitle = '';

		if (promoSlug == 'all') {
			selectDefault.label = 'Semua Kota';
		} else {
			selectDefault.label = promoSlug;
			cityTitle = promoSlug;
		}

		var article = '{!! $article !!}';
	</script>
@endsection

@section('styles')
	@include('web.@style.preloading-style')
@endsection

@section('content')
	@include('web.@style.preloading-content')
	{{-- Complete Content After JS Rendered --}}
	<div id="app">
		<app></app>
	</div>
@endsection

@section('scripts')
	@include('web.@script.global-auth')
	
	@if (Auth::check() and Auth::user()->is_owner == 'false')
			@include('web.@script.sendbird-widget')
	@endif

	<script src="{{ mix_url('dist/js/_promo/app.js') }}" async defer></script>
@endsection