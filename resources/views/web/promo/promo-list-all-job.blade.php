@extends('web.promo.promoLayout')

@section('meta-custom')


<title> {{ $metaTitle }} - Mamikos</title>

<meta name="twitter:title" content="Lowongan Kerja Terbaru {{ $month_calendar }} - Mamikos.">
<meta name="twitter:description" content="{{ $metaDescription }}">
<meta name="twitter:image" content="/assets/og/og_loker.jpg">
<meta property="og:description" content="{{ $metaDescription }}" />
<meta property="og:image" content="/assets/og/og_loker.jpg" />
<meta name="description" content="{{ $metaDescription }}" />

<meta name="keywords" content="lowongan kerja, info lowongan kerja, lowongan kerja terbaru">

<link rel="canonical" href="https://mamikos.com/loker" />

<link rel="preload" href="{{ mix_url('dist/js/promo-list-all-job.js') }}" as="script">

<style type="text/css">
	#prerender {
		margin-top: 100px;
	}
	#prerender .prerender-header {
		height: 120px;
	}

	#prerender .prerender-fill {
		background-color: #eee;
		height: 100%;
		border-radius: 5px;
	}
	#prerender .prerender-content {
		height: 300px;
		margin-top: 100px;
		margin-bottom: 50px;
	}

	@media (max-width: 767px) {
		#prerender .prerender-header {
			height: 200px;
		}
	}

	@media (min-width: 768px) {
		#prerender {
			padding-left: 100px;
			padding-right: 100px;
		}
	}

	#prerender .prerender-footer {
		margin-bottom: 100px;
	}
</style>

<script>
		var dataLayerParams;

    try {
      dataLayerParams = new URLSearchParams(window.location.search);
    } catch (e) {
      dataLayerParams = null;
    };

    var dataLayerProps = {
      device: "none",
      utm_source: "none",
      utm_medium: "none",
      utm_content: "none",
      utm_campaign: "none",
      source: "none",
      source_medium: "none",
      source_content: "none",
      source_campaign: "none",
      status: @JZON($jobTypeFilter)
    };

    if (window.innerWidth < 768) {
      dataLayerProps.device = "mobile";
    } else {
      dataLayerProps.device = "desktop";
    };

    try {
      dataLayerProps.utm_source = dataLayerParams.get('utm_source');
      if (dataLayerProps.utm_source === null) dataLayerProps.utm_source = "none";
    } catch (e) {
      dataLayerProps.utm_source = "none";
    };

    try {
      dataLayerProps.utm_medium = dataLayerParams.get('utm_medium');
      if (dataLayerProps.utm_medium === null) dataLayerProps.utm_medium = "none";
    } catch (e) {
      dataLayerProps.utm_source = "none";
    };

    try {
      dataLayerProps.utm_content = dataLayerParams.get('utm_content');
      if (dataLayerProps.utm_content === null) dataLayerProps.utm_content = "none";
    } catch (e) {
      dataLayerProps.utm_content = "none";
    };

    try {
      dataLayerProps.utm_campaign = dataLayerProps.dataLayerParams.get('utm_campaign');
      if (dataLayerProps.utm_campaign === null) dataLayerProps.utm_campaign = "none";
    } catch (e) {
      dataLayerProps.utm_campaign = "none";
    };

		
    dataLayer.push({
      "language": "id",
      "page_category": "ImpressionLoker",
      "currency": "IDR",
      "device": dataLayerProps.device,
      "utm_source": dataLayerProps.utm_source,
      "utm_medium": dataLayerProps.utm_medium,
      "utm_content": dataLayerProps.utm_content,
      "utm_campaign": dataLayerProps.utm_campaign,
      "page_url": "https://mamikos.com/loker",
      "step": 1,
      "status": dataLayerProps.status,
      @if (Auth::check())
      "customer_id": "{{ (Auth::user()->id) }}",
      "profile_id": "{{ (Auth::user()->id) }}",
      "gender": "{{ (Auth::user()->gender) }}"
      @else
      "customer_id": "none",
      "profile_id": "none",
      "gender": "none"
      @endif
    });
</script>
@endsection

@section('data')

<script>
  var educationBasicList = {
    'all': 'all',
    'sma_smk': 'high_school',
    'diploma': 'diploma',
    'sarjana': 'bachelor',
    'master': 'master',
    'doktor': 'doctor',
    'mahasiswa': 'college',
    'fresh_graduate': 'fresh_grad'
  };


  var jobTypeOptions = @JZON($jobTypeOptions);
  var educationOptions = @JZON($educationOptions);


  var article = '{!! $article !!}';
  var metaTitle = '{{ $metaTitle }}';
  var urlParams = {
    monthCalendar : '{{ $month_calendar }}',
    cityFilter : '{{ $cityFilter }}',
    jobTypeFilter : '{{ $jobTypeFilter }}',
    lastEducationFilter : educationBasicList['{{ $lastEducationFilter }}'],
    criteriaFilter : '{{ $userCriteria }}'
  };
</script>

@endsection

@section('content')

	<div id="prerender">
		<section>
			<div class="prerender-header col-md-12 col-sm-12 col-xs-12">
				<div class="prerender-fill"></div>
			</div>

			<div class="prerender-content col-md-12 col-xs-12 col-sm-12">
				<div class="prerender-fill"></div>
			</div>

			<div class="prerender-footer col-md-12 col-sm-12 col-xs-12">
				<div class="prerender-article">
					{!! $article !!}
				</div>
			</div>
		</section>
	</div>

	{{-- Complete Content After JS Rendered --}}
	<div id="app">
		<container-promo-list></container-promo-list>
	</div>
@endsection

@section('script')
  @include('web.@script.global-auth')

  <script src="{{ mix_url('dist/js/promo-list-all-job.js') }}" ></script>

  @include('web.@script.aws-kds-logged-user-tracker')
@endsection