@extends('web.promo.promoLayout')

@section('meta-custom')

<title>{{ $metaTitle }}</title>

<meta name="twitter:title" content="{{ $metaTitle }}" />
<meta name="twitter:description" content="{{ $metaDescription }}" />
<meta name="twitter:image" content="/assets/og/og_apartemen.jpg">
<meta property="og:description" content="{{ $metaDescription }}" />
<meta property="og:image" content="/assets/og/og_apartemen.jpg" />
<meta name="description" content="{{ $metaDescription }}" />
<meta name="keywords" content="sewa apartemen, sewa apartemen harian, sewa apartemen bulanan, sewa apartemen tahunan">


<link rel="canonical" href="https://mamikos.com/apartemen" />

<link rel="preload" href="{{ mix_url('dist/js/promo-list-all-apartment.js') }}" as="script">

<style type="text/css">
	#prerender {
		margin-top: 100px;
	}
	#prerender .prerender-header {
		height: 120px;
	}

	#prerender .prerender-fill {
		background-color: #eee;
		height: 100%;
		border-radius: 5px;
	}
	#prerender .prerender-content {
		height: 300px;
		margin-top: 100px;
		margin-bottom: 50px;
	}

	@media (max-width: 767px) {
		#prerender .prerender-header {
			height: 200px;
		}
	}

	@media (min-width: 768px) {
		#prerender {
			padding-left: 100px;
			padding-right: 100px;
		}
	}

	#prerender .prerender-footer {
		margin-bottom: 100px;
	}
</style>

<script>
	var dataLayerParams;

    try {
      dataLayerParams = new URLSearchParams(window.location.search);
    } catch (e) {
      dataLayerParams = null;
    };

    var device;
    var utm_source;
    var utm_medium;
    var utm_content;
    var utm_campaign;

    if (window.innerWidth < 768) {
      device = "mobile";
    } else {
      device = "desktop";
    };

    try {
      utm_source = dataLayerParams.get('utm_source');
      if (utm_source === null) utm_source = "none";
    } catch (e) {
      utm_source = "none";
    };

    try {
      utm_medium = dataLayerParams.get('utm_medium');
      if (utm_medium === null) utm_medium = "none";
    } catch (e) {
      utm_source = "none";
    };

    try {
      utm_content = dataLayerParams.get('utm_content');
      if (utm_content === null) utm_content = "none";
    } catch (e) {
      utm_content = "none";
    };

    try {
      utm_campaign = dataLayerParams.get('utm_campaign');
      if (utm_campaign === null) utm_campaign = "none";
    } catch (e) {
      utm_campaign = "none";
    };

		
    dataLayer.push({
      "language": "id",
      "page_category": "ImpressionUnit",
      "currency": "IDR",
      "device": device,
      "utm_source": utm_source,
      "utm_medium": utm_medium,
      "utm_content": utm_content,
      "utm_campaign": utm_campaign,
      "page_url": "https://mamikos.com/apartemen",
      "step": 1,
      @if (Auth::check())
      "customer_id": "{{ (Auth::user()->id) }}",
      "profile_id": "{{ (Auth::user()->id) }}",
      "gender": "{{ (Auth::user()->gender) }}"
      @else
      "customer_id": "none",
      "profile_id": "none",
      "gender": "none"
      @endif
    });
</script>

@endsection

@section('data')

<script>
  var article = '{!! $article !!}';
	var metaTitle = '{{ $metaTitle }}';
	var furnishedFilter = '{{ $isFurnishedFilter }}';
  var urlParams = {
    cityFilter : '{{ $cityFilter }}',
    isFurnishedFilter : furnishedFilter == 'all' ? furnishedFilter : parseInt(furnishedFilter),
    rentTypeFilter : {{ $rentTypeFilter }},
    criteriaFilter : '{{ $userCriteria }}'
  };
  var areaFilter = @JZON($area_filter);

</script>

@endsection

@section('content')

	<div id="prerender">
		<section>
			<div class="prerender-header col-md-12 col-sm-12 col-xs-12">
				<div class="prerender-fill"></div>
			</div>

			<div class="prerender-content col-md-12 col-xs-12 col-sm-12">
				<div class="prerender-fill"></div>
			</div>

			<div class="prerender-footer col-md-12 col-sm-12 col-xs-12">
				<div class="prerender-article">
					{!! $article !!}
				</div>
			</div>
		</section>
	</div>

{{-- Complete Content After JS Rendered--}}
<div id="app">
	<container-promo-list></container-promo-list>
</div>
@endsection

@section('script')
  @include('web.@script.global-auth')

  <script src="{{ mix_url('dist/js/promo-list-all-apartment.js') }}"></script>

  @include('web.@script.aws-kds-logged-user-tracker')
@endsection