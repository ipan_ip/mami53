@extends('web.@layout.globalLayout')

@section('head')
	@if (!empty($_SERVER['HTTP_HOST']))
		@if ($_SERVER['HTTP_HOST'] == 'mamikos.com')
			<meta name="robots" content="index, follow">
		@else
			<meta name="robots" content="noindex, nofollow">
		@endif
	@else
		<meta name="robots" content="noindex, nofollow">
	@endif

	@if($title == '')
	<title>Booking Kost Tidak Ditemukan - Mamikos</title>
	@else
	<title>{{ $title }}</title>
	@endif
	<meta name="twitter:title" content="{{ $title }}">
	<meta name="twitter:description" content="{{ $description }}">
	<meta name="twitter:image" content="/assets/og/og_kost_v2.jpg">
	<meta property="og:description" content="{{ $description }}" />
	<meta property="og:image" content="/assets/og/og_kost_v2.jpg" />
	<meta name="description" content="{{ $description }}" />
	<meta name="keywords" content="{{ $keyword }}">
	<link rel="canonical" href="https://mamikos.com{{ $_SERVER['REQUEST_URI'] }}">

	<link rel="preload" href="{{ mix_url('dist/js/_promo/app.js') }}" as="script">
@endsection

@section('data')
	@yield('data-default')
	<script type="text/javascript">
		var landingType = 'booking';
		var landingTitle = 'Mudah cari kostnya, Mudah bookingnya, Mudah bayarnya.';

		var promoSlug = '{{ $city }}';
		var parentCity = '{{ $parentCity }}';

		var cityList = @JZON($cityList);
		var cityLabel = promoSlug.replace('-',' ');
		cityLabel = cityLabel.charAt(0).toUpperCase() + cityLabel.slice(1).toLowerCase();

		var parentLanding = @JZON($parent_landing);
		var parentId = '{{ $parent_id }}';
		var subLocation = @JZON($location);
		var filters = @JZON($filters);

		var selectDefault = {
			value: promoSlug
		};

		var cityTitle = '';

		if (promoSlug == 'all') {
			selectDefault.label = 'Semua Kota';
		} else {
			selectDefault.label = cityLabel;
			cityTitle = cityLabel;
		}

		var article = '{!! $article !!}';
	</script>
@endsection

@section('styles')
	@include('web.@style.preloading-style')
@endsection

@section('content')
	@include('web.@style.preloading-content')
	{{-- Complete Content After JS Rendered --}}
	<div id="app">
		<app></app>
	</div>
@endsection

@section('scripts')
	@include('web.@script.global-auth')
	
	@if (Auth::check() and Auth::user()->is_owner == 'false')
			@include('web.@script.sendbird-widget')
	@endif

	<script src="{{ mix_url('dist/js/_promo/app.js') }}" async defer></script>
	@include('web.@script.aws-kds-logged-user-tracker')
@endsection