@extends('web.landing-modern.landingModernLayout')

@section('meta-custom')
	<title>MAMIKOS KOST - NETWORK</title>

	<meta name="description" content="Tingkatkan penghasilan kost jadi 3 kali lipat dengan MAMIKOS Network. Peningkatan layanan, harga sewa dan okupansi yang diberikan MAMIKOS bisa meningkatkan harga properti anda dan membuat areal sekitar kost andapun meningkat potensi investasinya." />
	<meta name="keywords" content="Mamikos, Info Kost, Cari kost, kost, Kamar Kost, Kamar Kos, Kostan, Kos, Rumah Kost, Rumah Kos, Kost Harian">
	<meta name="og:site_name" content="Mamikos"/>
	<meta property="og:description" content="Tingkatkan penghasilan kost jadi 3 kali lipat dengan MAMIKOS Network. Peningkatan layanan, harga sewa dan okupansi yang diberikan MAMIKOS bisa meningkatkan harga properti anda dan membuat areal sekitar kost andapun meningkat potensi investasinya." />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="{{ Request::url() }}" />
	<meta property="og:image" content="/assets/share-image-default.jpg" />
	<meta property="fb:app_id" content="607562576051242"/>
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:site" content="@mamikosapp">
	<meta name="twitter:title" content="Mamikos - Cari Kost Gampang & Akura">
	<meta name="twitter:description" content="Tingkatkan penghasilan kost jadi 3 kali lipat dengan MAMIKOS Network. Peningkatan layanan, harga sewa dan okupansi yang diberikan MAMIKOS bisa meningkatkan harga properti anda dan membuat areal sekitar kost andapun meningkat potensi investasinya.">
	<meta name="twitter:creator" content="@mamikosapp">
	<meta name="twitter:image" content="/assets/share-image-default.jpg">
	<meta name="twitter:domain" content="mamikos.com">

	<meta name="classification" content="Business, Rent House, Sewa Kost, Property, Rent Room, Info Kost, Information, Kost, Room, Cari Kost, Kost Murah, Kost Bebas, Application, Mobile Application, Kamar Kost, Kamar Kos, Kostan, Kos, Rumah Kost, Rumah Kos, Kost Harian">
	<meta name="distribution" content="global">
  <meta http-equiv="Reply-to" content="mamikos.app@gmail.com">
  <meta name="revisit-after" content="1 day">
  <meta name="keyphrases" content="Info Kost, Cari Kost, Sewa Kost, Kost Bebas, Kost Murah, Kost pasutri, Aplikasi Kost, Aplikasi Pencarian Kost, Aplikasi Info Kost, APlikasi Cari Kost, Kost, Mamikost, Mamikosapp, Kamar Kost, Kamar Kos, Kostan, Kos, Rumah Kost, Rumah Kos, Kost Harian">
@endsection

@section('data')
	<script type="text/javascript">
		//
	</script>
@endsection

@section('content')
  	<div id="app">
  		<app></app>
  	</div>
@endsection

@section('script')
	@include('web.@script.global-auth')

	<script src="{{ mix_url('dist/js/landing-standard.js') }}"></script>
@endsection
