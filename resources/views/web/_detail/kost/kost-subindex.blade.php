@extends('web._detail.detail-index')


@section('head-meta')
	<title>{{ $meta['title'] }}</title>
	<meta name="description" content="{{ $meta['description'] }}">
	<link rel="canonical" href="https://mamikos.com/room/{{ $detail['slug'] }}">

	<meta property="og:url" content="https://mamikos.com/room/{{ $detail['slug'] }}">
	<meta property="og:title" content="{{ $meta['title'] }}">
	<meta property="og:image" content="{{ $meta['image_url'] }}">
	<meta property="og:image:alt" content="Foto {{ $meta['title'] }}">
	<meta property="og:description" content="{{ $meta['description'] }}">

	<meta name="twitter:url" content="https://mamikos.com/room/{{ $detail['slug'] }}">
	<meta name="twitter:title" content="{{ $meta['title'] }}">
	<meta name="twitter:description" content="{{ $meta['description'] }}">
	<meta name="twitter:image" content="{{ $meta['image_url'] }}">

	<meta name="keyphrases" content="Info Kost, Cari Kost, Sewa Kost, Kost Bebas, Kost Murah, Kost pasutri, Aplikasi Kost, Aplikasi Pencarian Kost, Aplikasi Info Kost, APlikasi Cari Kost, Kost, Mamikost, Mamikosapp, Kamar Kost, Kamar Kos, Kostan, Kos, Rumah Kost, Rumah Kos, Kost Harian">

	<meta name="classification" content="Business, Rent House, Sewa Kost, Property, Rent Room, Info Kost, Information, Kost, Room, Cari Kost, Kost Murah, Kost Bebas, Application, Mobile Application, Kamar Kost, Kamar Kos, Kostan, Kos, Rumah Kost, Rumah Kos, Kost Harian">

    <meta name="keywords" content="{{$meta['keywords']}}">

	<link rel="alternate" href="android-app://com.git.mami.kos/mamikos/room/{{ $detail['slug'] }}" />

	<meta name="this-id" content="{{ $detail['_id'] }}">
	<meta name="load-time" content="{{ date('Y-m-d H:i:s', time()) }}">
@endsection


@section('data-default')
	<script>
		var price_card_info = {
			available_room: @JZON($detail['available_room']),
			updated_at: @JZON($detail['updated_at']),
			{{-- price_daily: @JZON($detail['price_daily']),
			price_weekly: @JZON($detail['price_weekly']),
			price_monthly: @JZON($detail['price_monthly']),
			price_quarterly: @JZON($detail['price_quarterly']),
			price_semiannualy: @JZON($detail['price_semiannualy']),
			price_yearly: @JZON($detail['price_yearly']), --}}
			price: @JZON($detail['price_title_formats']),
			fac_price_icon: @JZON($detail['fac_price_icon']),
			fac_keyword: @JZON($detail['fac_keyword']),
			min_month: @JZON($detail['min_month']),
			promotion: @JZON($detail['promotion'])
		};

		var detail = @JZON($detail);
		detail.type = 'kost';

		var detailTypeName = 'Kost';
		
		var AB_TEST_URL = "{{config('abtest.base_url').config('abtest.path_url')}}";
		var AB_TEST_API_KEY = "{{config('abtest.api_key')}}";
		var AB_TEST_API_AUTHORIZATION = "{{config('abtest.authorization')}}";
	</script>

	@include('web._detail.partial.detail-data-layer')
@endsection

@section('extra-script')
	@include('web.@script.aws-kds-logged-user-tracker')
@endsection
