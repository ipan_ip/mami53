@extends('web._detail.detail-index')


@section('head-meta')

	<title>{{ $detail['name_slug'] }} - Mamikos </title>

	<meta name="csrf-token" content="{{ csrf_token() }}">

  <meta name="this-id" content="{{ $detail['seq'] }}">
  <meta name="load-time" content="{{date('Y-m-d H:i:s', time())}}">

  @if ($detail['is_indexed'] == 1)
  	<link rel="canonical" href="{{ $detail['share_url'] }}" />
  @endif

  <meta name="description" content="Sewa {{ $detail['name_slug'] }}. Sewa langsung dari pemilik. Terlengkap & lebih dari 8 juta database apartemen disewakan, cek sekarang di Mamikos.com">

  <meta name="keywords" content="Sewa {{ $detail['name_slug'] }} murah, harian, bulanan, tahunan.">

  <meta property="og:title" content="{{ $detail['name_slug'] }}" />
  <meta property="og:description" content="Sewa {{ $detail['name_slug'] }} murah. Dengan alamat di {{ $detail['project']['address'] }}. Sewa {{ $detail['name_slug'] }} murah, harian, bulanan, tahunan. Klik di sini untuk info lengkapnya, hanya di Mamikos.com" />
  <meta property="og:url" content="{{ $detail['share_url'] }}" />
  <meta property="og:image" content="{{ $detail['photo_url']['medium'] }}" />

  <meta name="twitter:title" content="{{ $detail['name_slug'] }}">
  <meta name="twitter:description" content="Sewa {{ $detail['name_slug'] }} murah. Dengan alamat di {{ $detail['project']['address'] }}. Sewa {{ $detail['name_slug'] }} murah, harian, bulanan, tahunan. Klik di sini untuk info lengkapnya, hanya di Mamikos.com">
  <meta name="twitter:image" content="{{ $detail['photo_url']['medium'] }}">

  <script>
    window.addEventListener(
      'load',
      function() {
        if (!window.fbq || typeof fbq === 'undefined') {
          window.fbq = function() {
            return null;
          };
        } else {
          var city = detail.area_city;
          var region = detail.area_subdistrict;

          if (city === '' && region === '') {
            city = detail.location.toString();
            region = detail.location.toString();
          }

          dataLayer.push({
            'event': 'ViewProduct',
            'ecommerce': {
              'currencyCode': 'IDR',
              'detail': {
                'actionField': {
                  'list': 'Home'
                },
                'products': {
                  'name': detail.name_slug,
                  'id': detail._id,
                  'price': detail.price_title,
                  'type': 'hotel',
                  'city': city,
                  'region': region,
                  'category': 'Apartement',
                  'brand': detail.name_slug,
                }

              }
            }
          });
        }
      },
      false
    );
  </script>

@endsection


@section('data-default')
	<script>
		var detail = @JZON($detail);
		detail.type = 'apartment';

		var detailTypeName = 'Apartemen';
  </script>
  
	@include('web.@script.aws-kds-logged-user-tracker')
@endsection
