@extends('web._detail.detail-index')


@section('head-meta')
	<title>{{ $detail['name'] }}</title>
	<meta name="description" content="Info {{ $detail['name'] }}. Dapatkan info kontrakan murah dengan fasilitas lengkap hanya di Mamikos.com!">
	<link rel="canonical" href="https://mamikos.com/disewakan/{{ $detail['slug'] }}">

	<meta property="og:url" content="https://mamikos.com/disewakan/{{ $detail['slug'] }}">
	<meta property="og:title" content="{{ $detail['name'] }}">
	<meta property="og:image" content="{{ $detail['photo_url']['medium'] }}">
	<meta property="og:image:alt" content="Foto {{ $detail['name_slug'] }}">
	<meta property="og:description" content="Info {{ $detail['name'] }}. Dapatkan info kontrakan murah dengan fasilitas lengkap hanya di Mamikos.com!">

	<meta name="twitter:url" content="https://mamikos.com/disewakan/{{ $detail['slug'] }}">
	<meta name="twitter:title" content="{{ $detail['name'] }}">
	<meta name="twitter:description" content="Info {{ $detail['name'] }}. Dapatkan info kontrakan murah dengan fasilitas lengkap hanya di Mamikos.com!">
	<meta name="twitter:image" content="{{ $detail['photo_url']['medium'] }}">

	@if (is_null($detail['area_subdistrict']) || is_null($detail['area_city']) || is_null($detail['area_big']))
		<meta name="keywords" content="rumah kontrakan, sewa kontrakan, sewa rumah, kontrakan murah, info kontrakan">
	@else
		<meta name="keywords" content="rumah kontrakan, sewa kontrakan, sewa rumah {{ $detail['area_subdistrict'] }} {{ $detail['area_city'] }} {{ $detail['area_big'] }}, {{ $detail['area_subdistrict'] }} {{ $detail['area_city'] }} {{ $detail['area_big'] }}, kontrakan murah {{ $detail['area_subdistrict'] }} {{ $detail['area_city'] }} {{ $detail['area_big'] }}, sewa kontrakan {{ $detail['area_subdistrict'] }} {{ $detail['area_city'] }} {{ $detail['area_big'] }}, info kontrakan {{ $detail['area_subdistrict'] }} {{ $detail['area_city'] }} {{ $detail['area_big'] }}">
	@endif

	<link rel="alternate" href="android-app://com.git.mami.kos/mamikos/disewakan/{{ $detail['slug'] }}" />

	<meta name="this-id" content="{{ $detail['_id'] }}">
	<meta name="load-time" content="{{ date('Y-m-d H:i:s', time()) }}">
@endsection


@section('data-default')
	<script>
		var priceDailyRaw = @JZON($detail['price_daily']);
		var priceWeeklyRaw = @JZON($detail['price_weekly']);
		var priceMonthlyRaw = @JZON($detail['price_monthly']);
		var priceYearlyRaw = @JZON($detail['price_yearly']);

		var price_card_info = {
			available_room: null,
			updated_at: @JZON($detail['updated_at']),

			price: {
				price_daily: {
					price: priceDailyRaw.toLocaleString('id')
				},
				price_weekly: {
					price: priceWeeklyRaw.toLocaleString('id')
				},
				price_monthly: {
					price: priceMonthlyRaw.toLocaleString('id')
				},
				price_yearly: {
					price: priceYearlyRaw.toLocaleString('id')
				}
			},

			fac_price_icon: @JZON($detail['fac_price_icon']),
			min_month: @JZON($detail['min_month']),
			fac_keyword: null,
			promotion: null
		};

		var detail = @JZON($detail);
		detail.type = 'house';

		var detailTypeName = 'Kontrakan';
	</script>
@endsection
