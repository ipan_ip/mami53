@extends('web.@layout.globalLayout')


@section('head')
	@if (!empty($_SERVER['HTTP_HOST']))
		@if ($_SERVER['HTTP_HOST'] == 'mamikos.com')
			<meta name="robots" content="{{ $detail['index_status'] }}">
		@else
			<meta name="robots" content="noindex, nofollow">
		@endif
	@else
		<meta name="robots" content="noindex, nofollow">
	@endif

	@yield('head-meta')

	<link rel="preload" href="{{ mix_url('dist/js/@vendor/detail-booking/script.js') }}" as="script">

	@if (isset($detail['has_photo_round']) && $detail['has_photo_round'])
		<link rel="preload" href="{{ mix_url('dist/vendor/photo-sphere-viewer/photo-sphere-viewer.css') }}" as="style">

		<link rel="preload" href="{{ mix_url('dist/vendor/photo-sphere-viewer/photo-sphere-viewer.js') }}" as="script">
	@endif

	<link rel="preload" href="{{ mix_url('dist/js/_detail/app.js') }}" as="script">

	@if (isset($detail['has_photo_round']) && $detail['has_photo_round'])
		<link rel="stylesheet" href="{{ mix_url('dist/vendor/photo-sphere-viewer/photo-sphere-viewer.css') }}">
	@endif

@endsection


@section('data')

	@yield('data-default')

@endsection


@section('styles')
	@include('web.@style.preloading-style')
@endsection


@section('content')
	@include('web.@style.preloading-content')

	<div id="app">
		<app></app>
	</div>
@endsection


@section('scripts')
	@if (Auth::check())
		@if (Auth::user()->is_owner == 'false')
			@include('web.@script.sendbird-widget')
		@endif
	@endif

	<script src="{{ mix_url('dist/js/@vendor/detail-booking/script.js') }}"></script>

	@if (isset($detail['has_photo_round']) && $detail['has_photo_round'])
		<script src="{{ mix_url('dist/vendor/photo-sphere-viewer/photo-sphere-viewer.js') }}"></script>
	@endif

	<script src="{{ mix_url('dist/js/_detail/app.js') }}" async defer></script>

	@include('web._detail.partial.detail-marketing-script')

	@include('web._detail.partial.detail-schema-org')

	@yield('extra-script')
@endsection
