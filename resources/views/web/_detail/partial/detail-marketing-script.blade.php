<!-- Google Code for Remarketing Tag -->
<script type="text/javascript">
var google_tag_params = {
	listing_id: {{ $detail['_id'] }},
	listing_pagetype: 'offerdetail',
	@if (is_null($detail['price_title']) || $detail['price_title'] == '')
		listing_totalvalue: 0,
	@else
		listing_totalvalue: {{ $detail['price_title'] }},
	@endif
};
</script>
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 938973445;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>

<noscript>
	<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/938973445/?guid=ON&amp;script=0"/>
	</div>
</noscript>

<script>
	//Prevent the iframe for causing UI bug
	try {
		document.querySelector('iframe[name=google_conversion_frame]').style.display = 'none';
	} catch(e) {}
</script>
