<script type="application/ld+json">
	{
		"@context": "http://schema.org",
		"@type": "BreadcrumbList",
		"itemListElement": [
			@foreach ($detail['breadcrumbs'] as $key => $breadcrumb)
				{
					"@type": "ListItem",
					"position": {{ $key + 1 }},
					"item": {
						"@id": "{{ $breadcrumb["url"] }}",
						"name": "{{ $breadcrumb["name"] }}"
					}
				}
				@if ($key != count($detail['breadcrumbs']) - 1)
				,
				@endif
			@endforeach
		]
	}
</script>

@if (isset($detail['type']) && $detail['type'] == 'villa')
	<script type="application/ld+json">
		{
			"@context" : "http://schema.org",
			"@type" : "Hotel",
			"hasMap" : "https://maps.google.com/maps?z=7&q={{ $detail['latitude'] }},{{ $detail['longitude'] }}",
			"description" : "Sewa {{ $detail['name_slug'] }} - Mamikos",
			"address" : {
				@if ($detail['area_subdistrict'] == "")
				"streetAddress" : "{{ $detail['address'] }}, {{ $detail['area_city'] }}, Indonesia",
				@elseif ($detail['area_city'] == "")
				"streetAddress" : "{{ $detail['address'] }}, {{ $detail['area_subdistrict'] }}, Indonesia",
				@else
				"streetAddress" : "{{ $detail['address'] }}, {{ $detail['area_subdistrict'] }}, {{ $detail['area_city'] }}, Indonesia",
				@endif
				"addressLocality" : "{{ $detail['area_subdistrict'] }}",
				"addressRegion" : "{{ $detail['area_city'] }}",
				"addressCountry" : "Indonesia",
				"@type" : "PostalAddress"
			},
			"telephone": "0856-4140-5202",
			"url" : "https://mamikos.com/sewa/{{ $detail['slug'] }}",
			"priceRange" : "Rp {{ $detail['price_title'] }} - Rp {{ $detail['price_title'] }}",
			"name" : "{{ $detail['name'] }}",
			"image" : "{{ $detail['photo_url']['medium'] }}"
		}
	</script>
@elseif (isset($detail['type']) && $detail['type'] == 'kontrakan')
	<script type="application/ld+json">
		{
			"@context" : "http://schema.org",
			"@type" : "Hotel",
			"hasMap" : "https://maps.google.com/maps?z=7&q={{ $detail['latitude'] }},{{ $detail['longitude'] }}",
			"description" : "Sewa {{ $detail['name_slug'] }} - Mamikos",
			"address" : {
				@if ($detail['area_subdistrict'] == "")
				"streetAddress" : "{{ $detail['address'] }}, {{ $detail['area_city'] }}, Indonesia",
				@elseif ($detail['area_city'] == "")
				"streetAddress" : "{{ $detail['address'] }}, {{ $detail['area_subdistrict'] }}, Indonesia",
				@else
				"streetAddress" : "{{ $detail['address'] }}, {{ $detail['area_subdistrict'] }}, {{ $detail['area_city'] }}, Indonesia",
				@endif
				"addressLocality" : "{{ $detail['area_subdistrict'] }}",
				"addressRegion" : "{{ $detail['area_city'] }}",
				"addressCountry" : "Indonesia",
				"@type" : "PostalAddress"
			},
			"telephone": "0856-4140-5202",
			"url" : "https://mamikos.com/disewakan/{{ $detail['slug'] }}",
			"priceRange" : "Rp {{ $detail['price_title'] }} - Rp {{ $detail['price_title'] }}",
			"name" : "{{ $detail['name'] }}",
			"image" : "{{ $detail['photo_url']['medium'] }}"
		}
	</script>
@else
	<script type="application/ld+json">
		{
			"@context" : "http://schema.org",
			"@type" : "Hotel",
			"hasMap" : "https://maps.google.com/maps?z=7&q={{ $detail['latitude'] }},{{ $detail['longitude'] }}",
			"description" : "{{ $detail['name_slug'] }} - Mamikos",
			"address" : {
				@if ($detail['area_subdistrict'] == "")
				"streetAddress" : "{{ $detail['area_city'] }}, Indonesia",
				@elseif ($detail['area_city'] == "")
				"streetAddress" : "{{ $detail['area_subdistrict'] }}, Indonesia",
				@else
				"streetAddress" : "{{ $detail['area_subdistrict'] }}, {{ $detail['area_city'] }}, Indonesia",
				@endif
				"addressLocality" : "{{ $detail['area_subdistrict'] }}",
				"addressRegion" : "{{ $detail['area_city'] }}",
				"addressCountry" : "Indonesia",
				"@type" : "PostalAddress"
			},
			"telephone": "0856-4140-5202",
			"url" : "https://mamikos.com/room/{{ $detail['slug'] }}",
			"priceRange" : "Rp {{ $detail['price_title'] }} - Rp {{ $detail['price_title'] }}",
			"name" : "{{ $detail['room_title'] }}",
			"image" : "{{ $detail['photo_url']['medium'] }}"
		}
	</script>
@endif