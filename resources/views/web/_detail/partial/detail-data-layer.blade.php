<script>
var dataLayerParams;

try {
	dataLayerParams = new URLSearchParams(window.location.search);
} catch (e) {
	dataLayerParams = null;
}

var dataLayerProps = {
	device: 'none',
	utm_source: 'none',
	utm_medium: 'none',
	utm_content: 'none',
	utm_campaign: 'none',
	type_room: 'none',
	city: 'none',
	area: 'none',
	price_day: '0',
	price_week: '0',
	price_month: '0',
	image_url: 'none',
	rating: 'none'
};

if (window.innerWidth < 768) {
	dataLayerProps.device = 'mobile';
} else {
	dataLayerProps.device = 'desktop';
}

try {
	dataLayerProps.utm_source = dataLayerParams.get('utm_source');
	if (dataLayerProps.utm_source === null) dataLayerProps.utm_source = 'none';
} catch (e) {
	dataLayerProps.utm_source = 'none';
}

try {
	dataLayerProps.utm_medium = dataLayerParams.get('utm_medium');
	if (dataLayerProps.utm_medium === null) dataLayerProps.utm_medium = 'none';
} catch (e) {
	dataLayerProps.utm_source = 'none';
}

try {
	dataLayerProps.utm_content = dataLayerParams.get('utm_content');
	if (dataLayerProps.utm_content === null) dataLayerProps.utm_content = 'none';
} catch (e) {
	dataLayerProps.utm_content = 'none';
}

try {
	dataLayerProps.utm_campaign = dataLayerProps.dataLayerParams.get(
		'utm_campaign'
	);
	if (dataLayerProps.utm_campaign === null)
		dataLayerProps.utm_campaign = 'none';
} catch (e) {
	dataLayerProps.utm_campaign = 'none';
}
</script>

<script>
window.addEventListener(
	'load',
	function() {
		if (!window.fbq || typeof fbq === 'undefined') {
			window.fbq = function() {
				return null;
			};
		} else {
			var city = detail.area_city;
			var region = detail.area_subdistrict;

			if (city === '' && region === '') {
				city = detail.location.toString();
				region = detail.location.toString();
			}

			dataLayer.push({
				'event': 'ViewProduct',
				'ecommerce': {
					'currencyCode': 'IDR',
					'detail': {
						'actionField': {
							'list': 'Home'
						},
						'products': {
							'name': detail.room_title,
							'id': detail._id,
							'price': detail.price_title,
							'type': 'hotel',
							'city': city,
							'region': region,
							'category': 'Kost',
							'brand': detail.room_title,
						}
					}
				}
			});
		}
	},
	false
);
</script>