@extends('web.@layout.globalLayout')


@section('head')
	@if (!empty($_SERVER['HTTP_HOST']))
		@if ($_SERVER['HTTP_HOST'] == 'mamikos.com')
			<meta name="robots" content="index, follow">
		@else
			<meta name="robots" content="noindex, nofollow">
		@endif
	@else
		<meta name="robots" content="noindex, nofollow">
	@endif

	<title>Konfirmasi Pembayaran Booking - Mamikos</title>

	<meta name="description" content="Konfirmasi pembayaran booking di Mamikos.">

	<link rel="preload" href="{{ mix_url('dist/js/_booking/user/app.js') }}" as="script">
@endsection


@section('data')
@endsection


@section('styles')
	@include('web.@style.preloading-style')
@endsection


@section('content')
	@include('web.@style.preloading-content')
	
	<div id="app">
		<app></app>
	</div>
@endsection


@section('scripts')
	@include('web.@script.global-auth')

	<script src="{{ mix_url('dist/js/_booking/user/app.js') }}" async defer></script>
@endsection
