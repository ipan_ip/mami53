<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" Cache-Control:public;>
<html lang="en">

<title>Login - Mamikos</title>
<link rel="canonical" href="https://mamikos.com/login" />
<link rel="author" href="https://plus.google.com/u/1/+Mamikos/posts" />
<meta name="csrf-token" content="{{ csrf_token() }}">

<meta name="application-name" content="Mamikos">
<meta name="theme-color" content="#1BAA56">
<link rel="manifest" href="/json/manifest.json">

@include('web.@meta.link-prebrowsing')

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

@include('web.@meta.favicon')

<meta name="classification" content="Business, Rent House, Sewa Kost, Property, Rent Room, Info Kost, Information, Kost, Room, Cari Kost, Kost Murah, Kost Bebas, Application, Mobile Application">
<meta name="distribution" content="global">
<meta http-equiv="Reply-to" content="mamikos.app@gmail.com">
<meta name="revisit-after" content="1 day">
<meta name="keyphrases" content="Info Kost, Cari Kost, Sewa Kost, Kost Bebas, Kost Murah, Kost pasutri, Aplikasi Kost, Aplikasi Pencarian Kost, Aplikasi Info Kost, APlikasi Cari Kost, Kost, Mamikost, Mamikosapp">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="keywords" content="mamikost, Info Kost, Cari kost, kost yogyakarta, kost jogja, kost jakarta, kost bandung, kost depok, kost surabaya, indekost, kos, indekos, sewa">
<meta name="description" content="MAMIKOS adalah aplikasi pencari kost no.1 di Indonesia. MAMIKOS menyediakan layanan pencari kost yang gampang dan cepat serta memiliki data kost yang akurat.">
<meta name="author" content="Mamikos">
<meta name="robots" content="index, follow">
<meta property="fb:app_id" content="607562576051242" />
<meta property="og:title" content="Login - Mamikos" />
<meta name="og:site_name" content="Mamikos" />
<meta property="og:description" content="MAMIKOS adalah aplikasi pencari kost no.1 di Indonesia. MAMIKOS menyediakan layanan pencari kost yang gampang dan cepat serta memiliki data kost yang akurat." />
<meta property="og:type" content="website" />
<meta property="og:url" content="https://mamikos.com/promo/nonton-gratis" />
<meta property="og:image" content="/assets/share-image-default.jpg" />
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@mamikosapp">
<meta name="twitter:title" content="Login - Mamikos">
<meta name="twitter:description" content="MAMIKOS adalah aplikasi pencari kost no.1 di Indonesia. MAMIKOS menyediakan layanan pencari kost yang gampang dan cepat serta memiliki data kost yang akurat.">
<meta name="twitter:creator" content="@mamikosapp">
<meta name="twitter:image" content="/assets/share-image-default.jpg">
<meta name="twitter:domain" content="mamikos.com">





<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-K9V8K4" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>
  (function(w, d, s, l, i) {
    w[l] = w[l] || [];
    w[l].push({
      'gtm.start': new Date().getTime()
      , event: 'gtm.js'
    });
    var f = d.getElementsByTagName(s)[0]
      , j = d.createElement(s)
      , dl = l != 'dataLayer' ? '&l=' + l : '';
    j.async = true;
    j.src =
      '//www.googletagmanager.com/gtm.js?id=' + i + dl;
    f.parentNode.insertBefore(j, f);
  })(window, document, 'script', 'dataLayer', 'GTM-K9V8K4');

</script>
<!-- End Google Tag Manager -->

<!--  CSS Syle
  	================================================== -->
@include('web.part.external.global-head')
<link rel="stylesheet" href="{{ mix_url('dist/css/vendor.css') }}">
<link rel="stylesheet" href="{{ mix_url('dist/css/appmamikos.css') }}">
<style>
  html,
  body {
    font-family: 'Lato', sans-serif;
    height: 100%;
    overflow: hidden;
  }

  input[type=number]::-webkit-inner-spin-button,
  input[type=number]::-webkit-outer-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }

  @media (max-width: 767px) {

    html,
    body {
      background-color: #039b50;
    }
  }

  @media (min-width: 768px) {

    html,
    body {
      background: url(/assets/100-1-blur.jpg) no-repeat center center fixed;
      -webkit-background-size: cover;
      -moz-background-size: cover;
      -o-background-size: cover;
      background-size: cover;
    }
  }

  .flex-center-row {
    display: -webkit-flex;
    display: -moz-flex;
    display: flex;

    -webkit-align-items: center;
    -moz-align-items: center;
    align-items: center;

    -webkit-justify-content: center;
    -moz-justify-content: center;
    justify-content: center;
  }

  .flex-center-col {
    display: -webkit-flex;
    display: -moz-flex;
    display: flex;

    -webkit-flex-direction: column;
    -moz-flex-direction: column;
    flex-direction: column;

    -webkit-align-items: center;
    -moz-align-items: center;
    align-items: center;

    -webkit-justify-content: center;
    -moz-justify-content: center;
    justify-content: center;
  }

  .login-container {
    padding: 30px 30px 60px 30px;
    background-color: #039b50;
  }

  @media (max-width: 767px) {
    .login-container {
      width: 100%;
      padding: 30px 0 60px 0;
    }
  }

  @media (min-width: 768px) {
    .login-container {
      width: 384px;
      height: 545px;
      box-shadow: 0 19px 38px rgba(0, 0, 0, 0.30), 0 15px 12px rgba(0, 0, 0, 0.22);
    }
  }

  .form-container {
    width: 80%;
  }

  @media (max-width: 320px) {
    .form-container {
      width: 95%;
    }
  }

  .form-login-owner h3 {
    color: yellow;
    font-weight: bold;
    text-align: center;
  }

  .form-login-owner label {
    color: #eee;
    text-align: center;
    width: 100%;
    font-size: 16px
  }

  .form-login-owner .form-control {
    font-size: 16px;
    text-align: center;
  }

  .form-login-owner .btn {
    width: 100%;
    font-size: 16px;
    font-weight: bold;
    margin-bottom: 15px;
  }

  .form-container .additional {
    margin-top: 15px;
    text-align: center;
  }

  .form-container .additional>a {
    color: yellow;
  }

  .form-container .additional>span {
    color: #fff;
  }

</style>

</head>

<body class="flex-center-row">
  <div class="flex-center-col login-container">
    <a href="/">
      <img alt="logo_mamikos" src="/assets/home-desktop/header_mamikos.png" style="height:50px">
    </a>
    <br>
    <div id="loginRoot" class="form-container">
      <form class="col-xs-12 form-login-owner" v-if="formState == 'login'" @submit.prevent="submitLogin">
        <h3>LOGIN AKUN PEMILIK</h3>
        <br>
        <div class="form-group">
          <label for="phone_number">No. Handphone</label>
          <input type="tel" class="form-control" id="phone_number" required pattern="^0(\d{3,4}-?){2}\d{3,4}$" placeholder="Masukkan No. Handphone" v-model="login.phone_number">
        </div>
        <div class="form-group">
          <label for="password">Password:</label>
          <input type="password" class="form-control" id="password" required minlength="6" maxlength="20" placeholder="Masukkan Password" v-model="login.password">
        </div>
        <button type="submit" class="btn btn-mamiorange track-owner-login-submit">LOGIN</button>
        <div class="form-group additional">
          <a @click="toggleForm('forget')">Lupa password?</a>
        </div>
        <div class="form-group additional">
          <span>Belum punya akun? </span>
          <a href="/?register=kost" class="track-kost-promotion">Daftar sekarang</a>
        </div>
      </form>
      <form class="col-xs-12 form-login-owner" v-if="formState == 'forget'" v-cloak @submit.prevent="submitForget">
        <h3>LUPA PASSWORD</h3>
        <br>
        <div class="form-group">
          <label for="phone_number">No. Handphone</label>
          <input type="tel" class="form-control" id="phone_number" required placeholder="Masukkan No. Handphone" v-model="forget.phone_number">
        </div>
        <div class="form-group">
          <label for="password">Password Baru:</label>
          <input type="password" class="form-control" id="password" required minlength="6" maxlength="20" placeholder="Masukkan Password" v-model="forget.password">
        </div>
        <button type="submit" class="btn btn-mamiorange">MINTA KODE VERIFIKASI</button>
        <button type="button" class="btn btn-mamiorange-inverse" @click="toggleForm('verif')">SAYA SUDAH PUNYA KODE</button>
        <div class="form-group additional">
          <a @click="toggleForm('login')">Kembali</a>
        </div>
      </form>
      <form class="col-xs-12 form-login-owner" v-if="formState == 'verif'" v-cloak @submit.prevent="submitVerif">
        <div class="form-group">
          <label for="phone_number">No. Handphone</label>
          <input type="tel" class="form-control" id="phone_number" required pattern="^0(\d{3,4}-?){2}\d{3,4}$" placeholder="Masukkan No. Handphone" v-model="forget.phone_number">
        </div>
        <div class="form-group">
          <label for="password">Password Baru:</label>
          <input type="password" class="form-control" id="password" required minlength="6" maxlength="20" placeholder="Masukkan Password" v-model="forget.password">
        </div>
        <div class="form-group">
          <label for="verif">Masukkan Kode Verifikasi</label>
          <input type="text" class="form-control form-verif" id="verif" required minlength="1" maxlength="4" placeholder="XXXX" v-model="forget.verifInput">
        </div>
        <button type="submit" class="btn btn-mamiorange">BUAT PASSSWORD BARU</button>
        <div class="form-group additional">
          <a @click="toggleForm('forget')">Kembali</a>
        </div>
      </form>

      @include ('web.part.modalloading')
    </div>
  </div>


  <!--  JavaScripts
	================================================== -->
  <script src="https://maps.googleapis.com/maps/api/js?libraries=places&language=id&key={{ENV('MIX_GOOGLE_MAPS_JAVASCRIPT_API_KEY')}}"></script>
  <script src="{{ mix_url('dist/js/vendor.js') }}"></script>
  @include ('web.part.script.bugsnagapikey')
  @include ('web.part.script.tokenerrorhandlescript')


  <script>
    Vue.config.productionTip = false;

    var loginRoot = new Vue({
      el: '#loginRoot'
      , data: {
        token: document.head.querySelector('meta[name="csrf-token"]').content
        , login: {
          phone_number: ''
          , password: ''
        }
        , forget: {
          phone_number: ''
          , password: ''
          , verifResponse: ''
          , verifInput: ''
        }
        , formState: 'login'
      }
      , methods: {
        toggleForm: function(state) {
          loginRoot.formState = state;
          if (state == 'forget' && loginRoot.login.phone_number != '') {
            loginRoot.forget.phone_number = loginRoot.login.phone_number;
          }
        }
        , submitLogin: function() {
          loginRoot.forget.phone_number = loginRoot.login.phone_number;
          $('#modalLoading').modal({
            backdrop: 'static'
            , keyboard: false
          });
          $.ajax({
            type: "POST"
            , url: "/garuda/auth/owner/login"
            , headers: {
              "Content-Type": "application/json"
              , "X-GIT-Time": "1406090202"
              , "Authorization": "GIT WEB:WEB"
            }
            , data: JSON.stringify({
              "phone_number": loginRoot.login.phone_number
              , "password": md5(loginRoot.login.password)
              , "_token": loginRoot.token
            })
            , crossDomain: true
            , dataType: 'json'
            , success: function(response) {
              if (response.status == true) {
                if (response.login.redirect_path) {
                  window.location.replace(response.login.redirect_path);
                } else {
                  window.location.replace('/ownerpage');
                }
              } else {
                $('#modalLoading').modal('hide');
                alert('Nomor HP atau Password yang Anda masukkan salah');
              }
            }
          });
        }
        , submitForget: function() {
          $('#modalLoading').modal({
            backdrop: 'static'
            , keyboard: false
          });
          $.ajax({
            type: "POST"
            , url: "/garuda/auth/owner/phone/verify"
            , headers: {
              "Content-Type": "application/json"
              , "X-GIT-Time": "1406090202"
              , "Authorization": "GIT WEB:WEB"
            }
            , data: JSON.stringify({
              "phone_number": loginRoot.forget.phone_number
              , "password": md5(loginRoot.forget.password)
              , "is_forget": true
            })
            , crossDomain: true
            , dataType: 'json'
            , success: function(response) {
              $('#modalLoading').modal('hide');
              if (response.status == true) {
                if (response.is_registered == false) {
                  alert('Kelasahan dalam pengisian, nomor HP Anda belum terdaftar');
                } else {
                  loginRoot.forget.verifResponse = response.code;
                  loginRoot.formState = 'verif';
                  alert('Kami telah mengirimkan SMS kode verifikasi ke nomor HP Anda');
                }
              } else {
                alert(response.meta.message);
              }
            }
          });
        }
        , submitVerif: function() {
          $('#modalLoading').modal({
            backdrop: 'static'
            , keyboard: false
          });
          $.ajax({
            type: "PUT"
            , url: "/garuda/auth/owner/password?verification_version=2"
            , headers: {
              "Content-Type": "application/json"
              , "X-GIT-Time": "1406090202"
              , "Authorization": "GIT WEB:WEB"
            }
            , data: JSON.stringify({
              "phone_number": loginRoot.forget.phone_number
              , "password": md5(loginRoot.forget.password)
              , "verification_code": loginRoot.forget.verifInput
            })
            , crossDomain: true
            , dataType: 'json'
            , success: function(response) {
              $('#modalLoading').modal('hide');
              if (response.status == true) {
                alert('Password Anda telah berhasil diperbarui..');
                $('#modalLoading').modal({
                  backdrop: 'static'
                  , keyboard: false
                });
                $.ajax({
                  type: "POST"
                  , url: "/garuda/auth/owner/login"
                  , headers: {
                    "Content-Type": "application/json"
                    , "X-GIT-Time": "1406090202"
                    , "Authorization": "GIT WEB:WEB"
                  }
                  , data: JSON.stringify({
                    "phone_number": loginRoot.forget.phone_number
                    , "password": md5(loginRoot.forget.password)
                    , "_token": loginRoot.token
                  })
                  , crossDomain: true
                  , dataType: 'json'
                  , success: function(response) {
                    $('#modalLoading').modal('hide');
                    if (response.status == true) {
                      if (response.login.redirect_path) {
                        window.location.replace(response.login.redirect_path);
                      } else {
                        window.location.replace('/ownerpage');
                      }
                    } else {
                      alert('Terjadi Kesalahan. Silakan login secara manual');
                      location.reload();
                    }
                  }
                });
              } else {
                alert(response.meta.message);
                loginRoot.forget.verifInput = '';
              }
            }
          });
        }
      , }
    });

  </script>
</body>
