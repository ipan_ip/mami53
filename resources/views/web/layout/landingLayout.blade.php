<!DOCTYPE html>
<html>
<head>
  <!-- Meta
  ================================================== -->
    @if($_SERVER['HTTP_HOST'] != 'mamikos.com')
      <meta name="robots" content="noindex, nofollow">
    @endif
    <meta name="csrf-token" content="{{ csrf_token() }}">

<meta name="application-name" content="Mamikos">
<meta name="theme-color" content="#1BAA56">
<link rel="manifest" href="/json/manifest.json">

@include('web.@meta.link-prebrowsing')

    @yield('meta')

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries
  ================================================== -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
  <![endif]-->

  <!-- Stylesheet
  ================================================== -->
    @include('web.part.external.global-head')
    <link rel="stylesheet" href="{{ mix_url('dist/css/vendor.css' ) }}">
    @yield ('style-source')
    <link rel="stylesheet" href="{{ asset('css/landing.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.1.4/css/ion.rangeSlider.skinModern.css">


  <!--  Javascript
  ================================================== -->
    
    <script src="https://maps.googleapis.com/maps/api/js?libraries=places&language=id&key={{ENV('MIX_GOOGLE_MAPS_JAVASCRIPT_API_KEY')}}"></script>

    <script src="{{ mix_url('dist/js/vendor.js' )}}"></script>
    @include ('web.part.script.bugsnagapikey')

    @yield('style')
    @yield('jsonld')
    @yield('analytic')

  

  

</head>

@yield('body-start')
<div class="container-fluid">
  <div class="row">
  <!-- Navbar Mobile -->
    <nav class="navbar navbar-default navbar-custom visible-xs visible-sm" ng-class="{'banner-off':bannerdisplay.value==false}">
    <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false" style="background-color: #1BAA56; float:left; margin-left:15px;">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" id="homepage" style="cursor:pointer;"><img class="imglogo-navxs visible-xs" src="/assets/home-mobile/mamikos_header_mobile.png" ng-cloak></a>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="border:0px;">
      <ul class="nav navbar-nav navbar-left">
        <li class="menu-top active" style="cursor:pointer;">
          <a class="menu-top" href="/promosi-kost" target="_blank" rel="noopener" title="Promosikan Kost" style="background:#1BAA56;text-decoration:none;">
            <span class="brand-text-right">Promosikan Kost</span>
          </a>
          <a class="menu-top" href="/cari" target="_blank" rel="noopener" title="Promosikan Kost" style="background:#1BAA56;text-decoration:none;">
            <span class="brand-text-right">Cari Lewat Peta</span>
          </a>
          <a class="menu-top" href="/kost" target="_blank" rel="noopener" title="Promosikan Kost" style="background:#1BAA56;text-decoration:none;">
            <span class="brand-text-right">Cari Apa Saja</span>
          </a>
        </li>

      </ul>
    </div><!-- /.navbar-collapse -->
    </div>
    </nav>
    <!-- Navbar Desktop -->
    <div class="col-xs-12 navbarTop hidden-xs hidden-sm" id="navbarTop">
        <a href="/" style="width:30%; text-align:left;">
          <div style="padding-left: 15px;">
            <img src="/assets/home-desktop/header_mamikos.png"   class="logo" alt="logo_mamikos">
          </div>
        </a>
        <div class="menu-right-bar" style="width:70%;">
          <div>
           <a href="/cari" target="_self">
              <span class="search-by-loc hidden-xs">Cari Lewat Peta</span>
            </a>
          </div>
          <div ng-click="addRoom()" style="text-align:center;">
            <a href="/promosi-kost" target="_blank" rel="noopener" style="text-decoration:none;">
              <span class="menuText promosi-kost hidden-xs" style="margin-right:5px;"><i class="fa fa-plus"></i>&nbsp;&nbsp;&nbsp; Promosikan Kostmu</span>
              <span class="menuText visible-xs">Promosi Kost</span>
            </a>
          </div>
          <form ng-submit="searchkeyword()">
              <input type="search" class="col-xs-12 input-custom-search" id="search" ng-model="keyword" placeholder="Cari Apa Saja Di Sini" minlength="3" maxlength="100">
              <i class="fa fa-search search-symbol" aria-hidden="true" ng-click="searchkeyword()"></i>
              <button type="submit" style="display:none;"></button>
          </form>
        </div>
    </div>

    <!-- Banner TOP -->
    <!-- Original -->
   <!--  <div class="bannerinstall navbar-fixed-top visible-xs visible-sm" ng-class="{'bannernone':bannerdisplay.value==false}">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 bannercontent" style="background-color:#F2F2F2;padding:10px 0px;" ng-class="{'bannernone':bannerdisplay.value==false}">
            <div class="col-xs-1" style="padding:0px 8px;" ng-click="closebanner()">
              <img src="/assets/bannerappxclose.png" alt="close" alt="banner_mamikos_close" style="width:10px;position:inherit;top:12px;">
            </div>
            <div class="col-xs-2" style="padding:0px;text-align:center;">
              <img src="/assets/bannerappicon.png" alt="banner_mamikos_icon" style="width:40px;position:inherit;top:3px;">
            </div>
            <div class="col-xs-6" style="padding:0px 0px 3px 15px;line-height:20px;">
              <span style="font-size:12px;">Install Mamikos</span><br>
              <img src="/assets/bannerappstars.png" alt="banner_mamikos_stars" style="width:100px;">
            </div>
            <div class="col-xs-3" style="padding:5px 0px;text-align:center;" ng-click="installMamikos()">
              <img src="/assets/bannerappinstallbtn.png" alt="banner_mamikos_install" style="width:60px;">
            </div>
          </div>
        </div>
      </div>
    </div> -->
    <!-- Variant 2-->
    <div class="bannerinstall navbar-fixed-top visible-xs visible-sm" ng-class="{'bannernone':bannerdisplay.value==false}">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 bannercontent" style="background-color:#33cc33;padding:7px 0px;" ng-class="{'bannernone':bannerdisplay.value==false}">
            <div class="col-xs-1" style="padding:0px 8px;" ng-click="closebanner()">
              <img src="/assets/bannerappxclose.png" alt="close" style="width:10px;position:inherit;top:15px;">
            </div>
            <div class="col-xs-2" style="padding:5px;text-align:center;">
              <img src="/assets/banner/B/logo.png" alt="app icon" style="width:45px;position:inherit;top:0px;">
            </div>
            <div class="col-xs-6" style="padding:2px 0px 3px 15px;line-height:20px;margin-top:2px">
              <span style="font-size:12px;color:#FFF">Aplikasi Mamikos</span><br>
              <img src="/assets/banner/B/rating.png" alt="app stars" style="width:90px;">
            </div>
            <div class="col-xs-3" style="padding:5px 0px;text-align:center;margin-top:5px" ng-click="installMamikos()">
              <img src="/assets/banner/B/button.png" alt="app install" style="width:75px;">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container">
  <div class="row">

    @yield('body-content')

    <!-- Footer -->
    <div class="col-xs-12" style="text-align: center;padding: 50px 15px;background-color:whitesmoke; color:#2D2D2D;border-bottom:2px solid #F5F5F5;">
        <span style="font-size:25px;font-weight:bold">Apakah Anda Pemilik Kost-Kostan?</span><br>
        <span style="font-size:18px;">Promosikan kost anda di Mamikos.com agar lebih dikenal.</span><br><br>
        <a href="/promosi-kost" target="_blank" rel="noopener" style="text-decoration:none !important;">
          <div class="tambahkanKos visible-xs" style="float:none;position:initial;font-size:18px;padding:10px;background-color:#009b4c;"><i class="fa fa-plus"></i>&nbsp;&nbsp;&nbsp;Promosikan Kost mu</div>
          <div class="tambahkanKos hidden-xs" style="float:none;position:initial;font-size:18px;padding:10px;background-color:#009b4c;width: 300px;margin: auto;"><i class="fa fa-plus"></i>&nbsp;&nbsp;&nbsp;Promosikan Kost mu</div>
        </a>
    </div>
            <div class="col-xs-12 hidden-xs hidden-sm" style="text-align:justify;">
              <div class="col-xs-12">
                <div class="col-xs-12 hidden-xs hidden-sm footer-info" style="padding-top: 30px;">
                  <!-- <p style="font-size: 15px;font-weight:bold;">
                    Mamikos - Aplikasi Pencari Info Kost No 1 Di Indonesia
                  </p> -->
                  @yield('description1')
                </div>
                <div class="col-xs-12 footer-info">
                  @yield('description2')
                </div>
                <div class="col-xs-12 footer-info">
                  @yield('description3')
                </div>
              </div>
            </div>
            <div class="col-xs-12 footer-page visible-xs visible-sm">
              <div class="col-xs-12 footer-title">
                <div class="col-xs-12" style="text-align:center;padding-bottom: 20px;padding-top: 30px;">
                  <span style="font-size:22px;color: #009b4c;">
                  Mamikos - Aplikasi Pencari Info Kost No 1 Di Indonesia
                  </span><br>
                </div><br>
              </div>
              <a data-toggle="collapse" role="button" href="#about" aria-expanded="false" aria-controls="about">
                  <div class="col-xs-12 visible-xs visible-sm footer-about">
                    @yield('title-desc-1')
                  </div>
              </a>
              <div class="col-xs-12 collapse footer-about-text" id="about">
                  @yield('description1')
              </div>
              <a data-toggle="collapse" role="button" href="#information" aria-expanded="false" aria-controls="information">
                <div class="col-xs-12 visible-xs visible-sm footer-about">
                    @yield('title-desc-2')
                </div>
              </a>
              <div class="col-md-6 col-xs-12 collapse footer-info-text" id="information">
                @yield('description2')
              </div>
              <a data-toggle="collapse" role="button" href="#feature" aria-expanded="false" aria-controls="feature">
                <div class="col-xs-12 visible-xs visible-sm footer-about">
                    @yield('title-desc-3')
                </div>
              </a>
              <div class="col-md-6 col-xs-12 collapse footer-info-text" id="feature">
                    @yield('description3')
              </div>
            </div>
            <div class="clearfix"></div>
            @include ('web.part.footer')
  @yield('body-bottom')
  <!-- MODAL Login-->
    <div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" ng-cloak>
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body" style="text-align:center;">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" ng-click="zopimChatShow()"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title login-title">SILAKAN  LOGIN</h4>
              <p class="login-description">Untuk cek kost yang direkomendasikan untuk kamu dan kost yang kamu favorit</p>
            <form method="POST" action="/auth/facebook" name="formLogin">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <input class="form-control" type="e-mail" name="email" placeholder="E-mail" required>
              <br>
              <input class="form-control" type="text" name="phone_number" placeholder="No. Handphone" required ng-pattern="/^[0-9]{6,15}$/">
              <button class="facebook-login-button" type="submit" ng-disabled="formLogin.$invalid">
                <i class="fa fa-facebook-official" aria-hidden="true"></i>
                Login with Facebook
              </button>
            </form>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    

    @include ('web.part.script.navbarscript')
  

  @yield('script')
  @include ('web.part.script.browseralertscript')
</body>
</html>
