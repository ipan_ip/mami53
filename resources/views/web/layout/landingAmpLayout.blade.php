<!doctype html>
<html ⚡ lang="en">
  <head>
    <meta charset="utf-8">
    <script async custom-element="amp-analytics" src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"></script>
    <script async src="https://cdn.ampproject.org/v0.js"></script>
    <title>{{$heading_1}} - Mamikos</title>
    <link rel="canonical" href="{{$canonical}}" />
    <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">

    @include('web.@meta.favicon')

    @if($_SERVER['HTTP_HOST'] != 'mamikos.com')
      <meta name="robots" content="noindex, nofollow">
    @endif
    <meta name="description" content="Mau info {{ $heading_1 }}? atau mau cari kost harian di {{ $keyword }}?  Kost bebas {{ $keyword }}, dan kost pasutri {{ $keyword }} juga ada. Simak Infonya di aplikasi Mamikos. Download sekarang! Gratis!" />
    <meta name="keywords" content=" info {{ $heading_1 }}, cari {{ $heading_1 }}, {{ $heading_1 }}, kost harian {{ $keyword }}, kost pasutri {{ $keyword }}">

    <meta name="og:site_name" content="Mamikos"/>
    <meta property="og:description" content="Mau info {{ $heading_1 }}? atau mau cari kost harian di {{$keyword}}?  Kost bebas {{ $keyword }}, dan kost pasutri {{ $keyword }} juga ada. Simak Infonya di aplikasi Mamikos. Download sekarang! Gratis!" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="{{ $canonical }}" />
    <meta property="og:image" content="/assets/share-image-default.jpg" />

    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@mamikosapp">
    <meta name="twitter:title" content="{{ $heading_1 }} - Mamikos">
    <meta name="twitter:description" content="Mau info {{ $heading_1 }}? atau mau cari kost harian di {{ $keyword }}?  Kost bebas {{ $keyword }}, dan kost pasutri {{ $keyword }} juga ada. Simak Infonya di aplikasi Mamikos. Download sekarang! Gratis!">
    <meta name="twitter:creator" content="@mamikosapp">
    <meta name="twitter:image" content="/assets/share-image-default.jpg">
    <meta name="twitter:domain" content="mamikos.com">

    <style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>


    @yield('amp-custom-style')
  </head>
  <body>
    @yield('content')

    <amp-analytics type="googleanalytics" id="gamp-analytics">
      <script type="application/json">
      {
        "vars": {
          "account": "UA-71556143-1"
        },
        "triggers": {
          "trackPageview": {
            "on": "visible",
            "request": "pageview"
          },
          "trackClickOnMore" : {
            "on": "click",
            "selector": "#button-more",
            "request": "event",
            "vars": {
              "eventCategory": "button",
              "eventAction": "button-more-click",
              "eventLabel": "Lihat lebih banyak"
            }
          }
        }
      }
      </script>
    </amp-analytics>
    <!-- <amp-analytics config="https://www.googletagmanager.com/amp.json?id=GTM-5F6DBB2&gtm.url=SOURCE_URL" data-credentials="include"></amp-analytics> -->
  </body>
</html>