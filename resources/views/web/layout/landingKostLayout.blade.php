<!DOCTYPE html>
<html>
<head>
  <!-- Meta
  ================================================== -->
    @if($_SERVER['HTTP_HOST'] != 'mamikos.com')
      <meta name="robots" content="noindex, nofollow">
    @endif
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    @yield('meta')

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries
  ================================================== -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
  <![endif]-->

  

  <!-- Stylesheet
  ================================================== -->
    <link rel="preload" href="/css/fonts/lato.css" as="style">
    <link rel="stylesheet" href="/css/fonts/lato.css">
    <link rel="stylesheet" href="{{ mix_url('dist/css/vendorlanding.css' ) }}">
    @yield('style')


  <!--  Javascript
  ================================================== -->
    

    @yield('jsonld')

    <!-- start Mixpanel --><script type="text/javascript">(function(e,a){if(!a.__SV){var b=window;try{var c,l,i,j=b.location,g=j.hash;c=function(a,b){return(l=a.match(RegExp(b+"=([^&]*)")))?l[1]:null};g&&c(g,"state")&&(i=JSON.parse(decodeURIComponent(c(g,"state"))),"mpeditor"===i.action&&(b.sessionStorage.setItem("_mpcehash",g),history.replaceState(i.desiredHash||"",e.title,j.pathname+j.search)))}catch(m){}var k,h;window.mixpanel=a;a._i=[];a.init=function(b,c,f){function e(b,a){var c=a.split(".");2==c.length&&(b=b[c[0]],a=c[1]);b[a]=function(){b.push([a].concat(Array.prototype.slice.call(arguments,
    0)))}}var d=a;"undefined"!==typeof f?d=a[f]=[]:f="mixpanel";d.people=d.people||[];d.toString=function(b){var a="mixpanel";"mixpanel"!==f&&(a+="."+f);b||(a+=" (stub)");return a};d.people.toString=function(){return d.toString(1)+".people (stub)"};k="disable time_event track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config reset people.set people.set_once people.increment people.append people.union people.track_charge people.clear_charges people.delete_user".split(" ");
    for(h=0;h<k.length;h++)e(d,k[h]);a._i.push([b,c,f])};a.__SV=1.2;b=e.createElement("script");b.type="text/javascript";b.async=!0;b.src="undefined"!==typeof MIXPANEL_CUSTOM_LIB_URL?MIXPANEL_CUSTOM_LIB_URL:"file:"===e.location.protocol&&"//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js".match(/^\/\//)?"https://cdn.mxpnl.com/libs/mixpanel-2-latest.min.js":"//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js";c=e.getElementsByTagName("script")[0];c.parentNode.insertBefore(b,c)}})(document,window.mixpanel||[]);
    mixpanel.init("fdead9ca37613a9a303a23447907ca84");</script><!-- end Mixpanel -->
    
    <style>.async-hide { opacity: 0 !important} </style>
    <script>(function(a,s,y,n,c,h,i,d,e){s.className+=' '+y;h.start=1*new Date;
    h.end=i=function(){s.className=s.className.replace(RegExp(' ?'+y),'')};
    (a[n]=a[n]||[]).hide=h;setTimeout(function(){i();h.end=null},c);h.timeout=c;
    })(window,document.documentElement,'async-hide','dataLayer',4000,
    {'GTM-KS73SZC':true});</script>
    <!--Start Google Analytic-->
    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-71556143-1', 'auto');
    ga('require', 'GTM-KS73SZC');
    tracker('ga', ['send', 'pageview']);
    </script>
    <!--End Google Analytic-->

</head>

@yield('body-start')

@yield('body-content')

@yield('body-bottom')



 
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&language=id&key={{ENV('MIX_GOOGLE_MAPS_JAVASCRIPT_API_KEY')}}&language=id-ID"></script>

<script src="{{ mix_url('dist/js/vendorlanding.js' )}}"></script>

@include ('web.part.script.bugsnagapikey')
@yield('script')
  @include ('web.part.script.tokenerrorhandlescript')
  @include ('web.part.script.centermodalscript')
  @include ('web.part.script.navbarscript')
<script src="{{ mix_url('dist/js/login-user.js') }}" async></script>
@include ('web.part.script.browseralertscript')

  </body>
</html>
