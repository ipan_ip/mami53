<!DOCTYPE html>
<html>
<head>
  <!-- Meta
  ================================================== -->
    @if($_SERVER['HTTP_HOST'] != 'mamikos.com')
      <meta name="robots" content="noindex, nofollow">
    @endif
    <meta name="csrf-token" content="{{ csrf_token() }}">

<meta name="application-name" content="Mamikos">
<meta name="theme-color" content="#1BAA56">
<link rel="manifest" href="/json/manifest.json">

@include('web.@meta.link-prebrowsing')

    @yield('meta')

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries
  ================================================== -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
  <![endif]-->

  <!-- Stylesheet
  ================================================== -->
    @include('web.part.external.global-head')
    <link rel="stylesheet" href="{{ mix_url('dist/css/vendor.css' ) }}">
    <link rel="stylesheet" href="{{ mix_url('dist/css/appmamikos.css') }}">
    @yield('style')


  

  


</head>
@yield('body-start')

@yield('body-content')


<script src="https://maps.googleapis.com/maps/api/js?libraries=places&language=id&key={{ENV('MIX_GOOGLE_MAPS_JAVASCRIPT_API_KEY')}}"></script>

<script src="{{ mix_url('dist/js/vendor.js' )}}"></script>
@include ('web.part.script.bugsnagapikey')

@include('web.@script.global-auth')

@yield('script')
  @include ('web.part.script.tokenerrorhandlescript')

  @include ('web.part.script.navbarscript')
  @include ('web.part.script.scrolltopscript')

<script src="{{ mix_url('dist/js/login-user.js') }}"></script>
@include ('web.part.script.browseralertscript')

  </body>
</html>
