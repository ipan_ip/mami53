<!DOCTYPE html>
<html>
<head>
  <!-- Meta
  ================================================== -->
  <title>Promosi Gratis Kost di Mamikos.com</title>
  @if($_SERVER['HTTP_HOST'] != 'mamikos.com')
  <meta name="robots" content="noindex, nofollow">
  @endif
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <meta name="description" content="Form Pendaftaran Kost MamiKos - Cari Kost Gampang & Akurat. Promosikan Kost Mu Gratis di Mamikos.com!">
  <meta name="author" content="https://plus.google.com/u/1/+Mamikos/posts">
  
  @include('web.@meta.favicon')

  <meta property="og:title" content="Form Pendaftaran Kost MamiKos - Cari Kost Gampang & Akurat" />
  <meta name="og:site_name" content="Mamikos" />
  <meta property="og:description" content="Form Pendaftaran Kost MamiKos - Cari Kost Gampang & Akurat. Promosikan Kost Mu Gratis di Mamikos.com!" />
  <meta property="og:type" content="website" />
  <meta property="fb:app_id" content="607562576051242" />
  <meta property="og:url" content="https://mamikos.com/form" />
  <meta property="og:image" content="/assets/mamikos_form.png" />
  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:site" content="@mamikosapp">
  <meta name="twitter:title" content="Form Pendaftaran Kost MamiKos - Cari Kost Gampang & Akurat">
  <meta name="twitter:description" content="Form Pendaftaran Kost MamiKos - Cari Kost Gampang & Akurat. Promosikan Kost Mu Gratis di Mamikos.com!">
  <meta name="twitter:creator" content="@mamikosapp">
  <meta name="twitter:image" content="/assets/mamikos_form.png">
  <meta name="twitter:domain" content="mamikos.com">
  <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Expires" content="0" />

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries
  ================================================== -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
  <![endif]-->

  <!-- Stylesheet
  ================================================== -->

  <!-- Vendor -->
  <link rel="stylesheet" href="{{ mix_url('dist/css/vendor.css') }}">
  <link rel="stylesheet" href="{{ mix('css/form.css') }}">
  <!-- FLAT UI -->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/flat-ui/2.2.2/css/flat-ui.css" rel="stylesheet">


  <!--  JavaScripts
  ================================================== -->


  <script src="https://maps.googleapis.com/maps/api/js?libraries=places&language=id&key={{ENV('MIX_GOOGLE_MAPS_JAVASCRIPT_API_KEY')}}"></script>

  <script src="{{ mix_url('dist/js/vendor.js') }}"></script>
  @include ('web.part.script.bugsnagapikey')
</head>

@yield('content')

@yield('script')
<script src="{{ asset('assets/vendor/bootstrap-filestyle/src/bootstrap-filestyle.min.js') }}"></script>
@include ('web.part.script.tokenerrorhandlescript')

@include ('web.part.script.browseralertscript')

<script>
  $(".form-action-toggle").click(function() {
    var toggleOpen = $("#formAction").hasClass("is-open");
    var toggleClose = $("#formAction").hasClass("is-close");

    if (toggleOpen) {
      $("#formAction").removeClass("is-open");
      $("#formAction").addClass("is-close");
      $("#formAction").css("width", "40px");
      $(".form-action-tooltip").remove();
      $(".form-action-icon").removeClass("col-xs-2");
      $(".form-action-icon").addClass("col-xs-12");
      $(".form-action-button").hide();
    } else if (toggleClose) {
      $("#formAction").removeClass("is-close");
      $("#formAction").addClass("is-open");
      $("#formAction").css("width", "250px");
      $(".form-action-icon").addClass("col-xs-2");
      $(".form-action-icon").removeClass("col-xs-12");
      $(".form-action-button").show();
    }
  });

</script>

</body>
<!-- <script type="text/javascript" src="main.js?v=12392823"></script> -->
</html>
