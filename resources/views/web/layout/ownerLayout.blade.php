<!DOCTYPE html>
<html>
<head>
  <!-- Meta
  ================================================== -->
    @if($_SERVER['HTTP_HOST'] != 'mamikos.com')
      <meta name="robots" content="noindex, nofollow">
    @endif
    <meta id="tokenOwner" name="csrf-token" content="{{ csrf_token() }}">
    @yield('meta')

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries
  ================================================== -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
  <![endif]-->

  <!-- Stylesheet
  ================================================== -->
    @include('web.part.external.global-head')
    <link rel="stylesheet" href="{{ mix_url('dist/css/vendor.css' ) }}">
    @yield ('style-source')
    <link rel="stylesheet" href="{{ mix_url('dist/css/appmamikos.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owner.css') }}">
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.1.4/css/ion.rangeSlider.skinModern.css"> -->
    @if(Request::is('ownerpage/statistics/*'))
    <style>
      .breadcrumb-owner{
        display: none;
      }
      #ownerMenu .menu-container{
        top: 6em;
        left: 0;
      }
      .is-ownerpage-sidebar-container{
        margin-top: 3.5em;
        margin-left: 262px;
      }
      .is-ownerpage-sidebar-container .breadcrumb-owner{
        display: block;
        margin-left: 11px;
      }
      .back-button-statistick{
        position: relative;
        border-bottom: 1px solid #eee;
        background-color: #fff;
        z-index: 100;
        top: 5em;
        padding: 10px 0;
      }
      .back-button-statistick a{
        color: #000000;
        font-size: 16px;
        font-weight: bold;
      }
      .back-button-statistick img{
        margin-right: 10px;
      }
      .is-ownerpage-sidebar-container .container{
        width: 100%;
      }
      @media (max-width: 800px){
        .back-button-statistick{
          top: 0;
        }
      }
      @media (max-width: 767px) {
        #ownerMenu {
          display: none;
        }

        .is-ownerpage-sidebar-container{
          margin-top: 0;
          margin-left: 0;
        }

        .ownerpage-container.app-wrapper{
          margin-top: 6px;
        }
      }
      @media (max-width: 400px){
        .is-ownerpage-sidebar-container #statistics .stats-list .tats-date{
          width: 90.666667%;
        }
      }
    </style>
    @endif


  <!--  Javascript
  ================================================== -->

    <script>
      var tokenOwner = document.getElementById("tokenOwner").content;
      var roomId = {};
      var roomSlug = {};
      var roomReport = {};
      var ownerInput = {};
      var ownerInputMobile = {};
      var updateAvailableRoom = {};
      var updatePriceDaily = {};
      var updatePriceWeekly = {};
      var updatePriceMonthly = {};
      var updatePriceYearly = {};
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?libraries=places&language=id&key={{ENV('MIX_GOOGLE_MAPS_JAVASCRIPT_API_KEY')}}"></script>

    @include('web.@script.global-auth')
    <script src="{{ mix_url('dist/js/vendor.js' )}}"></script>
    @if(Request::is('ownerpage/statistics/*'))
    <script src="{{ mix_url('dist/js/navbar-owner.js') }}" async></script>
    <script>
      function goBack() {
        window.history.back()
      }
    </script>
    @else
    <script src="{{ mix_url('dist/js/navbar-custom.js') }}" async></script>
    @endif

    @include ('web.part.script.bugsnagapikey')

    @yield('style')
    @yield('jsonld')
    @yield('analytic')



	<!--Start Google Analytic-->

	<!--End Google Analytic-->

</head>

@yield('body-start')
  @if(Request::is('ownerpage/statistics/*'))
  <div id="navbarOwner" class="hidden-xs">
    <navbar-owner></navbar-owner>
  </div>
  @else
  <div id="navbarCustom">
    <navbar-custom
      :show-input-search="false"
      :show-promotion="false"
      :navigations="navigations"
    ></navbar-custom>
  </div>
  @endif
  
  <ol class="breadcrumb breadcrumb-owner col-xs-12">
    <li class="breadcrumb-list"><a href="/">Home</a></li>
    <li class="breadcrumb-list"><a href="/ownerpage">Halaman Pemilik Iklan</a></li>
  </ol>
  <div class="clerfix"></div>
  
  @if(Request::is('ownerpage/statistics/*'))
  <div class="ownerpage-container app-wrapper">
    <div id="ownerMenu">
      @if($is_apartment)
      <owner-menu :is-apartement-active="true"></owner-menu>
      @else
      <owner-menu :is-kos-active="true"></owner-menu>
      @endif
    </div>

    <div class="is-ownerpage-sidebar-container body-container">
      <div class="container">
        <div class="back-button-statistick">
          <a onclick="goBack()">
            <img src="/assets/icon_back.svg">
            Statistik {{ $room_name }}
          </a>
        </div>
      </div>
  @else
  <div class="container ownerpage-container app-wrapper">
    <div class="row">
  @endif

  @yield('body-content')

  @yield('body-bottom')

  @include ('web.part.script.tokenerrorhandlescript')


	@yield('script')
  	<script src="{{ mix_url('dist/js/bundle/maincontroller.js') }}"></script>
    @include ('web.part.script.browseralertscript')
</div>

</body>
</html>
