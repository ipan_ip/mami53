<!DOCTYPE html>
<html>
<head>
  <!-- Meta
  ================================================== -->
    @if($_SERVER['HTTP_HOST'] != 'mamikos.com')
      <meta name="robots" content="noindex, nofollow">
    @endif
    <meta name="csrf-token" content="{{ csrf_token() }}">

<meta name="application-name" content="Mamikos">
<meta name="theme-color" content="#1BAA56">
<link rel="manifest" href="/json/manifest.json">

@include('web.@meta.link-prebrowsing')

    
    @yield('meta')

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries
  ================================================== -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
  <![endif]-->

  

  <!-- Stylesheet
  ================================================== -->
    @include('web.part.external.global-head')
    <link rel="stylesheet" href="{{ mix_url('dist/css/vendorlanding.css' ) }}">
    @yield('style')


  <!--  Javascript
  ================================================== -->
    

    @yield('jsonld')

    
    
    <style>.async-hide { opacity: 0 !important} </style>
    <script>(function(a,s,y,n,c,h,i,d,e){s.className+=' '+y;h.start=1*new Date;
    h.end=i=function(){s.className=s.className.replace(RegExp(' ?'+y),'')};
    (a[n]=a[n]||[]).hide=h;setTimeout(function(){i();h.end=null},c);h.timeout=c;
    })(window,document.documentElement,'async-hide','dataLayer',4000,
    {'GTM-KS73SZC':true});</script>
    

</head>

@yield('body-start')

@yield('body-content')

@yield('body-bottom')



 
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&language=id&key={{ENV('MIX_GOOGLE_MAPS_JAVASCRIPT_API_KEY')}}&language=id-ID"></script>

<script src="{{ mix_url('dist/js/vendorlanding.js' )}}"></script>

@include ('web.part.script.bugsnagapikey')
@yield('script')
  @include ('web.part.script.tokenerrorhandlescript')
  
  @include ('web.part.script.navbarscript')
<script src="{{ mix_url('dist/js/login-user.js') }}" async></script>
@include ('web.part.script.browseralertscript')

  </body>
</html>
