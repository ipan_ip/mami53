<!DOCTYPE html>
<html>
<head>
  @if($_SERVER['HTTP_HOST'] != 'mamikos.com')
  <meta name="robots" content="noindex, nofollow">
  @endif
  <meta name="csrf-token" content="{{ csrf_token() }}">

<meta name="application-name" content="Mamikos">
<meta name="theme-color" content="#1BAA56">
<link rel="manifest" href="/json/manifest.json">

@include('web.@meta.link-prebrowsing')

  <base href="/">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

  @yield('meta-custom')

  @include('web.@meta.favicon')

  <link rel="preload" href="{{ mix_url('dist/css/common.css') }}" as="style">
  <link rel="preload" href="{{ mix_url('dist/js/common.js') }}" as="script">

  <meta property="fb:app_id" content="607562576051242"/>
  <meta name="keywords" content="Kos, Terbaru">
  <meta name="og:site_name" content="Mamikos"/>
  <meta property="og:type" content="website" />
  <meta property="og:url" content="<?php echo Request::url() ?>" />
  <meta property="og:image" content="/assets/og/og_kost_v2.jpg" />
  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:site" content="@mamikosapp">
  <meta name="twitter:creator" content="@mamikosapp">
  <meta name="twitter:image" content="/assets/og/og_kost_v2.jpg">
  <meta name="twitter:domain" content="mamikos.com">

  <!--  Fonts, Icons and CSS
  ================================================== -->
  @include('web.part.external.global-head')
  <link href="{{ mix_url('dist/css/common.css') }}" rel="stylesheet">
</head>
<body ng-app="myApp" ng-controller="bodyCtrl">
  @include('web.part.external.gtm-body')

  @yield('content')

  <!--  JavaScripts
  ================================================== -->
  <script src="{{ mix_url('dist/js/common.js') }}"></script>
  @include ('web.part.script.bugsnagapikey')

  @yield('script')

  @include ('web.part.script.tokenerrorhandlescript')
  @include ('web.part.script.browseralertscript')
</body>
</html>