@extends('web.newest.newestLayout')

@section('meta-custom')
	@if(isset($filters['title']))
	<title>Kost Terbaru {{ $filters['title'] }} - Mamikos</title>
	@else
	<title>Kost Terbaru - Mamikos</title>
	@endif

  <link rel="preload" href="{{ mix_url('dist/js/newest-list.js') }}" as="script">

	<meta name="twitter:title" content="Kost Terbaru MamiKos - Cari Kost Gampang & Akurat.">
	<meta name="twitter:description" content="Kost Terbaru MamiKos - Cari Kost Gampang & Akurat.">
	<meta property="og:description" content="Kos9t Terbaru MamiKos - Cari Kost Gampang & Akurat." />
	<meta name="description" content="Kost Terbaru MamiKos - Cari Kost Gampang & Akurat." />
	@if(isset($filters['title']))
	<meta name="title" content="{{ $filters['title'] }}">
	@else
	<meta name="title" content="">
	@endif

	@if(isset($filters['place']))
	<meta name="place" content="{{ $filters['place'] }}">
	@else
	<meta name="place" content="">
	@endif

	@if(isset($filters['tags']))
	<meta name="tags" content="{{ implode(',', $filters['tags']) }}">
	@else
	<meta name="tags" content="">
	@endif
@endsection

@section('content')
<div id="app">
	<container-newest-list></container-newest-list>
</div>
@endsection

@section('script')
@include('web.@script.global-auth')

	<script src="{{ mix_url('dist/js/newest-list.js') }}"></script>
@endsection