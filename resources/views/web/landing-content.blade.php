@extends('web.@layout.globalLayout')


@section('head')
<title>{{ $landingContent->title }} - Mamikos</title>
<meta name="description" content="{{ $meta_description }}" />

<link rel="canonical" href="https://mamikos.com/">

<meta property="og:url" content="<?php echo Request::url() ?>" />
<meta property="og:title" content="Mamikos - {{ $landingContent->title }} ...">
@if(!isset($og_image) || is_null($og_image) || $og_image==='')
  <meta property="og:image" content="/assets/share-image-default.jpg" />
@else
  <meta property="og:image" content="{{ $og_image }}" />
@endif
<meta property="og:description" content="{{ $meta_description }}" />

<meta name="twitter:url" content="https://mamikos.com/">
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@mamikosapp">
<meta name="twitter:title" content="Mamikos - {{ $landingContent->title }}">
<meta name="twitter:description" content="{{ $meta_description }}">
@if(!isset($og_image) || is_null($og_image) || $og_image==='')
  <meta name="twitter:image" content="/assets/share-image-default.jpg" />
@else
  <meta name="twitter:image" content="{{ $og_image }}">
@endif


<meta name="keyphrases" content="Info Kost, Cari Kost, Sewa Kost, Kost Bebas, Kost Murah, Kost pasutri, Aplikasi Kost, Aplikasi Pencarian Kost, Aplikasi Info Kost, APlikasi Cari Kost, Kost, Mamikost, Mamikosapp, Kamar Kost, Kamar Kos, Kostan, Kos, Rumah Kost, Rumah Kos, Kost Harian">

<meta name="classification" content="Business, Rent House, Sewa Kost, Property, Rent Room, Info Kost, Information, Kost, Room, Cari Kost, Kost Murah, Kost Bebas, Application, Mobile Application, Kamar Kost, Kamar Kos, Kostan, Kos, Rumah Kost, Rumah Kos, Kost Harian">

<meta name="keywords" content="Rekomendasi, Favorit, Kos">

<link rel="preload" href="{{ mix_url('dist/js/_landing-content/app.js') }}" as="script">

@endsection


@section('styles')
  @include('web.@style.preloading-style')
@endsection

@section('content')
  @include('web.@style.preloading-content')

  <div id="app">
    <app></app>
  </div>
@endsection


@section('scripts')
<script>
  var latestArticles = @JZON($new_article);

  var suggestionSlug = '{{$landingContent->slug}}';
  var contentType = '{{$landingContent->type}}';
  var landingContent = {
    title: '{{ $landingContent->title }}',
    isLoginRequired: @JZON($landingContent->need_login),
    contentOpen: @JZON($landingContent->content_open),
    contentHidden: @JZON($landingContent->content_hidden),
    buttonLabel: '{{ $landingContent->button_label }}',
    type: 'landing-content-' + '{{$landingContent -> id }}',
    seq: '{{ $landingContent->id }}'
  };

  var isSubscribed = '{{ $subscribed ? "true" : "false" }}' == 'true';

</script>
@if (Auth::check())
<script>
  var expiredDownloadToken = "{{(Auth::user()->id)}}";

</script>
@else
<script>
  var expiredDownloadToken = "false";

</script>
@endif

@if (Auth::check())
  @if (Auth::user()->is_owner == 'false')
    @include('web.@script.sendbird-widget')
  @endif
@endif

<script src="{{ mix_url('dist/js/_landing-content/app.js') }}" async defer></script>

@include('web.@script.global-auth')


@endsection

