@extends('web.@layout.globalLayout')

@section('head')
@if (!empty($_SERVER['HTTP_HOST']))
@if ($_SERVER['HTTP_HOST'] == 'mamikos.com')
<meta name="robots" content="index, follow">
@else
<meta name="robots" content="noindex, nofollow">
@endif
@else
<meta name="robots" content="noindex, nofollow">
@endif

<title>Mamikos - Kos Deket Kampus di Indonesia</title>
<meta name="description" content="Cari Kost dekat kampus ? Temukan disini banyak pilihan kost dekat kampus di seluruh indonesia, kost putra, kost putri dan kost campur bebas 24 jam. Ada diskon setiap hari dari mamikos!">
<link rel="canonical" href="https://mamikos.com/kampus">


<meta property="og:url" content="https://mamikos.com/kampus">
<meta property="og:title" content="Mamikos - Kos Deket Kampus di Indonesia">
<meta property="og:image" content="/assets/og/og_kost_v2.jpg">
<meta property="og:description" content="Cari Kost dekat kampus ? Temukan disini banyak pilihan kost dekat kampus di seluruh indonesia, kost putra, kost putri dan kost campur bebas 24 jam. Ada diskon setiap hari dari mamikos!">

<meta name="twitter:url" content="https://mamikos.com/kampus">
<meta name="twitter:title" content="Mamikos - Kos Deket Kampus di Indonesia">
<meta name="twitter:description" content="Cari Kost dekat kampus ? Temukan disini banyak pilihan kost dekat kampus di seluruh indonesia, kost putra, kost putri dan kost campur bebas 24 jam. Ada diskon setiap hari dari mamikos!">
<meta name="twitter:image" content="/assets/og/og_kost_v2.jpg">

<meta name="keyphrases" content="Info Kost, Cari Kost, Sewa Kost, Kost Bebas, Kost Murah, Kost pasutri, Aplikasi Kost, Aplikasi Pencarian Kost, Aplikasi Info Kost, APlikasi Cari Kost, Kost, Mamikost, Mamikosapp, Kamar Kost, Kamar Kos, Kostan, Kos, Rumah Kost, Rumah Kos, Kost Harian">

<meta name="classification" content="Business, Rent House, Sewa Kost, Property, Rent Room, Info Kost, Information, Kost, Room, Cari Kost, Kost Murah, Kost Bebas, Application, Mobile Application, Kamar Kost, Kamar Kos, Kostan, Kos, Rumah Kost, Rumah Kos, Kost Harian">

<meta name="keywords" content="kost dekat kampus, kost, kos, kosan murah, rumah kost, sewa kost, cari kost, kost harian, kost mingguan, kost bulanan, kost tahunan, mamikos">

<link rel="preload" href="{{ mix_url('dist/js/_area/app.js') }}" as="script">
@endsection

@section('data')

@endsection


@section('styles')
@include('web.@style.preloading-style')
@endsection


@section('content')
@include('web.@style.preloading-content')

<div id="app">
  <app></app>
</div>
@endsection


@section('scripts')
<script src="{{ mix_url('dist/js/_area/app.js') }}" async defer></script>
@endsection
