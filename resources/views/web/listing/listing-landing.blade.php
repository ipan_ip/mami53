@extends('web.listing.listingLayout')

@section('meta-custom')


<title>{{ $landingList->keyword }}</title>

<meta name="twitter:title" content="{{ $landingList->keyword }}">
<meta name="twitter:description" content="{{ $landingList->description }}">
<meta property="og:description" content="{{ $landingList->description }}" />
<meta name="description" content="{{ $landingList->description }}" />

<link rel="preload" href="{{ mix_url('dist/js/listing-landing.js') }}" as="script">

<style type="text/css">
	#prerender {
		margin-top: 100px;
	}
	#prerender .prerender-header {
		height: 120px;
	}

	#prerender .prerender-fill {
		background-color: #eee;
		height: 100%;
		border-radius: 5px;
	}
	#prerender .prerender-content {
		height: 300px;
		margin-top: 100px;
		margin-bottom: 50px;
	}

	@media (max-width: 767px) {
		#prerender .prerender-header {
			height: 200px;
		}
	}

	@media (min-width: 768px) {
		#prerender {
			padding-left: 100px;
			padding-right: 100px;
		}
	}

	#prerender .prerender-footer {
		margin-bottom: 100px;
	}
</style>

@endsection

@section('data')
<script type="text/javascript">
	var listingData = @JZON($landingList);
	var listingOptions = @JZON($options);
	var spesialisasiOptions;

	@if ($landingType === 'vacancy') {
		spesialisasiOptions = @JZON($spesialisasiOptions);
	}
	@endif

</script>
@endsection

@section('content')

<div id="prerender">
	<section>
		<div class="prerender-header col-md-12 col-sm-12 col-xs-12">
			<div class="prerender-fill"></div>
		</div>

		<div class="prerender-content col-md-12 col-xs-12 col-sm-12">
			<div class="prerender-fill"></div>
		</div>

		<div class="prerender-footer col-md-12 col-sm-12 col-xs-12">
			<div class="prerender-article">
				{!! $landingList -> description !!}
			</div>
		</div>
	</section>
</div>

<div id="app">
	<container-listing></container-listing>
</div>
@endsection

@section('script')
	<script src="{{ mix_url('dist/js/listing-landing.js') }}" ></script>
@endsection