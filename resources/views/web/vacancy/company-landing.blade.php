@extends('web.vacancy.vacancyLayout')

@section('meta-custom')


<title>Daftar perusahaan di indonesia dari berbagai industri 2019</title>

<meta name="description" content="Daftar perusahaan di indonesia. Temukan perusahaan bumn dan perusahaan swasta dari berbagai industri, lengkap dengan lowongan kerjanya!" />
<meta name="keywords" content="Daftar perusahaan indonesia, daftar nama perusahaan BUMN, daftar nama perusahaan swasta, daftar perusahaan industri di indonesia" />
<meta property="og:description" content="Daftar perusahaan di indonesia. Temukan perusahaan bumn dan perusahaan swasta dari berbagai industri, lengkap dengan lowongan kerjanya!" />
<meta property="og:type" content="website" />
<meta property="og:url" content="{{ Request::url() }}" />
<meta property="og:image" content="/assets/og_apartemen.jpg" />
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@mamikosapp">
<meta name="twitter:title" content="Mamikos - Cari Kost Gampang & Akurat">
<meta name="twitter:description" content="Daftar perusahaan di indonesia. Temukan perusahaan bumn dan perusahaan swasta dari berbagai industri, lengkap dengan lowongan kerjanya!">
<meta property="og:description" content="Daftar perusahaan di indonesia. Temukan perusahaan bumn dan perusahaan swasta dari berbagai industri, lengkap dengan lowongan kerjanya!" />
<link rel="canonical" href="https://mamikos.com{{ $_SERVER['REQUEST_URI'] }}" />

<link rel="preload" href="{{ mix_url('dist/js/company-landing.js') }}" as="script">

@endsection

@section('data')
<script type="text/javascript">
	var industryOptions = @JZON($industry);
	var article = @JZON($article);
</script>
@endsection

@section('content')
<div id="app">
	<app></app>
</div>
@endsection

@section('script')
	@include('web.@script.global-auth')

	<script src="{{ mix_url('dist/js/company-landing.js') }}" ></script>
@endsection