@extends('web.vacancy.vacancyLayout')

@section('meta-custom')

	<title>Profil {{ $data['name'] }}</title>

	<meta name="description" content="Info {{ $data['name'] }} lengkap beserta lowongan kerjanya. Cek hanya disini!" />
	<meta name="keywords" content="{{ $data['name'] }}, alamat {{ $data['name'] }}, gaji {{ $data['name'] }}, lowongan kerja {{ $data['name'] }}">
	<link rel="canonical" href="https://mamikos.com{{ $_SERVER['REQUEST_URI'] }}" />
	<meta name="og:site_name" content="Mamikos"/>
	<meta property="og:title" content="Mamikos" />
	<meta property="og:description" content="Info {{ $data['name'] }} lengkap beserta lowongan kerjanya. Cek hanya disini!" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="{{ Request::url() }}" />
	<meta property="og:image" content="/assets/share-image-default.jpg" />
	<meta property="fb:app_id" content="607562576051242"/>
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:site" content="@mamikosapp">
	<meta name="twitter:title" content="Mamikos">
	<meta name="twitter:description" content="Info {{ $data['name'] }} lengkap beserta lowongan kerjanya. Cek hanya disini!">
	<meta name="twitter:creator" content="@mamikosapp">
	<meta name="twitter:image" content="/assets/share-image-default.jpg">
	<meta name="twitter:domain" content="mamikos.com">
	<link rel="canonical" href="https://mamikos.com{{ $_SERVER['REQUEST_URI'] }}" />

	<meta name="slug" content="{{ $data['slug'] }}">

	<link rel="preload" href="{{ mix_url('dist/js/company-profile.js') }}" as="script">
@endsection

@section('data')
	<script type="text/javascript">
	</script>
@endsection

@section('content')
  	<div id="app">
  		<app></app>
  	</div>
@endsection

@section('script')
	@include('web.@script.global-auth')

	<script src="{{ mix_url('dist/js/company-profile.js') }}"></script>
@endsection
