@extends('web.vacancy.vacancyLayout')

@section('meta-custom')

	<title>{{ $page_title }} - Mamikos</title>

	<meta name="description" content="{{ $meta_description }}" />
	<meta name="keywords" content="{{ $landing->keyword }}">
	<link rel="canonical" href="{{ url(str_replace('http','https',$breadcrumb[sizeof($breadcrumb)-1]['url'])) }}" />
	<link rel="alternate" href="android-app://com.git.mami.kos/mamikos/jobs/landing/{{ $landing->slug }}" />
	<meta name="og:site_name" content="Mamikos"/>
	<meta property="og:title" content="{{ $landing->heading_1 }} - Mamikos" />
	<meta property="og:description" content="{{ $meta_description }}" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="{{ Request::url() }}" />
	<meta property="og:image" content="/assets/og/og_loker.jpg" />
	<meta property="fb:app_id" content="607562576051242"/>
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:site" content="@mamikosapp">
	<meta name="twitter:title" content="{{ $landing->heading_1 }} - Mamikos">
	<meta name="twitter:description" content="{{ $meta_description }}">
	<meta name="twitter:creator" content="@mamikosapp">
	<meta name="twitter:image" content="/assets/og/og_loker.jpg">
	<meta name="twitter:domain" content="mamikos.com">

	<meta name="slug" content="{{ $landing->slug }}">

	<link rel="preload" href="{{ mix_url('dist/js/landing-vacancy.js') }}" as="script">

	<style>
		#prerender {
			margin-top: 54px;
		}
		#prerender .prerender-header {
			background-color: #fff;
			z-index: 2;
			position: fixed;
		}

		@media (max-width: 991px) {
			#prerenderLandingVacancy .prerender-header {
				margin-top: -50px;
			}
		}

		#prerender .prerender-title {
			color: #1BAA56;
			font-size: 16px;
			font-weight: 700;
			text-transform: uppercase;
		}
		#prerender .prerender-header_right {
			height: 100px;
			padding: 10px 0;
		}
		#prerender .prerender-fill {
			background-color: #eee;
			height: 100%;
			border-radius: 5px;
		}
		#prerender .prerender-map {
			top: 160px;
			bottom: 0;
			height: 74%;
			position: fixed;
		}
		#prerender .prerender-content {
			margin-top: 105px;
		}
		#prerender .prerender-list {
			height: 250px;
			margin-bottom: 15px;
		}

		@media (max-width: 991px) {
			#prerender .prerender-content {
				padding-top: 100px;
			}
		}
	</style>
@endsection

@section('data')
	<script type="text/javascript">
		var mamiLanding = @JZON($landing);
		var mamiBreadcrumb = @JZON($breadcrumb);
		var mamiLandingConnector = @JZON($landing_connector);
		var mamiCenterpoint = @JZON($centerpoint);
		var educationOptions = @JZON($educationOptions);
		var spesialisasiOptions = @JZON($spesialisasiOptions);
		var jobTypeOptions = @JZON($jobTypeOptions);

		var relatedKostLink = @JZON($related_kos_link);
		var relatedApartmentLink = @JZON($related_apartment_link);
		var relatedVacancyLink = null;

		var mapTypeSource = '{{ $map_type }}';
	</script>

<script>
		var dataLayerParams;

		try {
			dataLayerParams = new URLSearchParams(window.location.search);
		} catch (e) {
			dataLayerParams = null;
		};

		var dataLayerProps = {
			device: "none",
			utm_source: "none",
			utm_medium: "none",
			utm_content: "none",
			utm_campaign: "none",
			city: "none",
			area: "none",
			salary: "0"
		};

		if (mamiLanding.area_city !== null) {
    	dataLayerProps.city = mamiLanding.area_city;
    };

    if (mamiLanding.area_subdistrict !== null) {
    	dataLayerProps.area = mamiLanding.area_subdistrict;
    };

    if (window.innerWidth < 768) {
      dataLayerProps.device = "mobile";
    } else {
      dataLayerProps.device = "desktop";
    };

    try {
      dataLayerProps.utm_source = dataLayerParams.get('utm_source');
      if (dataLayerProps.utm_source === null) dataLayerProps.utm_source = "none";
    } catch (e) {
      dataLayerProps.utm_source = "none";
    };

    try {
      dataLayerProps.utm_medium = dataLayerParams.get('utm_medium');
      if (dataLayerProps.utm_medium === null) dataLayerProps.utm_medium = "none";
    } catch (e) {
      dataLayerProps.utm_source = "none";
    };

    try {
      dataLayerProps.utm_content = dataLayerParams.get('utm_content');
      if (dataLayerProps.utm_content === null) dataLayerProps.utm_content = "none";
    } catch (e) {
      dataLayerProps.utm_content = "none";
    };

    try {
      dataLayerProps.utm_campaign = dataLayerProps.dataLayerParams.get('utm_campaign');
      if (dataLayerProps.utm_campaign === null) dataLayerProps.utm_campaign = "none";
    } catch (e) {
      dataLayerProps.utm_campaign = "none";
    };

		
    dataLayer.push({
      "language": "id",
      "page_category": "ImpressionLoker",
      "currency": "IDR",
      "device": dataLayerProps.device,
      "utm_source": dataLayerProps.utm_source,
      "utm_medium": dataLayerProps.utm_medium,
      "utm_content": dataLayerProps.utm_content,
      "utm_campaign": dataLayerProps.utm_campaign,
      "page_url": "https://mamikos.com/loker/{{ $landing['slug'] }}",
      "step": 2,
      "country": "Indonesia",
      "status": mamiLanding.type,
      "city": dataLayerProps.city,
      "area": dataLayerProps.area,
      "salary": dataLayerProps.salary,
      @if (Auth::check())
      "customer_id": "{{ (Auth::user()->id) }}",
      "profile_id": "{{ (Auth::user()->id) }}",
      "gender": "{{ (Auth::user()->gender) }}"
      @else
      "customer_id": "none",
      "profile_id": "none",
      "gender": "none"
      @endif
    });
</script>
@endsection

@section('content')
	{{-- Prerendered Priority Contents --}}
	<div id="prerender">
		<section>
			<div class="prerender-header col-md-12 col-sm-12 col-xs-12">
				<div class="prerender-header_left col-md-2 col-sm-12">
					<h1 class="prerender-title">{{ $landing['keyword'] }}</h1>
				</div>
				<div class="prerender-header_right col-md-10 col-sm-12 col-xs-12">
					<div class="prerender-fill"></div>
				</div>
			</div>
		</section>
		<section>
			<div class="prerender-content col-md-7">
				<div class="prerender-list">
					<div class="prerender-fill"></div>
				</div>
				<div class="prerender-description">{!! $landing['description'] !!}</div>
			</div>
			<div class="prerender-map col-md-5 col-md-push-7">
				<div class="prerender-fill"></div>
			</div>
		</section>
	</div>

	{{-- Complete Content After JS Rendered --}}
  	<div id="app">
  		<app></app>
  	</div>
@endsection

@section('script')
	@include('web.@script.global-auth')

	<script src="{{ mix_url('dist/js/landing-vacancy.js') }}"></script>

	<script type="application/ld+json">
		{
			"@context": "http://schema.org",
			"@type": "BreadcrumbList",
			"itemListElement":
				[
				@php
					$no=1
				@endphp
				@foreach($breadcrumb as $breadcumbItem)
					{
					"@type": "ListItem",
					"position": {{ $no }},
					"item":
						{
							"@id": "{{ $breadcumbItem['url'] }}",
							"name": "{{ $breadcumbItem['name'] }}"
						}
					}
				@if(count($breadcrumb) != $no++)
					{{ ',' }}
				@endif

				@endforeach
				]
		}
	</script>
@endsection
