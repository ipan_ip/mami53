@extends('web.vacancy.vacancyLayout')

@section('meta-custom')


<title>Mamikos</title>

<meta name="twitter:title" content="">
<meta name="twitter:description" content="">
<meta property="og:description" content="" />
<meta name="description" content="" />

<link rel="preload" href="{{ mix_url('dist/js/company-vacancy.js') }}" as="script">

@endsection

@section('data')
<script type="text/javascript">

</script>
@endsection

@section('content')
<div id="app">
	<container-company-vacancy></container-company-vacancy>
</div>
@endsection

@section('script')
<script src="{{ mix_url('dist/js/company-vacancy.js') }}" ></script>
<!-- <script src="dist/js/company-vacancy.js"></script> -->
{{-- <script src="/dist/js/company-vacancy.js"></script> --}}
@endsection