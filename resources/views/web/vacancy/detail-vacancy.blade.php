@extends('web.vacancy.vacancyLayout')

@section('meta-custom')
	@if($data['is_expired'] == 1)
	<title>{{ $page_title }} (Tidak Aktif) - Mamikos</title>
	@else
	<title>{{ $page_title }} - Mamikos</title>
	@endif

	<meta name="description" content="{{ $meta_description }}" />
	<meta name="keywords" content="{{ $data['title'] }}" />

	{{--{{ $type_title }} {{ $data['title'] }} di {{ $data['place_name'] }} {{ $data['city'] }}. Area penempatan {{ $data['city'] }}, gaji: {{ $salary_format }}. Kirimkan lamaran ke {{ $data['place_name'] }}. Dapatkan info lowongan di {{ $data['place_name'] }} lainnya di Mamikos.com">--}}

	<link rel="canonical" href="{{ $data['share_url'] }}" />
	<link rel="alternate" href="android-app://com.git.mami.kos/mamikos/jobs/{{ $data['slug'] }}" />
	<meta name="og:site_name" content="Mamikos"/>
	<meta name="og:title" content="{{ $page_title }} - Mamikos">
	<meta property="og:description" content="{{ $meta_description }}" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="{{ Request::url() }}" />
	<meta property="og:image" content="/assets/og/og_loker.jpg" />
	<meta property="fb:app_id" content="607562576051242"/>
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:site" content="@mamikosapp">
	<meta name="twitter:title" content="{{ $page_title }} - Mamikos">
	<meta name="twitter:description" content="{{ $meta_description }}">
	<meta name="twitter:creator" content="@mamikosapp">
	<meta name="twitter:image" content="/assets/og/og_loker.jpg">
	<meta name="twitter:domain" content="mamikos.com">

	<meta name="verification" content="{{ $verification_status }}">
	<meta name="verification-for" content="{{ $verification_for }}">

	<meta name="slug" content="{{ $data['slug'] }}">

	<link rel="preload" href="{{ mix_url('dist/js/detail-vacancy.js') }}" as="script">

	<style>
		#prerender {
			margin-top: 85px;
		}
		#prerender .prerender-header {
			background-color: grey;
			height: 85px;
		}
		#prerender .prerender-company {
				color: #fff;
				overflow: hidden;
				text-overflow: ellipsis;
				font-weight: bold;
		}
		#prerender .prerender-content_left {
			float: left;
		}
		#prerender .prerender-title_info {
			height: 100px;
			margin-top: 15px;
			margin-bottom: 15px;
		}
		#prerender .prerender-title {
			font-size: 28px;
			font-weight: bold;
		}
		#prerender .prerender-description {
			height: 400px;
			margin-bottom: 15px;
		}
		#prerender .prerender-description .prerender-fill {
			padding: 15px;
		}
		#prerender .prerender-description_text {
				font-size: 14px;
				margin: 0;
		}
		#prerender .prerender-label {
			font-weight: bold;
				margin-right: 50px;
				font-size: 14px;
		}
		#prerender .prerender-fill {
			background-color: #eee;
			height: 100%;
			border-radius: 5px;
		}
		#prerender .prerender-detail {
			height: 300px;
			margin-top: -10px;
		}
		#prerender .prerender-salary {
			font-size: 16px;
				color: #1BAA56;
				font-weight: bold;
					text-align: center;
				padding-top: 15px;
		}
		#prerender .prerender-subcategory {
			background-color: #eee;
		}
		#prerender .prerender-subtitle {
			font-size: 16px;
			font-weight: bold;
		}
		#prerender .prerender-box {
			height: 100px;
			margin-top: 15px;
			margin-bottom: 15px;
		}
		#prerender .prerender-map {
			height: 300px;
			margin-top: 15px;
			margin-bottom: 25px;
		}

		@media (max-width: 767px) {
			#prerender .prerender-container {
				padding-left: 0;
				padding-right: 0;
			}
			#prerender .prerender-detail {
				margin-top: 10px;
				margin-bottom: 15px;
					height: 50px;
					margin-left: -30px;
					margin-right: -30px;
			}
			#prerender .prerender-detail.prerender-fill {
				border-radius: 0;
			}
			#prerender .prerender-description {
				margin-left: -15px;
				margin-right: -15px;
				height: 200px;
			}
			#prerender .prerender-box {
				height: 100px;
				margin-left: -15px;
				margin-right: -15px;
				margin-top: 15px;
			}
			#prerender .prerender-content  .prerender-subcategory {
				margin-left: -30px;
				margin-right: -30px;
				margin-bottom: 15px;
				padding: 7px;
				padding-left: 15px;
			}
		}

		@media (min-width: 768px) {
			#prerender .prerender-company {
				margin-left: 25px;
			}
		}

	</style>
@endsection

@section('data')
	<script type="text/javascript">
		var mamiData = @JZON($data);

		var dataLayerParams;

		try {
			dataLayerParams = new URLSearchParams(window.location.search);
		} catch (e) {
			dataLayerParams = null;
		};

		var dataLayerProps = {
			device: "none",
			utm_source: "none",
			utm_medium: "none",
			utm_content: "none",
			utm_campaign: "none",
			city: "none",
			area: "none",
			image_url: "none",
			salary: "0"
		};

		if (mamiData.salary !== "" && mamiData.salary !== 0 && mamiData.salary !== "Login untuk melihat gaji") {
			dataLayerProps.salary = mamiData.salary.toString();
		};

		if (mamiData.city !== null) {
			dataLayerProps.city = mamiData.city;
		};

		if (mamiData.subdistrict !== null) {
			dataLayerProps.area = mamiData.subdistrict;
		};

		if (mamiData.photo.medium !== null) {
			dataLayerProps.image_url = mamiData.photo.medium;
		};

		if (window.innerWidth < 768) {
			dataLayerProps.device = "mobile";
		} else {
			dataLayerProps.device = "desktop";
		};

		try {
			dataLayerProps.utm_source = dataLayerParams.get('utm_source');
			if (dataLayerProps.utm_source === null) dataLayerProps.utm_source = "none";
		} catch (e) {
			dataLayerProps.utm_source = "none";
		};

		try {
			dataLayerProps.utm_medium = dataLayerParams.get('utm_medium');
			if (dataLayerProps.utm_medium === null) dataLayerProps.utm_medium = "none";
		} catch (e) {
			dataLayerProps.utm_source = "none";
		};

		try {
			dataLayerProps.utm_content = dataLayerParams.get('utm_content');
			if (dataLayerProps.utm_content === null) dataLayerProps.utm_content = "none";
		} catch (e) {
			dataLayerProps.utm_content = "none";
		};

		try {
			dataLayerProps.utm_campaign = dataLayerProps.dataLayerParams.get('utm_campaign');
			if (dataLayerProps.utm_campaign === null) dataLayerProps.utm_campaign = "none";
		} catch (e) {
			dataLayerProps.utm_campaign = "none";
		};

		
		dataLayer.push({
			"language": "id",
			"page_category": "ImpressionLoker",
			"currency": "IDR",
			"device": dataLayerProps.device,
			"utm_source": dataLayerProps.utm_source,
			"utm_medium": dataLayerProps.utm_medium,
			"utm_content": dataLayerProps.utm_content,
			"utm_campaign": dataLayerProps.utm_campaign,
			"page_url": mamiData.share_url,
			"step": 3,
			"country": "Indonesia",
			"city": dataLayerProps.city,
			"area": dataLayerProps.area,
			"image_url": dataLayerProps.image_url,
			"status": mamiData.type,
			"salary": dataLayerProps.salary,
			@if (Auth::check())
			"customer_id": "{{ (Auth::user()->id) }}",
			"profile_id": "{{ (Auth::user()->id) }}",
			"gender": "{{ (Auth::user()->gender) }}"
			@else
			"customer_id": "none",
			"profile_id": "none",
			"gender": "none"
			@endif
		});
	</script>

	<script>
	window.addEventListener(
		'load',
		function() {
			if (!window.fbq || typeof fbq === 'undefined') {
				window.fbq = function() {
					return null;
				};
			} else {
				dataLayer.push({
					'event': 'ViewProduct',
					'ecommerce': {
						'currencyCode': 'IDR',
						'detail': {
							'actionField': {
								'list': 'Home'
							},
							'products': {
								'name': mamiData.title,
								'id': mamiData.id,
								'price': 0,
								'brand': mamiData.title,
								'category': 'Job Vacancy',
								'type': 'Job'
							}

						}
					}
				});
			}
		},
		false
	);
	</script>
@endsection

@section('content')
	{{-- Prerendered Priority Contents --}}
	<div id="prerender">

		<section>

			<div class="prerender-header col-md-12 col-sm-12 col-xs-12">
				<div class="container">
					<h2 class="prerender-company">{{ $data['place_name'] }}</h2>
				</div>
			</div>

			<div class="container prerender-container">

				<div class="prerender-content col-md-12 col-sm-12 col-xs-12">

					<div class="prerender-content_left col-md-8 col-sm-8 col-xs-12">
						<div class="prerender-title">
							<h1 class="prerender-title">{{{ $data['title'] }}}</h1>
						</div>

						<div class="prerender-box">
							<div class="prerender-fill"></div>
						</div>

						<div class="prerender-detail hidden-lg hidden-md hidden-sm">
							<div class="prerender-fill">
								<h4 class="prerender-salary">{{ $data['salary'] }}</h4>
							</div>
						</div>

						<div class="prerender-box hidden-sm hidden-md hidden-lg">
							<div class="prerender-fill"></div>
						</div>

						<div class="prerender-subcategory hidden-sm hidden-md hidden-lg">
							<h2 class="prerender-subtitle">Deskripsi Pekerjaan</h2>
						</div>

						<div class="prerender-description">
								<h2 class=" prerender-label hidden-xs">Deskripsi Pekerjaan</span>
								<h2 class="prerender-description_text">{!! $data['description'] !!}</h2>
						</div>
					</div>

					<div class="prerender-content_right col-md-4 col-sm-4 hidden-xs">
						<div class="prerender-detail">
							<div class="prerender-fill">
								<h4 class="prerender-salary">{{ $data['salary'] }}</h4>
							</div>
						</div>
					</div>

				</div>

				<div class="prerender-subcategory col-md-12 col-sm-12 col-xs-12">
					<h4 class="prerender-subtitle">Rekomendasi Lowongan Kerja Lainnya</h4>
				</div>

				<div class="prerender-box col-md-12 col-sm-12 col-xs-12">
					<div class="prerender-fill"></div>
				</div>

				<div class="prerender-subcategory col-md-12 col-sm-12 col-xs-12">
					<h4 class="prerender-subtitle">Peta Lokasi Perusahaan</h4>
				</div>

				<div class="prerender-map col-md-12 col-sm-12 col-xs-12">
					<div class="prerender-fill"></div>
				</div>

			</div>

		</section>
	</div>
	{{-- Complete Content After JS Rendered --}}
		<div id="app">
			<app></app>
		</div>
@endsection

@section('script')
	@include('web.@script.global-auth')

	<script src="{{ mix_url('dist/js/detail-vacancy.js') }}"></script>

	@if(!$data['is_expired'] || !(isset($data['deleted_at']) || $data['deleted_at'] === null))
	<script type="application/ld+json">
		{
			"@context": "http://schema.org",
			"@type": "JobPosting",
			"hiringOrganization": "{{ $data['place_name'] }}",
			"validThrough": "{{ $data['valid_through'] ?? "-" }}",
			"datePosted": "{{ $data['created_at_schema'] }}",
			"description": "{{ $data['description_meta'] ?? "-" }}",
			"educationRequirements": "{{ $data['last_education'] }}",
			"employmentType": "{{ $data['type'] }}",
			"jobLocation": {
				"@type": "Place",
				"address": {
					"@type": "PostalAddress",
					"streetAddress": "{{ $data['address'] }}",
					"postalCode": "-",
					"addressLocality": "{{ $data['subdistrict_schema'] ?? "" }}",
					"addressRegion": "{{ $data['city'] }}"
				}
			},
			"occupationalCategory": "{{ $data['spesialisasi'] }}",
			"salaryCurrency": "IDR",
			"title": "{{ $data['title'] }}"
		}
	</script>
	@endif

	<script type="application/ld+json">
		{
			"@context": "http://schema.org",
			"@type": "BreadcrumbList",
			"itemListElement":
				[
				@php
					$no=1
				@endphp
				@foreach($data['breadcrumb'] as $breadcumb)
					{
					"@type": "ListItem",
					"position": {{ $no }},
					"item":
						{
							"@id": "{{ $breadcumb['url'] }}",
							"name": "{{ $breadcumb['name'] }}"
						}
					}
				@if(count($data['breadcrumb']) != $no++)
					{{ ',' }}
				@endif

				@endforeach
				]
		}
	</script>

	@include('web.@script.aws-kds-logged-user-tracker')
@endsection