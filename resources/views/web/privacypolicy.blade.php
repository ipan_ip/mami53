<!DOCTYPE html>
<html lang="id">

<head>
	<title>Mamikos - Kebijakan Privasi</title>
	<link rel="canonical" href="https://mamikos.com/privacy" />
	<link rel="author" href="https://plus.google.com/u/1/+Mamikos/posts"/>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	
	@include('web.@meta.favicon')

	<meta name="classification" content="Business, Rent House, Sewa Kost, Property, Rent Room, Info Kost, Information, Kost, Room, Cari Kost, Kost Murah, Kost Bebas, Application, Mobile Application">
	<meta name="distribution" content="global">
	<meta http-equiv="Reply-to" content="mamikos.app@gmail.com">
	<meta name="revisit-after" content="1 day">
	<meta name="keyphrases" content="Info Kost, Cari Kost, Sewa Kost, Kost Bebas, Kost Murah, Kost pasutri, Aplikasi Kost, Aplikasi Pencarian Kost, Aplikasi Info Kost, APlikasi Cari Kost, Kost, Mamikost, Mamikosapp">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta name="keywords" content="mamikost, Info Kost, Cari kost, kost yogyakarta, kost jogja, kost jakarta, kost bandung, kost depok, kost surabaya, indekost, kos, indekos, sewa">
	<meta name="description" content="MAMIKOS adalah aplikasi pencari kost no.1 di Indonesia. MAMIKOS menyediakan layanan pencari kost yang gampang dan cepat serta memiliki data kost yang akurat.">
	<meta name="author" content="Mamikos">
	<meta name="robots" content="index, follow">
	<meta property="fb:app_id" content="607562576051242"/>
	<meta property="og:title" content="Mamikos - Kebijakan Privasi" />
	<meta name="og:site_name" content="Mamikos"/>
	<meta property="og:description" content="MAMIKOS adalah aplikasi pencari kost no.1 di Indonesia. MAMIKOS menyediakan layanan pencari kost yang gampang dan cepat serta memiliki data kost yang akurat." />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="https://mamikos.com/privacy" />
	<meta property="og:image" content="/assets/share-image-default.jpg" />
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:site" content="@mamikosapp">
	<meta name="twitter:title" content="Mamikos - Kebijakan Privasi">
	<meta name="twitter:description" content="MAMIKOS adalah aplikasi pencari kost no.1 di Indonesia. MAMIKOS menyediakan layanan pencari kost yang gampang dan cepat serta memiliki data kost yang akurat.">
	<meta name="twitter:creator" content="@mamikosapp">
	<meta name="twitter:image" content="/assets/share-image-default.jpg">
	<meta name="twitter:domain" content="mamikos.com">

	<link rel="stylesheet" href="{{ mix_url('dist/css/vendor.css') }}">

	<style>
		html,body {
			font-family: 'Lato', sans-serif;
			background: url(/assets/100-1.jpg) no-repeat center center fixed;
			background-size: cover;
			font-size: 14px;
		}
		a,
		a:active,
		a:hover,
		a:focus {
			color: #1BAA56;
		}

		.title {
			font-size: 27px;
		}

		h1 {
			font-size: 18px;
		}

		.box {
			margin-top: 50px;
			margin-bottom: 50px;
			padding: 30px 50px;
			background-color: white;
		}
		
		.top {
			color:#ADADAD;
			font-size: 12px;
		}
	</style>


	<!-- Google Tag Manager -->
	<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-K9V8K4"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
			new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-K9V8K4');</script>
	<!-- End Google Tag Manager -->
</head>

<body>
	<div class="container-fluid">
		<div class="row backdrop">
			<div clas="col-xs-12">

				<div class="col-xs-12 col-md-8 col-md-offset-2 box">
					@if(request()->input('from') !== 'webview')
					<div class="top">
						<a style="color:#ADADAD" href="/">Home</a>> Privacy Policy
					</div>
					@endif
					<img src="/assets/logo_3.png" style="width:200px;margin-top:30px"></img>
					<h1 class="title">Kebijakan Privasi MAMIKOS</h1>

					@include('web.privacypolicy-content')
					
				</div>
			</div>
		</div>
	</div>
</body>
</html>



