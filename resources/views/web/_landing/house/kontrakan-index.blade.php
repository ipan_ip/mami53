@extends('web.@layout.globalLayout')


@section('head')
	@if (!empty($_SERVER['HTTP_HOST']))
		@if ($_SERVER['HTTP_HOST'] == 'mamikos.com')
			<meta name="robots" content="index, follow">
		@else
			<meta name="robots" content="noindex, nofollow">
		@endif
	@else
		<meta name="robots" content="noindex, nofollow">
	@endif

	@if ($dummyCounterTitle == '')
		<title>{{ $heading_1 }} - Mamikos</title>
	@else
		<title>{{ $dummyCounterTitle }}</title>
	@endif
	@if ($dummyCounterMetaDescription == '')
		<meta name="description" content="Temukan promo dan diskon {{ $heading_1 }} dan sekitarnya dengan harga terbaik.!">
	@else
		<meta name="description" content="{{ $dummyCounterMetaDescription }}">
	@endif
	<link rel="canonical" href="{{ url(str_replace('http','https',$breadcrumb[sizeof($breadcrumb)-1]['url'])) }}">

	<meta property="og:url" content="{{ url(str_replace('http','https',$breadcrumb[sizeof($breadcrumb)-1]['url'])) }}">
	@if ($dummyCounterTitle == '')
		<meta property="og:title" content="{{ $heading_1 }} - Mamikos">
	@else
		<meta property="og:title" content="{{ $dummyCounterTitle }}">
	@endif
	<meta property="og:image" content="/general/img/pictures/share-image-default.jpg">
	@if ($dummyCounterMetaDescription == '')
		<meta name="og:description" content="Temukan promo dan diskon {{ $heading_1 }} dan sekitarnya dengan harga terbaik.!">
	@else
		<meta name="og:description" content="{{ $dummyCounterMetaDescription }}">
	@endif

	<meta name="twitter:url" content="{{ url(str_replace('http','https',$breadcrumb[sizeof($breadcrumb)-1]['url'])) }}">
	@if ($dummyCounterTitle == '')
		<meta property="twitter:title" content="{{ $heading_1 }} - Mamikos">
	@else
		<meta property="twitter:title" content="{{ $dummyCounterTitle }}">
	@endif
	@if ($dummyCounterMetaDescription == '')
		<meta name="twitter:description" content="Temukan promo dan diskon {{ $heading_1 }} dan sekitarnya dengan harga terbaik.!">
	@else
		<meta name="twitter:description" content="{{ $dummyCounterMetaDescription }}">
	@endif
	<meta name="twitter:image" content="/general/img/pictures/share-image-default.jpg">

	@if (is_null($area_subdistrict) || is_null($area_city))
		<meta name="keywords" content="kontrakan, rumah kontrakan, info kontrakan">
	@else
		<meta name="keywords" content="kontrakan, rumah kontrakan, info kontrakan, sewa kontrakan {{ $area_subdistrict }} {{ $area_city }}, kontrakan murah {{ $area_subdistrict }} {{ $area_city }}, sewa kontrakan {{ $area_subdistrict }} {{ $area_city }}, info kontrakan {{ $area_subdistrict }} {{ $area_city }}">
	@endif

	<link rel="alternate" href="android-app://com.git.mami.kos/mamikos/kontrakan/{{ $slug }}" />

	<link rel="preload" href="{{ mix_url('dist/js/_landing/house/app.js') }}" as="script">

@endsection


@section('data')
	<script>
		var relatedKostLink = null;
		var relatedApartmentLink = null;
		var relatedVacancyLink = null;

		var landingRelatedArea = @JZON($landingRelatedsArea);
		var landingRelatedCampus = @JZON($landingRelatedsCampus);

		var description1 = @JZON($description);
		var description2 = '';
		var description3 = '';
		var description = {
			description_1: description1,
			description_2: description2,
			description_3: description3
		};

		var rentType = @JZON($rent_type);

		var priceMin = @JZON($price_min);
		var priceMax = @JZON($price_max);
		var priceRange = [priceMin, priceMax];

		var tagIds = @JZON($tag_ids);
		var slug = @JZON($slug);

		var longitude_1 = @JZON($longitude_1);
		var latitude_1 = @JZON($latitude_1);
		var longitude_2 = @JZON($longitude_2);
		var latitude_2 = @JZON($latitude_2);
		var locPage = [[longitude_1, latitude_1],[longitude_2, latitude_2]];
		var cornerMap = [[latitude_1, longitude_1], [latitude_2, longitude_2]];

		var centerLatitude = @JZON($centerLatitude);
		var centerLongitude = @JZON($centerLongitude);

		var landing_connector = [];
		var breadcrumb = @JZON($breadcrumb);
		var place = null;
		var radius = 0;

		var landingType = 'kontrakan';
	</script>
@endsection

@section('styles')
	@include('web.@style.preloading-style')
@endsection

@section('content')
	@include('web.@style.preloading-content')

	<div id="app">
		<app></app>
	</div>
@endsection


@section('scripts')
	<script src="{{ mix_url('dist/js/_landing/house/app.js') }}" async defer></script>

	<script type="application/ld+json">
		{
			"@context": "http://schema.org",
			"@type": "BreadcrumbList",
			"itemListElement": [
				@foreach ($breadcrumb as $key => $item)
					{
						"@type": "ListItem",
						"position": {{ $key + 1 }},
						"item": {
							"@id": "{{ $item["url"] }}",
							"name": "{{ $item["name"] }}"
						}
					}
					@if ($key != count($breadcrumb) - 1)
					,
					@endif
				@endforeach
			]
		}
	</script>

	<script type="application/ld+json">
		{
			"@context" : "http://schema.org",
			"@type" : "LodgingBusiness",
			"hasMap" : "https://maps.google.com/maps?z=7&q={{ $centerLatitude }},{{ $centerLongitude }}",
			"url" : "https://mamikos.com/kontrakan/{{ $slug }}",
			@if (!is_null($price_min) && !is_null($price_max))
			"priceRange" : "Rp {{ $price_min }} - Rp {{ $price_max }}",
			@endif
			"name" : "{{ $keyword }}",
			"telephone": "0856-4140-5202"
		}
	</script>
@endsection
