@extends('web.layout.landingOldLayout')
@section('meta')
  <base href="/">
  <title>Beri Rating Customer Service - Mamikos</title>
  <link rel="canonical" href="https://mamikos.com/ratecs/" />
  <meta name="robots" content="noindex, nofollow">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

  @include('web.@meta.favicon')

  <meta property="fb:app_id" content="607562576051242"/>

@endsection

@section('style')
<style>
  body {
    font-family: 'Lato', sans-serif;
    font-size: 14px;
    line-height: 1.42857143;
    color: #333;
    background-color: #fff;
  }

  .navbar-ratecs {
    background: #1BAA56;
    padding: 5px;
    text-align: center;
  }

  .navbar-ratecs .header-logo {
    height: 40px;
  }

  .ratecs-content h1 {
    color: #1BAA56;
    font-size: 24px;
    font-weight: bold;
    margin-bottom: 15px;
    padding-bottom: 15px;
    position: relative;
  }

  .ratecs-content h1:before {
    background: #1BAA56;
    bottom: 0;
    content: "";
    display: block;
    height: 2px;
    left: 0;
    position: absolute;
    width: 100px;
  }

  .ratecs-content p.content-big {
    font-size: 18px;
  }

  .ratecs-separator {
    background: #eee;
    height: 3px;
    margin: 20px 0;
  }

  .br-theme-fontawesome-stars .br-widget {
    height: 60px;
  }

  .br-theme-fontawesome-stars .br-widget a {
    font-size: 56px;
  }

  .br-theme-fontawesome-stars .br-widget a.br-selected:after,
  .br-theme-fontawesome-stars .br-widget a.br-active:after {
    color: #1BAA56;
  }

  .btn-ratecs {
    background: #1BAA56;
    color: #fff;
  }

  .btn-ratecs:hover,
  .btn-ratecs:active,
  .btn-ratecs:focus {
    background: #1BAA56;
    border-color: #1BAA56;
    color: #fff;
  }

  /*Include Footer*/
  .page-footer {
    padding-top: 10px;
    padding-bottom: 10px;
  }
  .logo-footer {
    display: flex;
    align-items: center;
    justify-content: center;
  }
  .logo-footer .img-logo-footer {
    width: 240px;
  }
  .text-footer {
    color: #000;
    text-align: center;
  }
  .link-footer,
  .social-footer,
  .contact-footer {
    display: flex;
    align-items: center;
    justify-content: center;
    flex-wrap: wrap;
    padding-top: 10px;
    padding-bottom: 10px;
  }
  .link-footer > a:hover {
    cursor: pointer;
    text-decoration: underline;
  }
  @media screen and (max-width: 768px) {
    .link-footer > a {
      color: #009b4c;
    }
  }
  @media screen and (min-width: 767px) {
    .link-footer > a {
      color: #009b4c;
      margin-right: 10px;
      margin-left: 10px;
    }
  }
  .link-footer > span {
    color: #888;
  }
  .social-footer > a {
    margin-right: 5px;
    margin-left: 5px;
  }
  .social-footer .icon-social-footer {
    width: 45px;
  }
  .contact-footer > a {
    color:#333;
    text-decoration: none;
    margin-right: 2px;
    margin-left: 2px;
  }
  .contact-footer .icon-contact-footer {
    width: 20px;
  }
  .contact-footer .text-contact-footer {
    font-size: 11px;
    font-weight: bold;
  }
</style>
@endsection

@section('body-start')
<body>
@endsection

@section('body-content')
  <nav class="navbar navbar-new navbar-ratecs">
    <div class="container-fluid">
      <a href="/">
        <img class="header-logo" alt="mamikos.com" title="Mamikos.com | Home" src="/assets/home-mobile/mamikos_header_mobile.png">
      </a>
    </div>
  </nav>

  <div class="container">
    <div class="row">
      <div class="col-md-6 col-md-push-3 ratecs-content">
        
        <h1>Beri Rating CS Mamikos</h1>

        @if($allowed)
          

          @if (count($errors) > 0)
            <div class="alert alert-danger">
              <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
              </ul>
          </div>
          @endif

          <form action="/ratecs" method="post">
            {{csrf_field()}}
            <input type="hidden" name="signer" value="{{$expire}}">
            <input type="hidden" name="cid" value="{{$csId}}">
            <input type="hidden" name="gid" value="{{$groupId}}">

            <div class="form-group">
              <select name="rating" id="rating" class="form-control">
                @for($i = 0; $i <= 4; $i++)
                  <option value="{{$i == 0 ? '' : $i}}">{{$i == 0 ? '- Beri Rating -' : $i}}</option>
                @endfor
              </select>
            </div>

            <div class="form-group">
              <textarea name="comment" id="comment" class="form-control" rows="4" placeholder="Beri Komentar"></textarea>
            </div>

            <div class="form-group">
              <button type="submit" class="btn btn-default btn-ratecs">Beri Rating</button>
            </div>
          </form>

        @else

          <p class="content-big">
            {{$reason}}
          </p>

        @endif
        
      </div>
    </div>
  </div>

  <hr class="ratecs-separator">
  
  @include ('web.part.footer')

@endsection

@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-bar-rating/1.2.2/jquery.barrating.min.js"></script>
<script>
    
  $(function() {
    $('#rating').barrating({
      theme: 'fontawesome-stars'
    });

    setInterval(function() {
      $.ajax({
        type: 'GET',
        url: "/drip",
        headers : {"Content-Type":"application/json", "X-GIT-Time": "1406090202", "Authorization": "GIT WEB:WEB"},
        crossDomain: true,
        success: function (data) {
          
        }
      });
    }, 60 * 30 * 1000);
  });

</script>
@endsection