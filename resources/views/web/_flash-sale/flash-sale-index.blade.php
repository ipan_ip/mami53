@extends('web.@layout.globalLayout')


@section('head')
	@if (!empty($_SERVER['HTTP_HOST']))
		@if ($_SERVER['HTTP_HOST'] == 'mamikos.com')
			<meta name="robots" content="index, follow">
		@else
			<meta name="robots" content="noindex, nofollow">
		@endif
	@else
		<meta name="robots" content="noindex, nofollow">
	@endif

	<title>Bersiap menuju Mamikos Promo Ngebut! - Mamikos</title>
	<meta name="description" content="Jangan sampai kelewat! sebentar lagi mamikos promo ngebut akan di buka, flash sale diskon kost besar besaran di mamikos, lihat & bersiap menuju mamikos promo ngebut">
	<link rel="canonical" href="https://mamikos.com/promo-ngebut">

	<meta property="og:url" content="https://mamikos.com/promo-ngebut">
	<meta property="og:title" content="Bersiap menuju Mamikos Promo Ngebut! - Mamikos">
	<meta property="og:image" content="/assets/og/og_kost_v2.jpg">
	<meta property="og:description" content="Jangan sampai kelewat! sebentar lagi mamikos promo ngebut akan di buka, flash sale diskon kost besar besaran di mamikos, lihat & bersiap menuju mamikos promo ngebut">

	<meta name="twitter:url" content="https://mamikos.com/promo-ngebut/">
	<meta name="twitter:title" content="Bersiap menuju Mamikos Promo Ngebut! - Mamikos">
	<meta name="twitter:description" content="Jangan sampai kelewat! sebentar lagi mamikos promo ngebut akan di buka, flash sale diskon kost besar besaran di mamikos, lihat & bersiap menuju mamikos promo ngebut">
	<meta name="twitter:image" content="/assets/og/og_kost_v2.jpg">

	<meta name="keywords" content="mamikos promo ngebut, flash sale mamikos, flash sale kost, promo kost, diskon kost">

	<link rel="preload" href="{{ mix_url('dist/js/_flash-sale/app.js') }}" as="script">
@endsection

@section('data')
	<script>
    var flashSaleData = @JZON($flashSaleData);
  </script>
@endsection

@section('styles')
	@include('web.@style.preloading-style')
@endsection

@section('content')
	@include('web.@style.preloading-content')
	<div id="app">
		<app></app>
	</div>
@endsection


@section('scripts')
	<script src="{{ mix_url('dist/js/_flash-sale/app.js') }}" async defer></script>
@endsection