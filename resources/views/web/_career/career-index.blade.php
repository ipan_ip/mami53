@extends('web.@layout.globalLayout')

@section('head')
	@if (!empty($_SERVER['HTTP_HOST']))
		@if ($_SERVER['HTTP_HOST'] == 'mamikos.com')
			<meta name="robots" content="index, follow">
		@else
			<meta name="robots" content="noindex, nofollow">
		@endif
	@else
		<meta name="robots" content="noindex, nofollow">
	@endif

	<title>Join our journey and find your dream career here - Mamikos</title>
	<meta name="description" content="Our team in Mamikos is made up of people who enjoy making things people love. We're building a company where the principles of sharing and openness have taken the place of our everydays life.">
	<link rel="canonical" href="https://mamikos.com/career/">

	<meta property="og:url" content="https://mamikos.com/career/">
	<meta property="og:title" content="Join our journey and find your dream career here - Mamikos">
	<meta property="og:image" content="/general/img/pictures/share-image-default.jpg">
	<meta property="og:description" content="Our team in Mamikos is made up of people who enjoy making things people love. We're building a company where the principles of sharing and openness have taken the place of our everydays life.">

	<meta name="twitter:url" content="https://mamikos.com/career/">
	<meta name="twitter:title" content="Join our journey and find your dream career here - Mamikos">
	<meta name="twitter:description" content="Our team in Mamikos is made up of people who enjoy making things people love. We're building a company where the principles of sharing and openness have taken the place of our everydays life.">
	<meta name="twitter:image" content="/general/img/pictures/share-image-default.jpg">

	<meta name="keyphrases" content="Info Kost, Cari Kost, Sewa Kost, Kost Bebas, Kost Murah, Kost pasutri, Aplikasi Kost, Aplikasi Pencarian Kost, Aplikasi Info Kost, APlikasi Cari Kost, Kost, Mamikost, Mamikosapp, Kamar Kost, Kamar Kos, Kostan, Kos, Rumah Kost, Rumah Kos, Kost Harian">

	<meta name="classification" content="Business, Rent House, Sewa Kost, Property, Rent Room, Info Kost, Information, Kost, Room, Cari Kost, Kost Murah, Kost Bebas, Application, Mobile Application, Kamar Kost, Kamar Kos, Kostan, Kos, Rumah Kost, Rumah Kos, Kost Harian">

	<meta name="keywords" content="mamikost, Info Kost, Cari kost, kost, Kamar Kost, Kamar Kos, Kostan, Kos, Rumah Kost, Rumah Kos, Kost Harian">

	<link rel="stylesheet" href="/css/fonts/tiempos-headline.css">

	<link rel="preload" href="{{ mix_url('dist/js/_career/app.js') }}" as="script">
@endsection

@section('data')

@endsection


@section('styles')
	@include('web.@style.preloading-style')
@endsection


@section('content')
	@include('web.@style.preloading-content')

	<div id="app">
		<app></app>
	</div>
@endsection


@section('scripts')
	<script src="{{ mix_url('dist/js/_career/app.js') }}" async defer></script>
@endsection
