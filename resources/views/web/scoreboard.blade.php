<!DOCTYPE html>
<html>
<head>
  @if($_SERVER['HTTP_HOST'] != 'mamikos.com')
  <meta name="robots" content="noindex, nofollow">
  @endif
      <base href="/">
      @yield('meta-custom')
      <meta name="csrf-token" content="{{ csrf_token() }}">

      <meta name="application-name" content="Mamikos">
      <meta name="theme-color" content="#1BAA56">
      <link rel="manifest" href="/json/manifest.json">

      @include('web.@meta.link-prebrowsing')

      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
      <meta charset="utf-8">

      @include('web.@meta.favicon')

  <!--  Fonts, Icons and CSS
      ================================================== -->
      @include('web.part.external.global-head')
      <link href="{{ mix_url('dist/css/common.css') }}" rel="stylesheet">

      
</head>
<body>
  @include('web.part.external.gtm-body')

  <div class="scoreboard box col-xs-12 col-md-6 col-md-offset-3 stretch">

    <ul class="nav nav-tabs col-md-6">
      <li class="col-xs-4 active"><a data-toggle="tab" href="#trade">Tukarkan Point</a></li>
      <li id="statusTab" class="col-xs-4"><a data-toggle="tab" href="#status">Status Input Kos</a></li>
      <li class="col-xs-4"><a data-toggle="tab" href="#rank">Ranking Saya</a></li>
    </ul>

     <div class="tab-content">

       <div id="trade" class="tab-pane fade in active">
         <div class="label-status">
           <label>Point Saya</label>
           <label class="number-status verified">{{ $userTotalPoint }}</label>
         </div>
         <div class="get-point">
           <label>Cara mendapatkan point :</label>
           <div>
             1. <span class="verified">Input Kost</span> dapat <span class="verified">2 point<br></span>
             2. <span class="verified">Input Kost secara lengkap & Verif </span>dapat<span class="verified"> 10 point</span>
           </div>
           <div><a href="https://mamikos.com/input-kost"><button class="btn btn-mamiorange">Tambahkan Data Kost</button></a></div>
         </div>
         <label class="label-status">Tukarkan dengan Hadiah</label>
         <table class="table exchange-point">
           <tbody>
             <tr>
               <td class="items">
                 <span>Pulsa 50rb</span>
                 <span class="verified">20</span>
               </td>
               @if($userTotalPoint >= 20)
               <td class="trade-nav"><a data-toggle="modal" data-target="#modalPulsa">TUKAR</a></td>
               @else
               <td class="trade-nav off"><label>TUKAR</label></td>
               @endif
             </tr>
             <tr>
               <td class="items">
                 <span>Pulsa Listrik 75rb</span>
                 <span class="verified">80</span>
               </td>
               @if($userTotalPoint >= 80)
               <td class="trade-nav"><a data-toggle="modal" data-target="#modalListrik">TUKAR</a></td>
               @else
               <td class="trade-nav off"><label>TUKAR</label></td>
               @endif
             </tr>
             <tr>
               <td class="items">
                 <span>Saldo Go-Pay 50ribu</span>
                 <span class="verified">20</span>
               </td>
               @if($userTotalPoint >= 20)
               <td class="trade-nav"><a data-toggle="modal" data-target="#modalGopay">TUKAR</a></td>
               @else
               <td class="trade-nav off"><label>TUKAR</label></td>
               @endif
             </tr>
             <tr>
               <td class="items">
                 <span>2 Tiket Nonton</span>
                 <span class="verified">40</span>
               </td>
               @if($userTotalPoint >= 40)
               <td class="trade-nav"><a data-toggle="modal" data-target="#modalTiket">TUKAR</a></td>
               @else
               <td class="trade-nav off"><label>TUKAR</label></td>
               @endif
             </tr>
             <tr>
               <td class="items">
                 <span>Headset</span>
                 <span class="verified">50</span>
               </td>
               @if($userTotalPoint >= 50)
               <td class="trade-nav"><a data-toggle="modal" data-target="#modalHeadset">TUKAR</a></td>
               @else
               <td class="trade-nav off"><label>TUKAR</label></td>
               @endif
             </tr>
             <tr>
               <td class="items">
                 <span>Subsidi uang kost 500rb</span>
                 <span class="verified">200</span>
               </td>
               @if($userTotalPoint >= 200)
               <td class="trade-nav"><a data-toggle="modal" data-target="#modalKost">TUKAR</a></td>
               @else
               <td class="trade-nav off"><label>TUKAR</label></td>
               @endif
             </tr>
           </tbody>
         </table>

         <div class="modal-trade modal fade" id="modalPulsa" role="dialog">
           <div class="modal-dialog modal-sm">
             <div class="modal-content">
               <div class="modal-header">
                 <label>Tukarkan Point</label>
               </div>
               <div class="modal-body">
                 <p>Anda yakin akan menukarkan <br> point Anda dengan</p>
                 <p><span>Pulsa 50rb</span></p>
                 <p><span>50 Point</span></p>
                 <a href="https://wa.me/6285641405202?text=TUKAR%2350%23PULSA50RB" target="_blank" rel="noopener"><button class="btn btn-mamiorange">OK</button>
               </div>
             </div>
           </div>
         </div>
         <div class="modal-trade modal fade" id="modalListrik" role="dialog">
           <div class="modal-dialog modal-sm">
             <div class="modal-content">
               <div class="modal-header">
                 <label>Tukarkan Point</label>
               </div>
               <div class="modal-body">
                 <p>Anda yakin akan menukarkan <br> point Anda dengan</p>
                 <p><span>Pulsa Listrik 75ribu</span></p>
                 <p><span>30 Point</span></p>
                 <a href="https://wa.me/6285641405202?text=TUKAR%2380%23PULSALISTRIK75RB" target="_blank" rel="noopener"><button class="btn btn-mamiorange">OK</button>
               </div>
             </div>
           </div>
         </div>
         <div class="modal-trade modal fade" id="modalGopay" role="dialog">
           <div class="modal-dialog modal-sm">
             <div class="modal-content">
               <div class="modal-header">
                 <label>Tukarkan Point</label>
               </div>
               <div class="modal-body">
                 <p>Anda yakin akan menukarkan <br> point Anda dengan</p>
                 <p><span>Saldo Go-Pay 50ribu</span></p>
                 <p><span>50 Point</span></p>
                 <a href="https://wa.me/6285641405202?text=TUKAR%2350%23SALDOGOPAY50RB" target="_blank" rel="noopener"><button class="btn btn-mamiorange">OK</button>
               </div>
             </div>
           </div>
         </div>
         <div class="modal-trade modal fade" id="modalTiket" role="dialog">
           <div class="modal-dialog modal-sm">
             <div class="modal-content">
               <div class="modal-header">
                 <label>Tukarkan Point</label>
               </div>
               <div class="modal-body">
                 <p>Anda yakin akan menukarkan <br> point Anda dengan</p>
                 <p><span>2 Tiket Nonton</span></p>
                 <p><span>100 Point</span></p>
                 <a href="https://wa.me/6285641405202?text=TUKAR%23100%232TIKETNONTON" target="_blank" rel="noopener"><button class="btn btn-mamiorange">OK</button></a>
               </div>
             </div>
           </div>
         </div>
         <div class="modal-trade modal fade" id="modalHeadset" role="dialog">
           <div class="modal-dialog modal-sm">
             <div class="modal-content">
               <div class="modal-header">
                 <label>Tukarkan Point</label>
               </div>
               <div class="modal-body">
                 <p>Anda yakin akan menukarkan <br> point Anda dengan</p>
                 <p><span>Headset</span></p>
                 <p><span>200 Point</span></p>
                 <a href="https://wa.me/6285641405202?text=TUKAR%23200%23HEADSET" target="_blank" rel="noopener"><button class="btn btn-mamiorange">OK</button></a>
               </div>
             </div>
           </div>
         </div>
         <div class="modal-trade modal fade" id="modalKost" role="dialog">
           <div class="modal-dialog modal-sm">
             <div class="modal-content">
               <div class="modal-header">
                 <label>Tukarkan Point</label>
               </div>
               <div class="modal-body">
                 <p>Anda yakin akan menukarkan <br> point Anda dengan</p>
                 <p><span>Subsidi uang kost</span></p>
                 <p><span>500 Point</span></p>
                 <a href="https://wa.me/6285641405202?text=TUKAR%23500%23SUBSIDIUANGKOST" target="_blank" rel="noopener"><button class="btn btn-mamiorange">OK</button></a>
               </div>
             </div>
           </div>
         </div>
       </div>

         <div id="status" class="tab-pane fade">
          <div class="label-status">
            <label>Total Kost Terverifikasi</label>
            <label class="number-status verified">{{ $userTotalVerified }}</label>
          </div>
          <div class="label-status">
            <label>Total Kost Terinput</label>
            <label class="number-status input-kost">{{ $userTotalInput }}</label>
          </div>
          <table class="table status-input">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama Kost</th>
                <th>Status</th>
                <th>Point</th>
              </tr>
            </thead>
            <tbody>            
              @if(!is_null($history)) 
                  @foreach($history as $key => $historyItem)
                    <tr>
                      <td>{{$key + 1}}</td>
                      <td class="kost-name">{{ $historyItem->description }}</td>
                      @if($historyItem->status == 'verified')
                      <td class="verif-stat verified">{{ $historyItem->status }}</td>
                      <td class="verif-stat verified">{{ $historyItem->point }}</td>
                      @else
                      <td class="verif-stat">{{ $historyItem->status }}</td>
                      <td class="verif-stat">{{ $historyItem->point }}</td>
                      @endif
                    </tr>
                  @endforeach
                @endif
          </tbody>
          </table>
         </div>

         <div id="rank" class="tab-pane fade">
           <div class="announce">
             <div><label>Hadiah TOP 3 periode sampai dengan<span class="verified"> 31 Januari 2018</span></label></div>
             <div><label>Peringkat<span class="verified"> 1 : Xiaomi Redmi 4X</span></label></div>
             <div><label>Peringkat<span class="verified"> 2 : Xiaomi Yi 16MP</span></label></div>
             <div><label>Peringkat<span class="verified"> 3 : Jam Tangan</span></label></div>
           </div>
           @if(!is_null($userRank))
           <table class="table my-rank">
             <thead>
               <tr><th colspan="3">Ranking Saya</th></tr>
             </thead>
             <tbody>
               @foreach($userRank as $key => $userRankItem)
               <tr>
                 <td>{{ $userRankItem->rank }}</td>
                 <td class="kost-name">{{ $user->name }}</td>
                 <td>{{ $userRankItem->total }}</td>
               </tr>
               @endforeach
             </tbody>
           </table>
           @endif
           <table class="table top-ten">
             <thead>
               <tr><th colspan="3">Top 10</th></tr>
             </thead>
             <tbody>
               @if(!is_null($leaderboards))
               @foreach($leaderboards as $key => $leaderboardsItem)
               <tr>
                 <td>{{ $key + 1 }}</td>
                 <td class="kost-name">{{ $leaderboardsItem->user->name }}</td>
                 <td>{{ $leaderboardsItem->total_point }} Point</td>
               </tr>
               @endforeach
               @endif
             </tbody>
           </table>
         </div>

         <div class="modal fade" id="termsModal" role="dialog">
           <div class="modal-dialog modal-sm">
             <div class="modal-content">
               <div class="modal-header">
                 <label>SYARAT & KETENTUAN</label>
               </div>
               <div class="modal-body">
                 <p>
                   1. Data tidak duplikat dengan<br>data iklan aktif di Mamikos<br>
                   2. Ada minimal 1 foto kost<br>
                   3. No handphone pemilik/pengelola<br>yang dicantumkan diatas<br>boleh dihubungi oleh Mamikos.<br>
                   4. Minimal 1 data valid untuk menukar point.
                   <button class="btn btn-mamiorange" data-dismiss="modal">OK</button>
                 </p>
               </div>
             </div>
           </div>
         </div>

    </div>
    <div class="footer navbar-fixed-bottom nav-bottom col-md-offset-3">
        <a data-toggle="modal" data-target="#termsModal">SYARAT & KETENTUAN</a>
    </div>
  </div>

      <!--  JavaScripts
      ================================================== -->
      <script src="{{ mix_url('dist/js/common.js') }}"></script>
      <script src="{{ mix_url('dist/js/user-scoreboard.js') }}"></script>

</body>
</html>