<meta charset="utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<meta name="csrf-token" content="{{ csrf_token() }}">

<meta name="application-name" content="Mamikos">
<meta name="theme-color" content="#1BAA56">
<meta name="mobile-web-app-capable" content="yes">

<meta name="apple-itunes-app" content="app-id=1055272843">
<meta name="google-play-app" content="app-id=com.git.mami.kos">

@include('web.@meta.favicon')

<meta name="msapplication-TileColor" content="#1baa56">
<meta name="msapplication-config" content="/browserconfig.xml">

<link rel="manifest" href="/json/manifest.json">

<meta name="google-site-verification" content="grtdHp44CrZpWDuquG54IUO-FyZeaflA6MHtQlh4ySg">
<meta name="p:domain_verify" content="1947d14ed84936368c9c2c229d535f39">
<meta name="msvalidate.01" content="8F441EFF8C6571840616F325C34D7698">
<meta name="rating" content="general">

<meta property="fb:app_id" content="607562576051242">
<meta property="og:type" content="website">
<meta property="og:site_name" content="Mamikos">
<meta property="og:locale" content="id_ID">

<meta name="twitter:card" content="summary">
<meta name="twitter:site" content="@mamikosapp">
<meta name="twitter:creator" content="@mamikosapp">

@include('web.@meta.link-prebrowsing')
