<link rel="icon" type="image/png" sizes="16x16" href="{{ asset_url('general/img/manifest/favicon-16x16.png') }}">
<link rel="icon" type="image/png" sizes="32x32" href="{{ asset_url('general/img/manifest/favicon-32x32.png') }}">
<link rel="shortcut icon" href="{{ asset_url('general/img/manifest/favicon.ico') }}">
<link rel="mask-icon" href="{{ asset_url('general/img/manifest/safari-pinned-tab.svg') }}" color="#1baa56">