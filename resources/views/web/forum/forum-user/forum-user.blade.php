@extends('web.forum.forum-user.forumUserLayout')

@section('meta-custom')
	<title>{{ $meta_title }}</title>

	<meta name="description" content="{{ $meta_description }}" />
	<meta name="og:site_name" content="Mamikos"/>
	<meta property="og:title" content="{{ $meta_title }}" />
	<meta property="og:description" content="{{ $meta_description }}" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="{{ Request::url() }}" />
	<meta property="og:image" content="/assets/BannerMamikosForum.png" />
	<meta property="fb:app_id" content="607562576051242"/>
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:site" content="@mamikosapp">
	<meta name="twitter:title" content="{{ $meta_title }}">
	<meta name="twitter:description" content="{{ $meta_description }}">
	<meta name="twitter:creator" content="@mamikosapp">
	<meta name="twitter:image" content="/assets/BannerMamikosForum.png">
	<meta name="twitter:domain" content="mamikos.com">

	<meta name="classification" content="Business, Rent House, Sewa Kost, Property, Rent Room, Info Kost, Information, Kost, Room, Cari Kost, Kost Murah, Kost Bebas, Application, Mobile Application, Kamar Kost, Kamar Kos, Kostan, Kos, Rumah Kost, Rumah Kos, Kost Harian">
	<meta name="distribution" content="global">
  <meta http-equiv="Reply-to" content="mamikos.app@gmail.com">
  <meta name="revisit-after" content="1 day">
  <meta name="keyphrases" content="Info Kost, Cari Kost, Sewa Kost, Kost Bebas, Kost Murah, Kost pasutri, Aplikasi Kost, Aplikasi Pencarian Kost, Aplikasi Info Kost, APlikasi Cari Kost, Kost, Mamikost, Mamikosapp, Kamar Kost, Kamar Kos, Kostan, Kos, Rumah Kost, Rumah Kos, Kost Harian">

  <link rel="preload" href="{{ mix_url('dist/js/forum-user.js') }}" as="script">
@endsection

@section('data')
	<script type="text/javascript">
		var forumUser = @JZON($forum_user);
	</script>
@endsection

@section('content')
  	<div id="app">
  		<app></app>
  	</div>

  	@include('web.part.modalloginuser-jquery')
@endsection

@section('script')
	@include('web.@script.global-auth')

	<script src="{{ mix_url('dist/js/forum-user.js') }}"></script>
	<script src="{{ mix_url('dist/js/login-user-jquery.js') }}"></script>
@endsection
