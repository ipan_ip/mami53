@extends('web.@layout.globalLayout')

@section('head')
	<title>Data penyewa - Yuk isi data kamu! </title>
	<meta name="description" content="Data penyewa - Yuk isi data kamu! ">
	<link rel="canonical" href="https://mamikos.com/ob/{{ $dbetCode }}">

	<meta property="og:url" content="https://mamikos.com/ob/{{ $dbetCode }}">
	<meta property="og:title" content="Data penyewa - Yuk isi data kamu! ">
	<meta property="og:image" content="/general/img/pictures/share-image-default.jpg">

	<meta name="twitter:url" content="https://mamikos.com/ob/{{ $dbetCode }}">
	<meta name="twitter:title" content="Data penyewa - Yuk isi data kamu! ">
	<meta name="twitter:image" content="/general/img/pictures/share-image-default.jpg">

	<link rel="preload" href="{{ mix_url('dist/js/_dbet-tenant/app.js') }}" as="script">

@endsection

@section('data')
	<script>
		var dbetCode = @JZON($dbetCode);
		var dbetData = @JZON($dbetData);
	</script>
@endsection


@section('styles')
	@include('web.@style.preloading-style')
@endsection


@section('content')
	@include('web.@style.preloading-content')

	<div id="app">
		<app></app>
	</div>
@endsection


@section('scripts')
	<script src="{{ mix_url('dist/js/_dbet-tenant/app.js') }}" async defer></script>
@endsection
