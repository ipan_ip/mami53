<style>
  ol.main-list {
    padding-left: 20px;
  }

  ol.main-list > li::marker,
  ol.main-list > li > .list-title {
    font-size: 16px;
    font-weight: bold;
    margin-top: 20px;
    margin-bottom: 10px;
  }
</style>

<p>Halo, terima kasih telah mengunjungi situs web (website) <a href="https://mamikos.com">mamikos.com</a> atau aplikasi Mamikos (selanjutnya disebut “<i>platform</i>”).</p>

<p><i>Platform</i> ini dimiliki dan dioperasikan oleh Mamikos dan afiliasinya (selanjutnya disebut, “Kami”). Kami sangat menghormati privasi atas Informasi Pribadi pendatang, pengguna, ataupun pengguna terdaftar (selanjutnya disebut “Anda”) dan ingin melindungi Informasi Pribadi tersebut sesuai dengan Kebijakan Privasi (selanjutnya disebut “Kebijakan”) ini.</p>

<p>Kebijakan ini berlaku pada setiap <i>platform</i> yang dioperasikan oleh Kami. Kebijakan ini menjelaskan bagaimana Kami mengumpulkan, menyimpan, menjaga, menggunakan dan (pada kondisi tertentu) mengungkapkan informasi pribadi yang teridentifikasi yang Anda berikan selama menggunakan <i>platform</i> Kami (selanjutnya disebut “Informasi Pribadi”) serta menjelaskan bagaimana langkah yang telah Kami ambil untuk mengamankan Informasi Pribadi Anda. Dengan mengunjungi, menggunakan dan/atau mendaftarkan diri Anda pada <i>platform</i> Kami, maka Anda dianggap telah membaca, mengerti, memahami dan menyetujui seluruh isi yang tertuang dalam kebijakan ini. Apabila Anda tidak setuju dengan sebagian atau seluruh isi Kebijakan ini, Anda dapat meninggalkan <i>platform</i> yang Kami kelola.</p>

<ol class="main-list">
  <li>
    <div class="list-title">
      Informasi Pribadi
    </div>

    <p>
      Informasi Pribadi adalah setiap informasi atau keterangan tentang individu yang benar dan nyata yang melekat dan dapat diidentifikasi secara wajar dari informasi atau keterangan tersebut baik langsung maupun tidak langsung, pada masing-masing individu
    </p>
  </li>
  <li>
    <div class="list-title">
      Informasi Pribadi yang Kami Kumpulkan, Simpan, Jaga dan Gunakan
    </div>

    <p>
      Kami mengumpulkan Informasi Pribadi yang Anda berikan kepada Kami saat menggunakan <i>platform</i> yang Kami kelola. Informasi Pribadi yang Kami kumpulkan diantaranya adalah nama, alamat tempat tinggal, nomor telepon, alamat email, nomor identitas diri Anda, identifikasi pengguna Mamikos termasuk kredensial log in, komentar atau umpan balik yang Anda berikan pada konten <i>platform</i> ini. Selain itu kami juga dapat mengumpulkan informasi non-pribadi yang dapat digunakan untuk mengidentifikasi Anda termasuk diantaranya adalah alamat protokol internet (<i>internet protocol</i>/IP) Anda, data lokasi geografis, jenis sistem pengoperasian, kebangsaan, preferensi pencarian Anda, dan data-data umum lainnya yang ada pada Internet. 
    </p>

    <p>Kami akan menggunakan Informasi Pribadi Anda yang kami kumpulkan melalui <i>platform</i> untuk tujuan-tujuan sebagai berikut: </p>
    
    <ol type="a">
      <li>melakukan pendaftaran, mengelola, mengurus penggunaan dan/atau akses pada <i>platform</i> Kami, </li>
      <li>mengelola, mengoperasikan, mengurus dan memberikan kepada Anda penawaran ataupun layanan yang ada pada <i>platform</i> Kami,</li>
      <li>menghubungi Anda  sehubungan dengan hal yang berkaitan dengan penggunaan dan akses terkait promosi maupun layanan <i>platform</i> Kami, serta memberikan konfirmasi atas pertanyaan maupun permintaan yang Anda ajukan kepada Kami (dan sebaliknya),</li>
      <li>melakukan penyesuaian fitur atau pengalaman Anda saat menggunakan <i>platform</i>,</li>
      <li>mempublikasikan ulasan, komentar maupun rating yang Anda berikan melalui <i>platform</i> dalam bentuk apapun termasuk digital dan cetak untuk dapat diakses oleh publik,</li>
      <li>melakukan pengukuran dan meningkatkan pengalaman Anda saat menggunakan <i>platform</i>,</li>
      <li>melaksanakan dan mengaplikasikan S&K maupun penyelesaian sengketa, pemecahan masalah,  aduan, atau mengumpulkan pembayaran, dan/atau</li>
      <li>tujuan-tujuan lain yang akan Kami beritahukan kepada Anda pada saat dilakukannya pengumpulan dan/atau penggunaan Informasi Pribadi. </li>
    </ol>

    <p>
      Selain untuk tujuan sebagaimana tersebut di atas, Kami juga dapat menggunakan Informasi Pribadi maupun informasi non-pribadi Anda untuk tujuan termasuk namun tidak terbatas pada pemasaran atas produk baru, penawaran khusus, newsletter, survei maupun informasi lain yang Kami kirimkan melalui media apapun dan dalam bentuk apapun (seperti: poster, infografis, teks) dan Kami tawarkan kepada Anda. sehubungan dengan hal ini Anda senantiasa dapat memilih untuk keluar, berhenti berlangganan atau menggunakan dari setiap pemasaran yang Kami kirimkan kepada Anda kapanpun dengan mengikuti setiap panduan maupun instruksi pemberhentian berlangganan yang diatur pada materi promosi tersebut.
    </p>

    <p>
      Kami juga dapat menggunakan Informasi Pribadi Anda sebagai pertimbangan Kami dalam melakukan riset pasar dimana Kami akan tetap memperhatikan dan menghormati kepercayaan yang Anda berikan kepada Kami terkait Informasi Pribadi, oleh karenanya Kami akan menyediakan dalam bentuk rincian anonim dan hanya akan digunakan untuk keperluan statistik. 
    </p>
  </li>

  <li>
    <div class="list-title">
      Pembagian Informasi Pribadi
    </div>

    <p>
      Kami hanya akan membagi Informasi Pribadi Anda kepada perusahaan-perusahaan yang terafiliasi dengan Kami, pihak berwenang seperti pemerintah, mitra bisnis terpercaya Kami, pemasok maupun agen Kami dari waktu ke waktu dan dimanapun.
      Anda memahami bahwa Kami dapat membagikan Informasi Pribadi Anda pada situasi-situasi tertentu seperti:

      <ol type="a">
        <li>melakukan pembelaan terhadap klaim, gugatan, tuntutan apapun,</li>
        <li>mematuhi peraturan perundang-undangan, proses dan/atau perintah pengadilan permintaan yang sah  oleh pejabat penegak hukum atau pihak yang berwenang,</li>
        <li>melakukan pemeriksaan atau penyelidikan atas penipuan atau kesalahan lainnya yang dipersyaratkan atau diperlukan dalam rangka proses mematuhi hukum yang berlaku, atau untuk melindungi kepentingan sah Kami,</li> 
        <li>kepada pembeli atau mitra Kami berkaitan dengan pelaksanaan penjualan, layanan, pengalihan, pekerjaan lain baik sebagian ataupun seluruhnya dari kegiatan bisnis perusahaan Kami,</li>
        <li>untuk mempertahankan, melindungi hak Kami, pengguna <i>platform</i> lainnya, atau pihak ketiga sesuai dengan kebijaksanaan Kami, dan</li>
        <li>kondisi-kondisi lain selama diperbolehkan oleh hukum.</li>
      </ol>
    </p>
  </li>

  <li>
    <div class="list-title">
      Persetujuan dan Penarikan Persetujuan
    </div>

    <p>
      Sebagaimana telah kami jelaskan pada bagian awal Kebijakan ini, dengan mengakses, menjelajah, menggunakan, mendaftarkan  diri Anda, menggunakan layanan atau produk yang Kami berikan pada <i>platform</i> atau dengan meng-klik tombol "<b>konfirmasi</b>" atau "<b>setuju</b>" atau pada saat Anda membuat akun baru pada situs, maka Anda menyetujui Kami dan perusahaan yang terafiliasi dengan Kami untuk mengumpulkan, menggunakan, mengungkapkan, mentransfer dan/atau mengolah Informasi Pribadi sebagaimana tertuang dalam Kebijakan ini.
    </p>

    <p>
      Sewaktu-waktu Anda dapat menarik persetujuan terhadap pengumpulan, penggunaan atau penyingkapan Kami atas Informasi Pribadi dengan memberikan menyampaikan pemberitahuan secara tertulis kepada Kami dan disertai dengan alasan Anda. Setelah konfirmasi atas penarikan persetujuan, Kami akan berhenti mengumpulkan, menggunakan atau mentransfer, menyingkap Informasi Pribadi. Namun demikian hal ini tidak berlaku dalam hal diwajibkan oleh hukum atau dalam hal Kami menjalankan kegiatan usaha yang sah atau berdasarkan tujuan hukum untuk mempertahankannya. Dalam hal anda menarik persetujuan Anda maka Kami mungkin tidak dapat untuk terus memberikan layanan Kami kepada Anda dan Anda setuju untuk membebaskan Kami dan tidak akan meminta pertanggung jawaban atas setiap kerugian atau kerusakan yang timbul dari atau terkait penghentian layanan tersebut.
    </p>
  </li>

  <li>
    <div class="list-title">
      Kebijakan Cookie dan Location 
    </div>

    <p>
      Cookie merupakan file teks kecil yang mengidentifikasi komputer, tablet, atau smartphone Anda ke server Kami dan disimpan oleh <i>browser</i> Internet Anda. Cookie memungkinkan Kami untuk mengenali alamat <i>Internet Protocol</i> Anda dan peningkatan layanan yang Kami berikan. Anda dapat mengakses informasi lebih lanjut terkait Cookie pada laman <a href="https://www.aboutcookies.org">www.aboutcookies.org</a> atau <a href="https://www.allaboutcookies.org">www.allaboutcookies.org</a>. Kami menginformasikan kepada Anda bahwa Cookie hanya Kami gunakan untuk kenyamanan Anda dalam menggunakan <i>platform</i> Kami.
    </p>

    <p>
      Selain cookie, Kami juga mengambil data lokasi Anda saat ini pada saat Anda pertama kali mengakses <i>platform</i> agar Kami dapat memberikan data kost yang terdekat dari lokasi Anda sekarang. Browser akan mengirimkan notifikasi untuk meminta Anda mengaktifkan lokasi tersebut sebelumnya.
    </p>
  </li>

  <li>
    <div class="list-title">
      Perlindungan Informasi Pribadi
    </div>

    <p>
      Kami senantiasa melindungi Informasi Pribadi di bawah kepemilikan atau kendali Kami dengan mempertahankan pengaturan keamanan yang wajar untuk mencegah akses, pengumpulan, penggunaan, penyingkapan, penyalinan, modifikasi, penghapusan yang tidak sah atau risiko yang sama salah satunya menggunakan SSL (<i>Secure Socket Layer</i>).
    </p>
  </li>

  <li>
    <div class="list-title">
      Akses dan Perbaikan Informasi Pribadi
    </div>

    <p>
      Anda harus memastikan kebenaran, akurasi, dan kelengkapan setiap data maupun Informasi Pribadi yang Anda berikan kepada Kami, dan dalam hal Anda mengetahui atau meyakini bahwa Informasi Pribadi yang Anda berikan terdapat kesalahan atau ketidaksesuaian Anda dapat segera memperbaikinya. Dalam hal data dan Informasi Pribadi yang akan Anda perbaiki tidak dapat diperbaiki melalui <i>platform</i>, Anda dapat segera mengajukan permintaan kepada Kami <a href="mailto:cs@mamikos.com">disini</a>. Dalam hal terdapat beban atas biaya yang mungkin timbul akibat perbaikan ini, Anda akan membayarkan biaya yang wajar untuk akses ke Informasi Pribadi dimana sebelumnya akan kami sampaikan untuk mendapatkan persetujuan Anda. Kami akan melakukan verifikasi terhadap identitas diri Anda sebelum memberikan akses atau melakukan perubahan pada Informasi Pribadi Anda. 
    </p>
  </li>

  <li>
    <div class="list-title">
      Keberlakukan 
    </div>

    <p>
      Kebijakan ini merupakan bagian dari <a href="https://help.mamikos.com/syarat-dan-ketentuan/?category=pemilik-dan-penghuni&subCategory=syarat-dan-ketentuan-umum&slug=syarat-dan-ketentuan-umum">S&K</a> oleh karenanya segala ketentuan yang ada pada <a href="https://help.mamikos.com/syarat-dan-ketentuan/?category=pemilik-dan-penghuni&subCategory=syarat-dan-ketentuan-umum&slug=syarat-dan-ketentuan-umum">S&K</a> berlaku dan menjadi satu kesatuan yang tidak terpisahkan dengan Kebijakan ini. 
    </p>
  </li>

  <li>
    <div class="list-title">
      Pembaruan dan Persetujuan Kebijakan Privasi Data 
    </div>

    <p>
      Kebijakan ini mungkin diubah dan/atau diperbaharui dari waktu ke waktu sebagai upaya Kami dalam menyesuaikan dengan perubahan hukum dan peraturan yang ada tanpa pemberitahuan sebelumnya. Kami menyarankan untuk membaca secara seksama dan memeriksa kembali laman Kebijakan ini dari waktu ke waktu untuk mengetahui perubahan apapun. Dengan tetap mengakses dan menggunakan <i>platform</i>, maka Anda dianggap menyetujui perubahan-perubahan dalam Kebijakan yang ada. Anda memahami bahwa versi terbaru dari Kebijakan ini akan menggantikan semua versi sebelumnya. 
    </p>
  </li>

  <li>
    <div class="list-title">
      Pihak Ketiga 
    </div>

    <p>
      <i>Platform</i> dan materi pemasaran kami dapat berisi tautan ke situs web yang dioperasikan oleh pihak ketiga. Kami tidak mengontrol atau menerima tanggung jawab atas kegiatan situs web pihak ketiga terkait pengumpulan, penggunaan, penyimpanan, dan pengungkapan Informasi Pribadi Anda oleh pihak ketiga tersebut. Silahkan membaca syarat dan ketentuan serta kebijakan privasi dari situs web pihak ketiga untuk mengetahui bagaimana mereka mengumpulkan dan menggunakan Informasi Pribadi Anda.
    </p>
  </li>

  <li>
    <div class="list-title">
      Hukum Yang Berlaku
    </div>

    <p>
      Kebijakan Privasi ini diatur dan ditafsirkan sesuai dengan hukum Negara Republik Indonesia.
    </p>
  </li>

  <li>
    <div class="list-title">
      Kontak Kami
    </div>
  
    <p>
      Kapanpun Anda dapat melakukan kontak dengan kami <a href="mailto:cs@mamikos.com">di sini</a>
    </p>
  </li>
</ol>
<br>
<br>

<p>
  <strong>Diperbaharui pada tanggal 2 Februari  2021</strong> <br />
  <strong>&copy; <a href="https://mamikos.com">Mamikos.com</a></strong>
</p>
