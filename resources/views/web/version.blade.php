<html lang="en">
    <head>
        <title>MamiKos - Version</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="Gilbok Lee">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.3.1/lumen/bootstrap.min.css" rel="stylesheet" integrity="sha384-iqcNtN3rj6Y1HX/R0a3zu3ngmbdwEa9qQGHdkXwSRoiE+Gj71p0UNDSm99LcXiXV" crossorigin="anonymous">
        <style>
            body { background-color:#1baa56; }
            .container { max-width:960px; }
            .hero-text { color:white; }
        </style>
    </head>
    <body>
        <div class="px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center hero-text">
            <h1 class="display-4">Version</h1>
            <p class="lead">Have a nice day.</p>
        </div>

        <div class="container">
            <div class="card-deck mb-3 text-center">
                <div class="card border-secondary mb-4">
                    <div class="card-header">MamiKos Web/API</div>
                    <div class="card-body">
                        <h4 class="card-title">{{ $data['app_version'] }}</h4>
                    </div>
                </div>

                @if (isset($data['framework_version']) == true) 
                <div class="card border-secondary mb-4">
                    <div class="card-header">Framework</div>
                    <div class="card-body">
                        <h4 class="card-title">{{ $data['framework_version'] }}</h4>
                    </div>
                </div>
                @endif
            </div>
            <div class="card-deck mb-3">
                <div class="card border-secondary mb-4">
                    <div class="card-header">Config</div>
                    <div class="card-body">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">Chat Daily Total Limit: {{ $configInfo['chat_daily_limit_total'] }}</li>
                            <li class="list-group-item">Chat Daily Limit / Kost: {{ $configInfo['chat_daily_limit_per_kost'] }}</li>
                        </ul>
                    </div>
                </div>

                <div class="card border-secondary mb-4">
                    <div class="card-header">Client Detection</div>
                    <div class="card-body">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">Is mobile?: {{ $agentInfo['is_mobile'] ? 'true' : 'false' }}</li>
                            <li class="list-group-item">Is tablet?: {{ $agentInfo['is_tablet'] ? 'true' : 'false'  }}</li>
                            <li class="list-group-item">OS: {{ $agentInfo['os'] }}</li>
                            <li class="list-group-item">Device: {{ $agentInfo['device'] }}</li>
                            <li class="list-group-item">Browser: {{ $agentInfo['browser'] }}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </body>
</html>