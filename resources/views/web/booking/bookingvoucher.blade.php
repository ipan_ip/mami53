@extends('web.booking.bookingLayout')

@section('meta-custom')
  <meta name="booking-code" content="{{$bookingCode}}">
@endsection

@section('title', 'Voucher Booking')

@section('navbar')
  @include('web.part.navbarbooking')
@endsection

@section('content')
  <ol class="breadcrumb breadcrumb-new col-xs-12">
    <li class="breadcrumb-list"><a href="/">Home</a></li>
    <li class="breadcrumb-list"><a href="/booking/history">Booking</a></li>
    <li class="breadcrumb-list active">{{$bookingCode}}</li>
  </ol>
  <div class="clearfix"></div>
  <section id="header">
    <div class="col-md-12 col-xs-12 booking-header-title">
    </div>
  </section>
  <div id="booking" class="container stretch-sm stretch-xs booking-voucher-container">
    <section id="breadcrumb" v-if="page.section == 'payment'" v-cloak>
      @include('web.booking.partial.bookingbreadcrumb')
    </section>
    <section id="summary" v-if="page.section == 'payment'" v-cloak>
      @include('web.booking.partial.bookingsummary')
    </section>
    <section id="body">
      @include('web.booking.partial.bookingpayment')
      @include('web.booking.partial.bookingreceipt')
    </section>
    @include('web.booking.partial.bookingloading')
  </div>
@endsection
