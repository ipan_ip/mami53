@extends('web.booking.bookingLayout')

@section('meta-custom')
  <meta name="this-id" content="{{$room['_id']}}">
  <meta name="room-title" content="{{$room['room_title']}}">
  <meta name="booking-type" content="{{$booking_type}}">
@endsection

@section('title', 'Booking Kost')

@section('navbar')
  @include('web.part.navbarbooking')
@endsection

@section('content')

  <div id="booking" class="container stretch-sm stretch-xs booking-container">
    <section id="header">
      <div class="col-md-12 col-xs-12 booking-header-title">
        BOOKING {{strtoupper($room['room_title'])}}
      </div>
    </section>
    <section id="breadcrumb">
      @include('web.booking.partial.bookingbreadcrumb')
    </section>
    <section id="summary" :class="page.section == 'select' || page.section == 'detail' ? 'hidden-sm hidden-xs' : ''">
      @include('web.booking.partial.bookingsummary')
    </section>
    <section id="body">
      @include('web.booking.partial.bookingselect')
      @include('web.booking.partial.bookingdetail')
      @include('web.booking.partial.bookingreview')
      <!-- @include('web.booking.partial.bookingpayment') -->
    </section>
    @include ('web.part.modalloading')
  </div>
@endsection
