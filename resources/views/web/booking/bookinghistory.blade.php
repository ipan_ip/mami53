@extends('web.booking.bookingLayout')

@section('title', 'Histori Booking')

@section('navbar')
  @include('web.part.navbaruser')
@endsection

@section('content')

  <ol class="breadcrumb breadcrumb-new col-xs-12">
    <li class="breadcrumb-list"><a href="/">Home</a></li>
    <li class="breadcrumb-list active">Booking</li>
  </ol>
  
  <div class="clearfix"></div>

  <div id="booking" class="container-fluid stretch booking-history-container">
    <section id="history" v-if="page.section == 'history'" v-cloak>
      <div class="col-lg-5 col-md-6 col-sm-8 col-xs-12" v-for="history in bookingHistory.data">
        <div class="col-md-12 col-xs-12 stretch booking-history-data">
          <div class="data-title">
            @{{history.room.name}}
          </div>
          <div class="col-md-3 col-xs-3 data-photo">
            <img :src="history.room.photo.small">
          </div>
          <div class="col-md-9 col-xs-9 stretch">
            <div class="col-md-12 col-xs-12 data-status">
              <label :class="history.status == 0 ? 'is-grey' : 'is-green'">No. Booking @{{history.booking_code}}</label>
              <label :class="history.status == 0 ? 'is-grey' : 'is-green'">@{{history.payment_status}}</label>
            </div>
            <div class="col-md-12 col-xs-12 data-info">
              <p>@{{history.room.type}}</p>
              <p>
                <span>Check in: </span>
                <span>@{{history.checkin_date_formatted}}</span>
              </p>
            </div>
            <div class="col-md-12 col-xs-12 data-detail">
              <a role="button" tabindex="0" :href="page.path + '/' + history.booking_code" class="btn btn-mamigreen">Lihat Detail Booking</a>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-12 col-xs-12 booking-history-null" v-if="bookingHistory.data.length == 0">
        <p>Anda belum memiliki data histori booking.</p>
        <img src="/assets/history_zero.png">
      </div>
    </section>
    @include('web.booking.partial.bookingloading')
  </div>
@endsection
