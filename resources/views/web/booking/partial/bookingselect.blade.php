<div id="select" class="col-md-8 col-xs-12 booking-select" v-show="page.section == 'select'" v-cloak>
  <div class="col-md-12 col-xs-12 stretch">
    <div class="col-md-12 col-xs-12 stretch-xs booking-select-content">
      <form id="checkAvailable" @submit.prevent="checkBooking">
        <div class="col-md-3 col-sm-4 col-xs-6 booking-select-check">
          <label for="checkStartDate">Mulai Tanggal</label>
          <input id="checkStartDate" class="form-control" required placeholder="YYYY-MM-DD" @blur="toggleEndDate">
        </div>
        @if ($booking_type == 'harian')
        <div class="col-md-3 col-sm-4 col-xs-6 booking-select-check">
          <label for="checkEndDate">Berakhir Tanggal</label>
          <input id="checkEndDate" class="form-control" required placeholder="YYYY-MM-DD" disabled>
          <p v-show="durationDays.int > 0" v-cloak>Total: @{{durationDays.str}} Hari</p>
        </div>
        @elseif ($booking_type == 'bulanan')
        <div class="col-md-3 col-sm-2 col-xs-6 booking-select-check">
          <label for="checkDuration">Lama Waktu</label>
          <div class="check-input">
            <input id="checkDuration" type="number" class="form-control" required min="1" max="12" placeholder="0" v-model="checkItems.duration">
            <span>&nbsp; Bulan</span>
          </div>
        </div>
        @endif
        <div class="col-md-3 col-sm-3 col-xs-6 booking-select-check">
          <label for="checkTotalRoom">Jumlah Kamar</label>
          <div class="check-input">
            <input id="checkTotalRoom" type="number" class="form-control" required min="1" max="100" placeholder="0" v-model="checkItems.totalRoom">
          </div>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-6 booking-select-check">
          <div class="check-submit" data-toggle="tooltip" data-placement="top" title="Cek ketersediaan kamar">
            <button type="submit" class="btn btn-mamiorange">Cek Kamar</button>
          </div>
        </div>
        <!-- <div class="col-md-12 col-xs-12 booking-select-info">
          Saran: mulai tanggal 23 Januari 2017 bisa dibooking maksimal untuk 3 bulan
        </div> -->
      </form>
    </div>
    <div class="col-sm-12 col-xs-12 stretch-xs booking-select-result">
      <div class="col-sm-12 col-xs-12 result-title" v-if="(rooms.available.length > 0 || rooms.lowQty.length > 0) && rooms.notes == ''" v-cloak>
        Pilih Tipe Kamar
      </div>
      <!-- Available Rooms -->
      <div :id="room.id" class="col-sm-12 col-xs-12 result-room" v-for="room in rooms.available">
        <div class="col-sm-12 col-xs-12 room-title">
          @{{room.name}}
        </div>
        <div class="col-sm-8 col-xs-7 stretch-r-xs">
          <div class="col-sm-4 col-xs-3 room-photo" style="background-image: url('{{$room['photo_url']['small']}}');">
          </div>
          <div class="col-sm-8 col-xs-9 stretch-r-xs">
            <div class="col-sm-12 col-xs-12 room-label" v-if="room.guaranteed">
              <p class="main"><img src="/assets/booking-kost/garansi_24.png"> 24 Jam Garansi</p>
            </div>
            <div class="col-sm-12 col-xs-12 room-label" v-if="room.prices.extra_guest !== null">
              <p class="main"><img src="/assets/booking-kost/sekamar_berdua.png"> Sekamar Berdua</p>
              <p class="extra">tambah <span>@{{room.prices.extra_guest.price_string}}</span>
              </p>
            </div>
            <div class="col-md-6 col-sm-4 col-xs-12 room-person as-title">
              <label for="selectPerson">Jumlah Orang: </label>
            </div>
            <div class="col-md-6 col-sm-8 col-xs-12 room-person">
              <select id="selectPerson" class="form-control" :disabled="room.type == 'harian' ? true : false" v-model="selectItems.totalGuest">
                <option v-if="room.type == 'bulanan'" v-for="n in room.max_guest*parseInt(checkItems.totalRoom)" >@{{n}}</option>
                <option v-if="room.type == 'harian'">1</option>
              </select>
            </div>
          </div>
        </div>
        <div class="col-sm-4 col-xs-5 stretch-xs">
          <p class="room-price is-before" v-if="room.prices.base.regular_price !== room.prices.base.price">
            <span class="hidden-xs">Rp </span>
            <span>@{{room.prices.base.regular_price_string.substring(4)}}</span>
            <span>/@{{room.price_unit}}</span>
          </p>
          <p class="room-price is-current">
            <span class="hidden-xs">Rp </span>
            <span>@{{room.prices.base.price_string.substring(4)}}</span>
            <span>/@{{room.price_unit}}</span>
          </p>
        </div>
        <div class="col-sm-12 col-xs-12 room-choose">
          <p class="text-danger" v-if="room.type == 'bulanan' && !passMinimumStay.monthly">
            <i class="fa fa-exclamation-circle" aria-hidden="true"></i>
            <span> Minimal @{{room.minimum_stay}} Bulan</span>
          </p>
          <p class="text-danger" v-if="room.type == 'harian' && !passMinimumStay.daily">
            <i class="fa fa-exclamation-circle" aria-hidden="true"></i>
            <span> Minimal @{{room.minimum_stay}} Hari</span>
          </p>
          <button type="button" class="btn" :class="passMinimumStay.monthly || passMinimumStay.daily ? 'btn-mamigreen': 'btn-mamigrey'" :disabled="passMinimumStay.monthly || passMinimumStay.daily ? false : true" @click="selectRoom(room)">Pesan Kamar Sekarang</button>
        </div>
      </div> <!-- End of Available Rooms -->
      <!-- Low Qty Rooms -->
      <div :id="room.id" class="col-sm-12 col-xs-12 result-room is-low-qty" v-for="room in rooms.lowQty">
        <div class="col-sm-12 col-xs-12 room-title">
          @{{room.name}} <span class="extra">*tinggal @{{room.available_room}} kamar</span>
        </div>
        <div class="col-sm-8 col-xs-7 stretch-r-xs">
          <div class="col-sm-4 col-xs-3 room-photo" style="background-image: url('{{$room['photo_url']['small']}}');">
          </div>
          <div class="col-sm-8 col-xs-9 stretch-r-xs">
            <div class="col-sm-12 col-xs-12 room-label" v-if="room.guaranteed">
              <p class="main"><img src="/assets/booking-kost/garansi_24.png"> 24 Jam Garansi</p>
            </div>
            <div class="col-sm-12 col-xs-12 room-label" v-if="room.prices.extra_guest !== null">
              <p class="main"><img src="/assets/booking-kost/sekamar_berdua.png"> Sekamar Berdua</p>
              <p class="extra">tambah <span>@{{room.prices.extra_guest.price_string}}</span>
              </p>
            </div>
            <div class="col-md-6 col-sm-4 col-xs-12 room-person as-title">
              <label for="selectPerson">Jumlah Orang: </label>
            </div>
            <div class="col-md-6 col-sm-8 col-xs-12 room-person">
              <select id="selectPerson" class="form-control" disabled>
                <option v-if="room.type == 'bulanan'" v-for="n in room.max_guest*parseInt(checkItems.totalRoom)" >@{{n}}</option>
                <option v-if="room.type == 'harian'">1</option>
              </select>
            </div>
          </div>
        </div>
        <div class="col-sm-4 col-xs-5 stretch-xs">
          <p class="room-price is-before" v-if="room.prices.base.regular_price !== room.prices.base.price">
            <span class="hidden-xs">Rp </span>
            <span>@{{room.prices.base.price_string.substring(4)}}</span>
            <span>/@{{room.price_unit}}</span>
          </p>
          <p class="room-price is-current">
            <span class="hidden-xs">Rp </span>
            <span>@{{room.prices.base.price_string.substring(4)}}</span>
            <span>/@{{room.price_unit}}</span>
          </p>
        </div>
        <div class="col-sm-12 col-xs-12 room-choose">
          <button type="button" class="btn btn-mamigrey" disabled>Pesan Kamar Sekarang</button>
        </div>
      </div> <!-- End of Low Qty Rooms -->
      <div class="result-room is-null" v-if="rooms.notes !== ''" v-cloak>
        <p>@{{rooms.notes}}</p>
        <img src="/assets/history_zero.png">
      </div>
    </div>
  </div>
</div>
