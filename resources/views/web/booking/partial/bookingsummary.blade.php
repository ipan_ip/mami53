<div class="col-md-4 col-xs-12 pull-left stretch-lg stretch-md">
  <div class="col-md-12 col-xs-12 booking-summary">
    <div class="booking-summary-title">
      <span v-if="page.section !== 'payment'" v-cloak>Ringkasan Booking</span>
      <span v-else v-cloak>No. Booking @{{finalBooking.data.booking_data.booking_code}}</span>
    </div>
    <div class="container-fluid booking-summary-content">
      <p class="col-md-12 col-xs-12 stretch text-capitalize">
        <span class="col-md-5 col-xs-4">Tipe Sewa</span>
        <span class="col-md-7 col-xs-8">: @{{bookingType}}</span>
      </p>
      <div v-if="page.section == 'detail' || page.section == 'review' || page.section == 'payment'" v-cloak>
        <p class="col-md-12 col-xs-12 stretch text-capitalize">
          <span class="col-md-5 col-xs-4">Tipe Kamar</span>
          <span class="col-md-7 col-xs-8" v-if="page.section !== 'payment'">: @{{selectItems.room.name}}</span>
          <span class="col-md-7 col-xs-8" v-else>: @{{selectItems.room.type_name}}</span>
        </p>
        <p class="col-md-12 col-xs-12 stretch">
          <span class="col-md-5 col-xs-4">Check In</span>
          <span class="col-md-7 col-xs-8">: @{{checkItems.startDate}}</span>
        </p>
        <p class="col-md-12 col-xs-12 stretch">
          <span class="col-md-5 col-xs-4">Check Out</span>
          <span class="col-md-7 col-xs-8">: @{{checkItems.endDate}}</span>
        </p>
        <p class="col-md-12 col-xs-12 stretch">
          <span class="col-md-5 col-xs-4">Jml Kamar</span>
          <span class="col-md-7 col-xs-8">: @{{checkItems.totalRoom}} Kamar</span>
        </p>
        <p class="col-md-12 col-xs-12 stretch">
          <span class="col-md-5 col-xs-4">@{{selectItems.price.base.price_label}}</span>
          <span class="col-md-7 col-xs-8">:
            <s v-if="selectItems.price.base.price_unit_regular !== selectItems.price.base.price_unit"> @{{selectItems.price.base.price_unit_regular_string}}</s>
            <span> @{{selectItems.price.base.price_unit_string}}</span>
          </span>
        </p>
        <p class="col-md-12 col-xs-12 stretch" v-if="selectItems.price.extra_guest !== undefined">
          <span class="col-md-5 col-xs-4">@{{selectItems.price.extra_guest.price_label}}</span>
          <span class="col-md-7 col-xs-8">: @{{selectItems.price.extra_guest.price_total_string}}</span>
        </p>
        <p class="col-md-12 col-xs-12 stretch">
          <span class="col-md-5 col-xs-4">Lama Waktu</span>
          <span class="col-md-7 col-xs-8">
            <span>: @{{checkItems.duration}} </span>
            <span v-if="bookingType == 'bulanan'">Bulan</span>
            <span v-if="bookingType == 'harian'">Hari</span>
          </span>
        </p>
        <p class="col-md-12 col-xs-12 stretch">
          <span class="col-md-5 col-xs-4">@{{selectItems.price.booking_fee.price_label}}</span>
          <span class="col-md-7 col-xs-8">: @{{selectItems.price.booking_fee.price_total_string}}</span>
        </p>
        <p class="col-md-12 col-xs-12 stretch">
          <span class="col-md-5 col-xs-4">@{{selectItems.price.total.price_label}}</span>
          <span class="col-md-7 col-xs-8">: @{{selectItems.price.total.price_total_string}}</span>
        </p>
      </div>
    </div>
    <div class="col-md-12 col-xs-12 stretch" v-if="page.section == 'review' || page.section == 'payment'" v-cloak>
      <div class="booking-summary-title">
        Data Pemesan
      </div>
      <div class="container-fluid booking-summary-content" :class="guestsData.length > 1 && index > 0 ? 'has-border-top' : ''" v-for="(guest, index) in guestsData">
        <p class="col-md-12 col-xs-12 stretch">
          <span class="col-md-5 col-xs-4">Nama Tamu</span>
          <span class="col-md-7 col-xs-8">: @{{guest.name}}</span>
        </p>
        <p class="col-md-12 col-xs-12 stretch">
          <span class="col-md-5 col-xs-4">Tgl Lahir</span>
          <span class="col-md-7 col-xs-8" v-if="page.section !== 'payment'">: @{{guest.birthday}}</span>
          <span class="col-md-7 col-xs-8" v-else>: @{{guest.birthday_formatted}}</span>
        </p>
        <p class="col-md-12 col-xs-12 stretch">
          <span class="col-md-5 col-xs-4">Jml Kamar</span>
          <span class="col-md-7 col-xs-8">: @{{checkItems.totalRoom}} Kamar</span>
        </p>
        <p class="col-md-12 col-xs-12 stretch">
          <span class="col-md-5 col-xs-4">Email</span>
          <span class="col-md-7 col-xs-8">: @{{guest.email}}</span>
        </p>
        <p class="col-md-12 col-xs-12 stretch">
          <span class="col-md-5 col-xs-4">No. Handphone</span>
          <span class="col-md-7 col-xs-8">: @{{guest.phone_number}}</span>
        </p>
      </div>
    </div>
  </div>
</div>
