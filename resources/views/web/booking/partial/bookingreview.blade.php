<div id="review" class="col-md-8 col-xs-12 booking-review" v-show="page.section == 'review'" v-cloak>
  <div class="col-md-12 col-xs-12 stretch">
    <div class="col-md-12 col-xs-12 stretch-xs booking-review-content">
      <div class="col-md-7 hidden-sm hidden-xs booking-review-action">
        <p>Silakan cek ringkasan booking di samping, jika sudah cocok silakan lanjut ke pembayaran.</p>
        <button type="button" class="btn btn-mamiorange" @click="submitRoomBooking">Lanjut ke Pembayaran</button>
        <p>Atau</p>
        <button type="button" class="btn btn-mamiorange-inverse" @click="rewriteGuest">Ubah Data Pemesan</button>
      </div>
      <div class="col-xs-12 stretch hidden-lg hidden-md booking-review-action">
        <button type="button" class="btn btn-mamiorange" @click="submitRoomBooking">Selanjutnya</button>
        <button type="button" class="btn btn-mamiorange-inverse" @click="rewriteGuest">Ubah Data Pemesan</button>
      </div>
    </div>
  </div>
</div>
