<div id="payment" class="col-md-8 col-xs-12 booking-payment" v-if="page.section == 'payment'" v-cloak>
  <div class="col-md-12 col-xs-12 stretch">
    <div class="col-md-12 col-xs-12 stretch-xs booking-payment-content">
      <div class="col-md-12 col-xs-12 booking-payment-countdown">
        <label class="col-md-4 col-xs-12 main" v-if="finalBooking.data.booking_data.status == 'booked' || finalBooking.data.booking_data.status == 'confirmed'">@{{countdownTime.hours}} : @{{countdownTime.minutes}} : @{{countdownTime.seconds}}</label>
        <label class="col-md-4 col-xs-12 main" v-else>Status Booking : @{{finalBooking.data.payment.status}}</label>
        <label class="col-md-8 col-xs-12 extra">
          <span v-if="finalBooking.data.booking_data.status == 'booked'"> Menunggu Konfirmasi Admin</span>
          <span v-else-if="finalBooking.data.booking_data.status == 'confirmed'">Batas Pembayaran Pukul @{{finalBooking.data.booking_data.expired_date}}</span>
        </label>
      </div>
      <div class="col-md-12 col-xs-12 booking-payment-info">
        <p class="text-bold" v-if="finalBooking.data.booking_data.status == 'booked' || finalBooking.data.booking_data.status == 'confirmed'">
          <span>Harga Total</span>
          <span class="text-mamigreen">@{{finalBooking.data.booking_data.prices.total.price_total_string}}</span>
        </p>
        <p v-if="finalBooking.data.booking_data.status == 'confirmed'" v-cloak>silakan transfer ke salah satu rekening di bawah ini : </p>
      </div>
      <div class="col-md-12 col-xs-12" v-if="finalBooking.data.booking_data.status == 'confirmed'" v-cloak>
        <div class="col-md-7 col-xs-12 booking-payment-account" v-for="bank in finalBooking.data.bank_accounts">
          <p>@{{bank.name}}</p>
          <p class="text-bold">@{{bank.account_name}}</p>
          <p class="text-bold">@{{bank.number}}</p>
        </div>
      </div>
      <div class="col-md-12 col-xs-12 booking-payment-confirm" v-if="finalBooking.data.booking_data.status == 'confirmed'" v-cloak>
        <div class="confirm-alert">
          Setelah melakukan pembayaran silakan isi data di bawah ini untuk konfirmasi pembayaran
        </div>
        <form id="paymentConfirm" @submit.prevent="submitPaymentConfirm">
          <div class="form-group">
            <label for="paymentSource">Nama Bank Pengirim</label>
            <input id="paymentSource" class="form-control" required minlength="3" maxlength="10" placeholder="Contoh: Bank BNI" v-model="paymentConfirm.source">
          </div>
          <div class="form-group">
            <label for="paymentName">Nama Pemilik Rekening</label>
            <input id="paymentName" class="form-control" required minlength="2" maxlength="30" placeholder="Contoh: Ahmad Sururi" v-model="paymentConfirm.name">
          </div>
          <div class="form-group">
            <label for="paymentNumber">Nomor Rekening</label>
            <input id="paymentNumber" class="form-control" required minlength="7" maxlength="15" placeholder="Contoh: 0373231859" v-model="paymentConfirm.number">
          </div>
          <div class="form-group">
            <label for="paymentDestination">Nama Bank Tujuan</label>
            <select id="paymentDestination" class="form-control" v-model="paymentConfirm.destination">
              <option v-for="bank in finalBooking.data.bank_accounts" :value="bank.id">@{{bank.name}}</option>
            </select>
          </div>
          <div class="form-group">
            <label for="paymentTotal">Jumlah yang ditransfer</label>
            <input id="paymentTotal" type="number" min="10000" max="999999999" class="form-control" required placeholder="Contoh: 750000" v-model="paymentConfirm.total">
          </div>
          <div class="form-group">
            <label for="paymentDate">Tanggal Transfer</label>
            <input id="paymentDate" class="form-control" required placeholder="Contoh: 2017-08-17">
          </div>
          <button type="submit" class="btn btn-mamigreen">KONFIRMASI PEMBAYARAN</button>
        </form>
      </div>
    </div>
  </div>
</div>
