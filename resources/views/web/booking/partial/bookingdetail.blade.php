<div id="detail" class="col-md-8 col-xs-12 booking-detail" v-show="page.section == 'detail'" v-cloak>
  <div class="col-md-12 col-xs-12 stretch">
    <div class="col-md-12 col-xs-12 stretch-xs booking-detail-content">
      <form id="personDetail" @submit.prevent="submitPerson">
        <!-- Data Kontak -->
        <div class="col-md-12 col-xs-12 stretch">
          <div class="col-sm-12 col-xs-12 booking-detail-person-title">
            Data Kontak
          </div>
          <div class="col-sm-6 col-xs-12 booking-detail-person">
            <label for="contactName">Nama Pemesan (Sesuai KTP / SIM) <span class="text-danger">*</span></label>
            <input id="contactName" class="form-control" name="name" required minlength="2" maxlength="30" placeholder="Masukkan nama Anda di sini" v-model="contactData.name">
          </div>
          <div class="col-sm-6 col-xs-12 booking-detail-person">
            <label for="contactEmail">Email <span class="text-danger">*</span></label>
            <input id="contactEmail" type="email" name="email" class="form-control" required pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" placeholder="Masukkan email Anda di sini" v-model="contactData.email">
          </div>
          <div class="col-sm-6 col-xs-12 booking-detail-person">
            <label for="contactPhone">No. Handphone <span class="text-danger">*</span></label>
            <input id="contactPhone" type="tel" name="phone_number" class="form-control" required pattern="^0(\d{3,4}-?){2}\d{3,4}$" placeholder="Masukkan no. handphone Anda di sini" v-model="contactData.phone_number">
          </div>
        </div> <!-- End of Data Kontak -->
        <!-- Data Tamu -->
        <div class="col-md-12 col-xs-12 stretch" v-for="(guest, index) in guestsData">
          <div class="col-sm-12 col-xs-12 booking-detail-person-title">
            Data Tamu<span v-if="guestsData.length > 1"> @{{index+1}}</span>
          </div>
          <div class="col-sm-12 col-xs-12 booking-detail-person" v-if="index == 0">
            <label class="d-inline check-similarity">Data tamu<span v-if="guestsData.length > 1"> @{{index+1}}</span> sama dengan data kontak? </label>
            <div class="checkbox checkbox-success d-inline">
                <input type="checkbox" id="similarPerson" :disabled="(contactData.name.trim()) !== '' && (contactData.email.trim()) !== '' && (contactData.phone_number.trim()) !== '' ? false : true" v-model="similarPerson.bool">
                <label for="similarPerson">
                    Ya
                </label>
            </div>
          </div>
          <div class="col-sm-6 col-xs-12 booking-detail-person">
            <label :for="'guestName' + index">Nama Pemesan (Sesuai KTP / SIM) <span class="text-danger">*</span></label>
            <input :id="'guestName' + index" class="form-control" name="name" required placeholder="Masukkan nama Anda di sini" v-model="guest.name">
          </div>
          <div class="col-sm-6 col-xs-12 booking-detail-person">
            <label :for="'guestEmail' + index">Email <span class="text-danger">*</span></label>
            <input :id="'guestEmail' + index" type="email" name="email" class="form-control" required pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" placeholder="Masukkan email Anda di sini" v-model="guest.email">
          </div>
          <div class="col-sm-6 col-xs-12 booking-detail-person">
            <label :for="'guestPhone' + index">No. Handphone <span class="text-danger">*</span></label>
            <input :id="'guestPhone' + index" type="tel" name="phone_number" class="form-control" required pattern="^0(\d{3,4}-?){2}\d{3,4}$" placeholder="Masukkan no. handphone Anda di sini" v-model="guest.phone_number">
          </div>
          <div class="col-sm-6 col-xs-12 booking-detail-person">
            <label :for="'guestBirthday' + index">Tanggal Lahir <span class="text-danger">*</span></label>
            <input :id="'guestBirthday' + index" class="form-control guest-birthday" name="birthday" required placeholder="Masukkan tanggal lahir Anda di sini" v-model="guest.birthday" :data-key="index">
          </div>
        </div> <!-- End of Data Tamu -->
        <div class="col-sm-8 col-sm-offset-4 col-xs-12 booking-detail-person">
          <button type="submit" class="btn btn-mamiorange" title="Submit data diri pemesan">Selanjutnya</button>
        </div>
        <div class="col-sm-8 col-sm-offset-4 col-xs-12 booking-detail-person">
          <button type="button" class="btn btn-mamiorange-inverse" title="Ubah pilihan kamar" @click="reselectRoom">Ulangi pilih kamar</button>
        </div>
      </form>
    </div>
  </div>
</div>
