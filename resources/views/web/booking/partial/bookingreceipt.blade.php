<div id="receipt" class="col-md-12 col-xs-12 booking-receipt" v-if="page.section == 'receipt'" v-cloak>
  <div class="col-md-12 col-xs-12 stretch booking-receipt-content">
    <div class="col-md-6 col-xs-12 stretch">
      <div class="col-md-12 col-xs-12 receipt-title">
        <img src="/assets/mamikos_booking.png">
        <label>No. Pemesanan @{{finalBooking.data.booking_data.booking_code}}</label>
      </div>
      <div class="col-md-12 col-xs-12 receipt-header">
        <h3>VOUCHER BOOKING</h3>
      </div>
      <div class="col-md-12 col-xs-12 receipt-room">
        <div class="room-name">
          <p class="receipt-data-subheader is-bigger">Nama Kost</p>
          <p class="receipt-data-object is-bigger">@{{selectItems.room.name}}</p>
        </div>    
        <p class="receipt-data-object is-smaller">@{{selectItems.room.address}}</p>
        <p class="receipt-data-object is-smaller is-green">@{{selectItems.room.phone}}</p>
      </div>
      <div class="col-md-12 col-xs-12 stretch receipt-data has-separator">
        <div class="col-md-4 col-xs-4">
          <p class="receipt-data-subheader is-bigger">Check In</p>
          <p class="receipt-data-object">@{{checkItems.startDate}}</p>
        </div>
        <div class="col-md-4 col-xs-4">
          <p class="receipt-data-subheader is-bigger">Check Out</p>
          <p class="receipt-data-object">@{{checkItems.endDate}}</p>
        </div>
      </div>
      <div class="col-md-12 col-xs-12 stretch receipt-data has-separator">
        <div class="col-md-12 col-xs-12 as-header">
          <p>Detail Tamu</p>
        </div>
        <div class="guest-container" v-for="(guest, index) in guestsData">
          <div class="col-md-1 col-xs-1 guest-index">
            @{{index + 1}}.
          </div>
          <div class="col-md-6 col-xs-6 stretch">
            <p class="receipt-data-subheader">Nama Tamu</p>
            <p class="receipt-data-object">@{{guest.name}}</p>
          </div>
          <div class="col-md-5 col-xs-5 stretch">
            <p class="receipt-data-subheader">Tanggal Lahir</p>
            <p class="receipt-data-object">@{{guest.birthday_formatted}}</p>
          </div>
          <div class="col-md-6 col-xs-6 col-md-offset-1 col-xs-offset-1 stretch">
            <p class="receipt-data-subheader">Email</p>
            <p class="receipt-data-object">@{{guest.email}}</p>
          </div>
          <div class="col-md-5 col-xs-5 stretch">
            <p class="receipt-data-subheader">No. Handphone</p>
            <p class="receipt-data-object">@{{guest.phone_number}}</p>
          </div>
        </div>
      </div>
      <div class="col-md-12 col-xs-12 stretch receipt-detail receipt-data has-separator">
        <div class="col-md-12 col-xs-12 as-header">
          <p>Detail Pemesanan</p>
        </div>
        <div class="col-md-12 col-xs-12">
          <p class="receipt-data-subheader">Tipe Kamar</p>
          <p class="receipt-data-object">
            <span>@{{selectItems.room.type_name}}</span>
            <span v-if="finalBooking.data.booking_data.guaranteed"><img src="/assets/booking-kost/garansi_24.png"> 24 Jam Garansi</span>
            <span v-if="selectItems.price.extra_guest !== undefined"><img src="/assets/booking-kost/sekamar_berdua.png"> Sekamar Berdua</span>
          </p>
        </div>
        <div class="col-md-4 col-xs-6">
          <p class="receipt-data-subheader">Jumlah Kamar</p>
          <p class="receipt-data-object">@{{checkItems.totalRoom}}</p>
        </div>
        <div class="col-md-4 col-xs-6">
          <p class="receipt-data-subheader">@{{selectItems.price.base.price_label}}</p>
          <p class="receipt-data-object">
            <s v-if="selectItems.price.base.price_unit_regular !== selectItems.price.base.price_unit">@{{selectItems.price.base.price_unit_regular_string}}</s>
            <span>@{{selectItems.price.base.price_unit_string}}</span>
          </p>
        </div>
        <div class="col-md-4 col-xs-6">
          <p class="receipt-data-subheader">Lama Waktu</p>
          <p class="receipt-data-object">
            <span>@{{checkItems.duration}}</span>
            <span v-if="bookingType == 'bulanan'">Bulan</span>
            <span v-if="bookingType == 'harian'">Hari</span>
          </p>
        </div>
        <div class="col-md-4 col-xs-6">
          <p class="receipt-data-subheader">@{{selectItems.price.booking_fee.price_label}}</p>
          <p class="receipt-data-object">@{{selectItems.price.booking_fee.price_total_string}}</p>
        </div>
        <div class="col-md-4 col-xs-6">
          <p class="receipt-data-subheader">@{{selectItems.price.total.price_label}}</p>
          <p class="receipt-data-object">@{{selectItems.price.total.price_total_string}}</p>
        </div>
      </div>
      <div class="receipt has-separator">
        <b>CS Mamikos</b>
        <p>
          <span>Whatsapp : <a href="intent://send/+6287734003208#Intent;scheme=smsto;package=com.whatsapp;action=android.intent.action.SENDTO;end">0877-3400-3208</a></span>
          <span> | Email : <a href="mailto:saran@mamikos.com">booking@mamikos.com</a></span>
        </p>
      </div>
    </div>
    <div class="col-md-6 hidden-xs hidden-sm stretch">
      <div class="col-md-12 receipt-action" v-if="finalBooking.data.booking_data.allow_cancel">
        <button class="btn btn-mamigreen" @click="initBookingCancel">Batalkan Booking</button>
      </div>
      <div class="col-md-12 receipt-action" v-if="finalBooking.data.booking_data.is_guaranteed">
        <button class="btn btn-mamigreen" @click="initClaimGuarantee">Klaim Garansi</button>
      </div>
    </div>
  </div>
</div>
<div class="col-sm-11 col-xs-12 stretch hidden-lg hidden-md receipt-action" v-if="page.section == 'receipt'" v-cloak>
  <button class="btn btn-mamigreen" @click="initBookingCancel" v-if="finalBooking.data.booking_data.allow_cancel">Batalkan Booking</button>
  <button class="btn btn-mamigreen" @click="initClaimGuarantee" v-if="finalBooking.data.booking_data.is_guaranteed">Klaim Garansi</button>
</div>
<!-- Modal Cancel Booking -->
<div id="cancelBookingModal" class="modal fade cancel-claim-modal" role="dialog">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <form @submit.prevent="submitBookingCancel">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <p class="modal-title"><b>Silakan masukkan alasan pembatalan Anda di sini :</b></p>
        </div>
        <div class="modal-body">
          <textarea class="form-control" placeholder="Tuliskan alasan Anda di sini..." required minlength="10" maxlength="200" v-model="cancelReason.content"></textarea>
        </div>
        <div class="modal-footer flex-center">
          <button type="submit" class="btn btn-mamigreen">Batalkan Booking</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- Modal Claim Guarantee -->
<div id="claimGuaranteeModal" class="modal fade cancel-claim-modal" role="dialog">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <form @submit.prevent="submitClaimGuarantee">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <p class="modal-title"><b>Silakan masukkan alasan pembatalan Anda di sini :</b></p>
        </div>
        <div class="modal-body">
          <textarea class="form-control" placeholder="Tuliskan alasan Anda di sini..." required minlength="10" maxlength="200" v-model="cancelReason.content"></textarea>
        </div>
        <div class="modal-footer flex-center">
          <button type="submit" class="btn btn-mamigreen">Batalkan Booking</button>
        </div>
      </form>
    </div>
  </div>
</div>
