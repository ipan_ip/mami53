<div class="col-md-8 col-xs-12 pull-right booking-breadcrumb">
    <div class="col-md-12 col-xs-12 booking-breadcrumb-content">
      	<span class="col-md-3 col-xs-3 most-left" :class="page.section == 'select' ? 'is-selected' : ''">1. Pilih</span>
      	<span class="col-md-3 col-xs-3" :class="page.section == 'detail' ? 'is-selected' : ''">2. Detail</span>
      	<span class="col-md-3 col-xs-3" :class="page.section == 'review' ? 'is-selected' : ''">3. Review</span>
      	<span class="col-md-3 col-xs-3 most-right" :class="page.section == 'payment' ? 'is-selected' : ''">4. Bayar</span>
    </div>
 </div>
 