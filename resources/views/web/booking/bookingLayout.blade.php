<!DOCTYPE html>
<html>
<head>
  @if($_SERVER['HTTP_HOST'] != 'mamikos.com')
  <meta name="robots" content="noindex, nofollow">
  @endif
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <meta name="application-name" content="Mamikos">
  <meta name="theme-color" content="#1BAA56">
  <link rel="manifest" href="/json/manifest.json">

  @include('web.@meta.link-prebrowsing')

  @yield('meta-custom')
  <base href="/">
  <title>@yield('title') - Mamikos</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  
  @include('web.@meta.favicon')
  
  <meta name="description" content="Booking Kost - Mamikos" />
  <meta property="fb:app_id" content="607562576051242" />
  <meta name="keywords" content="@yield('title') - Mamikos ">
  <meta name="robots" content="index, follow">
  <meta name="og:site_name" content="Mamikos" />
  <meta property="og:description" content="@yield('title') - Mamikos" />
  <meta property="og:type" content="website" />
  <meta property="og:url" content="https://mamikos.com/kost" />
  <meta property="og:image" content="https://mamikos.com/assets/share-image-default.jpg" />
  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:site" content="@mamikosapp">
  <meta name="twitter:title" content="Booking Kost Murah, Bebas, Pasutri Semaumu - Mamikos">
  <meta name="twitter:description" content="@yield('title') - Mamikos">
  <meta name="twitter:creator" content="@mamikosapp">
  <meta name="twitter:image" content="https://mamikos.com/assets/share-image-default.jpg">
  <meta name="twitter:domain" content="mamikos.com">

  @include('web.part.external.global-head')
  <link href="{{ mix_url('dist/css/vendor.css') }}" rel="stylesheet">
  <link href="{{ mix_url('dist/css/appmamikos.css') }}" rel="stylesheet">
  <link href="{{ asset('css/booking.css') }}" rel="stylesheet">

</head>
<body>
  @include('web.part.external.gtm-body')

  <!-- Navbar -->
  @yield('navbar')

  @yield('content')



  <script src="{{ mix_url('dist/js/vendor.js') }}"></script>
  @include ('web.part.script.bugsnagapikey')
  @include ('web.part.script.tokenerrorhandlescript')

  @include ('web.part.script.navbarscript')

  <script src="{{ mix_url('dist/js/pages/booking/bookingCtrl.js') }}"></script>
  @yield('script')

  @include ('web.part.script.browseralertscript')
</body>
</html>
