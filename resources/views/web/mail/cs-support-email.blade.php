<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Document</title>
</head>
<body>
	<p>
		Nama : {{ $emailData['name'] }}
	</p>

	<p>
		Email: {{ $emailData['email'] }}
	</p>

	<p style="margin-bottom: 20px;">
		Nomor HP : {{ $emailData['phone_number'] }}
	</p>

	<p>
		Kategori Permasalahan : {{ $emailData['category'] }}
	</p>

	<p>
		Nama Kos : {{ $emailData['kost_name'] }}
	</p>

	<p style="margin-bottom: 20px;">
		Lokasi Kos : {{ $emailData['kost_location'] }}
	</p>

	<div>
		<p>
			Detail Permasalahan :
		</p>
		<p>
			{{ $emailData['detail_problem'] }}
		</p>
	</div>
</body>
</html>