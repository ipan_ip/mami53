@extends('web.layout.landingOldLayout')
@section('meta')
  <base href="/">
  <title>Berhenti Berlangganan Email Rekomendasi - Mamikos</title>
  <link rel="canonical" href="https://mamikos.com/unsubscribe" />
  <meta name="robots" content="noindex, nofollow">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <meta property="fb:app_id" content="607562576051242"/>

  @include('web.@meta.favicon')

@endsection

@section('style')
<style>
  body {
    font-family: 'Lato', sans-serif;
    font-size: 14px;
    line-height: 1.42857143;
    color: #333;
    background-color: #fff;
  }

  .navbar-unsubscribe {
    background: #1BAA56;
    padding: 5px;
    text-align: center;
  }

  .navbar-unsubscribe .header-logo {
    height: 40px;
  }

  .unsubscribe-content h1 {
    color: #1BAA56;
    font-size: 24px;
    font-weight: bold;
    margin-bottom: 15px;
    padding-bottom: 15px;
    position: relative;
  }

  .unsubscribe-content h1:before {
    background: #1BAA56;
    bottom: 0;
    content: "";
    display: block;
    height: 2px;
    left: 0;
    position: absolute;
    width: 100px;
  }

  .unsubscribe-content p.content-big {
    font-size: 18px;
    line-height: 24px;
  }

  .unsubscribe-content .btn-unsubscribe {
    color: #1BAA56;
    border: 2px solid #1BAA56;
    font-size: 16px;
  }

  .unsubscribe-content .btn-unsubscribe:hover {
    background: #1BAA56;
    color: #fff;
  }

  .unsubscribe-separator {
    background: #eee;
    height: 3px;
    margin: 20px 0;
  }

  /*Include Footer*/
  .page-footer {
    padding-top: 10px;
    padding-bottom: 10px;
  }
  .logo-footer {
    display: flex;
    align-items: center;
    justify-content: center;
  }
  .logo-footer .img-logo-footer {
    width: 240px;
  }
  .text-footer {
    color: #000;
    text-align: center;
  }
  .link-footer,
  .social-footer,
  .contact-footer {
    display: flex;
    align-items: center;
    justify-content: center;
    flex-wrap: wrap;
    padding-top: 10px;
    padding-bottom: 10px;
  }
  .link-footer > a:hover {
    cursor: pointer;
    text-decoration: underline;
  }
  @media screen and (max-width: 768px) {
    .link-footer > a {
      color: #009b4c;
    }
  }
  @media screen and (min-width: 767px) {
    .link-footer > a {
      color: #009b4c;
      margin-right: 10px;
      margin-left: 10px;
    }
  }
  .link-footer > span {
    color: #888;
  }
  .social-footer > a {
    margin-right: 5px;
    margin-left: 5px;
  }
  .social-footer .icon-social-footer {
    width: 45px;
  }
  .contact-footer > a {
    color:#333;
    text-decoration: none;
    margin-right: 2px;
    margin-left: 2px;
  }
  .contact-footer .icon-contact-footer {
    width: 20px;
  }
  .contact-footer .text-contact-footer {
    font-size: 11px;
    font-weight: bold;
  }
</style>
@endsection

@section('body-start')
<body ng-app="myApp">
@endsection

@section('body-content')
  <nav class="navbar navbar-new navbar-unsubscribe">
    <div class="container-fluid">
      <a href="/">
        <img class="header-logo" alt="mamikos.com" title="Mamikos.com | Home" src="/assets/home-mobile/mamikos_header_mobile.png">
      </a>
    </div>
  </nav>

  <div class="container">
    <div class="row">
      <div class="col-md-6 col-md-push-3 unsubscribe-content">

        @if($allowed)
          @if(!$verified)
            <h1>Berhenti Berlangganan</h1>
            
            <p class="content-big">Apakah Anda yakin untuk berhenti berlangganan dari email rekomendasi kos dari MamiKos?</p>

            <br>

            <a href="/unsubscribe?type={{$original_query_string}}&verify=true" class="btn btn-unsubscribe">Berhenti Berlangganan</a>
          @else 
            <h1>Berhenti Berlangganan</h1>
            <p class="content-big">Anda sudah berhasil berhenti berlangganan dari email rekomendasi kami.</p>
            <p class="content-big">Jika Anda ingin berlangganan kembali, silakan simpan filter yang Anda inginkan kembali.</p>
          @endif
        @else
          <h1>Berhenti Berlangganan</h1>

          <p class="content-big">Link Anda tidak valid.</p>
          <p class="content-big">Pastikan Anda klik dari link "berhenti berlangganan" yang terdapat pada email.</p>
        @endif
      </div>
    </div>
  </div>

  <hr class="unsubscribe-separator">
  
  @include ('web.part.footer')
@endsection