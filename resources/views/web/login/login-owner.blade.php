@extends('web.login.loginLayout')

@section('meta-custom')
@if($_SERVER['HTTP_HOST'] != 'mamikos.com')
	<meta name="robots" content="noindex, nofollow">
@endif

<title>Login - Mamikos</title>
<link rel="canonical" href="https://mamikos.com/login"/>
<meta name="description" content="Ingin pasang iklan kost, apartemen atau lowongan kerja di Mamikos? Silakan klik disini, GRATIS!" />
<meta name="keywords" content="login pemilik, daftar pemilik, pasang iklan, kost, apartemen, lapangan kerja">
<meta name="twitter:title" content="Login - Mamikos">
<meta name="twitter:description" content="Ingin pasang iklan kost, apartemen atau lowongan kerja di Mamikos? Silakan klik disini, GRATIS!">
<meta name="og:title" content="Login - Mamikos">
<meta property="og:description" content="Ingin pasang iklan kost, apartemen atau lowongan kerja di Mamikos? Silakan klik disini, GRATIS!" />

<link rel="preload" href="{{ mix_url('dist/js/_login/app.js') }}" as="script">
@endsection


@section('data')
<script>

	var captchaKey = '{{ env('RECAPTCHA_SITE_KEY') }}';

</script>
@endsection

@section('content')
<div id="app">
	<app></app>
</div>
@endsection



@section('script')
	<script src="{{ mix_url('dist/js/_login/app.js') }}"></script>
@endsection
