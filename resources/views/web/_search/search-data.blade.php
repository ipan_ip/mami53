	<script>
		@if(isset($criteria))
			@if(!is_null($criteria))
				var mamiCriteria = @JZON($criteria);
			@else
				var mamiCriteria = {
					_id: null,
					gender: null,
					keywords: "",
					latitude: null,
					longitude: null,
					price_max: 10000000,
					price_min: 0,
					rent_type: 2,
					zoom: 14
				};
			@endif
		@else
			var mamiCriteria = {
				_id: null,
				gender: null,
				keywords: "",
				latitude: null,
				longitude: null,
				price_max: 10000000,
				price_min: 0,
				rent_type: 2,
				zoom: 14
			};
		@endif
	</script>

	<script>
		var dataLayerParams;

		try {
			dataLayerParams = new URLSearchParams(window.location.search);
		} catch (e) {
			dataLayerParams = null;
		};

		var dataLayerProps = {
			device: "none",
			utm_source: "none",
			type_room: "none",
			price_day: "0",
			price_week: "0",
			price_month: "0",
			city: "none",
			area: "none"
		};

		if (window.innerWidth < 768) {
			dataLayerProps.device = "mobile";
		} else {
			dataLayerProps.device = "desktop";
		};

		try {
			dataLayerProps.utm_source = dataLayerParams.get('utm_source');
			if (dataLayerProps.utm_source === null) dataLayerProps.utm_source = "none";
		} catch (e) {
			dataLayerProps.utm_source = "none";
		};

		if (mamiCriteria.gender === "2") {
			dataLayerProps.type_room = "Khusus Putri";
		} else if (mamiCriteria.gender === "1") {
			dataLayerProps.type_room = "Khusus Putra";
		} else if (mamiCriteria.gender === "0,2") {
			dataLayerProps.type_room = "Putri dan Campur";
		} else if (mamiCriteria.gender === "0,1") {
			dataLayerProps.type_room = "Putra dan Campur";
		} else if (mamiCriteria.gender === "0") {
			dataLayerProps.type_room = "Campur";
		} else {
			type_room = "All";
		}

		
		dataLayer.push({
			"language": "id",
			"page_category": "ImpressionDetailKost",
			"currency": "IDR",
			"device": dataLayerProps.device,
			"utm_source": dataLayerProps.utm_source,
			"utm_medium": dataLayerProps.utm_medium,
			"utm_content": dataLayerProps.utm_content,
			"utm_campaign": dataLayerProps.utm_campaign,
			"step": 2,
			"page_url": location.href,
			"country": "Indonesia",
			"city": dataLayerProps.city,
			"area": dataLayerProps.area,
			"price_day": dataLayerProps.price_day,
			"price_week": dataLayerProps.price_week,
			"price_month": dataLayerProps.price_month,
			"type_room": dataLayerProps.type_room,
			@if (Auth::check())
			"customer_id": "{{ (Auth::user()->id) }}",
			"profile_id": "{{ (Auth::user()->id) }}",
			"gender": "{{ (Auth::user()->gender) }}"
			@else
			"customer_id": "none",
			"profile_id": "none",
			"gender": "none"
			@endif
		});
	</script>