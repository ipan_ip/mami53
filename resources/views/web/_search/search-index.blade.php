@extends('web.@layout.globalLayout')

@section('head')
	@if (!empty($_SERVER['HTTP_HOST']))
		@if ($_SERVER['HTTP_HOST'] == 'mamikos.com')
			<meta name="robots" content="index, follow">
		@else
			<meta name="robots" content="noindex, nofollow">
		@endif
	@else
		<meta name="robots" content="noindex, nofollow">
	@endif

	<title>{{$meta['title']}}</title>
	<meta name="description" content="{{$meta['description']}}">
	<link rel="canonical" href="{{ Request::url() }}">

	<meta property="og:url" content="{{ Request::url() }}">
	<meta property="og:title" content="{{$meta['title']}}">
	<meta property="og:image" content="{{$meta['image_url']}}">
	<meta property="og:description" content="{{$meta['description']}}">

	<meta name="twitter:url" content="{{ Request::url() }}">
	<meta name="twitter:title" content="{{$meta['title']}}">
	<meta name="twitter:description" content="{{$meta['description']}}">
	<meta name="twitter:image" content="{{$meta['image_url']}}">

	<meta name="keyphrases" content="Info Kost, Cari Kost, Sewa Kost, Kost Bebas, Kost Murah, Kost pasutri, Aplikasi Kost, Aplikasi Pencarian Kost, Aplikasi Info Kost, APlikasi Cari Kost, Kost, Mamikost, Mamikosapp, Kamar Kost, Kamar Kos, Kostan, Kos, Rumah Kost, Rumah Kos, Kost Harian">

	<meta name="classification" content="Business, Rent House, Sewa Kost, Property, Rent Room, Info Kost, Information, Kost, Room, Cari Kost, Kost Murah, Kost Bebas, Application, Mobile Application, Kamar Kost, Kamar Kos, Kostan, Kos, Rumah Kost, Rumah Kos, Kost Harian">

	<meta name="keywords" content="{{$meta['keywords']}}">

	<link rel="preload" href="{{ mix_url('dist/js/_search/app.js') }}" as="script">
@endsection

@section('data')
	@include('web._search.search-data')
@endsection

@section('styles')
	@include('web.@style.preloading-style')
@endsection

@section('content')
	@include('web.@style.preloading-content')
	<div id="app">
		<app></app>
	</div>
@endsection

@section('scripts')
	@if (Auth::check() and Auth::user()->is_owner == 'false')
		@include('web.@script.sendbird-widget')
	@endif
	
	<script src="{{ mix_url('dist/js/_search/app.js') }}" async defer></script>
	@include('web.@script.aws-kds-logged-user-tracker')
@endsection