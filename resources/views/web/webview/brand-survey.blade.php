<html>
<head>
	<link rel="canonical" href="https://mamikos.com/privacy" />
	<link rel="author" href="https://plus.google.com/u/1/+Mamikos/posts"/>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	@include('web.@meta.favicon')

	<link href="{{ mix_url('dist/css/vendorsimple.css') }}" rel="stylesheet">
	<script src="{{ mix_url('dist/js/vendorsimple.js') }}"></script>

	<style type="text/css">
	.survey-container {
		padding: 15px;
		max-width: 420px;
		min-height: 180px;
		margin:	auto;
	}

	.survey-title {
		padding: 5px 0;
	}
	.survey-title h4{

	}

	.survey-input {
		margin-top: 30px;
	}

	.survey-button {
		margin-top: 25px;
	}

	.btn-mamigreen {
		color: #fff;
		background-color: #1BAA56;
		border-color: #239923;
	}

	.btn-mamigreen:active,
	.btn-mamigreen:hover,
	.btn-mamigreen:focus {
		color: #fff;
		background-color: #1f881f;
		border-color: #1b771b;
		text-decoration: none;
	}

	.full-width {
		width: 100%;
	}

	.success {
		color: #1BAA56;
		font-size: 24px;
	}

</style>
</head>
<body>

	<div id="brandSurveyApp" class="survey-container">
		<form @submit.prevent="sendBrandSurvey(brandSurvey.answer)" v-if="!brandSurvey.success">
			<div class="survey-title">
				<h4 class="text-center">Dari mana kamu tahu tentang Mamikos?</h4>
			</div>

			<div class="radio">
				<label><input type="radio" name="optSurvey" v-model="brandSurvey.answer" value="Event">Event</label>
			</div>
			<div class="radio">
				<label><input type="radio" name="optSurvey" v-model="brandSurvey.answer" value="Teman">Teman</label>
			</div>
			<div class="radio">
				<label><input type="radio" name="optSurvey" v-model="brandSurvey.answer" value="Iklan">Iklan di situs lain</label>
			</div>
			<div class="radio">
				<label><input type="radio" name="optSurvey" v-model="brandSurvey.answer" value="Youtube">Youtube</label>
			</div>
			<div class="radio">
				<label><input type="radio" name="optSurvey" v-model="brandSurvey.answer" value="Instagram">Instagram</label>
			</div>
			<div class="radio">
				<label><input type="radio" name="optSurvey" v-model="brandSurvey.answer" value="Facebook">Facebook</label>
			</div>
			<div class="radio">
				<label><input type="radio" name="optSurvey" v-model="brandSurvey.answer" value="Google">Google</label>
			</div>
			<div class="radio">
				<label><input type="radio" name="optSurvey" v-model="brandSurvey.answer" value="other">Lainnya</label>
			</div>
			<!-- <div class="radio radio-success">
				<input type="radio" id="gender-input-male" name="gender-input-male" value="male" v-model="brandSurvey.answer">
				<label for="gender-input-male">Google</label>
			</div> -->

			<div class="input">
				<input id="surveyOthers" type="text" class="form-control" required minlength="3" maxlength="50" placeholder="Silakan isi sendiri :) " :disabled="brandSurvey.loading || brandSurvey.answer != 'other'" v-model="brandSurvey.answerOther">
			</div>



			<div class="survey-button">
				<button type="submit" class="btn btn-mamigreen full-width" v-if="!brandSurvey.loading">Submit</button v-cloak>
					<button type="button" class="btn btn-mamigreen full-width" disabled v-else v-cloak>Mengirim...</button>
				</div>
			</form>

			<div class="survey-title" v-else>
				<h4 class="text-center success">Terimakasih!</h4>
				<p class="text-center">Jawabanmu sudah kami terima :)</p>
			</div>
		</div>


		<script type="text/javascript">
			Vue.config.productionTip = false;

			new Vue({

				el: '#brandSurveyApp',
				data: {
					brandSurvey: {
						loading: false,
						answer: '',
						success: false,
						answerOther: ''
					},
					textboxActive: false,
				},
				created() {
				// tracker('ga', 'send', 'event', {
				// 	eventCategory: 'Detail Brand Survey Open',
				// 	eventAction: 'mounted',
				// 	eventLabel: 'Modal Brand Survey'
				// });
			},
			methods: {
				sendBrandSurvey(answer) {

					this.brandSurvey.loading = true;

					var theAnswer = '';
					if (this.brandSurvey.answer == 'other') {
						theAnswer = this.brandSurvey.answerOther;
					} else {
						theAnswer = this.brandSurvey.answer;
					}

					$.ajax({
						type: 'POST',
						url: '/garuda/brand/survey',
						headers: {
							'Content-Type': 'application/json',
							'X-GIT-Time': '1406090202',
							Authorization: 'GIT WEB:WEB'
						},
						data: JSON.stringify({
							answer: theAnswer
						}),
						crossDomain: true,
						dataType: 'json',
						success: (response) => {
							if (response.status) {
								Cookies.set('brandSurveyStatus', 'true', { expires: 365 });
									this.brandSurvey.success = true;

									setTimeout(function() {
										window.location.replace('https://mamikos.com?user_dismiss=true');
									},
									3000);
								}
							}
						});

							}
						}


					});

				</script>
			</body>
			</html>