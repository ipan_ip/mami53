<!DOCTYPE html>
<html lang="en">
    <head>
     <!-- Meta
  ================================================== -->
    <title>Maintenance Mamikos</title>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="Mohon Maaf, Mamikos.com sedang maintenance, silakan tunggu beberapa saat lagi.">
    <meta name="author" content="https://plus.google.com/u/1/+Mamikos/posts">
    
    @include('web.@meta.favicon')

    <meta property="og:title" content="Maintenance Mamikos"/>
    <meta property="og:description" content="Mohon Maaf, Mamikos.com sedang maintenance, silakan tunggu beberapa saat lagi." />
    <meta property="og:type" content="website" />
    <meta property="fb:app_id" content="607562576051242"/>
    <meta property="og:image" content="/assets/mamikos_maintenance_trans_icon.png/">
  
  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries
  ================================================== -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
  <![endif]-->
    <link rel="stylesheet" href="/dist/css/vendor.css">
  <!-- Stylesheet
  ================================================== -->

    <style type="text/css">
        body {
            font-family: 'Lato', sans-serif;
            font-size: 14px;
            line-height: 1.42857143;
            color: #333;
            background-color: #fff;
            padding-bottom: 0px !important;
        }
        .img-head-div {
            text-align: center; 
            padding-top: 50px;
        }
        .img-head {
            width:250px;
        }
        .jobsabout{
            padding:12px 15px;
            border:1px solid white; 
            color:white;
            cursor: pointer;
        }
        .link-mamikos a {
            color:white;
        }
        .foot-maintenance {
            padding:0px;
            background-color:#2D2D2D;
            color:white;
            text-align:center;
            margin-top: 50px;
        }
        .title-maintenance {
          font-size: 30px;
          color: #666666;
          font-weight: bold;
        }
        .report-maintenance {
          margin-top: 20px;
          font-size: 25px;
          color: #8a8a8a;
          font-weight: bold;
        }

    </style>
    </head>
    <body>
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-4 col-xs-12 img-head-div">
              <img class="img-head" src="/assets/mamikos_maintenance_trans_full.png" alt="Maintenance Mamikos">
          </div>
          <div class="col-md-8 col-xs-12" style="padding-top:50px;">
             <div class="col-xs-12 title-maintenance">
               <span>Mamikos Sedang Dalam Perbaikan.</span>
             </div>
             <div class="col-xs-12 report-maintenance">
               <span>Laporkan ke Mami (Whatsapp Only) ke <a href="https://wa.me/6281929749399">0819-2974-9399</a> atau email ke <a href="mailto:saran@mamikos.com">saran@mamikos.com</a>.</span>
             </div>
             <div class="col-xs-12 report-maintenance">
               <span>Mamikos akan segera kembali!</span>
             </div>
          </div>
          <div class="col-xs-12 foot-maintenance">
            <br><br>
            <a href="/">
              <img style="width:220px;cursor:pointer;" src="/assets/lplogo.png">
            </a>
            <br>
            <br>
            <a href="http://jobs.mamikos.com" target="_blank" rel="noopener" style="color:white;"><span class="jobsabout col-md-2 col-md-offset-4 col-xs-5 col-xs-offset-1" style="margin-right:10px;">Jobs Mamikos</span></a>
            <a href="/tentang-kami" target="_blank" rel="noopener" style="color:white;"><span class="jobsabout col-md-2 col-xs-5" style="margin-right:10px;">About Us</span></a>
            <br>
            <h2 class="col-xs-12" style="font-size:15px;"><strong>Dapatkan "info kost murah" hanya di Mamikos App. Mau "Sewa Kost Murah"? Download Mamikos App Sekarang!</strong></h2>
            <br>
          </div>
          <div class="col-xs-12" style="padding:0px;background-color:#2D2D2D;color:white;">
            <div class="col-xs-12 link-mamikos" style="padding:0px;background-color:#2D2D2D;padding-top: 20px;color:white;text-align:center; display:inline-block; border-top:2px solid whitesmoke;">
              <a href="https://wa.me/6281929749399"><i class="fa fa-phone-square"></i>&nbsp; 0819-2974-9399 (Whatsapp Only)</a><div class="visible-xs visible-sm"><br></div>
              &nbsp;&nbsp;&nbsp;<a href="mailto:saran@mamikos.com"><i class="fa fa-envelope-o"></i>&nbsp; saran@mamikos.com</a>
              &nbsp;&nbsp;&nbsp;<a href="https://www.facebook.com/Mamikos-Cari-Kos-Gampang-1043376389026435/?fref=ts"><i class="fa fa-facebook-square"></i>&nbsp; mamikos - cari kos gampang</a>
              &nbsp;&nbsp;&nbsp;<a href="https://www.instagram.com/mamikosapp/"><i class="fa fa-instagram"></i>&nbsp; mamikosapp</a>
              &nbsp;&nbsp;&nbsp;<a href="https://twitter.com/mamikosapp"><i class="fa fa-twitter-square"></i>&nbsp; mamikosapp</a>
              &nbsp;&nbsp;&nbsp;<a href="https://line.me/ti/p/edZkGCB5ci"><img src="/assets/icon-line.png" alt="icon line mamikos" style="width:20px;">&nbsp; mamikosapp</a>
            <br>
            <br>
            </div>
          </div>
        </div>
      </div>
    </body>
</html>
