@extends('web.@layout.globalLayout')

@section('head')
	@if (!empty($_SERVER['HTTP_HOST']))
		@if ($_SERVER['HTTP_HOST'] == 'mamikos.com')
			<meta name="robots" content="index, follow">
		@else
			<meta name="robots" content="noindex, nofollow">
		@endif
	@else
		<meta name="robots" content="noindex, nofollow">
	@endif

	<title>About Us - Mamikos.com</title>
	<meta name="description" content="Mamikos adalah aplikasi pencari kost no.1 di Indonesia. Mamikos menyediakan layanan pencari kost yang gampang dan cepat serta memiliki data kost yang akurat.">
	<link rel="canonical" href="https://mamikos.com/tentang-kami">

	<meta property="og:url" content="https://mamikos.com/tentang-kami">
	<meta property="og:title" content="About Us - Mamikos.com">
	<meta property="og:image" content="/general/img/pictures/share-image-default.jpg">
	<meta property="og:description" content="Mamikos adalah aplikasi pencari kost no.1 di Indonesia. Mamikos menyediakan layanan pencari kost yang gampang dan cepat serta memiliki data kost yang akurat.">

	<meta name="twitter:url" content="https://mamikos.com/tentang-kami">
	<meta name="twitter:title" content="About Us - Mamikos.com">
	<meta name="twitter:description" content="Mamikos adalah aplikasi pencari kost no.1 di Indonesia. Mamikos menyediakan layanan pencari kost yang gampang dan cepat serta memiliki data kost yang akurat.">
	<meta name="twitter:image" content="/general/img/pictures/share-image-default.jpg">

	<meta name="keyphrases" content="Info Kost, Cari Kost, Sewa Kost, Kost Bebas, Kost Murah, Kost pasutri, Aplikasi Kost, Aplikasi Pencarian Kost, Aplikasi Info Kost, APlikasi Cari Kost, Kost, Mamikost, Mamikosapp, Kamar Kost, Kamar Kos, Kostan, Kos, Rumah Kost, Rumah Kos, Kost Harian">

	<meta name="classification" content="Business, Rent House, Sewa Kost, Property, Rent Room, Info Kost, Information, Kost, Room, Cari Kost, Kost Murah, Kost Bebas, Application, Mobile Application, Kamar Kost, Kamar Kos, Kostan, Kos, Rumah Kost, Rumah Kos, Kost Harian">

	<meta name="keywords" content="Mamikos, Tentang Mamikos, About Mamikos, Perusahaan mamikos, Apa itu Mamikos">

	<link rel="preload" href="{{ mix_url('dist/js/_about-us/app.js') }}" as="script">
@endsection

@section('data')

@endsection


@section('styles')
	@include('web.@style.preloading-style')
@endsection


@section('content')
	@include('web.@style.preloading-content')

	<div id="app">
		<app></app>
	</div>
@endsection


@section('scripts')
	@if (Auth::check())
		@if (Auth::user()->is_owner == 'false')
			@include('web.@script.sendbird-widget')
		@endif
	@endif

	<script src="{{ mix_url('dist/js/_about-us/app.js') }}" async defer></script>
	@include('web._about-us.about-us-schema-org')
	@include('web.@script.aws-kds-logged-user-tracker')
@endsection
