<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "Organization",
  "name": "About Us - Mamikos.com",
  "url": "https://mamikos.com/tentang-kami",
  "logo": "https://mamikos.com/general/img/logo/icon_mamikos.svg",
  "sameAs": [
    "https://mamikos.com",
    "https://twitter.com/mamikosapp",
    "https://www.instagram.com/mamikosapp/",
    "https://facebook.com/mamikosapp/",
    "https://www.youtube.com/channel/UCAFR9AxAP9bnUghQ1C-6kIQ"
  ]
}
</script>