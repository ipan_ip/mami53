<section id="modals">
  <!-- Modal Loading Overlay -->
  <div id="modalLoading" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="spinner">
          <div class="bounce1"></div>
          <div class="bounce2"></div>
          <div class="bounce3"></div>
        </div>
      </div>
    </div>
  </div> <!-- End of loading overlay modal -->
</section>