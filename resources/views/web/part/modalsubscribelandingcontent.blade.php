<!-- MODAL Login User-->
<div class="modal fade" id="subscribe" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
ng-class="{'modal-hide': modalHide, 'modal-show':!modalHide}">
  <div class="modal-dialog modal-subscribe">
    <div class="modal-content">
      <div class="modal-body">
        <div class="modal-header" style="border: none;padding: 0;">
          <button type="button" class="close" data-dismiss="modal" ng-click="closeForm(true);">&times;</button>
        </div>
          <h4 class="modal-title login-title text-center">Silakan isi data diri Anda</h4>
          @if(isset($landingContent->login_subtitle) && !is_null($landingContent->login_subtitle))
            <p class="login-description text-center">{{ $landingContent->login_subtitle }}</p>
          @endif
          <form class="form-login" name="formLogin" ng-submit="subscribeSubmit()">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <label for="job">Kamu Tim mana?</label>
            <div class="job-options">
              <div class="tabs" ng-init="selected='kuliah'">
                <a ng-click="selected='kuliah'; chooseJob('kuliah');"><button class="btn btn-mamigreen" ng-class="{'is-active-kuliah': isActiveKuliah}">Kuliah</button></a>
                <a ng-click="selected='kerja'; chooseJob('kerja');"><button class="btn btn-mamigreen" ng-class="{'is-active-kerja': isActiveKerja}">Kerja</button></a>
              </div>
            </div>

            <div class="tab-content">
              <div ng-if="selected==='kuliah'" class="animate-switch">
                <label for="chooseKuliah" name="kuliah">Kampus mana pilihanmu?</label>
                <select class="form-control" id="chooseKuliah" ng-model="userData.selectedArea" required>
                  <option ng-repeat="(i, campus) in campusList" value="<% campus %>"><% i %></option>
                </select>
                <br>
              </div>
              <div ng-if="selected==='kerja'" class="animate-switch">
                 <label for="chooseKerja" name="kerja">Kerja dimana?</label>
                 <select class="form-control" id="chooseKerja" ng-model="userData.selectedArea"  required>
                   <option ng-repeat="city in cityList"><% city %></option>
                 </select>
                 <br>
              </div>
            </div>

            <label for="email">Email</label>
            <input class="form-control user-login-email" type="email" name="email" placeholder="E-mail" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" ng-model="userData.email" title="Masukkan Email Anda" required>
            <br>

            <label for="phone_number">Nomor Telepon <span class="text-red">( Tidak Wajib )</span></label>
            <input class="form-control user-login-phone" type="tel" name="phone_number" placeholder="No. Handphone" minlength="9" maxlength="15" pattern="\d*" ng-model="userData.phone" title="Masukkan No. HP Anda">
            <br>

            <button class="btn btn-mamigreen btn-subscribe track-user-subscribe" type="submit">
              Subscribe
            </button>
          </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
