<nav class="navbar navbar-fixed-top banner-section visible-xs">
  <div class="container-fluid">
    <div class="navbar-header banner-container">
      <div class="col-xs-8 banner-content" style="padding-right: 0;">
        <span class="content-close" onclick="hideBanner()">&times;</span>
        <img src="/assets/banner_icon.png" alt="banner_icon mamikos" class="track-download-app-mobile-banner" style="height:50px" onclick="installMamikos()">
        <div class="banner-app flex-center-col track-download-app-mobile-banner" onclick="installMamikos()">
          <p id="bannerText" style="font-size: 12px;">Dapatkan Aplikasi MamiKos</p>
          <!-- <div class="app-star">
            <i class="fa fa-star" aria-hidden="true"></i>
            <i class="fa fa-star" aria-hidden="true"></i>
            <i class="fa fa-star" aria-hidden="true"></i>
            <i class="fa fa-star" aria-hidden="true"></i>
            <i class="fa fa-star-half-o" aria-hidden="true"></i>
          </div> -->
        </div>
      </div>
      <div class="col-xs-4 banner-button track-download-app-mobile-banner" onclick="installMamikos()">
        <label>Install</label>
      </div>
    </div>
  </div>
</nav>

<nav class="navbar navbar-fixed-top navbar-new">
  <div id="navbarCommon" class="container-fluid">
    <div class="navbar-header">
      <div class="navbar-mobile">
        <button type="button" class="navbar-toggle pull-left" data-toggle="collapse" data-target="#mamikosNavbarLeft">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>                        
        </button>
        <a class="navbar-brand navbar-logo hidden-sm" href="/">
           <img alt="logo_mamikos" title="mamikos.com" src="/assets/logo/mamikos_header_logo_full_default.png"   style="height:40px">
        </a>
        <a class="navbar-brand navbar-logo visible-sm" href="/">
         <img alt="logo_mamikos" title="mamikos.com" src="/assets/logo/mamikos_header_logo_default.png" style="height:45px">
        </a>
        <button type="button" class="navbar-toggle toggle-search pull-right" data-toggle="collapse" data-target="#mamikosNavbarRight">
          <span class="glyphicon glyphicon-search"></span>           
        </button>
      </div>
    </div>
    <form class="navbar-form navbar-left navbar-search hidden-xs hidden-sm" @submit.prevent="searchForRooms($event)">
      <div class="form-group">
        <div class="input-group">
          <input type="search" name="q" class="form-control" title="Pencarian Kost di Mamikos" placeholder="Ketik nama kost, tempat atau alamat" required minlength="3" maxlength="140" v-model="data.input">
          <button type="submit" class="input-group-addon"><i class="fa fa-search search-symbol" aria-hidden="true"></i></button>
        </div>
      </div>
    </form>
    <div class="collapse navbar-collapse" id="mamikosNavbarLeft">
      <ul class="nav navbar-nav navbar-right">
        <li class="hidden-xs"><a href="/cari">Cari Lewat Peta</a></li>
        @if (!Auth::check())
          <li class="navbar-promotion hidden-xs"><a class="track-kost-promotion" href="/promosi-kost"><i class="fa fa-plus"></i>&nbsp; Promosikan Kost mu</a></li>
          <li class="visible-xs"><a class="track-kost-promotion" href="/promosi-kost">Promosikan Kost mu</a></li>
        @endif
        @if (Auth::check())
          @if(Auth::user()->is_owner == 'true')
            <li class="account-dropdown">
              <a class="dropdown-toggle" id="dropdownOwner" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" title="Akun Pemilik Kost">
                <span class="owner-photo" style="background-image: url('/assets/icons/ic_akun_pemilik.png');"></span>
                <span class="owner-name"><span class="user-photo"></span>{{substr(Auth::user()->name, 0, 20) . (strlen(Auth::user()->name) > 20 ? '...' : '')}}</span>
                <i class="fa fa-caret-down dropdown-caret" aria-hidden="true"></i>
              </a>
              <ul class="dropdown-menu" aria-labelledby="dropdownOwner">
                <li><a href="/ownerpage">Halaman Pemilik</a></li>
                <!-- <li role="separator" class="divider"></li> -->
                <li><a href="/auth/logout">Keluar</a></li>
              </ul>
            </li>
          @else
            <li><a class="track-kost-history" href="/history">Histori</a></li>
            <li class="hidden-md hidden-lg"><a href="/user">
              <span class="user-photo" style="background-image: url('/assets/icons/ic_akun_user.png');"></span> Halaman Profil</a>
            </li>
            <li class="hidden-md hidden-lg"><a href="/auth/logout">Keluar</a></li>
            <li class="account-dropdown hidden-xs hidden-sm">
              <a class="dropdown-toggle" id="dropdownUser" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" title="Akun Pencari Kost">
                <span class="user-photo" style="background-image: url('/assets/icons/ic_akun_user.png');"></span>
                <span class="user-name"><span class="user-photo"></span>{{substr(Auth::user()->name, 0, 20) . (strlen(Auth::user()->name) > 20 ? '...' : '')}}</span>
                <i class="fa fa-caret-down dropdown-caret" aria-hidden="true"></i>
              </a>
              <ul class="dropdown-menu" aria-labelledby="dropdownUser">
                <!-- <li><a href="">Histori</a></li>
                <li><a href="">Rekomendasi</a></li>
                <li><a href="">Pengaturan</a></li>
                <li role="separator" class="divider"></li> -->
                <li><a href="/user">Halaman Profil</a></li>
                <li><a href="/auth/logout">Keluar</a></li>
              </ul>
            </li>
          @endif
        @else
          <li><a data-toggle="modal" data-target="#login" data-modal-text="lewat :">Login User</a></li>
        @endif
      </ul>
    </div>
    <div class="collapse navbar-collapse" id="mamikosNavbarRight">
      <ul class="nav navbar-nav text-right visible-xs">
        <li><a href="/cari">Cari Lewat Peta</a></li>
      </ul>
    </div>
  </div>
</nav>