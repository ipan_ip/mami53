<!-- MODAL Login User-->
<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body text-center">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title login-title">Silakan isi data diri Anda</h4>
          @if(!is_null($landingContent->login_subtitle))
            <p class="login-description">{{ $landingContent->login_subtitle }}</p>
          @endif
          <img class="login-popup-img" src="/assets/popup-login.png">
          <form class="form-login" method="POST" action="/auth/facebook" name="formLogin" v-if="selectedLogin == 1" v-cloak ng-submit="loginSubmit()">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input class="form-control user-login-email" type="email" name="email" placeholder="E-mail" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" title="Masukkan Email Anda" required>
            <br>
            <input class="form-control user-login-phone" type="tel" name="phone_number" placeholder="No. Handphone" minlength="9" maxlength="15" pattern="\d*" title="Masukkan No. HP Anda" required>
            <br>
            <input type="search" name="residence" ng-model="residence.name" required places-auto-complete xxx-component-restrictions="{country:'id'}" types="" on-place-changed="placeChanged(place)" class="form-control" placeholder="{{ !is_null($landingContent->location_placeholder) ? $landingContent->location_placeholder : '' }}" title="Masukkan Nama Lokasi">
            <input type="hidden" name="url" value="{{str_replace('http','https',request()->url())}}"><br>
            <h4 class="modal-title login-title">Berikutnya Silakan Login Via</h4>
            <h4 class="modal-title login-title login-select">
              <select id="loginSelector" class="login-selector" v-on:change="loginUsing()">
                <option value="1">Facebook</option>
                <option value="2">Google</option>
              </select>
            </h4>
            <button class="facebook-login-button track-user-login" type="submit" v-if="selectedLogin == 1" v-cloak>
              <i class="fa fa-facebook-official" aria-hidden="true"></i>
              Login with Facebook
            </button>
            <button class="google-login-button track-user-login" type="submit" v-else-if="selectedLogin == 2" v-cloak>
              Login with Google
            </button>
          </form>
        <form class="form-login" method="POST" action="/auth/google" name="formLogin" v-if="selectedLogin == 2" v-cloak ng-submit="loginSubmit()">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <input class="form-control user-login-email" type="email" name="email" placeholder="E-mail" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" title="Masukkan Email Anda" required>
          <br>
          <input class="form-control user-login-phone" type="tel" name="phone_number" placeholder="No. Handphone" minlength="9" maxlength="15" pattern="\d*" title="Masukkan No. HP Anda" required>
          <br>
          <input type="search" name="residence" ng-model="residence.name" required places-auto-complete xxx-component-restrictions="{country:'id'}" types="" on-place-changed="placeChanged(place)" class="form-control" placeholder="{{ !is_null($landingContent->location_placeholder) ? $landingContent->location_placeholder : '' }}" title="Masukkan Nama Lokasi">
          <input type="hidden" name="url" value="{{str_replace('http','https',request()->url())}}"><br>
          <h4 class="modal-title login-title">Berikutnya Silakan Login Via</h4>
          <h4 class="modal-title login-title login-select">
            <select id="loginSelector" class="login-selector" v-on:change="loginUsing()">
              <option value="1">Facebook</option>
              <option value="2">Google</option>
            </select>
          </h4>
          <button class="facebook-login-button track-user-login" type="submit" v-if="selectedLogin == 1" v-cloak>
            <i class="fa fa-facebook-official" aria-hidden="true"></i>
            Login with Facebook
          </button>
          <button class="google-login-button track-user-login" type="submit" v-else-if="selectedLogin == 2" v-cloak>
            Login with Google
          </button>
        </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->