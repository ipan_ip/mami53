{{-- <script>
  //SHOW KOST PHOTOS WITH SWIPEBOX
  $( '#mediaPhoto' ).click( function( e ) {
      e.preventDefault();
      $.swipebox( [
        @foreach ($cards as $key => $card)
          { href:'{{$card['photo_url']['large']}}', title:'{{ preg_replace("/[^A-Za-z0-9.]/", " ", str_replace(["-", "-"], " ", substr($card['description'], 0, 140))) }}' },
        @endforeach
      ],
      { useSVG : false, removeBarsOnMobile: false, hideBarsDelay : 0 }
      );
  });
</script> --}}

@if($has_photo_round)
<script>
  //SHOW KOST 360 PHOTO ON MODAL WITH PHOTOSPHEREJS
  $( '#media360' ).click( function( e ) {
      e.preventDefault();
      $('#360Modal').modal('show');
      $('#360Modal').on('shown.bs.modal', function () {
        var pageWidth = $(window).width();
        var modalWidth = $(".modal-photo-sphere").width();

        //Handle 360 Modal Size
        if (pageWidth >= 768) {
          $('#photoSphereViewer').css({"width": modalWidth-30, "height": 480});
        }
        else {
          $('#photoSphereViewer').css({"width": modalWidth-30, "height": 240});
        }

        //Handle 360 Modal Position
        if (pageWidth >= 768 && pageWidth <= 991) {
          $(".modal-photo-sphere").css("margin-top", "223.5px");
        }
        else if (pageWidth >= 992) {
          $(".modal-photo-sphere").css("margin-top", "30.5px");
        }

        //Fire PhotoSphereViewer
        var PSV = new PhotoSphereViewer({
          panorama: '{{$photo_360['large']}}',
          container: 'photoSphereViewer',
          time_anim: 3000,
          loading_txt:'Memuat Foto 360...',
          navbar: true,
          navbar_style: {
            backgroundColor: 'rgba(58, 67, 77, 0.7)'
          }
        });
      })
  } );
</script>
@endif

@if(!is_null ($youtube_id) && $youtube_id !== "")
  <script>
  //SHOW KOST VIDEO ON MODAL
  $( '#mediaVideo' ).click( function( e ) {
      e.preventDefault();
      $('#youtubeModal').modal('show');
  });
  </script>
@endif
