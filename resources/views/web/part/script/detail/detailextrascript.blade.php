<script>
  var scopeData= {
    "id":<?php echo $seq?>,
    @if(is_null($price_title) || $price_title == '')
      "price": 0,
    @else
      "price": {{ $price_title }},
    @endif
    "cover_photo_url" : "<?php echo $photo_url['large'];?>",
    "share_url":"<?php echo $slug;?>/",
    "location":
      [<?php echo $location[1]+$location_id;?>,
      <?php echo $location[0]+$location_id;?>]
    ,
    "photos":
    [
      <?php
      $count = count($cards);
      $x = 0;
      foreach($cards as $card){
      if($x < $count-1){
      echo '{"url":"'.$card['photo_url']['large'].'" , "description":"'.str_replace("\"", "", str_replace("-"," ", preg_replace("/[\n\r]/","",$card['description']))).'"},';
       }
      $x++;

      }
      if(isset($cards[$count-1])){
      echo '{"url":"'.$cards[$count-1]['photo_url']['large'].'" , "description":"'.str_replace("\"", "",str_replace("-"," ", preg_replace("/[\n\r]/","",$cards[$count-1]['description']))).'"}';
      }
      ?>
    ],
    "update":"<?php echo ($updated_at)?>",
    "loc_id": <?php echo $location_id;?>
  };

  @if(!isset($apartment_project_id))
    @if ($gender == 2)
      var criteriaGender = 'putri';
    @elseif ($gender == 1)
      var criteriaGender = 'putra';
    @else
      var criteriaGender = 'campur';
    @endif
  @endif
  </script>

<script>
  var detailLocation = {};
  var centerz = [];
  var namez = "{{$room_title}}";

  $.ajax({
    type: 'GET',
    url: "/garuda/stories/location/{{$location_id}}",
    headers : {"Content-Type":"application/json", "X-GIT-Time": "1406090202", "Authorization": "GIT WEB:WEB"},
    crossDomain: true,
    success: function (data) {
      centerz = [
        data.location.latitude,
        data.location.longitude
      ];

      detailLocation = data.location;
    }
  });
</script>