@if($is_premium_owner && !isset($apartment_project_id))
<script>
    //SHOW KOST PHOTOS WITH SWIPER MODAL
    $( '.room-info-action' ).click( function( e ) {
      e.preventDefault();

      $('#photosModal').modal('show');
      $('#photosModal').on('shown.bs.modal', function () {
        var roomPhotoSwiper = new Swiper ('.swiper-container', {
          loop: true,
          autoplay: 6000,
          autoplayDisableOnInteraction: true,
          preloadImages: false,
          lazyLoading: true,
          pagination: '.swiper-pagination',
          paginationType: 'fraction',
          nextButton: '.swiper-button-next',
          prevButton: '.swiper-button-prev',
          keyboardControl: true,
          centeredSlides: true
        });
        roomPhotoSwiper.update();
      });
    });

    $( '.media-photo' ).click( function( e ) {
      e.preventDefault();
      e.stopPropagation();

      $('#photosModal').modal('show');
      $('#photosModal').on('shown.bs.modal', function () {
        var roomPhotoSwiper = new Swiper ('.swiper-container', {
          loop: true,
          autoplay: 6000,
          autoplayDisableOnInteraction: true,
          preloadImages: false,
          lazyLoading: true,
          pagination: '.swiper-pagination',
          paginationType: 'fraction',
          nextButton: '.swiper-button-next',
          prevButton: '.swiper-button-prev',
          keyboardControl: true,
          centeredSlides: true
        })
        roomPhotoSwiper.update();
      });
    });
</script>
@else
<script>
	$(document).ready(function () {
		var roomPhotoSwiper = new Swiper ('.swiper-container', {
	    loop: true,
	    autoplay: 6000,
	    autoplayDisableOnInteraction: true,
	    preloadImages: false,
	    lazyLoading: true,
	    pagination: '.swiper-pagination',
	    paginationType: 'fraction',
	    nextButton: '.swiper-button-next',
	    prevButton: '.swiper-button-prev',
      keyboardControl: true
  	});
	});
</script>
@endif

@if($has_photo_round)
<script>
  //SHOW KOST 360 PHOTO ON MODAL WITH PHOTOSPHEREJS
  var showedPhotoSphere = false;
  $( '.media-360' ).click( function( e ) {
      e.preventDefault();
      e.stopPropagation();

      $('#360Modal').modal('show');
      $('#360Modal').on('shown.bs.modal', function () {
        var pageWidth = $(window).width();
        var modalWidth = $(".modal-photo-sphere").width();

        //Handle 360 Modal Size
        if (pageWidth >= 768) {
          $('#photoSphereViewer').css({"width": modalWidth-30, "height": 480});
        }
        else {
          $('#photoSphereViewer').css({"width": modalWidth-30, "height": 240});
        }

        //Handle 360 Modal Position
        if (pageWidth >= 768 && pageWidth <= 991) {
          $(".modal-photo-sphere").css("margin-top", "223.5px");
        }
        else if (pageWidth >= 992) {
          $(".modal-photo-sphere").css("margin-top", "30.5px");
        }

        //Fire PhotoSphereViewer
        if (!showedPhotoSphere) {
          var PSV = new PhotoSphereViewer({
            panorama: '{{$photo_360['large']}}',
            container: 'photoSphereViewer',
            time_anim: 3000,
            loading_txt:'Memuat Foto 360...',
            navbar: true,
            navbar_style: {
              backgroundColor: 'rgba(58, 67, 77, 0.7)'
            }
          });

          showedPhotoSphere = true;
        }
      })
  } );
</script>
@endif

@if(!is_null ($youtube_id) && $youtube_id !== "")
  <script>
  //SHOW KOST VIDEO ON MODAL
  $( '.media-video' ).click( function( e ) {
      e.preventDefault();
      e.stopPropagation();

      $('#youtubeModal').modal('show');
  } );
  </script>
@endif

@if($promotion !== null)
<script>
  $('.btn-promo-link').on('click', function(e) {
      e.preventDefault();
      e.stopPropagation();

      $('#promotionModal').modal('show');
  });
</script>
@endif