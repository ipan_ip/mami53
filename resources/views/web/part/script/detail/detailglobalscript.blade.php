<script>
  @if(!isset($apartment_project_id))
    var propertyType = 'Kost';
  @else
    var propertyType = 'Apartment';
  @endif
</script>

<script>
  @if($is_premium_owner)
    var isPremiumOwner = true;
  @else
    var isPremiumOwner = false;
  @endif
</script>

<script>
  var roomData = {
    title: '{{$room_title}}',
    adminId: {{ $admin_id }},
    photo: {
      large: '{{ $photo_url['large'] }}',
      medium: '{{ $photo_url['medium'] }}',
      small: '{{ $photo_url['small'] }}'
    },
    locId: {{$location_id}},
    seq: {{$seq}},
    slug: '{{$slug}}',
    availableRoom: '{{$available_room}}',
    @if($is_promoted)
    isPromoted: 1,
    @else
    isPromoted: 0,
    @endif
    payFor: '{{$pay_for}}'
  };
</script>

<script>
  @if($is_booking)
    var roomCheckBooking = true;
  @else
    var roomCheckBooking = false;
  @endif
</script>

<script>
  @if($love_by_me)
    var isLoved = {
      state : true,
      count : {{ $love_count }}
    };
  @else
    var isLoved = {
      state : false,
      count : {{ $love_count }}
    };
  @endif
</script>

<script>
  var authCheckAll = false;
  var authCheckUser = false;
  var authCheckOwner = false;
  var authCheckOwnerPremium = false;
  var authCheckOwnerNormal = false;
  var userPhone = '';
</script>

<script>
  @if (Auth::check())
    authCheckAll = true;

    userPhone = '{{ Auth::user()->phone_number }}';

    @if(Auth::user()->is_owner == 'false')
        authCheckUser = true;
    @elseif(Auth::user()->is_owner == 'true')
        authCheckOwner = true;
    @endif

    @if($is_premium_owner)
      authCheckOwnerPremium = true;
    @else
      authCheckOwnerNormal = true;
    @endif

  @endif
</script>

<script>
  var authUserData = {
    id: '',
    name: '',
    email: '',
    phone: ''
  };
  var authOwnerData = {
    id: null,
    chatName: ''
  };
</script>

<script>
  @if (Auth::check())

    @if(Auth::user()->is_owner == 'false')
        authUserData = {
          id: '{{(Auth::user()->id)}}',
          name: '{{(Auth::user()->name)}}',
          email: '{{(Auth::user()->email)}}',
          phone: '{{(Auth::user()->phone_number)}}'
        };
    @endif

    @if($is_premium_owner)
      authOwnerData = {
        id : {{$owner_id}},
        chatName : '{{$owner_chat_name}}'
      };
    @endif

  @endif
</script>

<script>
  var chatVersion = {{ $chat_version }};
</script>