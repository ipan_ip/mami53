<script type="text/javascript">
    var centerModal = function () {
        function reposition() {
            var modal = $(this),
            dialog = modal.find('.modal-dialog');
            modal.css('display', 'block');

            // Dividing by two centers the modal exactly, but dividing by three
            // or four works better for larger screens.
            dialog.css("margin-top", Math.max(0, ($(window).height() - dialog.height()) / 2));
            dialog.css("margin-left", Math.max(0, ($(window).width() - dialog.width()) / 2));
        }
        // Reposition when a modal is shown
        $('.modal').on('show.bs.modal', reposition);
        // Reposition when the window is resized
        $(window).on('resize', function() {
            $('.modal:visible').each(reposition);
        });
    };

    //CENTER PLACING MODAL
    $(function() {
        centerModal();
    });

</script>

{{-- @if(!isset($modal_persuade))
    @if (Auth::check())
        @if(Auth::user()->is_owner == 'false')
        <script type="text/javascript">
            //LOGIN SOCIAL USER
            var persuadeUser = new Vue ({
              el: '#persuadeUser',
              data: {
                popup: {
                    type: 1,
                    title: ''
                },
                user: {
                    isLogin: false,
                    cookieValue: null
                }
              },
              mounted: function () {
                this.setModalType();

                @if (Auth::check())
                    @if(Auth::user()->is_owner == 'false')
                        this.user.isLogin = true;
                    @endif
                @endif

                if (Cookies.get('persuadeUser') != undefined) {
                    this.cookieValue = parseInt(Cookies.get('persuadeUser'));
                }

                if (this.user.isLogin) {
                    if (this.cookieValue == null) {
                        Cookies.set('persuadeUser', 0, { expires: 30 });
                    }

                    else if (this.cookieValue == 0) {
                        this.setModalText();

                        $('#persuadeUser').modal({ backdrop: 'static', keyboard: false });
                        Cookies.set('persuadeUser', 1, { expires: 30 });
                    }
                }
              },
              methods: {
                setModalType: function () {
                    var types, max, min, randomize;

                    types = [1, 2];
                    max = 1;
                    min = 0;
                    randomize = Math.floor(Math.random() * (max - min + 1)) + min;
                    this.popup.type = types[randomize];
                },
                setModalText: function () {
                    var titles, max, min, randomize;

                    if (this.popup.type == 1) {
                        titles = [
                            'Sudahkah kostmu / kost di sekitarmu terdaftar di Mamikos?',
                            'Apakah kostmu / kost di sekitarmu sudah terdaftar di Mamikos?',
                            'Kostmu belum terdaftar di Mamikos?',
                            'Sudahkah kamu mendapatkan kost dari Mamikos?'
                        ];
                        max = 3;
                        min = 0;
                        randomize = Math.floor(Math.random() * (max - min + 1)) + min;
                        this.popup.title = titles[randomize];
                    }
                    else {
                        titles = [
                            'Sudahkah kost di sekitarmu terdaftar di Mamikos?',
                            'Apakah kost di sekitarmu sudah terdaftar di Mamikos?',
                            'Ingin memiliki kebebasan finansial sejak dini?',
                            'Ingin ngekost di kost ekslusif tapi budget tidak mencukupi?'
                        ];
                        max = 3;
                        min = 0;
                        randomize = Math.floor(Math.random() * (max - min + 1)) + min;
                        this.popup.title = titles[randomize];
                    }
                },
                clickTrack: function (src) {
                    Cookies.set('persuadeUserLogin' + src, true, { expires: 30 });
                }
              }
            })
        </script>
        @endif
    @endif
@endif --}}