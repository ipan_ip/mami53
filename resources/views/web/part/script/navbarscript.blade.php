{{-- Setup Axios --}}
<script>
	if (typeof window.axios !== 'undefined') {
		window.axios.defaults.headers.common = {
		'X-Requested-With': 'XMLHttpRequest'
		};
		window.axios.defaults.baseURL = '/garuda';
		window.axios.defaults.headers = {
			'Content-Type': 'application/json',
			'X-GIT-Time': '1406090202',
			Authorization: 'GIT WEB:WEB'
		};
	}
</script>

<script>
	//RANDOMIZE BANNER INSTALL TEXT
	// var bannerTexts = [
	//   'Dapatkan Aplikasi MamiKos',
	//   'Lihat Peta Kost lebih mudah di App MamiKos',
	//   'Hubungi Pemilik lebih mudah, hanya di App MamiKos',
	//   'Dapatkan Rekomendasi Kost, hanya di App MamiKos'
	// ];

	// function setBannerText () {
	//   var max = 3;
	//   var min = 0;
	//   var randomize = Math.floor(Math.random() * (max - min + 1)) + min;
	//   $( "#bannerText" ).html( bannerTexts[randomize] );
	// };

	// setBannerText();

	hideBanner();

	//HIDE MOBILE BANNER
	function hideBanner(){
		$('.banner-section').remove();
		$('.navbar-new').css('top', '0');
		$('.breadcrumb-new').css('margin-top', '54px');
		//homepage
		$('.list-group-mobile').addClass('list-group-mobile-2');
		$('.list-group-mobile-2').addClass('list-group-mobile');
		//ownerpage
		$('.ownerpage-container').css('margin-top', '54px');
		//bookingpage
		$('.booking-container').css('margin-top', '54px');
		//common wrapper
		$('.wrapper').addClass('hidden-banner');
		$('.app-wrapper').addClass('banner-is-hidden');
		//others
		$('.header-margin').css('height', '54px');
		$('.promo-body').css('margin-top', '0');
		Cookies.set('bannerHidden', true, { expires: 1, path: '/' });
	};

	// //HIDE MOBILE BANNER IF HAS COOKIE
	// var bannerIsHidden = Cookies.get('bannerHidden');
	// if (bannerIsHidden) {
	// 	hideBanner();
	// }

	// //INSTALL MAMIKOS FROM BANNER
	// function installMamikos () {
	// 	//send analytic
	// 	tracker('ga', 'send', {
	// 		hitType: 'KlikInstallHP',
	// 		eventCategory: 'KlikInstallHP',
	// 		eventAction: 'KlikInstallHP',
	// 		eventLabel: 'KlikInstallHP'
	// 	});
	// 	var downloadMamikos = {
	// 		redirectSchemaWithFallback : function (schema)
	// 		{
	// 			//console.log(schema);
	// 			// Enabling Schema with Fallback if The User not Installed the App
	// 			if( navigator.userAgent.match(/Android/i) || (navigator.userAgent.toLowerCase().indexOf("android") > -1) )
	// 			{
	// 				// window.location.replace('market://details?id=com.git.stories');
	// 				// The Reason set to 1000, to make sure, the play store not override the app
	// 				// Set TimeOut to Low, The Anomaly will Happen
	// 				setTimeout(function(){
	// 					window.open('https://play.google.com/store/apps/details?id=com.git.mami.kos&utm_campaign=DAppAndroMwebHeader&utm_source=DAppMwebHeader&utm_medium=DAppMwebHeader&utm_term=DAppMwebHeader');
	// 				}, 1000);
	// 			}
	// 			// else if ( navigator.userAgent.match(/iPhone/i) )
	// 			else if ( navigator.userAgent.match(/(iPad|iPhone|iPod)/g) )
	// 			{
	// 					window.open('https://click.google-analytics.com/redirect?tid=UA-68769829-2&url=https%3A%2F%2Fitunes.apple.com%2Fid%2Fapp%2Fmami-kos%2Fid1055272843&aid=com.git.MamiKos&idfa=%25%7Bidfa%7D)&cs=DiosMwebHeader&cm=DiosMwebHeader&cn=DiosMwebHeader&cc=DiosMwebHeader&ck=DiosMwebHeader');
	// 			}
	// 		 else
	// 		 {
	// 				setTimeout(function(){
	// 					window.open('https://play.google.com/store/apps/details?id=com.git.mami.kos&utm_campaign=DAppAndroMwebHeader&utm_source=DAppMwebHeader&utm_medium=DAppMwebHeader&utm_term=DAppMwebHeader');
	// 				 }, 1000);
	// 			}
	// 		 },
	// 	};
	// 	downloadMamikos.redirectSchemaWithFallback('test');
	// }

</script>

<script type="text/javascript">
	$(document).ready(function(){
		//DOUBLE NAVBAR MENU MOBILE
		var navbarLeftOpen = false;
		var navbarRightOpen = false;
		$("#mamikosNavbarLeft").on('show.bs.collapse', function(){
			navbarLeftOpen = true;
			if (navbarLeftOpen == true) {
				$("#mamikosNavbarRight").collapse('hide');
			}
		});
		$("#mamikosNavbarRight").on('show.bs.collapse', function(){
			navbarRightOpen = true;
			if (navbarRightOpen == true) {
				$("#mamikosNavbarLeft").collapse('hide');
			}
		});
	});
</script>

@if (Auth::check())
<script type="text/javascript">
	//CHANGE COLOR OF DROPDOWN MENU CARET
	$(document).ready(function(){
		$(".account-dropdown").on("show.bs.dropdown", function(){
			$(".dropdown-caret").css("color", "yellow");
		});
		$(".account-dropdown").on("hide.bs.dropdown", function(){
			$(".dropdown-caret").css("color", "white");
		});
	});
</script>
@endif

@if (Auth::check())
	@if(Auth::user()->is_owner == 'false')

		@if(isset($autocomplete) && !$autocomplete)
			<script>
				//GET BOOKING COUNTER

				var bookingCounter = {
					total: '',
					status: false
				}

				Vue.config.productionTip = false;

				var navbarCommon = new Vue({
					el: '#navbarCommon',
					data: {
						bookingCounter,
					},
					mounted: function() {
						var cookiesBooking = false;
						if (Cookies.get('bookingCount')) {
								cookiesBooking = true;
						}
						if (!cookiesBooking) {
							$.ajax({
								type : "GET",
								url : "/garuda/booking/counter",
								headers : {"Content-Type":"application/json", "X-GIT-Time": "1406090202", "Authorization": "GIT WEB:WEB"},
								crossDomain: true,
								dataType: "json",
								success : function (response) {
									bookingCounter.total = response.data.counter.toString();
									if (response.data.counter == 0) {
										Cookies.set('bookingCount', bookingCounter.total, { expires: 1 });
										bookingCounter.status = false;
									} else {
										bookingCounter.status = true;
									}
								}
							});
						}
					}
				});
			</script>
		@endif

	@endif
@endif