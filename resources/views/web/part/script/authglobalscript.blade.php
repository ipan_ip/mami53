<script>
	var authCheck = {
		all: false,
		user: false,
		owner: false
	};
</script>

<script>
	@if (Auth::check())
		authCheck.all = true;

		@if (Auth::user()->is_owner == 'false')
				authCheck.user = true;
		@elseif (Auth::user()->is_owner == 'true')
				authCheck.owner = true;
		@endif

	@endif
</script>

<script>
	var authData = {
		all: {
			id: '',
			name: '',
			email: '',
			phone_number: '',
			photo_user: ''
		}
	};
</script>

<script>
	@if (Auth::check())
		authData = {
			all: {
				id: '{{ (Auth::user()->id) }}',
				name: '{{ (Auth::user()->name) }}',
				email: '{{ (Auth::user()->email) }}',
				phone_number: '{{ (Auth::user()->phone_number) }}',
				photo_user: '{{ (Auth::user()->photo_user) }}'
			}
		};
	@endif
</script>
