<nav class="navbar navbar-fixed-top banner-section visible-xs">
  <div class="container-fluid">
    <div class="navbar-header banner-container">
      <div class="col-xs-8 banner-content" style="padding-right: 0;">
        <span class="content-close" onclick="hideBanner()">&times;</span>
        <img src="/assets/banner_icon.png" alt="banner_icon mamikos" class="track-download-app-mobile-banner" style="height:50px" onclick="installMamikos()">
        <div class="banner-app flex-center-col track-download-app-mobile-banner" onclick="installMamikos()">
          <p id="bannerText" style="font-size: 12px;">Dapatkan Aplikasi MamiKos</p>
          <!-- <div class="app-star">
            <i class="fa fa-star" aria-hidden="true"></i>
            <i class="fa fa-star" aria-hidden="true"></i>
            <i class="fa fa-star" aria-hidden="true"></i>
            <i class="fa fa-star" aria-hidden="true"></i>
            <i class="fa fa-star-half-o" aria-hidden="true"></i>
          </div> -->
        </div>
      </div>
      <div class="col-xs-4 banner-button track-download-app-mobile-banner" onclick="installMamikos()">
        <label>Install</label>
      </div>
    </div>
  </div>
</nav>

<nav class="navbar navbar-fixed-top navbar-new">
  <div id="navbarCommon" class="container-fluid">
    <div class="navbar-header">
      <div class="navbar-mobile">
        @if ($var == 1 || $var == 2)
        <a href="{{$_SERVER['REQUEST_URI']}}#register_" class="mobile-menu visible-xs" :class="hash.name == 'register_' ? 'nav-is-active' : ''">Daftar</a>
        @endif
        <a href="{{$_SERVER['REQUEST_URI']}}#facilities_" class="mobile-menu visible-xs" :class="hash.name == 'facilities_' ? 'nav-is-active' : ''">Fasilitas</a>
        <a href="{{$_SERVER['REQUEST_URI']}}#testimonies_" class="mobile-menu visible-xs" :class="hash.name == 'testimonies_' ? 'nav-is-active' : ''">Testimoni</a>
        @if ($var == '' || is_null ($var) || $var == 3)
        <a href="{{$_SERVER['REQUEST_URI']}}#register_" class="mobile-menu visible-xs" :class="hash.name == 'register_' ? 'nav-is-active' : ''">Daftar</a>
        @endif
        <a href="{{$_SERVER['REQUEST_URI']}}#premium_" class="mobile-menu visible-xs" :class="hash.name == 'premium_' ? 'nav-is-active' : ''">PREMIUM</a>
        <a href="/login?page=login" target="_blank" rel="noopener" class="mobile-menu visible-xs">Login</a>
        <a class="navbar-brand navbar-logo visible-xs" href="/">
           <img alt="logo_mamikos" title="mamikos.com" src="/assets/logo/mamikos_header_logo_default.png"   style="height:40px;">
        </a>
        <a class="navbar-brand navbar-logo hidden-xs" href="/">
           <img alt="logo_mamikos" title="mamikos.com" src="/assets/logo/mamikos_header_logo_full_default.png"   style="height:40px;">
        </a>
      </div>
    </div>
    <div class="collapse navbar-collapse" id="mamikosNavbarLeft">
      <!-- <ul class="nav navbar-nav navbar-right visible-xs">
        <li><a href="{{$_SERVER['REQUEST_URI']}}#facilities_">Fasilitas</a></li>
        <li><a href="{{$_SERVER['REQUEST_URI']}}#testimonies_">Testimonial</a></li>
        <li><a href="{{$_SERVER['REQUEST_URI']}}#register_"><span class="navbar-tab-register">Daftar Kost</span></a></li>
        <li><a href="{{$_SERVER['REQUEST_URI']}}#premium_"><span class="navbar-tab-premium">PREMIUM</span></a></li>
      </ul> -->
      <ul class="nav navbar-nav navbar-right hidden-xs">
        @if ($var == 1 || $var == 2)
        <li><a href="{{$_SERVER['REQUEST_URI']}}#register" :class="hash.name == 'register' ? 'nav-is-active' : ''"><span class="navbar-tab-register" style="background-color: #fff;color: #1BAA56;font-weight: bold; padding: 4px 10px 5px;"><span style="color: #EC4A0C;">GRATIS</span>&nbsp;Promosi</span></a></li>
        @endif
        <li><a href="{{$_SERVER['REQUEST_URI']}}#facilities" :class="hash.name == 'facilities' ? 'nav-is-active' : ''">Fasilitas</a></li>
        <li><a href="{{$_SERVER['REQUEST_URI']}}#testimonies" :class="hash.name == 'testimonies' ? 'nav-is-active' : ''">Testimonial</a></li>
        @if ($var == '' || is_null ($var) || $var == 3)
        <li><a href="{{$_SERVER['REQUEST_URI']}}#register" :class="hash.name == 'register' ? 'nav-is-active' : ''"><span class="navbar-tab-register" style="background-color: #fff;color: #1BAA56;font-weight: bold; padding: 4px 10px 5px;"><span style="color: #EC4A0C;">GRATIS</span>&nbsp;Promosi</span></a></li>
        @endif
        <li><a href="{{$_SERVER['REQUEST_URI']}}#premium" :class="hash.name == 'premium' ? 'nav-is-active' : ''"><span class="navbar-tab-premium" style="font-weight: bold;">PREMIUM</span></a></li>
        <li><a href="/login?page=login" target="_blank" rel="noopener">Login - Mamikos</a></li>
      </ul>
    </div>
  </div>
</nav>