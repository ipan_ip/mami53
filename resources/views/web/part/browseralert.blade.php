<!-- MODAL Ganti Browser-->
<div id="changeBrowser" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body" style="text-align:center;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span>x</span></button>
          <p class="modal-title" style="text-align:center;color:green;padding-bottom:20px;font-size:18px;"><b> - Perhatian - </b></p>
        <img style="width:150px;" src="/assets/iconfailed.png" alt="failed">
        <p style="color:#000;padding-top:20px;font-size:15px;">Maaf, browser yang Anda gunakan kurang optimal untuk menjalankan aplikasi web MamiKos. Silakan gunakan internet browser lainnya untuk kenyamanan Anda dalam menggunakan fitur-fitur kami.</p>
        <p style="color:#000;font-size:15px;">Atau silakan download aplikasi mobile MamiKos di smartphone Anda.</p>
      </div>
      <div class="modal-footer" style="border:0px;">
        <p style="color:#1BAA56;text-align:left;font-size:14px;">*Recommended Browser:</p>
        <div class="pull-left">
          <a href="https://www.mozilla.org/en-US/firefox/products/">
            <img src="/assets/browsers/icon_firefox.gif" alt="Mozilla Firefox" title="Mozilla Firefox">
          </a>
          <a href="https://www.google.com/chrome/">
            <img src="/assets/browsers/icon_chrome.gif" alt="Google Chrome" title="Google Chrome">
          </a>
          <a href="http://www.opera.com/download">
            <img src="/assets/browsers/icon_opera.gif" alt="Opera Browser" title="Opera Browser">
          </a>
        </div>
      </div>
    </div>
  </div>
</div>