<link rel="preload" href="/css/fonts/lato.css" as="style">
<link rel="stylesheet" href="/css/fonts/lato.css">

{{-- AddThis style//don't remove --}}
<style>
@media (max-width:991px){.addthis_bar{font-family:Lato,sans-serif!important}.addthis_bar .addthis_bar_message{font-size:14px!important;font-family:Lato,sans-serif!important}.addthis_bar .at-cv-button,.addthis_bar .at-cv-input{font-size:12px!important;font-family:Lato,sans-serif!important}}@media (min-width:992px){.addthis_bar.addthis_bar_bottom{display:none!important;visibility:hidden!important}}
</style>

@include('web.@script.polyfill-snippet')

@include('web.@script.utility')

@include('web.@script.third-parties-sdk-head')

@include('web.@script.service-worker')
