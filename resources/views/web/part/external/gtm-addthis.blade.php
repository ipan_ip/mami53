{{-- AddThis style --}}
<style>
@media (min-width:992px){.addthis_bar{font-family:Lato,sans-serif!important}.addthis_bar.addthis_bar_bottom{display:none!important;visibility:hidden!important}.addthis_bar .addthis_bar_message{font-size:14px!important;font-family:Lato,sans-serif!important}.addthis_bar .at-cv-button,.addthis_bar .at-cv-input{font-size:12px!important;font-family:Lato,sans-serif!important}}
</style>

<!-- Google Tag Manager -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K9V8K4"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<script async>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-K9V8K4');</script>
<!-- End Google Tag Manager -->

<script>
	window.addEventListener(
		'load',
		function() {
			if (!window.ga || !ga.create) {
				window.ga = function() {
					return null;
				};
			}
		},
		false
	);
</script>

<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-592531df92c0e6ca" async></script>
