<!-- MODAL Cari Lokasi 1-->
<div class="modal fade" tabindex="-1" role="dialog" id="checkLoc" ng-cloak>
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body" style="text-align:center;">
        <button type="button" class="close closemodal" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="closetext">x</span>
        </button>
        <h4 class="modal-title" style="text-align:center; color:green; font-weight:bold;padding-bottom: 20px;">Maaf</h4>
        <img style="width:150px;" src="/assets/iconarea.png" alt="icon area">
        <p style="color: limegreen;padding-top: 20px;">Silakan masukkan lokasi/daerah yang ingin kamu cari.</p>
      </div>
      <div class="modal-footer" style="border:0px;">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- MODAL Cari Lokasi 2-->
<div class="modal fade" tabindex="-1" role="dialog" id="checkLoc2" ng-cloak>
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body" style="text-align:center;">
        <button type="button" class="close closemodal" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="closetext">x</span>
        </button>
        <h4 class="modal-title" style="text-align:center; color:green; font-weight:bold;padding-bottom: 20px;">Perhatian</h4>
        <img style="width:150px;" src="/assets/iconarea.png" alt="icon area">
        <p style="color: limegreen;padding-top: 20px;">Silakan masukkan lokasi/daerah dari pilihan yang tersedia</p>
      </div>
      <div class="modal-footer" style="border:0px;">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>