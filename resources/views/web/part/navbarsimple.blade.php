<nav class="navbar navbar-fixed-top banner-section visible-xs">
  <div class="container-fluid">
    <div class="navbar-header banner-container">
      <div class="col-xs-8 banner-content" style="padding-right: 0;">
        <span class="content-close" onclick="hideBanner()">&times;</span>
        <img src="/assets/banner_icon.png" alt="banner_icon mamikos" class="track-download-app-mobile-banner" style="height:50px" onclick="installMamikos()">
        <div class="banner-app flex-center-col track-download-app-mobile-banner" onclick="installMamikos()">
          <p id="bannerText" style="font-size: 12px;">Dapatkan Aplikasi MamiKos</p>
          <!-- <div class="app-star">
            <i class="fa fa-star" aria-hidden="true"></i>
            <i class="fa fa-star" aria-hidden="true"></i>
            <i class="fa fa-star" aria-hidden="true"></i>
            <i class="fa fa-star" aria-hidden="true"></i>
            <i class="fa fa-star-half-o" aria-hidden="true"></i>
          </div> -->
        </div>
      </div>
      <div class="col-xs-4 banner-button track-download-app-mobile-banner" onclick="installMamikos()">
        <label>Install</label>
      </div>
    </div>
  </div>
</nav>

<nav class="navbar navbar-fixed-top navbar-new">
  <div id="navbarCommon" class="container-fluid">
    <div class="navbar-header">
      <div class="navbar-mobile">
        <button type="button" class="navbar-toggle pull-left" data-toggle="collapse" data-target="#mamikosNavbarLeft">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>                        
        </button>
        <a class="navbar-brand navbar-logo" href="/">
           <img alt="logo_mamikos" title="mamikos.com" src="/assets/logo/mamikos_header_logo_full_default.png"   style="height:40px;">
        </a>
        <button type="button" class="navbar-toggle toggle-search pull-right" data-toggle="collapse" data-target="#mamikosNavbarRight">
          <span class="glyphicon glyphicon-search"></span>           
        </button>
      </div>
    </div>
    <div class="collapse navbar-collapse" id="mamikosNavbarLeft">
      <ul class="nav navbar-nav navbar-right">
          <li class="account-dropdown">
            <a class="dropdown-toggle" id="dropdownOwner" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" title="Akun Pemilik Kost">
              <span class="owner-photo" style="background-image: url('/assets/icons/ic_akun_pemilik.png');"></span>
              <span class="owner-name"><span class="user-photo"></span>{{(Auth::user()->name)}}</span>
              <i class="fa fa-caret-down dropdown-caret" aria-hidden="true"></i>
            </a>
            <ul class="dropdown-menu" aria-labelledby="dropdownOwner">
              <!-- <li role="separator" class="divider"></li> -->
              <li><a href="/auth/logout">Keluar</a></li>
            </ul>
          </li>
      </ul>
    </div>
    <div class="collapse navbar-collapse" id="mamikosNavbarRight">
      <ul class="nav navbar-nav text-right visible-xs">
        <li><a href="/cari" target="_blank" rel="noopener">Cari Kost</a></li>
      </ul>
    </div>
  </div>
</nav>