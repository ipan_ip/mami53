<nav class="navbar navbar-fixed-top banner-section visible-xs">
  <div class="container-fluid">
    <div class="navbar-header banner-container">
      <div class="col-xs-8 banner-content" style="padding-right: 0;">
        <span class="content-close" onclick="hideBanner()">&times;</span>
        <img src="/assets/banner_icon.png" alt="banner_icon mamikos" class="track-download-app-mobile-banner" style="height:50px" onclick="installMamikos()">
        <div class="banner-app flex-center-col track-download-app-mobile-banner" onclick="installMamikos()">
          <p id="bannerText" style="font-size: 12px;">Dapatkan Aplikasi MamiKos</p>
          <!-- <div class="app-star">
            <i class="fa fa-star" aria-hidden="true"></i>
            <i class="fa fa-star" aria-hidden="true"></i>
            <i class="fa fa-star" aria-hidden="true"></i>
            <i class="fa fa-star" aria-hidden="true"></i>
            <i class="fa fa-star-half-o" aria-hidden="true"></i>
          </div> -->
        </div>
      </div>
      <div class="col-xs-4 banner-button track-download-app-mobile-banner" onclick="installMamikos()">
        <label>Install</label>
      </div>
    </div>
  </div>
</nav>

<nav class="navbar navbar-fixed-top navbar-new">
  <div id="navbarCommon" class="container-fluid">
    <div class="navbar-header">
      <div class="navbar-mobile">
        <button type="button" class="navbar-toggle pull-left" data-toggle="collapse" data-target="#mamikosNavbarLeft">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand navbar-logo hidden-xs hidden-sm" href="/">
           <img alt="logo_mamikos" title="mamikos.com" src="/assets/logo/mamikos_header_logo_full_default.png"   style="width:200px">
        </a>
        <a class="navbar-brand navbar-logo visible-sm" href="/">
           <img alt="logo_mamikos" title="mamikos.com" src="/assets/logo/mamikos_header_logo_default.png"   style="height:45px">
        </a>
        <a class="navbar-brand navbar-logo visible-xs" href="/">
         <img alt="logo_mamikos" title="mamikos.com" src="/assets/logo/mamikos_header_logo_full_default.png"   style="height:40px">
        </a>
        <button type="button" class="navbar-toggle toggle-search pull-right" data-toggle="collapse" data-target="#mamikosNavbarRight">
          <span class="glyphicon glyphicon-search"></span>
        </button>
      </div>
    </div>
    <div class="collapse navbar-collapse" id="mamikosNavbarLeft">
      <ul class="nav navbar-nav navbar-right">
        <li class="hidden-sm hidden-xs" data-toggle="dropdown">
          <a href="javascript:void(0)" data-target="#modal-download-app" data-toggle="modal">Download App</a>
        </li>
        {{-- <div class="dropdown-menu dropdown-download">
          <div class="col-xs-12 dropdown-header">Download Aplikasi Mamikos</div>
          <img class="col-xs-4 mamikos-logo" src="/assets/logo.png">
          <div class="col-xs-8 dropdown-text">Masukkan <strong>alamat Email</strong> Anda untuk<br>download Aplikasi MamiKos. Kami akan<br>mengirimkan tautan ke alamat Email<br>Anda.</div>
          <form class="form-download-app dropdown-form">
            <input id="email-download" class="col-xs-8 email-download" type="email" title="Masukkan alamat email di sini" placeholder="contoh: nama@domain.com" required>
            <button class="col-xs-2 track-user-download-app" type="button">KIRIM</button>
          </form>
          <div class="clearfix"></div>
          <a rel="nofollow" class="col-xs-5 p0" href="https://play.google.com/store/apps/details?id=com.git.mami.kos&utm_campaign=DAppAndroHeader&utm_source=DAppAndroidHeader&utm_medium=DAppAndroidHeader&utm_term=DAppAndroidHeader" target="_blank" rel="noopener">
            <img class="col-xs-12 gplay-logo" src="/assets/home-desktop/download_gplay.png">
          </a>
          <a rel="nofollow" class="col-xs-5 p0" href="https://click.google-analytics.com/redirect?tid=UA-68769829-2&url=https%3A%2F%2Fitunes.apple.com%2Fid%2Fapp%2Fmami-kos%2Fid1055272843&aid=com.git.MamiKos&idfa=%25%7Bidfa%7D)&cs=DAppIosHeader&cm=DAppIosHeader&cn=DIosHeader&cc=DAppIosHeader&ck=DAppIosHeader" target="_blank" rel="noopener">
            <img class="col-xs-12 appstore-logo" src="/assets/home-desktop/download_ios.png">
          </a>
        </div> --}}
        <li class="hidden-xs hidden-md track-apt-home-navbar">
          <a href="/apartemen" class="track-apt-home-navbar">Cari Apartemen</a>
        </li>
        <li class="hidden-xs hidden-md track-loker-home-navbar">
          <a href="/loker" class="track-loker-home-navbar">Cari Lowongan Kerja <span class="new-label track-loker-home-navbar">BARU</span></a>
        </li>
        <li class="hidden-xs hidden-sm hidden-lg track-apt-home-navbar">
          <a href="/apartemen" class="track-apt-home-navbar">Apartemen</a>
        </li>
        <li class="hidden-xs hidden-sm hidden-lg track-loker-home-navbar">
          <a href="/loker" class="track-loker-home-navbar">Loker <span class="new-label track-loker-home-navbar">BARU</span></a>
        </li>
        @if (!Auth::check())
        <li class="navbar-promotion hidden-xs hidden-sm track-kost-promotion" data-toggle="tooltip" data-placement="bottom" title="Masuk untuk pemilik" onclick="openNav()">
          <img src="/assets/home-desktop/daftarkan-kost.gif" class="track-kost-promotion">
          <span class="navbar-promotion-text">
            <b class="text-mamiorange">
              GRATIS!
            </b>
            <a class="track-kost-promotion">
              Promosikan Iklan Anda di sini
            </a>
          </span>
        </li>
        <li class="navbar-promotion visible-sm track-kost-promotion" data-toggle="tooltip" data-placement="bottom" title="Masuk untuk pemilik" onclick="openNav()">
          <img src="/assets/home-desktop/daftarkan-kost.gif" class="track-kost-promotion" style="height: 35px;">
          <span class="navbar-promotion-text">
            <b class="text-mamiorange">
              GRATIS!
            </b>
            <a class="track-kost-promotion">
              Promosikan Iklan Anda
            </a>
          </span>
        </li>
        <li class="visible-xs track-kost-promotion" onclick="openNav()">
          <a class="track-kost-promotion">
            Promosikan Iklan Anda di sini
            <span class="new-label new-label-mobile">
              GRATIS
            </span>
          </a>
        </li>
        <li class="visible-xs track-loker-home-navbar">
          <a href="/loker" class="track-loker-home-navbar">Cari Lowongan Kerja <span class="new-label new-label-mobile track-loker-home-navbar">BARU</span></a>
        </li>
        <li class="visible-xs track-apt-home-navbar">
          <a href="/apartemen" class="track-apt-home-navbar">Cari Apartemen</a>
        </li>
        @endif
        @if (Auth::check())
          @if(Auth::user()->is_owner == 'true')
          <li class="account-dropdown">
            <a class="dropdown-toggle" id="dropdownOwner" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" title="Akun Pemilik Kost">
              <span class="owner-photo" style="background-image: url('/assets/icons/ic_akun_pemilik.png');"></span>
              <span class="owner-name"><span class="user-photo"></span>{{substr(Auth::user()->name, 0, 20) . (strlen(Auth::user()->name) > 20 ? '...' : '')}}</span>
              <i class="fa fa-caret-down dropdown-caret" aria-hidden="true"></i>
            </a>
            <ul class="dropdown-menu" aria-labelledby="dropdownOwner">
              <li><a href="/ownerpage">Halaman Pemilik</a></li>
              <!-- <li role="separator" class="divider"></li> -->
              <li><a href="/auth/logout">Keluar</a></li>
            </ul>
          </li>
          @else
          <li><a class="track-kost-history" href="/history">Histori</a></li>
          {{-- <li v-show="bookingCounter.status == true">
            <a href="/booking/history" title="Jumlah booking yang aktif">
              <span>Booking </span>
              <span class="booking-counter is-loading" v-if="bookingCounter.total == ''"><i class="fa fa-circle-o-notch fa-spin"></i></span>
              <span class="booking-counter is-shown" v-else v-cloak>@{{bookingCounter.total}}</span>
            </a>
          </li> --}}
          <li class="hidden-md hidden-lg"><a href="/user">
            <span class="user-photo" style="background-image: url('/assets/icons/ic_akun_user.png');"></span> Halaman Profil</a>
          </li>
          <li class="hidden-md hidden-lg"><a href="/auth/logout">Keluar</a></li>
          <li class="account-dropdown hidden-xs hidden-sm">
            <a class="dropdown-toggle" id="dropdownUser" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" title="Akun Pencari Kost">
              <span class="user-photo" style="background-image: url('/assets/icons/ic_akun_user.png');"></span>
              <span class="user-name"><span class="user-photo"></span>{{substr(Auth::user()->name, 0, 20) . (strlen(Auth::user()->name) > 20 ? '...' : '')}}</span>
              <i class="fa fa-caret-down dropdown-caret" aria-hidden="true"></i>
            </a>
            <ul class="dropdown-menu" aria-labelledby="dropdownUser">
              <!-- <li><a href="">Histori</a></li>
              <li><a href="">Rekomendasi</a></li>
              <li><a href="">Pengaturan</a></li>
              <li role="separator" class="divider"></li> -->
              <li><a href="/user">Halaman Profil</a></li>
              <li><a href="/auth/logout">Keluar</a></li>
            </ul>
          </li>
          @endif
        @else
        <li>
          <a data-toggle="modal" data-target="#login" data-placement="bottom" title="Untuk pencari" data-modal-text="lewat :">Login User</a>
        </li>
        @endif
      </ul>
    </div>
    <div class="collapse navbar-collapse" id="mamikosNavbarRight">
      <ul class="nav navbar-nav text-right visible-xs">
        <li>
          <a href="/cari">
            Cari Berdasarkan Lokasi
          </a>
        </li>
        <li>
          <a href="/search" target="_blank" rel="noopener">
          Cari Berdasarkan Nama
          </a>
        </li>
      </ul>
    </div>
  </div>
</nav>