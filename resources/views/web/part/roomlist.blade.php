<div class="row">
  @foreach ($rooms as $key => $room)
    @if($key < $limit)
    <div 
      class="col-md-4 col-sm-6 col-xs-12" 
      >

      <div 
        class="kost-list-container material-card-1" 
        title="{{ $room['room-title'] }}"
        @click="openRoom(
          '{{ $room['share_url'] }}', 
          {{ $room['_id'] }}, 
          @if ($room['is_promoted']) true @else false @endif        
        )">
        
        @if(is_null($room['photo_url']))
        <div 
          class="kost-photo" 
          title="{{ $room['room-title'] }}"> <!-- background photo -->
        </div>
        @else
        <div 
          class="kost-photo" 
          @if($room['available_room'] == 0)
          :class="'fade'"
          @endif
          title="{{ $room['room-title'] }}" 
          v-lazy:background-image="'{{ $room['photo_url']['medium'] }}'"> <!-- background photo -->

          <div 
            class="kost-icon-bar" 
            >
              
              {{-- <div 
              class="love-by-me">
              @if($room['love_by_me'])
              <a
                class="set" 
                @click.stop="toggleLove(room._id,index)"
                >
              </a>
              @else
              <a 
                @click.stop="toggleLove(room._id,index)"
                >
              </a>
              @endif
            </div> --}}
            
            @if(isset($room['is_promoted']))
            <div class="sponsored">
              Sponsored tested
            </div>
            @endif

          </div>
        </div>
        @endif
        
        <div class="wrap-content">
        <!-- AREA LABEL KOST -->
        @if($room['apartment_project_id'] === 0)
        <div class="kost-detail">
          
          @if($room['gender'] === 0)
          <div 
            class="kost-tag kost-tag-gender-mixed" 
            >Campur
          </div>
          @endif
          
          @if($room['gender'] === 1)
          <div 
            class="kost-tag kost-tag-gender-male" 
            >Putra
          </div>
          @endif

          @if($room['gender'] === 2)
          <div 
            class="kost-tag kost-tag-gender-female"
            >Putri
          </div>
          @endif

           @if($room['area_label'])
              @if($room['promo_title'])
                <div 
                class="area-label area-promo"
                >
                <span class="sparator">·</span> 
                {{ $room['area_label'] }}  
                </div>
                @else
                <div 
                class="area-label"
                >
                <span class="sparator">·</span> 
                {{ $room['area_label'] }}  
                </div>
              @endif
            @endif
            
            @if($room['promo_title'])
            <a   
              href="#" 
              class="mami-room-promo">Ada Promo
            </a>
            @endif
        </div>
        @endif

        <!-- AREA LABEL APARTEMEN -->
        @if($room['apartment_project_id'] !== 0)
        <div class="kost-detail"> <!-- below this class should be inlines -->
          <div 
            class="kost-tag apartment-tag" 
            >Apartemen
          </div>
            
            @if($room['area_label'])
              @if($room['promo_title'])
                <div 
                class="area-label area-promo"
                >
                <span class="sparator">·</span> 
                {{ $room['area_label'] }}  
                </div>
                @else
                <div 
                class="area-label"
                >
                <span class="sparator">·</span> 
                {{ $room['area_label'] }}  
                </div>
              @endif
            @endif
            
            @if($room['promo_title'])
            <a   
              href="#" 
              class="mami-room-promo">Ada Promo
            </a>
            @endif
        </div>
        @endif

        <!-- PRICE KOST -->
        <div class="detail-price-kost">
          <span class="price">{{ $room['price_title_format']['currency_symbol'] }} {{ $room['price_title_format']['price'] }}<span class="rent-unit"> / {{$room['price_title_format']['rent_type_unit']}} </span>
          @if($room['apartment_project_id'] === 0)
          <span 
            class="sparator rent-unit">·
          </span>
          @endif 
          
          @if($room['apartment_project_id'] === 0)
          @if($room['available_room'] <= 1)
          <span 
            class="available-room red">
            Ada {{ $room['available_room'] }} kamar
          </span>
          @else
          <span 
            class="available-room green">
            Ada {{ $room['available_room'] }} kamar
          </span>
          @endif
          @endif

          </span>
        </div>

        <!-- TITLE KOST AND APARTMENT -->
        <div  
          class="kost-title"
          >{{ $room['room-title'] }}
        </div>

        <!-- KOST DETAIL -->
        @if($room['apartment_project_id'] === 0)
        <div class="kost-detail-two"> 
            <ul>
              
              @if($room['is_premium_owner'])
              <li>
                <img
                title="Premium" 
                alt="ic_premium" 
                src="/assets/icons/svg/ic_premium.svg">
              </li>
              @endif
              
              @if($room['verification_status']['is_verified_kost'])
              <li>
                <img 
                title="Terverifikasi"
                alt="ic_u_promoted" 
                src="/assets/icons/svg/ic_u_promoted.svg">
              </li>
              @endif
              
              @if($room['rating_string'] !== '0.0')
              <li 
              class="rating">
              <img
              title="Rating" 
              src="/assets/icons/svg/ic_star_s_green.svg"> 
              {{ $room['rating_string'] }}
              </li>
              @endif
              
              @if($room['online'])
              <li  
              class="history"><span class="sparator">·</span>
              {{ $room['online'] }}
              </li>
              @endif
            
            </ul>
        </div>
        @endif

        <!-- DETAIL APARTMENT -->
        @if($room['apartment_project_id'] !== 0)
        <div class="kost-detail-two"> 
            <ul>
              
              @if($room['is_premium_owner'])
              <li>
                <img
                title="Premium" 
                alt="ic_premium" 
                src="/assets/icons/svg/ic_premium.svg">
              </li>
              @endif
              
              @if($room['verification_status']['is_verified_kost'])
              <li>
                <img
                title="Terverifikasi" 
                alt="ic_u_promoted" 
                src="/assets/icons/svg/ic_u_promoted.svg">
              </li>
              @endif
              
              @if($room['rating_string'] !== '0.0')
              <li class="rating">
              <img
              title="Rating" 
              src="/assets/icons/svg/ic_star_s_green.svg"> 
              {{ $room['rating_string'] }}
              </li>
              @endif
              
              <li>
                 <div class="unit-tag-label">
                  {{ $room['unit_type'] }} - {{ $room['size'] }} m<sup>2</sup>
                 </div>
              <div class="unit-tag-label">{{ $room['furnished_status'] }}</div>
              </li>
            </ul>
        </div>
        @endif

        @if($room['apartment_project_id'] === 0)
        <div class="icon-container">

          @foreach($room['fac_room_ids'] as $fac_room_id)
            @if($fac_room_id == 1)
            <img 
              class="icon-facility" 
              src="/assets/icons/svg/ic_bathroom_o_grey_new.svg" 
              title="Kamar Mandi Dalam" 
              alt="km_dalam">
            @endif
          @endforeach

          @foreach($room['fac_room_ids'] as $fac_room_id)
            @if($fac_room_id == 13)
            <img 
              class="icon-facility" 
              src="/assets/icons/svg/ic_ac_o_grey.svg" 
              title="AC" 
              alt="ac">
            @endif
          @endforeach

          @foreach($room['fac_room_ids'] as $fac_room_id)
            @if($fac_room_id == 3)
            <img 
              class="icon-facility" 
              src="/assets/icons/svg/ic_bed_o_grey.svg" 
              title="Kasur" 
              alt="kasur">
            @endif
          @endforeach

          @foreach($room['fac_room_ids'] as $fac_room_id)
            @if($fac_room_id == 15)
            <img 
              class="icon-facility" 
              src="/assets/icons/svg/ic_wifi_o_grey.svg" 
              title="Wifi" 
              alt="wifi">
            @endif
          @endforeach

          @foreach($room['fac_room_ids'] as $fac_room_id)
            @if($fac_room_id == 9)
            <img 
              class="icon-facility" 
              src="/assets/icons/svg/ic_free24h_o_grey.svg" 
              title="24 Jam" 
              alt="24_jam">
            @endif
          @endforeach

          

        </div>
        @endif

        
        @if($room['apartment_project_id'] !== 0)
        <div 
          class="unit-icons" 
          >

          @if(is_null($room['unit_type_rooms']))
          <div class="unit-icon-container">
            <div class="unit-icon">
              <img 
                src="/assets/icons/apartment/bedroom_count.png" 
                class="unit-icon-image" 
                alt="bedroom_count" 
                title="Kamar Tidur">
            </div>
            <div class="unit-icon-label">{{ room.unit_type_rooms.bedroom }}</div> <!-- hang on -->
          </div>
          @endif

          @if(is_null($room['unit_type_rooms']))
          <div class="unit-icon-container">
            <div class="unit-icon">
              <img 
                src="/assets/icons/apartment/bathroom_count.png" 
                class="unit-icon-image" 
                alt="bathroom_count" 
                title="Kamar Mandi">
            </div>
            <div class="unit-icon-label">{{ $room['unit_type_rooms']['bathroom'] }}</div>
          </div>
          @endif

          @if(!is_null($room['floor']))
          <div class="unit-icon-container">
            <div class="unit-icon">
              <img 
                src="/assets/icons/apartment/floor_count.png" 
                class="unit-icon-image" 
                alt="floor_count" 
                title="Lantai">
            </div>
            <div class="unit-icon-label">{{ $room['floor'] }}</div>
          </div>
          @endif
        </div>
        @endif
      
      </div>
      </div>
    </div>
    @endif
  @endforeach
</div>
