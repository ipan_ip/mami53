<nav class="navbar navbar-fixed-top navbar-new">
  <div id="navbarCommon" class="container-fluid">
    <div class="navbar-header">
      <div class="navbar-mobile">
        <div class="navbar-brand navbar-logo">
           <img alt="logo_mamikos" title="mamikos.com" src="/assets/logo/mamikos_header_logo_full_default.png"   style="height:40px;">
        </div>
      </div>
    </div>
  </div>
</nav>