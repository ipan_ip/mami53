<div class=" container-fluid page-footer" style="width: 100%;">

  <div class="logo-footer">
    <img class="img-logo-footer" src="/assets/logo/svg/logo_mamikos_green.svg" alt="logo_mamikos_green" title="logo mamikos green">
  </div>

  <div class="text-footer">
    <h5>Dapatkan "info kost murah" hanya di MamiKos App.</h5>    
    <h5>Mau "Sewa Kost Murah"?</h5>
    <h5><strong>Download MamiKos App Sekarang!</strong></h5>
  </div>

  <div class="link-footer">
    <a href="/career" target="_blank" rel="noopener">Job MamiKos</a>
    <span>&nbsp;|&nbsp;</span>
    <a href="/tentang-kami" target="_blank" rel="noopener">Tentang Kami </a>
    <span>&nbsp;|&nbsp;</span>
    <a id="promosiKost" class="track-kost-promotion" href="/promosi-kost" target="_blank" rel="noopener">GRATIS! Promosikan Properti Anda</a>
    <span>&nbsp;|&nbsp;</span>
    <a href="/privacy" target="_blank" rel="noopener">Kebijakan Privasi</a>
    <span>&nbsp;|&nbsp;</span>
    <a href="https://help.mamikos.com/syarat-dan-ketentuan/?category=pemilik-dan-penghuni&subCategory=syarat-dan-ketentuan-umum&slug=syarat-dan-ketentuan-umum" target="_blank" rel="noopener">Syarat dan Ketentuan Umum</a>
    <span>&nbsp;|&nbsp;</span>
    @if (Auth::check() && Auth::user()->is_owner == 'true')
      <a href="https://help.mamikos.com/booking/owner" target="_blank" rel="noopener">Pusat Bantuan</a>
    @else 
      <a href="https://help.mamikos.com/booking/user" target="_blank" rel="noopener">Pusat Bantuan</a>
    @endif
  </div>

  <div class="social-footer">
    <a rel="nofollow" href="https://www.facebook.com/Mamikos-Cari-Kos-Gampang-1043376389026435/?fref=ts" target="_blank" rel="noopener">
      <img class="icon-social-footer" src="/assets/home-mobile/facebook.png">
    </a>
    <a rel="nofollow" href="https://twitter.com/mamikosapp" target="_blank" rel="noopener">
      <img class="icon-social-footer" src="/assets/home-mobile/twitter.png">
    </a>
    <a rel="nofollow" href="https://www.instagram.com/mamikosapp/" target="_blank" rel="noopener">
      <img class="icon-social-footer" src="/assets/home-mobile/instagram.png">
    </a>
  </div>

  <div class="contact-footer">
    <a href="mailto:saran@mamikos.com">
      <img class="icon-contact-footer" src="/assets/home-mobile/email.png">
      <span class="text-contact-footer"> saran@mamikos.com</span>
    </a>
    <a href="https://wa.me/6281929749399">
      <img class="icon-contact-footer" src="/assets/home-mobile/telp.png">
      <span class="text-contact-footer"> 0819-2974-9399 (Whatsapp Only)</span>
    </a>
  </div>
  
</div>