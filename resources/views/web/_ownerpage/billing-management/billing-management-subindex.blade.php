@extends('web._ownerpage.ownerpage-index')


@section('head-meta')
		<title>Dashboard Kelola Tagihan dan Booking</title>
		<meta name="description" content="Atur tagihan dan terima permintaan booking calon penghuni oleh pemilik iklan.">
		<link rel="canonical" href="https://mamikos.com/ownerpage/manage/">

		<meta property="og:url" content="https://mamikos.com/ownerpage/manage/">
		<meta property="og:title" content="Mamikos - Dashboard Kelola Tagihan dan Booking">
		<meta property="og:image" content="/assets/og/og_kost_v2.jpg">
		<meta property="og:description" content="Atur tagihan dan terima permintaan booking calon penghuni oleh pemilik iklan.">

		<meta name="twitter:url" content="https://mamikos.com/ownerpage/manage/">
		<meta name="twitter:title" content="Mamikos - Dashboard Kelola Tagihan dan Booking">
		<meta name="twitter:description" content="Atur tagihan dan terima permintaan booking calon penghuni oleh pemilik iklan.">

		<link rel="preload" href="{{ mix_url('dist/js/_ownerpage/billing-management/app.js') }}" as="script">
@endsection


@section('sub-scripts')
	<script src="{{ mix_url('dist/js/_ownerpage/billing-management/app.js') }}" async defer></script>

	@include('web.@script.aws-kds-logged-user-tracker')
@endsection
