@extends('web.@layout.globalLayout')


@section('head')
  <meta name="robots" content="noindex, nofollow">

	@yield('head-meta')

	@include('web.@style.preloading-style')
@endsection


@section('data')

	@yield('data-default')

@endsection


@section('styles')
	@include('web.@style.preloading-style')
@endsection


@section('content')
	@include('web.@style.preloading-content')

	<div id="app">
		<app></app>
	</div>
@endsection


@section('scripts')
	@yield('sub-scripts')
@endsection
