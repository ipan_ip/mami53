<!DOCTYPE html>
<html>
<head>
  <!-- Meta
  ================================================== -->
  <base href="/">
  <title>Daftarkan Kost untuk Booking</title>
  <meta name="robots" content="noindex, nofollow">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <meta name="application-name" content="Mamikos">
  <meta name="theme-color" content="#1BAA56">
  <link rel="manifest" href="/json/manifest.json">

  @include('web.@meta.link-prebrowsing')

  <link rel="canonical" href="https://mamikos.com/webview/request-booking">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  
  @include('web.@meta.favicon')
  
  <meta name="description" content="Mau kost mu ramai dikunjungi? Promosikan kost mu di MAMIKOS sekarang! Promosi di MAMIKOS GRATIS dan lebih EFEKTIF." />
  <meta property="fb:app_id" content="607562576051242" />
  <meta name="keywords" content="">
  <meta name="og:site_name" content="Mamikos" />
  <meta property="og:description" content="Mau kost mu ramai dikunjungi? Promosikan kost mu di MAMIKOS sekarang! Promosi di MAMIKOS GRATIS dan lebih EFEKTIF." />
  <meta property="og:type" content="website" />
  <meta property="og:url" content="https://mamikos.com/webview/request-booking" />
  <meta property="og:image" content="/assets/share-image-default.jpg" />
  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:site" content="@mamikosapp">
  <meta name="twitter:title" content="Promosikan Kost mu Gratis di MAMIKOS.COM">
  <meta name="twitter:description" content="Mau kost mu ramai dikunjungi? Promosikan kost mu di MAMIKOS sekarang! Promosi di MAMIKOS GRATIS dan lebih EFEKTIF.">
  <meta name="twitter:creator" content="@mamikosapp">
  <meta name="twitter:image" content="/assets/share-image-default.jpg">
  <meta name="twitter:domain" content="mamikos.com">

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries
  ================================================== -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
  <![endif]-->
  <!-- Stylesheet
  ================================================== -->
  <link rel="stylesheet" href="{{ mix_url('dist/css/vendor.css') }}">
  <link rel="stylesheet" href="{{ mix_url('dist/css/appmamikos.css') }}">

  @include('web.part.external.global-head')
  <!--  JavaScripts
    ================================================== -->
  <script src="https://maps.googleapis.com/maps/api/js?libraries=places&language=id&key={{ENV('MIX_GOOGLE_MAPS_JAVASCRIPT_API_KEY')}}"></script>





  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K9V8K4" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->

  <style>
    @media (max-width: 767px) {
      .navbar-new {
        top: 0;
      }

      .request-top .choice {
        margin-bottom: 0;
        font-size: 12px;
        font-weight: bold;
      }

      .request-top>p {
        margin-top: 5px;
        margin-bottom: 5px;
      }

      .request-bottom>input {
        padding-top: 2px;
        padding-bottom: 2px;
        margin-bottom: 5px;
      }

      .request-bottom>select {
        padding-top: 2px;
        padding-bottom: 2px;
        margin-bottom: 5px;
        margin-top: 5px;
      }

      .request-bottom .select-status {
        top: 9px;
        padding: 0;
      }

      .side-by-side .sr {
        margin-top: 15px;
      }
    }

    #request-booking-container {
      margin-top: 60px;
    }

    .p0 {
      padding: 0;
    }

    .request-btn {
      font-size: 14px;
      background-color: #196c18;
      color: #fff;
      border: none;
      padding-top: 6px;
      padding-bottom: 6px;
      margin-top: 5px;
      margin-bottom: 5px;
    }

    .request-btn:hover,
    .request-btn:active,
    .request-btn:focus {
      color: #fff;
      border-bottom: 3px solid #1BAA56;
    }

    .tl {
      text-align: left !important;
    }

    .navbar-mobile .navbar-logo {
      margin: 0 auto;
    }

    label.error {
      color: #ff0000;
    }

  </style>
</head>

<body>

  @include('web.part.navbarlogo')

  <div class="container" id="request-booking-container">
    <div class="row">
      <div class="col-md-6 col-md-push-3">
        <form id="requestForm" class="modal-request" v-on:submit.prevent>
          <div class="request-top">
            <p class="col-xs-12 p0">Silakan masukkan data Anda. Tim MamiKos akan menindaklanjuti permintaan Anda :</p>
          </div>

          <div class="request-bottom">
            <div class="form-horizontal">
              <div class="form-group">
                <div class="col-md-12">
                  <input class="col-xs-12 form-control" type="text" minlength="2" maxlength="30" name="name" title="tuliskan nama lengkap anda" placeholder="Tuliskan Nama Anda Di Sini" required v-model="name">
                </div>
              </div>

              <div class="form-group side-by-side">
                <div class="col-md-6 col-xs-12 sl">
                  <input class="col-xs-12 col-md-6 form-control" type="text" type="text" rangelength="[9,15]" number="true" name="phone" title="tuliskan no. hp yang valid" placeholder="Tuliskan No. HP" required v-model="phone">
                </div>
                <div class="col-md-6 col-xs-12 sr">
                  <input class="col-xs-12 col-md-6 form-control" type="email" name="email" title="tuliskan email yang valid" placeholder="Tuliskan Email" required v-model="email">
                </div>
              </div>

              <div class="form-group">
                <label for="status" class="control-label col-md-2 col-xs-2">Status</label>
                <div class="col-md-10 col-xs-10">
                  <select class="form-control" name="status" v-model="status">
                    <option>Agen</option>
                    <option>Pemilik</option>
                    <option>Anak Kost</option>
                    <option>Pengelola Kost</option>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <div class="col-md-12">
                  <input class="col-xs-12 form-control" type="text" minlength="6" maxlength="50" name="kost-name" title="tuliskan nama kost anda secara lengkap" placeholder="Tuliskan Nama Kost Anda Di Sini..." required v-model="kostName">
                </div>
              </div>

              <div class="form-group">
                <label class="col-xs-12 control-label tl" for="kostUrl"><strong>Apabila KOST Anda sudah ada di MamiKos masukkan linknya:</strong></label>
                <div class="col-md-12">
                  <input id="kostUrl" class="col-xs-12 form-control" type="url" rangelength="[15,250]" name="kost-url" title="isikan url kost yang valid" placeholder="Masukkan Link Kost Anda Di Sini..." v-model="kostUrl">
                </div>
              </div>

              <div class="form-group">
                <button class="col-xs-12 request-btn btn" type="submit" v-on:click="submitRequest()"><i class="request-loading" v-cloak></i>KIRIM</button>
              </div>
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>

  <!-- MODAL SUKSES-->
  <div class="modal fade" id="modalSuccess" role="dialog">

    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body" style="text-align:center;">
          <h4 class="modal-title" style="text-align:center;color:green;font-weight:bold;padding-bottom:20px;">Terima Kasih!</h4>
          <img style="width:150px;" src="/assets/suksesicon.png">
          <p style="color:limegreen;padding-top:20px;font-size:16px;">Permintaan Anda telah berhasil dikirim.<br>Silakan tunggu Tim dari MamiKos untuk menghubungi Anda.</p>

        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->

  <!-- MODAL SUKSES-->
  <div class="modal fade" id="modalError" role="dialog">

    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body" style="text-align:center;">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

          <h4 class="modal-title" style="text-align:center;color:green;font-weight:bold;padding-bottom:20px;">Aduh!</h4>
          <img style="width:150px;" src="/assets/iconfailed.png">
          <p style="color:limegreen;padding-top:20px;font-size:16px;" class="dialog-error-message"></p>
        </div>
        <div class="modal-footer" style="border:0px;">
          <button type="button" class="btn btn-default foot-btn" data-dismiss="modal" ng-click="zopimChatShow()">Close</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->

  <!--  JavaScripts
================================================== -->

  <script src="{{ mix_url('dist/js/vendor.js') }}"></script>
  @include ('web.part.script.bugsnagapikey')
  @include ('web.part.script.tokenerrorhandlescript')



  <script>
    $(function() {
      $('#modalSuccess').modal({
        keyboard: false
        , backdrop: 'static'
        , show: false
      });

      $('#modalError').modal({
        keyboard: false
        , backdrop: 'static'
        , show: false
      });

      $('#modalError').on('hidden.bs.modal', function(e) {
        $('.request-btn').removeAttr('disabled')
          .text('KIRIM');
      });
    });

    Vue.config.productionTip = false;

    var requestOwner = new Vue({
      el: '#requestForm'
      , data: {
        requestItem: ['Ingin Daftarkan Kamar Saya Untuk Layanan Booking']
        , name: ''
        , phone: ''
        , email: ''
        , status: 'Pemilik'
        , kostName: ''
        , kostUrl: ''
      }
      , methods: {
        submitRequest: function() {
          if (this.requestItem.length > 0) {
            var validator = $("#requestForm").validate();
            var valid = validator.form();

            if (valid == true) {
              $(".request-loading").show();
              $.ajax({
                type: "POST"
                , url: "/garuda/premium/request-direction"
                , headers: {
                  "Content-Type": "application/json"
                  , "X-GIT-Time": "1406090202"
                  , "Authorization": "GIT WEB:WEB"
                }
                , data: JSON.stringify({
                  "request-item": this.requestItem
                  , "name": this.name
                  , "phone": this.phone
                  , "email": this.email
                  , "status": this.status
                  , "kost-name": this.kostName
                  , "kost-url": this.kostUrl
                })
                , crossDomain: true
                , dataType: "json"
                , beforeSend: function() {
                  $('.request-btn').attr('disabled', 'disabled')
                    .text('SEDANG MENGIRIM');
                }
                , success: function(requestResponse) {
                  if (requestResponse.status == true) {
                    $(".request-loading").hide();
                    $("#modalSuccess").modal("show");
                  } else {
                    $('.dialog-error-message').text(requestResponse.meta.message);
                    $("#modalError").modal("show");
                  }
                }
              });
            }

          } else {
            alert('Pilihan permintaan Anda belum diisi');
          }
        }
      }
    });

  </script>

</body>
</html>
