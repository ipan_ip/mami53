@extends('web.@layout.globalLayout')


@section('head')

	<title>Lebih dari 2.000 kamar sudah bergabung dengan MamiRooms - Mamikos</title>
	<meta name="description" content="MamiRooms adalah property management untuk membantu pemilik kost mengoptimalkan pendapatan kost nya.">
	<link rel="canonical" href="https://mamikos.com/mamirooms">

	<meta property="og:url" content="https://mamikos.com/mamirooms">
	<meta property="og:title" content="Lebih dari 2.000 kamar sudah bergabung dengan MamiRooms - Mamikos">
	<meta property="og:image" content="/general/img/pictures/share-image-default.jpg">
	<meta property="og:description" content="MamiRooms adalah property management untuk membantu pemilik kost mengoptimalkan pendapatan kost nya.">

	<meta name="twitter:url" content="https://mamikos.com/mamirooms">
	<meta name="twitter:title" content="Lebih dari 2.000 kamar sudah bergabung dengan MamiRooms - Mamikos">
	<meta name="twitter:description" content="MamiRooms adalah property management untuk membantu pemilik kost mengoptimalkan pendapatan kost nya.">
	<meta name="twitter:image" content="/general/img/pictures/share-image-default.jpg">

	<link rel="stylesheet" href="/css/fonts/tiempos-headline.css">
	<link rel="stylesheet" href="/css/fonts/merriweather.css">

	<link rel="preload" href="{{ mix_url('dist/js/_mamirooms/landing/app.js') }}" as="script">

@endsection


@section('data')
 <script>
	 var conditions = @JZON($condition);
	 var facilities = @JZON($facility);
 </script>
@endsection

@section('styles')
	@include('web.@style.preloading-style')
@endsection


@section('content')
	@include('web.@style.preloading-content')

	<div id="app">
		<app></app>
	</div>
@endsection


@section('scripts')
	<script src="{{ mix_url('dist/js/_mamirooms/landing/app.js') }}" async defer></script>
@endsection
