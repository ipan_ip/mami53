<!DOCTYPE html>
<html lang="en">

<title>Mamikos - Input FAQ</title>
<link rel="canonical" href="https://mamikos.com/input-faq" />
<link rel="author" href="https://plus.google.com/u/1/+Mamikos/posts" />
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link rel="canonical" href="https://mamikos.com{{ $_SERVER['REQUEST_URI'] }}" />

@include('web.@meta.favicon')

<meta name="classification" content="Business, Rent House, Sewa Kost, Property, Rent Room, Info Kost, Information, Kost, Room, Cari Kost, Kost Murah, Kost Bebas, Application, Mobile Application">
<meta name="distribution" content="global">
<meta http-equiv="Reply-to" content="mamikos.app@gmail.com">
<meta name="revisit-after" content="1 day">
<meta name="keyphrases" content="Info Kost, Cari Kost, Sewa Kost, Kost Bebas, Kost Murah, Kost pasutri, Aplikasi Kost, Aplikasi Pencarian Kost, Aplikasi Info Kost, APlikasi Cari Kost, Kost, Mamikost, Mamikosapp">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="keywords" content="mamikost, Info Kost, Cari kost, kost yogyakarta, kost jogja, kost jakarta, kost bandung, kost depok, kost surabaya, indekost, kos, indekos, sewa">
<meta name="description" content="MAMIKOS adalah aplikasi pencari kost no.1 di Indonesia. MAMIKOS menyediakan layanan pencari kost yang gampang dan cepat serta memiliki data kost yang akurat.">
<meta name="author" content="Mamikos">
<meta name="robots" content="index, follow">
<meta property="fb:app_id" content="607562576051242" />
<meta property="og:title" content="Mamikos - Input FAQ" />
<meta name="og:site_name" content="Mamikos" />
<meta property="og:description" content="MAMIKOS adalah aplikasi pencari kost no.1 di Indonesia. MAMIKOS menyediakan layanan pencari kost yang gampang dan cepat serta memiliki data kost yang akurat." />
<meta property="og:type" content="website" />
<meta property="og:url" content="https://mamikos.com/input-faq" />
<meta property="og:image" content="/assets/share-image-default.jpg" />
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@mamikosapp">
<meta name="twitter:title" content="Mamikos - Input FAQ">
<meta name="twitter:description" content="MAMIKOS adalah aplikasi pencari kost no.1 di Indonesia. MAMIKOS menyediakan layanan pencari kost yang gampang dan cepat serta memiliki data kost yang akurat.">
<meta name="twitter:creator" content="@mamikosapp">
<meta name="twitter:image" content="/assets/share-image-default.jpg">
<meta name="twitter:domain" content="mamikos.com">

<!--  JavaScripts
  ================================================== -->
<script src="{{ mix_url('dist/js/vendor.js') }}"></script>
@include ('web.part.script.bugsnagapikey')
<link rel="stylesheet" href="{{ mix_url('dist/css/vendor.css') }}">
<style>
  html,
  body {
    font-family: 'Lato', sans-serif;
    background: url(/assets/100-1-blur.jpg) no-repeat center center fixed;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
  }

  a,
  a:active,
  a:hover,
  a:focus {
    color: #1BAA56;
  }

  .box {
    margin-top: 50px;
    margin-bottom: 50px;
    padding: 30px 50px;
    background-color: white;
    /*opacity: 0.9;*/
  }

  .top {
    color: #ADADAD;
    font-size: 12px;
  }

  .contact-us p {
    margin-bottom: 0;
  }

  .panel-group {
    margin-top: 20px;
  }

  .fa {
    color: #1BAA56;
    margin-right: 0.3em;
  }

  .panel-body {
    font-size: 16px;
  }

</style>




<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-K9V8K4" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>
  (function(w, d, s, l, i) {
    w[l] = w[l] || [];
    w[l].push({
      'gtm.start': new Date().getTime()
      , event: 'gtm.js'
    });
    var f = d.getElementsByTagName(s)[0]
      , j = d.createElement(s)
      , dl = l != 'dataLayer' ? '&l=' + l : '';
    j.async = true;
    j.src =
      '//www.googletagmanager.com/gtm.js?id=' + i + dl;
    f.parentNode.insertBefore(j, f);
  })(window, document, 'script', 'dataLayer', 'GTM-K9V8K4');

</script>
<!-- End Google Tag Manager -->
</head>
<body>
  <div class="container-fluid">
    <div class="row backdrop">
      <div clas="col-xs-12">
        <!-- Body Tulisan -->
        <div class="col-xs-12 col-md-8 col-md-offset-2 box">
          <div class="top">
            <a style="color:#ADADAD" href="/">Home</a>> Input FAQ
          </div>
          <img src="/assets/logo_3.png" style="width:200px;margin-top:30px">

          <h2>FAQ (Frequently Asked Questions) </h2>
          <h3>Input Kos di Mamikos </h3>

          <div class="panel-group" id="accordion">

            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <i class="fa fa-question-circle" aria-hidden="true"></i>
                  <a data-toggle="collapse" data-parent="#accordion" href="#question1">
                    Apa saja yang wajib di input?</a>
                </h4>
              </div>
              <div id="question1" class="panel-collapse collapse in">
                <div class="panel-body">Peta, alamat lengkap, nama kost, nama pemilik, nomor pemilik, tipe kost, luas kamar, harga kamar per bulan, kamar mandi.</div>
              </div>
            </div>

            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <i class="fa fa-question-circle" aria-hidden="true"></i>
                  <a data-toggle="collapse" data-parent="#accordion" href="#question2">
                    Foto boleh dilewati?</a>
                </h4>
              </div>
              <div id="question2" class="panel-collapse collapse">
                <div class="panel-body">Boleh. Tapi data anda menjadi tidak lengkap, dan tidak mendapat reward pulsa.</div>
              </div>
            </div>

            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <i class="fa fa-question-circle" aria-hidden="true"></i>
                  <a data-toggle="collapse" data-parent="#accordion" href="#question3">
                    Apa nanti data kost ini bisa saya rubah lagi?</a>
                </h4>
              </div>
              <div id="question3" class="panel-collapse collapse">
                <div class="panel-body">Tidak bisa. Yang dapat merubah data saat ini hanya pemilik kost.</div>
              </div>
            </div>

            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <i class="fa fa-question-circle" aria-hidden="true"></i>
                  <a data-toggle="collapse" data-parent="#accordion" href="#question4">
                    Kenapa foto harus landscape?</a>
                </h4>
              </div>
              <div id="question4" class="panel-collapse collapse">
                <div class="panel-body">Supaya tampilan detail kost anda menjadi rapi dan bagus.</div>
              </div>
            </div>

            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <i class="fa fa-question-circle" aria-hidden="true"></i>
                  <a data-toggle="collapse" data-parent="#accordion" href="#question5">
                    Bagaimana cara membuat foto landscape?</a>
                </h4>
              </div>
              <div id="question5" class="panel-collapse collapse">
                <div class="panel-body">Anda dapat crop dan edit foto atau bisa mengambil foto ulang.</div>
              </div>
            </div>

            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <i class="fa fa-question-circle" aria-hidden="true"></i>
                  <a data-toggle="collapse" data-parent="#accordion" href="#question6">
                    Apa ada reward jika saya menambahkan kost?</a>
                </h4>
              </div>
              <div id="question6" class="panel-collapse collapse">
                <div class="panel-body">Tergantung event yang sedang berlangsung. Untuk saat ini anda dapat mendapatkan pulsa 25rb jika data anda valid &amp; lengkap.</div>
              </div>
            </div>

            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <i class="fa fa-question-circle" aria-hidden="true"></i>
                  <a data-toggle="collapse" data-parent="#accordion" href="#question7">
                    Kenapa kost yang saya input tidak langsung tampil?</a>
                </h4>
              </div>
              <div id="question7" class="panel-collapse collapse">
                <div class="panel-body">Karena kita butuh waktu untuk memverifikasi data kost tersebut. Jika data valid akan kami tampilkan.</div>
              </div>
            </div>

          </div>

        </div>
      </div>
    </div>
  </div>
</body>
</html>
