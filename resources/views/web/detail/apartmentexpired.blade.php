

<!DOCTYPE html>
<html>
<head>
	<base href="/">

	<?php
	//$meta_title             = "Kost " . $area_city_keyword . " - " . $name_slug;
	$meta_title             =  $name_slug;
	$meta_author            = "https://plus.google.com/u/1/+Mamikos/posts";
	?>

	<base href="/">
	<title>{{ $meta_title }} - Mamikos </title>

	<meta name="csrf-token" content="{{ csrf_token() }}">

<meta name="application-name" content="Mamikos">
<meta name="theme-color" content="#1BAA56">
<link rel="manifest" href="/json/manifest.json">

@include('web.@meta.link-prebrowsing')

	<meta name="this-id" content="{{$_id}}">
	<meta name="load-time" content="{{date('Y-m-d H:i:s', time())}}">

	@if ($is_indexed == 1 )
	<link rel="canonical" href="{{ $share_url }}" />
	@endif

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">


	<meta name="description" content="Sewa {{ $meta_title }} murah. Dengan alamat di {{ $project['address'] }}. Sewa {{ $meta_title }} murah, harian, bulanan, tahunan. Klik di sini untuk info lengkapnya, hanya di Mamikos.com">
	<meta name="keywords" content="Sewa {{ $meta_title }} murah, harian, bulanan, tahunan.">
	<meta name="author" content="{{ $meta_author }}">

	<meta property="og:title" content="{{ $meta_title }}" />
	<meta property="og:description" content="Sewa {{ $meta_title }} murah. Dengan alamat di {{ $project['address'] }}. Sewa {{ $meta_title }} murah, harian, bulanan, tahunan. Klik di sini untuk info lengkapnya, hanya di Mamikos.com" />
	<meta property="og:type" content="website" />
	<meta name="og:site_name" content="Mamikos"/>
	<meta property="og:url" content="{{ $share_url }}" />
	<meta property="og:image" content="{{ $photo_url['large'] }}" />
	<meta property="fb:app_id" content="607562576051242"/>

	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:site" content="@mamikosapp">
	<meta name="twitter:title" content="{{ $meta_title }}">
	<meta name="twitter:description" content="Sewa {{ $meta_title }} murah. Dengan alamat di {{ $project['address'] }}. Sewa {{ $meta_title }} murah, harian, bulanan, tahunan. Klik di sini untuk info lengkapnya, hanya di Mamikos.com">
	<meta name="twitter:creator" content="@mamikosapp">
	<meta name="twitter:image" content="{{ $photo_url['large'] }}">
	<meta name="twitter:domain" content="mamikos.com">

	<meta name="p:domain_verify" content="1947d14ed84936368c9c2c229d535f39"/>
	<meta name="msvalidate.01" content="8F441EFF8C6571840616F325C34D7698" />
	
	@include('web.@meta.favicon')

	<!-- STYLE -->

	@include('web.part.external.global-head')
	<link href="{{ mix_url('dist/css/vendorsimple.css') }}" rel="stylesheet">
	<link href="{{ asset('css/detailproperty.css') }}" rel="stylesheet">

	<style>
	.navbarTop {
		display: -moz-flex;
		display: -ms-flex;
		display: -o-flex;
		display: -webkit-box;
		display: -ms-flexbox;
		display: flex;
		-ms-align-items: center;
		-webkit-box-align: center;
		-ms-flex-align: center;
		align-items: center;
		-webkit-box-pack: justify;
		-ms-flex-pack: justify;
		justify-content: space-between;
	}

	.navbar-margin {
		margin-top: 80px;
	}

	.inactive-title, .inactive-subtitle, .inactive-status {
		font-size: 20px;
		text-align: center;
		font-weight: bold;
	}
	.inactive-title {
		font-size: 30px;
	}
	.inactive-status {
		padding: 10px 20px;
		background-color: #EC4A0C;
		color: #fff;
		width: -webkit-fit-content;
		width: -moz-fit-content;
		width: fit-content;
		margin: auto;
		margin-top: 25px;
	}

	.suggestion-title {
		padding: 15px 20px;
		font-size: 18px;
		background-color: #eeefec;
		font-weight: 700;
		margin-top: 30px;
	}

	.suggestion-promotion {
		padding: 10px 15px;
		font-size: 16px;
		color: #1BAA56;
	}

	.room-list {
		overflow-y: hidden;
		overflow-x: auto;
		white-space: nowrap;
	}

	.is-other-wrap {
		white-space: normal;
	}

	@media (max-width: 767px) {
		.inactive-title {
			font-size: 20px;
		}
		.suggestion-title {
			font-size: 14px;
		}
	}

</style>

</head>
<body>

	<div class="col-xs-12 navbarTop" id="navbarTop" style="height: 54px;">
		<a href="/">
			<div style="padding-left: 20px;">
				<img src="/assets/logo/svg/logo_mamikos_white.svg" alt="logo_mamikos_white" style="height: 25px;" alt="header_mamikos">
			</div>
		</a>
		<a href="/cari" style="color: #fff;padding-right: 20px;">Cari Di Sini</a>
	</div>


	<div class="container navbar-margin">

		<h1 class="inactive-subtitle">Maaf, saat ini</h1>
		<h2 class="inactive-title">{{ $name_slug }}</h2>
		<h3 class="inactive-status" style="border-radius: 50px;">Sedang Tidak Aktif</h3>

		<div class="suggestion-title">
			<h4><b>Kunjungi apartemen lain terdekat berikut ini:</b></h4>
		</div>

			<div class="suggestion-container" id="suggestion">
				<div class="room-list" style="padding-top: 20px;" v-cloak>
					<a :href="room.share_url" target="_blank" rel="noopener" class="col-md-3 related-card track-promoted-kost" v-for="room in suggestion.promoted">
						<div class="related-box is-promoted" :title="room['room-title']" :style="{backgroundImage:'url('+room.photo_url.medium+')'}">
							<div class="related-content">
								<div class="related-content-data">
									<span class="related-content-price">@{{room.price_title_time}}</span>
									<span class="related-content-gender is-female" v-if="room.gender == 2">Putri</span>
									<span class="related-content-gender is-male" v-else-if="room.gender == 1">Putra</span>
									<span class="related-content-gender is-mix" v-else>Campur</span>
								</div>
								<div class="related-content-data">
									<p class="related-content-title">@{{room['room-title']}}</p>
								</div>
								<div class="related-content-data flex-center">
									<label class="related-content-premium" v-if="room.is_premium_owner">
										<img :src="icon.premium2" alt="premium2" v-cloak>
										<span>PREMIUM</span>
									</label>
									<label class="related-content-booking" v-if="room.is_booking">
										<img :src="icon.booking2" alt="booking2" v-cloak>
										<span>BISA BOOKING</span>
									</label>
								</div>
							</div>
						</div>
					</a>

					<a :href="room.share_url" target="_blank" rel="noopener" class="col-md-3 related-card track-no-promoted-kost" v-for="room in suggestion.normal">
						<div class="related-box is-normal" :title="room['room-title']" :style="{backgroundImage:'url('+room.photo_url.medium+')'}">
							<div class="related-content">
								<div class="related-content-data">
									<span class="related-content-price">@{{room.price_title_time}}</span>
									<span class="related-content-gender is-female" v-if="room.gender == 2">Putri</span>
									<span class="related-content-gender is-male" v-else-if="room.gender == 1">Putra</span>
									<span class="related-content-gender is-mix" v-else>Campur</span>
								</div>
								<div class="related-content-data">
									<p class="related-content-title">@{{room['room-title']}}</p>
								</div>
								<div class="related-content-data flex-center">
									<label class="related-content-premium" v-if="room.is_premium_owner">
										<img :src="icon.premium2" alt="premium2" v-cloak>
										<span>PREMIUM</span>
									</label>
									<label class="related-content-booking" v-if="room.is_booking">
										<img :src="icon.booking2" alt="booking2" v-cloak>
										<span>BISA BOOKING</span>
									</label>
								</div>
							</div>
						</div>
					</a>

					<a href ="<?= $breadcrumbs[count($breadcrumbs)-2]["url"] ?>" target="_blank" class="col-md-3 related-card track-related-detail">
						<div class="related-box is-other is-other-wrap track-related-detail" alt="<?= $breadcrumbs[count($breadcrumbs)-2]["name"] ?> Mamikos.com" title="<?= $breadcrumbs[count($breadcrumbs)-2]["name"] ?> Mamikos.com">
							<span>Lihat Unit</span>
							<span>lainnya di</span>
							<span><b>{{ $breadcrumbs[count($breadcrumbs)-2]["name"] }}</b></span>
						</div>
					</a>
				</div>

			</div>
		</div>


		<script  src="{{ mix_url('dist/js/vendorsimple.js') }}"></script>
		@include ('web.part.script.bugsnagapikey')
		@include ('web.part.script.tokenerrorhandlescript')
		@include ('web.part.script.browseralertscript')

		<script>
			var icon = {
				promoted : '/assets/icons/promote_kost_icon_dekstop.png',
				photo360 : '/assets/icons/ic_vid360.png',
				video : '/assets/icons/ic_video.png',
				premium : '/assets/icons/ic_premium.png',
				premium2 : '/assets/icons/ic_premium2.png',
				booking : '/assets/icons/ic_booking_ijo.png',
				booking2 : '/assets/icons/ic_booking_ijo2.png'
			};

			var loading = {
				suggestion: true,
			};

			var suggestion = {
				promoted: [],
				normal: []
			};

			Vue.config.productionTip = false;

			var relatedSuggestion = new Vue({
				el: '#suggestion',
				data: {
					icon,
					loading,
					suggestion
				},
				mounted () {
					$.ajax({
						type : "GET",
						url : "/garuda/stories/{{$_id}}/suggestion",
						headers : {"Content-Type":"application/json", "X-GIT-Time": "1406090202", "Authorization": "GIT WEB:WEB"},
						crossDomain: true,
						dataType: "json",
						success : function (suggestionResponse) {
							loading.suggestion = false;
							if(suggestionResponse.normal_suggestions){
								suggestion.promoted = suggestionResponse.promoted_suggestions;
								if (suggestion.promoted !== null) {
									suggestion.normal = suggestionResponse.normal_suggestions.slice(0, 2);
								}
								else {
									suggestion.normal = suggestionResponse.normal_suggestions.slice(0, 3);
								}
							}
						}
					});
				}
			});
		</script>
	</body>

	</html>

