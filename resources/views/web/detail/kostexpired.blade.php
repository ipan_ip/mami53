<?php
    //$meta_title             = "Kost " . $area_city_keyword . " - " . $name_slug;
  $meta_title             =  $name_slug;
    $meta_description       = "Cari info kost $area_city_keyword $price_tag, Bebas, Pasutri seperti $room_title? Cukup ketik lokasi yang kamu mau, dan Filter! Dapatkan Kost dari aplikasi Mamikos, Download sekarang!";
    $meta_author            = "https://plus.google.com/u/1/+Mamikos/posts";

    $arrayGender            = ["campur","putra","putri"];
    $meta_gender            = $arrayGender[$gender];

?>
<!DOCTYPE html>
<html>
  <head>
    <!-- Meta
  ================================================== -->
  <base href="/">
  <title><?php echo $meta_title; ?> - MamiKos </title>

  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta name="application-name" content="Mamikos">
  <meta name="theme-color" content="#1BAA56">
  <link rel="manifest" href="/json/manifest.json">

  @include('web.@meta.link-prebrowsing')

  <meta name="this-id" content="{{$_id}}">
  <meta name="load-time" content="{{date('Y-m-d H:i:s', time())}}">

  @if ($is_indexed == 1 )
  <link rel="canonical" href="https://mamikos.com/room/<?php echo $slug;?>" />
  @endif
  <link rel="alternate" href="android-app://com.git.mami.kos/mamikos/room/<?php echo $slug;?>" />
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <meta name="description" content="Mau kost di <?php echo $meta_title ?>, <?php echo $area_city_keyword?>? Dapatkan Fasilitas Kamar Kost Berupa <?php

    $facroom_origin = array_pluck($fac_room_icon, 'name');
    $facroom = array();

      if((in_array('Kamar kosongan', $facroom_origin))){
        echo 'Kamar kosongan';
      }elseif ((in_array('TV', $facroom_origin)) || (in_array('AC', $facroom_origin)) || (in_array('Wifi - Internet', $facroom_origin)) || (in_array('Bed', $facroom_origin))){
        echo "";
      }

    if (in_array('Bed', $facroom_origin)) {
      array_push($facroom,'Bed');
    }
    if (in_array('TV', $facroom_origin)) {
      array_push($facroom,'TV');
    }
    if (in_array('AC', $facroom_origin)) {
      array_push($facroom,'AC');
    }
    if (in_array('Wifi - Internet', $facroom_origin)) {
      array_push($facroom,'Internet');
    }
    $arrlength = count($facroom);

    for($x = 0; $x < $arrlength; $x++) {
      if($x!=$arrlength-1){
        echo $facroom[$x];
        echo ", ";
      } else {
        echo $facroom[$x];
      }
    }?><?php

    $facbath_origin = array_pluck($fac_bath_icon, 'name');
    $facbath = array();
    if ((in_array('Kamar Mandi Dalam', $facbath_origin)) || (in_array('Kamar Mandi Luar', $facbath_origin))){
      echo ", ";
    }

    if (in_array('Kamar Mandi Dalam', $facbath_origin)) {
      array_push($facbath,'Kamar Mandi Dalam');
    }


    if (in_array('Kamar Mandi Luar', $facbath_origin)) {
      array_push($facbath,'Kamar Mandi Luar');
    }
    if (in_array('Air Panas', $facbath_origin)) {
      array_push($facbath,'Air Panas');
    }

    $arrlengthbath = count($facbath);

    for($x = 0; $x < $arrlengthbath; $x++) {
      if($x!=$arrlengthbath-1){
      echo $facbath[$x];
      echo ", ";
        }else{
        echo $facbath[$x];
        }

    }

    ?>. Simak infonya di sini.">
  <meta name="author" content="<?php echo $meta_author ?>">
  <meta name="keywords" content="Info kost <?php echo $area_city_keyword ?>, Cari Kost <?php echo $area_city_keyword ?>, Kost <?php echo $area_city_keyword ?>, Kost <?php echo $meta_gender ?> <?php echo $area_city_keyword ?>, Kost Murah <?php echo $area_city_keyword ?>">
  @if($_SERVER['HTTP_HOST'] != 'mamikos.com')
    <meta name="robots" content="noindex, nofollow">
  @else
    <meta name="robots" content="<?php echo $index_status; ?>">
  @endif

  <meta property="og:title" content="<?php echo $meta_title;?>" />
  <meta property="og:description" content="Mau kost di <?php echo $meta_title ?>, <?php echo $area_city_keyword?>? Dapatkan Fasilitas Kamar Kost Berupa <?php

    $facroom = array();
      if((in_array('Kamar kosongan', $facroom_origin))){
        echo 'Kamar kosongan';
      }elseif ((in_array('TV', $facroom_origin)) || (in_array('AC', $facroom_origin)) || (in_array('Wifi - Internet', $facroom_origin)) || (in_array('Bed', $facroom_origin))){
        echo "";
      }

    if (in_array('Bed', $facroom_origin)) {
      array_push($facroom,'Bed');
    }
    if (in_array('TV', $facroom_origin)) {
      array_push($facroom,'TV');
    }
    if (in_array('AC', $facroom_origin)) {
      array_push($facroom,'AC');
    }
    if (in_array('Wifi - Internet', $facroom_origin)) {
      array_push($facroom,'Internet');
    }
    $arrlength = count($facroom);

    for($x = 0; $x < $arrlength; $x++) {
      if($x!=$arrlength-1) {
        echo $facroom[$x];
        echo ", ";
      } else {
        echo $facroom[$x];
      }
    }?><?php
    $facbath = array();
    if ((in_array('Kamar Mandi Dalam', $facbath_origin)) || (in_array('Kamar Mandi Luar', $facbath_origin))){
      echo ", ";
    }

    if (in_array('Kamar Mandi Dalam', $facbath_origin)) {
      array_push($facbath,'Kamar Mandi Dalam');
    }

    if (in_array('Kamar Mandi Luar', $facbath_origin)) {
      array_push($facbath,'Kamar Mandi Luar');
    }
    if (in_array('Air Panas', $facbath_origin)) {
      array_push($facbath,'Air Panas');
    }

    $arrlengthbath = count($facbath);

    for($x = 0; $x < $arrlengthbath; $x++) {
      if($x!=$arrlengthbath-1){
      echo $facbath[$x];
      echo ", ";
        }else{
        echo $facbath[$x];
        }

    }

    ?>. Simak infonya di sini." />
  <meta property="og:type" content="website" />
  <meta name="og:site_name" content="Mamikos"/>
  <meta property="fb:app_id" content="607562576051242"/>
  <meta property="og:url" content="https://mamikos.com/room/<?php echo $slug;?>/" />
  <meta property="og:image" content="<?php echo $photo_url['large'];?>" />
  <meta name="shareaholic:image" content="https://<?php echo $photo_url['large'];?>" />
  
  @include('web.@meta.favicon')

  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:site" content="@mamikosapp">
  <meta name="twitter:title" content="<?php echo $meta_title;?>">
  <meta name="twitter:description" content="Mau kost di <?php echo $meta_title ?>, <?php echo $area_city_keyword?>? Dapatkan Fasilitas Kamar Kost Berupa <?php
    $facroom = array();
      if((in_array('Kamar kosongan', $facroom_origin))){
        echo 'Kamar kosongan';
      }elseif ((in_array('TV', $facroom_origin)) || (in_array('AC', $facroom_origin)) || (in_array('Wifi - Internet', $facroom_origin)) || (in_array('Bed', $facroom_origin))){
        echo "";
      }

    if (in_array('Bed', $facroom_origin)) {
      array_push($facroom,'Bed');
    }
    if (in_array('TV', $facroom_origin)) {
      array_push($facroom,'TV');
    }
    if (in_array('AC', $facroom_origin)) {
      array_push($facroom,'AC');
    }
    if (in_array('Wifi - Internet', $facroom_origin)) {
      array_push($facroom,'Internet');
    }
    $arrlength = count($facroom);

    for($x = 0; $x < $arrlength; $x++) {
      if($x!=$arrlength-1){
        echo $facroom[$x];
        echo ", ";
      } else {
        echo $facroom[$x];
      }

    }?><?php
    $facbath = array();
    if ((in_array('Kamar Mandi Dalam', $facbath_origin)) || (in_array('Kamar Mandi Luar', $facbath_origin))) {
      echo ", ";
    }

    if (in_array('Kamar Mandi Dalam', $facbath_origin)) {
      array_push($facbath,'Kamar Mandi Dalam');
    }

    if (in_array('Kamar Mandi Luar', $facbath_origin)) {
      array_push($facbath,'Kamar Mandi Luar');
    }
    if (in_array('Air Panas', $facbath_origin)) {
      array_push($facbath,'Air Panas');
    }

    $arrlengthbath = count($facbath);

    for($x = 0; $x < $arrlengthbath; $x++) {
      if($x!=$arrlengthbath-1) {
        echo $facbath[$x];
        echo ", ";
      } else {
        echo $facbath[$x];
      }
    }

    ?>. Simak infonya di sini.">
  <meta name="twitter:creator" content="@mamikosapp">
  <meta name="twitter:image" content="<?php echo $photo_url['large'];?>">
  <meta name="twitter:domain" content="mamikos.com">
  <meta name="p:domain_verify" content="1947d14ed84936368c9c2c229d535f39"/>
  <meta name="msvalidate.01" content="8F441EFF8C6571840616F325C34D7698" />

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries
  ================================================== -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
  <![endif]-->

  <!-- Style
  ==================================================== -->
  @include('web.part.external.global-head')
  <link href="{{ mix_url('dist/css/vendorsimple.css') }}" rel="stylesheet">
  <link href="{{ asset('css/detailproperty.css') }}" rel="stylesheet">

  <style>
    .navbarTop {
      display: -moz-flex;
      display: -ms-flex;
      display: -o-flex;
      display: -webkit-box;
      display: -ms-flexbox;
      display: flex;
      -ms-align-items: center;
      -webkit-box-align: center;
      -ms-flex-align: center;
      align-items: center;
      -webkit-box-pack: justify;
      -ms-flex-pack: justify;
      justify-content: space-between;
    }

    .title-status-exp {
      width: -webkit-fit-content;
      width: -moz-fit-content;
      width: fit-content;
      margin: auto;
    }

    @media (max-width: 767px) {
      .list-group-item {
        padding: 0;
      }
    }
  </style>

  <script type="application/ld+json">
    {
      "@context": "http://schema.org",
      "@type": "BreadcrumbList",
      "itemListElement": [

      <?php foreach($breadcrumbs as $key => $breadcrumb):  ?>
        {
          "@type": "ListItem",
          "position": <?php echo $key + 1 ?>,
          "item": {
            "@id": "<?php echo $breadcrumb['url'] ?>",
            "name": "<?php echo $breadcrumb['name'] ?>"
          }
        }
        <?php echo ($key == sizeof($breadcrumbs) - 1 ) ? '' : ',' ?>
      <?php endforeach; ?>

      ]
    }
  </script>
  <script>
    var slug = {
      str: '{{$slug}}',
      encode: '{{urlencode($slug)}}'
    };
  </script>

</head>

<body>
  @include('web.part.external.gtm-body')

  @if ($expired_phone == true)
    <div class="col-xs-12 navbarTop" id="navbarTop" style="height: 54px;">
      <a href="/">
        <div style="padding-left: 20px;">
          <img src="/assets/logo/svg/logo_mamikos_white.svg" alt="logo_mamikos_white" style="height: 25px;" alt="header_mamikos">
        </div>
      </a>
      <a href="/cari" style="color: #fff;padding-right: 20px;">Cari Di Sini</a>
    </div>
    <div class="expired-page">
      <ul class="list-group">
        <div class="container-fluid">
          <div class="col-xs-12 container-custom" style="margin-top:5px;border:0px;">
            <li class="list-group-item noborder">
              <div class="row">
                <div class="col-md-12 col-xs-12 carousel-pad visible-xs" style="margin-top:30px;">
                </div>
              </div>
            </li>
            <!-- INFORMASI KAMAR -->
            <li class="list-group-item noborder visible-xs">
              <div class="row">
                <div class="col-md-12 col-xs-12 box-exp">
                  <h1 class="title-sorry-exp">
                    Maaf, saat ini
                  </h1>
                  <h2 class="title-kos title-room-exp">
                    {{ $room_title }}
                  </h2>
                  <br>
                  <h3 class="title-status-exp" style="border-radius: 50px;background: #EC4A0C;">
                    Sedang Tidak Aktif
                  </h3>
                </div>
              </div>
            </li>
            <li class="list-group-item noborder hidden-xs">
              <div class="row">
                <div class="col-md-12 col-xs-12 box-exp">
                  <h1 class="title-sorry-exp">
                    Maaf, saat ini
                  </h1>
                  <h2 class="title-kos title-room-exp-lg">
                    {{ $room_title }}
                  </h2>
                  <br>
                  <h3 class="title-status-exp" style="border-radius: 50px;background: #EC4A0C;">
                    Sedang Tidak Aktif
                  </h3>
                </div>
              </div>
            </li>
          </div>
        </div>
      </ul>
    </div>
    <!-- Related Kost -->
    <div id="suggestion" class="container-fluid room-related-container">
      <div class="container stretch">
        <div class="col-md-12 stretch related-body">
          <div class="col-md-12 related-header">
            <h4><b>Kunjungi kost lain terdekat berikut ini:</b></h4>
          </div>
          <!-- Loading Related Kost -->
          <div class="related-result" v-if="loading.suggestion">
            <div class="clearfix"></div>
            <div class="related-container-fixed">
              <div class="related-container-grid">
                <div class="col-md-12 related-promoted-icon">
                </div>
                <div class="col-md-3 related-card" v-for="n in 3">
                  <div class="related-box is-loading">
                    <div class="related-content">
                    </div>
                  </div>
                </div>
                <div class="col-md-3 related-card">
                  <div class="related-box is-other">
                  </div>
                </div>
              </div>
            </div>
          </div> <!-- End Related Kost -->
          <!-- Show Related Kost -->
          <div class="related-result" v-else v-cloak>
            <div class="clearfix"></div>
            <div class="related-container-fixed">
              <div class="related-container-grid" style="padding-top: 20px;">
                <a :href="room.share_url" target="_blank" rel="noopener" class="col-md-3 related-card track-promoted-kost" v-for="room in suggestion.promoted">
                  <div class="related-box is-promoted" :title="room['room-title']" :style="{backgroundImage:'url('+room.photo_url.medium+')'}">
                    <div class="related-content">
                      <div class="related-content-data">
                        <span class="related-content-price">@{{room.price_title_time}}</span>
                        <span class="related-content-gender is-female" v-if="room.gender == 2">Putri</span>
                        <span class="related-content-gender is-male" v-else-if="room.gender == 1">Putra</span>
                        <span class="related-content-gender is-mix" v-else>Campur</span>
                      </div>
                      <div class="related-content-data">
                        <p class="related-content-title">@{{room['room-title']}}</p>
                      </div>
                      <div class="related-content-data flex-center">
                        <label class="related-content-premium" v-if="room.is_premium_owner">
                          <img :src="icon.premium2" alt="premium2" v-cloak>
                          <span>PREMIUM</span>
                        </label>
                        <label class="related-content-booking" v-if="room.is_booking">
                          <img :src="icon.booking2" alt="booking2" v-cloak>
                          <span>BISA BOOKING</span>
                        </label>
                      </div>
                    </div>
                  </div>
                </a>
                <a :href="room.share_url" target="_blank" rel="noopener" class="col-md-3 related-card track-no-promoted-kost" v-for="room in suggestion.normal">
                  <div class="related-box is-normal" :title="room['room-title']" :style="{backgroundImage:'url('+room.photo_url.medium+')'}">
                    <div class="related-content">
                      <div class="related-content-data">
                        <span class="related-content-price">@{{room.price_title_time}}</span>
                        <span class="related-content-gender is-female" v-if="room.gender == 2">Putri</span>
                        <span class="related-content-gender is-male" v-else-if="room.gender == 1">Putra</span>
                        <span class="related-content-gender is-mix" v-else>Campur</span>
                      </div>
                      <div class="related-content-data">
                        <p class="related-content-title">@{{room['room-title']}}</p>
                      </div>
                      <div class="related-content-data flex-center">
                        <label class="related-content-premium" v-if="room.is_premium_owner">
                          <img :src="icon.premium2" alt="premium2" v-cloak>
                          <span>PREMIUM</span>
                        </label>
                        <label class="related-content-booking" v-if="room.is_booking">
                          <img :src="icon.booking2" alt="booking2" v-cloak>
                          <span>BISA BOOKING</span>
                        </label>
                      </div>
                    </div>
                  </div>
                </a>
                <a href ="<?=isset($urllanding)?$urllanding:"" ?>" target="_blank" rel="noopener" class="col-md-3 related-card track-related-detail">
                  <div class="related-box is-other" alt="<?=isset($namalanding)?$namalanding:"" ?>Mamikos.com" title="<?=isset($namalanding)?$namalanding:"" ?>Mamikos.com">
                    <span>Lihat Kost</span>
                    <span>di Sekitar</span>
                    <span><b>{{$namalanding}}</b></span>
                    <span>Lainnya</span>
                  </div>
                </a>
              </div>
            </div>
          </div> <!-- End Show Related Kost -->
        </div>
        <div class="clearfix"></div>
      </div>
    </div><!-- End Related Kost -->
    <div class="related-result" v-else v-cloak>
      <div class="clearfix"></div>
      <div class="related-container-fixed">
        <div class="related-container-grid">
          <div class="col-md-12 related-promoted-icon">
            <span v-if="suggestion.promoted !== null">
              Sponsored Kost
            </span>
          </div>
          <a :href="room.share_url" target="_blank" rel="noopener" class="col-md-3 related-card track-promoted-kost" v-for="room in suggestion.promoted">
            <div class="related-box is-promoted" :title="room['room-title']" :style="{backgroundImage:'url('+room.photo_url.medium+')'}">
              <div class="related-content">
                <div class="related-content-data">
                  <span class="related-content-price">@{{room.price_title_time}}</span>
                  <span class="related-content-gender is-female" v-if="room.gender == 2">Putri</span>
                  <span class="related-content-gender is-male" v-else-if="room.gender == 1">Putra</span>
                  <span class="related-content-gender is-mix" v-else>Campur</span>
                </div>
                <div class="related-content-data">
                  <p class="related-content-title">@{{room['room-title']}}</p>
                </div>
                <div class="related-content-data flex-center">
                  <label class="related-content-premium" v-if="room.is_premium_owner">
                    <img :src="icon.premium2" alt="premium2" v-cloak>
                    <span>PREMIUM</span>
                  </label>
                  <label class="related-content-booking" v-if="room.is_booking">
                    <img :src="icon.booking2" alt="booking2" v-cloak>
                    <span>BISA BOOKING</span>
                  </label>
                </div>
              </div>
            </div>
          </a>
          <a :href="room.share_url" target="_blank" rel="noopener" class="col-md-3 related-card track-no-promoted-kost" v-for="room in suggestion.normal">
            <div class="related-box is-normal" :title="room['room-title']" :style="{backgroundImage:'url('+room.photo_url.medium+')'}">
              <div class="related-content">
                <div class="related-content-data">
                  <span class="related-content-price">@{{room.price_title_time}}</span>
                  <span class="related-content-gender is-female" v-if="room.gender == 2">Putri</span>
                  <span class="related-content-gender is-male" v-else-if="room.gender == 1">Putra</span>
                  <span class="related-content-gender is-mix" v-else>Campur</span>
                </div>
                <div class="related-content-data">
                  <p class="related-content-title">@{{room['room-title']}}</p>
                </div>
                <div class="related-content-data flex-center">
                  <label class="related-content-premium" v-if="room.is_premium_owner">
                    <img :src="icon.premium2" alt="premium2" v-cloak>
                    <span>PREMIUM</span>
                  </label>
                  <label class="related-content-booking" v-if="room.is_booking">
                    <img :src="icon.booking2" alt="booking2" v-cloak>
                    <span>BISA BOOKING</span>
                  </label>
                </div>
              </div>
            </div>
          </a>
          <a href ="<?=isset($urllanding)?$urllanding:"" ?>" target="_blank" rel="noopener" class="col-md-3 related-card track-related-detail">
            <div class="related-box is-other" alt="<?=isset($namalanding)?$namalanding:"" ?>Mamikos.com" title="<?=isset($namalanding)?$namalanding:"" ?>Mamikos.com">
              <span>Lihat Kost</span>
              <span>di Sekitar</span>
              <span><b>{{$namalanding}}</b></span>
              <span>Lainnya</span>
            </div>
          </a>
        </div>
      </div>
    </div> <!-- End Show Related Kost -->
  @endif

  <!-- Page if expired phone == false, show information-->

  <!-- Javascript
    ================================================== -->
  <script  src="{{ mix_url('dist/js/vendorsimple.js') }}"></script>
  @include ('web.part.script.bugsnagapikey')
  @include ('web.part.script.tokenerrorhandlescript')
  @include ('web.part.script.browseralertscript')

  <script>
    //API RELATED SUGGESTION KOST
    var icon = {
        promoted : '/assets/icons/promote_kost_icon_dekstop.png',
        photo360 : '/assets/icons/ic_vid360.png',
        video : '/assets/icons/ic_video.png',
        premium : '/assets/icons/ic_premium.png',
        premium2 : '/assets/icons/ic_premium2.png',
        booking : '/assets/icons/ic_booking_ijo.png',
        booking2 : '/assets/icons/ic_booking_ijo2.png'
    };

    var loading = {
      suggestion: true,
    };

    var suggestion = {
        promoted: [],
        normal: []
    };

    Vue.config.productionTip = false;

    var relatedSuggestion = new Vue({
      el: '#suggestion',
      data: {
        icon,
        loading,
        suggestion
      },
      mounted () {
        $.ajax({
          type : "GET",
          url : "/garuda/stories/{{$_id}}/suggestion",
          headers : {"Content-Type":"application/json", "X-GIT-Time": "1406090202", "Authorization": "GIT WEB:WEB"},
          crossDomain: true,
          dataType: "json",
          success : function (suggestionResponse) {
              loading.suggestion = false;
              suggestion.promoted = suggestionResponse.promoted_suggestions;
              if(suggestionResponse.normal_suggestions){
                if (suggestion.promoted !== null) {
                    suggestion.normal = suggestionResponse.normal_suggestions.slice(0, 2);
                }
                else {
                    suggestion.normal = suggestionResponse.normal_suggestions.slice(0, 3);
                }
              }
          }
        });
      }
    });
  </script>

  </body>
</html>
