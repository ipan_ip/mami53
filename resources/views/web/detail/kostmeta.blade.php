  <?php
    
    $meta_title             =  $name_slug;
    $meta_description       = "Cari info kost $area_city_keyword $price_tag, Bebas, Pasutri seperti $room_title? Cukup ketik lokasi yang kamu mau, dan Filter! Dapatkan Kost dari aplikasi Mamikos, Download sekarang!";
    $meta_author            = "https://plus.google.com/u/1/+Mamikos/posts";

    $arrayGender            = ["campur","putra","putri"];
    $meta_gender            = $arrayGender[$gender];

  ?>
    <!-- Meta
  ================================================== -->
  <base href="/">
  <title><?php echo $meta_title; ?> - Mamikos </title>
  
  <meta name="csrf-token" content="{{ csrf_token() }}">

<meta name="application-name" content="Mamikos">
<meta name="theme-color" content="#1BAA56">
<link rel="manifest" href="/json/manifest.json">

@include('web.@meta.link-prebrowsing')

  <meta name="this-id" content="{{$_id}}">
  <meta name="load-time" content="{{date('Y-m-d H:i:s', time())}}">

  @if ($is_indexed == 1 )
  <link rel="canonical" href="https://mamikos.com/room/<?php echo $slug;?>" />
  @endif
  <link rel="alternate" href="android-app://com.git.mami.kos/mamikos/room/<?php echo $slug;?>" />
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <meta name="description" content="Mau kost di <?php echo $meta_title ?>, <?php echo $area_city_keyword?>? Dapatkan Fasilitas Kamar Kost Berupa <?php

     $facroom_origin = array_pluck($fac_room_icon, 'name');
     $facroom = array();

      if((in_array('Kamar kosongan', $facroom_origin))){
        echo 'Kamar kosongan';
      }elseif ((in_array('TV', $facroom_origin)) || (in_array('AC', $facroom_origin)) || (in_array('Wifi - Internet', $facroom_origin)) || (in_array('Bed', $facroom_origin))){
        echo "";
      }

    if (in_array('Bed', $facroom_origin))
       {
       array_push($facroom,'Bed');
       }
    if (in_array('TV', $facroom_origin))
       {
       array_push($facroom,'TV');
       }
    if (in_array('AC', $facroom_origin))
       {
       array_push($facroom,'AC');
       }
    if (in_array('Wifi - Internet', $facroom_origin))

       {
       array_push($facroom,'Internet');
       }
    $arrlength = count($facroom);

    for($x = 0; $x < $arrlength; $x++) {
      if($x!=$arrlength-1){
      echo $facroom[$x];
      echo ", ";
        }else{
        echo $facroom[$x];
        }

    }?><?php

    $facbath_origin = array_pluck($fac_bath_icon, 'name');
    $facbath = array();
    if ((in_array('Kamar Mandi Dalam', $facbath_origin)) || (in_array('Kamar Mandi Luar', $facbath_origin))){
      echo ", ";
    }

    if (in_array('Kamar Mandi Dalam', $facbath_origin))

       {
       array_push($facbath,'Kamar Mandi Dalam');
       }


    if (in_array('Kamar Mandi Luar', $facbath_origin))
       {
       array_push($facbath,'Kamar Mandi Luar');
       }
    if (in_array('Air Panas', $facbath_origin))

       {
       array_push($facbath,'Air Panas');
       }

    $arrlengthbath = count($facbath);

    for($x = 0; $x < $arrlengthbath; $x++) {
      if($x!=$arrlengthbath-1){
      echo $facbath[$x];
      echo ", ";
        }else{
        echo $facbath[$x];
        }

    }

    ?>. Simak infonya di sini.">
  <meta name="author" content="<?php echo $meta_author ?>">
  <meta name="keywords" content="Info kost <?php echo $area_city_keyword ?>, Cari Kost <?php echo $area_city_keyword ?>, Kost <?php echo $area_city_keyword ?>, Kost <?php echo $meta_gender ?> <?php echo $area_city_keyword ?>, Kost Murah <?php echo $area_city_keyword ?>">
  
  <meta property="og:title" content="<?php echo $meta_title;?>" />
  <meta property="og:description" content="Mau kost di <?php echo $meta_title ?>, <?php echo $area_city_keyword?>? Dapatkan Fasilitas Kamar Kost Berupa <?php

     $facroom = array();
      if((in_array('Kamar kosongan', $facroom_origin))){
        echo 'Kamar kosongan';
      }elseif ((in_array('TV', $facroom_origin)) || (in_array('AC', $facroom_origin)) || (in_array('Wifi - Internet', $facroom_origin)) || (in_array('Bed', $facroom_origin))){
        echo "";
      }

    if (in_array('Bed', $facroom_origin))
       {
       array_push($facroom,'Bed');
       }
    if (in_array('TV', $facroom_origin))
       {
       array_push($facroom,'TV');
       }
    if (in_array('AC', $facroom_origin))
       {
       array_push($facroom,'AC');
       }
    if (in_array('Wifi - Internet', $facroom_origin))

       {
       array_push($facroom,'Internet');
       }
    $arrlength = count($facroom);

    for($x = 0; $x < $arrlength; $x++) {
      if($x!=$arrlength-1){
      echo $facroom[$x];
      echo ", ";
        }else{
        echo $facroom[$x];
        }

    }?><?php
    $facbath = array();
    if ((in_array('Kamar Mandi Dalam', $facbath_origin)) || (in_array('Kamar Mandi Luar', $facbath_origin))){
      echo ", ";
    }

    if (in_array('Kamar Mandi Dalam', $facbath_origin))

       {
       array_push($facbath,'Kamar Mandi Dalam');
       }

    if (in_array('Kamar Mandi Luar', $facbath_origin))
       {
       array_push($facbath,'Kamar Mandi Luar');
       }
    if (in_array('Air Panas', $facbath_origin))

       {
       array_push($facbath,'Air Panas');
       }

    $arrlengthbath = count($facbath);

    for($x = 0; $x < $arrlengthbath; $x++) {
      if($x!=$arrlengthbath-1){
      echo $facbath[$x];
      echo ", ";
        }else{
        echo $facbath[$x];
        }

    }

    ?>. Simak infonya di sini." />
  <meta property="og:type" content="website" />
  <meta name="og:site_name" content="Mamikos"/>
  <meta property="fb:app_id" content="607562576051242"/>
  <meta property="og:url" content="https://mamikos.com/room/<?php echo $slug;?>/" />
  <meta property="og:image" content="<?php echo $photo_url['medium'];?>" />
  
  @include('web.@meta.favicon')

  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:site" content="@mamikosapp">
  <meta name="twitter:title" content="<?php echo $meta_title;?>">
  <meta name="twitter:description" content="Mau kost di <?php echo $meta_title ?>, <?php echo $area_city_keyword?>? Dapatkan Fasilitas Kamar Kost Berupa <?php
    $facroom = array();
      if((in_array('Kamar kosongan', $facroom_origin))){
        echo 'Kamar kosongan';
      }elseif ((in_array('TV', $facroom_origin)) || (in_array('AC', $facroom_origin)) || (in_array('Wifi - Internet', $facroom_origin)) || (in_array('Bed', $facroom_origin))){
        echo "";
      }

    if (in_array('Bed', $facroom_origin))
       {
       array_push($facroom,'Bed');
       }
    if (in_array('TV', $facroom_origin))
       {
       array_push($facroom,'TV');
       }
    if (in_array('AC', $facroom_origin))
       {
       array_push($facroom,'AC');
       }
    if (in_array('Wifi - Internet', $facroom_origin))

       {
       array_push($facroom,'Internet');
       }
    $arrlength = count($facroom);

    for($x = 0; $x < $arrlength; $x++) {
      if($x!=$arrlength-1){
      echo $facroom[$x];
      echo ", ";
        }else{
        echo $facroom[$x];
        }

    }?><?php
    $facbath = array();
    if ((in_array('Kamar Mandi Dalam', $facbath_origin)) || (in_array('Kamar Mandi Luar', $facbath_origin))){
      echo ", ";
    }

    if (in_array('Kamar Mandi Dalam', $facbath_origin))

       {
       array_push($facbath,'Kamar Mandi Dalam');
       }

    if (in_array('Kamar Mandi Luar', $facbath_origin))
       {
       array_push($facbath,'Kamar Mandi Luar');
       }
    if (in_array('Air Panas', $facbath_origin))

       {
       array_push($facbath,'Air Panas');
       }

    $arrlengthbath = count($facbath);

    for($x = 0; $x < $arrlengthbath; $x++) {
      if($x!=$arrlengthbath-1){
      echo $facbath[$x];
      echo ", ";
        }else{
        echo $facbath[$x];
        }

    }

    ?>. Simak infonya di sini.">
  <meta name="twitter:creator" content="@mamikosapp">
  <meta name="twitter:image" content="<?php echo $photo_url['medium'];?>">
  <meta name="twitter:domain" content="mamikos.com">
  <meta name="p:domain_verify" content="1947d14ed84936368c9c2c229d535f39"/>
  <meta name="msvalidate.01" content="8F441EFF8C6571840616F325C34D7698" />