<!-- Harga dan Pembayaran (Mobile) -->
<div class="col-xs-12 display-room-content hidden-lg hidden-md">
  <!-- if(is_null(is_updated)) -->
      @if($not_updated == true)
        @if(Auth::check())
          @if(Auth::user()->is_owner == 'false')
          <div class="display-room is-not-updated clickable" data-toggle="modal" data-target="#chatModal">
            <span class="display-title orange">Tanya Kesediaan Kamar </span>
            <i class="fa fa-pencil update-room" data-toggle="tooltip" title="Edit ketersediaan kamar" v-if="updateRoom.code" v-cloak  @click="openUpdateRoom"></i>
          </div>
          @else
          <div class="display-room is-not-updated">
            <span class="display-title orange">Tanya Kesediaan Kamar </span>
            <i class="fa fa-pencil update-room" data-toggle="tooltip" title="Edit ketersediaan kamar" v-if="updateRoom.code" v-cloak  @click="openUpdateRoom"></i>
          </div>
          @endif
        @else
          <div class="display-room is-not-updated clickable" data-toggle="modal" data-target="#login" data-modal-text="untuk mengirim pesan">
            <span class="display-title orange">Tanya Kesediaan Kamar </span>
            <i class="fa fa-pencil update-room" data-toggle="tooltip" title="Edit ketersediaan kamar" v-if="updateRoom.code" v-cloak  @click="openUpdateRoom"></i>
          </div>
        @endif

      @elseif($available_room == 1)
      <div class="display-room is-one">
        <span class="display-title">TINGGAL 1 KAMAR</span>
        <i class="fa fa-pencil update-room" data-toggle="tooltip" title="Edit ketersediaan kamar" v-if="updateRoom.code" v-cloak  @click="openUpdateRoom"></i>
      </div>
      @elseif($available_room > 1)
      <div class="display-room is-more">
        <span class="display-title">ADA {{$available_room}} KAMAR </span>
        <i class="fa fa-pencil update-room" data-toggle="tooltip" title="Edit ketersediaan kamar" v-if="updateRoom.code" v-cloak  @click="openUpdateRoom"></i>
      </div>
      @else
      <div class="display-room is-full">
        <span class="display-title">PENUH </span>
        <i class="fa fa-pencil update-room" data-toggle="tooltip" title="Edit ketersediaan kamar" v-if="updateRoom.code" v-cloak  @click="openUpdateRoom"></i>
      </div>
      @endif

  @if(!isset($apartment_project_id))
  <div class="col-xs-12 stretch display-room-price">
    @if($price_daily !== 0 && !is_null ($price_daily))
    <div class="col-xs-8 price-data">
      Rp {{number_format($price_daily)}}
    </div>
    <div class="col-xs-4 price-term">
      / hari
    </div>
    @endif
    @if($price_weekly !== 0 && !is_null ($price_weekly))
    <div class="col-xs-8 price-data">
      Rp {{number_format($price_weekly)}}
    </div>
    <div class="col-xs-4 price-term">
      / minggu
    </div>
    @endif
    @if($price_monthly !== 0 && !is_null ($price_monthly))
    <div class="col-xs-8 price-data">
      Rp {{number_format($price_monthly)}}
    </div>
    <div class="col-xs-4 price-term">
      / bulan
    </div>
    @endif
    @if($price_yearly !== 0 && !is_null ($price_yearly))
    <div class="col-xs-8 price-data">
      Rp {{number_format($price_yearly)}}
    </div>
    <div class="col-xs-4 price-term">
      / tahun
    </div>
    @endif
    <div class="clearfix"></div> 
    
    @foreach ($fac_price_icon as $key => $fac_price_icon)
    <div class="col-lg-8 col-md-12 price-electricity">
      <div class="price-electricity-icon-container">
        <img src="{{$fac_price_icon['small_photo_url']}}" class="price-electricity-icon" alt="icon_electricity">
      </div>
      <div class="price-electricity-label">
        {{$fac_price_icon['name']}}
      </div> 
    </div>
    @endforeach
    
    <div class="col-xs-12 price-payment">
      @if(!is_null ($fac_keyword[0]))
      <span>Pembayaran {{ $fac_keyword[0] }}</span>
      @else
      <span>Tidak ada ketentuan min. pembayaran</span>
      @endif
    </div>

    @if(mt_rand(1, 100) < 200)

      @if(Auth::check())
        @if(Auth::user()->is_owner == 'false')
          <div class="col-md-12 price-payment ask-phone-number">
            <span>Nomor Telepon : 
              @if(Auth::user()->phone_number == null)
              <a 
              tabindex="0" 
              role="button" 
              class="text-mamiorange link-mamiorange"
              data-toggle="popover" 
              data-placement="bottom" 
              data-html="true"
              data-template='<div class="popover" role="tooltip"><div class="arrow"></div><div class="popover-title" style="height: 50px;"></div><div class="popover-content"></div></div>'
              title="<div class='popover-title-text'>Agar pemilik bisa menghubungimu, tulis nomor hp mu disini</div><a role='button' onclick='togglePopover()' class='popover-title-close'> &times; </a>" 
              data-content="
                <input type='number' id='phoneNumberPopover'  pattern='.{10,}' required>
                <button 
                  id='popoverButton'
                  type='button' 
                  class='btn btn-mamiorange'
                  onclick='sendPopoverChat()'
                >
                  Kirim
                </button>"
              >
                Lihat Nomor Telepon
              </a>
              @else
              <a class="text-mamiorange link-mamiorange" role="button" data-toggle="modal" data-target="#chatModal">
                Lihat Nomor Telepon
              </a>
              @endif
            </span>
          </div>
        @endif
      @else
        <div class="col-md-12 price-payment ask-phone-number">
          <span>Nomor Telepon : 
            <a class="text-mamiorange link-mamiorange" role="button" tabindex="0" data-toggle="modal" data-target="#login">
              Lihat Nomor Telepon
            </a>
          </span>
        </div>
      @endif

    @endif 
    
  </div>


  @else
  <div class="col-xs-12 stretch display-room-price">
    @if($price_daily !== 0 && !is_null ($price_daily))
    <div class="col-xs-8 price-data">
      Rp {{number_format($price_daily)}}
    </div>
    <div class="col-xs-4 price-term">
      / hari
    </div>
    @endif
    @if($price_weekly !== 0 && !is_null ($price_weekly))
    <div class="col-xs-8 price-data">
      Rp {{number_format($price_weekly)}}
    </div>
    <div class="col-xs-4 price-term">
      / minggu
    </div>
    @endif
    @if($price_monthly !== 0 && !is_null ($price_monthly))
    <div class="col-xs-8 price-data">
      Rp {{number_format($price_monthly)}}
    </div>
    <div class="col-xs-4 price-term">
      / bulan
    </div>
    @endif
    @if($price_yearly !== 0 && !is_null ($price_yearly))
    <div class="col-xs-8 price-data">
      Rp {{number_format($price_yearly)}}
    </div>
    <div class="col-xs-4 price-term">
      / tahun
    </div>
    @endif
    <div class="clearfix"></div>

    @foreach ($price_components as $key => $price_component)
    <div>
      <div class="col-sm-8 col-xs-12 price-term">
        {{$price_component['label']}} :
      </div>
      <div class="col-sm-4 col-xs-12 price-data is-extra">
        {{$price_component['price_reguler_formatted']}}
      </div>
    </div>
    @endforeach
    <div class="col-xs-12 price-payment">
      @if(!is_null ($fac_keyword[0]))
      <span>Pembayaran {{ $fac_keyword[0] }}</span>
      @else
      <span>Tidak ada ketentuan min. pembayaran</span>
      @endif
    </div>

    @if(mt_rand(1, 100) < 200)

      @if(Auth::check())
        @if(Auth::user()->is_owner == 'false')
          <div class="col-md-12 price-payment ask-phone-number">
            <span>Nomor Telepon : 
              @if(Auth::user()->phone_number == null)
              <a 
              tabindex="0" 
              role="button" 
              class="text-mamiorange link-mamiorange"
              data-toggle="popover" 
              data-placement="bottom" 
              data-html="true"
              data-template='<div class="popover" role="tooltip"><div class="arrow"></div><div class="popover-title" style="height: 50px;"></div><div class="popover-content"></div></div>'
              title="<div class='popover-title-text'>Agar pemilik bisa menghubungimu, tulis nomor hp mu disini</div><a role='button' onclick='togglePopover()' class='popover-title-close'> &times; </a>" 
              data-content="
                <input type='number' id='phoneNumberPopover'  pattern='.{10,}' required>
                <button 
                  id='popoverButton'
                  type='button' 
                  class='btn btn-mamiorange'
                  onclick='sendPopoverChat()'
                >
                  Kirim
                </button>"
              >
                Lihat Nomor Telepon
              </a>
              @else
              <a class="text-mamiorange link-mamiorange" role="button" data-toggle="modal" data-target="#chatModal">
                Lihat Nomor Telepon
              </a>
              @endif
            </span>
          </div>
        @endif
      @else
        <div class="col-md-12 price-payment ask-phone-number">
          <span>Nomor Telepon : 
            <a class="text-mamiorange link-mamiorange" role="button" tabindex="0" data-toggle="modal" data-target="#login">
              Lihat Nomor Telepon
            </a>
          </span>
        </div>
      @endif

    @endif


  </div>
  @endif

  <div class="col-xs-12 stretch display-room-latest">
    <span>Update terakhir pada</span>
    <br>
    <span class="text-mamigreen">{{$updated_at}}</span>
    <br>
    <span class="small"><span class="text-danger">*</span>Data bisa berubah sewaktu-waktu</span>
  </div>
</div>
<!-- End Harga dan Pembayaran (Mobile) -->
