<footer class="container-fluid stretch footer-action-container">
  <div class="footer-action-body">
    @if(Auth::check())
      @if(Auth::user()->is_owner == 'false')
        @if($is_booking)   
          <a role="button" tabindex="0" class="col-xs-6 btn-mamiorange footer-action footer-right-separator track-booking" @click="placeBooking">
            <img class="track-booking" src="/assets/icons/ic_booking_white.png" alt="ic_booking_white">
            <span class="track-booking">BOOKING</span>
          </a>
          <a role="button" tabindex="0" class="col-xs-6 btn-mamiorange footer-action footer-left-separator track-message {{ $msg_tracking }}" data-toggle="modal" data-target="#chatModal">
            <!-- <img class="track-message {{ $msg_tracking }}" src="/assets/icons/ic_kirim_pesan_white.png" alt="ic_kirim_pesan_white"> -->
            @if(!isset($apartment_project_id))
            <span class="track-message {{ $msg_tracking }}"><h2><strong>HUBUNGI KOST</strong></h2></span>
            @else
            <span class="track-message {{ $msg_tracking }}"><h2><strong>HUBUNGI PENGELOLA</strong></h2></span>
            @endif
          </a>
        @else
          @if(isset($checkin_attempt) && $checkin_attempt)
            <a role="button" tabindex="0" class="col-xs-12 btn-mamigreen footer-action footer-left-separator track-checkin" data-toggle="modal" data-target="#checkinModal">
              <span class="track-message {{ $msg_tracking }}">TANDAI KOS SAYA</span>
            </a>
          @else
            <a role="button" tabindex="0" class="col-xs-12 btn-mamiorange footer-action footer-left-separator track-message {{ $msg_tracking }}" data-toggle="modal" data-target="#chatModal">
              <!-- <img class="track-message {{ $msg_tracking }}" src="/assets/icons/ic_kirim_pesan_white.png" alt="ic_kirim_pesan_white"> -->
              @if(!isset($apartment_project_id))
              <span class="track-message {{ $msg_tracking }}"><h2><strong>HUBUNGI KOST</strong></h2></span>
              @else
              <span class="track-message {{ $msg_tracking }}"><h2><strong>HUBUNGI PENGELOLA</strong></h2></span>
              @endif
            </a>
          @endif
        @endif
      @else
      <a role="button" tabindex="0" class="col-xs-12 btn-mamiorange footer-action track-owner-claim" data-toggle="modal" data-target="#claimModal">
        <span class="track-owner-claim">KLAIM {{ strtoupper($property_type) }} INI</span>
      </a>
      @endif 
    @else
      @if($is_booking)   
        <a role="button" tabindex="0" class="col-xs-6 btn-mamiorange footer-action footer-right-separator track-booking" data-toggle="modal" data-target="#login" data-modal-text="untuk membooking kos ini">
          <img class="track-booking" src="/assets/icons/ic_booking_white.png" alt="ic_booking_white">
          <span class="track-booking">BOOKING</span>
        </a>
        <a role="button" tabindex="0" class="col-xs-6 btn-mamiorange footer-action footer-left-separator track-message {{ $msg_tracking }}" data-toggle="modal" data-target="#login" data-modal-text="untuk mengirim pesan">
          <!-- <img class="track-message {{ $msg_tracking }}" src="/assets/icons/ic_kirim_pesan_white.png" alt="ic_kirim_pesan_white"> -->

          @if(!isset($apartment_project_id))
          <span class="track-message {{ $msg_tracking }}" data-modal-text="untuk mengirim pesan"><h2><strong>HUBUNGI KOST</strong></h2></span>
          @else
          <span class="track-message {{ $msg_tracking }}" data-modal-text="untuk mengirim pesan"><h2><strong>HUBUNGI PENGELOLA</strong></h2></span>
          @endif
        </a>
      @else
        <a role="button" tabindex="0" class="col-xs-12 btn-mamiorange footer-action track-message {{ $msg_tracking }}" data-toggle="modal" data-target="#login" data-modal-text="untuk mengirim pesan">
          <!-- <img class="track-message {{ $msg_tracking }}" src="/assets/icons/ic_kirim_pesan_white.png" alt="ic_kirim_pesan_white"> -->
         @if(!isset($apartment_project_id))
          <span class="track-message {{ $msg_tracking }}" data-modal-text="untuk mengirim pesan"><h2><strong>HUBUNGI KOST</strong></h2></span>
          @else
          <span class="track-message {{ $msg_tracking }}" data-modal-text="untuk mengirim pesan"><h2><strong>HUBUNGI PENGELOLA</strong></h2></span>
          @endif
        </a>
      @endif
    @endif
  </div>
</footer>