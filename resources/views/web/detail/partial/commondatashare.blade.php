<!-- Social Share -->
<div class="col-md-8 col-xs-12 stretch room-data-share">
  <div class="share-container">
    <div class="col-lg-2 col-md-3 col-xs-12 share-title">
      Bagikan Lewat
    </div>
    <div id="mamiShare" class="col-lg-10 col-md-9 col-xs-12 share-list">
      <!-- Go to www.addthis.com/dashboard to customize your tools -->
      <mami-share></mami-share>
    </div>
  </div>
</div> <!-- End Social Share -->