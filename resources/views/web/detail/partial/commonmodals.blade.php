<!-- Modal Photos -->
<div class="modal fade" id="photosModal" tabindex="-1" role="dialog" aria-labelledby="modalPhotos">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><i class="fa fa-close fa-lg"></i></button>
			</div>
			<div class="modal-body">
				<div class="swiper-container">
					<div class="swiper-wrapper">
						@foreach ($cards as $key => $card)
						<div class="swiper-slide">
							<img
								title="Sewa {{ preg_replace("/[^A-Za-z0-9.]/", " ", str_replace(["-", "-"], " ", substr($card['description'], 0, 140))) }}"
								alt="Sewa {{ preg_replace("/[^A-Za-z0-9.]/", " ", str_replace(["-", "-"], " ", substr($card['description'], 0, 140))) }}"
								@if($key === 0)
								src="{{ $card['photo_url']['large'] }}"
								@else
								v-lazy="{
									src: '{{ $card['photo_url']['large'] }}',
									loading: '{{ $card['photo_url']['small'] }}'
								}"
								@endif
								>
						</div>

						@endforeach
					</div>
					<div class="swiper-pagination"></div>
					<div class="swiper-button-prev swiper-button-white"></div>
					<div class="swiper-button-next swiper-button-white"></div>
				</div>
			</div>
			<div class="modal-footer">
				@if(Auth::check())
					@if(Auth::user()->is_owner == 'false')
					<a type="button" class="btn btn-mamiorange track-message {{ $msg_tracking }} track-message {{ $msg_tracking }}-foto" data-toggle="modal" data-target="#chatModal" data-dismiss="modal">
						<span class="track-message {{ $msg_tracking }} track-message {{ $msg_tracking }}-foto"><h2><strong>HUBUNGI KOST</strong></h2></span>
					</a>
					@endif
				@else
				<a type="button" class="btn btn-mamiorange track-message {{ $msg_tracking }} track-message {{ $msg_tracking }}-foto" data-toggle="modal" data-target="#login" data-modal-text="untuk mengirim pesan" data-dismiss="modal">
					<span class="track-message {{ $msg_tracking }} track-message {{ $msg_tracking }}-foto"><h2><strong>HUBUNGI KOST</strong></h2></span>
				</a>
				@endif
			</div>
		</div>
	</div>
</div> <!-- End of photos modal -->


@if($has_photo_round)
<!-- Modal 360 -->
<div class="modal fade" id="360Modal" tabindex="-1" role="dialog" aria-labelledby="modal360">
	<div class="modal-dialog modal-xl modal-photo-sphere" role="document">
		<div class="modal-content">
			<div class="modal-body text-center">
				<div id="photoSphereViewer"></div>
			</div>
			<div class="modal-footer">
				@if(Auth::check())
					@if(Auth::user()->is_owner == 'false')
					<a role="button" tabindex="0" class="btn btn-mamiorange track-message {{ $msg_tracking }} track-message {{ $msg_tracking }}-360" data-toggle="modal" data-target="#chatModal" data-dismiss="modal">
						<span class="track-message {{ $msg_tracking }} track-message {{ $msg_tracking }}-360"><h2><strong>HUBUNGI KOST</strong></h2></span>
					</a>
					@endif
				@else
					<a role="button" tabindex="0" class="btn btn-mamiorange track-message {{ $msg_tracking }} track-message {{ $msg_tracking }}-360" data-toggle="modal" data-target="#login" data-modal-text="untuk mengirim pesan" data-dismiss="modal">
						<span class="track-message {{ $msg_tracking }} track-message {{ $msg_tracking }}-360"><h2><strong>HUBUNGI KOST</strong></h2></span>
					</a>
				@endif
				<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
			</div>
		</div>
	</div>
</div> <!-- End of 360 modal -->
@endif

<!-- check if has promotion available -->
@if($promotion !== null)
<!-- Modal for room promotion -->
<div class="modal fade" id="promotionModal" tabindex="-1" role="dialog" aria-labelledby="promotionModal">
	<div class="modal-dialog modal-sm modal-photo-sphere" role="document">
		<div class="modal-content">
			<div class="modal-header">
			 <h4>
				<img src="/assets/icons/ic_promo_for_user.png" class="promo-icon" alt="ic_promo_for_user">&nbsp;&nbsp;
				<strong>{{ $promotion->title }}</strong>
			</h4>
			</div>
			<div class="modal-body">
				<p class="expiration-label">Detail Promosi</p>
				<p>{{ $promotion->content }}</p>
				<div class="expiration">
					<p class="expiration-label">Masa berlaku</p>
					<p class="expiration-date">
						{{ $promotion->from }}
						-
						{{ $promotion->to }}
					</p>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
			</div>
		</div>
	</div>
</div>
@endif
<!-- endif -->

@if(!is_null ($youtube_id) && $youtube_id !== "")
<!-- Modal Youtube -->
<div class="modal fade" id="youtubeModal" tabindex="-1" role="dialog" aria-labelledby="modalYoutube">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<div style="position:relative;height:0;padding-bottom:56.21%"><iframe src="https://www.youtube.com/embed/{{$youtube_id}}?ecver=2" style="position:absolute;width:100%;height:100%;left:0" width="641" height="360" frameborder="0" allowfullscreen></iframe></div>
			</div>
			<div class="modal-footer">
				@if(Auth::check())
					@if(Auth::user()->is_owner == 'false')
					<a role="button" tabindex="0" class="btn btn-mamiorange track-message {{ $msg_tracking }} track-message {{ $msg_tracking }}-video" data-toggle="modal" data-target="#chatModal" data-dismiss="modal">
							<span class="track-message {{ $msg_tracking }} track-message {{ $msg_tracking }}-video"><h2><strong>HUBUNGI KOST</strong></h2></span>
						</a>
					@endif
				@else
					<a role="button" tabindex="0" class="btn btn-mamiorange track-message {{ $msg_tracking }} track-message {{ $msg_tracking }}-video" data-toggle="modal" data-target="#login" data-modal-text="untuk mengirim pesan" data-dismiss="modal">
						<span class="track-message {{ $msg_tracking }} track-message {{ $msg_tracking }}-video"><h2><strong>HUBUNGI KOST</strong></h2></span>
					</a>
				@endif
				<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
			</div>
		</div>
	</div>
</div> <!-- End of youtube modal -->
@endif

<!-- Modal Chat -->
<div class="modal fade" id="chatModal" tabindex="-1" role="dialog" aria-labelledby="modalChat">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-body text-center">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title chat-title">HUBUNGI {{ strtoupper($property_type) }} </h4>
					<form id="formChat" name="formChat" @submit.prevent="sendChat(chat.input, chat.phone)">
						<div class="text-left form-group" style="margin-top: 5px">
							<label for="phoneInput">Nomor Handphone</label>
							<p>Silakan menuliskan nomor handphone-mu di sini. Pemilik iklan yang akan menghubungimu.</p>
							<input type="tel" class="form-control" id="phoneInput" name="phoneInput" placeholder="Masukan nomor HP 081234xxx" v-model="chat.phone" pattern="^0(\d{3,4}-?){2}\d{3,4}$" required>
						</div>

						<div class="chat-option">

							@if(isset($questions))
								@if($chat_version == 1)

									@for ($i = 0; $i < sizeof($questions); $i++)
									<div class="radio radio-success radio-inline">
										<input type="radio" id="chatOption{{ $questions[$i]['id'] }}"
										name="chatOption" value='"option" : "{{ $questions[$i]['id'] }}", "note" : "{{ $questions[$i]['question'] }}"'
										v-model="chat.option"
										@change="selectChat(chat.option)">
										<label for="chatOption{{ $questions[$i]['id'] }}">{{ $questions[$i]['question'] }}</label>
									</div>
									<br>
									@endfor
								@endif
								@if($chat_version == 2)
									@for ($i = 0; $i < sizeof($questions["main"]); $i++)
										<div class="radio radio-success radio-inline">
											<input type="radio" id="chatOption{{ $questions['main'][$i]['id'] }}"
											name="chatOption" value='"option" : "{{ $questions["main"][$i]["id"] }}", "note" : "{{ $questions["main"][$i]["question"] }}"'
											v-model="chat.option"
											@change="selectChat(chat.option)">
											<label for="chatOption{{ $questions['main'][$i]['id'] }}">{{ $questions["main"][$i]["question"] }}</label>
										</div>
										<br>
									@endfor
									<div class="toogle-container">
										<span>Lihat selengkapnya</span>
										<a class="collapsed" href="#chatSecondary" data-toggle="collapse">
											<span class="if-collapsed">Lihat</span>
											<span class="if-not-collapsed">Tutup</span>
										</a>
									</div>

									<div id="chatSecondary" class="collapse">
										@for ($i = 0; $i < sizeof($questions["second"]); $i++)
											<div class="radio radio-success radio-inline">
												<input type="radio" id="chatOption{{ $questions['second'][$i]['id'] }}"
												name="chatOption" value='"option" : "{{ $questions["second"][$i]["id"] }}", "note" : "{{ $questions["second"][$i]["question"] }}"'
												v-model="chat.option"
												@change="selectChat(chat.option)">
												<label for="chatOption{{ $questions['second'][$i]['id'] }}">{{ $questions["second"][$i]["question"] }}</label>
											</div>
											<br>
										@endfor
									</div>
								@endif


							@endif

							<div class="radio radio-success radio-inline" style="display: none;">
								<input type="radio" id="chatOptionOther"
								name="chatOption" value='"option" : "another", "note" : ""'
								v-model="chat.option"
								@change="selectChat(chat.option)">
								<label for="chatOptionOther">Lainnya (isi di bawah)</label>
							</div>
							<!-- <br> -->

						</div>

						{{-- <textarea id="chatInputArea" class="chat-input" name="chatInputArea" placeholder="Minimal 3 karakter" :disabled="chat.id == 0 ? false : true" :autofocus="chat.id !== 1 ? true : false" v-model="chat.input"></textarea> --}}

						<p class="text-left">
							Untuk melindungi pengguna kami, Anda tidak dapat menghubungi pemilik
							iklan untuk kepentingan komersial maupun non-komersial.
						</p>

						<div class="flex">
							<div class="checkbox checkbox-success">
								<input id="agreedTerms" type="checkbox" v-model="agreedTerms.state">
								<label for="agreedTerms">
									Saya setuju
								</label>
							</div>
						</div>

						<button
							type="submit"
							class="btn btn-mamiorange chat-button"
							:class="agreedTerms.state ? 'track-message-apt-submit' : 'disabled'"
						>
							<h2 :class="{'track-message-apt-submit': agreedTerms.state}">
								<strong :class="{'track-message-apt-submit': agreedTerms.state}">
									KIRIM PESAN KE {{ strtoupper($property_type) }}
								</strong>
							</h2>
						</button>

						<button type="button" id="survey-langsung-button" class="btn btn-mamigreen-inverse survey-button" data-toggle="modal" data-target="#survey">MAU SURVEY LANGSUNG</button>
					</form>
			</div>
		</div>
	</div>
</div> <!-- End of chat modal -->

@if(Auth::check())
	@if(Auth::user()->is_owner == 'false' && $is_booking)
	<!-- Modal Booking -->
	<div class="modal fade" id="bookingModal" tabindex="-1" role="dialog" aria-labelledby="modalBooking">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div class="modal-body text-uppercase">
					BOOKING {{$room_title}}
				</div>
				<div class="modal-footer">
					@foreach ($booking_type as $key => $type)
					<form method="POST" action="/booking" class="booking-select">
						{{ csrf_field() }}
						<input type="hidden" name="seq" value="{{$_id}}">
						<input type="hidden" name="type" value="{{$type}}">
						<button type="submit" class="btn btn-mamigreen-inverse2 text-uppercase">{{ $property_type }} {{$type}}</button>
					</form>
					@endforeach
				</div>
			</div>
		</div>
	</div> <!-- End of booking modal -->
	@endif
@endif

<!-- Modal Survey -->
<div class="modal fade" id="survey" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-body">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title survey-title"><b>FORM UNTUK SURVEY LANGSUNG</b></h4>
					<form name="formSurvey" id="formSurvey" type="post">
						<div id="survey-page-1">

							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="nama_surveyor">Nama</label>
										<input type="text" name="nama_surveyor" class="form-control" id="nama_surveyor" placeholder="Tulis nama Anda di sini" required>
									</div>
									<div class="text-left form-group" style="margin-top: 5px">
										<label for="phoneInput">Nomor Handphone</label>
										<p>Silakan menuliskan nomor handphone-mu di sini. Pemilik iklan yang akan menghubungimu.</p>
										@if( isset(Auth::user()->phone_number))
										<input type="tel" class="form-control" id="phoneInput" name="phoneInput" placeholder="Masukan nomor HP 081234xxx" pattern="^0(\d{3,4}-?){2}\d{3,4}$" value="{{ Auth::user()->phone_number }}" required>
										@else
										<input type="tel" class="form-control" id="phoneInput" name="phoneInput" placeholder="Masukan nomor HP 081234xxx" pattern="^0(\d{3,4}-?){2}\d{3,4}$" value="" required>
										@endif
									</div>
									<div class="form-group">
										<label for="gender">Jenis Kelamin</label>
										<div>
											<label class="radio-inline">
												<input type="radio" name="gender" id="gender-male" value="male" required> Laki-laki
											</label>
											<label class="radio-inline">
												<input type="radio" name="gender" id="gender-female" value="female" required> Perempuan
											</label>
										</div>
									</div>
									<div class="form-group">
										<label for="birthday">Tanggal Lahir</label>
										<input type="text" name="birthday" id="birthday" class="form-control" placeholder="YYYY-MM-DD" required style="max-width: 200px;">
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
										<label for="occupation">Pekerjaan (pilih salah satu di bawah ini)</label>
										<div class="btn-group" data-toggle="buttons" role="group" id="occupation-selector">
											<label class="btn btn-occupation active">
												<input type="radio" name="occupation" id="occupation-kerja" value="kerja" checked> Kerja
											</label>
											<label class="btn btn-occupation">
												<input type="radio" name="occupation" id="occupation-kuliah" value="kuliah"> Kuliah
											</label>
											<label class="btn btn-occupation">
												<input type="radio" name="occupation" id="occupation-other" value="lainnya"> Lainnya
											</label>
										</div>
									</div>

									<div class="occupation-group occupation-group-kerja active">
										<div class="form-group">
											<label for="occupation-posisi">Posisi</label>
											<input type="text" name="occupation_posisi" id="occupation-posisi" class="form-control" placeholder="Tulis posisi Anda di sini">
										</div>
										<div class="form-group">
											<label for="occupation-workplace">Nama Kantor</label>
											<input type="text" name="occupation_workplace" id="occupation-workplace" class="form-control" placeholder="Tulis nama kantor Anda di sini">
										</div>
									</div>

									<div class="occupation-group occupation-group-kuliah">
										<div class="form-group">
											<label for="occupation-university">Universitas</label>
											<input type="text" name="occupation_university" id="occupation-university" class="form-control" placeholder="Tulis nama universitas Anda di sini">
										</div>
										<div class="form-group">
											<label for="occupation-major">Jurusan</label>
											<input type="text" name="occupation_major" id="occupation-major" class="form-control" placeholder="Tulis jurusan Anda di sini">
										</div>
										<div class="form-group">
											<label for="occupation-semester">Semester</label>
											<input type="text" name="occupation_semester" id="occupation-semester" class="form-control" placeholder="Tulis posisi Anda di sini">
										</div>
									</div>

									<div class="occupation-group occupation-group-lainnya">
										<div class="form-group">
											<textarea name="occupation_other_desc" id="occupation-other-desc" class="form-control"></textarea>
										</div>
									</div>

									<button type="button" class="btn btn-mamiorange survey-button" id="survey-next-page">Selanjutnya</button>
								</div>
							</div>
						</div>

						<div id="survey-page-2">
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label for="stay-time">Lama waktu yang diinginkan</label>
										<div class="col-md-12">
											<div class="col-md-2">
												<input type="number" name="stay_time" id="stay-time" class="form-control" value="1" min="1" max="999" required>
											</div>
											<div class="col-md-3">
												<select name="stay_period" id="stay-period" class="form-control">
													<option value="Tahun">Tahun</option>
													<option value="Bulan">Bulan</option>
													<option value="Minggu">Minggu</option>
													<option value="Hari">Hari</option>
												</select>
											</div>
										</div>
									</div>

									<div class="form-group">
										<label for="survey-date">Tentukan Tanggal &amp; Waktu Survey</label>
										<div class="col-md-12">
											<div class="col-md-5">
												<input type="text" name="survey_date" id="survey-date" placeholder="YYYY-MM-DD" class="form-control" required>
											</div>
											<div class="col-md-3">
												<input type="text" name="survey_time" id="survey-time" placeholder="HH:MM" class="form-control" required>
											</div>
										</div>
									</div>

									<div class="form-group">
										<label>Tandai Pernyataan Berikut :</label>
										<div>
											<label>
												<input type="checkbox" name="forward" id="forward" value="1"> Saya ingin nomor saya dikirim ke Pemilik {{ $property_type }}.
											</label>
										</div>
										<div>
											<label>
												<input type="checkbox" name="serious" id="serious" value="1"> Saya menyatakan serius ingin survey {{ $property_type }} pada jadwal tersebut.
											</label>
										</div>
									</div>

									<button type="submit" class="btn btn-mamiorange survey-button" id="survey-send">Kirim</button>
								</div>
							</div>
						</div>
					</form>

			</div>
		</div>
	</div>
</div> <!-- End of survey modal -->

<!-- Modal Claim Kost Owner -->
<div class="modal fade" id="claimModal" tabindex="-1" role="dialog" aria-labelledby="claimModal">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-body text-center" style="font-size:18px;">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<img src="/assets/bannerappicon.png" style="margin-bottom:10px" alt="bannerappicon">
				<span class="col-xs-12"><b>Nama {{ $property_type }}</b></span>
				<textarea class="text-center form-control col-xs-12" placeholder="{{$room_title}}" disabled style="font-size:16px;margin-bottom:10px">{{$room_title}}</textarea>
				<form name="formUpdate">
					<span class="col-xs-12"><b>Status Pengklaim</b></span>
					<select class="col-xs-12 btn btn-default" v-model="claimerStatus" style="font-size:16px;margin-bottom:10px">
						<option>Agen</option>
						<option>Pemilik Kos</option>
						<option>Pengelola Kos</option>
						<option>Anak Kos</option>
					</select>
					<button type="button" class="btn btn-mamigreen" data-dismiss="modal" @click="postClaim(claimerStatus)" @submit.prevent="postClaim(claimerStatus)" style="font-size:16px;">
						KLAIM
					</button>
				</form>
			</div>
		</div>
	</div>
</div> <!-- End of claim modal -->

<!-- Modal Report Kost -->
<div class="modal fade" id="reportModal" tabindex="-1" role="dialog" aria-labelledby="reportModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Laporkan {{ $property_type }}</h4>
			</div>
			<div class="modal-body">
				<form class="form-report" id="form-report">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								@foreach($report_types as $reportTypeKey => $reportType)
									<label for="room-report-type-{{ $reportTypeKey }}" class="report-type-label">
										<input type="checkbox" name="report_type[]" value="{{ $reportType['type'] }}" id="room-report-type-{{ $reportTypeKey }}"> <span>{{ $reportType['name'] }}</span>
									</label>
								@endforeach
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="report-description" class="control-label col-md-12">
									Laporkan lebih detail di sini
								</label>
								<div class="col-md-12">
									<textarea name="report_description" class="form-control" rows="6" placeholder="Tulis lebih detail tentang laporan Anda" id="report-description" required></textarea>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<button type="submit" class="btn btn-mamiorange" id="report-submit">SUBMIT</button>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div> <!-- End of report -->


<!-- Modal Love/Favorit -->
<div class="modal fade" tabindex="-1" role="dialog" id="modalDownload">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body" style="padding:0px 15px;">
				<button type="button" class="close closemodal" data-dismiss="modal" aria-label="Close" >
					<span aria-hidden="true" class="closetext">x</span>
				</button>

				<div class="row">
					<div class="col-xs-4" style="padding:0px;background-color:#33CC33;height:10px;">
					</div>
					<div class="col-xs-4" style="padding:0px;background-color:#1F7B2B;height:10px;">
					</div>
					<div class="col-xs-4" style="padding:0px;background-color:#33CC33;height:10px;">
					</div>
					<div class="col-xs-12" style="padding:0px;text-align:center;padding:25px;">
						<img style="width:120px;" src="/assets/iconfavorit.png" alt="iconfavorit">
					</div>
					<br>
					<div class="col-xs-12" style="padding:5px;text-align:center;font-size:10px;font-weight:bold;">
						<span style="color:#1BAA56;font-size:15px;">Kami bisa menyimpan kos favoritmu di Aplikasi MamiKos lho!</span><br>
					</div>
					<div class="col-xs-12" style="margin:5px 0px; text-align:center;">
						<a href="https://play.google.com/store/apps/details?id=com.git.mami.kos&referrer=utm_source%3DWebDetail%26utm_medium%3Dfav%26utm_term%3Dmamikos%252Bapp%26utm_content%3Dfav%252Bdetail%26utm_campaign%3DMamikos%2520Web%2520Detail">
							<img style="width:120px;"src="/assets/googleplay.png" alt="mamikos gplay">
						</a>
						<a href="https://click.google-analytics.com/redirect?tid=UA-68769829-2&url=https%3A%2F%2Fitunes.apple.com%2Fid%2Fapp%2Fmami-kos%2Fid1055272843%3Fmt%3D8&aid=com.git.MamiKos&idfa=%{idfa}&cs=WebDetail&cm=fav&cn=Mamikos%20Web%20Detail&cc=fav%2Bdetail&ck=mamikos%2Bapp">
							<img style="width:120px;"src="/assets/button_iphone.png" alt="ios_button_mamikos">
						</a>
					</div>
					<div class="col-xs-12" style="padding:5px; text-align:center;">
						<span style="color:#1BAA56;font-size:15px;">Kirimkan link download untuk install Aplikasi MamiKos ke Email saya :</span>
					</div>
					<div class="col-xs-12" style="padding:15px;">
						<form name="formContacts" class="form-download-app" v-on:submit.prevent>
							<div class="form-group col-xs-10" style="text-align:left;color:green;padding-left:0;">
								<input type="e-mail" class="form-control email-download" id="email-download" name="email-download" placeholder="contoh: nama@domain.com" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" title="Masukkan Email Anda" required>
							</div>
							<button type="submit" class="btn btn-mamigreen col-xs-2" style="padding-right:0;padding-left:0;">KIRIM</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div> <!-- End of Modal Love/Favorit -->

 <!-- actual modal love/favorit -->


<!-- end actual modal love/favorit -->

<!-- Modal Loading Overlay -->
<div id="modalLoading" class="modal fade" role="dialog">
	<div class="modal-dialog modal-sm">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-body modal-loading">
					<h4>Mohon tunggu...</h4>
					<i class="fa fa-circle-o-notch fa-spin"></i>
			</div>
		</div>
	</div>
</div>
<!-- End of loading overlay modal -->



@if(isset($checkin_attempt) && $checkin_attempt)
<!-- Modal for checking in -->
<div class="modal fade" id="checkinModal" tabindex="-1" role="dialog" aria-labelledby="checkinModal">
	<div class="modal-dialog modal-md modal-photo-sphere" role="document">
		<form @submit.prevent="checkIn">
			<div class="modal-content">
				<div class="modal-header">
					<label>
						<strong>Saya mulai kost disini sejak</strong>
					</label>
					<button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-xs-3">
							<label for="date-input">Tanggal</label>
						</div>
						<div class="col-xs-5">
							<label for="month-input">Bulan</label>
						</div>
						<div class="col-xs-4">
							<label for="year-input">Tahun</label>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-3">
							<input type="number" id="date-input" class="form-control" placeholder="Tanggal" max="31" min="1" maxlength="2" v-model="checkinDate" required>
						</div>
						<div class="col-xs-5">
							<select id="month-input" class="form-control" v-model="checkinMonth">
								<option value="01">Januari</option>
								<option value="02">Februari</option>
								<option value="03">Maret</option>
								<option value="04">April</option>
								<option value="05">Mei</option>
								<option value="06">Juni</option>
								<option value="07">Juli</option>
								<option value="08">Agustus</option>
								<option value="09">September</option>
								<option value="10">Oktober</option>
								<option value="11">November</option>
								<option value="12">Desember</option>
							</select>
						</div>
						<div class="col-xs-4">
							<input type="number" id="year-input" class="form-control" placeholder="Tahun" max="9999" min="1" maxlength="4" v-model="checkinYear" required>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-mamiorange btn-modal">Submit</button>
				</div>
			</div>
		</form>
	</div>
</div>
@endif

<!-- Modal Update Availibilty -->
<div class="modal fade" id="updateAvailability" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">

			<div class="modal-body">
				<button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
				<div class="content-availability">
					<label class="availability-title">Apakah kamu berhasil<br> menghubungi kost ini?</label>
					<br>
					<p>Ayo bantu kami update <br> ketersediaan kamar kost <br> yang sudah kamu hubungi</p>
					<form @submit.prevent="submitUpdateRoom">
						<div class="form-inline">
							<div class="form-group">
								<input
									type="number"
									id="updateRoom"
									class="form-control"
									required
									min="0"
									max="100"
									v-model="updateRoom.value"
								>
								<label for="updateRoom">Kamar Kosong</label>
							</div>
						</div>
						<button type="submit" class="btn btn-mamigreen btn-update">Submit</button>
					</form>
				</div>
			</div>

		</div>
	</div>
</div>
<!-- End of Modal Update Availibilty -->

<!-- Modal Brand Survey -->
<div id="brandSurveyModal" class="modal fade" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content modal-no-border">
			<form @submit.prevent="sendBrandSurvey(brandSurvey.answer)">
				<div class="modal-header">
					<h4 class="modal-title text-center">Dari mana kamu tahu tentang Mamikos?</h4>
				</div>
				<div class="modal-body">
					<input type="text" class="form-control" required minlength="3" maxlength="50" placeholder="silakan tulis di sini terima kasih :)" :disabled="brandSurvey.loading" v-model="brandSurvey.answer">
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-mamigreen full-width" v-if="!brandSurvey.loading">Submit</button>
					<button type="button" class="btn btn-mamigreen full-width" disabled v-else v-cloak>Mengirim...</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- End of Modal Brand Survey -->
