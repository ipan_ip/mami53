<!-- Harga dan Pembayaran (Desktop - FLoating) -->
<div class="col-md-4 pull-right hidden-sm hidden-xs display-room-container">
  <div id="roomDisplay">
    <div class="col-md-12 display-room-content">
      @if(!isset($apartment_project_id))
      <div class="col-md-12 stretch display-room-price">
        @if($price_daily !== 0 && !is_null ($price_daily))
        <div class="col-md-8 price-data">
          Rp {{number_format($price_daily)}}
        </div>
        <div class="col-md-4 price-term">
          / hari
        </div>
        @endif
        @if($price_weekly !== 0 && !is_null ($price_weekly))
        <div class="col-md-8 price-data">
          Rp {{number_format($price_weekly)}}
        </div>
        <div class="col-md-4 price-term">
          / minggu
        </div>
        @endif
        @if($price_monthly !== 0 && !is_null ($price_monthly))
        <div class="col-md-8 price-data">
          Rp {{number_format($price_monthly)}}
        </div>
        <div class="col-md-4 price-term">
          / bulan
        </div>
        @endif
        @if($price_yearly !== 0 && !is_null ($price_yearly))
        <div class="col-md-8 price-data">
          Rp {{number_format($price_yearly)}}
        </div>
        <div class="col-md-4 price-term">
          / tahun
        </div>
        @endif
        <div class="clearfix"></div>
        @foreach ($fac_price_icon as $key => $fac_price_icon)
        <div class="col-lg-8 col-md-12 price-electricity">
          <div class="price-electricity-icon-container">
            <img src="{{$fac_price_icon['photo_url']}}" class="price-electricity-icon" alt="{{$fac_price_icon['name']}}">
          </div>
          <div class="price-electricity-label">
            {{$fac_price_icon['name']}}
          </div> 
        </div>
        @endforeach
        <div class="col-md-12 price-payment">
          @if(!is_null ($fac_keyword[0]))
          <span>Pembayaran {{ $fac_keyword[0] }}</span>
          @else
          <span>Tidak ada ketentuan min. pembayaran</span>
          @endif
        </div>
        @if(mt_rand(1, 100) < 200)

          @if(Auth::check())
            @if(Auth::user()->is_owner == 'false')
              <div class="col-md-12 price-payment ask-phone-number">
                <span>Nomor Telepon : 
                  @if(Auth::user()->phone_number == null)
                  <a 
                  tabindex="0" 
                  role="button" 
                  class="text-mamiorange link-mamiorange"
                  data-toggle="popover" 
                  data-placement="bottom" 
                  data-html="true"
                  data-template='<div class="popover" role="tooltip"><div class="arrow"></div><div class="popover-title" style="height: 50px;"></div><div class="popover-content"></div></div>'
                  title="<div class='popover-title-text'>Agar pemilik bisa menghubungimu, tulis nomor hp mu disini</div><a role='button' onclick='togglePopover()' class='popover-title-close'> &times; </a>" 
                  data-content="
                    <input type='number' id='phoneNumberPopover'  pattern='.{10,}' required>
                    <button 
                      id='popoverButton'
                      type='button' 
                      class='btn btn-mamiorange'
                      onclick='sendPopoverChat()'
                    >
                      Kirim
                    </button>"
                  >
                    Lihat Nomor Telepon
                  </a>
                  @else
                  <a class="text-mamiorange link-mamiorange" role="button" data-toggle="modal" data-target="#chatModal">
                    Lihat Nomor Telepon
                  </a>
                  @endif
                </span>
              </div>
            @endif
          @else
            <div class="col-md-12 price-payment ask-phone-number">
              <span>Nomor Telepon : 
                <a class="text-mamiorange link-mamiorange" role="button" tabindex="0" data-toggle="modal" data-target="#login">
                  Lihat Nomor Telepon
                </a>
              </span>
            </div>
          @endif

        @endif  
        
      </div>
      @else
      <div class="col-md-12 stretch display-room-price">
        @foreach ($price_components as $key => $price_component)
        <div>
          <div class="col-lg-8 col-md-12 price-term" title="{{$price_component['label']}}">
            {{$price_component['label']}} :
          </div>
          <div class="col-lg-4 col-md-12 price-data is-extra" title="{{$price_component['price_reguler_formatted']}}">
            {{$price_component['price_reguler_formatted']}}
          </div>
        </div>
        @endforeach
        

        @if($price_daily !== 0 && !is_null ($price_daily))
        <div class="col-md-8 price-data">
          Rp {{number_format($price_daily)}}
        </div>
        <div class="col-md-4 price-term">
          / hari
        </div>
        @endif
        @if($price_weekly !== 0 && !is_null ($price_weekly))
        <div class="col-md-8 price-data">
          Rp {{number_format($price_weekly)}}
        </div>
        <div class="col-md-4 price-term">
          / minggu
        </div>
        @endif
        @if($price_monthly !== 0 && !is_null ($price_monthly))
        <div class="col-md-8 price-data">
          Rp {{number_format($price_monthly)}}
        </div>
        <div class="col-md-4 price-term">
          / bulan
        </div>
        @endif
        @if($price_yearly !== 0 && !is_null ($price_yearly))
        <div class="col-md-8 price-data">
          Rp {{number_format($price_yearly)}}
        </div>
        <div class="col-md-4 price-term">
          / tahun
        </div>
        @endif
        <div class="clearfix"></div>


        <div class="col-md-12 price-payment">
          @if(!is_null ($fac_keyword[0]))
          <span>Pembayaran {{ $fac_keyword[0] }}</span>
          @else
          <span>Tidak ada ketentuan min. pembayaran</span>
          @endif
        </div>

        @if(mt_rand(1, 100) < 200)

          @if(Auth::check())
            @if(Auth::user()->is_owner == 'false')
              <div class="col-md-12 price-payment ask-phone-number">
                <span>Nomor Telepon : 
                  @if(Auth::user()->phone_number == null)
                  <a 
                  tabindex="0" 
                  role="button" 
                  class="text-mamiorange link-mamiorange"
                  data-toggle="popover" 
                  data-placement="bottom" 
                  data-html="true"
                  data-template='<div class="popover" role="tooltip"><div class="arrow"></div><div class="popover-title" style="height: 50px;"></div><div class="popover-content"></div></div>'
                  title="<div class='popover-title-text'>Agar pemilik bisa menghubungimu, tulis nomor hp mu disini</div><a role='button' onclick='togglePopover()' class='popover-title-close'> &times; </a>" 
                  data-content="
                    <input type='number' id='phoneNumberPopover'  pattern='.{10,}' required>
                    <button 
                      id='popoverButton'
                      type='button' 
                      class='btn btn-mamiorange'
                      onclick='sendPopoverChat()'
                    >
                      Kirim
                    </button>"
                  >
                    Lihat Nomor Telepon
                  </a>
                  @else
                  <a class="text-mamiorange link-mamiorange" role="button" data-toggle="modal" data-target="#chatModal">
                    Lihat Nomor Telepon
                  </a>
                  @endif
                </span>
              </div>
            @endif
          @else
            <div class="col-md-12 price-payment ask-phone-number">
              <span>Nomor Telepon : 
                <a class="text-mamiorange link-mamiorange" role="button" tabindex="0" data-toggle="modal" data-target="#login">
                  Lihat Nomor Telepon
                </a>
              </span>
            </div>
          @endif

        @endif

      </div>
      @endif

      @if(Auth::check())
        @if(Auth::user()->is_owner == 'false')
        <div class="col-md-12 stretch display-room-action">
          @if($is_booking)  
          <a role="button" tabindex="0" class="btn btn-mamiorange-inverse track-booking" @click="placeBooking">
            <img class="track-booking" src="/assets/icons/ic_booking_orange.png" alt="ic_booking_orange">
            <span class="track-booking">BOOKING</span>
          </a>
          @endif
          @if(isset($checkin_attempt) && $checkin_attempt)
          <a role="button" tabindex="0" class="btn btn-mamigreen track-checkin" data-toggle="modal" data-target="#checkinModal">
            <span class="track-message {{ $msg_tracking }}">TANDAI KOS SAYA</span>
          </a>
          @else
            @if(!isset($apartment_project_id))
            <a role="button" tabindex="0" class="btn btn-mamiorange track-message {{ $msg_tracking }}" data-toggle="modal" data-target="#chatModal">
              <!-- <img class="track-message {{ $msg_tracking }}" src="/assets/icons/ic_kirim_pesan_white.png" alt="ic_kirim_pesan_white"> -->
              <span class="track-message {{ $msg_tracking }}"><h2><strong>HUBUNGI KOST</strong></h2></span>
            </a>
            @else
            <a role="button" tabindex="0" class="btn btn-mamiorange track-message {{ $msg_tracking }}" data-toggle="modal" data-target="#chatModal">
              <!-- <img class="track-message {{ $msg_tracking }}" src="/assets/icons/ic_kirim_pesan_white.png" alt="ic_kirim_pesan_white"> -->
              <span class="track-message {{ $msg_tracking }}"><h2><strong>HUBUNGI PENGELOLA</strong></h2></span>
            </a>
            @endif
          @endif
        </div>
        @else
        <div class="col-md-12 stretch display-room-action">
          <a role="button" tabindex="0" class="btn btn-mamiorange track-owner-claim" data-toggle="modal" data-target="#claimModal">
            <span class="track-owner-claim">KLAIM {{ strtoupper($property_type) }} INI</span>
          </a>
        </div>
        @endif
      @else
      <div class="col-md-12 stretch display-room-action">
        @if($is_booking)  
        <a role="button" tabindex="0" class="btn btn-mamiorange-inverse track-booking" data-toggle="modal" data-target="#login" data-modal-text="untuk membooking kos ini">
          <img class="track-booking" src="/assets/icons/ic_booking_orange.png" alt="ic_booking_orange">
          <span class="track-booking">BOOKING</span>
        </a>
        @endif
          @if(!isset($apartment_project_id))
          <a role="button" tabindex="0" class="btn btn-mamiorange track-message {{ $msg_tracking }}" data-toggle="modal" data-target="#login" data-modal-text="untuk mengirim pesan">
            <!-- <img class="track-message {{ $msg_tracking }}" src="/assets/icons/ic_kirim_pesan_white.png" alt="ic_kirim_pesan_white"> -->
            <span class="track-message {{ $msg_tracking }}"><h2><strong>HUBUNGI KOST</strong></h2></span>
          </a>
          @else
          <a role="button" tabindex="0" class="btn btn-mamiorange track-message {{ $msg_tracking }}" data-toggle="modal" data-target="#login" data-modal-text="untuk mengubungi pengelola">
            <!-- <img class="track-message {{ $msg_tracking }}" src="/assets/icons/ic_kirim_pesan_white.png" alt="ic_kirim_pesan_white"> -->
            <span class="track-message {{ $msg_tracking }}"><h2><strong>HUBUNGI PENGELOLA</strong></h2></span>
          </a>
          @endif
      </div>
      @endif
      <div class="col-md-12 stretch display-room-latest">
        <span>Update terakhir pada</span>
        <br>
        <span class="text-mamigreen">{{$updated_at}}</span>
        <br>
        <span class="small"><span class="text-danger">*</span>Data bisa berubah sewaktu-waktu</span>
      </div>
    </div>
  </div>
</div> <!-- End Harga dan Pembayaran (Desktop - FLoating) -->
