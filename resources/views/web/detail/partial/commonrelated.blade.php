<!-- Related Kost -->
<div id="related" class="container-fluid room-related-container" v-if="suggestion.normal !== null">
	<div class="container stretch">
		<div class="col-md-12 stretch related-body">
			<div class="col-md-12 related-header">
				<span><h3><strong>Rekomendasi {{ $property_type }} Menarik Lainnya : </strong></h3></span>
			</div>
			<!-- Loading Related Kost -->
			<div class="related-result" v-if="loading.suggestion">
				<div class="clearfix"></div>
				<div class="related-container-fixed">
					<div class="related-container-grid">
						<div class="col-xs-12 related-promoted-icon">
						</div>
						<div class="col-xs-3 related-card">
							<div class="related-box is-loading">
								<div class="related-content">
								</div>
							</div>
						</div>
						<div class="col-xs-3 related-card">
							<div class="related-box is-loading">
								<div class="related-content">
								</div>
							</div>
						</div>
						<div class="col-xs-3 related-card">
							<div class="related-box is-loading">
								<div class="related-content">
								</div>
							</div>
						</div>
						<div class="col-xs-3 related-card">
							<div class="related-box is-other">
							</div>
						</div>
					</div>
				</div>
			</div> <!-- End Related Kost -->
			<!-- Show Related Kost -->
			<div class="related-result" v-else v-cloak>
				<div class="clearfix"></div>
				<div class="related-container-fixed">
					<div class="related-container-grid">
						<div class="col-md-12 related-promoted-icon">
							<span v-if="suggestion.promoted !== null">
								<img src="/assets/icons/promote_kost_icon_dekstop.png" alt="promote_kost_icon_dekstop">&nbsp; Sponsored {{ $property_type }}
							</span>
						</div>
						<a :href="room.share_url" target="_blank" rel="noopener" class="col-xs-3 related-card track-promoted-kost track-kost-lainnya-related" v-for="room in suggestion.promoted">
							<div
								class="related-box is-promoted track-promoted-kost"
								:title="room['room-title']"
								v-lazy:background-image="room.photo_url.medium">
								<div class="related-content">
									<div class="related-content-data">
										<span class="related-content-price">@{{room.price_title_time}}</span>
										<span class="related-content-gender is-female" v-if="room.gender == 2">Putri</span>
										<span class="related-content-gender is-male" v-else-if="room.gender == 1">Putra</span>
										<span class="related-content-gender is-mix" v-else>Campur</span>
									</div>
									<div class="related-content-data">
										<p class="related-content-title">@{{room['room-title']}}</p>
									</div>
									<div class="related-content-data flex-center">
										<label class="related-content-premium" v-if="room.is_premium_owner">
											<img :src="icon.premium2" alt="premium2" v-cloak>
											<span>PREMIUM</span>
										</label>
										<label class="related-content-booking" v-if="room.is_booking">
											<img :src="icon.booking2" alt="booking2" v-cloak>
											<span>BISA BOOKING</span>
										</label>
									</div>
								</div>
							</div>
						</a>
						<a :href="room.share_url" target="_blank" rel="noopener" class="col-xs-3 related-card track-no-promoted-kost track-kost-lainnya-related" v-for="room in suggestion.normal">
							<div
								class="related-box is-normal track-no-promoted-kost"
								:title="room['room-title']"
								v-lazy:background-image="room.photo_url.medium">
								<div class="related-content">
									<div class="related-content-data">
										<span class="related-content-price">@{{room.price_title_time}}</span>
										<span class="related-content-gender is-female" v-if="room.gender == 2">Putri</span>
										<span class="related-content-gender is-male" v-else-if="room.gender == 1">Putra</span>
										<span class="related-content-gender is-mix" v-else>Campur</span>
									</div>
									<div class="related-content-data">
										<p class="related-content-title">@{{room['room-title']}}</p>
									</div>
									<div class="related-content-data flex-center">
										<label class="related-content-premium" v-if="room.is_premium_owner">
											<img :src="icon.premium2" alt="premium2" v-cloak>
											<span>PREMIUM</span>
										</label>
										<label class="related-content-booking" v-if="room.is_booking">
											<img :src="icon.booking2" alt="booking2" v-cloak>
											<span>BISA BOOKING</span>
										</label>
									</div>
								</div>
							</div>
						</a>
						@if(!isset($apartment_project_id))
						<a href ="<?=isset($urllanding)?$urllanding:"" ?>" target="_blank" rel="noopener" class="col-xs-3 related-card track-related-detail track-kost-lainnya-related">
							<div class="related-box is-other track-related-detail" alt="<?=isset($namalanding)?$namalanding:"" ?>Mamikos.com" title="<?=isset($namalanding)?$namalanding:"" ?>Mamikos.com">
								<span>Lihat {{ $property_type }}</span>
								<span>di Sekitar</span>
								<span><b>{{$namalanding}}</b></span>
								<span>Lainnya</span>
							</div>
						</a>
						@else
							@if(isset($breadcrumbs))
							<a href ="<?= $breadcrumbs[count($breadcrumbs)-2]["url"] ?>" target="_blank" class="col-xs-3 related-card track-related-detail">
								<div class="related-box is-other track-related-detail" alt="<?= $breadcrumbs[count($breadcrumbs)-2]["name"] ?> Mamikos.com" title="<?= $breadcrumbs[count($breadcrumbs)-2]["name"] ?> Mamikos.com">
									<span>Lihat Unit</span>
									<span>lainnya di</span>
									<span><b>{{ $breadcrumbs[count($breadcrumbs)-2]["name"] }}</b></span>
								</div>
							</a>
							@endif
						@endif
					</div>
				</div>
			</div> <!-- End Show Related Kost -->
		</div>
		<div class="clearfix"></div>
	</div>
</div><!-- End Related Kost -->
