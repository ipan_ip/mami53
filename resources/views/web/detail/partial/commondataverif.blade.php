<!-- Verifikasi -->
<div id="verif" class="col-md-8 col-xs-12 stretch room-data-verif">
  @if(!isset($apartment_project_id))
  <div class="verif-container">
    <div class="col-lg-2 col-md-3 col-xs-12 verif-title">
      Verifikasi
    </div>
    <div class="col-lg-10 col-md-9 col-xs-12 verif-list">
      @if ($verification_status['is_verified_kost']==true)
      <div class="col-md-4 col-xs-6 verif-content is-big">
        <img v-lazy="'assets/home-desktop/sudah_terverifikasi.png'" alt="sudah_terverifikasi">
        <span>VERIFIKASI LENGKAP</span>
      </div>
      @else
      <div class="col-md-4 col-xs-6 verif-content is-big is-not-verified">
        <img v-lazy="'assets/home-desktop/belum_terverifikasi.png'" alt="belum_terverifikasi">
        <span>VERIFIKASI BELUM LENGKAP</span>
      </div>
      @endif
      <div class="col-md-8 col-xs-6 stretch">
        @if ($verification_status['is_visited_kost']==true)
        <div class="col-md-6 col-xs-12 verif-content is-small">
          <img v-lazy="'assets/home-desktop/sudah_dikunjungi.png'" alt="sudah_dikunjungi">
          <span>Sudah dikunjungi</span>
        </div>
        @else
        <div class="col-md-6 col-xs-12 verif-content is-small is-not-verified">
          <img v-lazy="'assets/home-desktop/belum_dikunjungi.png'" alt="belum_dikunjungi">
          <span>Belum dikunjungi</span>
        </div>
        @endif
        @if ($verification_status['is_verified_address']==true)
        <div class="col-md-6 col-xs-12 verif-content is-small">
          <img v-lazy="'assets/home-desktop/sudah_alamat.png'" alt="sudah_alamat">
          <span>Alamat terverifikasi</span>
        </div>
        @else
        <div class="col-md-6 col-xs-12 verif-content is-small is-not-verified">
          <img v-lazy="'assets/home-desktop/belum_alamat.png'" alt="belum_alamat">
          <span>Alamat belum terverifikasi</span>
        </div>
        @endif
        @if ($verification_status['is_verified_phone']==true)
        <div class="col-md-6 col-xs-12 verif-content is-small">
          <img v-lazy="'assets/home-desktop/sudah_telepon.png'" alt="sudah_telepon">
          <span>Telepon terverifikasi</span>
        </div>
        @else
        <div class="col-md-6 col-xs-12 verif-content is-small is-not-verified">
          <img v-lazy="'assets/home-desktop/belum_telepon.png'" alt="belum_telepon">
          <span>Telepon belum terverifikasi</span>
        </div>
        @endif
      </div>
    </div>
  </div>
  @endif
</div> <!-- End Verifikasi -->