<div class="container">
	<div class="row">
		<div class="col-md-12 room-info-action">
			<a href="javascript:void(0)" class="btn-report-room">Laporkan {{ $property_type }}</a>
		</div>
	</div>
</div>

<div class="container room-photo-swiper">
	<div class="swiper-container">
		<div class="swiper-wrapper">
			@foreach ($cards as $key => $card)
			<div class="swiper-slide">
				<img
					title="Sewa {{ preg_replace("/[^A-Za-z0-9.]/", " ", str_replace(["-", "-"], " ", substr($card['description'], 0, 140))) }}"
					alt="Sewa {{ preg_replace("/[^A-Za-z0-9.]/", " ", str_replace(["-", "-"], " ", substr($card['description'], 0, 140))) }}"
					@if($key === 0)
					src="{{ $card['photo_url']['large'] }}"
					@else
					v-lazy="{
						src: '{{ $card['photo_url']['large'] }}',
						loading: '{{ $card['photo_url']['small'] }}'
					}"
					@endif
					>
			</div>
			@endforeach
		</div>
		<div class="swiper-pagination"></div>
		<div class="swiper-button-prev swiper-button-white"></div>
		<div class="swiper-button-next swiper-button-white"></div>
	</div>
</div>
