<div
	class="container-fluid stretch room-info-container"
	v-lazy:background-image="{
		src: '{{ $photo_url['large'] }}',
		loading: '{{ $photo_url['small'] }}'
	}">
	<div class="full-width full-height room-info-overlay">
		<div class="container stretch room-info-action">

			@if($promotion !== null)
			<div class="promo-container">
				<span class="promo-label">
					<span class="promo-ic-container">
						<span class="promo-ic-wrapper">
							<img src="/assets/icons/ic_kado_putih.png" class="promo-icon" alt="ic_kado_putih">
						</span>
					</span>
					<span class="promo-content">
						<span class="promo-caption-container">
							{{ $promotion->title }}
						</span>
						<span class="promo-link-container track-lihat-promo-kost">
							<a href="javascript:void(0)" class="promo-link btn-promo-link track-kost-promo-open">
								Lihat promo
							</a>
						</span>
					</span>
				</span>
			</div>
			@endif


			<a href="javascript:void(0)" class="btn-report-room">Laporkan {{ $property_type }}</a>


		</div>
		<div class="container stretch room-info-section">
			<div class="col-md-12 col-xs-12 room-info-label">
				@if($gender == 2)
				<label class="label-female">Putri</label>
				@elseif($gender == 1)
				<label class="label-male">Putra</label>
				@else
				<label class="label-mix">Campur</label>
				@endif

				@if($is_booking)
				<label class="label-booking" title="{{ $property_type }} ini bisa di-Booking">
					<img :src="icon.booking2" alt="mamikos booking badge" alt="booking2" v-cloak>
					<span>BISA BOOKING</span>
				</label>
				@endif
			</div>
			<div class="col-lg-6 col-md-5 col-xs-12">
				<div class="room-info-title">
					<h1><strong>{{$room_title}}</strong></h1>
				</div>
				<div class="room-info-rating">
					<span v-if="getrating.intRating !== 0">
						<i class="fa fa-star rating-star" aria-hidden="true" v-for="n in getrating.intRating" v-cloak></i>
						<i class="fa fa-star-o rating-star" aria-hidden="true" v-for="n in 4 - getrating.intRating" v-cloak></i>
					</span>
					<span class="rating-title" v-if="getrating.count > 0 && getrating.count !== null" v-cloak>@{{ getrating.strRating }}</span>
					<label class="rating-count" v-if="getrating.count > 0 && getrating.count !== null" v-cloak @click="selectTab('review')">@{{ getrating.count }} Review</label>
					<span class="room-info-map">
						<span class="room-info-map-label">
							@if($area_subdistrict !== '')
								<span>{{ $area_subdistrict }}</span>
							@endif

							@if($area_subdistrict !== '' && $area_city !== '')
								<span>, </span>
							@endif

							@if($area_city !== '')
								<span>{{ $area_city }}</span>
							@endif
						</span>
						<br class="visible-xs" style="line-height: 0.5;">
						<span class="room-info-map-icon track-lihat-lokasi-kost" @click="scrollToMap()"><i class="fa fa-map-marker"></i> Lihat Lokasi</span>
					</span>
				</div>
			</div>
			<div class="col-md-2 col-xs-12 room-info-mark">
				@if($is_premium_owner)
				<label class="mark-premium" title="{{ $property_type }} PREMIUM">
					<img :src="icon.premium2" alt="premium2" v-cloak>
					<span>PREMIUM</span>
				</label>
				@endif
				@if(Auth::check())
					@if(Auth::user()->is_owner == 'false')
					<label class="mark-favorite">
						<i class="fa"
						:class="isLoved.state ? 'fa-heart' : 'fa-heart-o'"
						aria-hidden="true"
						@click="sendLove(isLoved)"
						></i>
						<span :title="'Disukai sebanyak ' + isLoved.count + ' kali'" v-cloak> @{{ isLoved.count }}</span>
					</label>
					@else
					<label class="mark-favorite unclickable">
						<i class="fa fa-heart" aria-hidden="true"></i>
						<span :title="'Disukai sebanyak ' + isLoved.count + ' kali'" v-cloak> @{{ isLoved.count }}</span>
					</label>
					@endif
				@else
				<label class="mark-favorite">
					<i class="fa fa-heart-o" aria-hidden="true"
					data-toggle="modal" data-target="#login"
					data-modal-text="untuk menyimpan kos favoritmu"
					></i>
					<span :title="'Disukai sebanyak ' + isLoved.count + ' kali'" v-cloak> @{{ isLoved.count }}</span>
				</label>
				@endif
			</div>
			<div class="col-lg-4 col-md-5 col-xs-12 room-info-media">
				<div class="col-xs-4 media-title">Foto-foto</div>
				@if($has_photo_round)
				<div class="col-xs-4 media-title">Foto 360°</div>
				@endif
				@if(!is_null ($youtube_id) && $youtube_id !== "")
				<div class="col-xs-4 media-title">Video</div>
				@endif
				<div class="clearfix"></div>
				<a id="mediaPhoto" class="col-xs-4 media-photo" title="Foto-foto {{$room_title}}" v-lazy:background-image="'{{ $photo_url['small'] }}'">
					<div class="media-overlay">
						<p class="text-center"><b>{{count($cards)}} foto</b></p>
					</div>
				</a>
				@if($has_photo_round)
				<a id="media360" class="col-xs-4 media-360" title="Foto 360° {{$room_title}}" v-lazy:background-image="'{{ $photo_360['small'] }}'">
					<div class="media-overlay">
					</div>
				</a>
				@endif
				@if(!is_null ($youtube_id) && $youtube_id !== "")
				<a id="mediaVideo" class="col-xs-4 media-video" title="Video {{$room_title}}" v-lazy:background-image="'https://img.youtube.com/vi/{{ $youtube_id }}/mqdefault.jpg'">
					<div class="media-overlay">
					</div>
				</a>
				@endif
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
</div>
