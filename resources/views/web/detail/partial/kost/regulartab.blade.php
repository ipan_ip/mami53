<div id="roomTab" class="container-fluid">
	<div class="container stretch">
		<div class="col-md-8 col-xs-12 stretch">
		</div>
		<div class="col-md-4 room-tab-display hidden-sm hidden-xs">
			<!-- if(is_null(is_updated)) -->
			@if($not_updated == true)
				@if(Auth::check())
					@if(Auth::user()->is_owner == 'false')
					<div class="display-room is-not-updated clickable" data-toggle="modal" data-target="#chatModal">
						<span class="display-title orange"><h2><strong>Tanya Kesediaan Kamar</strong></h2></span>
						<i class="fa fa-pencil update-room" data-toggle="tooltip" title="Edit ketersediaan kamar" v-if="updateRoom.code" v-cloak  @click="openUpdateRoom"></i>
					</div>
					@else
					<div class="display-room is-not-updated">
						<span class="display-title orange"><h2><strong>Tanya Kesediaan Kamar</strong></h2></span>
						<i class="fa fa-pencil update-room" data-toggle="tooltip" title="Edit ketersediaan kamar" v-if="updateRoom.code" v-cloak  @click="openUpdateRoom"></i>
					</div>
					@endif
				@else
					<div class="display-room is-not-updated clickable" data-toggle="modal" data-target="#login" data-modal-text="untuk melaporkan kos ini">
						<span class="display-title orange"><h2><strong>Tanya Kesediaan Kamar</strong></h2></span>
						<i class="fa fa-pencil update-room" data-toggle="tooltip" title="Edit ketersediaan kamar" v-if="updateRoom.code" v-cloak  @click="openUpdateRoom"></i>
					</div>
				@endif

			@elseif($available_room == 1)
			<div class="display-room is-one">
				<span class="display-title">TINGGAL 1 KAMAR </span>
				<i class="fa fa-pencil update-room" data-toggle="tooltip" title="Edit ketersediaan kamar" v-if="updateRoom.code" v-cloak  @click="openUpdateRoom"></i>
			</div>
			@elseif($available_room > 1)
			<div class="display-room is-more">
				<span class="display-title">ADA {{$available_room}} KAMAR </span>
				<i class="fa fa-pencil update-room" data-toggle="tooltip" title="Edit ketersediaan kamar" v-if="updateRoom.code" v-cloak  @click="openUpdateRoom"></i>
			</div>
			@else
			<div class="display-room is-full">
				<span class="display-title">PENUH </span>
				<i class="fa fa-pencil update-room" data-toggle="tooltip" title="Edit ketersediaan kamar" v-if="updateRoom.code" v-cloak  @click="openUpdateRoom"></i>
			</div>
			@endif
		</div>
	</div>
</div>
<div class="clearfix"></div>
