<!-- Fasilitas Kamar -->
<div class="col-md-8 col-xs-12 stretch room-data-fac">
  <div class="fac-room fac-container">
    <div class="col-lg-2 col-md-3 col-xs-12 fac-title">
      <h3><strong>Fasilitas Kamar</strong></h3>
    </div>
    <div class="col-lg-10 col-md-9 col-xs-12 fac-list">
      @if(count($fac_room) > 0)
      @foreach ($fac_room_icon as $key => $fac_room_icon)
      <div class="col-lg-3 col-md-6 col-sm-4 col-xs-12 fac-icon">
        <img v-lazy="'{{$fac_room_icon['photo_url']}}'" alt="{{$fac_room_icon['name']}}">
        <span>{{$fac_room_icon['name']}}</span>
      </div>
      @endforeach
      @else
      <div class="col-lg-3 col-md-6 col-sm-4 col-xs-12 fac-icon">
        <b>-</b>
      </div>
      @endif
    </div>
  </div>
</div> <!-- End Fasilitas Kamar -->
<!-- Luas Kamar -->
<div class="col-md-8 col-xs-12 stretch room-data-fac">
  <div class="fac-size fac-container">
    <div class="col-lg-2 col-md-3 col-xs-12 fac-title">
      <h3><strong>Luas Kamar</strong></h3>
    </div>
    <div class="col-lg-10 col-md-9 col-xs-12 fac-list">
      <div class="col-md-12 fac-text">
        {{$size}}
      </div>
    </div>
  </div>
</div> <!-- End Luas Kamar -->
<!-- Fasilitas Kamar Lainnya -->
@if($fac_room_other !== '' && !is_null ($fac_room_other))
<div class="col-md-8 col-xs-12 stretch room-data-fac">
  <div class="fac-room-other fac-container">
    <div class="col-lg-2 col-md-3 col-xs-12 fac-title">
      <h3><strong>Fasilitas Kamar Lain</strong></h3>
    </div>
    <div class="col-lg-10 col-md-9 col-xs-12 fac-list">
      <div class="col-md-12 fac-text">
        {{$fac_room_other}}
      </div>
    </div>
  </div>
</div>
@endif <!-- End Fasilitas Kamar Lainnya -->
<!-- Fasilitas Kamar Mandi -->
<div class="col-md-8 col-xs-12 stretch room-data-fac">
  <div class="fac-bath fac-container">
    <div class="col-lg-2 col-md-3 col-xs-12 fac-title">
      <h3><strong>Kamar Mandi</strong></h3>
    </div>
    <div class="col-lg-10 col-md-9 col-xs-12 fac-list">
      @if(count($fac_bath) > 0)
      @foreach ($fac_bath_icon as $key => $fac_bath_icon)
      <div class="col-lg-3 col-md-6 col-sm-4 col-xs-12 fac-icon">
        <img v-lazy="'{{$fac_bath_icon['photo_url']}}'" alt="{{$fac_bath_icon['name']}}">
        <span>{{$fac_bath_icon['name']}}</span>
      </div>
      @endforeach
      @else
      <div class="col-lg-3 col-md-6 col-sm-4 col-xs-12 fac-text">
        <b>-</b>
      </div>
      @endif
    </div>
  </div>
</div> <!-- End Fasilitas Kamar Mandi -->
<!-- Fasilitas Kamar Mandi Lainnya -->
@if($fac_bath_other !== '' && !is_null ($fac_bath_other))
<div class="col-md-8 col-xs-12 stretch room-data-fac">
  <div class="fac-bath-other fac-container">
    <div class="col-lg-2 col-md-3 col-xs-12 fac-title">
      <h3><strong>Fasilitas Kamar Lain</strong></h3>
    </div>
    <div class="col-lg-10 col-md-9 col-xs-12 fac-list">
      <div class="col-md-12 fac-text">
        {{$fac_bath_other}}
      </div>
    </div>
  </div>
</div>
@endif <!-- End Fasilitas Kamar Mandi Lainnya -->
<!-- Fasilitas Umum -->
<div class="col-md-8 col-xs-12 stretch room-data-fac">
  <div class="fac-share fac-container">
    <div class="col-lg-2 col-md-3 col-xs-12 fac-title">
      <h3><strong>Fasilitas Umum</strong></h3>
    </div>
    <div class="col-lg-10 col-md-9 col-xs-12 fac-list">
      @if(count($fac_share) > 0)
      @foreach ($fac_share_icon as $key => $fac_share_icon)
      <div class="col-lg-3 col-md-6 col-sm-4 col-xs-12 fac-icon">
        <img v-lazy="'{{$fac_share_icon['photo_url']}}'" alt="{{$fac_share_icon['name']}}">
        <span>{{$fac_share_icon['name']}}</span>
      </div>
      @endforeach
      @else
      <div class="col-lg-3 col-md-6 col-sm-4 col-xs-12 fac-text">
        <b>-</b>
      </div>
      @endif
    </div>
  </div>
</div> <!-- End Fasilitas Umum -->
<!-- Fasilitas Umum Lainnya -->
@if($fac_share_other !== '' && !is_null ($fac_share_other))
<div class="col-md-8 col-xs-12 stretch room-data-fac">
  <div class="fac-share-other fac-container">
    <div class="col-lg-2 col-md-3 col-xs-12 fac-title">
      <h3><strong>Fasilitas Umum Lain</strong></h3>
    </div>
    <div class="col-lg-10 col-md-9 col-xs-12 fac-list">
      <div class="col-md-12 fac-text">
        {{$fac_share_other}}
      </div>
    </div>
  </div>
</div>
@endif <!-- End Fasilitas Umum Lainnya -->
<!-- Fasilitas Parkir -->
<div class="col-md-8 col-xs-12 stretch room-data-fac">
  <div class="fac-park fac-container">
    <div class="col-lg-2 col-md-3 col-xs-12 fac-title">
      <h3><strong>Fasilitas Parkir</strong></h3>
    </div>
    <div class="col-lg-10 col-md-9 col-xs-12 fac-list">
      @if(count($fac_park) > 0)
      @foreach ($fac_park_icon as $key => $fac_park_icon)
      <div class="col-lg-3 col-md-6 col-sm-4 col-xs-12 fac-icon">
        <img v-lazy="'{{$fac_park_icon['photo_url']}}'" alt="{{$fac_park_icon['name']}}">
        <span>{{$fac_park_icon['name']}}</span>
      </div>
      @endforeach
      @else
      <div class="col-lg-3 col-md-6 col-sm-4 col-xs-12 fac-text">
        <b>-</b>
      </div>
      @endif
    </div>
  </div>
</div> <!-- End Fasilitas Parkir -->
<!-- Akses Lingkungan -->
<div class="col-md-8 col-xs-12 stretch room-data-fac">
  <div class="fac-near fac-container">
    <div class="col-lg-2 col-md-3 col-xs-12 fac-title">
      <h3><strong>Akses Lingkungan</strong></h3>
    </div>
    <div class="col-lg-10 col-md-9 col-xs-12 fac-list">
      @if(count($fac_near) > 0)
      @foreach ($fac_near_icon as $key => $fac_near_icon)
      <div class="col-lg-3 col-md-6 col-sm-4 col-xs-12 fac-icon">
        <img v-lazy="'{{$fac_near_icon['photo_url']}}'" alt="{{$fac_near_icon['name']}}">
        <span>{{$fac_near_icon['name']}}</span>
      </div>
      @endforeach
      @else
      <div class="col-lg-3 col-md-6 col-sm-4 col-xs-12 fac-text">
        <b>-</b>
      </div>
      @endif
    </div>
  </div>
</div> <!-- End Akses Lingkungan -->
<!-- Akses Lingkungan Lainnya -->
@if($fac_near_other !== '' && !is_null ($fac_near_other))
<div class="col-md-8 col-xs-12 stretch room-data-fac">
  <div class="fac-near-other fac-container">
    <div class="col-lg-2 col-md-3 col-xs-12 fac-title">
      <h3><strong>Akses Lingkungan Lain</strong></h3>
    </div>
    <div class="col-lg-10 col-md-9 col-xs-12 fac-list">
      <div class="col-md-12 fac-text">
        {{$fac_near_other}}
      </div>
    </div>
  </div>
</div>
@endif <!-- End Akses Lingkungan Lainnya -->
<!-- Keterangan Lain -->
<div class="col-md-8 col-xs-12 stretch room-data-fac">
  <div class="fac-remark fac-container">
    <div class="col-lg-2 col-md-3 col-xs-12 fac-title">
      <h3><strong>Keterangan Lain</strong></h3>
    </div>
    <div class="col-lg-10 col-md-9 col-xs-12 fac-list">
      @if($remarks !== '')
      <div class="col-md-12 fac-text">
        {{$remarks}}
      </div>
      @else
      <div class="col-lg-3 col-md-6 col-sm-4 col-xs-12 fac-text">
        <b>-</b>
      </div>
      @endif
    </div>
  </div>
</div> <!-- End Keterangan Lain -->
<!-- Keterangan Biaya Lain -->
<div class="col-md-8 col-xs-12 stretch room-data-fac">
  <div class="fac-price-remark fac-container">
    <div class="col-lg-2 col-md-3 col-xs-12 fac-title">
      <h3><strong>Keterangan Biaya Lain</strong></h3>
    </div>
    <div class="col-lg-10 col-md-9 col-xs-12 fac-list">
      @if($price_remark !== '')
      <div class="col-md-12 fac-text">
        {{$price_remark}}
      </div>
      @else
      <div class="col-lg-3 col-md-6 col-sm-4 col-xs-12 fac-text">
        <b>-</b>
      </div>
      @endif
    </div>
  </div>
</div> <!-- End Keterangan Biaya Lain -->
<!-- Deskripsi -->
<div class="col-md-8 col-xs-12 stretch room-data-fac">
  <div class="fac-desc fac-container">
    <div class="col-lg-2 col-md-3 col-xs-12 fac-title">
      <h3><strong>Deskripsi {{ $property_type }}</strong></h3>
    </div>
    <div class="col-lg-10 col-md-9 col-xs-12 fac-list">
      @if($description !== '')
      <div class="col-md-12 fac-text">
        {{$description}}
      </div>
      @else
      <div class="col-lg-3 col-md-6 col-sm-4 col-xs-12 fac-text">
        <b>-</b>
      </div>
      @endif
    </div>
  </div>
</div> <!-- End Deskripsi -->