<!-- Review --> 
<div id="review" class="col-md-8 col-xs-12 stretch room-data-review">
  <div class="review-container">
    <div class="col-lg-2 col-md-3 col-xs-12 review-title">
      Review
    </div>
    <!-- Loading Review -->
    <div class="col-lg-10 col-md-9 col-sm-12 col-xs-12" v-if="loading.review">
      <div class="col-lg-4 col-md-12 col-xs-12 review-header">
        <i class="fa fa-star rating-star" aria-hidden="true" v-for="n in getrating.intRating"></i>
        <i class="fa fa-star-o rating-star" aria-hidden="true" v-for="n in 4 - getrating.intRating"></i>
        <span class="review-sum" v-cloak>@{{ getrating.strRating }}</span>
        <label class="review-count" v-if="getrating.count == null" v-cloak>... Review</label>
        <label class="review-count" v-else v-cloak>@{{ getrating.count }} Review</label>
      </div>
      <div class="clearfix"></div>
      <div class="col-md-4 col-xs-6 review-grade">
        <p>Kebersihan</p>
        <i class="fa fa-star rating-star" aria-hidden="true" v-for="n in getrating.all.clean"></i>
        <i class="fa fa-star-o rating-star" aria-hidden="true" v-for="n in 4 - getrating.all.clean"></i>
      </div>
      <div class="col-md-4 col-xs-6 review-grade">
        <p>Keamanan</p>
        <i class="fa fa-star rating-star" aria-hidden="true" v-for="n in getrating.all.safe"></i>
        <i class="fa fa-star-o rating-star" aria-hidden="true" v-for="n in 4 - getrating.all.safe"></i>
      </div>               
      <div class="col-md-4 col-xs-6 review-grade">
        <p>Kenyamanan</p>
        <i class="fa fa-star rating-star" aria-hidden="true" v-for="n in getrating.all.happy"></i>
        <i class="fa fa-star-o rating-star" aria-hidden="true" v-for="n in 4 - getrating.all.happy"></i>
      </div>
      <div class="col-md-4 col-xs-6 review-grade">
        <p>Fasilitas Kamar</p>
        <i class="fa fa-star rating-star" aria-hidden="true" v-for="n in getrating.all.public_facilities"></i>
        <i class="fa fa-star-o rating-star" aria-hidden="true" v-for="n in 4 - getrating.all.public_facilities"></i>
      </div>
      <div class="col-md-4 col-xs-6 review-grade">
        <p>Fasilitas Umum</p>
        <i class="fa fa-star rating-star" aria-hidden="true" v-for="n in getrating.all.room_facilities"></i>
        <i class="fa fa-star-o rating-star" aria-hidden="true" v-for="n in 4 - getrating.all.room_facilities"></i>
      </div>
      <div class="col-md-4 col-xs-6 review-grade">
        <p>Harga</p>
        <i class="fa fa-star rating-star" aria-hidden="true" v-for="n in getrating.all.pricing"></i>
        <i class="fa fa-star-o rating-star" aria-hidden="true" v-for="n in 4 - getrating.all.pricing"></i>
      </div>
      <div class="clearfix"></div>
      <div class="review-list is-loading" v-for="n in 3">
      </div>
    </div> 
    <!-- End Loading Review -->

    <!-- Show Review -->
    <div class="col-lg-10 col-md-9 col-sm-12 col-xs-12" v-else v-cloak>
      <div class="col-lg-4 col-md-12 col-xs-12 review-header">
        <i class="fa fa-star rating-star" aria-hidden="true" v-for="n in getrating.intRating"></i>
        <i class="fa fa-star-o rating-star" aria-hidden="true" v-for="n in 4 - getrating.intRating"></i>
        <span class="review-sum" v-cloak>@{{ getrating.strRating }}</span>
        <label class="review-count" v-if="getrating.result == null" v-cloak>0 Review</label>
        <label class="review-count" v-else-if="getrating.result != null" v-cloak>@{{ getrating.count }} Review</label>
      </div>
      <div class="col-lg-4 col-md-6 col-sm-4 col-xs-12 review-header" v-show="getrating.count > 1">
        <span>Urutkan dari</span>
        <select class="review-sort-btn" v-model="sortrating" @change="seeReview(1, sortrating)">
          <option value="new">Paling Baru</option>
          <option value="last">Paling Lama</option>
          <option value="best">Tertinggi</option>
          <option value="bad">Terendah</option>
        </select>
      </div>

      <div class="col-lg-4 col-md-6 col-sm-4 col-xs-12 review-header" v-if="authCheckAll && getrating.result != null">
        <button class="review-add-btn" @click="addReview" v-if="getrating.result[0].is_me == false">Tambah Rating & Review</button>
      </div>
      <div class="col-lg-4 col-md-6 col-sm-4 col-xs-12 review-header" v-else-if="authCheckAll && getrating.result == null">
        <button class="review-add-btn" @click="addReview">Tambah Rating & Review</button>
      </div>
      <div class="col-lg-4 col-md-6 col-xs-12 review-header" v-else-if="!authCheckAll">
        <button class="review-add-btn" data-toggle="modal" data-target="#login" data-modal-text="untuk mereview kos ini">Tambah Rating & Review</button>
      </div>

      <div class="clearfix"></div>
      <div class="col-md-4 col-xs-6 review-grade">
        <p>Kebersihan</p>
        <i class="fa fa-star rating-star" aria-hidden="true" v-for="n in getrating.all.clean"></i>
        <i class="fa fa-star-o rating-star" aria-hidden="true" v-for="n in 4 - getrating.all.clean"></i>
      </div>
      <div class="col-md-4 col-xs-6 review-grade">
        <p>Keamanan</p>
        <i class="fa fa-star rating-star" aria-hidden="true" v-for="n in getrating.all.safe"></i>
        <i class="fa fa-star-o rating-star" aria-hidden="true" v-for="n in 4 - getrating.all.safe"></i>
      </div>               
      <div class="col-md-4 col-xs-6 review-grade">
        <p>Kenyamanan</p>
        <i class="fa fa-star rating-star" aria-hidden="true" v-for="n in getrating.all.happy"></i>
        <i class="fa fa-star-o rating-star" aria-hidden="true" v-for="n in 4 - getrating.all.happy"></i>
      </div>
      <div class="col-md-4 col-xs-6 review-grade">
        <p>Fasilitas Kamar</p>
        <i class="fa fa-star rating-star" aria-hidden="true" v-for="n in getrating.all.public_facilities"></i>
        <i class="fa fa-star-o rating-star" aria-hidden="true" v-for="n in 4 - getrating.all.public_facilities"></i>
      </div>
      <div class="col-md-4 col-xs-6 review-grade">
        <p>Fasilitas Umum</p>
        <i class="fa fa-star rating-star" aria-hidden="true" v-for="n in getrating.all.room_facilities"></i>
        <i class="fa fa-star-o rating-star" aria-hidden="true" v-for="n in 4 - getrating.all.room_facilities"></i>
      </div>
      <div class="col-md-4 col-xs-6 review-grade">
        <p>Harga</p>
        <i class="fa fa-star rating-star" aria-hidden="true" v-for="n in getrating.all.pricing"></i>
        <i class="fa fa-star-o rating-star" aria-hidden="true" v-for="n in 4 - getrating.all.pricing"></i>
      </div>
      <div class="clearfix"></div>



      <div class="review-list" v-for="rating in getrating.result" v-if="getrating.result !== null" v-cloak>
        <div class="review-top">
          <span class="review-name">@{{rating.name}}</span>
          <span class="review-waiting" v-if="rating.status == 'waiting' && rating.is_me == true || rating.status == 'rejected' && rating.is_me == true">tunggu disetujui</span>
          <span class="review-edit" v-else-if="rating.status == 'live' && rating.is_me == true" @click="editReview">Edit Review</span>
          <span class="review-date">| @{{rating.tanggal}}</span>
          <span><i class="fa fa-star rating-star rating-star-small" aria-hidden="true"></i>@{{ rating.rating }}</span>
        </div>


        <div class="review-content"> <!-- pls dont remove comment mark below, it's for removing white space -->
          @{{rating.content.substring(0, 279)}}<!--
          --><a class="collapsed" href="#hiddenReview" data-toggle="collapse" v-show="rating.content.length >= 280"><!--
            --><span class="if-collapsed">... Selengkapnya </span><!--
            --><span class="if-not-collapsed"></span><!--
          --></a><!--
          --><span id="hiddenReview" class="collapse" v-show="rating.content.length >= 280">@{{rating.content.substring(280, rating.content.length)}}</span>
          <!-- ok its safe now -->
        </div>
        <div class="col-xs-12 stretch review-photo" v-if="rating.photo !== null">
          <div v-for="photo in rating.photo" class="col-xs-6 col-sm-4 col-md-3">
            <a 
              :href="photo.photo_url.large" 
              class="swipebox" 
              :title="getrating.caption + rating.name" 
              v-lazy:background-image="{
                src: photo.photo_url.small,
                error: 'assets/loading/mamikos_placeholder_load.svg',
                loading: 'assets/loading/mamikos_placeholder_load.svg'
              }">
            </a>
          </div>
        </div>
        <div class="col-md-12 col-xs-12 stretch" v-if="rating.status == 'live' && rating.is_me == true">
          <button class="btn-facebook clearfix track-review-share" title="Bagikan reviewmu lewat Facebook" @click="shareReview"><i class="fa fa-facebook-square" aria-hidden="true"></i>&nbsp; Bagikan</button>
        </div>
        <div class="clearfix"></div>

        <div class="review-reply" v-if="rating.reply_show"> 
          <div class="review-heading">
            <span class="review-name">Pemilik Kos</span> <span class="review-date"> | @{{ rating.reply_owner.date }} </span>
          </div>
          <div class="review-content">
            <span class="room-info-title"> @{{ rating.reply_owner.reply }} </span>
          </div>

          <div class="review-footer" v-if="rating.reply_count > 1">
          <!-- <div class="review-footer"> -->
            <button class="btn btn-mamigreen-inverse" @click="showReplies(rating.token)">Lihat semua balasan</button>
          </div>
        </div>


        <div class="review-reply" v-if="rating.can_reply && rating.is_me && rating.reply_count <= 1">
        <!-- <div class="review-reply"> -->
          <a class="reply-show-link" data-toggle="collapse" href="#reply">Balas review</a>
          <div class="collapse" id="reply">
            <textarea class="reply-textarea form-control" name="reply-textarea" v-model="replyContent" placeholder="Tulis balasan anda disini..."></textarea>
            <button class="btn btn-mamigreen-inverse reply-button" 
            @click="sendReplyFromDetail(rating.song_id,rating.review_id)">Kirim Balasan</button>
          </div>
        </div>

      </div>


      <div class="review-page" v-if="getrating.page >= 1">
        <button class="btn btn-mamigreen-inverse" @click="seeReview(getrating.page-1, sortrating)" v-show="getrating.page <= getrating.totalPage && getrating.page > 1">
          <i class="fa fa-chevron-left" aria-hidden="true"></i> Lihat Sebelumnya
        </button>
        <button class="btn btn-mamigreen-inverse" @click="seeReview(getrating.page+1, sortrating)" v-show="getrating.page < getrating.totalPage">
          Lihat Selanjutnya <i class="fa fa-chevron-right" aria-hidden="true"></i>
        </button>
      </div>

    </div> <!-- End Show Review -->
  </div>
</div> <!-- End Review -->