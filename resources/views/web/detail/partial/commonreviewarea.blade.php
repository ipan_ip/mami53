<!-- Add Review Area -->
<div class="container-fluid stretch" v-show="areaReview == true" v-cloak>
	<div class="container review-area">
		<button type="button" class="col-xs-12 review-back btn btn-mamigreen-inverse visible-xs visible-sm" v-on:click="cancelReview">
			<i class="fa fa-chevron-left" aria-hidden="true"></i>&nbsp;&nbsp;Kembali Ke Detail<br>{{ $room_title }}
		</button>
		<button type="button" class="col-md-12 review-back btn btn-mamigreen-inverse hidden-xs hidden-sm" v-on:click="cancelReview">
			<i class="fa fa-chevron-left" aria-hidden="true"></i>&nbsp;&nbsp;Kembali Ke Detail {{ $room_title }}
		</button>
		<div class="clearfix"></div>
		<div class="col-md-6 col-xs-12 stretch">
			<div class="col-md-12 col-xs-12 stretch">
				<div class="col-md-3 col-xs-12">
					Kebersihan:
				</div>
				<div class="col-md-3 col-xs-12">
					<select id="cleanRating" class="select-rating">
						<option value=""></option>
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
					</select>
				</div>
				<div class="col-md-3 col-xs-12">
					Kenyamanan:
				</div>
				<div class="col-md-3 col-xs-12">
					<select id="happyRating" class="select-rating">
						<option value=""></option>
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
					</select>
				</div>
			</div>
			<div class="col-md-12 col-xs-12 stretch">
				<div class="col-md-3 col-xs-12">
					Keamanan:
				</div>
				<div class="col-md-3 col-xs-12">
					<select id="safeRating" class="select-rating">
						<option value=""></option>
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
					</select>
				</div>
				<div class="col-md-3 col-xs-12">
					Harga:
				</div>
				<div class="col-md-3 col-xs-12">
					<select id="pricingRating" class="select-rating">
						<option value=""></option>
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
					</select>
				</div>
			</div>
			<div class="col-md-12 col-xs-12 stretch">
				<div class="col-md-3 col-xs-4">
					Fasilitas Kamar:
				</div>
				<div class="col-md-3 col-xs-12">
					<select id="roomfacRating" class="select-rating">
						<option value=""></option>
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
					</select>
				</div>
				<div class="col-md-3 col-xs-12">
					Fasilitas Umum:
				</div>
				<div class="col-md-3 col-xs-12">
					<select id="publicfacRating" class="select-rating">
						<option value=""></option>
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
					</select>
				</div>
			</div>
		</div>
		<div class="col-md-6 col-xs-12" style="margin-bottom:25px;">
			<div class="col-md-3 col-xs-12 stretch margin-rating">
				Tulis Review:
			</div>
			<textarea minlength="10" maxlength="500" class="col-md-12 col-xs-12 form-control review-input" placeholder="panjang review minimal 20 karakter" v-model="postrating.content"></textarea>

			<div class="col-md-6 col-xs-12 stretch margin-rating">
				<hr>
				<p>Identitas saya sebagai (pilih salah satu)</p>
				<p>
					<input type="radio" name="anonim" v-model="postrating.anonim" value="0"> Sesuai nama Facebook/Google
				</p>
				<p>
					<input type="radio" name="anonim" v-model="postrating.anonim" value="1"><strong> Sebagai Anonim</strong>
				</p>
			</div>

			<div class="col-md-12 col-xs-12 stretch">
				<div class="review-image">
					<div class="col-md-12 col-xs-12 stretch edit-image" v-show="beforeReview == true && resetimgReview == true" @click="editphotoReview">GANTI FOTO REVIEW</div>
					<form id="reviewImage" action="/garuda/media" method="POST" class="dropzone">
							{{ csrf_field() }}
							<input type="hidden" name="media_type" value="review_photo">
							<div class="col-md-12 col-xs-12 stretch add-image dz-default dz-message" v-show="resetimgReview == false">TAMBAHKAN FOTO</div>
							<div class="col-md-12 col-xs-12 stretch reset-image" @click="resetImageReview">ULANGI PILIH FOTO</div>
							<i>*opsional (maks. 3 foto)</i>
							<br><br>
							<div class="col-md-4 col-xs-4 stretch review-photoedit" v-for="(photoall, index) in postrating.photoall">
								<img
									v-lazy="{
										src: photoall.photo_url.large,
										loading: photoall.photo_url.small
									}"
									alt="Foto Review Kost"
									class="review-photoedit">
							</div>
					</form>
					<div id="previewImage" style="display:none;">
						<div class="col-md-4 col-xs-4 stretch dz-preview dz-file-preview">
							<div class="dz-details">
								<div class="dz-filename"><span data-dz-name></span></div>
								<img data-dz-thumbnail />
							</div>
							<div class="dz-progress progress">
								<div class="progress-bar" role="dz-upload progressbar" data-dz-uploadprogress></div>
							</div>
							<div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
							<div class="dz-error-message"><span data-dz-errormessage></span></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
	<footer class="container-fluid stretch footer-action-container is-review">
		<div class="footer-action-body">
			<div class="col-xs-6 btn-mamigreen footer-action footer-right-separator" @click="cancelReview">
				<i class="fa fa-times" aria-hidden="true"></i>
				<span>&nbsp; BATAL</span>
			</div>
			<div class="col-xs-6 btn-mamigreen footer-action footer-left-separator" @click="submitReview">
				<i class="fa fa-check" aria-hidden="true"></i>
				<span>&nbsp; SUBMIT</span>
			</div>
		</div>
	</footer>
</div><!-- End of Add Review Area -->