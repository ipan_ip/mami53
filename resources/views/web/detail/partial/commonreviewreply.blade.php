<!-- Add Review Area -->
<div class="container-fluid stretch" v-show="replyReview == true" v-cloak>
	<div class="container review-area">

		<button type="button" class="col-xs-12 review-back btn btn-mamigreen-inverse visible-xs visible-sm" v-on:click="cancelReview">
			<i class="fa fa-chevron-left" aria-hidden="true"></i>&nbsp;&nbsp;Kembali Ke Detail<br>{{ $room_title }}
		</button>
		<button type="button" class="col-md-12 review-back btn btn-mamigreen-inverse hidden-xs hidden-sm" v-on:click="cancelReview">
			<i class="fa fa-chevron-left" aria-hidden="true"></i>&nbsp;&nbsp;Kembali Ke Detail {{ $room_title }}
		</button>

		<div class="clearfix"></div>

		<div class="review-list" v-if="getreplies.result.token != undefined" v-cloak>
			<div class="review-top">
				<span class="review-name">@{{ getreplies.result.name }}</span>
				<span class="review-waiting" v-if="getreplies.result.status == 'waiting' && getreplies.result.is_me == true || getreplies.result.status == 'rejected' && getreplies.result.is_me == true">tunggu disetujui</span>
				<span class="review-edit" v-else-if="getreplies.result.status == 'live' && getreplies.result.is_me == true" @click="editReview">Edit Review</span>
				<span class="review-date">| @{{ getreplies.result.tanggal }}</span>
				<span><i class="fa fa-star rating-star rating-star-small" aria-hidden="true"></i>@{{ getreplies.result.rating }}</span>
			</div>


			<div class="review-content">
				@{{ getreplies.result.content }}
			</div>
			<div class="col-xs-12 stretch review-photo" v-if="getreplies.result.photo !== null">
				<a :href="photo.photo_url.large" class="col-xs-4 swipebox" :title="getreplies.result.caption + getreplies.result.name" v-for="photo in getreplies.result.photo">
					<img class="full-width stretch" :src="photo.photo_url.medium" alt="foto_review">
				</a>
			</div>
			<div class="col-md-12 col-xs-12 stretch" v-if="getreplies.result.status == 'live' && getreplies.result.is_me == true">
				<button class="btn-facebook clearfix track-review-share" title="Bagikan reviewmu lewat Facebook" @click="shareReview"><i class="fa fa-facebook-square" aria-hidden="true"></i>&nbsp; Bagikan</button>
			</div>
			<div class="clearfix"></div>
		</div>


		<div v-for="reply in getreplies.replies">
			<div class="review-reply" v-if="reply.is_show" > 
				<!-- <div class="review-heading"> -->
				<span class="review-name">@{{ reply.name }}</span> <span class="review-date"> | @{{ reply.time }} </span>
				<div class="review-content">
					<span class="room-info-title"> @{{ reply.content }} </span>
				</div>
			</div>
		</div>

		<div class="review-reply" v-if="getreplies.result.can_reply && getreplies.result.is_me">
			<!-- <div class="review-reply"> -->
			<div id="reply">
				<textarea class="reply-textarea form-control" name="reply-textarea" v-model="replyContent" placeholder="Tulis balasan anda disini..."></textarea>
				<button class="btn btn-mamigreen-inverse reply-button" @click="sendReply">Kirim Balasan</button>
			</div>
		</div>

	</div>
	
	<div class="clearfix"></div>
</div><!-- End of Add Review Area -->