<div id="roomTab" class="container-fluid">
	<div class="container stretch">
		<div class="col-md-8 col-xs-12 stretch">
		</div>
		@if(!isset($apartment_project_id))
		<div class="col-md-4 room-tab-display hidden-sm hidden-xs">
			@if($available_room == 1)
			<div class="display-room is-one">
				<span class="display-title">TINGGAL 1 KAMAR</span>
			</div>
			@elseif($available_room > 1)
			<div class="display-room is-more">
				<span class="display-title">ADA {{$available_room}} KAMAR</span>
			</div>
			@else
			<div class="display-room is-full">
				<span class="display-title">PENUH</span>
			</div>
			@endif
		</div>
		@else
			@if($available_room == 0)
				<div class="col-md-4 room-tab-display hidden-sm hidden-xs">
					<div class="display-room is-full">
						<span class="display-title">UNIT APARTEMEN PENUH</span>
					</div>
				</div>
			@else
				<div class="col-md-4 room-tab-display hidden-sm hidden-xs">
					<div class="display-room is-price">
						<span class="display-title">UNIT APARTEMEN</span>
					</div>
				</div>
			@endif
		@endif
	</div>
</div>
<div class="clearfix"></div>
