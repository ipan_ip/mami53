<div class="container">
	<div class="row">
		<div class="col-md-12 room-info-action">
			@if(Auth::check() && Auth::user()->is_owner == 'false')
			<a href="javascript:void(0)" class="btn-report-room">Laporkan {{ $property_type }}</a>
			@else
			<a href="javascript:void(0)" class="btn-report-room" data-toggle="modal" data-target="#login" data-modal-text="untuk melaporkan kos ini">Laporkan {{ $property_type }}</a>
			@endif
			@if($promotion !== null)
			<div class="promo-container">
				<span class="promo-label">
					<span class="promo-ic-container">
						<span class="promo-ic-wrapper">
							<img src="/assets/icons/ic_kado_putih.png" class="promo-icon" alt="ic_kado_putih">
						</span>
					</span>
					<span class="promo-content">
						<span class="promo-caption-container">
							{{ $promotion->title }}
						</span>
						<span class="promo-link-container track-lihat-promo-kost">
							<a href="javascript:void(0)" class="promo-link btn-promo-link track-kost-promo-open">
								Lihat promo
							</a>
						</span>
					</span>
				</span>
			</div>
			@endif
		</div>


	</div>
</div>

<div class="container room-photo-swiper">
	<div class="swiper-container">
		<div class="swiper-wrapper">
			@foreach ($cards as $key => $card)
			<div class="swiper-slide">
				<img
					title="Sewa {{ preg_replace("/[^A-Za-z0-9.]/", " ", str_replace(["-", "-"], " ", substr($card['description'], 0, 140))) }}"
					alt="Sewa {{ preg_replace("/[^A-Za-z0-9.]/", " ", str_replace(["-", "-"], " ", substr($card['description'], 0, 140))) }}"
					@if($key === 0)
					src="{{ $card['photo_url']['large'] }}"
					@else
					v-lazy="{
						src: '{{ $card['photo_url']['large'] }}',
						loading: '{{ $card['photo_url']['small'] }}'
					}"
					@endif
					>
			</div>
			@endforeach
		</div>
		<div class="swiper-pagination"></div>
		<div class="swiper-button-prev swiper-button-white"></div>
		<div class="swiper-button-next swiper-button-white"></div>
	</div>
</div>
