<div class="col-md-8 col-xs-12 stretch room-info-section">
	<div class="col-md-10 col-xs-12">
		<div class="room-info-title">
			<h1><strong>{{$room_title}}</strong></h1>
		</div>
		<div class="room-info-rating">
			<span v-if="getrating.intRating !== 0">
				<i class="fa fa-star rating-star" aria-hidden="true" v-for="n in getrating.intRating" v-cloak></i>
				<i class="fa fa-star-o rating-star" aria-hidden="true" v-for="n in 4 - getrating.intRating" v-cloak></i>
			</span> 
			<span class="rating-title" v-if="getrating.count > 0 && getrating.count !== null" v-cloak>@{{ getrating.strRating }}</span>
			<!-- <label class="rating-count" v-if="getrating.count == null" v-cloak @click="selectTab('review')">0 Review</label> -->
			<label class="rating-count" v-if="getrating.count > 0 && getrating.count !== null" v-cloak @click="selectTab('review')">@{{ getrating.count }} Review</label>
		</div>
	</div>
	<div class="col-md-2 col-xs-12 room-info-mark">
		@if($is_premium_owner)
		<label class="mark-premium" title="{{ $property_type }} PREMIUM">
			<img :src="icon.premium2" alt="mamikos premium badge" v-cloak>
			<span>PREMIUM</span>
		</label>
		@endif
		@if(Auth::check())
			@if(Auth::user()->is_owner == 'false')
			  <label class="mark-favorite">
			    <i class="fa" 
			    :class="isLoved.state ? 'fa-heart' : 'fa-heart-o'" 
			    aria-hidden="true" 
			    data-toggle="modal"
			    data-target=""
			    @click="sendLove(isLoved)"
			    ></i>
			    <span :title="'Disukai sebanyak ' + isLoved.count + ' kali'" v-cloak> @{{ isLoved.count }}</span>
			  </label>
			@else
	  	<label class="mark-favorite">
		    <i class="fa fa-heart-o unclickable" aria-hidden="true"></i>
		    <span :title="'Disukai sebanyak ' + isLoved.count + ' kali'" v-cloak> @{{ isLoved.count }}</span>
		  </label>
		  @endif
	  @else
	  <label class="mark-favorite">
	    <i class="fa fa-heart-o" aria-hidden="true"
	    data-toggle="modal" data-target="#login" data-modal-text="untuk menyimpan kos favoritmu"></i>
	    <span :title="'Disukai sebanyak ' + isLoved.count + ' kali'" v-cloak> @{{ isLoved.count }}</span>
	  </label>
  	@endif
	</div>
	<div class="col-md-12 col-xs-12 room-info-label">
		@if($is_booking)
		<label class="label-booking" title="{{ $property_type }} ini bisa di-Booking">
			<img :src="icon.booking2" alt="mamikos booking badge" v-cloak>
			<span>BISA BOOKING</span>
		</label>
		@endif
	</div>
</div>
<div class="col-md-8 col-xs-12 room-info-media">
	@if($has_photo_round)
	<div class="col-xs-5 media-title">Foto 360°</div>
	@endif
	@if(!is_null ($youtube_id) && $youtube_id !== "")
	<div class="col-xs-5 media-title">Video</div>
	@endif
	<div class="clearfix"></div>
	@if($has_photo_round)
	<a id="media360" class="col-xs-5 media-360" title="Foto 360° {{$room_title}}" v-lazy:background-image="'{{$photo_360['small']}}'">
	  <div class="media-overlay">
	  	<label>Klik gambar untuk melihat</label>
	  </div>
	</a>
	@endif
	@if(!is_null ($youtube_id) && $youtube_id !== "")
	<a id="mediaVideo" class="col-xs-5 media-video" title="Video {{$room_title}}" v-lazy:background-image="'https://img.youtube.com/vi/{{ $youtube_id }}/mqdefault.jpg'">
	  <div class="media-overlay">
	  	<label>Klik gambar untuk melihat</label>
	  </div>
	</a>
	@endif
</div>
