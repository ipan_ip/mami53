<!-- Deskripsi -->
<div class="col-md-8 col-xs-12 stretch room-data-fac">
  <div class="fac-room fac-container">
    <div class="col-lg-2 col-md-3 col-xs-12 fac-title">
      <h3><strong>Deskripsi</strong></h3>
    </div>
    <div class="col-lg-10 col-md-9 col-xs-12 fac-list">
      @if($description != null)
      <div class="col-md-12 fac-text">
        {!! $description !!}
      </div>
      @else
      <div class="col-lg-3 col-md-6 col-sm-4 col-xs-12 fac-text">
        <b>-</b>
      </div>
      @endif
    </div>
  </div>
</div> <!-- End Deskripsi -->
<!-- Info Unit -->

<div class="col-md-8 col-xs-12 stretch room-data-fac">
  <div class="fac-room fac-container">
    <div class="col-md-12 col-xs-12">
      <div class="col-md-4 col-sm-6 col-xs-12 stretch">
        <div class="col-md-6 col-xs-6 stretch fac-title is-grouped">
          <h3><strong>Tipe Unit</strong></h3>
        </div>
        <div class="col-md-6 col-xs-6 fac-list is-grouped">
          @if(isset($unit_properties['unit_type']) && $unit_properties['unit_type'] != null)
          <span>{{ $unit_properties['unit_type'] }}</span>
          @else
          <span> - </span>
          @endif
        </div>
      </div>
      <div class="col-md-4 col-sm-6 col-xs-12 stretch">
        <div class="col-md-6 col-xs-6 stretch fac-title is-grouped">
          <h3><strong>Luas Unit</strong></h3>
        </div>
        <div class="col-md-6 col-xs-6 fac-list is-grouped">
          @if(isset($size) && $size != null)
          <span>{{ $size }} m<sup>2</sup></span>
          @else
          <span> - m<sup>2</sup></span>
          @endif
        </div>
      </div>
      <div class="col-md-4 col-sm-6 col-xs-12 stretch">
        <div class="col-md-6 col-xs-6 stretch fac-title is-grouped">
          <h3><strong>Kondisi Unit</strong></h3>
        </div>
        <div class="col-md-6 col-xs-6 fac-list is-grouped">
          @if(isset($unit_properties['condition']) && $unit_properties['condition'] != null)
          <span>{{ $unit_properties['condition'] }}</span>
          @else
          <span> - </span>
          @endif
        </div>
      </div>
      <div class="col-md-4 col-sm-6 col-xs-12 stretch">
        <div class="col-md-6 col-xs-6 stretch fac-title is-grouped">
          <h3><strong>Lantai</strong></h3>
        </div>
        <div class="col-md-6 col-xs-6 fac-list is-grouped">
          @if(isset($unit_properties['floor']) && $unit_properties['floor'] != null)
          <span>{{ $unit_properties['floor'] }}</span>
          @else
          <span> - </span>
          @endif
        </div>
      </div>
    </div>

  </div>
</div> <!-- End Info Unit -->

<div class="col-md-8 col-xs-12 stretch room-data-fac">
  <div class="fac-room fac-container">

    <div class="col-md-12 col-xs-12 unit-facility-stretch">
      <div class="col-md-2 col-xs-12 unit-facility-title">
        <label><h3><strong>Fasilitas Unit</strong></h3></label>
      </div>
      <div class="col-md-10 col-xs-12">
        @if(isset($fac_room_icon) && $fac_room_icon != null)
          @foreach($fac_room_icon as $facility)
          <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 stretch unit-facility-caption">
            <img src="{{ $facility['small_photo_url'] }}" class="unit-facility-image" alt="{{ $facility['name'] }}">
            <span class="unit-facility-label">{{ $facility['name'] }}</span>
          </div>
          @endforeach
        @else
        <div class="col-md-3 col-sm-6 col-xs-12 stretch unit-facility-caption">
          <span>-</span>
        </div>
        @endif
      </div>
    </div>
  </div>
</div>


<!-- Project facility -->

<div class="col-md-8 col-xs-12 stretch room-data-fac">
  @if(isset($project) && $project != null)
  <div class="fac-room fac-container col-xs-12 unit-facility-secondary-title">
    <div class="room-info-title">
      <h2 class="unit-project-title"><strong>{{ $project['name'] }}</strong></h2>
    </div>
    <p>{{ $project['address'] }}</p>
  </div>
  @endif
  <div class="fac-room fac-container">
    <div class="col-md-12 col-xs-12 unit-facility-stretch">
      <div class="col-md-2 col-xs-12 unit-facility-title">
        <label><h3><strong>Fasilitas Proyek</strong></h3></label>
      </div>
      <div class="col-md-10 col-xs-12">
        @if(isset($project) && $project != null)
          @foreach($project['facilities'] as $facility)
          <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 stretch unit-facility-caption">
            <img src="{{ $facility['small_photo_url'] }}" class="unit-facility-image" alt="{{ $facility['name'] }}">
            <span class="unit-facility-label">{{ $facility['name'] }}</span>
          </div>
          @endforeach
        @else
        <div class="col-md-3 col-sm-6 col-xs-12 stretch unit-facility-caption">
          <span>-</span>
        </div>
        @endif
      </div>
    </div>
  </div>
</div>



<!-- Fasilitas Kamar Lainnya -->
@if($fac_room_other !== '')
<div class="col-md-8 col-xs-12 stretch room-data-fac">
  <div class="fac-room-other fac-container">
    <div class="col-lg-2 col-md-3 col-xs-12 fac-title">
      <h3><strong>Fasilitas Lainnya</strong></h3>
    </div>
    <div class="col-lg-10 col-md-9 col-xs-12 fac-list">
      <div class="col-md-12 fac-text">
        {{$fac_room_other}}
      </div>
    </div>
  </div>
</div>
@endif <!-- End Fasilitas Kamar Lainnya -->
