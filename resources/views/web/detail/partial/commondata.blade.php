<div id="data" class="container-fluid stretch room-data-container">
  	<div class="container stretch">
	  	@if($is_premium_owner)
	  		@include ('web.detail.partial.commondatapricedesktop')

	  		@if(isset($apartment_project_id))
	  			@include ('web.detail.partial.apartment.regulardatainfo')
	  		@endif

	  		@include ('web.detail.partial.commondatapricemobile')

	  	@else
	  		@include ('web.detail.partial.commondatapricedesktop')

	  		@if(!isset($apartment_project_id))
	  			@include ('web.detail.partial.kost.regulardatainfo')
	  		@else
	  			@include ('web.detail.partial.apartment.regulardatainfo')
	  		@endif

	  		@include ('web.detail.partial.commondatapricemobile')
	  	@endif

	  	@if(!isset($apartment_project_id))
   			@include ('web.detail.partial.kost.commondatafacility')
   			@include ('web.detail.partial.commondatareview')
  		@else
  			@include ('web.detail.partial.apartment.commondatafacility')
  		@endif

	    @include ('web.detail.partial.commondataverif')
	    @include ('web.detail.partial.commondatashare')
  	</div>
</div>