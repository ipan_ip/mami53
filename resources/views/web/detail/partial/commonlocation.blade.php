<!-- Lokasi Kost -->
<div id="location" class="container-fluid room-location-container">
  <div class="container stretch">
    <div class="col-md-12 location-header">
      <span><h2><strong>Peta Lokasi {{ $property_type }}</strong></h2></span>
    </div>
    <div class="col-md-12 col-xs-12 location-info">
      <i class="fa fa-info-circle" aria-hidden="true"></i>
      <span>Untuk mendapatkan <b>detail lokasi {{ $property_type }}</b>, silakan bertanya dengan cara <b>Hubungi {{ $property_type }}</b></span>
    </div>
    <div class="col-md-12 col-xs-12 location-map is-hidden" v-show="!getMap.visible" v-cloak>
      <button class="btn btn-mamigreen" @click="showMap" :disabled="loading.map">
        <span v-if="!loading.map" v-cloak><i class="fa fa-map-marker" aria-hidden="true"></i> &nbsp;&nbsp;Tampilkan Lokasi</span>
        <span v-else v-cloak><i class="fa fa-map-marker" aria-hidden="true"></i> &nbsp;&nbsp;Memuat Peta...</span>
      </button>
    </div>
    <div class="col-md-12 col-xs-12 location-map is-visible" v-show="getMap.visible" v-cloak>
      <div id="map">
      </div>
    </div>
  </div>
</div>
<div class="clearfix"></div>
<!-- End Lokasi Kost -->
