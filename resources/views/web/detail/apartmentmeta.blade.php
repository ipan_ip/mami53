  <?php
    //$meta_title             = "Kost " . $area_city_keyword . " - " . $name_slug;
    $meta_title             =  $name_slug;
    $meta_author            = "https://plus.google.com/u/1/+Mamikos/posts";

  ?>
     <!-- Meta
  ================================================== -->
  <base href="/">
  <title>{{ $meta_title }} - Mamikos </title>

  <meta name="csrf-token" content="{{ csrf_token() }}">

<meta name="application-name" content="Mamikos">
<meta name="theme-color" content="#1BAA56">
<link rel="manifest" href="/json/manifest.json">

@include('web.@meta.link-prebrowsing')

  <meta name="this-id" content="{{$_id}}">
  <meta name="load-time" content="{{date('Y-m-d H:i:s', time())}}">

  @if ($is_indexed == 1 )
  <link rel="canonical" href="{{ $share_url }}" />
  @endif

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

  <meta name="description" content="Sewa {{ $meta_title }}. Sewa langsung dari pemilik. Terlengkap & lebih dari 8 juta database apartemen disewakan, cek sekarang di Mamikos.com">

  <meta name="keywords" content="Sewa {{ $meta_title }} murah, harian, bulanan, tahunan.">
  <meta name="author" content="{{ $meta_author }}">

  <meta property="og:title" content="{{ $meta_title }}" />
  <meta property="og:description" content="Sewa {{ $meta_title }} murah. Dengan alamat di {{ $project['address'] }}. Sewa {{ $meta_title }} murah, harian, bulanan, tahunan. Klik di sini untuk info lengkapnya, hanya di Mamikos.com" />
  <meta property="og:type" content="website" />
  <meta name="og:site_name" content="Mamikos"/>
  <meta property="og:url" content="{{ $share_url }}" />
  <meta property="og:image" content="{{ $photo_url['medium'] }}" />
  <meta property="fb:app_id" content="607562576051242"/>

  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:site" content="@mamikosapp">
  <meta name="twitter:title" content="{{ $meta_title }}">
  <meta name="twitter:description" content="Sewa {{ $meta_title }} murah. Dengan alamat di {{ $project['address'] }}. Sewa {{ $meta_title }} murah, harian, bulanan, tahunan. Klik di sini untuk info lengkapnya, hanya di Mamikos.com">
  <meta name="twitter:creator" content="@mamikosapp">
  <meta name="twitter:image" content="{{ $photo_url['medium'] }}">
  <meta name="twitter:domain" content="mamikos.com">

  <meta name="p:domain_verify" content="1947d14ed84936368c9c2c229d535f39"/>
  <meta name="msvalidate.01" content="8F441EFF8C6571840616F325C34D7698" />
  
  @include('web.@meta.favicon')

  <script>
  window.addEventListener(
    'load',
    function() {
      if (!window.fbq || typeof fbq === 'undefined') {
        window.fbq = function() {
          return null;
        };
      } else {
        var city = detail.area_city;
        var region = detail.area_subdistrict;

        if (city === '' && region === '') {
          city = detail.location.toString();
          region = detail.location.toString();
        }

        dataLayer.push({
          'event': 'ViewProduct',
          'ecommerce': {
            'currencyCode': 'IDR',
            'detail': {
              'actionField': {
                'list': 'Home'
              },
              'products': {
                'name': detail.name_slug,
                'id': detail._id,
                'price': detail.price_title,
                'type': 'hotel',
                'city': city,
                'region': region,
                'category': 'Apartement',
                'brand': detail.name_slug,
              }

            }
          }
        });
      }
    },
    false
  );
  </script>
