<!DOCTYPE html>
<html lang="en">

<title>Password baru Pemilik Kost</title>
<link rel="canonical" href="https://mamikos.com/promo/nonton-gratis" />
<link rel="author" href="https://plus.google.com/u/1/+Mamikos/posts" />
<meta name="csrf-token" content="{{ csrf_token() }}">

<meta name="application-name" content="Mamikos">
<meta name="theme-color" content="#1BAA56">
<link rel="manifest" href="/json/manifest.json">

@include('web.@meta.link-prebrowsing')

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

@include('web.@meta.favicon')

<meta name="classification" content="Business, Rent House, Sewa Kost, Property, Rent Room, Info Kost, Information, Kost, Room, Cari Kost, Kost Murah, Kost Bebas, Application, Mobile Application">
<meta name="distribution" content="global">
<meta http-equiv="Reply-to" content="mamikos.app@gmail.com">
<meta name="revisit-after" content="1 day">
<meta name="keyphrases" content="Info Kost, Cari Kost, Sewa Kost, Kost Bebas, Kost Murah, Kost pasutri, Aplikasi Kost, Aplikasi Pencarian Kost, Aplikasi Info Kost, APlikasi Cari Kost, Kost, Mamikost, Mamikosapp">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="keywords" content="mamikost, Info Kost, Cari kost, kost yogyakarta, kost jogja, kost jakarta, kost bandung, kost depok, kost surabaya, indekost, kos, indekos, sewa">
<meta name="description" content="MAMIKOS adalah aplikasi pencari kost no.1 di Indonesia. MAMIKOS menyediakan layanan pencari kost yang gampang dan cepat serta memiliki data kost yang akurat.">
<meta name="author" content="Mamikos">
<meta name="robots" content="index, follow">
<meta property="fb:app_id" content="607562576051242" />
<meta property="og:title" content="Mamikos - Promo" />
<meta name="og:site_name" content="Mamikos" />
<meta property="og:description" content="MAMIKOS adalah aplikasi pencari kost no.1 di Indonesia. MAMIKOS menyediakan layanan pencari kost yang gampang dan cepat serta memiliki data kost yang akurat." />
<meta property="og:type" content="website" />
<meta property="og:url" content="https://mamikos.com/promo/nonton-gratis" />
<meta property="og:image" content="/assets/share-image-default.jpg" />
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@mamikosapp">
<meta name="twitter:title" content="Mamikos - Promo">
<meta name="twitter:description" content="MAMIKOS adalah aplikasi pencari kost no.1 di Indonesia. MAMIKOS menyediakan layanan pencari kost yang gampang dan cepat serta memiliki data kost yang akurat.">
<meta name="twitter:creator" content="@mamikosapp">
<meta name="twitter:image" content="/assets/share-image-default.jpg">
<meta name="twitter:domain" content="mamikos.com">





<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-K9V8K4" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>
  (function(w, d, s, l, i) {
    w[l] = w[l] || [];
    w[l].push({
      'gtm.start': new Date().getTime()
      , event: 'gtm.js'
    });
    var f = d.getElementsByTagName(s)[0]
      , j = d.createElement(s)
      , dl = l != 'dataLayer' ? '&l=' + l : '';
    j.async = true;
    j.src =
      '//www.googletagmanager.com/gtm.js?id=' + i + dl;
    f.parentNode.insertBefore(j, f);
  })(window, document, 'script', 'dataLayer', 'GTM-K9V8K4');

</script>
<!-- End Google Tag Manager -->

<!--  CSS Syle
    ================================================== -->
@include('web.part.external.global-head')
<link rel="stylesheet" href="{{ mix_url('dist/css/vendor.css') }}">
<style>
  html,
  body {
    font-family: 'Lato', sans-serif;
    height: 100%;
    overflow: hidden;
  }

  @media (max-width: 767px) {

    html,
    body {
      background-color: #039b50;
    }
  }

  @media (min-width: 768px) {

    html,
    body {
      background: url(/assets/100-1-blur.jpg) no-repeat center center fixed;
      -webkit-background-size: cover;
      -moz-background-size: cover;
      -o-background-size: cover;
      background-size: cover;
    }
  }

  .flex-center-row {
    display: -webkit-flex;
    display: -moz-flex;
    display: flex;

    -webkit-align-items: center;
    -moz-align-items: center;
    align-items: center;

    -webkit-justify-content: center;
    -moz-justify-content: center;
    justify-content: center;
  }

  .flex-center-col {
    display: -webkit-flex;
    display: -moz-flex;
    display: flex;

    -webkit-flex-direction: column;
    -moz-flex-direction: column;
    flex-direction: column;

    -webkit-align-items: center;
    -moz-align-items: center;
    align-items: center;

    -webkit-justify-content: center;
    -moz-justify-content: center;
    justify-content: center;
  }

  .login-container {
    padding: 30px 30px 150px 30px;
    background-color: #039b50;
  }

  .loginBoxOwner {
    text-align: center;
  }

  @media (max-width: 767px) {
    .login-container {
      width: 100%;
    }

    .loginBoxOwner a {
      padding: 8px 8px 8px 8px;
      text-decoration: none;
      font-size: 22px;
      color: #f1f1f1;
      display: block;
      transition: 0.3s
    }

    .loginBoxOwner a:hover,
    .offcanvas a:focus {
      color: #818181;
    }

    .loginBoxOwner p {
      padding: 8px 8px 0px 8px;
      font-size: 20px;
      color: #f1f1f1;
      display: block;
      margin-bottom: 0px;
    }

    .loginBoxOwner button {
      padding: 8px 8px 8px 8px;
      text-decoration: none;
      font-size: 22px;
      color: #f1f1f1;
      display: block;
      transition: 0.3s
    }
  }

  @media (min-width: 768px) {
    .login-container {
      width: 384px;
      box-shadow: 0 19px 38px rgba(0, 0, 0, 0.30), 0 15px 12px rgba(0, 0, 0, 0.22);
    }

    .loginBoxOwner a {
      padding: 8px 8px 8px 8px;
      text-decoration: none;
      font-size: 22px;
      color: #f1f1f1;
      display: block;
      transition: 0.3s
    }

    .loginBoxOwner a:hover,
    .offcanvas a:focus {
      color: #818181;
    }

    .loginBoxOwner p {
      padding: 8px 8px 0px 8px;
      font-size: 20px;
      color: #f1f1f1;
      display: block;
      margin-bottom: 0px;
    }

    .loginBoxOwner button {
      padding: 8px 8px 8px 8px;
      text-decoration: none;
      font-size: 22px;
      color: #f1f1f1;
      display: block;
      transition: 0.3s
    }
  }

  label.error {
    color: yellow;
    font-weight: normal;
    font-size: 16px;
  }

</style>

</head>

<body class="flex-center-row">
  <div class="flex-center-col login-container">
    <a href="/">
      <img alt="logo_mamikos" src="/assets/home-desktop/header_mamikos.png" style="height:50px">
    </a>
    <br>
    <div class="loginBoxOwner">
      <form id="loginFormOwner" class="col-xs-12">
        @if($user->phone_number == '' || is_null($user->phone_number))
        <p>Masukkan Nomor HP Anda</p>
        <input type="text" name="phone_number" id="phone_number" required style="text-align:center;font-size:18px;" class="col-xs-12">
        <div class="clearfix"></div>
        <br>
        @endif

        <p>Masukkan password baru</p>
        <input id="n_pass" class="col-xs-12" required type="password" name="n_pass" pattern=".{6,20}" title="Panjang password 6 sampai 20 karakter" style="text-align:center;font-size:18px;">
        <div class="clearfix"></div>
        <br>
        <p>Ulangi password baru</p>
        <input id="v_pass" class="col-xs-12" required type="password" name="v_pass" pattern=".{6,20}" title="Panjang password 6 sampai 20 karakter" style="text-align:center;font-size:18px;">
        <div class="clearfix"></div>
        <br>
        <input id="loginTokenOwner" type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" id="strcode" name="strcode" value="{{ $code }}">
        <button id="loginSubmitOwner" class="col-xs-12 track-login-submit" type="submit" style="font-weight:bold;background-color:#196c18;margin-bottom:15px;">Buat password</button>
        <button id="loginLoadingOwner" class="col-xs-12" type="button" disabled style="font-weight:bold;background-color:#818181;margin-bottom:15px;display:none;">MENUNGGU...</button>
      </form>
    </div>
  </div>


  <!--  JavaScripts
  	================================================== -->
  <script src="https://maps.googleapis.com/maps/api/js?libraries=places&language=id&key={{ENV('MIX_GOOGLE_MAPS_JAVASCRIPT_API_KEY')}}"></script>
  <script src="{{ mix_url('dist/js/vendor.js') }}"></script>
  @include ('web.part.script.bugsnagapikey')
  @include ('web.part.script.tokenerrorhandlescript')

  <script type="text/javascript">
    $("form").submit(function(e) {
      e.preventDefault();
    });

    var csrfToken = $('meta[name=csrf-token]').attr("content");

    document.getElementById("loginSubmitOwner").addEventListener("click", postLoginOwner);

    function postLoginOwner(e) {
      e.preventDefault();
      phone_number = document.getElementById("phone_number") !== null ? document.getElementById("phone_number").value : null;
      n_pass = document.getElementById("n_pass").value;
      v_pass = document.getElementById("v_pass").value;
      strcode = document.getElementById("strcode").value;

      var validator = $("#loginFormOwner").validate();
      var valid = validator.form();

      if (valid) {
        document.getElementById("loginSubmitOwner").style.display = "none";
        document.getElementById("loginLoadingOwner").style.display = "block";
        $.ajax({
          type: "POST"
          , url: "/garuda/auth/storepassword"
          , headers: {
            "Content-Type": "application/json"
            , "X-GIT-Time": "1406090202"
            , "Authorization": "GIT WEB:WEB"
          }
          , data: JSON.stringify({
            "phone_number": phone_number
            , "n_pass": md5(n_pass)
            , "v_pass": md5(v_pass)
            , "strcode": strcode
            , "_token": csrfToken
          })
          , crossDomain: true
          , dataType: 'json'
          , success: function(loginResponse) {
            if (loginResponse.status == true) {
              window.location.replace("/ownerpage");
            } else {
              // alert('Panjang password 6 sampai 20 karakter');
              alert(loginResponse.meta.message);
              document.getElementById("loginLoadingOwner").style.display = "none";
              document.getElementById("loginSubmitOwner").style.display = "block";
            }
          }
        });
      }
    }

  </script>
</body>
