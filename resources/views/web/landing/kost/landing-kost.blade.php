@extends('web.landing.kost.landingKostLayout')

@section('meta-custom')
  <title>{{ $title }}</title>
  <link rel="canonical" href="{{ url(str_replace('http','https',$breadcrumb[sizeof($breadcrumb)-1]['url'])) }}" />
  @if($use_amp == 1)
    <link rel="amphtml" href="https://mamikos.com/amp/kost/{{ $slug }}">
  @endif
  <link rel="alternate" href="android-app://com.git.mami.kos/mamikos/kost/{{ $slug }}" />
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

  @include('web.@meta.favicon')

  <meta name="description" content="{{ $description }}">
  <meta name="keywords" content="{{$keywords}}">
  <meta property="fb:app_id" content="607562576051242"/>

  <meta name="og:site_name" content="Mamikos"/>
  <meta name="og:description" content="{{ $description }}">
  <meta property="og:type" content="website" />
  <meta property="og:url" content="{{ Request::url() }}" />
  <meta property="og:image" content="{{ $image_url }}" />

  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:site" content="@mamikosapp">
  <meta name="twitter:title" content="{{ $heading_1 }} - Mamikos">
  <meta name="twitter:description" content="{{ $description }}">
  <meta name="twitter:creator" content="@mamikosapp">
  <meta name="twitter:image" content="{{ $image_url }}">
  <meta name="twitter:domain" content="mamikos.com">

  <link rel="preload" href="{{ mix_url('dist/js/landing-kost.js') }}" as="script">

  <style>
  #prerender .prerender-navbar {
    background-color: #1BAA56;
    z-index: 1000;
    position: fixed;
    height: 52px;
  }

  #prerender .prerender-header {
    background-color: #fff;
    z-index: 2;
    position: fixed;
    padding: 0;
    padding-top: 80px;
  }

  #prerender .prerender-title {
    color: #1BAA56;
    font-size: 16px;
    font-weight: 700;
    text-transform: uppercase;
  }

  #prerender .prerender-header_right {
    height: 110px;
    padding: 10px;
  }
  @media (max-width: 991px) {
    #prerender .prerender-header_right {
      height: 188px;
    }
  }
  @media (max-width: 767px) {
    #prerender .prerender-header_right {
      height: 120px;
    }
  }

  #prerender .prerender-fill {
    background-color: #eee;
    height: 100%;
    border-radius: 5px;
  }

  #prerender .prerender-map {
    top: 195px;
    height: 68%;
    position: fixed;
  }
  @media only screen and (min-width: 992px) and (max-width: 1151px) {
    #prerender .col-md-5.prerender-map {
      width: 30.666667% !important;
    }
    #prerender .prerender-map {
      right: 0;
    }
    #prerender .col-md-push-7.prerender-map {
      left: auto;
    }
  }

  #prerender .prerender-content {
    margin-top: 195px;
  }
  @media only screen and (min-width: 992px) and (max-width: 1151px) {
    #prerender .col-md-7.prerender-content {
      width: 69.333333% !important;
    }
  }
  @media (max-width: 991px) {
    #prerender .prerender-content {
      margin-top: 0;
      padding: 175px 10px 0 10px;
    }
  }

  #prerender .prerender-list {
    height: 250px;
    margin-bottom: 15px;
  }
  @media (max-width: 991px) {
    #prerender .prerender-list {
      height: 125px;
      margin-top: 155px;
    }
  }
  @media (max-width: 767px) {
    #prerender .prerender-list {
      margin-top: 80px;
    }
  }
  </style>
@endsection

@section('data')
  <script>
    var relatedKostLink = null;
    var relatedApartmentLink = @JZON($related_apartment_link);
    var relatedVacancyLink = @JZON($related_vacancy_link);

    var landingRelatedArea = @JZON($landingRelatedsArea);
    var landingRelatedCampus = @JZON($landingRelatedsCampus);

    var description1 = @JZON($description_1);
    var description2 = @JZON($description_2);
    var description3 = @JZON($description_3);
    var description = {
      description_1: description1,
      description_2: description2,
      description_3: description3
    };

    var genderType = @JZON($gender);
    var rentType = @JZON($rent_type);

    var priceMin = @JZON($price_min);
    var priceMax = @JZON($price_max);
    var priceRange = [priceMin, priceMax];

    var tagIds = @JZON($tag_ids);
    var faqs = @JZON($faq);
    var slug = @JZON($slug);

    var longitude_1 = @JZON($longitude_1);
    var latitude_1 = @JZON($latitude_1);
    var longitude_2 = @JZON($longitude_2);
    var latitude_2 = @JZON($latitude_2);
    var locPage = [[longitude_1, latitude_1],[longitude_2, latitude_2]];
    var cornerMap = [[latitude_1, longitude_1], [latitude_2, longitude_2]];

    var centerLatitude = @JZON($centerLatitude);
    var centerLongitude = @JZON($centerLongitude);

    var landing_connector = @JZON($landing_connector);
    var breadcrumb = @JZON($breadcrumb);
    var place = @JZON($place);
    var radius = @JZON($radius);
  </script>

  <script type="application/ld+json">
    {
      "@context": "http://schema.org",
      "@type": "BreadcrumbList",
      "itemListElement": [
        @foreach ($breadcrumb as $key => $item)
          {
            "@type": "ListItem",
            "position": {{ $key + 1 }},
            "item": {
              "@id": "{{ $item["url"] }}",
              "name": "{{ $item["name"] }}"
            }
          }
          @if ($key != count($breadcrumb) - 1)
          ,
          @endif
        @endforeach
      ]
    }
  </script>

  <script type="application/ld+json">
    {
      "@context" : "http://schema.org",
      "@type" : "LodgingBusiness",
      "hasMap" : "https://maps.google.com/maps?z=7&q={{ $centerLatitude }},{{ $centerLongitude }}",
      "url" : "https://mamikos.com/kost/{{ $slug }}",
      @if (!is_null($price_min) && !is_null($price_max))
      "priceRange" : "Rp {{ $price_min }} - Rp {{ $price_max }}",
      @endif
      "name" : "{{ $keyword }}",
      "image": "{{ $image }}",
      "telephone": "0856-4140-5202"
    }
  </script>
@endsection

@section('content')
  {{-- Prerendered Priority Contents --}}
  <div id="prerender">
    <section>
      <div class="prerender-navbar col-md-12 col-sm-12 col-xs-12"></div>
    </section>

    <section>
      <div class="prerender-header col-md-12 col-sm-12 col-xs-12">
        <div class="prerender-header_left col-md-2 col-sm-12">
          <h1 class="prerender-title">{{ $breadcrumb[sizeof($breadcrumb)-1]['name'] }}</h1>
        </div>
        <div class="prerender-header_right col-md-10 col-sm-12 col-xs-12">
          <div class="prerender-fill"></div>
        </div>
      </div>
    </section>

    <section>
      <div class="prerender-content col-md-7">
        <div class="prerender-list">
          <div class="prerender-fill"></div>
        </div>
        <div class="prerender-description">{!! $description_1 !!}</div>
        <div class="prerender-description">{!! $description_2 !!}</div>
        <div class="prerender-description">{!! $description_3 !!}</div>

      </div>
      <div class="prerender-map col-md-5 col-md-push-7 hidden-xs hidden-sm">
        <div class="prerender-fill"></div>
      </div>
    </section>
  </div>

  {{-- Complete Content After JS Rendered --}}
  <div id="app">
    <app></app>
  </div>
@endsection

@section('script')
  @include('web.@script.global-auth')

  @if (Auth::check() and Auth::user()->is_owner == 'false')
      @include('web.@script.sendbird-widget')
  @endif

  <script src="{{ mix_url('dist/js/landing-kost.js') }}" async defer></script>
@endsection
