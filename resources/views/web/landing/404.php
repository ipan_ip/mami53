<!DOCTYPE html>
<html>
  <head>
     <!-- Meta
  ================================================== -->
    <title>Maaf Halaman Tidak Ditemukan</title>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="google-site-verification" content="W_MCejRQlVQpaPkxxnlcCmvDD8J2Vt55yCrxZx4OnyA" />
    <link rel="shortcut icon" href="/general/img/manifest/favicon.ico">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="Halaman Tidak Ditemukan. Mau cari info kost di area Jogja, Jakarta, Surabaya, atau di seluruh Indonesia? Coba Mamikos App. Cari Kost Jogja, Kost Bandung, Kost Jakarta, Kost Surabaya, Kost Depok, Makin Gampang!">
    <meta name="author" content="https://plus.google.com/u/1/+Mamikos/posts">
    <meta name="robots" content="noindex,nofollow">
  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries
  ================================================== -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
  <![endif]-->

  <!-- Stylesheet
  ================================================== -->
    
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <!--lokal
    <link href="css/bootstrap.min.css" rel="stylesheet">-->

    <!-- FLAT UI -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/flat-ui/2.2.2/css/flat-ui.min.css" rel="stylesheet">
    <!--lokal
    <link href="css/flat-ui.min.css" rel="stylesheet">-->

     <!-- Range Slider-->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.1.1/css/ion.rangeSlider.min.css" rel="stylesheet">
    <!-- <link rel="stylesheet" type="text/css" href="https://
mamikos.com/css/ion.rangeSlider.skinModern.css">-->
    <!--lokal
    <link href="css/ion.rangeSlider.css" rel="stylesheet">-->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.1.1/css/ion.rangeSlider.skinModern.min.css" rel="stylesheet">

    <!-- Fonts -->
    <link rel="preload" href="/css/fonts/lato.css" as="style">
    <link rel="stylesheet" href="/css/fonts/lato.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!--  JavaScripts
  ================================================== -->
    <!-- Jquery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    <!-- Angular -->
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.7/angular.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.7/angular-resource.min.js"></script>
    <!--lokal
    <!-- GMAPS -->
    <script src="https://maps.googleapis.com/maps/api/js?libraries=places&language=id&key=AIzaSyBKrYUJWbB3062-ReMevtzqPLWv9IPm56U"></script>
    <script src="https://cdn.rawgit.com/allenhwkim/angularjs-google-maps/master/build/scripts/ng-map.min.js"></script>

    <!-- Bootstrap -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

  <!-- Style 
  ==================================================== -->
  <style>
  body {
      font-family: 'Lato', sans-serif;
    }
  .main{
    text-align: center;
  }
  .navbar-custom {
    background-color: #25AF6B;
    border-radius: 0px;
    margin-bottom: 0px;
    height:60px;
  }
  .brand-text{
    color: #ffffff;
    font-weight: bold;
    font-size: 25px;
    position: relative;
    top:-8px;
  }
  .brand-text-right{
    color: #ffffff;
    font-weight: bold;
  }
  .brand-text-right:hover{
    color: #1BAA56;
    font-weight: bold;
  }
  .nav-brand{
    padding: 5px;
  }
  .tambahkanKos{
    top:25px;
    padding:5px 15px;
    border:1px solid white;
    color:white;
    float:right;
    cursor: pointer;
  }
  .logo{
    width:70px;
    position: relative;
    top: 0px;
  }
  .foot1{
    font-size:25px;
    font-weight:bold
  }
  .foot2{
    font-size:18px;
  }
  .foot3{
    top:25px;
    border:1px solid white;
    color:white;
    cursor: pointer;
    float:none;
    position:initial;
    font-size:18px;
    padding:10px;
  }
  a{
    color:#D4D4D4;
  }
  .logo{
    width:50px;
  }
  .urllanding{
    cursor: pointer;
    font-size: 12px;
  }
  .urllanding:hover{
    font-weight: bold;
  }
  .urllandingArea{
    cursor: pointer;
    font-size: 13px;
  }
  .urlactive:hover{
    font-weight: bold;
    color:green;
  }
  .form_search {
    position: inherit;
    width: 30%;
    margin: 0 auto;
    padding: 0px 0px 0px 0px;
       
  }
  .form_search input[type=search] {
    width: 100%;
    margin-right: -40px;
    padding: 0px 20px;
    font-size: 15px;
    border: 1px solid #006634;
    border-radius: 40px;
    text-align: center;
    box-sizing: border-box;
      
  }
  .city-landing{
    background-color: #25AF6B;
    color: white;
    border-radius: 20px;
    padding: 5px;
    margin-left: 5px;
    margin-bottom: 10px;
  }
  .jobsabout{
    padding:5px 15px;
    border:1px solid white; 
    color:white;
    cursor: pointer;
    font-size: 18px;
  }
  </style>





<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K9V8K4"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->


</head>
<body ng-app="myApp" ng-controller="notfoundCtrl">
  <!-- NAVBAR-->
  <nav class="navbar navbar-default navbar-custom navbar-fixed-top">
  <div class="container-fluid">
  <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a href="/" class="navbar-brand" id="homepage" style="cursor:pointer;"><span class="brand-text">
         <img src="/assets/header_logo_desktop.png" class="logo"></img>
         Mami<b>Kos</b></span></a>
    </div>

  <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li class="menu-top active" style="cursor:pointer;margin-top: 0px;"><a href="/cari" class="menu-top" id="carikos" title="Cari Kos pada Info MamiKos"><span class="brand-text-right">Cari Kos</span></a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
  </nav>

  <div class="container-fluid" style="margin-top:100px;margin-bottom:100px;">
    <div class="row">
      <div class="col-xs-12 main">
        <span style="font-size:25px;font-weight:bold;">Maaf..</span>
        <h1 style="font-size:25px;margin-bottom: 35px;">Halaman Tidak Ditemukan</h1>
        <img style="width:150px;margin-bottom: 35px;" src="/assets/mamikos_failed.png"></img><br>
        <span style="font-size:20px;font-weight:bold;">Mungkin halaman telah dihapus atau penulisan url keliru</span><br>
        <span style="font-size:20px;font-weight:bold;">Cari Info Kost lain di :</span><br><br>
        <div class="col-xs-12">
          <div class="col-md-1 col-xs-12">
          </div>
          <a href="/kost/kost-jakarta-murah">
            <div class="col-md-2 col-xs-12 city-landing">
              Jakarta
            </div>
          </a>
          <a href="/kost/kost-depok-murah">
          <div class="col-md-2 col-xs-12 city-landing">
            Depok
          </div>
          </a>
          <a href="/kost/kost-jogja-murah">
          <div class="col-md-2 col-xs-12 city-landing">
            Jogja
          </div>
          </a>
          <a href="/kost/kost-bandung-murah">
          <div class="col-md-2 col-xs-12 city-landing">
            Bandung
          </div>
          </a>
          <a href="/kost/kost-surabaya-murah">
          <div class="col-md-2 col-xs-12 city-landing" style="margin-bottom: 40px;">
            Surabaya
          </div>
          </a>
          <div class="col-md-1 col-xs-12">
          </div>
        </div><br><br><br>
        <form ng-submit="searchkeyword()">
          <div class="form_search visible-lg visible-md navRightSearch">
            <input type="search" class="form-control col-xs-8" id="search" placeholder="Cari Apa Aja Disini!" ng-model="address" style="max-width:800px; vertical-align:middle; display:inline-block;">
            <button type="submit" style="display:none;"></button>
          </div><br><br>
          <div class="form_search navRightSearch hidden-lg hidden-md" style="width:100%;">
            <input type="search" class="form-control col-xs-8" id="search" placeholder="Cari Apa Aja Disini!" ng-model="address" style="max-width:800px; vertical-align:middle; display:inline-block;">
            <button type="submit" style="display:none;"></button>
          </div><br><br>
        </form>
        <a style="color:#006634;font-weight:bold;"href="mailto:saran@mamikos.com"><i class="fa fa-envelope-o"></i>&nbsp; Laporkan ke saran@mamikos.com!</a>
      </div>
    </div>
  </div>
  <div class="list-group-item" style="padding:0px;border: 0px;">
    <div class="col-xs-12" style="text-align: center;padding: 50px 0px;background-color:#2D2D2D; color:white;border-bottom:2px solid #464646;">
        <span class="foot1">Apakah Anda Pemilik Kos-Kosan?</span><br>
        <span class="foot2">Promosikan kost anda di Mamikos.com agar lebih dikenal.</span><br><br>
        <a href="/form"><span class="foot3"><i class="fa fa-plus"></i>&nbsp;&nbsp;&nbsp;Tambahkan Kos mu</span></a>
    </div>
  </div>
  <div class="col-xs-12" style="padding:0px;background-color:#2D2D2D;color:white;text-align:center;">
      <div class="col-xs-12" style="margin-top: 30px;margin-bottom: 30px;text-align:justify;">
        <div class="col-xs-12" style="text-align:center;">
          <span style="font-size:22px;">Mamikos - Aplikasi Pencari Info Kost No 1 Di Indonesia</span><br>
        </div><br>
        <br><p style="font-weight: 100;font-size:15px;">MAMIKOS menyajikan data kost dengan detail dan foto fasilitas kost yang lengkap, mulai dari fasilitas kamar kost, kamar mandi,  fasilitas umum.
Setiap Kost yang ada di data kost di MAMIKOS benar-benar kami datangi,  kami verifikasi, kami seleksi dan kami ambil data langsung. 
Informasi ketersediaan kamar kost dan harga kost kami update max setiap 2 minggu sekali untuk memastikan data kost kami akurat.
Kami memiliki lebih dari 1000+ data kost dan terus bertambah di Indonesia, terdiri dari area Depok, Yogyakarta, Jakarta, Bandung, dan Surabaya.</p>
      </div>
      <div class="col-md-6 col-xs-12" style="text-align: left;margin-bottom: 30px;">
        <!-- <div class="col-xs-12" style="margin-bottom:40px;">
          <img class="col-xs-12" src="https://
mamikos.com/uploads/cache/data/style/2016-04-19/KvJXaelR-540x720.jpg">
        </div> -->
        <span>Apa yang bisa kamu dapatkan dari Aplikasi Mamikos:</span>
        <ul style="font-weight: 100;font-size:15px;">
          <li>Alamat Kost Akurat</li>
          <li>Foto-foto lengkap fasilitas kost</li>
          <li>Harga Kost yang selalu update</li>
          <li>Info ketersediaan kamar kost yang selalu update</li>
          <li>Hubungi Kost via Whatsapp</li>
          <li>Hubungi Kost via Pesan</li>
          <li>Google Map untuk membantu navigasi kamu ke kost</li>
          <li>Halaman filter dan pembanding untuk membandingkan kost</li>
        </ul>
      </div>
      <div class="col-md-6 col-xs-12" style="text-align: left;margin-bottom: 30px;">
        <!-- <div class="col-xs-12" style="margin-bottom:40px;">
          <img class="col-xs-12" src="https://
mamikos.com/uploads/cache/data/style/2016-04-19/NOTcN3NI-540x720.jpg">
        </div> -->
        <span>Fitur Pencarian Mamikos:</span>
        <ul style="font-weight: 100;font-size:15px;">
          <li>Cari kost dekat Kampus/Universitas di masing-masing kota</li>
          <li>Cari kost di Jogja, Depok, Jakarta, Surabaya, Bandung, dan Kota besar lainnya</li>
          <li>Cari kost di sekitar lokasi saya saat ini</li>
          <li>Fitur berlangganan (dapatkan update info kost yang kamu inginkan lewat email)</li>
          <li>Fitur favorit kost (simpan kost yang kamu suka)</li>
          <li>Fitur Histori/Kunjungi (cek data-data kost yang pernah kamu kunjungi)</li>
        </ul>
      </div>
    </div>
  <div class="list-group-item" style="padding:0px;background-color:#2D2D2D;color:white;border: 0px;">
    <div class="col-xs-12" style="padding:0px;background-color:#2D2D2D;color:white;text-align:center;">
      <br>
      <div class="hidden-xs">
        <img style="width:220px;"  src="/assets/lplogo.png">
      </div>
      <div class="visible-xs">
        <img style="width:150px;" src="/assets/lplogo.png">
      </div>
      <br><br>
      <a href="http://jobs.mamikos.com" style="color:white;"><span class="jobsabout col-md-2 col-md-offset-4 col-xs-4 col-xs-offset-2" style="margin-right:10px;">Jobs Mamikos</span></a>
      <a href="/tentang-kami" style="color:white;"><span class="jobsabout col-md-2 col-xs-4" style="margin-right:10px;">About Us</span></a>
      <br>
      <h2 class="col-xs-12" style="font-size:15px;"><strong>Dapatkan "info kost murah" hanya di Mamikos App. Mau "Sewa Kost Murah"? Download Mamikos App Sekarang!</strong></h2>
      <br>
    </div>
    <div class="col-xs-12" style="padding:0px;background-color:#2D2D2D;color:white;">
      <div class="col-xs-12 link-mamikos" style="padding:0px;background-color:#2D2D2D;color:white;text-align:center; display:inline-block; border-top:2px solid whitesmoke;font-size: 15px;">
        <a href="https://wa.me/6281929749399"><i class="fa fa-phone-square"></i>&nbsp; 0819-2974-9399 (Whatsapp Only)</a><div class="visible-xs visible-sm"><br></div>
        &nbsp;<a href="mailto:saran@mamikos.com"><i class="fa fa-envelope-o"></i>&nbsp; saran@mamikos.com</a>
        &nbsp;<a href="https://www.facebook.com/Mamikos-Cari-Kos-Gampang-1043376389026435/?fref=ts"><i class="fa fa-facebook-square"></i>&nbsp; mamikos - cari kos gampang</a>
        &nbsp;<a href="https://www.instagram.com/mamikosapp/"><i class="fa fa-instagram"></i>&nbsp; mamikosapp</a>
        &nbsp;<a href="https://twitter.com/mamikosapp"><i class="fa fa-twitter-square"></i>&nbsp; mamikosapp</a>
        &nbsp;<a href="http://line.me/ti/p/V-6yV1X-28"><img src="/assets/icon-line.png" alt="icon line mamikos" style="width:20px;">&nbsp; mamikosapp</a>
      <br>
      <br>
      </div>
    </div>
  </div>
<script src="/js/404.js?v=1"></script>
</body>
</html>
