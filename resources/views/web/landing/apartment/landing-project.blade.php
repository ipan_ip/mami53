@extends('web.landing.apartment.landingApartmentLayout')

@section('meta-custom')
	<title>{{ $metaTitle }} - Mamikos</title>

	<meta name="description" content="Sewa {{ $apartmentProject['name'] }} di {{ $apartmentProject['city'] }}. Sewa langsung dari pemilik, Terlengkap & lebih dari 8 juta database apartemen murah di {{ $apartmentProject['city'] }}, cek sekarang di Mamikos.com" />

	<meta name="keywords" content="	sewa {{ strtolower($apartmentProject['name']) }}, sewa {{ strtolower($apartmentProject['name']) }} murah, harga sewa {{ strtolower($apartmentProject['name']) }}, sewa {{ strtolower($apartmentProject['name']) }} harian, sewa {{ strtolower($apartmentProject['name']) }} mingguan, sewa {{ strtolower($apartmentProject['name']) }} bulanan, sewa {{ strtolower($apartmentProject['name']) }} tahunan, sewa {{ strtolower($apartmentProject['name']) }} fully furnished, sewa {{ strtolower($apartmentProject['name']) }} studio" />

	<meta property="fb:app_id" content="607562576051242"/>
	<meta name="og:site_name" content="Mamikos"/>
	<meta property="og:title" content="{{$metaTitle}} - Mamikos" />
	<meta property="og:description" content="Sewa {{ $apartmentProject['name'] }} {{ $apartmentProject['city'] }}. Lihat harga Sewa {{ $apartmentProject['name'] }}. Disewakan {{ $apartmentProject['name'] }} di {{ $apartmentProject['city'] }}. Simak Infonya di aplikasi Mamikos. Download sekarang! Gratis!" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="{{ Request::url() }}" />
	<meta property="og:image" content="/assets/og/og_apartemen.jpg" />
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:site" content="@mamikosapp">
	<meta name="twitter:title" content="{{ $metaTitle }} - Mamikos">
	<meta name="twitter:description" content="Sewa {{ $apartmentProject['name'] }} {{ $apartmentProject['city'] }}. Lihat harga Sewa {{ $apartmentProject['name'] }}. Disewakan {{ $apartmentProject['name'] }} di {{ $apartmentProject['city'] }}. Simak Infonya di aplikasi Mamikos. Download sekarang! Gratis!">
	<meta name="twitter:creator" content="@mamikosapp">
	<meta name="twitter:image" content="/assets/og/og_apartemen.jpg">
	<meta name="twitter:domain" content="mamikos.com">

	<link rel="preload" href="{{ mix_url('dist/js/landing-project.js') }}" as="script">

	<style>
		#prerender {
			min-height: 80vh;
		}
		#prerender .container {
			margin-top: 120px;
		}
		#prerender .prerender-title {
			color: #1BAA56;
			font-size: 24px;
			font-weight: bold;
			margin-bottom: 10px;
		}
		#prerender .prerender-subtitle {
			display: inline;
			font-size: 14px;
			font-weight: 700;
		}
		#prerender .prerender-subtitle.--separator {
			display: inline;
			color: #eee;
			font-weight: normal;
			text-align: center;
		}
		#prerender .prerender-placeholder {
			height: 80vh;
			width: 100%;
			position: relative;
		}
		#prerender .prerender-placeholder .prerender-placeholder-child {
			border-radius: 5px;
			background-color: #e2e6ea;
			margin: 30px 15px;
		}
		#prerender .prerender-placeholder .prerender-placeholder-child.--1 {
			height: 100px;
		}
		#prerender .prerender-placeholder .prerender-placeholder-child.--2 {
			height: 300px;
		}
		#prerender .prerender-description {
			padding: 15px;
		}
	</style>
@endsection

@section('data')
	<script type="text/javascript">
		var mamiApartmentProject = @JZON($apartmentProject);
		var mamiApartmentSuggestion = @JZON($apartmentSuggestion);
	</script>

	<script>
		var dataLayerParams;

		try {
			dataLayerParams = new URLSearchParams(window.location.search);
		} catch (e) {
			dataLayerParams = null;
		};

    var dataLayerProps = {
      device: "none",
      utm_source: "none",
      utm_medium: "none",
      utm_content: "none",
      utm_campaign: "none",
      price_day: "0",
      price_week: "0",
      price_month: "0",
      type_room: "none",
      rating: "none"
    };

    if (window.innerWidth < 768) {
      dataLayerProps.device = "mobile";
    } else {
      dataLayerProps.device = "desktop";
    };

    try {
      dataLayerProps.utm_source = dataLayerParams.get('utm_source');
      if (dataLayerProps.utm_source === null) dataLayerProps.utm_source = "none";
    } catch (e) {
      dataLayerProps.utm_source = "none";
    };

    try {
      dataLayerProps.utm_medium = dataLayerParams.get('utm_medium');
      if (dataLayerProps.utm_medium === null) dataLayerProps.utm_medium = "none";
    } catch (e) {
      dataLayerProps.utm_source = "none";
    };

    try {
      dataLayerProps.utm_content = dataLayerParams.get('utm_content');
      if (dataLayerProps.utm_content === null) dataLayerProps.utm_content = "none";
    } catch (e) {
      dataLayerProps.utm_content = "none";
    };

    try {
      dataLayerProps.utm_campaign = dataLayerProps.dataLayerParams.get('utm_campaign');
      if (dataLayerProps.utm_campaign === null) dataLayerProps.utm_campaign = "none";
    } catch (e) {
      dataLayerProps.utm_campaign = "none";
    };

		
    dataLayer.push({
      "language": "id",
      "page_category": "ImpressionUnit",
      "currency": "IDR",
      "device": dataLayerProps.device,
      "utm_source": dataLayerProps.utm_source,
      "utm_medium": dataLayerProps.utm_medium,
      "utm_content": dataLayerProps.utm_content,
      "utm_campaign": dataLayerProps.utm_campaign,
      "page_url": mamiApartmentProject.share_url,
      "country": "Indonesia",
      "city": mamiApartmentProject.city,
      "area": mamiApartmentProject.subdistrict,
      "name": mamiApartmentProject.name,
      "image_url": mamiApartmentProject.photos[0].photo_url.medium,
      "price_day": dataLayerProps.price_day,
      "price_week": dataLayerProps.price_week,
      "price_month": dataLayerProps.price_month,
      "type_room": dataLayerProps.type_room,
      "rating": dataLayerProps.rating,
      @if (Auth::check())
      "customer_id": "{{ (Auth::user()->id) }}",
      "profile_id": "{{ (Auth::user()->id) }}",
      "gender": "{{ (Auth::user()->gender) }}"
      @else
      "customer_id": "none",
      "profile_id": "none",
      "gender": "none"
      @endif
    });
	</script>
@endsection

@section('content')
	{{-- Prerendered Priority Contents --}}
	<div id="prerender">
		<div class="container">
			<section>
			  <h1 class="prerender-title"><strong>{{ $apartmentProject['name'] }} - {{ $apartmentProject['city'] }}</strong></h1>
			</section>
			<section>
			  <h2 class="prerender-subtitle"><strong>List Unit di {{ $apartmentProject['name'] }} - {{ $apartmentProject['city'] }}</strong></h2>
			</section>
			<section>
				<div class="prerender-placeholder">
					<div class="prerender-placeholder-child --1"></div>
					<div class="prerender-placeholder-child --2"></div>
				</div>
			</section>
			<section>
				<div class="prerender-description">
					{!! $apartmentProject['description'] !!}
				</div>
			</section>
		</div>
	</div>

	{{-- Complete Content After JS Rendered --}}
	<div id="app">
		<app></app>
	</div>
@endsection

@section('script')

	<script src="{{ mix_url('dist/js/landing-project.js') }}"></script>

	<script type="application/ld+json">
	  {
	    "@context": "http://schema.org",
	    "@type": "BreadcrumbList",
	    "itemListElement": [
	      @foreach ($apartmentProject['breadcrumb'] as $key => $breadcrumb)
		      {
		        "@type": "ListItem",
		        "position": {{ $key + 1 }},
		        "item": {
		          "@id": "{{ $breadcrumb["url"] }}",
		          "name": "{{ $breadcrumb["name"] }}"
		        }
		      }
		      @if ($key != count($apartmentProject['breadcrumb']) - 1)
		      ,
		      @endif
	      @endforeach
	    ]
	  }
	</script>

	<script type="application/ld+json">
	  {
	    "@context" : "http://schema.org",
	    "@type" : "LodgingBusiness",
	    "address" : {
	    	@if ($apartmentProject['subdistrict'] == "")
        "streetAddress" : "{{ $apartmentProject['city'] }}, Indonesia",
        @elseif ($apartmentProject['city'] == "")
        "streetAddress" : "{{ $apartmentProject['subdistrict'] }}, Indonesia",
        @else
        "streetAddress" : "{{ $apartmentProject['subdistrict'] }}, {{ $apartmentProject['city'] }}, Indonesia",
        @endif
        "addressLocality" : "{{ $apartmentProject['subdistrict'] }}",
        "addressRegion" : "{{ $apartmentProject['city'] }}",
        "addressCountry" : "Indonesia",
        "@type" : "PostalAddress"
      },
      "telephone": "0856-4140-5202",
	    "url" : "{{ $apartmentProject['share_url'] }}",
	    @if (!is_null($apartmentProject['unit_price_min']) && !is_null($apartmentProject['unit_price_max']))
	    "priceRange" : "Rp{{ $apartmentProject['unit_price_min'] }} - Rp{{ $apartmentProject['unit_price_max'] }}",
	    @endif
	    "name" : "{{ $apartmentProject['name'] }} {{ $apartmentProject['city'] }}",
	    @if (!empty($apartmentProject['photos']))
	    "image" : "{{ $apartmentProject['photos'][0]['photo_url']['medium'] }}"
	    @else
	    "image" : "https://mamikos.com/assets/photos/mamikos_apartemen.png"
	    @endif
	  }
	</script>
@endsection
