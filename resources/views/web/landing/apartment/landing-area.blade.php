@extends('web.landing.apartment.landingApartmentLayout')

@section('meta-custom')
	<title>{{ $landing['title'] }}</title>

	<meta name="description" content="{{ $landing['keyword'] }}. Sewa langsung dari pemilik, Terlengkap & lebih dari 8 juta database apartemen disewakan, cek sekarang di Mamikos.com" />
	<meta name="keywords" content="{{ strtolower($landing['keyword']) }}, harga {{ strtolower($landing['keyword']) }}, {{ strtolower($landing['keyword']) }} harian, {{ strtolower($landing['keyword']) }} mingguan, {{ strtolower($landing['keyword']) }} bulanan, {{ strtolower($landing['keyword']) }} tahunan, {{ strtolower($landing['keyword']) }} fully furnished, {{ strtolower($landing['keyword']) }} studio">

	<meta property="fb:app_id" content="607562576051242"/>
	<meta name="og:site_name" content="Mamikos"/>
	<meta property="og:title" content="{{ $landing['heading_1'] }} - Mamikos" />
	<meta property="og:description" content="Cari {{ $landing['heading_1'] }}? Kami menyediakan info {{ $landing['heading_1'] }} terbaru {{ date('Y') }} dengan fasilitas lengkap dan harga terjangkau. Dapatkan update kost dan apartemen di Mamikos.com." />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="{{ Request::url() }}" />
	<meta property="og:image" content="/assets/og/og_apartemen.jpg" />
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:site" content="@mamikosapp">
	<meta name="twitter:title" content="{{ $landing['heading_1'] }} - Mamikos">
	<meta name="twitter:description" content="Cari {{ $landing['heading_1'] }}? Kami menyediakan info {{ $landing['heading_1'] }} terbaru {{ date('Y') }} dengan fasilitas lengkap dan harga terjangkau. Dapatkan update kost dan apartemen di Mamikos.com.">
	<meta name="twitter:creator" content="@mamikosapp">
	<meta name="twitter:image" content="/assets/og/og_apartemen.jpg">
	<meta name="twitter:domain" content="mamikos.com">

	<link rel="preload" href="{{ mix_url('dist/js/landing-area.js') }}" as="script">

	<style>
		#prerender {
			min-height: 80vh;
		}
		#prerender .container {
			margin-top: 120px;
		}
		#prerender .prerender-title {
			color: #1BAA56;
			font-size: 24px;
			font-weight: bold;
			margin-bottom: 10px;
		}
		#prerender .prerender-subtitle {
			display: inline;
			font-size: 14px;
			font-weight: 700;
		}
		#prerender .prerender-subtitle.--separator {
			display: inline;
			color: #eee;
			font-weight: normal;
			text-align: center;
		}
		#prerender .prerender-placeholder {
			height: 80vh;
			width: 100%;
			position: relative;
		}
		#prerender .prerender-placeholder .prerender-placeholder-child {
			border-radius: 5px;
			background-color: #e2e6ea;
			margin: 30px 15px;
		}
		#prerender .prerender-placeholder .prerender-placeholder-child.--1 {
			height: 100px;
		}
		#prerender .prerender-placeholder .prerender-placeholder-child.--2 {
			height: 300px;
		}
		#prerender .prerender-description {
			padding: 15px;
		}
	</style>
@endsection

@section('data')
	<script type="text/javascript">
		var mamiLanding = @JZON($landing);
		var mapTypeSource = '{{ $landing['map_type'] }}';

		var relatedKostLink = @JZON($landing['related_kos_link']);
		var relatedApartmentLink = null;
		var relatedVacancyLink = @JZON($landing['related_vacancy_link']);
	</script>

	<script>
		var dataLayerParams;

		try {
			dataLayerParams = new URLSearchParams(window.location.search);
		} catch (e) {
			dataLayerParams = null;
		};

    var dataLayerProps = {
      device: "none",
      utm_source: "none",
      utm_medium: "none",
      utm_content: "none",
      utm_campaign: "none",
      price_day: "0",
      price_week: "0",
      price_month: "0",
      rating: "none",
      city: "none",
      area: "none"
    };

    if (window.innerWidth < 768) {
      dataLayerProps.device = "mobile";
    } else {
      dataLayerProps.device = "desktop";
    };

    try {
      dataLayerProps.utm_source = dataLayerParams.get('utm_source');
      if (dataLayerProps.utm_source === null) dataLayerProps.utm_source = "none";
    } catch (e) {
      dataLayerProps.utm_source = "none";
    };

    try {
      dataLayerProps.utm_medium = dataLayerParams.get('utm_medium');
      if (dataLayerProps.utm_medium === null) dataLayerProps.utm_medium = "none";
    } catch (e) {
      dataLayerProps.utm_source = "none";
    };

    try {
      dataLayerProps.utm_content = dataLayerParams.get('utm_content');
      if (dataLayerProps.utm_content === null) dataLayerProps.utm_content = "none";
    } catch (e) {
      dataLayerProps.utm_content = "none";
    };

    try {
      dataLayerProps.utm_campaign = dataLayerProps.dataLayerParams.get('utm_campaign');
      if (dataLayerProps.utm_campaign === null) dataLayerProps.utm_campaign = "none";
    } catch (e) {
      dataLayerProps.utm_campaign = "none";
    };

		
    dataLayer.push({
      "language": "id",
      "page_category": "ImpressionUnit",
      "currency": "IDR",
      "device": dataLayerProps.device,
      "utm_source": dataLayerProps.utm_source,
      "utm_medium": dataLayerProps.utm_medium,
      "utm_content": dataLayerProps.utm_content,
      "utm_campaign": dataLayerProps.utm_campaign,
      "page_url": "https://mamikos.com/daftar/{{ $landing['slug'] }}",
      "step": 2,
      "country": "Indonesia",
      "type_room": mamiLanding.unit_type,
      "price_day": dataLayerProps.price_day,
      "price_week": dataLayerProps.price_week,
      "price_month": dataLayerProps.price_month,
      "city": dataLayerProps.city,
      "area": dataLayerProps.area,
      "rating": dataLayerProps.rating,
      @if (Auth::check())
      "customer_id": "{{ (Auth::user()->id) }}",
      "profile_id": "{{ (Auth::user()->id) }}",
      "gender": "{{ (Auth::user()->gender) }}"
      @else
      "customer_id": "none",
      "profile_id": "none",
      "gender": "none"
      @endif
    });
	</script>
@endsection

@section('content')
	{{-- Prerendered Priority Contents --}}
	<div id="prerender">
		<div class="container">
			<section>
			  <h1 class="prerender-title"><strong>{{ $landing['keyword'] }}</strong></h1>
			</section>
			<section>
			  <h2 class="prerender-subtitle"><strong>Cari Unit Apartemen</strong></h2>
			  <span class="prerender-subtitle --separator"> | </span>
			  <h2 class="prerender-subtitle"><strong>Cari Project Apartemen</strong></h2>
			</section>
			<section>
				<div class="prerender-placeholder">
					<div class="prerender-placeholder-child --1"></div>
					<div class="prerender-placeholder-child --2"></div>
				</div>
			</section>
			<section>
				<div class="prerender-description">
					{!! $landing['description_1'] !!}
					{!! $landing['description_2'] !!}
					{!! $landing['description_3'] !!}
				</div>
			</section>
		</div>
	</div>

	{{-- Complete Content After JS Rendered --}}
	<div id="app">
		<app></app>
	</div>
@endsection

@section('script')
	<script src="{{ mix_url('dist/js/landing-area.js') }}"></script>

	<script type="application/ld+json">
	  {
	    "@context": "http://schema.org",
	    "@type": "BreadcrumbList",
	    "itemListElement": [
	      @foreach ($landing['breadcrumb'] as $key => $breadcrumb)
		      {
		        "@type": "ListItem",
		        "position": {{ $key + 1 }},
		        "item": {
		          "@id": "{{ $breadcrumb["url"] }}",
		          "name": "{{ $breadcrumb["name"] }}"
		        }
		      }
	      	@if ($key != count($landing['breadcrumb']) - 1)
	      	,
	      	@endif
	      @endforeach
	    ]
	  }
	</script>

	<script type="application/ld+json">
	  {
	    "@context" : "http://schema.org",
	    "@type" : "LodgingBusiness",
	    "hasMap" : "https://maps.google.com/maps?z=7&q={{ $landing['center_latitude'] }},{{ $landing['center_longitude'] }}",
	    "url" : "{{ $landing['breadcrumb'][count($landing['breadcrumb']) - 1]['url'] }}",
	    @if (!is_null($landing['unit_price_min']) && !is_null($landing['unit_price_max']))
	    "priceRange" : "Rp{{ $landing['unit_price_min'] }} - Rp{{ $landing['unit_price_max'] }}",
	    @endif
	    "name" : "{{ $landing['keyword'] }}",
	    "image" : "{{ $landing['lodging_business_image'] }}",
	    "telephone": "0856-4140-5202"
	  }
	</script>
@endsection
