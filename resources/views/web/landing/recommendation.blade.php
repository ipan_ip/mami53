@extends('web.layout.landingLayout')
@section('meta')
<!-- Meta
  ================================================== -->
<base href="/">
<title>Rekomendasi dan Favorit</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

@include('web.@meta.favicon')

<meta name="description" content="Temukan Rekomendasi dan Kos Favorit mu Disini!" />
<meta property="fb:app_id" content="607562576051242" />
<meta name="keywords" content="Rekomendasi, Favorit, Kos">
<meta name="og:site_name" content="Mamikos" />
<meta property="og:description" content="Temukan Rekomendasi dan Kos Favorit mu Disini!" />
<meta property="og:type" content="website" />
<meta property="og:url" content="<?php echo Request::url() ?>" />
<meta property="og:image" content="/assets/share-image-default.jpg" />
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@mamikosapp">
<meta name="twitter:title" content="Rekomendasi dan Favorit">
<meta name="twitter:description" content="Temukan Rekomendasi dan Kos Favorit mu Disini!">
<meta name="twitter:creator" content="@mamikosapp">
<meta name="twitter:image" content="/assets/share-image-default.jpg">
<meta name="twitter:domain" content="mamikos.com">
@endsection

@section('body-start')
<body ng-app="myApp" ng-controller="recommendationCtrl">
  @endsection

  @section('body-content')
  <!--Breadcrumb-->
  <div class="col-xs-12 breadcrumbdiv p0 breadcrumb-recfav" ng-class="{'bannerOn':bannerdisplay.value==true&&windowsWidth<993,'bannerOff':bannerdisplay.value==false}">
    <ul class="breadcrumb visible-xs">
      <li><a href="/"><i class="fa fa-home" aria-hidden="true"></i></a></li>
      <li class="visible-xs"><a href="">Rekomendasi dan Favorit</a></li>
    </ul>
    <ul class="breadcrumb hidden-xs">
      <li><a href="/"><i class="fa fa-home" aria-hidden="true"></i></a></li>
      <li class="hidden-xs"><a href="">Rekomendasi dan Favorit</a></li>
    </ul>
  </div>
  <!-- Listing XS -->
  <ul class="nav nav-tabs visible-xs visible-sm" role="tablist">
    <li role="presentation" class="active col-xs-4">
      <a href="#recommendation" aria-controls="recommendation" role="tab" data-toggle="tab">Rekomendasi</a>
    </li>
    <li role="presentation" class="col-xs-4">
      <a href="#favorite" aria-controls="favorite" role="tab" data-toggle="tab">Favorit</a>
    </li>
    <li role="presentation" class="col-xs-4">
      <a href="/auth/logout">Logout</a>
    </li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content visible-xs visible-sm">

    <div role="tabpanel" class="tab-pane active" id="recommendation">
      <!-- Room Repeat -->
      <div class="col-xs-12 p0 room-recfav-div">
        <div class="col-xs-5 photo-recfav full">
          <div class="label-full">
            PENUH
          </div>
        </div>
        <div class="col-xs-7 room-recfav-info">
          <div>600 rb/bl</div>
          <div class="title-recfav">Kost Putri 90A</div>
          <div class="labelFemale label-gender-recfav">Putri</div>
        </div>
      </div>
      <div class="col-xs-12 p0 room-recfav-div">
        <div class="col-xs-5 photo-recfav">
          <div class="label-one-left">
            TINGGAL 1 KAMAR
          </div>
        </div>
        <div class="col-xs-7 room-recfav-info">
          <div>800 rb/bl</div>
          <div class="title-recfav">Kost Putra Eksklusif LASAGNA ENAK NO 98 Jakarta</div>
          <div class="labelMale label-gender-recfav">Putra</div>
        </div>
      </div>
      <div class="col-xs-12 p0 room-recfav-div">
        <div class="col-xs-5 photo-recfav">
          <div class="label-more-left">
            ADA 3 KAMAR
          </div>
        </div>
        <div class="col-xs-7 room-recfav-info">
          <div>
            <span>800 rb/bl</span>
            <span class="label-minimal-rent">Min 1 Bln.</span>
          </div>
          <div class="title-recfav">Kost Putra Eksklusif LASAGNA ENAK NO 98 Jakarta</div>
          <div class="labelMale label-gender-recfav">Putra</div>
        </div>
      </div>
    </div>


    <div role="tabpanel" class="tab-pane" id="favorite">
      <!-- Room Repeat -->
      <div class="col-xs-12 p0 room-recfav-div">
        <div class="col-xs-5 photo-recfav full">
          <div class="label-full">
            PENUH
          </div>
        </div>
        <div class="col-xs-7 room-recfav-info">
          <div>600 rb/bl</div>
          <div class="title-recfav">Kost Putri Murah Depok Catur Tunggal A</div>
          <div class="labelFemale label-gender-recfav">Putri</div>
        </div>
      </div>
      <div class="col-xs-12 p0 room-recfav-div">
        <div class="col-xs-5 photo-recfav">
          <div class="label-one-left">
            TINGGAL 1 KAMAR
          </div>
        </div>
        <div class="col-xs-7 room-recfav-info">
          <div>800 rb/bl</div>
          <div class="title-recfav">Kost Putra Eksklusif LASAGNA ENAK NO 98 Jakarta</div>
          <div class="labelMale label-gender-recfav">Putra</div>
        </div>
      </div>
      <div class="col-xs-12 p0 room-recfav-div">
        <div class="col-xs-5 photo-recfav">
          <div class="label-more-left">
            ADA 2 KAMAR
          </div>
        </div>
        <div class="col-xs-7 room-recfav-info">
          <div>
            <span>800 rb/bl</span>
            <span class="label-minimal-rent">Min 1 Bln.</span>
          </div>
          <div class="title-recfav">Kost Campur Terong Goreng NO 202 Surabaya</div>
          <div class="labelMix label-gender-recfav">Campur</div>
        </div>
      </div>
    </div>

  </div>

  <!-- End of Listing XS -->


  <!-- Listing LG -->
  <div class="col-sm-12 hidden-xs hidden-sm p0">
    <div class="col-sm-2 p0">
      <ul class="nav nav-tabs hidden-xs hidden-sm" role="tablist">
        <li role="presentation" class="active col-xs-12">
          <a href="#recommendationLg" aria-controls="recommendationLg" role="tab" data-toggle="tab">Rekomendasi</a>
        </li>
        <li role="presentation" class="col-xs-12">
          <a href="#favoriteLg" aria-controls="favoriteLg" role="tab" data-toggle="tab">Favorit</a>
        </li>
        <li role="presentation" class="col-xs-12">
          <a href="/auth/logout">Logout</a>
        </li>
      </ul>
    </div>
    <div class="tab-content hidden-xs hidden-sm">
      <!-- Recommendation LG -->
      <div role="tabpanel" class="tab-pane active" id="recommendationLg">
        <div class="col-sm-5">
          <!-- Room Repeat -->
          <div class="col-xs-12 p0 room-recfav-div">
            <div class="col-xs-5 photo-recfav full">
              <div class="label-full">
                PENUH
              </div>
            </div>
            <div class="col-xs-7 room-recfav-info">
              <div>600 rb/bl</div>
              <div class="title-recfav">Kost Putri Murah Depok Catur Tunggal A</div>
              <div class="labelFemale label-gender-recfav">Putri</div>
            </div>
          </div>
          <div class="col-xs-12 p0 room-recfav-div">
            <div class="col-xs-5 photo-recfav">
              <div class="label-one-left">
                TINGGAL 1 KAMAR
              </div>
            </div>
            <div class="col-xs-7 room-recfav-info">
              <div>800 rb/bl</div>
              <div class="title-recfav">Kost Putra Eksklusif LASAGNA ENAK NO 98 Jakarta</div>
              <div class="labelMale label-gender-recfav">Putra</div>
            </div>
          </div>
          <div class="col-xs-12 p0 room-recfav-div">
            <div class="col-xs-5 photo-recfav">
              <div class="label-more-left">
                ADA 2 KAMAR
              </div>
            </div>
            <div class="col-xs-7 room-recfav-info">
              <div>
                <span>800 rb/bl</span>
                <span class="label-minimal-rent">Min 1 Bln.</span>
              </div>
              <div class="title-recfav">Kost Campur Terong Goreng NO 202 Surabaya</div>
              <div class="labelMix label-gender-recfav">Campur</div>
            </div>
          </div>
        </div>
        <div class="col-sm-5">
          <!-- Room Repeat -->
          <div class="col-xs-12 p0 room-recfav-div">
            <div class="col-xs-5 photo-recfav full">
              <div class="label-full">
                PENUH
              </div>
            </div>
            <div class="col-xs-7 room-recfav-info">
              <div>600 rb/bl</div>
              <div class="title-recfav">Kost Putri Murah Depok Catur Tunggal A</div>
              <div class="labelFemale label-gender-recfav">Putri</div>
            </div>
          </div>
          <div class="col-xs-12 p0 room-recfav-div">
            <div class="col-xs-5 photo-recfav">
              <div class="label-one-left">
                TINGGAL 1 KAMAR
              </div>
            </div>
            <div class="col-xs-7 room-recfav-info">
              <div>800 rb/bl</div>
              <div class="title-recfav">Kost Putra Eksklusif LASAGNA ENAK NO 98 Jakarta</div>
              <div class="labelMale label-gender-recfav">Putra</div>
            </div>
          </div>
          <div class="col-xs-12 p0 room-recfav-div">
            <div class="col-xs-5 photo-recfav">
              <div class="label-more-left">
                ADA 2 KAMAR
              </div>
            </div>
            <div class="col-xs-7 room-recfav-info">
              <div>
                <span>800 rb/bl</span>
                <span class="label-minimal-rent">Min 1 Bln.</span>
              </div>
              <div class="title-recfav">Kost Campur Terong Goreng NO 202 Surabaya</div>
              <div class="labelMix label-gender-recfav">Campur</div>
            </div>
          </div>
        </div>
      </div>
      <!-- Favorite LG -->
      <div role="tabpanel" class="tab-pane" id="favoriteLg">
        <div class="col-sm-5">
          <!-- Room Repeat -->
          <div class="col-xs-12 p0 room-recfav-div">
            <div class="col-xs-5 photo-recfav">
              <div class="label-full">
                PENUH
              </div>
            </div>
            <div class="col-xs-7 room-recfav-info">
              <div>900 rb/bl</div>
              <div class="title-recfav">Kost Putri Murah Depok Catur LsAlajasasd A</div>
              <div class="labelMix label-gender-recfav">Putri</div>
            </div>
          </div>
          <div class="col-xs-12 p0 room-recfav-div">
            <div class="col-xs-5 photo-recfav">
              <div class="label-one-left">
                TINGGAL 1 KAMAR
              </div>
            </div>
            <div class="col-xs-7 room-recfav-info">
              <div>1 jt/bl</div>
              <div class="title-recfav">Kost Putra Eksklusif RATATATTA NO 100 Jakarta</div>
              <div class="labelMale label-gender-recfav">Putra</div>
            </div>
          </div>
          <div class="col-xs-12 p0 room-recfav-div">
            <div class="col-xs-5 photo-recfav">
              <div class="label-more-left">
                ADA 2 KAMAR
              </div>
            </div>
            <div class="col-xs-7 room-recfav-info">
              <div>
                <span>750 rb/bl</span>
                <span class="label-minimal-rent">Min 1 Bln.</span>
              </div>
              <div class="title-recfav">Kost Campur Terong Goreng NO 202 Surabaya</div>
              <div class="labelFemale label-gender-recfav">Campur</div>
            </div>
          </div>
        </div>
        <div class="col-sm-5">
          <!-- Room Repeat -->
          <div class="col-xs-12 p0 room-recfav-div">
            <div class="col-xs-5 photo-recfav">
              <div class="label-full">
                PENUH
              </div>
            </div>
            <div class="col-xs-7 room-recfav-info">
              <div>600 rb/bl</div>
              <div class="title-recfav">Kost Putri Murah Depok Catur Tunggal A</div>
              <div class="labelFemale label-gender-recfav">Putri</div>
            </div>
          </div>
          <div class="col-xs-12 p0 room-recfav-div">
            <div class="col-xs-5 photo-recfav">
              <div class="label-one-left">
                TINGGAL 1 KAMAR
              </div>
            </div>
            <div class="col-xs-7 room-recfav-info">
              <div>800 rb/bl</div>
              <div class="title-recfav">Kost Putra Eksklusif LASAGNA ENAK NO 98 Jakarta</div>
              <div class="labelMale label-gender-recfav">Putra</div>
            </div>
          </div>
          <div class="col-xs-12 p0 room-recfav-div">
            <div class="col-xs-5 photo-recfav">
              <div class="label-more-left">
                ADA 2 KAMAR
              </div>
            </div>
            <div class="col-xs-7 room-recfav-info">
              <div>
                <span>800 rb/bl</span>
                <span class="label-minimal-rent">Min 1 Bln.</span>
              </div>
              <div class="title-recfav">Kost Campur Terong Goreng NO 202 Surabaya</div>
              <div class="labelMix label-gender-recfav">Campur</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-xs-12" style="height:50px;">

  </div>
  </div>
  </div>
  <div class="container-fluid">
    <div class="row">

      @endsection

      @section('title-desc-1')
      Tentang Mamikos
      @endsection

      @section('description1')
      <h2 style="text-align: justify;">
        <p style="font-weight: 100;font-size:15px;line-height:20px;">MAMIKOS menyajikan data kost dengan detail dan foto fasilitas kost yang lengkap, mulai dari fasilitas kamar kost, kamar mandi, fasilitas umum. Setiap Kost yang ada di data kost di MAMIKOS, baik kost murah ataupun kost eksklusif, benar-benar kami datangi, kami verifikasi, kami seleksi dan kami ambil data langsung. Informasi ketersediaan kamar kost dan harga kost kami update max setiap 2 minggu sekali untuk memastikan data kost kami akurat. Kami memiliki lebih dari 1000+ data kost dan terus bertambah di Indonesia, terdiri dari area Depok, Yogyakarta, Jakarta, Bandung, dan Surabaya.</p>
      </h2>
      <h2 style="text-align: justify;">
        <p style="font-weight: 100;font-size:15px;line-height:20px;">Kenapa lebih banyak kost eksklusfi yang tersedia dan update? kami tidak memilih data kost yang kami masukkan, namun demikian beberapa kost non eksklusif belum bersedia bekerja sama. Jikapun kost bekerja sama, informasi update yang diberikan menunggu pihak mamikos mengkonfirmasi, sehingga untuk memastikan kost tersedia mohon hubungi CS kami agar bisa kami konfirmasi kembali. Kost eksklusif cenderung lebih berani dalam beriklan dan mencoba metode promosi, sehingga pemilik atau pengelola kost ekslusif tersebut lebih aktif menanggapi ajakan kami untuk mengiklankan kost yang dimilkinya.</p>
      </h2>
      @endsection

      @section('title-desc-2')
      Informasi Akurat Mamikos
      @endsection

      @section('description2')
      <span>Apa yang bisa kamu dapatkan dari Aplikasi Mamikos:</span>
      <ul style="font-weight: 100;font-size:15px;">
        <li>Alamat Kost Akurat</li>
        <li>Foto-foto lengkap fasilitas kost yang eksklusif</li>
        <li>Harga Kost yang selalu update</li>
        <li>Info ketersediaan kamar kost yang selalu update</li>
        <li>Hubungi Kost via Whatsapp</li>
        <li>Hubungi Kost via Pesan</li>
        <li>Google Map untuk membantu navigasi kamu ke kost</li>
        <li>Halaman filter dan pembanding untuk membandingkan kost</li>
      </ul>
      @endsection

      @section('title-desc-3')
      Fitur Pencarian Mamikos
      @endsection

      @section('description3')
      <span>Fitur Pencarian Mamikos:</span>
      <ul style="font-weight: 100;font-size:15px;">
        <li>Cari kost dekat Kampus/Universitas di masing-masing kota</li>
        <li>Cari kost di Jogja, Depok, Jakarta, Surabaya, Bandung, dan Kota besar lainnya</li>
        <li>Cari kost di sekitar lokasi saya saat ini</li>
        <li>Fitur berlangganan (dapatkan update info kost yang kamu inginkan lewat email)</li>
        <li>Fitur favorit kost (simpan kost yang kamu suka)</li>
        <li>Fitur Histori/Kunjungi (cek data-data kost yang pernah kamu kunjungi)</li>
      </ul>
      @endsection

      @section('script')
      <script>
        $(function() {
          function reposition() {
            var modal = $(this)
              , dialog = modal.find('.modal-dialog');
            modal.css('display', 'block');
            dialog.css("margin-top", Math.max(0, ($(window).height() - dialog.height()) / 2));
            dialog.css("margin-left", Math.max(0, ($(window).width() - dialog.width()) / 2));
          }
          $('.modal').on('show.bs.modal', reposition);
          $(window).on('resize', function() {
            $('.modal:visible').each(reposition);
          });
        });
        var token = '{{ Auth::user()->token }}';

      </script>
      <script src="{{ mix_url('dist/js/bundle/bundlerecommend.js') }}"></script>


      @endsection
