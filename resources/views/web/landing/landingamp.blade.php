@extends('web.layout.landingAmpLayout')

@section('amp-custom-style')
  <link rel="preload" href="/css/fonts/lato.css" as="style">
  <link rel="stylesheet" href="/css/fonts/lato.css">>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

  <style amp-custom>
    * {
      -webkit-box-sizing: border-box;
         -moz-box-sizing: border-box;
              box-sizing: border-box;
    }

    body {
      display: block;
      font-family: 'Lato', sans-serif;
      height: 100%;
      position: relative;
      width: 100%;
    }

    h1, h2, h3, h4, h5, h6 {
      margin: 0;
    }

    .container {
      display: block;
      margin: 0 auto;
      max-width: 640px;
      width: 100%;
    }

    .row {
      display: block;
      height: auto;
      margin: 0;
      width: 100%;
    }

    .header-ampg {
      background: #1BAA56;
      text-align: center;
      padding: 15px 0;
    }

    .content {
      margin: 10px 15px 15px 0;
    }

    .below-header-ampg-head {
      font-size: 18px;
      text-align: center;
    }

    .main-header-ampg {
      color: #1BAA56;
      font-size: 20px;
      font-weight: bold;
      text-align: center;
      text-transform: uppercase;
    }

    .room-counter-info {
      font-size: 12px;
      font-weight: bold;
      text-align: center;
    }

    .room-list {
      margin: 0 auto 60px auto;
      width: 90%;
    }

    .room-item {
      box-shadow: 0 0 1px #555;
      display: block;
      color: #000;
      margin-bottom: 15px;
      text-decoration: none
    }

    .room-image-container {
      display: block;
      height: 200px;
      overflow: hidden;
      width: 100%;
    }

    .room-image-container amp-img {
      background: #000;
    }

    .room-item-head-desc {
      border-bottom: 1px solid #ccc;
      display: block;
      overflow: hidden;
      width: 100%;
    }

    .room-item-head-desc > span {
      color: #1BAA56;
      display: inline-block;
      float: left;
      font-weight: bold;
      padding: 5px;
      text-align: center;
      width: 50%;
    }

    .room-item-head-desc > span.room-danger {
      color: #ff0000;
    }

    .room-item-head-desc > span.room-full {
      color: #555;
    }

    .room-item-desc-container {
      display: block;
      overflow: hidden;
      padding: 10px;
    }

    .room-item-tagging {
      display: block;
    }

    .room-item-tagging > span {
      display: inline-block;
      font-weight: bold;
    }

    .room-item-tagging > span.gender-male {
      color: #3498DB;
    }

    .room-item-tagging > span.gender-female {
      color: #F26389;
    }

    .room-item-tagging > span.gender-mixed {
      color: #9B59B6;
    }

    .room-item-tagging > span.premium {
      color: #E0AF50;
      margin-left: 15px;
    }

    .room-item-title {
      display: block;
      font-weight: bold;
      height: 24px;
      overflow: hidden;
      position: relative;
      -ms-text-overflow: clip;
      text-overflow: clip;
      width: 100%;
    }

    .room-item-title:after {
      background: #fff;
      background: -moz-linear-gradient(left, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 100%); /* FF3.6-15 */
      background: -webkit-linear-gradient(left, rgba(255,255,255,0) 0%,rgba(255,255,255,1) 100%); /* Chrome10-25,Safari5.1-6 */
      background: linear-gradient(to right, rgba(255,255,255,0) 0%,rgba(255,255,255,1) 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
      filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00ffffff', endColorstr='#ffffff',GradientType=1 ); /* IE6-9 */
      content: '';
      display: block;
      height: 24px;
      position: absolute;
      right: 0;
      top: 0;
      width: 30px;
    }

    .rating-star {
      color: #1BAA56;
      margin-right: 15px;
    }

    .review-count {
      color: #aaa;
    }

    .button-more-container {
      background: #fff;
      bottom: 0;
      display: block;
      left: 0;
      position: fixed;
      width: 100%;
    }

    .button-more-container .button-more {
      background: #1BAA56;
      border-radius: 5px;
      color: #fff;
      display: block;
      margin: 0 auto 10px auto;
      padding: 5px 15px;
      text-align: center;
      text-decoration: none;
      width: 90%;
    }
  </style>
@endsection

@section('content')
  <div class="container">
    <header class="row header-ampg">
      <amp-img 
        alt="Logo Mamikos" 
        src="/assets/header_mamikos_text.png" 
        width="150" 
        height="25" 
        layout="fixed"></amp-img>
    </header>

    <div class="row content">
      <h3 class="below-header-ampg-head">Rekomendasi Kost</h3>

      <h1 class="main-header-ampg">{{ $keyword }}</h1>

      <p class="room-counter-info">
        {{count($roomsData['rooms'])}} Kost dari {{ $roomsData['total'] }} Kos-Kosan di sekitar {{ $keyword }}
      </p>

      <div class="room-list">
        @foreach($roomsData['rooms'] as $room)
          <a class="room-item" href="{{ $room['share_url'] }}">
            <span class="room-image-container">
              <amp-img alt="{{$room['room_title']}}" 
                src="{{$room['photo_url']['medium']}}" 
                width="16" 
                height="16"
                layout="responsive">
              </amp-img>
            </span>
            
            <span class="room-item-head-desc">
              <span class="left {{ $room['available_room'] == 1 ? 'room-danger' : ($room['available_room'] <= 0 ? 'room-full' : '') }}">
                @if($room['available_room'] <= 0)
                  PENUH
                @else
                  {{ ($room['available_room'] > 1 ? 'ADA ' : 'TINGGAL ') . $room['available_room'] }} KAMAR
                @endif
              </span>
              <span class="right">
                Rp. {{ $room['price_title_time'] }}
              </span>
            </span>

            <span class="room-item-desc-container">
              <span class="room-item-tagging">
                <span class="{{$room['gender'] == 2 ? 'gender-female' : ($room['gender'] == 1 ? 'gender-male' : 'gender-mixed')}}">{{$room['gender'] == 2 ? 'PUTRI' : ($room['gender'] == 1 ? 'PUTRA' : 'CAMPUR')}}</span>
                @if($room['is_premium_owner'])
                  <span class="premium">PREMIUM</span>
                @endif
              </span>

              <span class="room-item-title">
                {{$room['room_title']}}
              </span>

              <span class="room-item-rating-review">
                <span class="rating-star">
                  @for($i = 1; $i <= 4; $i++)
                    @if($room['rating'] >= $i)
                      <i class="fa fa-star"></i>
                    @else
                      <i class="fa fa-star-o"></i>
                    @endif
                  @endfor
                </span>

                <span class="review-count">{{ $room['review_count'] }} Review</span>
              </span>
            </span>
          </a>
        @endforeach
      </div>

      <div class="button-more-container">
        <a href="{{$canonical}}" title="Lihat lebih banyak" class="button-more" id="button-more">Lihat lebih banyak</a>
      </div>
    </div>
  </div>
@endsection