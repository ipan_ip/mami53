<!DOCTYPE html>
<html lang="id">
	<head resource="/">
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<meta name="csrf-token" content="{{ csrf_token() }}">

		<meta name="application-name" content="Mamikos">
		<meta name="theme-color" content="#1BAA56">
		<meta name="mobile-web-app-capable" content="yes">

		<meta name="apple-itunes-app" content="app-id=1055272843">
		<meta name="google-play-app" content="app-id=com.git.mami.kos">

		<link rel="icon" type="image/png" sizes="16x16" href="assets/singgahsini/favicon-16x16.png">
		<link rel="icon" type="image/png" sizes="32x32" href="assets/singgahsini/favicon-32x32.png">
		<link rel="shortcut icon" href="/assets/singgahsini/favicon.ico">
		<link rel="mask-icon" href="/general/img/manifest/safari-pinned-tab.svg" color="#1baa56">
		<meta name="msapplication-TileColor" content="#1baa56">
		<meta name="msapplication-config" content="/browserconfig.xml">

		<link rel="manifest" href="{{ asset_url('dist/json/singgahsini-manifest.json') }}">

		<meta name="google-site-verification" content="grtdHp44CrZpWDuquG54IUO-FyZeaflA6MHtQlh4ySg">
		<meta name="p:domain_verify" content="1947d14ed84936368c9c2c229d535f39">
		<meta name="msvalidate.01" content="8F441EFF8C6571840616F325C34D7698">
		<meta name="rating" content="general">

		<meta property="fb:app_id" content="607562576051242">
		<meta property="og:type" content="website">
		<meta property="og:site_name" content="Singgahsini">
		<meta property="og:locale" content="id_ID">
		<meta property="og:image" content="assets/singgahsini/singgahsini-og-image.jpg" />

		<meta name="twitter:card" content="summary">
		<meta name="twitter:site" content="@singgahsini_idn">
		<meta name="twitter:creator" content="@singgahsini_idn">

		@include('web.@meta.link-prebrowsing')

		<link rel="preload" as="font" type="font/woff2" href="/general/fonts/Lato/lato-v17-latin-regular.woff2" crossorigin>
		<link rel="preload" as="font" type="font/woff2" href="/general/fonts/Lato/lato-v17-latin-700.woff2" crossorigin>
		<link rel="preload" href="/css/fonts/lato.css" as="style">
    	<link rel="stylesheet" href="/css/fonts/lato.css">

		<link rel="preload" href="{{ mix_url('dist/css/app.css') }}" as="style">
		<link rel="preload" href="{{ mix_url('dist/js/@vendor/main/script.js') }}" as="script">

		@yield('head')

		@if(!isset($from_consultant_tools))
			@include('web.@script.global-auth')
		@endif

		@include('web.@script.polyfill-snippet')

		@include('web.@script.utility')

		@yield('data')

		@include('web.@script.bugsnag-api-key')

		<link rel="stylesheet" href="{{ mix_url('dist/css/app.css') }}">

		@yield('styles')
	</head>

	<body>
		@include('web.@script.gtm-body')

		@include('web.@script.service-worker')

		{{-- unuse this temporarily for experiment --}}
		{{-- @include('web.@content.moengage-notif-soft-ask') --}}


		@yield('content')


		<script src="{{ mix_url('dist/js/@vendor/main/script.js') }}"></script>

		@yield('scripts')

		@include('web.@script.session-token')

		@if(!isset($from_consultant_tools))
			@include('web.@script.browser-alert')
		@endif
	</body>
</html>
