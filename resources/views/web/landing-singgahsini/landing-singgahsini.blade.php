@extends('web.landing-singgahsini.singgahsiniLayout')

@section('head')
@if (!empty($_SERVER['HTTP_X_FORWARDED_SERVER']))
{{-- Accessed from singgahsini.id --}}
@if ($_SERVER['HTTP_X_FORWARDED_SERVER'] == 'singgahsini.id')
<meta name="robots" content="index, follow">
@else
<meta name="robots" content="noindex, nofollow">
@endif
@else
<meta name="robots" content="noindex, nofollow">
@endif

<!-- Primary Meta Tags -->
<title>Gabung Singgahsini Pemilik Kos</title>
<meta name="title" content="Gabung Singgahsini Pemilik Kos">
<meta name="description" content="Optimalkan okupansi & pendapatan bulanan bisnis kos Anda bersama Singgahsini. Lebih dari ribuan kamar di ratusan kos telah bergabung dengan Singgahsini. Daftar sekarang!">
<meta name="keywords" content="Singgahsini, bisnis kos">
<link rel="canonical" href="https://singgahsini.id">
<meta name="google-site-verification" content="TNv_Bg8_X1gVzs7_t3Eg0glE6HNaBaFlIZScXRpGTWQ" />
<meta name="keyphrases" content="Info Kost, Cari Kost, Sewa Kost, Kost Bebas, Kost Murah, Kost pasutri, Aplikasi Kost, Aplikasi Pencarian Kost, Aplikasi Info Kost, APlikasi Cari Kost, Kost, Mamikost, Mamikosapp, Kamar Kost, Kamar Kos, Kostan, Kos, Rumah Kost, Rumah Kos, Kost Harian">
<meta name="classification" content="Business, Rent House, Sewa Kost, Property, Rent Room, Info Kost, Information, Kost, Room, Cari Kost, Kost Murah, Kost Bebas, Application, Mobile Application, Kamar Kost, Kamar Kos, Kostan, Kos, Rumah Kost, Rumah Kos, Kost Harian">

<!-- Open Graph / Facebook -->
<meta property="og:type" content="website">
<meta property="og:url" content="https://singgahsini.id">
<meta property="og:title" content="Gabung Singgahsini Pemilik Kos">
<meta property="og:description" content="Optimalkan okupansi & pendapatan bulanan bisnis kos Anda bersama Singgahsini. Lebih dari ribuan kamar di ratusan kos telah bergabung dengan Singgahsini. Daftar sekarang!">
<meta property="og:image" content="https://singgahsini.id/assets/logo/svg/logo_singgahsini_full_color.svg">

<!-- Twitter -->
<meta property="twitter:card" content="summary_large_image">
<meta property="twitter:url" content="https://singgahsini.id">
<meta property="twitter:title" content="Gabung Singgahsini Pemilik Kos">
<meta property="twitter:description" content="Optimalkan okupansi & pendapatan bulanan bisnis kos Anda bersama Singgahsini. Lebih dari ribuan kamar di ratusan kos telah bergabung dengan Singgahsini. Daftar sekarang!">
<meta property="twitter:image" content="https://singgahsini.id/assets/logo/svg/logo_singgahsini_full_color.svg">


<link rel="preload" href="{{ mix_url('dist/js/_landing-singgahsini/app.js') }}" as="script">
@endsection

@section('data')
@include('web.@script.third-parties-sdk-head')
@endsection

@section('styles')
@include('web.@style.preloading-style')
@endsection


@section('content')
@include('web.@style.preloading-content')

<div id="app">
  <app></app>
</div>
@endsection


@section('scripts')
@if (Auth::check())
@if (Auth::user()->is_owner == 'false')
@include('web.@script.sendbird-widget')
@endif
@endif
<script>
  var rooms = {!! json_encode($rooms) !!};
</script>
<script src="{{ mix_url('dist/js/_landing-singgahsini/app.js') }}" async defer>
</script>
@endsection
