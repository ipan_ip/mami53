<style>
#preloading {
	height: 100vh;
	display: -moz-flex;
	display: -ms-flex;
	display: -o-flex;
	display: -webkit-box;
	display: flex;
	-ms-align-items: center;
	-webkit-box-align: center;
	align-items: center;
	-webkit-box-pack: center;
	justify-content: center;
}
#preloading .spinner {
	margin: auto;
	width: auto;
	text-align: center;
	vertical-align: middle;
}
#preloading .spinner > div {
	width: 18px;
	height: 18px;
	background-color: #3cb33c;
	border-radius: 100%;
	display: inline-block;
	-webkit-animation: sk-bouncedelay 1.4s infinite ease-in-out both;
	animation: sk-bouncedelay 1.4s infinite ease-in-out both;
}
#preloading .spinner .bounce1 {
	-webkit-animation-delay: -0.32s;
	animation-delay: -0.32s;
}
#preloading .spinner .bounce2 {
	-webkit-animation-delay: -0.16s;
	animation-delay: -0.16s;
}
@-webkit-keyframes sk-bouncedelay {
	0%,
	100%,
	80% {
		-webkit-transform: scale(0);
	}
	40% {
		-webkit-transform: scale(1);
	}
}
@keyframes sk-bouncedelay {
	0%,
	100%,
	80% {
		-webkit-transform: scale(0);
		transform: scale(0);
	}
	40% {
		-webkit-transform: scale(1);
		transform: scale(1);
	}
}
</style>
