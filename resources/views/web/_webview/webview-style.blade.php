<style type="text/css">
.wrapper-term {
	text-align: center;
	font-family: Lato,sans-serif;
  font-size: 14px;
  line-height: 1.75;
  color: #383746;
  margin-top: 3rem;
  padding: 1rem;
  height: 100vh;
}

.title-term {
	height: 20vh;
	display: flex;
	flex-direction: column;
	justify-content: center;
}

.title-term > h1 {
	margin-bottom: 0.3rem;
	margin-top: 0;
}

.title-term > p {
	color: #9f9d9d;
	margin-bottom: 2rem;
}

.textarea-term {
	max-width: 768px;
	text-align: justify;
	height: 70vh;
	max-height: 500px;
	overflow-y: scroll;
	background-color: #f4f2f3;
	border: none;
	padding: 2rem;
	font-size: 1em;
	margin: 0 auto;
}

.textarea-term > h1 {
	margin: 0;
	font-size: 1.2em;
	font-weight: 600;
	margin-bottom: 1rem;
}

.textarea-term > ul {
	padding-left: 1rem;
}

.textarea-term > ul > ul {
	padding-left: 1rem;
}

.zero-bottom {
	margin-bottom: 0;
}

.distance {
	margin-bottom: 2rem !important;
}
	</style>
