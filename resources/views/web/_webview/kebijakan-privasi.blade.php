@extends('web.@layout.globalLayout')

@section('head')
	<meta name="robots" content="noindex, nofollow">

	<title>Kebijakan Privasi  - Mamikos</title>
	<meta name="description" content="Halaman Kebijakan dan Privasi MAMIKOS.">
	<link rel="canonical" href="https://mamikos.com/webview/booking-syarat-ketentuan">
@endsection

@section('data')

@endsection


@section('styles')
	@include('web._webview.webview-style')
@endsection


@section('content')

<div class="wrapper-term">
  <div class="title-term">
    <h1>Kebijakan Privasi</h1>
  </div>

  <div class="textarea-term">

    @include('web.privacypolicy-content')
    
  </div>
</div>
@endsection


@section('scripts')

@endsection