<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Mamikos - Chat CS</title>

	<style>
		/* http://meyerweb.com/eric/tools/css/reset/
			v2.0 | 20110126
			License: none (public domain)
		*/

		html, body, div, span, applet, object, iframe,
		h1, h2, h3, h4, h5, h6, p, blockquote, pre,
		a, abbr, acronym, address, big, cite, code,
		del, dfn, em, img, ins, kbd, q, s, samp,
		small, strike, strong, sub, sup, tt, var,
		b, u, i, center,
		dl, dt, dd, ol, ul, li,
		fieldset, form, label, legend,
		table, caption, tbody, tfoot, thead, tr, th, td,
		article, aside, canvas, details, embed,
		figure, figcaption, footer, header, hgroup,
		menu, nav, output, ruby, section, summary,
		time, mark, audio, video {
			margin: 0;
			padding: 0;
			border: 0;
			font-size: 100%;
			font: inherit;
			vertical-align: baseline;
		}
		/* HTML5 display-role reset for older browsers */
		article, aside, details, figcaption, figure,
		footer, header, hgroup, menu, nav, section {
			display: block;
		}
		body {
			line-height: 1;
		}
		ol, ul {
			list-style: none;
		}
		blockquote, q {
			quotes: none;
		}
		blockquote:before, blockquote:after,
		q:before, q:after {
			content: '';
			content: none;
		}
		table {
			border-collapse: collapse;
			border-spacing: 0;
		}
		/* end of reset */

		body {
			font-family: system-ui, -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Oxygen-Sans, Ubuntu, Cantarell, Helvetica Neue, Arial, sans-serif;
		}

		#launcher {
			display: none;
		}

		.page-loading {
			position: absolute;
			left: 50%;
			top: 50%;
			transform: translate(-50%, -50%);
			font-size: 18px;
		}

		.page-loading::after {
			content: '';
			position: absolute;
			left: calc(50% - 18px);
			top: calc(100% + 10px);
			width: 18px;
			height: 18px;
			border-radius: 50%;
			border-width: 2px;
			border-style: solid;
			border-color: rgb(27, 170, 86) rgb(27, 170, 86) rgb(27, 170, 86) transparent;
			transform: rotateZ(0deg);

			animation-name: rotate;
			animation-duration: 1s;
			animation-iteration-count: infinite;
			animation-timing-function: linear;
		}

		@keyframes rotate {
			from {
				transform: rotateZ(0deg);
			}
			to {
				transform: rotateZ(360deg);
			}
		}

		.header-overlay {
			position: absolute;
			top: 0;
			left: 0;
			right: 0;
			width: 100vw;
			height: 44px;
			border-bottom: 1px solid rgb(27, 170, 86);
			z-index: 1999999;
			pointer-events: none;
		}

		.header-overlay::after {
			content: '';
			position: absolute;
			right: 0;
			top: 0;
			width: 100px;
			height: 44px;
			pointer-events: auto;
			background-color: #fff;
		}

		.copytext-guide {
			position: relative;
			top: 44px;
			left: 0;
			padding: 10px;
			font-size: 14px;
			line-height: 1.4;
			display: none;
			color: #666;
		}
	</style>
</head>
<body>
	<div class="header-overlay">&nbsp;</div>

	<div class="page-loading">Loading...</div>

	<div class="copytext-guide">Klik ikon panah kiri untuk kembali ke aplikasi.</div>

	<!-- Start of mamiteam2517 Zendesk Widget script -->
	<script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=06238846-f976-43a0-9ed0-641a16997f93"> </script>
	<!-- End of mamiteam2517 Zendesk Widget script -->

	<script type="text/javascript">
		window.zESettings = {
			webWidget: {
				color: {
					header: '#ffffff'
				},

				helpCenter: {
					title: {
						'*': ' '
					}
				},
				contactForm: {
					title: {
						'*': ' '
					}
				},
				chat: {
					title: {
						'*': ' '
					}
				},
				talk: {
					title: {
						'*': ' '
					}
				},
				answerBot: {
					title: {
						'*': ' '
					}
				}
			}
		};

		zE('webWidget:on', 'open', function() {
			document.getElementsByClassName('page-loading')[0].style.display = 'none';
		});

		zE('webWidget:on', 'close', function() {
			document.getElementsByClassName('copytext-guide')[0].style.display = 'block';
		});

		document.addEventListener("DOMContentLoaded", function(event) {
			zE('webWidget', 'open');
		});
	</script>
</body>
</html>