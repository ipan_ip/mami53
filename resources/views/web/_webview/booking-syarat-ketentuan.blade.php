@extends('web.@layout.globalLayout')

@section('head')
	<meta name="robots" content="noindex, nofollow">

	<title>Syarat dan Ketentuan Booking  - Mamikos</title>
	<meta name="description" content="Halaman syarat dan ketentuan Booking.">
	<link rel="canonical" href="https://mamikos.com/webview/booking-syarat-ketentuan">
@endsection

@section('data')

@endsection


@section('styles')
	@include('web._webview.webview-style')
@endsection


@section('content')
<div class="wrapper-term">
		<div class="title-term">
			<h1>Syarat dan Ketentuan</h1>
			<p>Berlaku sejak 23 April 2019</p>
		</div>

		<div class="textarea-term">
			<h1>Pasal 1 : Definisi/Terms</h1>
			<ul>
				<li>
					<b>Mamikos</b> adalah aplikasi pencari kost yang menghubungkan antara
					User pencari kost dengan pemilik kost tersedia di Web dan aplikasi.
				</li>
				<li>
					<b>Sistem Manajemen Properti MamiPAY</b> adalah sistem manajemen
					properti yang dibuat oleh Mamikos dengan fungsi meliputi manajemen
					listing property terhubung di aplikasi dan web mamikos, pengelolaan
					user yang kontak Mamikos, pengelolaan booking, antrian dan pengelolaan
					tagihan serta manajemen laporan keuangan.
				</li>
				<li>
					<b>Merchant Kost</b> adalah dalam hal ini pemilik kost yang Namanya
					tertera di surat kepemilikan properti sebagai pemilik kost atau
					pengelola kost yang memiliki hak pengelolaan atau kuasa untuk
					mengelola properti kost yang didaftarkan dan bisa dibuktikan secara
					legal melalui surat.
				</li>
				<li>
					<b>User</b> adalah pelaku Transaksi Internet di Merchant, melalui
					Sistem Pembayaran Internet.
				</li>
				<li>
					<b>Kontrak</b> adalah perjanjian sewa menyewa yang terjadi antara
					merchant kost dan user yang dikelola lewat sistem manajemen propertI
					MamiPAY dari Mamikos dimana dalam kontrak tersebut tertera jangka
					waktu kontrak, jadwal pembayaran nilai kontrak dan nominal tagihan
					setiap jadwal pembayaran.
				</li>
				<li>
					<b>Jangka Waktu Kontrak</b> mengandung informasi tanggal kontrak
					dimulai hingga tanggal kontrak selesai.
				</li>
				<li>
					<b>Jadwal Pembayaran</b> adalah tanggal-tanggal dimana user memiliki
					kewajiban membayar tagihan pembayaran kos dan biaya lain yang tertera
					dalam kontrak.
				</li>
				<li>
					<b>Nilai Kontrak</b> adalah total nilai perjanjian sewa selama jangka
					waktu kontrak.
				</li>
				<li>
					<b>Nominal Tagihan</b> adalah nilai yang harus dibayarkan oleh user
					untuk setiap tagihan.
				</li>
				<li>
					<b>Pengingat Tagihan</b> adalah fitur yang disediakan oleh sistem
					manajemen property MamiPAY oleh mamikos untuk secara otomatis
					mengingatkan pembayaran user.
				</li>
				<li>
					<b>Booking</b> adalah fasilitas untuk bisa memesan kamar secara
					langsung dari aplikasi yang bisa diaktifkan oleh Merchant Kost lewat
					sistem manajemen property MamiPAY oleh Mamikos untuk User, adapun
					pengelolaannya juga bisa dilakukan lewat MamiPAY oleh Mamikos.
				</li>
				<li>
					<b>Antri/Waiting list</b> adalah fasilitas untuk bisa memesan kamar
					ketika kamar penuh secara langsung dari aplikasi yang bisa diaktifkan
					oleh Merchant Kost lewat sistem manajemen property MamiPAY oleh
					Mamikos untuk User, adapun pengelolaannya juga bisa dilakukan lewat
					MamiPAY oleh Mamikos.
				</li>
				<li>
					<b>Harga</b> disini merujuk pada harga sewa kamar dengan jangka waktu
					tertentu dari harian, mingguan, bulanan, tahunan yang dapat disetting
					sendiri oleh Merchant.
				</li>
				<li>
					<b>Deposit</b> adalah uang yang dijaminkan oleh user ketika menyewa
					kamar kepada Merchant yang bisa ditagihkan , dikelola, disimpan,
					kemudian dikembalikan secara otomatis pada saat jangka waktu kontrak
					habis lewat sistem manajemen property MamiPAY.
				</li>
				<li>
					<b>Biaya Tambahan Pro-Rata</b> adalah biaya tambahan ketika User tidak
					masuk di tanggal awal/akhir bulan namun tagihan berikutnya dihitung
					setiap tanggal awal/akhir bulan yang bisa disetting oleh Merchant Kost
					dan ditagihkan sekaligus lewat sistem manajemen property MamiPAY.
				</li>
				<li>
					<b>Biaya Tambahan Per Bulan</b> adalah biaya tambahan atas layanan
					tambahan yang digunakan user diluar standar sewa kamar yang tersedia
					misalnya tambahan sewa parkir , laundry dll yang secara rutin
					ditagihkan perbulan yang bisa disetting oleh Merchant Kost dan
					ditagihkan sekaligus lewat sistem manajemen property MamiPAY.
				</li>
				<li>
					<b>Biaya Tambahan Insidental</b> adalah biaya tambahan atas layanan
					tambahan yang digunakan user diluar standar sewa kamar yang tersedia
					misalnya tambahan ada teman menginap beberapa hari yang tidak secara
					rutin ditagihkan perbulan yang bisa disetting oleh Merchant Kost dan
					ditagihkan sekaligus lewat sistem manajemen property MamiPAY.
				</li>
				<li>
					<b>Bank</b> adalah badan hukum yang bergerak di bidang perbankan dan
					didirikan berdasarkan hukum Indonesia, yang dalam hal ini memiliki
					fungsi sebagai penyedia jasa sarana Layanan Pembayaran.
				</li>
				<li>
					<b>Biaya Transaksi</b> adalah biaya yang dikenakan oleh Sistem
					Manajamen Properti kepada Merchant untuk setiap Transaksi yang
					berhasil dilaporkan.
				</li>
				<li>
					<b>Hari Kerja</b> adalah hari selain Sabtu, Minggu dan hari libur
					nasional, di mana bank-bank di Indonesia buka dan operasional untuk
					kegiatan sehari-hari.
				</li>
				<li>
					<b>Layanan Pembayaran</b> adalah jenis-jenis metode pembayaran
					Transaksi Internet yang tersedia di Sistem Manajemen Properti MamiPAY.
				</li>
				<li>
					<b>Service Provider</b> adalah penyedia jasa metode pembayaran selain
					Bank, yang memiliki kerjasama dan telah terhubung dengan Sistem
					manajemen property.
				</li>
				<li>
					<b>Payment Gateway</b> adalah pihak penyelenggara system pembayaran
					yang dimana Pihak Pertama bekerjasama dalam rangka memberikan layanan
					metode pembayaran ke system manajemen property.
				</li>
				<li>
					<b>Transaksi</b> adalah transaksi melalui media elektronik maupun
					manual yang dilakukan oleh User yang dilaporkan secara otomatis maupun
					manual dikonfirmasi oleh merchant melalui sistem manajemen property
					MamiPAY.
				</li>
			</ul>
			<h1>Pasal 2: Hak dan Kewajiban Merchant</h1>
			<ul>
				<li>
					Hak Merchant, selain hak-hak yang telah dinyatakan dalam pasal-pasal
					lain Perjanjian ini adalah:
				</li>
				<ul>
					<li>
						mendapatkan dukungan teknis maupun operasional dari Sistem Manajemen
						Properti MamiPAY.
					</li>
					<li>menerima informasi status Transaksi otomatis dan manual</li>
					<li>
						Untuk transaksi manual , merchant memiliki hak untuk mengkonfirmasi
						status transaksi
					</li>
					<li>
						menerima edukasi terkait akan penyelenggaraan layanan Sistem
						Manajemen property.
					</li>
					<li>
						Menerima pembayaran dari user kos paling lambat 2 (dua) hari kerja
						sejak transaski diterima di sistem manajemen properti MamiPAY hanya
						apabila rekening bank dengan nomor dan nama akun dan identitas diri
						pemilik akun sistem manajemen property sudah diverifikasi.
					</li>
					<li>
						Menerima informasi perubahan layanan minimal 1 (satu) hari sebelum
						perubahan terjadi.
					</li>
					<li>
						Mengubah data properti dan mengajukan perubahan data properti kepada
						sistem manajemen property MamiPAY, dan mendapatkan kepastian dalam 2
						hari kerja apakah perubahan disetujui.
					</li>
					<li>
						Mengupdate data properti kapanpun dan mengajukan update data
						tersebut kepada sistem manajemen properti MamiPAY dan mendapatkan
						kepastian dalam 2 hari kerja apakah update disetujui.
					</li>
				</ul>
				<li>
					Kewajiban Merchant, selain kewajiban-kewajiban yang telah dinyatakan
					dalam pasal-pasal lain Perjanjian ini adalah:
				</li>
				<ul>
					<li>
						melengkapi data pribadi merchant sesuai formulir dan menyertakan
						identitas terbaru dan foto diri dengan identitas yang sesuai dengan
						nama di no rekening penerima.
					</li>
					<li>
						apabila yang mendaftarkan akun atau/dan yang namanya tertera di no
						rekening bukan pemilik properti yang tertulis di sertifikat sah
						wajib melampirkan surat kuasa pengelolaan atau menunjukkan bukti
						bahwa yang mendaftar atau no rekening yang didaftarkan punya hak
						untuk mendaftar dan mengelola sistem manajemen properti MamiPAY.
					</li>
					<li>
						Identitas merchant , nomor rekening dan nama pemilik rekening wajib
						terverifikasi untuk merchant bisa menerima pembayaran dari sistem
						manajemen properti MamiPAY.
					</li>
					<li>
						memberikan informasi yang sebenar-benarnya mengenai data properti ,
						sistem manajemen properti MamiPAY tidak bertanggung jawab atas
						sanksi hukum yang mungkin berlaku dan sistem manajemen properti
						MamiPAY bisa memproses secara hukum atas kerugian yang diakibatkan
						data property yang tidak benar.
					</li>
					<li>
						memberikan deskripsi dan data ketersediaan kamar secara benar dan
						sesuai dengan keadaan.
					</li>
					<li>
						memberikan hak atas sewa kamar beserta fasilitasnya kepada User
						dengan benar sesuai dengan keadaan sesungguhnya.
					</li>
					<li>
						melakukan pembayaran kepada Sistem Manajemen Properti MamiPAY atas
						Biaya Transaksi apabila ada.
					</li>
					<li>
						Mematuhi prosedur system manajemen properti terlampir dibawah.
					</li>
					<li>
						mematuhi ketentuan dan persyaratan dari masing-masing Layanan
						Pembayaran yang telah ditetapkan oleh Bank dan Service Provider.
					</li>
				</ul>
			</ul>
			<h1>Pasal 3 : Hak Dan Kewajiban User</h1>
			<ul>
				<li>
					Hak User, selain hak-hak yang telah dinyatakan dalam pasal-pasal lain
					Perjanjian ini adalah:
				</li>
				<ul>
					<li>
						mendapatkan dukungan teknis maupun operasional dari Sistem Mamikos.
					</li>
					<li>menerima informasi status Transaksi otomatis dan manual.</li>
					<li>
						menerima konfirmasi Booking maksimal 1 (satu) hari sebelum tanggal
						masuk.
					</li>
					<li>menempati hak sewa kamar sesuai kontrak yang telah disetujui.</li>
					<li>
						Menerima informasi perubahan layanan minimal 1 (satu) hari sebelum
						perubahan terjadi.
					</li>
					<li>
						membatalkan Booking maksimal 1 (satu) hari sebelum tanggal masuk
						dengan kompensasi pembatalan, dan mendapatkan kepastian dalam 2 hari
						kerja apakah perubahan disetujui.
					</li>
				</ul>
				<li>
					Kewajiban User, selain kewajiban-kewajiban yang telah dinyatakan dalam
					pasal-pasal lain Perjanjian ini adalah:
				</li>
				<ul>
					<li>
						melengkapi data pribadi sesuai formulir dan menyertakan identitas
						terbaru dan foto diri dengan identitas yang sesuai dengan kartu
						identitas.
					</li>
					<li>melengkapi dan melakukan verifikasi nomor telepon aktif.</li>
					<li>
						memberikan informasi yang sebenar-benarnya mengenai data pemesanan
						Booking, sistem Booking Mamikos tidak bertanggung jawab atas sanksi
						hukum yang mungkin berlaku dan sistem Booking Mamikos bisa memproses
						secara hukum atas kerugian yang diakibatkan data pemesanan yang
						tidak benar.
					</li>
					<li>
						memastikan data kontrak yang diberikan benar dan sesuai sebelum
						melakukan pembayaran, sistem Booking Mamikos tidak bertanggung jawab
						atas perubahan kontrak secara manual ataupun ketidaksesuaian
						fasilitas dengan ekspektasi user.
					</li>
					<li>
						melakukan pembayaran Booking sesuai kontrak yang telah disetujui
						dalam rentang waktu 1 (satu) hari kerja sejak Booking diterima oleh
						merchant. Pembayaran dilakukan melalui Sistem Manajemen Properti
						MamiPAY, Sistem Manajemen Properti MamiPAY bisa melakukan pembatalan
						secara otomatis apabila melewati batas waktu pembayaran.
					</li>
					<li>
						melakukan pembayaran kepada Sistem Manajemen Properti MamiPAY atas
						Biaya Transaksi apabila ada.
					</li>
					<li>
						mematuhi ketentuan dan persyaratan dari masing-masing Layanan
						Pembayaran yang telah ditetapkan oleh Bank dan Service Provider.
					</li>
				</ul>
			</ul>
			<h1>
				Pasal 4 : Prosedur Operasi Standar (Standard Operating System / SOP)
			</h1>
			<ul class="zero-bottom">
				<li>Transaksi Booking</li>
			</ul>
			<p>
				User dapat melakukan pemesanan Booking pada merchant terdaftar melalui
				situs dan aplikasi Mamikos.
			</p>
			<ul class="zero-bottom">
				<li>Laporan Transaksi</li>
			</ul>
			<p>
				User dapat mengakses laporan dan status Transaksi Internet melalui situs
				dan aplikasi Mamikos secara real-time (langsung setelah Transaksi
				dikonfirmasi oleh merchant). Batas waktu penerimaan konfirmasi dari
				merchant maksimal 1 (satu) hari sebelum tanggal masuk.
			</p>
			<ul class="zero-bottom">
				<li>Pembayaran Transaksi</li>
			</ul>
			<p>
				Pembayaran transaksi pertama dan seterusnya akan dilakukan melalui
				Sistem Manajemen Properti MamiPAY dalam rentang waktu 1 (satu) hari
				kerja setelah tagihan diberikan.
			</p>
			<ul class="zero-bottom">
				<li>Jadwal Pengiriman Uang Transaksi ke Merchant</li>
			</ul>
			<p>
				Kebijakan Pembatalan :
			</p>
			<ul class="zero-bottom">
				<li>
					Apabila user melakukan pembatalan setelah menyelesaikan pembayaran
					booking, maka merchant berhak mendapatkan persenan ganti rugi dari
					nominal transaksi seperti berikut:
				</li>
			</ul>
			<table class="table">
				<thead>
					<tr>
						<th></th>
						<th>Pembatalan saat baru melunasi biata Down Payment (DP)</th>
						<th>Lebih dari 30 hari (Booking telah dibayar lunas)*</th>
						<th>H-30 s/d H-15 (Booking telah dibayar lunas)*</th>
						<th>Kurang dari 15 Hari (Booking telah dibayar lunas)*</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><b>User</b></td>
						<td>0%</td>
						<td>75%</td>
						<td>50%</td>
						<td>25%</td>
					</tr>
					<tr>
						<td><b>Owner</b></td>
						<td>50%</td>
						<td>25%</td>
						<td>30%</td>
						<td>50%</td>
					</tr>
					<tr>
						<td><b>Mamikos</b></td>
						<td>50%</td>
						<td>0%</td>
						<td>20%</td>
						<td>25%</td>
					</tr>
				</tbody>
			</table>
			<p>
				*dihitung dari tanggal user melakukan pembatalan sampai tanggal user
				seharusnya check-in kost, presentase diambil dari nominal pelunasan transaksi (tidak termasuk biaya Down Payment (DP).<br>
				**apabila booking menggunakan opsi Down Payment (DP) dalam pelunasan transaksinya.
			</p>
			<h1>
				Pasal 5 : Biaya Transaksi Dan Pembayaran Tagihan
			</h1>
			<ul>
				<li>
					User wajib membayar Biaya Transaksi kepada Sistem Mamikos kecuali ada
					kebijakan atau program tertentu.
				</li>
				<li>
					User akan dikenakan Biaya Transaksi sebagaimana yang telah disepakati,
					dimana biaya tersebut tidak termasuk biaya administrasi Bank dan Pajak
					yang berlaku kecuali ada kebijakan atau program tertentu.
				</li>
			</ul>
			<h1>
				Pasal 6 : Penghentian Sementara Layanan Sistem Booking Mamikos dan
				Sistem Manajemen Properti MamiPAY
			</h1>
			<ul>
				<li>
					Sistem Booking Mamikos dan Sistem Manajemen Properti MamiPAY dapat
					setiap saat menghentikan/mematikan Sistemnya untuk sementara waktu
					dengan pemberitahuan selambat-lambatnya 5 (lima) Hari Kerja sebelumnya
					kepada MerchantKost .
				</li>
				<li>
					Penghentian layanan Sistem Pembayaran Internet dapat disebabkan oleh
					alasan-alasan sebagai berikut:
				</li>
				<ul>
					<li>inspeksi, perbaikan, pemeliharaan atau peningkatan sistem;</li>
					<li>
						adanya alasan tertentu berupa melindungi hak-hak dan/atau
						kepentingan Para Pihak; atau
					</li>
					<li>
						alasan jelas lain yang ditentukan oleh Payment Gateway, Bank, atau
						Service Provider.
					</li>
				</ul>
				<li>
					Apabila terdapat gangguan tiba-tiba terhadap Sistem Pembayaran
					Internet akibat kegagalan sistem, jaringan, koneksi internet atau
					alasan lainnya, Sistem Manajemen Properti MamiPAY akan memberitahu
					Merchant Kos secara tertulis mengenai gangguan ini, serta informasi
					lanjutan apabila gangguan tersebut telah selesai.
				</li>
			</ul>
			<h1>Pasal 7 : Keamanan dan Perlindungan Informasi</h1>
			<ul>
				<li>
					Sistem Booking Mamikos dan Sistem Manajemen Properti MamiPAY memiliki
					sistem dan jaringan yang aman guna melindungi informasi yang bersifat
					sensitif, termasuk namun tidak terbatas kepada:
				</li>
				<li>informasi Transaksi Internet (user id, dan lain-lain);</li>
				<li>informasi User (data pribadi, alamat, dan lain-lain); dan</li>
				<li>informasi lainnya yang dianggap sensitif oleh Para Pihak.</li>
				<li>
					Sistem Booking Mamikos dan Sistem Manajemen Properti MamiPAY akan
					menyimpan dan melindungi data Transaksi selama 24 (dua puluh empat)
					bulan setelah tanggal terjadinya Transaksi tersebut.
				</li>
				<li>
					Merchant kos menjamin bahwa Merchant tidak akan melakukan hal-hal
					sebagai berikut:
				</li>
				<li>
					melakukan upaya pemecahan kode (reverse engineering) terhadap Sistem
					Booking Mamikos dan Sistem Manajemen Properti MamiPAY;
				</li>
				<li>
					melakukan hal-hal yang mengakibatkan kerusakan terhadap Sistem Booking
					Mamikos dan Sistem Manajemen Properti MamiPAY dengan sengaja; atau
				</li>
				<li>
					melakukan hal-hal yang bertujuan untuk mencuri data Transaksi dan
					User.
				</li>
				<li>
					Merchant wajib menjamin keamanan data informasi User dan miliknya.
					Dalam hal Merchant Kos melanggar ketentuan ini, maka Sistem Booking
					Mamikos dan Sistem Manajemen Properti MamiPAY berhak untuk
					mengnonaktifkan akun Merchant sampai Merchant bisa memenuhi
					kewajibannya.
				</li>
			</ul>
			<h1>Pasal 8 : Jangka Waktu Dan Pengakhiran Perjanjian</h1>
			<ul>
				<li>
					Perjanjian ini berlaku selama 1 (satu) tahun sejak tanggal
					ditandatanganinya Perjanjian ini dan akan diperpanjang secara
					otomatis, sepanjang tidak ada pemberitahuan untuk mengakhiri
					Perjanjian sekurang-kurangnya 30 (tiga puluh) hari kalendar sebelum
					tanggal berakhirnya Perjanjian.
				</li>
				<li>
					Para Pihak berhak untuk sewaktu-waktu mengakhiri Perjanjian ini
					apabila terjadi salah satu dari hal-hal sebagai berikut:
				</li>
				<li>
					Salah satu Pihak melanggar salah satu ketentuan di Perjanjian ini, dan
					pelanggaran tersebut tidak dapat diperbaiki;
				</li>
				<li>
					Salah satu Pihak melanggar salah satu ketentuan di Perjanjian ini, dan
					pelanggaran tersebut dapat diperbaiki, namun Pihak yang melanggar
					tidak mampu memperbaiki pelanggaran tersebut dalam jangka waktu 30
					(tiga puluh) hari kalendar sejak tanggal terjadinya pelanggaran
					tersebut; atau
				</li>
				<li>
					Salah satu Pihak pailit, menghentikan layanan serta kegiatan
					operasionalnya dan izin usahanya dicabut baik sebagian atau secara
					keseluruhan, memenuhi ketentuan Force Majeure Pasal 15 Perjanjian ini,
					dan oleh karena itu tidak mampu melanjutkan kegiatan usahanya;
				</li>
				<li>
					Pengakhiran Perjanjian ini tidak melepaskan Para Pihak dari
					kewajibannya yang timbul sebelum dan/atau pada saat pengakhiran
					Perjanjian ini. Para Pihak wajib menyelesaikan kewajiban dimaksud
					dalam jangka waktu selambat-lambatnya 7 (tujuh) Hari Kerja setelah
					tanggal efektif berakhirnya Perjanjian ini.
				</li>
				<li>
					Dalam pelaksanaan pengakhiran Perjanjian ini, Para Pihak sepakat untuk
					mengesampingkan ketentuan- yang tercantum di dalam Pasal 1266 dan
					Pasal 1267 Kitab Undang-Undang Hukum Perdata Indonesia.
				</li>
			</ul>
			<h1>Pasal 9 : Pernyataan Dan Jaminan</h1>
			<ul>
				<li>
					Masing-masing penandatangan Perjanjian ini adalah pihak yang berwenang
					atau wakil yang dalam mengikatkan dirinya atau badan usahanya (baik
					badan hukum maupun bukan badan hukum); dalam hal ternyata penanda
					tangan bukan pihak yang berwenang atau wakil yang sah, maka
					penandatangan bertanggung jawab secara pribadi.
				</li>
				<li>
					Para Pihak menjamin bahwa dokumen yang diberikan kepada pihak lainnya
					adalah merupakan dokumen resmi, asli, sah, masih berlaku dan setiap
					informasi yang tercantum dalam dokumen tersebut adalah benar, sesuai
					dengan keadaan sebenarnya dan bukan merupakan rekayasa maupun tipu
					muslihat atau kebohongan serta sesuai dengan ketentuan atau peraturan
					yang berlaku.
				</li>
				<li>
					Para Pihak dengan ini dan untuk seterusnya dikemudian hari membebaskan
					dan melepaskan Pihak lainnya dari segala gugatan, tuntutan atau
					tagihan dari siapapun yang tidak langsung berhubungan dengan
					Perjanjian ini.
				</li>
			</ul>
			<h1>Pasal 10 : Domisili Hukum dan Penyelesaian Sengketa</h1>
			<ul>
				<li>
					Perjanjian ini diatur dan tunduk pada hukum yang berlaku di Negara
					Republik Indonesia.
				</li>
				<li>
					Apabila dalam pelaksanaan Perjanjian ini terjadi perbedaan pendapat
					dan/atau penafsiran maupun terjadi perselisihan diantara Para Pihak
					dalam Perjanjian ini, maka Para Pihak sepakat untuk menyelesaikannya
					secara musyawarah dengan itikad baik untuk mencapai mufakat. Apabila
					musyawarah mufakat tidak tercapai, maka Para Pihak sepakat memilih
					kedudukan hukum yang tetap dan seumumnya di Kantor Kepaniteraan
					Pengadilan Negeri Jakarta Pusat di Jakarta sebagai sarana penyelesaian
					perselisihan tersebut.
				</li>
			</ul>
			<h1>Pasal 11 : Kerahasiaan</h1>
			<ul>
				<li>
					Para Pihak sepakat bahwa pertukaran informasi yang muncul karena
					Perjanjian ini dikategorikan sebagai rahasia dan untuk itu Para Pihak
					sepakat untuk saling menjaga kerahasiaan informasi tersebut kecuali
					telah mendapatkan izin tertulis dari salah satu pihak lainnya dan/atau
					informasi tersebut telah berlaku dan diketahui secara umum.
				</li>
				<li>
					Selama Perjanjian ini berlaku maupun setelah berakhirnya Perjanjian
					ini, Para Pihak dan semua pihak yang bekerja pada/untuk Para Pihak
					berikut afiliasinya wajib menjaga kerahasiaan data dan/atau informasi
					dalam bentuk apapun mengenai User baik yang diperoleh dari pihak
					lainnya maupun dari Transaksi Internet melalui Sistem Pembayaran
					Internet baik yang termasuk sebagai rahasia Bank dan/atau Para Pihak,
					ataupun hal-hal yang wajib dan sepatutnya dirahasiakan kepada
					siapapun.
				</li>
			</ul>
			<h1>Pasal 12 : Keadaan Memaksa (Force Majeure)</h1>
			<ul>
				<li>
					Yang dimaksud dengan Force Majeure adalah suatu peristiwa atau keadaan
					yang terjadi di luar kekuasaan atau kemampuan salah satu atau Para
					Pihak, yang mengakibatkan salah satu atau Para Pihak tidak dapat
					melaksanakan hak-hak dan/atau kewajiban-kewajiban sesuai dengan
					ketentuan-ketentuan di dalam Perjanjian ini, termasuk namun tidak
					terbatas kepada kebakaran, tidak berfungsinya tenaga listrik untuk
					jangka waktu lama, putusnya koneksi internet, bencana alam,
					peperangan, aksi militer, huru-hara, malapetaka, pemogokan, epidemi,
					dan kebijaksanaan maupun peraturan Pemerintah atau penguasa setempat
					yang secara langsung dapat mempengaruhi pemenuhan pelaksanaan
					Perjanjian ini.
				</li>
				<li>
					Dalam hal terjadi Force Majeure, Pihak yang mengalami peristiwa yang
					dikategorikan sebagai Force Majeure wajib memberikan pemberitahuan
					secara tertulis tentang hal tersebut kepada Pihak lainnya
					selambat-lambatnya 7 (tujuh) hari terhitung sejak tanggal terjadinya
					Force Majeure tersebut, karenanya Para Pihak dengan ini menjadi tidak
					bertanggung jawab atas kelalaian atau keterlambatan pelaksanaan
					seluruh atau sebagian Perjanjian ini.
				</li>
				<li>
					Segala permasalahan yang timbul sebagai akibat dari terjadinya Force
					Majeure tersebut akan diselesaikan secara musyawarah oleh Para Pihak.
					Dalam hal Force Majeure berkanjut terus-menerus sampai dengan 30 (tiga
					puluh) hari kalender, maka pihak yang tidak mengalami Force Majeure
					dapat mengakhiri Perjanjian ini.
				</li>
			</ul>
			<h1>Pasal 13 : Korespondensi</h1>
			<p>
				Untuk kelancaran pelaksanaan Perjanjian ini, segala surat menyurat atau
				korespondensi dilakukan ke alamat sebagai berikut :
			</p>
			<h1>PT MAMA TEKNOLOGI PROPERTI</h1>
			<h1>Puri Gejayan Indah B12 Kaliwaru CondongCatur</h1>
			<h1>Sleman DI Yogyakarta 55281</h1>
			<h1 class="distance">0274 2923340</h1>
			<h1>Pasal 14 : Ketentuan Lainnya</h1>
			<ul>
				<li>
					Segala Lampiran, Addendum, Surat Komunikasi, serta dokumen-dokumen
					lainnya yang dibuat berdasarkan atau sehubungan dengan Perjanjian ini,
					merupakan bagian integral dari dan menjadi lampiran yang tidak
					terpisahkan dari Perjanjian ini.
				</li>
				<li>
					<b><u>Perubahan.</u></b> Perjanjian ini tidak boleh diubah atau
					ditambah kecuali disetujui oleh Para Pihak dan termaktub dalam
					perjanjian formal yang ditandatangani oleh Para Pihak.
				</li>
				<li>
					<b><u>Addendum.</u></b> Hal-hal yang tidak atau belum diatur dalam
					atau perubahan atas Perjanjian ini, akan diatur kemudian melalui
					addendum yang disepakati dan ditandatangani oleh Para Pihak; addendum
					mana menjadi bagian integral dan menjadi lampiran yang tidak
					terpisahkan dari Perjanjian ini.
				</li>
				<li>
					<b><u>Pelepasan Hak.</u></b> Dalam hal terjadi kegagalan, penundaan
					atau keterlambatan oleh salah satu Pihak dalam melaksanakan haknya
					atau menuntut pemenuhan kewajiban dari Pihak lainnya berdasarkan
					Perjanjian ini, maka kegagalan, penundaan atau keterlambatan tersebut
					bukan merupakan pelepasan hak oleh pihak tersebut untuk dikemudian
					hari melaksanakan haknya atau menuntut pemenuhan kewajiban pihak
					lainnya berdasarkan Perjanjian ini.
				</li>
				<li>
					<b><u>Tidak Ada Pengalihan.</u></b> Perjanjian ini mengikat dan dibuat
					untuk kepentingan dari setiap Pihak dan penerima dan/atau pengganti
					haknya masing-masing, akan tetapi dengan ketentuan, bahwa tidak ada
					Pihak yang boleh mengalihkan setiap hak-hak yang timbul dari atau
					berkenaan dengan Perjanjian ini kepada pihak ketiga manapun, tanpa
					persetujuan tertulis terlebih dahulu dari Pihak lainnya.
				</li>
			</ul>
		</div>
	</div>
@endsection


@section('scripts')

@endsection