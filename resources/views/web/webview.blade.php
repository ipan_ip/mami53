<!DOCTYPE html>
<html>
<head>
	<title></title>


	<style type="text/css">
    
    .acbutton {
		    background-color: #1BAA56;
		    color: white;
		    font-size: 20px;
		    font-weight: bold;
		    margin: 35px 0px 0px 0px;
		    padding: 20px 45px;
		    cursor: pointer;
		    text-decoration: none;
    }
    
    body , html{ padding:0px; margin: 0px;}
    .fonti { font-family: 'Lato', sans-serif; } 
    header {
    	background: #1BAA56;
    	width: 100%;
    	padding: 1px 3px 3px 3px;
    	margin: 0px;
    }
    header h1 { padding-top: 0px; color: #ffffff; font-weight: bold;}
    
    header h1 i { margin-left: 10px; margin-right: 50px; } 
    section{
       width: 100%;
       margin: 50px 0px 30px 0px;
    }

    section h1{
        margin-bottom: 50px;
    	text-align: center;
    	font-weight: bold;
    	/*font-size: 30px;*/
    }

    .linked { text-decoration: none; color: #000000; }
    .linked:hover{ text-decoration: none; color: #000000;}
    .linked:active{ text-decoration: none; color: #000000;  }

    section h1 span { color:  #1BAA56; }

    img.center {
    display: block;
    width: 70%;
    margin: 0 auto;
    }

    footer {
    	text-align: center;
    	width: 100%;
    	display: table;
    }
	</style>
   
        <link rel="stylesheet" href="{{ URL::to('dist/css/vendor.css') }}">

	<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
 -->
</head>
<body>

@yield('content')

</body>
</html>