@extends('web.input.inputLayout')

@section('meta-custom')
  @if(is_null($room['apartment_project_id']))
  <title>Form Edit Kost MamiKos - Cari Kost Gampang & Akurat</title>

  <meta name="description" content="Form Edit Kost MamiKos - Cari Kost Gampang & Akurat. Promosikan Kost Mu Gratis di Mamikos.com!">
  <meta property="og:title" content="Form Edit Kost MamiKos - Cari Kost Gampang & Akurat" />
  <meta property="og:description" content="Form Edit Kost MamiKos - Cari Kost Gampang & Akurat. Promosikan Kost Mu Gratis di Mamikos.com!" />
  <meta property="og:image" content="/assets/mamikos_form.png" />
  <meta name="twitter:title" content="Form Edit Kost MamiKos - Cari Kost Gampang & Akurat">
  <meta name="twitter:description" content="Form Edit Kost MamiKos - Cari Kost Gampang & Akurat. Promosikan Kost Mu Gratis di Mamikos.com!">
  <meta name="twitter:image" content="/assets/mamikos_form.png">

	<meta name="property_type" content="Kost" />

  @else
  <title>Form Edit Unit Apartemen MamiKos - Cari Kost Gampang & Akurat</title>

  <meta name="description" content="Form Edit Unit Apartemen MamiKos. Promosikan Unit Apartemen Mu Gratis di Mamikos.com!">
  <meta property="og:title" content="Form Edit Unit Apartemen MamiKos" />
  <meta property="og:description" content="Form Edit Unit Apartemen MamiKos. Promosikan Unit Apartemen Mu Gratis di Mamikos.com!" />
  <meta property="og:image" content="/assets/mamikos_form.png" />
  <meta name="twitter:title" content="Form Edit Unit Apartemen MamiKos">
  <meta name="twitter:description" content="Form Edit Unit Apartemen MamiKos. Promosikan Unit Apartemen Mu Gratis di Mamikos.com!">
  <meta name="twitter:image" content="/assets/mamikos_form.png">

  <meta name="property_type" content="Apartment" />
  @endif
  <meta name="property_index" content="{{$room['song_id']}}" />

  @if(is_null($room['apartment_project_id']))
    @include('web.@style.preloading-style')
  @endif
@endsection

@section('content')
  @if(is_null($room['apartment_project_id']))
    @include('web.@style.preloading-content')
  @endif

  <div id="app" class="wrapper">
  	<app></app>
  </div>
@endsection

@section('script')
  @include('web.@script.global-auth')

	@if(is_null($room['apartment_project_id']))
    <script src="{{ mix_url('dist/js/input-edit.js') }}"></script>
	@else
    <script src="{{ mix_url('dist/js/input-edit-apartment.js') }}"></script>
  @endif

  @include('web.@script.aws-kds-logged-user-tracker')
@endsection
