@extends('web.input.inputLayout')

@section('meta-custom')
  <title>Form Pendaftaran Kost Agen MamiKos - Cari Kost Gampang & Akurat</title>

  <meta name="description" content="Form Pendaftaran Kost Agen MamiKos - Cari Kost Gampang & Akurat. Promosikan Kost Mu Gratis di Mamikos.com!">
  <meta property="og:title" content="Form Pendaftaran Kost Agen MamiKos - Cari Kost Gampang & Akurat" />
  <meta property="og:description" content="Form Pendaftaran Kost Agen MamiKos - Cari Kost Gampang & Akurat. Promosikan Kost Mu Gratis di Mamikos.com!" />
  <meta property="og:image" content="/assets/mamikos_form.png" />
  <meta name="twitter:title" content="Form Pendaftaran Kost Agen MamiKos - Cari Kost Gampang & Akurat">
  <meta name="twitter:description" content="Form Pendaftaran Kost Agen MamiKos - Cari Kost Gampang & Akurat. Promosikan Kost Mu Gratis di Mamikos.com!">
  <meta name="twitter:image" content="/assets/mamikos_form.png">

  <meta name="property_type" content="Kost" />
@endsection

@section('content')
  <div id="app" class="wrapper">
  	<container-input-kost></container-input-kost>
  </div>
@endsection

@section('script')
  @include('web.@script.global-auth')

  <script src="{{ mix_url('dist/js/input-agent.js') }}"></script>
@endsection