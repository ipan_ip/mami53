@extends('web.input.inputLayout')

@section('meta-custom')
	@if($property_type == 'Kost')
		<title>Form Pendaftaran Kost MamiKos - Cari Kost Gampang & Akurat</title>
	@elseif($property_type == 'Apartment')
		<title>Form Pendaftaran Unit Apartemen MamiKos</title>
	@endif

	@if($property_type == 'Kost')
	<meta name="description" content="Form Pendaftaran Kost MamiKos - Cari Kost Gampang & Akurat. Promosikan Kost Mu Gratis di Mamikos.com!">
	<meta property="og:title" content="Form Pendaftaran Kost MamiKos - Cari Kost Gampang & Akurat" />
	<meta property="og:description" content="Form Pendaftaran Kost MamiKos - Cari Kost Gampang & Akurat. Promosikan Kost Mu Gratis di Mamikos.com!" />
	<meta property="og:image" content="/assets/mamikos_form.png" />
	<meta name="twitter:title" content="Form Pendaftaran Kost MamiKos - Cari Kost Gampang & Akurat">
	<meta name="twitter:description" content="Form Pendaftaran Kost MamiKos - Cari Kost Gampang & Akurat. Promosikan Kost Mu Gratis di Mamikos.com!">
	<meta name="twitter:image" content="/assets/mamikos_form.png">

	@elseif($property_type == 'Apartment')
	<meta name="description" content="Form Pendaftaran Unit Apartemen MamiKos. Promosikan Unit Apartemen Mu Gratis di Mamikos.com!">
	<meta property="og:title" content="Form Pendaftaran Unit Apartemen MamiKos" />
	<meta property="og:description" content="Form Pendaftaran Unit Apartemen MamiKos. Promosikan Unit Apartemen Mu Gratis di Mamikos.com!" />
	<meta property="og:image" content="/assets/mamikos_form.png" />
	<meta name="twitter:title" content="Form Pendaftaran Unit Apartemen MamiKos">
	<meta name="twitter:description" content="Form Pendaftaran Unit Apartemen MamiKos. Promosikan Unit Apartemen Mu Gratis di Mamikos.com!">
	<meta name="twitter:image" content="/assets/mamikos_form.png">
	@endif

	<meta name="property_type" content="{{ $property_type }}" />
	<meta name="input_as" content="{{ $input_as }}" />
	<meta name="phone_number" content="{{ $phone_number }}" />

	@if($property_type == 'Kost')
	<meta name="claim" content="{{ $claim }}" />
	@elseif($property_type == 'Apartment')
	<meta name="claim" content="false" />
	@endif

	<meta name="user_name" content="{{ is_numeric($user->name) ? '' : $user->name }}" />

	@if($user->password == '')
	<meta name="user_key" content="false" />
	@else
	<meta name="user_key" content="true" />
	@endif

	<meta name="user_email" content="@if (filter_var($user->email, FILTER_VALIDATE_EMAIL)) {{ $user->email }} @else {{ '' }} @endif" />
@endsection

@section('content')
	<div id="app" class="wrapper">
		@if($property_type == 'Kost')

			<container-input-property-kost></container-input-property-kost>

		@elseif($property_type == 'Apartment')

			<container-input-property-apartment></container-input-property-apartment>

		@endif
	</div>
@endsection

@section('script')
	@include('web.@script.global-auth')

	@if($property_type == 'Kost')
		<script>
			var chatData = {
				chatName: '',
				adminId: null
			};
		</script>

		@if($is_first && is_null($user->date_owner_limit))
			@include('web.@script.sendbird-widget')

			<script>
				chatData = {
					chatName: '{{ $chat_name }}',
					adminId: {{ $default_admin_id }}
				};
			</script>
		@endif

		<script src="{{ mix_url('dist/js/input-properti.js') }}"></script>


	@elseif($property_type == 'Apartment')
		<script src="{{ mix_url('dist/js/input-properti-apartment.js') }}"></script>
	@endif

	@include('web.@script.aws-kds-logged-user-tracker')
@endsection
