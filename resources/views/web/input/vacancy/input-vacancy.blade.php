@extends('web.input.inputLayout')

@section('meta-custom')
  <title>Form Lowongan Kerja</title>

  @if(isset($id))
  <meta name="seq" content="{{ $id }}">
  @else
  <meta name="seq" content="-1">
  @endif

@endsection

@section('content')
  <div id="app" class="wrapper">
    <container-input-vacancy></container-input-vacancy>
  </div>
@endsection

@section('data')
  <script type="text/javascript">
    var educationOptions = @JZON($educationOptions);
    var spesialisasiOptions = @JZON($spesialisasiOption);
    var jobTypeOptions =  @JZON($jobTypeOptions);
  </script>
@endsection

@section('script')
  @include('web.@script.global-auth')

  <script src="{{ mix_url('dist/js/input-vacancy.js') }}"></script>

  @include('web.@script.aws-kds-logged-user-tracker')
@endsection