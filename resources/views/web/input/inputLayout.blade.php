<!DOCTYPE html>
<html>
<head>
  @if($_SERVER['HTTP_HOST'] != 'mamikos.com')
  <meta name="robots" content="noindex, nofollow">
  @endif
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <meta name="application-name" content="Mamikos">
  <meta name="theme-color" content="#1BAA56">
  <link rel="manifest" href="/json/manifest.json">

  @include('web.@meta.link-prebrowsing')

  <base href="/">
  
  @include('web.@meta.favicon')

  <meta name="author" content="https://plus.google.com/u/1/+Mamikos/posts">
  <meta name="og:site_name" content="Mamikos"/>
  <meta property="og:url" content="{{ $_SERVER['REQUEST_URI'] }}" />
  <meta property="og:type" content="website" />
  <meta property="fb:app_id" content="607562576051242"/>
  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:site" content="@mamikosapp">
  <meta name="twitter:creator" content="@mamikosapp">
  <meta name="twitter:domain" content="mamikos.com">
  <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Expires" content="0" />
  @yield('meta-custom')

  

  <!--  Fonts, Icons and CSS
  ================================================== -->
  @include('web.part.external.global-head')
  <link href="{{ mix_url('dist/css/common.css') }}" rel="stylesheet">

  <style>
  @media (min-width: 992px) {
    body,
    input,
    button {
      font-size: 15px !important;
    }
  }
  #navbarCommon {
    font-size: 14px;
  }
  .form-group {
    margin-bottom: 30px;
  }
  @media (max-width: 991px) {
      .input-container {
      padding-top: 15px;
      padding-bottom: 15px;
    }
  }
  @media (min-width: 992px) {
    .input-container {
      padding-top: 30px;
      padding-bottom: 30px;
    }
  }
  .section-container {
    margin-top: 30px;
  }

  .btn {
    font-weight: bold;
  }
  .input-footer-btn {
    display: -webkit-box;
    display: -moz-box;
    display: -ms-flexbox;
    display: -webkit-flex;
    display: flex;
    -webkit-align-items: center;
    -moz-align-items: center;
    -ms-align-items: center;
    align-items: center;
    -webkit-justify-content: flex-end;
    -moz-justify-content: flex-end;
    -ms-justify-content: flex-end;
    justify-content: flex-end;
    margin-top: 15px;
  }
  @media (min-width: 768px) {
    .input-footer-btn > .btn {
      width: 25%;
    }
  }
  .input-footer-btn > .btn:last-child {
    margin-left: 10px;
  }
  </style>

  @yield('style')

  

  @yield('data')
</head>
<body>
  @include('web.part.external.gtm-body')

  <!-- Navbar -->
  {{-- @if(isset($has_navbar))
    @if($has_navbar == 1)
      @include('web.part.navbarinput')
    @endif
  @else
    @include('web.part.navbarinput')
  @endif --}}

  @yield('content')



  <!--  JavaScripts
  ================================================== -->
  <script src="{{ mix_url('dist/js/common.js') }}"></script>
  @include ('web.part.script.bugsnagapikey')

  @yield('script')

  @include ('web.part.script.tokenerrorhandlescript')

  {{-- @if(isset($has_navbar))
    @if($has_navbar == 1)
      @include ('web.part.script.navbarscript')
    @endif
  @else
    @include ('web.part.script.navbarscript')
  @endif --}}

  @include ('web.part.script.browseralertscript')
</body>
</html>