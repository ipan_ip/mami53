@extends('web.user.userLayout')

@section('meta-custom')
<title>Histori Kost - Mamikos</title>
<meta name="twitter:title" content="Histori Kost MamiKos - Cari Kost Gampang & Akurat.">
<meta name="twitter:description" content="Histori Kost MamiKos - Cari Kost Gampang & Akurat.">
<meta property="og:description" content="Histori Kost MamiKos - Cari Kost Gampang & Akurat." />
<meta name="description" content="Histori Kost MamiKos - Cari Kost Gampang & Akurat." />
@endsection

@section('content')
<div id="app">
	<container-user-history></container-user-history>
</div>
@endsection

@section('script')
	@include('web.@script.global-auth')

	@if (Auth::check() and Auth::user()->is_owner == 'false')
			@include('web.@script.sendbird-widget')
	@endif

	<script src="{{ mix_url('dist/js/user-history.js') }}"></script>
	@include('web.@script.aws-kds-logged-user-tracker')
@endsection