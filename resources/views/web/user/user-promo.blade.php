@extends('web.user.userLayout')

@section('meta-custom')
<title>{{$meta['title']}}</title>
<meta name="description" content="{{$meta['description']}}" />
<meta name="keywords" content="{{$meta['keywords']}}">
<meta name="twitter:title" content="{{$meta['title']}}">
<meta name="twitter:description" content="{{$meta['description']}}">
<meta property="og:description" content="{{$meta['description']}}" />

@endsection

@section('content')
<div id="app">
	<container-user-promo></container-user-promo>
</div>

@include ('web.part.modalloginuser')
@endsection

@section('script')
	@include('web.@script.global-auth')

	@if (Auth::check() and Auth::user()->is_owner == 'false')
			@include('web.@script.sendbird-widget')
	@endif

	<script src="{{ mix_url('dist/js/user-promo.js') }}"></script>
@endsection