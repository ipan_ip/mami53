<!DOCTYPE html>
<html lang="id">
	<head resource="/">
		@include('web.@meta.common-meta-link')
		<link rel="preload" as="font" type="font/woff2" href="/general/fonts/Lato/lato-v17-latin-regular.woff2" crossorigin>
		<link rel="preload" as="font" type="font/woff2" href="/general/fonts/Lato/lato-v17-latin-700.woff2" crossorigin>
		<link rel="preload" href="/css/fonts/lato.css" as="style">
    <link rel="stylesheet" href="/css/fonts/lato.css">

		<link rel="preload" href="{{ mix_url('dist/css/app.css') }}" as="style">
		<link rel="preload" href="{{ mix_url('dist/js/@vendor/main/script.js') }}" as="script">

		@yield('head')

		@if(!isset($from_consultant_tools))
			@include('web.@script.global-auth')
		@endif

		@include('web.@script.polyfill-snippet')

		@include('web.@script.utility')

		@include('web.@script.third-parties-sdk-head')


		@yield('data')


		@include('web.@script.bugsnag-api-key')

		<link rel="stylesheet" href="{{ mix_url('dist/css/app.css') }}">

		@yield('styles')
	</head>

	<body>
		@include('web.@script.gtm-body')

		@include('web.@script.service-worker')

		{{-- unuse this temporarily for experiment --}}
		{{-- @include('web.@content.moengage-notif-soft-ask') --}}


		@yield('content')


		<script src="{{ mix_url('dist/js/@vendor/main/script.js') }}"></script>

		@yield('scripts')

		@include('web.@script.session-token')

		@if(!isset($from_consultant_tools))
			@include('web.@script.browser-alert')
		@endif
	</body>
</html>
