<script>
var dataLayerParams;

try {
	dataLayerParams = new URLSearchParams(window.location.search);
} catch (e) {
	dataLayerParams = null;
}

var dataLayerProps = {
	device: "none",
	utm_source: "none",
	utm_medium: "none",
	utm_content: "none",
	utm_campaign: "none",
};

if (window.innerWidth < 768) {
	dataLayerProps.device = "mobile";
} else {
	dataLayerProps.device = "desktop";
};

try {
	dataLayerProps.utm_source = dataLayerParams.get('utm_source');
	if (dataLayerProps.utm_source === null) dataLayerProps.utm_source = "none";
} catch (e) {
	dataLayerProps.utm_source = "none";
};

try {
	dataLayerProps.utm_medium = dataLayerParams.get('utm_medium');
	if (dataLayerProps.utm_medium === null) dataLayerProps.utm_medium = "none";
} catch (e) {
	dataLayerProps.utm_source = "none";
};

try {
	dataLayerProps.utm_content = dataLayerParams.get('utm_content');
	if (dataLayerProps.utm_content === null) dataLayerProps.utm_content = "none";
} catch (e) {
	dataLayerProps.utm_content = "none";
};

try {
	dataLayerProps.utm_campaign = dataLayerProps.dataLayerParams.get('utm_campaign');
	if (dataLayerProps.utm_campaign === null) dataLayerProps.utm_campaign = "none";
} catch (e) {
	dataLayerProps.utm_campaign = "none";
};


dataLayer.push({
	"language": "id",
	"page_category": "ViewHome",
	"currency": "IDR",
	"device": dataLayerProps.device,
	"utm_source": dataLayerProps.utm_source,
	"utm_medium": dataLayerProps.utm_medium,
	"utm_content": dataLayerProps.utm_content,
	"utm_campaign": dataLayerProps.utm_campaign,
	"step": 1,
	"page_url": "https://mamikos.com",
	@if (Auth::check())
	"customer_id": "{{ (Auth::user()->id) }}",
	"profile_id": "{{ (Auth::user()->id) }}",
	"gender": "{{ (Auth::user()->gender) }}"
	@else
	"customer_id": "none",
	"profile_id": "none",
	"gender": "none"
	@endif
});
</script>