<script>
var cityOptions = @JZON($cityOptions);
var relationLandings = [];

@foreach ($cityOptions as $key => $city)
	@if($key == 0)
	var cityOption = {
		id: {{ $city['id'] }},
		landingId: {{ $city['landing_id'] }},
		name: '{{ $city['name'] }}'
	};
	@endif

	relationLandings.push(@JZON($cityOptions[$key]->landing_kost));
@endforeach

var initialStaticRooms = @JZON($landingRecommendationRooms);

////////////////////////////////////////////////////////////////////////////////

var childLandings = [];
var relationLandingsChild = [];
var childLandingActivePosition = {};

@if (count($childLandings) > 0)
	@foreach ($childLandings as $key => $childLanding)
		childLandings.push(@JZON($childLanding));
		relationLandingsChild.push(@JZON($childLanding->landing_kost));
	@endforeach

	childLandingActivePosition = {
		landingId: relationLandingsChild[0].id,
		slug: relationLandingsChild[0].slug
	};
@endif

var initialStaticRoomsChild = @JZON($childLandingRecommendationRooms);
</script>
