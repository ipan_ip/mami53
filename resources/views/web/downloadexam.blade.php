@extends('web.@layout.globalLayout')


@section('head')
  <title>{{ $title }}</title>
  <meta name="description" content="{{ $content }}" />

  <link rel="canonical" href="https://mamikos.com/">

  <meta property="og:url" content="<?php echo Request::url() ?>" />
  <meta property="og:title" content="Mamikos - {{ $title }} ...">
  <meta property="og:image" content="/assets/share-image-default.jpg" />
  <meta property="og:description" content="{{ implode(' ', array_slice(explode(' ', strip_tags($content)), 0, 48)) }} ..." />


  <meta name="twitter:url" content="https://mamikos.com/">
  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:site" content="@mamikosapp">
  <meta name="twitter:title" content="Mamikos - {{ $title }} ...">
  <meta name="twitter:description" content="{{ implode(' ', array_slice(explode(' ', strip_tags($content)), 0, 48)) }} ...">
  <meta name="twitter:image" content="/assets/share-image-default.jpg">

  <meta name="keyphrases" content="Info Kost, Cari Kost, Sewa Kost, Kost Bebas, Kost Murah, Kost pasutri, Aplikasi Kost, Aplikasi Pencarian Kost, Aplikasi Info Kost, APlikasi Cari Kost, Kost, Mamikost, Mamikosapp, Kamar Kost, Kamar Kos, Kostan, Kos, Rumah Kost, Rumah Kos, Kost Harian">

  <meta name="classification" content="Business, Rent House, Sewa Kost, Property, Rent Room, Info Kost, Information, Kost, Room, Cari Kost, Kost Murah, Kost Bebas, Application, Mobile Application, Kamar Kost, Kamar Kos, Kostan, Kos, Rumah Kost, Rumah Kos, Kost Harian">

  <meta name="keywords" content="Rekomendasi, Favorit, Kos">

  <link rel="preload" href="{{ mix_url('dist/js/_download-exam/app.js') }}" as="script">

@endsection


@section('styles')
  @include('web.@style.preloading-style')
@endsection

@section('content')
  @include('web.@style.preloading-content')

  <div id="app">
    <app></app>
  </div>
@endsection


@section('scripts')
  <script>
    var isSubscribed = '{{ $isSubscribed }}';

    var downloadArticle = {
      examId: '{{$_seq}}',
      slug: '{{ $slug }}',
      title: '{{ $title }}',
      subtitle: '{{ $subtitle }}',
      excerpt: @JZON($excerpt),
      content: @JZON($content)
    };

    var examFiles = [];

    @foreach($files as $file)
      examFiles.push(@JZON($file));
    @endforeach


  </script>
  @if (Auth::check())
  <script>
    var isAuth = true;
    var expiredDownloadToken = "{{(Auth::user()->id)}}";

  </script>
  @else
  <script>
    var isAuth = false;
    var expiredDownloadToken = "false";

  </script>
  @endif

  @if (Auth::check())
    @if (Auth::user()->is_owner == 'false')
      @include('web.@script.sendbird-widget')
    @endif
  @endif

  <script src="{{ mix_url('dist/js/_download-exam/app.js') }}" async defer></script>

  @include('web.@script.global-auth')


@endsection
