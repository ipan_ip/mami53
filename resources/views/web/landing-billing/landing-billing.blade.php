@extends('web.@layout.globalLayout')

@section('head')
@if (!empty($_SERVER['HTTP_HOST']))
@if ($_SERVER['HTTP_HOST'] == 'mamikos.com')
<meta name="robots" content="index, follow">
@else
<meta name="robots" content="noindex, nofollow">
@endif
@else
<meta name="robots" content="noindex, nofollow">
@endif

<title>Manajemen Kos</title>

<meta name="description" content="Landing Billing">

<meta name="keywords" content="Landing Billing">

<link rel="canonical" href="{{ Request::url() }}">

<meta name="google-site-verification" content="w0YS5vfldeVZyEmya7TP46hb4icE_SSOWLEVmzFPAJQ" />

<meta name="keyphrases" content="Info Kost, Cari Kost, Sewa Kost, Kost Bebas, Kost Murah, Kost pasutri, Aplikasi Kost, Aplikasi Pencarian Kost, Aplikasi Info Kost, APlikasi Cari Kost, Kost, Mamikost, Mamikosapp, Kamar Kost, Kamar Kos, Kostan, Kos, Rumah Kost, Rumah Kos, Kost Harian">

<meta name="classification" content="Business, Rent House, Sewa Kost, Property, Rent Room, Info Kost, Information, Kost, Room, Cari Kost, Kost Murah, Kost Bebas, Application, Mobile Application, Kamar Kost, Kamar Kos, Kostan, Kos, Rumah Kost, Rumah Kos, Kost Harian">

<link rel="preload" href="{{ mix_url('dist/js/_landing-billing/app.js') }}" as="script">
@endsection

@section('styles')
@include('web.@style.preloading-style')
@endsection


@section('content')
@include('web.@style.preloading-content')

<div id="app">
  <app></app>
</div>
@endsection


@section('scripts')
@if (Auth::check())
@if (Auth::user()->is_owner == 'false')
@include('web.@script.sendbird-widget')
@endif
@endif
<script src="{{ mix_url('dist/js/_landing-billing/app.js') }}" async defer></script>
@endsection