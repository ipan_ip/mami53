@extends('web.webview')
@section('content')
<a href="bang.kerupux.com://owner_profile" class="linked">
	<header>
		<h1 class="fonti"><i class="fa fa-arrow-left"></i> Aktifkan Iklan Kost</h1>
	</header>

	<section>
	     <h1 class="fonti">Untuk mengaktifkan iklan <br/>bisa klik tombol <span>ON</span> <br/>di akun pemilik</h1>
	     <img src="{{ URL::to('assets/switch-on.gif') }}" class="img-responsive center" alt="" />
	</section>

	<footer>
		<a class="acbutton fonti" href="bang.kerupux.com://owner_profile">OK</a>
	</footer>
</a>
@stop