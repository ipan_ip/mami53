<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="noindex, nofollow">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <title>Test QA - MamiKos</title>

    <style>
      .navbar-mami {
        background: #1BAA56;
        margin-bottom: 50px;
      }

      #form-tab {
        margin-top: 30px;
        margin-bottom: 30px;
      }

      .footer {
        border-top: 1px solid #ddd;
        margin-top: 50px;
        padding-top: 30px;
      }
    </style>
  </head>
  <body>

    <nav class="navbar navbar-mami">
      <a class="navbar-brand" href="#">
        <img src="{{ asset('assets/logo/mamikos_header_logo_full_default.png') }}" height="50" alt="">
      </a>
    </nav>
    
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          
          <h1>Edit User Profile</h1>

          @if (session('message'))
            <div class="alert alert-info">
              {{ session('message') }}
            </div>
          @endif

          @if (count($errors) > 0)
            <div class="alert alert-danger">
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
          @endif
          
          <ul class="nav nav-tabs" id="form-tab" role="tablist">
            <li class="nav-item">
              <a href="#general-profile" class="nav-link 
                {{ $inputMode == 'general' ? 'active' : ''}}" 
                id="general-profile-tab" data-toggle="tab" role="tab" aria-controls="general-profile" 
                aria-selected="{{ $inputMode == 'general' ? 'true' : 'false'}}">
                General Profile
              </a>
            </li>
            <li class="nav-item">
              <a href="#cv" class="nav-link {{ $isInputCVAllowed ? '' : 'disabled' }} 
                {{ $inputMode == 'cv' ? 'active' : ''}}" 
                id="cv-tab" data-toggle="tab" role="tab" aria-controls="cv" 
                aria-selected="{{ $inputMode == 'cv' ? 'true' : 'false'}}">
                CV
              </a>
            </li>
          </ul>

          <div class="tab-content" id="tab-content">
            
            <div class="tab-pane fade show 
              {{ $inputMode == 'general' ? 'active' : ''}}" 
              id="general-profile" role="tab-panel" aria-labelledby="general-profile-tab">
              
              <form action="/test-qa/general" method="POST">
                {{ csrf_field() }}


                <div class="form-group row">
                  <label for="name-input" class="col-sm-2 col-form-label">Name</label>
                  <div class="col-sm-10">
                    <input type="text" name="name" id="name-input" class="form-control" 
                      value="{{ $input['name'] }}">
                  </div>
                </div>

                <div class="form-group row">
                  <label for="email-input" class="col-sm-2 col-form-label">Email</label>
                  <div class="col-sm-10">
                    <input type="text" name="email" id="email-input" class="form-control" 
                    value="{{ $input['email'] }}">
                  </div>
                </div>

                <div class="form-group row">
                  <label for="phone-input" class="col-sm-2 col-form-label">Phone Number</label>
                  <div class="col-sm-10">
                    <input type="text" name="phone" id="phone-input" class="form-control" 
                      value="{{ $input['phone'] }}" placeholder="08xxxxxxxx">
                    <small class="form-text text-muted">
                      Phone number format 08xxxxxxxx
                    </small>
                  </div>
                </div>

                <div class="form-group row">
                  <label for="gender-input" class="col-sm-2 col-form-label">Gender</label>
                  <div class="col-sm-3">
                    <select name="gender" id="gender-input" class="form-control">
                      @foreach ($genderOptions as $gender)
                        <option value="{{ $gender }}" {{ $input['gender'] == $gender ? 'selected="selected"' : '' }}>
                          {{ $gender }}
                        </option>
                      @endforeach
                    </select>
                  </div>
                </div>

                <div class="form-group row">
                  <label for="birthday-input" class="col-sm-2 col-form-label">Birthday</label>
                  <div class="col-sm-3">
                    <input type="date" name="birthday" id="birthday-input" class="form-control" 
                      value="{{ $input['birthday'] }}">
                  </div>
                </div>

                <div class="form-group row">
                  <label for="hometown-input" class="col-sm-2 col-form-label">Hometown</label>
                  <div class="col-sm-10">
                    <input type="text" name="hometown" id="hometown-input" class="form-control" 
                      value="{{ $input['hometown'] }}">
                  </div>
                </div>

                <div class="form-group row">
                  <label for="address-input" class="col-sm-2 col-form-label">Current Address</label>
                  <div class="col-sm-10">
                    <textarea name="address" id="address-input" rows="5" class="form-control">{{ $input['address'] }}</textarea>
                  </div>
                </div>

                <div class="form-group row">
                  <div class="col-sm-10 offset-sm-2">
                    <button type="submit" class="btn btn-primary">Save</button>
                  </div>
                </div>
                
              </form>

            </div>

            <div class="tab-pane {{ $inputMode == 'cv' ? 'active' : ''}}" 
              id="cv" role="tab-panel" aria-labelledby="cv-tab">
              
              <form action="/test-qa/cv" method="POST" accept-charset="utf-8">

                {{ csrf_field() }}
                
                <h3>Education</h3>

                <div class="form-group row">
                  <label for="education-1-input" class="col-sm-2 col-form-label">Education (1)</label>
                  <div class="col-sm-10">
                    <input type="text" name="education_1" id="education-1-input" class="form-control" 
                      value="{{ $input['education_1'] }}">
                  </div>
                </div>

                <div class="form-group row">
                  <label for="education-2-input" class="col-sm-2 col-form-label">Education (2)</label>
                  <div class="col-sm-10">
                    <input type="text" name="education_2" id="education-2-input" class="form-control" 
                      value="{{ $input['education_2'] }}">
                  </div>
                </div>

                <div class="form-group row">
                  <label for="education-3-input" class="col-sm-2 col-form-label">Education (3)</label>
                  <div class="col-sm-10">
                    <input type="text" name="education_3" id="education-3-input" class="form-control" 
                      value="{{ $input['education_3'] }}">
                  </div>
                </div>

                <h3>Work History</h3>

                <div class="form-group row">
                  <label for="work-1-input" class="col-sm-2 col-form-label">Work (1)</label>
                  <div class="col-sm-10">
                    <input type="text" name="work_1" id="work-1-input" class="form-control" 
                      value="{{ $input['work_1'] }}">
                  </div>
                </div>

                <div class="form-group row">
                  <label for="work-2-input" class="col-sm-2 col-form-label">Work (2)</label>
                  <div class="col-sm-10">
                    <input type="text" name="work_2" id="work-2-input" class="form-control" 
                      value="{{ $input['work_2'] }}">
                  </div>
                </div>

                <div class="form-group row">
                  <label for="work-3-input" class="col-sm-2 col-form-label">Work (3)</label>
                  <div class="col-sm-10">
                    <input type="text" name="work_3" id="work-3-input" class="form-control" 
                      value="{{ $input['work_3'] }}">
                  </div>
                </div>

                <div class="form-group row">
                  <div class="col-sm-10 offset-sm-2">
                    <button type="submit" class="btn btn-primary">Save</button>
                  </div>
                </div>
              </form>

            </div>
          </div>

        </div>
      </div>
    </div>

    <div class="container">
      <div class="row">
        <div class="col-sm-12 footer">
          <p>
            All input data are saved within a cookie. 
            To emulate first time usage, please do one of following : 
            <ul>
              <li>Use Private Mode</li>
              <li>Remove Cookie Named : <code>input-qa</code></li>
              <li>Click <a href="/test-qa/remove-input">Here</a> to automatically remove <code>input-qa</code> cookie</li>
            </ul>
          </p>
        </div>
      </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>