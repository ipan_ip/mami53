@extends('web.agent.agentLayout')

@section('meta-custom')
	<title>Cek Properti Terdaftar - Mamikos</title>

	<meta name="description" content="Apakah properti yang akan kamu inputkan sudah terdaftar di Mamikos? Silakan cek di sini!">
	<meta property="og:title" content="Cek Properti Terdaftar - Mamikos" />
	<meta property="og:description" content="Apakah properti yang akan kamu inputkan sudah terdaftar di Mamikos? Silakan cek di sini!" />
	<meta property="og:image" content="/assets/mamikos_form.png" />
	<meta name="twitter:title" content="Cek Properti Terdaftar - Mamikos">
	<meta name="twitter:description" content="Apakah properti yang akan kamu inputkan sudah terdaftar di Mamikos? Silakan cek di sini!">
	<meta name="twitter:image" content="/assets/mamikos_form.png">
@endsection

@section('content')
  	<div id="app" class="wrapper">
  		<container-agent-check></container-agent-check>
  	</div>
@endsection

@section('script')
	@include('web.@script.global-auth')

	<script src="{{ mix_url('dist/js/agent-check.js') }}"></script>
@endsection
