<!DOCTYPE html>
<html>
<head>
  @if($_SERVER['HTTP_HOST'] != 'mamikos.com')
  <meta name="robots" content="noindex, nofollow">
  @endif
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <meta name="application-name" content="Mamikos">
  <meta name="theme-color" content="#1BAA56">
  <link rel="manifest" href="/json/manifest.json">

  @include('web.@meta.link-prebrowsing')

  @include('web.@meta.favicon')

  <base href="/">
  <meta name="author" content="https://plus.google.com/u/1/+Mamikos/posts">
  <meta name="og:site_name" content="Mamikos"/>
  <meta property="og:url" content="{{ $_SERVER['REQUEST_URI'] }}" />
  <meta property="og:type" content="website" />
  <meta property="fb:app_id" content="607562576051242"/>
  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:site" content="@mamikosapp">
  <meta name="twitter:creator" content="@mamikosapp">
  <meta name="twitter:domain" content="mamikos.com">
  <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Expires" content="0" />
  @yield('meta-custom')
  
  <!--  Fonts, Icons and CSS
  ================================================== -->
  @include('web.part.external.global-head')
  <link href="{{ mix_url('dist/css/common.css') }}" rel="stylesheet">

  @yield('style')
</head>
<body>
  @include('web.part.external.gtm-body')
  
  @yield('content')

  <!--  JavaScripts
  ================================================== -->  
  <script src="{{ mix_url('dist/js/common.js') }}"></script>
  @include ('web.part.script.bugsnagapikey')

  @yield('script')

  @include ('web.part.script.tokenerrorhandlescript')
  @include ('web.part.script.browseralertscript')
</body>
</html>