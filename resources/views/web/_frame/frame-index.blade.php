@extends('web.@layout.globalLayout')


@section('head')
	<meta name="robots" content="noindex, nofollow">

	@yield('head-meta')
@endsection


@section('data')

@endsection


@section('styles')

	@yield('sub-style')

@endsection


@section('content')

	@yield('sub-content')

@endsection


@section('scripts')

@endsection
