@extends('web._frame.frame-index')


@section('head-meta')
	<title>Room Virtual Tour by Matterport - Mamikos</title>
	<meta name="description" content="Room Virtual Tour by Matterport - Mamikos">
	<link rel="canonical" href="https://mamikos.com/vrtour/matterport/{{ $id }}">

	<meta property="og:url" content="https://mamikos.com/vrtour/matterport/{{ $id }}">
	<meta property="og:title" content="Room Virtual Tour by Matterport - Mamikos">
	<meta property="og:image" content="/general/img/pictures/share-image-default.jpg">
	<meta property="og:description" content="Room Virtual Tour by Matterport - Mamikos">

	<meta name="twitter:url" content="https://mamikos.com/vrtour/matterport/{{ $id }}">
	<meta name="twitter:title" content="Room Virtual Tour by Matterport - Mamikos">
	<meta name="twitter:description" content="Room Virtual Tour by Matterport - Mamikos">
	<meta name="twitter:image" content="/general/img/pictures/share-image-default.jpg">
@endsection


@section('sub-style')
	<style>
		body {
			background-color: #000;
		}

		.matterport-showcase {
			position: relative;
			padding-bottom: 56.25%;
			height: 100vh;
			overflow: hidden;
		}

		.matterport-showcase iframe {
			position: absolute;
			top: 0;
			left: 0;
			width: 100%;
			height: 90%;
		}
	</style>
@endsection


@section('sub-content')
	<main class="matterport-showcase">
		<iframe
			width="853"
			height="480"
			frameborder="0"
			allowfullscreen
			allow="vr"
			src="https://my.matterport.com/show/?m={{ $id }}&hl=1&play=1"
		></iframe>
	</main>
@endsection
