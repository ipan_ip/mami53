@extends('web.@layout.globalLayout')

@section('head')
	<meta name="robots" content="noindex, nofollow">

	<title>Success Email Verification - Mamikos</title>
	<meta name="description" content="Email verification success page">
	<link rel="canonical" href="https://mamikos.com/success/sukses-email-verifikasi">
@endsection

@section('data')

@endsection


@section('styles')
<style type="text/css">
.wrapper-email-success {
	max-width: 298px;
	margin: 0 auto;
	margin-top: 23vh;
	text-align: center;
}

.wrapper-email-success > p {
		margin-top: 19px;
		margin-bottom: 40px;
		color: #686868;
		font-size: 1.2em;
	}

.wrapper-email-success > a {
		margin-bottom: 44px;
	}
	</style>
@endsection


@section('content')
<div class="wrapper-email-success">
	<img src="/general/img/icons/ic_email-verified_s.svg">
	<p>Selamat, Email Kamu sudah terverifikasi</p>
	<a href="/user" class="btn btn-primary">Ke halaman profil</a>
</div>
@endsection


@section('scripts')

@endsection