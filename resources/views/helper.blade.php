<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Page Title</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<div class="form-control">
		<div class="card__head"></div>
		<div class="card__content">
			<div class="card__button"></div>
		</div>
		<div class="card__content"></div>
		<div class="card__footer"></div>
	</div>

	<div
		v-for=""
		v-if=""
		v-else=""
		v-show=""
		v-cloak=""
		id=""
		class=""
		ref=""
		:key=""
		slot=""
		:class=""
		:title=""
		:props=""
		v-model=""
		@click=""
		v-html=""
		v-text="">
	</div>

	<link rel="stylesheet" type="text/css" media="screen" href="main.css" />
	<script src="main.js"></script>
</head>
<body>

</body>
</html>

<style type="text/css">
	@media (max-width: 767px) {}
	@media (min-width: 768px) {}
	@media (max-width: 991px) and (min-width: 768px) {}
	@media (max-width: 991px) {}
	@media (min-width: 992px) {}
	@media (max-width: 1199px) and (min-width: 992px)  {}
	@media (max-width: 1199px) {}
	@media (min-width: 1200px) {}
</style>

<style type="text/css">
	/*https://dev.opera.com/articles/advanced-cross-browser-flexbox/#fallbacks*/
	.flexbox {
		display: -webkit-box;
		display: -moz-box;
		display: -ms-flexbox;
		display: -webkit-flex;
		display: flex;

		-webkit-align-items: center;
		-moz-align-items: center;
		-ms-align-items: center;
		align-items: center;

		-webkit-align-items: flex-end;
		-moz-align-items: flex-end;
		-ms-align-items: flex-end;
		align-items: flex-end;

		-webkit-align-items: flex-start;
		-moz-align-items: flex-start;
		-ms-align-items: flex-start;
		align-items: flex-start;

		-webkit-justify-content: center;
		-moz-justify-content: center;
		-ms-justify-content: center;
		justify-content: center;

		-webkit-justify-content: space-between;
		-moz-justify-content: space-between;
		-ms-justify-content: space-between;
		justify-content: space-between;

		-webkit-justify-content: flex-end;
		-moz-justify-content: flex-end;
		-ms-justify-content: flex-end;
		justify-content: flex-end;

		-webkit-justify-content: flex-start;
		-moz-justify-content: flex-start;
		-ms-justify-content: flex-start;
		justify-content: flex-start;

		-webkit-justify-content: space-around;
		-moz-justify-content: space-around;
		-ms-justify-content: space-around;
		justify-content: space-around;

		-webkit-flex-direction: column;
		-moz-flex-direction: column;
		-ms-flex-direction: column;
		flex-direction: column;

		-webkit-flex: 1;
		-moz-flex: 1;
		-ms-flex: 1;

		-webkit-flex-wrap: wrap;
		-moz-flex-wrap: wrap;
		-ms-flex-wrap: wrap;
		flex-wrap: wrap;

		-webkit-align-content: center;
		-moz-align-content: center;
		-ms-align-content: center;
		align-content: center;
	}
</style>

<i class="fa fa-spinner fa-pulse fa-3x fa-fw loading-spinner-default"></i>

<script> //80 lines separator
// -------------------------------------------------------------------------- //
</script>

<script type="text/javascript">
	// read: https://vuejs.org/v2/style-guide/#Component-instance-options-order-recommended
	new Vue({
		el: '',
		components: {}
		mixins: [],
		props: {},
		data: {},
		computed: {},
		watch: {},
		created() {},
		mounted () {},
		methods: {}
	})

	$.ajax({
		type: 'GET',
		url: '/garuda/',
		headers: {
			'Content-Type': 'application/json',
			'X-GIT-Time': '1406090202',
			'Authorization': 'GIT WEB:WEB'
		},
		crossDomain: true,
		dataType: 'json',
		success: function(response) {
			if (response.status) {
			} else {
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {}
	});

	$.ajax({
		type: 'POST',
		url: '/garuda/',
		headers: {
			'Content-Type': 'application/json',
			'X-GIT-Time': '1406090202',
			'Authorization': 'GIT WEB:WEB'
		},
		data: JSON.stringify({
			_token: csrfToken
		}),
		crossDomain: true,
		dataType: 'json',
		success: function(response) {
			if (response.status) {
			} else {
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {}
	});

	var validator = $( "#id-form" ).validate();
	var valid = validator.form();
	if (valid) {}

	 $( document ).ready(function() {});
	 $('#id').('');
	 $('.class').('');
</script>

<input type="email" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$">
<input type="tel" name="phone" pattern="^0(\d{3,4}-?){2}\d{3,4}$">

<!-- Vue Component -->
<template>
	<div>

	</div>
</template>

<script>
export default {

}
</script>

<style scoped>

</style>

<a href="javascript:void(0)" role="button"></a>

<card-item @list-changed="handleListChanged" />

<script>
	watch: {
		addressName(val) {
			this.watchAddressName(val);
		}
	},
	methods: {
		getSome() {},
		setSome() {},
		removeSome() {},
		addSome() {},
		deleteSome() {},
		fetchSome() {},
		submitSome() {},
		watchAddressName(val) {
			//handle watcher
		},
		combineSome() {},
		handleListChanged() {}
	}
</script>