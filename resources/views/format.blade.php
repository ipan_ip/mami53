<script>
fetchBlaBla() {
	//to request data from API (get, post, put, delete)
}

changeBlaBla() {
	//to change value in String and Number model
}

updateBlaBla() {
	//to update value in Object and Array model
}

emptyBlaBla() {
	//to empty value in any type of model
}

toggleBlaBla() {
	//to toggle value in Boolean model
}

storeBlaBla() {
	//to trigger commit action in Vuex state with latest value
}

restoreBlaBla() {
	//to trigger commit action in Vuex state with default value
}
</script>
