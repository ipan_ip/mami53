<!DOCTYPE html>
<html lang="en" >
<head>
  <title>CodePen - Detail Transaction v2</title>
    <style>
      * {
        font-size: 14px;
        font-family: 'Lato', sans-serif;
      }

      .big-wrapper {
        padding-top: 1rem;
        max-width: 500px;
        margin: 0 auto;
      }
      .big-wrapper h1.title-wrapper {
        text-align: center;
        color: #22c264;
        font-size: 1.2em;
      }

      .wrapper-transaction {
        margin-bottom: 2rem;
      }
      .wrapper-transaction h1.title-transaction {
        font-size: 1em;
        margin-bottom: 1.5em;
        color: #2e2d39;
      }

      .list-transaction {
        display: flex;
        justify-content: space-between;
        margin-bottom: 0.8rem;
      }
      .list-transaction span.type {
        color: #9e9e9e;
      }
      .list-transaction span.value {
        color: #3b445b;
      }

      .single-line {
        padding-bottom: 1rem;
        border-bottom: 1px solid #E5E5E5;
      }

      .dashed {
        border-bottom-style: dashed;
        border-bottom-color: black;
      }

      .bolder {
        font-weight: bold;
        color: #2e2d39 !important;
      }

      .new-background {
        padding: 1rem;
        border-radius: 5px;
        background-color: #F5F5F5;
      }

      table.tabel-transaksi {
        width: 100%;
      }

      table.tabel-transaksi tr td {
        width:50%;
        padding-bottom: 4px;
      }

      table.tabel-transaksi tr td:first-child {
        text-align:left;
      }

      table.tabel-transaksi tr td:nth-child(2) {
        text-align:right;
      }

      tr.last-table td {
        padding-bottom: 50px;
        border-bottom: 1px solid #E5E5E5;
      }

      tr.last-tabel-two td {
          padding-bottom: 50px;
        border-bottom: 1px dashed black;
      }
    </style>
</head>

<body>
  <div class="big-wrapper">
    <h1 class="title-wrapper">Detail Transaksi</h1>
    <div class="wrapper-transaction">
      <h1 class="title-transaction">Detail Pemesanan</h1>
      <table class="tabel-transaksi">
        <tr>
          <td>No. Invoice</td>
          <td>{{ $invoice->invoice_number }}</td>
        </tr>
        <tr>
          <td>Tanggal Transaksi</td>
          <td>{{ $invoice->created_at }}</td>
        </tr>
        <tr>
          <td>Jenis Pembayaran</td>
          <td>{{ $invoice->name }}</td>
        </tr>
        <tr class="last-table">
          <td>Tujuan Pembayaran</td>
          <td>{{ $invoice->created_at }}</td>
        </tr>
      </table>
    </div>
    <div class="wrapper-transaction new-background">
      <h1 class="title-transaction">Rincian Harga</h1>
      <table class="tabel-transaksi">
        <tr>
          <td>Harga Kost</td>
          <td>{{ $invoice->amount }}</td>
        </tr>
        @if ($invoice->additional_costs) 
          @foreach ($invoice->additional_costs as $item)
            <tr>
              <td>{{ $item->cost_title }}</td>
              <td>{{ $item->cost_value }}</td>
            </tr>
          @endforeach
        @endif
        <tr>
          <td>Total</td>
          <td>{{ $invoice->total }}</td>
        </tr>
      </table>
    </div>
  </div>
</body>
</html>
 

