@extends('layouts.app')

@section('content')
<div class="container" ng-app="myApp" ng-controller="userHome">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    You are logged in!
                    <br>
                    <br>
                    Welcome {{ $user->name }}.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')

<!-- <script>
   var app = angular.module('myApp', ['ngMap', 'ngResource', 'ngCookies', 'ui.bootstrap']);

    app.factory("Favorite", function($resource) {
        return $resource('/garuda/:api/:id/:data', {
            api: 'stories',
            id : '97816367',
            data: 'love'
        }, {
            list: {
                method: 'POST',
                headers: {
                    'Authorization': 'GIT WEB:WEB',
                    'X-GIT-Time': '1406090202',
                    'Content-Type': 'application/json'
                }
            }
        })
    });

    app.controller('userHome', ['$scope', '$rootScope', '$http', 'Favorite', '$window', function($scope, $rootScope, $http, Favorite, $window) {

        Favorite.list(function(result) {
            console.log(result);
        });

    }]);

</script> -->


@endsection
