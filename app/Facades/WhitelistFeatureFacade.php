<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class WhitelistFeatureFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'whitelistfeature';
    }
}
