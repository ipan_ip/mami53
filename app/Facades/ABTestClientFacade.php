<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class ABTestClientFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'abtestclient';
    }
}
