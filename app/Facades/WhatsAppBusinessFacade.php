<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class WhatsAppBusinessFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'whatsappbusiness';
    }
}
