<?php

namespace App\Facades;

use \Illuminate\Support\Facades\Storage;
use \App\Abstractor\ErrorNotifier;

use Illuminate\Filesystem\Filesystem;

class MamikosStorage extends Storage
{
    // override storage fake to enable custom config
    public static function fake($disk = null, $config = [])
    {
        $disk = $disk ?: self::$app['config']->get('filesystems.default');

        (new Filesystem)->cleanDirectory(
            $root = app()->storagePath().'/framework/testing/disks/'.$disk
        );

        $config['root'] = $root;

        static::set($disk, parent::createLocalDriver($config));
    }
}
