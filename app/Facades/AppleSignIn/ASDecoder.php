<?php

namespace App\Facades\AppleSignIn;

use Illuminate\Support\Facades\Facade;

/**
 * Facade class of ASDecoder wrapper.
 * For easy testing.
 * @codeCoverageIgnore
 */
class ASDecoder extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return \App\Libraries\AppleSignIn\ASDecoder::class;
    }
}
