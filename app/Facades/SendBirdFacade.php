<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class SendBirdFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'sendbird';
    }
}
