<?php

namespace App\Http\Helpers;

use App\Entities\Room\Room;

class PhotoCategoryHelper
{

    public const CATEGORY_BANGUNAN = 'bangunan';
    public const CATEGORY_KAMAR = 'kamar';
    public const CATEGORY_KAMAR_MANDI = 'kamar mandi';
    public const CATEGORY_KONDISI_SEKITAR_KOS = 'kondisi sekitar kos';
    public const CATEGORY_FASILITAS_BERSAMA = 'fasilitas bersama';

    public const SEARCH_KEY_CATEGORY_BANGUNAN = 'bangunan';
    public const SEARCH_KEY_CATEGORY_KAMAR = 'kamar';
    public const SEARCH_KEY_CATEGORY_KAMAR_MANDI = 'kamar mandi';
    public const SEARCH_KEY_CATEGORY_KONDISI_SEKITAR_KOS = 'kondisi sekitar kos';
    public const SEARCH_KEY_CATEGORY_FASILITAS_BERSAMA = 'fasilitas bersama|dapur';

    /** @var array list of photo available photo category */
    public const PHOTO_CATEGORY = [
        [
            'category' => self::CATEGORY_KAMAR_MANDI,
            'search_key' => self::SEARCH_KEY_CATEGORY_KAMAR_MANDI
        ],
        [
            'category' => self::CATEGORY_KAMAR,
            'search_key' => self::SEARCH_KEY_CATEGORY_KAMAR
        ],
        [
            'category' => self::CATEGORY_BANGUNAN,
            'search_key' => self::SEARCH_KEY_CATEGORY_BANGUNAN
        ],
        [
            'category' => self::CATEGORY_KONDISI_SEKITAR_KOS,
            'search_key' => self::SEARCH_KEY_CATEGORY_KONDISI_SEKITAR_KOS
        ],
        [
            'category' => self::CATEGORY_FASILITAS_BERSAMA,
            'search_key' => self::SEARCH_KEY_CATEGORY_FASILITAS_BERSAMA
        ]
    ];

    /** @var string wording category lain-lain  */
    public const OTHERS_CATEGORY = 'lainnya';

    /**
     * Get a category from string of desction photo
     * 
     * @param $description
     * @return string 
     */
    public static function getCategoryFromDescription($description)
    {
        $category = self::OTHERS_CATEGORY;
        if (
            is_null($description)
            || $description === ""
        ) {
            return $category;
        }

        // make it description clean with space and -
        $description = str_replace([" ", "-"],"",$description);

        // searching category
        foreach (self::PHOTO_CATEGORY as $categoryIterator) {

            $isFound = false;

            // divide search key string into array with delimiter "|"
            $arrSearchKey = explode('|', $categoryIterator['search_key']);

            foreach ($arrSearchKey as $key) {
                $cleanKey = str_replace(" ","",$key);
                $regex = "/(?i)" . $cleanKey . "/";

                // searching
                if(preg_match($regex, $description, $match)) {
                    $category = $categoryIterator['category'];
                    $isFound = true;
                    break;
                }
            }

            if ($isFound) break;
        }

        return $category;
    }

    /**
     * Get list card (photo kamar) with photo category
     *
     * @param Room|null $room
     * @return array
     */
    public static function getListCardWithCategory(?Room $room): array
    {
        if (is_null($room)) {
            return [];
        }

        $cards = $room->list_card;
        $newCards = [];

        $listCategories = [
            'building-card'   => [],
            'room-card'       => [],
            'bath-card'       => [],
            'share-card'      => [],
            'around-kos-card' => [],
            'other-card'      => []
        ];

        foreach ($cards as $card) {
            $description = array_key_exists( 'category_description', $card)
                ? $card['category_description'] : '';

            // append new attribute category on card
            $category = self::getCategoryFromDescription($description);
            $card['category'] = $category;
            $card['category_title'] = 'Foto ' . ucwords($category);

            $listCategories = self::pushIntoCategory($listCategories, $card);
        }

        foreach ($listCategories as $cards) {
            $newCards = array_merge($newCards, $cards);
        }

        return $newCards;
    }

    /**
     * Populate data list of photo (card) per category
     * 
     * @param []Card|null $listCard
     * @return array
     */
    public static function populatePhotoPerCategory($listCard): array
    {
        $populate = [];

        foreach ($listCard as $card) {

            // get category name
            $category = $card['category'];

            // new data on category
            if (! array_key_exists($category, $populate)) {
                $populate[$category] = [$card];
                continue;
            }

            // if this category already exist then merged it
            array_push($populate[$category], $card);
        }

        $result = [];

        // deconstruct array
        foreach ($populate as $key => $item) {
            $result[] = [
                'category' => $key,
                'category_title' => 'Foto ' . ucwords($key),
                'items' => $item
            ];
        }

        return $result;
    }

    public static function pushIntoCategory($categoriesCard, $card)
    {
        switch ($card['category']) {
            case self::CATEGORY_BANGUNAN :
                array_push($categoriesCard['building-card'], $card);
                break;
            case self::CATEGORY_KAMAR:
                array_push($categoriesCard['room-card'], $card);
                break;
            case self::CATEGORY_KAMAR_MANDI :
                array_push($categoriesCard['bath-card'], $card);
                break;
            case self::CATEGORY_FASILITAS_BERSAMA :
                array_push($categoriesCard['share-card'], $card);
                break;
            case self::CATEGORY_KONDISI_SEKITAR_KOS :
                array_push($categoriesCard['around-kos-card'], $card);
                break;
            case self::OTHERS_CATEGORY :
            default:
                array_push($categoriesCard['other-card'], $card);
                break;
        }
        return $categoriesCard;
    }

}