<?php

namespace App\Http\Helpers;

use App\Entities\Level\KostLevel;
use App\Entities\Room\Room;
use App\Repositories\Level\KostLevelRepository;
use App\Repositories\Level\KostLevelRepositoryEloquent;
use Bugsnag;
use Exception;
use App\Presenters\KostLevelValuePresenter;

class KostLevelValueHelper
{
    public const STRING_MAMIROOMS_1 = 'Mamirooms';
    public const STRING_MAMIROOMS_2 = 'Mamiroom';
    public const STRING_REGULAR = 'Regular';

    /*
     * Default "Kost Level Value" mapping for each predefined "Kost Level"
     * Referenced task & details: https://mamikos.atlassian.net/browse/BG-2999
     */
    protected $kostLevelValueMapping = [
        /* Mamirooms */
        0 => [1, 2, 3, 4, 5, 6, 7],
        /* Gold Plus 1 */
        1 => [1, 2, 3],
        /* Gold Plus 2 */
        2 => [1, 2, 3],
        /* Gold Plus 3 */
        3 => [1, 2, 3, 4, 5],
        /* Gold Plus 4 */
        4 => [1, 2, 3, 4, 5, 6]
    ];

    /**
     * @return \int[][]
     */
    public function getKostLevelValueMapping()
    {
        return $this->kostLevelValueMapping;
    }

    /**
     * @return bool
     */
    public function assignDefaultValueToKostLevels(): bool
    {
        $targetIDs = $this->getAllKostLevelIDs();
        if (empty($targetIDs)) {
            return false;
        }

        $processed = 0;
        foreach ($targetIDs as $index => $targetID) {
            $kostLevel = KostLevel::find($targetID);

            if (!$kostLevel) {
                continue;
            }

            /* Sync pivot table */
            $this->synchronizeLevelValues($kostLevel, $this->kostLevelValueMapping[$index]);

            $processed++;
        }

        // If not single Level get processed
        if ($processed < 1) {
            return false;
        }

        return true;
    }

    /**
     * @param KostLevel $level
     * @param array $values
     * @return bool
     */
    public function assignValuesToKostLevel(KostLevel $level, array $values = []): bool
    {
        if (empty($values)) {
            /* If it's about to remove "Values" */
            $this->removeLevelValues($level);
        }

        /* Sync pivot table */
        $this->synchronizeLevelValues($level, $values);

        return true;
    }

    /**
     * @return array
     */
    public function getAllKostLevelIDs()
    {
        $ids = $this->getGoldPlusIds();

        /*
         * To handle future kost level: "Mamirooms"
         */
        $mamiroomsID = KostLevel::where('name', 'Mamirooms')
            ->pluck('id')
            ->first();

        if ($mamiroomsID) {
            array_unshift($ids, $mamiroomsID);
        }

        return $ids;
    }

    /**
     * @param string $string
     * @return array
     */
    public function getMappingByLevelName(string $string = ''): array
    {
        if (empty($string)) {
            return [];
        }

        switch ($string) {
            /* GoldPlus 1 */
            case Room::VALUE_OF_GOLDPLUS_1:
            case Room::LEVEL_KOST_GOLDPLUS_1:
                return $this->kostLevelValueMapping[1];

            /* GoldPlus 2 */
            case Room::VALUE_OF_GOLDPLUS_2:
            case Room::LEVEL_KOST_GOLDPLUS_2:
                return $this->kostLevelValueMapping[2];

            /* GoldPlus 3 */
            case Room::VALUE_OF_GOLDPLUS_3:
            case Room::LEVEL_KOST_GOLDPLUS_3:
                return $this->kostLevelValueMapping[3];

            /* GoldPlus 4 */
            case Room::VALUE_OF_GOLDPLUS_4:
            case Room::LEVEL_KOST_GOLDPLUS_4:
                return $this->kostLevelValueMapping[4];

            /* Mamirooms */
            case self::STRING_MAMIROOMS_1:
            case self::STRING_MAMIROOMS_2:
                return $this->kostLevelValueMapping[0];

            default:
                return [];
        }
    }

    /**
     * @return array
     */
    public function getGoldPlusIds()
    {
        return [
            1 => config('kostlevel.id.goldplus1'),
            2 => config('kostlevel.id.goldplus2'),
            3 => config('kostlevel.id.goldplus3'),
            4 => config('kostlevel.id.goldplus4')
        ];
    }

    /**
     * @param KostLevel $level
     * @param array $targetIDs
     * @return bool
     */
    private function synchronizeLevelValues(KostLevel $level, array $targetIDs): bool
    {
        try {
            $level->values()->sync($targetIDs);

            if ($level->has_value != '1') {
                $level->has_value = '1';
                $level->save();
            }
        } catch (Exception $exception) {
            Bugsnag::notifyException($exception);
            return false;
        }

        return true;
    }

    /**
     * @param KostLevel $level
     * @return bool
     */
    private function removeLevelValues(KostLevel $level): bool
    {
        try {
            $level->values()->detach();
        } catch (Exception $exception) {
            Bugsnag::notifyException($exception);
            return false;
        }

        return true;
    }

    /**
     * Get kost value / benefit
     * 
     * @param Room $room
     * @return array
     */
    public static function getKostValue(Room $room)
    {
        $level = $room->level_info;
        if (is_null($level)) {
            return [];
        }

        $kostLevelQuery = KostLevel::with(['values.photo', 'values.photoSmall']);

        if ($room->is_mamirooms) {
            /*
             * Special checking for Kos "Mamirooms"
             * TODO: This patch will be removed once Artisan command for Kos Mamirooms assignment is done
             */
            $kostLevel = $kostLevelQuery->where('name', self::STRING_MAMIROOMS_1)->first();
        } else {
            $kostLevel = $kostLevelQuery->find($level['id']);
        }

        if (
            is_null($kostLevel)
            || empty($kostLevel->values())
        ) {
            return [];
        }

        // add with kost value alacarte
        $benefitsAlaCarte = (new KostLevelValuePresenter('list'))
            ->present($room->kost_values);

        $benefits = (new KostLevelValuePresenter('list'))
            ->present($kostLevel->values);

        // todo: merge with distinct
        $allBenefit = array_merge(
            $benefits['data'], 
            $benefitsAlaCarte['data']
        );

        return $allBenefit;
    }

    public static function getKostValueWithoutId($room)
    {
        $benefits = KostLevelValueHelper::getKostValue($room);
        $benefitCount = count($benefits);

        // for Kos Benefit API, Attribute ID is Hidden
        for ($i = 0; $i < $benefitCount; $i++) {
            unset($benefits[$i]['id']);
        }

        return [
            "_id" => $room->song_id,
            "benefits" => $benefits
        ];
    }

    public static function assignMamiroomsLevelToKos(Room $room)
    {
        $level = KostLevel::where('name', self::STRING_MAMIROOMS_1)->first();
        if (is_null($level)) {
            return false;
        }

        $repository = app(KostLevelRepository::class);
        $repository->changeLevel(
            $room,
            $level
        );

        return true;
    }

    public static function getKosBookingTransaction($room): array
    {
        $transaction = $room->getKosBookingTransaction();

        if (!$transaction) {
            return [];
        }

        return [
            'title' => $transaction . ' transaksi booking berhasil',
            'description' => 'Jumlah transaksi booking yang berhasil dilakukan di kos ini.',
            'icon_url' => config('url.app') . "/assets/icons/ic_booking_success.png",
            'icon_small_url' => config('url.app') . "/assets/icons/ic_booking_success_small.png",
        ];
    }
}