<?php

namespace App\Http\Helpers;

class RequestInputHelper
{
    /**
     * getNumericValue checks input if null or numeric.
     * Gives 0 if not set, '-', or not a numeric.
     * 
     * @param  string|null  $input
     * @param  string|array|null  $input
     * @return int
     */
    public static function getNumericValue($input) : int
    {
        if (!isset($input) || $input == '-' || !is_numeric($input)) {
            return 0;
        }
        return $input;
    }
}