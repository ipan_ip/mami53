<?php

namespace App\Http\Helpers;

use App\Entities\Room\Room;
use App\Entities\Level\KostLevel;

/**
 * Helper class to format the goldplus room.
 */
class GoldplusRoomHelper
{
    private const PATH_OWNER_STATISTIC = '/goldplus/statistic/';

    /**
     * Format the gold plus status information from room.
     * This method will search the value from available copy in `api.goldplus.filters` using key that were matched from
     * config key of `kostlevel.id`.
     *
     * @param Room $room
     * @return array
     */
    public static function formatGoldplusStatus(Room $room): array
    {
        $goldplusLevelId = $room->goldplus_level_id;
        if (is_null($goldplusLevelId)) {
            return [
                'key' => '',
                'value' => '',
            ];
        }
        $room_level = $room->level_info;
        return [
            'key' => (int) $goldplusLevelId,
            'value' => __('api.goldplus.filters')[$goldplusLevelId]['value'] ?? $room_level['name'] ?? '',
        ];
    }

    /**
     * Format the gold plus status information from room.
     * This method will search the value from available copy in `api.goldplus-acquisition.filters` using key that were matched from
     * config key of `kostlevel.id`.
     *
     * @param Room|null $room
     * @return array
     */
    public static function formatGoldplusAcquisitionStatus(?Room $room): array
    {
        if (is_null($room)) {
            throw new \RuntimeException(__('error.message'));
        }
        
        $roomLevel = $room->level_info;
        if (empty($roomLevel)) {
            return [
                'key' => '',
                'value' => '',
            ];
        }
        $goldplusLevelId = $roomLevel['id'];
        return [
            'key' => (int) $goldplusLevelId,
            'value' => KostLevel::getGoldplusNameByLevelId($goldplusLevelId) ?? $roomLevel['name'] ?? '',
        ];
    }

    /**
     * Generate the web url for statistic.
     *
     * @param int $roomSongId
     * @return string
     */
    public static function getGoldplusStatisticWebUrl(int $roomSongId): string
    {
        return url(config('owner.dashboard_url') . self::PATH_OWNER_STATISTIC . $roomSongId);
    }
}
