<?php
/**
 * @param $segment
 * @param $slug
 * @return bool|string
 */
function activeSlug($segment, $slug)
{
    $curentSlug = Request::segment($segment);

    if (is_array($slug)) {
        foreach ($slug as $v) {
            if ($curentSlug === trim($v)) {
                return 'active';
            }
        }
    } else {
        if ($curentSlug === trim($slug)) {
            return 'active';
        }
    }

    return false;
}

/**
 * @param $url
 * @param array $attributes
 * @param null $secure
 * @return string
 */
function style($url, $attributes = array(), $secure = null)
{
    $defaults = array('media' => 'all', 'type' => 'text/css', 'rel' => 'stylesheet');

    $attributes = $attributes + $defaults;

    $attributes['href'] = $this->url->asset($url, $secure);

    return '<link' . $this->attributes($attributes) . '>' . PHP_EOL;
}

function getDomainFromSlug($slug)
{
    $domain_pieces = explode(".", parse_url($slug, PHP_URL_HOST));

    $l = sizeof($domain_pieces);

    $secondleveldomain = $domain_pieces[$l-2] . "." . $domain_pieces[$l-1];

    return $secondleveldomain;
}

/**
 * @param $selectType
 * @param $formType
 * @param $dataType
 * @param $toPicks
 * @param $pickeds
 * @return array
 */
function mySelectPreprocessing($selectType, $formType, $dataType, $toPicks, $pickeds)
{
    $results = array();

    if ($selectType === 'multi') {
        if ($dataType === 'key-value') {
            foreach ($toPicks as $toPick) {
                if (empty($pickeds)) {
                    $results[] = (object)array(
                        'id' => $toPick->id,
                        'name' => $toPick->name,
                        $formType => ''
                    );
                } else {
                    $found = FALSE;

                    foreach ($pickeds as $picked) {
                        if ($picked == $toPick->id) {
                            $results[] = (object)array(
                                'id' => $toPick->id,
                                'name' => $toPick->name,
                                $formType => $formType
                            );
                            $found = TRUE;
                            break;
                        }
                    }

                    if (!$found) {
                        $results[] = (object)array(
                            'id' => $toPick->id,
                            'name' => $toPick->name,
                            $formType => ''
                        );
                    }
                }
            }
        } else if ($dataType === 'value') {
            foreach ($toPicks as $toPick) {
                if (empty($pickeds)) {
                    $results[] = (object)array(
                        'name' => $toPick,
                        $formType => ''
                    );
                } else {
                    $found = FALSE;

                    foreach ($pickeds as $picked) {
                        if ($picked == $toPick) {
                            $results[] = (object)array(
                                'name' => $toPick,
                                $formType => $formType
                            );
                            $found = TRUE;
                            break;
                        }
                    }

                    if (!$found) {
                        $results[] = (object)array(
                            'name' => $toPick,
                            $formType => ''
                        );
                    }
                }
            }
        }
    } else if ($selectType === 'single') {
        $picked = $pickeds;

        if ($dataType === 'key-value') {
            foreach ($toPicks as $toPick) {
                if ($picked == $toPick->id) {
                    $results[] = (object)array(
                        'id' => $toPick->id,
                        'name' => $toPick->name,
                        $formType => $formType
                    );
                } else {
                    $results[] = (object)array(
                        'id' => $toPick->id,
                        'name' => $toPick->name,
                        $formType => ''
                    );
                }
            }
        } else if ($dataType === 'value') {
            foreach ($toPicks as $toPick) {
                if ($picked == $toPick) {
                    $results[] = (object)array(
                        'name' => $toPick,
                        $formType => $formType
                    );
                } else {
                    $results[] = (object)array(
                        'name' => $toPick,
                        $formType => ''
                    );
                }
            }
        }
    }

    return $results;
}