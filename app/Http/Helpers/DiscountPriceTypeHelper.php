<?php

namespace App\Http\Helpers;

use App\Entities\Room\Element\Price;

class DiscountPriceTypeHelper
{
    # beware that annually -> price_yearly
    public const ROOM_PRICE_TYPE_MAPPING = [
        Price::STRING_DAILY => Price::PRICE_DAILY,
        Price::STRING_WEEKLY => Price::PRICE_WEEKLY,
        Price::STRING_MONTHLY => Price::PRICE_MONTHLY,
        Price::STRING_QUARTERLY => Price::PRICE_QUARTERLY,
        Price::STRING_SEMIANNUALLY => Price::PRICE_SEMIANNUALLY,
        Price::STRING_ANNUALLY => Price::PRICE_YEARLY,
        Price::STRING_DAILY_USD => Price::PRICE_DAILY_USD,
        Price::STRING_WEEKLY_USD => Price::PRICE_WEEKLY_USD,
        Price::STRING_MONTHLY_USD => Price::PRICE_MONTHLY_USD,
        Price::STRING_ANNUALLY_USD => Price::PRICE_YEARLY_USD
    ];

    /**
     * getRoomPriceType is to get mapping attribute for price type between discount and room
     * 
     */
    public static function getRoomPriceType(string $discountPriceType) : ?string
    {
        return isset(self::ROOM_PRICE_TYPE_MAPPING[$discountPriceType]) ? self::ROOM_PRICE_TYPE_MAPPING[$discountPriceType] : null;
    }
}