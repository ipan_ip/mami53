<?php namespace App\Http\Helpers;

use App\Entities\Log\Log;
use App\Entities\Log\LogTemp;
use Session;
use Config;
use Illuminate\Http\JsonResponse;
use stdClass;

class ApiResponse
{
    /*
    |--------------------------------------------------------------------------
    | Api Helpers
    |--------------------------------------------------------------------------
    |
    | Response
    | Response Data
    | Response Error
    | Exception Handler
    | Auto Logging to Database
    */

    /**
     * List of Default Message Response
     *
     * @var array
     * @access protected
     */
    static protected $messageCode = array(
        '0' => array(
            'severity' => 'Success',
            'message'  => 'Request Success',
        ),
        '1' => array(
            'severity' => 'Error',
            'message'  => 'Error Happen',
        ),
        '200' => array(
            'severity' => 'OK',
            'message'  => 'The request has succeeded. An entity corresponding to the requested resource is sent in the response.',
        ),
        '201' => array(
            'severity' => 'CREATED',
            'message'  => 'The request has been fulfilled and resulted in a new resource being created.',
        ),
        '204' => array(
            'severity' => 'NO CONTENT',
            'message'  => 'The server successfully processed the request, but is not returning any content.',
        ),
        '400' => array(
            'severity' => 'INVALID REQUEST',
            'message'  => 'The request could not be understood by the server due to malformed syntax or request.',
        ),
        '401' => array(
            'severity' => 'UNAUTHORIZE',
            'message'  => 'Not authorized.',
        ),
        '403' => array(
            'severity' => 'FORBIDDEN',
            'message'  => 'Disallowed to run this resource.',
        ),
        '404' => array(
            'severity' => 'NOT FOUND',
            'message'  => 'The server has not found anything matching the request.',
        ),
        '500' => array(
            'severity' => 'INTERNAL SERVER ERROR',
            'message'  => 'The server encountered an unexpected condition which prevented it from fulfilling the request.',
        ),
    );

    /**
     * List of available error levels
     *
     * @var array
     * @access protected
     */
    static public $errorLevels = array(
        E_ERROR             => 'Error', // 1
        E_WARNING           => 'Warning', // 2
        E_PARSE             => 'Parsing Error', // 4
        E_NOTICE            => 'Notice', // 8
        E_CORE_ERROR        => 'Core Error', // 16
        E_CORE_WARNING      => 'Core Warning', // 32
        E_COMPILE_ERROR     => 'Compile Error', // 64
        E_COMPILE_WARNING   => 'Compile Warning', // 128
        E_USER_ERROR        => 'User Error', // 256
        E_USER_WARNING      => 'User Warning', // 512
        E_USER_NOTICE       => 'User Notice',
        E_STRICT            => 'Runtime Notice',
        E_RECOVERABLE_ERROR => 'E_RECOVERABLE_ERROR',
        E_DEPRECATED        => 'E_DEPRECATED',
        E_USER_DEPRECATED   => 'E_USER_DEPRECATED'
    );


    public static function traceThrowError(Exception $e)
    {
        return '. Error: ' . $e->getMessage() . ', File: ' . $e->getFile() . ', Line: ' . $e->getLine();
    }

    /**
     * Function Variety Use Case
     *
     * responseError('message');
     * responseError('message', 'severity');
     * responseError('message', 'severity', 200, 1);
     * responseError(false, false, false, 1);
     * responseError(false, false, false, 1, 'file', 'line');
     * responseError(false, false, false, 1, 'file', 'line', true);
     * responseError(false, false, false, 1, false, false, true);
     */

    static public function responseError($message = false, $severity = false, $http_code = false,
            $error_code = false, $file = false, $line = false, $debug = false)
    {
        if (($error_code !== false) && ($severity === false)) {
            $severity = self::$messageCode[$error_code]['severity'];
            $message  = self::$messageCode[$error_code]['message'];
        } else {
            $error_code = $error_code ? $error_code : 1;
            $severity   = $severity ? $severity : self::$messageCode[$error_code]['severity'];
            $message    = $message ? $message : self::$messageCode[$error_code]['message'];
        }

        $response = array(
            'status' => false,
            'meta'   => array(
                'response_code' => $http_code,
                'code'          => $error_code,
                'severity'      => $severity,
                'message'       => $message
            )
        );

        if ($file !== false) {
            $response['meta']['file'] = $file;
            $response['meta']['line'] = $line;
        }

        $responseError = true;
        return self::response($response, $http_code, $debug, $responseError);
    }

    public static function badRequest($message, $http_code = 400) {
        $response = array(
            'status' => false,
            'meta'   => array(
                'message'       => $message
            )
        );
        return self::response($response, $http_code)
            ->header('Content-Type', 'application/json');
    }

    /**
     * Function Variety Use Case
     *
     * responseData(array());
     * responseData(array(), true, 200);
     * responseData(array(), true, 200, 'message');
     * responseData(array(), true, 200, 'message', 'severity');
     * responseData(array(), true, 200, null, null, 1);
     * responseData(array(), true, 200, null, 'severity', 1, true);
     * responseData(array(), true, 200, null, null, null, true);
     *
     */

    static public function responseData($data = array(), $counData = false , $status = true, $http_code = 200,
            $message = null, $severity = null, $status_code = null, $debug = false)
    {
        if (($status_code !== null) && ($severity === null)) {
            $severity = self::$messageCode[$status_code]['severity'];
            $message  = self::$messageCode[$status_code]['message'];
        } else {
            $status_code = $status_code ? $status_code : 200;
            $severity    = $severity ? $severity : self::$messageCode[$status_code]['severity'];
            $message     = $message ? $message : self::$messageCode[$status_code]['message'];
        }

        $response = array(
            'status' => $status,
            'meta'   => array(
                'response_code' => $http_code,
                'code'          => $status_code,
                'severity'      => $severity,
                'message'       => $message
            )
        );

        foreach ($data as $key => $value) {
            if ($key == 'meta') {
                $response[$key] = array_merge($response[$key], $value);
            } else {
                $response[$key] = $value;
            }
        }

        return self::response($response, $http_code, $debug);
    }

    /**
     *  Convert a response object into equivalent JsonResponse
     *
     *  This class aids mainly when calling mamipay API using MamipayApiHelper
     *
     *  @param stdClasss $data Response to convert
     *
     *  @return JsonResponse
     */
    public static function responseObj(stdClass $data = null): JsonResponse
    {
        $statusCode = 200;
        $severity    = self::$messageCode[$statusCode]['severity'];
        $message     = self::$messageCode[$statusCode]['message'];

        $response = [
            'status' => true,
            'meta'   => [
                'response_code' => 200,
                'code'          => $statusCode,
                'severity'      => $severity,
                'message'       => $message
            ]
        ];

        foreach ($data as $key => $value) {
            switch ($key) {
                case 'meta':
                    $response[$key] = array_merge($response[$key], (array) $value);
                    break;
                default:
                    $response[$key] = $value;
                    break;
            }
        }

        return self::response($response, 200, false);
    }

    public static function validationErrorsToString($errArray) {
        $valArr = array();
        foreach ($errArray->toArray() as $key => $value) { 
            $errStr = $value[0];
            array_push($valArr, $errStr);
        }
        if(!empty($valArr)){
            $errStrFinal = implode(', ', $valArr);
        }
        return $errStrFinal;
    }

    static public function response($data = array(), $http_code = null, $debug = null, $responseError = false)
    {
        $newMeta = array();

        if ((Session::get('api.log.enable') && Config::get('api.log.enable')) ||
                (((\Request::input('log') === 'enable') &&
                (\Request::input('devel_access_token') === Config::get('api.devel_access_token'))))) {

            $newMeta = array(
                'response_time' => round((microtime(true) - Session::get('api.log.request_time')) * 1000) . ' ms',
            );
        }

        // Merging the Previous Meta
        // To Put Response Time to The Top, that's why using Array Merge
        if (array_key_exists('meta', $data)) {
            $meta['meta'] = array_merge($newMeta, $data['meta']);
        } else {
            $meta['meta'] = $newMeta;
        }

        // Debugbar
        // Since Optional, put it at the bottom
        if (($debug) ||
                (Config::get('api.debug_level') == 3) ||
                ((\Request::input('debugbar') === 'enable') &&
                (\Request::input('devel_access_token') === Config::get('api.devel_access_token')))) {

            Debugbar::enable();
            $meta['meta']['debugbar'] = Debugbar::collect();
        }

        $response = array(
            'status'   => $data['status'],
            'meta'     => $meta['meta']
        );

        // Merge Data except Meta and Status
        foreach ($data as $key => $value) {
            if (! in_array($key, array('status', 'meta'))) {
                $response[$key] = $value;
            }
        }

        // KOS-12733
        if (env('APP_ENV') != 'production')
        {
            LogTemp::logApiRequest($response);
        }

        if ($responseError && env('API_ERROR_RESPONSE_LIE', false)) {
            $data = self::lieResponse($data['meta']['code']);
        }

        return \Response::json($data, $http_code);
    }

    public static function activeDevice()
    {
        return \Session::get('api.device.data');
    }

    public static function activeUser()
    {
        return \Session::get('api.user.data');
    }

    private static function lieResponse($code = 256)
    {
        return array(
            'status'    => false,
            'meta' => array(
                'response_code' => 500,
                'code'  => $code,
                'severity' => 'Error',
                'message' => 'Error cant be identified',
                'file' => 'public/main.php',
                'line'  => 17
            )
        );
    }

    static public function responseUserFailed($data = array(), $counData = false , $status = true, $http_code = 200,
            $message = null, $severity = null, $status_code = null, $debug = false)
    {
        if (($status_code !== null) && ($severity === null)) {
            $severity = self::$messageCode[$status_code]['severity'];
            $message  = self::$messageCode[$status_code]['message'];
        } else {
            $status_code = $status_code ? $status_code : 200;
            $severity    = $severity ? $severity : self::$messageCode[$status_code]['severity'];
            $message     = $message ? $message : self::$messageCode[$status_code]['message'];
        }

        $response = array(
            'status' => false,
            'meta'   => array(
                'response_code' => $http_code,
                'code'          => $status_code,
                'severity'      => $severity,
                'message'       => "Anda bukan user"
            )
        );

        foreach ($data as $key => $value) {
            if ($key == 'meta') {
                $response[$key] = array_merge($response[$key], $value);
            } else {
                $response[$key] = $value;
            }
        }

        return self::response($response, $http_code, $debug);
    }


}
