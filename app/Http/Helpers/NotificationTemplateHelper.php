<?php

namespace App\Http\Helpers;

use App\Entities\Notif\NotificationWhatsappTemplate;
use App\Entities\Room\Room;
use App\User;
use Carbon\Carbon;

/**
 * Class UserEventsHelper
 * @package App\Http\Helpers
 */
class NotificationTemplateHelper
{
    const EVENT_ACCOUNT_REGISTERED = 'account_registered';
    const EVENT_NEW_BOOKING = 'get_new_booking';
    const EVENT_REMINDER_NEW_BOOKING_5 = 'reminder_new_booking_5';
    const EVENT_REMINDER_NEW_BOOKING_23 = 'reminder_new_booking_23';
    const EVENT_REMINDER_NEW_BOOKING_95 = 'reminder_new_booking_95';
    const EVENT_ACCEPTED_BOOKING = 'get_booking_accepted';
    const EVENT_BOOKING_PAID_DP = 'transaction_down_payment';
    const EVENT_BOOKING_PAID_ST = 'transaction_settlement_payment';
    const EVENT_BOOKING_PAID_FULL = 'transaction_full_payment';
    const EVENT_BOOKING_PAID_RECURRING = 'transaction_invoice_payment';
    const EVENT_PAYOUT_PAID_DP = 'payout_down_payment';
    const EVENT_PAYOUT_PAID_ST = 'payout_settlement_payment';
    const EVENT_PAYOUT_PAID_FULL = 'payout_full_payment';
    const EVENT_PAYOUT_PAID_RECURRING = 'payout_invoice_payment';
    const EVENT_REMINDER_UNREAD_CHAT = 'reminder_unread_chat';
    const EVENT_BOOKING_REMINDER_1H = 'reminder_booking_1h';
    const EVENT_BOOKING_REMINDER_3H = 'reminder_booking_3h';
    const EVENT_BOOKING_REMINDER_6H = 'reminder_booking_6h';
    const EVENT_BBK_ACTIVATED = 'account_activated';
    const EVENT_BOOKING_STATUS_PAID = 'transaction_payment';
    const EVENT_THANOS_SNAP = 'thanos_notification';
    const EVENT_GP4_INVOICE_NOTIFICATION = 'gp4_invoice_notification';
    const EVENT_RECURRING_NOTIFICATION_THIS_DAY = 'recurring_unpaid_invoice_this_day';
    const EVENT_RECURRING_NOTIFICATION_MINUS_ONE = 'recurring_unpaid_invoice_minus_one';
    const EVENT_RECURRING_NOTIFICATION_PLUS_ONE = 'recurring_unpaid_invoice_plus_one';
    const EVENT_REJECTED_CONTRACT_SUBMISSION = 'rejected_dbet_tenant_request';
    const EVENT_CONFIRMED_CONTRACT_SUBMISSION = 'accepted_dbet_tenant_request';
    const EVENT_CONFIRMED_WITHOUT_REVISION_CONTRACT_SUBMISSION = 'accepted_failed_chage_dbet_tenant_request';
    const EVENT_UNOPENED_CONTRACT_SUBMISSION = 'unopened_dbet_tenant_request';
    const EVENT_VERIFICATION_CODE_REQUESTED = 'verification_code_requested';

    const NAME_REQUEST_VERIFICATION_CODE = 'otp_via_wa';

    const REMANING_HOURS = [
        self::EVENT_BOOKING_REMINDER_1H => 1, //hour
        self::EVENT_BOOKING_REMINDER_3H => 3, //hour
        self::EVENT_BOOKING_REMINDER_6H => 6, //hour
    ];

    public static function generateTestingData(array $variables)
    {
        $generatedVariables = [];

        foreach ($variables as $var)
        {
            switch ($var)
            {
                case 'owner_name':
                    $generatedVariables[] = 'John Doe';
                    break;
                case 'kost_name':
                    $generatedVariables[] = 'Kost For Testing 123';
                    break;
                case 'kost_address':
                    $generatedVariables[] = 'Jalan Kahuripan No. 123 Jakarta';
                    break;
                case 'duration':
                    $generatedVariables[] = '1 Bulan';
                    break;
                case 'check-in date':
                case 'check_in_date':
                    $generatedVariables[] = Carbon::today()->addDay(14)->format('D, d/m/Y');
                    break;
                case 'room_number':
                    $generatedVariables[] = '123';
                    break;
                case 'total_invoice':
                    $generatedVariables[] = 'Rp. 1.250.000';
                    break;
                case 'expired_invoice_time':
                    $generatedVariables[] = Carbon::today()->addDay(3)->format('D, d/m/Y');;
                    break;
                case 'tenant_name':
                    $generatedVariables[] = 'Jane Doe';
                    break;
                case 'link':
                    $generatedVariables[] = 'https://mamikos.com/s/abcdef';
                    break;
                case 'ad_link':
                    $generatedVariables[] = 'https://mamikos.com/room/kost-kost-campur-eksklusif-kost-mamirooms-salam-tipe-b-ngaglik-sleman-1';
                    break;
                case 'url_helpcenter':
                    $generatedVariables[] = 'https://help.mamikos.com';
                    break;
                case 'paid_amount':
                    $generatedVariables[] = 'Rp. 500.000';
                    break;
                case 'month_name':
                    $generatedVariables[] = 'Pembayaran untuk bulan Testing';
                    break;
                case 'owner_bank_account_number':
                    $generatedVariables[] = 'BCA Norek. 123-456-789';
                    break;
                default:
                    $generatedVariables[] = '[undefined]';
                    break;
            }
        }

        return $generatedVariables;
    }

    public static function generateData(array $variables, array $templateData, string $event)
    {
        setlocale(LC_TIME, 'IND');

        $generatedVariables = [];

        $tenantName         = '';
        $kostName           = '';
        $kostAddress		= '';
        $ownerName          = '';
        $duration           = '';
        $checkInDate        = '';
        $roomNumber         = '';
        $expiredInvoiceTime = '';
        $totalInvoice       = '';
        $urlInvoice         = '';
        $link               = '';
        $adLink             = '';
        $paidAmount         = '';
        $monthName          = '';
        $ownerBankAccount   = '';
        $urlHelpCenter		= '';
        $remainingHours		= '';
        $location           = '';
        $voucherName        = '';
        $voucherDeduction   = '';
        $helpLink           = '';
        $contractLink       = '';
        $amountOfRequest    = 0;
        $confirmLink        = '';
        $code               = '';

        switch ($event)
        {
            case (self::EVENT_ACCOUNT_REGISTERED) :
                $ownerName = $templateData['owner_name'];
                $kostName  = $templateData['room_name'];
                $link      = 'https://mamikos.com/s/owlist';
                break;

            case (self::EVENT_NEW_BOOKING) :
            case (self::EVENT_REMINDER_NEW_BOOKING_5) :
            case (self::EVENT_REMINDER_NEW_BOOKING_23) :
            case (self::EVENT_REMINDER_NEW_BOOKING_95) :
                $ownerName   = $templateData['owner_name'];
                $kostName    = $templateData['room_name'];
                $tenantName  = $templateData['tenant_name'];
                $duration    = $templateData['duration'];
                $checkInDate = $templateData['check_in_date'];
                $link        = !empty($templateData['link']) ? $templateData['link'] :'https://mamikos.com/s/owbook';
                break;

            case (self::EVENT_ACCEPTED_BOOKING) :
                $rentType           = RentCountHelper::getFormatRentCount($templateData['duration'], $templateData['rent_type']);
                $tenantName         = NotificationTemplateHelper::getUserName($templateData['tenant_user_id']);
                $kostName           = NotificationTemplateHelper::getKostName($templateData['designer_id']);
                $duration           = $rentType;
                $checkInDate        = Carbon::createFromFormat('Y-m-d', $templateData['check_in_date'])->formatLocalized('%e %B %Y');
                $roomNumber         = $templateData['room_number'];
                $expiredInvoiceTime = Carbon::createFromFormat('Y-m-d H:i:s', $templateData['expired_invoice_time'])->formatLocalized('%e %B %Y');
                $totalInvoice       = NotificationTemplateHelper::getFormattedCurrency($templateData['total_invoice']);
                $link               = 'https://mamikos.com/s/tebook'; // Ref wiki: https://mamikos.atlassian.net/wiki/x/F4ALAg
                $urlInvoice         = !empty($templateData['url_invoice']) ? $templateData['url_invoice'] : config('app.url').'/user/booking';
                break;

            case (self::EVENT_REMINDER_UNREAD_CHAT) :
                $link = $templateData['link'];
                break;

            case (self::EVENT_BBK_ACTIVATED) :
                $ownerName = ucfirst($templateData['owner_name']);
                $kostName  = ucfirst($templateData['room_name']);
                $link      = 'https://mamikos.com/s/uphaka';
                $adLink    = $templateData['ad_link'];
                break;

            case (self::EVENT_BOOKING_PAID_DP) :
            case (self::EVENT_BOOKING_PAID_ST) :
            case (self::EVENT_BOOKING_PAID_FULL) :
            case (self::EVENT_BOOKING_PAID_RECURRING) :
                $ownerName  = NotificationTemplateHelper::getUserName($templateData['owner_id']);
                $tenantName = NotificationTemplateHelper::getUserName($templateData['tenant_user_id']);
                $kostName   = NotificationTemplateHelper::getKostName($templateData['designer_id']);
                $link       = 'https://mamikos.com/detailpenghuni/' . $templateData['tenant_contract_id'];
                break;

            case (self::EVENT_PAYOUT_PAID_DP) :
            case (self::EVENT_PAYOUT_PAID_ST) :
            case (self::EVENT_PAYOUT_PAID_FULL) :
                $ownerName        = NotificationTemplateHelper::getUserName($templateData['owner_id']);
                $kostName         = NotificationTemplateHelper::getKostName($templateData['designer_id']);
                $tenantName       = NotificationTemplateHelper::getUserName($templateData['tenant_user_id']);
                $paidAmount       = NotificationTemplateHelper::getFormattedCurrency($templateData['paid_amount']);
                $ownerBankAccount = NotificationTemplateHelper::getCompiledBankAccount($templateData);
                break;

            case (self::EVENT_PAYOUT_PAID_RECURRING) :
                $ownerName        = NotificationTemplateHelper::getUserName($templateData['owner_id']);
                $kostName         = NotificationTemplateHelper::getKostName($templateData['designer_id']);
                $tenantName       = NotificationTemplateHelper::getUserName($templateData['tenant_user_id']);
                $paidAmount       = NotificationTemplateHelper::getFormattedCurrency($templateData['paid_amount']);
                $monthName        = NotificationTemplateHelper::getMonthName($templateData['month']);
                $ownerBankAccount = NotificationTemplateHelper::getCompiledBankAccount($templateData);
                break;

            case (self::EVENT_BOOKING_REMINDER_1H) :
            case (self::EVENT_BOOKING_REMINDER_3H) :
            case (self::EVENT_BOOKING_REMINDER_6H) :
                $tenantName 			= ucfirst($templateData['tenant_name']);
                $expiredInvoiceTime 	= Carbon::createFromFormat('Y-m-d H:i:s', $templateData['expired_invoice_time'])->format('d F Y H:i:s');
                $kostName				= NotificationTemplateHelper::getKostName($templateData['designer_id']);
                $checkInDate 			= Carbon::createFromFormat('Y-m-d', $templateData['check_in_date'])->format('d F Y');
                $roomNumber 			= $templateData['room_number'];
                $duration 				= RentCountHelper::getFormatRentCount($templateData['stay_duration'], $templateData['rent_type']);
                $totalInvoice			= NotificationTemplateHelper::getFormattedCurrency($templateData['total_invoice']);
                $link 					= 'https://mamikos.com/s/tebook';
                $remainingHours         = self::REMANING_HOURS[$event];
                $urlInvoice             = $templateData['url_invoice'];
                $location               = $templateData['location'];
                break;

            case (self::EVENT_RECURRING_NOTIFICATION_THIS_DAY) :
            case (self::EVENT_RECURRING_NOTIFICATION_MINUS_ONE) :
            case (self::EVENT_RECURRING_NOTIFICATION_PLUS_ONE) :
                $tenantName 	    = ucfirst($templateData['tenant_user_name']);
                $voucherName        = isset($templateData['voucher_name']) ? $templateData['voucher_name'] : '';
                $voucherDeduction   = isset($templateData['voucher_deduction']) ? $templateData['voucher_deduction'] : '';
                $urlInvoice         = isset($templateData['url_invoice']) ? $templateData['url_invoice'] : '';
                break;

            case (self::EVENT_BOOKING_STATUS_PAID) :
                $tenantName		= ucfirst($templateData['tenant_name']);
                $kostName		= NotificationTemplateHelper::getKostName($templateData['designer_id']);
                $kostAddress	= NotificationTemplateHelper::getKostAddress($templateData['designer_id']);
                $checkInDate 	= Carbon::createFromFormat('Y-m-d', $templateData['check_in_date'])->format('d F Y');
                $roomNumber 	= $templateData['room_number'];
                $link 			= 'https://mamikos.com/s/tebook';
                $urlHelpCenter 	= 'https://help.mamikos.com/penghuni';
                break;

            case (self::EVENT_THANOS_SNAP):
                $ownerName      = $templateData['owner_name'];
                $kostName       = $templateData['kos_name'];
                $link           = $templateData['link'];
                break;

            case (self::EVENT_GP4_INVOICE_NOTIFICATION):
                $link 			= $templateData['link'];
                break;

            case (self::EVENT_REJECTED_CONTRACT_SUBMISSION):
                $helpLink = $templateData['help_link'] ?? '';
                break;

            case (self::EVENT_CONFIRMED_CONTRACT_SUBMISSION):
            case (self::EVENT_CONFIRMED_WITHOUT_REVISION_CONTRACT_SUBMISSION) :
                $contractLink = $templateData['contract_link'] ?? '';
                break;

            case (self::EVENT_UNOPENED_CONTRACT_SUBMISSION):
                $kostName = $templateData['kos_name'] ?? '';
                $amountOfRequest = $templateData['amount_of_request'] ?? 0;
                $confirmLink = $templateData['confirm_link'] ?? '';
                break;

            case (self::EVENT_VERIFICATION_CODE_REQUESTED):
                $code = $templateData['code'] ?? '';
                break;

            default:
                break;
        }

        foreach ($variables as $var)
        {
            switch ($var)
            {
                case 'owner_name':
                    $generatedVariables[] = ucwords($ownerName);
                    break;
                case 'kost_name':
                case 'kos_name':
                    $generatedVariables[] = ucwords($kostName);
                    break;
                case 'kost_address':
                    $generatedVariables[] = ucwords($kostAddress);
                    break;
                case 'duration':
                    $generatedVariables[] = $duration;
                    break;
                case 'check-in date':
                case 'check_in_date':
                    $generatedVariables[] = $checkInDate;
                    break;
                case 'room_number':
                    $generatedVariables[] = $roomNumber;
                    break;
                case 'total_invoice':
                case 'total_amount_charged':
                    $generatedVariables[] = $totalInvoice;
                    break;
                case 'expired_invoice_time':
                case 'time_and_date_expired':
                    $generatedVariables[] = $expiredInvoiceTime;
                    break;
                case 'tenant_name':
                    $generatedVariables[] = ucwords($tenantName);
                    break;
                case 'link':
                case 'link_apps':
                    $generatedVariables[] = $link;
                    break;
                case 'ad_link':
                    $generatedVariables[] = $adLink;
                    break;
                case 'url_helpcenter':
                    $generatedVariables[] = $urlHelpCenter;
                    break;
                case 'paid_amount':
                    $generatedVariables[] = $paidAmount;
                    break;
                case 'month_name':
                    $generatedVariables[] = $monthName;
                    break;
                case 'owner_bank_account_number':
                    $generatedVariables[] = $ownerBankAccount;
                    break;
                case 'remaining_hours':
                    $generatedVariables[] = $remainingHours;
                    break;
                case 'url_invoice':
                    $generatedVariables[] = $urlInvoice;
                    break;
                case 'kost_location':
                    $generatedVariables[] = $location;
                    break;
                case 'voucher_name':
                    $generatedVariables[] = $voucherName;
                    break;
                case 'voucher_deduction':
                    $generatedVariables[] = $voucherDeduction;
                    break;
                case 'help_link':
                    $generatedVariables[] = $helpLink;
                    break;
                case 'contract_link':
                    $generatedVariables[] = $contractLink;
                    break;
                case 'amount_of_request':
                    $generatedVariables[] = $amountOfRequest;
                    break;
                case 'confirm_link':
                    $generatedVariables[] = $confirmLink;
                    break;
                case 'code':
                    $generatedVariables[] = $code;
                    break;
                default:
                    $generatedVariables[] = '[undefined]';
                    break;
            }
        }

        return $generatedVariables;
    }

    private static function getUserName(int $userId)
    {
        $userName = User::where('id', $userId)->pluck('name')->first();

        if (is_null($userName))
            return null;

        return ucfirst($userName);
    }

    private static function getKostName(int $roomId)
    {
        $kostName = Room::where('id', $roomId)->pluck('name')->first();

        if (is_null($kostName))
            return null;

        return ucfirst(trim($kostName));
    }

    private static function getKostAddress(int $roomId)
    {
        $kostAddress = Room::where('id', $roomId)->pluck('address')->first();

        if (is_null($kostAddress) || empty($kostAddress))
            return null;

        return ucfirst($kostAddress);
    }

    private static function getFormattedCurrency(int $numbers)
    {
        return 'Rp ' . number_format($numbers, 0, ',', '.');
    }

    private static function getMonthName(int $month)
    {
        Carbon::setLocale('id');

        return 'Pembayaran untuk bulan ' . Carbon::createFromFormat('m', $month)->format('F');
    }

    private static function getCompiledBankAccount(array $data)
    {
        $bankName          = isset($data['owner_bank_name']) && !is_null($data['owner_bank_name']) ? $data['owner_bank_name'] : '-';
        $bankAccountName   = isset($data['owner_bank_account_name']) && !is_null($data['owner_bank_account_name']) ? $data['owner_bank_account_name'] : '-';
        $bankAccountNumber = isset($data['owner_bank_account_number']) && !is_null($data['owner_bank_account_number']) ? $data['owner_bank_account_number'] : '-';

        return 'Bank ' . $bankName . ' Norek. ' . $bankAccountNumber . ' (a/n ' . $bankAccountName . ')';
    }
}