<?php

namespace App\Http\Helpers;

use App\Entities\Booking\BookingUser;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Room\Room;

class RentCountHelper
{
    /**
    * Rent Count Weekly
    * Hardcoded to reduce API and query rent count
    * @var Array
    */
    private static $rentCountWeekly = [
        "1 Minggu",
        "2 Minggu",
        "3 Minggu",
        "4 Minggu",
        "5 Minggu",
        "6 Minggu",
        "7 Minggu",
        "8 Minggu",
        "9 Minggu",
        "10 Minggu",
        "11 Minggu",
        "12 Minggu",
    ];

    /**
    * Rent Count Monthly
    * Hardcoded to reduce API and query rent count
    * @var Array
    */
    private static $rentCountMonthly = [
        "1 Bulan",
        "2 Bulan",
        "3 Bulan",
        "4 Bulan",
        "5 Bulan",
        "6 Bulan",
        "7 Bulan",
        "8 Bulan",
        "9 Bulan",
        "10 Bulan",
        "11 Bulan",
        "12 Bulan",
    ];

    /**
    * Rent Count Quarterly
    * Hardcoded to reduce API and query rent count
    * @var Array
    */
    private static $rentCountQuarterly = [
        "3 Bulan",
        "6 Bulan",
        "9 Bulan",
        "12 Bulan",
    ];

    /**
    * Rent Count Semiannualy
    * Hardcoded to reduce API and query rent count
    * @var Array
    */
    private static $rentCountSemiannually = [
       "6 Bulan",
       "12 Bulan",
    ];

    /**
    * Rent Count Monthly
    * Hardcoded to reduce API and query rent count
    * @var Array
    */
    private static $rentCountYearly = [
        "1 Tahun",
        "2 Tahun",
        "3 Tahun",
    ];

    /** 
     * @return array always return array
     */
    public static function getRentCountWeekly()
    {
        return self::$rentCountWeekly;
    }

    /** 
     * @return array always return array
     */
    public static function getRentCountWeeklyByKey($key)
    {
        $weekly = array_combine(range(1, count(self::$rentCountWeekly)), array_values(self::$rentCountWeekly));
        
        if (empty($weekly[$key])) {
            return "";
        }
        
        return $weekly[$key];
    }

    /** 
     * @return array always return array
     */
    public static function getRentCountMonthly()
    {
        return self::$rentCountMonthly;
    }

    /** 
     * @return array always return array
     */
    public static function getRentCountMonthlyByKey($key)
    {
        $monthly = array_combine(range(1, count(self::$rentCountMonthly)), array_values(self::$rentCountMonthly));

        if (empty($monthly[$key])) {
            return "";
        }
        
        return $monthly[$key];
    }

    /** 
     * @return array always return array
     */
    public static function getRentCountQuarterly()
    {
        return self::$rentCountQuarterly;
    }

    /** 
     * @return array always return array
     */
    public static function getRentCountQuarterlyByKey($key)
    {
        $quarterly = array_combine(range(1, count(self::$rentCountQuarterly)), array_values(self::$rentCountQuarterly));

        if (empty($quarterly[$key])) {
            return "";
        }

        return $quarterly[$key];
    }

    /** 
     * @return array always return array
     */
    public static function getRentCountSemiannually()
    {
        return self::$rentCountSemiannually;
    }

    /** 
     * @return array always return array
     */
    public static function getRentCountSemiannuallyByKey($key)
    {
        $semiannually = array_combine(range(1, count(self::$rentCountSemiannually)), array_values(self::$rentCountSemiannually));
        
        if (empty($semiannually[$key])) {
            return "";
        }
        
        return $semiannually[$key];
    }

    /** 
     * @return array always return array
     */
    public static function getRentCountYearly()
    {
        return self::$rentCountYearly;
    }

    /** 
     * @return array always return array
     */
    public static function getRentCountYearlyByKey($key)
    {
        $yearly = array_combine(range(1, count(self::$rentCountYearly)), array_values(self::$rentCountYearly));

        if (empty($yearly[$key])) {
            return "";
        }
        
        return $yearly[$key];
    }

    public static function getFormatRentCount($duration, $rentCountType)
    {
        switch ($rentCountType) {
            case "weekly" :
                return $duration . " Minggu";
            case "quarterly" :
                return $duration * 3 . " Bulan";
            case "semiannually" :
                return $duration * 6 . " Bulan";
            case "yearly" :
                return $duration . " Tahun";
            default :
                return $duration . " Bulan";
        }
    }

    public static function getFormatRentCountFromMamipayContract(int $duration, string $durationUnit): string
    {
        switch ($durationUnit) {
            case MamipayContract::UNIT_DAY :
                return $duration . " Hari";
            case MamipayContract::UNIT_WEEK :
                return $duration . " Minggu";
            case MamipayContract::UNIT_MONTH :
                return $duration . " Bulan";
            case MamipayContract::UNIT_YEAR :
                return $duration . " Tahun";
            case MamipayContract::UNIT_3MONTH :
                return $duration * 3 . " Bulan";
            case MamipayContract::UNIT_6MONTH :
                return $duration * 6 . " Bulan";
            default :
                return $duration . " Bulan";
        }
    }
    public static function convertToRentCountTypeFromMamipayContract(string $durationUnit): string
    {
        switch ($durationUnit) {
            case MamipayContract::UNIT_DAY :
                return BookingUser::DAILY_TYPE;
            case MamipayContract::UNIT_WEEK :
                return BookingUser::WEEKLY_TYPE;
            case MamipayContract::UNIT_MONTH :
                return BookingUser::MONTHLY_TYPE;
            case MamipayContract::UNIT_YEAR :
                return BookingUser::YEARLY_TYPE;
            case MamipayContract::UNIT_3MONTH :
                return BookingUser::QUARTERLY_TYPE;
            case MamipayContract::UNIT_6MONTH :
                return BookingUser::SEMIANNUALLY_TYPE;
            default :
                return $durationUnit;
        }
    }

    /** 
     * @param Object $roomPrice -> Room::price()
     * @return array always return array
     */
    public static function getAliasesRentCount($roomPrice)
    {
        $rentCountType = [];
        
        if ($roomPrice->weekly_time != "") {
            $rentCountType[] = [
                "format" => "weekly",
                "label" => "Mingguan"
            ];
        } 
        
        if ($roomPrice->monthly_time != "") {
            $rentCountType[] = [
                "format" => "monthly",
                "label" => "Bulanan"
            ];
        } 
        
        if ($roomPrice->quarterly_time != "") {
            $rentCountType[] = [
                "format" => "quarterly",
                "label" => "3 - Bulanan"
            ];
        } 
        
        if ($roomPrice->semiannualy_time != "") {
            $rentCountType[] = [
                "format" => "semiannually",
                "label" => "6 - Bulanan"
            ];
        } 
        
        if ($roomPrice->yearly_time != "") {
            $rentCountType[] = [
                "format" => "yearly",
                "label" => "Tahunan"
            ];
        }

        return $rentCountType;
    }

    public static function getFormatRentTypeMamipay($rentType)
    {
        switch ($rentType) {    
            case 'weekly':
                return MamipayContract::UNIT_WEEK;
            break;
            case 'monthly':
                return MamipayContract::UNIT_MONTH;
            break;
            case 'quarterly':
                return MamipayContract::UNIT_3MONTH;
            break;
            case 'semiannually':
                return MamipayContract::UNIT_6MONTH;
            break;
            case 'yearly':
                return MamipayContract::UNIT_YEAR;
            break;
            default:
                return MamipayContract::UNIT_MONTH;
            break;
        }
    }


    public static function getFormatPrice($rentType)
    {
        switch ($rentType) {    
            case 'weekly':
                return 1;
            break;
            case 'yearly':
                return 3;
            break;
            case 'quarterly':
                return 4;
            break;
            case 'semiannually':
                return 5;
            break;
            default:
                return 2;
            break;
        }
    }

    public static function getMinimumDuration($minDuration, $rentCountType)
    {
        switch ($rentCountType) {
            case "weekly":
                $rentCounts = self::getRentCountWeekly();
                break;
            case "quarterly":
                $rentCounts = self::getRentCountQuarterly();
                break;
            case "semiannually":
                $rentCounts = self::getRentCountSemiannually();
                break;
            case "yearly":
                $rentCounts = self::getRentCountYearly();
                break;
            default:
                $rentCounts = self::getRentCountMonthly();
        }

        $durations = [];
        foreach ($rentCounts as $key => $rentCount) {
            $slicing = explode(" ", $rentCount);
            if ($minDuration <= $slicing[0] && !in_array($slicing[1], ['Tahun', 'Minggu'])) {
                $durations[] = $rentCount;
            }

            if (in_array($slicing[1], ['Tahun', 'Minggu'])) {
                $durations[] = $rentCount;
            }
        }

        return $durations;
    }

    public static function getRentCount($rentCountType)
    {
        switch ($rentCountType) {
            case "weekly":
                return RentCountHelper::getRentCountWeekly();
            case "quarterly":
                return RentCountHelper::getRentCountQuarterly();
            case "semiannually":
                return RentCountHelper::getRentCountSemiannually();
            case "yearly":
                return RentCountHelper::getRentCountYearly();
            default:
                return RentCountHelper::getRentCountMonthly();
        }
    }
}
