<?php

namespace App\Http\Helpers;

use App\Channel\InfoBip\WhatsAppBusinessClient;
use App\Entities\Notif\NotificationWhatsappTemplate;
use App\Libraries\SMSLibrary;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Exception;

class WhatsAppBusinessHelper
{
	public const MAX_ATTEMPT = 3;
	
	protected $resources = [
		'scenario'     => 'omni/1/scenarios', // This route has 3 different methods: GET, POST and PUT
		'send_simple'  => 'omni/1/text',
		'send_advance' => 'omni/1/advanced',
		'reports'      => 'omni/1/reports',
		'logs'         => 'omni/1/logs',
	];

	private $client;

	public function __construct(WhatsAppBusinessClient $client)
	{
		$this->client = $client;
	}

	private static function compileTemplate($template)
	{

		return $template;
	}

	/**
	 * Create OMNI Scenario
	 * This should be executed ONE TIME ONLY !!!
	 * https://dev.infobip.com/whatsapp-business-messaging/creating-scenario
	 *
	 * @param bool $default
	 *
	 * @return mixed
	 */
	public function createScenario($default = true)
	{
		// compile parameters
		$parameters = [
			'name'    => config('whatsapp_business.scenario_name'),
			'flow'    => [
				[
					"from"    => config('whatsapp_business.sender_number'),
					"channel" => "WHATSAPP",
				],
			],
			'default' => $default,
		];

		try
		{
			$this->client->setParams($parameters);
			$response = $this->client->post($this->resources['scenario']);

			return $this->handleResponse($response);
		}
		catch (Exception $e)
		{
			Bugsnag::notifyException($e);
		}
	}

	/**
	 * Check existing OMNI Scenario
	 * https://dev.infobip.com/whatsapp-business-messaging/creating-scenario
	 *
	 * @return bool|mixed
	 */
	public function checkScenario()
	{
		try
		{
			$key = config('whatsapp_business.scenario_key');

			if (empty($key))
			{
				return false;
			}

			$response = $this->client->get($this->resources['scenario'] . '/' . $key);

			return $this->handleResponse($response);
		}
		catch (Exception $e)
		{
			Bugsnag::notifyException($e);
		}
	}

	/**
	 * Send OMNI WhatsApp
	 * https://dev.infobip.com/whatsapp-business-messaging/sending-free-form-messages
	 *
	 * @param \App\Entities\Notif\NotificationWhatsappTemplate $template
	 * @param array $templateData
	 * @param array $destinations
	 *
	 * @return mixed
	 */
	public function send(NotificationWhatsappTemplate $template, array $templateData = [], array $destinations)
	{
		// compile destinations
		$formattedDestinations = [];
		foreach ($destinations as $key => $destination)
		{
			$data = [
				"to" => [
					"phoneNumber" => $destination,
				],
			];

			// push into $formattedDestinations array
			$formattedDestinations[] = $data;
		}

		// compile parameters
		$parameters = [
			"scenarioKey"  => config('whatsapp_business.scenario_key'),
			'destinations' => $formattedDestinations,
			'whatsApp'     => [
				"text" => $message,
			],
		];

		try
		{
			$this->client->setParams($parameters);
			$response = $this->client->post($this->resources['send_simple']);

			return $this->handleResponse($response);
		}
		catch (Exception $e)
		{
			Bugsnag::notifyException($e);
		}
	}

	/**
	 * Send OMNI WhatsApp with Template
	 * https://dev.infobip.com/whatsapp-business-messaging/sending-the-message-template
	 *
	 * @param \App\Entities\Notif\NotificationWhatsappTemplate $template
	 * @param array $templateData
	 * @param array $destinations
	 * @param bool $isTesting
	 *
	 * @return mixed
	 */
	public function sendTemplate(
		NotificationWhatsappTemplate $template, 
		array $templateData, 
		array $destinations, 
		bool $isTesting = false
	) {
		if (
			$template->isSmsActive() &&
			is_array($destinations) && 
			!is_null($destinations[0]) && 
			strlen($destinations[0]) > 2 
		) {
			SMSLibrary::send(
				substr($destinations[0], 2),
				$template->getCompiledTemplateWithVariableData(
					$templateData, 
					$isTesting
				)
			);
		}

		try
		{
			$formattedDestinations = array_map(
				function ($destination) {
					return [
						"to" => [
							"phoneNumber" => $destination,
						],
					];
				}, 
				$destinations
			);
			
			// compile parameters
			$parameters = [
				"scenarioKey"  => config('whatsapp_business.scenario_key'),
				'destinations' => $formattedDestinations,
				'whatsApp'     => [
					"templateName"      => $template->name,
					"templateNamespace" => config('whatsapp_business.namespace'),
					"templateData"      => $templateData,
					"language"          => config('whatsapp_business.language'),
				],
			];

			$response = $this->client->post($this->resources['send_advance'], $parameters);

			return $this->handleResponse($response);
		}
		catch (Exception $exception)
		{
			$url = $this->resources['send_advance'];
			Bugsnag::notifyException(
				$exception,
				function ($report) use ($url, $parameters) {
					$report->setMetaData([
						'infobip' => [
							'url' => $url,
							'payload' => $parameters
						]
					]);
				}
			);
		}
	}

	/**
	 * Get OMNI Logs
	 * https://dev.infobip.com/omni-channel/logs
	 *
	 * @param string $messageId
	 *
	 * @return mixed
	 */
	public function getReport(string $messageId)
	{
		$parameters = "?messageId=" . $messageId . "&channel=WHATSAPP";

		try
		{
			$response = $this->client->get($this->resources['logs'] . $parameters);

			return $this->handleResponse($response);
		}
		catch (Exception $e)
		{
			Bugsnag::notifyException($e);
		}
	}

	/**
	 * @param $response
	 *
	 * @return array
	 */
	private function handleResponse($response): array
	{
		$this->client->clearParams();

		return json_decode((string) $response->getBody(), true);
	}
}
