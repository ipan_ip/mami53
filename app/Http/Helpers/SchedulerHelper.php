<?php

namespace App\Http\Helpers;

/**
 * Ref doc: https://laravel.com/docs/5.8/scheduling#schedule-frequency-options
 */
class SchedulerHelper
{
    protected $frequencies = [
        'every 5 minutes',
        'every 10 minutes',
        'every 30 minutes',
        'every 1 hour',
        'every 6 hours',
        'every 12 hours',
        'daily',
        'weekly',
        'biweekly',
        'monthly',
        'every first week of month',
        'every second week of month',
        'every third week of month',
        'every last week of month',
        'quarterly',
        'semiannually',
        'annually',
        'custom'
    ];
    
    protected function getSchedule()
    {
        return $this->frequencies;
    }
}