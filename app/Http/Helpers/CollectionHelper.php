<?php

namespace App\Http\Helpers;

use Illuminate\Database\Eloquent\Collection;

/**
 * Class CollectionHelper
 * @package App\Http\Helpers
 */
class CollectionHelper
{
    public static function getFirstInstanceOfCollection($collection, $intance)
    {
        if ($collection instanceof Collection) {
            if (isset($collection[0]) && $collection[0] instanceof $intance)
                return $collection[0];
        }
        return null;
    }
}