<?php

namespace App\Http\Helpers;

use App\Entities\Booking\BookingUser;
use App\Entities\Lpl\Criteria;
use App\Entities\Mamipay\MamipayContractKost;
use App\Entities\Room\Room;
use App\Entities\Room\RoomSortScoreTracker;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Log;

class LplScoreHelper
{
    private const NULL_SCORE = 0;
    private const FULL_SCORE = 4100;
    private const PARTNER_PREMIUM_SALDO_ON_SCORE = 3668;
    private const PARTNER_PREMIUM_SALDO_OFF_SCORE = 3604;
    private const PARTNER_NON_PREMIUM_SCORE = 3597;
    private const MIN_GOOD_STANDAR_BAR = 70; // on percentage
    private const VERSION_2_KICKSTART = '2020-08-12 23:59:59'; // After Tuesday's Hotfix (2020-08-12)
    private const VERSION_3 = 3;

    /*
     * Criteria name for tracking spec.
     * Ref: https://mamikos.atlassian.net/wiki/spaces/BGS/pages/568623123/BG+Draft+Improving+LPL+Scoring+System+-+GP#9.-Tracking-Spec
     */
    private static $criteriaForTracking = [
        'has_room' => 'AVAIL_ROOM',
        'is_booking' => 'BBK',
        'is_flash_sale' => 'F_SALE',
        'is_gp_1' => 'GP1',
        'is_gp_2' => 'GP2',
        'is_gp_3' => 'GP3',
        'is_gp_4' => 'GP4',
        'is_guaranteed' => 'GUARANTEE',
        'is_mamiroom' => 'MAMIROOMS',
        'is_not_hostile' => 'NOT_HOSTILE',
        'has_photo' => 'PHOTO',
        'is_premium' => 'PREMIUM',
        'is_premium_saldo_on' => 'PREMIUM_ON',
        'is_partner' => 'PARTNER',
        'is_bar' => 'BAR',
        'is_basic' => 'BASIC_KOS',
        'has_good_booking_performance' => 'GOOD_BOOKING_PERFORMANCE'
    ];

    /**
     * @return Builder[]|Collection
     */
    public static function getAvailableCriteria()
    {
        return Criteria::query()
            ->orderBy('order', 'desc')
            ->get();
    }

    public static function isEligibleForUplifting(Room $room): bool
    {
        if (!$room) {
            return false;
        }

        if ($room->room_available < 1) {
            return false;
        }

        /*
         * This is a temporary Partner Kos list that would be score-uplifted for Business Team requirement
         * Ref task: https://mamikos.atlassian.net/browse/BG-1761
         * Slug data taken on: July 17, 2020 at 2.30 PM
         *
         * Update ref task: https://mamikos.atlassian.net/browse/BG-1879
         * Update ref task: https://mamikos.atlassian.net/browse/BG-1929
         *
         * TODO: Will be adjusted later
         */
        $upliftedRoomIds = [
            /* Previous uplifted Kos */
            224731, // "kost-jakarta-barat-kost-campur-eksklusif-kost-grogol-petamburan-trans-jakarta-barat-rmz"

            /* Kos Survey Assistance Trial */
            192659, // "kost-sleman-kost-putri-murah-kost-ugm-lovely-tipe-a-sleman-yogyakarta-rmz"
            230468, // "kost-sleman-kost-putri-murah-kost-greenhouse-tipe-a-depok-sleman-rmz"
            230451, // "kost-sleman-kost-putri-eksklusif-kost-uny-ugm-pemondokan-baiti-tipe-a-depok-sleman-rmz-1"

            /* Mamirooms */
            201473, // "kost-jakarta-barat-kost-campur-eksklusif-kost-eksklusif-murah-tomang-grogol-gelong-residence-25-tipe-a-jakarta-barat-1"
            177726, // "kost-jakarta-barat-kost-campur-eksklusif-kost-eksklusif-murah-tomang-mamirooms-gelong-residence-tipe-a-jakarta-barat-1"
            195705, // "kost-jakarta-barat-kost-campur-eksklusif-kost-eksklusif-murah-hayam-wuruk-mamirooms-keagungan-20-tipe-a-taman-sari-jakarta-barat"
            177807, // "kost-jakarta-selatan-kost-campur-eksklusif-kost-gandaria-eksklusif-murah-mamirooms-praja-sapphire-kebayoran-lama-jakarta-selatan"
            183193, // "kost-jakarta-pusat-kost-putri-eksklusif-kost-mamirooms-tanah-abang-mnv-coliving-tipe-b-jakarta-pusat-1"

            /* Mamirooms https://mamikos.atlassian.net/browse/BG-2125 */
            163196, // "kost-sleman-kost-putri-eksklusif-kost-mamirooms-ugm-griya-amira-tipe-a-depok-sleman",
            190171, // "kost-sleman-kost-putra-eksklusif-kost-mamirooms-ugm-pandega-duta-sleman-yogyakarta-1"
            182845, // "kost-sleman-kost-campur-eksklusif-kost-mamirooms-ugm-yang-dji-tipe-b-mlati-sleman"
            162249, // "kost-sleman-kost-putri-eksklusif-kost-mamirooms-kataji-tipe-a-mlati-sleman"
            227689, // "kost-sleman-kost-putri-eksklusif-kost-mamirooms-ugm-griya-pastika-d2-tipe-a-depok-sleman"

            /* Mamirooms https://mamikos.atlassian.net/browse/BG-2168 */
            239927, // "kost-jakarta-selatan-kost-campur-eksklusif-kost-eksklusif-murah-fatmawati-tipe-f-mamirooms-jakarta-selatan"
            170274, // "kost-jakarta-selatan-kost-putri-eksklusif-kost-eksklusif-cilandak-mamirooms-cozee-home-tipe-a-jakarta-selatan-diskon-1-juta-"
            170701, // "kost-jakarta-selatan-kost-putri-eksklusif-kost-mamirooms-cilandak-cozee-home-tipe-b-jakarta-selatan-1"
            239921, // "kost-jakarta-selatan-kost-campur-eksklusif-kost-eksklusif-murah-fatmawati-tipe-a-mamirooms-jakarta-selatan"
        ];

        // for non production, override the id for testing
        if (app()->environment() !== 'production') {
            $upliftedRoomIds = [
                1000009248, // Uplift Mamirooms 01
                1000009251, // Uplift Mamirooms 02
                1000009253, // Uplift Mamirooms 03
                1000009255, // Uplift Mamirooms 04
            ];
        }

        if (!in_array($room->id, $upliftedRoomIds)) {
            return false;
        }

        return true;
    }

    public static function getFullScore(): int
    {
        return self::FULL_SCORE;
    }

    public static function isCriteriaMet(Room $room, Criteria $criterion): bool
    {
        $isFulfilled = false;

        switch ($criterion->attribute) {
            case 'is_not_hostile':
                $isFulfilled = (int) $room->hostility < 1;
                break;
            case 'has_photo':
                $isFulfilled = $room->getHasPhotos();
                break;
            case 'has_room':
                $isFulfilled = $room->room_available > 0;
                break;
            case 'is_premium_saldo_on':
                $isFulfilled = $room->is_promoted == 'true';
                break;
            case 'is_booking':
                $isFulfilled = (int) $room->is_booking === 1;
                break;
            case 'is_guaranteed':
                $isFulfilled = $room->getIsGuarantee();
                break;
            case 'is_premium':
                $isFulfilled = $room->getIsOwnedByPremiumOwner();
                break;
            case 'is_flash_sale':
                $isFulfilled = $room->getIsFlashSale();
                break;
            case 'is_mamiroom':
                $isFulfilled = (int) $room->is_mamirooms === 1;
                break;
            case 'is_gp_1':
                $isFulfilled = $room->gold_plus_level === Room::VALUE_OF_GOLDPLUS_1;
                break;
            case 'is_gp_2':
                $isFulfilled = $room->gold_plus_level === Room::VALUE_OF_GOLDPLUS_2;
                break;
            case 'is_gp_3':
                $isFulfilled = $room->gold_plus_level === Room::VALUE_OF_GOLDPLUS_3;
                break;
            case 'is_gp_4':
                $isFulfilled = $room->gold_plus_level === Room::VALUE_OF_GOLDPLUS_4;
                break;
            case 'is_partner':
                $isFulfilled = $room->getIsKosPartner();
                break;
            case 'is_bar':
                $isFulfilled = BookingAcceptanceRateHelper::getAcceptanceRate($room) 
                    >= self::MIN_GOOD_STANDAR_BAR;
                break;
            case 'is_basic':
                $isFulfilled = self::isBasicKos($room);
                break;
            case 'has_good_booking_performance';
                $isFulfilled = self::isHavingGoodBookingPerformance($room);
                break;
            default:
                break;
        }

        return $isFulfilled;
    }

    /**
     * is basic kos means this kost not GP1, GP2, GP3, GP4 
     * and not mamiroom
     * 
     * @param Room $room
     * @return bool
     */
    public static function isBasicKos(Room $room)
    {
        if (
            $room->gold_plus_level === Room::VALUE_OF_NON_GOLDPLUS
            && (int) $room->is_mamirooms !== 1
        ) {
            return true;
        }
        
        return false;
    }

    /**
     * method to define that is kos has good booking performance
     * 
     * @param Room $room
     * @return bool
     */
    public static function isHavingGoodBookingPerformance(Room $room)
    {
        $strActiveFrom  = $room->owner_active_since;
        $dayRange       = 14; // in days

        $now            = date('Y-m-d');
        $activeFrom     = date('Y-m-d', strtotime($strActiveFrom));
        $lastXDaysDate  = date('Y-m-d', strtotime($now. ' - '.$dayRange.' days'));

        // Check 1: check whether the owner kos just turn on
        // booking feature kos in past 14 days
        if ($activeFrom >= $lastXDaysDate) {
            return true;
        }

        // Check 2: check whether kost has at least 1 paid booking
        // in past 14 days
        $nBooking = BookingUser
            ::getTotalPaidBookingLastXDay($dayRange, $room->song_id);
        
        if ($nBooking > 0) {
            return true;
        }

        // Check 3: check whether kost has at least 1 active contract
        // in past 14 days
        $nActiveContract = MamipayContractKost
            ::getTotalActiveContractKosWithin($dayRange, $room->id);

        if ($nActiveContract > 0) {
            return true;
        }

        return false;
    }

    public static function getScore(Room $room): int
    {
        $score = 0;

        // Bypass score for uplifted Kos list
        if (self::isEligibleForUplifting($room)) {
            return self::getFullScore();
        }

        if ($room->getIsKosPartner()) { 
            $score = self::getScoreForPartnerKos($room);
        } else {
            $score = self::getScoreForRegularKos($room);
        }

        return $score;
    }

    public static function getScoreForPartnerKos(Room $room)
    {
        $score = 0;

        $isPremium          = (bool) $room->getIsOwnedByPremiumOwner();
        $isSaldoPremiumOn   = (bool) ($room->is_promoted == 'true');

        if (!$isPremium) {
            $score = self::PARTNER_NON_PREMIUM_SCORE;
        } else {
            $score = $isSaldoPremiumOn 
                ? self::PARTNER_PREMIUM_SALDO_ON_SCORE 
                : self::PARTNER_PREMIUM_SALDO_OFF_SCORE;
        }

        $criteria = self::getAvailableCriteria();
        if (empty($criteria)) {
            return $score;
        }

        foreach ($criteria as $criterion) {
            $score += self::getCriteriaScoreForPartner($room, $criterion);
        }

        return $score;
    }

    public static function getScoreForRegularKos(Room $room)
    {
        $score = 0;

        $criteria = self::getAvailableCriteria();
        if (empty($criteria)) {
            return $score;
        }

        foreach ($criteria as $criterion) {
            $score += self::isCriteriaMet($room, $criterion) ? $criterion->score : self::NULL_SCORE;
        }

        return $score;
    }

    /**
     * Method for partnerisp kost, get score per criteria
     * 
     * if criteria not fulfilled than will return (-1) * criteria->score
     * else return 0
     * 
     * @param Room
     * @param Builder|Criteria
     * 
     * @return int
     */
    public static function getCriteriaScoreForPartner(Room $room, $criterion): int
    {
        if ($criterion->attribute === 'is_flash_sale') {
            return (bool) $room->getIsFlashSale() ? $criterion->score : self::NULL_SCORE;
        }

        $isFulfilled    = false;
        $notInCriteria  = false;

        switch ($criterion->attribute) {
            case 'is_not_hostile':
                $isFulfilled = (int) $room->hostility < 1;
                break;
            case 'has_photo':
                $isFulfilled = $room->getHasPhotos();
                break;
            case 'has_room':
                $isFulfilled = $room->room_available > 0;
                break;
            default:
                $notInCriteria = true;
                break;
        }

        if ($notInCriteria || $isFulfilled) return 0;

        return (-1) * $criterion->score;
    }

    public static function getCriteriaForTracking(Room $room): string
    {
        $compiledCriteria = [];

        $criteria = self::getAvailableCriteria();
        if (empty($criteria)) {
            return '';
        }

        foreach ($criteria as $criterion) {
            if (self::isCriteriaMet($room, $criterion)) {
                $compiledCriteria[] = self::$criteriaForTracking[$criterion->attribute];
            }
        }

        // Alphabetical sorting
        if (!sort($compiledCriteria)) {
            return '';
        }

        return implode(' | ', $compiledCriteria);
    }

    public static function recalculateRoomScoreById(int $roomId, $logChannel = "roomScore"): void
    {
        $room = Room::with([
                'owners',
                'cards',
                'level',
                'discounts'
            ])
            ->where('id', $roomId)
            ->first();

        if (is_null($room)) {
            return;
        }

        self::recalculateRoomScore($room, $logChannel);
    }

    public static function recalculateRoomScore(Room $room, $logChannel = "roomScore"): void
    {
        try {
            $log = Log::channel($logChannel);
            $newScore = self::getScore($room);

            $scoreData = [
                'designer_id' => $room->id,
                'old_score' => $room->sort_score,
                'new_score' => $newScore
            ];

            if ($newScore === $room->sort_score) {
                $log->info('No recalculation needed for ' . $room->name, $scoreData);
            } else {
                $room->sort_score = $newScore;
                $room->save();

                $log->info($room->name . '\'s score recalculated!', $scoreData);
                RoomSortScoreTracker::trackScoreAndVersion(self::VERSION_3, $newScore, $room->id);
            }
        } catch (Exception $exception) {
            $log->error('Failed!', ['error' => $exception->getMessage()]);
        }
    }

    public static function recalculateRoomScoreWithDryRun(Room $room): void
    {
        try {
            $start = microtime(true);

            $newScore = self::getScore($room);

            $scoreData = [
                'designer_id' => $room->id,
                'old_score' => $room->sort_score,
                'new_score' => $newScore
            ];

            if ($newScore === $room->sort_score) {
                Log::channel('roomScore')
                    ->info('No recalculation needed for ' . $room->name, $scoreData);
            } else {
                Log::channel('roomScore')
                    ->info($room->name . '\'s score recalculated!', $scoreData);
            }

            $executionTime = microtime(true) - $start;
            Log::channel('roomScore')
                ->info('Processing time: ' . $executionTime . ' seconds');
        } catch (Exception $exception) {
            Log::channel('roomScore')
                ->error(
                    'Failed!',
                    [
                        'error' => $exception->getMessage()
                    ]
                );
        }
    }

    public static function isEligibleForVersion2(): bool
    {
        /* Bypass if it's not Production environment */
        if (app()->environment() !== 'production') {
            return true;
        }

        $milestone = Carbon::parse(self::VERSION_2_KICKSTART);
        $now = Carbon::now();

        return $now->gte($milestone);
    }

    public static function isScoreShownForTesting(): bool
    {
        if (app()->environment() !== 'production') {
            return true;
        }

        return false;
    }
}
