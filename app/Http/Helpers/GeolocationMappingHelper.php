<?php

namespace App\Http\Helpers;

use App\Entities\Area\AreaGeolocation;
use App\Entities\Room\Geolocation;
use App\Entities\Room\Room;
use App\Entities\Search\InputKeyword;
use Cache;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialExpression;
use Grimzy\LaravelMysqlSpatial\Types\Geometry;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Illuminate\Support\Collection;

class GeolocationMappingHelper
{
    public const DEFAULT_CACHE_LIFETIME = 10080; // 7 days
    public const CACHE_TAG = 'geocode';
    public const CACHE_KEY_PREFIX = 'room-ids-';

    protected $disabledTriggers = [
        'scheduler',
        'observer'
    ];

    /**
     * Sync-up against "Area Geolocation"
     *
     * @param Room $room
     * @param string $trigger
     * @return array|false
     */
    public function syncGeocode(Room $room, string $trigger = 'command')
    {
        /*
         * Disable command which triggered from certain referrers
         * Ref Task: https://mamikos.atlassian.net/browse/BG-3663
         */
        if (in_array($trigger, $this->disabledTriggers) !== false) {
            return false;
        }

        if (!$room->geolocation) {
            return false;
        }

        // Handle data that coming from `RoomGeolocationObserver`
        $kosGeolocation = $room->geolocation;
        if (get_class($kosGeolocation->geolocation) === SpatialExpression::class) {
            $location = Point::fromWKT($kosGeolocation->geolocation->getSpatialValue());
        } else {
            $location = $kosGeolocation->geolocation;
        }

        $areaGeocodeIDs = $this->getCoveringAreaGeocodeIDs($location);
        if (empty($areaGeocodeIDs)) {
            return false;
        }

        return $room->geocodes()->sync($areaGeocodeIDs);
    }

    /**
     * Remove "Area Geocode" relation data
     *
     * @param Room $room
     * @param string $trigger
     * @return false|int
     */
    public function removeGeocode(Room $room, string $trigger = 'command')
    {
        /*
         * Disable command which triggered from certain referrers
         * Ref Task: https://mamikos.atlassian.net/browse/BG-3663
         */
        if (in_array($trigger, $this->disabledTriggers) !== false) {
            return false;
        }

        if (!$room->geolocation) {
            return false;
        }

        return $room->geocodes()->detach();
    }

    /**
     * Get Kos IDs by using Search Input Keyword
     *
     * @param string $keyword
     * @return array
     */
    public function getRoomIDsBySearchKeyword(string $keyword): array
    {
        $roomIDs = [];

        if (
            empty($keyword)
            || !ctype_alpha($keyword)
        ) {
            return $roomIDs;
        }

        $input = InputKeyword::with('suggestions.geolocation')
            ->has('suggestions.geolocation')
            ->where('keyword', 'like', $keyword)
            ->first();

        if (is_null($input)) {
            return $roomIDs;
        }

        return $this->getRoomIDsBySuggestions($input->suggestions);
    }

    public function getRoomIDsByMultipleSearchKeyword(array $keywords): array
    {
        $roomIDs = new Collection();

        foreach ($keywords as $keyword) {
            $targetRoomIDs = $this->getRoomIDsBySearchKeyword((string)trim($keyword));
            $roomIDs = $roomIDs->merge($targetRoomIDs);
        }

        return $roomIDs->unique()->toArray();
    }

    /**
     * Getting all Room IDs within specified Geocode data boundary
     *
     * @param $geolocation
     * @return Collection
     */
    public function getRoomIDWithinGeolocationBoundary($geolocation): Collection
    {
        $cachedResponse = Cache::tags([self::CACHE_TAG])
            ->get(self::CACHE_KEY_PREFIX . $geolocation->id);

        if (!is_null($cachedResponse)) {
            return $cachedResponse;
        }

        $roomsIDs = Geolocation::with(
            [
                'room' => function ($room) {
                    $room->active();
                }
            ]
        )
            ->select(
                [
                    'designer_id',
                    'geolocation'
                ]
            )
            ->within('geolocation', $geolocation->geolocation)
            ->pluck('designer_id');

        Cache::tags([self::CACHE_TAG])
            ->put(self::CACHE_KEY_PREFIX . $geolocation->id, $roomsIDs, self::DEFAULT_CACHE_LIFETIME);

        return $roomsIDs;
    }

    /**
     * Getting all Room IDs within specified Suggestion's geolocation data
     *
     * @param $suggestions
     * @return array
     */
    public function getRoomIDsBySuggestions($suggestions): array
    {
        if (empty($suggestions)) {
            return [];
        }

        $roomIDs = new Collection();

        foreach ($suggestions as $suggestion) {
            if (count($suggestion->geolocation) > 0) {
                $targetRoomIDs = $this->getRoomIDWithinGeolocationBoundary($suggestion->geolocation->first());
                $roomIDs = $roomIDs->merge($targetRoomIDs);
            }
        }

        if ($roomIDs->isEmpty()) {
            return [];
        }

        return $roomIDs->unique()->toArray();
    }

    /**
     * Get covering Area Geocode of certain Kos geolocation
     *
     * @param Geometry $location
     * @return mixed
     */
    private function getCoveringAreaGeocodeIDs(Geometry $location)
    {
        return AreaGeolocation::contains('geolocation', $location)
            ->pluck('id')
            ->toArray();
    }
}