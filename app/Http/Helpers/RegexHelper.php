<?php

namespace App\Http\Helpers;


class RegexHelper
{
    public const NUMERIC = '/^(0|[1-9][0-9]*)$/';

    // SendBird
    public const SENDBIRD_DEFAULT_GROUP_CHANNEL = '/^sendbird_group_channel_.\w+$/';

    // Private Ip
    public const PRIVATE_IP_ADDRESS = '/(^127\.)|(^10\.)|(^172\.1[6-9]\.)|(^172\.2[0-9]\.)|(^172\.3[0-1]\.)|(^192\.168\.)/';

    /**
     * @return string
     */
    public static function alphanumeric(): string
    {
        return '/[^a-z0-9_\s-]*$/';
    }

    /**
     * @return string
     */
    public static function alphanumericAndSpace(): string
    {
        return '/^[a-zA-Z0-9 ]*$/';
    }

    /**
     * @return string
     */
    public static function smallCaseAlphanumericAndSpace(): string
    {
        return '/^[a-z0-9 ]*$/';
    }

    /**
     * @return string
     */
    public static function alphaOnlyAndSpace(): string
    {
        return '/^[a-zA-Z ]+$/u';
    }

    /**
     * @return string
     */
    public static function alphaCommaAndSpace(): string
    {
        return '/^[a-zA-Z, ]+$/u';
    }

    /**
     * @return string
     */
    public static function alphaNumericComaDotAndSpace(): string
    {
        return '/^[a-zA-Z0-9,. ]*$/';
    }

    /**
     * @return string
     */
    public static function numericOnly(): string
    {
        return '/[^0-9]/';
    }

    /**
     * @return string
     */
    public static function whitespaceAndUnderscore(): string
    {
        return '/[\s_]/';
    }

    /**
     * @return string
     */
    public static function multipleDashes(): string
    {
        return '/[\s-]+/';
    }


    /**
     * Example case: this is {{processed}} word
     *
     * @return string
     */
    public static function surroundByDoubleCurlyBrackets(): string
    {
        return '/{{2}([^~*{}\n]+)}{2}/';
    }

    /**
     * Example case: this is *processed* word
     *
     * @return string
     */
    public static function surroundByAsterisks(): string
    {
        return '/\*([^~_{}`*\n]+)\*/';
    }

    /**
     * Example case: this is _processed_ word
     *
     * @return string
     */
    public static function surroundByUnderscores(): string
    {
        return '/_([^*~{}_`,.\n]+)_/';
    }

    /**
     * Example case: this is ~processed~ word
     *
     * @return string
     */
    public static function surroundByTildes(): string
    {
        return '/~([^*_{}`~\n]+)~/';
    }

    /**
     * Example case: this is ```processed``` word
     *
     * @return string
     */
    public static function surroundByTripleBackticks(): string
    {
        return '/`{3}([^*~_{}`\n]+)`{3}/';
    }

    /**
     * @return string
     */
    public static function surroundByDashes(): string
    {
        return '/-[\d]+-/';
    }

    /**
     * @return string
     */
    public static function phoneNumberFormat(): string
    {
        return '/^(08[1-9]\d{7,10})$/';
    }

    /**
     *    New regex for phone number
     *
     *    There are a lot of user with number between 8 - 10 digits
     *    Instead of 10 - 14 digits
     */
    public static function phoneNumber(): string
    {
        return '/^(08[1-9]\d{5,11})$/';
    }

    /**
     *    Regex for checking name
     *
     *    Allow alphabetic character, allow space, disallow numeric.
     */
    public static function name(): string
    {
        return '/^[a-zA-Z ]+$/';
    }

    /**
     * Regex for valid email -> according to Rfc 2822
     * 
     * @return string
     */
    public static function validEmail(): string 
    {
        return '/^(?:[a-z0-9!#$%\'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&\'*+\/=?^_`{|}'.
            '~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\['.
            '\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-'.
            '9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-'.
            '9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?'.
            '|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-' .
            '\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/';
    }

    /**
     * Regex for getting uppercase letters only
     *
     * @return string
     */
    public static function uppercaseLetters(): string
    {
        return '/\p{Lu}/u';
    }

    /**
     * Regex for Non Alphanumeric and Non Hyphen characters
     *
     * @return string
     */
    public static function nonAlphaNumericAndHyphen(): string
    {
        return '/[^\w-]/';
    }

    /**
     * Regex for valid property name
     *
     * @return string
     */
    public static function validPropertyName(): string
    {
        return '/^([a-z])+([^\‘\“\’\”\'\"\/\(\)\,\.\-])+$/i';
    }
}
