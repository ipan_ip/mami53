<?php

namespace App\Http\Helpers;

use App\User;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use Laravel\Passport\Http\Controllers\HandlesOAuthErrors;
use League\OAuth2\Server\AuthorizationServer;
use App\Libraries\Oauth\AuthorizationServer as MamiAuthorizationServer;
use stdClass;
use Zend\Diactoros\Response as Psr7Response;

/**
 * OAuthHelper class
 *
 * all helper function about OAuth2 with laravel passport
 */
class OAuthHelper
{
    use HandlesOAuthErrors;

    /**
     * @var League\OAuth2\Server\AuthorizationServer
     */
    private $server;

    public function __construct(AuthorizationServer $server)
    {
        $this->server = $server;
    }

    /**
     * issue token for User object
     *
     * @param \App\User $user
     * @return array|null
     */
    public function issueTokenFor(User $user)
    {
        if ($this->server instanceof MamiAuthorizationServer) {
            return $this->withErrorHandling(function () use ($user) {
                $response = $this->server->respondAccessTokenForUser($user, new Psr7Response);
                return json_decode((string) $response->getBody(), true);
            });
        }

        return null;
    }

    /**
     * issueToken for issueing OAuth token using API by using Guzzle Client
     *
     * @param string $username
     * @param string $hashedPassword
     * @param callable|null $mapper
     * @return callable|null
     */
    public function issueTokenFromApi(
        string $username,
        string $hashedPassword,
        ?callable $mapper = null
    ): ?stdClass {
        $clientId = config('passport.client_id');
        $clientSecret = config('passport.client_secret');

        try {
            $response = app()->make(Client::class)->request(
                "POST",
                url('oauth/token'),
                [
                    'multipart' => [
                        [
                            'name'     => 'grant_type',
                            'contents' => 'password'
                        ],
                        [
                            'name'     => 'client_id',
                            'contents' => $clientId
                        ],
                        [
                            'name'     => 'client_secret',
                            'contents' => $clientSecret
                        ],
                        [
                            'name'     => 'username',
                            'contents' => $username
                        ],
                        [
                            'name'     => 'password',
                            'contents' => $hashedPassword
                        ],
                    ],
                ]
            );

            if ($response->getStatusCode() !== 200) {
                return null;
            }

            $result = json_decode($response->getBody()->getContents());

            if (is_null($mapper)) {
                $mapper = function ($item) {
                    return (object) [
                        'token_type'    => $item->token_type,
                        'expires_in'    => $item->expires_in,
                        'access_token'  => $item->access_token,
                        'refresh_token' => $item->refresh_token,
                    ];
                };
            }

            return $mapper($result);
        } catch (\Exception $e) {
            Log::warning($e);
            Bugsnag::notifyException(
                $e,
                function ($report) use ($username, $clientId, $clientSecret) {
                    $report->setMetaData([
                        'oauth' => [
                            'username' => $username,
                            'client_id' => $clientId,
                            'secret' => $clientSecret
                        ]
                    ]);
                }
            );

            return null;
        }
    }
}
