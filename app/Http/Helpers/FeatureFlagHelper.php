<?php

namespace App\Http\Helpers;

class FeatureFlagHelper
{

    /**
     * Feature flag for is flash sale based on device request
     * @return bool
     */
    public static function isSupportFlashSale(): bool
    {
        try {
            // access from web
            if (
                !isset(app()->device)
                && !isset(app()->device->app_version_code)
            ) {
                return true;
            }

            // access from app
            return self::isFromApp() && app()->device->getIsFlashSaleSupported();
        } catch (\Exception $exception) {
            return true; // default value set as web
        }
    }

    /**
     * Feature flag for is support new filter based on device request
     * @return bool
     */
    public static function isSupportKosRule(): bool
    {
        try {
            // access from web
            if (
                !isset(app()->device)
                && !isset(app()->device->app_version_code)
            ) {
                return true;
            }

            // access from app
            return self::isFromApp() && app()->device->getIsSupportKosRule();
        } catch (\Exception $exception) {
            return true; // default value set as web
        }
    }

    /**
     * Feature flag for is support new Recommendation City on Device request
     * @return bool
     */
    public static function isSupportNewRecommendationCity(): bool
    {
        try {
            // access from web
            if (
                !isset(app()->device)
                && !isset(app()->device->app_version_code)
            ) {
                return true;
            }

            // access from app
            return self::isFromApp() && app()->device->getIsSupportNewRecommendationCity();
        } catch (\Exception $exception) {
            return true; // default value set as web
        }
    }

    /**
     * Feature flag for is support rules with photo on Device request
     * 
     * @return bool
     */
    public static function isSupportKosRuleWithPhoto(): bool
    {
        try {
            // access from web
            if (
                !isset(app()->device)
                && !isset(app()->device->app_version_code)
            ) {
                return true;
            }

            // access from app
            return self::isFromApp() && app()->device->getIsSupportKosRuleWithPhoto();
        } catch (\Exception $exception) {
            return true; // default value set as web
        }
    }

    /**
     * Identify whether this request come from app or not
     * @return bool
     */
    public static function isFromApp(): bool
    {
        try {
            return !is_null(app()->device) && isset(app()->device->app_version_code);
        } catch (\Exception $exception) {
            return false; // default value set as web
        }
    }

}
