<?php

namespace App\Http\Helpers;

/**
 * Class UserEventsHelper
 * @package App\Http\Helpers
 */
class UserEventsHelper
{
    protected $events;

    /**
     * UserEventsHelper constructor.
     *
     * TODO: Store all of these events in Model
     */
    public function __construct()
    {
        $this->events = [
            'owner'  => [
                'account_registered'             => "When owner account is registered",
                'account_activated'              => "When owner account is activated",
                'get_new_booking'                => "When owner get new booking",
                'reminder_new_booking_5'         => "When owner has 5 hours left for confirm booking",
                'reminder_new_booking_23'        => "When owner has 23 hours left for confirm booking",
                'reminder_new_booking_95'        => "When owner has 95 hours left for confirm booking",
                'reminder_booking'               => "When owner has unconfirmed booking request",
                'reminder_check_in'              => "When owner has unconfirmed booking check-in",
                'reminder_expiration'            => "When owner has unconfirmed booking expiration",
                'transaction_down_payment'       => "When owner get successful DP payment",
                'transaction_settlement_payment' => "When owner get successful settlement payment",
                'transaction_full_payment'       => "When owner get successful full payment",
                'transaction_invoice_payment'    => "When owner get successful invoice payment",
                'payout_down_payment'            => "When owner get DP payment payout",
                'payout_settlement_payment'      => "When owner get settlement payment payout",
                'payout_full_payment'            => "When owner get full payment payout",
                'payout_invoice_payment'         => "When owner get invoice payment payout",
                'reminder_unread_chat'           => "When owner has unread chats",
                'thanos_notification'            => "When owner has thanos hidden kos",
                'gp4_invoice_notification'       => "When owner has GP4 Invoice",
                'unopened_dbet_tenant_request'   => "When owner unopened dbet tenant request",
            ],
            'tenant' => [
                'get_booking_accepted' => "When tenant get booking request confirmed",
                'reminder_booking_6h'  => "When tenant has 6 hours left for payment",
                'reminder_booking_3h'  => "When tenant has 3 hours left for payment",
                'reminder_booking_1h'  => "When tenant has 1 hour left for payment",
                'transaction_payment'  => "When tenant successfully making payment",
                'reminder_unread_chat' => "When tenant has unread chats",
                'recurring_unpaid_invoice_this_day'     => "When tenant invoice expired on this day",
                'recurring_unpaid_invoice_minus_one'    => "When tenant invoice expired less than one day",
                "recurring_unpaid_invoice_plus_one"     => "When tenant invoice expired more than one day",
                'rejected_dbet_tenant_request'          => 'When owner reject contract submission',
                'accepted_dbet_tenant_request'          => 'When owner accept contract submission',
                'accepted_failed_chage_dbet_tenant_request' => 'When owner accept contract submission without revision',
            ],
            'all' => [
                'verification_code_requested' => 'When user request verification code',
            ],
        ];
    }

    /**
     * @param string $userType
     *
     * @return array
     */
    public function getAllEvents(string $userType): array
    {
        return $this->events[$userType];
    }

    /**
     * @param string $userType
     * @param string $key
     *
     * @return string
     */
    public function getSingleEvent(string $userType, string $key): string
    {
        $event      = "";
        $eventGroup = $this->events[$userType];

        if (count($eventGroup) > 0) {
            $event = isset($eventGroup[$key]) ? $eventGroup[$key] : $event;
        }

        return $event;
    }
}