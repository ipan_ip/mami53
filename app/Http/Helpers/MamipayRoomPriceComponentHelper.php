<?php

namespace App\Http\Helpers;

use App\Entities\Room\Room;
use App\Enums\Mamipay\MamipayRoomPriceComponentType;

class MamipayRoomPriceComponentHelper
{
    /**
     * Function to get status active DP
     *
     * @param Room $room
     * @return bool
     */
    public static function getRoomEnableDownPayment(?Room $room): bool
    {
        try {
            if (!$room)
                return false;

            $prices = collect($room->mamipay_price_components);
            $dp = $prices->where('component', MamipayRoomPriceComponentType::DP)->first();

            return ($dp && $dp->is_active ) ? true : false;

        } catch (\Exception $e) {
            return false;
        }
    }
}