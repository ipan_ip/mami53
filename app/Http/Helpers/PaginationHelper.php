<?php

namespace App\Http\Helpers;

use Illuminate\Pagination\LengthAwarePaginator;


class PaginationHelper
{
    /**
     * Create pagination
     * 
     * @param array $collects
     * @param integer $count
     * @param integer $page
     * @param integer $limit
     * 
     * @return array()
    */
    public static function pagination($collects, $count, $page, $limit)
    {
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $offset = ($page - 1) * $limit;
        $currentResults = array_slice($collects, $offset, $limit);
        $results = new LengthAwarePaginator($currentResults, $count, $limit);

        return $results;
    }
}