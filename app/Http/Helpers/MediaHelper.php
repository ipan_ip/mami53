<?php

namespace App\Http\Helpers;
use App\Entities\Media\Media;

use Config;
/**
 *
 */
class MediaHelper
{
    /**
     * Getting size of media (width and height) for specific type of media.
     *
     * @param $type
     * @return mixed
     */
    public static function sizeByType($type)
    {
        if ($type === 'user_photo') {
            return Config::get('api.media.size.user');
        }

        if ($type === 'style_photo') {
            return Config::get('api.media.size.style');
        }

        if ($type === 'round_style_photo') {
            return Config::get('api.media.size.round_style');
        }

        if ($type === 'review_photo') {
            return Config::get('api.media.size.review');
        }

        if ($type == 'photobooth') {
            return Config::get('api.media.size.photobooth');
        }

        if ($type === 'company_photo') {
            return Config::get('api.media.size.company');
        }

        return Config::get('api.media.size.style');
    }

    public static function realSizeByType($type)
    {
        if ($type === 'user_photo') {
            return Config::get('api.media.real_size.user');
        }

        if ($type === 'style_photo') {
            return Config::get('api.media.real_size.style');
        }

        if ($type === 'round_style_photo') {
            return Config::get('api.media.real_size.round_style');
        }

        if ($type === 'review_photo') {
            return Config::get('api.media.real_size.review');
        }

        if ($type == 'photobooth') {
            return Config::get('api.media.real_size.photobooth');
        }

        if ($type === 'company_photo') {
            return Config::get('api.media.real_size.company');
        }

        return Config::get('api.media.real_size.style');
    }

    /**
     * Removing extension of file media.
     *
     * @param $fileName
     * @return mixed
     */
    public static function stripExtension($fileName)
    {
        $fileName = str_replace('.jpg', '', $fileName);
        $fileName = str_replace('.png', '', $fileName);
        $fileName = str_replace('.jpeg', '', $fileName);
        $fileName = str_replace('.gif', '', $fileName);

        return $fileName;
    }

    /**
    * Get Cache URL for media
    *
    * @param Media      Media Model
    * @param integer    width
    * @param integer    height
    *
    * @return string    URL
    */
    public static function getCacheUrl($media, $width, $height)
    {
        return Media::getCacheUrl($media, $width, $height);
    }
}
