<?php

namespace App\Http\Helpers;

use App\Entities\Room\Review;
use Illuminate\Http\JsonResponse;

class RatingHelper
{

    public static function isSupportFiveRating(): bool
    {
        // access from web
        if (!isset(app()->device->app_version_code)) {
            return true;
        }

        // access from mobile with supported version
        $isSupportFiveRating = app()->device->getIsRatingFiveSupported();
        if ($isSupportFiveRating) {
            return true;
        }

        return false;
    }

    public static function getScaleConfig()
    {
        return config('rating.scale');
    }

    public static function fourToFiveStarScale($rating)
    {
        return (float) ($rating * (5/4));
    }

    public static function fiveToFourStarScale($rating)
    {
        return (float) ($rating * (4/5));
    }

    public static function isScaleAllowed($anything)
    {
        return $anything == 4 || $anything == 5;
    }

    /** 
     * Converter rating object from 5 to four
     */
    public static function normalizeRatingBasedOnPlatform(Review $review)
    {
        $cleanliness       = $review->cleanliness;
        $comfort           = $review->comfort;
        $safe              = $review->safe;
        $price             = $review->price;
        $room_facility     = $review->room_facility;
        $public_facility   = $review->public_facility;
        
        if (!self::isSupportFiveRating()) {
            // if not support 5 stars
            if (self::getScaleConfig() == 5) {
                $cleanliness       = (int) self::fiveToFourStarScale($review->cleanliness);
                $comfort           = (int) self::fiveToFourStarScale($review->comfort);
                $safe              = (int) self::fiveToFourStarScale($review->safe);
                $price             = (int) self::fiveToFourStarScale($review->price);
                $room_facility     = (int) self::fiveToFourStarScale($review->room_facility);
                $public_facility   = (int) self::fiveToFourStarScale($review->public_facility);
            } else {
                $cleanliness       = (int) $cleanliness;
                $comfort           = (int) $comfort;
                $safe              = (int) $safe;
                $price             = (int) $price;
                $room_facility     = (int) $room_facility;
                $public_facility   = (int) $public_facility;
            }
        } else {
            // if support 5 stars
            if (self::getScaleConfig() == 4) {
                $cleanliness       = self::fourToFiveStarScale($review->cleanliness);
                $comfort           = self::fourToFiveStarScale($review->comfort);
                $safe              = self::fourToFiveStarScale($review->safe);
                $price             = self::fourToFiveStarScale($review->price);
                $room_facility     = self::fourToFiveStarScale($review->room_facility);
                $public_facility   = self::fourToFiveStarScale($review->public_facility);
            }
            // else if RATING_SCALE == 5 no need type casting
        }

        return (object) [
            'cleanliness'      => $cleanliness,
            'comfort'          => $comfort,
            'safe'             => $safe,
            'price'            => $price,
            'room_facility'    => $room_facility,
            'public_facility'  => $public_facility,
        ];
    }

    /** 
     * Convert float rating to int
     * 
     * @param JsonResponse $jsonResponse
     * @return JsonResponse
     */
    public static function normalizeRatingToInt($jsonResponse): JsonResponse
    {
        $data = $jsonResponse->getData();
        $newDataRoom = [];

        foreach ($data->rooms as $key => $room) {
            $room->rating = (int) $room->rating;
            $newDataRoom[] = $room;
        }

        $data->rooms = $newDataRoom;
        $jsonResponse->setData($data);

        return $jsonResponse;
    }

    /** 
     * Room rating to int
     * 
     * @param array $room
     * @return array
     */
    public static function roomsRatingToInt($rooms): array
    {
        foreach ($rooms as $key => $room) {
            $rooms[$key]['rating'] = (int) $rooms[$key]['rating'];
        }

        return $rooms;
    }

    /**
     * Rating value adjustment
     *
     * @param float
     * @return float
     */
    public static function adjustValue($value)
    {
        return round($value, 1, PHP_ROUND_HALF_UP);
    }

    /** 
     * Adjust average rating based on rating_scale env and request device  
     * @param int|double $rating
     */
    public static function normalizedAvarageRatingBasedOnPlatform($rating)
    {
        if (self::isSupportFiveRating()) {
            if (self::getScaleConfig() == 4) {
                $rating = self::fourToFiveStarScale($rating);
            }
        } else {
            if (self::getScaleConfig() == 5) {
                $rating = self::fiveToFourStarScale($rating);
            }
        }

        return $rating;
    }
}
