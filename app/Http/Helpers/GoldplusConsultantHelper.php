<?php

namespace App\Http\Helpers;

use App\Http\Helpers\StringHelper;
use App\Entities\Room\Room;
use App\Entities\Level\KostLevel;

class GoldplusConsultantHelper
{

    public const DELIMITER_CONFIG_GP_CONSULTANT_IDS = ',';

    /**
     * Get the configured goldplus dedicated consultant user ids.
     *
     * @return array
     */
    public static function getGoldplusConsultantIds(): array
    {
        return StringHelper::explodeAndCastToInt(
            self::DELIMITER_CONFIG_GP_CONSULTANT_IDS,
            config('chat.goldplus.dedicated_consultant_ids')
        );
    }

    /**
     * Get the single goldplus dedicated consultant user id.
     *
     * @throws InvalidArgumentException if the configuration is not set.
     * @return integer
     */
    public static function getRandomGoldplusConsultantId(): int
    {
        return collect(self::getGoldplusConsultantIds())->random();
    }
}
