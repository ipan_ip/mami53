<?php

namespace App\Http\Helpers;

use App\Entities\Room\Room;
use App\Repositories\Booking\BookingStatusChangeRepository;
use App\Repositories\Booking\BookingUserRepository;

class BookingAcceptanceRateHelper 
{

    /**
     * Get booking_acceptance_rate data
     * 
     * @param Room $room
     * @return array
     */
    public static function get(Room $room)
    {
        $data = [
            "active_from"   => null,
            "accepted_rate" => null,
            "average_time"  => null,
        ];

        if (!$room->is_booking) {
            return $data;
        }

        $data['active_from'] = $room->owner_active_since;
        $bar = $room->booking_acceptance_rate;

        if (is_null($bar)) {
            return $data;
        }

        if ((int) $bar->is_active === 0) {
            return $data;
        }

        $data['accepted_rate']  = $bar->rate ?? null;
        $data['average_time']   = self::getAvgTime($bar->average_time);
        return $data;
    }

    /**
     * Method to retrieve acceptance rate of kos
     * 
     * @param \App\Entities\Room\Room $room
     * @return int|float
     */
    public static function getAcceptanceRate(Room $room) {
        $bar = $room->booking_acceptance_rate;

        if (is_null($bar)) {
            return 0;
        }

        return $bar->rate ?? 0;
    }

    /**
     * Calculate Rate Acceptance of each kost
     * 
     * @param int $songId
     * @return int
     */
    public static function calculateRateAcceptance($songId): int
    {
        $bookingStatusRepo = app()->make(BookingStatusChangeRepository::class);

        $totalBooking = $bookingStatusRepo->getTotalBookingBySongId($songId);
        if ($totalBooking === 0) return 0;

        $totalAcceptedBooking = $bookingStatusRepo->getTotalAcceptedBookingBySongId($songId);
        $rateAcceptance = $totalAcceptedBooking / $totalBooking;

        return (int) ($rateAcceptance * 100);
    }

    /**
     * Calculate proccess time of given kos by songId
     * 
     * @param int $songId
     * @return int
     */
    public static function calculateProceessTime($songId): int
    {
        $bookingUserRepo = app()->make(BookingUserRepository::class);

        $result = $bookingUserRepo->getProcessTimeBySongId($songId);
        if (!$result) return 0;

        return self::calculate($result->toArray());
    }

    /**
     * Convert minute to hour
     * 
     * @param int minute
     * @return int
     */
    public static function minuteToHour(int $minute): int
    {
        return (int) $minute / 60;
    }

    private static function calculate($data)
    {
        $n = count($data);
        $total = $counter = 0;

        for ($i=0; $i < $n; $i++) { 
            $row = self::extractEachRow($data[$i]);
            if ($row['valid']) {
                $total = $total + $row['value'];
                $counter++;
            }
        }

        // to avoid division by zero
        if ($counter === 0) return 0;

        return (int) ($total / $counter);
    }

    private static function extractEachRow($row): array
    {
        if (is_null($row['process_time'])) {
            return ["valid" => false, "value" => 0];
        }

        return ["valid" => true, "value" => $row['process_time']];
    }

    private static function getAvgTime($avgTime): ?int
    {
        $minute = isset($avgTime) ? (int) $avgTime : null;
        if ($minute < 1) return null;

        return self::minuteToHour($minute);
    }
}
