<?php namespace App\Http\Helpers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Config;
use DateTime;
use Illuminate\Support\Facades\Validator;
use App\Entities\Config\AppConfig;

class ApiHelper
{
    /*
    |--------------------------------------------------------------------------
    | Api Helpers
    |--------------------------------------------------------------------------
    |
    | Helper for API
    | -> ApiMiddleware
    | Auth
    | Required Header
    | Authorization Header
    | Time Limit Access Token
    | Generate Auth Hash
    | Generate Token
    |
    | -> Response
    | Response
    | Response Data
    | Response Error
    | Exception Handler
    | Auto Logging to Database
    |
    | -> Input Validation
    | Validation Input
    |
    | -> Get User Id
    | Getting User Id (Input User Id)
    | Getting Active User Id (User Token)
    |
    | ->Insert Bulk
    | Model
    | Insert Bulk Generator
    |
    | Array
    | Chunk
    | Usort
    |
    | Pagination
    | Page
    |
    */

    const IV = "4d70ebb03f4a1409";

    /**
     * List of Default Message Response
     *
     * @var array
     * @access protected
     */

    static protected $messageCode = array(
        '0' => array(
            'severity' => 'Success',
            'message'  => 'Request Success',
        ),
        '1' => array(
            'severity' => 'Error',
            'message'  => 'Error Happen',
        ),
        '200' => array(
            'severity' => 'OK',
            'message'  => 'The request has succeeded. An entity corresponding to the requested resource is sent in the response.',
        ),
        '201' => array(
            'severity' => 'CREATED',
            'message'  => 'The request has been fulfilled and resulted in a new resource being created.',
        ),
        '204' => array(
            'severity' => 'NO CONTENT',
            'message'  => 'The server successfully processed the request, but is not returning any content.',
        ),
        '400' => array(
            'severity' => 'INVALID REQUEST',
            'message'  => 'The request could not be understood by the server due to malformed syntax or request.',
        ),
        '404' => array(
            'severity' => 'NOT FOUND',
            'message'  => 'The server has not found anything matching the request.',
        ),
        '500' => array(
            'severity' => 'INTERNAL SERVER ERROR',
            'message'  => 'The server encountered an unexpected condition which prevented it from fulfilling the request.',
        ),
    );

    /**
     * List of available error levels
     *
     * @var array
     * @access protected
     */

    static protected $errorLevels = array(
        E_ERROR             => 'Error', // 1
        E_WARNING           => 'Warning', // 2
        E_PARSE             => 'Parsing Error', // 4
        E_NOTICE            => 'Notice', // 8
        E_CORE_ERROR        => 'Core Error', // 16
        E_CORE_WARNING      => 'Core Warning', // 32
        E_COMPILE_ERROR     => 'Compile Error', // 64
        E_COMPILE_WARNING   => 'Compile Warning', // 128
        E_USER_ERROR        => 'User Error', // 256
        E_USER_WARNING      => 'User Warning', // 512
        E_USER_NOTICE       => 'User Notice',
        E_STRICT            => 'Runtime Notice',
        E_RECOVERABLE_ERROR => 'E_RECOVERABLE_ERROR',
        E_DEPRECATED        => 'E_DEPRECATED',
        E_USER_DEPRECATED   => 'E_USER_DEPRECATED'
    );

    static public function getAuthorizationHeader($softCheck = false)
    {
        try {
            $authData        = explode('GIT ', Request::header('Authorization'));
            $authKeys        = explode(':', $authData[1]);
            $token           = $authKeys[1];
            $clientSignature = $authKeys[0];
        } catch (Exception $e) {
            // Soft determine it's okay to not have valid token. It doesnt make error.
            if ($soft) return null;

            $detail = ApiHelper::traceThrowError($e);

            throw new Exception(Lang::get('api.authorization.authorization_header_invalid') . $detail, 256);
        }

        return array(
            'token'            => $token,
            'client_signature' => $clientSignature
        );
    }

    public static function traceThrowError(Exception $e)
    {
        return '. Error: ' . $e->getMessage() . ', File: ' . $e->getFile() . ', Line: ' . $e->getLine();
    }

    static public function validateTimeLimit()
    {
        $develAccessToken = Config::get('api.devel_access_token');
        $signatureTimeLimit = Config::get('api.signatureTimeLimit');

        if (\Request::input('devel_access_token') == $develAccessToken) {
            return true;
        }

        // CLient Time
        $clientTime  = Request::header('X-GIT-Time');

        // Compare The Client Time with Current Server Time
        $minutesDiff = Carbon::createFromTimeStampUTC($clientTime)->diffInMinutes();

        if ($minutesDiff >= $signatureTimeLimit) {
            throw new Exception(Lang::get('api.authorization.access_token_expired'), 256);
        }

        return true;
    }

    static public function generateSignature()
    {
        $data = Request::method() . " " . Request::path() . " " .
                Request::header('X-GIT-Time');

        $key = Config::get('api.secret_key');

        return hash_hmac('sha256', $data, $key);
    }


    static public function checkToken($authType, $authHeader)
    {
        // Checking Token
        // The Rule is Guest < User < Designer

        if ($authType === 'guest') {
            if (Config::get('api.guest_token') != $authHeader['token'])
            {
                // Check whether this is a device
                $userDevice = UserDevice::where('device_token', '=', $authHeader['token'])->first();

                if ($userDevice != null)
                {
                    $user = new stdClass;
                    $user->role = 'device';
                    Session::put('api.device.data', $userDevice);
                }

                if ($userDevice->user_id != 0)
                {
                    $user = User::find($userDevice->user_id);
                }

            }
            else
            {
                $user = new stdClass;
                $user->role = 'guest';
            }

        } else if ($authType === 'user') {
            $userDevice = UserDevice::where('device_token', '=', $authHeader['token'])->first();

            if ($userDevice != null)
            {
                if ($userDevice->user_id != 0)
                {
                    $user = User::find($userDevice->user_id);
                }
                else
                {
                    $user = new stdClass;
                    $user->role = 'device';
                    Session::put('api.device.data', $userDevice);
                }
            } else {
                $user = null;
            }

        } else if ($authType === 'designer') {
            $user = User::with('designer')->where('role', '=', 'designer')->where('token', '=', $authHeader['token'])->first();
        } else {
            if (Config::get('api.guest_token') == $authHeader['token']) {
                $user = new stdClass;
                $user->role = 'guest';
            } else {
                throw new Exception(Lang::get('api.authorization.token_invalid'), 256);
            }
        }

        if ($user === null) {
            throw new Exception(Lang::get('api.access.forbidden'), 256);
        } else {
            // Summary::store('user',$user->id,'login');
        }

        if ($user->role === 'guest') {
            Session::put('api.user.data', null);
        } else {
            Session::put('api.user.data', $user);
        }

        Session::put('api.user.role', $user->role);
    }

    /**
     * Same with checkToken(), but in this function has no exception as checkToken do, Exception
     * will refuse token if it's not valid. This function is used when we need to determine
     * who is the user behind this request, by checking token. If token seem unknown,
     * then assume he is guest.
     */
    static public function softCheckToken($authType, $authHeader)
    {
        $user = null;
        $userDevice = UserDevice::where('device_token', '=', $authHeader['token'])->first();

        if ($userDevice){
            Session::put('api.device.data', $userDevice);

            if ($userDevice->user_id){
                $user = User::find($userDevice->user_id);
            }
        } else {
            Session::put('api.device.data', null);
        }

        if (!$user){
            $user = new stdClass();
            $user->role = 'guest';
        }

        Session::put('api.user.data', $user);

    }

    /*
     * The difference between Base64 and hex is really just how bytes are represented.
     * Hex is another way of saying "Base16". Hex will take two characters for each byte
     * - Base64 takes 4 characters for every 3 bytes, so it's more efficient than hex.
     * Assuming you're using UTF-8 to encode the XML document, a 100K file will take
     * 200K to encode in hex, or 133K in Base64.
     *
     * Of course it may well be that you don't care about the space efficiency -
     * in many cases it won't matter. If it does matter, then clearly Base64 is better
     * on that front.
     * (There are alternatives which are even more efficient, but they're not as common.)
     */
    public static function generateToken($unique)
    {
        $data = Str::random(10) . date('Y-m-d H:i:s') . $unique;
        $key  = Config::get('app.key');

        return hash_hmac('sha256', $data, $key);
    }

    public static function exceptionHandler($exception, $http_code, $message = false, $code = false)
    {
        if (Config::get('api.http_code.default') !== null) {
            $http_code = Config::get('api.http_code.default');
        }

        $code = $code ? $code : $exception->getCode();

        $severity = $code;
        $severity = ( ! isset(self::$errorLevels[$severity])) ? 'Error' : self::$errorLevels[$severity];

        if ($message == false) {
            $message = $exception->getMessage();
        }

        $debugLevel = Config::get('api.debug_level');

        // Show Message Only
        if ($debugLevel === 1) {
            return ApiHelper::responseError(
                $message, $severity, $http_code, $code
            );
        }

        // Show Enough Debugging
        if (in_array($debugLevel, array(2,3)))  {

            $file = str_replace("\\", "/", $exception->getFile());

            // For security reasons we do not show the full file path
            if (strpos($file, '/') !== false) {
                $x = explode('/', $file);
                $file = $x[count($x)-2].'/'.end($x);
            }

            return ApiHelper::responseError(
                $message, $severity, $http_code,
                $code, $file, $exception->getLine()
            );
        }
    }

    /**
     * Function Variety Use Case
     *
     * responseError('message');
     * responseError('message', 'severity');
     * responseError('message', 'severity', 200, 1);
     * responseError(false, false, false, 1);
     * responseError(false, false, false, 1, 'file', 'line');
     * responseError(false, false, false, 1, 'file', 'line', true);
     * responseError(false, false, false, 1, false, false, true);
     */

    static public function responseError($message = false, $severity = false, $http_code = false,
            $error_code = false, $file = false, $line = false, $debug = false)
    {
        if (($error_code !== false) && ($severity === false)) {
            $severity = self::$messageCode[$error_code]['severity'];
            $message  = self::$messageCode[$error_code]['message'];
        } else {
            $error_code = $error_code ? $error_code : 1;
            $severity   = $severity ? $severity : self::$messageCode[$error_code]['severity'];
            $message    = $message ? $message : self::$messageCode[$error_code]['message'];
        }

        $response = array(
            'status' => false,
            'meta'   => array(
                'response_code' => $http_code,
                'code'          => $error_code,
                'severity'      => $severity,
                'message'       => $message
            )
        );

        if ($file !== false) {
            $response['meta']['file'] = $file;
            $response['meta']['line'] = $line;
        }

        return self::response($response, $http_code, $debug);
    }

    /**
     * Create an API response
     *
     * responseData(array());
     * responseData(array(), true, 200);
     * responseData(array(), true, 200, 'message');
     * responseData(array(), true, 200, 'message', 'severity');
     * responseData(array(), true, 200, null, null, 1);
     * responseData(array(), true, 200, null, 'severity', 1, true);
     * responseData(array(), true, 200, null, null, null, true);
     *
     */

    static public function responseData($data = array(), $counData = false , $status = true, $http_code = 200,
            $message = null, $severity = null, $status_code = null, $debug = false)
    {
        if (($status_code !== null) && ($severity === null)) {
            $severity = self::$messageCode[$status_code]['severity'];
            $message  = self::$messageCode[$status_code]['message'];
        } else {
            $status_code = $status_code ? $status_code : 200;
            $severity    = $severity ? $severity : self::$messageCode[$status_code]['severity'];
            $message     = $message ? $message : self::$messageCode[$status_code]['message'];
        }

        if ($counData == true) {
            $response = array(
                'status' => $status,
                'meta'   => array(
                    'response_code' => $http_code,
                    'code'          => $status_code,
                    'severity'      => $severity,
                    'message'       => $message
                ),
                'counts' => count($data['stories']),
            );
        } else {
            $response = array(
                'status' => $status,
                'meta'   => array(
                    'response_code' => $http_code,
                    'code'          => $status_code,
                    'severity'      => $severity,
                    'message'       => $message
                ),
            );
        }

        foreach ($data as $key => $value) {
            if ($key == 'meta') {
                $response[$key] = array_merge($response[$key], $value);
            } else {
                $response[$key] = $value;
            }
        }

        return self::response($response, $http_code, $debug);
    }

    /**
     *  Return a validation error API response
     * 
     *  Convert errors to API validation error response
     * 
     *  @param string $errors Error message string
     */
    static public function validationError(string $error) {
        return self::responseData(['message' => $error], false, false, 200, 'A validation error occurred');
    }

    static public function response($data = array(), $http_code = null, $debug = null)
    {
        $newMeta = array();

        if ((Session::get('api.log.enable') && Config::get('api.log.enable')) ||
                (((\Request::input('log') === 'enable') &&
                (\Request::input('devel_access_token') === \Config::get('api.devel_access_token'))))) {

            $newMeta = array(
                'response_time' => round(microtime(true) - Session::get('api.log.request_time'), 3) * 1000 . ' ms',
            );
        }

        // Merging the Previous Meta
        // To Put Response Time to The Top, that's why using Array Merge
        if (array_key_exists('meta', $data)) {
            $meta['meta'] = array_merge($newMeta, $data['meta']);
        } else {
            $meta['meta'] = $newMeta;
        }

        // Debugbar
        // Since Optional, put it at the bottom
        if (($debug) ||
                (\Config::get('api.debug_level') == 3) ||
                ((\Request::input('debugbar') === 'enable') &&
                (\Request::input('devel_access_token') === Config::get('api.devel_access_token')))) {

            Debugbar::enable();
            $meta['meta']['debugbar'] = Debugbar::collect();
        }

        $response = array(
            'status'   => $data['status'],
            'meta'     => $meta['meta']
        );

        // Merge Data except Meta and Status
        foreach ($data as $key => $value) {
            if (! in_array($key, array('status', 'meta'))) {
                $response[$key] = $value;
            }
        }

        if ((Session::get('api.log.enable') && Config::get('api.log.enable')) ||
                (((\Request::input('log') === 'enable') &&
                (\Request::input('devel_access_token') === \Config::get('api.devel_access_token'))))) {

            // self::dbLog($response);
        }

        if (Session::get('api.v2.enabled') == true) {
            $message = json_encode($response);

            $encrypted = self::encrypt($message);

            $data = array( 'data' => $encrypted );
        } else {
            $data = $response;
        }

        return \Response::json($data, $http_code);
    }

    static public function decryptRequest()
    {
        // $mcrypt = new MCrypt();

        $data = \Request::input('data');

        // $decrypt = $mcrypt->decrypt($data);

        // $input = json_decode($decrypt,true);
        $input = self::decrypt($data);

        foreach ($input as $i => $d) {
            Input::merge(array($i => $d));
        }
    }

    static public function encrypt($message)
    {
        return OpenSslEncryptor::encrypt($message, Config::get('api.api_secret_key'), self::IV);
    }

    static public function decrypt($message)
    {
        return OpenSslEncryptor::decryptIntoArray($message,  Config::get('api.api_secret_key'), self::IV);
    }

    /**
     * @deprecated not being used anywhere
     */
    static public function dbLog($response = '{}')
    {
        $postParam = null;
        $getParam  = null;
        $header    = null;

        if (in_array(Request::method(), array('POST', 'PUT'))) {
            // First we fetch the Request instance
            $request = Request::instance();

            // Now we can get the content from it
            $post = $request->getContent();

            // decode and encode to prettify the input, remove whitespace, etc
            $postParam = json_encode(json_decode($post));
        }

        if (Input::query() !== null) {
            $inputQuery = Input::query();
            unset($inputQuery["devel_access_token"]);
            unset($inputQuery["cron_access_token"]);
            unset($inputQuery["log"]);
            unset($inputQuery["debugbar"]);

            $getParam = json_encode($inputQuery);
        }

        $header = array(
            'authorization' => Request::header('Authorization'),
            'x-git-time'    => Request::header('X-GIT-Time'),
        );

        if (Session::get('api.user.role') === 'guest') {
            $activeDevice = self::activeDevice();

            $identifier     = $activeDevice['id'];
            $identifierType = $activeDevice['id_type'];
        } else {
            $identifier     = self::activeUserId();
            $identifierType = 'user_id';
        }

        $dataLog = array(
            // Originator The Request
            'identifier'      => $identifier,
            'identifier_type' => $identifierType,

            // Request
            'method'          => Request::method(),
            'resource'        => Request::path(),
            'ip_address'      => Request::getClientIp(),
            'header'          => json_encode($header),
            'post_param'      => $postParam,
            'get_param'       => $getParam,

            // Response
            'response'        => json_encode($response),
            'response_time'   => rtrim($response['meta']['response_time'], ' ms'),
            'response_status' => $response['status'] ? 'true' : 'false',
            'created_at'      => date('Y-m-d H:i:s')
        );

        DB::table('log')->insertGetId($dataLog);


        try {
          self::summaryLog($dataLog);
        } catch (Exception $e) {

        }

        return true;
    }

    public static function summaryLog($dataLog)
    {
        /*
         * Loging the User at first
         *---------------------*/
        $resource = str_replace('api/v1/','',$dataLog['resource']);

        $method      = $dataLog['method'];
        $uri_segment = explode('/', $resource);

        /*
         * Product Loging
         *---------------------*/


        if (isset($uri_segment[4])) {
            if ($uri_segment[4] == 'comments' && $method == 'POST') {
                Summary::store('card', $uri_segment[3] , 'comment');
            }
        }

        if ($uri_segment[0] == 'stories')
        {
            if (isset($uri_segment[2]) ) {
                $id      = $uri_segment[1];
                $story   = Designer::find($id);

                if ($uri_segment[2] == 'love') {
                    Summary::store('story',$id,'like');
                } else if ($uri_segment[2] == 'view') {
                    Summary::store('story',$id,'mobile_view');
                }
                else if ($uri_segment[2] == 'share')
                {
                    Summary::store('story',$id,'share');
                }
            }
        }

        if ($uri_segment[0] == 'contents') {
            if (isset($uri_segment[1]) && (! array_key_exists(2, $uri_segment))) {
                $id      = $uri_segment[1];
                Summary::store('story',$id,'web_view');
            }
        }
    }


    public static function userId()
    {
        // user_id is used for getting friends user profile,
        // if not exist, then its means getting the user profile by user_token

        $user_id = \Request::input('user_id');

        if ($user_id === null) self::activeUserId();

        return $user_id;
    }

    public static function activeUserId()
    {
        // getting the user originate the request
        // [user_id] by User Token (not from input user_id)

        $user = Session::get('api.user.data');

        try {
          $user_id = $user->id;
        } catch (Exception $e) {
          $user_id = null;
        }

        return $user_id;
    }

    public static function activeDeviceId()
    {
        $device = app()->device;

        if ($device == null) return null;

        return $device->id;
        $user_device = Session::get('api.device.data');

        try {
          $user_device_id = $user_device->id;
        } catch (Exception $e) {
          $user_device_id = null;
        }
        return $user_device_id;
    }

    /**
     * @deprecated not used anywhere, class UserDevice not exist
     */
    public static function activeDevice()
    {
        $userDevice = UserDevice::where('identifier', '=', \Request::input('device_identifier'))
                        ->where('platform', '=', \Request::input('device_platform'))
                        ->where('uuid','=',\Request::input('uuid'))
                        ->first();

        if ($userDevice !== null) {
            return array(
                'id'      => $userDevice->id,
                'id_type' => 'device_id',
            );
        } else {
            if (\Request::input('device_platform') === 'android') {
                return array(
                    'id'      => \Request::input('device_identifier'),
                    'id_type' => 'android_id',
                );
            } else if (\Request::input('device_platform') === 'ios') {
                return array(
                    'id'      => \Request::input('device_identifier'),
                    'id_type' => 'ios_id',
                );
            }
        }
    }

    /**
     * Just to Consider
     * There is Security Open in This SQL,
     * because the sql will be directly put into DB::insert(DB::raw(sql));
     * find better way to enabling more secure way
     * convert to be like this instead
     * DB::select( DB::raw("SELECT  * FROM some_table WHERE some_col = :somevariable"), array(
     *   'somevariable' => $someVariable,
     * ));
     */
    public static function generateInsertIgnoreBulk($table, $data)
    {
        $values = array();
        $statement = "INSERT IGNORE INTO {$table} ";

        if (is_array($data)) {
            if (isset($data[0])) {
                $fields = implode(', ', array_keys($data[0]));
                $fields = "( {$fields} )";

                foreach ($data as $v1) {
                    foreach ($v1 as $k2 => $v2) {
                        $v1[$k2] = "\"{$v2}\"";
                    }
                    $value = implode(', ', $v1);
                    $values[] = "( {$value} )";
                }

                $values = implode(', ', $values);
                return $query = "{$statement} {$fields} VALUES {$values};";
            }
        }

        return false;
    }

    // will output multi array even only one group
    public static function arrayChunk($ids, $chunk_count = null, $first_chunk = false)
    {
        $chunkCount = ($chunk_count === null) ? 1000 : $chunk_count;

        $groupIds = array_chunk( $ids, $chunkCount );

        if ($first_chunk)
        {
            // remove other index array
            // to enabling foreach outside this function
            $firstGroupIds[] = $groupIds[0];
            return $firstGroupIds;
        }
        return $groupIds;
    }

    /**
     * Sort Array or Object by Key
     */

    public static function sortByKey($data, $key, $type = 'array', $sort = 'ASC')
    {
        $tempData = $data;

        usort($tempData, function ($left, $right) use ($key, $type, $sort)
        {
            if ($type === 'array')
            {
                $leftKey  = $left[$key];
                $rightKey = $right[$key];
            }
            else if ($type === 'object')
            {
                $leftKey  = $left->$key;
                $rightKey = $right->$key;
            }

            if ($leftKey == $rightKey)
            {
                return 0;
            }

            if (strtoupper($sort) === 'ASC')
            {
                return ($leftKey < $rightKey) ? -1 : 1;
            }
            else
            {
                return ($leftKey > $rightKey) ? -1 : 1;
            }
        });

        return $tempData;
    }

    public static function isValidInput($input, $rule)
    {
        $validator = Validator::make($input, $rule);
        if ($validator->fails()) {
            $message        = json_decode($validator->messages());
            $format_message = 'Input: Validation Error. ';
            $divider        = ' ';
            foreach ($message as $key => $value) {
                $arrMsg          = $value;
                $msg             = implode(', ', $arrMsg);
                $format_message .= $key . ' : ' . $msg . $divider;
            }
            throw new Exception($format_message, 256);
        }
        return true;
    }

    public static function validateInput($validator)
    {
        if ($validator->fails())
        {
            $message        = json_decode($validator->messages());
            $format_message = 'Input: Validation Error. ';
            $divider        = ' ';

            foreach ($message as $key => $value)
            {
                $arrMsg          = $value;
                $msg             = implode(', ', $arrMsg);
                $format_message .= $key . ' : ' . $msg . $divider;
            }
            throw new Exception($format_message, 256);
        }
        return true;
    }

    public static function inputPagination($default_limit = 9, $default_offset = 0, $default_order = 'desc')
    {
        ApiHelper::isValidInput(
            Input::all(),
            [   'limit'  => 'numeric',
                  'order'  => 'in:DESC,ASC,asc,desc',
                  'offset' => 'numeric' ]
            );

        $array_pagination   = Input::only('limit','offset','order');
        extract($array_pagination);

        if (!isset($order)  || $order == null)  $order   = $default_order;
        if (!isset($limit)  || $limit == null)  $limit   = $default_limit;
        if (!isset($offset) || $offset == null) $offset  = $default_offset;

        if ($limit > 20) {
            $limit = 20;
        }

        if ($offset > 150) {
            $offset = 150;
        }


        $dataPagination = compact('limit','order','offset');

        return $dataPagination;
    }

    public static function forcePagination($limit = 9, $offset = 0, $order = 'desc')
    {
        return compact('limit','offset','order');
    }

    /**
     * CodeIgniter String Helpers
     *
     * @package   CodeIgniter
     * @subpackage  Helpers
     * @category  Helpers
     * @author    ExpressionEngine Dev Team
     * @link    http://codeigniter.com/user_guide/helpers/string_helper.html
     */

    // ------------------------------------------------------------------------

    /**
     * Create a Random String
     *
     * Useful for generating passwords or hashes.
     *
     * @access  public
     * @param string  type of random string.
     * basic, alpha, alunum, numeric, nozero, unique, md5, encrypt and sha1
     * @param integer number of characters
     * @return  string
     */

    public static function random_string($type = 'alnum', $len = 8)
    {
        switch($type)
        {
            case 'basic'  : return mt_rand();
                break;
            case 'alnum'  :
            case 'lowalnum' :
            case 'ucalnum'  :
            case 'numeric'  :
            case 'nozero' :
            case 'alpha'  :

            switch ($type)
            {
                case 'alpha'  : $pool = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                break;
                case 'alnum'  : $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                break;
                case 'lowalnum'  : $pool = '0123456789abcdefghijklmnopqrstuvwxy';
                break;
                case 'ucalnum': $pool = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                break;
                case 'numeric'  : $pool = '0123456789';
                break;
                case 'nozero' : $pool = '123456789';
                break;
            }

            $str = '';
            for ($i=0; $i < $len; $i++)
            {
                $str .= substr($pool, mt_rand(0, strlen($pool) -1), 1);
            }
            return $str;
            break;

            case 'easy' :
                return self::easy_random_string(rand(0,9), $len);
            break;
            case 'unique' :
            case 'md5'    :
                return md5(uniqid(mt_rand()));
            break;
        }
    }

    public static function easy_random_string($id,$minLength=4,$isJustNumber=false){
        $dateTime=new DateTime('now');
        if (strlen($id)){
            $id='0'.$id;
        }
        if ($id>100){
            $id=$id % 100;
        }
        if ($isJustNumber){
            $keyList=array('5 6 4 7 3 8 2 9 1 0');
        } else {
            $keyList=array(
                  '7 6 S 5 A 1 R 0 2 8',
                  '5 L T I A M 1 N 4 H',
                  '8 J K L Y T 7 9 K 2',
                  'Q W E R 1 L P U S G',
                  'O X C V 5 B D E F S',
              );
              $keyList=array($keyList[array_rand($keyList,1)]);
        }

        $id = substr($id.$dateTime->format('isHdmy'),0,$minLength);
        $key = $keyList[rand(0, 0)];

        $keySplit = explode(' ', $key);
        $result = "";

        for($i=0;$i<strlen($id);$i++){
            $substr=  substr($id, $i, 1);
            $result.=$keySplit[$substr];
        }
        return $result;
    }

    public static function randomArray(array $array,$limited=null)
    {
        if (count($array) == 0) return array();
        if ($limited!=null){
            $rand_keys = array_rand($array, $limited);
            if ($limited==1){
                return $result[]=$array[$rand_keys];
            }
        } else {
            $rand_keys = array_rand($array);
        }
        $result=array();
        if (!is_array($rand_keys)) return $array;
        foreach($rand_keys as $key){
          $result[]=$array[$key];
        }
        return $result;
    }

    public static function xmlStringToArray($xmlString, $type = 1)
    {
        $xml = simplexml_load_string($xmlString);
        return self::xmlObjectToarray($xml, $type);
    }

    public static function xmlObjectToarray($xml, $type = 1)
    {
        switch ($type)
        {
            /**
            * One Producing Multi Array Even for One
            */
            case '1':
                $arr = array();

                foreach ($xml->children() as $r) {
                    $t = array();
                    if (count($r->children()) == 0) {
                        $arr[$r->getName()] = strval($r);
                    } else {
                        $arr[$r->getName()][] = self::xmlObjectToarray($r);
                    }
                }

                return $arr;
            break;

            /**
             * Two and Three is Producing the Same Output
             */

            case '2':
                $arr = json_decode(json_encode($xml), 1);
                return $arr;
                break;

            case '3':
                $out = array();
                foreach ( (array) $xml as $index => $node ) {
                    $out[$index] = ( is_object ( $node ) ) ? self::xmlObjectToarray ( $node ) : $node;
                }
                return $out;
                break;
        }
    }

    /**
    * Batch String Replace
    */

    public static function batchStrReplace($tpl, $arr_replace)
    {
        $string = $tpl;

        foreach ($arr_replace as $key => $value) {
            $string = str_replace($key, $value, $string);
        }

        return $string;
    }

    /**
     * Read JSON File
     */
    public static function readJson($filename , $filter = false)
    {
        $string = file_get_contents(public_path() . '/assets/main/js/json/' . $filename );

        if ($filter) {
            return $string;
        } else {
            echo $string;
            exit;
        }
    }

    public static function removeQuote( $string )
    {
        return str_replace("'","",str_replace('"','',$string));
    }

    public static function cleanHttpChecking($input)
    {
        if(trim($input) == '') {
            return trim($input);
        } elseif(strpos($input, 'bang.kerupux.com://') !== false) {
            return $input;
        } elseif (strpos($input, 'http://') !== false) {
            return $input;
        } elseif (strpos($input, 'https://') !== false) {
            return $input;
        }

        return "http://".$input;
    }

    public static function removeEmoji($text) {

        $clean_text = "";

        // Match Emoticons
        $regexEmoticons = '/[\x{1F600}-\x{1F64F}]/u';
        $clean_text = preg_replace($regexEmoticons, '', $text);

        // Match Miscellaneous Symbols and Pictographs
        $regexSymbols = '/[\x{1F300}-\x{1F5FF}]/u';
        $clean_text = preg_replace($regexSymbols, '', $clean_text);

        // Match Transport And Map Symbols
        $regexTransport = '/[\x{1F680}-\x{1F6FF}]/u';
        $clean_text = preg_replace($regexTransport, '', $clean_text);

        // Supplemental Symbols and Pictographs
        $regexSuplemental = '/[\x{1F900}-\x{1F9FF}]/u';
        $clean_text = preg_replace($regexSuplemental, '', $text);

        // Match Miscellaneous Symbols
        $regexMisc = '/[\x{2600}-\x{26FF}]/u';
        $clean_text = preg_replace($regexMisc, '', $clean_text);

        // Match Dingbats
        $regexDingbats = '/[\x{2700}-\x{27BF}]/u';
        $clean_text = preg_replace($regexDingbats, '', $clean_text);

        return $clean_text;
    }

    /**
     * This function is deprecated (2018-01-18)
     * Use App\Libraries\AreaFormatted instead
     *
     */
    public static function sanitizeCityOrDistrictName($word)
    {
        $areaType = [
            'Kabupaten' => "",
            'Kota'      => "",
            'Kecamatan' => "",
            'Kec.'
        ];

        $dataAnswer=preg_split("[ ]", $word);

        $dataAnswerCount = count($dataAnswer);
        for ($i=0; $i < $dataAnswerCount; $i++) {

            if (array_key_exists($dataAnswer[$i], $areaType)) { 
                $dataAnswer[$i] = $areaType[$dataAnswer[$i]]; 
            }

            $fin[] = $dataAnswer[$i];  
        }

        $result = trim(implode(' ', $fin));
        
        return $result;
    }

    public static function getDummyImageList($name = null)
    {
        $dummyData = self::getDummyImage();

        return $dummyData['photo_url'];
    }

    public static function getDummyImage($name = null)
    {
        $baseUrl = Config::get('app.url');
        $imagePath = '/assets/dummy-photo.png';

        $dummyData = [
            "description" => $name,
            "photo_url" => [
                        'real' => $baseUrl . $imagePath,
                        'small' => $baseUrl . $imagePath,
                        'medium' => $baseUrl . $imagePath,
                        'large' => $baseUrl . $imagePath,
                    ],
            "url" => [
                        'real' => $baseUrl . $imagePath,
                        'small' => $baseUrl . $imagePath,
                        'medium' => $baseUrl . $imagePath,
                        'large' => $baseUrl . $imagePath,
                    ]
        ];

        return $dummyData;
    }

    public static function getDummyImageUser()
    {
        $baseUrl = Config::get('app.url');
        $random = rand(1,3);
        $imagePath = '/assets/icons/forum/user_0' . $random . '.jpg';

        $dummyData = [
            'real' => $baseUrl . $imagePath,
            'small' => $baseUrl . $imagePath,
            'medium' => $baseUrl . $imagePath,
            'large' => $baseUrl . $imagePath
        ];

        return $dummyData;
    }

}
