<?php

namespace App\Http\Helpers;

use App\Entities\Api\ErrorCode;
use Exception;

class ApiErrorHelper
{
    public const ERROR_CODE_NOT_AVAILABLE = 'Error code not available';
    public const ERROR_MESSAGE_NOT_AVAILABLE = 'Error message not available';

    /**
     * Helper to set issue props
     * 
     * @param int $code
     * @param array $details
     * 
     * @return array
     */
    public static function generateErrorResponse(
        int $code,
        array $details = null
    ): array {
        if (!ErrorCode::hasValue($code)) {
            $code = ErrorCode::FALLBACK_CODE;
        }

        $issue = [
            'code' => $code,
            'message' => ErrorCode::MESSAGE_MAP[$code],
        ];

        if (!is_null($details) && is_array($details)) {
            $issue['details'] = $details;
        }

        return [
            'status' => false,
            'issue' => $issue
        ];
    }
}