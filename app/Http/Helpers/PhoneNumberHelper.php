<?php

namespace App\Http\Helpers;

class PhoneNumberHelper
{
	const COUNTRY_CODE = '62';
	const COUNTRY_CODE_PLUS = '+62';
	const COUNTRY_CODE_ZERO = '0';

	/**
	 * @param string $number
	 *
	 * @return string
	 */
	public static function localize(string $number)
	{
		$number = self::sanitizeNumber($number);

		$number = self::COUNTRY_CODE_ZERO . $number;

		return $number;
	}

	/**
	 * @param string $number
	 * @param bool $withPlusSign
	 *
	 * @return string
	 */
	public static function internationalize(string $number, bool $withPlusSign = false)
	{
		$number = self::sanitizeNumber($number);

		if ($withPlusSign)
		{
			$number = self::COUNTRY_CODE_PLUS . $number;
		}
		else
		{
			$number = self::COUNTRY_CODE . $number;
		}

		return $number;
	}

	/**
	 * @param string $number
	 *
	 * @return string
	 */
	public static function sanitizeNumber(string $number)
	{
		$number = self::removeNonNumeric($number);

		if (substr($number, 0, 1) === self::COUNTRY_CODE_ZERO)
		{
			$number = ltrim($number, self::COUNTRY_CODE_ZERO);
		}

		if (substr($number, 0, 2) === self::COUNTRY_CODE)
		{
			$number = ltrim($number, self::COUNTRY_CODE);
		}

		return $number;
	}

	/**
	 * @param string $number
	 *
	 * @return string|string[]|null
	 */
	private static function removeNonNumeric(string $number)
	{
		return preg_replace(RegexHelper::numericOnly(), "", $number);
	}
}