<?php

namespace App\Http\Helpers;
use App\Entities\Activity\Tracking;
use App\Entities\Activity\TrackingPlatforms;
use App\Entities\Booking\BookingDiscount;
use App\Entities\Booking\BookingUser;
use App\Entities\Level\KostLevel;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Room\Element\TagMaxRenter;
use App\Entities\Room\Room;
use App\Entities\Activity\Call;
use App\Entities\Activity\ChatAdmin;
use App\Entities\Activity\Question;
use App\Entities\Consultant\Consultant;
use App\Entities\Room\Element\Price;
use App\Jobs\Booking\BookingSendBirdAutoChatQueue;
use App\User;
use Carbon\Carbon;
use Bugsnag;
use Exception;
use SendBird;
use App\Http\Helpers\GoldplusPermissionHelper;
use App\Enums\Goldplus\Permission\GoldplusPermission;

class BookingUserHelper
{
    // DATA GOLD PLUS LEVEL NAME FROM PRODUCTION
    // It's hard coded because on kost level we didn't have other flag
    const PREBOOK_KOST_LEVEL = [
        'mamikos goldplus 1',
        'mamikos goldplus 2',
        'mamikos goldplus 3'
    ];

    // platform interface for MoEngage
    const INTERFACE_DESKTOP = 'desktop';
    const INTERFACE_MOBILE  = 'mobile';
    const INTERFACE_ANDROID = 'mobile-android';
    const INTERFACE_IOS     = 'mobile-ios';

    // get field price designer
    const FIELD_PRICE = [
        'monthly'       => 'price_monthly',
        'quarterly'     => 'price_quarterly',
        'semiannually'  => 'price_semiannually',
        'annually'      => 'price_yearly',
        'weekly'        => 'price_weekly',
        'daily'         => 'price_daily'
    ];

    public static function getOriginalPriceAtBooking($booking)
    {

        $room = $booking->booking_designer->room;

        $durationUnit = $booking->rent_count_type;        

        $createDate = $booking->contract ? $booking->updated_at : $booking->created_at;
        
        $roomPriceLastRevision = self::getRoomPriceAtEntityCreated($room,$createDate,$durationUnit);

        $discount = self::getDiscount($room,$createDate,$durationUnit);

        if (!empty($discount['ori_price'])) {
            return $discount['ori_price'];
        }

        $valueToGet = 'new_value';
        if (!empty($discount['has_discount'])) {
            $valueToGet = 'old_value';
        }

        $priceType = 'price_'.$durationUnit;
        return $roomPriceLastRevision ? $roomPriceLastRevision->{$valueToGet} : $room->{$priceType};
    }

    public static function getDiscount($room,$discountDate,$rentType)
    {
        $rentCountType = $rentType == 'yearly' ? 'annually' : $rentType;
        $discount = $room->active_discounts
            ->where('price_type', $rentCountType)
            ->where('created_at', '<=', $discountDate)
            ->first();

        $discountData = [];
        //check revision to get exact with time created
        if ($discount) {
            
            $revisionDiscount = $discount->revisionHistory()->where('key','price')->where('created_at','<=',$discountDate)->orderBy('id','desc')->first();
            $discountData = array(
                'ori_price' => $revisionDiscount ? $revisionDiscount->new_value : false,
                'has_discount' => true
            );
        }

        return $discountData;
    }

    public static function getRoomPriceAtEntityCreated($room,$entityCreateDate,$priceType)
    {
        if (!in_array($priceType, Price::TYPES)) {
            return;
        }
        $priceChangeHistory = $room->revisionHistory()->where('key','price_'.$priceType)->where('created_at','<=',$entityCreateDate)->orderBy('id','desc')->first();
        return $priceChangeHistory ? $priceChangeHistory : false;
    }

    public static function getCheckoutDate($rentCountType, $checkInDate, $duration)
    {
        switch ($rentCountType) {
            case "weekly":
                return Carbon::parse($checkInDate)->addWeeks($duration)->format('Y-m-d');
            case "quarterly":
                return Carbon::parse($checkInDate)->addQuarters($duration)->format('Y-m-d');
            case "semiannually":
                return Carbon::parse($checkInDate)->addMonths($duration * 6)->format('Y-m-d');
            case "yearly":
                return  Carbon::parse($checkInDate)->addYear($duration)->format('Y-m-d');
            default:
                return Carbon::parse($checkInDate)->addMonths($duration)->format('Y-m-d');
        }
    }

    public static function isPremiumOwner($owner): bool
    {
        return
            !is_null($owner)
            && !is_null($owner->user->date_owner_limit)
            && date('Y-m-d') <= $owner->user->date_owner_limit;
    }

    /**
     * Checking kost / room level
     * only level gold plus 1, gold plus 2, and gold plus 3
     * we will return true
     *
     * @param Room $room
     * @return bool
     */
    public static function isKostLevelGoldPlus(Room $room): bool
    {
        // get relation to kost level
        $level = $room->level();

        // check kost have level
        $isKostLevelGP = false;
        if ($level->count() > 0) {
            // get level name
            $kostLevelIds = $level->pluck('id')->toArray();
            $goldPlusIds = array_merge(
                KostLevel::getGoldplusLevelIds(),
                KostLevel::getNewGoldplusLevelIds()
            );

            if (!empty($kostLevelIds) && !empty($goldPlusIds)) {
                $matchingLevel = array_intersect($kostLevelIds, $goldPlusIds);

                $isKostLevelGP = !empty($matchingLevel) ? true : false;
            }
        }

        return $isKostLevelGP;
    }

    /**
     * Getting data max month checking tenant
     * if owner is premium and kost / room have level gold plus 1, gold plus 2, and gold plus 3
     * preebook will active on this kost / room
     *
     * @param Room $room
     * @return int
     */
    public static function maxMonthCheckInPrebook(Room $room): int
    {
        // default max checkin is 2 month
        $maxMonthCheckIn = config('booking.prebook_checkin_normal_month');

        // if config prebook active will check this
        if ( config('booking.prebook_premium_active') ) {

            // check is room mamirooms always show maximum of month check-in
            if ($room->is_mamirooms) {
                return config('booking.prebook_checkin_max_month');
            }

            $roomOwner = optional($room)->verified_owner;

            // check is owner premium and kost level is gold plus
            if (BookingUserHelper::isPremiumOwner($roomOwner) && BookingUserHelper::isKostLevelGoldPlus($room)) {
                $totalPrebookRoom = BookingUser::totalOfPreebook($room, $maxMonthCheckIn);
                if ($totalPrebookRoom < config('booking.prebook_premium_max'))
                    $maxMonthCheckIn = config('booking.prebook_checkin_max_month');
            }
        } else {
            $maxMonthCheckIn = config('booking.prebook_checkin_max_month');
        }
        return $maxMonthCheckIn;
    }

    public static function getPrice($price, $format, $formated = false)
    {
        switch ($format) {
            case 'daily':
                return $price->priceTitleFormats(0, $formated);
                break;
            case 'weekly':
                return $price->priceTitleFormats(1, $formated);
                break;
            case 'monthly':
                return $price->priceTitleFormats(2, $formated);
                break;
            case 'quarterly':
                return $price->priceTitleFormats(4, $formated);
                break;
            case 'semiannually':
                return $price->priceTitleFormats(5, $formated);
                break;
            case 'yearly':
                return $price->priceTitleFormats(3, $formated);
                break;
            default:
                return $price->priceTitleFormats(2, $formated);
                break;
        }
    }

    public static function isBookingWithCalendar(?User $user): bool
    {
        $response = false;
        try{
            // check if user not null
            if(config('booking.is_booking_with_calendar')){
                $response = true;
            }
        } catch(Exception $e){
            Bugsnag::notifyException(new Exception($e->getMessage()));
        }

        return $response;
    }

    /**
     * Function form mapping get platform function
     * to return with requirements team data
     *
     * @return string|null
     */
    public static function getInterfaceForMoEngage(): ?string
    {
        // get platform
        $platform = Tracking::getPlatform();
        $interface = null;

        // mapping return to requirements team data
        switch ($platform) {
            case TrackingPlatforms::WebDesktop:
                $interface = self::INTERFACE_DESKTOP;
                break;
            case TrackingPlatforms::WebMobile:
                $interface = self::INTERFACE_MOBILE;
                break;
            case TrackingPlatforms::Android:
                $interface = self::INTERFACE_ANDROID;
                break;
            case TrackingPlatforms::IOS:
                $interface = self::INTERFACE_IOS;
                break;
            default:
                break;
        }
        return $interface;
    }

    public static function getOriginalPrice($room, $rentType): int
    {
        $price = 0;
        try {
            $roomPrice  = $room->price();
            $price      = $roomPrice->getMarkupPrice($rentType);
            if ($price == 0 || is_null($price)) {
                $field = self::FIELD_PRICE[$rentType];
                $price = $room->{$field} ?? 0;
            }

        } catch (\Exception $e) {
            $price = 0;
        }
        return (int) $price;
    }

    /**
     * get max renter by tag ids
     * @param array $tagIds
     * @return array
     */
    public static function getMaxRenterByTagIds(array $tagIds = []): array
    {
        // define default output
        $name   = '';
        $max    = 1;
        try {
            if (!empty($tagIds)) {
                $tagMaxRenter = TagMaxRenter::with('tag')
                    ->whereIn('tag_id', $tagIds)
                    ->orderBy('max_renter', 'DESC')
                    ->first();

                if (!$tagMaxRenter)
                    throw new Exception('Max renter not found');

                // get tag
                $tag = optional($tagMaxRenter)->tag;

                // get tag name
                if (optional($tag)->name !== null)
                    $name = optional($tag)->name;

                // get value max renter
                if (isset($tagMaxRenter->max_renter))
                    $max = $tagMaxRenter->max_renter ?? 1;
            }

        } catch (Exception $e) {
            $name   = '';
            $max    = 1;
        }

        // response
        return [
            'name'  => $name,
            'max'   => (int) $max
        ];
    }

}