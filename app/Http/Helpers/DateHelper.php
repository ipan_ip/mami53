<?php

namespace App\Http\Helpers;

class DateHelper
{
    const MONTHS = [
        1 => 'Januari',
        2 => 'Februari',
        3 => 'Maret',
        4 => 'April',
        5 => 'Mei',
        6 => 'Juni',
        7 => 'Juli',
        8 => 'Agustus',
        9 => 'September',
        10 => 'Oktober',
        11 => 'November',
        12 => 'Desember'
    ];

    /**
     * @param int $month
     * @return String
     */
    public static function monthIdnFormated(int $month)
    {
        $month = intval($month);
        if (isset(self::MONTHS[$month]))
            return self::MONTHS[$month];

        return null;
    }

    /**
     * Calculate the difference in months between two dates (v1 / 18.11.2013)
     * ref : https://stackoverflow.com/a/20047131
     *
     * @param \DateTime $date1
     * @param \DateTime $date2
     * @return int
     */
    public static function diffInMonths(\DateTime $date1, \DateTime $date2)
    {
        $diff =  $date1->diff($date2);
    
        $months = $diff->y * 12 + $diff->m + $diff->d / 30;
    
        return (int) round($months);
    }
}