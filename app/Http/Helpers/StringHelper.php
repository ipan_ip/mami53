<?php

namespace App\Http\Helpers;

class StringHelper
{
    /**
     * Exploding a string and cast each of them as int.
     *
     * @param string $delimiter
     * @param string|null $string
     * @return array
     */
    public static function explodeAndCastToInt(string $delimiter, ?string $string): array
    {
        $result = [];
        if (is_null($string) || empty($delimiter)) {
            return $result;
        }
        foreach (explode($delimiter, $string) as $levelId) {
            if (empty(trim($levelId))) {
                continue;
            }
            $result[] = (int) $levelId;
        }
        return $result;
    }
}
