<?php 
namespace Test\App\Http\Helpers;

use App\Http\Helpers\PriceHelper;
use App\Test\MamiKosTestCase;

class PriceHelperTest extends MamikosTestCase
{
	public function testGetAdditionalTotalForRentType()
	{
		$result = PriceHelper::getAdditionalTotalForRentType(2, 100);
		$this->assertEquals(100, $result);
		
		$result = PriceHelper::getAdditionalTotalForRentType(3, 100);
		$this->assertEquals(100 * 12, $result);
		
		$result = PriceHelper::getAdditionalTotalForRentType(4, 100);
		$this->assertEquals(100 * 3, $result);
		
		$result = PriceHelper::getAdditionalTotalForRentType(5, 100);
		$this->assertEquals(100 * 6, $result);
	}
	
	public function testTransformDiscountPercentage()
	{
		$this->assertEquals(80, PriceHelper::transformDiscountPercentage(100, 20));
	}
	
	public function testSumPriceWithAdditionalPrice()
	{
		$this->assertEquals(10 + 9, PriceHelper::sumPriceWithAdditionalPrice(10, 9));
	}
}