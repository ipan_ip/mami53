<?php

namespace App\Http\Helpers;

class LocationHelper
{
    public const ADD_CONSTANT = 0.00035;
    public const EARTH_RADIUS = 6378137;
    public const OFFSET_RADIUS = 20000; // 20 Kilometers

    /**
     * @param array $location
     *
     * @return string
     */
    public static function getCenterLocationForTracker(array $location): string
    {
        $centerLocation = "";

        if (isset($location[0])) {
            if (count($location) > 1) {
                $latitude  = (floatval($location[0][0]) + floatval($location[1][0])) / 2;
                $longitude = (floatval($location[0][1]) + floatval($location[1][1])) / 2;
            } else {
                $latitude  = floatval($location[0][0]);
                $longitude = floatval($location[0][1]);
            }

            $centerLocation = $latitude . "," . $longitude;
        }

        return $centerLocation;
    }

    /**
     * @param $coordinate
     * @return string
     */
    public static function getGeoCoordinate($coordinate)
    {
        $coordinate = $coordinate + self::ADD_CONSTANT * (rand(-10, 10) / 10);

        return number_format($coordinate, 16);
    }

    /**
     * @param $geoLocation
     * @return array|float[]|int[]
     */
    public static function expandRangeLocation($geoLocation = []): array
    {
        if (
            !is_array($geoLocation)
            || empty($geoLocation)
        ) {
            return [];
        }

        $centerPoint = UtilityHelper::getCenterPoint($geoLocation);

        if (empty($centerPoint)) {
            return [];
        }

        $centerLatLong = [
            (float)$centerPoint[0],
            (float)$centerPoint[1]
        ];

        //Earth’s radius, sphere
        $earthRadius = self::EARTH_RADIUS;

        //offsets in meters
        $moveRange = self::OFFSET_RADIUS;

        //Coordinate offsets in radians
        $dLat = $moveRange / $earthRadius;
        $dLon = $moveRange / ($earthRadius * cos(pi() * $centerLatLong[1] / 180));

        //OffsetPosition, decimal degrees
        $latTopRight = floatval($centerLatLong[1] + $dLat * 180 / pi());
        $lonTopRight = floatval($centerLatLong[0] + $dLon * 180 / pi());

        $latBottomLeft = floatval($centerLatLong[1] - $dLat * 180 / pi());
        $lonBottomLeft = floatval($centerLatLong[0] - $dLon * 180 / pi());

        return [
            [
                $lonBottomLeft,
                $latBottomLeft
            ],
            [
                $lonTopRight,
                $latTopRight
            ]
        ];
    }
}
