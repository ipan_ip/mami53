<?php

namespace App\Http\Helpers;

use App\Entities\Activity\ChatAdmin;

/**
* 
*/
class UtilityHelper
{
	const VALID_AREA_ARRAY_SIZE = 2;
	
	public static function unsubscribeLinkGenerator($userId, $type = 'recommendation')
	{
		$baseUrl = 'https://mamikos.com/unsubscribe?type=';

		$userIdMultiply = $userId * 2;

		$format = $type . ':' . ($userIdMultiply * 3) . ':' . $userIdMultiply;

		$unsubscribeUrl = $baseUrl . base64_encode($format);

		return $unsubscribeUrl;
	}

	public static function unsubscribeLinkVerifier($dataEncoded)
	{
		$dataDecoded = base64_decode($dataEncoded);

        $unsubscribeData = explode(':', $dataDecoded);

        if(count($unsubscribeData) != 3) {
        	return false;
        }

        if($unsubscribeData[1] / 3 != $unsubscribeData[2]) {
        	return false;
        }

        $unsubscribeData[2] = $unsubscribeData[2] / 2;

        return $unsubscribeData;
	}

	public static function rateCSLinkVerifier($dataEncoded)
	{
		$adminIds = ChatAdmin::getAdminIdsForRatingCSPost();

		$dataDecoded = base64_decode($dataEncoded);

		$rateCSData = explode('|', $dataDecoded);

		if(count($rateCSData) != 3) {
			return [false, 'Link yang Anda masukkan tidak valid.'];
		}

		if(!in_array((int)$rateCSData[0], $adminIds)) {
			return [false, 'Link yang Anda masukkan tidak valid.'];
		}

		if(!is_numeric($rateCSData[1])) {
			return [false, 'Link yang Anda masukkan tidak valid.'];
		}

		if((int)$rateCSData[2] < time()) {
			return [false, 'Link yang Anda masukkan sudah tidak bisa digunakan.'];
		}

		return [true, $rateCSData];
	}


	public static function checkValidEmail($email)
	{
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
           return false;
        }
        return true;
    }

	/**
	 * Get area center point coordinate
	 *
	 * @param $area
	 *
	 * @return array
	 */
	public static function getCenterPoint($area)
	{
		$centerPoint = [];

		if (!self::getIsValidAreaCoordinates($area))
		{
			return $centerPoint;
		}

        // 0 index, should be longitude
        $centerPoint = [
			(($area[0][0] + $area[1][0]) / 2),
			(($area[0][1] + $area[1][1]) / 2)
		];

        return $centerPoint;
	}

	/**
	 * Helper for validating area coordinates
	 *
	 * @param $area
	 *
	 * @return bool
	 */
	public static function getIsValidAreaCoordinates($area)
	{
		if (!is_array($area))
			return false;

		if (sizeof($area) !== self::VALID_AREA_ARRAY_SIZE)
			return false;

		foreach ($area as $location)
		{
			if (!is_array($location))
			{
				return false;
				break;
			}

			if (sizeof($location) !== self::VALID_AREA_ARRAY_SIZE)
			{
				return false;
				break;
			}

			if (array_search(null, $location) !== false)
			{
				return false;
				break;
			}
		}

		return true;
	}

	public static function isNotOutlier($dataset, $comparedValue)
	{
		$cleanDataset = self::removeOutliers($dataset);

		$cleanCount = count($cleanDataset);
		$cleanMean = array_sum($cleanDataset) / $cleanCount;

		$cleanDeviation = self::getStandardDeviation($cleanDataset, $cleanCount, $cleanMean);

		if($comparedValue <= $cleanMean + $cleanDeviation && $comparedValue >= $cleanMean - $cleanDeviation) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * remove outliers from list of numbers
	 * https://stackoverflow.com/questions/15174952/finding-and-removing-outliers-in-php
	 */
	public static function removeOutliers($dataset, $magnitude = 1) 
	{
		$count = count($dataset);
		$mean = array_sum($dataset) / $count;

		// Calculate standard deviation and times by magnitude
		$deviation = self::getStandardDeviation($dataset, $count, $mean) * $magnitude; 

		// Return filtered array of values that lie within $mean +- $deviation.
		return array_filter($dataset, function($x) use ($mean, $deviation) { 
			return ($x <= $mean + $deviation && $x >= $mean - $deviation); 
		}); 
	}

	public static function getStandardDeviation($dataset, $count = null, $mean = null)
	{
		if(is_null($count)) {
			$count = count($dataset);
		}

		if(is_null($mean)) {
			$mean = array_sum($dataset) / $count;
		}

		return sqrt(array_sum(
			array_map(function($x, $mean) {
				return pow($x - $mean, 2);
			}, 
			$dataset, array_fill(0, $count, $mean))) / $count);
	}

	public static function roundDown($decimal, $precision)
    {
        $fraction = substr($decimal - floor($decimal), 2, $precision); // calculates the decimal places to $precision length
        $newDecimal = floor($decimal). '.' .$fraction; // reconstructs decimal with new decimal places
        
        return floatval($newDecimal);
    }

    public static function checkApplyOtherWeb($request)
    {
        if(isset($request['data_input']) && $request['data_input'] == 'aggregator') return true;

    	$email = trim($request['user_email']);
        $phone = trim($request['user_phone']);
        $detailUrl = isset($request['detail_url']) ? trim($request['detail_url']) : '';

        if (!filter_var($email, FILTER_VALIDATE_EMAIL) AND (strlen($phone) < 1 OR is_null($phone) OR $phone == "-")) {
            if($detailUrl != '') {
                return true;
            }
        }
        return false;
    }

    public static function popupImagesRandom()
    {
    	$baseUrl = "https://mamikos.com/assets/popup/";
        $images = [
        	"popup_marketplace_assets_a.png",
        	"popup_marketplace_assets_b.png",
        	"popup_marketplace_assets_c.png"
        ];
        $random_key = array_rand($images, 1);
        return $baseUrl.$images[$random_key];
    }

    /**
     * Combine all combination possibilities from a multidimensional array
     *
     * Reference : 
     * http://www.farinspace.com/php-array-combinations/
     * https://gist.github.com/farinspace/1126042
     */
    public static function combos($data, &$all = array(), $group = array(), $val = null, $i = 0) {
        if (isset($val)) {
            array_push($group, $val);
        }

        if ($i >= count($data)) {
            array_push($all, $group);
        } else {
            foreach ($data[$i] as $v) {
                self::combos($data, $all, $group, $v, $i + 1);
            }
        }

        return $all;
    }   

    public static function modifyQueryStringForSorting($url, $sortName, $returnType = 'string')
    {
        $qs = parse_url($url, PHP_URL_QUERY);

        parse_str($qs, $qsSplitted);

        // $currentSort = isset($qsSplitted['sort']) ? $qsSplitted : '';
        $currentDirection = isset($qsSplitted['sort_dir']) ? 
            ($qsSplitted['sort_dir'] == 'asc' ? 'desc' : 'asc') : 'asc';

        $qsSplitted['sort'] = $sortName;
        $qsSplitted['sort_dir'] = $currentDirection;

        if ($returnType == 'string') {
            return http_build_query($qsSplitted);
        } else {
            return $qsSplitted;
        }
    }

    /**
     * Turn all URLs in clickable links.
     * 
     * @param string $value
     * @param array  $protocols  http/https, ftp, mail, twitter
     * @param array  $attributes
     * @param string $mode       normal or all
     * @return string
     */
    public static function linkify($value, $protocols = array('http', 'mail'), array $attributes = array())
    {
        // Link attributes
        $attr = '';
        foreach ($attributes as $key => $val) {
            $attr = ' ' . $key . '="' . htmlentities($val) . '"';
        }
        
        $links = array();
        
        // Extract existing links and tags
        $value = preg_replace_callback('~(<a .*?>.*?</a>|<.*?>)~i', function ($match) use (&$links) { return '<' . array_push($links, $match[1]) . '>'; }, $value);
        
        // Extract text links for each protocol
        foreach ((array)$protocols as $protocol) {
            switch ($protocol) {
                case 'http':
                case 'https':   $value = preg_replace_callback('~(?:(https?)://([^\s<]+)|(www\.[^\s<]+?\.[^\s<]+))(?<![\.,:])~i', function ($match) use ($protocol, &$links, $attr) { if ($match[1]) $protocol = $match[1]; $link = $match[2] ?: $match[3]; return '<' . array_push($links, "<a $attr href=\"$protocol://$link\">$link</a>") . '>'; }, $value); break;
                case 'mail':    $value = preg_replace_callback('~([^\s<]+?@[^\s<]+?\.[^\s<]+)(?<![\.,:])~', function ($match) use (&$links, $attr) { return '<' . array_push($links, "<a $attr href=\"mailto:{$match[1]}\">{$match[1]}</a>") . '>'; }, $value); break;
                /*case 'twitter': $value = preg_replace_callback('~(?<!\w)[@#](\w++)~', function ($match) use (&$links, $attr) { return '<' . array_push($links, "<a $attr href=\"https://twitter.com/" . ($match[0][0] == '@' ? '' : 'search/%23') . $match[1]  . "\">{$match[0]}</a>") . '>'; }, $value); break;*/
                default:        $value = preg_replace_callback('~' . preg_quote($protocol, '~') . '://([^\s<]+?)(?<![\.,:])~i', function ($match) use ($protocol, &$links, $attr) { return '<' . array_push($links, "<a $attr href=\"$protocol://{$match[1]}\">{$match[1]}</a>") . '>'; }, $value); break;
            }
        }
        
        // Insert all link
        return preg_replace_callback('/<(\d+)>/', function ($match) use (&$links) { return $links[$match[1] - 1]; }, $value);
    }

    public function hideNonUTF8($word)
    {
        //reject overly long 2 byte sequences, as well as characters above U+10000 and replace with empty string
        $hideNonUTF8 = preg_replace('/[\x00-\x08\x10\x0B\x0C\x0E-\x19\x7F]'.
         '|[\x00-\x7F][\x80-\xBF]+'.
         '|([\xC0\xC1]|[\xF0-\xFF])[\x80-\xBF]*'.
         '|[\xC2-\xDF]((?![\x80-\xBF])|[\x80-\xBF]{2,})'.
         '|[\xE0-\xEF](([\x80-\xBF](?![\x80-\xBF]))|(?![\x80-\xBF]{2})|[\x80-\xBF]{3,})/S',
         '', $word );
         
        //reject overly long 3 byte sequences and UTF-16 surrogates and replace with empty string
        $hideNonUTF8 = preg_replace('/\xE0[\x80-\x9F][\x80-\xBF]'.
         '|\xED[\xA0-\xBF][\x80-\xBF]/S','', $hideNonUTF8 );

        return $hideNonUTF8;
    }
}