<?php

namespace App\Http\Helpers;

use App\Entities\Level\KostLevel;
use App\Entities\Room\Room;

class GoldplusPermissionHelper
{

    /**
     * Checking is the selected golplus has the permission.
     *
     * @param integer $goldplusLevelId
     * @param string $permission See App\Enums\Goldplus\Permission\GoldplusPermission for possible permission.
     * @return boolean
     */
    public static function isAllow(int $goldplusLevelId, string $permission): bool
    {
        return self::isGroupLevelAllow(KostLevel::getGoldplusGroupByLevel($goldplusLevelId), $permission);
    }

    /**
     * Checking is the selected golplus group has the permission.
     * This method is created to handle multiple goldplus level ids of each level.
     *
     * @param integer $goldplusGroupLevel Goldplus group. 1-4
     * @param string $permission See App\Enums\Goldplus\Permission\GoldplusPermission for possible permission.
     * @return boolean
     */
    public static function isGroupLevelAllow(int $goldplusGroupLevel, string $permission): bool
    {
        return isset(config('gp_permissions')[$goldplusGroupLevel]['allow'][$permission]);
    }

    /**
     * Return the array of permission that were assigned for the goldplus level id
     *
     * @param integer $goldplusLevelId
     * @return array if the level id is not found, will return empty array
     */
    public static function getPermissions(int $goldplusLevelId): array
    {
        return self::getGroupPermissions(KostLevel::getGoldplusGroupByLevel($goldplusLevelId));
    }

    /**
     * Return the array of permission that were assigned for the goldplus level.
     * This method is created to handle multiple goldplus level ids of each level.
     *
     * @param integer $goldplusGroupLevel Goldplus group. 1-4
     * @return array if the level id is not found, will return empty array
     */
    public static function getGroupPermissions(int $goldplusGroupLevel): array
    {
        return array_keys(config('gp_permissions')[$goldplusGroupLevel]['allow'] ?? []);
    }

    /**
     * Checking is the room has required permission. Will return true if the room is not GP.
     *
     * @param Room $room
     * @param string $permission See App\Enums\Goldplus\Permission\GoldplusPermission for possible permission.
     * @return boolean
     */
    public static function isRoomAllow(Room $room, string $permission): bool
    {
        if (is_null($room->goldplus_level_id)) {
            // Always allow for non-gp room
            return true;
        }
        return self::isAllow($room->goldplus_level_id, $permission);
    }

    /**
     * Checking room permission using OR logic.
     * If at least 1 permission checking is true. This function will return true.
     *
     * @param Room $room
     * @param array $permissions See App\Enums\Goldplus\Permission\GoldplusPermission for possible permission.
     * @return boolean
     */
    public static function isRoomAllowAny(Room $room, array $permissions): bool
    {
        foreach ($permissions as $permission) {
            if (self::isRoomAllow($room, $permission)) {
                return true;
            }
            unset($permission);
        }
        return false;
    }

    /**
     * Checking room permission using AND logic.
     * If at least 1 permission checking is false. This function will return false.
     *
     * @param Room $room
     * @param array $permissions See App\Enums\Goldplus\Permission\GoldplusPermission for possible permission.
     * @return boolean
     */
    public static function isRoomAllowAll(Room $room, array $permissions): bool
    {
        foreach ($permissions as $permission) {
            if (!self::isRoomAllow($room, $permission)) {
                return false;
            }
            unset($permission);
        }
        return true;
    }

    /**
     * Checking gp group permission using AND logic.
     * If at least 1 permission checking is false. This function will return false.
     *
     * @param int $goldplusGroupLevel
     * @param array $permissions See App\Enums\Goldplus\Permission\GoldplusPermission for possible permission.
     * @return boolean
     */
    public static function isGroupLevelAllowAll(int $goldplusGroupLevel, array $permissions): bool
    {
        foreach ($permissions as $permission) {
            if (!self::isGroupLevelAllow($goldplusGroupLevel, $permission)) {
                return false;
            }
            unset($permission);
        }
        return true;
    }

    /**
     * Extract the list of goldplus level ids that doesnt have certaion permissions.
     *
     * @param string $permission
     * @return array
     */
    public static function getGoldplusLevelIdsWithDenyPermissions(array $permissions): array
    {
        $result = [];
        for ($gpGroup = 1; $gpGroup <= KostLevel::GP_MAX_GROUP; $gpGroup++) {
            if (!self::isGroupLevelAllowAll($gpGroup, $permissions)) {
                $result = array_merge(
                    $result,
                    KostLevel::getGoldplusLevelIdsByGroups($gpGroup)
                );
            }
        }
        return $result;
    }
}
