<?php

namespace App\Http\Helpers;

class PriceHelper
{
    /** Return the specific total for each rent type
     *
     * @param $type
     * @param $total
     * @return float|int
     */
    public static function getAdditionalTotalForRentType($type, $total) {
        switch ($type) {
            case 3 :
                $total = $total * 12;
                break;
            case 4 :
                $total = $total * 3;
                break;
            case 5 :
                $total = $total * 6;
                break;
            default :
                break;
        }
        return $total;
    }

    /** Get The discount percentage after adding additional price
     *
     * @param $originalPrice
     * @param $discountPrice
     * @return float
     */
    public static function transformDiscountPercentage($originalPrice, $discountPrice)
    {
        return round((($originalPrice - $discountPrice) / $originalPrice) * 100, 0);
    }

    public static function sumPriceWithAdditionalPrice($price, $additionalPrice)
    {
        return ($price + $additionalPrice);
    }
}
