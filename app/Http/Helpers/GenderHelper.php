<?php

namespace App\Http\Helpers;

/**
 * 
 */
class GenderHelper
{
    const GENDER_REPLACE = [
        "perempuan" => "female",
        "laki-laki" => "male"
    ];

    public static function getUserGenderReplace($gender)
    {
        if ($gender == "perempuan") {
            return self::GENDER_REPLACE[$gender];
        } elseif ($gender == "laki-laki") {
            return self::GENDER_REPLACE[$gender];
        } else {
            return $gender;
        }
    }
}