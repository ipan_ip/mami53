<?php

namespace App\Http\Helpers;

use App\Entities\Activity\Tracking;
use App\Entities\Activity\TrackingPlatforms;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayInvoice;
use App\Entities\Mamipay\MamipayBillingRule;
use Carbon\Carbon;

class BookingInvoiceHelper
{
    const PRICE_BASE = "Base";
    const PRICE_DEPOSIT = "Deposit";
    const PRICE_DP = "DP";
    const PRICE_BIAYA_SEWA = "Biaya Sewa";

    const PRICE_TYPE_FIXED = "fixed";
    const PRICE_TYPE_DISCOUNT = "discount";
    const PRICE_TYPE_ADMIN = "admin";

    /**
     * Get booking invoice amount by mamipay contract.
     *
     * @param int $contractId
     * @return array|null
     */
    public static function getAmountByContractId(int $contractId): ?array
    {
        $contract = MamipayContract::getFromExternalMamipay($contractId);
        return self::getAmountByContract($contract, $contractId);
    }

    /**
     * Get booking invoice amount by mamipay contract.
     *
     * @param mixed $contract . Can be retrevied from MamipayApi via MamipayContract::getFromExternalMamipay
     * @param int $contractId
     * @return array|null
     */
    public static function getAmountByContract($contract, int $contractId): ?array
    {
        if (is_null($contract)) {
            return null;
        }

        $billingRule = $contract->billing_rule;
        $startDate = Carbon::parse($contract->contract_profile->start_date_raw)->format('d');

        $baseAmount         = !empty($billingRule) ? $billingRule->base_amount : 0;
        $firstAmount        = !empty($billingRule) ? $billingRule->first_amount : 0;
        $depositAmount      = !empty($billingRule) ? $billingRule->deposit_amount : 0;
        $fineAmount         = !empty($billingRule) ? $billingRule->fine_amount : 0;
        $billingDate        = !empty($billingRule) ? !empty($billingRule->billing_date) ? $billingRule->billing_date : $startDate : null;
        $fineMaxLength      = !empty($billingRule) ? $billingRule->fine_maximum_length : null;
        $fineDurationType   = !empty($billingRule) ? $billingRule->fine_duration_type : 'day';

        $fine = MamipayBillingRule::getFineAmount($fineAmount, $billingDate, $fineMaxLength, $fineDurationType);

        $base        = self::priceFormat(self::PRICE_BASE, $baseAmount);
        $basicAmount = self::priceFormat(self::PRICE_BIAYA_SEWA, $baseAmount, self::PRICE_TYPE_FIXED);
        if ($firstAmount > 0) {
            $base        = self::priceFormat(self::PRICE_BASE, $firstAmount);
            $basicAmount = self::priceFormat(self::PRICE_BIAYA_SEWA, $firstAmount, self::PRICE_TYPE_FIXED);
        }

        $prorata        = self::priceFormat(self::PRICE_BASE, $firstAmount);
        $deposit        = self::priceFormat(self::PRICE_DEPOSIT, $depositAmount);
        $downPayment    = self::priceFormat(self::PRICE_DP, $contract->downpayment_amount);
        $additionalCost = self::getInvoiceCostByContractId($contractId);
        
        $other = $additionalCost['costs'];
        $other[] = $deposit;
        $other[] = $downPayment;
        $voucher = isset($additionalCost['vouchers']) ? (isset($additionalCost['vouchers']['price_total']) ? abs($additionalCost['vouchers']['price_total']) : 0) : 0;

        $total = ($base['price_total'] + $additionalCost['total'] + $deposit['price_total']);
        if ($contract->downpayment_amount) {
            $adminAmount = isset($additionalCost['admin']['price_total']) ? $additionalCost['admin']['price_total'] : 0;
            $total = ($contract->downpayment_amount - $voucher + $adminAmount);
        }

        return $priceComponents = [
            "base"  => $base,
            "other" => $other,
            "total" => self::priceFormat("Total", $total),
            "fine"  => $fine,
        ];
    }

    public static function priceFormat($label, $price, $type = "")
    {
        $priceString = 'Rp' . number_format($price, 0, ',', '.');

        return [
            "price_name" => $label,
            "price_label" => $label,
            "price_total" => (float)$price,
            "price_total_string" => $priceString,
            "price_type" => $type
        ];
    }

    public static function getInvoiceCostByContractId($contractId)
    {
        $invoiceCosts = MamipayInvoice::with('additional_costs')->where('contract_id', $contractId)->first();
        
        if (is_null($invoiceCosts)) {
            return [
                'costs' => [],
                'admin' => [],
                'total' => 0
            ];             
        }

        $costs  = [];
        $adminAmount = [];
        $vouchers = [];
        $total = 0;
        foreach($invoiceCosts->additional_costs as $invoiceCost) {
            $cost = self::priceFormat($invoiceCost->cost_title, $invoiceCost->cost_value, $invoiceCost->cost_type);
            $total += $invoiceCost->cost_value;

            $costs[] = $cost;
            if ($invoiceCost->cost_type == self::PRICE_TYPE_ADMIN) {
                $adminAmount = self::priceFormat($invoiceCost->cost_title, $invoiceCost->cost_value, $invoiceCost->cost_type);
            }

            if ($invoiceCost->cost_type == self::PRICE_TYPE_DISCOUNT) {
                $vouchers = self::priceFormat($invoiceCost->cost_title, $invoiceCost->cost_value, $invoiceCost->cost_type);
            }
        }

        return [
            'costs' => $costs,
            'admin' => $adminAmount,
            'vouchers' => $vouchers,
            'total' => $total
        ];
    }

    public static function invoice($invoices)
    {
        $transformData = [];
        foreach ($invoices as $invoice) {
            $descriptionStatus = null;
            $scheduledDate = Carbon::parse($invoice->scheduled_date);
            $paidAt = Carbon::parse($invoice->paid_at);
            $checkScheduledPaid = $paidAt->diffInDays($scheduledDate, false);

            $statusPayment = $invoice->payment_status_formatted;
            if ($checkScheduledPaid < 0) {
                $statusPayment = MamipayInvoice::LATE_PAYMENT.' '. abs($checkScheduledPaid) ." hari";
            } else {
                if ($invoice->status == MamipayInvoice::PAYMENT_STATUS_PAID) {
                    $statusPayment = MamipayInvoice::ONTIME_PAYMENT;
                }
            }

            $amount = $invoice->amount;
            if ($invoice->calculated_amount > 0) {
                $amount = $invoice->calculated_amount;
            }

            $invoiceUrl = null;
            $manualReminder = $invoice->manual_reminder;
            $lastPayment = $invoice->last_payment;
            if ($manualReminder || $lastPayment) {
                $invoiceUrl = self::withPlatformParam($invoice->shortlink_url);
            }

            $credentials = [
                '_id'            => $invoice->id,
                'contract_id'    => $invoice->contract_id,
                'name'           => $invoice->name,
                'invoice_number' => $invoice->invoice_number,
                'shortlink'      => $invoiceUrl,
                'scheduled_date' => Carbon::parse($invoice->scheduled_date)->format('d F Y H:i'),
                'scheduled_date_second' => $invoice->scheduled_date,
                'paid_at'        => $invoice->paid_at,
                'amount'         => $amount,
                'amount_string'  => 'Rp. ' . number_format($amount, 0, ',', '.'),
                'status'         => ($invoice->status == MamipayInvoice::PAYMENT_STATUS_NOT_IN_MAMIPAY) ? MamipayInvoice::PAYMENT_STATUS_PAID : $invoice->status,
                'late_payment'   => abs($checkScheduledPaid),
                'status_payment' => $statusPayment,
                'manual_reminder' => $manualReminder,
                'last_payment'   => $lastPayment
            ];

            $transformData[] = $credentials;
        }

        return $transformData;
    }

    /**
     * Generate years information for my room api.
     *
     * @param array $invoices. Retrieved from BookingInvoiceHelper::invoices
     * @return array
     */
    public static function years(array $invoices): array
    {
        $years = [];
        foreach ($invoices as $invoice) {
            $year = Carbon::parse($invoice['scheduled_date_second'])->format('Y');

            if (!in_array($year, $years)) {
                $years[] = $year;
            }
        }
        return $years;
    }

    public static function withPlatformParam($url)
    {
        $platform = Tracking::getPlatform();
        if (in_array($platform, [TrackingPlatforms::WebDesktop, TrackingPlatforms::WebMobile])) {
            return \sprintf("%s?platform=%s", $url, 'web');
        } 
        return \sprintf("%s?platform=%s", $url, $platform);
    }
}
