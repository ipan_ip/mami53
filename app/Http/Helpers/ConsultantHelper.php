<?php

namespace App\Http\Helpers;

use App\Entities\Room\Room;
use App\Entities\Consultant\ConsultantMapping;
use App\Entities\Consultant\Consultant;
use Cache;

class ConsultantHelper
{
    const CACHE_KEY_CONSULTANT_MAPPING = 'consultant-mapping';
    const SUBKEY_MAMIROOMS = "mamirooms";
    const SUBKEY_NON_MAMIROOMS = "non-mamirooms";

    public static function refreshCache()
    {
        Cache::rememberForever(self::CACHE_KEY_CONSULTANT_MAPPING, function() {
            $cacheData = [];

            $mappingGroups = ConsultantMapping::all()->groupBy('area_city');
            
            foreach ($mappingGroups as $key => $mappingGroup) {
                $cacheData[$key] = [ 
                    self::SUBKEY_MAMIROOMS => [],
                    self::SUBKEY_NON_MAMIROOMS => []
                ];

                foreach ($mappingGroup as $mapping) {
                    $subKey = $mapping->is_mamirooms ? self::SUBKEY_MAMIROOMS : self::SUBKEY_NON_MAMIROOMS;
                    $cacheData[$key][$subKey][] = $mapping->consultant_id;
                }                
            }

            return $cacheData;
        });

        return Cache::get(self::CACHE_KEY_CONSULTANT_MAPPING);
    }

    public static function getConsultantByAreaCity($areaCity, bool $isMamirooms)
    {
        if (empty($areaCity)) return null;

        $mapping = Cache::get(self::CACHE_KEY_CONSULTANT_MAPPING);

        if (is_null($mapping)) $mapping = self::refreshCache();

        if (isset($mapping[$areaCity]) == false) return null;

        $subKey = $isMamirooms ? self::SUBKEY_MAMIROOMS : self::SUBKEY_NON_MAMIROOMS;
        $consultants = $mapping[$areaCity][$subKey];

        $consultantId = 0;
        switch (count($consultants))
        {
            case 0:
                return null;
            case 1:
                $consultantId = $consultants[0];
                break;
            default:
                // throw dice!
                $randomIndex = array_rand($consultants, 1);
                $consultantId = $consultants[$randomIndex];
                break;
        }
        
        $consultant = Consultant::find($consultantId);
        if (is_null($consultant)) return null;
        
        return $consultant;
    }
}