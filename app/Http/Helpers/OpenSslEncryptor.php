<?php

namespace App\Http\Helpers;

class OpenSslEncryptor
{
    // encryption algoritim
    const DEFAULT_CIPHER_METHOD = 'aes-256-cbc';
    
    /**
     * Output encrypted and base64 encoded message
     *
     * @param string $message
     * @param string $key
     * @param string $iv
     * @return string
     */
    public static function encrypt(string $message, string $key, string $iv) : string
    {
        $encrypted = openssl_encrypt($message, self::DEFAULT_CIPHER_METHOD, $key, OPENSSL_RAW_DATA, $iv);
        $base64Encoded = base64_encode($encrypted);

        return $base64Encoded;           
    }

    /**
     * Output base64 decoded and decrypted message 
     *
     * @param string $message
     * @param string $key
     * @param string $iv
     * @return string
     */
    public static function decrypt(string $encrypted, string $key, string $iv) : string
    {
        $base64Decoded = base64_decode($encrypted);
        $decrypted = openssl_decrypt($base64Decoded, self::DEFAULT_CIPHER_METHOD, $key, OPENSSL_RAW_DATA, $iv);

        return $decrypted;
    }

    /**
     * Decrypt and json_decode into array
     *
     * @param string $message
     * @param string $key
     * @param string $iv
     * @return ?array
     */
    public static function decryptIntoArray(string $encrypted, string $key, string $iv) :?array
    {
        $decryptedData = self::decrypt($encrypted, $key, $iv);
        return json_decode($decryptedData, true);
    }
}