<?php

namespace App\Http\Helpers;

use App\User;
use App\Entities\Notif\EmailBounce;

/**
 * 
 */
class MailHelper
{

    protected static $blockedDomains = [
        'facebook.com',
        'mailinator.com'
    ];
    
    public static function isBlockedEmail($email)
    {
        $blockedDomains = self::$blockedDomains;

        $isBlocked = false;

        foreach ($blockedDomains as $domain) {
            if (strpos($email, $domain) !== false) {
                $isBlocked = true;

                break;
            }
        }

        if ($isBlocked) return $isBlocked;

        // check blocked email
        $bouncedEmail = EmailBounce::where('email', $email)->first();

        if ($bouncedEmail) {
            $isBlocked = true;
        }
        
        return $isBlocked;
    }
}