<?php

namespace App\Http\Helpers;

use App\Entities\Area\AreaGeolocation;
use App\Entities\Search\InputKeywordSuggestion;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;

class GeolocationHelper
{
    protected $areaLevels = [
        0 => 'country',
        1 => 'province',
        2 => 'city',
        3 => 'subdistrict',
        4 => 'village'
    ];

    protected $loopLimit = [
        'province' => 3,
        'city' => 2,
        'subdistrict' => 1,
        'village' => 0
    ];

    protected $ignoredAdministrativeTypes = [
        'accounting',
        'airport',
        'amusement_park',
        'aquarium',
        'archipelago',
        'art_gallery',
        'atm',
        'bakery',
        'bank',
        'bar',
        'beauty_salon',
        'bicycle_store',
        'book_store',
        'bowling_alley',
        'bus_station',
        'cafe',
        'campground',
        'car_dealer',
        'car_rental',
        'car_repair',
        'car_wash',
        'casino',
        'cemetery',
        'church',
        'city_hall',
        'city_mall',
        'clothing_store',
        'colloquial_area',
        'continent',
        'convenience_store',
        'country',
        'courthouse',
        'dentist',
        'department_store',
        'doctor',
        'electrician',
        'electronics_store',
        'embassy',
        'establishment',
        'finance',
        'fire_station',
        'florist',
        'food',
        'funeral_home',
        'furniture_store',
        'gas_station',
        'general_contractor',
        'grocery_or_supermarket',
        'gym',
        'hair_care',
        'hardware_store',
        'health',
        'hindu_temple',
        'home_goods_store',
        'hospital',
        'insurance_agency',
        'jewelry_store',
        'laundry',
        'lawyer',
        'library',
        'liquor_store',
        'local_government_office',
        'locksmith',
        'lodging',
        'meal_delivery',
        'meal_takeaway',
        'mosque',
        'movie_rental',
        'movie_theater',
        'moving_company',
        'museum',
        'natural_feature',
        'neighborhood',
        'night_clun',
        'not_supported',
        'other',
        'painter',
        'park',
        'parking',
        'pet_store',
        'pharmacy',
        'physiotherapist',
        'place_of_worship',
        'plumber',
        'point_of_interest',
        'police',
        'political',
        'post_office',
        'postal_code',
        'postal_town',
        'premise',
        'real_estate_agency',
        'restaurant',
        'roofing_contractor',
        'route',
        'rv_park',
        'school',
        'shoe_store',
        'shopping_mall',
        'spa',
        'stadium',
        'storage',
        'store',
        'street_address',
        'sublocality',
        'sublocality_level_1',
        'sublocality_level_4',
        'subpremise',
        'subway_station',
        'synagogue',
        'taxi_stand',
        'train_station',
        'transit_station',
        'travel_agency',
        'university',
        'veterinary_cafe',
        'zoo',
    ];

    protected $ignorableWords = [
        'kabupaten',
        'kecamatan',
        'kelurahan',
        'provinsi',
        'distrik',
        'district',
        'sub-district',
        'pantai',
        'gedung',
        'taman',
    ];

    protected $blockedKeywords = [
        'Jalan',
        'Jln',
        'Rumah',
        'RS',
        'Klinik',
        'Plaza',
        'Tower',
        'Mall',
        'Building',
        'Gang',
        'Univ',
        'Universitas',
        'Sekolah',
        'ATM',
        'Kost',
        'Kos',
        'SD',
        'SMP',
        'SMA',
        'Office',
        'PT',
        'Pabrik',
        'Hotel',
        'Centre',
        'Center',
    ];

    protected $veryImportantKeyword = [
        'kota'
    ];

    protected $synonyms = [
        // Translations
        'south jakarta' => 'jakarta selatan',
        'north jakarta' => 'jakarta utara',
        'west jakarta' => 'jakarta barat',
        'east jakarta' => 'jakarta timur',
        'central jakarta' => 'jakarta pusat',
        'west java' => 'jawa barat',
        'east java' => 'jawa timur',
        'central java' => 'jawa tengah',
        'bogor city' => 'kota bogor',
        'batam city' => 'kota batam',

        // Well-known names
        'daerah khusus ibukota jakarta' => 'jakarta raya',
        'daerah istimewa yogyakarta' => 'yogyakarta',

        // Typo names
        'nagri' => 'nageri',
        'nagri kaler' => 'nageri kaler',
        'nagri kidul' => 'nageri kidul',
        'nagri tengah' => 'nageri tengah',
        'manyar sido mukti' => 'manyarsidomukti',
        'manyar rejo' => 'manyarejo',
    ];

    protected $dualAdministrativeCities = [
        'Bandung',
        'Baru',
        'Bekasi',
        'Bima',
        'Binjai',
        'Blitar',
        'Bogor',
        'Cirebon',
        'Gorontalo',
        'Jayapura',
        'Kediri',
        'Kupang',
        'Madiun',
        'Magelang',
        'Malang',
        'Medan',
        'Mojokerto',
        'Pasuruan',
        'Pekalongan',
        'Pontianak',
        'Probolinggo',
        'Semarang',
        'Serang',
        'Solok',
        'Sorong',
        'Sukabumi',
        'Tangerang',
        'Tanjungbalai',
        'Tasikmalaya',
        'Tegal',
        'Yogyakarta',
    ];

    protected $possiblyDuplicatedNameDistricts = [
        'padang'
    ];

    protected $disabledTriggers = [
        'scheduler',
        'observer'
    ];

    /**
     * Map InputKeywordSuggestion with AreaGeolocation
     *
     * @param InputKeywordSuggestion $suggestion
     * @return bool
     */
    public function mapGeolocation(InputKeywordSuggestion $suggestion): bool
    {
        if (in_array($suggestion->administrative_type, $this->ignoredAdministrativeTypes) !== false) {
            return false;
        }

        // Set geolocation
        if (!$this->setGeolocation($suggestion)) {
            return false;
        }

        return true;
    }

    public function setGeolocation(InputKeywordSuggestion $suggestion): bool
    {
        $formattedAddress = $this->getFormattedAddress($suggestion->suggestion . ', ' . $suggestion->area);
        if (empty($formattedAddress)) {
            return false;
        }

        $geolocationID = $this->getGeolocationID(
            $formattedAddress['level'],
            $formattedAddress['address']
        );

        // If `geolocationID` is still not found, try to strip last element of the `address`
        if (!$geolocationID) {
            unset($formattedAddress['address'][count($formattedAddress['address']) - 1]);
            if (empty($formattedAddress['address'])) {
                return false;
            }

            // Set new level
            $currentLevel = array_search($formattedAddress['level'], $this->areaLevels);
            $formattedAddress['level'] = $this->areaLevels[$currentLevel - 1];

            $geolocationID = $this->getGeolocationID(
                $formattedAddress['level'],
                $formattedAddress['address']
            );
        }

        if (!$geolocationID) {
            return false;
        }

        $suggestion->geolocation()->sync($geolocationID);

        return true;
    }

    public function getFormattedAddress(string $initialAddress): array
    {
        $data = [];

        // Get address mapping in array
        $address = $this->getAddressMapping($initialAddress);
        if (
            empty($address)
            // Or, if the country level is not Indonesia
            || $address[0] !== 'Indonesia'
        ) {
            return $data;
        }

        // Get area level by analyzing address mapping
        $level = $this->getAreaLevel($address);
        if (empty($level)) {
            return $data;
        }

        // Prepare address mapping for query statement
        $address = $this->prepareAddressForQuery($address);

        return [
            'level' => $level,
            'address' => $address
        ];
    }

    public function getAddressMapping(string $address): array
    {
        // Exit immediately if `address` is empty
        if (empty($address)) {
            return [];
        }

        // Check of address contains blocked keywords
        if ($this->doesContainBlockedWords($address)) {
            return [];
        }

        // convert to array
        $address = explode(', ', $address);

        // Do more processing...
        $address = array_map(
            function ($string) {
                // Remove unnecessary words
                $string = $this->removeIgnorableWords($string);

                // Find synonym words
                $string = $this->findSynonymWord($string);

                // Remove `veryImportantKeyword` if it's not `dualAdministrativeCity` type
                if (
                    $this->contains($string, $this->veryImportantKeyword)
                    && $this->contains(Str::title($string), $this->dualAdministrativeCities) !== true
                ) {
                    $string = substr(strstr($string, " "), 1);
                }

                return $string;
            },
            $address
        );

        // Reverse the order so that country-level would be on top
        $address = array_reverse($address);

        return $address;
    }

    public function getAreaLevel(array $address): string
    {
        // if address count more than available area levels
        if (count($address) > count($this->areaLevels)) {
            // Try to remove duplicated keyword first
            $address = array_unique($address);

            // if address count IS STILL more than available area levels
            if (count($address) > count($this->areaLevels)) {
                return '';
            }
        }

        // Manipulate "sub district" string, in case it's possibly duplicated with "city" name
        $cityIndex = (int)array_search('city', $this->areaLevels);
        $subDistrictIndex = (int)array_search('subdistrict', $this->areaLevels);
        if (count($address) === ($subDistrictIndex + 1)) {
            if (
                $this->contains($address[$subDistrictIndex], $this->possiblyDuplicatedNameDistricts)
                && $address[$subDistrictIndex] === $address[$cityIndex]
            ) {
                unset($address[$subDistrictIndex]);
            }
        }

        return $this->areaLevels[count($address) - 1];
    }

    public function prepareAddressForQuery($address, bool $returnAsString = false)
    {
        if (!is_array($address)) {
            $address = explode(' ', $address);
        }

        $address = array_map(
            function ($string) {
                // Check if current string is `dualAdministrativeCities` with type of "Kabupaten", instead "Kota"
                $isFlaggedAsKabupaten = false;
                if (
                    $this->contains($string, $this->dualAdministrativeCities)
                    && $this->contains($string, $this->veryImportantKeyword) === false
                ) {
                    $isFlaggedAsKabupaten = true;
                }

                $array = explode(' ', $string);
                $string = array_map(
                    function ($key, $value) use ($string, $isFlaggedAsKabupaten) {
                        // Filter keyword which has `isFlaggedAsKabupaten`
                        if ($isFlaggedAsKabupaten) {
                            // If it's the city name, then add "-Kota" to select "Kabupaten" only
                            if (in_array(Str::title($value), $this->dualAdministrativeCities) !== false) {
                                return '-Kota +' . $value;
                            }
                        }

                        return '+' . $value;
                    },
                    array_keys($array),
                    $array
                );

                return implode(' ', $string);
            },
            $address
        );

        if ($returnAsString) {
            $address = implode(' ', $address);
        }

        return $address;
    }

    /*
     * PRIVATE METHODS
     */

    private function contains($str, array $arr): bool
    {
        foreach ($arr as $a) {
            if (stripos($str, $a) !== false) {
                return true;
            }
        }

        return false;
    }

    private function removeIgnorableWords($keyword): string
    {
        $allowedWords = array_udiff(explode(" ", $keyword), $this->ignorableWords, 'strcasecmp');

        return implode(' ', $allowedWords);
    }

    private function findSynonymWord($keyword): string
    {
        return Str::title(str_ireplace(array_keys($this->synonyms), array_values($this->synonyms), $keyword));
    }

    private function doesContainBlockedWords(string $string): bool
    {
        foreach ($this->blockedKeywords as $blockedKeyword) {
            if (stripos($string, $blockedKeyword) !== false) {
                return true;
            }
        }

        return false;
    }

    private function getGeolocationID(string $level, array $address)
    {
        $geolocationID = null;

        $finished = false;
        $initialState = true;
        $retry = 0;
        do {
            $query = AreaGeolocation::query();
            $query = $this->setConditions($level, $query, $address, $retry);

            $geolocationID = $query->pluck('id')->first();
            if (!$geolocationID) {
                if ($initialState) {
                    $initialState = false;
                }

                $retry++;
            } else {
                $finished = true;
            }
        } while (
            !$finished
            && $retry <= $this->loopLimit[$level]
        );

        return $geolocationID;
    }

    private function setConditions(
        string $level,
        Builder $query,
        array $address,
        int $retry
    ): Builder {
        switch ($level) {
            case 'village':
                $query
                    ->whereRaw("MATCH (`province`) AGAINST ('" . $address[1] . "' IN BOOLEAN MODE)")
                    ->whereRaw("MATCH (`city`) AGAINST ('" . $address[2] . "' IN BOOLEAN MODE)")
                    ->whereRaw("MATCH (`subdistrict`) AGAINST ('" . $address[3] . "' IN BOOLEAN MODE)")
                    ->whereRaw("MATCH (`village`) AGAINST ('" . $address[4] . "' IN BOOLEAN MODE)");
                break;
            case 'subdistrict':
                if ($retry === 1) {
                    $query
                        ->whereRaw("MATCH (`province`) AGAINST ('" . $address[1] . "' IN BOOLEAN MODE)")
                        ->whereRaw("MATCH (`city`) AGAINST ('" . $address[2] . "' IN BOOLEAN MODE)")
                        ->whereRaw("MATCH (`village`) AGAINST ('" . $address[3] . "' IN BOOLEAN MODE)");
                } else {
                    $query
                        ->whereRaw("MATCH (`province`) AGAINST ('" . $address[1] . "' IN BOOLEAN MODE)")
                        ->whereRaw("MATCH (`city`) AGAINST ('" . $address[2] . "' IN BOOLEAN MODE)")
                        ->whereRaw("MATCH (`subdistrict`) AGAINST ('" . $address[3] . "' IN BOOLEAN MODE)");
                }
                break;
            case 'city':
                if ($retry === 1) {
                    $query
                        ->whereRaw("MATCH (`province`) AGAINST ('" . $address[1] . "' IN BOOLEAN MODE)")
                        ->whereRaw("MATCH (`subdistrict`) AGAINST ('" . $address[2] . "' IN BOOLEAN MODE)");
                } elseif ($retry === 2) {
                    $query
                        ->whereRaw("MATCH (`province`) AGAINST ('" . $address[1] . "' IN BOOLEAN MODE)")
                        ->whereRaw("MATCH (`village`) AGAINST ('" . $address[2] . "' IN BOOLEAN MODE)");
                } else {
                    $query
                        ->whereRaw("MATCH (`province`) AGAINST ('" . $address[1] . "' IN BOOLEAN MODE)")
                        ->whereRaw("MATCH (`city`) AGAINST ('" . $address[2] . "' IN BOOLEAN MODE)");
                }
                break;
            default:
                if ($retry === 1) {
                    $query->whereRaw("MATCH (`city`) AGAINST ('" . $address[1] . "' IN BOOLEAN MODE)");
                } elseif ($retry === 2) {
                    $query->whereRaw("MATCH (`subdistrict`) AGAINST ('" . $address[1] . "' IN BOOLEAN MODE)");
                } elseif ($retry === 3) {
                    $query->whereRaw("MATCH (`village`) AGAINST ('" . $address[1] . "' IN BOOLEAN MODE)");
                } else {
                    $query->whereRaw("MATCH (`province`) AGAINST ('" . $address[1] . "' IN BOOLEAN MODE)");
                }
                break;
        }

        return $query;
    }
}