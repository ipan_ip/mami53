<?php

namespace App\Http\Helpers;

use App\Entities\Room\RoomOwner;
use App\Jobs\SendPushNotificationQueue;
use App\Jobs\SendSMSQueue;
use Validator;
use Auth;
use Notification;
use App\Entities\User\Notification as NotifOwner;
use App\Libraries\SMSLibrary;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Exception;
use App\Entities\Room\Room;
use App\Http\Helpers\GoldplusPermissionHelper;
use App\Enums\Goldplus\Permission\GoldplusPermission;

class BookingNotificationHelper
{
    /**
     * The current globally available container (if any).
     *
     * @var static
     */
    protected static $instance;

    const TITLE_FINISH_BOOKING_TENANT = 'Sudah menemukan kos baru?';
    const MESSAGE_FINISH_BOOKING_TENANT = 'Tak perlu bingung, Mamikos punya banyak pilihan kost di sekitarmu lho.';
    
    const TITLE_SUCCESS_BOOKING_TENANT = "Yeay! Booking telah diteruskan ke pemilik!";
    const MESSAGE_SUCCESS_BOOKING_TENANT = "Beri waktu pemilik untuk mengulas pesanan kamu atau hubungi melalui chatroom kami.";

    const TITLE_CANCEL_BOOKING_TENANT = "Booking berhasil dibatalkan";
    const MESSAGE_CANCEL_BOOKING_TENANT = "Ayo cek aplikasi mamikos untuk melihat pilihan kost lain!";

    const TITLE_HAS_PASSED_TO_TENANT = "Sudah menemukan kos baru?";
    const MESSAGE_HAS_PASSED_TO_TENANT = "Tak perlu bingung, Mamikos punya banyak pilihan kost di sekitarmu lho.";

    const TITLE_SUCCESS_BOOKING_OWNER = "Ada Booking Baru, Ayo konfirmasi sekarang!";
    const MESSAGE_SUCCESS_BOOKING_OWNER = "Segera konfirmasi booking sebelum kadaluarsa";

    const TITLE_CANCEL_BOOKING_OWNER = "Maaf, permintaan booking dibatalkan";
    const MESSAGE_CANCEL_BOOKING_OWNER = "Calon Penghuni membatalkan booking karena ";

    const SMS_WAITING_CONFIRM_TO_TENANT = 'Saat ini pemilik kost sedang mengecek booking kamu loh. Harap sabar dan pantau terus status booking di menu akun mamikos. ';
    const TITLE_NOTIF_WAITING_CONFIRM_TO_TENANT = 'Pemilik masih mengecek booking nih';
    const MESSAGE_NOTIF_WAITING_CONFIRM_TO_TENANT = 'Harap sabar dan pantau terus status booking di menu akun mamikos.';

    const SMS_WAITING_CONFIRM_TO_OWNER = 'Masih ada booking menunggu konfirmasi lho. Segera konfirmasi di aplikasi mamikos sebelum kadaluarsa!';
    const TITLE_NOTIF_WAITING_CONFIRM_TO_OWNER = 'Masih ada Booking menunggu lho!';
    const MESSAGE_NOTIF_WAITING_CONFIRM_TO_OWNER = 'Segera konfirmasi booking sebelum kadaluarsa';

    const SMS_WAITING_CONFIRM_TODAY_TO_OWNER = 'Booking di Kost Anda masih menunggu konfirmasi. Segera konfirmasi di aplikasi mamikos sebelum kadaluarsa!';
    const TITLE_NOTIF_WAITING_CONFIRM_TODAY_TO_OWNER = 'Jangan biarkan calon penghuni menunggu.';
    const MESSAGE_NOTIF_WAITING_CONFIRM_TODAY_TO_OWNER = 'Ayo konfirmasi booking Anda sekarang sebelum kadaluarsa!';

    const SCHEME_BOOKING_DETAIL = 'list_booking?id=';
    const SCHEME_BOOKING_DETAIL_OWNER = 'booking_detail/';
    const SCHEME_BOOKING_LANDING = 'booking_landing';
    const URL_WEB_BOOKING_LANDING = 'https://mamikos.com/booking';
	const URL_WEB_SEARCH = 'https://mamikos.com/cari';
    const ICON_NOTIF = 'https://mamikos.com/assets/logo%20mamikos_beta_220.png';

    const BOOKING_REQUEST_NOTIFICATION_TYPE = 'booking_request';

    private function __clone() {}
    
    private final function __wakeup() {}

    public static function getInstance()
    {
        if (static::$instance === null){
            static::$instance = new BookingNotificationHelper();
        }
        
        return static::$instance;
    }
    
    public function sendNotif($userId, $template, $toMamipayOwner)
    {
        SendPushNotificationQueue::dispatch($template, $userId, $toMamipayOwner);
    }

    public function notifSuccessBookingToTenant($userId, $bookingId)
    {
        try {
            // Send Notification To User
            $notificationUser = [
                "title"     => $this::TITLE_SUCCESS_BOOKING_TENANT,
                "message"   => $this::MESSAGE_SUCCESS_BOOKING_TENANT,
                "scheme"    => $this::SCHEME_BOOKING_DETAIL . $bookingId,
            ];

            return $this->sendNotif($userId, $notificationUser, false);
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
            return null;
        }
    }

    public function notifCancelBookingToTenant($userId, $bookingId)
    {
        try {
            // Send Notification To User
            $notificationUser = [
                "title"     => $this::TITLE_CANCEL_BOOKING_TENANT,
                "message"   => $this::MESSAGE_CANCEL_BOOKING_TENANT,
                "scheme"    => $this::URL_WEB_BOOKING_LANDING,
            ];

            return $this->sendNotif($userId, $notificationUser, false);
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
            return null;
        }

    }

    public function notifWaitingConfirmToTenant($userId, $bookingId)
    {
        // Send Notification To User
        $notificationUser = [
            "title"     => $this::TITLE_NOTIF_WAITING_CONFIRM_TO_TENANT,
            "message"   => $this::MESSAGE_NOTIF_WAITING_CONFIRM_TO_TENANT,
            "scheme"    => $this::SCHEME_BOOKING_DETAIL . $bookingId,
        ];

        return $this->sendNotif($userId, $notificationUser, false);
    }

    public function smsWaitingConfirmToTenant($room, $phoneNumber)
    {
        $isValid = SMSLibrary::validateIndonesianMobileNumber($phoneNumber);
        if ($isValid) {
            try {
                $message = self::SMS_WAITING_CONFIRM_TO_TENANT;
                SMSLibrary::send(SMSLibrary::phoneNumberCleaning($phoneNumber), $message);
            } catch (Exception $e) {
                Bugsnag::notifyException($e);
                return null;
            }
        }
    }

    public function notifBookingHasPassedToTenant($userId, $bookingId)
    {
        // Send Notification To User
        $notificationUser = [
            "title"     => $this::TITLE_HAS_PASSED_TO_TENANT,
            "message"   => $this::MESSAGE_HAS_PASSED_TO_TENANT,
            "scheme"    => $this::URL_WEB_BOOKING_LANDING,
        ];

        return $this->sendNotif($userId, $notificationUser, false);
    }

    public function notifSuccessBookingToOwner($room, $bookingId)
    {
        try {
            if (!self::isRoomOwnerShouldBeNotified($room)) {
                return null;
            }
            // Send Notification Booking To Owner
            $roomOwner = $room->owners->first();
            if ($roomOwner) {
                // get notification data
                $template = __('notification.submit-booking-owner-notification.real-time', ['bookingId' => $bookingId]);
                $this->sendNotif($roomOwner->user_id, $template, true);

                // Store Notification for Owner
                NotifOwner::NotificationStore([
                    'title'         => isset($template['title']) ? $template['title'] . ' ' : '',
                    'user_id'       => $roomOwner->user_id,
                    'designer_id'   => $room->id,
                    'type'          => self::BOOKING_REQUEST_NOTIFICATION_TYPE,
                    'identifier'    => $bookingId,
                ]);
                return true;
            }
            // @codeCoverageIgnoreStart
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
            return null;
        }
        // @codeCoverageIgnoreEnd
    }

    public function sendSmsSuccessBookingToOwner($room)
    {
        try {
            if (!self::isRoomOwnerShouldBeNotified($room)) {
                return null;
            }
            $roomOwner = $room->owners->first();
            if ($roomOwner) {
                $phoneNumber = $roomOwner->user->phone_number;
                $isValid = SMSLibrary::validateIndonesianMobileNumber($phoneNumber);
                if ($isValid) {
                    $message = "Ada booking baru di " . $room->name . ". Konfirmasi sekarang di aplikasi Mamikos Anda sebelum pesanan kadaluarsa.";
                    SendSMSQueue::dispatch($phoneNumber, $message);
                }
            }
            // @codeCoverageIgnoreStart
        }  catch (Exception $e) {
            Bugsnag::notifyException($e);
            return null;
        }
        // @codeCoverageIgnoreEnd
    }

    public function notifCancelBookingToOwner($room, $bookingId, $cancelReason)
    {
        try {
            if (!self::isRoomOwnerShouldBeNotified($room)) {
                return null;
            }
            // Send Notification Booking To Owner
            $roomOwner = null;
            $roomOwner = CollectionHelper::getFirstInstanceOfCollection($room->owners, RoomOwner::class);
            if ($roomOwner === null) $roomOwner = $room->owners->first();

            if ($roomOwner) {
                $template = [
                    "title"     => $this::TITLE_CANCEL_BOOKING_OWNER,
                    "message"   => $this::MESSAGE_CANCEL_BOOKING_OWNER.$cancelReason,
                    "scheme"    => $this::SCHEME_BOOKING_DETAIL_OWNER . $bookingId,
                ];

                $this->sendNotif($roomOwner->user_id, $template, true);
                // Store Notification for Owner
                return NotifOwner::NotificationStore([
                    "user_id" => $roomOwner->user_id,
                    "designer_id" => $room->id,
                    "type" => "booking_cancelled",
                    "identifier" => $bookingId,
                ]);
            }
            // @codeCoverageIgnoreStart
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
            return null;
        }
        // @codeCoverageIgnoreEnd
    }

    public function sendSmsCancelBookingToOwner($room, $cancelReason)
    {
        try {
            if (!self::isRoomOwnerShouldBeNotified($room)) {
                return null;
            }
            $roomOwner = null;
            $roomOwner = CollectionHelper::getFirstInstanceOfCollection($room->owners, RoomOwner::class);
            if ($roomOwner === null) $roomOwner = $room->owners->first();

            if ($roomOwner) {
                $phoneNumber = $roomOwner->user->phone_number;
                $isValid = SMSLibrary::validateIndonesianMobileNumber($phoneNumber);
                if ($isValid) {
                    $message = "Mohon maaf, permintaan booking untuk kos ".$room->name.". dibatalkan oleh calon penghuni dengan alasan ".$cancelReason;
                    $sms = SMSLibrary::smsVerification($phoneNumber, $message);

                    return $sms;
                }

                return null;
            }
            // @codeCoverageIgnoreStart
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
            return null;
        }
        // @codeCoverageIgnoreEnd
    }

    public function notifWaitingConfirmToOwner($room, $bookingId)
    {
        if (!self::isRoomOwnerShouldBeNotified($room)) {
            return null;
        }
        // Send Notification Booking To Owner
        $roomOwner = $room->owners->first();
        if ($roomOwner) {
            $template = [
                "title"     => $this::TITLE_NOTIF_WAITING_CONFIRM_TO_OWNER,
                "message"   => $this::MESSAGE_NOTIF_WAITING_CONFIRM_TO_OWNER,
                "scheme"    => $this::SCHEME_BOOKING_DETAIL_OWNER . $bookingId,
            ];

            try {
                $this->sendNotif($roomOwner->user_id, $template, true);
                
                // Store Notification for Owner
                NotifOwner::NotificationStore(["user_id" => $roomOwner->user_id, 
                                        "designer_id" => $room->id, 
                                        "type" => "booking_request",
                                        "identifier" => $bookingId,
                                    ]);

                return true;

                // @codeCoverageIgnoreStart
            } catch (Exception $e) {
                Bugsnag::notifyException($e);   
            }
            // @codeCoverageIgnoreEnd
        }
    }

    public function smsWaitingConfirmToOwner($room, $phoneNumber)
    {
        $isValid = SMSLibrary::validateIndonesianMobileNumber($phoneNumber);
        if ($isValid && self::isRoomOwnerShouldBeNotified($room)) {
            try {
                $message = self::SMS_WAITING_CONFIRM_TO_OWNER;
                SMSLibrary::send(SMSLibrary::phoneNumberCleaning($phoneNumber), $message);
            } catch (Exception $e) {
                Bugsnag::notifyException($e);            
            }
        }
    }

    public function notifWaitingConfirmTodayToOwner($room, $bookingId)
    {
        if (!self::isRoomOwnerShouldBeNotified($room)) {
            return null;
        }
        // Send Notification Booking To Owner
        $roomOwner = $room->owners->first();
        if ($roomOwner) {
            $template = [
                "title"     => $this::TITLE_NOTIF_WAITING_CONFIRM_TODAY_TO_OWNER,
                "message"   => $this::MESSAGE_NOTIF_WAITING_CONFIRM_TODAY_TO_OWNER,
                "scheme"    => $this::SCHEME_BOOKING_DETAIL_OWNER . $bookingId,
            ];

            try {
                $this->sendNotif($roomOwner->user_id, $template, true);
                
                // Store Notification for Owner
                NotifOwner::NotificationStore(["user_id" => $roomOwner->user_id, 
                                        "designer_id" => $room->id, 
                                        "type" => "booking_request",
                                        "identifier" => $bookingId,
                                    ]);

                return true;

            } catch (Exception $e) {
                Bugsnag::notifyException($e);   
            }
        }
    }

    public function smsWaitingConfirmTodayToOwner($room, $phoneNumber)
    {
        $isValid = SMSLibrary::validateIndonesianMobileNumber($phoneNumber);
        if ($isValid && self::isRoomOwnerShouldBeNotified($room)) {
            try {
                $message = self::SMS_WAITING_CONFIRM_TODAY_TO_OWNER;
                SMSLibrary::send(SMSLibrary::phoneNumberCleaning($phoneNumber), $message);
            } catch (Exception $e) {
                Bugsnag::notifyException($e);            
            }
        }
    }

    public function notifBookingFinsihedToTenant($userId, $room, $bookingId)
    {
        try {
            // Send Notification To User
            $notificationUser = [
                "title"     => $this::TITLE_FINISH_BOOKING_TENANT,
                "message"   => $this::MESSAGE_FINISH_BOOKING_TENANT,
                "scheme"    => $this::URL_WEB_SEARCH,
            ];

            return $this->sendNotif($userId, $notificationUser, false);
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
        }
    }

    /**
     * Function that determine is owner should be notified or not.
     * Currently calculated by goldplus permissions.
     * Non-GP room owner should always be allowed to receive notification.
     *
     * @param Room $room
     * @return boolean
     */
    public static function isRoomOwnerShouldBeNotified(?Room $room): bool
    {
        if (is_null($room)) {
            return false;
        }
        // Only notify owner if the room has permission to manage booking.
        return GoldplusPermissionHelper::isRoomAllow($room, GoldplusPermission::BOOKING_UPDATE);
    }

}
