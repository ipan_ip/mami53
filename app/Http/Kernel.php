<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \Fruitcake\Cors\HandleCors::class
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            'throttle:60,1',
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
            \App\Http\Middleware\AdCookie::class,
            \App\Http\Middleware\RememberMeTimeout::class,
            \App\Http\Middleware\PageSpeed\CollapseWhitespace::class,
            \App\Http\Middleware\PageSpeed\RemoveComments::class,
            \App\Http\Middleware\FrameGuard::class,
        ],

        'api.v1' => [
            'throttle:60,1',
            'api.basic',
            'api.auth:anyone',
            'api.log',
            'api.input.check'
        ],

        'api.v2' => [
            'throttle:60,1',
            'api.basic',
            'api.encrypt',
            'api.auth:anyone',
            'api.log',
            'api.input.check'
        ],

        'api.agent' => [
            'throttle:60,1',
            'api.basic',
            'api.encrypt_agent',
            //'api.auth:anyone',
            'api.giant',
            'api.log',
            //'api.input.check'
        ],

        'api' => [
            'throttle:60,1',
            'bindings',
        ],

        'api.web' => [
            'throttle:60,1',
            'api.basic',
            'api.log',
        ],

        'api.webhook' => [
            'throttle:1000,1',
            'api.basic',
            'api.log',
        ]
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'api.basic' => \App\Http\Middleware\ApiMiddleware::class,
        'api.auth' => \App\Http\Middleware\ApiAuthMiddleware::class,
        'api.auth.session' => \App\Http\Middleware\ApiSessionAuthMiddleware::class,
        'api.log' => \App\Http\Middleware\ApiLogMiddleware::class,
        'api.encrypt' => \App\Http\Middleware\ApiEncryptMiddleware::class,
        'api.input.check' => \App\Http\Middleware\Api\InputCheckMiddleware::class,
        'api.guest' => \App\Http\Middleware\Api\GuestMiddleware::class,
        'api.track.search' => \App\Http\Middleware\Api\TrackSearchMiddleware::class,

        'api.giant' => \App\Http\Middleware\Giant\AgentCheckLogin::class,
        'api.encrypt_agent' => \App\Http\Middleware\Giant\ApiEncryptAgentMiddleware::class,

        'auth' => \Illuminate\Auth\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'throttle' => \App\Http\Middleware\MamikosThrottleRequests::class,
        'role' => \Zizaco\Entrust\Middleware\EntrustRole::class,
        'permission' => \Zizaco\Entrust\Middleware\EntrustPermission::class,
        'ability' => \Zizaco\Entrust\Middleware\EntrustAbility::class,

        'access.ip' => \App\Http\Middleware\IPGuard::class,

        'oauth.client.credential' => \Laravel\Passport\Http\Middleware\CheckClientCredentials::class,

        'webview.auth' => \App\Http\Middleware\WebviewAuthMiddleware::class
    ];
}
