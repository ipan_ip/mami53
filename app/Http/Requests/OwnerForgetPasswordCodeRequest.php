<?php

namespace App\Http\Requests;

use App\Http\Helpers\RegexHelper;
use Illuminate\Contracts\Validation\Validator;
use App\Http\Requests\MamikosApiFormRequest;

class OwnerForgetPasswordCodeRequest extends MamikosApiFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'phone_number' => 'required|min:8|max:14|regex:' . RegexHelper::phoneNumber(),
        ];
    }

    /**
     *  Validation message for each validation error
     *
     *  @return array
     */
    public function messages(): array
    {
        return [
            'phone_number.required' => __('api.input.phone_number.required'),
            'phone_number.min' => __('api.input.phone_number.min'),
            'phone_number.max' => __('api.input.phone_number.max'),
            'phone_number.unique' => __('api.input.phone_number.unique'),
            'phone_number.regex' => __('api.input.phone_number.regex'),
            'phone_number.numeric' => __('api.input.phone_number.invalid'),
        ];
    }
}
