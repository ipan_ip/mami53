<?php
namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use App\Http\Requests\MamikosApiFormRequest;
use App\Entities\Owner\Goldplus\GoldplusStatisticType;

class GoldplusStatisticRequest extends MamikosApiFormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $validFilter = $isValidFilter = [
            GoldplusStatisticType::CHAT, 
            GoldplusStatisticType::VISIT,
            GoldplusStatisticType::UNIQUE_VISIT,
            GoldplusStatisticType::FAVORITE
        ];
        return [
            'filter_list' => 'required|string|in:'.implode(',', $validFilter),
        ];
    }

    /**
     *  Validation message for each validation error
     *
     *  @return array
     */
    public function messages(): array
    {
        return [
            'filter_list.required'  => __('goldplus-statistic.error.filter.required'),
            'filter_list.string'    => __('goldplus-statistic.error.filter.filter_not_valid'),
            'filter_list.in'        => __('goldplus-statistic.error.filter.selected_filter_not_valid')
        ];
    }

}
