<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use App\Http\Requests\MamikosApiFormRequest;

class ModifyAdditionalCostRequest extends MamikosApiFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'additional_costs.*.cost_type' => 'sometimes|in:fixed,other',
            'additional_costs.*.action' => 'required|in:CREATE,UPDATE,DELETE'
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator(Validator $validator)
    {
        $this->additional_costs = collect($this->additional_costs);

        $validator->after(function ($validator) {
            $this->validateCreateRequest($validator);
            $this->validateUpdateRequest($validator);
            $this->validateDeleteRequest($validator);
        });
    }

    /**
     *  Validate create request
     *
     *  @param Validator $validator
     *
     *  @return void
     */
    private function validateCreateRequest(Validator $validator)
    {
        $this->additional_costs->where('action', 'CREATE')->each(function ($item, $key) use ($validator) {
            if (!isset($item['invoice_id']) || empty($item['invoice_id'])) {
                $validator->errors()->add('invoice_id', __('api.invoice_id.required'));
            }

            if (!isset($item['cost_value']) || empty($item['cost_value'])) {
                $validator->errors()->add('cost_value', __('api.cost_value.required'));
            }

            if (!isset($item['cost_title']) || empty($item['cost_title'])) {
                $validator->errors()->add('cost_title', __('api.cost_title.required'));
            }

            if (!isset($item['cost_type']) || empty($item['cost_type'])) {
                $validator->errors()->add('cost_type', __('api.cost_type.required'));
            }
        });
    }

    /**
     *  Validate update request
     *
     *  @param Validator $validator
     *
     *  @return void
     */
    private function validateUpdateRequest(Validator $validator)
    {
        $this->additional_costs->where('action', 'UPDATE')->each(function ($item, $key) use ($validator) {
            if (!isset($item['cost_value']) || empty($item['cost_value'])) {
                $validator->errors()->add('cost_value', __('api.input.cost_value.required'));
            }

            if (!isset($item['additional_cost_id']) || empty($item['additional_cost_id'])) {
                $validator->errors()->add('additional_cost_id', __('api.input.additional_cost_id.required'));
            }
        });
    }

    /**
     *  Validate delete request
     *
     *  @param Validator $validator
     *
     *  @return void
     */
    private function validateDeleteRequest(Validator $validator)
    {
        $this->additional_costs->where('action', 'DELETE')->each(function ($item, $key) use ($validator) {
            if (!isset($item['additional_cost_id']) || empty($item['additional_cost_id'])) {
                $validator->errors->add('additional_cost_id', __('api.additional_cost_id.required'));
            }
        });
    }
}
