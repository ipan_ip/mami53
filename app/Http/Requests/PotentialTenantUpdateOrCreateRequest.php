<?php

namespace App\Http\Requests;

use App\Entities\Consultant\PotentialTenantRemarks;
use App\Entities\Mamipay\MamipayContract;
use Illuminate\Contracts\Validation\Validator;
use App\Http\Requests\MamikosApiFormRequest;
use Illuminate\Validation\Rule;

class PotentialTenantUpdateOrCreateRequest extends MamikosApiFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        // TODO: Currently other fields are not validated. In the future it should be added.
        return [
            'remarks_category' => [
                'sometimes',
                'required',
                'string',
                Rule::in([
                    PotentialTenantRemarks::REMARKS_MESSAGE_FOR_ADMIN,
                    PotentialTenantRemarks::REMARKS_HAS_ACTIVE_CONTRACT,
                    PotentialTenantRemarks::REMARKS_HAS_BOOKING_DATA,
                    PotentialTenantRemarks::REMARKS_HAS_PAID_TO_OWNER,
                    PotentialTenantRemarks::REMARKS_REJECT_PAYMENT_TO_MAMIPAY,
                    PotentialTenantRemarks::REMARKS_OWNER_REJECT_PAYMENT_TO_MAMIPAY,
                    PotentialTenantRemarks::REMARKS_DOES_NOT_INHABIT_ROOM,
                    PotentialTenantRemarks::REMARKS_WANT_TO_CONFIRM_TO_OWNER,
                    PotentialTenantRemarks::REMARKS_PHONE_NUMBER_UNREACHABLE,
                    PotentialTenantRemarks::REMARKS_CHAT_UNRESPONSIVE,
                    PotentialTenantRemarks::REMARKS_OTHER,
                ])
            ],
            'remarks_note' => 'sometimes|required|string|max:200',
            'duration_unit' => [
                'required',
                Rule::in([
                    MamipayContract::UNIT_3MONTH,
                    MamipayContract::UNIT_6MONTH,
                    MamipayContract::UNIT_DAY,
                    MamipayContract::UNIT_MONTH,
                    MamipayContract::UNIT_WEEK,
                    MamipayContract::UNIT_YEAR
                ])
            ],
            'scheduled_date' => 'required|date'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'remarks_note.max' => __('api.input.remarks_note.max')
        ];
    }
}
