<?php

namespace App\Http\Requests;

use App\Entities\Activity\ActivationCode;
use App\Entities\Activity\ActivationCodeType;
use App\Http\Helpers\RegexHelper;
use Carbon\Carbon;
use Illuminate\Contracts\Validation\Validator;
use App\Http\Requests\MamikosApiFormRequest;

class TenantForgetPasswordRequest extends MamikosApiFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'phone_number' => 'required|min:8|max:14|regex:' . RegexHelper::phoneNumber(),
            'password' => 'required|string|confirmed',
            'verification_code' => 'required|alpha_num|size:4',
        ];
    }

    /**
     *  Validation message for each validation error
     *
     *  @return array
     */
    public function messages(): array
    {
        return [
            'phone_number.required' => __('api.input.phone_number.required'),
            'phone_number.min' => __('api.input.phone_number.min'),
            'phone_number.max' => __('api.input.phone_number.max'),
            'phone_number.unique' => __('api.input.phone_number.unique'),
            'phone_number.regex' => __('api.input.phone_number.regex'),
            'phone_number.numeric' => __('api.input.phone_number.invalid'),
            'password.required' => __('api.input.password.required'),
            'password.string' => __('api.input.password.string'),
            'password.min' => __('api.input.password.min'),
            'password.max' => __('api.input.password.max'),
            'verification_code.required'     => __('api.input.code.required'),
            'verification_code.size'         => __('api.owner.verification.code.invalid'),
            'verification_code.alpha_num'    => __('api.owner.verification.code.invalid'),
        ];
    }

    /**
     *  Add validation for verification code
     *
     *  @param Validator $validator
     *
     *  @return void
     */
    public function withValidator(Validator $validator)
    {
        $activationCode = ActivationCode::where([
            ['phone_number', '=', $this->get('phone_number')],
            ['for', '=', ActivationCodeType::TENANT_FORGET_PASSWORD]
        ])
            ->whereCode($this->input('verification_code', ''))
            ->whereNotExpired()
            ->first();

        $validator->after(function ($validator) use ($activationCode) {
            if (is_null($activationCode)) {
                $validator->errors()->add('verification_code', __('api.owner.verification.code.invalid'));
            }
        });
    }
}
