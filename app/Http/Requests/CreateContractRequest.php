<?php

namespace App\Http\Requests;

use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayTenant;
use App\Http\Helpers\RegexHelper;
use Illuminate\Contracts\Validation\Validator;
use App\Http\Requests\MamikosApiFormRequest;

/**
 *  Validate contract creation request
 *
 *  @property $tenant
 */
class CreateContractRequest extends MamikosApiFormRequest
{
    protected $tenant;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $this->tenant = MamipayTenant::wherePhoneNumber($this->phone_number)->first();

        if (!is_null($this->tenant)) {
            return $this->getMamipayTenantValidationRules();
        }

        return $this->getPotentialAndNewTenantValidationRules();
    }

    /**
     *  Validation message for each validation error
     *
     *  @return array
     */
    public function messages(): array
    {
        return [
            'email.required' => __('api.input.email.required'),
            'email.email' => __('api.input.email.email'),
            'email.unique' => __('api.input.email.unique'),
            'phone_number.required' => __('api.input.phone_number.required'),
            'phone_number.min' => __('api.input.phone_number.min'),
            'phone_number.max' => __('api.input.phone_number.max'),
            'phone_number.unique' => __('api.input.phone_number.unique'),
            'phone_number.regex' => __('api.input.phone_number.regex'),
            'name.required' => __('api.input.name.required'),
            'name.string' => __('api.input.name.alpha'),
            'name.min' => __('api.input.name.min'),
            'name.max' => __('api.input.name.max'),
            'name.regex' => __('api.input.name.alpha'),
            'gender.required' => __('api.input.gender.required'),
            'gender.string' => __('api.input.gender.string'),
            'gender.in' => __('api.input.gender.in'),
            'occupation.required' => __('api.input.occupation.required'),
            'occupation.string' => __('api.input.occupation.string'),
            'occupation.in' => __('api.input.occupation.in'),
            'marital_status.required' => __('api.input.marital_status.required'),
            'marital_status.string' => __('api.input.marital_status.string'),
            'marital_status.in' => __('api.input.marital_status.in'),
            'parent_name.required_if' => __('api.input.parent_name.required_if'),
            'parent_name.string' => __('api.input.parent_name.string'),
            'parent_name.min' => __('api.input.parent_name.min'),
            'parent_name.max' => __('api.input.parent_name.max'),
            'parent_name.regex' => __('api.input.parent_name.regex'),
            'parent_phone_number.required_if' => __('api.input.parent_phone_number.required_if'),
            'parent_phone_number.min' => __('api.input.parent_phone_number.min'),
            'parent_phone_number.max' => __('api.input.parent_phone_number.max'),
            'parent_phone_number.regex' => __('api.input.parent_phone_number.regex'),
            'room_number.required' => __('api.input.room_number.required'),
            'room_number.string' => __('api.input.room_number.string'),
            'start_date.required' => __('api.input.start_date.required'),
            'start_date.date_format' => __('api.input.start_date.date_format'),
            'rent_type.required' => __('api.input.rent_type.required'),
            'rent_type.string' => __('api.input.rent_type.string'),
            'rent_type.in' =>  __('api.input.rent_type.in'),
            'amount.required' => __('api.input.amount.required'),
            'amount.numeric' => __('api.input.amount.numeric'),
            'amount.min' => __('api.input.amount.min'),
            'duration.required' => __('api.input.duration.required'),
            'duration.integer' => __('api.input.duration.integer'),
            'duration.min' => __('api.input.duration.min'),
            'fine_amount.numeric' => __('api.input.fine_amount.numeric'),
            'fine_maximum_length.integer' => __('api.input.fine_maximum_length.integer'),
            'fine_duration_type.string' => __('api.input.fine_duration_type.string'),
            'fine_duration_type.in' => __('api.input.fine_duration_type.in'),
            'additional_costs.array' => __('api.input.additional_costs.array'),
            'additional_costs.*.field_title.required' => __('api.input.additional_costs.*.field_title.required'),
            'additional_costs.*.field_title.string' => __('api.input.additional_costs.*.field_title.string'),
            'additional_costs.*.field_title.min' => __('api.input.additional_costs.*.field_title.min'),
            'additional_costs.*.field_title.max' => __('api.input.additional_costs.*.field_title.max'),
            'additional_costs.*.field_value.required' => __('api.input.additional_costs.*.field_value.required'),
            'additional_costs.*.field_value.numeric' => __('api.input.additional_costs.*.field_value.numeric'),
            'additional_costs.*.cost_type.sometimes' => __('api.input.additional_costs.*.cost_type.sometimes'),
            'additional_costs.*.cost_type.required' => __('api.input.additional_costs.*.cost_type.required'),
            'additional_costs.*.cost_type.string' => __('api.input.additional_costs.*.cost_type.string'),
            'additional_costs.*.cost_type.in' => __('api.input.additional_costs.*.cost_type.in')
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $hasContract = false;

            if (!is_null($this->tenant)) {
                $contractsCount = $this->tenant
                    ->contracts()
                    ->where(function ($query) {
                        $query->where('status', MamipayContract::STATUS_ACTIVE)
                            ->orWhere('status', MamipayContract::STATUS_BOOKED);
                    })
                    ->count();

                $hasContract = ($contractsCount > 0);
            }

            if (!is_null($this->tenant) && $hasContract) {
                // Error message below should never be returned by frontend. Since the flow should block this case before creating contract.
                $validator->errors()->add('phone_number', 'Tenant already has active or booked contract');
            }
        });
    }

    /**
     *  Validation rules when creating new contract for potential and new tenant
     * 
     *  @return array
     */
    private function getPotentialAndNewTenantValidationRules(): array
    {
        $rules = $this->getMamipayTenantValidationRules();
        $rules['phone_number'] .= '|unique:mamipay_tenant,phone_number';

        return $rules;
    }

    /**
     *  Validation rules when creating new contract for existing tenant
     * 
     *  @return array
     */
    private function getMamipayTenantValidationRules(): array
    {
        return [
            'email' => 'required|email',
            'phone_number' => 'required|min:8|max:14|regex:' . RegexHelper::phoneNumber(),
            'name' => 'required|string|min:3|max:255|regex:' . RegexHelper::name(),
            'gender' => 'required|string|in:male,female',
            'occupation' => 'required|string|in:Mahasiswa,Karyawan,Lainnya',
            'marital_status' => 'required|string|in:Kawin,Belum Kawin,Kawin dan Memiliki Anak',
            'parent_name' => $this->getParentNameRules(),
            'parent_phone_number' => $this->getParentPhoneNumberRules(),
            'photo_identifier_id' => 'required|numeric|exists:mamipay_media,id',
            'photo_document_id' => 'required|numeric|exists:mamipay_media,id',
            'room_id' => 'required|integer|exists:designer,song_id',
            'room_number' => 'required|string',
            'start_date' => 'required|date_format:Y-m-d',
            'rent_type' => 'required|string|in:day,week,month,3_month,6_month,year',
            'amount' => 'required|numeric|min:10000',
            'duration' => 'required|integer|min:1',
            'fine_amount' => $this->getFineAmountRules(),
            'fine_maximum_length' => 'nullable|integer', // Denda setelah jml waktu
            'fine_duration_type' => 'nullable|string|in:day,week,month', // Unit waktu denda
            'additional_costs' => 'nullable|array',
            'additional_costs.*.field_title' => 'sometimes|required|string|min:1|max:255',
            'additional_costs.*.field_value' => 'sometimes|required|numeric',
            'additional_costs.*.cost_type'  => 'sometimes|required|string|in:fixed,other'
        ];
    }

    private function getParentNameRules()
    {
        $occupation = $this->input('occupation');

        switch ($occupation) {
            case 'Mahasiswa':
                return 'required_if:occupation,Mahasiswa|string|min:3|max:255|regex:' . RegexHelper::name();
            default:
                return 'required_if:occupation,Mahasiswa';
        }
    }

    private function getParentPhoneNumberRules()
    {
        $occupation = $this->input('occupation');

        switch ($occupation) {
            case 'Mahasiswa':
                return 'required_if:occupation,Mahasiswa|min:8|max:13|regex:' . RegexHelper::phoneNumber();
            default:
                return 'required_if:occupation,Mahasiswa';
        }
    }

    private function getFineAmountRules()
    {
        if (empty($this->input('fine_amount'))) {
            return 'nullable';
        }

        return 'nullable|numeric';
    }
}
