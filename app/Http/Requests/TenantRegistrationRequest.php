<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use App\Http\Requests\MamikosApiFormRequest;
use App\Entities\Activity\ActivationCode;
use App\Entities\Activity\ActivationCodeType;
use App\Http\Helpers\RegexHelper;
use App\Rules\GoogleRecaptcha;
use Carbon\Carbon;

class TenantRegistrationRequest extends MamikosApiFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     *  Validation rules for this request
     *
     *  @return array
     */
    public function rules(): array
    {
        $rules = [
            'email' => 'required|email|unique:user,email',
            'name' => 'required|string|min:3|max:50|regex:' . RegexHelper::name(),
            'password' => 'required|string',
            'phone_number' => 'required|min:8|max:14|unique:user,phone_number|regex:' . RegexHelper::phoneNumber(),
            'verification_code' => 'required|alpha_num|size:4',
        ];

        if ($this->filled('_token')) {
            $rules += ['g-recaptcha-response' => ['required', new GoogleRecaptcha()]]; //
        }

        return $rules;
    }

    /**
     *  Validation message for each validation error
     *
     *  @return array
     */
    public function messages(): array
    {
        return [
            'email.required' => __('api.input.email.required'),
            'email.email' => __('api.input.email.email'),
            'email.unique' => __('api.input.email.unique'),
            'name.required' => __('api.input.name.required'),
            'name.alpha' => __('api.input.name.alpha'),
            'name.min' => __('api.input.name.min'),
            'name.max' => __('api.input.name.max'),
            'name.regex' => __('api.input.name.alpha'),
            'password.required' => __('api.input.password.required'),
            'password.string' => __('api.input.password.string'),
            'password.min' => __('api.input.password.min'),
            'password.max' => __('api.input.password.max'),
            'phone_number.required' => __('api.input.phone_number.required'),
            'phone_number.min' => __('api.input.phone_number.min'),
            'phone_number.max' => __('api.input.phone_number.max'),
            'phone_number.unique' => __('api.input.phone_number.unique'),
            'phone_number.regex' => __('api.input.phone_number.regex'),
            'phone_number.numeric' => __('api.input.phone_number.invalid'),
            'verification_code.required'     => __('api.input.code.required'),
            'verification_code.size'         => __('api.owner.verification.code.invalid'),
            'verification_code.alpha_num'    => __('api.owner.verification.code.invalid'),
        ];
    }

    /**
     *  Override of failedValidation
     *
     *  This method is overriden to prevent FormRequest from throwing error.
     *
     *  @param Validator $validator
     *
     *  @return void
     */
    protected function failedValidation(Validator $validator)
    {
        parent::failedValidation($validator);

        $messages = [];

        foreach ($validator->errors()->getMessages() as $key => $message) {
            $messages[$key] = $message[0];
        }

        $this->errors = $messages;
    }

    /**
     *  Add validation for verification code
     *
     *  @param Validator $validator
     *
     *  @return void
     */
    public function withValidator(Validator $validator)
    {
        $activationCode = ActivationCode::where([
            ['phone_number', '=', $this->get('phone_number')],
            ['for', '=', ActivationCodeType::TENANT_VERIFICATION]
        ])
            ->whereCode($this->input('verification_code', ''))
            ->whereNotExpired()
            ->first();
        
        $validator->after(function ($validator) use ($activationCode) {
            if (is_null($activationCode)) {
                $validator->errors()->add('verification_code', __('api.owner.verification.code.invalid'));
            }
        });
    }
}
