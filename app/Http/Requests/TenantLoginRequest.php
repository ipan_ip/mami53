<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Database\Eloquent\Builder;
use App\Http\Requests\MamikosApiFormRequest;
use App\Http\Helpers\RegexHelper;
use App\User;

class TenantLoginRequest extends MamikosApiFormRequest
{
    /**
     *
     *  @var User $user
     */
    public $user;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'phone_number'  => 'required_without:email|string|min:8|max:14|regex:' . RegexHelper::phoneNumber(),
            'email'         => 'required_without:phone_number|email',
            'password'      => 'required|string'
        ];
    }

    /**
     *  Validation message for each validation error
     *
     *  @return array
     */
    public function messages(): array
    {
        return [
            'phone_number.required_without' => __('api.input.phone_number.required'),
            'phone_number.string'   => __('api.input.phone_number.invalid'),
            'phone_number.min'      => __('api.input.phone_number.min'),
            'phone_number.max'      => __('api.input.phone_number.max'),
            'phone_number.regex'    => __('api.input.phone_number.regex'),
            'phone_number.exists'   => __('api.auth.owner.phone_number.invalid'),
            'email.required_without' => __('api.input.email.required'),
            'email.email'            => __('api.input.email.email'),
            'email.exists'          => __('api.auth.owner.email.invalid'),
            'password.required'     => __('api.input.password.required'),
            'password.string'       => __('api.input.password.string'),
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator(Validator $validator)
    {
        $validator->after(function ($validator) {
            $user = User::where('is_owner', 'false');

            if ($this->filled('email')) {
                $user = $user->where('email', $this->email);
                $this->handleInsecureLogin($validator, $user);
                $user = $user->first();

                if (is_null($user)) {
                    $validator->errors()->add('phone_number', __('api.auth.tenant.email.invalid'));
                }
            } elseif ($this->filled('phone_number')) {
                $user = $user->where('phone_number', $this->phone_number);
                $this->handleInsecureLogin($validator, $user);
                $user = $user->first();

                if (is_null($user)) {
                    $validator->errors()->add('phone_number', __('api.auth.tenant.phone_number.invalid'));
                }
            }
            
            $this->user = $user;
        });
    }

    /**
     *  Prevent user with the same email or phone number from logging in
     *
     *  In the past, request validation was buggy and it was possible to create double account by
     *  logging in using facebook THEN google or vice versa. This create a security problem for us.
     *
     *  @param Validator $validator
     *  @param User $user
     *
     *  @return void
     */
    private function handleInsecureLogin(Validator $validator, Builder $user)
    {
        $duplicateRow = $user->count() > 1;

        if ($duplicateRow && $this->filled('email')) {
            $validator->errors()->add('email', __('api.auth.tenant.email.forbidden'));
        } else if ($duplicateRow && $this->filled('phone_number')) {
            $validator->errors()->add('phone_number', __('api.auth.tenant.phone_number.forbidden'));
        }
    }
}
