<?php

namespace App\Http\Requests;

use App\Http\Helpers\RegexHelper;
use Illuminate\Contracts\Validation\Validator;
use App\Http\Requests\MamikosApiFormRequest;

class OwnerForgetPasswordCheckRequest extends MamikosApiFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'phone_number' => 'required|min:8|max:14|regex:' . RegexHelper::phoneNumber(),
            'verification_code' => 'required|alpha_num|size:4',
        ];
    }

    /**
     *  Validation message for each validation error
     *
     *  @return array
     */
    public function messages(): array
    {
        return [
            'phone_number.required' => __('api.input.phone_number.required'),
            'phone_number.min' => __('api.input.phone_number.min'),
            'phone_number.max' => __('api.input.phone_number.max'),
            'phone_number.unique' => __('api.input.phone_number.unique'),
            'phone_number.regex' => __('api.input.phone_number.regex'),
            'phone_number.numeric' => __('api.input.phone_number.invalid'),
            'verification_code.required'     => __('api.input.code.required'),
            'verification_code.size'         => __('api.owner.verification.code.invalid'),
            'verification_code.alpha_num'    => __('api.owner.verification.code.invalid'),
        ];
    }
}
