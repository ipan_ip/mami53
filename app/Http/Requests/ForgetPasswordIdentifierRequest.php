<?php

namespace App\Http\Requests;

use App\Http\Helpers\RegexHelper;
use App\Http\Requests\MamikosApiFormRequest;

class ForgetPasswordIdentifierRequest extends MamikosApiFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'phone_number' => 'required|string|min:8|max:14|regex:' . RegexHelper::phoneNumber() . '|exists:user,phone_number',
            // 'email' => 'required_without:phone_number|string|email|exists:user,email', // Not yet enabled
        ];
    }

    /**
     *  Validation message for each validation error
     *
     *  @return array
     */
    public function messages(): array
    {
        return [
            // 'phone_number.required_without' => __('api.input.phone_number.required_without'),
            'phone_number.required' => __('api.input.phone_number.required'),
            'phone_number.min' => __('api.input.phone_number.min'),
            'phone_number.max' => __('api.input.phone_number.max'),
            'phone_number.regex' => __('api.input.phone_number.regex'),
            'phone_number.string' => __('api.input.phone_number.invalid'),
            'phone_number.exists' => __('api.input.phone_number.exists'),

            'email.required_without' => __('api.input.email.required_without'),
            'email.email' => __('api.input.email.format'),
            'email.string' => __('api.input.email.email'),
            'email.exists' => __('api.input.email.exists'),
        ];
    }
}
