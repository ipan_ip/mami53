<?php

namespace App\Http\Requests;

use App\Entities\Consultant\ActivityManagement\ActivityFunnel;
use App\Entities\Consultant\ActivityManagement\ActivityProgress;
use App\Entities\Consultant\PotentialOwner;
use App\Entities\Consultant\PotentialProperty;
use App\Entities\Consultant\PotentialTenant;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Room\Room;
use Illuminate\Support\Facades\Auth;

class TasksActivationRequest extends MamikosApiFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'funnel_id' => 'required|exists:consultant_activity_funnel,id',
            'tasks' => 'required|array',
            'tasks.*' => 'required|numeric|integer'
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $funnelId = $this->funnel_id;
            $funnel = ActivityFunnel::find($funnelId);
            $consultant = Auth::user()->consultant;
            $consultantId = !is_null($consultant) ? $consultant->id : null;

            if (!is_null($funnel) && !empty($this->tasks) && (gettype($this->tasks) === 'array')) {
                switch ($funnel->funnelable_type) {
                    case ActivityProgress::MORPH_TYPE_CONTRACT:
                        $count = MamipayContract::whereIn('id', $this->tasks)
                            ->whereDoesntHave('progress', function ($progress) use ($funnelId, $consultantId) {
                                $progress->where('consultant_activity_funnel_id', $funnelId)
                                    ->where('consultant_id', $consultantId);
                            })
                            ->count();
                        break;
                    case ActivityProgress::MORPH_TYPE_DBET:
                        $count = PotentialTenant::whereIn('id', $this->tasks)
                            ->whereDoesntHave('progress', function ($progress) use ($funnelId, $consultantId) {
                                $progress->where('consultant_activity_funnel_id', $funnelId)
                                    ->where('consultant_id', $consultantId);
                            })
                            ->count();
                        break;
                    case ActivityProgress::MORPH_TYPE_PROPERTY:
                        $count = Room::whereIn('id', $this->tasks)
                            ->whereDoesntHave('progress', function ($progress) use ($funnelId, $consultantId) {
                                $progress->where('consultant_activity_funnel_id', $funnelId)
                                    ->where('consultant_id', $consultantId);
                            })
                            ->count();
                        break;
                    case ActivityProgress::MORPH_TYPE_POTENTIAL_PROPERTY:
                        $count = PotentialProperty::whereIn('id', $this->tasks)
                            ->whereDoesntHave('progress', function ($progress) use ($funnelId, $consultantId) {
                                $progress->where('consultant_activity_funnel_id', $funnelId)
                                    ->where('consultant_id', $consultantId);
                            })
                            ->count();
                        break;
                    case ActivityProgress::MORPH_TYPE_POTENTIAL_OWNER:
                        $count = PotentialOwner::whereIn('id', $this->tasks)
                            ->whereDoesntHave('progress', function ($progress) use ($funnelId, $consultantId) {
                                $progress->where('consultant_activity_funnel_id', $funnelId)
                                    ->where('consultant_id', $consultantId);
                            })
                            ->count();
                        break;
                    default:
                        $validator->errors()->add('tasks.*', 'Invalid task id');
                        return;
                }

                if ($count !== count($this->tasks)) {
                    $validator->errors()->add('tasks.*', 'Invalid task id');
                }
            }
        });
    }
}
