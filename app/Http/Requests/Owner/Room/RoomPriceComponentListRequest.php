<?php

namespace App\Http\Requests\Owner\Room;

use App\Http\Requests\MamikosApiFormRequest;
use App\Repositories\RoomRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\Validator;

class RoomPriceComponentListRequest extends MamikosApiFormRequest
{
    protected $room;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    public function rules()
    {
        return [];
    }

    public function messages()
    {
        return [];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            // validate room data
            $this->validateRoomFromPath($validator);
        });
    }

    /**
     * Validate room data
     * @param Validator $validator
     * @return void
     */
    private function validateRoomFromPath(Validator $validator)
    {
        try {
            $repository = app()->make(RoomRepository::class);
            $data = $repository->getRoomWithOwnerBySongId($this->route('songId'));

            if (!$data)
                $validator->errors()->add('songId', 'Kos tidak ditemukan');

            $this->room = $data;
        } catch (ModelNotFoundException $e) {
            $validator->errors()->add('songId', 'Kos tidak ditemukan');
        }
    }

    /**
     * @return mixed
     */
    public function getRoom()
    {
        return $this->room;
    }
}
