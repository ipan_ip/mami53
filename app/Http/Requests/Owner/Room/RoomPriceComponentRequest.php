<?php

namespace App\Http\Requests\Owner\Room;

use App\Entities\Mamipay\MamipayRoomPriceComponent;
use App\Entities\Room\Room;
use App\Enums\Mamipay\MamipayRoomPriceComponentType;
use App\Http\Requests\MamikosApiFormRequest;
use App\Repositories\Mamipay\MamipayRoomPriceComponentRepository;
use App\Repositories\RoomRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Validator;

class RoomPriceComponentRequest extends MamikosApiFormRequest
{
    const METHOD_POST   = 'POST';
    const METHOD_PUT    = 'PUT';

    protected $room, $data;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    public function rules()
    {
        return [
            'type' => [
                'required',
                Rule::in([
                    MamipayRoomPriceComponentType::ADDITIONAL,
                    MamipayRoomPriceComponentType::FINE,
                    MamipayRoomPriceComponentType::DEPOSIT,
                    MamipayRoomPriceComponentType::DP
                ])
            ],
            'name' => [
                Rule::requiredIf(function (){
                    return $this->type === MamipayRoomPriceComponentType::ADDITIONAL;
                }),
                'string'
            ],
            'price' => [
                Rule::requiredIf(function () {
                    return in_array($this->type, [
                        MamipayRoomPriceComponentType::ADDITIONAL,
                        MamipayRoomPriceComponentType::FINE,
                        MamipayRoomPriceComponentType::DEPOSIT
                    ]);
                }),
                'integer',
            ],
            'percentage' => [
                Rule::requiredIf(function (){
                    return $this->type == MamipayRoomPriceComponentType::DP;
                }),
                'integer'
            ],
            'fine_maximum_length' => [
                Rule::requiredIf(function (){
                    return $this->type == MamipayRoomPriceComponentType::FINE;
                }),
                'integer'
            ],
            'fine_duration_type' => [
                Rule::requiredIf(function (){
                    return $this->type == MamipayRoomPriceComponentType::FINE;
                }),
                Rule::in(['day', 'week', 'month']),
                'string'
            ]
        ];
    }

    public function messages()
    {
        return [
            'type.required'                 => 'Tipe tidak boleh kosong',
            'type.in'                       => 'Tipe tidak terdaftar',
            'name.string'                   => 'Nama tidak sesuai karakter yang di ijinkan',
            'name.required'                 => 'Nama tidak boleh kosong',
            'price.integer'                 => 'Harga harus berupa angka',
            'price.required'                => 'Harga tidak boleh kosong',
            'percentage.integer'            => 'Persentase harus berupa angka',
            'percentage.required'           => 'Persentase tidak boleh kosong',
            'fine_maximum_length.integer'   => 'Maksimal rentang denda harus berupa angka',
            'fine_maximum_length.required'  => 'Maksimal rentang denda tidak boleh kosong',
            'fine_duration_type.string'     => 'Durasi tipe tidak sesuai karakter yang di ijinkan',
            'fine_duration_type.required'   => 'Durasi tipe tidak boleh kosong',
            'fine_duration_type.in'         => 'Durasi tipe tidak terdaftar'
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            if ($this->method() === self::METHOD_POST) {
                // validate room data
                $this->validateRoomFromPath($validator);
            } elseif ($this->method() === self::METHOD_PUT) {
                // validate room price data
                $this->validatePriceComponentData($validator);
            }
        });
    }

    /**
     * Validate room data
     * @param Validator $validator
     * @return void
     */
    private function validateRoomFromPath(Validator $validator)
    {
        try {
            $repository = app()->make(RoomRepository::class);
            $data = $repository->getRoomWithOwnerBySongId($this->route('songId'));

            if (!$data)
                $validator->errors()->add('songId', 'Kos tidak ditemukan');

            $this->room = $data;
        } catch (ModelNotFoundException $e) {
            $validator->errors()->add('songId', 'Kos tidak ditemukan');
        }
    }

    /**
     * Validate room price component
     * @param Validator $validator
     * @return void
     */
    private function validatePriceComponentData(Validator $validator)
    {
        try {
            $repository = app()->make(MamipayRoomPriceComponentRepository::class);
            $data = $repository->find($this->route('priceComponentId'));

            $this->data = $data;
        } catch (ModelNotFoundException $e) {
            $validator->errors()->add('priceComponentId', 'Data tidak ditemukan');
        }
    }

    /**
     * @return mixed
     */
    public function getRoom()
    {
        return $this->room;
    }

    /**
     * @return MamipayRoomPriceComponent|null
     */
    public function getData(): ?MamipayRoomPriceComponent
    {
        return $this->data;
    }
}
