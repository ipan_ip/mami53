<?php

namespace App\Http\Requests\Owner\Room;

use App\Enums\Mamipay\MamipayRoomPriceComponentType;
use App\Http\Requests\MamikosApiFormRequest;
use App\Repositories\RoomRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Validator;

class RoomPriceComponentSwitchRequest extends MamikosApiFormRequest
{
    protected $room;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    public function rules()
    {
        return [
            'type' => [
                'required',
                Rule::in([
                    MamipayRoomPriceComponentType::ADDITIONAL,
                    MamipayRoomPriceComponentType::FINE,
                    MamipayRoomPriceComponentType::DEPOSIT,
                    MamipayRoomPriceComponentType::DP
                ])
            ],
            'songId' => [
                'required'
            ],
            'active' => [
                'required',
                'integer',
                Rule::in([1, 0])
            ]
        ];
    }

    public function messages()
    {
        return [
            'type.required'     => 'Tipe tidak boleh kosong',
            'type.in'           => 'Tipe tidak terdaftar',
            'songId.required'   => 'Kos tidak boleh kosong',
            'active.required'   => 'Active tidak boleh kosong',
            'active.integer'    => 'Active harus berupa angka',
            'active.in'         => 'Data active tidak terdaftar'
        ];
    }

    public function all($keys = null)
    {
        $data = parent::all();
        $data['type'] = $this->route('type');
        $data['songId'] = $this->route('songId');

        return $data;
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            // validate room data
            $this->validateRoomFromPath($validator);
        });
    }

    /**
     * Validate room data
     * @param Validator $validator
     * @return void
     */
    private function validateRoomFromPath(Validator $validator)
    {
        try {
            $repository = app()->make(RoomRepository::class);
            $data = $repository->getRoomWithOwnerBySongId($this->route('songId'));

            if (!$data)
                $validator->errors()->add('songId', 'Kos tidak ditemukan');

            $this->room = $data;
        } catch (ModelNotFoundException $e) {
            $validator->errors()->add('songId', 'Kos tidak ditemukan');
        }
    }

    /**
     * @return mixed
     */
    public function getRoom()
    {
        return $this->room;
    }
}
