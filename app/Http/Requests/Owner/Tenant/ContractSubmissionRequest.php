<?php

namespace App\Http\Requests\Owner\Tenant;

use App\Entities\Dbet\DbetLink;
use App\Entities\Mamipay\MamipayContract;
use App\Http\Requests\MamikosApiFormRequest;
use App\Repositories\Dbet\DbetLinkRepository;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ContractSubmissionRequest extends MamikosApiFormRequest
{

    protected $dbetLink = null;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        // get rent count types
        $rentCountTypes = [
            MamipayContract::UNIT_WEEK,
            MamipayContract::UNIT_MONTH,
            MamipayContract::UNIT_3MONTH,
            MamipayContract::UNIT_6MONTH,
            MamipayContract::UNIT_YEAR
        ];

        return [
            'dbet_link_id'      => 'required',
            'designer_room_id'  => 'nullable',
            'fullname'          => 'required|string',
            'gender'            => 'required|in:male,female',
            'phone'             => 'required',
            'job'               => 'required',
            'work_place'        => 'nullable',
            'identity_id'       => 'nullable|integer',
            'due_date'          => 'required|integer',
            'rent_count_type'   => 'required|in:' . implode(',', $rentCountTypes),
            'price'             => 'required',
            'additional_price'  => 'nullable|array',
            'additional_price.*.name' => 'required|string',
            'additional_price.*.price' => 'required|integer',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'dbet_link_id.required' => 'Data dbet link tidak boleh kosong',
            'fullname.required' => 'Nama lengkap tidak boleh kosong',
            'gender.required' => 'Jenis kelamin tidak boleh kosong',
            'phone.required' => 'No. HP tidak boleh kosong',
            'job.required' => 'Pekerjaan tidak boleh kosong',
            'work_place.required' => 'Tempat pekerjaan tidak boleh kosong',
            'due_date.required' => 'Tanggal jatuh tempo tidak boleh kosong',
            'rent_count_type.required' => 'Rent tipe tidak boleh kosong',
            'price.required' => 'Harga tidak boleh kosong',
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  Validator  $validator
     * @return void
     */
    public function withValidator(Validator $validator)
    {
        $validator->after(function ($validator) {
            // validate room
            $this->validateLink($validator);
        });
    }

    /**
     * Validate room
     * @param Validator $validator
     * @return void
     */
    private function validateLink(Validator $validator)
    {
        try {
            // get room repository
            $repository = app()->make(DbetLinkRepository::class);
            $data = $repository->find($this->dbet_link_id);

            // assign data
            $this->dbetLink = $data;

            // check required identity
            if ($data->is_required_identity == 1 && empty($this->identity_id)) {
                $validator->errors()->add('identity_id', 'Identity tidak boleh kosong');
            }

        } catch (ModelNotFoundException $e) {
            $validator->errors()->add('dbet_link_id', 'DBET link data tidak ditemukan');
        }
    }

    /**
     * Get dbet link data
     * @return DbetLink|null
     */
    public function getDbetLink(): ?DbetLink
    {
        return $this->dbetLink;
    }
}
