<?php

namespace App\Http\Requests\Owner\Tenant;

use App\Entities\Room\Room;
use App\Http\Requests\MamikosApiFormRequest;
use App\Repositories\RoomRepository;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class OwnerTenantLinkRequest extends MamikosApiFormRequest
{

    protected $room = null;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'designer_id' => 'required',
            'is_required_identity' => 'required|in:0,1',
            'is_required_due_date' => 'required|in:0,1',
            'due_date' => 'required|integer'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'designer_id.required' => 'Data kos tidak boleh kosong',
            'is_required_identity.required' => 'Option identity tidak boleh kosong',
            'is_required_due_date.required' => 'Option jatuh tempo tidak boleh kosong',
            'due_date.required' => 'Tanggal jatuh tempo tidak boleh kosong.',
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  Validator  $validator
     * @return void
     */
    public function withValidator(Validator $validator)
    {
        $validator->after(function ($validator) {
            // validate room
            $this->validateRoom($validator);
        });
    }

    /**
     * Validate room
     * @param Validator $validator
     * @return void
     */
    private function validateRoom(Validator $validator)
    {
        try {
            // get room repository
            $repository = app()->make(RoomRepository::class);
            $data = $repository->getRoomWithOwnerBySongId($this->designer_id);

            // return error if room not found
            if (!$data)
                $validator->errors()->add('designer_id', 'Kos tidak ditemukan');

            // assign data
            $this->room = $data;
        } catch (ModelNotFoundException $e) {
            $validator->errors()->add('designer_id', 'Kos tidak ditemukan');
        }
    }

    /**
     * Get room data
     * @return Room|null
     */
    public function getRoom(): ?Room
    {
        return $this->room;
    }
}
