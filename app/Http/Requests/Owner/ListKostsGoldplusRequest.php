<?php

namespace App\Http\Requests\Owner;

use App\Http\Requests\MamikosApiFormRequest;
use App\Entities\Level\KostLevel;

class ListKostsGoldplusRequest extends MamikosApiFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'limit' => 'required|integer',
            'offset' => 'required|integer',
            'gp_status' => 'sometimes|in:0,' . $this->getGoldplusStatusAvailableRule(),
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'gp_status.in' => 'The selected Goldplus Status is invalid.',
        ];
    }

    private function getGoldplusStatusAvailableRule(): string
    {
        return implode(',', KostLevel::getGoldplusLevelIds());
    }
}
