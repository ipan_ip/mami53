<?php

namespace App\Http\Requests\Auth\Verification;

use App\Entities\Activity\ActivationCodeType;
use App\Http\Requests\MamikosApiFormRequest;
use App\Enums\Activity\ActivationCodeVia;
use App\Http\Helpers\RegexHelper;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Validation\Validator;

class BaseVerificationRequest extends MamikosApiFormRequest
{
    private $for;
    private $via;

    public function rules(): array
    {
        $rules = [
            'for' => [
                'required',
                new EnumValue(ActivationCodeType::class),
            ],
            'via' => [
                'required',
                'int',
                new EnumValue(ActivationCodeVia::class),
            ],
            'destination' => [
                'required',
                'string',
            ]
        ];
        switch ($this->input('via')) {
            case ActivationCodeVia::SMS:
            case ActivationCodeVia::WHATSAPP:
                $rules = $this->mergeRulePhoneNumber($rules);
                break;
        }

        return $rules;
    }

    public function mergeRulePhoneNumber(array $rules): array
    {
        $rules['destination'] = array_merge($rules['destination'], [
            'regex:' . RegexHelper::phoneNumber()
        ]);
        return $rules;
    }

    public function withValidator(Validator $validator)
    {
        if ($validator->fails()) {
            return;
        }
        // Fill the converted type
        $this->for = ActivationCodeType::fromValue($this->input('for'));
        $this->via = ActivationCodeVia::fromValue($this->input('via'));
    }

    /**
     * Get the value of for
     * @return ActivationCodeType
     */
    public function getFor(): ?ActivationCodeType
    {
        return $this->for;
    }

    /**
     * Get the value of via
     * @return ActivationCodeVia
     */
    public function getVia(): ?ActivationCodeVia
    {
        return $this->via;
    }
}
