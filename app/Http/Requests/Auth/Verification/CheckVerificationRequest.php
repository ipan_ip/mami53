<?php

namespace App\Http\Requests\Auth\Verification;

use App\Entities\Activity\ActivationCode;
use App\Http\Requests\Auth\Verification\BaseVerificationRequest;
use App\Services\Activity\ActivationCodeService;
use Illuminate\Validation\Validator;

/**
 * @property ActivationCode|null $activationCode
 */
class CheckVerificationRequest extends BaseVerificationRequest
{
    public $activationCode;

    public function rules(): array
    {
        return array_merge(parent::rules(), [
            'code' => [
                'string',
                'size:4',
            ]
        ]);
    }

    public function withValidator(Validator $validator)
    {
        parent::withValidator($validator);
        if ($validator->fails()) {
            return;
        }
        $validator->after(function (Validator $validator) {
            /** @var ActivationCodeService */
            $activationCodeService = app(ActivationCodeService::class);
            $activationCode = $activationCodeService->getActivationCode(
                $this->getFor(),
                $this->destination,
                $this->getVia()
            );

            if (is_null($activationCode) || $activationCode->code != $this->code) {
                $validator->errors()->add('code', __('api.tenant.verification.code.invalid'));
            }

            $this->activationCode = $activationCode;
        });
    }
}
