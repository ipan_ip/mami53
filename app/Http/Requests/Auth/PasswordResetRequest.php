<?php

namespace App\Http\Requests\Auth;

use App\Entities\Activity\ActivationCode;
use App\Http\Requests\Auth\Verification\CheckVerificationRequest;

class PasswordResetRequest extends CheckVerificationRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            'password' => [
                'required',
                'string',
                'confirmed'
            ]
        ]);
    }

    /**
     *  Validation message for each validation error
     *
     *  @return array
     */
    public function messages(): array
    {
        return [
            'password.required' => __('api.input.password.required'),
            'password.string' => __('api.input.password.string'),
            'password.confirmed' => __('api.input.password.confirmed'),
        ];
    }
}
