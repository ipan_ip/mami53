<?php

namespace App\Http\Requests\Booking;

use App\Http\Requests\MamikosApiFormRequest;
use Illuminate\Validation\Rule;

class BookingUserDraftRequest extends MamikosApiFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'room_id'   => 'required|exists:designer,id',
            'checkin'   => 'multi_date_format:Y-m-d,Y-m-j,Y-n-d,Y-n-j|after:yesterday',
            'checkout'  => 'multi_date_format:Y-m-d,Y-m-j,Y-n-d,Y-n-j',
            'duration'  => 'numeric',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'room_id.required'          => 'Kamar tidak boleh kosong',
            'room_id.exists'            => 'Kamar tidak di temukan',
            'checkin.multi_date_format' => 'Format Tanggal Checkin salah',
            'checkin.after'             => 'Tanggal Checkin harus lebih dari tanggal kemarin',
            'checkout.multi_date_format'=> 'Format Tanggal Checkout salah',
            'checkout.after'            => 'Tanggal Checkout harus lebih dari tanggal Checkin',
            'duration.numeric'          => 'Lama tinggal harus berupa angka',
        ];
    }
}