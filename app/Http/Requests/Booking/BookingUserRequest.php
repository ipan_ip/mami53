<?php

namespace App\Http\Requests\Booking;

use App\Criteria\FlashSale\RunningCriteria;
use App\Entities\Booking\BookingUser;
use App\Entities\Room\Room;
use App\Http\Helpers\BookingUserHelper;
use App\Http\Helpers\RegexHelper;
use App\Repositories\FlashSale\FlashSaleRepositoryEloquent;
use Carbon\Carbon;
use Illuminate\Contracts\Validation\Validator;
use App\Http\Requests\MamikosApiFormRequest;

class BookingUserRequest extends MamikosApiFormRequest
{

    /**
     * Label message for flash sale room terminated
     */
    const FLASH_SALE_TERMINATED = 'flash_sale_terminated';

    /**
     * variable flash sale to defined booking as flash sale
     * @var
     */
    protected $flashSale;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'rent_count_type'       => 'required',
            'checkin'               => 'required|multi_date_format:Y-m-d,Y-m-j,Y-n-d,Y-n-j|after:yesterday',
            'checkout'              => 'required|multi_date_format:Y-m-d,Y-m-j,Y-n-d,Y-n-j',
            'duration'              => 'required|numeric',
            'contact_name'          => 'required|max:190|regex:'.RegexHelper::alphaonlyAndSpace(),
            'contact_phone'         => 'required',
            'contact_job'           => 'required',
            'contact_gender'        => 'required',
            'contact_introduction'  => 'max:200|regex:'.RegexHelper::alphaNumericComaDotAndSpace()
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'rent_count_type.required'  => 'Rent Count Type harus diisi',
            'checkin.required'          => 'Tanggal Checkin harus diisi',
            'checkin.multi_date_format' => 'Format Tanggal Checkin salah',
            'checkin.after'             => 'Tanggal Checkin harus lebih dari tanggal kemarin',
            'checkout.required'         => 'Tanggal Checkout harus diisi',
            'checkout.required_if'      => 'Tanggal Checkout harus diisi',
            'checkout.multi_date_format'=> 'Format Tanggal Checkout salah',
            'checkout.after'            => 'Tanggal Checkout harus lebih dari tanggal Checkin',
            'duration.required'         => 'Lama tinggal harus diisi',
            'duration.required_if'      => 'Lama tinggal harus diisi',
            'duration.numeric'          => 'Lama tinggal harus berupa angka',
            'room_total.required'       => 'Jumlah kamar harus diisi',
            'room_total.numeric'        => 'Jumlah kamar harus berupa angka',
            'contact_name.required'     => 'Nama kontak harus diisi',
            'contact_name.max'          => 'Nama kontak maksimal :max karakter',
            'contact_name.regex'        => 'Nama hanya boleh mengandung karakter alfabetis',
            'contact_phone.required'    => 'Nomor telepon kontak harus diisi',
            'contact_phone.max'         => 'Nomor telepon kontak maksimal :max karakter',
            'contact_job.required'      => 'Profesi kontak harus diisi',
            'contact_gender.required'   => 'Jenis kelamin kontak harus diisi',
            'contact_introduction.max'  => 'Pengenalan Diri maksimal :max karakter',
            'contact_introduction.regex'=> 'Pengenalan Diri hanya boleh mengandung karakter alfabetis'
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  Validator  $validator
     * @return void
     */
    public function withValidator(Validator $validator)
    {
        $validator->after(function ($validator) {
            // validate checkout date
            $this->validateCheckoutDate($validator);

            // validate prebook premium
            $this->validatePrebookPremium($validator);

            // validate flash sale
            $this->validateFlashSale($validator);

            // validate booking time restriction
            $this->validateTimeRestriction($validator);
        });
    }

    /**
     * Validate checkout date
     * @param Validator $validator
     * @return void
     */
    private function validateCheckoutDate(Validator $validator)
    {
        $checkoutExpectedDate = BookingUserHelper::getCheckoutDate($this->rent_count_type, $this->checkin, $this->duration);
        if ($checkoutExpectedDate !== $this->checkout)
            $validator->errors()->add('checkout', 'Tanggal Checkout tidak valid');
    }

    /**
     * Validate prebook premium
     * @param Validator $validator
     * @return void
     */
    private function validatePrebookPremium(Validator $validator)
    {
        $checkoutErrorMessage = 'Tanggal checkin tidak bisa melebihi :month bulan dari hari ini';
        $room = Room::where('song_id', $this->route('roomId'))->first();
        if (!$room)
            $validator->errors()->add('room', 'Kamar tidak ditemukan');

        // if room is not mamirooms check pre-booking data
        if ($room && $room->is_mamirooms == false) {
            $diff = date_diff(date_create(Carbon::today()->format('Y-m-d')), date_create(Carbon::parse($this->checkin)->format('Y-m-d')));
            $diffInMonth = (int)$diff->format('%m');
            $diffInDay = (int)$diff->format('%d');

            // default max checkin is 2 month
            $maxMonthCheckInNormal = config('booking.prebook_checkin_normal_month');
            if (($diffInMonth == $maxMonthCheckInNormal && $diffInDay > 0 && config('booking.prebook_premium_active')) || ($diffInMonth > $maxMonthCheckInNormal && config('booking.prebook_premium_active'))) {
                // get verified owner
                $roomOwner = optional($room)->verified_owner;

                // check is owner premium and kost level is gold plus
                if (BookingUserHelper::isPremiumOwner($roomOwner) && BookingUserHelper::isKostLevelGoldPlus($room)) {
                    $totalPrebookRoom = BookingUser::totalOfPreebook($room, $maxMonthCheckInNormal);
                    if ($totalPrebookRoom >= config('booking.prebook_premium_max'))
                        $validator->errors()->add('checkin', str_replace(':month', $maxMonthCheckInNormal, $checkoutErrorMessage));

                } else {
                    $validator->errors()->add('checkin', str_replace(':month', $maxMonthCheckInNormal, $checkoutErrorMessage));
                }
            }
        }
    }

    /**
     * Validate flash sale
     * @param Validator $validator
     * @return void
     */
    private function validateFlashSale(Validator $validator)
    {
        if ($this->is_flash_sale) {
            $flashSaleRepository = app()->make(FlashSaleRepositoryEloquent::class);
            $flashSaleRepository->pushCriteria(new RunningCriteria());
            $runningFlashSale = $flashSaleRepository->first();

            // validate if flash sale already finish / stopped
            if (!$runningFlashSale)
                $validator->errors()->add('is_flash_sale', self::FLASH_SALE_TERMINATED);

            $room = Room::where('song_id', $this->route('roomId'))->first();
            if (!$room)
                $validator->errors()->add('room', 'Kamar tidak ditemukan');

            if (!$room->getIsFlashSale('all'))
                $validator->errors()->add('is_flash_sale', self::FLASH_SALE_TERMINATED);

            // set flash sale data
            $this->flashSale = $runningFlashSale;
        }
    }

    /**
     * Validate time restriction
     * @param Validator $validator
     * @return void
     */
    private function validateTimeRestriction(Validator $validator)
    {
        if (config('booking.booking_restriction.is_active') === true) {
            $now        = Carbon::now();
            $start      = Carbon::parse(config('booking.booking_restriction.available_time_start'));
            $end        = Carbon::parse(config('booking.booking_restriction.available_time_end'));
            $checkin    = Carbon::parse($this->checkin);
            
            if ( ($now->format('Y-m-d') == $checkin->format('Y-m-d') ) && !($now->greaterThanOrEqualTo($start) && $now->lessThanOrEqualTo($end)) ) {
                $message = str_replace(
                    [':timeStart', ':timeEnd'],
                    [$start->format('H.i'), $end->format('H.i')],
                    'Booking dan masuk kos di hari yang sama hanya bisa pada pukul :timeStart - :timeEnd WIB.'
                );
                $validator->errors()->add('booking_restricted', $message);
            }
        }
    }

    /**
     * get flash data
     * @return mixed
     */
    public function getFlashSale()
    {
        return $this->flashSale;
    }
}