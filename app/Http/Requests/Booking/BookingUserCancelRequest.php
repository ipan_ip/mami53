<?php

namespace App\Http\Requests\Booking;

use Illuminate\Contracts\Validation\Validator;
use App\Http\Requests\MamikosApiFormRequest;

class BookingUserCancelRequest extends MamikosApiFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'cancel_reason' => 'required|min:4|max:200'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'cancel_reason.required'    => 'Alasan Batal harus diisi',
            'cancel_reason.min'         => 'Alasan Batal minimal :min karakter',
            'cancel_reason.max'         => 'Alasan Batal maksimal :max karakter',
        ];
    }
}