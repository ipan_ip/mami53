<?php

namespace App\Http\Requests;

use App\Http\Helpers\RegexHelper;
use Illuminate\Contracts\Validation\Validator;
use App\Http\Requests\MamikosApiFormRequest;

class UserEditNameRequest extends MamikosApiFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string|min:3|max:255|regex:' . RegexHelper::name(),
        ];
    }

    /**
     *  Validation message for each validation error
     *
     *  @return array
     */
    public function messages(): array
    {
        return [
            'name.required' => __('api.input.name.required'),
            'name.alpha' => __('api.input.name.alpha'),
            'name.min' => __('api.input.name.min'),
            'name.max' => __('api.input.name.max'),
            'name.regex' => __('api.input.name.alpha'),
        ];
    }
}
