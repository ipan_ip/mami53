<?php

namespace App\Http\Requests\GoldPlus;

use App\Entities\Consultant\PotentialProperty;
use App\Http\Requests\MamikosApiFormRequest;
use Illuminate\Validation\Rule;

class CreateSubmissionRequest extends MamikosApiFormRequest
{
    public function rules(): array
    {
        return [
            'code' => [ 
                'required',
                Rule::in([
                    PotentialProperty::PRODUCT_GP1,
                    PotentialProperty::PRODUCT_GP2,
                    PotentialProperty::PRODUCT_GP3
                ])
            ],
            'listing_ids' => 'array'
        ];
    }
}
