<?php

namespace App\Http\Requests;

use App\Http\Helpers\RegexHelper;
use Illuminate\Contracts\Validation\Validator;
use App\Http\Requests\MamikosApiFormRequest;

class TenantVerificationCheckRequest extends MamikosApiFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone_number' => 'required|min:8|max:14|unique:user,phone_number|regex:' . RegexHelper::phoneNumber(),
            'code'              => 'required|alpha_num|size:4'
        ];
    }

    /**
     *  Validation message for each validation error
     *
     *  @return array
     */
    public function messages(): array
    {
        return [
            'phone_number.required' => __('api.input.phone_number.required'),
            'phone_number.regex' => __('api.input.phone_number.regex'),
            'code.required'     => __('api.input.code.required'),
            'code.size'         => __('api.owner.verification.code.invalid'),
            'code.alpha_num'    => __('api.owner.verification.code.invalid'),
        ];
    }
}
