<?php

namespace App\Http\Requests\Point;

use App\Entities\Point\PointActivity;
use App\Http\Requests\MamikosApiFormRequest;
use Illuminate\Contracts\Validation\Validator;

/**
 *  Validate point setting request
 */
class PointSettingRequest extends MamikosApiFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            '*.*.*.received_each' => 'required|numeric|min:0',
            '*.*.*.limit_type' => 'required',
            '*.*.*.limit' => 'required|numeric|min:0',
            '*.*.*.limit_room' => 'nullable|boolean',
            '*.*.*.is_active' => 'nullable|boolean'
        ];
    }

    /**
     * Get the validation attributes.
     * 
     * @return array
     */
    public function attributes(): array
    {
        return [
            '*.*.*.received_each' => 'How Many Points are Earned',
            '*.*.*.limit_type' => 'Point Limit',
            '*.*.*.limit' => 'Point Limit'
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            foreach ($this->request as $pointId => $activities) {
                foreach ($activities as $activityKey => $ownerRoomGroups) {
                    foreach ($ownerRoomGroups as $id => $data) {

                        // The percentage of earned point cannot be greater than 100 percent
                        if (in_array($activityKey, PointActivity::getTenantActivitiesWithPercentagePointConvertion())) {
                            if ($data['received_each'] > 100) {
                                $validator->errors()->add($pointId.'.'.$activityKey.'.'.$id.'.received_each', 'How Many Points are Earned cannot be greater than 100 percent.');
                            }
                        }

                    }
                }
            }
        });
    }
}