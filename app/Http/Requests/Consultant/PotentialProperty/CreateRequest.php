<?php

namespace App\Http\Requests\Consultant\PotentialProperty;

class CreateRequest extends Request
{
    /**
     *  Validation rules for creating a potential property
     *
     *  @return array
     */
    public function rules(): array
    {
        $rules = parent::rules();
        $rules['owner_id'] = 'required|integer|exists:consultant_potential_owner,id';
        return $rules;
    }

    /**
     *  Sanitize and return all allowed inputs
     *
     *  @return array
     */
    public function inputs(): array
    {
        $input = parent::inputs();
        $input['consultant_potential_owner_id'] = $this->owner_id;
        return $input;
    }
}
