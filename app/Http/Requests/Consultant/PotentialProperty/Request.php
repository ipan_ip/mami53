<?php

namespace App\Http\Requests\Consultant\PotentialProperty;

use App\Http\Requests\MamikosApiFormRequest;
use Illuminate\Validation\Rule;

class Request extends MamikosApiFormRequest
{
    /**
     *  Validation rules for potential property
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string|max:255',
            'address' => 'required|string|max:255',
            'area_city' => 'required|string|max:255',
            'province' => 'required|string|max:255',
            'photo' => 'nullable|integer|exists:media,id',
            'total_room' => 'nullable|integer',
            'product_offered' => 'required|string|in:gp1,gp2,gp3,gp4',
            'priority' => 'nullable|string|in:high,low',
            'bbk_status' => ['string', Rule::in(['non-bbk', 'waiting', 'bbk'])],
            'remark' => ['nullable', 'string', 'max:255'],
            'followup_status' => ['string', Rule::in(['new', 'in-progress', 'paid', 'approved', 'rejected'])]
        ];
    }

    /**
     *  Sanitize and return all allowed input
     *
     * @return array
     */
    public function inputs(): array
    {
        $input = $this->only(
            [
                'name',
                'address',
                'area_city',
                'province',
                'bbk_status',
                'followup_status'
            ]
        );

        $input['is_priority'] = (!is_null(
            $this->priority
        ) ? ($this->priority === 'high') : true); // Priority defaulted to high(true)
        $input['total_room'] = $this->total_room ?? 0;
        $input['offered_product'] = $this->product_offered;
        $input['media_id'] = $this->photo ?? null;
        $input['remark'] = !empty($this->remark) ? $this->remark : '';

        return $input;
    }
}
