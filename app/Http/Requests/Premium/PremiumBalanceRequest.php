<?php

namespace App\Http\Requests\Premium;

use App\Http\Requests\MamikosApiFormRequest;
use App\Entities\Promoted\ViewPromote;

class PremiumBalanceRequest extends MamikosApiFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'premium_package_id' => 'required|numeric|exists:premium_package,id'
        ];
    }

        
    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'premium_package_id.required' => 'Paket premium tidak ditemukan',
            'premium_package_id.numeric' => 'Paket premium tidak ditemukan',
            'premium_package_id.exist' => 'Paket premium tidak ditemukan'
        ];
    }
}
