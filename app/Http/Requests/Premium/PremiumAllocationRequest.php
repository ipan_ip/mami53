<?php

namespace App\Http\Requests\Premium;

use App\Http\Requests\MamikosApiFormRequest;
use App\Entities\Promoted\ViewPromote;

class PremiumAllocationRequest extends MamikosApiFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'allocation' => 'required|numeric'
        ];
    }

        
    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'allocation.required' => 'Jumlah alokasi tidak boleh kosong',
            'allocation.numeric' => 'Alokasi saldo minimal '.ViewPromote::DAILY_ALLOCATION_MINIMUM,
        ];
    }
}
