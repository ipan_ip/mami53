<?php

namespace App\Http\Requests\Premium;

use App\Http\Requests\MamikosApiFormRequest;
use App\Entities\Promoted\ViewPromote;

class PremiumPlusTokenRequest extends MamikosApiFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'invoice_number' => 'required|exists:premium_plus_invoice,invoice_number'
        ];
    }

        
    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'invoice_number.required' => 'Invoice tidak ditemukan',
            'invoice_number.exists' => 'Invoice tidak ditemukan',
        ];
    }
}
