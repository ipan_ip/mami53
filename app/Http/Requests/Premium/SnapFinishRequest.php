<?php

namespace App\Http\Requests\Premium;

use App\Http\Requests\MamikosApiFormRequest;

class SnapFinishRequest extends MamikosApiFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order_id'              => 'required',
            'payment_type'          => 'required',
            'transaction_status'    => 'required',
            'transaction_id'        => 'required',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'order_id.required'             => 'Order tidak ditemukan',
            'payment_type.required'         => 'Metode pembayaran tidak ditemukan',
            'transaction_status.required'   => 'Transaksi tidak ditemukan',
            'transaction_id.required'       => 'Transaksi tidak ditemukan',
        ];
    }
}
