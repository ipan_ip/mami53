<?php

namespace App\Http\Requests;

use App\Entities\Booking\BookingUser;
use Illuminate\Foundation\Http\FormRequest;

class BookingAdminCheckinRequest extends FormRequest
{
    private $bookingUser;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'datetime_checkin' => 'required|date:Y-m-d H:i:s',
            'booking_user_id' => 'required'
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {

            $this->bookingUser = BookingUser::find($this->route('booking_user_id'));
            if ($this->bookingUser === null) {
                $validator->errors()->add('booking_user_id', 'Booking tidak ditemukan');
                return;
            }
            if($this->bookingUser->status !== BookingUser::BOOKING_STATUS_VERIFIED) {
                $validator->errors()->add('booking_user_id', 'Aksi tidak dapat dilakukan');
            }
        });
    }

    public function all($keys = null)
    {
        $data = parent::all();
        $data['booking_user_id'] = $this->route('booking_user_id');
        return $data;
    }

    public function getBookingUser()
    {
        return $this->bookingUser;
    }
}
