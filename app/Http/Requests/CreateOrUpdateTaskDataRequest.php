<?php

namespace App\Http\Requests;

use App\Entities\Consultant\ActivityManagement\ActivityForm;
use App\Entities\Consultant\ActivityManagement\ActivityProgress;

class CreateOrUpdateTaskDataRequest extends MamikosApiFormRequest
{
    /**
     *  Task againts which to create or update data
     *
     *  @var ActivityProgress
     */
    public $task;

    /**
     *  Stage form againts which to create or update data
     *
     *  @var ActivityForm
     */
    public $form;

    /**
     *  Existing task data to update
     *
     *  @var ActivityProgressDetail|null
     */
    public $currentStageData;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'stage_id' => 'required|integer|exists:consultant_activity_form,id',
            'inputs' => 'required|array',
            'inputs.*' => 'required|array',
            'inputs.*.id' => 'required|numeric|integer',
            'inputs.*.value' => 'required_without:inputs.*.media_id',
            'inputs.*.media_id' => 'required_without:inputs.*.value|exists:media,id'
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $this->task = ActivityProgress::with('details')->find($this->taskId);
        $this->form = ActivityForm::find($this->stage_id);
        $this->currentStageData = null;

        if (!is_null($this->task) && !is_null($this->form)) {
            $currentStage = $this->form->stage;
            $this->currentStageData = $this->task->details()
                ->whereHas('form', function ($form) use ($currentStage) {
                    $form->where('stage', $currentStage);
                })
                ->first();
        }

        $validator->after(function ($validator) {
            if (is_null($this->task)) {
                $validator->errors()->add('taskId', 'Task id could not be found');
            }

            if ((!is_null($this->stage_id)) && (is_null($this->form))) {
                $validator->errors()->add('stage_id', 'Stage could not be found');
            }
        });
    }
}
