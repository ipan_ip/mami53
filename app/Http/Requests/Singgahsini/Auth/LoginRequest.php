<?php

namespace App\Http\Requests\Singgahsini\Auth;

use Illuminate\Contracts\Validation\Validator;
use App\Http\Requests\MamikosApiFormRequest;

class LoginRequest extends MamikosApiFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'email' => 'required|email',
            'password' => 'required|string'
        ];
    }

    /**
     *  Validation message for each validation error
     *
     *  @return array
     */
    public function messages(): array
    {
        return [
            'email.required' => __('api.input.email.required'),
            'email.email' => __('api.input.email.email'),
            'password.required' => __('api.input.password.required'),
            'password.string' => __('api.input.password.string'),
        ];
    }
}
