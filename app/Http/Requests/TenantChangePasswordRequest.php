<?php

namespace App\Http\Requests;

use App\User;
use Illuminate\Contracts\Validation\Validator;
use App\Http\Requests\MamikosApiFormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class TenantChangePasswordRequest extends MamikosApiFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        $this->retrieveUser();
        return !is_null($this->user);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'new_password' => 'required|string',
            'old_password' => 'required|string',
        ];
    }

    /**
     *  Validation message for each validation error
     *
     *  @return array
     */
    public function messages(): array
    {
        return [
            'new_password.required' => __('api.input.password.required'),
            'new_password.string' => __('api.input.password.string'),
            'old_password.required' => __('api.input.password.required'),
            'old_password.string' => __('api.input.password.string'),
        ];
    }

    /**
     *  Add validation for verification code
     *
     *  @param Validator $validator
     *
     *  @return void
     */
    public function withValidator(Validator $validator)
    {
        $validator->after(function ($validator) {
            if (!Hash::check($this->old_password, $this->user->password)) {
                $validator->errors()->add('old_password', __('api.input.change_password.tenant.old_password.invalid'));
            }

            if ($this->old_password === $this->new_password) {
                $validator->errors()->add('new_password', __('api.input.change_password.tenant.new_password.unchanged'));
            }
        });
    }

    /**
     *  Retrieve user currently authenticated through token or session
     *
     *  @return void
     */
    private function retrieveUser()
    {
        $this->user = app()->user;

        if (is_null($this->user)) {
            $this->user = Auth::user();
        }
    }
}
