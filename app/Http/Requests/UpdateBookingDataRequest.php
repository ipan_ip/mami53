<?php

namespace App\Http\Requests;

use App\Http\Requests\MamikosApiFormRequest;
use Illuminate\Contracts\Validation\Validator;
use App\Entities\Booking\BookingDiscount;
use App\Entities\Room\Room;
use App\Entities\Room\Element\Price;

class UpdateBookingDataRequest extends MamikosApiFormRequest
{
    private $char_min;
    public $room;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        // Minimum of 2 digits for usd and 4 digits for idr
        $this->char_min = (($this->filled('price_type') && $this->input('price_type') == 'usd') ? Price::USD_MIN_DIGIT : Price::IDR_MIN_DIGIT);
        $price_min = pow(10, ($this->char_min - 1));

        return [
//            'available_room'  => 'required|numeric',
            'price_yearly'    => 'required_without_all:price_monthly,price_weekly,price_daily,price_3_month,price_6_month|numeric|min:' . $price_min,
            'price_monthly'   => 'required_without_all:price_yearly,price_weekly,price_daily,price_3_month,price_6_month|numeric|min:' . $price_min,
            'price_weekly'    => 'required_without_all:price_yearly,price_monthly,price_daily,price_3_month,price_6_month|numeric|min:' . $price_min,
            'price_daily'     => 'required_without_all:price_yearly,price_monthly,price_weekly,price_3_month,price_6_month|numeric|min:' . $price_min,
            'price_3_month'   => 'required_without_all:price_yearly,price_monthly,price_weekly,price_daily,price_6_month|numeric|min:' . $price_min,
            'price_6_month'   => 'required_without_all:price_yearly,price_monthly,price_weekly,price_daily,price_3_month|numeric|min:' . $price_min
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages(): array
    {
        return [
//            'available_room.required' => __('api.input.available_room.required'),
//            'available_room.numeric' => __('api.input.available_room.numeric'),
            'price_yearly.numeric' => __('api.input.price_yearly.numeric'),
            'price_monthly.numeric' => __('api.input.price_monthly.numeric'),
            'price_weekly.numeric' => __('api.input.price_weekly.numeric'),
            'price_daily.numeric' => __('api.input.price_daily.numeric'),
            'price_3_month.numeric' => __('api.input.price_3_month.numeric'),
            'price_6_month.numeric' => __('api.input.price_6_month.numeric'),

            'price_yearly.min' => __('api.input.price_yearly.digit_min', ['digit' => $this->char_min]),
            'price_monthly.min' => __('api.input.price_monthly.digit_min', ['digit' => $this->char_min]),
            'price_weekly.min' => __('api.input.price_weekly.digit_min', ['digit' => $this->char_min]),
            'price_daily.min' => __('api.input.price_daily.digit_min', ['digit' => $this->char_min]),
            'price_3_month.min' => __('api.input.price_3_month.digit_min', ['digit' => $this->char_min]),
            'price_6_month.min' => __('api.input.price_6_month.digit_min', ['digit' => $this->char_min]),

            'price_yearly.required_without_all' => __('api.input.price_yearly.required_without_all'),
            'price_monthly.required_without_all' => __('api.input.price_monthly.required_without_all'),
            'price_weekly.required_without_all' => __('api.input.price_weekly.required_without_all'),
            'price_daily.required_without_all' => __('api.input.price_daily.required_without_all'),
            'price_3_month.required_without_all' => __('api.input.price_3_month.required_without_all'),
            'price_6_month.required_without_all' => __('api.input.price_6_month.required_without_all'),
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $this->room = Room::where('song_id', $this->id)->first();

            if (is_null($this->room)) {
                $validator->errors()->add('room', 'Kost / Apartemen tidak ditemukan');
            }

            $this->validateMonthlyPrice($validator);
            $this->validateDailyPrice($validator);
            $this->validateWeeklyPrice($validator);
            $this->validateYearlyPrice($validator);
            $this->validateQuarterlyPrice($validator);
            $this->validateSemiAnnualPrice($validator);
            $this->validateRoomAvailable($validator);
        });
    }

    private function validateDailyPrice($validator) {
        if (empty($this->price_daily)) {
            $this->price_daily = null;
        }

        if (!BookingDiscount::validateListingPrice($this->price_daily, $this->room, 'daily')) {
            $validator->errors()->add('price_daily', 'Harga harian yang dimasukkan terlalu rendah dari diskon');
        }

        return $validator;
    }

    private function validateWeeklyPrice($validator) {
        if (empty($this->price_weekly)) {
            $this->price_weekly = null;
        }

        if (!BookingDiscount::validateListingPrice($this->price_weekly, $this->room, 'weekly')) {
            $validator->errors()->add('price_weekly', 'Harga mingguan yang dimasukkan terlalu rendah dari diskon');
        }

        return $validator;
    }

    private function validateMonthlyPrice($validator) {
        if (empty($this->price_monthly)) { // Replace in case of 0
            $this->price_monthly = null;
        }

        if (!BookingDiscount::validateListingPrice($this->price_monthly, $this->room, 'monthly')) {
            $validator->errors()->add('price_monthly', 'Harga bulanan yang dimasukkan terlalu rendah dari diskon');
        }

        return $validator;
    }

    private function validateYearlyPrice($validator) {
        if (empty($this->price_yearly)) {
            $this->price_yearly = null;
        }

        if (!BookingDiscount::validateListingPrice($this->price_yearly, $this->room, 'annually')) {
            $validator->errors()->add('price_yearly', 'Harga tahunan yang dimasukkan terlalu rendah dari diskon');
        }

        return $validator;
    }

    private function validateQuarterlyPrice($validator) {
        if (empty($this->price_3_month)) {
            $this->price_3_month = null;
        }

        if (!BookingDiscount::validateListingPrice($this->price_3_month, $this->room, 'quarterly')) {
            $validator->errors()->add('price_3_month', 'Harga 3 bulan yang dimasukkan terlalu rendah dari diskon');
        }

        return $validator;
    }

    private function validateSemiAnnualPrice($validator) {
        if (empty($this->price_6_month)) {
            $this->price_6_month = null;
        }
        
        if (!BookingDiscount::validateListingPrice($this->price_6_month, $this->room, 'semiannually')) {
            $validator->errors()->add('price_6_month', 'Harga 6 bulan yang dimasukkan terlalu rendah dari diskon');
        }

        return $validator;
    }

    private function validateRoomAvailable($validator)
    {
        if ($this->available_room > $this->room->room_count) {
            $validator->errors()->add('available_room', 'Jumlah kamar tersedia tidak boleh melebihi jumlah kamar total');
        } else {
            $activeContract         = $this->room->getBookedAndActiveContract();
            $activeContractCount    = $activeContract->count();
            if ($this->available_room > ($this->room->room_count - $activeContractCount)) {
                $validator->errors()->add('available_room', 'Total kamar terisi tidak boleh kurang dari total kamar dengan kontrak.');
            }
        }
    }
}
