<?php

namespace App\Http\Requests\Admin\Room;

use App\Entities\Room\Room;
use App\Http\Requests\MamikosApiFormRequest;
use Illuminate\Contracts\Validation\Validator;

class UpdateKostRequest extends MamikosApiFormRequest
{
    public $validator = null;

    public function rules(): array
    {
        return [
            'name'                  => 'required|string|min:5|max:250',
            'gender'                => 'required|integer|between:0,2',
            'building_year'         => 'nullable|integer|min:0',
            'description'           => 'nullable|string',
            'remark'                => 'nullable|string',
            'size'                  => 'required|string|regex:/^\d{1,2}(\.\d?)?x\d{1,2}(\.\d?)?$/',
            'room_count'            => 'required|integer|min:1|lte:' . Room::MAX_ROOM_COUNT,
            'room_available'        => 'required|integer|min:0',
            'address'               => 'required|string|max:250',
            'latitude'              => 'required|numeric|min:-90|max:90',
            'longitude'             => 'required|numeric|min:-180|max:180',
            'province'              => 'required|string',
            'area_city'             => 'required|string',
            'area_subdistrict'      => 'required|string',
            'area_big'              => 'nullable|string',
            'guide'                 => 'nullable|string',
            'price_daily'           => 'nullable|integer|min:0',
            'price_weekly'          => 'nullable|integer|min:0',
            'price_monthly'         => 'required|integer|min:10000',
            'price_yearly'          => 'nullable|integer|min:0',
            'price_quarterly'       => 'nullable|integer|min:0',
            'price_semiannually'    => 'nullable|integer|min:0',
            'price_remark'          => 'nullable|string',
            'min_payment'           => 'nullable|integer',
            'additional_cost'       => 'array',
            'additional_cost.*.name' => 'required|string',
            'additional_cost.*.price' => 'required|integer|min:0',
            'deposit'               => 'nullable|integer|min:0',
            'dp'                    => 'nullable|integer|in:0,10,20,30,40,50',
            'fine_price'            => 'nullable|integer|min:0',
            'fine_type'             => 'required_with:fine_price|string|in:day,week,month',
            'fine_length'           => 'required_with:fine_price|integer|min:1',
            'facility_share_ids'    => 'nullable|array',
            'facility_share_ids.*'  => 'nullable|integer',
            'facility_room_ids'     => 'required|array',
            'facility_room_ids.*'   => 'required|integer',
            'facility_bath_ids'     => 'nullable|array',
            'facility_bath_ids.*'   => 'nullable|integer',
            'facility_park_ids'     => 'nullable|array',
            'facility_park_ids.*'   => 'nullable|integer',
            'facility_other_ids'     => 'nullable|array',
            'facility_other_ids.*'   => 'nullable|integer',
            'kos_rule_ids'          => 'nullable|array',
            'kos_rule_ids.*'        => 'nullable|integer|min:1',
            'kos_rule_photos'       => 'nullable|array|max:5',
            'kos_rule_photos.*.id'  => 'required|integer|min:1',
            'owner_name'            => 'required_without:manager_name',
            'owner_phone'           => 'required_with:owner_name',
            'manager_name'          => 'required_without:owner_name',
            'manager_phone'         => 'required_with:manager_name',
            'property_id'           => 'nullable|integer',
            'unit_type'             => 'nullable|string',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $this->validator = $validator;
    }
}
