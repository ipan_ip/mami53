<?php

namespace App\Http\Requests\Admin\Property;

use App\Http\Helpers\RegexHelper;
use App\Http\Requests\MamikosApiFormRequest;
use App\Repositories\UserDataRepository;
use Illuminate\Contracts\Validation\Validator;

class PropertyStoreRequest extends MamikosApiFormRequest
{
    public $validator = null;

    private $userDataRepository;
    private $_owner;

    public function __construct(UserDataRepository $userDataRepository)
    {
        $this->userDataRepository = $userDataRepository;
    }

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'property_name' => [
                'required',
                'string',
                sprintf('regex:%s', RegexHelper::validPropertyName())
            ],
            'owner_phone_number' => [
                'required',
                sprintf('regex:%s', RegexHelper::phoneNumberFormat()),
                'exists:user,phone_number'
            ],
            'owner_id' => ['required', 'integer']
        ];
    }

    public function messages(): array
    {
        return [
            'property_name.required' => 'Nama Property harus diisi',
            'owner_phone_number.required' => 'Nomor Telepon harus diisi',
            'owner_phone_number.regex' => 'Format telepon tidak tepat',
            'owner_phone_number.exists' => 'Nomor telepon user tidak terdaftar',
            'owner_id.required' => 'Owner tidak ada'
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'owner_id' => $this->getOwnerId()
        ]);
    }

    public function getOwner()
    {
        $this->_owner = $this->_owner ?? $this->userDataRepository
            ->getOwnerByPhoneNumber($this->owner_phone_number ?? null);
        return $this->_owner;
    }

    public function getOwnerId()
    {
        return $this->getOwner()->id ?? null;
    }

    protected function failedValidation(Validator $validator)
    {
        $this->validator = $validator;
    }
}
