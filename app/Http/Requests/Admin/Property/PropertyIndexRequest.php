<?php

namespace App\Http\Requests\Admin\Property;

use Illuminate\Foundation\Http\FormRequest;

class PropertyIndexRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'property_name' => ['nullable', 'string'],
            'owner_name' => ['nullable', 'string'],
            'owner_phone_number' => ['nullable', 'numeric'],
            'levels.*' => 'nullable',
        ];
    }
}
