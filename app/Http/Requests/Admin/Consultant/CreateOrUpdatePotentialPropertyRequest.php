<?php

namespace App\Http\Requests\Admin\Consultant;

use App\Entities\Consultant\PotentialProperty;
use App\Http\Requests\MamikosApiFormRequest;
use Illuminate\Validation\Rule;

class CreateOrUpdatePotentialPropertyRequest extends MamikosApiFormRequest
{
    /**
     *  Determine if the user is authorized to make this request
     *
     *  @return bool
     */
    public function authorize(): bool
    {
        $user = $this->user();
        $consultant = !is_null($user) ? $user->consultant : null;
        return !is_null($user) && !is_null($consultant);
    }

    public function rules(): array
    {
        return [
            'address' => 'required|string|max:255',
            'area_city' => 'required|string|max:255',
            'followup_status' => [
                'required',
                Rule::in([
                    PotentialProperty::FOLLOWUP_STATUS_NEW,
                    PotentialProperty::FOLLOWUP_STATUS_IN_PROGRESS
                ])
                ],
            'is_priority' => 'required|boolean',
            'media_id' => 'nullable|integer|exists:media,id',
            'name' => 'required|max:255',
            'offered_product' => 'required|array|max:3',
            'offered_product.*' => [ 
                'required',
                Rule::in([
                    PotentialProperty::PRODUCT_GP1,
                    PotentialProperty::PRODUCT_GP2,
                    PotentialProperty::PRODUCT_GP3
                ])
            ],
            'potential_gp_room_list' => 'required|string|max:1024',
            'potential_gp_total_room' => 'required|integer',
            'province' => 'required|string|max:255',
            'remark' => 'string|max:255',
            'total_room' => 'required|integer'
        ];
    }
}
