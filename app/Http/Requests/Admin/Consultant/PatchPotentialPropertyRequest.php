<?php

namespace App\Http\Requests\Admin\Consultant;

use App\Entities\Consultant\PotentialProperty;
use App\Entities\User\UserRole;
use App\Http\Requests\MamikosApiFormRequest;
use Illuminate\Validation\Rule;

class PatchPotentialPropertyRequest extends MamikosApiFormRequest
{
    /**
     *  Determine if the user is authorized to make this request
     *
     *  @return bool
     */
    public function authorize(): bool
    {
        $user = $this->user();
        $isAdmin = $user->role === UserRole::Administrator;
        $isConsultant = !is_null($user->consultant());
        return ($isAdmin || $isConsultant);
    }

    public function rules(): array
    {
        return [
            'followup_status' => [
                Rule::in([
                    PotentialProperty::FOLLOWUP_STATUS_NEW,
                    PotentialProperty::FOLLOWUP_STATUS_IN_PROGRESS,
                    PotentialProperty::FOLLOWUP_STATUS_PAID,
                    PotentialProperty::FOLLOWUP_STATUS_APPROVED,
                    PotentialProperty::FOLLOWUP_STATUS_REJECTED
                ])
            ],
            'is_priority' => 'boolean',
            'offered_product' => 'array|max:3',
            'offered_product.*' => [ 
                'required',
                Rule::in([
                    PotentialProperty::PRODUCT_GP1,
                    PotentialProperty::PRODUCT_GP2,
                    PotentialProperty::PRODUCT_GP3
                ])
            ],
            'remark' => 'string|max:255',
        ];
    }
}
