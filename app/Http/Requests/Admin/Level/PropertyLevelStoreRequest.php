<?php

namespace App\Http\Requests\Admin\Level;

use App\Repositories\UserDataRepository;
use Illuminate\Foundation\Http\FormRequest;

class PropertyLevelStoreRequest extends FormRequest
{
    private $userDataRepository;
    private $_owner;

    public function __construct(UserDataRepository $userDataRepository)
    {
        $this->userDataRepository = $userDataRepository;
    }

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
            'max_rooms' => ['required', 'integer', 'min:1'],
            'minimum_charging' => ['required', 'numeric'],
            'kost_level_id' => ['required', 'integer', 'exists:kost_level,id'],
            'goldplus_package_id' => ['required', 'integer', 'exists:goldplus_package,id']
        ];
    }

    public function messages(): array
    {
        return [
            'name.required' => 'Nama Property harus diisi',
            'max_rooms.required' => 'Jumlah kamar maksimal harus diisi',
            'max_rooms.integer' => 'Jumlah kamar harus berupa angka',
            'max_rooms.min' => 'Jumlah kamar harus lebih dari 0',
            'minimum_charging.required' => 'Minimum charging harus diisi',
            'minimum_charging.numeric' => 'Minimum charging harus berupa angka',
            'kost_level_id.required' => 'Kost Level harus diisi',
            'kost_level_id.integer' => 'Kost Level tidak valid',
            'kost_level_id.exists' => 'Kost Level tidak valid',
            'goldplus_package_id.exists' => 'Goldplus Package ID tidak valid',
            'goldplus_package_id.integer' => 'Goldplus Package ID tidak valid',
        ];
    }
}
