<?php

namespace App\Http\Requests\Admin\PropertyContract;

use Illuminate\Foundation\Http\FormRequest;

class PropertyContractStoreRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'property_level_id' => ['required', 'numeric'],
            'joined_at' => ['required', 'date:Y-m-d H:i:s'],
            'total_package' => ['required', 'integer']
        ];
    }

    public function messages(): array
    {
        return [
            'property_level_id.required' => 'Property Level belum diisi',
            'property_level_id.numeric' => 'Property Level ID harus angka',
            'joined_at.required' => 'Waktu bergabung belum diisi',
            'joined_at.date' => 'Waktu bergabung salah format',
            'total_package.required' => 'Total package belum diisi',
            'total_package.integer' => 'Total package harus angka',
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'joined_at' => $this->joined_at ?? date('Y-m-d H:i:s')
        ]);
    }
}
