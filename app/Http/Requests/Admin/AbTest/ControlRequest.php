<?php

namespace App\Http\Requests\Admin\AbTest;

use Illuminate\Foundation\Http\FormRequest;

class ControlRequest extends FormRequest
{
    public function authorize()
    {
        $user = $this->user();
        return !is_null($user) && $user->can('access-abtest');
    }

    public function rules()
    {
        return [];
    }
}