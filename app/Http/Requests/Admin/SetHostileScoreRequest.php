<?php

namespace App\Http\Requests\Admin;

use App\Entities\User\HostilityReason;
use App\Http\Requests\MamikosApiFormRequest;
use App\User;

class SetHostileScoreRequest extends MamikosApiFormRequest
{
    public $owner;

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'id' => 'required|integer',
            'score' => 'required|integer|max:100',
            'reason' => 'required|string|enum_value:' . HostilityReason::class
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $this->owner = User::where([['is_owner', '=', 'true'], ['id', '=', $this->id]])->first();

            if (is_null($this->owner)) {
                $validator->errors()->add('id', 'Owner tidak ditemukan! Coba lagi nanti');
            }
        });
    }
}
