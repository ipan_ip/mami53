<?php

namespace App\Http\Requests\Admin\Promo;

use App\Http\Requests\MamikosApiFormRequest;
use App\Entities\Promoted\Promotion;
use App\Entities\Room\RoomOwner;

class PromoVerifyRequest extends MamikosApiFormRequest
{
    private $getPromo;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'is_confirm' => 'required|in:'.Promotion::VERIFY.','.Promotion::UNVERIFY
        ];
    }
        
    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'is_confirm.required' => 'Gagal konfirmasi promo',
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {

            $this->promo = Promotion::with(['room.owners' => function($q) {
                                            $q->whereIn('owner_status', RoomOwner::OWNER_TYPES);
                                        }])->where('id', $this->route('id'))
                                        ->first();
                                               
            if (is_null($this->promo)) {
                $validator->errors()->add('is_confirm', 'Promo tidak ditemukan');
                return;
            }
        });
    }

    public function getPromo()
    {
        return $this->promo;
    }
}
