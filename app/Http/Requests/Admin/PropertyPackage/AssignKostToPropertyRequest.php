<?php

namespace App\Http\Requests\Admin\PropertyPackage;

use App\Http\Requests\MamikosApiFormRequest;

class AssignKostToPropertyRequest extends MamikosApiFormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'property_id' => 'required|integer|exists:properties,id',
            'kost_id' => 'required|integer|exists:designer,id'
        ];
    }
}
