<?php

namespace App\Http\Requests\Admin\PropertyPackage;

use App\Http\Requests\MamikosApiFormRequest;

class AddPropertyRequest extends MamikosApiFormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'property_contract_id' => ['required', 'integer', 'exists:property_contracts,id'],
            'property_id' => ['required', 'integer', 'exists:properties,id']
        ];
    }
}
