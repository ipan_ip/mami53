<?php

namespace App\Http\Requests\Admin\PropertyPackage;

use App\Http\Requests\MamikosApiFormRequest;

class PropertyPackageStoreRequest extends MamikosApiFormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'property_id' => ['required', 'integer', 'exists:properties,id'],
            'property_level_id' => ['required', 'integer', 'exists:property_levels,id'],
            'join_date' => ['required', 'date_format:Y-m-d H:i:s'],
            'custom_package' => ['required', 'boolean'],
            'total_package' => ['integer'],
        ];
    }

    public function messages(): array
    {
        return [
            'property_id.required' => 'Nama properti harus diisi.',
            'property_id.integer' => 'Nama properti tidak valid.',
            'property_id.exists' => 'Nama properti yang dipilih tidak valid.',
            'property_level_id.required' => 'Level properti harus diisi.',
            'property_level_id.integer' => 'Level properti harus angka.',
            'property_level_id.exists' => 'Level properti yang dipilih tidak valid.',
            'join_date.required' => 'Join date harus diisi.',
            'join_date.date_format' => 'Join date harus tanggal dengan format YYYY-MM-dd HH:mm:ss.',
            'custom_package.required' => 'Custom package harus diisi.',
            'custom_package.boolean' => 'Custom package harus boolean.',
            'total_package.integer' => 'Total package harus angka.',
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            if ($this->custom_package) {
                if (($this->total_package !== 0) && empty($this->total_package)) {
                    $validator->errors()->add('total_package', 'Total package harus diisi jika custom package dipilih.');
                }
            }
        });
    }
}
