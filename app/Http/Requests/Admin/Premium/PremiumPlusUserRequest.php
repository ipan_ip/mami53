<?php

namespace App\Http\Requests\Admin\Premium;

use App\Http\Requests\MamikosApiFormRequest;
use Illuminate\Contracts\Validation\Validator;
use App\Http\Helpers\RegexHelper;
use App\User;
use App\Entities\Room\Room;
use App\Entities\Consultant\Consultant;

class PremiumPlusUserRequest extends MamikosApiFormRequest
{
    // protected $consultant;
    protected $owner;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'required',
            'phone_number' => 'required|min:8|max:13|regex:' . RegexHelper::phoneNumber(),
            'email' => 'required|email',
            'consultant_id' => 'required|exists:consultant,id',
            'admin_phone' => 'required|min:8|max:13|regex:' . RegexHelper::phoneNumber(),
            'designer_id' => 'required|exists:designer,id',
            'request_date' => 'required|date_format:Y-m-d G:i:s',
            'activation_date' => 'required|date_format:Y-m-d G:i:s',
            'start_date' => 'required|date_format:Y-m-d G:i:s',
            'end_date' => 'required|date_format:Y-m-d G:i:s',
            'premium_rate' => 'required|integer',
            'static_rate' => 'required|integer',
            'registered_room' => 'required|integer',
            'guaranteed_room' => 'required|integer'
        ];
    }

    public function withValidator(Validator $validator)
    {
        $validator->after(function ($validator) {
            if (!empty(request()->input('consultant_id'))) {
                $consultant = $this->getConsultant();

                if (is_null($consultant)) {
                    $validator->errors()->add('consultant_id','Consultant not found');
                }

                $owner = $this->getUserData(request()->input('admin_phone'));
                if (!$owner) {
                    $validator->errors()->add('admin_phone','Owner phone not found');
                } else {
                    $this->owner = $owner;
                }

                $room = $this->getRoom();
                if (is_null($room)) {
                    $validator->errors()->add('designer_id','Room not found');
                }
            }

            if (request()->input('guaranteed_room') > request()->input('registered_room')) {
                $validator->errors()->add('guaranteed_room', 'Guaranteed room must less than Registered room');
            }
        });
    }

    public function getConsultant()
    {
        return Consultant::find(request()->input('consultant_id'));
    }

    public function getOwner()
    {
        return $this->owner;
    }

    public function getRoom()
    {
        return Room::find(request()->input('designer_id'));
    }

    public function getUserData(string $phoneNumber, $type=Null)
    {
        $user = User::where('phone_number', $phoneNumber);

        if ($type == 'consultant') {
            $user = $user->whereHas('consultant');
        }
        
        return $user->first();
    }
}
