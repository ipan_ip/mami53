<?php

namespace App\Http\Middleware;

use Closure;
use Session;

class ApiLogMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Before
        $timeStart = microtime(true);

        Session::put('api.log.enable', true);
        Session::put('api.log.request_time', $timeStart);

        // Processing
        return $next($request);
    }
}
