<?php

namespace App\Http\Middleware;

use App\Entities\Device\UserDevice;
use App\Http\Helpers\ApiHelper;
use App\User;
use Config;
use Closure;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use App\Libraries\StringHelper;
use App\Libraries\RequestHelper;

class ApiAuthMiddleware
{
    protected $authType = 'anyone';
    protected $request = null;
    protected $validateHeaderAuth = true;
    protected $validateHeaderTime = true;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param string $authType
     * @return mixed
     */
    public function handle($request, Closure $next, $authType = 'anyone')
    {
        $this->filterUserAgent($request);
        $this->request = $request;
        $this->authType = $authType;

        $this->performAuth($request);

        return $next($request);
    }

    protected function filterUserAgent($request)
    {
        $userAgent = $request->server('HTTP_USER_AGENT');
        if (StringHelper::startsWith($userAgent, 'java/'))
        {
            $ip = RequestHelper::getClientIpRegardingCF();
            Bugsnag::notifyError('Info', 'UserAgent ' . $userAgent . ' is rejected. IP: ' . $ip);
            throw new \Exception('Bad Request', 400);
        }
    }

    /**
     * Doing authentication code before execution of resource.
     * Authentication including validating header, check token, validating time limit.
     * In the end of the function we know who user or device that accessing this request.
     *
     *
     * @param $request
     * @return bool
     * @throws \Exception
     */
    public function performAuth($request)
    {
        // Development purpose using development access token.
        if ($request->input('devel_access_token') == Config::get('api.devel_access_token')) {

            $this->validateHeaderTime = false;
            $this->validateHeader();

            $authHeader = $this->getAuthorizationHeader();

        } else {

            // Live Version
            $this->validateHeader();

            $authHeader = $this->getAuthorizationHeader();
            $serverSignature = $this->generateSignature();

            if ( $authHeader['client_signature'] != $serverSignature ) {
                $this->errorTokenInvalid();
            }

            $this->validateTimeLimit();
        }

        $device = $this->checkDevice($authHeader);
        $user   = $this->checkUser($device);

        $this->signIn($user);
        $this->signInDevice($device);

        Session::put('api.user.role', $this->authType);

        return true;
    }

    protected function checkDevice($authHeader)
    {

        $userDevice = UserDevice::where('device_token', '=', $authHeader['token'])
                                ->first();

        if ($userDevice == null && ($this->authType == 'device' || $this->authType == 'user')) {
            if (($authHeader['token'] != Config::get('api.guest_token'))
                && env("CREATE_DUMMY_DEVICE_WHEN_TOKEN_ERROR", true)) {
                $userDevice = UserDevice::createDummy($authHeader['token']);
            } else {
                $this->errorTokenInvalid();
            }
        }

        return $userDevice;
    }

    protected function checkUser($userDevice)
    {
        if ($userDevice == null || $userDevice->user_id === 0) {
            $user = null;
        } else {
            $user = User::find($userDevice->user_id);
        }

        if ($user == null && ($this->authType == 'user')) {
            $this->errorSignInRequired();
        }

        return $user;
    }

    protected function signIn(User $user = null)
    {
        app()->user = $user;
    }

    protected function signInDevice(UserDevice $userDevice = null)
    {
        app()->device = $userDevice;
    }

    protected function validateHeader($auth = true, $time = true)
    {
        // Authorization must be exist
        if (($this->validateHeaderAuth) && (\Request::header('Authorization') == null)) {
            throw new \Exception(\Lang::get('api.authorization.authorization_header_required'), 330);
        }

        // If time is matter, X-GIT-Time must be exist.
        if (($this->validateHeaderTime) && (\Request::header('X-GIT-Time') == null) ) {
            throw new \Exception(\Lang::get('api.authorization.time_header_required'), 444);
        }
    }

    protected function getAuthorizationHeader($soft = false)
    {
        try {
            $authData        = explode('GIT ', \Request::header('Authorization'));
            $authKeys        = explode(':', $authData[1]);
            $token           = $authKeys[1];
            $clientSignature = $authKeys[0];
        } catch (\Exception $e) {
            // Soft determine it's okay to not have valid token. It doesnt make error.
            if ($soft) return null;

            $detail = ApiHelper::traceThrowError($e);

            throw new \Exception(\Lang::get('api.authorization.authorization_header_invalid') . $detail, 256);
        }

        return array(
            'token'            => $token,
            'client_signature' => $clientSignature
        );
    }

    protected function validateTimeLimit()
    {
        $develAccessToken = Config::get('api.devel_access_token');
        $signatureTimeLimit = Config::get('api.signatureTimeLimit');

        if (\Request::input('devel_access_token') == $develAccessToken) {
            return true;
        }

        // Client Time
        $clientTime  = \Request::header('X-GIT-Time');

        // Compare The Client Time with Current Server Time
        $minutesDiff = Carbon::createFromTimeStampUTC($clientTime)->diffInMinutes();

        if ($minutesDiff >= $signatureTimeLimit) {
            //throw new \Exception(\Lang::get('api.authorization.access_token_expired'), 256);
        }

        return true;
    }

    protected function generateSignature()
    {
        $data =  \Request::method() . " " . \Request::path() . " " . \Request::header('X-GIT-Time');

        $key = Config::get('api.secret_key');

        return hash_hmac('sha256', $data, $key);
    }

    protected function errorSignInRequired()
    {
        throw new \Exception(\Lang::get('api.authorization.signin_required'), 333);
    }

    protected function errorTokenInvalid()
    {
        throw new \Exception(\Lang::get('api.authorization.token_invalid'), 330);
    }


}
