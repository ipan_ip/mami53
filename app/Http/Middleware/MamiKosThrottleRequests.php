<?php
namespace App\Http\Middleware;

use Closure;
use Log;
use Monolog\Handler\RotatingFileHandler;

class MamikosThrottleRequests extends \Illuminate\Routing\Middleware\ThrottleRequests
{
    protected function resolveRequestSignature($request)
    {
        $logger = Log::getLogger();
        $logger->popHandler();
        $logger->pushHandler(new RotatingFileHandler(storage_path('logs/throttle.log'), 7));

        // TODO: This is only for CloudFlare. If you use another LoadBalance you need to modify here.
        $connectingIp = isset($_SERVER["HTTP_CF_CONNECTING_IP"]) ? $_SERVER["HTTP_CF_CONNECTING_IP"] : $request->ip();
        $ipCombo = $connectingIp.'|'.$request->ip();

        if ($user = $request->user()) {
            $signature = $ipCombo.'|'.$user->getAuthIdentifier();
            return sha1($signature);
        }

        if ($route = $request->route()) {
            $signature = $ipCombo.
                '|'.implode('|', $request->route()->methods()).
                '|'.$route->domain().
                '|'.$route->uri();
            return sha1($signature);
        }

        $error = 'Unable to generate the request signature. Route unavailable.';
        $logger->debug($error);
        throw new RuntimeException($error);
    }
}