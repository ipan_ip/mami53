<?php

namespace App\Http\Middleware;

use App\Entities\Device\UserDevice;
use App\Http\Helpers\ApiHelper;
use App\Libraries\RequestHelper;
use App\Libraries\StringHelper;
use App\User;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Carbon\Carbon;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Session;

class ApiAuthV2Middleware extends ApiAuthMiddleware
{
    public function performAuth($request)
    {
        // Development purpose using development access token.
        if ($request->input('devel_access_token') === Config::get('api.devel_access_token')) {
            $this->validateHeaderTime = false;
        }
            $this->validateHeader();

            $authHeader = $this->getAuthorizationHeader();

        $userDevice = $this->checkDevice($authHeader);
        $user = $this->checkUser($userDevice, $authHeader);

        $this->signIn($user);
        $this->signInDevice($userDevice);

        Session::put('api.user.role', $this->authType);

        return true;
    }

    protected function getAuthorizationHeader($soft = false)
    {
        try {
            $authData = explode('GIT ', Request::header('Authorization'));
            $authKeys = explode(':', $authData[1]);
            $deviceToken = $authKeys[1];
            $userToken = $authKeys[0];
        } catch (\Exception $e) {
            // Soft determine it's okay to not have valid token. It doesnt make error.
            if ($soft) {
                return null;
            }

            $detail = ApiHelper::traceThrowError($e);

            throw new \Exception(Lang::get('api.authorization.authorization_header_invalid') . $detail, 256);
        }

        return array(
            'device_token' => $deviceToken,
            'user_token' => $userToken
        );
    }

    protected function checkDevice($authHeader)
    {
        $userDevice = UserDevice::where('device_token', $authHeader['device_token'])
            ->first();

        if (is_null($userDevice) && ($this->authType === 'device' || $this->authType === 'user')) {
            if (($authHeader['device_token'] != Config::get('api.guest_token'))
                && env("CREATE_DUMMY_DEVICE_WHEN_TOKEN_ERROR", true)) {
                $userDevice = UserDevice::createDummy($authHeader['device_token']);
            } else {
                $this->errorTokenInvalid();
            }
        }

        return  $userDevice;
    }

    protected function checkUser($userDevice, $authHeader = null)
    {
        if (is_null($userDevice) || $userDevice->user_id === 0) {
            $user = null;
        } else {
            $user = User::find($userDevice->user_id);
        }

        if(is_null($authHeader)) {
            $this->errorTokenInvalid();
        }

        if ((is_null($user) && ($this->authType === 'user')) || (!is_null($user) && ($user->token != $authHeader['user_token']))) {
            $this->errorSignInRequired();
        }

        return $user;
    }
}
