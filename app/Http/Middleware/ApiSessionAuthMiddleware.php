<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Closure;
use App\Http\Helpers\ApiResponse as Api;

class ApiSessionAuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();

        if ($user) {
            return $next($request);
        }
        
        return Api::responseData([
            'status' => false,
            'meta' => [
                'message' => 'Silakan login terlebih dahulu'
            ]
        ]);
    }
}
