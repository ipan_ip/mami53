<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class RememberMeTimeout
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // this wont be called for the first time login, but will be called on the next requests
        if(Auth::check()) {
            $key = auth()->getRecallerName();
            
            if(isset($_COOKIE[$key])) {
                // Adding the cookie to the next outgoing response, with a 30 days expiry.
                cookie()->queue($key, $request->cookie($key), 60 * 24 * 30);
            }
            
        }
        
        return $next($request);
    }
}
