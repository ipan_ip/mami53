<?php

namespace App\Http\Middleware;

use Closure;

class AdCookie
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // set new cookie which expired in a day to used as ad view identifier
        if(!isset($_COOKIE['adsession'])) {
            setcookie('adsession', md5(base64_encode(time())), time() + (3600 * 24 * 14), "/");
        }

        if(!isset($_COOKIE['popupdownload'])) {
            setcookie('popupdownload', 'false', time() + (3600 * 24 * 30), "/");
        }

        return $next($request);
    }
}
