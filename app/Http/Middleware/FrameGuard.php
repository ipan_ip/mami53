<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;

/**
 * Class FrameGuard
 * 
 * This class purposed for preventing click-jacking attact
 * 
 * @author Angga Bayu Sejati<angga@mamiteam.com>
 */
class FrameGuard 
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        // Exception when response instance not instance from Illuminate\Http\Response
        // ex: /download-soal/download/{id}, /admin/booking/users/download
        if (! $response instanceof Response) {
            return $response;
        }

        //Only allow show in iFrame from sameorigin
        //We may change it into DENY, if needed.
        $response->header('X-Frame-Options', 'SAMEORIGIN');

        return $response;
    }

}
