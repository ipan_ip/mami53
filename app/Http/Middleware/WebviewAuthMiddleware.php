<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\AuthenticationException;
use function Aws\map;

class WebviewAuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $authVersion = 1)
    {
        try {
            if (Auth::check()) {
                return $next($request);
            } else {
                if($authVersion === 1) {
                    $apiMiddleware =  new ApiAuthMiddleware();
                } else {
                    $apiMiddleware =  new ApiAuthV2Middleware();
                }

                $apiMiddleware->handle($request, $next, 'user');
                $user = app()->user ?? null;
                if (!$user) {
                    throw new \Exception('Silakan login terlebih dahulu');
                }

                Auth::loginUsingId($user->id);

                return $next($request);
            }


        } catch (\Exception $e) {
            throw new AuthenticationException('Unauthenticated.');
        }
    }
}
