<?php

namespace App\Http\Middleware;

use App\Http\Helpers\OpenSslEncryptor;
use Closure;
use Config;

class ApiEncryptMiddleware
{
    protected $key = "";
    protected $iv = "";

    public function __construct()
    {        
        $this->key = Config::get("api.api_secret_key");
        $this->iv = "4d70ebb03f4a1409";
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $this->convertInput($request);

        $response = $next($request);

        $response = $this->convertOutput($response);

        return $response;
    }

    private function convertInput($request)
    {
        if ($query = $request->input('query', '')) {
            $request->merge($this->decrypt($query));
        }

        if ($data = $request->input('data', '')) {
            $request->merge($this->decrypt($data));
        }
    }

    private function convertOutput($response)
    {
        $content = $response->getContent();

        $contentEncrypted = $this->encrypt($content);

        $encryptedResponse = json_encode(array('data' => $contentEncrypted));
        
        $response->setContent($encryptedResponse);

        return $response;
    }

    private function encrypt($message)
    {
        return OpenSslEncryptor::encrypt($message, $this->key, $this->iv);
    }

    private function decrypt($message)
    {
        return OpenSslEncryptor::decryptIntoArray($message, $this->key, $this->iv);
    }
}
