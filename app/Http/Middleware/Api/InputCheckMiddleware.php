<?php

namespace App\Http\Middleware\Api;

use Closure;
use Request;

class InputCheckMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $this->performCheck();

        return $next($request);
    }

    public function performCheck()
    {
        if ( in_array(Request::method(), array('POST', 'PUT')) ) {

            // First we fetch the Request instance
             $request = Request::instance();

            // Now we can get the content from it
            $post = $request->getContent();

            // Remove Temporarily
            if (trim($post) === '' || trim($post) === '"":""') {
                // Disabling this to Allow Empty Post
                // throw new Exception(Lang::get('api.input.required'), 1);
            } else {
                // if not null, then try to decode
                $post = json_decode($post);
            }

            if (empty($post)) {
                throw new \Exception(\Lang::get('api.input.json_decode_failed'), 1);
            }
        }
    }
}
