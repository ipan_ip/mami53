<?php

namespace App\Http\Middleware\Api;

use Closure;
use Request;

class TrackSearchMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $this->performTrack();

        return $next($request);
    }

    public function performTrack()
    {
        $referer_type = null;

        if (! is_null(Request::input('Referal'))) {
            $referer_type = Request::input('Referal');
        } elseif (! is_null(Request::input('Referer'))) {
            $referer_type = Request::input('Referer');
        }

        $referer_type = $referer_type == 'landing-page' ? 'landing_page' : 'carikos';

        $filters = Request::input('filters');

        if (! is_string($filters)) {
            $filters = json_encode($filters);
        }

        $device = app()->device;

        $device_type = 'app';

        $device_type = ($device['id_type'] == 'device_id') ? 'web' : 'app';

        if (! is_null($referer_type)) {
            UserSearchHistory::create(array(
                'user_id'        => ApiHelper::activeUserId(),
                'user_device_id' => ApiHelper::activeDeviceId(),
                'device_type'    => $device_type,
                'type'           => $referer_type,
                'filters'        => $filters,
            ));
        }
    }
}
