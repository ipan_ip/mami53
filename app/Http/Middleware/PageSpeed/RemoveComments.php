<?php

namespace App\Http\Middleware\PageSpeed;

use Closure;
Use App\Http\Helpers\PageSpeedHelper;

class RemoveComments extends PageSpeedHelper
{
    public function apply($buffer)
    {
        $replace = [
            '/(\/\*[\w\'\s\r\n\*]*\*\/)/s' => '',
            '/(\s\/\/[^\n\r]*)/s' => '',
            '/<!--[^]><!\[](.*?)[^\]]-->/s' => '',
        ];

        return $this->replace($replace, $buffer);
    }
}