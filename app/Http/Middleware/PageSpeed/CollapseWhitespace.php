<?php

namespace App\Http\Middleware\PageSpeed;

use Closure;
Use App\Http\Helpers\PageSpeedHelper;

class CollapseWhitespace extends PageSpeedHelper
{
    public function apply($buffer)
    {
        $replace = [
            "/\n([\S])/" => '$1',
            "/\t/" => ' ',
            "/\n/" => ' ',
            "/\r/" => ' ',
            "/ + /" => ' ',
            "/> +</" => '><',
        ];

        return $this->replace($replace, $buffer);
    }
    
}