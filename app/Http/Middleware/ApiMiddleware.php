<?php

namespace App\Http\Middleware;

use App\Entities\Device\UserDevice;
use App\User;
use Closure;

class ApiMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        app()->user = null;
        app()->device = null;

        return $next($request);
    }
}
