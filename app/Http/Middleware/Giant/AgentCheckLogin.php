<?php

namespace App\Http\Middleware\Giant;

use Closure;
use App\Entities\Agent\Agent;

class AgentCheckLogin
{
    private $request = null;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $this->request = $request;

        $this->login($request);

        return $next($request);
    }

    public function login($request)
    {
        $authHeader = $this->getAuthorizationHeader();

        $agentSignIn = $this->agent_signin($authHeader);
    }

    private function agent_signin($authHeader)
    {
        $checkPhoneNumber = Agent::where('phone_number', $authHeader['token'])->where('is_active', 1)->first();

        if (is_null($checkPhoneNumber)) {
            throw new \Exception(\Lang::get('api.authorization.token_invalid'), 330);
        }
        app()->agent = $checkPhoneNumber;
    }

    private function getAuthorizationHeader($soft = false)
    {
        try {
            $authData        = explode('GIT ', \Request::header('Authorization'));
            $authKeys        = explode(':', $authData[1]);
            $token           = $authKeys[1];
            $clientSignature = $authKeys[0];
        } catch (\Exception $e) {
            // Soft determine it's okay to not have valid token. It doesnt make error.
            if ($soft) return null;

            $detail = ApiHelper::traceThrowError($e);

            throw new \Exception(\Lang::get('api.authorization.authorization_header_invalid') . $detail, 256);
        }

        return array(
            'token'            => $token,
            'client_signature' => $clientSignature
        );
    }
}
