<?php

namespace App\Http\Middleware;

use App\Libraries\RequestHelper;
use Closure;
use Exception;

class IPGuard 
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $clientIp = RequestHelper::getClientIpRegardingCF();
        $allowedIp = config('ip.vpn');
        if (!in_array($clientIp, $allowedIp)) {
            throw new Exception('IP rejected');
        }

        return $next($request);
    }
}
