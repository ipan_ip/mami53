<?php

namespace App\Http\Controllers\Oauth\Cms;

use App\Entities\ComplaintSystem\ResponseType;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Room\Room;
use App\Http\Controllers\Controller;
use App\Transformers\Cms\ContractTransformer;
use App\Transformers\Cms\RoomTransformer;
use App\Transformers\Cms\UserTransformer;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ValidationController extends Controller
{
    const USER_NOT_RELATED_TO_CONTRACT = 0;
    const USER_IS_CONTRACT_TENANT = 1;
    const USER_IS_CONTRACT_OWNER = 2;

    const CONTRACT_USER_RELATION_MESSAGES_MAPPING = [
        self::USER_NOT_RELATED_TO_CONTRACT => 'Contract not related to user',
        self::USER_IS_CONTRACT_TENANT => 'User is Tenant on This Contract',
        self::USER_IS_CONTRACT_OWNER => 'User is Owner on This Contract'
    ];

    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request): JsonResponse
    {
        $user = Auth::user();
        if (is_null($user)) {
            return response()->json(
                [
                    'result' => ResponseType::AUTH_FAILED,
                    'message' => 'Not Authenticated'
                ],
                401
            );
        }

        $validator = Validator::make(
            $request->all(),
            [
                'kost_id' => 'bail|required|integer',
                'contract_id' => 'bail|required|integer',
                'user_id' => 'bail|required|integer'
            ]
        );
        if ($validator->fails()) {
            return response()->json(
                [
                    'result' => ResponseType::REQUIRED_PARAM_MISSING,
                    'message' => $validator->errors()->all()
                ],
                422
            );
        }

        $kostId = $request->input('kost_id');
        $contractId = $request->input('contract_id');
        $userId = $request->input('user_id');

        $room = Room::where('song_id', $kostId)->first();
        $contract = MamipayContract::find($contractId);
        $requestedUser = User::find($userId);

        $validator = Validator::make(
            [
                'room' => $room,
                'contract' => $contract,
                'user' => $requestedUser
            ],
            [
                'contract' => 'required',
                'room' => 'required',
                'user' => 'required'
            ],
            [
                'room.required' => 'Room Not Exist',
                'contract.required' => 'Contract Not Exist',
                'user.required' => 'User Not Exist'
            ]
        );

        if ($validator->fails()) {
            return response()->json(
                [
                    'result' => ResponseType::INVALID_INPUT,
                    'message' => $validator->errors()->all()
                ],
                404
            );
        }

        $transformedContract = (new ContractTransformer)->transform($contract);
        if (! $this->contractAndKostIsMatched($contract, $room)) {
            return response()->json(
                [
                    'result' => ResponseType::INVALID_INPUT,
                    'message' => "Contract and Kos Inputed didn't Match",
                    'contract' => $transformedContract,
                ],
                422
            );
        }

        $contractToUserRelation = $this->contractToUserRelation($contract, $requestedUser);
        $transformedContract = array_merge(
            $transformedContract,
            [
                'relation' => [
                    'code' => $contractToUserRelation,
                    'message' => self::CONTRACT_USER_RELATION_MESSAGES_MAPPING[$contractToUserRelation]
                ]
            ]
        );

        if ($contractToUserRelation === self::USER_NOT_RELATED_TO_CONTRACT) {
            return response()->json(
                [
                    'result' => ResponseType::INVALID_INPUT,
                    'message' => self::CONTRACT_USER_RELATION_MESSAGES_MAPPING[self::USER_NOT_RELATED_TO_CONTRACT],
                    'contract' => $transformedContract,
                ],
                422
            );
        }

        return response()->json([
            'result' => ResponseType::SUCCESS,
            'contract' => $transformedContract,
            'user' => (new UserTransformer)->transform($requestedUser),
            'kost' => (new RoomTransformer)->transform($room)
        ]);
    }

    /**
     * contract is matched with inputed kost
     */
    private function contractAndKostIsMatched(MamipayContract $contract, Room $room): bool
    {
        if (is_null($contract->kost)) {
            return false;
        }

        return $contract->kost->designer_id === $room->id;
    }

    /**
     * contract to user relation maps
     */
    private function contractToUserRelation(MamipayContract $contract, User $user): int
    {
        if (! is_null($contract->tenant) && $contract->tenant->user_id === $user->id) {
            return self::USER_IS_CONTRACT_TENANT;
        }

        if (! is_null($contract->owner_profile) && $contract->owner_profile->user_id === $user->id) {
            return self::USER_IS_CONTRACT_OWNER;
        }

        return self::USER_NOT_RELATED_TO_CONTRACT;
    }
}
