<?php

namespace App\Http\Controllers\Oauth\Cms;

use App\Entities\ComplaintSystem\ResponseType;
use App\Http\Controllers\Controller;
use App\Repositories\UserDataRepositoryEloquent;
use App\Transformers\Cms\UserTransformer;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{

    /** 
     * Get user by it's id for CMS service
     * 
     * @param integer user_id
     * @return \Illuminate\Http\JsonResponse user
     */
    public function getUserById($userId, UserDataRepositoryEloquent $repository): JsonResponse
    {
        $user = $repository->getUserById($userId);

        if (is_null($user)) { // not found
            return response()->json([
                "result" => ResponseType::INVALID_INPUT,
                "user" => null
            ],
            404
        );
        }

        $transformedUser = (new UserTransformer)->transform($user);
        return response()->json(
            [
                "result" => ResponseType::SUCCESS,
                "user" => $transformedUser
            ]
        );
    }

    /**
     * check Who Am I currently using the token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function whoAmI(): JsonResponse
    {
        $user = Auth::user();
        if (is_null($user)) {
            return response()->json(
                [
                    'result' => ResponseType::AUTH_FAILED,
                    'message' => 'Not Authenticated'
                ],
                401
            );
        }

        return response()->json(
            [
                "result" => ResponseType::SUCCESS,
                "user" => (new UserTransformer)->transform($user)
            ]
        );
    }
    
}
