<?php

namespace App\Http\Controllers\Oauth\Cms;

use App\Entities\ComplaintSystem\ResourceType;
use App\Entities\ComplaintSystem\ResponseType;
use App\Entities\ComplaintSystem\RoleType;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Room\Room;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

/**
 * permission handling api
 */
class PermissionController extends Controller
{
    const PERMISSION_GRANTED = 1;
    const PERMISSION_REJECTED = 0;

    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request): JsonResponse
    {
        $user = Auth::user();
        if (is_null($user)) {
            return response()->json(
                [
                    'result' => ResponseType::AUTH_FAILED,
                    'message' => 'Not Authenticated'
                ],
                401
            );
        }

        $validator = Validator::make($request->all(), ['resources' => 'required']);
        $resources = json_decode($request->input('resources'), true);
        $unprocessableResources = null;
        $validator->after(function ($validator) use ($resources, $unprocessableResources) {
            if (empty($resources)) {
                $validator->errors()->add('resources', 'Resources to be accessed is empty or not a valid json');
                return;
            }

            $unprocessableResources = [];
            foreach ($resources as $index => $resource) {
                $resourceValidator = Validator::make(
                    $resource,
                    [
                        'resource_type' => 'required|integer',
                        'resource_id' => 'required|integer',
                        'role' => 'required|integer',
                    ]
                );
                if ($resourceValidator->fails()) {
                    $validator->errors()->merge(
                        $resourceValidator->errors()->all()
                    );
                    $validator->errors()->add(
                        "resource $index",
                        "Resource $index not well formatted"
                    );

                    $unprocessableResources[] = $resource;
                    return;
                }
            }
        });

        if ($validator->fails()) {
            return response()->json(
                [
                    'result' => ResponseType::REQUIRED_PARAM_MISSING,
                    'message' => $validator->errors()->all(),
                    'unprocessable_resources' => $unprocessableResources
                ],
                422
            );
        }
        
        $resourceCollection = collect($resources);
        $permissions = $resourceCollection->map(function ($resource) use ($user) {
            if ($resource['resource_type'] === ResourceType::TYPE_ROOM) {
                $room = Room::with('owners')->where('song_id', $resource['resource_id'])->first();
                if (! is_null($room)) {
                    if ($user->isAdmin()) {
                        return $this->approveGrant($resource);
                    }
                    
                    if ($user->isOwner() && $resource['role'] === RoleType::TYPE_OWNER) {
                        if (in_array($user->id, $room->owners->pluck('user_id')->toArray())) {
                            return $this->approveGrant($resource);
                        }
                    }

                    if ($resource['role'] === RoleType::TYPE_TENANT) {
                        return $this->approveGrant($resource);
                    }
                }

                return $this->disApproveGrant($resource);
            }
            
            $contract = MamipayContract::find($resource['resource_id']);
            if (! is_null($contract)) {
                if ($user->isAdmin()) {
                    return $this->approveGrant($resource);
                }

                if ($user->isOwner() && ($resource['role'] === RoleType::TYPE_OWNER)) {
                    if (
                        ! is_null($contract->owner_profile)
                        && $user->id === $contract->owner_profile->user_id
                    ) {
                        return $this->approveGrant($resource);
                    }
                }

                if ($resource['role'] === RoleType::TYPE_TENANT) {
                    return $this->approveGrant($resource);
                }
            }

            return $this->disApproveGrant($resource);
        });

        return response()->json([
            'result' => ResponseType::SUCCESS,
            'user_id' => (int) $user->id,
            'user_status' => (int) $user->isVerified(),
            'permissions' => $permissions
        ]);
    }

    /**
     * approve grant request
     */
    private function approveGrant(array $input): array
    {
        return array_merge($input, ['grant' => self::PERMISSION_GRANTED]);
    }

    /**
     * disapprove grant request
     */
    private function disApproveGrant(array $input): array
    {
        return array_merge($input, ['grant' => self::PERMISSION_REJECTED]);
    }
}
