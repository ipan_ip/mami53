<?php

namespace App\Http\Controllers\Oauth\Cms;

use App\Entities\ComplaintSystem\ResponseType;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use App\Repositories\RoomRepositoryEloquent;
use App\Transformers\Cms\RoomTransformer;

class RoomController extends Controller
{

    /** 
     * getRoom handle request for get room api by its song_id
     * 
     * @param Integer $songId
     * @return JsonResponse
     */
    public function getRoom($songId, RoomRepositoryEloquent $repository): JsonResponse
    {
        $room = $repository->getRoomBySongId($songId);

        if (is_null($room)) { // not found
            return response()->json(
                [
                    "result" => ResponseType::REQUIRED_PARAM_MISSING,
                    "kost" => null
                ],
                404
            );
        }

        $transformedRoom = (new RoomTransformer)->transform($room);
        return response()->json(
            [
                "result" => ResponseType::SUCCESS,
                "kost" => $transformedRoom
            ]
        );
    }
    
}