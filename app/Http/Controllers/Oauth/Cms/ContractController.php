<?php

namespace App\Http\Controllers\Oauth\Cms;

use App\Entities\ComplaintSystem\ResponseType;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use App\Entities\Mamipay\MamipayContract;
use App\Transformers\Cms\ContractTransformer;

class ContractController extends Controller
{

    /** 
     * getContract handle request for get contract api by its contract_id
     * 
     * @param Int $contractId
     * @return JsonResponse
     */
    public function getContract($contractId): JsonResponse
    {
        $contract = MamipayContract::find($contractId);
        if (is_null($contract)) {
            return response()->json(
                [
                    "result" => ResponseType::REQUIRED_PARAM_MISSING,
                    "contract" => null
                ],
                404
            );
        }

        $tranformedContract = (new ContractTransformer)->transform($contract);
        return response()->json(
            [
                "result" => ResponseType::SUCCESS,
                "contract" => $tranformedContract
            ]
        );
    }
    
}