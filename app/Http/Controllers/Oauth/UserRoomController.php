<?php

namespace App\Http\Controllers\Oauth;

use App\Entities\Room\RoomOwner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Helpers\ApiHelper;
use Illuminate\Support\Facades\Auth;

class UserRoomController extends Controller
{
    
    /**
     * Handle the incoming request.
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $user = Auth::user();
        
	    if (! $user->is_owner == 'true') {
            return response()->json(
                [
                    'message' => 'User not an owner',
                    'user' => $user
                ], 
                400
            );
        }

	    $roomData = RoomOwner::ownerListRoom($request->all(), $user);

        return ApiHelper::responseData(['rooms' => $roomData['room'], 'count' => $roomData['count']]);
    }
}
