<?php

namespace App\Http\Controllers\Oauth;

use App\Http\Controllers\Controller;
use App\Repositories\UserDataRepository;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Current user profile
     *
     */
    public function current(UserDataRepository $repository): JsonResponse
    {
        $user = Auth::user();
        if ($user instanceof User) {
            if (! $user->is_owner == 'true') {
                return response()->json(
                    [
                        'message' => 'User not an owner',
                        'user' => $user
                    ], 
                    400
                );
            }

            $data = $repository->getProfileUser($user, 'web');

            return response()->json([
                'data' => $data
            ]);
        }

        return response()->json(
            [
                'message' => 'Not authenticated'
            ],
            401
        );
    }

    /**
     * get user programatically
     */
    public function getUser($idUser, UserDataRepository $repository): JsonResponse
    {
        $user = User::find($idUser);
        if ($user instanceof User) {
            $data = $repository->getProfileUser($user, 'web');

            return response()->json([
                'data' => $data
            ]);
        }

        return response()->json(
            [
                'message' => 'User not exist'
            ],
            404
        );
    }
}
