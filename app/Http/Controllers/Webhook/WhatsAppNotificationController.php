<?php

namespace App\Http\Controllers\Webhook;

use App\Entities\Notif\NotificationWhatsappTemplate;
use App\Http\Helpers\ApiResponse;
use Bugsnag;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use RuntimeException;

class WhatsAppNotificationController extends BaseController
{
	/**
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse|\RuntimeException
	 */
	public function requestAccepted(Request $request)
	{
		try
		{
			NotificationWhatsappTemplate::dispatch('triggered', 'get_booking_accepted', $request->all());
		}
		catch (Exception $e)
		{
			Bugsnag::notifyException($e);

			return ApiResponse::responseData([
				'status'  => false,
				'message' => $e->getMessage(),
			]);
		}

		return ApiResponse::responseData([
			'status' => true,
			'data'   => $request->all(),
		]);
	}

	/**
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
	 */
	public function newBookingRequested(Request $request)
	{
		// TODO: trigger notification dispatch here

		return response($request);
	}

	/**
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse|\RuntimeException
	 */
	public function paidDownPayment(Request $request)
	{
		try
		{
			NotificationWhatsappTemplate::dispatch('triggered', 'transaction_down_payment', $request->all());
		}
		catch (Exception $e)
		{
			Bugsnag::notifyException($e);

			return ApiResponse::responseData([
				'status'  => false,
				'message' => $e->getMessage(),
			]);
		}

		return ApiResponse::responseData([
			'status' => true,
			'data'   => $request->all(),
		]);
	}

	public function paidSettlement(Request $request)
	{
		try
		{
			NotificationWhatsappTemplate::dispatch('triggered', 'transaction_settlement_payment', $request->all());
		}
		catch (Exception $e)
		{
			Bugsnag::notifyException($e);

			return ApiResponse::responseData([
				'status'  => false,
				'message' => $e->getMessage(),
			]);
		}

		return ApiResponse::responseData([
			'status' => true,
			'data'   => $request->all(),
		]);
	}

	public function paidFullPayment(Request $request)
	{
		try
		{
			NotificationWhatsappTemplate::dispatch('triggered', 'transaction_full_payment', $request->all());
		}
		catch (Exception $e)
		{
			Bugsnag::notifyException($e);

			return ApiResponse::responseData([
				'status'  => false,
				'message' => $e->getMessage(),
			]);
		}

		return ApiResponse::responseData([
			'status' => true,
			'data'   => $request->all(),
		]);
	}

	public function paidRecurring(Request $request)
	{
		try
		{
			NotificationWhatsappTemplate::dispatch('triggered', 'transaction_invoice_payment', $request->all());
		}
		catch (Exception $e)
		{
			Bugsnag::notifyException($e);

			return ApiResponse::responseData([
				'status'  => false,
				'message' => $e->getMessage(),
			]);
		}

		return ApiResponse::responseData([
			'status' => true,
			'data'   => $request->all(),
		]);
	}

	public function paidOutDownPayment(Request $request)
	{
		try
		{
			NotificationWhatsappTemplate::dispatch('triggered', 'payout_down_payment', $request->all());
		}
		catch (Exception $e)
		{
			Bugsnag::notifyException($e);

			return ApiResponse::responseData([
				'status'  => false,
				'message' => $e->getMessage(),
			]);
		}

		return ApiResponse::responseData([
			'status' => true,
			'data'   => $request->all(),
		]);
	}

	public function paidOutSettlement(Request $request)
	{
		try
		{
			NotificationWhatsappTemplate::dispatch('triggered', 'payout_settlement_payment', $request->all());
		}
		catch (Exception $e)
		{
			Bugsnag::notifyException($e);

			return ApiResponse::responseData([
				'status'  => false,
				'message' => $e->getMessage(),
			]);
		}

		return ApiResponse::responseData([
			'status' => true,
			'data'   => $request->all(),
		]);
	}

	public function paidOutFullPayment(Request $request)
	{
		try
		{
			NotificationWhatsappTemplate::dispatch('triggered', 'payout_full_payment', $request->all());
		}
		catch (Exception $e)
		{
			Bugsnag::notifyException($e);

			return ApiResponse::responseData([
				'status'  => false,
				'message' => $e->getMessage(),
			]);
		}

		return ApiResponse::responseData([
			'status' => true,
			'data'   => $request->all(),
		]);
	}

	public function paidOutRecurring(Request $request)
	{
		try
		{
			NotificationWhatsappTemplate::dispatch('triggered', 'payout_invoice_payment', $request->all());
		}
		catch (Exception $e)
		{
			Bugsnag::notifyException($e);

			return ApiResponse::responseData([
				'status'  => false,
				'message' => $e->getMessage(),
			]);
		}

		return ApiResponse::responseData([
			'status' => true,
			'data'   => $request->all(),
		]);
	}

	public function paidBooking(Request $request)
	{
		try
		{
			NotificationWhatsappTemplate::dispatch('triggered', 'transaction_payment', $request->all());
		}
		catch (Exception $e)
		{
			Bugsnag::notifyException($e);

			return ApiResponse::responseData([
				'status'  => false,
				'message' => $e->getMessage(),
			]);
		}

		return ApiResponse::responseData([
			'status' => true,
			'data'   => $request->all(),
		]);
	}

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function recurringUnpaidInvoice(Request $request): JsonResponse
    {
        try {
            $type = $request->whatsapp_template ?? null;
            if ($type == null)
                throw new Exception('Whatsapp template not found');

            NotificationWhatsappTemplate::dispatch('triggered', $type, $request->all());
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
            return ApiResponse::responseData([
                'status'  => false,
                'message' => $e->getMessage(),
            ]);
        }
        return ApiResponse::responseData([
            'status' => true,
            'data'   => $request->all(),
        ]);
    }
}
