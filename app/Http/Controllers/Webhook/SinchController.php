<?php
namespace App\Http\Controllers\Webhook;

use App\Http\Helpers\ApiResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Helpers\ApiResponse as Api;
use App\Http\Helpers\ApiHelper;
use App\Http\Requests;

class SinchController extends Controller
{

    /**
    * Function to sign Request callback from sinch.
    */
    private function signRequest(Request $request, $callbackType)
    {
        $key = "ec3767be-774c-4a1e-af55-00cba177694f";    
        $secret = '1zYFAUcoFUGBL/MLS9b5Dg==';

        // $body = $request->getContent();
        // \Log::info($body);

        // $timestamp = $request->input('timestamp');
        // \Log::info($timestamp);

        // $path                  = '/sinch/callback/' . $callbackType;
        // $content_type          = "application/json";
        // $canonicalized_headers = "x-timestamp:" . $timestamp;

        // $content_md5 = base64_encode( md5( utf8_encode($body), true ));
        // \Log::info($content_md5);

        // $string_to_sign =
        //     "POST\n".
        //     $content_md5."\n".
        //     $content_type."\n".
        //     $canonicalized_headers."\n".
        //     $path;

        // $signature = base64_encode(hash_hmac("sha256", utf8_encode($string_to_sign), base64_decode($secret), true));

        // $authorizationHeader = $request->header('Authorization');
        // $sinchAuthorizationHeader = str_replace('Application ', '', $authorizationHeader);
        // $sinchAuthorizationHeader = explode(':', $sinchAuthorizationHeader);

        // \Log::info($sinchAuthorizationHeader[0]);
        // \Log::info($sinchAuthorizationHeader[1]);
        // \Log::info($signature);

        // if($sinchAuthorizationHeader[0] == $key && $signature == $sinchAuthorizationHeader[1]) {
        //     return true;
        // }

        // return false;

        if($request->input('applicationKey') != $key) {
            return false;
        }

        return true;
        
    }

    public function handleEvent(Request $request)
    {
        $event = $request->input('event');

        // if(!$this->signRequest($request, $event)) {
        //     return \Response::json($this->buildSvaml(null, ['name'=>'Hangup']));
        // }
        
        if($event == 'ice') {
            return $this->handleIceEvent($request);
        } elseif($event == 'ace') {
            return $this->handleAceEvent($request);
        } elseif($event == 'dice') {
            return $this->handleDiceEvent($request);
        }
    }

    /**
    * Handle Incoming Call Event from Sinch
    * refer to this documentation : https://www.sinch.com/docs/voice/rest/#ICE
    */
    private function handleIceEvent(Request $request)
    {
        $instruction = null;

        $action = [
            'name'=>'ConnectPSTN',
            'maxDuration'=>600,
            'cli'=>'+6287738724848'
        ];

        return \Response::json($this->buildSvaml($instruction, $action));
    }

    /**
    * Handle Answered Call Event from Sinch
    * refer to this documentation : https://www.sinch.com/docs/voice/rest/#ACE
    */
    private function handleAceEvent(Request $request)
    {
        $instruction = null;

        $action = [
            'name'=>'Continue'
        ];

        return \Response::json($this->buildSvaml($instruction, $action));
    }

    private function handleDiceEvent(Request $request)
    {
        $instruction = null;

        $action = [
            'name'=>'Hangup'
        ];

        return \Response::json($this->buildSvaml($instruction, $action));
    }

    private function buildSvaml($instruction, $action)
    {
        $svaml = [];

        if(!is_null($instruction)) {
            $svaml['instruction'] = [$instruction];
        }

        $svaml['action'] = $action;

        return $svaml;
    }
}