<?php
namespace App\Http\Controllers\Webhook;

use Illuminate\Http\Request;

use Bugsnag;
use Config;
use App\Libraries\MessengerLibrary;
use App\Libraries\WitAILibrary;
use App\Entities\Room\RoomFilter;
use App\Entities\Room\Room;
use Cache;

class MessengerController extends BaseController
{
    protected $verificationToken = 'miGuzgVimqJOwrkmNCIRQEw2bxJu2KMq';
    protected $messenger;
    protected $aiProvider;

    protected $mode;
    protected $token;

    public function __construct(Request $request)
    {
        $this->messenger = new MessengerLibrary();

        $this->aiProvider = new WitAILibrary();

//        $this->verifySignature($request);
    }

    public function verifySignature(Request $request)
    {
        $signature = $request->header('x-hub-signature');

        if(!$signature) {
            throw new \Exception('Signature : ' . $signature . ' is not valid');
        } else {
            $elements = explode('=', $signature);
            $method = $elements[0];
            $signatureHash = $elements[1];

            $expectedHash = hash_hmac('sha1', $request->getContent(), $this->messenger->getAppSecret());

            if($signatureHash != $expectedHash) {
                throw new \Exception('Signature is not valid');
            }

        }
    }

    public function handleGetWebhook(Request $request)
    {
        $this->mode = $request->get('hub_mode');
        $this->token = $request->get('hub_verify_token');

        if($this->mode == 'subscribe' && $this->token == $this->verificationToken) {
            return response($request->get('hub_challenge'), 200)
                  ->header('Content-Type', 'text/plain');
        } else {
            return abort(403);
        }
    }

    public function handlePostWebhook(Request $request)
    {
        // return 'true';
        $httpRequestBody = json_decode($request->getContent());

        // \Log::info((array) $httpRequestBody);

        if($httpRequestBody->object == 'page') {
            foreach($httpRequestBody->entry as $entry) {
                $pageId = $entry->id;
                $timeOfEvent = $entry->time;

                foreach($entry->messaging as $messagingEvent) {
                    if(isset($messagingEvent->message)) {
                        $this->receiveMessage($messagingEvent);
                    } elseif(isset($messagingEvent->postback)) {
                        $this->receivePostback($messagingEvent);
                    } else {
                        $this->sendDefaultResponse($messagingEvent->sender->id);
                    }
                }
            }
        }
    }

    public function receiveMessage($event)
    {
        if($this->isBotStopped($event->sender->id)) {
            return true;
        }

        if(isset($event->message->quick_reply)) {
            $this->receivePostback($event, $event->message->quick_reply->payload);
        } elseif(isset($event->message->text)) {
            $userText = strtolower($event->message->text);

            $messageEntities = $this->aiProvider->getMessage($userText);

            // prevent multiple error sent by multiple messenger request
            if(empty($messageEntities)) {
                return 'ok';
            }

            $action = ['name' => '', 'score' => 0];
            $gender = ['name' => '', 'score' => 0];
            $location = ['name' => '', 'score' => 0];
            $price = ['name' => '', 'score' => 0];

            if(count($messageEntities->entities) > 0) {
                foreach($messageEntities->entities as $entityType => $entities) {
                    foreach($entities as $entity) {

                        if($entityType == 'intent') {
                            if($entity->confidence > $action['score']) {
                                $action = [
                                    'name' => $entity->value,
                                    'score' => $entity->confidence
                                ];
                            }
                        }
                    }
                }
            }

            $conversationContext = $this->getConversationContext($event->sender->id);

            if($conversationContext !== false) {
                if($conversationContext == 'cari_kost') {
                    $this->processRecommendation($event, $messageEntities);
                }
            } else {
                if($action['name'] == 'daftar_kost') {
                    $this->sendDaftarKost($event);
                } elseif($action['name'] == 'cari_kost') {
                    $this->processRecommendation($event, $messageEntities);
                } elseif($action['name'] == 'stop_bot') {
                    $this->stopBot($event->sender->id);
                } elseif($action['name'] == 'daftar_agent') {
                    $this->sendDaftarAgent($event);
                } else {   
                    if($userText == 'bantuan') {
                        $this->sendHelp($event->sender->id);
                    } else {
                        $this->sendDefaultResponse($event->sender->id);
                    }
                }
            }

        }
        
    }

    public function receivePostback($event, $payload = '')
    {
        if($payload == '' && $event->postback->payload) {
            $payload = $event->postback->payload;
        }
        
        if(strtolower($payload) == 'bantuan') {
            $this->sendHelp($event->sender->id);
        } elseif(strtolower($payload) == 'daftar kost') {
            $this->sendDaftarKost($event);
        } elseif(strtolower($payload) == 'daftar agen') {
            $this->sendDaftarAgent($event);
        } elseif(strtolower($payload) == 'stop bot') {
            $this->stopBot($event->sender->id);
        } else {
            $this->removeAllUserCache($event->sender->id);
            $this->messenger->sendTextMessage('Terima Kasih', $event->sender->id);
        }
    }

    public function sendDaftarKost($event)
    {
        $this->removeAllUserCache($event->sender->id);

        $this->messenger->sendGenericTemplate([
            [
                'title' => 'Daftar Kost',
                'image_url' => 'https://mamikos.com/assets/logo.png',
                'subtitle' => 'Dapatkan pulsa Rp. 10.000 gratis dengan mendaftarkan kost di sini',
                'default_action' => [
                    'type' => 'web_url',
                    'url' => 'https://mamikos.com/input/messenger',
                    'webview_height_ratio' => 'tall', 
                    'messenger_extensions' => true
                ],
                'buttons' => [
                    [
                        'type' => 'web_url',
                        'url' => 'https://mamikos.com/input/messenger',
                        'webview_height_ratio' => 'tall', 
                        'messenger_extensions' => true,
                        'title' => 'DAFTAR KOST'
                    ]
                ]
            ]
        ], $event->sender->id);
    }

    public function sendDaftarAgent($event)
    {
        $this->removeAllUserCache($event->sender->id);

        $agentMessage = "Untuk mendaftar sebagai agen MamiKos, silakan kunjungi tautan berikut ini : \n" .
                        "https://mamikos.com/daftar-agen";

        $this->messenger->sendTextMessage($agentMessage, $event->sender->id);
    }

    public function sendRecommendation($event, $params = [])
    {
        $this->removeAllUserCache($event->sender->id);

        $filter = new RoomFilter($params);

        $rooms = Room::attachFilter($filter)
                    ->with('photo', 'tags', 'apartment_project')
                    ->take(5)
                    ->get();

        

        if(count($rooms) > 0) {
            $messageElements = [];

            foreach($rooms as $room) {
                $messageElements[] = [
                    'title' => $room->name,
                    'image_url' => !is_null($room->photo) ? $room->photo->getMediaUrl()['small']  : 'https://mamikos.com/assets/logo.png',
                    'subtitle' => '',
                    'default_action' => [
                        'type' => 'web_url',
                        'url' => $room->share_url,
                        'messenger_extensions' => false,
                    ],
                    'buttons' => [
                        [
                            'type' => 'web_url',
                            'url' => $room->share_url,
                            'messenger_extensions' => false,
                            'title' => 'LIHAT KOST'
                        ]
                    ]
                ];
            }

            $this->messenger->sendGenericTemplate($messageElements, $event->sender->id);
        } else {
            $this->messenger->sendTextMessage('Maaf, kami belum dapat menemukan kost sesuai kriteria Anda.', $event->sender->id);
        }
        
    }

    public function sendHelp($recipientId)
    {
        $this->removeAllUserCache($recipientId);

        $helpMessage = "Halo, lewat Bot ini Anda dapat menambahkan kost baru ke aplikasi Mamikos. \n\n" .
                            "Caranya mudah, cukup ketikkan DAFTAR KOST. \n\n" .
                            "Untuk selanjutnya Anda akan dibawa ke halaman khusus untuk menambahkan kost.\n" . 
                            "Mamikos juga menyediakan hadiah menarik untuk setiap kost yang ditambahkan " .
                            "dan berhasil terverifikasi untuk ditampilkan di aplikasi Mamikos\n\n" .
                            "Untuk mendaftar sebagai agen Mamikos, ketikkan DAFTAR AGEN\n\n" . 
                            "Untuk menghentikan bot membalas otomatis, ketikkan STOP BOT.\n\n" .
                            "Selamat Mencoba";

        // $this->messenger->sendTextMessage($helpMessage, $recipientId);

        $this->messenger->sendQuickReply([
            'text' => $helpMessage,
            'quick_replies' => [
                [
                    'content_type' => 'text',
                    'title' => 'DAFTAR KOST',
                    'payload' => 'DAFTAR KOST'
                ],
                [
                    'content_type' => 'text',
                    'title' => 'DAFTAR AGEN',
                    'payload' => 'DAFTAR AGEN'
                ],
                [
                    'content_type' => 'text',
                    'title' => 'STOP BOT',
                    'payload' => 'STOP BOT'
                ]
            ]
        ], $recipientId);
    }

    public function sendDefaultResponse($recipientId)
    {
        $this->removeAllUserCache($recipientId);

        $this->messenger->sendQuickReply([
            'text' => 'Halo, butuh bantuan ?',
            'quick_replies'=> [
                [
                    'content_type' => 'text',
                    'title' => 'BANTUAN',
                    'payload' => 'BANTUAN'
                ],
                [
                    'content_type' => 'text',
                    'title' => 'DAFTAR KOST',
                    'payload' => 'DAFTAR KOST'
                ],
                [
                    'content_type' => 'text',
                    'title' => 'STOP BOT',
                    'payload' => 'STOP BOT'
                ]
            ]
        ], $recipientId);
    }


    public function removeAllUserCache($recipientId)
    {
        Cache::forget('bot:' . $recipientId);
    }

    public function setUserCache($recipientId, $data)
    {
        Cache::put('bot:' . $recipientId, $data, 120);
    }

    public function getConversationContext($recipientId)
    {
        if(Cache::has('bot:' . $recipientId)) {

            $cacheData = Cache::get('bot:' . $recipientId);
            
            return $cacheData['context'];

        } else {
            return false;
        }
    }

    public function processRecommendation($event, $messageEntities)
    {
        $cacheData = Cache::get('bot:' . $event->sender->id);

        $currentStep = isset($cacheData['step']) ? $cacheData['step'] : '';

        $context = 'cari_kost';
        $gender = isset($cacheData['gender']) ? $cacheData['gender'] : '';
        $location = isset($cacheData['location']) ? $cacheData['location'] : '';;
        $price = isset($cacheData['price']) ? $cacheData['price'] : '';

        if(count($messageEntities->entities) > 0) {
            $genderTemp = ['name' => '', 'score' => 0];
            $locationTemp = ['name' => '', 'score' => 0];
            $priceTemp = ['name' => '', 'score' => 0];

            foreach($messageEntities->entities as $entityType => $entities) {
                foreach($entities as $entity) {

                    if($entityType == 'gender') {
                        if($entity->confidence > $genderTemp['score']) {
                            $genderTemp = [
                                'name' => $entity->value,
                                'score' => $entity->confidence
                            ];
                        }
                    } elseif($entityType == 'location') {
                        if($entity->confidence > $locationTemp['score']) {
                            $locationTemp = [
                                'name' => $entity->value,
                                'score' => $entity->confidence
                            ];
                        }
                    } elseif($entityType == 'price') {
                        if($entity->confidence > $priceTemp['score']) {
                            $priceTemp = [
                                'name' => $entity->value,
                                'score' => $entity->confidence
                            ];
                        }
                    }
                    
                }
            }

            if(in_array($currentStep, ['', 'ask_location'])) {
                $location = $locationTemp['name'];
            }

            if(in_array($currentStep, ['', 'ask_gender'])) {
                $gender = $genderTemp['name'];
            }

            if(in_array($currentStep, ['', 'ask_price'])) {
                $price = $priceTemp['name'];
            }
        }

        $dataComplete = true;

        if($location == '') {
            $dataComplete = false;

            $newCacheData = [
                'context' => $context,
                'gender' => $gender,
                'location' => $location,
                'price' => $price,
                'step' => 'ask_location'
            ];

            $this->removeAllUserCache($event->sender->id);

            $this->setUserCache($event->sender->id, $newCacheData);

            $this->messenger->sendTextMessage('Di kota apa Anda mau mencari kost?', $event->sender->id);
        } elseif($gender == '') {
            if($currentStep == 'ask_gender') {
                if($gender == '') $gender = '-';
            } else {
                $dataComplete = false;

                $newCacheData = [
                    'context' => $context,
                    'gender' => $gender,
                    'location' => $location,
                    'price' => $price,
                    'step' => 'ask_gender'
                ];

                $this->removeAllUserCache($event->sender->id);

                $this->setUserCache($event->sender->id, $newCacheData);

                $this->messenger->sendTextMessage('Ingin kost khusus putra atau putri?', $event->sender->id);
            }
        }

        if($dataComplete) {
            $this->removeAllUserCache($event->sender->id);

            $genderCode = [0];

            if($gender == 'pria') {
                $genderCode[] = 1;
            } elseif($gender == 'wanita') {
                $genderCode[] = 2;
            }

            $this->messenger->sendTextMessage('Tunggu sebentar kami sedang mencarikan rekomendasi kost ' . $gender . ' di ' . $location . ' untuk Anda', $event->sender->id);

            $this->sendRecommendation($event, [
                'place' => [$location], 
                'price-range' => [0, 10000000],
                'gender' => $genderCode
            ]);
        }

    }

    public function stopBot($recipientId)
    {
        if(!Cache::has('bot-stop:' . $recipientId)) {

            $this->messenger->sendTextMessage('Bot akan berhenti membalas pesan Anda secara otomatis selama 24 jam', $recipientId);

            Cache::put('bot-stop:' . $recipientId, 'true', 60 * 24);
        }
    }

    public function isBotStopped($recipientId)
    {
        return Cache::has('bot-stop:' . $recipientId);
    }
}