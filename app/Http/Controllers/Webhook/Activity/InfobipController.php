<?php

namespace App\Http\Controllers\Webhook\Activity;

use App\Entities\Activity\ActivationCodeDeliveryReport;
use App\Enums\Activity\ActivationCodeDeliveryReportStatus;
use App\Http\Controllers\Webhook\BaseController;
use App\Libraries\Activity\Infobip\InfobipConverter;
use Illuminate\Http\Request;
use App\Services\Activity\ActivationCodeService;
use Exception;
use Illuminate\Support\Facades\Validator;
use App\Http\Helpers\ApiResponse;
use App\Traits\Activity\ActivityDeliveryReportConverter;
use Bugsnag;
use Illuminate\Http\JsonResponse;
use Throwable;

/**
 * Controller to handle infobip webhook notification.
 */
class InfobipController extends BaseController
{
    use ActivityDeliveryReportConverter;

    protected $activationCodeService;

    public function __construct(ActivationCodeService $activationCodeService)
    {
        $this->activationCodeService = $activationCodeService;
    }

    /**
     * Handle send sms delivery report update from infobip.
     *
     * @param int $activationCodeId
     * @param Request $request
     * @return JsonResponse
     */
    public function postDeliveryReport(int $activationCodeId, Request $request): JsonResponse
    {
        $activationCode = $this->activationCodeService->getActivationCodeWithTrashedById($activationCodeId);
        if (empty($activationCode)) {
            throw new Exception("Invalid URL (" . $activationCodeId . ")", 400);
        }
        if (!isset($request->results[0])) {
            throw new Exception("Invalid Report : " . $request->getContent(), 400);
        }
        // Use json_decode to convert the nested array to stdClass
        $providerDeliveryReport = InfobipConverter::convertToProviderDeliveryReport(json_decode($request->getContent()));
        $activationCodeDeliveryReport = $this->convertToActivationCodeDeliveryReport($providerDeliveryReport); 
        $this->activationCodeService->updateDeliveryReport($activationCode, $activationCodeDeliveryReport);

        return ApiResponse::responseData();
    }
}
