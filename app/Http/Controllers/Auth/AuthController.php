<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Socialite;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\SocialAccountService;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Redirect;

class AuthController extends Controller
{
    //use AuthenticatesUsers;

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }



    /**
     * Obtain the user information from GitHub.
     *
     * @return Response
     */
    public function handleProviderCallback(SocialAccountService $service)
    {

        $user = $service->createOrGetUser(Socialite::driver('facebook')->user());
        //dd($user);
        Auth::login($user);
        //$userLogin = Auth::user();
        //auth()->login($user);
       return redirect('/');
    }
}
