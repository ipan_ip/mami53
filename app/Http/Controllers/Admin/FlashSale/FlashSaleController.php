<?php

namespace App\Http\Controllers\Admin\FlashSale;

use App\Criteria\FlashSale\AdminCriteria;
use App\Entities\Activity\Action;
use App\Entities\Activity\ActivityLog;
use App\Entities\FlashSale\FlashSale;
use App\Entities\Landing\Landing;
use App\Http\Controllers\Web\MamikosController;
use App\Http\Helpers\ApiResponse as Api;
use App\Http\Helpers\RegexHelper;
use App\Presenters\FlashSalePresenter;
use App\Repositories\FlashSale\FlashSaleRepositoryEloquent;
use Artisan;
use Auth;
use Bugsnag;
use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Application;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use View;

class FlashSaleController extends MamikosController
{
    protected $title = 'Promo Ngebut Management';
    protected $repository;

    public const DEFAULT_LIMIT = 10;

    public function __construct(FlashSaleRepositoryEloquent $repository)
    {
        $this->middleware(
            function ($request, $next) {
                if (!Auth::user()->can('access-discount-management')) {
                    return redirect('admin');
                }

                return $next($request);
            }
        );

        View::share('contentHeader', $this->title);
        View::share('user', Auth::user());

        $this->repository = $repository;
    }

    /**
     * @return Factory|Application|RedirectResponse|\Illuminate\View\View
     */
    public function index()
    {
        $viewData = ['boxTitle' => $this->title];
        ActivityLog::LogIndex(Action::FLASH_SALE);
        return view('admin.contents.flash-sale.index', $viewData);
    }

    public function update($name)
    {
        $this->repository->setPresenter(new FlashSalePresenter('admin-update'));

        $response = $this->repository
            ->with(
                [
                    'areas.landings.landing',
                    'landings',
                    'banner'
                ]
            )
            ->findByField('name', trim($name));

        if (count($response['data']) < 1) {
            return abort(404);
        }

        $viewData = [
            'boxTitle' => $this->title,
            'data' => $response['data'][0]
        ];

        return view('admin.contents.flash-sale.update', $viewData);
    }

    /**
     * @return LengthAwarePaginator|Collection|mixed
     * @throws RepositoryException
     */
    public function getAllData(Request $request)
    {
        $limit = $request->get('limit', self::DEFAULT_LIMIT);

        $this->repository->setPresenter(new FlashSalePresenter());
        $this->repository->pushCriteria(new AdminCriteria());

        $response = $this->repository
            ->with(
                [
                    'areas.landings.landing',
                    'landings',
                    'banner'
                ]
            );

        $count = $response->count();
        $response = $response->paginate($limit);

        $response['total'] = $count;
        $response['rows'] = $response['data'];
        unset($response['data']);

        return $response;
    }

    /**
     * @param $id
     * @return LengthAwarePaginator|Collection|mixed
     */
    public function getData($id)
    {
        return $this->repository->find($id);
    }

    /**
     * @param Request $request
     * @return LengthAwarePaginator|Collection|mixed
     */
    public function storeData(Request $request)
    {
        try {
            $request->merge(
                [
                    'created_by' => auth()->user()->role . ' :: ' . auth()->user()->name
                ]
            );

            if ($request->filled('id')) {
                if (!is_numeric($request->id)) {
                    return $this->sendErrorResponse('Missing or malformed Promo data ID!');
                }

                if (!$this->repository->updateWithRelations($request->all())) {
                    return $this->sendErrorResponse('Failed updating Promo data. Please try again in 10 minutes.');
                }

                return Api::responseData(
                    [
                        'updated' => true
                    ]
                );
            }

            if (!$this->repository->create($request->all())) {
                return $this->sendErrorResponse('Failed creating Promo data. Please try again in 10 minutes.');
            }

            return Api::responseData(
                [
                    'created' => true
                ]
            );
        } catch (Exception $e) {
            Bugsnag::notifyException($e);

            return $this->sendErrorResponse(
                $e->getMessage(),
                'Internal Server Error',
                500
            );
        }
    }

    public function recalculateRoomScore(Request $request)
    {
        try {
            if (
                !$request->filled('id')
                || !is_numeric($request->id)
            ) {
                return $this->sendErrorResponse('Missing or malformed Flash Sale data ID!');
            }

            $flashSaleData = FlashSale::find((int)$request->id);

            if (is_null($flashSaleData)) {
                return $this->sendErrorResponse('Missing or malformed Flash Sale data ID!');
            }

            // Queue the command to recalculate LPL Scores
            dispatch(function () use ($flashSaleData) {
                Artisan::call('update:room-sort-score', [
                    '--flash-sale-id' => $flashSaleData->id
                ]);
            });

            return Api::responseData(
                [
                    'data' => $flashSaleData
                ]
            );
        } catch (Exception $e) {
            Bugsnag::notifyException($e);

            return $this->sendErrorResponse(
                $e->getMessage(),
                'Internal Server Error',
                500
            );
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function removeData(Request $request)
    {
        try {
            if (
                !$request->filled('id')
                || !is_numeric($request->id)
            ) {
                return $this->sendErrorResponse('Missing or malformed Promo data ID!');
            }

            $flashSaleData = FlashSale::find((int)$request->id);

            if (is_null($flashSaleData)) {
                return $this->sendErrorResponse('Missing or malformed Flash Sale data ID!');
            }

            if ($flashSaleData->isCurrentlyRunning()) {
                return $this->sendErrorResponse('Flash Sale is currently running! Please deactivate it first.');
            }

            if (!$this->repository->delete((int)$request->id)) {
                return $this->sendErrorResponse('Failed deleting Promo data. Please try again in 10 minutes.');
            }

            return Api::responseData(
                [
                    'deleted' => true
                ]
            );
        } catch (Exception $e) {
            Bugsnag::notifyException($e);

            return $this->sendErrorResponse(
                $e->getMessage(),
                'Internal Server Error',
                500
            );
        }
    }

    public function activate(Request $request)
    {
        try {
            if (
                !$request->filled('id')
                || !is_numeric($request->id)
            ) {
                return $this->sendErrorResponse('Missing or malformed Promo data ID!');
            }

            // compile data
            $data = [
                'is_active' => 1,
                'start_time' => Carbon::createFromFormat('d/m/Y H:i:s', $request->start_time)->toDateTimeString(),
                'end_time' => Carbon::createFromFormat('d/m/Y H:i:s', $request->end_time)->toDateTimeString()
            ];

            if (!$this->repository->update($data, (int)$request->id)) {
                return $this->sendErrorResponse('Failed activating Promo data. Please try again in 10 minutes.');
            }

            return Api::responseData(
                [
                    'title' => 'Success',
                    'message' => 'Promo data successfully activated'
                ]
            );
        } catch (Exception $e) {
            Bugsnag::notifyException($e);

            return $this->sendErrorResponse(
                $e->getMessage(),
                'Internal Server Error',
                500
            );
        }
    }

    public function deactivate(Request $request)
    {
        try {
            if (
                !$request->filled('id')
                || !is_numeric($request->id)
            ) {
                return $this->sendErrorResponse('Missing or malformed Promo data ID!');
            }

            $request->merge(
                [
                    'is_active' => 0,
                    'start_time' => null,
                    'end_time' => null
                ]
            );

            if (!$this->repository->update($request->all(), (int)$request->id)) {
                return $this->sendErrorResponse('Failed deactivating Promo data. Please try again in 10 minutes.');
            }

            return Api::responseData(
                [
                    'title' => 'Success',
                    'message' => 'Promo data successfully deactivated'
                ]
            );
        } catch (Exception $e) {
            Bugsnag::notifyException($e);

            return $this->sendErrorResponse(
                $e->getMessage(),
                'Internal Server Error',
                500
            );
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function verifyName(Request $request)
    {
        try {
            // Allow  alphanumeric only
            $name = strtolower(trim($request->name));
            $isValidCharacters = preg_match(RegexHelper::alphanumericAndSpace(), $name) === 1;
            if (!$isValidCharacters) {
                return $this->sendErrorResponse('Please use alphanumeric only!');
            }

            $name = preg_replace('/\s+/', '', $name);
            if (!FlashSale::validateName($name)) {
                return $this->sendErrorResponse('This codename is already in use. Please enter another one!');
            }

            return Api::responseData(
                [
                    'data' => [
                        'name' => $name
                    ],
                ]
            );
        } catch (Exception $e) {
            Bugsnag::notifyException($e);

            return $this->sendErrorResponse(
                $e->getMessage(),
                'Internal Server Error',
                500
            );
        }
    }

    public function getLandings(Request $request)
    {
        $landings = Landing::select(
            [
                'id',
                'heading_1',
                'type'
            ]
        )
            ->where('type', 'area')
            ->whereNotNull('parent_id');

        if ($request->filled('search')) {
            $landings->where('heading_1', 'like', '%' . $request->search . '%');
        }

        return $landings->paginate(10);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getHistory(Request $request)
    {
        try {
            $response = $this->repository
                ->with(
                    [
                        'histories' => function($q) {
                            $q->orderBy('created_at', 'desc');
                        }
                    ]
                )
                ->findByField('id', $request->id);

            if (!empty($response)) {
                $response = $this->repository->compileHistoryData($response[0]);
            }

            return Api::responseData(
                [
                    'data' => $response
                ]
            );
        } catch (Exception $e) {
            Bugsnag::notifyException($e);

            return $this->sendErrorResponse(
                $e->getMessage(),
                'Internal Server Error',
                500
            );
        }
    }

    /**
     * @param $message
     * @param string $severity
     * @param int $code
     * @return JsonResponse
     */
    public function sendErrorResponse($message, $severity = 'Bad Request', $code = 400)
    {
        return Api::responseError(
            $message,
            $severity,
            $code
        );
    }
}
