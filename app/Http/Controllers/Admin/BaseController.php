<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class BaseController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Auth Controller
    |--------------------------------------------------------------------------
    |
    | Used to authenticate whether it is user or administrator
    | and redirect to apropriate page
    |
    */

    /**
    * Validator
    */
    protected $validator;

    /**
    * Laravel to Bootstrap Validator
    */
    protected $mapLaravelToBootstrapRules = array(
        'required' => 'notEmpty',
        'email'    => 'emailAddress',
        'max'      => 'stringLength',
        'min'      => 'stringLength',
        'same'     => 'identical',
        'unique'   => 'callback'
    );

    /**
    * The layout that should be used for responses.
    */
    protected $layout = 'admin.layouts.main';

    public function __construct()
    {
        parent::__construct();

        $macroHelper = new MacroHelper;

        if (Request::is('admin/*') || Request::is('kost-admin/*')) {
            $this->_viewDataHeader();
        } else {
            $this->_viewDataHeaderEmbed();
        }

        $this->beforeFilter('csrf', array('on' => 'post'));

        View::share('url', url());
    }

    public function _viewDataHeader()
    {
        $names = explode(' ',trim(Auth::user()->name));
        $viewNickName = $names[0];

        $viewUser = array(
            'name'      => Auth::user()->name,
            'nick_name' => $viewNickName,
            'role'      => ucwords(Auth::user()->role),
        );

        View::share('user', $viewUser);
    }

    // This for Embed Controller
    public function _viewDataHeaderEmbed()
    {
        $designer = Session::get('embed.designer.data');
        // $user = User::find(3);
        $names = explode(' ',trim($designer->name));
        $viewNickName = $names[0];

        $photo = Media::getMediaUrl($designer->photo_id);

        $viewUser = array(
            'designer_id'    => $designer->id,
            'designer_photo' => $photo['small'],
            'name'           => $designer->name,
            'nick_name'      => $viewNickName,
            'role'           => 'designer',
        );

        View::share('user', $viewUser);
    }

    protected function _mappingLaravelToBootstrapRules($key)
    {
        $key = strtolower($key);
        return $this->mapLaravelToBootstrapRules[$key];
    }

    protected function _validator($arrInputOld)
    {
        $rules = $this->rules[Route::currentRouteName()];

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            $tempValidator = array();
            $laravelValidator = array();

            foreach ($validator->failed() as $failedField => $failedRules) {
                foreach ($failedRules as $failedRule => $failedValue) {
                    $mapRule = $this->_mappingLaravelToBootstrapRules($failedRule);
                    $tempValidator[] = array($failedField, 'INVALID', $mapRule);
                }
            }

            foreach ($arrInputOld as $inputOld) {
                foreach ($tempValidator as $value) {
                    if ($inputOld === $value[0]) {
                        $laravelValidator[] = $value;
                    } else {
                        $laravelValidator[] = array($inputOld, 'VALID');
                    }
                }
            }

            Session::flash('laravelValidatorJSON', json_encode($laravelValidator));
        }

        Input::flash();

        return $validator;
    }

    /*
    *  Change from key->value to id->key, name->value
    *----------------------------------------*/

    protected static function setArrayOption($the_array, $objectType = false)
    {
        $return = array();
        foreach ($the_array as $key => $element) {
            $item = array(
                'id'  => $key,
                'name'  => $element
                );

            if ($objectType) {
                $return[] = (object) $item;
            } else {
                $return[] = $item;
            }
        }

        return $return;
    }

    protected function mySelectPreprocessing($selectType, $formType, $dataType, $toPicks, $pickeds)
    {
        $results = array();

        if ($selectType === 'multi')
        {
            if ($dataType === 'key-value')
            {
                foreach ($toPicks as $toPick)
                {
                    if (empty($pickeds))
                    {
                        $results[] = (object) array(
                            'id'      => $toPick->id,
                            'name'    => $toPick->name,
                            $formType => ''
                        );
                    }
                    else
                    {
                        $found = FALSE;

                        foreach ($pickeds as $picked)
                        {
                            if ($picked == $toPick->id)
                            {
                                $results[] = (object) array(
                                    'id'      => $toPick->id,
                                    'name'    => $toPick->name,
                                    $formType => $formType
                                );
                                $found = TRUE;
                                break;
                            }
                        }

                        if (! $found)
                        {
                            $results[] = (object) array(
                                'id'      => $toPick->id,
                                'name'    => $toPick->name,
                                $formType => ''
                            );
                        }
                    }
                }
            }
            else if ($dataType === 'value')
            {
                foreach ($toPicks as $toPick)
                {
                    if (empty($pickeds))
                    {
                        $results[] = (object) array(
                            'name'    => $toPick,
                            $formType => ''
                        );
                    }
                    else
                    {
                        $found = FALSE;

                        foreach ($pickeds as $picked)
                        {
                            if ($picked == $toPick)
                            {
                                $results[] = (object) array(
                                    'name'    => $toPick,
                                    $formType => $formType
                                );
                                $found = TRUE;
                                break;
                            }
                        }

                        if (! $found)
                        {
                            $results[] = (object) array(
                                'name'    => $toPick,
                                $formType => ''
                            );
                        }
                    }
                }
            }
        }
        else if ($selectType === 'single')
        {
            $picked = $pickeds;

            if ($dataType === 'key-value')
            {
                foreach ($toPicks as $toPick)
                {
                    if ($picked == $toPick->id)
                    {
                        $results[] = (object) array(
                            'id'      => $toPick->id,
                            'name'    => $toPick->name,
                            $formType => $formType
                        );
                    }
                    else
                    {
                        $results[] = (object) array(
                            'id'      => $toPick->id,
                            'name'    => $toPick->name,
                            $formType => ''
                        );
                    }
                }
            }
            else if ($dataType === 'value')
            {
                foreach ($toPicks as $toPick)
                {
                    if ($picked == $toPick)
                    {
                        $results[] = (object) array(
                            'name'    => $toPick,
                            $formType => $formType
                        );
                    }
                    else
                    {
                        $results[] = (object) array(
                            'name'    => $toPick,
                            $formType => ''
                        );
                    }
                }
            }
        }

        return $results;

    }
}
