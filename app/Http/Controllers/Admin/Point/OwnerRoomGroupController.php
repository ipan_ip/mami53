<?php

namespace App\Http\Controllers\Admin\Point;

use App\Entities\Activity\Action;
use App\Entities\Activity\ActivityLog;
use App\Entities\Point\PointOwnerRoomGroup;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Validator;

class OwnerRoomGroupController extends Controller
{
    private const INDEX_ROUTE = 'admin.point.room-group.index';
    private const PER_PAGE = 20;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!Auth::user()->can('access-point-core')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        View::share('contentHeader', 'Point Room Group Management');
        View::share('user', Auth::user());
    }

    public function index(Request $request)
    {
        $roomGroup = PointOwnerRoomGroup::paginate(self::PER_PAGE);

        ActivityLog::LogIndex(Action::POINT_OWNER_ROOM_GROUP);

        $viewData = [
            'roomGroup' => $roomGroup,
            'boxTitle' => 'Manage Point Owner Room Group'
        ];
        return view('admin.contents.point.room-group.index', $viewData);
    }

    public function show(Request $request, PointOwnerRoomGroup $roomGroup)
    {
        abort(404);
    }

    public function create()
    {
        $viewData = [
            'boxTitle' => 'Create Owner Room Group'
        ];
        return view('admin.contents.point.room-group.edit', $viewData);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'floor' => 'required|numeric|min:0',
            'ceil' => 'required|numeric|min:0'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }
        if ($request->input('floor') > $request->input('ceil')) {
            return redirect()->back()->withInput()->with('errors', $validator->getMessageBag()->add('floor', 'Floor must be lower or equal than ceil.'));
        }

        $roomGroup = new PointOwnerRoomGroup();
        $roomGroup->floor = $request->input('floor');
        $roomGroup->ceil = $request->input('ceil');
        $roomGroup->save();

        ActivityLog::LogCreate(Action::POINT_OWNER_ROOM_GROUP, $roomGroup->id);

        return redirect()->route(self::INDEX_ROUTE, ['#point'])->with('message', 'Owner Room Group successfully added.');
    }

    public function edit(PointOwnerRoomGroup $roomGroup)
    {
        $viewData = [
            'roomGroup' => $roomGroup,
            'boxTitle' => 'Edit Owner Room Group'
        ];
        return view('admin.contents.point.room-group.edit', $viewData);
    }

    public function update(Request $request, PointOwnerRoomGroup $roomGroup)
    {
        $validator = Validator::make($request->all(), [
            'floor' => 'required|numeric|min:0',
            'ceil' => 'required|numeric|min:0'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }
        if ($request->input('floor') > $request->input('ceil')) {
            return redirect()->back()->withInput()->with('errors', $validator->getMessageBag()->add('floor', 'Floor must be lower or equal than ceil.'));
        }

        $roomGroup->floor = $request->input('floor');
        $roomGroup->ceil = $request->input('ceil');
        $roomGroup->save();

        ActivityLog::LogUpdate(Action::POINT_OWNER_ROOM_GROUP, $roomGroup->id);

        return redirect()->route(self::INDEX_ROUTE, ['#point'])->with('message', 'Owner Room Group successfully updated.');
    }

    public function destroy($id)
    {
        $roomGroup = PointOwnerRoomGroup::find($id);
        if (is_null($roomGroup)) {
            abort(404);
        }

        $roomGroup->delete();

        ActivityLog::LogDelete(Action::POINT_OWNER_ROOM_GROUP, $id);

        return redirect()->route(self::INDEX_ROUTE, ['#point'])->with('message', 'Owner Room Group successfully deleted.');
    }
}
