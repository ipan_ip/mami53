<?php

namespace App\Http\Controllers\Admin\Point;

use App\Entities\Activity\Action;
use App\Entities\Activity\ActivityLog;
use App\Entities\Point\Point;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Validator;

class ExpiryController extends Controller
{
    private const INDEX_ROUTE = 'admin.point.expiry.index';

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!Auth::user()->can('access-point-core')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        View::share('contentHeader', 'Expiry Point Management');
        View::share('user', Auth::user());
    }

    public function index()
    {
        $ownerExpiry = Point::firstOrCreate([
            'value' => Point::DEFAULT_VALUE, 
            'type' => Point::DEFAULT_TYPE,
            'target' => Point::TARGET_OWNER
        ])->expiry_value;

        $tenantExpiry = Point::firstOrCreate([
            'value' => Point::DEFAULT_VALUE, 
            'type' => Point::DEFAULT_TYPE_TENANT,
            'target' => Point::TARGET_TENANT
        ])->expiry_value;

        Point::firstOrCreate([
            'value' => Point::DEFAULT_VALUE,
            'type' => Point::MAMIROOMS_TYPE_TENANT,
            'target' => Point::TARGET_TENANT
        ]);

        ActivityLog::LogIndex(Action::POINT_EXPIRY);

        $viewData = [
            'owner_expiry' => $ownerExpiry,
            'tenant_expiry' => $tenantExpiry,
            'boxTitle' => 'Manage Point Expiry'
        ];
        return view('admin.contents.point.expiry.index', $viewData);
    }

    public function change(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'owner_expiry_value' => 'required|numeric|min:1',
            'tenant_expiry_value' => 'required|numeric|min:1'
        ], [], [
            'owner_expiry_value' => 'Owner Expiry Value',
            'tenant_expiry_value' => 'Tenant Expiry Value'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        $point = Point::updateOrCreate([
            'value' => Point::DEFAULT_VALUE,
            'type' => Point::DEFAULT_TYPE,
            'target' => Point::TARGET_OWNER
        ], [
            'expiry_value' => $request->input('owner_expiry_value')
        ]);

        // Sync the expiry_value to all owner segments
        Point::where('type', '<>', Point::DEFAULT_TYPE)
            ->where('target', Point::TARGET_OWNER)
            ->update([
                'expiry_value' => $request->input('owner_expiry_value')
            ]);

        ActivityLog::LogUpdate(Action::POINT, $point->id);

        $point = Point::updateOrCreate([
            'value' => Point::DEFAULT_VALUE,
            'type' => Point::DEFAULT_TYPE_TENANT,
            'target' => Point::TARGET_TENANT
        ], [
            'expiry_value' => $request->input('tenant_expiry_value')
        ]);

        // Sync the expiry_value to all tenant segments
        Point::where('type', '<>', Point::DEFAULT_TYPE_TENANT)
            ->where('target', Point::TARGET_TENANT)
            ->update([
                'expiry_value' => $request->input('tenant_expiry_value')
            ]);
        
        ActivityLog::LogUpdate(Action::POINT, $point->id);

        return redirect()->to(route(self::INDEX_ROUTE, []).'#point')->with('message', 'Point Expiry successfully updated.');
    }
}
