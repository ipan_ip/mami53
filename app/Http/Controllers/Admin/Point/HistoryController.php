<?php

namespace App\Http\Controllers\Admin\Point;

use App\Entities\Activity\Action;
use App\Entities\Activity\ActivityLog;
use App\Entities\Point\PointActivity;
use App\Entities\Point\PointHistory;
use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

class HistoryController extends Controller
{
    private const INDEX_ROUTE = 'admin.point.history.index';
    private const PER_PAGE = 20;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!$request->user()->can(['access-point', 'view-point'])) {
                return redirect('admin')->withErrors('You dont have permission to this page "'.request()->path().'".');
            }
            
            return $next($request);
        });

        View::share('contentHeader', 'Point History Management');
        View::share('user', Auth::user());
    }

    public function index(User $user, Request $request)
    {
        $query = PointHistory::with(['user', 'earnable', 'redeemable', 'redeem.reward', 'activity'])
            ->where('user_id', $user->id)
            ->where('value', '!=', 0);

        if ($request->filled('activity_id') && $request->input('activity_id') != 999999) {
            $query->where('activity_id', $request->input('activity_id'));
        }
        if ($request->filled('start_date')) {
            $startDate = Carbon::parse($request->input('start_date'))->startOfDay();
            $query->where('created_at', '>=', $startDate);
        }
        if ($request->filled('end_date')) {
            $endDate = Carbon::parse($request->input('end_date'))->endOfDay();
            $query->where('created_at', '<=', $endDate);
        }
        if (!$request->filled('sort_date') || !$request->input('sort_date')) {
            $query->latest();
        }

        $history = $query->paginate(self::PER_PAGE);

        $userType = 'tenant';
        if ($user->isOwner()) {
            $userType = 'owner';
        }
        $activityList = PointActivity::where('target', $userType)->get();

        ActivityLog::LogIndex(Action::POINT_HISTORY);

        $viewData = [
            'user' => $user,
            'history' => $history,
            'activityList' => $activityList,
            'boxTitle' => 'Manage Point History'
        ];
        return view('admin.contents.point.history.index', $viewData);
    }
}
