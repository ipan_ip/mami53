<?php

namespace App\Http\Controllers\Admin\Point;

use App\Entities\Activity\Action;
use App\Entities\Activity\ActivityLog;
use App\Entities\Level\KostLevel;
use App\Entities\Level\RoomLevel;
use App\Entities\Point\Point;
use App\Http\Controllers\Controller;
use App\Repositories\Point\SegmentRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Validator;

class SegmentController extends Controller
{
    private const INDEX_ROUTE = 'admin.point.segment.index';

    private $segmentRepository;

    public function __construct(SegmentRepository $segmentRepository)
    {
        $this->middleware(function ($request, $next) {
            if (!Auth::user()->can('access-point-core')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        $this->segmentRepository = $segmentRepository;

        View::share('contentHeader', 'Point Segment Management');
        View::share('user', Auth::user());
    }

    public function index(Request $request)
    {
        $limit = (int) $request->get('limit', SegmentRepository::PER_PAGE);

        $segments = $this->segmentRepository->getSegmentListPaginated($request->all(), $limit);

        $viewData = [
            'segments' => $segments,
            'boxTitle' => 'Manage Point Segment',
            'targetOptions' => Point::getTargetOptions(),
        ];

        ActivityLog::LogIndex(Action::POINT_SEGMENT);

        return view('admin.contents.point.segment.index', $viewData);
    }

    public function show(Request $request, Point $segment)
    {
        abort(404);
    }

    public function create()
    {
        $viewData = [
            'boxTitle' => 'Create Segment',
            'targetOptions' => Point::getTargetOptions(),
            'kostLevelList' => KostLevel::orderBy('order', 'ASC')->get()->keyBy('id'),
            'roomLevelList' => RoomLevel::orderBy('order', 'ASC')->get()->keyBy('id')
        ];
        return view('admin.contents.point.segment.edit', $viewData);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $validator = $this->makeRequestValidator($data);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        $exist = Point::where('type', $request->input('type'))->withTrashed()->first();
        if (!is_null($exist)) {
            return redirect()->back()->withInput()->with('errors', $validator->getMessageBag()->add('key', 'The segment has already been taken.'));
        }

        $segment = $this->segmentRepository->createSegment($data);

        ActivityLog::LogCreate(Action::POINT_SEGMENT, $segment->id);

        return redirect()->to(route(self::INDEX_ROUTE, []).'#point')->with('message', 'Segment successfully added.');
    }

    public function edit(Point $segment)
    {
        $viewData = [
            'segment' => $segment,
            'boxTitle' => 'Edit Segment',
            'targetOptions' => Point::getTargetOptions(),
            'kostLevelList' => KostLevel::orderBy('order', 'ASC')->get()->keyBy('id'),
            'roomLevelList' => RoomLevel::orderBy('order', 'ASC')->get()->keyBy('id')
        ];
        return view('admin.contents.point.segment.edit', $viewData);
    }

    public function update(Request $request, Point $segment)
    {
        $data = $request->all();
        $validator = $this->makeRequestValidator($data);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        $exist = Point::where('type', $request->input('type'))->withTrashed()->first();
        if (!is_null($exist) && $exist->id !== $segment->id) {
            return redirect()->back()->withInput()->with('errors', $validator->getMessageBag()->add('type', 'The segment has already been taken.'));
        }

        $segment = $this->segmentRepository->updateSegment($segment, $data);

        ActivityLog::LogUpdate(Action::POINT_SEGMENT, $segment->id);

        return redirect()->to(route(self::INDEX_ROUTE, []).'#point')->with('message', 'Segment successfully updated.');
    }

    public function destroy($id)
    {
        $segment = Point::find($id);
        if (is_null($segment)) {
            abort(404);
        }

        $segment->delete();

        ActivityLog::LogDelete(Action::POINT_SEGMENT, $id);

        return redirect()->to(route(self::INDEX_ROUTE, []).'#point')->with('message', 'Segment successfully deleted.');
    }

    private function makeRequestValidator(array $data)
    {
        return Validator::make($data, [
            'type' => 'required|max:25|alpha_dash',
            'title' => 'required|max:50',
            'target' => 'required|in:'. implode(",", array_keys(Point::getTargetOptions())),
            'kost_level_id' => 'required',
            'room_level_id' => 'required'
        ], [], [
            'type' => 'segment',
            'title' => 'title',
            'target' => 'target',
            'kost_level_id' => 'kost level',
            'room_level_id' => 'room level'
        ]);
    }
}