<?php

namespace App\Http\Controllers\Admin\Point;

use App\Entities\Level\KostLevel;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use App\Entities\Point\PointBlacklistConfig;

class BlacklistController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!Auth::user()->can('access-point-core')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        View::share('contentHeader', 'Owner Point Blacklist Management');
        View::share('user', Auth::user());
    }

    public function index()
    {
        $title = 'Manage Owner Point Blacklist';
        $model = PointBlacklistConfig::with(['level' => function ($q) {
            $q->withTrashed();
        }])
            ->latest('is_blacklisted')
            ->get();

        $viewData = [
            'blacklists' => $model,
            'pageTitle' => $title,
            'boxTitle' => $title,
        ];

        return view('admin.contents.point.blacklist.index', $viewData);
    }

    public function create()
    {
        $title = 'Add Point Blacklist';
        $usertypeDropdown = $this->getUsertypeDropdown();
        $viewData = [
            'usertypeDropdown' => $usertypeDropdown,
            'pageTitle' => $title,
            'boxTitle' => $title
        ];

        return view('admin.contents.point.blacklist.create', $viewData);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'value' => 'required|unique:point_blacklist_configs,value',
        ], [], [
            'value' => 'User Type'
        ]);

        PointBlacklistConfig::create($request->all());

        return redirect()->route('admin.point.blacklist.index', ['#point'])
            ->with('message', 'Blacklist Config successfully created');
    }

    public function toggle($id)
    {
        $blacklist = PointBlacklistConfig::findOrFail($id);
        $blacklist->is_blacklisted = !$blacklist->is_blacklisted;
        $blacklist->save();

        return redirect()->route('admin.point.blacklist.index', ['#point'])
            ->with('message', 'Blacklist Config successfully updated');
    }

    public function delete($id)
    {
        $blacklist = PointBlacklistConfig::findOrFail($id);
        $blacklist->delete();
        
        return redirect()->route('admin.point.blacklist.index', ['#point'])
            ->with('message', 'Blacklist Config successfully deleted.');
    }

    protected function getUsertypeDropdown()
    {
        $result = [];
        $result += PointBlacklistConfig::getUserTypeList();
        $result += KostLevel::orderBy('order', 'asc')->get()->pluck('name', 'id')->toArray();

        // remove existing
        if ($exists = PointBlacklistConfig::get()->pluck('value')->toArray()) {
            foreach($exists as $val) {
                unset($result[$val]);
            }
        }

        return $result;
    }
}
