<?php

namespace App\Http\Controllers\Admin\Point;

use App\Entities\Activity\Action;
use App\Entities\Activity\ActivityLog;
use App\Entities\Point\Point;
use App\Entities\Point\PointActivity;
use App\Entities\Point\PointOwnerRoomGroup;
use App\Entities\Point\PointSetting;
use App\Http\Controllers\Controller;
use App\Http\Requests\Point\PointSettingRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Validator;

class SettingController extends Controller
{
    private const INDEX_ROUTE = 'admin.point.setting.index';

    private $pointTarget;

    public function __construct(Request $request)
    {
        $this->middleware(function ($request, $next) {
            if (!Auth::user()->can('access-point')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        $this->pointTarget = $request->get('target', Point::TARGET_OWNER);
        if (!in_array($this->pointTarget, array_keys(Point::getTargetOptions()))) {
            $this->pointTarget = Point::TARGET_OWNER;
        }

        View::share('contentHeader', 'Point Setting Management');
        View::share('user', Auth::user());
    }

    public function index()
    {
        $pointSegments = Point::where('target', $this->pointTarget)->get();
        $limitOptions = PointSetting::limitOptions();
        $activity = PointActivity::where('is_active', 1)
            ->where('target', $this->pointTarget)
            ->orderBy('sequence', 'asc')
            ->get();
        $roomGroup = $this->pointTarget === Point::TARGET_OWNER ? PointOwnerRoomGroup::all() : null;
        $tnc = optional($pointSegments->where('value', Point::DEFAULT_VALUE)
            ->where('type', $this->getDefaultPointTypeByTarget())
            ->where('target', $this->pointTarget)
            ->first())->tnc;
        $setting = $this->prepareSettingData($activity);

        ActivityLog::LogIndex(Action::POINT_SETTING);

        $viewData = [
            'setting' => $setting,
            'pointSegments' => $pointSegments,
            'limitOptions' => $limitOptions,
            'activity' => $activity,
            'roomGroup' => $roomGroup,
            'tnc' => $tnc,
            'pointTarget' => $this->pointTarget,
            'boxTitle' => 'Manage '. ucfirst($this->pointTarget) .' Point Setting'
        ];
        return view('admin.contents.point.setting.index', $viewData);
    }

    private function prepareSettingData($activity): array
    {
        $setting = PointSetting::whereHas('point', function ($q) {
                $q->where('target', $this->pointTarget);
            })
            ->whereHas('activity', function ($q) { 
                $q->where('is_active', 1)
                    ->where('target', $this->pointTarget);
            })
            ->get()->toArray();
        $setting = $this->arrayGroupBy($setting, 'point_id');
        foreach ($setting as &$byPoint) {
            $byPoint = $this->arrayGroupBy($byPoint, 'activity_id', $activity->pluck('key', 'id')->toArray());
            foreach ($byPoint as &$byActivity) {
                $byActivity = $this->arrayGroupBy($byActivity, 'room_group_id');
            }
        }
        return $setting;
    }

    private function arrayGroupBy(array $array, string $key, $data = null): array
    {
        $return = [];
        foreach ($array as $item) {
            $arrayKey = $item[$key] ?? 0;
            if ($key === 'activity_id' && !isset($data[$arrayKey])) {
                continue;
            }
            if ($key === 'activity_id') {
                $arrayKey = $data[$arrayKey];
            }
            $return[$arrayKey][] = $item;
        }
        return $return;
    }

    private function getDefaultPointTypeByTarget()
    {
        $pointType = Point::DEFAULT_TYPE;
        if ($this->pointTarget === Point::TARGET_TENANT) {
            $pointType = Point::DEFAULT_TYPE_TENANT;
        }

        return $pointType;
    }

    public function tnc(Request $request)
    {
        $validator = Validator::make([
            'tnc' => trim(strip_tags($request->input('tnc')))
        ], [
            'tnc' => 'required'
        ], [], [
            'tnc' => 'Terms & Conditions'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        $point = Point::updateOrCreate([
            'value' => Point::DEFAULT_VALUE,
            'type' => $this->getDefaultPointTypeByTarget(),
            'target' => $this->pointTarget
        ], [
            'tnc' => $request->input('tnc')
        ]);

        // Sync the tnc to all segments
        Point::where('type', '<>', $this->getDefaultPointTypeByTarget())
            ->where('target', $this->pointTarget)
        ->update([
            'tnc' => $request->input('tnc')
        ]);

        ActivityLog::LogUpdate(Action::POINT, $point->id);

        return redirect()->to(route(self::INDEX_ROUTE, ['target' => $this->pointTarget]).'#point')->with('message', 'Terms & Conditions successfully updated.');
    }

    public function post($point, $activity, PointSettingRequest $request)
    {
        $activity = PointActivity::where('key', $activity)->first();
        $point = Point::firstOrCreate([
            'value' => Point::DEFAULT_VALUE,
            'type' => $point,
            'target' => $this->pointTarget
        ]);

        if ($request->isValidationFailed()) {
            return redirect()->back()->withInput()->with('errors', $request->failedValidator->errors());
        }

        foreach ($request->input($point->id) as $activities) {
            foreach ($activities as $roomGroupId => $input) {
                $setting = PointSetting::where('activity_id', $activity->id)
                    ->where('point_id', $point->id);
                if ($roomGroupId !== 0) {
                    $setting->where('room_group_id', $roomGroupId);
                }
                $setting = $setting->first();
                if (is_null($setting)) {
                    $setting = new PointSetting();
                }
                $setting->room_group_id = $roomGroupId ?: null;
                $setting->point_id = $point->id;
                $setting->activity_id = $activity->id;
                $setting->times_to_received = 1;
                $setting->received_each = $input['received_each'];
                $setting->limit_type = $input['limit_type'];
                $setting->limit = (
                    isset($input['limit_room']) ?
                    PointSetting::LIMIT_ROOM_COUNT :
                    ($input['limit_type'] === PointSetting::LIMIT_ONCE ? 1 : $input['limit'])
                );
                $setting->is_active = isset($input['is_active']);
                $setting->save();
    
                ActivityLog::LogUpdate(Action::POINT_SETTING, $setting->id);
            }
        }

        return redirect()->to(route(self::INDEX_ROUTE, ['target' => $this->pointTarget]).'#point')->with('message', 'Setting successfully updated.');
    }
}
