<?php

namespace App\Http\Controllers\Admin\Point;

use App\Entities\Point\PointUser;
use App\Entities\User\Notification;
use App\Events\EligibleEarnPoint;
use App\Http\Controllers\Controller;
use App\Notifications\EarnPointTenantNotification;
use App\Repositories\Point\PointRepository;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class UserPointController extends Controller
{
    private const PER_PAGE = 20;

    protected $repo;

    public function __construct(PointRepository $pointRepository)
    {
        $permissionError = 'You dont have permission to this page "'.request()->path().'".';
        $this->middleware(function ($request, $next) use ($permissionError) {
            if (!$request->user()->can(['access-point', 'view-point'])) {
                return redirect('admin')->withErrors($permissionError);
            }
            
            return $next($request);
        });

        $this->middleware(function ($request, $next) use ($permissionError) {
            if (!$request->user()->can(['access-point'])) {
                return redirect('admin')->withErrors($permissionError);
            }

            return $next($request);
        })->except(['index']);

        $this->repo = $pointRepository;

        View::share('contentHeader', 'Point User Management');
        View::share('user', Auth::user());
    }

    public function index(Request $request)
    {
        $pageTitle = 'Manage User Point';
        $userPoints = $this->repo->getUserPointPaginated(self::PER_PAGE, $request->all());
        $viewData = [
            'userPoints' => $userPoints,
            'boxTitle' => $pageTitle,
            'pageTitle' => $pageTitle,
        ];

        return view('admin.contents.point.user.index', $viewData);
    }

    public function updateAdjustPoint(PointUser $pointUser, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'amount' => 'required|numeric',
            'note' => 'required|max:20',
        ], [], [
            'amount' => 'Point Amount',
            'note' => 'Note'
        ]);

        $validator->after(function ($validator) use ($pointUser, $request) {
            if ($pointUser->total + $request->get('amount') < 0) {
                $validator->errors()->add('amount', 'Adjustment Amount cannot be less than User Total Point.');
            }
        });

        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        $isSuccess = $this->repo->adjustPoint(
            $pointUser,
            $request->get('amount'),
            $request->get('note')
        );

        if (! $isSuccess) {
            return redirect()->route('admin.point.user.index')
                ->with('error_message', 'Cannot adjust point for User "'. $pointUser->user->name .'" because is blacklisted.');
        }

        $isTopdown = str_contains($request->get('amount'), '-');
        $adjustmentType = 'admin_adjustment_topup';
        if ($isTopdown) {
            $adjustmentType = 'admin_adjustment_topdown';
        }

        $this->sendNotification($pointUser->user, $adjustmentType, $request->get('amount'));

        return redirect()->route('admin.point.user.index')
            ->with('message', 'Point User "'. $pointUser->user->name .'" successfully updated.');
    }

    public function updateBulkAdjustPoint(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'csv_bulk_adjust_point' => 'required|mimes:csv,txt'
        ], [], [
            'csv_bulk_adjust_point' => 'CSV Adjust Point File',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        $file = $request->file('csv_bulk_adjust_point');
        $this->extractCsvFile($file, 'bulk-adjust-point')
            ->each(function ($item) {
                $pointUser = (new PointUser)->firstOrCreate(['user_id' => $item['user_id']]);
                $this->repo->adjustPoint($pointUser, $item['point'], $item['note']);
            });

        return redirect()->route('admin.point.user.index')
            ->with('message', 'Bulk adjust point from file: "'. $file->getClientOriginalName() .'" successfully updated.');
    }

    public function updateBlacklist(PointUser $pointUser, Request $request)
    {
        $this->repo->toggleBlacklistPointUser($pointUser);

        return redirect()->route('admin.point.user.index')
            ->with('message', $pointUser->user->name .' successfully '. str_finish($request->get('action_type'), 'ed'));
    }

    public function updateBulkBlacklist(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'csv_bulk_blacklist' => 'required|mimes:csv,txt'
        ], [], [
            'csv_bulk_blacklist' => 'CSV Blacklist File',
        ]);
        
        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        $file = $request->file('csv_bulk_blacklist');
        $this->extractCsvFile($file, 'bulk-blacklist')
            ->each(function ($item) {
                $pointUser = (new PointUser)->firstOrCreate(['user_id' => $item['user_id']]);
                $this->repo->toggleBlacklistPointUser($pointUser, $item['blacklist']);
            });

        return redirect()->route('admin.point.user.index')
            ->with('message', 'Bulk Blacklist from file: "'. $file->getClientOriginalName() .'" successfully updated.');
    }

    /**
     * Extract given adjust point csv file
     * 
     * @param UploadedFile $file
     * @param string $type
     * @return Collection
     */
    private function extractCsvFile(UploadedFile $file, string $type = 'bulk-adjust-point'): Collection
    {
        if (!in_array($type, ['bulk-adjust-point', 'bulk-blacklist'])) {
            throw new \Exception("Error Processing Request {$type} value invalid. [Valid value: bulk-adjust-point|bulk-update]", 1);
        }

        $csv = fopen($file, 'r');
        $defaultNote = 'Bulk Adjust Point from file: '. $file->getClientOriginalName();
        $results = [];
        while (($data = fgetcsv($csv)) !== false) {
            if (!empty($data[0])) {
                if ($type == 'bulk-adjust-point') {
                    $results[] = [
                        'user_id' => trim(array_get($data, 0)),
                        'point' => trim(array_get($data, 1)),
                        'note' => trim(array_get($data, 2, $defaultNote))
                    ];
                } elseif ($type == 'bulk-blacklist') {
                    $results[] = [
                        'user_id' => trim(array_get($data, 0)),
                        'blacklist' => trim(array_get($data, 1)),
                    ];
                }
            }
        }
        fclose($csv);
        
        // remove first row, because its contains column information
        array_shift($results);

        $collection = collect($results);
        $csvUserIds = $collection->pluck('user_id')->toArray();
        $validUserIds = User::whereIn('id', $csvUserIds)
            ->get()
            ->pluck('id')
            ->toArray();

        $collection = $collection->filter(function ($item) use ($validUserIds) {
            return in_array(array_get($item, 'user_id'), $validUserIds);
        });

        return $collection;
    }

    private function sendNotification(User $user, string $adjustmentType, int $pointToAdd) 
    {
        if ($user->isOwner()) {
            // Send notification to owner
            event(
                new EligibleEarnPoint(
                    $user,
                    null,
                    [
                        'point' => $pointToAdd
                    ],
                    $adjustmentType
                )
            );
        } else {
            // Send notification to tenant
            if (in_array($adjustmentType, ['admin_adjustment_topup'])) {
                $pointNotif = new EarnPointTenantNotification(null, $pointToAdd, $adjustmentType);
                $user->notify($pointNotif);

                $details = 'Klik untuk cek poin yang sudah terkumpul.';
                Notification::NotificationStore([
                    'user_id' => $user->id,
                    'title' => $pointNotif->getTitle() . '. ' . $details,
                    'type' => 'mamipoin',
                    'designer_id' => null,
                    'identifier' => null,
                    'identifier_type' => null,
                    'url' => '/user/mamipoin/history',
                    'scheme' => $pointNotif->getScheme()
                ]);
            }
        }
    }
}
