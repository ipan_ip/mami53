<?php

namespace App\Http\Controllers\Admin\Point;

use App\Entities\Activity\Action;
use App\Entities\Activity\ActivityLog;
use App\Entities\Point\PointActivity;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Validator;

class ActivityController extends Controller
{
    private const INDEX_ROUTE = 'admin.point.activity.index';
    private const PER_PAGE = 20;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!Auth::user()->can('access-point-core')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        View::share('contentHeader', 'Point Activity Management');
        View::share('user', Auth::user());
    }

    public function index(Request $request)
    {
        $query = PointActivity::query();

        if ($request->filled('q')) {
            $query->where('key', 'like', '%' . $request->input('q') . '%');
        }

        if ($request->filled('t') && in_array($request->input('t'), ['owner', 'tenant'])) {
            $query->where('target', $request->input('t'));
        }

        if ($request->filled('s')) {
            $query->where('is_active', $request->input('s'));
        }
        
        $activity = $query->orderBy('target', 'asc')->orderBy('sequence', 'asc')->paginate(self::PER_PAGE);

        ActivityLog::LogIndex(Action::POINT_ACTIVITY);

        $viewData = [
            'activity' => $activity,
            'boxTitle' => 'Manage Point Activity'
        ];
        return view('admin.contents.point.activity.index', $viewData);
    }

    public function show(Request $request, PointActivity $activity)
    {
        abort(404);
    }

    public function create()
    {
        $ownerReceivedPaymentGeneralAvailable = PointActivity::whereOwnerReceivedPaymentGeneralActivityEnabled()->exists();
        $nonGeneralOwnerReceivedPaymentActivities = PointActivity::getNonGeneralOwnerReceivedPaymentActivities();

        $viewData = [
            'target' => ['owner', 'tenant'],
            'boxTitle' => 'Create Activity',
            'ownerReceivedPaymentGeneralAvailable' => $ownerReceivedPaymentGeneralAvailable,
            'nonGeneralOwnerReceivedPaymentActivities' => $nonGeneralOwnerReceivedPaymentActivities
        ];
        return view('admin.contents.point.activity.edit', $viewData);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'key' => 'required|max:100|alpha_dash',
            'name' => 'required|max:100',
            'target' => 'required|in:owner,tenant',
            'title' => 'max:100',
            'is_active' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        $exist = PointActivity::where('key', $request->input('key'))->withTrashed()->first();
        if (!is_null($exist)) {
            return redirect()->back()->withInput()->with('errors', $validator->getMessageBag()->add('key', 'Key already exist.'));
        }

        $activity = new PointActivity();
        $activity->fill($request->all());
        $activity->save();

        $this->updateNonGenerateOwnerReceivedPaymentActivities($activity);

        ActivityLog::LogCreate(Action::POINT_ACTIVITY, $activity->id);

        return redirect()->route(self::INDEX_ROUTE, ['#point'])->with('message', 'Activity successfully added.');
    }

    public function edit(PointActivity $activity)
    {
        $ownerReceivedPaymentGeneralAvailable = PointActivity::whereOwnerReceivedPaymentGeneralActivityEnabled()->exists();
        $nonGeneralOwnerReceivedPaymentActivities = PointActivity::getNonGeneralOwnerReceivedPaymentActivities();

        $viewData = [
            'activity' => $activity,
            'target' => ['owner', 'tenant'],
            'boxTitle' => 'Edit Activity',
            'ownerReceivedPaymentGeneralAvailable' => $ownerReceivedPaymentGeneralAvailable,
            'nonGeneralOwnerReceivedPaymentActivities' => $nonGeneralOwnerReceivedPaymentActivities
        ];
        return view('admin.contents.point.activity.edit', $viewData);
    }

    public function update(Request $request, PointActivity $activity)
    {
        $validator = Validator::make($request->all(), [
            'key' => 'required|max:100|alpha_dash',
            'name' => 'required|max:100',
            'target' => 'required|in:owner,tenant',
            'title' => 'max:100',
            'is_active' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        $exist = PointActivity::where('key', $request->input('key'))->withTrashed()->first();
        if (!is_null($exist) && $exist->id !== $activity->id) {
            return redirect()->back()->withInput()->with('errors', $validator->getMessageBag()->add('key', 'Key already exist.'));
        }

        $activity->fill($request->all());
        $activity->save();

        $this->updateNonGenerateOwnerReceivedPaymentActivities($activity);

        ActivityLog::LogUpdate(Action::POINT_ACTIVITY, $activity->id);

        return redirect()->route(self::INDEX_ROUTE, ['#point'])->with('message', 'Activity successfully updated.');
    }

    public function destroy($id)
    {
        $activity = PointActivity::find($id);
        if (is_null($activity)) {
            abort(404);
        }

        $activity->delete();

        ActivityLog::LogDelete(Action::POINT_ACTIVITY, $id);

        return redirect()->route(self::INDEX_ROUTE, ['#point'])->with('message', 'Activity successfully deleted.');
    }

    private function updateNonGenerateOwnerReceivedPaymentActivities(PointActivity $activity)
    {
        if ($activity->key != PointActivity::OWNER_RECEIVED_PAYMENT_GENERAL_ACTIVITY) {
            return;
        }

        PointActivity::whereIn('key', PointActivity::getNonGeneralOwnerReceivedPaymentActivities())
            ->update([
                'is_active' => !$activity->is_active
            ]);
    }
}
