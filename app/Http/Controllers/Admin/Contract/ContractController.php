<?php

namespace App\Http\Controllers\Admin\Contract;

use App\Entities\Mamipay\MamipayContract;
use App\Http\Controllers\Controller;
use App\Http\Helpers\ApiResponse;
use App\Presenters\BookingUserPresenter;
use App\Presenters\Contract\ContractPresenter;
use App\Repositories\Booking\BookingUserRepository;
use App\Repositories\Contract\ContractRepository;
use App\Repositories\Level\KostLevelRepository;
use App\Repositories\Level\RoomLevelRepository;
use App\Repositories\Room\RoomUnitRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;

class ContractController extends Controller
{
    protected $repository;

    public function __construct(ContractRepository $contractRepository)
    {
        $this->middleware(function ($request, $next) {
            if (!Auth::user()->can('access-data-booking')) {
                return redirect('admin');
            }
            return $next($request);
        });

        $this->repository = $contractRepository;

        View::share('contentHeader', 'Contract Management');
        View::share('user', Auth::user());
    }

    public function index(Request $request)
    {
        // get contract list data
        $filter = $this->mappingFilterData($request);
        $contracts = $this->repository->getContractListAdmin(
            $filter['searchBy'],
            $filter['searchValue'],
            $filter['kostLevel'],
            $filter['roomLevel'],
            $filter['funnel'],
            20
        );
        $contractsTransform = (new ContractPresenter('admin-list'))->present($contracts);

        $data = [];
        $data['contracts'] = $contractsTransform;
        $data['contractRaws'] = $contracts;
        $data['filters'] = $this->prepareFilterData();

        return view('admin.contents.contract.index', $data);
    }

    public function showInvoices($contractId): JsonResponse
    {
        $response = ['status' => false, 'meta' => ['message' => 'General Error']];
        try {
            $invoinces = $this->repository->getContractInvoices($contractId);
            $response = [
                'data' => $invoinces
            ];
        } catch (\Exception $e) {
            $response['meta']['message'] = $e->getMessage();
        }
        return ApiResponse::responseData($response);
    }

    public function listRoomAllotment($contractId): JsonResponse
    {
        $response = ['status' => false, 'meta' => ['message' => 'General Error']];
        try {
            $bookingUserRepository = app()->make(BookingUserRepository::class);

            $contract   = $this->repository->find($contractId);
            $typeKost   = optional($contract)->type_kost;
            $room       = optional($typeKost)->room;

            if ($room === null)
                throw new \Exception('Kost tidak ditemukan');

            $roomUnit = $room ? $bookingUserRepository->getRoomUnitList($room) : null;
            $tranformRoomUnit = (new BookingUserPresenter('room-unit-list'))->present($roomUnit);
            $sortingRoomUnit = $this->sortingRoomLevel($tranformRoomUnit);
            if ($sortingRoomUnit !== null) {
                $tranformRoomUnit = $sortingRoomUnit;
            }
            $response = $tranformRoomUnit;

        } catch (ModelNotFoundException $e) {
            $response['meta']['message'] = 'Data kontrak tidak ditemukan';
        } catch (\Exception $e) {
            $response['meta']['message'] = $e->getMessage();
        }
        return ApiResponse::responseData($response);
    }

    public function storeRoomAllotment(Request $request, $contractId): JsonResponse
    {
        $response = ['status' => false, 'meta' => ['message' => 'General Error']];
        try {
            DB::beginTransaction();

            $roomUnitRepo = app()->make(RoomUnitRepository::class);

            $contract   = $this->repository->find($contractId);
            $typeKost   = optional($contract)->type_kost;
            $room       = optional($typeKost)->room;

            if ($room === null)
                throw new \Exception('Kost tidak ditemukan');

            if (in_array($contract->status, [MamipayContract::STATUS_TERMINATED]))
                throw new \Exception('Kontrak tidak bisa dirubah');

            $designerRoomId = $request->designer_room_id ?? null;
            $currentDesignerRoomId = $typeKost->designer_room_id;

            $roomUnit = $roomUnitRepo->find($designerRoomId);

            $typeKost->room_number      = $roomUnit->name;
            $typeKost->designer_room_id = $roomUnit->id;
            $typeKost->save();

            // handle room allotment clear and update new room
            if ($designerRoomId !== null && $designerRoomId !== $currentDesignerRoomId) {
                $roomUnitRepo->increaseRoomAvailable($room, $currentDesignerRoomId);
                $roomUnitRepo->decreaseRoomAvailable($room, $designerRoomId);
            }
            $response = [
                'message' => 'Nomor kamar berhasil dirubah'
            ];
            DB::commit();
        } catch (ModelNotFoundException $e) {
            DB::rollBack();
            $response['meta']['message'] = 'Data kontrak tidak ditemukan';
        } catch (\Exception $e) {
            DB::rollBack();
            $response['meta']['message'] = $e->getMessage();
        }
        return ApiResponse::responseData($response);
    }

    /**
     * Get data to show filter on listing
     * @return array
     */
    private function prepareFilterData(): array
    {
        // repository
        $kostLevelRepository = app()->make(KostLevelRepository::class);
        $roomLevelRepository = app()->make(RoomLevelRepository::class);

        $data = [];
        $data['kostLevels'] = $kostLevelRepository->getAll();
        $data['roomLevels'] = $roomLevelRepository->all();
        $data['searchBy'] = [
            'room_name' => 'Kos Name',
            'owner_phone' => 'Owner Phone',
            'tenant_name' => 'Penyewa Name',
            'tenant_phone' => 'Penyewa Phone Number'
        ];
        $data['funnels'] = [
            'booking' => 'BOOKING',
            'dbet_consultant' => 'DBET CONSULTANT',
            'dbet_owner' => 'DBET OWNER'
        ];
        return $data;
    }

    /**
     * Mapping data filter with default value
     * @param Request $request
     * @return array
     */
    private function mappingFilterData(Request $request): array
    {
        $data = [];
        $data['searchBy']       = !empty($request->search_by) ? $request->search_by : null;
        $data['searchValue']    = !empty($request->search_value) ? $request->search_value : null;
        $data['kostLevel']      = !empty($request->kost_level) ? $request->kost_level : null;
        $data['roomLevel']      = !empty($request->room_level) ? $request->room_level : null;
        $data['funnel']         = !empty($request->funnel) ? $request->funnel : null;

        return $data;
    }

    /**
     * Mapping data sorting room level with higher priority goldplus
     * @param $roomUnits
     * @return array|null
     */
    private function sortingRoomLevel($roomUnits): ?array
    {
        try {
            if (isset($roomUnits['data'])) {
                $collect = collect($roomUnits['data']);
                $gpAvailable = $collect
                    ->where('available', true)
                    ->where('is_gold_plus', true)
                    ->sortBy('level_name');

                $nonGpAvailable = $collect
                    ->where('available', true)
                    ->where('is_gold_plus', false);

                $gpDisable = $collect
                    ->where('disable', true)
                    ->where('is_gold_plus', true)
                    ->sortBy('level_name');

                $nonGpDisable = $collect
                    ->where('disable', true)
                    ->where('is_gold_plus', false);

                $return = collect()->merge($gpAvailable)
                    ->merge($nonGpAvailable)
                    ->merge($gpDisable)
                    ->merge($nonGpDisable);

                if ($return->count() > 0) {
                    return [
                        'data' => $return->toArray()
                    ];
                }

                return null;
            }

            return null;

        } catch (\Exception $e) {
            return null;
        }
    }
}