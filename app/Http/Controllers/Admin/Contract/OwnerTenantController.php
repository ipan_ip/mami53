<?php

namespace App\Http\Controllers\Admin\Contract;

use App\Http\Controllers\Controller;
use App\Http\Requests\Owner\Tenant\OwnerTenantLinkRequest;
use App\Presenters\Owner\Tenant\OwnerTenantPresenter;
use App\Repositories\Dbet\DbetLinkRepository;
use App\Services\Booking\BookingService;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Validator;

class OwnerTenantController extends Controller
{
    protected $repository;

    public function __construct(DbetLinkRepository $repository)
    {
        $this->middleware(function ($request, $next) {
            if (!Auth::user()->can('access-data-booking')) {
                return redirect('admin');
            }
            return $next($request);
        });

        $this->repository = $repository;

        View::share('contentHeader', 'DBET Link Management');
        View::share('user', Auth::user());
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     * @throws \Exception
     */
    public function index(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'search_value' => 'string',
            ]);

            if ($validator->fails()) {
                return redirect()->back()->withInput()->with('errors', $validator->errors());
            }

            // get data from params
            $filter = $request->filter ?? null;
            $searchValue = $request->search_value ?? null;

            // get data
            $dataLink = $this->repository->getListAdmin($filter, $searchValue, 20);

            // transform data for response
            $transform = (new OwnerTenantPresenter('admin-index'))->present($dataLink);

            $data = [];
            $data['filters'] = [
                'room_name'     => 'Kos Name',
                'owner_name'    => 'Owner Name',
                'owner_phone'   => 'Owner Phone',
                'code'          => 'Code Link'
            ];
            $data['links']      = $transform;
            $data['linkRaw']    = $dataLink;
            return view('admin.contents.contract.dbet-link.index', $data);
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        $data = [];
        return view('admin.contents.contract.dbet-link.form', $data);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function edit(Request $request, $id)
    {
        try {
            // get data dbet link
            $dbetLink = $this->repository->with(['room.owners.user'])->find($id);

            $data = [];
            $data['data'] = $dbetLink;
            return view('admin.contents.contract.dbet-link.form', $data);

        } catch (ModelNotFoundException $e) {
            return redirect()->back()->withInput()->with('errors', $e->getMessage());
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
            return redirect()->back()->withInput()->with('errors', $e->getMessage());
        }
    }

    /**
     * @param OwnerTenantLinkRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(OwnerTenantLinkRequest $request)
    {
        try {
            // prepare
            $service = app()->make(BookingService::class);

            // validate data
            $validator = $request->failedValidator;
            if ($validator && $validator->fails()) {
                return redirect()->back()->withInput()->with('errors', $validator->errors());
            }

            // get data
            $room = $request->getRoom();

            // checking unique code is null
            if (optional($room)->unique_code === null) {
                $room->generateUniqueCode();
                $room = $room->fresh();
            }

            $owner      = optional($room)->verified_owner;
            $ownerUser  = optional($owner)->user;
            $allowedOwner = $service->ownerEnableDbetLink($ownerUser) ?? false;

            DB::beginTransaction();

            $data = $this->repository->findByDesignerId($room->id);
            $dueDate = $request->due_date ?? 0;
            if ($request->is_required_due_date == 0) {
                $dueDate = 0;
            }

            if ($ownerUser == null)
                return redirect()->route('admin.dbet-link.index')->with('error_message', 'Owner data tidak ditemukan');

            if ($allowedOwner === false)
                return redirect()->route('admin.dbet-link.index')->with('error_message', 'Owner tidak diijinkan menggunakan fitur ini');

            if ($data !== null) {
                return redirect()->route('admin.dbet-link.index')->with('error_message', 'Owner telah memiliki link');
            } else {
                $this->repository->create([
                    'designer_id' => $room->id,
                    'code' => $room->unique_code->code,
                    'is_required_identity' => $request->is_required_identity,
                    'is_required_due_date' => $request->is_required_due_date,
                    'due_date' => $dueDate
                ]);
            }
            DB::commit();

            return redirect()->route('admin.dbet-link.index')->with('message', 'Data berhasil di buat');
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error_message', $e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        try {
            $validator = Validator::make($request->all(), [
                'is_required_identity' => 'required|in:0,1',
                'is_required_due_date' => 'required|in:0,1',
                'due_date' => 'required|integer'
            ]);

            if ($validator->fails()) {
                return redirect()->back()->withInput()->with('errors', $validator->errors());
            }

            // get data dbet link
            $dbetLink = $this->repository->find($id);

            // due date data
            $dueDate = $request->due_date ?? 0;
            if ($request->is_required_due_date == 0) {
                $dueDate = 0;
            }

            // updated data
            $this->repository->update([
                'is_required_identity' => $request->is_required_identity,
                'is_required_due_date' => $request->is_required_due_date,
                'due_date' => $dueDate
            ], $dbetLink->id);

            return redirect()->route('admin.dbet-link.index')->with('message', 'Data berhasil di update');

        } catch (ModelNotFoundException $e) {
            return redirect()->back()->withInput()->with('errors', $e->getMessage());
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
            return redirect()->back()->withInput()->with('errors', $e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request, $id)
    {
        try {
            // get data dbet link
            $dbetLink = $this->repository->find($id);

            // deleted data
            $this->repository->delete($dbetLink->id);

            return redirect()->route('admin.dbet-link.index')->with('message', 'Data berhasil di hapus');

        } catch (ModelNotFoundException $e) {
            return redirect()->back()->withInput()->with('errors', $e->getMessage());
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
            return redirect()->back()->withInput()->with('errors', $e->getMessage());
        }
    }
}