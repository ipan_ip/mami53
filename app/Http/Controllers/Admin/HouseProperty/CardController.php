<?php

namespace App\Http\Controllers\Admin\HouseProperty;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\BaseController;
use App\Entities\HouseProperty\HousePropertyMedia;
use App\Entities\HouseProperty\HouseProperty;
use Auth;
use DB;

class CardController extends BaseController
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if(!Auth::user()->can('access-kost')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        \View::share('contentHeader', 'Stories Management');
        \View::share('user' , Auth::user());
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect()->back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $media = HousePropertyMedia::with(['house_property'])->where('house_property_id', $id)->get();
        $viewData = array(
            'boxTitle' => 'List Images',
            'media' => $media,
            'propertyId' => $id
        );
        return view('admin.contents.house_property.card.list', $viewData);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $house = HouseProperty::find($id);
        $media = HousePropertyMedia::categorizedMedia($house);
        $viewData = array(
            'boxTitle' => 'List Images',
            'media' => $media,
            'propertyId' => $id,
            'propertyName' => $house->name,
            'back' => '/admin/house_property/card/'.$id.'?type=house_property',
        );
        
        return view('admin.contents.house_property.card.upload', $viewData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $media = HousePropertyMedia::where('id', $id)->first();
        if (is_null($media)) {
            return redirect()->back()->with(["message", "Media tidak ditemukan"]);
        }
        $media->delete();
        return redirect()->back()->with(["message", "Berhasil hapus gambar"]);
    }

    public function setCover(Request $request, $propertyId, $cardId)
    {
        DB::beginTransaction();
        $media = HousePropertyMedia::where('id', $cardId)->firstOrFail();
        $oldMedia = HousePropertyMedia::where('description', 'LIKE', '%cover%')->where('house_property_id', $propertyId)->first();
        
        if (!is_null($oldMedia)) {
            $description = str_replace("-cover", "", $oldMedia->description);
            $oldMedia->description = $description;
            $oldMedia->save();
        }
        
        $media->description .= "-cover";
        $media->save();

        $property = HouseProperty::where('id', $propertyId)->firstOrFail();
        $property->cover_id = $media->id;
        $property->save();
        DB::commit();
        return redirect()->back()->with(["message", "Berhasil update cover"]);
    }
}
