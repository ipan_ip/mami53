<?php

namespace App\Http\Controllers\Admin\HouseProperty;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Entities\HouseProperty\HouseProperty;
use App\Entities\HouseProperty\HousePropertyTag;
use App\Entities\Room\Element\Tag;
use App\Libraries\AreaFormatter;
use Validator;
use Cviebrock\EloquentSluggable\Services\SlugService;
use App\Entities\HouseProperty\HousePropertySlug;
use App\Entities\HouseProperty\HousePropertyReject;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;
use App\Libraries\SMSLibrary;
use DB;
use Illuminate\Support\Facades\Input;
use Config;

class HousePropertyController extends Controller
{

    protected $parking = ["mobil,motor", "motor", "mobil"];
    protected $validatorMessages = [];

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if(!Auth::user()->can('access-kost')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        \View::share('contentHeader', 'Stories Management');
        \View::share('user' , Auth::user());
    }

    public function index(Request $request)
    {
        $query = HouseProperty::query();

        $query = $this->filter($request, $query);

        $housePropertyList = $query->orderBy('id', 'desc')->paginate(20);
        
        $viewData = array(
            'boxTitle' => 'List Kost',
            'villa' => $housePropertyList,
            'status' => ["All", "Active", "Not active"],
            'type' => ["Villa & Kontrakan", "Villa", "Kontrakan"],
        );

        ActivityLog::LogIndex(Action::HOUSE_PROPERTY);

        return view('admin.contents.house_property.index', $viewData);
    }

    public function filter(Request $request, $query)
    {
        if ($request->filled('name')) {
            $query->where(function($p) use($request) {
                $p->where('name', 'like', '%'.$request->input('name').'%')
                    ->orWhere('song_id', $request->input('name'))
                    ->orWhere('id', $request->input('name'));
            });
        }

        if ($request->filled('phone')) {
            $query->where(function($p) use($request) {
                $p->where('owner_phone', $request->input('phone'))
                    ->orWhere('manager_phone', $request->input('phone'));
            });
        }

        if ($request->filled('agent_name')) {
            $query->where('agent_name', $request->input('agent_name'));
        }

        if ($request->filled('status')) {
            $status = $request->input('status');
            if ($status > 0) $query->where('is_active', $status == 1 ? 1 : 0);
        }

        if ($request->filled('type') and $request->input('type') > 0) {
            $type = $request->input('type') == 1 ? "villa" : "rented_house";
            $query->where('type', $type);
        }

        return $query;
    }

    public function create(Request $request)
    {
        $houseProperty = new HouseProperty();
        $houseProperty->name = Input::old('name');
        $houseProperty->size = Input::old('size');
        $houseProperty->price_daily = Input::old('price_daily');
        $houseProperty->price_weekly = Input::old('price_weekly');
        $houseProperty->price_monthly = Input::old('price_monthly');
        $houseProperty->price_yearly = Input::old('price_yearly');
        $houseProperty->floor = Input::old('floor');
        $houseProperty->building_year = Input::old('building_year');
        $houseProperty->is_available = Input::old('is_available');
        $houseProperty->address = Input::old('address');
        $houseProperty->latitude = -7.7735457;//Input::old('latitude');
        $houseProperty->longitude = 110.3874203;//Input::old('longitude');
        $houseProperty->area_city = Input::old('area_city');
        $houseProperty->area_subdistrict = Input::old('area_subdistrict');
        $houseProperty->bed_room = Input::old('bed_room');
        $houseProperty->bed_total = Input::old('bed_total');
        $houseProperty->guest_max = Input::old('guest_max');
        $houseProperty->bath_total = Input::old('bath_total');
        $houseProperty->parking = Input::old('parking');
        $houseProperty->other_cost_information = Input::old('other_cost_information');
        $houseProperty->other_information = Input::old('other_information');
        $houseProperty->description = Input::old('description');

        $tag = Tag::where('type', '<>', 'fac_project')->get();

        $viewData = array(
            'boxTitle' => 'Add new property',
            'data' => $houseProperty,
            'concern_ids' => [],
            'tag' => $tag,
            'parking' => $this->parking,
            'action' => 'admin/house_property',
            'is_update' => false,
        );
        
        return view('admin.contents.house_property.edit', $viewData);

    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:10',
            'size' => 'nullable',
            'price_daily' => 'nullable|numeric',
            'price_weekly' => 'nullable|numeric',
            'price_monthly' => 'nullable|numeric',
            'price_yearly' => 'nullable|numeric',
            'address' => 'required|max:200',
            'latitude' => 'required',
            'longitude' => 'required',
            'area_city' => 'required',
            'area_subdistrict' => 'required',
            'bed_room' => 'numeric',
            'bed_total' => 'numeric',
            'guest_max' => 'numeric',
            'bath_total' => 'numeric',
            'parking' => 'required',
            'other_cost_information' => 'nullable'
        ], $this->validatorMessages);

        $validator->after(function($validator) use ($request) {
            if($request->name != '') {
                $roomExist = HouseProperty::where('name', $request->name)->first();
                if($roomExist) {
                    $validator->errors()->add('name', 'Nama sudah pernah digunakan sebelumnya');
                }
            }
        });

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $request = $request->all();
        $houseProperty = new HouseProperty();
        $houseProperty->name = $request['name'];
        $houseProperty->type = $request['type'];
        $houseProperty->address = $request['address'];
        $houseProperty->latitude = $request['latitude'];
        $houseProperty->longitude = $request['longitude'];
        if (strlen($request['area_city']) > 0) {
            $houseProperty->area_city = AreaFormatter::format($request['area_city']);
            $houseProperty->area_big = $request['area_city'];
        }
        $houseProperty->area_subdistrict = $request['area_subdistrict'];
        $houseProperty->price_daily = $request['price_daily'];
        $houseProperty->price_weekly = $request['price_weekly'];
        $houseProperty->price_monthly = $request['price_monthly'];
        $houseProperty->price_yearly = $request['price_yearly'];
        $houseProperty->bed_room = $request['bed_room'];
        $houseProperty->size = $request['size'];
        $houseProperty->guest_max = $request['guest_max'];
        $houseProperty->bed_total = $request['bed_total'];
        $houseProperty->bath_total = $request['bath_total'];
        $houseProperty->parking = $request['parking'];
        $houseProperty->other_cost_information = $request['other_cost_information'];
        $houseProperty->other_information = $request['other_information'];
        $houseProperty->description = $request['description'];
        $houseProperty->owner_name = $request['owner_name'];
        $houseProperty->owner_phone = $request['owner_phone'];
        $houseProperty->manager_name = $request['manager_name'];
        $houseProperty->manager_phone = $request['manager_phone'];
        $houseProperty->agent_name = $request['agent_name'];
        $houseProperty->live_at = date('Y-m-d H:i:s');
        $houseProperty->floor = $request['floor'];
        $houseProperty->building_year = isset($request['building_year']) ? $request['building_year'] : Null;
        $houseProperty->is_available = $request['is_available'];
        $houseProperty->song_id = HouseProperty::generateSongId();
        $houseProperty->save();

        if (isset($request['concern_ids'])) {
            if(count($request['concern_ids']) > 0) {
                $houseProperty->house_property_tags()->sync($request['concern_ids']);
            }
        }

        ActivityLog::LogCreate(Action::HOUSE_PROPERTY, $houseProperty->id);

        return redirect('/admin/house_property')->with('message', 'success');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $houseProperty = HouseProperty::findOrFail($id);
        $concern_ids  = HousePropertyTag::where('house_property_id', $houseProperty->id)->get()->pluck('tag_id')->toArray();
        $tag = Tag::where('type', '<>', 'fac_project')->get();

        $viewData = array(
            'boxTitle' => 'Edit Property',
            'data' => $houseProperty,
            'concern_ids' => $concern_ids,
            'tag' => $tag,
            'parking' => $this->parking,
            'action' => 'admin/house_property/'. $houseProperty->id,
            'is_update' => true,
        );
        
        return view('admin.contents.house_property.edit', $viewData);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:10',
            'size' => 'nullable',
            'price_daily' => 'nullable|numeric',
            'price_weekly' => 'nullable|numeric',
            'price_monthly' => 'nullable|numeric',
            'price_yearly' => 'nullable|numeric',
            'address' => 'required|max:200',
            'latitude' => 'required',
            'longitude' => 'required',
            'area_city' => 'required',
            'area_subdistrict' => 'required',
            'bed_room' => 'numeric',
            'bed_total' => 'numeric',
            'guest_max' => 'numeric',
            'bath_total' => 'numeric',
            'parking' => 'required',
            'other_cost_information' => 'nullable'
        ], $this->validatorMessages);

        $validator->after(function($validator) use ($request, $id) {
            if($request->name != '') {
                $roomExist = HouseProperty::where('name', $request->name)->where('id', '<>', $id)->first();
                if($roomExist) {
                    $validator->errors()->add('name', 'Nama sudah pernah digunakan sebelumnya');
                }
            }
        });

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $request = $request->all();
        $houseProperty = HouseProperty::findOrFail($id);
        $oldSlug = $houseProperty->slug;
        $houseProperty->name = $request['name'];
        $houseProperty->type = $request['type'];
        $houseProperty->address = $request['address'];
        $houseProperty->latitude = $request['latitude'];
        $houseProperty->longitude = $request['longitude'];
        if (strlen($request['area_city']) > 0) {
            $houseProperty->area_city = AreaFormatter::format($request['area_city']);
            $houseProperty->area_big = $request['area_city'];
        }
        $houseProperty->area_subdistrict = $request['area_subdistrict'];
        $houseProperty->price_daily = $request['price_daily'];
        $houseProperty->price_weekly = $request['price_weekly'];
        $houseProperty->price_monthly = $request['price_monthly'];
        $houseProperty->price_yearly = $request['price_yearly'];
        $houseProperty->bed_room = $request['bed_room'];
        $houseProperty->size = $request['size'];
        $houseProperty->guest_max = $request['guest_max'];
        $houseProperty->bed_total = $request['bed_total'];
        $houseProperty->bath_total = $request['bath_total'];
        $houseProperty->parking = $request['parking'];
        $houseProperty->other_cost_information = $request['other_cost_information'];
        $houseProperty->other_information = $request['other_information'];
        $houseProperty->description = $request['description'];
        $houseProperty->owner_name = $request['owner_name'];
        $houseProperty->owner_phone = $request['owner_phone'];
        $houseProperty->manager_name = $request['manager_name'];
        $houseProperty->manager_phone = $request['manager_phone'];
        $houseProperty->floor = $request['floor'];
        $houseProperty->building_year = isset($request['building_year']) ? $request['building_year'] : Null;
        $houseProperty->is_available = $request['is_available'];
        $houseProperty->save();

        

        if ($houseProperty->name_slug != $oldSlug) {
            $newSlug = SlugService::createSlug(HouseProperty::class, 'slug', $houseProperty->name_slug);
            $houseProperty->slug = $newSlug;
            HousePropertySlug::store($oldSlug, $houseProperty);
            $houseProperty->save();
        }

        if (isset($request['concern_ids'])) {
            if (count($request['concern_ids']) > 0) {
                foreach ($request['concern_ids'] as $tag) {
                    $housePropertyTag = HousePropertyTag::where('house_property_id', $id)->where('tag_id', $tag)->first();
                    if (is_null($housePropertyTag)) {
                        $houseTag = new HousePropertyTag();
                        $houseTag->house_property_id = $id;
                        $houseTag->tag_id = $tag;
                        $houseTag->save();
                    }
                }
            } else {
                HousePropertyTag::where('house_property_id', $id)->delete();
            }
        }
        ActivityLog::LogUpdate(Action::HOUSE_PROPERTY, $id);
        return redirect('/admin/house_property')->with('message', 'Sukses update data');
    }

    public function destroy($id)
    {
        //
    }

    public function rejectPage(Request $request, $id) 
    {
        $houseProperty = HouseProperty::with('house_property_reject')->where('id', $id)->first();
        
        if (is_null($houseProperty)) {
            return redirect()->back()->with(['message', "Data tidak ditemukan"]);
        }

        $viewData = array(
            'boxTitle' => 'Reject reason',
            'reject_reason' => $houseProperty->house_property_reject
        );

        return view('admin.contents.house_property.reject', $viewData);
    }

    public function rejectPost(Request $request, $id)
    {
        $houseProperty = HouseProperty::with('house_property_reject')->where('id', $id)->first();
        
        if (is_null($houseProperty)) {
            return redirect()->back()->with(['message', "Data tidak ditemukan"]);
        }
        $reject = new HousePropertyReject();
        $reject->house_property_id = $id;
        $reject->detail = $request->input('reject_description');
        $reject->save();

        $houseProperty->is_active = 0;
        $houseProperty->save();

        return Redirect()->back()->with(['message', 'Sukses, Status properti tidak aktif sekarang']);
    }

    public function verify(Request $request, $id)
    {
        $houseProperty = HouseProperty::find($id);
        if (is_null($houseProperty) or $houseProperty->is_active == 1) {
            return redirect("/admin/house_property/reject/".$id);
        }
        $status = $houseProperty->is_active == 1 ? 0 : 1;
        $houseProperty->is_active = $status;
        if ($status) {
            $slug = $houseProperty->slug_verify;
            if (!is_null($slug)) $houseProperty->slug = $slug;
        }
        $houseProperty->save();
        
        return redirect()->back()->with(['message', 'Success update status']);
    }

    public function phoneVerification(Request $request, $id)
    {
        DB::beginTransaction();
        $houseProperty = HouseProperty::findOrFail($id);
        if ($houseProperty->is_verified_phone == 0) {

            $smsNotif = false;
            if (!is_null($houseProperty->owner_phone) and strlen($houseProperty->owner_phone) > 8) {
                $smsNotif = true;
                $phone = $houseProperty->owner_phone;
            } else if (!is_null($houseProperty->manager_phone) and strlen($houseProperty->manager_phone) > 8) {
                $smsNotif = true;
                $phone = $houseProperty->manager_phone;
            }

            if ($smsNotif) {
                $message = "MAMIKOS: Iklan ". $houseProperty->name ." Anda aktif di Mamikos.com. Terima kasih. link ".$houseProperty->share_url." . CS: ".Config::get('api.cs.phone_number');
                $cleanPhoneNumber = "0".SMSLibrary::phoneNumberCleaning($phone);
                SMSLibrary::send($cleanPhoneNumber, $message);
            }
            $houseProperty->is_verified_phone = 1;
            $houseProperty->save();
        }
        DB::commit();
        return redirect()->back()->with(['message', 'Sukses verifikasi nomer hp pemilik']);
    }
}
