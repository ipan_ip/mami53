<?php

namespace App\Http\Controllers\Admin\GoldPlus;

use App\Entities\Activity\Action;
use App\Entities\Activity\ActivityLog;
use App\Entities\GoldPlus\GoldPlusAdminActionLog;
use App\Entities\GoldPlus\Package;
use App\Entities\Room\Room;
use App\Enums\GoldPlus\AdminActionCode;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Validator;
use App\User;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Exception;
use App\Entities\Level\KostLevel;
use App\Entities\Owner\Goldplus\OwnerGoldplusStatistic;
use App\Http\Helpers\GoldplusRoomHelper;
use App\Repositories\GoldPlus\PackageRepository;
use Illuminate\Support\Collection;

/**
 * Controller for goldplus admin dashboard.
 * @see https://mamikos.atlassian.net/wiki/spaces/U/pages/622887050/Goldplus+Statistic+Performance+in+Bangkerupux for confluence link
 */
class GoldPlusController extends Controller
{
    private const CONTENT_HEADER = 'Gold Plus Management';
    private const URL_DASHBOARD_INDEX = '/admin/gold-plus';
    private const PATH_OWNER_STATISTIC = '/goldplus/statistic/';

    protected $repository;

    public function __construct(PackageRepository $repository)
    {
        $this->repository = $repository;

        view()->share('contentHeader', self::CONTENT_HEADER);
    }

    /**
     * Open Gold Plus Admin Dashboard
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request): \Illuminate\View\View
    {
        /** @var \Illuminate\Database\Eloquent\Builder|Room  */
        $rooms = Room::with([
            'owners.user',
            'level',
        ])
        ->whereNull('apartment_project_id')
        ->whereHas('owners', function($owner) {
            $owner->whereIn('status', ['add', 'verified', 'draft2']);
        })
        ->whereHas('level', function ($level) use ($request) {
            /** @var KostLevel $level */
            /** @var Request $request */
            if (!empty($request->gp_level)) {
                $level->whereId($request->gp_level);
            } else {
                $level->whereIn('id', KostLevel::getGoldplusLevelIds());
            }
        })
        ->orderBy('updated_at', 'DESC');
        if ($request->anyFilled(['owner_name', 'owner_phone'])) {
            $rooms->whereHas('owners.user', function ($user) use ($request) {
                /** @var Request $request */
                /** @var User $user */
                if (!empty($request->owner_name)) {
                    $user->whereName($request->owner_name);
                }
                if (!empty($request->owner_phone)) {
                    $user->wherePhoneNumber($request->owner_phone);
                }
            });
        }
        if (!empty($request->room_name)) {
            $rooms->whereName($request->room_name);
        }
        if (!empty($request->is_testing)) {
            $rooms->whereIsTesting($request->is_testing);
        }

        $paginatedRooms = $rooms->paginate();
        $roomCollections = new Collection($paginatedRooms->items());
        $this->assignLatestReport($roomCollections);
        unset($rooms);

        $data = [
            'rooms' => $roomCollections,
            'pagination' => $paginatedRooms->appends($request->except('page'))->render(),
        ];
        return view('admin.contents.gold-plus.index', $data);
    }

    /**
     * Assigning the latest report information to each room (if available)
     *
     * @param Collection $roomCollections
     * @return void
     */
    private function assignLatestReport(Collection $roomCollections): void
    {
        $roomIds = $roomCollections->pluck('id');
        /** @var \Illuminate\Database\Eloquent\Collection|OwnerGoldplusStatistic[] */
        $statisticDocs = OwnerGoldplusStatistic::whereIn('room.id', $roomIds)->latest()->groupBy('room')->get(['created_at']);
        $roomCollections->map(function (Room $room) use ($statisticDocs) {
            $latestStatistic = $statisticDocs->where('room.id', $room->id)->first();
            if (!is_null($latestStatistic)) {
                $room->latestReportTimestamp = $latestStatistic->created_at;
                $room->statisticUrl = GoldplusRoomHelper::getGoldplusStatisticWebUrl($room->song_id);
            }
        });
        unset($statisticDocs);
        unset($roomIds);
    }

    /**
     * Regenerate the report for selected kost id
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function regenerateReport(Request $request): \Illuminate\Http\RedirectResponse
    {
        Validator::make($request->all(), [
            'id' => 'required|numeric',
        ]);
        $redirect = redirect($request->current_url ?? self::URL_DASHBOARD_INDEX);
        try {
            $id = $request->id;
            $room = Room::find($id);
            if (is_null($room)) {
                throw new Exception("Room not found");
            }
            $loggedInUser = auth()->user();
            if (is_null($loggedInUser)) {
                throw new Exception("No logged in user information");
            }
            $actionLog = GoldPlusAdminActionLog::new($id, $loggedInUser->id, AdminActionCode::REGENERATE_REPORT);
            $actionLog->save();
            $redirect->with('message', __('api.goldplus.success.regenerate-report', ['roomId' => $id, 'roomName' => $room->name]));
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
            $redirect->with('error_message', __('api.goldplus.error.regenerate-report', ['message' => $e->getMessage()]));
        }
        return $redirect;
    }


    /**
     * Open goldplus action log history admin page
     *
     * @param int $id
     * @return \Illuminate\View\View
     */
    public function getHistory(int $id): \Illuminate\View\View
    {
        /** @var \Illuminate\Database\Eloquent\Collection|GoldPlusAdminActionLog[] */
        $logs = GoldPlusAdminActionLog::where('room_id', $id)->get();
        /** @var \Illuminate\Database\Eloquent\Collection|User[] */
        $admins = User::whereIn('id', $logs->pluck('user_id')->unique())->get();
        // Manually map the logs with associated admins
        $logs->map(function (GoldPlusAdminActionLog $log) use ($admins) {
            $log->admin = $admins->find($log->user_id);
            $log->action_code_message = __('api.goldplus.action-code')[$log->action_code]['message'] ?? null;
            return $log;
        });
        $data = [
            'logs' => $logs,
            'room' => Room::find($id),
        ];
        return view('admin.contents.gold-plus.history', $data);
    }

    /**
     * Open goldplus package admin page
     *
     * @return \Illuminate\View\View
     */
    public function listPackage(): \Illuminate\View\View
    {
        /** @var \Illuminate\Database\Eloquent\Collection|Package[] */
        $packages = $this->repository->all();
        return view('admin.contents.gold-plus.package.index')->with('packages', $packages);
    }

    /**
     * Edit goldplus package
     *
     * @param int $id
     * 
     * @return \Illuminate\View\View
     */
    public function editPackage(int $id): \Illuminate\View\View
    {
        /** @var \App\Entities\GoldPlus\Package */
        $package = $this->repository->find($id);
        return view('admin.contents.gold-plus.package.edit')->with('package', $package);
    }

    /**
     * Update goldplus package data
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * 
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updatePackage(Request $request, int $id): \Illuminate\Http\RedirectResponse
    {
        /** @var \App\Entities\GoldPlus\Package */
        $package = $this->repository->find($id);
        $validatedData = $request->validate([
            'description' => 'string|max:250',
            'price' => 'required|numeric|gt:0'
        ], [
            'price.required' => 'Harga harus diisi',
            'price.numeric' => 'Harga harus berupa angka',
            'description.string' => 'Deskripsi harus berupa string',
            'description.max' => 'Deskripsi maksimal 250 karakter'
        ]);

        $packageData = $request->only([
            'description',
        ]);

        $updatePrice = $this->repository->updatePriceById($package->id, $validatedData['price']);
        $updateData = $this->repository->update($packageData, $package->id);

    	if (!$updatePrice || is_null($updateData)) {
            return redirect()->back()->with('error_message', 'Data GoldPlus package gagal diubah');
        }

        ActivityLog::LogUpdate(Action::GOLD_PLUS_PACKAGE, $id);
        return redirect()->route('admin.gold-plus.package.index')->with('message', 'Data GoldPlus package berhasil diubah');
    }
}
