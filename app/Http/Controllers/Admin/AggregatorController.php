<?php

namespace App\Http\Controllers\Admin;

use App\Http\Helpers\ApiHelper;
use Illuminate\Http\Request;
use App\Entities\Aggregator\AggregatorPartner;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Auth;
use Validator;
use Illuminate\Validation\Rule;

class AggregatorController extends Controller
{
    protected $type = [
        'Job'
    ];

    public function __construct() 
    {
        $this->middleware(function ($request, $next) {
            if(!Auth::user()->can('access-aggregator')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        View::share('contentHeader', 'Aggregator Controller');
        View::share('user', Auth::user());
    }

    public function index(Request $request)
    {
        $query = AggregatorPartner::query();

        if($request->filled('q') && $request->get('q') != '') {
            $query = $query->where('partner_name', 'like', '%' . $request->get('q') . '%')
                        ->orWhere('feeds_url', 'like', '%' . $request->get('q') . '%');
        }

        $partners = $query->paginate(20);

        $viewData    = array(
            'boxTitle'    =>  'Aggregator Partner',
            'partners'    => $partners
        ); 

        ActivityLog::LogIndex(Action::AGGREGATOR_PARTNER);

        return view ("admin.contents.aggregator-partner.index", $viewData);
    }

     public function create(Request $request)
    {
        $type = $this->type;
        $viewData = [
            'boxTitle'    => 'Create Aggregator Partner',
            'typeOptions' => $type
        ];

        return view('admin.contents.aggregator-partner.create', $viewData);
    }

    public function store(Request $request)
    {
    	$validator = Validator::make($request->all(), [
            'type'          => 'required',
    		'partner_name'  => 'required',
            'feeds_url'     => 'required|url'
        ]);

        if($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        $aggregator_partner 		        = new AggregatorPartner;
        $aggregator_partner->type           = $request->input('type');
        $aggregator_partner->partner_name   = $request->input('partner_name');
        $aggregator_partner->feeds_url  	= $request->input('feeds_url');
        $aggregator_partner->save();

        ActivityLog::LogCreate(Action::AGGREGATOR_PARTNER, $aggregator_partner->id);

        return redirect()->route('admin.aggregator.index')->with('messages', 'Aggregator Partner berhasil ditambahkan');
    }

    public function edit(Request $request, $id)
    {
        $partner = AggregatorPartner::find($id);

        if(!$partner) {
            return redirect()->back()->with('error_message', 'Data tidak ditemukan');
        }

        $viewData = [
            'boxTitle'       => 'Edit Aggregator Partner',
            'partner'        => $partner,
            'typeOptions'    => $this->type
        ];

        return view('admin.contents.aggregator-partner.edit', $viewData);
    }

    public function update(Request $request, $id)
    {
        $partners = AggregatorPartner::find($id);

        if(!$partners) {
            return redirect()->back()->with('error_message', 'Data tidak ditemukan');
        }

        $validator = Validator::make($request->all(), [
            'type'          => 'required',
            'partner_name'  => 'required',
            'feeds_url'     => 'required|url'
        ]);

        if($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        $partners->type           = $request->input('type');
        $partners->partner_name   = $request->input('partner_name');
        $partners->feeds_url      = $request->input('feeds_url');
        $partners->save();

        ActivityLog::LogUpdate(Action::AGGREGATOR_PARTNER, $id);

        return redirect()->route('admin.aggregator.index')->with('message', 'Data berhasil diupdate');
    }

    public function destroy(Request $request,$id)
    {
        $partners = AggregatorPartner::findOrFail($id);
        $partners->delete();

        ActivityLog::LogDelete(Action::AGGREGATOR_PARTNER, $id);

        return redirect()->route('admin.aggregator.index')->with('message', 'Aggregator Partner has been succesfully deleted');
    }
}
