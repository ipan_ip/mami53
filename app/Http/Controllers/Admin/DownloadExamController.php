<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;
use Auth;
use Validator;
use Illuminate\Validation\Rule;

use App\Entities\DownloadExam\DownloadExam;
use App\Entities\DownloadExam\DownloadExamFile;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;

use App\Http\Helpers\ApiResponse as Api;

/**
* 
*/
class DownloadExamController extends Controller
{
    protected $validationMessages = [
        'slug.required' => 'Slug harus diisi',
        'slug.max' => 'Slug maksimal :max karakter',
        'slug.unique' => 'Slug sudah digunakan sebelumnya',
        'title.required' => 'Judul harus diisi',
        'title.max' => 'Judul maksimal :max karakter',
        'title.unique' => 'Judul sudah digunakan sebelunya',
        'subtitle.max' => 'Subtitle maksimal :max karakter',
        'form_type.required' => 'Tipe form harus diisi',
        'files.required' => 'File download minimal 1',
        'files.required_unless' => 'File download minimal 1',
    ];

    protected $formTypes = ['general', 'college', 'multiple', 'multiple-child'];


    function __construct()
    {
        $this->middleware(function ($request, $next) {
            if(!Auth::user()->can('access-form-download-soal')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        View::share('contentHeader', 'Download Exam Management');
        View::share('user', Auth::user());
    }

    public function index(Request $request)
    {
        $query = DownloadExam::query();

        if($request->filled('q') && $request->get('q') != '') {
            $query = $query->where('title', 'like', '%' . $request->get('q') . '%');
        }

        $exams = $query->paginate(20);

        $viewData = [];
        $viewData['exams'] = $exams;
        $viewData['boxTitle'] = 'Download Exam List';

        ActivityLog::LogIndex(Action::FORM_DOWNLOAD_SOAL);

        return view('admin.contents.download-exam.index', $viewData);
    }

    public function create(Request $request)
    {
        $viewData = [];
        $viewData['boxTitle'] = 'Add Download Exam';
        $viewData['formTypes'] = $this->formTypes;

        return view('admin.contents.download-exam.create', $viewData);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => [
                'required',
                'max:255',
                Rule::unique('download_exam')->whereNull('deleted_at')
            ],
            'slug' => [
                'required',
                'max:190',
                Rule::unique('download_exam')->whereNull('deleted_at')
            ],
            'subtitle' => 'required|max:255',
            'excerpt' => 'nullable',
            'content' => 'nullable',
            'form_type' => 'required',
            'parent_id' => 'required_if:form_type,multiple-child',
            'files' => 'required_unless:form_type,multiple'
        ], $this->validationMessages);

        $validator->after(function($validator) use ($request) {
            if ($request->input('parent_id') != '') {
                if (!DownloadExam::find($request->input('parent_id'))) {
                    $validator->errors()->add('parent_id', 'Download Soal parent tidak ditemukan');
                }
            }
        });

        if ($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors())->withInput($request->all());
        }

        \DB::beginTransaction();

        $downloadExam = new DownloadExam;
        $downloadExam->slug = $request->input('slug');
        $downloadExam->title = $request->input('title');
        $downloadExam->subtitle = $request->input('subtitle');
        $downloadExam->excerpt = $request->input('excerpt');
        $downloadExam->content = $request->input('content');
        $downloadExam->form_type = $request->input('form_type');
        $downloadExam->parent_id = $request->input('form_type') == 'multiple-child' ? 
            $request->input('parent_id') : null;
        $downloadExam->save();

        if (count($request->input('files')) > 0 && $request->input('files') != '') {

            $files = DownloadExamFile::whereIn('id', $request->input('files'))->get();
            $fileNames = $request->input('file_names');

            foreach($files as $key => $file) {
                $file->download_exam_id = $downloadExam->id;

                if (isset($fileNames[$key]) && $fileNames[$key] != '') {
                    $file->name = $fileNames[$key];
                }
                
                $file->save();
            }
        }

        \DB::commit();

        return redirect()->route('admin.download-exam.index')->with('message', 'Form download berhasil ditambahkan');
    }

    public function edit(Request $request, $id)
    {
        $downloadExam = DownloadExam::with('files')->find($id);

        if(!$downloadExam) {
            return redirect()->route('admin.download-exam.index')->with('error_message', 'Data tidak ditemukan');
        }

        $viewData = [
            'boxTitle'    => 'Edit Form Download Soal',
            'downloadExam' => $downloadExam,
            'formTypes' => $this->formTypes
        ];

        return view('admin.contents.download-exam.edit', $viewData);
    }

    public function update(Request $request, $id)
    {
        $downloadExam = DownloadExam::find($id);

        if(!$downloadExam) {
            return redirect()->route('admin.download-exam.index')->with('error_message', 'Data tidak ditemukan');
        }

        $validator = Validator::make($request->all(), [
            'title' => [
                'required',
                'max:255',
                Rule::unique('download_exam')->ignore($id, 'id')->whereNull('deleted_at')
            ],
            'slug' => [
                'required',
                'max:190',
                Rule::unique('download_exam')->ignore($id, 'id')->whereNull('deleted_at')
            ],
            'subtitle' => 'required|max:255',
            'excerpt' => 'nullable',
            'content' => 'nullable',
            'form_type' => 'required',
            'parent_id' => 'required_if:form_type,multiple-child',
            'files' => 'required_unless:form_type,multiple'
        ], $this->validationMessages);

        $validator->after(function($validator) use ($request) {
            if ($request->input('parent_id') != '') {
                if (!DownloadExam::find($request->input('parent_id'))) {
                    $validator->errors()->add('parent_id', 'Download Soal parent tidak ditemukan');
                }
            }
        });

        if ($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors())->withInput($request->all());
        }

        \DB::beginTransaction();

        $downloadExam->slug = $request->input('slug');
        $downloadExam->title = $request->input('title');
        $downloadExam->subtitle = $request->input('subtitle');
        $downloadExam->excerpt = $request->input('excerpt');
        $downloadExam->content = $request->input('content');
        $downloadExam->form_type = $request->input('form_type');
        $downloadExam->parent_id = $request->input('form_type') == 'multiple-child' ? 
            $request->input('parent_id') : null;
        $downloadExam->save();

        if (count($request->input('files')) > 0 && $request->input('files') != '') {
            $files = DownloadExamFile::whereIn('id', $request->input('files'))->get();
            $fileNames = $request->input('file_names');

            foreach($files as $key => $file) {
                $file->download_exam_id = $downloadExam->id;

                if (isset($fileNames[$key]) && $fileNames[$key] != '') {
                    $file->name = $fileNames[$key];
                }
                
                $file->save();
            }
        }

        $originalFiles = $downloadExam->files;
        if(count($originalFiles) > 0) {
            foreach($originalFiles as $file) {
                if (is_null($request->input('files')) || empty($request->input('files'))) {
                    $file->delete();
                    continue;
                }

                if(!in_array($file->id, $request->input('files'))) {
                    $file->delete();
                }
            }
        }

        \DB::commit();

        ActivityLog::LogUpdate(Action::FORM_DOWNLOAD_SOAL, $id);

        return redirect()->route('admin.download-exam.index')->with('message', 'Data berhasil diupdate');
    }

    public function fileUpload(Request $request)
    {
        $file = $request->file('file');

        $uploadedFile = DownloadExamFile::upload($file);

        return Api::responseData(compact('uploadedFile'));
    }

    public function delete(Request $request, $id)
    {
        $downloadExam = DownloadExam::find($id);

        if(!$downloadExam) {
            return redirect()->route('admin.download-exam.index')->with('error_message', 'Data tidak ditemukan');
        }

        $downloadExam->delete();

        ActivityLog::LogDelete(Action::FORM_DOWNLOAD_SOAL, $id);

        return redirect()->route('admin.download-exam.index')->with('message', 'Data berhasil dihapus');
    }

    public function redirectPage(Request $request, $downloadId)
    {
        $downloadData = DownloadExam::where('id', $downloadId)->first();
        if (is_null($downloadData)) {
            return back()->with('message', 'Download data tidak ditemukan');
        }

        $viewData    = array(
            'boxTitle' => 'Download redirect',
            'download' => $downloadData
        ); 

        return view ("admin.contents.download-exam.redirect", $viewData);
    }

    public function storeRedirectData(Request $request, $downloadId)
    {
        $downloadData = DownloadExam::where('id', $downloadId)->first();
        if (empty($request->input('destination_id'))) {
            $downloadData->redirect_id = null;
            $downloadData->save();
            return redirect('admin/download-exam')->with('message', 'Berhasil update');
        }
        $donwloadDestination = DownloadExam::where('id', $request->input('destination_id'))->first();

        if (is_null($downloadData) or is_null($donwloadDestination)) {
            return back()->with('error_message', 'Data tidak ditemukan');
        }
        $downloadData->redirect_id = $donwloadDestination->id;
        $downloadData->save();
        return redirect('admin/download-exam')->with('message', 'Berhasil update');
    }
}