<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Activity\Action;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\AppSchemes;
use App\Entities\Activity\View;
use App\Entities\Booking\BookingOnwerRequestEnum;
use App\Entities\Booking\BookingOwner;
use App\Entities\Generate\ListingReason;
use App\Entities\Mamipay\MamipayOwner;
use App\Entities\Media\PhotoCompleteQueue;
use App\Entities\Notif\AllNotif;
use App\Entities\Notif\NotifData;
use App\Entities\Notif\SendNotif;
use App\Entities\Notif\SettingNotif;
use App\Entities\Promoted\ViewPromote;
use App\Entities\Room\BookingOwnerRequest;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Entities\Room\RoomRevision;
use App\Entities\User\Notification AS NotifOwner;
use App\Http\Controllers\Controller;
use App\Http\Helpers\ApiResponse as Api;
use App\Http\Requests;
use App\Notifications\ClaimVerified;
use App\Notifications\KostLive;
use App\Notifications\PropertyUnverified;
use App\Services\Room\BookingOwnerRequestService;
use App\User;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Validator;
use Yajra\Datatables\Facades\Datatables;
use Yajra\Datatables\Html\Builder;

class OwnerController extends Controller
{
    protected $inputTypes   = array("Semua", "Agen", "Pemilik Kos", "Pengelola Kos", "Lainnya", "Anak Kos");

    protected $htmlBuilder;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            // TODO : separate kost owner and kost agent
            if(!\Auth::user()->can('access-kost-owner') && !\Auth::user()->can('access-top-owner') && !\Auth::user()->can('access-kost-agent')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        \View::share('user', \Auth::user());
        \View::share('contentHeader', 'Event Management');
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(Request $request)
    {
        $query = RoomOwner::with([
            'room',
            'user',
            'room.tags',
            'room.cards',
            'room.tracking_update',
            'user.mamipay_owner.bank_city',
            'room.booking_owner_requests',
            'room.verification',
        ])
        ->orderBy('updated_at', 'desc');

        // Filter
        $query = $this->filterIndex($request, $query);
    
        $rowsOwner = $query->paginate(10);

        $statusFilter = [
            'claim'=>'Claim',
            'add'=>'Add',
            'edited' => 'Edited',
            'edited-ready' => 'Edited - Ready to Verify',
            'draft' => 'Draft',
            'verified' => 'Verified',
            'unverified' => 'Unverified',
            'decline' => 'Decline'
        ];

        $bbkFilter = [
            BookingOwnerRequest::BOOKING_APPROVE => 'Verified',
            BookingOwnerRequest::BOOKING_WAITING => 'Not Verified',
            BookingOwnerRequest::BOOKING_NOT_ACTIVE => 'Not Active',
            BookingOwnerRequest::BOOKING_REJECT => 'Reject',
            'no_bbk' => 'No BBK Data',
        ];

        $statusOwner = [
            "0" => "Semua",
            "1" => "Pemilik & pengelola kost",
            "2" => "Lainnya",
            "3" => "Pemilik, Agen & Pengelola Apartemen",
        ];

        $viewData = [
            'rowsOwner'    => $rowsOwner,
            'boxTitle'     => 'Room Owner Management',
            'searchUrl'    => route('admin.owner.index'),
            'statusFilter' => $statusFilter,
            'bbkFilter' => $bbkFilter,
            'statusOwner'  => $statusOwner
        ];

        ActivityLog::LogIndex(Action::KOST_OWNER);

        return view('admin.contents.owner.index', $viewData);
    }

    public function changeOwner(Request $request, $id)
    { 
        $room = Room::with('owners', 'owners.user')->where('id', $id)->first();

         $viewData = array(
            'boxTitle'     => 'Owner Detail',
            'deleteAction' => 'admin.tag.destroy',
            'room'         => $room,
        );

        return view('admin.contents.owner.change', $viewData);
    }

    public function checkOldNameKos(Request $request)
    {
        $status = true;
        if (strlen($request->input('old_name')) < 5) {
            return ["status" => false, "data" => []];
        }
        $revision = RoomRevision::where('old_value', 'LIKE', '%'.$request->input('old_name').'%')->select('revisionable_id', 'old_value', 'created_at', 'new_value')->orderBy('id', 'DESC')->get()->toArray();
        if (count($revision) < 1) $status = false;

        return ["status" => $status, "data" => $revision];
    }

    public function agenOwner(Request $request)
    {
        $query = RoomOwner::query()
                         ->whereIn('owner_status', ['Agen', 'Lainnya'])
                         ->where('status', 'add')
                         ->with('room', 'user', 'room.tags', 'room.cards')
                         ->orderBy('updated_at','desc');
        
        //if ($request->filled('q') OR $request->filled('owner_name')) {
            // ini_set('memory_limit', '410M');
        //}

        // Filter
        //$query = $this->filterIndex($request, $query);
        $rowsOwner = $query->paginate(10);
        
        $statusFilter = [
            'claim'=>'Claim', 
            'add'=>'Add', 
            'edited' => 'Edited', 
            'draft' => 'Draft', 
            'verified' => 'Verified', 
            'unverified' => 'Unverified', 
            'decline' => 'Decline'
        ];

        $statusOwner = [
              "0" => "Semua",
              "1" => "Pemilik & pengelola kost",
              "2" => "Lainnya"
        ];

        $viewData = [
            'rowsOwner'    => $rowsOwner,
            'boxTitle'     => 'Room Agent Owner Management',
            'searchUrl'    => route('admin.owner.index'),
            'statusFilter' => $statusFilter,
            'statusOwner'  => $statusOwner
        ];

        ActivityLog::LogIndex(Action::KOST_AGENT);

        return view('admin.contents.agen.index', $viewData);
    }

    public function changeToOwnerPage(Request $request, $id)
    {
        $roomOwner = RoomOwner::find($id);
        $roomOwner->status = 'edited';
        $roomOwner->save();
        ActivityLog::LogUpdate(Action::KOST_AGENT, $id);
        return back()->with('message','Berhasil edited');        
    }



    public function updateRoomOwner(Request $request, $id)
    {
        DB::beginTransaction();
        $roomPromote = ViewPromote::where('designer_id', $id)->first();
        if (!is_null($roomPromote)) {
            if ($roomPromote->is_active == 1) {
                return back()->with('message', 'Kos ini sedang aktif premiumnya, silahkan matikan dulu di menu premium request');
            } else {
                $roomPromote->delete();
            }
        }

        $user = User::where('phone_number', $request->input('owner_phone'))
                    ->where('is_owner', 'true')
                    ->first();

        if (is_null($user)) {
           return back()->with('message','Tidak menemukan user yang dimaksud');
        }

        $status = 'verified';
        if ($request->filled('status_kost')) $status = $request->input('status_kost');

        $data = array(
                 "designer_id" => $id,
                 "user_id"     => $user->id,
                 "status"      => $status,
                 "owner_status"=> $request->input('owner_status')
        );
        RoomOwner::where('designer_id', $id)->delete();
        RoomOwner::store($data);
        DB::commit();
        return redirect()->route('admin.room.index')->with('message', 'Berhasil mengganti owner');        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $owner = RoomOwner::with(['room.view_promote'])->where('id', $id)->first();
        if (is_null($owner)) {
            return back()->with('error_message', 'Owner tidak ditemukan');
        }

        if ($owner->room->is_promoted == "true") {
            return back()->with('error_message', 'Nonaktifkan premium untuk kos ini terlebih dahulu.');
        }
        
        $owner->room->view_promote()->delete();
        $owner->delete();

        ActivityLog::LogDelete(Action::KOST_OWNER, $id);

        return back()->with('message','Owner record deleted');    
    }

    public function verify(BookingOwnerRequestService $service, $id)
    {
        $roomOwner = RoomOwner::with('user', 'room')->where('id', $id)->first();

        if (is_null($roomOwner)) {
            return back()->with('error_message', 'Data tidak ditemukan');
        }

        if (is_null($roomOwner->user)) {
            return back()->with('error_message', 'User tidak ditemukan');
        }

        if ($roomOwner->user->is_owner == 'false') {
            return back()->with('error_message', 'User not owner');
        }

        $oldRoomStatus = $roomOwner->status;

        if ($roomOwner->status != 'verified') {
            $checkWasVerified = $this->checkStatusKost($roomOwner->designer_id);

            $room = $roomOwner->room;
            $room->is_verified_phone   = 1;
            $room->is_verified_address = 1;
            $room->save();

            if (!is_null($room->apartment_project_id)) {
                $apartmentProject = $room->apartment_project;
                if ($apartmentProject->is_active == 0) {
                    return redirect()->back()
                                ->with('error_message', "Apartemen Project sedang tidak aktif.");
                }
            }

            if ($checkWasVerified == true) {
                
                $OwnerStatus = array("Pemilik Kos", "Pemilik Kost", "Pengelola Kos", "Pengelola Kost",
                                    'Pemilik Apartemen', 'Pengelola Apartemen', 'Agen Apartemen');

                if ($roomOwner->status == 'add' && in_array($roomOwner->owner_status, $OwnerStatus)) {
                    $propertyType = is_null($room->apartment_project_id) ? 'Kost' : 'Unit';
                    $message = "MAMIKOS: " . $propertyType . " Anda sudah terlisting di Mamikos.com, silakan cek preview link ".$room->share_url.". Terima kasih.";
                    
                    // (new AllNotif)->smsToUser($roomOwner->user->phone_number, $message);
                }

                $this->doVerify($roomOwner);

                // prepare notification about photo completion
                PhotoCompleteQueue::makeReady($room);
            }

            Notification::send($roomOwner->user, new KostLive($room));
            NotifOwner::NotificationStore([
                "user_id" => $roomOwner->user_id,
                "designer_id" => $roomOwner->designer_id,
                "type" => "room_verif"
            ]);

            if (
                !is_null($roomOwner->user->mamipay_owner)
                && $roomOwner->user->mamipay_owner->status == MamipayOwner::STATUS_APPROVED
                // && $oldRoomStatus != RoomOwner::ROOM_EDIT_STATUS
            ) {
                try {
                    $service->singleApprove($roomOwner->user->id, $room->id, false);
                } catch (Exception $e) {
                    Bugsnag::notifyException($e, function ($report) use ($roomOwner) {
                        $report->setSeverity('info');
                        $report->setMetaData([
                            'room_owner' => $roomOwner
                        ]);
                    });
                }
            }
        }

        ActivityLog::LogUpdate(Action::KOST_AGENT, $id);

        return back()->with('message', 'Room has been successfully updated');
    }

    public function unverify(Request $request, $id)
    {
        $roomOwner = RoomOwner::with('user')->where('id', $id)->firstOrFail();

        if ($roomOwner->user->is_owner == 'false') {
           return back()->with('message','User not owner');
        }

        if ($roomOwner->status != 'unverified') {
            $this->doUnverify($roomOwner);
        } else {
            return back()->with('message','Room has been unverified');
        }

        return back()->with('message','Room has been successfully updated');
    }

    public function checkStatusKost($designer_id)
    { 
        $roomOwner = RoomOwner::where('designer_id', $designer_id)->where('status', 'verified')->count();
        
        if ($roomOwner > 0) return false;
        else return true;  
    }

    public function doVerify(RoomOwner $roomOwner)
    {
        $room = Room::with('room_input_progress')->find($roomOwner->designer_id);
        $room->kost_updated_date = date('Y-m-d H:i:s');
        $room->reject_remark = Null;
        $room->is_active = 'true';

        if(is_null($room->live_at) && $room->created_at->diffInDays(\Carbon\Carbon::now(), false) < 30) {
            $room->live_at = date('Y-m-d H:i:s');
        }

        $room->save(); 
        
        if (!is_null($room->room_input_progress) && !is_null($roomOwner)) {
            BookingOwner::insertOrUpdate($roomOwner->user);
        }

        Room::verifySlugDesigner($room->id);

        $roomOwner->status = 'verified';
        $roomOwner->save();

        // (re)Index to Elasticsearch
        $room->addToElasticsearch();

        $verification = $room->verification;
        if (! is_null($verification)) {
            $verification->is_ready = false;
            $verification->save();
        }
    }

    public function sendNotifToUser($user_id,$namaKost)
    {
        $kostOwned = RoomOwner::where('user_id', $user_id)->get();

        $user = User::find($user_id);

        $scheme = AppSchemes::getOwnerProfile();

        if(count($kostOwned) == 1 && strtotime($user->date_owner_limit) > time()) {
            $scheme .= '?is_tutorial=true';
        }
        
        // $notifData = array(
        //     'id'       => '100',
        //     'title'    => "mamiKost",
        //     'message'  => "Kost ".$namaKost." berhasil anda claim.",
        //     'scheme'   => $scheme,
        //     'photo'    => 'https://mamikos.com/assets/logo%20mamikos_beta_220.png',
        // );

        // $notifSend = new SendNotif(new NotifData($notifData));
        // $notifSend->setUserIds($user_id);
        // $notifSend->send();

        $message = "Kost " . $namaKost . " berhasil Anda claim";
        $userId  = $user_id; 
        $name    = 'MAMIKOS';
        $photo   = 'https://mamikos.com/assets/logo%20mamikos_beta_220.png';
        $scheme  = $scheme;
        $notifDataOwner = NotifData::buildFromParam($name, $message, $scheme, $photo);

        $sendNotif = new SendNotif($notifDataOwner);
        $sendNotif->setUserIds($userId);
        $sendNotif->send();

        Notification::send($user, new ClaimVerified());
    }

    public function doUnverify(RoomOwner $roomOwner)
    {
        $roomOwner->status = 'unverified';
        $unverifiedRoom =  $roomOwner->room->unverifyDesigner($roomOwner->designer_id);
        $roomOwner->save();

        if (!is_null($roomOwner->user)) {
            Notification::send($roomOwner->user, new PropertyUnverified($roomOwner->room));
        }

        $verification = $roomOwner->room->verification;
        if (! is_null($verification)) {
            $verification->is_ready = false;
            $verification->save();
        }

        return $unverifiedRoom;
    }

    private function filterIndex(Request $request, $query)
    {
        if ($request->filled('q')) {
            $query = $this->searchIndex($request->input('q'), $query);
        }

        if ($request->filled('owner_name') && $request->input('owner_name')) {
            $user = User::where('name', 'like', '%'.$request->input('owner_name').'%')->orWhere('id', $request->input('owner_name'))->get()->pluck('id');

            $query->whereIn('user_id', $user);
        }

        if($request->filled('phone_owner') && $request->input('phone_owner')) {
            $user = User::where('phone_number', 'like', '%'.$request->input('phone_owner').'%')->get()->pluck('id');

            if (count($user) == 0) {
               $room = Room::where('owner_phone', 'like', '%'.$request->input('phone_owner').'%')->get()->pluck('id');
               $query->whereIn('designer_id', $room);
            } else {
               $query->whereIn('user_id', $user);
            }   
        }

        if ($request->filled('status_owner') && $request->input('status_owner') == '2') {
           $query = $query->whereNotIn('owner_status', ['Pemilik Kost', 'Pemilik Kos', 'Pengelola Kost', 'Pengelola Kos']);
        } else if ($request->filled('status_owner') && $request->input('status_owner') == '1') {
            $query = $query->whereIn('owner_status', ['Pemilik Kost', 'Pemilik Kos', 'Pengelola Kost', 'Pengelola Kos']);
        } else if ($request->filled('status_owner') && $request->input('status_owner') == "3") {
            $query = $query->whereIn('owner_status', ['Pemilik Apartemen', 'Pengelola Apartemen', 'Agen Apartemen']);
        }

        if ($request->filled('from')) {
            $from = Carbon::createFromFormat("d-m-Y", $request->get('from'))->setTime(0,0,0);
            $query->where('created_at', '>', $from);
        }

        if ($request->filled('to')) {
            $to = Carbon::createFromFormat("d-m-Y", $request->get('to'))->addDay()->setTime(0,0,0);
            $query->where('created_at', '<', $to);
        }

        if ($request->filled('status')) {
            $status = array($request->input('status'));
            if ($request->input('status') == 'edited') {
                $status = array("edited", "draft2");
            } elseif ($request->input('status') == 'edited-ready') {
                $status = array("edited", "draft2");
                $query->whereHas('room.verification', function ($q) {
                    $q->where('is_ready', true);
                });
            }
        
            $query->whereIn('status', $status);
        }

        if($request->filled('bbk-status')) {
            $bbkStatus = $request->input('bbk-status');
            if ($bbkStatus == 'no_bbk') {
                $query->doesntHave('room.booking_owner_requests');
            }else{                
                $query->whereIn('designer_id', function($query) use ($bbkStatus) {
                    $query->select('designer_id')
                    ->from(with(new BookingOwnerRequest())->getTable())
                    ->where('status', $bbkStatus);
                });
            }
        }

        return $query;
    }

    private function searchIndex($search, $query)
    {
        // $room = Room::where('name', 'like', '%'. $search .'%')->get();

        // if ($room != null) {
        //     $ids = $room->pluck('id')->toArray();
        // } else {
        //     $roomWithId = Room::where('id', $search)->get();
        //     if ($roomWithId != null) {
        //         $ids = $roomWithId->pluck('id')->toArray();
        //     } else {
        //         return $query;
        //     }
        // }

        return $query->whereHas('room', function($query) use ($search) {
            $query->where('designer.name', 'like', '%' . $search . '%');
        });

        // return $query->whereIn('designer_id', $ids);
    }

    public function settingOwner($userId)
    {
        $owner = RoomOwner::with('room', 'room.setting_notif')->where('user_id', $userId)->get();
        $setting = SettingNotif::with('user')->where('for', 'user')->where('identifier', $userId)->get();

        $data = array(
                "owners" => $owner,
                "settings" => $setting
            );

        return view('admin.contents.owner.setting', $data);
    }

    public function settingOwnerNotif($key, $roomId)
    {
        $data = array(
                "key" => $key,
                "identifier" => $roomId,
                "for" => 'designer' 
            );

        $SettingNotif = SettingNotif::insertSettingNotif($data);
        
        return back()->with('message','Success changed');
    }

    public function editRejectRemark(Request $request, $id)
    {
        $room = Room::find($id);

        if (!$room) {
            return redirect()->back()->with('error_message', 'Kos tidak ditemukan');
        }

        if ($room->isKost()) {
            return view('admin.contents.owner.reject-reason');
        }

        $rejectReasonHistory = ListingReason::with('user')
            ->where('listing_id', $room->id)
            ->where('from', 'room')
            ->get();

        $viewData = [
            'room' => $room,
            'boxTitle' => 'Alasan Ditolak',
            'reject_reason' => $rejectReasonHistory,
            'reject_reason_select' => __('notification.admin.reject-remark'),
        ];

        return view('admin.contents.owner.reject-remark', $viewData);
    }

    public function updateRejectRemark(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'room_id'=>'required',
                'reject_remark_text'=>'max:300'
            ],
            [
                'room_id.required'=>'Room ID harus diisi',
                'reject_remark_text.max'=>'Alasan ditolak maksimal :max karakter'
            ]
        );

        if ($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors())->withInput($request->all());
        }

        $rejectRemark = $request->input('reject_remark');
        if ($rejectRemark == 'textarea') {
            $rejectRemark = $request->input('reject_remark_text');
        }
        $rejectRemarkLength = strlen(trim($rejectRemark));

        $room = Room::with('owners', 'owners.user')->where('id', $request->get('room_id'))->firstOrFail();

        $isFirstListing = ListingReason::where('listing_id', $room->id)->where('from', 'room')->orderBy('id', 'desc')->first();

        if ($rejectRemarkLength == 0 && is_null(($isFirstListing))) {
            return redirect()->back()->with('error_message', 'Alasan ditolak harus diisi');
        } elseif ($rejectRemarkLength == 0 && !is_null(($isFirstListing))) {
            $rejectRemark = $isFirstListing->content;
        }

        if (is_null($room)) {
            return redirect()->back()->with('error_message', 'Kost tidak ditemukan');
        }

        if (!$room->owners->count()) {
            return redirect()->back()->with('error_message', 'Pemilik kost tidak ditemukan');
        }

        // get the unverified Room
        $unverifiedRoom = $this->doUnverify($room->owners->first());

        if ($rejectRemarkLength > 0) {
            $reason = new ListingReason();
            $reason->from = "room";
            $reason->listing_id = $room->id;
            $reason->content = $rejectRemark;
            $reason->user_id = Auth::id();
            $reason->save();
        }

        //variable unverifiedRoom is used for latest updated data(is_active = 0)
        $unverifiedRoom->reject_remark = $rejectRemark;
        $unverifiedRoom->save();

        $notifType = is_null($room->apartment_project_id) ? 'kost_reject' : 'apartment_reject';

        NotifOwner::NotificationStore([
            "user_id" => $room->owners[0]->user_id,
            "type" => $notifType,
            "designer_id" => $room->id,
        ]);

        return redirect()->route('admin.owner.index');
    }

    public function kostOwner(Request $request, $phone)
    {
        $user = User::with(['room_owner'])
                    ->where('phone_number', $phone)
                    ->where('is_owner', 'true')
                    ->first();
        if (is_null($user)) {
            return Api::responseData(["status" => false, "meta" => ["message" => "Pemilik tidak ditemukan"]]);
        }

        $roomOwner = $user->room_owner;
        $room = [];
        foreach ($roomOwner as $key => $value) {
            $room[] = [
                "id" => $value->room->id,
                "name" => $value->room->name,
                "status" => $value->status,
                "is_booking" => $value->room->is_booking 
            ];
        }

        return Api::responseData(["status" => true, "room" => $room, "owner_id" => $user->id]);
    }

    public function bookingRoom(Request $request)
    {
        if (!$request->filled('room_id') or !$request->filled('owner_id')) {
            return Api::responseData(["status" => false, "meta" => ["message" => "Kos tidak ditemukan"]]);
        }

        $ownerId = $request->input('owner_id');
        $roomIds = json_decode($request->input('room_id'), true);
        if (count($roomIds) == 0 or $ownerId == 0) {
            return Api::responseData(["status" => false, "meta" => ["message" => "Pilih salah satu kos."]]);
        }

        foreach ($roomIds as $value) {
            $bookingRequest = BookingOwnerRequest::where('designer_id', $value)->first();
            if (!is_null($bookingRequest)) {
                continue;
            }
            $bookingRequest = new BookingOwnerRequest();
            $bookingRequest->designer_id = $value;
            $bookingRequest->user_id = $ownerId;
            $bookingRequest->status = "waiting";
            $bookingRequest->registered_by = BookingOnwerRequestEnum::SYSTEM;
            $bookingRequest->save();
        }

        return Api::responseData(["status" => true]);
    }
}
