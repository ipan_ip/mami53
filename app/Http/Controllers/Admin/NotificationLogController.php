<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\Component\NotificationLogFilterTrait;

use App\Entities\Notif\Log\NotificationLog;

use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;

class NotificationLogController extends Controller {
    use NotificationLogFilterTrait;

    public function __construct() {
        View::share('contentHeader', 'Notification Log');
        View::share('user', Auth::user());
    }

    public function index(Request $request) {
        $logs = NotificationLog::query();
        $logs = $this->filterLogByNotificationID($logs, $request);
        $logs = $this->filterLogByChannel($logs, $request);
        $logs = $this->filterLogByCreationDate($logs, $request);
        $logs = $logs->paginate(20);

        $viewData = [
            'boxTitle' => 'Notification Log',
            'logs' => $logs,
        ];

        ActivityLog::LogIndex(Action::NOTIFICATION_LOG);

        return view('admin.contents.notification_log.index', $viewData);
    }
}