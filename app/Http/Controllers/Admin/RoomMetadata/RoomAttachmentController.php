<?php

namespace App\Http\Controllers\Admin\RoomMetadata;

use Illuminate\Http\Request;
use Illuminate\Validation\Validator;

use Auth;

use App\Http\Controllers\Admin\Component\Room\RoomFilterTrait;
use App\Http\Controllers\Admin\Component\Room\RoomSorterTrait;

use App\Entities\Room\RoomAttachment;
use App\Entities\Room\Room;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;
use App\Presenters\RoomPresenter;

use App\Http\Controllers\Controller;

class RoomAttachmentController extends Controller {

    use RoomFilterTrait, RoomSorterTrait;

    public function __construct()
    {
        \View::share('contentHeader', 'VR Tour Management');
        \View::share('user' , Auth::user());
    }

    public function index(Request $request) {
        $query = Room::query()->has('attachment')->with([
            'photo_by' => function($t) {
                $t->orderBy('id', 'desc');
            },
            'minMonth', 
            'view_promote', 
            'owners', 
            'view_promote.premium_request.user', 
            'apartment_project', 
            'photo', 
            'agents',
            'tracking_update', 
            'active_discounts',
        ]);

        // Filter
        $query = $this->filterIndex($request, $query);

        $query = $this->sortByActivityStatus($request, $query);

        $query = $this->sortByCreationDate($request, $query);

        $rowsDesigner = $query->paginate(20);

        $rowsRoom = (new RoomPresenter('vrtour-index'))->present($rowsDesigner);

        $viewData = [
            'rowsDesigner'    => $rowsDesigner,
            'rowsRoom'        => $rowsRoom['data'],
            'startDate'       => $request->input('st'),
            'endDate'         => $request->input('en'),
            'redirectHere'    => \Request::url() . '?' . http_build_query($_GET),
        ];

        ActivityLog::LogIndex(Action::VR_TOUR);

        return view('admin.contents.vrtour.index', $viewData);
    }

    public function create(Room $room) {
        $viewData = [
            'room' => $room,
            'create' => true,
        ];

        return view('admin.contents.vrtour.form', $viewData);
    }

    public function store(Request $request, Room $room) {
        $isConfirmedMatterportNotUnique = ($request->confirm == '1');

        if ($isConfirmedMatterportNotUnique) {
            $validator = validator($request->all(), ['matterport_id' => 'required|string|size:11']);
        }
        else {
            // User is required to confirm if they want to save a vr tour with a non-unique matterport id.
            $validator = validator($request->all(), ['matterport_id' => 'required|string|size:11|unique:designer_attachment']);
        }

        if ($validator->fails()) {
            return redirect()->route('admin.vrtour.create', $room->id)->withErrors($validator)->withInput();
        }

        $vrTour = new RoomAttachment();
        $vrTour->matterport_id = $request->matterport_id;
        $vrTour->is_matterport_active = ($request->is_matterport_active ?? 0);

        $room->attachment()->save($vrTour);

        ActivityLog::LogCreate(Action::VR_TOUR, $vrTour->id);

        return redirect()->route('admin.vrtour.index');
    }

    public function edit(Request $request, RoomAttachment $vrTour) {
        $viewData = [
            'room' => $vrTour->room,
            'edit' => true,
            'vrTour' => $vrTour,
        ];

        return view('admin.contents.vrtour.form', $viewData);
    }

    public function update(Request $request, RoomAttachment $vrTour) {
        $isConfirmedMatterportNotUnique = ($request->confirm == '1');

        if ($isConfirmedMatterportNotUnique) {
            $validator = validator($request->all(), ['matterport_id' => 'required|string|size:11']);
        }
        else {
            // User is required to confirm if they want to save a vr tour with a non-unique matterport id.
            $validator = validator($request->all(), ['matterport_id' => 'required|string|size:11|unique:designer_attachment']);
        }

        if ($validator->fails()) {
            return redirect()->route('admin.vrtour.edit', $vrTour->id)->withErrors($validator)->withInput();
        }

        $vrTour->matterport_id = $request->matterport_id;
        $vrTour->is_matterport_active = ($request->is_matterport_active ?? 0);
        $vrTour->save();
        ActivityLog::LogUpdate(Action::VR_TOUR, $vrTour->id);
        return redirect()->route('admin.vrtour.index');
    }

    public function destroy(Request $request, RoomAttachment $vrTour) {
        $vrTour->delete();

        ActivityLog::LogDelete(Action::VR_TOUR, $vrTour->id);

        return redirect()->route('admin.vrtour.index');
    }
}