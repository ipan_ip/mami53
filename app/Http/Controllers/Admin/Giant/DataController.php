<?php

namespace App\Http\Controllers\Admin\Giant;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\Agent\DummyDesigner;
use Auth;
use App\Libraries\SMSLibrary;
use App\Entities\Room\Element\Tag;
use App\Entities\Room\Element\RoomTag;
use App\Libraries\AreaFormatter;
use App\Entities\Room\Review;
use App\User;
use DB;
use App\Entities\Media\Media;
use App\Entities\Room\Room;
use App\Entities\Room\Element\Card;
use App\Entities\Room\Element\StyleReview;
use App\Entities\Generate\ListingReason;
use App\Entities\Agent\DummyPhoto;
use App\Entities\Agent\DummyReview;
use App\Entities\Agent\Agent;
use Illuminate\Support\Facades\Input;
use File;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;

class DataController extends Controller
{

    protected $inputTypes   = array("Semua", "Agen", "Pemilik Kos", "Pengelola Kos", "Lainnya", "Anak Kos", "Kos Duplikat", 
                                'Pemilik Apartemen', 'Pengelola Apartemen', 'Agen Apartemen', 'Gojek', 'scrap', 'scrap google', 'scrap koran');

    protected $status = ["Semua status", "submit", "live", "photo", "checkin"];

    protected $for_type = ["Semua data", "kos", "apartment"];

    public function __construct()
    {
        \View::share('contentHeader', 'Stories Management');
        \View::share('user' , Auth::user());
    }

    public function listData(Request $request)
    {
        //dd($request->all());
        $data_req = $request->all();
        if ($request->filled('room_name')) {
            $room = Room::where(function($p) use($data_req) {
                $p->where('name', 'like', '%'.$data_req['room_name'].'%')
                    ->orWhere('owner_phone', 'like', '%'.$data_req['room_name'].'%');
            })->get()->pluck('id')->toArray();
        }

        if ($request->filled('phone_number')) {
            $agent = Agent::where(function($p) use($data_req) {
                $p->where('name', 'like', '%'.$data_req['phone_number'].'%')
                    ->orWhere('phone_number', 'like', '%'.$data_req['phone_number'].'%');
            })->get()->pluck('id')->toArray();
        }

        $list_kos = DummyDesigner::with('room', 'agent');

        if (isset($room) AND count($room) > 0) {
            $list_kos = $list_kos->whereIn('designer_id', $room);
        }

        if (isset($agent) AND count($agent) > 0) {
            $list_kos = $list_kos->whereIn('agent_id', $agent);
        }

        if ($request->filled('for_type') AND in_array($request->input('for_type'), ['kos', 'apartment'])) {
            $list_kos = $list_kos->where('type', $request->input('for_type'));
        }

        if ($request->filled('statuses') AND in_array($request->input('statuses'), ["submit", "live", "photo", "checkin"])) {
            $list_kos = $list_kos->where('statuses', $request->input('statuses'));
        }

        if ($request->filled('from') AND $request->filled('to')) {
            $list_kos = $list_kos->whereDate('created_at', '>=', $request->input('from'))->whereDate('created_at', '<=', $request->input('to'));
        }

        $totalLive = 0;
        $totalSubmit = 0;
        $stat = false;
        if (count($request->all()) > 0) {
            //$totalSubmit = $this->count_data($list_kos, "submit");//$list_kos->where('statuses', 'submit')->count();
            //$totalLive = $this->count_data($list_kos, "live");//$list_kos->where('statuses', 'live')->count();
            $stat = true;
        }

        $list_kos = $list_kos->orderBy('updated_at', 'desc')->paginate(20);

        $data = [
            "boxTitle" => "List",
            "data" => $list_kos,
            "status" => $this->status,
            "for_type" => $this->for_type,
            "total_submit" => $totalSubmit,
            "total_live" => $totalLive,
            "stat" => $stat,
        ];

        ActivityLog::LogIndex(Action::AGENT_APP);

        return view('admin.contents.giant.index', $data);
    }

    public function count_data($data, $status)
    {
        return $data->where('statuses', $status)->count();
    }

    public function editData(Request $request, $id)
    {
        $dummy = DummyDesigner::with(['agent', 'room', 'room.dummy_photo', 'room.dummy_review'])->where('id', $id)->first();

        $checkin_photo = null;
        if (isset($dummy->room->dummy_photo)) {
            $checkin_photo = $dummy->room->dummy_photo->where('type', 'checkin')->first();
        }

        $ownerNow = User::where('phone_number', $dummy->agent->phone_number)->where('is_owner', 'false')->first();
        if (is_null($ownerNow)) {
            $is_user = false;
        } else {
            $is_user = true;
        }
        // Tag
        $dummy->room->concern_ids  = RoomTag::where('designer_id', '=', $dummy->room->id)->pluck('tag_id');

        $rowsConcern = Tag::where('type', '<>', 'fac_project')->get();
        $rowsConcernView = mySelectPreprocessing(
            'multi',
            'selected',
            'key-value',
            $rowsConcern,
            $dummy->room->concern_ids
        );

        $pricingusd = false;
        if (isset($dummy->room->room_price_type) AND $dummy->room->room_price_type->is_active == 1) {
            $pricing = [
                "price_monthly" => $dummy->room->price_monthly_usd,
                "price_weekly"  => $dummy->room->price_weekly_usd,
                "price_daily" => $dummy->room->price_daily_usd,
                "price_yearly" => $dummy->room->price_yearly_usd,
            ];
            $pricingusd = true;
        }

        if (!$pricingusd) {
            $pricing = [
                "price_yearly" => $dummy->room->price_yearly,
                "price_monthly" => $dummy->room->price_monthly,
                "price_weekly" => $dummy->room->price_weekly,
                "price_daily" => $dummy->room->price_daily 
            ];
        }
        
        $viewData = array(
            'boxTitle'    => 'View or Edit Stories',
            'rowDesigner' => $dummy->room,
            'manager_phone' => SMSLibrary::validateDomesticIndonesianMobileNumber($dummy->room->manager_phone),
            'owner_phone' => SMSLibrary::validateDomesticIndonesianMobileNumber($dummy->room->owner_phone),
            'pricing' => $pricing,
            'rowsConcern' => $rowsConcernView,
            'dummy' => $dummy,
            'price_type' => $pricingusd,
            'data_all'    => $request->all(),
            'status_agent'=> $this->inputTypes,
            'checkin_photo' => $checkin_photo,
            'photo_cover' => $dummy->room->dummy_photo->where('type', 'cover'),
            'photo_building' => $dummy->room->dummy_photo->where('type', 'bangunan'),
            'photo_bedroom' => $dummy->room->dummy_photo->where('type', 'kamar'),
            'photo_bathroom' => $dummy->room->dummy_photo->where('type', 'kamar-mandi'),
            'photo_other' => $dummy->room->dummy_photo->where('type', 'lainnya'),
            'photo_speed' => $dummy->room->dummy_photo->where('type', 'speed-test'),
            'is_user' => $is_user,
            'user' => $ownerNow
        );
//dd($checkin_photo);
        return view('admin.contents.giant.edit', $viewData);
    }

    public function storeReview(Request $request, $id)
    {
        $dummy_review = DummyReview::where('id', $id)->first();
        $user = User::where('phone_number', $request->input('phone_number'))->where('is_owner', 'false')->first();

        if (is_null($dummy_review) OR is_null($user)) {
            return redirect()->back()->with(["error_message" => "Data tidak ditemukan / user tidak ditemukan"]);
        }

        $check_review = Review::where('user_id', $user->id)->where('is_super', 1)->where("designer_id", $dummy_review->designer_id)->first();
        if (!is_null($check_review)) {
            //return redirect()->back()->with(["error_message" => "User sudah review sebelumnya"]);
            //$check_review->delete();
            $review = $check_review;
        } else {
            $review = new Review();
        }

        $review->cleanliness        = $dummy_review->cleanliness;
        $review->comfort            = $dummy_review->comfort;
        $review->safe               = $dummy_review->safe;
        $review->price              = $dummy_review->price;
        $review->room_facility      = $dummy_review->room_facilities;
        $review->public_facility    = $dummy_review->public_facilities;
        $review->status             = "live";
        $review->is_super           = 1;
        $review->content            = $request->input('review_content');//$dummy_review->content;
        $review->user_id            = $user->id;
        $review->designer_id        = $dummy_review->designer_id;
        $review->save();
        //delete old photo if exist
        $this->deleteStyleReview($review);
        $speedtest = DummyPhoto::where('designer_id', $dummy_review->designer_id)->where('type', 'speed-test')->first();
        
        $server = (int) $request->input('server');

        if (!is_null($speedtest)) {
            if ( $server == 1) {
                $speedtestimages = 'http://songturu2.mamikos.com/uploads/cache/data/dummy/'. $speedtest->file_name;
            } else {
                $speedtestimages = url('uploads/cache/data/dummy/', $speedtest->file_name);
            }
            $reviewmediaspeedtest = Media::postMedia($speedtestimages, 'url', null, null, null, 'review_photo', true, null, false, ["from" => "agent", "type" => "speed-test"]);
            $this->addStyleReview($reviewmediaspeedtest['id'], $review);    
        }
        
        if ($request->filled('review_photo') AND count($request->input('review_photo')) > 0) {
            foreach ($request->input('review_photo') AS $key => $value) {
                $this->addStyleReview($value, $review);
            }
            Media::whereIn('id', $request->input('review_photo'))->update(["type" => "review_photo"]);
        } else {
            if ($server == 1) {
                $urlreview = 'http://songturu2.mamikos.com/uploads/cache/data/dummy/'. $dummy_review->photo;
            } else {
                $urlreview = url('uploads/cache/data/dummy/', $dummy_review->photo);
            }
    
            $reviewmedia = Media::postMedia($urlreview, 'url', null, null, null, 'review_photo', true, null, false, ["from" => "agent"]);
            $this->addStyleReview($reviewmedia['id'], $review);
        }

        return redirect('admin/giant/data')->with(["message" => "Berhasil manual review"]);
    }

    public function checkRemoteFile($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        // don't download content
        curl_setopt($ch, CURLOPT_NOBODY, 1);
        curl_setopt($ch, CURLOPT_FAILONERROR, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($ch);
        curl_close($ch);
        if($result !== FALSE)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function updateData(Request $request, $id)
    {
        /* if ($this->checkRemoteFile("http://songturu2.mamikos.com/uploads/cache/data/dummy/Kost%20Pak%20Dodi%20Ngaglik%20Sleman-5-dalam-kamar0964840618.jpg")) {
            dd("Yup. It exists.");
        } else {
            dd("nggak");
        } */

        if (!$request->filled('roomid')) {
            return redirect()->back()->with(['message' => "Data tidak ditemukan"]);
        }

        if (isset($request['is_reject']) AND $request['is_reject'] == "1") {
            $dummy = DummyDesigner::where('id', $id)->first();
            if (is_null($dummy)) {
                return redirect()->back()->with(['message' => 'Data tidak ditemukan']);
            }
            $dummy->agent_note = $request->input('agent_note');
            $dummy->statuses = "reject";
            $dummy->verificator = $request->input('verificator');
            $dummy->save();

            return redirect('admin/giant/data')->with(['message' => "Berhasil menolak"]);
        }
        
        DB::beginTransaction();
        $room = Room::where('id', $request->input('roomid'))->first();

        if ($request->filled('use_loc_agent') AND $request->input('use_loc_agent') == '1') {
            $room->latitude = $request->input('latitude_2');
            $room->longitude = $request->input('longitude_2');

            if (!empty($request->input('area_city_2'))) {
                $area_city = AreaFormatter::format($request->input('area_city_2'));
                $room->area_city = $area_city;
            }
    
            $area_subdistrict = NULL;
            if (!empty($request->input('area_subdistrict_2'))) {
               $area_subdistrict =  AreaFormatter::format($request->input('area_subdistrict_2'));
               $room->area_subdistrict = $area_subdistrict;
            }
            //$room->save();
        }

        $dummy = DummyDesigner::with(['agent', 'room', 'room.dummy_photo' => function($p) {
            $p->where('type', '!=', 'checkin');
        }, 'room.dummy_review'])->where('id', $id)->first();
        
        foreach ($dummy->room->dummy_photo AS $key => $value) {
            if ($dummy->server == 1) {
                $url = "http://songturu2.mamikos.com/uploads/cache/data/dummy/".$value->file_name;
            } else {
                $url = url('uploads/cache/data/dummy', $value->file_name);
            }

            $urlexist = false;
            if ($this->checkRemoteFile($url)) {
                $media = Media::postMedia($url, 'url', null, null, null, 'style_photo', true, null, false, ["from" => "agent"]);

                $card = new Card;
                $card->designer_id = $room->id;
                $card->description = str_replace(".jpg", "", $value->file_name);
                $card->photo_id = $media['id'];
                $card->save();
                $urlexist = true;
            } else {
                $url = preg_replace("/ /", "%20", $url);
                $media = Media::postMedia($url, 'url', null, null, null, 'style_photo', true, null, false, ["from" => "agent"]);

                $card = new Card;
                $card->designer_id = $room->id;
                $card->description = str_replace(".jpg", "", $value->file_name);
                $card->photo_id = $media['id'];
                $card->save();
                $urlexist = true;
            }
            
            if ($value->type == 'cover' AND $urlexist == true) $cover = $media['id'];
        } 

        $totalPhotoFromAgent = count($dummy->room->dummy_photo);

        $totalPhotoFromAgentDelete = 0;
        if (!is_null($dummy->card_delete)) {
            $card_delete = explode(",", $dummy->card_delete);
            Card::whereIn('id', $card_delete)->delete();
            if (!is_null($dummy->card_delete) AND $dummy->card_delete != 0) $totalPhotoFromAgentDelete = count($card_delete);
        }

        if ($request->input('room_available_agent_status') == '1') $room->room_available = $request->input('room_available_agent');
        if (isset($cover)) $room->photo_id = $cover;
        
        if ($request->filled('description_agent_status') AND $request->input('description_agent_status') == '1') {
            $room->description = $request->input('description_agent');
        } 

        $room->photo_count += $totalPhotoFromAgent - $totalPhotoFromAgentDelete;
        $room->is_visited_kost = 1;
        $room->kost_updated_date = date("Y-m-d H:i:s");
        
        if ($request->filled('wifi_speed')) {
            $room->wifi_speed = $request->input('wifi_speed');
        }

        $room->save();

        if (strlen(trim($request->input('agent_note'))) > 0) {
            $reason = ListingReason::where('listing_id', $room->id)->where('from', 'room')->first();

            if (is_null($reason)) {
                $reason = new ListingReason();
                $reason->from = "room";
                $reason->listing_id = $room->id;
                $reason->content    = $request->input('agent_note');
                $reason->save();
            } else {
                $reason->content = $request->input('agent_note');
                $reason->save();
            }
        }

        // insert to review table
        if ($dummy->type == 'kos') {
            $user = User::where('phone_number', $dummy->agent->phone_number)->where('is_owner', 'false')->first();
            if (!is_null($user)) {
                $dummy_review = $dummy->room->dummy_review[0];
                $reviewChecker = Review::where('designer_id', $dummy_review->designer_id)
                                        ->where('is_super', 1)
                                        ->where('user_id', $user->id)
                                        ->first();
                if (is_null($reviewChecker)) {
                    $review = new Review();
                } else {
                    $review = $reviewChecker;
                }
                $review->cleanliness       = $dummy_review->cleanliness;
                $review->comfort           = $dummy_review->comfort;
                $review->safe              = $dummy_review->safe;
                $review->price             = $dummy_review->price;
                $review->room_facility     = $dummy_review->room_facilities;
                $review->public_facility   = $dummy_review->public_facilities;
                $review->status            = "live";
                $review->is_super = 1;
                $review->content           = $request->input('review_content');//$dummy_review->content;
                $review->user_id = $user->id;
                $review->designer_id = $dummy_review->designer_id;
                $review->save();

                //delete old photo if exist
                $this->deleteStyleReview($review);
                $speedtest = DummyPhoto::where('designer_id', $dummy->designer_id)->where('type', 'speed-test')->first();

                if (!is_null($speedtest)) {
                    if ($dummy->server == 1) {
                        $speedtestimages = 'http://songturu2.mamikos.com/uploads/cache/data/dummy/'. $speedtest->file_name;
                    } else {
                        $speedtestimages = url('uploads/cache/data/dummy', $speedtest->file_name);
                    }
                    
                    $speedtestimages = preg_replace("/ /", "%20", $speedtestimages);
                    $reviewmediaspeedtest = Media::postMedia($speedtestimages, 'url', null, null, null, 'review_photo', true, null, false, ["from" => "agent", "type" => "speed-test"]);
                    $this->addStyleReview($reviewmediaspeedtest['id'], $review);    
                }
                
                // manual photo from admin
                if ($request->filled('review_photo') AND count($request->input('review_photo')) > 0) {
                    foreach ($request->input('review_photo') AS $key => $value) {
                        $this->addStyleReview($value, $review);
                    }
                    Media::whereIn('id', $request->input('review_photo'))->update(["type" => "review_photo"]);
                } else {
                    if ($dummy->server == 1) {
                        $urlreview = 'http://songturu2.mamikos.com/uploads/cache/data/dummy/'. $dummy_review->photo;
                    } else {
                        $urlreview = url('uploads/cache/data/dummy', $dummy_review->photo);
                    }
                    
                    $reviewmedia = Media::postMedia($urlreview, 'url', null, null, null, 'review_photo', true, null, false, ["from" => "agent"]);
                    $this->addStyleReview($reviewmedia['id'], $review);    
                }
            }
        }

        //update status dummy
        $dummy->statuses = 'live';
        $dummy->agent_note = null;
        $dummy->verificator = $request->input('verificator');
        $dummy->save();
        DB::commit();
        
        return redirect('admin/giant/data')->with(['message' => "Berhasil live"]);
    }

    private function addStyleReview($photo_id, $review)
    {
        $style = new StyleReview();
        $style->review_id = $review->id;
        $style->photo_id = $photo_id;
        $style->save();
        return true;
    }

    private function deleteStyleReview($review)
    {
        $style = styleReview::where('review_id', $review->id)->delete();
        return true;
    }

    public function rejectDataAgent(Request $request, $id)
    {
        $dummy = DummyDesigner::where('id', $id)->first();
        if (is_null($dummy)) {
            return redirect()->back()->with(["message" => "Data tidak ditemukan"]);
        }

        $dummy->statuses = "reject";
        $dummy->save();

        return redirect('admin/giant/data')->with(["message" => "Berhasil"]);
    }

    public function deleteData(Request $request, $id)
    {
        $dummy = DummyDesigner::where('id', $id)->first();
        if (is_null($dummy)) {
            return redirect()->back()->with(['message' => "Data tidak ditemukan"]);
        }

        DummyPhoto::where('designer_id', $dummy->designer_id)->delete();
        DummyReview::where('designer_id', $dummy->designer_id)->delete();

        $dummy->delete();
        
        return redirect()->back()->with(['message' => "Sukses hapus"]);
    }

    public function agentList(Request $request)
    {
        $agents = Agent::whereNotNull('unique_code')->orderBy('id', 'desc')->paginate(20);
        $data = [
            "boxTitle" => "Agent list",
            "agent" => $agents
        ];
        return view('admin.contents.giant.kos.list', $data);
    }

    public function addNewAgent(Request $request)
    {
        $agent = new Agent;
        $agent->unique_code = Input::old('unique_code');
        $agent->type = Input::old('type');
        $agent->name = Input::old('name');
        $agent->phone_number = Input::old('phone_number');
        $agent->is_active = Input::old('is_active');
        
        $data = [
            "boxTitle" => "Add new agent",
            "agent" => $agent,
            "action" => "/admin/giant/kos/agent"
        ];
        return view('admin.contents.giant.kos.form', $data);
    }

    public function getEdit(Request $request, $id)
    {
        $agent = Agent::where('id', $id)->first();
        if (is_null($agent)) {
            abort(404);
        }

        $data = [
            "boxTitle" => "Add new agent",
            "agent" => $agent,
            "action" => "/admin/giant/kos/agent/update/".$id
        ];
        return view('admin.contents.giant.kos.form', $data);
    }

    public function updateAgent(Request $request, $id)
    {
        $agent = Agent::where('id', $id)->first();
        if (is_null($agent)) {
            abort(404);
        }
        
        $agent->unique_code = $request->input('unique_code');
        $agent->type = $request->input('type');
        $agent->name = $request->input('name');
        $agent->phone_number = $request->input('phone_number');
        $agent->is_active = $request->input('is_active');
        $agent->save();
        
        ActivityLog::LogUpdate(Action::AGENT, $id);

        return redirect('admin/giant/kos/list')->with(['message', 'Sukses edit akun '.$agent->name]);
    }

    public function storeAgent(Request $request)
    {
        $agent = Agent::where(function($p) use($request) {
                            $p->where('unique_code', $request->input('unique_code'))
                                ->orWhere('phone_number', $request->input('phone_number'));
                        })->first();
        
        if (!is_null($agent)) {
            return redirect()->back()->with(['message', 'No hp atau kode agen sudah terpakai']);
        }

        $agent = new Agent();
        $agent->unique_code = $request->input('unique_code');
        $agent->type = $request->input('type');
        $agent->name = $request->input('name');
        $agent->phone_number = $request->input('phone_number');
        $agent->is_active = $request->input('is_active');
        $agent->save();
        return redirect('admin/giant/kos/list')->with(['message', 'Agen berhasil dibuat']);
    }
}
