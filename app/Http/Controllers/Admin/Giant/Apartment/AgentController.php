<?php

namespace App\Http\Controllers\Admin\Giant\Apartment;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\Agent\ApartmentAssign;
use Auth;
use Illuminate\Support\Facades\Input;
use App\Entities\Agent\Agent;
use Validator;
use App\Entities\Room\Room;

class AgentController extends Controller
{
    protected $validatorMessage = [
        "agent_id.required" => "Agen tidak boleh kosong",
        "assign_to.required" => "No hp pemilik / pengelola apartemen tidak boleh kosong",
        "is_active.required" => "Pilih salah satu status" 
    ];

    public function __construct()
    {
        \View::share('contentHeader', 'Stories Management');
        \View::share('user' , Auth::user());
    }

    public function assignAgent(Request $request)
    {
        $assign = ApartmentAssign::with(['agent'])->orderBy('id', 'desc')->paginate(20);
        
        $data = [
            "boxTitle" => "Assign to",
            "data" => $assign,
        ];

        return view('admin.contents.giant.apartment.assign', $data);  
    }

    public function assignNew(Request $request)
    {
        $assign = new ApartmentAssign();
        $assign->agent_id = Input::old('agent_id');
        $assign->assign_to = Input::old('assign_to');
        $assign->status   = Input::old('status');
        $agent = Agent::where('type', 'apartment')->get();

        $data = ["data" => $assign, 
                "boxTitle" => "Tambah baru",
                "agent" => $agent,
                "action" => "/admin/giant/apartment/assign/store",
        ];

        return view('admin.contents.giant.apartment.form', $data);
    }

    public function storeData(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "agent_id" => "required",
            "assign_to" => "required",
            "is_active" => "required",
        ], $this->validatorMessage);

        $assign = ApartmentAssign::with(['agent'])->where('assign_to', $request->input('assign_to'))->first();
        $room = Room::where('owner_phone', $request->input('assign_to'))->orWhere('manager_phone', $request->input('assign_to'))->count();
        
        $validator->after(function ($validator) use($assign, $room) {
            if (!is_null($assign)) {
                $validator->errors()->add('message', 'No hp pemilik / manager sudah di assign ke agen '.$assign->agent->name);
            }

            if ($room < 1) {
                $validator->errors()->add('message', 'No pemilik / manager tidak mempunyai apartemen');
            }
        });

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $assignTo = new ApartmentAssign();
        $assignTo->agent_id = $request->input('agent_id');
        $assignTo->assign_to = $request->input('assign_to');
        $assignTo->status = $request->input('is_active');
        $assignTo->save();

        return redirect("/admin/giant/apartment/assign")->with(['message' => "Berhasil menambah data"]);
    }

    public function deleted(Request $request, $id)
    {
        $assign = ApartmentAssign::where('id', $id)->first();
        if (is_null($assign)) {
            return redirect()->back()->with(['error_message' => "Data berhasil di hapus"]);
        }
        $assign->delete();
        return redirect()->back()->with(["message" => "Data berhasil dihapus"]);
    }
}
