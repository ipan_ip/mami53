<?php

namespace App\Http\Controllers\Admin;

use App\Criteria\Consultant\AdminBSECriteria;
use App\Criteria\Consultant\PaginationCriteria;
use App\Entities\Booking\StatusChangedBy;
use App\Entities\Booking\BookingUserRoom;
use App\Entities\Mamipay\MamipayContractKost;
use App\Entities\Room\Room;
use App\Enums\Booking\RefundReason;
use App\Http\Helpers\RegexHelper;
use App\Repositories\UserDataRepositoryEloquent;
use App\Services\Booking\BookingService;
use App\User;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use View;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use RuntimeException;

use App\Entities\Booking\BookingUser;
use App\Entities\Booking\BookingReject;
use App\Repositories\Booking\BookingUserRepository;
use App\Presenters\BookingUserPresenter;

use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayInvoice;
use App\Entities\Mamipay\MamipayTenant;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;

use Notification;
use App\Entities\Notif\SendNotif;
use App\Entities\Notif\NotifData;
use App\Libraries\BookingUserExporter;
use App\Libraries\SMSLibrary;
use App\Http\Helpers\RentCountHelper;
use App\Http\Controllers\Admin\Component\CreationDateFilterTrait;
use App\Http\Controllers\Admin\Component\BookingUserSorterTrait;
use App\Http\Controllers\Admin\Component\BookingUserFilterTrait;
use App\Http\Helpers\BookingNotificationHelper;
use App\Entities\Activity\AppSchemes;
use App\Entities\Consultant\BookingUserRemark;
use App\Entities\Consultant\Consultant;
use App\Http\Helpers\ApiHelper;
use App\Http\Requests\BookingAdminCheckinRequest;
use App\Presenters\Consultant\ConsultantPresenter;
use App\Presenters\Mamipay\MamipayInvoicePresenter;
use App\Repositories\Booking\RejectReason\BookingRejectReasonRepository;
use App\Repositories\Consultant\ConsultantRepository;
use App\Services\Consultant\BookingUserIndexService;
use App\Transformers\Mamipay\Invoice\AdminBookingInvoiceTransformer;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

class BookingUserController extends Controller
{
    use BookingUserSorterTrait, BookingUserFilterTrait, CreationDateFilterTrait;

    const PRICE_ADMIN_TYPE = "admin";
    const URL_VIEWS = 'admin.contents.booking';

    const GENDER_FEMALE = 'female';
    const GENDER_MALE = 'male';
    const GENDER_ALL = 'all';

    const REJECT = 'reject';
    const CANCEL = 'cancel';

    protected $repository;
    protected $repoRejectReason;
    protected $consultantRepository;

    protected $validationMessages = [
        'checkin_date.required'     => 'Check In Date is Required',
        'checkin.date_format'       => 'Check In Date wrong format',
        'checkin_date.after'        => 'Check In Dates must be more than yesterday',
        'checkout_date.required'    => 'Check Out Date is Required',
        'checkout_date.date_format' => 'Check Out Date wrong format',
        'checkout.after'            => 'Check Out date must be later than the Check In date',
        'duration.required'         => 'Duration is Required',
        'duration.numeric'          => 'Duration can only numeric format',
        'tenant_name.required'      => 'Tenant Name is Required',
        'tenant_name.max'           => 'Maximum Tenant Name :max characters',
        'tenant_name.regex'         => 'Names can only contain alphabetical characters',
        'tenant_phone_number.required'  => 'Phone Number is Required',
        'tenant_gender.required'        => 'Gender is Required',
    ];

    public function __construct(BookingUserRepository $repository, BookingRejectReasonRepository $repoRejectReason, ConsultantRepository $consultantRepository)
    {
        $this->middleware(function ($request, $next) {
            if(!Auth::user()->can('access-data-booking')) {
                return redirect('admin');
            }

            return $next($request);
        });

        $this->repository = $repository;
        $this->repoRejectReason = $repoRejectReason;
        $this->consultantRepository = $consultantRepository;

    	View::share('contentHeader', 'Booking Management');
        View::share('user', Auth::user());
    }

    public function index(Request $request)
    {
        $rowsBookingUser = $this->repository->getListBookingFromAdminPage($request);
        $bookingUsers = (new BookingUserPresenter('admin-index'))->present($rowsBookingUser);

        $rejectReason = $this->repoRejectReason->getAllByType(self::REJECT);
        $cancelReason = $this->repoRejectReason->getAllByType(self::CANCEL);

        $statusChangedBy = [];
        $statusChangedBy['all'] = 'All';
        foreach (StatusChangedBy::toArray() as $changedBy)
            $statusChangedBy[$changedBy] = ucfirst($changedBy);

        $viewData = [];
        $viewData['rowsBookingUser']    = $rowsBookingUser;
        $viewData['bookingUsers']       = $bookingUsers['data'];
        $viewData['rejectReasons']      = $rejectReason;
        $viewData['cancelReasons']      = $cancelReason;
        $viewData['boxTitle']           = 'Data Booking';
        $viewData['status']             = ["All", "Active", "Not active"];
        $viewData['statusChangedBy']    = $statusChangedBy;
        $viewData['kostLevels']         = $this->repository->getKostLevelGoldPlusAndOyoFilter();
        $viewData['areaCities']         = request()->input('area_city');
        $viewData['count']              = $this->repository->getListBookingFromAdminPage($request, false, true);

        $viewData['cities'] = Room::whereNotNull('area_city')
            ->where('area_city', '!=', DB::RAW("''"))
            ->where('area_city', '!=', '-')
            ->select('area_city')
            ->distinct()
            ->orderBy('area_city')
            ->get()
            ->pluck('area_city');

        ActivityLog::LogIndex(Action::BOOKING_DATA);

        return view('admin.contents.booking.booking_users', $viewData);
    }

    public function download(Request $request)
    {
        $validator = Validator::make($request->all(), ['from' => 'required|date']);

        $validator->after(function ($validator) use ($request) {
            if ($request->has('from')) {
                $date = trim((string) $request->get('from'));

                if (preg_match('/[\d]{2}\-[\d]{2}-[\d]{4}/i', $date)) {
                    $from = Carbon::createFromFormat("d-m-Y", $date)->startOfDay();
                    if ($from->diffInDays(Carbon::now()) > 3) {
                        $validator->errors()->add(
                            'from',
                            'Tidak bisa download transaksi sebelum 3 hari yang lalu.'
                        );
                    }
                } else {
                    $validator->errors()->add(
                        'from',
                        'Format tanggal tidak sesuai, gunakan date picker atau pastikan format dd-mm-YYYY'
                    );
                }
            }
        });

        if ($validator->fails()) {
            return redirect()->route('admin.booking.users', $request->all())->withErrors($validator); // Redirect with query string
        }

        $bookingUsers = $this->repository->getListBookingFromAdminPage($request, true);
        return Excel::download(new BookingUserExporter($bookingUsers), 'booking.csv');
    }

    public function show(Request $request, $bookingUserId)
    {
        // booking service
        $bookingService = app()->make(BookingService::class);

        $bookingUser = BookingUser::with([
            'user',
            'booking_designer',
            'booking_designer.room',
            'booking_status_changes',
            'revisionables' => function ($query) {
                $query->whereIn('revisionable_type', [
                    'booking_user',
                    'App\Entities\Booking\BookingUser'
                ]);
            },
            'consultantNote',
            'deletedNotes' => function ($query) {
                $query->orderBy('deleted_at', 'desc');
            },
            'invoices' => function ($invoices) {
                $invoices->orderBy('name')->limit(2);
            },
            'invoices.manual_payout',
            'invoices.split_payout_processed_transactions',
            'invoices.contract',
            'invoices.contract.owner_profile',
        ])
            ->where('id', $bookingUserId)
            ->first();

        if (!$bookingUser) {
            return redirect()->back()->with('error_message', 'Booking tidak ditemukan');
        }

        $contract = null;
        $invoices = null;
        $contractTypeKost = null;
        if (!is_null($bookingUser->contract_id)) {
            $contract = MamipayContract::getFromExternalMamipay($bookingUser->contract_id);
            $invoices = $bookingService->getInvoiceByContractIdFromMamipay($bookingUser->contract_id);

            // get mamipay contract type kost
            $mamipayContractTypeKost = MamipayContractKost::with('room_unit')->where('contract_id', $bookingUser->contract_id)->first();
            if (optional($mamipayContractTypeKost)->room_unit) {
                $contractTypeKost = (new BookingUserPresenter('room-unit-list'))->present($mamipayContractTypeKost->room_unit);
                $contractTypeKost = $contractTypeKost['data'] ?? null;
            }
        }

        $transformData = (new BookingUserPresenter('detail'))->present($bookingUser);
        $priceComponents = $transformData['data']['booking_data']['prices'];

        $adminFee = 0;
        $totalFirstInvoice = 0;
        if (!empty($priceComponents)) {
            $totalFirstInvoice = $priceComponents['total']['price_total_string'];

            if (!empty($priceComponents['other'])) {
                foreach ($priceComponents['other'] as $price) {
                    if ($price['price_type'] == $this::PRICE_ADMIN_TYPE) {
                        $adminFee = $price['price_total'];
                    }
                }
            }
        }

        $viewData = [];
        $viewData['bookingUser'] = $transformData['data'];
        $viewData['prices'] = $priceComponents;
        $viewData['totalFirstInvoice'] = $totalFirstInvoice;
        $viewData['tenant'] = is_null($contract) ? null : (is_null($contract->contract_profile) ? null : $contract->contract_profile);
        $viewData['contract'] = is_null($contract) ? null : $contract;
        $viewData['invoices'] = is_null($invoices) ? null : $invoices;
        $viewData['adminFee'] = $adminFee;
        $viewData['revisionables'] = $bookingUser->revisionables;
        $viewData['contractTypeKost'] = $contractTypeKost;
        $viewData['noteHistories'] = $bookingUser->deletedNotes;
        $viewData['transfer_status'] = [];

        if ($bookingUser->invoices->isNotEmpty()) {
            if ($bookingUser->invoices->first()->is_dp) {
                $viewData['transfer_status'] = (new MamipayInvoicePresenter(new AdminBookingInvoiceTransformer()))
                    ->present($bookingUser->invoices)['data'];
            } else {
                $viewData['transfer_status'] = [
                    (new AdminBookingInvoiceTransformer())->transform($bookingUser->invoices->first())
                ];
            }
        }

        return view('admin.contents.booking.booking_users_detail', $viewData);
    }

    /**
     *  Delete note related to booking user if already exist finally create a new note related to booking user
     *
     * @param Request $request
     * @param BookingUser $bookingUser Load BookingUser specified through dependency injection
     * @return mixed
     */
    public function updateNote(Request $request, BookingUser $bookingUser)
    {
        if (is_null(Auth::user()->consultant)) {
            abort(403);
        }

        // Consultant note has soft deletion and we log old note through soft deletion
        if (!is_null($bookingUser->consultantNote)) {
            $bookingUser->consultantNote->delete();
        }

        $bookingUser->consultantNote()->create([
            'content' => $request->input('content'),
            'consultant_id' => Auth::user()->consultant->id,
            'activity' => $request->category
        ]);

        return ApiHelper::responseData();
    }

    public function previewConfirm($bookingUserId) {
        $bookingUser = BookingUser::with(['user', 'booking_designer.room', 'contract'])
                                ->where('id', $bookingUserId)
                                ->first();

        if(!$bookingUser) {
            return redirect()->back()->with('error_message', 'Booking tidak ditemukan');
        }

        $price = null;
        if ($bookingUser->booking_designer) {
            $room = !is_null($bookingUser->booking_designer->room) ? $bookingUser->booking_designer->room : null;
            $price = !is_null($room) ? $bookingUser::getRoomPrice($room, $bookingUser->rent_count_type) : null;
        }

        $viewData = [];
        $contract = $bookingUser->contract;
        if ($contract) {
            $contractInvoice = MamipayInvoice::where('contract_id', $contract->id)->get();
            $viewData['contractInvoice'] = $contractInvoice;
        } else {
            $viewData['contractInvoice'] = null;
        }

        $tenant = MamipayTenant::where('user_id', $bookingUser->user_id)->first();

        $viewData['bookingUser'] = $bookingUser;
        $viewData['rentType'] = RentCountHelper::getFormatRentCount($bookingUser->stay_duration, $bookingUser->rent_count_type);
        $viewData['price'] = $price;
        $viewData['tenant'] = $tenant;
        $viewData['contract'] = $contract;

        return view('admin.contents.booking.confirm_booking_users_detail', $viewData);
    }

    public function confirmAvailability(Request $request, $bookingUserId)
    {
        $bookingUser = BookingUser::find($bookingUserId);

        if(!$bookingUser) {
            return redirect()->back()->with('error_message', 'Booking tidak ditemukan');
        }

        if(!in_array($bookingUser->status, [
                BookingUser::BOOKING_STATUS_BOOKED,
                BookingUser::BOOKING_STATUS_CANCEL_BY_ADMIN
            ])) {
            return redirect()->back()->with('error_message', 'Aksi tidak dapat dilakukan');
        }

        if(Carbon::parse($bookingUser->checkin_date)->diffInDays(Carbon::now(), false) > 0) {
            return redirect()->back()->with('error_message', 'Konfirmasi ketersediaan kamar sudah tidak dapat dilakukan. Tanggal checkin sudah lewat.');
        }

        $booking = $this->repository->newConfirmAvailability($bookingUser, $request->all());

        if ($booking['status'] == false) {
            return redirect()->back()->with('error_message', $booking['message']);
        }

        // Send Notification
        $message = [
            "title" => "Booking Kost Diterima!",
            "message" => "Booking kos diterima! Lakukan pembayaran dalam 1 x 24 jam sebelum booking hangus. Klik disini untuk instruksi pembayaran"
        ];
        $schema = "list_booking";
        $this->sendNotificationToUser($bookingUser->user_id, $message, $schema);

        return redirect('admin/booking/users')->with('message', 'Booking berhasil dikonfirmasi.');

    }

    public function cancelBooking(Request $request, $bookingUserId)
    {
        try {
            $bookingUser = BookingUser::find($bookingUserId);

            if (!$bookingUser) {
                return redirect()->back()->with('error_message', 'Booking tidak ditemukan');
            }

            if (!in_array($bookingUser->status, [
                    BookingUser::BOOKING_STATUS_BOOKED,
                    BookingUser::BOOKING_STATUS_VERIFIED,
                    BookingUser::BOOKING_STATUS_PAID
                ])) {
                return redirect()->back()->with('error_message', 'Aksi tidak dapat dilakukan');
            }

            if (isset($request->other_reason) || $request->cancel_id == BookingReject::OTHER_REASONS) {
                $reason = !empty($request->other_reason) ? $request->other_reason : $request->cancel_id;
            } else {
                $cancelReason = BookingReject::find($request->cancel_id);
                if (!$cancelReason) throw new \Exception('Cancel Reason Not Found');
            }

            $params = [
                'cancel_reason' => $cancelReason->description ?? $reason,
                'booking_reject_reason_id' => $cancelReason->id ?? null,
            ];

            $this->repository->cancelBookingByAdmin($bookingUser, $params);

            // Send Notification
            $template = [
                "title" => "Booking Kost Berhasil Dibatalkan!",
                "message" => "Booking kost dibatalkan, klik disini untuk melihat alasan.",
                "scheme" => AppSchemes::getBookingListPage()
            ];
            $notification = BookingNotificationHelper::getInstance();
            $notification->sendNotif($bookingUser->user_id, $template, $toMamipayOwner = false);

            // Send SMS
            $this->sendSMSToUser($bookingUser->contact_phone, "Booking kost dibatalkan, cek aplikasi mamikos kamu untuk melihat alasan.");

            return redirect()->back()->with('message', 'Booking dibatalkan');

        } catch (\Exception $exception) {
            $exception = new RuntimeException("Failed cancel booking from admin page - ".$exception->getMessage());
            Bugsnag::notifyException($exception);
            return redirect()->back()->with('error_message', 'Failed cancel booking - '.$exception->getMessage());
        }
    }

    public function sendNotificationToUser($userId, $message, $schema)
    {
        $notifData = NotifData::buildFromParam(
            $message['title'],
            $message['message'],
            $schema,
            ''
        );

        $sendNotif = new SendNotif($notifData);
        $sendNotif->setUserIds($userId);
        $sendNotif->send();
    }

    public function sendSMSToUser($userMobilePhoneNumber, $message)
    {
        $isValid = SMSLibrary::validateDomesticIndonesianMobileNumber($userMobilePhoneNumber);
        if ($isValid) {
            SMSLibrary::smsVerification($userMobilePhoneNumber, $message);
        }
    }

    public function rejectBooking(Request $request, $bookingUserId, BookingUserIndexService $bookingUserIndexService)
    {
        try {
            $bookingUser = BookingUser::find($bookingUserId);

            if(!$bookingUser) {
                return redirect()->back()->with('error_message', 'Booking tidak ditemukan');
            }

            if(!in_array($bookingUser->status, [
                    BookingUser::BOOKING_STATUS_BOOKED
                ])) {
                return redirect()->back()->with('error_message', 'Aksi tidak dapat dilakukan');
            }

            if(isset($request->other_reason)){
                $reasons = $request->other_reason;
            } else{
                $rejectReason = BookingReject::find($request->reject_id);
                if(!$rejectReason) throw new \Exception('Reject Reason Not Found');
            }

            $params = [
                'reject_reason' => $rejectReason->description ?? $reasons,
                'booking_reject_reason_id' => $rejectReason->id ?? null,
            ];

            $this->repository->rejectBookingByAdmin($bookingUser, $params);

            // Send Notification
            $template = [
                "title" => "Booking Kost Ditolak!",
                "message" => "Booking kost ditolak, klik disini untuk melihat alasan.",
                "scheme" => AppSchemes::getBookingListPage()
            ];
            $notification = BookingNotificationHelper::getInstance();
            $notification->sendNotif($bookingUser->user_id, $template, $toMamipayOwner = false);

            // Send SMS
            $this->sendSMSToUser($bookingUser->contact_phone, "Booking kost ditolak, cek aplikasi mamikos kamu untuk melihat alasan.");

            return redirect()->back()->with('message', 'Booking ditolak');
        } catch (\Exception $exception) {
            $exception = new RuntimeException("Failed reject booking from admin page - ".$exception->getMessage());
            Bugsnag::notifyException($exception);
            return redirect()->back()->with('error_message', 'Failed reject booking - '.$exception->getMessage());
        }

    }

    public function extendWaitingTime(Request $request, $bookingUserId)
    {
        $bookingUser = BookingUser::find($bookingUserId);

        if(!$bookingUser) {
            return redirect()->back()->with('error_message', 'Booking tidak ditemukan');
        }

        if(!in_array($bookingUser->status, [
                BookingUser::BOOKING_STATUS_BOOKED
            ])) {
            return redirect()->back()->with('error_message', 'Aksi tidak dapat dilakukan');
        }

        $this->repository->extendWaitingTime($bookingUser);

        return redirect()->back()->with('message', 'Waktu tunggu konfirmasi admin berhasil ditambah');
    }

    public function create(Request $request)
    {
        $viewData = [];
        $viewData['boxTitle'] = 'Create Booking';

        ActivityLog::LogCreate(Action::BOOKING_DATA, 'id');

        return view($this::URL_VIEWS.'.booking_users_create', $viewData);
    }

    public function store(Request $request)
    {
        // Validate request
        $validator = Validator::make($request->all(),
            [
                'checkin_date'  => 'required|multi_date_format:Y-m-d,Y-m-j,Y-n-d,Y-n-j|after:yesterday',
                'duration'      => 'required|numeric',
                'tenant_name'   => 'required|max:190|regex:'.RegexHelper::alphaonlyAndSpace(),
                'tenant_gender' => 'required',
                'tenant_email'  => 'required|email',
                'tenant_phone_number' => 'required'
            ], $this->validationMessages);

        if ($validator->fails()) {
            return redirect()->back()->with('error_message', implode(", ",$validator->messages()->all()));
        }

        $room = Room::find($request->room_id);
        if (empty($room)) {
            return redirect()->back()->with('error_message', 'Room not found');
        }

        if (!$room->is_booking) {
            return redirect()->back()->with('error_message', 'Sorry! The booking feature is not active for this room');
        }

        $bookingDesigner = $room->booking_designers()->first();
        if (empty($bookingDesigner)) {
            return redirect()->back()->with('error_message', 'The Room cannot be booked');
        }

        $tenant = User::find($request->tenant_id);
        if (empty($tenant)) {
            return redirect()->back()->with('error_message', 'Data Tenant is Empty');
        }

        // check gender kost
        $gender = $room->gender == 1 ? $this::GENDER_MALE : ($room->gender == 2 ? $this::GENDER_FEMALE : $this::GENDER_ALL);
        if ($gender != $request->tenant_gender && $gender != $this::GENDER_ALL) {
            return redirect()->back()->with('error_message', 'Sorry! The gender doesnt match');
        }

        $bookingUser = new BookingUser();
        $checkUserBookingActive = $bookingUser->bookingUserActive($tenant);
        $existingSameRoomActive = $bookingUser->existingRoomBooked($tenant, $room);

        if ($checkUserBookingActive->count() || $existingSameRoomActive->count()) {
            return redirect()->back()->with('error_message', 'Sorry! Tenants still have active bookings');
        }

        // Check User Mamipay Contract Status Active
        $tenantContractActive = MamipayTenant::tenantContractActive($tenant->id);
        if ($tenantContractActive) {
            return redirect()->back()->with('error_message', 'Sorry! Tenants still have active contract');
        }

        // Check valid Phone number
        $isValidPhoneNumber = SMSLibrary::validateDomesticIndonesianMobileNumber($request->tenant_phone_number);
        if (!$isValidPhoneNumber) {
            return redirect()->back()->with('error_message', 'Sorry! The phone number is not valid');
        }

        // Check available phone number
        $phoneNumberAvailable = $tenant->isPhoneNumberAvailable($request->tenant_phone_number);
        if (!$phoneNumberAvailable) {
            return redirect()->back()->with('error_message', 'Sorry! The phone number already use');
        }

        try {
            $params = [
                'checkin'           => $request->checkin_date,
                'rent_count_type'   => $request->rent_count,
                'duration'          => $request->duration,
                'contact_identity'  => null,
                'contact_name'      => $request->tenant_name,
                'contact_phone'     => $request->tenant_phone_number,
                'contact_job'       => $request->tenant_job,
                'contact_email'     => $request->tenant_email,
                'contact_gender'    => $request->tenant_gender,

                'contact_parent_name'   => !empty($request->tenant_parent_name) ? $request->tenant_parent_name : null,
                'contact_parent_phone'  => !empty($request->tenant_parent_phone) ? $request->tenant_parent_phone : null,

                'song_id' => $room->song_id,
                'session_id' => $request->session_id
            ];

            $bookingUser = $this->repository->newBookRoom($bookingDesigner, $params, $tenant, $room);

            ActivityLog::LogCreate(Action::BOOKING_DATA, $bookingUser->id);

            return redirect('admin/booking/users')->with('message', 'Successfully created booking');
        } catch (\Exception $exception) {
            $exception = new RuntimeException("Failed create booking from admin page - ".$exception->getMessage());
            Bugsnag::notifyException($exception);
            return redirect()->back()->with('error_message', 'Failed create booking - '.$exception->getMessage());
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\View\View
     */
    public function edit($id)
    {
        $booking = BookingUser::find($id);
        if (is_null($booking)) {
            return redirect()->back()->with('error_message', 'Booking not found');
        }

        $bookingUserRoom = BookingUserRoom::where('booking_user_id', $booking->id)->first();
        $roomPrices = $booking->roomPrices();

        $viewData = [];
        $viewData['boxTitle']        = 'Update Booking';
        $viewData['booking']         = $booking;
        $viewData['bookingUserRoom'] = $bookingUserRoom;
        $viewData['roomPrices']      = $roomPrices;

        return view($this::URL_VIEWS.'.booking_users_edit', $viewData);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        // Validate request
        $validator = Validator::make($request->all(),
            [
                'checkin_date'  => 'required|multi_date_format:Y-m-d,Y-m-j,Y-n-d,Y-n-j|after:yesterday',
                'duration'      => 'required|numeric',
                'tenant_name'   => 'required|max:190|regex:'.RegexHelper::alphaonlyAndSpace(),
                'tenant_gender' => 'required',
                'tenant_email'  => 'required',
                'tenant_phone_number' => 'required'
            ], $this->validationMessages);

        if ($validator->fails()) {
            return redirect()->back()->with('error_message', implode(", ",$validator->messages()->all()));
        }

        $bookingUser = BookingUser::find($id);
        if (empty($bookingUser)) {
            return redirect()->back()->with('error_message', 'Data booking tenant not found');
        }

        // check status booking
        if ($bookingUser->status != BookingUser::BOOKING_STATUS_BOOKED) {
            return redirect()->back()->with('error_message', 'Sorry! booking status has changed');
        }

        // check gender kost
        $gender = BookingUser::TENANT_GENDER[$bookingUser->designer->gender];
        if ($gender != $request->tenant_gender && $gender != BookingUser::TENANT_GENDER[0]) {
            return redirect()->back()->with('error_message', 'Sorry! The gender doesnt match');
        }

        // Check valid Phone number
        $isValidPhoneNumber = SMSLibrary::validateDomesticIndonesianMobileNumber($request->tenant_phone_number);
        if (!$isValidPhoneNumber) {
            return redirect()->back()->with('error_message', 'Sorry! The phone number is not valid');
        }

        // Check available phone number
        $phoneNumberAvailable = $bookingUser->user->isPhoneNumberAvailable($request->tenant_phone_number);
        if (!$phoneNumberAvailable) {
            return redirect()->back()->with('error_message', 'Sorry! The phone number already use');
        }

        try {
            $params = [
                'checkin'           => $request->checkin_date,
                'rent_count_type'   => $request->rent_count,
                'duration'          => $request->duration,
                'contact_name'      => $request->tenant_name,
                'contact_phone'     => $request->tenant_phone_number,
                'contact_job'       => $request->tenant_job,
                'contact_email'     => $request->tenant_email,
                'contact_gender'    => $request->tenant_gender,
            ];

            $bookingUser = $this->repository->updateBooking($bookingUser, $params);

            ActivityLog::LogUpdate(Action::BOOKING_DATA, $bookingUser->id);

            return redirect('admin/booking/users')->with('message', 'Booking has been updated');
        } catch (\Exception $exception) {
            $exception = new RuntimeException("Failed update booking from admin page - ".$exception->getMessage());
            Bugsnag::notifyException($exception);
            return redirect()->back()->with('error_message', 'Failed update booking - '.$exception->getMessage());
        }
    }

    public function getRentCount($rentCountType)
    {
        if (empty($rentCountType)) {
            return response()->json([
                'data' => null
            ], 200);
        }

        return response()->json([
            'data' => RentCountHelper::getRentCount($rentCountType)
        ], 200);
    }

    public function getTenants(Request $request, UserDataRepositoryEloquent $userRepository)
    {
        return response()->json($userRepository->getTenants($request->input('search')));
    }

    public function checkIn(BookingAdminCheckinRequest $request)
    {
        $params = $request->validated();
        $bookingUser = $request->getBookingUser();

        if (!$this->repository->checkIn($bookingUser, $params['datetime_checkin'], Auth::user())) {
            return redirect()->back()->with('error_message', 'Gagal Melakukan Check In');
        }

        return redirect()->back()->with('message', 'Berhasil Check In!');
    }

    public function getTenantById($id, UserDataRepositoryEloquent $userRepository)
    {
        return response()->json([
            'data' => $userRepository->getUserById($id)
        ]);
    }

    public function setTransferPermissionStatus(Request $request, $id)
    {
        $validator = Validator::make($request->all(),[
            'refund_reason' => 'enum_value:' . RefundReason::class
        ]);

        if ($validator->fails()) {
            $response = [
                'key' => 'error_message',
                'message' => $validator->errors()
            ];

            return redirect()->back()->with($response['key'], $response['message']);
        }

        try {
            $booking = BookingUser::find($id);
            if (is_null($booking)) {
                $response = [
                    'key' => 'error_message',
                    'message' => 'Set Transfer Permission Status Failed'
                ];
            } else {
                if (in_array($booking->status, [
                        BookingUser::BOOKING_STATUS_VERIFIED,
                        BookingUser::BOOKING_STATUS_PAID,
                        BookingUser::BOOKING_STATUS_REJECTED,
                        BookingUser::BOOKING_STATUS_CHECKED_IN,
                        BookingUser::BOOKING_STATUS_EXPIRED_BY_OWNER,
                        BookingUser::BOOKING_STATUS_CANCEL_BY_ADMIN,
                        BookingUser::BOOKING_STATUS_TERMINATED,
                        BookingUser::BOOKING_STATUS_EXPIRED,
                        BookingUser::BOOKING_STATUS_FINISHED
                ])
                ) {

                    $booking->transfer_permission = $request->input('status');
                    $booking->refund_reason = $request->input('refund_reason', null);
                    $booking->update();

                    $response = [
                        'key' => 'message',
                        'message' => 'Set Transfer Permission Status Success'
                    ];
                } else {
                    $response = [
                        'key' => 'error_message',
                        'message' => 'Booking does not have permission for refund or disburse'
                    ];
                }
            }
        } catch (\Exception $exception) {
            $response = [
                'key' => 'error_message',
                'message' => 'Set Transfer Permission Status Failed: '.$exception->getMessage()
            ];
        }

        return redirect()->back()->with($response['key'], $response['message']);
    }

    /**
     *  Count number of booking with status booked, confirmed, paid and will paid
     *
     *  @param Request $request
     *
     *  @return JsonResponse
     */
    public function countInProcess(Request $request): JsonResponse
    {
        $mappedCities = null;
        if (!empty($request->consultant_id) && ($request->consultant_id !== 'all')) {
            $consultantSelected = Consultant::withTrashed()->with('mapping')->find($request->consultant_id);
            $mappedCities = !is_null($consultantSelected) ? $consultantSelected->mapping->pluck('area_city')->toArray() : null;
        }
        return ApiHelper::responseData([
            'data' => [
                'booked_and_instant_booking' => BookingUser::filterByInstantBooking(true)
                    ->filterByTimespan($request->input('created_at'))
                    ->where('status', BookingUser::BOOKING_STATUS_BOOKED)
                    ->when($mappedCities, function ($query, $mappedCities) {
                        if (!is_null($mappedCities) && count($mappedCities) != 0) {
                            $query->whereHas('room', function ($kost) use ($mappedCities) {
                                $kost->whereIn('area_city', $mappedCities);
                            });
                        }
                    })
                    ->count(),
                'confirmed' => BookingUser::where('status', BookingUser::BOOKING_STATUS_CONFIRMED)
                    ->filterByTimespan($request->input('created_at'))
                    ->when($mappedCities, function ($query, $mappedCities) {
                        if (!is_null($mappedCities) && count($mappedCities) != 0) {
                            $query->whereHas('room', function ($kost) use ($mappedCities) {
                                $kost->whereIn('area_city', $mappedCities);
                            });
                        }
                    })
                    ->count(),
                'paid' => BookingUser::where('status', BookingUser::BOOKING_STATUS_VERIFIED)
                    ->orWhere('status', BookingUser::BOOKING_STATUS_CHECKED_IN)
                    ->filterByTimespan($request->input('created_at'))
                    ->when($mappedCities, function ($query, $mappedCities) {
                        if (!is_null($mappedCities) && count($mappedCities) != 0) {
                            $query->whereHas('room', function ($kost) use ($mappedCities) {
                                $kost->whereIn('area_city', $mappedCities);
                            });
                        }
                    })
                    ->count(),
                'will_pay' => BookingUser::filterByTimespan($request->input('created_at'))
                    ->whereHas('consultantNote', function ($query) {
                        $query->where('activity', BookingUserRemark::WILL_PAY);
                    })
                    ->when($mappedCities, function ($query, $mappedCities) {
                        if (!is_null($mappedCities) && count($mappedCities) != 0) {
                            $query->whereHas('room', function ($kost) use ($mappedCities) {
                                $kost->whereIn('area_city', $mappedCities);
                            });
                        }
                    })
                    ->count()
            ]
        ]);
    }

    public function countOnGoing(Request $request): JsonResponse
    {
        $mappedCities = null;
        if (!empty($request->consultant_id) && ($request->consultant_id !== 'all')) {
            $consultantSelected = Consultant::withTrashed()->with('mapping')->find($request->consultant_id);
            $mappedCities = !is_null($consultantSelected) ? $consultantSelected->mapping->pluck('area_city')->toArray() : null;
        }
        return ApiHelper::responseData([
            'data' => [
                'survey' => BookingUser::filterByTimespan($request->input('created_at'))
                    ->whereHas('consultantNote', function ($query) {
                        $query->where('activity', BookingUserRemark::SURVEY);
                    })
                    ->when($mappedCities, function ($query, $mappedCities) {
                        if (!is_null($mappedCities) && count($mappedCities) != 0) {
                            $query->whereHas('room', function ($kost) use ($mappedCities) {
                                $kost->whereIn('area_city', $mappedCities);
                            });
                        }
                    })
                    ->count(),
                'checked_in' => BookingUser::where('status', BookingUser::BOOKING_STATUS_CHECKED_IN)
                    ->filterByTimespan($request->input('created_at'))
                    ->when($mappedCities, function ($query, $mappedCities) {
                        if (!is_null($mappedCities) && count($mappedCities) != 0) {
                            $query->whereHas('room', function ($kost) use ($mappedCities) {
                                $kost->whereIn('area_city', $mappedCities);
                            });
                        }
                    })
                    ->count(),
                'checked_in_date' => BookingUser::where('status', BookingUser::BOOKING_STATUS_VERIFIED)
                    ->filterByTimespan($request->input('created_at'), 'checkin_date')
                    ->when($mappedCities, function ($query, $mappedCities) {
                        if (!is_null($mappedCities) && count($mappedCities) != 0) {
                            $query->whereHas('room', function ($kost) use ($mappedCities) {
                                $kost->whereIn('area_city', $mappedCities);
                            });
                        }
                    })
                    ->count(),
                'checked_out_date' => BookingUser::where('status', BookingUser::BOOKING_STATUS_VERIFIED)
                    ->filterByTimespan($request->input('created_at'), 'checkout_date')
                    ->when($mappedCities, function ($query, $mappedCities) {
                        if (!is_null($mappedCities) && count($mappedCities) != 0) {
                            $query->whereHas('room', function ($kost) use ($mappedCities) {
                                $kost->whereIn('area_city', $mappedCities);
                            });
                        }
                    })
                    ->count()
            ]
        ]);
    }

    public function countFinished(Request $request): JsonResponse
    {
        $mappedCities = null;
        if (!empty($request->consultant_id) && ($request->consultant_id !== 'all')) {
            $consultantSelected = Consultant::withTrashed()->with('mapping')->find($request->consultant_id);
            $mappedCities = !is_null($consultantSelected) ? $consultantSelected->mapping->pluck('area_city')->toArray() : null;
        }
        return ApiHelper::responseData([
            'data' => [
                'rejected' => BookingUser::where('status', BookingUser::BOOKING_STATUS_REJECTED)
                    ->filterByTimespan($request->input('created_at'))
                    ->when($mappedCities, function ($query, $mappedCities) {
                        if (!is_null($mappedCities) && count($mappedCities) != 0) {
                            $query->whereHas('room', function ($kost) use ($mappedCities) {
                                $kost->whereIn('area_city', $mappedCities);
                            });
                        }
                    })
                    ->count(),
                'cancelled' => BookingUser::where('status', BookingUser::BOOKING_STATUS_CANCELLED)
                    ->filterByTimespan($request->input('created_at'))
                    ->when($mappedCities, function ($query, $mappedCities) {
                        if (!is_null($mappedCities) && count($mappedCities) != 0) {
                            $query->whereHas('room', function ($kost) use ($mappedCities) {
                                $kost->whereIn('area_city', $mappedCities);
                            });
                        }
                    })
                    ->count()
            ]
        ]);
    }

    public function getViewMonitor() {
        return view('admin.contents.booking.booking_users_monitor');
    }

    public function getAdminBSE(Request $request)
    {
        $offset = $request->input('offset', 0);

        $data = $this->consultantRepository
            ->pushCriteria(AdminBSECriteria::class)
            ->pushCriteria(new PaginationCriteria(PaginationCriteria::DEFAULT_LIMIT, $offset))
            ->setPresenter(new ConsultantPresenter(ConsultantPresenter::ADMIN_BSE))
            ->with('mapping')
            ->get()['data'];

        if ($offset == 0) {
            array_unshift(
                $data,
                [
                    'id' => 'all',
                    'name' => 'All',
                    'area_city' => []
                ]
            );
        }

        $count = $this->consultantRepository->pushCriteria(AdminBSECriteria::class)->count();

        $hasMore = ($request->offset + PaginationCriteria::DEFAULT_LIMIT) < $count;

        return response()->json([
            'data' => $data,
            'pagination' => [
                'more' => $hasMore
            ]
        ]);
    }
}
