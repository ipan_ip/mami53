<?php

namespace App\Http\Controllers\Admin\KostValue;

use App\Entities\Level\KostLevelValue;
use App\Entities\Level\KostLevelValueMapping;
use App\Entities\Media\Media;
use App\Entities\Media\MediaHandler;
use App\Http\Controllers\Web\MamikosController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use RuntimeException;
use Validator;

class KostValueController extends MamikosController
{
    protected $title = 'Kost Benefit Management';
    protected $repository;

    public const DEFAULT_LIMIT = 10;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            
            if (!Auth::user()->can('access-kost-level')) {
                return redirect('admin');
            }

            return $next($request);
        });

        View::share('user', Auth::user());
        View::share('contentHeader', $this->title);
    }

    public function index(Request $request)
    {
        $benefits = KostLevelValue::getBenefit(
            self::DEFAULT_LIMIT, $request->all()
        );

        $viewData = [
            'boxTitle' => $this->title,
            'benefits' => $benefits,
        ];

        return view('admin.contents.kost-value.index', $viewData);
    }

    public function create()
    {
        $viewData = ['boxTitle' => 'Add Benefit'];
        return view('admin.contents.kost-value.create', $viewData);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:100',
            'description' => 'required',
            'icon' => 'required',
            'icon_small' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        $benefit = new KostLevelValue;
        $benefit->name = $request->name;
        $benefit->description = $request->description;
        $benefit->created_by = Auth::id();

        if ($request->file('icon')) {
            $icon = $this->upload($request->file('icon'));
            if ($icon) {
                $benefit->image_id = $icon['id'];
            }
        }

        if ($request->file('icon_small')) {
            $icon = $this->upload($request->file('icon_small'));
            if ($icon) {
                $benefit->small_image_id = $icon['id'];
            }
        }

        $benefit->save();

        return redirect()
            ->route('admin.kost-value.index')
            ->with('messages', 'Success Add Benefit');
    }

    public function remove(Request $request)
    {
        if (!$request->filled('id')) {
            new RuntimeException('Failed fetching benefit. Please refresh page and try again.');
        }

        $id = (int) $request->id;
        $benefit = KostLevelValue::find($id);

        if (is_null($benefit)) {
            return [
                'success' => false,
                'message' => 'Benefit is not exist!',
            ];
        }

        $benefit->kost_levels()->detach();
        $benefit->delete();

        return [
            'success' => true,
            'title'   => 'Benefit Successfully Removed!',
            'message' => "Page will be refreshed after you clicked OK",
        ];
    }

    public function edit($id)
    {
        $benefit = KostLevelValue::find($id);
        if (is_null($benefit)) {
            return redirect()
                ->route('admin.kost-value.index')
                ->with('messages', 'Benefit doesnt exist');
        }

        $viewData = [
            'benefit' => $benefit,
            'boxTitle' => 'Edit Benefit',
        ];

        return view('admin.contents.kost-value.edit', $viewData);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:100',
            'description' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        $benefit = KostLevelValue::find($id);
        if (is_null($benefit)) {
            return redirect()
                ->route('admin.kost-value.index')
                ->with('messages', 'Benefit doesnt exist');
        }

        $benefit->name = $request->name;
        $benefit->description = $request->description;
        $benefit->created_by = Auth::id();

        if ($request->file('icon')) {
            $icon = $this->upload($request->file('icon'));
            if ($icon) {
                $benefit->image_id = $icon['id'];
            }
        }

        if ($request->file('icon_small')) {
            $icon = $this->upload($request->file('icon_small'));
            if ($icon) {
                $benefit->small_image_id = $icon['id'];
            }
        }

        $benefit->save();

        return redirect()
            ->route('admin.kost-value.index')
            ->with('messages', 'Success Edit Benefit');
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getValues(Request $request)
    {
        $values = KostLevelValue::select(
            [
                'id',
                'name'
            ]
        )
            ->orderBy('name');

        if ($request->filled('search')) {
            $keyword = '*' . $request->search . '*';
            $values->whereRaw('MATCH(name) AGAINST (? IN BOOLEAN MODE)', $keyword);
        }

        return $values->paginate(10);
    }


    public function upload($file)
    {
        $icon = Media::uploadBenefitIcon(
            $file, 
            MediaHandler::getKostValueIconDirectory(),
            'upload', 
            Auth::id(), 
            'user_id', 
            'user_photo'
        );

        return $icon;
    }
}