<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use View;
use Auth;
use App\Entities\LandingContent\LandingContent;
use App\Entities\LandingContent\LandingContentFile;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;
use Validator;
use App\Http\Helpers\ApiResponse as Api;
use Cache;

class LandingContentController extends Controller
{

    protected $validationMessages = [
        'title.required'=>'Judul harus diisi',
        'title.max'=>'Judul maksimal :max karakter',
        'slug.required'=>'Slug harus diisi',
        'slug.max'=>'Slug maksimal :max karakter',
        'slug.alpha_dash' => 'Slug hanya boleh berisi karakter (a-z), angka (0-9), dash (-), atau underscore (_)',
        'content_open.required' => 'Konten yang dimunculkan harus diisi',
        'content_hidden.required' => 'Konten yang disembunyikan harus diisi',
        'button_label.required' => 'Text tombol "Klik di sini" harus diisi',
        'button_label.max' => 'Text tombol "Klik di sini" maksimal :max karakter',
        'login_subtitle.max' => 'Subtitle login maksimal :max karakter',
        'location_placeholder.max' => 'Placeholder field lokasi maksimal :max karakter'
    ];

    protected $type = [
        'konten', 'loker', 'apartment'
    ];
    
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if(!Auth::user()->can('access-landing-content')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        View::share('contentHeader', 'Landing Content Management');
        View::share('user', Auth::user());
    }

   
    /**
     * Show listing 
     *
     * @param Request   $request
     */
    public function index(Request $request)
    {
        $landingContents = LandingContent::query();


        if($request->filled('q') && $request->get('q') != '') {
            $landingContents = $landingContents->where('slug', 'like', '%' . $request->get('q') . '%');
        } 

        $landingContents = $landingContents->orderByRaw(\DB::raw('IFNULL(redirect_id, 0) asc'))
                            ->orderBy('id', 'desc')
                            ->paginate(20);

        $viewData = array(
            'boxTitle'     => 'Landing Content',
            'landingContents' => $landingContents
        );

        ActivityLog::LogIndex(Action::LANDING_CONTENT);

        return View::make('admin.contents.landing_content.index', $viewData);
    }

    public function create(Request $request)
    {
        $type = $this->type;

        $viewData = [
            'boxTitle'    => 'Tambah Landing Content',
            'typeOptions' => $type
        ];

        return view('admin.contents.landing_content.create', $viewData);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),
                            [
                                'title'=>'required|max:190',
                                'slug'=>'required|max:190|alpha_dash',
                                'content_open'=>'required|min:300',
                                'content_hidden'=>'nullable',
                                'button_label'=>'required|max:100',
                                'login_subtitle'=>'max:250',
                                'location_placeholder' => 'max:100'
                            ], $this->validationMessages);

        $validator->after(function($validator) use ($request) {
            if($request->get('slug') != '') {
                $slugExist = LandingContent::where('slug', $request->get('slug'))->first();

                if($slugExist) {
                    $validator->errors()->add('slug', 'Slug sudah digunakan sebelumnya');
                }
            }
        });

        if($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors())->withInput($request->all());
        }

        $landingContent                       = new LandingContent;
        $landingContent->title                = $request->title;
        $landingContent->slug                 = $request->slug;
        $landingContent->content_open         = preg_replace("/<a(?![^>]+rel)\s/", '<a rel="nofollow" ', $request->content_open);
        $landingContent->content_hidden       = preg_replace("/<a(?![^>]+rel)\s/", '<a rel="nofollow" ', $request->content_hidden);
        $landingContent->button_label         = $request->button_label;
        $landingContent->login_subtitle       = $request->login_subtitle;
        $landingContent->location_placeholder = $request->location_placeholder;
        $landingContent->type                 = $request->type;
        if ($request->need_login == NULL){
            $landingContent->need_login       = 0;
        } else {
            $landingContent->need_login       = $request->need_login;
        }
        $landingContent->save();

        ActivityLog::LogCreate(Action::LANDING_CONTENT, $landingContent->id);

        if(is_array($request->get('files')) && count($request->get('files')) > 0) {
            $landingContentFiles = LandingContentFile::whereIn('id', $request->get('files'))->get();

            $fileNames = $request->get('file_names');

            foreach($landingContentFiles as $key => $file) {
                $file->landing_content_id = $landingContent->id;

                if (isset($fileNames[$key]) && $fileNames[$key] != '') {
                    $file->name = $fileNames[$key];
                }

                $file->save();
            }
        }

        return redirect()->route('admin.landing-content.index')->with('message', 'Sukses menambah landing konten');
    }

    public function edit(Request $request, $id)
    {
        $landingContent = LandingContent::with('files')->find($id);

        if(!$landingContent) {
            return redirect()->back()->with('error_message', 'Data tidak ditemukan');
        }

        $viewData = [
            'boxTitle'       => 'Tambah Landing Content',
            'landingContent' => $landingContent,
            'typeOptions'    => $this->type
        ];

        return view('admin.contents.landing_content.edit', $viewData);
    }

    public function update(Request $request, $id)
    {
        $landingContent = LandingContent::with('files')->find($id);

        if(!$landingContent) {
            return redirect()->back()->with('error_message', 'Data tidak ditemukan');
        }

        $validator = Validator::make($request->all(), 
                            [
                                'title'=>'required|max:190',
                                'slug'=>'required|max:190|alpha_dash',
                                'content_open'=>'required',
                                'content_hidden'=>'nullable',
                                'button_label'=>'required|max:100',
                                'login_subtitle'=>'max:250',
                                'location_placeholder' => 'max:100'
                            ], $this->validationMessages);

        $validator->after(function($query) use ($request, $id) {
            if($request->get('slug') != '') {
                $slugExist = LandingContent::where('slug', $request->get('slug'))
                                        ->where('id', '<>', $id)
                                        ->first();

                if($slugExist) {
                    $validator->errors()->add('slug', 'Slug telah digunakan sebelumnya');
                }
            }
        });

        if($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors())->withInput($request->all());
        }

        Cache::forget('landingcontent-' . $landingContent->type . ':' . $landingContent->slug);

        $landingContent->title = $request->title;
        $landingContent->slug = $request->slug;
        $landingContent->content_open = preg_replace("/<a(?![^>]+rel)\s/", '<a rel="nofollow" ', $request->content_open);
        $landingContent->content_hidden = preg_replace("/<a(?![^>]+rel)\s/", '<a rel="nofollow" ', $request->content_hidden);
        $landingContent->button_label = $request->button_label;
        $landingContent->login_subtitle = $request->login_subtitle;
        $landingContent->location_placeholder = $request->location_placeholder;
        $landingContent->type                 = $request->type;
        if ($request->need_login == NULL){
            $landingContent->need_login       = 0;
        } else {
            $landingContent->need_login       = $request->need_login;
        }
        $landingContent->save();

        if(is_array($request->get('files')) && count($request->get('files')) > 0) {
            $landingContentFiles = LandingContentFile::whereIn('id', $request->get('files'))->get();

            $fileNames = $request->get('file_names');

            foreach($landingContentFiles as $key => $file) {
                $file->landing_content_id = $landingContent->id;

                if(isset($fileNames[$key])) {
                    $file->name = $fileNames[$key] == '' ? null : $fileNames[$key];
                }

                $file->save();
            }

            $originalFiles = $landingContent->files;
            if(count($originalFiles) > 0) {
                foreach($originalFiles as $file) {
                    if(!in_array($file->id, $request->get('files'))) {
                        $file->delete();
                    }
                }
            }
        } else {
            if(count($landingContent->files) > 0) {
                foreach($landingContent->files as $file) {
                    $file->delete();
                }
            }
        }
        ActivityLog::LogUpdate(Action::LANDING_CONTENT, $id);
        return redirect()->route('admin.landing-content.index')->with('message', 'Data berhasil diupdate');
    }

    public function delete($id)
    {
        $content = LandingContent::find($id);

        if(!$content) {
            return redirect()->back()->with('error_message', 'Data tidak ditemukan');
        }

        $content->delete();

        ActivityLog::LogDelete(Action::LANDING_CONTENT, $id);

        return redirect(route('admin.landing-content.index'))->with('message', 'Data berhasil dihapus');
    }

    public function fileUpload(Request $request)
    {
        $file = $request->file('file');

        $uploadedFile = LandingContentFile::upload($file);

        return Api::responseData(compact('uploadedFile'));
    }

    public function redirectlanding(Request $request, $id)
    {
        $landingContent = LandingContent::find($id);

        $viewData = array(
            'boxTitle'        => 'Edit Landing Content Redirect',
            'landingContents' => $landingContent
        );

        return view('admin.contents.landing_content.redirect',$viewData);
    }

    public function redirectlandingpost(Request $request, $id)
    {
        $landingContent = LandingContent::find($id);

        if(is_null($request->get('landing_destination')) || $request->get('landing_destination') == '') {
            $landingContent->redirect_id = NULL;
            $landingContent->save();

            return redirect()->route('admin.landing-content.index')->with('message', 'Redirect Landing Content sudah dibatalkan');
        }

        $landingContentDestination = LandingContent::find($request->get('landing_destination'));

        if(!$landingContentDestination) {
            return redirect()->back()->with('error_message', 'Landing Content tujuan tidak ditemukan');
        }

        Cache::forget('landingcontent-' . $landingContent->type . ':' . $landingContent->slug);

        $landingContent->redirect_id = $landingContentDestination->id;
        $landingContent->save();

        return redirect()->route('admin.landing-content.index')->with('message', 'Landing Content sudah di-redirect');
    }
}
