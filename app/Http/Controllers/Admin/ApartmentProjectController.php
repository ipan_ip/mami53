<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use View;
use App\Entities\Apartment\ApartmentProject;
use App\Entities\Apartment\ApartmentProjectStyle;
use App\Entities\Apartment\ApartmentProjectType;
use App\Entities\Apartment\ApartmentProjectTower;
use Auth;
use Validator;
use App\Entities\Room\Element\Tag;
use App\Repositories\PropertyInputRepository;
use App\Repositories\ApartmentProjectRepositoryEloquent;
use App\Libraries\CSVParser;
use App\Entities\Media\Media;
use App\Entities\Room\Room;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;


class ApartmentProjectController extends Controller
{
	protected $validationMessages = [
		'project_id.required'=>'Project ID harus diisi',
		'name.required'=>'Nama Project harus diisi',
		'name.max'=>'Nama Project Maksimum :max karakter',
		'name.min'=>'Nama Project Maksimum :min karakter',
		'address.max'=>'Alamat maksimal :max karakter',
		'description.max'=>'Deskripsi maksimal :max karakter',
		'latitude.required'=>'Latitude harus diisi',
		'latitude.numeric'=>'Latitude harus berupa angka',
		'longitude.required'=>'Longitude harus diisi',
		'longitude.numeric'=>'Longitude harus berupa angka',
		'subdistrict.max'=>'Subdistrik maksimal :max karakter',
		'area_city.max'=>'Area City maksimal :max karakter',
		'area_city.required'=>'Area City harus diisi',
		'phone_number_building.max'=>'Nomor Telepon Gedung maksimal :max karakter',
		'phone_number_marketing.max'=>'Nomor Telepon Marketing maksimal :max karakter',
		'phone_number_other.max'=>'Nomor Telepon Lainnya maksimal :max karakter',
		'developer.max'=>'Pengembang maksimal :max karakter',
		'unit_count.numeric'=>'Jumlah Unit harus berupa angka',
		'floor_count.numeric'=>'Jumlah Lantai harus berupa angka',
		'size.numeric'=>'Luas harus berupa angka',
		'photo_cover.required'=>'Foto harus diisi'
	];

	protected $projectTypeValidationMessages = [
		'name.required' => 'Nama Tipe harus diisi',
		'name_custom.required_if' => 'Nama Tipe (custom) harus diisi',
		'bedroom_count.required' => 'Jumlah Kamar harus diisi',
		'bedroom_count.numeric' => 'Jumlah Kamar harus berupa angka',
		'bathroom_count.required' => 'Jumlah Kamar Mandi harus diisi',
		'bathroom_count.numeric' => 'Jumlah Kamar Mandi harus berupa angka',
		'room_size.numeric' => 'Luas harus berupa angka'
	];

	protected $projectTowerValidationMessages = [
		'name.required' => 'Nama Tower harus diisi',
		'name.max' => 'Nama Tower maksimal :max karakter',
		'status.max' => 'Status maksimal :max karakter'
	];

	protected $repository;


	public function __construct(PropertyInputRepository $repository)
	{
		$this->middleware(function ($request, $next) {
            if(!Auth::user()->can('access-apartment-project')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

		View::share('contentHeader', 'Apartment Project Management');
        View::share('user', Auth::user());

        $this->repository = $repository;
	}


	public function index(Request $request)
	{
		$query = ApartmentProject::with(['styles', 'styles.photo', 'rooms', 'redirect']);

		if($request->filled('q') && $request->get('q') != '') {
			$query = $query->where('name', 'like', '%' . $request->get('q') . '%');
		}

		$apartmentProjects = $query->orderBy('is_active', 1)->orderBy('created_at', 'desc')->paginate(20);

		$unitCount = [];

		foreach ($apartmentProjects as $apartmentProject) {
			$unitCount [$apartmentProject->id] = 
			[
				'active'   => 0,
				'inActive' => 0
			];
			foreach ($apartmentProject->rooms as $room) {
				if ($room->is_active == "true") {
					$unitCount [$apartmentProject->id] ['active'] +=1;
				} elseif ($room->is_active == "false") {
					$unitCount [$apartmentProject->id] ['inActive'] +=1;
				}
			}

		}

		$viewData = array(
            'boxTitle'        	=> 'List Apartment Project',
            'apartmentProjects'	=> $apartmentProjects,
            'unitCount' 		=> $unitCount
		);
		
		ActivityLog::LogIndex(Action::APARTMENT_PROJECT);

        return view("admin.contents.apartments.projects-index", $viewData);
	}

	public function create(Request $request)
	{
		$tags = Tag::where('type', 'fac_project')->get();

		$viewData = [
			'boxTitle'        	=> 'Create Apartment Project',
			'tags'				=> $tags
		];

		return view('admin.contents.apartments.project-create', $viewData);
	}

	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'name'=>'required|min:5|max:190',
			'address'=>'max:250',
			'description'=>'nullable',
			'latitude'=>'required|numeric',
			'longitude'=>'required|numeric',
			'area_city'=>'required|max:250',
			'subdistrict'=>'max:250',
			'phone_number_building'=>'max:100',
			'phone_number_marketing'=>'max:100',
			'phone_number_other'=>'max:100',
			'developer'=>'max:250',
			'unit_count'=>'nullable|numeric',
			'floor_count'=>'nullable|numeric',
			'size'=>'nullable|numeric',
			'photo_cover'=>'required'
		], $this->validationMessages);


		if($validator->fails()) {
			return redirect()->back()->with('errors', $validator->errors())->withInput($request->all());
		}

		$params = [
			'name'=>$request->name,
			'address'=>$request->address,
			'description'=>$request->description,
			'latitude'=> $request->latitude,
			'longitude'=> $request->longitude,
			'area_city'=>$request->area_city,
			'area_subdistrict'=>$request->subdistrict,
			'tags'=>$request->facilities,
			'phone_number_building'=>$request->phone_number_building,
			'phone_number_marketing'=>$request->phone_number_marketing,
			'phone_number_other'=>$request->phone_number_marketing,
			'developer'=>$request->developer,
			'unit_count'=>$request->unit_count,
			'floors'=>$request->floor_count,
			'size'=>$request->size,
			'photo_cover'=>$request->photo_cover,
			'photo_other'=>$request->photo_other
		];

		$savedId = $this->repository->saveNewApartmentProjectByAdmin($params);

		ActivityLog::LogCreate(Action::APARTMENT_PROJECT, $savedId);

		return redirect(route('admin.apartment.project.index'))->with('message', 'Apartment Project berhasil ditambahkan');
	}

	public function edit(Request $request, $id)
	{
		$apartmentProject = ApartmentProject::with(['styles', 'styles.photo'])->find($id);

		$tags = Tag::where('type', 'fac_project')->get();

		$styles = $apartmentProject->styles()->get();
		$photoCover = null;
		$photoOthers = [];
		foreach($styles as $style) {
			if(strpos($style->title, 'cover') !== FALSE) {
				$photoCover = $style->photo;
			} else {
				$photoOthers[] = $style->photo;
			}
		}

		$viewData = [
			'boxTitle'        	=> 'Edit Apartment Project',
			'apartmentProject'	=> $apartmentProject,
			'projectTags'		=> $apartmentProject->tags->pluck('id')->toArray(),
			// 'projectPhoto'		=> !is_null($apartmentProject->styles->first()) ? $apartmentProject->styles->first()->photo : '',
			'photoCover'		=> $photoCover,
			'photoOthers'		=> $photoOthers,
			'tags'				=> $tags
		];

		return view('admin.contents.apartments.project-update', $viewData);
	}

	public function update(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'project_id'=>'required',
			'name'=>'required|min:5|max:190',
			'address'=>'max:250',
			'description'=>'nullable',
			'latitude'=>'required|numeric',
			'longitude'=>'required|numeric',
			'area_city'=>'required|max:250',
			'subdistrict'=>'max:250',
			'phone_number_building'=>'max:100',
			'phone_number_marketing'=>'max:100',
			'phone_number_other'=>'max:100',
			'developer'=>'max:250',
			'unit_count'=>'nullable|numeric',
			'floor_count'=>'nullable|numeric',
			'size'=>'nullable|numeric',
			'photo_cover'=>'required'
		], $this->validationMessages);


		if($validator->fails()) {
			return redirect()->back()->with('errors', $validator->errors())->withInput($request->all());
		}

		$apartmentProject = ApartmentProject::find($request->project_id);

		$params = [
			'name'=>$request->name,
			'address'=>$request->address,
			'description'=>$request->description,
			'latitude'=> $request->latitude,
			'longitude'=> $request->longitude,
			'area_city'=>$request->area_city,
			'area_subdistrict'=>$request->subdistrict,
			'tags'=>$request->facilities,
			'phone_number_building'=>$request->phone_number_building,
			'phone_number_marketing'=>$request->phone_number_marketing,
			'phone_number_other'=>$request->phone_number_marketing,
			'developer'=>$request->developer,
			'unit_count'=>$request->unit_count,
			'floors'=>$request->floor_count,
			'size'=>$request->size,
			'photo_cover'=>$request->photo_cover,
			'photo_other'=>$request->photo_other
		];

		$this->repository->updateApartmentProjectByAdmin($apartmentProject, $params);

		ActivityLog::LogUpdate(Action::APARTMENT_PROJECT, $apartmentProject->id);

		return redirect(route('admin.apartment.project.index'))->with('message', 'Apartment Project berhasil diupdate');
	}

	public function activate(Request $request, $id)
	{
		if(!Auth::user()->can('access-apartment-project-activation')) return redirect ('/admin');

		$apartmentProject = ApartmentProject::find($id);

		if(!$apartmentProject) {
			return redirect()->back()->with('error_message', 'Data tidak ditemukan');
		}

		$apartmentProjectRepository = new ApartmentProjectRepositoryEloquent(app());

		$apartmentProjectRepository->activate($apartmentProject);

		return redirect()->back()->with('message', 'Apartemen berhasil diaktifkan');
	}

	public function deactivate(Request $request, $id)
	{
		if(!Auth::user()->can('access-apartment-project-activation')) return redirect ('/admin');

		$apartmentProject = ApartmentProject::find($id);

		if(!$apartmentProject) {
			return redirect()->back()->with('error_message', 'Data tidak ditemukan');
		}

		$apartmentProjectRepository = new ApartmentProjectRepositoryEloquent(app());

		$apartmentProjectRepository->deactivate($apartmentProject);

		return redirect()->back()->with('message', 'Apartemen berhasil di-nonaktifkan');
	}

	public function listType(Request $request, $apartmentProjectId)
	{
		if(!Auth::user()->can('access-apartment-project-type')) return redirect ('/admin');

		$apartmentProject = ApartmentProject::find($apartmentProjectId);

		if(!$apartmentProject) {
			return redirect()->route('admin.apartment.project.index');
		}

		$apartmentProjectTypes = ApartmentProjectType::where('apartment_project_id', $apartmentProjectId)
													->get();

		$viewData = array(
            'boxTitle'        		=> 'List Apartment Project Type (' . $apartmentProject->name . ')',
            'apartmentProject'		=> $apartmentProject,
            'apartmentProjectTypes'	=> $apartmentProjectTypes,
        );

        return view("admin.contents.apartments.project-types", $viewData);
	}

	public function createType(Request $request, $apartmentProjectId)
	{
		$apartmentProject = ApartmentProject::find($apartmentProjectId);

		if(!$apartmentProject) {
			return redirect()->route('admin.apartment.project.index');
		}

		$typeOptions = [
			'1-Room Studio' => ['bedroom'=>1, 'bathroom'=>1],
			'1 BR' => ['bedroom'=>1, 'bathroom'=>1],
			'2 BR' => ['bedroom'=>2, 'bathroom'=>1],
			'3 BR' => ['bedroom'=>3, 'bathroom'=>2],
			'4 BR' => ['bedroom'=>4, 'bathroom'=>2],
			'Lainnya' => ['bedroom'=>'', 'bathroom'=>'']
		];

		$viewData = array(
            'boxTitle'        	=> 'Tambah Apartment Project Type',
            'apartmentProject'	=> $apartmentProject,
            'typeOptions'		=> $typeOptions
        );

        return view("admin.contents.apartments.project-types-create", $viewData);
	}

	public function storeType(Request $request, $apartmentProjectId)
	{
		$apartmentProject = ApartmentProject::find($apartmentProjectId);

		if(!$apartmentProject) {
			return redirect()->route('admin.apartment.project.index');
		}

		$validator = Validator::make($request->all(),
						[
							'name'=>'required',
							'name_custom'=> 'required_if:name,Lainnya',
							'bedroom_count' => 'required|numeric',
							'bathroom_count' => 'required|numeric',
							'room_size' => 'numeric'
						], $this->projectTypeValidationMessages);

		if($validator->fails()) {
			return redirect()->back()->with('errors', $validator->errors())->withInput();
		}

		$apartmentProjectType = new ApartmentProjectType;
		$apartmentProjectType->apartment_project_id = $apartmentProject->id;
		$apartmentProjectType->type_name = $request->get('name') != 'Lainnya' ? $request->get('name') : $request->get('name_custom');
		$apartmentProjectType->bedroom_count = $request->get('bedroom_count');
		$apartmentProjectType->bathroom_count = $request->get('bathroom_count');
		$apartmentProjectType->size = $request->get('room_size');
		$apartmentProjectType->save();

		return redirect()->route('admin.apartment.project.type.list', $apartmentProjectId);
	}

	public function editType(Request $request, $typeId)
	{
		$apartmentProjectType = ApartmentProjectType::find($typeId);

		if(!$apartmentProjectType) {
			return redirect()->back()->with('error_message', 'Tipe tidak ditemukan');
		}

		$typeOptions = [
			'1-Room Studio' => ['bedroom'=>1, 'bathroom'=>1],
			'1 BR' => ['bedroom'=>1, 'bathroom'=>1],
			'2 BR' => ['bedroom'=>2, 'bathroom'=>1],
			'3 BR' => ['bedroom'=>3, 'bathroom'=>2],
			'4 BR' => ['bedroom'=>4, 'bathroom'=>2],
			'Lainnya' => ['bedroom'=>'', 'bathroom'=>'']
		];

		$viewData = array(
            'boxTitle'        	=> 'Edit Apartment Project Type',
            'apartmentProject'	=> $apartmentProjectType->apartment_project,
            'apartmentProjectType'	=> $apartmentProjectType,
            'typeOptions'		=> $typeOptions
        );

        return view("admin.contents.apartments.project-types-edit", $viewData);
	}

	public function updateType(Request $request, $typeId)
	{
		$apartmentProjectType = ApartmentProjectType::find($typeId);

		if(!$apartmentProjectType) {
			return redirect()->back()->with('error_message', 'Tipe tidak ditemukan');
		}

		$validator = Validator::make($request->all(),
						[
							'name'=>'required',
							'name_custom'=> 'required_if:name,Lainnya',
							'bedroom_count' => 'required|numeric',
							'bathroom_count' => 'required|numeric',
							'room_size' => 'numeric'
						], $this->projectTypeValidationMessages);

		if($validator->fails()) {
			return redirect()->back()->with('errors', $validator->errors())->withInput();
		}

		$apartmentProjectType->type_name = $request->get('name') != 'Lainnya' ? $request->get('name') : $request->get('name_custom');
		$apartmentProjectType->bedroom_count = $request->get('bedroom_count');
		$apartmentProjectType->bathroom_count = $request->get('bathroom_count');
		$apartmentProjectType->size = $request->get('room_size');
		$apartmentProjectType->save();

		return redirect()->route('admin.apartment.project.type.list', $apartmentProjectType->apartment_project_id);
	}

	public function destroyType(Request $request, $typeId)
	{
		$apartmentProjectType = ApartmentProjectType::find($typeId);

		if(!$apartmentProjectType) {
			return redirect()->back()->with('error_message', 'Tipe tidak ditemukan');
		}

		$apartmentProjectId = $apartmentProjectType->apartment_project_id;

		$apartmentProjectType->delete();

		return redirect()->route('admin.apartment.project.type.list', $apartmentProjectId);
	}

	public function listTower(Request $request, $apartmentProjectId)
	{
		if(!Auth::user()->can('access-apartment-project-tower')) return redirect ('/admin');

		$apartmentProject = ApartmentProject::find($apartmentProjectId);

		if(!$apartmentProject) {
			return redirect()->route('admin.apartment.project.index');
		}

		$apartmentProjectTowers = ApartmentProjectTower::where('apartment_project_id', $apartmentProjectId)
													->get();

		$viewData = array(
            'boxTitle'        		=> 'List Apartment Project Tower',
            'apartmentProject'		=> $apartmentProject,
            'apartmentProjectTowers'	=> $apartmentProjectTowers,
        );

        return view("admin.contents.apartments.project-towers", $viewData);
	}

	public function createTower(Request $request, $apartmentProjectId)
	{
		$apartmentProject = ApartmentProject::find($apartmentProjectId);

		if(!$apartmentProject) {
			return redirect()->route('admin.apartment.project.index');
		}

		$viewData = array(
            'boxTitle'        	=> 'Tambah Apartment Project Type',
            'apartmentProject'	=> $apartmentProject,
        );

        return view("admin.contents.apartments.project-towers-create", $viewData);
	}

	public function storeTower(Request $request, $apartmentProjectId)
	{
		$apartmentProject = ApartmentProject::find($apartmentProjectId);

		if(!$apartmentProject) {
			return redirect()->route('admin.apartment.project.index');
		}

		$validator = Validator::make($request->all(),
						[
							'name'=>'required|max:190',
							'status' => 'max:250',
						], $this->projectTowerValidationMessages);

		if($validator->fails()) {
			return redirect()->back()->with('errors', $validator->errors());
		}

		$apartmentProjectTower = new ApartmentProjectTower;
		$apartmentProjectTower->apartment_project_id = $apartmentProject->id;
		$apartmentProjectTower->name = $request->get('name');
		$apartmentProjectTower->status = $request->get('status');
		$apartmentProjectTower->save();

		return redirect()->route('admin.apartment.project.tower.list', $apartmentProjectId);
	}

	public function editTower(Request $request, $towerId)
	{
		$apartmentProjectTower = ApartmentProjectTower::find($towerId);

		if(!$apartmentProjectTower) {
			return redirect()->back()->with('error_message', 'Tower tidak ditemukan');
		}

		$viewData = array(
            'boxTitle'        	=> 'Edit Apartment Project Tower',
            'apartmentProject'	=> $apartmentProjectTower->apartment_project,
            'apartmentProjectTower'	=> $apartmentProjectTower
        );

        return view("admin.contents.apartments.project-towers-edit", $viewData);
	}

	public function updateTower(Request $request, $towerId)
	{
		$apartmentProjectTower = ApartmentProjectTower::find($towerId);

		if(!$apartmentProjectTower) {
			return redirect()->back()->with('error_message', 'Tower tidak ditemukan');
		}

		$validator = Validator::make($request->all(),
						[
							'name'=>'required|max:190',
							'status' => 'max:250',
						], $this->projectTowerValidationMessages);

		if($validator->fails()) {
			return redirect()->back()->with('errors', $validator->errors());
		}

		$apartmentProjectTower->name = $request->get('name');
		$apartmentProjectTower->status = $request->get('status');
		$apartmentProjectTower->save();

		return redirect()->route('admin.apartment.project.tower.list', $apartmentProjectTower->apartment_project_id);
	}

	public function destroyTower(Request $request, $towerId)
	{
		$apartmentProjectTower = ApartmentProjectTower::find($towerId);

		if(!$apartmentProjectTower) {
			return redirect()->back()->with('error_message', 'Tower tidak ditemukan');
		}

		$apartmentProjectId = $apartmentProjectTower->apartment_project_id;

		$apartmentProjectTower->delete();

		return redirect()->route('admin.apartment.project.tower.list', $apartmentProjectId);
	}

	public function parse(Request $request)
	{
		$viewData   = [
            'boxTitle'          => 'Parse Apartment Project CSV',
        ];

        return view('admin.contents.apartments.parser', $viewData);
	}

	public function parsePost(Request $request)
	{
		$this->validate($request, [
            'file_csv'=>'required|mimes:csv,txt'
        ]);

        $availableTags = Tag::where('type', 'fac_project')->get();
        $tagMapping = $availableTags->pluck('id', 'name')->toArray();
        $tagMapping = array_change_key_case($tagMapping, CASE_LOWER);

        $csv = $request->file_csv->path();

        $csvArray = CSVParser::csv_parse($csv, ['eol'=>"\r\n"]);

        $failedRow = [];
        foreach($csvArray as $key=>$row) {
        	$slug = $row['slug'];

        	$existingLanding = ApartmentProject::where('slug', $slug)
                                    ->orWhere('project_code', $row['project_code'])
                                    ->get();

            if(count($existingLanding) > 0) {
            	$failedRow[] = 'Gagal input karena slug atau project code telah digunakan sebelumnya : ' . $row['name'];
                continue;
            }

        	try {
        		$apartmentProject = new ApartmentProject;
	            $apartmentProject->name = $row['name'];
	            $apartmentProject->project_code = $row['project_code'];
	            $apartmentProject->slug = $slug;
	            $apartmentProject->address = $row['address'];
	            $apartmentProject->description = $row['description'];
	            $apartmentProject->latitude = $row['latitude'];
	            $apartmentProject->longitude = $row['longitude'];
	            $apartmentProject->area_city = $row['area_city'];
	            $apartmentProject->area_subdistrict = $row['area_subdistrict'];
	            $apartmentProject->phone_number_building = $row['phone_number_building'];
	            $apartmentProject->phone_number_marketing = $row['phone_number_marketing'];
	            $apartmentProject->phone_number_other = $row['phone_number_other'];
	            $apartmentProject->developer = $row['developer'];
	            $apartmentProject->floor = $row['floors'];
	            $apartmentProject->unit_count = $row['unit_count'];
	            $apartmentProject->size = $row['size'];
	            $apartmentProject->is_active = 0;
	            $apartmentProject->save();

	            $explodedFacilities = $row['facilities'] != '' ? explode('|', $row['facilities']) : [];
	            $tagIds = [];
	            if(!empty($explodedFacilities)) {
	                foreach($explodedFacilities as $facility) {
	                    $lowerCasedFacility = strtolower($facility);
	                    if(isset($tagMapping[$lowerCasedFacility])) {
	                        $tagIds[] = $tagMapping[$lowerCasedFacility];
	                    }
	                }
	            }

	            $apartmentProject->tags()->sync($tagIds);

        	} catch (\Exception $e) {
        		$failedRow[] = 'Gagal diimport : ' . $row['name'] ;
        	}
            
        }

        return redirect()->back()
                        ->with(!empty($failedRow) ? 'errors' : 'message', 
                            !empty($failedRow) ? collect($failedRow) : 'Sukses input data');
	}

	/**
	 * Simple Parser, just name and slug
	 */
	public function parseSimple(Request $request)
	{
		$viewData   = [
            'boxTitle'          => 'Parse Apartment Project CSV (Simple Version)',
        ];

        return view('admin.contents.apartments.parser-simple', $viewData);
	}

	/**
	 *
	 */
	public function parseSimplePost(Request $request)
	{
		$this->validate($request, [
            'file_csv'=>'required|mimes:csv,txt'
        ]);

        $csv = $request->file_csv->path();

        $csvArray = CSVParser::csv_parse($csv, ['eol'=>"\r\n"]);

        $failedRow = [];
        foreach($csvArray as $key=>$row) {
        	$slug = $row['slug'];

        	$existingLanding = ApartmentProject::where('slug', $slug)->get();

            if(count($existingLanding) > 0) {
            	$failedRow[] = 'Gagal input karena slug telah digunakan sebelumnya : ' . $row['name'];
                continue;
            }

        	try {
        		$apartmentProject = new ApartmentProject;
	            $apartmentProject->name = $row['name'];
	            $apartmentProject->slug = $slug;
	            $apartmentProject->is_active = 0;
	            $apartmentProject->save();

	            $apartmentProject->generateApartmentCode();

        	} catch (\Exception $e) {
        		$failedRow[] = 'Gagal diimport : ' . $row['name'] ;
        	}
            
        }

        return redirect()->back()
                        ->with(!empty($failedRow) ? 'errors' : 'message', 
                            !empty($failedRow) ? collect($failedRow) : 'Sukses input data');
	}


	public function parseType(Request $request)
	{
		$viewData   = [
            'boxTitle'          => 'Parse Apartment Project Type CSV',
        ];

        return view('admin.contents.apartments.parser-type', $viewData);
	}

	public function parseTypePost(Request $request)
	{
		$this->validate($request, [
            'file_csv'=>'required|mimes:csv,txt'
        ]);

        $csv = $request->file_csv->path();

        $csvArray = CSVParser::csv_parse($csv, ['eol'=>"\r\n"]);

        $failedRow = [];
        foreach($csvArray as $key=>$row) {
        	$apartmentProject = ApartmentProject::where('name', $row['project_name'])
        										->first();

        	if(!$apartmentProject) {
        		$failedRow[] = 'Gagal input, apartemen tidak ditemukan : ' . $row['project_name'];
                continue;
        	}

        	if($row['name'] == '' || is_null($row['name'])) {
        		$failedRow[] = 'Gagal input, Nama tipe kosong : ' . $row['project_name'];
                continue;
        	}

        	try {
	        	$apartmentProjectType = new ApartmentProjectType;
	        	$apartmentProjectType->type_name = $row['name'];
	        	$apartmentProjectType->bedroom_count = $row['bedroom'];
	        	$apartmentProjectType->bathroom_count = $row['bathroom'];
	        	$apartmentProjectType->size = $row['size'] == '' || is_null($row['size']) ? NULL : $row['size'];
	        	$apartmentProjectType->apartment_project_id = $apartmentProject->id;
	        	$apartmentProjectType->save();
        	} catch(\Exception $e) {
        		$failedRow[] = 'Gagal diimport : ' . $row['project_name'] ;
        	}
        }

        return redirect()->back()
                        ->with(!empty($failedRow) ? 'errors' : 'message', 
                            !empty($failedRow) ? collect($failedRow) : 'Sukses input data');
	}

	public function parseTower(Request $request)
	{
		$viewData   = [
            'boxTitle'          => 'Parse Apartment Project Tower CSV',
        ];

        return view('admin.contents.apartments.parser-tower', $viewData);
	}

	public function parseTowerPost(Request $request)
	{
		$this->validate($request, [
            'file_csv'=>'required|mimes:csv,txt'
        ]);

        $csv = $request->file_csv->path();

        $csvArray = CSVParser::csv_parse($csv, ['eol'=>"\r\n"]);

        $failedRow = [];
        foreach($csvArray as $key=>$row) {
        	$apartmentProject = ApartmentProject::where('name', $row['project_name'])
        										->first();

        	if(!$apartmentProject) {
        		$failedRow[] = 'Gagal input, apartemen tidak ditemukan : ' . $row['project_name'];
                continue;
        	}

        	if($row['name'] == '' || is_null($row['name'])) {
        		$failedRow[] = 'Gagal input, Nama tower kosong : ' . $row['project_name'];
                continue;
        	}

        	try {
	        	$apartmentProjectTower = new ApartmentProjectTower;
	        	$apartmentProjectTower->name = $row['name'];
	        	$apartmentProjectTower->status = $row['status'];
	        	$apartmentProjectTower->apartment_project_id = $apartmentProject->id;
	        	$apartmentProjectTower->save();
        	} catch(\Exception $e) {
        		$failedRow[] = 'Gagal diimport : ' . $row['project_name'] ;
        	}
        }

        return redirect()->back()
                        ->with(!empty($failedRow) ? 'errors' : 'message', 
                            !empty($failedRow) ? collect($failedRow) : 'Sukses input data');
	}

	public function redirectLanding($apartmentProjectId)
    {
        $listApartmentProjets = ApartmentProject::whereNull('redirect_id')->get();

	    $viewData = [
            'boxTitle'        => 'Edit Landing Apartment Project Redirect',
            'apartmentId'     => $apartmentProjectId,
            'listApartment'   => $listApartmentProjets
        ];

        return view('admin.contents.apartments.redirect-landing', $viewData);

    }

    public function saveRedirectLanding(Request $request)
    {
        try {
            $apartmentProject = ApartmentProject::find($request->input('apartment_id'));
            $apartmentProject->redirect_id = $request->input('apartment_project_landing') != '' ? $request->get('apartment_project_landing') : null;
            $apartmentProject->save();

        } catch (\Exception $e) {
            $failedRow[] = 'Failed input redirect landing : ' . $apartmentProject['name'] ;

        }

        return redirect()->back()
            ->with(!empty($failedRow) ? 'errors' : 'message',
                !empty($failedRow) ? collect($failedRow) : 'Success input redirect landing');
    }

}