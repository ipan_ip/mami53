<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Activity\Action;
use App\Entities\Activity\ActivityLog;
use App\Entities\Event\EventModel;
use App\Entities\Notif\Category;
use App\Entities\User\Notification;
use App\Events\NotificationAdded;
use App\Http\Controllers\Controller;
use App\Http\Helpers\ApiHelper;
use App\Http\Helpers\ApiResponse;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;
use Event;

class EventController extends Controller
{
    const ITEM_PER_PAGE = 10;

    protected $rules = [
        'admin.event.store' => [
            'title'       => 'required|min:3|max:50',
            'description' => 'required|min:3',
        ],
        'admin.event.update' => [
            'title'       => 'required|min:3|max:50',
            'description' => 'required|min:3',
        ]
    ];

    protected $indexRoute      = 'admin.event.index';
    protected $createRoute     = 'admin.event.create';
    protected $editRoute       = 'admin.event.edit';
    protected $updateRoute     = 'admin.event.update';
    protected $publishRoute    = 'admin.event.publish';
    protected $deleteRoute     = 'admin.event.destroy';
    protected $redirectBrowser = ['No', 'Yes'];

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!Auth::user()->can('access-event')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        View::share('contentHeader', 'Event Management');
        View::share('user', Auth::user());
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $rowsEvents = EventModel::getEventBanner(
            (int) self::ITEM_PER_PAGE, $request->all()
        );

        $viewData = [
            'boxTitle'     => 'Lists Events',
            'createRoute'  => $this->createRoute,
            'editRoute'    => $this->editRoute,
            'publishRoute' => $this->publishRoute,
            'deleteRoute'  => $this->deleteRoute,
            'rowsEvents'   => $rowsEvents,
            'searchUrl'    => URL::route('admin.event.search'),
        ];

        ActivityLog::LogIndex(Action::EVENT);

        return View::make('admin.contents.event.index', $viewData);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $rowEvent = new EventModel;

        $rowEvent->title            = Input::old('title');
        $rowEvent->description      = Input::old('description');
        $rowEvent->image_url        = Input::old('media1');
        $rowEvent->action_url       = Input::old('action_url');
        $rowEvent->scheme_url       = Input::old('scheme_url');
        $rowEvent->ordering         = Input::old('ordering');
        $rowEvent->is_owner         = Input::old('is_owner');
        $rowEvent->is_published     = Input::old('is_published');
        $rowEvent->category_id      = Input::old('category_id');
        $rowEvent->photo_id         = Input::old('photo_id');
        $rowEvent->area_big         = Input::old('photo_big');
        $rowEvent->area_city        = Input::old('area_city');
        $rowEvent->area_subdistrict = Input::old('area_subdistrict');
        $rowEvent->is_broadcast     = Input::old('is_broadcast');
        $rowEvent->redirect_browser = Input::old('redirect_browser');
        // $rowEvent->use_scheme       = Input::old('use_scheme');

        $count = EventModel::where('is_published', 1)->count();
        $categories = Category::orderBy('order', 'asc')->get();

        $viewData = [
            'boxTitle'        => 'Insert Event',
            'rowEvent'        => $rowEvent,
            'photo'           => null,
            'formAction'      => 'admin.event.store',
            'formMethod'      => 'POST',
            'formCancel'      => URL::route($this->indexRoute),
            'isOwner'        => EventModel::IS_OWNER_OPTION,
            'categories'      => $categories,
            'ordering'        => $count,
            'buttonText'      => 'Submit',
            'redirectBrowser' => $this->redirectBrowser,
            'isOwnerOption' => EventModel::IS_OWNER_YES
        ];
        return View::make('admin.contents.event.form', $viewData);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title'  => 'required',
        ]);

        if (!filter_var(Input::get('action_url'), FILTER_VALIDATE_URL)) {
            return  Redirect::route('admin.event.create')
                ->with('error_message', 'URL tidak valid')
                ->withInput();
        }
        $url = ApiHelper::cleanHttpChecking(Input::get('action_url')); 

        $eventDetail = [
            'title'            => Input::get('title'),
            'description'      => Input::get('description'),
            'action_url'       => $url,
            'scheme_url'       => Input::get('scheme_url'),
            'ordering'         => Input::get('ordering'),
            'is_owner'         => Input::get('is_owner'),
            'is_published'     => !is_null(Input::get('auto_live')) ? Input::get('auto_live') : 0,
            'category_id'      => Input::get('category_id'),
            'photo_id'         => Input::get('photo_id') != '' ? Input::get('photo_id') : null, 
            'area_big'         => Input::get('area_big') != '' ? Input::get('area_big') : null, 
            'area_city'        => Input::get('area_city') != '' ? Input::get('area_city') : null, 
            'area_subdistrict' => Input::get('area_subdistrict') != '' ? Input::get('area_subdistrict') : null,
            'is_broadcast'     => Input::get('is_broadcast'),
            'redirect_browser' => Input::get('redirect_browser'),
            // 'use_scheme'       => Input::get('use_scheme')
        ];

        $isSuccess = EventModel::add($eventDetail);

        ActivityLog::LogCreate(Action::EVENT, $isSuccess->id);

        if ($isSuccess) {
            $this->fireAddNotificationEvent($isSuccess);

            return redirect()->route('admin.event.index')
                ->with('message', 'New Event Created' . ($eventDetail['is_published'] == 1 ? ' and Published' : ''));
        } else {
            return Redirect::route('admin.event.create')
                ->with('error_message', 'Aborted')
                ->withInput();
        }
    }

    /**
     * @param $id
     * @return View
     */
    public function show($id) // unused in view?
    {
        $event = EventModel::findOrFail($id);

        if ($event->photo_id != null) {
            $photos = EventModel::showPhotos($event);
        } else {
            $photos = null;
        }

        $count = EventModel::where('is_published', 1)->count();

        $viewData = array(
            'boxTitle'   => 'Upload Event Photos',
            'event'      => $event,
            'photos'     => $photos,
            'formAction' => 'admin.event.update',
            'ordering'   => $count,
            'formMethod' => 'PUT'
        );
        return view('admin.contents.event.show', $viewData);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $rowEvent = EventModel::find($id);
        if (is_null($rowEvent)) {
            return back()->input('error_message', 'Event tidak ditemukan');
        }

        $count = EventModel::where('is_published', 1)->count();
        $categories = Category::orderBy('order', 'asc')->get();

        $isOwnerOption = $rowEvent->is_owner;
        if ($rowEvent->is_owner == EventModel::IS_OWNER_YES && $rowEvent->condition == EventModel::OWNER_BOOKING_CONDITION) {
            $isOwnerOption = EventModel::IS_OWNER_YES_BOOKING;
        }

        $viewData = [
            'boxTitle'        => 'View or Edit Event',
            'rowEvent'        => $rowEvent,
            'photo'           => $rowEvent->photo,
            'formAction'      => ['admin.event.update', $id],
            'formMethod'      => 'PUT',
            'formCancel'      => $this->indexRoute,
            'categories'      => $categories,
            'isOwner'        => EventModel::IS_OWNER_OPTION,
            'ordering'        => $count,
            'buttonText'      => 'Update',
            'redirectBrowser' => $this->redirectBrowser,
            'isOwnerOption' => $isOwnerOption
        ];
        return View::make('admin.contents.event.form', $viewData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title'  => 'required',
        ]);

        if (!filter_var(Input::get('action_url'), FILTER_VALIDATE_URL)) {
            return  back()->with('error_message', 'URL tidak valid');
        }
        $url = ApiHelper::cleanHttpChecking(Input::get('action_url')); 

        $event = EventModel::find($id);
        if (is_null($event)) {
            return back()->with('error_message', 'Event tidak ditemukan');
        }

        $event->title            = Input::get('title');
        $event->description      = Input::get('description');
        $event->action_url       = $url != '' ? $url : null;
        $event->scheme_url       = Input::get('scheme_url') != '' ? Input::get('scheme_url') : null;
        $event->ordering         = Input::get('ordering');
        
        $isOwnerEvent = Input::get('is_owner');
        if ($event['is_owner'] == EventModel::IS_OWNER_YES_BOOKING) {
            $isOwnerEvent = EventModel::IS_OWNER_YES;
            $event->condition = EventModel::OWNER_BOOKING_CONDITION;
        } else {
            $event->condition = null;
        }
        
        $event->is_owner         = $isOwnerEvent;
        $event->is_published     = !is_null(Input::get('auto_live')) ? Input::get('auto_live') : 0;
        $event->category_id      = Input::get('category_id');
        $event->photo_id         = Input::get('photo_id') != '' ? Input::get('photo_id') : null;
        $event->area_big         = Input::get('area_big') != '' ? Input::get('area_big') : null;
        $event->area_city        = Input::get('area_city') != '' ? Input::get('area_city') : null;
        $event->area_subdistrict = Input::get('area_subdistrict') ? Input::get('area_subdistrict') : null;
        $event->is_broadcast     = Input::get('is_broadcast');
        $event->redirect_browser = Input::get('redirect_browser');
        // $event->use_scheme       = Input::get('use_scheme');

        $isSuccess = $event->save();

        if ($isSuccess) {
            ActivityLog::LogUpdate(Action::EVENT, $id);
            return  Redirect::route('admin.event.index')
                ->with('message', 'Event Updated');
        } else {
            return  Redirect::route('admin.event.edit', $event->id)
                ->with('error_message', 'Failed to update')
                ->withInput();
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public function publish($id)
    {
        $event = EventModel::find($id);
        if (is_null($event)) {
            return  Redirect::route('admin.event.index')
                ->with('error_message', 'Oops, something went wrong')
                ->withInput();
        }

        $isSuccess = '';
        if ($event->is_published == 0) {
            $event->is_published = 1;
            $isSuccess = 'published';
            
            $this->fireAddNotificationEvent($event);
        } else {
            $event->is_published = 0;
            $isSuccess = 'unpublished';
        }
        $event->save();

        if ($isSuccess == 'published') {
            return  Redirect::route('admin.event.index')
                ->with('message', 'Event Published');
        } else {
            return  Redirect::route('admin.event.index')
                ->with('message', 'Event unpublished')
                ->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $event = EventModel::find($id);
        $event->delete();

        (new Notification)->deleteEvent($id);

        ActivityLog::LogDelete(Action::EVENT, $id);

        return  Redirect::route('admin.event.index')
            ->with('message', 'Event deleted');
    }

    public function apiIndex() // unused in view?
    {
        $events = EventModel::get(['id', 'title']);

        $eventsArr = [];
        foreach ($events as $event) {
            $eventsArr[] = [
                'id' => $event->id,
                'name' => $event->title,
            ];
        }

        $rowsEvent = json_decode(json_encode($eventsArr));
        $rowsEventView = mySelectPreprocessing(
            'single',
            'selected',
            'key-value',
            $rowsEvent,
            null
        );

        $data = array(
            'event' => $rowsEventView
        );
        return ApiResponse::responseData($data);
    }

    private function fireAddNotificationEvent(EventModel $event) {
        if ($event->is_owner == '1') {
            $notificationEvent = [
                'owner_id'    => null,
                'type'        => 'event_owner',
                'designer_id' => null,
                'identifier'  => $event->id 
            ];

            Event::fire(new NotificationAdded($notificationEvent));
        }
    }
}
