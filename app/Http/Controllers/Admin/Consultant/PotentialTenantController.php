<?php

namespace App\Http\Controllers\Admin\Consultant;

use App\Entities\Consultant\PotentialTenant;
use App\Entities\User\UserRole;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\DB;

class PotentialTenantController extends Controller
{
    public function __construct()
    {
        $this->middleware(
            function ($request, $next) {
                if (!\Auth::user()->can('access-consultant') || (!isset(Auth::user()->consultant) && !Auth::user()->can(
                            'access-all-consultant-potential-tenant'
                        ))) {
                    return redirect('admin');
                }

                return $next($request);
            }
        );

        \View::share('user', \Auth::user());
        \View::share('contentHeader', 'Potential Tenant');
    }

    public function index()
    {
        $viewData = [
            'boxTitle' => 'Potential Tenants',
            'searchUrl' => route('admin.consultant.potential-tenant.index'),
        ];

        return view('admin.contents.consultant.potential-tenant-index', $viewData);
    }

    public function data(Request $request)
    {
        $potentialTenants = PotentialTenant::leftJoin(
            'user',
            function ($join) {
                $join->on('user.phone_number', 'consultant_tools_potential_tenant.phone_number')
                    ->where('user.is_owner', 'false')
                    ->where('user.role', UserRole::User)
                    ->whereNull('user.deleted_at');
            }
        )
            ->leftJoin('designer', 'consultant_tools_potential_tenant.designer_id', 'designer.id')
            ->leftJoin('designer_owner', 'designer.id', 'designer_owner.designer_id')
            ->leftJoin('user as owner', 'owner.id', 'designer_owner.user_id')
            ->select(
                'consultant_tools_potential_tenant.id',
                'consultant_tools_potential_tenant.name',
                'consultant_tools_potential_tenant.phone_number',
                'designer.song_id as property_id',
                DB::RAW('IFNULL(designer.name,consultant_tools_potential_tenant.property_name) as property_name'),
                DB::RAW('IFNULL(designer.area_city,"") as area_city'),
                'consultant_tools_potential_tenant.email',
                'consultant_tools_potential_tenant.gender',
                'job_information',
                'consultant_tools_potential_tenant.due_date',
                'consultant_tools_potential_tenant.price',
                'owner.name as owner_name',
                'owner.phone_number as owner_phone',
                DB::RAW('IF(user.id is null,false,true) as registered')
            );

        if (Auth::user()->can('access-all-consultant-potential-tenant')) {
            $potentialTenants = $potentialTenants->orderBy('consultant_tools_potential_tenant.updated_at', 'desc')
                ->orderBy('consultant_tools_potential_tenant.id', 'desc');
        } else {
            $potentialTenants = $potentialTenants->where('consultant_id', Auth::user()->consultant->id)
                ->orderBy('consultant_tools_potential_tenant.updated_at', 'desc')
                ->orderBy('consultant_tools_potential_tenant.id', 'desc');
        }

        if ($request->has('search')) {
            $potentialTenants = $potentialTenants->where(
                'consultant_tools_potential_tenant.name',
                'LIKE',
                '%' . $request->input('search') . '%'
            );
        }
        $total = $potentialTenants->count();

        $offset = $request->has('offset') ? $request->input('offset') : 0;
        $limit = $request->has('limit') ? $request->input('limit') : 10;

        $potentialTenants = $potentialTenants->offset($offset)
            ->limit($limit)
            ->get();

        $potentialTenants->map(
            function ($data) {
                $data->active_contract = $data->hasActiveContract();

                return $data;
            }
        );

        return response()->json(
            [
                'total' => $total,
                'rows' => $potentialTenants
            ]
        );
    }

}
