<?php

namespace App\Http\Controllers\Admin\Consultant;

use App\Http\Helpers\ApiResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;

use App\Entities\Consultant\Consultant;
use App\Entities\Consultant\ConsultantMapping;
use App\Entities\Room\Room;
use App\Repositories\UserDataRepository;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;
use Illuminate\Support\Facades\Validator;

class ConsultantMappingController extends Controller
{
    public function __construct(UserDataRepository $userRepository)
    {
        $this->middleware(
            function ($request, $next) {
                if (!\Auth::user()->can('access-consultant')) {
                    return redirect('admin');
                }

                return $next($request);
            }
        );

        $this->userRepository = $userRepository;

        \View::share('user', \Auth::user());
        \View::share('contentHeader', 'Consultant Mapping');
    }

    public function index(Request $request)
    {
        $query = ConsultantMapping::with(['consultant' => function ($consultant) {
            $consultant->withTrashed();
        }]);

        if ($request->filled('name')) {
            $query->whereHas(
                'consultant.user',
                function ($u) use ($request) {
                    $u->where('name', 'LIKE', '%' . $request->name . '%');
                }
            );
        }

        if ($request->filled('city')) {
            $query->where('area_city', 'LIKE', '%' . $request->city . '%');
        }

        $consultantMapping = $query->paginate(20);
        $cities = Room::select('area_city')
            ->whereNotNull('area_city')
            ->where('area_city', '!=', '')
            ->where('area_city', '!=', '-')
            ->distinct('area_city')
            ->get();

        $viewData = [
            'cities' => $cities,
            'mappings' => $consultantMapping,
            'boxTitle' => 'Consultant Mapping',
            'searchUrl' => route('admin.consultant.mapping.index'),
        ];

        // Activity Log
        ActivityLog::LogIndex(Action::CONSULTANT_MAPPING);

        return view('admin.contents.consultant.mapping-index', $viewData);
    }

    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'city' => 'required',
                'consultant_id' => 'required',
                'is_mamirooms' => 'required'
            ]
        );

        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        $consultant = Consultant::find($request->consultant_id);

        if (is_null($consultant)) {
            return redirect()->back()->with('error_message', "Sorry, the data consultant not found");
        }

        try {
            $cities = $request->input('city');
            $isMamirooms = $request->input('is_mamirooms');

            $existings = ConsultantMapping::where('consultant_id', $consultant->id)
                ->whereIn('area_city', $cities)
                ->get();
            $toCreate = [];

            if ($existings->count() < 10) {
                foreach ($cities as $city) {
                    foreach ($isMamirooms as $isMamiroom) {
                        $exists = $existings->where('area_city', $city)
                            ->where('is_mamirooms', $isMamiroom)
                            ->isNotEmpty();

                        if (!$exists) {
                            $toCreate[] = [
                                'area_city' => $city,
                                'is_mamirooms' => $isMamiroom
                            ];
                        }
                    }
                }

                $consultant->mapping()->createMany($toCreate)->each(function ($mapping, $key) {
                    ActivityLog::LogCreate(Action::CONSULTANT_MAPPING, $mapping->id);
                });
            }

            $consultant->refreshCache();

            return redirect()->back()->with('message', 'Successfully created');
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);

            return redirect()->back()->withInput()->with('errors', "Failed add consultant mapping");
        }
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'city' => 'required',
            ]
        );

        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }


        $consultantMapping = ConsultantMapping::find($id);

        if (is_null($consultantMapping)) {
            return redirect()->back()->with('error_message', "Sorry, the data mapping not found");
        }

//        $consultant = $consultantMapping->consultant;
//
//        if($consultant->roles()->where('role',ConsultantRole::ADMIN_BSE)->count() > 0 && count($consultant->mapping) >= 5){
//            return redirect()->back()->with('error_message', "Sorry, the consultant already have 5 or more cities mapped");
//        }

        $isMamirooms = isset($request->is_mamirooms) ? true : false;
        $existingMapping = ConsultantMapping::where('id', $id)
            ->where('is_mamirooms', $isMamirooms)
            ->where('area_city', $request->input('city'))
            ->count();

        if ($existingMapping > 0) {
            return redirect()->back()->with('error_message', "Sorry, the data consultant already registered");
        }

        try {
            $consultantMapping->area_city = $request->city;
            $consultantMapping->is_mamirooms = $isMamirooms;
            $consultantMapping->save();

            $consultant = new Consultant();
            $consultant->refreshCache();

            // Activity Log
            ActivityLog::LogUpdate(Action::CONSULTANT_MAPPING, $id);

            return redirect()->back()->with('message', 'Successfully update consultant mapping');
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);

            return redirect()->back()->withInput()->with('error_message', "Failed update consultant mapping");
        }
    }

    public function destroy($id)
    {
        $consultantMapping = ConsultantMapping::find($id);
        if (is_null($consultantMapping)) {
            return ApiResponse::responseData(
                [
                    'status' => false
                ]
            );
        }

        $consultantMapping->delete();

        // Activity Log
        ActivityLog::LogDelete(Action::CONSULTANT_MAPPING, $id);

        return ApiResponse::responseData(
            [
                'status' => true
            ]
        );
    }
}