<?php

namespace App\Http\Controllers\Admin\Consultant;

use App\Criteria\Consultant\PaginationCriteria;
use App\Criteria\Consultant\RoleCriteria;
use App\Criteria\Consultant\SearchByNameCriteria;
use App\Entities\Consultant\Consultant;
use App\Entities\Consultant\SalesMotion\SalesMotion;
use App\Entities\Consultant\SalesMotion\SalesMotionAssignee;
use App\Http\Helpers\ApiResponse;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Helpers\ApiHelper;
use App\Presenters\Consultant\ConsultantPresenter;
use App\Presenters\Consultant\SalesMotion\SalesMotionAssigneePresenter;
use App\Presenters\Consultant\SalesMotion\SalesMotionPresenter;
use App\Repositories\Consultant\ConsultantRepository;
use App\Repositories\Consultant\SalesMotion\SalesMotionAssigneeRepository;
use App\Repositories\Consultant\SalesMotion\SalesMotionRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\View\View;

class SalesMotionController extends Controller
{
    /**
     *  Instance of sales motion repository
     *
     * @var SalesMotionRepository
     */
    protected $salesMotionRepository;

    /**
     *  Instance of sales motion assignee repository
     *
     * @var SalesMotionAssigneeRepository
     */
    protected $salesMotionAssigneeRepository;

    /**
     * Instance of ConsultantRepository
     *
     * @var ConsultantRepository
     */
    protected $consultantRepository;

    public function __construct(
        SalesMotionRepository $salesMotionRepository,
        SalesMotionAssigneeRepository $salesMotionAssigneeRepository,
        ConsultantRepository $consultantRepository
    ) {
        \View::share('user', \Auth::user());
        \View::share('contentHeader', 'Consultants Sales Motion');

        $this->salesMotionRepository = $salesMotionRepository;
        $this->salesMotionAssigneeRepository = $salesMotionAssigneeRepository;
        $this->consultantRepository = $consultantRepository;
    }


    /**
     *  Return view sales motion
     *
     *  @return View
     */
    public function index(): View
    {
        return view('admin.contents.consultant.sales-motion.index');
    }

    /**
     *  Show data for index of sales motion
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function indexData(Request $request): JsonResponse
    {
        $count = $this->salesMotionRepository
            ->pushCriteria(
                new SearchByNameCriteria($request->input('q', null))
            )
            ->count();

        $data = $this->salesMotionRepository
            ->setPresenter(new SalesMotionPresenter(SalesMotionPresenter::PRESENTER_TYPE_ADMIN_LIST))
            ->pushCriteria(
                new SearchByNameCriteria($request->input('q', null))
            )
            ->pushCriteria(
                new PaginationCriteria(
                    $request->input('limit', PaginationCriteria::DEFAULT_LIMIT),
                    $request->input('offset', PaginationCriteria::DEFAULT_OFFSET)
                )
            )
            ->orderBy('updated_at', 'desc')
            ->with('lastUpdatedBy')
            ->get();

        return ApiHelper::responseData(
            [
                'total' => $count,
                'sales_motions' => $data['data']
            ]
        );
    }

    /**
     *  Return view sales motion detail
     *
     *  @return View
     */
    public function detail($salesMotionId): View
    {
        return view('admin.contents.consultant.sales-motion.detail', compact('salesMotionId'));
    }

    /**
     *  Get sales motion detail and assignee data
     *
     *  @param Request $request
     *  @param $salesMotionId Id of the sales motion
     *
     *  @return JsonResponse
     */
    public function detailData(Request $request, $salesMotionId): JsonResponse
    {
        try {
            $salesMotion = $this->salesMotionRepository
                ->with([
                    'lastUpdatedBy',
                    'createdBy'
                ])
                ->setPresenter(
                    new SalesMotionPresenter(SalesMotionPresenter::PRESENTER_TYPE_ADMIN_DETAIL)
                )
                ->find($salesMotionId);

            $assigneeCount = $this->salesMotionAssigneeRepository
                ->where('consultant_sales_motion_id', $salesMotionId)
                ->count();

            $assignees = $this->salesMotionAssigneeRepository
                ->setPresenter(
                    new SalesMotionAssigneePresenter(SalesMotionAssigneePresenter::PRESENTER_TYPE_ADMIN_LIST)
                )
                ->pushCriteria(
                    new PaginationCriteria(
                        $request->input('limit', PaginationCriteria::DEFAULT_LIMIT),
                        $request->input('offset', PaginationCriteria::DEFAULT_OFFSET)
                    )
                )
                ->with([
                    'consultant'
                ])
                ->findByField('consultant_sales_motion_id', $salesMotionId);

            return ApiHelper::responseData([
                'sales_motion' => $salesMotion['data'],
                'total' => $assigneeCount,
                'assignees' => $assignees['data']
            ]);
        } catch (ModelNotFoundException $e) {
            return ApiHelper::responseError(__('api.exception.not_found'), false, 404);
        }
    }

    /**
     *  Return view sales motion add
     *
     *  @return View
     */
    public function add(): View
    {
        return view('admin.contents.consultant.sales-motion.add');
    }

    /**
     *  Return view sales motion edit
     *
     *  @return View
     */
    public function edit($salesMotionId): View
    {
        return view('admin.contents.consultant.sales-motion.edit', compact('salesMotionId'));
    }

    /**
     *  Sales motion update assignee page
     *
     *  @param int $salesMotionId Id of sales motion to assign to consultant
     *
     *  @return View
     */
    public function assigneeUpdate(int $salesMotionId): View
    {
        return view('admin.contents.consultant.sales-motion.assignee', compact('salesMotionId'));
    }

    /**
     *  Assign sales motion to all consultant
     *
     *  @param Request
     *
     *  @param SalesMotion $salesMotion Sales motion to assign
     *
     *  @return mixed
     */
    public function assignAll(Request $request, SalesMotion $salesMotion)
    {
        $request->validate([
            'role' => 'required|string|in:admin,supply,demand'
        ]);

        $selectedRole = $request->role;

        Consultant::whereHas('roles', function ($roles) use ($selectedRole) {
            $roles->where('role', $selectedRole);
        })->chunk(100, function ($consultants) use ($salesMotion) {
            foreach ($consultants as $consultant) {
                SalesMotionAssignee::firstOrCreate([
                    'consultant_id' => $consultant->id,
                    'consultant_sales_motion_id' => $salesMotion->id
                ]);
            }
        });

        return response()->redirectToRoute('admin.consultant.sales-motion.index');
    }

    /**
     *  Show all consultants and indicate if they are assigned to the sales motion
     *
     * @param Request $request
     * @param int $salesMotionId
     *
     * @return JsonResponse
     */
    public function listConsultant(Request $request, int $salesMotionId): JsonResponse
    {
        $count = $this->consultantRepository
            ->pushCriteria(
                new RoleCriteria($request->input('role', null))
            )
            ->count();

        $consultants = $this->consultantRepository
            ->pushCriteria(
                new RoleCriteria($request->input('role', null))
            )
            ->pushCriteria(
                new PaginationCriteria(
                    $request->input('limit', PaginationCriteria::DEFAULT_LIMIT),
                    $request->input('offset', PaginationCriteria::DEFAULT_OFFSET)
                )
            )
            ->setPresenter(
                new ConsultantPresenter(ConsultantPresenter::SALES_MOTION_LIST)
            )
            ->with(
                [
                    'roles',
                    'sales_motions' => function ($salesMotion) use ($salesMotionId) {
                        $salesMotion->where('id', $salesMotionId);
                    }
                ]
            )
            ->get();

        return ApiHelper::responseData(
            [
                'total' => $count,
                'consultants' => $consultants['data']
            ]
        );
    }

    public function listAssignedConsultant(Request $request, int $salesMotionId): JsonResponse
    {
        $assigneeIds = $this->salesMotionAssigneeRepository
            ->where('consultant_sales_motion_id', $salesMotionId)
            ->select('consultant_id')
            ->get()
            ->pluck('consultant_id')
            ->toArray();

        return ApiHelper::responseData(
            [
                'assignee_ids' => $assigneeIds
            ]
        );
    }

    /**
     *  Update status of sales motion to active and update it's due date
     *
     * @param SalesMotion $salesMotion
     * @param Request $request
     */
    public function activate(Request $request, SalesMotion $salesMotion): JsonResponse
    {
        $validator = Validator::make(
            $request->all(),
            [
                'end_date' => 'required|date'
            ]
        );

        if ($validator->fails()) {
            return ApiResponse::responseData(
                [
                    'status' => false,
                    'message' => $validator->errors()
                ]
            );
        }

        $input = $request->input();
        $input['start_date'] = Carbon::now()->format('Y-m-d');

        $this->salesMotionRepository->activateSalesMotion($salesMotion, $input);

        return ApiResponse::responseData(
            [
                'status' => true,
                'message' => 'Sales motion successfully activated'
            ]
        );
    }

    /**
     *  Update status of sales motion to not activate
     *
     * @param SalesMotion $salesMotion
     */
    public function deactivate(SalesMotion $salesMotion)
    {
        $this->salesMotionRepository->deactivateSalesMotion($salesMotion);

        return ApiResponse::responseData(
            [
                'status' => true,
                'message' => 'Sales motion successfully deactivated'
            ]
        );
    }

    /**
     *  Store new sales motion detail
     *
     * @param Request $request
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'name' => 'required|string',
                'type' => [
                    'required',
                    Rule::in(
                        [
                            SalesMotion::TYPE_CAMPAIGN,
                            SalesMotion::TYPE_FEATURE,
                            SalesMotion::TYPE_PRODUCT,
                            SalesMotion::TYPE_PROMOTION
                        ]
                    )
                ],
                'created_by_division' => [
                    'required',
                    Rule::in([SalesMotion::DIVISION_COMMERCIAL, SalesMotion::DIVISION_MARKETING])
                ],
                'start_date' => 'required|date',
                'end_date' => 'required|date',
                'objective' => 'string',
                'terms_and_condition' => 'string',
                'requirement' => 'string',
                'benefit' => 'string',
                'max_participant' => 'string',
                'url' => 'url',
                'media_id' => 'nullable|integer|exists:media,id'
            ]
        );

        if ($validator->fails()) {
            return ApiResponse::responseData(
                [
                    'status' => false,
                    'message' => $validator->errors()
                ]
            );
        }

        $input = $request->input();
        $input['created_by'] = Auth::id();
        if (Carbon::now() >= Carbon::createFromFormat('Y-m-d', $input['start_date']) && Carbon::now(
            ) <= Carbon::createFromFormat('Y-m-d', $input['end_date'])) {
            $input['is_active'] = true;
        } else {
            $input['is_active'] = false;
        }

        $salesMotion = $this->salesMotionRepository->create($input);

        return ApiResponse::responseData(
            [
                'status' => true,
                'sales_motion_id' => $salesMotion->id
            ]
        );
    }

    /**
     *  Update sales motion
     *
     * @param Request $request
     * @param $salesMotionId
     * 
     * @return JsonResponse
     */
    public function update(Request $request, $salesMotionId): JsonResponse
    {
        $validator = Validator::make(
            $request->all(),
            [
                'name' => 'required|string',
                'type' => [
                    'required',
                    Rule::in(
                        [
                            SalesMotion::TYPE_CAMPAIGN,
                            SalesMotion::TYPE_FEATURE,
                            SalesMotion::TYPE_PRODUCT,
                            SalesMotion::TYPE_PROMOTION
                        ]
                    )
                ],
                'created_by_division' => [
                    'required',
                    Rule::in([SalesMotion::DIVISION_COMMERCIAL, SalesMotion::DIVISION_MARKETING])
                ],
                'end_date' => 'required|date',
                'objective' => 'string',
                'terms_and_condition' => 'string',
                'requirement' => 'string',
                'benefit' => 'string',
                'max_participant' => 'string',
                'url' => 'url',
                'media_id' => 'nullable|integer|exists:media,id',
                'is_active' => 'required|boolean'
            ]
        );

        if ($validator->fails()) {
            return ApiResponse::responseData(
                [
                    'status' => false,
                    'message' => $validator->errors()
                ]
            );
        }

        $input = $request->except(['start_date']);
        $input['updated_by'] = Auth::id();

        $salesMotion = $this->salesMotionRepository->update($input, $salesMotionId);

        return ApiResponse::responseData(
            [
                'status' => true,
                'message' => 'Successfully updated',
                'sales_motion_id' => $salesMotion->id
            ]
        );
    }

    /**
     *  Set assignee of sales motion
     *
     * @param SalesMotion $salesMotion
     * @param Request $request
     */

    public function syncAssignee(SalesMotion $salesMotion, Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'consultants' => 'required|array',
                'consultants.*' => 'required|exists:consultant,id'
            ]
        );

        if ($validator->fails()) {
            return ApiResponse::responseData(
                [
                    'status' => false,
                    'message' => $validator->errors()
                ]
            );
        }

        $salesMotion->consultants()->sync($request->input('consultants'));

        return ApiResponse::responseData(
            [
                'status' => true,
                'message' => 'Assignee successfully updated'
            ]
        );
    }
}
