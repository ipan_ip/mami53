<?php

namespace App\Http\Controllers\Admin\Consultant;

use App\Criteria\Consultant\PaginationCriteria;
use App\Entities\Consultant\PotentialProperty;
use App\Http\Controllers\Controller;
use App\Http\Helpers\ApiHelper;
use App\Http\Requests\Admin\Consultant\CreateOrUpdatePotentialPropertyRequest;
use App\Http\Requests\Admin\Consultant\PatchPotentialPropertyRequest;
use App\Repositories\Consultant\PotentialOwnerRepository;
use App\Repositories\Consultant\PotentialPropertyRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class PotentialPropertyController extends Controller
{
    /**
     *  Instance of PotentialPropertyRepository
     *
     *  @var PotentialPropertyRepository
     */
    protected $potentialPropertyRepository;

    /**
     *  Instance of PotentialOwnerRepository
     *
     *  @var PotentialOwnerRepository
     */
    protected $potentialOwnerRepository;

    public function __construct(
        PotentialPropertyRepository $potentialPropertyRepository,
        PotentialOwnerRepository $potentialOwnerRepository
    ) {
        $this->potentialPropertyRepository = $potentialPropertyRepository;
        $this->potentialOwnerRepository = $potentialOwnerRepository;
        \View::share('contentHeader', 'Potential Property');
    }

    /**
     *  Get all potential property owned by a potential owner
     *
     *  @param Request $request
     *  @param mixed $ownerId
     *
     *  @return JsonResponse
     */
    public function getPropertyByOwner(Request $request, $ownerId): JsonResponse
    {
        $count = $this->potentialPropertyRepository
            ->where('consultant_potential_owner_id', $ownerId)
            ->count();
        $data = $this->potentialPropertyRepository
            ->with('photo')
            ->with('room.level')
            ->where('consultant_potential_owner_id', $ownerId)
            ->get()
            ->map(function ($item, $key) {
                $itemArr = $item->toArray();
                $itemArr['photo_small'] = $item->getPhotoUrl();
                $itemArr['offered_product'] = $item->extractOfferedProduct();
                $itemArr['kost_url'] = $item->getKostUrl();

                return $itemArr;
            });

        return ApiHelper::responseData([
            'total' => $count,
            'properties' => $data
        ]);
    }


    /**
     *  Create potential property for $ownerId
     *
     *  @param CreateOrUpdatePotentialPropertyRequest $request
     *  @param mixed $ownerId Potential owner that own the property
     *
     *  @return JsonResponse
     */
    public function store(CreateOrUpdatePotentialPropertyRequest $request, $ownerId): JsonResponse
    {
        try {
            $owner = $this->potentialOwnerRepository
                ->find($ownerId);

            if ($request->isValidationFailed()) {
                return ApiHelper::responseData(
                    ['messages' => $request->failedValidator->errors()],
                    false,
                    false,
                    501
                );
            }

            $consultant = Auth::user()->consultant;
            $property = $request->only([
                'address',
                'area_city',
                'booking_status',
                'followup_status',
                'is_priority',
                'media_id',
                'name',
                'offered_product',
                'potential_gp_room_list',
                'potential_gp_total_room',
                'province',
                'remark',
                'total_room',
            ]);
            $property['consultant_potential_owner_id'] = $ownerId;
            $property['consultant_id'] = $consultant->id;
            $property['created_by'] = Auth::id();
            $property['offered_product'] = implode(',', $property['offered_product']);
            $property = $this->potentialPropertyRepository->create($property);

            $owner->total_property += 1;
            $owner->total_room += (!empty($request->total_room) ? $request->total_room : 0);
            $owner->updated_by = Auth::id();
            $owner->save();

            $property->offered_product = $property->extractOfferedProduct();
            return ApiHelper::responseData(['property' => $property->toArray()]);
        } catch (ModelNotFoundException $e) {
            return ApiHelper::responseError(
                'The potential owner does not exist',
                false,
                404
            );
        }
    }

    /**
     *  Update some fields of potential property
     *
     *  @param PatchPotentialPropertyRequest $request
     *  @param mixed $propertyId Id of potential property to ypdate
     *
     *  @return JsonResponse
     */
    public function update(PatchPotentialPropertyRequest $request, $propertyId): JsonResponse
    {
        try {
            if ($request->isValidationFailed()) {
                return ApiHelper::responseData(
                    ['messages' => $request->failedValidator->errors()],
                    false,
                    false,
                    501
                );
            }

            // raise error if property not not found.
            $this->potentialPropertyRepository->findOrFail($propertyId);

            $attribute = $request->only([
                'followup_status',
                'is_priority',
                'offered_product',
                'remark'
            ]);

            $attribute['updated_by'] = Auth::id();
            if (!empty($attribute['offered_product'])) {
                $attribute['offered_product'] = implode(',', $attribute['offered_product']);
            }

            $property = $this->potentialPropertyRepository->update($attribute, $propertyId);
            $property['offered_product'] = $property->extractOfferedProduct();
            return ApiHelper::responseData(['property' => $property->toArray()]);
        } catch (ModelNotFoundException $e) {
            return ApiHelper::responseError(
                'The potential property does not exist',
                false,
                404
            );
        }
    }

    /**
     *  Get all potential property owned by a potential owner
     *
     *  @param Request $request
     *  @param int $potentialProperty
     *
     *  @return View
     */
    public function detail(Request $request, int $potentialProperty): View
    {
        $potentialProperty = $this->potentialPropertyRepository
            ->with([
                'photo',
                'potential_owner',
                'first_created_by',
                'room',
                'last_updated_by'
            ])
            ->findOrFail($potentialProperty);
        $media = $potentialProperty->photo;
        $medium = !is_null($media) ? $media->getMediaUrl()['medium'] : '';
        $thumbnail = !is_null($media) ? $media->getMediaUrl()['small'] : '';
        $potentialProperty->offered_product = implode(', ', $potentialProperty->extractOfferedProduct());
        return view('admin.contents.consultant.potential-owner.detail-property', ['potentialProperty' => $potentialProperty, 'medium' => $medium, 'thumbnail' => $thumbnail]);
    }

    /**
     *  Delete a potential property
     *
     *  @param Request $request
     *  @param mixed $propertyId
     *
     *  @return JsonResponse
     */
    public function destroy(Request $request, $propertyId): JsonResponse
    {
        try {
            $toDelete = $this->potentialPropertyRepository->find($propertyId);
            $this->potentialPropertyRepository->delete($propertyId);

            $owner = $toDelete->potential_owner;
            $owner->total_property -= 1;
            $owner->total_room -= $toDelete->total_room;
            $owner->updated_by = Auth::id();
            $owner->save();

            return ApiHelper::responseData(['property' => $toDelete]);
        } catch (ModelNotFoundException $e) {
            return ApiHelper::responseError(
                'The potential property does not exist',
                false,
                404
            );
        }
    }
}
