<?php

namespace App\Http\Controllers\Admin\Consultant;

use App\Entities\Consultant\ActivityManagement\ActivityForm;
use App\Entities\Consultant\ActivityManagement\ActivityFunnel;
use App\Repositories\Consultant\ActivityManagement\ActivityFormRepository;
use App\Repositories\Consultant\ActivityManagement\ActivityFunnelRepository;
use App\Entities\Consultant\ActivityManagement\FormInput;
use App\Entities\Consultant\ActivityManagement\InputType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Helpers\ApiResponse;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\View\View;

class ActivityManagementController extends Controller
{
    public const DEFAULT_PAGINATION = 25;
    protected $funnelRepository;
    protected $stageRepository;

    public function __construct(ActivityFunnelRepository $funnelRepository, ActivityFormRepository $stageRepository)
    {
        $this->funnelRepository = $funnelRepository;
        $this->stageRepository = $stageRepository;

        \View::share('user', \Auth::user());
        \View::share('contentHeader', 'Consultants Activity Management');
    }

    /**
     *  Return view that display table of all available funnel
     */
    public function index(): View
    {
        return view('admin.contents.consultant.consultant-activity-management-index');
    }

    /**
     *  Show all activity funnel available
     *
     * @param Request $request Incoming HTTP request
     *
     * @return JsonResponse
     */
    public function funnelData(Request $request): JsonResponse
    {
        $funnels = ActivityFunnel::select(
            'id',
            'name as funnel_name',
            'consultant_role as role',
            'related_table as data_type',
            'total_stage as staging',
            'updated_at'
        )
        ->orderBy('updated_at', 'desc');

        $count = $funnels->count();
        $funnels = $funnels->offset($request->input('offset', 0))
            ->limit($request->input('limit', self::DEFAULT_PAGINATION));

        return ApiResponse::responseData(
            [
                'total' => $count,
                'funnels' => $funnels->get()
            ]
        );
    }

    /**
     *  Return view that display table of staging for a funnel with a given id
     *
     * @param Request $request Incoming HTTP request
     * @param int $funnelId Id of the funnel
     *
     * @return View
     */
    public function show(Request $request, int $funnelId): View
    {
        $funnel = ActivityFunnel::find($funnelId);

        if (is_null($funnel)) {
            abort(404);
        }

        return view(
            'admin.contents.consultant.consultant-activity-management-detail',
            [
                'funnel_id' => $funnel->id,
                'funnel_name' => $funnel->name,
                'consultant_role' => $funnel->consultant_role,
                'data_type' => $funnel->related_table,
            ]
        );
    }

    /**
     *  Show all staging available for a given id of a funnel
     *
     * @param Request $request Incoming HTTP request
     * @param int $funnelId Id of funnel
     *
     * @return JsonResponse
     */
    public function stagingData(Request $request, int $funnelId): JsonResponse
    {
        $funnel = ActivityFunnel::find($funnelId);

        if (is_null($funnel)) {
            abort(404);
        }

        $stageCount = $funnel->forms()->count();
        $stages = $funnel->forms()
            ->select(
                'id',
                'stage',
                'name',
                'detail',
                'form_element'
            )
            ->get();

        $stages = $stages->map(
            function ($item) {
                return [
                    'id' => $item->id,
                    'stage_phase' => $item->stage,
                    'stage_name' => $item->name,
                    'description' => $item->detail,
                    'input_form' => count($item->form_element)
                ];
            }
        );

        return ApiResponse::responseData(
            [
                'total' => $stageCount,
                'stages' => $stages
            ]
        );
    }

    /**
     *  Return view that display table of all available form for a stage
     *
     * @param Request $request Incoming HTTP request
     * @param int $stageId Id of stage to retrieve
     *
     * @return View
     */
    public function stageDetail(Request $request, int $stageId): View
    {
        $forms = ActivityForm::findOrFail($stageId);
        $funnel = $forms->funnel;
        $allStage = $funnel->forms()->select('id', 'name')->get()->toArray();
        return view(
            'admin.contents.consultant.consultant-activity-management-staging-detail',
            [
                'stage_id' => $stageId,
                'funnel_name' => $funnel->name,
                'consultant_role' => $funnel->consultant_role,
                'data_type' => $funnel->related_table,
                'stage_name' => $forms->name,
                'all_stage' => $allStage
            ]
        );
    }

    /**
     *  Show all available forms for a given stage
     *
     * @param Request $request Incoming HTTP request
     * @param int $stageId Id of stage to retrieve
     *
     * @return JsonResponse
     */
    public function formsData(Request $request, int $stageId): JsonResponse
    {
        $forms = ActivityForm::findOrFail($stageId)->form_element;

        return ApiResponse::responseData(
            [
                'total' => count($forms),
                'forms' => $forms
            ]
        );
    }

    /**
     *  Return view that display create form for Activity Funnel
     *
     *
     * @return View
     */
    public function createFunnel(): View
    {
        return view('admin.contents.consultant.consultant-activity-management-create-update-funnel');
    }

    /**
     *  Return redirect to create staging route after success save new Activity Funnel
     *
     *
     * @param Request $request
     *
     */
    public function storeFunnel(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'name' => 'required',
                'role' => ['required', Rule::in(['admin', 'supply', 'demand'])],
                'relatedTable' => [
                    'required',
                    Rule::in([
                        ActivityFunnel::TABLE_DBET,
                        ActivityFunnel::TABLE_CONTRACT,
                        ActivityFunnel::TABLE_PROPERTY,
                        ActivityFunnel::TABLE_POTENTIAL_PROPERTY,
                        ActivityFunnel::TABLE_POTENTIAL_OWNER
                    ])
                ],
            ]
        );

        if ($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors())->withInput($request->all());
        }

        $funnel = $this->funnelRepository->store($request->all());

        return redirect('/admin/consultant/activity-management/create/staging/' . $funnel->id);
    }

    /**
     *  Return view for edit form Activity Funnel
     *
     *
     * @param int $funnelId
     *
     * return View
     */
    public function editFunnel(int $funnelId): View
    {
        $edit = true;
        $funnel = ActivityFunnel::findOrFail($funnelId);

        return view(
            'admin.contents.consultant.consultant-activity-management-create-update-funnel',
            compact('edit', 'funnel')
        );
    }

    /**
     *  Return redirect to Activity Management Index after data is updated
     *
     *
     * @param int $funnelId
     * @param Request $request
     *
     */
    public function updateFunnel(int $funnelId, Request $request)
    {
        // dd($request->all());
        $validator = Validator::make(
            $request->all(),
            [
                'name' => 'required',
                'role' => ['required', Rule::in(['admin', 'supply', 'demand'])],
                'relatedTable' => [
                    'required',
                    Rule::in([
                        ActivityFunnel::TABLE_DBET,
                        ActivityFunnel::TABLE_CONTRACT,
                        ActivityFunnel::TABLE_PROPERTY,
                        ActivityFunnel::TABLE_POTENTIAL_PROPERTY,
                        ActivityFunnel::TABLE_POTENTIAL_OWNER
                    ])
                ],
            ]
        );

        if ($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors())->withInput($request->all());
        }

        $response = $this->funnelRepository->update($request->input(), $funnelId);

        if (is_null($response)) {
            return redirect()->back()->with('error_message', 'Funnel Not Found')->withInput($request->all());
        }

        return redirect('/admin/consultant/activity-management');
    }

    /**
     *  Delete Funnel and it's forms
     *
     *
     * @param int $funnelId
     *
     *  return JsonResponse
     */
    public function deleteFunnel(int $funnelId): JsonResponse
    {
        $response = $this->funnelRepository->destroy($funnelId);

        if (!$response) {
            return ApiResponse::responseData(
                [
                    'status' => false,
                    'message' => 'Failed to delete funnel with given id'
                ]
            );
        }

        return ApiResponse::responseData(
            [
                'status' => true,
                'message' => 'Funnel data successfully deleted'
            ]
        );
    }

    /**
     *  Return redirect to Activity Management Index after data is updated
     *
     *
     * @param int $funnelId
     * @param Request $request
     *
     *  return View
     */
    public function createStaging($funnelId): View
    {
        return view(
            'admin.contents.consultant.consultant-activity-management-create-staging',
            ['funnel_id' => $funnelId]
        );
    }

    /**
     *
     * Store new staging data
     *
     * @param int $funnelId
     * @param Request $request
     *
     *  return JsonResponse
     */
    public function storeStaging($funnelId, Request $request): JsonResponse
    {
        $validator = Validator::make(
            $request->all(),
            [
                'stages' => 'array|required'
            ]
        );

        if ($validator->fails()) {
            return ApiResponse::responseData(['status' => false]);
        }

        $stages = $request->input('stages');
        foreach ($stages as $stage) {
            $this->stageRepository->storeStagingData($funnelId, $stage);
        }

        return ApiResponse::responseData(
            [
                'status' => true,
                'message' => 'Stage Data successfully added'
            ]
        );
    }

    /**
     *
     * Update given staging data
     *
     * @param int $funnelId
     * @param Request $request
     *
     *  return JsonResponse
     */
    public function updateStaging($stageId, Request $request): JsonResponse
    {
        $response = $this->stageRepository->updateStagingData($request->input(), $stageId);

        if (is_null($response)) {
            return ApiResponse::responseData(['status' => false, 'message' => 'Failed to update stage data']);
        }

        return ApiResponse::responseData(
            [
                'status' => true,
                'message' => 'Stage Data successfully updated'
            ]
        );
    }

    /**
     *  Delete Forms by id
     *
     *
     * @param int $stageId
     *
     *  return JsonResponse
     */
    public function deleteStaging(int $stageId): JsonResponse
    {
        $response = $this->stageRepository->deleteStaging($stageId);

        if (!$response) {
            return ApiResponse::responseData(
                [
                    'status' => false,
                    'message' => 'Failed to delete stage with given id'
                ]
            );
        }

        return ApiResponse::responseData(
            [
                'status' => true,
                'message' => 'Stage data successfully deleted'
            ]
        );
    }

    /**
     *  Delete Forms Element of form by id
     *
     *
     * @param int $stageId
     * @param int $formId
     *
     *  return JsonResponse
     */
    public function deleteFormInput(int $stageId, int $formId): JsonResponse
    {
        $response = $this->stageRepository->deleteFormInput($stageId, $formId);

        if (!$response) {
            return ApiResponse::responseData(
                [
                    'status' => false,
                    'message' => 'Failed to delete form input with given id'
                ]
            );
        }

        return ApiResponse::responseData(
            [
                'status' => true,
                'message' => 'Form input successfully removed'
            ]
        );
    }

    /**
     *  Update a specific input field in form_element of ActivityForm
     *
     *  @param Request $request Incoming HTTP request
     *  @param int $formId Id of input field to update
     *
     *  @return JsonResponse
     *
     *  @see App\Entities\Consultant\ActivityManagement\ActivityForm
     */
    public function formUpdate(Request $request, int $stageId): JsonResponse
    {
        $stage = ActivityForm::find($stageId);
        $form = !is_null($stage) ? collect($stage->form_element) : null;

        $validator = Validator::make(
            $request->all(),
            [
                'id' => 'sometimes|required|numeric|integer',
                'type' => ['sometimes', 'required', new EnumValue(InputType::class)],
                'label' => 'sometimes|required|min:1|max:255|string',
                'placeholder' => 'sometimes|min:1|max:255|string',
                'values' => Rule::requiredIf(function () use ($request) {
                    if (!empty($request->type)) {
                        return in_array($request->type, InputType::HAS_VALUES);
                    }

                    return false;
                }),
                'order' => 'sometimes|required|numeric|min:1',
            ],
            [
                'values.required' => 'Values must be filled if type has multiple value option.'
            ]
        );

        $validator->after(function ($validator) use ($request, $form) {
            if (is_null($form)) {
                $validator->errors()->add('id', 'Stage could not be found');
            } elseif ($form->where('id', $request->id)->isEmpty()) {
                $validator->errors()->add('id', 'Input id could not be found');
            }

            if (!empty($request->order) && !is_null($form) && $form->where('order', $request->order)->whereNotIn('id', [$request->id])->isNotEmpty()) {
                $validator->errors()->add('order', 'Order selected has been used');
            }
        });

        if ($validator->fails()) {
            return ApiResponse::responseData([
                'status' => false,
                'messages' => $validator->errors()
            ]);
        }

        $toUpdate = $request->only(['id', 'type', 'label', 'placeholder', 'values', 'order']);
        $stage->form_element = $form->map(function ($item, $key) use ($toUpdate) {
            if ($item['id'] === $toUpdate['id']) {
                if (!empty($toUpdate['type'])) {
                    $item['type'] = $toUpdate['type'];

                    if (!in_array($toUpdate['type'], InputType::HAS_VALUES)) {
                        $item['values'] = []; // Values is empty when input type does not have multiple predefined values.
                    }
                }

                if (!empty($toUpdate['label'])) {
                    $item['label'] = $toUpdate['label'];
                }

                if (isset($toUpdate['placeholder'])) {
                    $item['placeholder'] = $toUpdate['placeholder'];
                }

                if (!empty($toUpdate['order'])) {
                    $item['order'] = $toUpdate['order'];
                }

                if (in_array($item['type'], InputType::HAS_VALUES) && !empty($toUpdate['values'])) {
                    $item['values'] = $toUpdate['values'];
                }

                return $item;
            }
            return $item;
        });

        $stage->updated_by = Auth::id();
        $stage->save();

        return ApiResponse::responseData([]);
    }

    /**
     *  Add an input field to an existing form
     *
     *  @param Request $request Incoming HTTP Request
     *  @param int $stageId Id of form to add input field
     *
     *  @return JsonResponse
     */
    public function formAdd(Request $request, int $stageId): JsonResponse
    {
        $stage = ActivityForm::find($stageId);
        $form = !is_null($stage) ? collect($stage->form_element) : null;

        $validator = Validator::make(
            $request->all(),
            [
                'fields.*.type' => ['required', new EnumValue(InputType::class)],
                'fields.*.label' => 'required|min:1|max:255|string',
                'fields.*.placeholder' => 'min:1|max:255|string',
                'fields.*.order' => 'required|numeric|min:1',
            ],
            [
                'fields.*.type.required' => 'The type field is required.',
                'fields.*.label.required' => 'The label field is required.',
                'fields.*.label.min' => 'The label must be at least 1.',
                'fields.*.label.max' => 'The label must be less than 255.',
                'fields.*.placeholder.min' => 'The placeholder must be at least 1.',
                'fields.*.placeholder.max' => 'The placeholder must be less than 255.',
                'fields.*.order.required' => 'The order field is required.',
                'fields.*.order.min' => 'The order must be at least 1.',
                'fields.*.order.max' => 'The order must be less than 255',
            ]
        );

        $validator->after(function ($validator) use ($request, $form) {
            if (is_null($form)) {
                $validator->errors()->add('id', 'Stage could not be found');
            }

            foreach ($request->fields as $index => $field) {
                if (isset($field['order']) && !is_null($form) && $form->where('order', $field['order'])->isNotEmpty()) {
                    $validator->errors()->add("fields.$index.order", 'Order selected has been used');
                }

                if (isset($field['type']) && in_array($field['type'], InputType::HAS_VALUES) && !isset($field['values'])) {
                    $validator->errors()->add("fields.$index.values", 'Values must be filled if type has multiple value option.');
                }
            }
        });

        if ($validator->fails()) {
            return ApiResponse::responseData([
                'status' => false,
                'messages' => $validator->errors()
            ]);
        }

        foreach ($request->fields as $field) {
            $newInput = new FormInput();
            $newInput->setType(new InputType($field['type']))
                ->setLabel($field['label'])
                ->setOrder($field['order'])
                ->setId(($form->count() + 1));

            if (isset($field['placeholder'])) {
                $newInput->setPlaceholder($field['placeholder']);
            }

            if (in_array($field['type'], InputType::HAS_VALUES)) {
                $newInput->setValues($field['values']);
            } else {
                $newInput->setValues([]);
            }

            $form->push($newInput->toArray());
        }

        $stage->form_element = $form;
        $stage->updated_by = Auth::id();
        $stage->save();

        return ApiResponse::responseData([]);
    }

    /**
     *  Return view create form input
     *
     *
     * @param int $stageId
     * @param Request $request
     *
     *  return View
     */
    public function createForms($stageId): View
    {
        return view(
            'admin.contents.consultant.consultant-activity-management-create-form-input',
            ['stage_id' => $stageId]
        );
    }
}
