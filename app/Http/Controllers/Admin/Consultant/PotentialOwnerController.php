<?php

namespace App\Http\Controllers\Admin\Consultant;

use App\Criteria\Consultant\PaginationCriteria;
use App\Criteria\Consultant\PotentialOwner\FollowupStatusCriteria;
use App\Criteria\Consultant\PotentialOwner\SearchCriteria;
use App\Entities\Consultant\PotentialOwner;
use App\Http\Controllers\Admin\BaseController;
use App\Http\Helpers\ApiHelper;
use App\Http\Helpers\ApiResponse;
use App\Http\Helpers\PhoneNumberHelper;
use App\Presenters\Consultant\PotentialOwnerPresenter;
use App\Repositories\Consultant\PotentialOwnerRepository;
use App\Services\Consultant\PotentialOwnerService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\View\View;

class PotentialOwnerController extends BaseController
{
    protected const PAGINATION_LIMIT = 15;

    /**
     *  Instance of PotentialOwnerRepository
     *
     *  @var PotentialOwnerRepository
     */
    protected $potentialOwnerRepository;

    /**
     *  Instance of PotentialOwnerService
     *
     *  @var PotentialOwnerService
     */
    protected $potentialOwnerService;

    public function __construct(
        PotentialOwnerRepository $potentialOwnerRepository,
        PotentialOwnerService $potentialOwnerService
    )
    {
        $this->potentialOwnerRepository = $potentialOwnerRepository;
        $this->potentialOwnerService = $potentialOwnerService;
        \View::share('contentHeader', 'Potential Owner');
    }

    /**
     *  Get list of potential owner view
     *
     * @return View
     */
    public function index(): View
    {
        return view('admin.contents.consultant.potential-owner.index');
    }

    /**
     *  Get view of create new potential owner
     *
     * @return View
     */
    public function add(): View
    {
        return view('admin.contents.consultant.potential-owner.add');
    }

    /**
     *  Get view of create new potential owner
     *
     * @return View
     */
    public function edit(PotentialOwner $owner): View
    {
        return view('admin.contents.consultant.potential-owner.edit', compact('owner'));
    }

    /**
     *  Get list of potential owner data
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function indexData(Request $request): JsonResponse
    {
        $searchKeys = ['name', 'phone_number', 'bbk_status'];
        $searchParams = array_filter($request->all(), function($key) use ($searchKeys) {
            return in_array($key, $searchKeys);
        }, ARRAY_FILTER_USE_KEY);

        $ids = [];
        $total = 0;
        $queryBuilder = $this->potentialOwnerRepository;

        if (!empty($searchParams)) {
            $result = $this->potentialOwnerService->search($searchParams);
            $total = $result['total'];

            // only push search criteria when searchParams does exists.
            $queryBuilder = $queryBuilder->pushCriteria(
                new SearchCriteria($result['ids'])
            );
        } else {
            if ($request->filled('followup_status')) {
                $queryBuilder = $queryBuilder->pushCriteria(
                    new FollowupStatusCriteria($request->followup_status)
                );
            }

            $total = $queryBuilder->count();
        }

        $queryBuilder = $queryBuilder
            ->orderBy('followup_status', 'asc')
            ->orderBy('updated_at', 'desc')
            ->with('user');

        $queryBuilder = $queryBuilder
            ->setPresenter(
                new PotentialOwnerPresenter(
                    PotentialOwnerPresenter::ADMIN_LIST
                )
            )
            ->pushCriteria(
                new PaginationCriteria(
                    $request->input('limit', self::PAGINATION_LIMIT),
                    $request->input('offset', PaginationCriteria::DEFAULT_OFFSET)
                )
            )
            ->with(['last_updated_by:id,name']);

        $data = $queryBuilder->get();

        return ApiHelper::responseData(
            [
                'total' => $total,
                'rows' => $data['data']
            ]
        );
    }

    /**
     *  Get view of potential owner detail
     *
     * @return View
     */
    public function detail(PotentialOwner $owner): View
    {
        $owner->load('user');
        return view('admin.contents.consultant.potential-owner.detail', compact('owner'));
    }

    /**
     *
     * Add new potential owner
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $validator = Validator::make(
            $request->all(),
            [
                'name' => 'required|string',
                'phone_number' => 'required|string',
                'email' => 'email',
                'remark' => 'string|max:255',
                'date_to_visit' => 'nullable|date'
            ]
        );

        $validator->after(function ($validator) use ($request) {
            if (!is_null($request->phone_number)) {
                $isPhoneNumberExist = $this->potentialOwnerRepository
                    ->where('phone_number', PhoneNumberHelper::sanitizeNumber($request->phone_number))
                    ->exists();

                if ($isPhoneNumberExist) {
                    $validator->errors()->add('phone_number', 'Phone number must be unique.');
                }
            }

            if (!empty($request->email)) {
                $isPhoneNumberExist = $this->potentialOwnerRepository->where('email', $request->email)->exists();

                if ($isPhoneNumberExist) {
                    $validator->errors()->add('email', 'Email must be unique.');
                }
            }
        });

        if ($validator->fails()) {
            return ApiResponse::responseData(
                [
                    'status' => false,
                    'message' => $validator->errors()
                ]
            );
        }

        $input = $request->only(['name', 'phone_number', 'email', 'remark', 'date_to_visit']);

        $this->potentialOwnerService->store($input, Auth::id());

        $data = $this->potentialOwnerRepository->where('phone_number', PhoneNumberHelper::sanitizeNumber($request->phone_number))->first();

        return ApiResponse::responseData(
            [
                'status' => true,
                'message' => 'Potential Owner data successfully added',
                'owner_id' => is_null($data) ? null : $data->id
            ]
        );
    }

    /**
     *
     * Update potential owner
     *
     * @param Request $request
     * @param PotentialOwner $owner
     *
     * @return JsonResponse
     */
    public function update(Request $request, PotentialOwner $owner)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'name' => 'required|string',
                'phone_number' => 'required|string',
                'email' => [
                    'email',
                    Rule::unique('consultant_potential_owner', 'email')->ignore($owner->id)
                ],
                'remark' => 'string|max:255',
                'date_to_visit' => 'nullable|date'
            ]
        );

        if ($validator->fails()) {
            return ApiResponse::responseData(
                [
                    'status' => false,
                    'message' => $validator->errors()
                ]
            );
        }

        $input = $request->only(['name', 'phone_number', 'email', 'remark', 'date_to_visit']);
        $input['phone_number'] = PhoneNumberHelper::sanitizeNumber($input['phone_number']);
        $input['email'] = empty($input['email']) ? null : $input['email'];

        $checkOwner = $this->potentialOwnerRepository->findByField('phone_number', $input['phone_number']);

        //check the phone number after sanitized
        if (count($checkOwner) > 0 && $checkOwner[0]->id != $owner->id) {
            return ApiResponse::responseData(
                [
                    'status' => false,
                    'message' => [
                        'phone_number' => ['Phone number is already used by other owner']
                    ]
                ]
            );
        }

        $input['updated_by'] = Auth::user()->id;
        $this->potentialOwnerRepository->update($input, $owner->id);

        return ApiResponse::responseData(
            [
                'status' => true,
                'message' => 'Potential Owner data successfully updated'
            ]
        );
    }

    /**
     *  Process bulk upload of potential owner
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function bulkUpload(Request $request): JsonResponse
    {
        $request->validate(
            [
                'file' => 'required|file|mimes:csv,txt|max:1000'
            ]
        );

        $file = $request->file('file');

        $csv = fopen($file, 'r');
        $rowCount = 0;
        $failed = [];
        $successCount = 0;
        $failedCount = 0;
        while (($row = fgetcsv($csv)) !== false) {
            if (!empty($row)) {
                if ($rowCount === 0) {
                    // Skip first row which contain header
                    $rowCount = $rowCount + 1;
                    continue;
                }

                $phoneNumber = PhoneNumberHelper::sanitizeNumber($row[1]);
                $email = $row[2];

                $isEmailNotUnique = $this->potentialOwnerRepository
                    ->where([
                        ['email', '=', $email],
                        ['phone_number', '!=', $phoneNumber]
                    ])
                    ->exists();

                if ($isEmailNotUnique && !empty($email)) {
                    $failedCount++;
                    $failed[] = [
                        $row[2] => 'Email potential owner sudah dipakai'
                    ];
                    continue;
                }

                $existingOwner = $this->potentialOwnerRepository->where('phone_number', $phoneNumber)->first();
                if (is_null($existingOwner)) {
                    $this->potentialOwnerService->store(
                        [
                            'name' => $row[0],
                            'phone_number' => PhoneNumberHelper::sanitizeNumber($row[1]),
                            'email' => $row[2],
                            'remark' => $row[3],
                            'date_to_visit' => $row[4]
                        ],
                        Auth::id()
                    );
                    $successCount++;
                } else {
                    $existingOwner->update([
                        'name' => $row[0],
                        'phone_number' => PhoneNumberHelper::sanitizeNumber($row[1]),
                        'email' => $row[2],
                        'remark' => $row[3],
                        'date_to_visit' => $row[4],
                        'updated_by' => Auth::id(),
                    ]);
                    $successCount++;
                }
            }
        }

        return ApiHelper::responseData([
            'data' => [
                'success_count' => $successCount,
                'failed_count' => $failedCount,
                'failed' => $failed
            ]
        ]);
    }

    public function destroy(Request $request, $id): JsonResponse
    {
        try {
            $this->potentialOwnerRepository->delete($id);

            return ApiHelper::responseData([]);
        } catch (ModelNotFoundException $e) {
            return ApiHelper::responseError(false, false, 404);
        }
    }
}
