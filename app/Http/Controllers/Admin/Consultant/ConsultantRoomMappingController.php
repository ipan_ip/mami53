<?php

namespace App\Http\Controllers\Admin\Consultant;

use App\Entities\Consultant\Consultant;
use App\Entities\Consultant\ConsultantRole;
use App\Entities\Room\Room;
use App\Entities\Consultant\ConsultantRoom;
use App\Entities\Level\KostLevel;
use App\Repositories\Consultant\ConsultantRepository;
use App\Http\Controllers\Controller;
use App\Http\Helpers\ApiResponse;
use App\Services\Consultant\ConsultantRoomIndexService;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;

class ConsultantRoomMappingController extends Controller
{
    private $consultantRepository;
    protected $consultantRoomIndexService;

    const DEFAULT_PAGINATION = 15;
    const FILTER_BOOKING = 'booking';
    const FILTER_FREE_LISTING = 'free_listing';

    public function __construct(ConsultantRepository $consultantRepository, ConsultantRoomIndexService $consultantRoomIndexService)
    {
        $this->consultantRepository = $consultantRepository;
        $this->consultantRoomIndexService = $consultantRoomIndexService;

        \View::share('user', \Auth::user());
        \View::share('contentHeader', 'Consultants Room Mapping');
    }

    public function index()
    {
        $consultants = Consultant::all();
        $cities = Room::whereNotNull('area_city')
            ->where('area_city', '!=', DB::RAW("''"))
            ->where('area_city', '!=', '-')
            ->select('area_city')
            ->distinct()
            ->orderBy('area_city')
            ->get();

        return view(
            'admin.contents.consultant.consultant-room-mapping-index',
            ['boxTitle' => 'Consultant Kost Mapping', 'consultants' => $consultants, 'cities' => $cities]
        );
    }

    public function indexData(Request $request)
    {
        try {
            $consultants = Consultant::join(
                'consultant_designer',
                function ($join) {
                    $join->on('consultant.id', 'consultant_designer.consultant_id')
                        ->whereNull('consultant_designer.deleted_at');
                }
            )
                ->join(
                    'designer',
                    function ($join) {
                        $join->on('designer.id', 'consultant_designer.designer_id')
                            ->whereNull('designer.deleted_at')
                            ->whereNotNull('area_city')
                            ->where('area_city', '!=', DB::RAW("''"))
                            ->where('area_city', '!=', '-');
                    }
                )
                ->select(
                    'consultant.id as consultant_id',
                    'consultant.name as consultant',
                    'designer.area_city as area',
                    DB::RAW('COUNT(designer.id) as property_mapped')
                )
                ->groupBy(
                    'consultant.id',
                    'consultant.name',
                    'designer.area_city'
                );

            if ($request->has('search') && !empty($request->input('search'))) {
                switch ($request->input('searchBy')) {
                    case 'consultant':
                        $consultants = $consultants->where(
                            'consultant.name',
                            'like',
                            '%' . $request->input('search') . '%'
                        );
                        break;
                    case 'property':
                        $consultants = $consultants->where(
                            'designer.name',
                            'like',
                            '%' . $request->input('search') . '%'
                        );
                        break;
                    case 'area':
                        $consultants = $consultants->where(
                            'designer.area_city',
                            'like',
                            '%' . $request->input('search') . '%'
                        );
                        break;
                }
            }

            $total = count($consultants->get());

            $consultants = $consultants->offset($request->input('offset', 0))
                ->limit($request->input('limit', self::DEFAULT_PAGINATION));

            return response()->json(
                [
                    'total' => $total,
                    'rows' => $consultants->get()
                ]
            );
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);

            return response()->json(
                [
                    'total' => 0,
                    'rows' => []
                ]
            );
        }
    }

    public function add(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'consultant' => 'required',
                'areaCity' => 'required'
            ]
        );

        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        $consultant = Consultant::find($request->input('consultant'));
        $areaCity = $request->input('areaCity');

        $kostLevels = KostLevel::select('id', 'name')
            ->orderBy('name')
            ->get();

        if (is_null($consultant)) {
            return redirect()->back()->with('error_message', 'Consultant not found');
        }

        return view('admin.contents.consultant.consultant-room-mapping-add', compact('consultant', 'areaCity', 'kostLevels'));
    }

    public function addData($consultantId, $areaCity, Request $request)
    {
        try {
            $rooms = Room::whereDoesntHave('consultant_rooms', function ($mapping) use ($consultantId) {
                $mapping->where('consultant_id', $consultantId);
            })
                ->leftjoin('kost_level_map', 'kost_level_map.kost_id', 'designer.song_id')
                ->leftjoin('kost_level', 'kost_level.id', 'kost_level_map.level_id')
                ->select(
                    'designer.id',
                    'designer.song_id',
                    'designer.name',
                    'designer.area_city',
                    DB::RAW('IF(designer.is_booking=1,"Booking Langsung","Free Listing") as type'),
                    'kost_level.name as kost_level'
                )
                ->where('area_city', $areaCity)
                ->where('expired_phone', false)
                ->where('is_active', 'true');

            $consultant = Consultant::find($consultantId);
            $isAdminChat = !is_null($consultant) ? $consultant->roles()->where('role', ConsultantRole::ADMIN_CHAT)->exists() : false;
            if ($isAdminChat) {
                $rooms = $rooms->whereDoesntHave('consultant_rooms', function ($mapping) {
                    $mapping->whereDoesntHave('consultant', function ($consultant) {
                        $consultant->whereDoesntHave('roles', function ($roles) {
                            $roles->where('role', ConsultantRole::ADMIN_CHAT);
                        });
                    });
                });
            }

            if ($request->has('search')) {
                $rooms = $rooms->where('designer.name', 'LIKE', '%' . $request->input('search') . '%');
            }

            if ($request->has('property_type') && !empty($request->input('property_type'))) {
                if ($request->input('property_type') === self::FILTER_BOOKING) {
                    $rooms = $rooms->where('designer.is_booking', 1);
                } elseif ($request->input('property_type') === self::FILTER_FREE_LISTING) {
                    $rooms = $rooms->where('designer.is_booking', 0);
                }
            }

            if ($request->has('level_id') && !empty($request->input('level_id'))) {
                $rooms = $rooms->where('kost_level.id', $request->input('level_id'));
            }

            $total = $rooms->count();
            $rooms = $rooms->limit($request->input('limit', self::DEFAULT_PAGINATION))
                ->offset($request->input('offset', 0));

            return response()->json(
                [
                    'total' => $total,
                    'rows' => $rooms->get()
                ]
            );
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);

            return response()->json(
                [
                    'total' => 0,
                    'rows' => []
                ]
            );
        }
    }

    public function store($consultantId, Request $request)
    {
        try {
            $validator = Validator::make(
                $request->all(),
                [
                    'rooms' => 'required',
                ]
            );

            if ($validator->fails()) {
                return ApiResponse::responseData(
                    [
                        'status' => false,
                        'messages' => $validator->errors()->getMessages()
                    ]
                );
            }

            $consultant = Consultant::find($consultantId);
            if (is_null($consultant)) {
                return ApiResponse::responseData(
                    [
                        'status' => false,
                        'messages' => 'Consultant not found'
                    ]
                );
            }

            $selectedRooms = array_unique($request->input('rooms'));
            foreach ($selectedRooms as $selectedRoom) {
                $map = ConsultantRoom::where('consultant_id', $consultant->id)
                    ->where('designer_id', $selectedRoom)
                    ->count();

                if ($map === 0) {
                    $mapping = new ConsultantRoom();
                    $mapping->designer_id = $selectedRoom;
                    $mapping->consultant_id = $consultantId;
                    $mapping->save();
                } 
            }

            return ApiResponse::responseData(['message' => 'Consultant successfully assigned']);
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);

            return ApiResponse::responseData(['status' => false, 'message' => $e->getMessage()]);
        }
    }

    /**
     *  Return 4 consultant with matching name if provided
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function getConsultant(Request $request): JsonResponse
    {
        try {
            $consultants = Consultant::limit(4);
            $searchQuery = $request->input('search', '');

            if (!empty($searchQuery)) {
                $consultants->where('name', 'like', ('%' . $searchQuery . '%'));
            }

            $consultants = $consultants->get();

            return ApiResponse::responseData(['status' => true, 'data' => $consultants]);
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);

            return ApiResponse::responseData(['status' => false, 'data' => []]);
        }
    }

    /**
     *  Return consultant room mapping detail view for a consultant
     *
     * @param Request $request
     * @param int $consultantId
     *
     * @return View
     */
    public function detail(Request $request, int $consultantId, string $areaCity): View
    {
        $consultant = Consultant::find($consultantId);

        if (is_null($consultant)) {
            abort(404);
        }

        return view(
            'admin.contents.consultant.consultant-room-mapping-detail',
            [
                'boxTitle' => 'Consultant Kost Mapping Detail',
                'consultantName' => $consultant->name,
                'consultantId' => $consultantId,
                'areaCity' => $areaCity
            ]
        );
    }

    /**
     *  Get rooms data mapped to a consultant
     *
     * @param Request $request
     * @param int $consultantId
     *
     * @return JsonResponse
     */
    public function detailData(Request $request, int $consultantId, string $areaCity): JsonResponse
    {
        try {
            $mappings = Consultant::select('id', 'name')
                ->where('id', $consultantId);

            if (!$mappings->exists()) {
                return ApiResponse::responseData(['status' => false, 'meta' => ['message' => 'Consultant not found']]);
            }

            $searchQuery = $request->input('search', '');
            $paginationLimit = $request->input('limit', 15);
            $paginationOffset = $request->input('offset', 0);
            $hasIsBooking = $request->input('booking', '');
            $isBooking = ($hasIsBooking !== 'false');

            $mappings = $mappings->with(
                [
                    'rooms' => function ($rooms) use (
                        $searchQuery,
                        $paginationLimit,
                        $paginationOffset,
                        $areaCity,
                        $hasIsBooking,
                        $isBooking
                    ) {
                        $rooms
                            ->leftjoin('kost_level_map', 'kost_level_map.kost_id', 'designer.song_id')
                            ->leftjoin('kost_level', 'kost_level.id', 'kost_level_map.level_id')
                            ->select(
                                'designer.id',
                                'designer.song_id',
                                'designer.name',
                                'designer.area_city',
                                'designer.is_booking',
                                'kost_level.name as kost_level'
                            );

                        if (!empty($searchQuery)) {
                            $rooms->where('designer.name', 'like', ('%' . $searchQuery . '%'));
                        }

                        if (!empty($hasIsBooking)) {
                            $rooms->where('designer.is_booking', $isBooking);
                        }

                        $rooms->where('designer.area_city', $areaCity);
                        $rooms->limit($paginationLimit);
                        $rooms->offset($paginationOffset);
                    }
                ]
            )->withCount(
                [
                    'rooms' => function ($rooms) use (
                        $searchQuery,
                        $hasIsBooking,
                        $isBooking,
                        $areaCity
                    ) {
                        if (!empty($searchQuery)) {
                            $rooms->where('name', 'like', ('%' . $searchQuery . '%'));
                        }

                        if (!empty($hasIsBooking)) {
                            $rooms->where('is_booking', $isBooking);
                        }

                        $rooms->where('area_city', $areaCity);
                    }
                ]
            );

            $mappings = $mappings->first();
            return ApiResponse::responseData(
                [
                    'count' => $mappings->rooms_count,
                    'data' => $mappings->rooms,
                    'consultant' => ['id' => $mappings->id, 'name' => $mappings->name]
                ]
            );
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);

            return ApiResponse::responseData(
                [
                    'count' => 0,
                    'data' => [],
                    'consultant' => []
                ]
            );
        }
    }

    /**
     *  Reassign area held by a consultant to another consultant
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function reassign(Request $request): JsonResponse
    {
        try {
            $validator = Validator::make(
                $request->all(),
                [
                    'old_consultant_id' => 'required|numeric|exists:consultant_designer,consultant_id',
                    'new_consultant_id' => 'required|numeric|exists:consultant,id',
                    'area_city' => 'required|string|exists:designer,area_city'
                ]
            );

            if ($validator->fails()) {
                return ApiResponse::responseData(['status' => false, 'messages' => $validator->errors()->all()]);
            }

            $areaCity = $request->area_city;
            $newConsultantId = $request->new_consultant_id;
            $rooms = ConsultantRoom::where('consultant_id', $request->old_consultant_id)->whereHas(
                'room',
                function ($room) use ($areaCity) {
                    $room->withTrashed()->where('area_city', $areaCity);
                }
            )
                ->select('designer_id');

            $toAttach = $rooms->get()->pluck('designer_id')->toArray();
            $toDelete = Room::select('id')
                ->withTrashed()
                ->whereIn('id', $toAttach)
                ->where(function ($query) {
                    $query->where('is_active', 'false')
                        ->orWhere('expired_phone', true)
                        ->orWhereNotNull('deleted_at');
                })
                ->withTrashed()
                ->get()
                ->pluck('id')
                ->toArray();
            $toAttach = array_diff($toAttach, $toDelete); // Do not include rooms not indexed by ES

            $attachedRooms = ConsultantRoom::where('consultant_id', $newConsultantId)
                ->whereIn('designer_id', $toAttach)
                ->select('designer_id')
                ->get()
                ->pluck('designer_id')
                ->toArray();
            $toAttach = array_diff($toAttach, $attachedRooms); // Do not include rooms already mapped to consultant

            foreach ($toAttach as $attach) {
                $mapping = new ConsultantRoom();
                $mapping->designer_id = $attach;
                $mapping->consultant_id = $newConsultantId;
                $mapping->save();
            }

            $rooms->delete();

            return ApiResponse::responseData(['status' => true]);
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
            return ApiResponse::responseData(['status' => false]);
        }
    }

    /**
     *  Display form to delete property mapping held by a consultant
     *
     * @param Request $request
     * @param int $consultantId
     * @param string $areaCity
     *
     * @return JsonResponse
     */
    public function edit(Request $request, int $consultantId, string $areaCity): View
    {
        $consultant = Consultant::find($consultantId);

        if (is_null($consultant)) {
            abort(404);
        }

        $isBooking = $request->input('booking', '');
        $isFiltered = !empty($isBooking);

        return view(
            'admin.contents.consultant.consultant-room-mapping-edit',
            [
                'boxTitle' => 'Consultant Kost Mapping Detail',
                'consultantName' => $consultant->name,
                'consultantId' => $consultantId,
                'areaCity' => $areaCity,
                'isFiltered' => $isFiltered,
                'isBooking' => $isBooking
            ]
        );
    }

    /**
     *  Delete mapping for a given consultant and property ids
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function destroy(Request $request): JsonResponse
    {
        try {
            $validator = Validator::make(
                $request->all(),
                [
                    'consultant_id' => 'required|numeric|exists:consultant_designer,consultant_id',
                    'property_ids' => 'required|array',
                    'property_ids.*' => 'exists:consultant_designer,designer_id'
                ]
            );

            if ($validator->fails()) {
                return ApiResponse::responseData(['status' => false, 'messages' => $validator->errors()->all()]);
            }

            $this->consultantRoomIndexService->bulkDelete($request->consultant_id, $request->property_ids);

            ConsultantRoom::where('consultant_id', $request->consultant_id)
                ->whereIn('designer_id', $request->property_ids)
                ->delete();

            return ApiResponse::responseData(['status' => true]);
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);

            return ApiResponse::responseData(['status' => false]);
        }
    }

    public function destroyByArea(Request $request)
    {
        try {
            $consultantId = $request->input('consultant');
            $roomIds = ConsultantRoom::where('consultant_id', $request->input('consultant'))
                ->whereIn(
                    'designer_id',
                    function ($query) use ($request) {
                        $query->select('id')
                            ->from('designer')
                            ->where('area_city', $request->input('area_city'));
                    }
                )
                ->select('designer_id')
                ->get()
                ->pluck('designer_id')
                ->toArray();

            $this->consultantRoomIndexService->bulkDelete($consultantId, $roomIds);

            ConsultantRoom::where('consultant_id', $request->input('consultant'))
                ->whereIn(
                    'designer_id',
                    function ($query) use ($request) {
                        $query->select('id')
                            ->from('designer')
                            ->where('area_city', $request->input('area_city'));
                    }
                )
                ->delete();

            return ApiResponse::responseData(['status' => true]);
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);

            return ApiResponse::responseData(['status' => false]);
        }
    }
}
