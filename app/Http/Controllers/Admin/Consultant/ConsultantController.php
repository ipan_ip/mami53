<?php

namespace App\Http\Controllers\Admin\Consultant;

use App\Entities\Consultant\ConsultantMapping;
use App\Entities\Consultant\ConsultantRole;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use SendBird;
use Redirect;
use Auth;
use App\Repositories\UserDataRepository;
use App\User;
use App\Entities\Consultant\Consultant;
use App\Entities\Activity\ActivityLog;
use App\Http\Helpers\UtilityHelper;
use App\Entities\Activity\Action;
use App\Entities\Consultant\ConsultantRoom;

class ConsultantController extends Controller
{
    const CONSULTANT_DEFAULT_PASSWORD = 'mami-consultant-2019';
    const CONSULTANT_DEFAULT_ROLE = 'administrator';
    const PERMISSION_CHAT_CONSULTANT = 'access-consultant-chat-';

    public function __construct(UserDataRepository $userRepository)
    {
        $this->middleware(function ($request, $next) {
            if (!\Auth::user()->can('access-consultant')) {
                return redirect('admin');
            }

            return $next($request);
        });

        $this->userRepository = $userRepository;

        \View::share('user', \Auth::user());
        \View::share('contentHeader', 'Consultants');
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(Request $request)
    {
        // Activity Log
        ActivityLog::LogIndex(Action::CONSULTANT);

        $query = Consultant::with(['user', 'roles'])->orderBy('updated_at', 'desc');

        if ($request->filled('key')) {
            $query->whereHas('user', function($u) use ($request) {
                $u->where('name', 'LIKE', '%' . $request->key . '%');
            });
        }

        $consultants = $query->paginate(20); 
        foreach ($consultants as $key => $row) {
            if (!is_null($row->user) && !is_null($row->user->photo)) {
                $row->photo_url = $row->user->photo->getMediaUrl();
            } else {
                $row->photo_url = null;
            }
        }

        $viewData = [
            'consultants' => $consultants,
            'boxTitle' => 'Consultants',
            'searchUrl' => route('admin.consultant.index'),
        ];

        return view('admin.contents.consultant.index', $viewData);
    }

    public function store(Request $request)
    {
        if (!empty($request->user_id)) {
            $user = User::find($request->user_id);
            if (is_null($user)) {
                return redirect()->back()->with('error_message', 'User not found');
            }
        } else {
            $user = User::where('email', 'like', $request->email)->first();
            if (is_null($user)) {
                $password = Hash::make(md5(self::CONSULTANT_DEFAULT_PASSWORD));
                $role = self::CONSULTANT_DEFAULT_ROLE;
                $user = User::storeLite($request, $password, $role);
            }
            //mark consultant as admin in sendbird meta data
            $user->markAsAdmin();
        }

        $consultant = Consultant::where('user_id', $user->id)->first();
        if ($consultant) {
            return redirect()->back()->with('error_message', 'User already registered');
        }

        $newConsultant = Consultant::register($user, $request);
        if (is_null($newConsultant)) {
            return redirect()->back()->with('error_message', 'Failed created');
        }

        if ($request->hasFile('photo')) {
            $this->storePhoto($request->photo, $user);
        }

        $rolesToCreate = $request->roles;

        foreach ($rolesToCreate as $roleToCreate) {
            $newConsultant->roles()->create([
                'role' => $roleToCreate
            ]);
        }

        // Refresh Cache
        Consultant::refreshCache();

        // Activity Log
        ActivityLog::LogCreate(Action::CONSULTANT, $newConsultant->id);

        return redirect()->back()->with('message', 'Successfully created');
    }

    public function storePhoto($photo, $user)
    {
        $datas = [
            "photo" => $photo,
        ];

        try {
            $uploadPhoto = $this->userRepository->uploadProfilePhoto($datas, $user);

            return $uploadPhoto;
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);

            return null;
        }
    }

    public function update(Request $request, $id)
    {
        $consultant = Consultant::find($id);
        if (is_null($consultant)) {
            return redirect()->back()->with('error_message', 'Consultant not found');
        }

        $user = User::find($consultant->user_id);
        if (is_null($user)) {
            return redirect()->back()->with('error_message', 'Consultant profile not found');
        }

        // Update Consultant Profile
        $consultant->name = $request->name;
        $consultant->email = $request->email;
        $consultant->update();

        if (!is_null($consultant)) {

            $selectedRoles = $request->roles;
//            if(in_array(ConsultantRole::ADMIN_BSE, $selectedRoles) && $consultant->mapping->count() > 5) {
//                return redirect()->back()->with('error_message', 'Failed to set role for admin bse, please check mapping');
//            }

            // Delete existing roles that were not selected
            $toDelete = array_diff($consultant->roles->pluck('role')->toArray(), $selectedRoles);
            foreach ($toDelete as $role) {
                $consultant->roles()->where('role', $role)->delete();
            }

            // Create selected roles that does not exist in db
            $toCreate = array_diff($selectedRoles, $consultant->roles->pluck('role')->toArray());
            foreach ($toCreate as $role) {
                $consultant->roles()->create([
                    'role' => $role
                ]);
            }

            // Refresh Cache
            Consultant::refreshCache();

            // Activity Log
            ActivityLog::LogUpdate(Action::CONSULTANT, $id);

            if ($request->hasFile('photo')) {
                $this->storePhoto($request->photo, $user);
            }

            $photoProfileUrl = !empty($user->photo) ? $user->photo->getMediaUrl()['medium'] : '';
            $sendBird = SendBird::updateUser($consultant->user_id, $consultant->name, $photoProfileUrl);
            if (is_null($sendBird)) {
                return redirect()->back()->with('message', 'Successfully updated but failed updated to SendBird');
            }
            
            return redirect()->back()->with('message', 'Successfully updated');

        }

        return redirect()->back()->with('error_message', 'Failed updated');
    }

    public function destroy($id)
    {
        $consultant = Consultant::find($id);
        if (is_null($consultant)) {
            return redirect()->back()->with('error_message', 'Consultant not found');
        }

        ConsultantMapping::where('consultant_id', '=', $consultant->id)->delete();
        ConsultantRoom::where('consultant_id', $consultant->id)->delete();

        // Prevent error on existing consultant without roles
        if ($consultant->roles()->exists()) {
            $consultant->roles()->delete();
        }

        $consultant->delete();

        // Refresh Cache
        Consultant::refreshCache();

        // Activity Log
        ActivityLog::LogDelete(Action::CONSULTANT, $id);
        
        return redirect()->back()->with('message', 'Successfully deleted');        
    }

    public function getRoomChat($consultantId)
    {
        $consultant = Consultant::find($consultantId);
        if (is_null($consultant)) {
            return redirect()->back()->with('error_message', 'Consultant not found');
        }

        $permissionConsultant = self::PERMISSION_CHAT_CONSULTANT. $consultant->user_id;
        if(!Auth::user()->can($permissionConsultant)) {
            return redirect()->back()->with('error_message', 'You have no access, please check your permission');
        }

        $roomChatUrl = $consultant->getRoomChatUrl();
        
        // Activity Log
        ActivityLog::Log(Action::CONSULTANT, "Open Chat Room Consultant - ".$consultant->user_id);

        if (is_null($roomChatUrl)) {
            return redirect()->back()->with('error_message', 'Failed to open chat room, please check your permission');
        }

        return redirect()->to($roomChatUrl);
    }

    public function getConsultants(Request $request)
    {
        $consultant = Consultant::where('name', 'LIKE', '%' . $request->search . '%')->with('roles')->withCount('mapping')->paginate(10);
        return $consultant;
    }
}
