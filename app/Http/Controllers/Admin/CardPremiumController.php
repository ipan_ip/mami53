<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Media\Media;
use App\Entities\Room\Element\Tag;
use App\Entities\Room\Room;
use App\Http\Controllers\Controller;
use Exception;
use Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
// use Request;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Session;
use App\Entities\Room\Element\Card;
use App\Entities\Room\Element\CardPremium;
use App\Entities\Room\StyleCheckpoint;


class CardPremiumController extends Controller {

    protected $request;
    protected $baseRoute = '';

    protected $layout = 'embed.layouts.main';

    protected $rules = array(
        'admin.stories.style.store' => array(
            'title'      => 'required|min:3|max:30',
        ),
        'admin.stories.style.update' => array(
            'title' => 'required|min:3|max:30',
        ),
        'stories.style.store' => array(
            'title'      => 'required|min:3|max:30',
        ),
        'stories.style.update' => array(
            'title' => 'required|min:3|max:30',
        )
    );


    public function __construct(Request $request)
    {
        $this->request = $request;

        View::share('user' , Auth::user());
        View::share('contentHeader', 'Booking Media Management');
    }

    
    public function index($designerId = null, $is_verified=false)
    {
        if (!is_null($designerId)) 
        {
            $room = Room::find($designerId);
            $roomName = !is_null($room) ? $room->name : '';
        } 

        $cards = CardPremium::with('photo')
            ->where('designer_id', '=', $designerId)
            ->where('type', '<>', 'video');

        if ($is_verified) 
        {
            $cards->where('is_verified', '=', "N");
        }

        $cards = $cards->get();

        if($this->request->is('admin/card/premium')) 
        {
            $$designerId = 0;
        }

        $viewData = array(
            'login_type'  => Auth::user()->role,
            'boxTitle'  => 'Booking Media List: <strong>' . $roomName . '</strong>',
            'designerId' => $designerId,
            'designer' => $room,
            'rowsStyle' => $cards,
            'answerAction'  => "admin.card.premium.get",
            'deleteAction' => "admin.card.premium.destroy",
            'editAction' => "admin.card.premium.edit",
            'createAction' => "admin.card.premium.create",
            'rotateAction' => "admin.card.premium.rotate",
            'coverAction' => "admin.card.premium.cover"
        );

        return View::make('admin.contents.style-premium.index', $viewData);
    }


    public function store($designerId)
    {
        $cards = Input::get('cards');

        if (empty($cards)) {
            return redirect()->route('admin.card.premium.create', $designerId)
                ->with('error_message', 'Aborted')
                ->withInput();
        }

        try {
            CardPremium::register($cards, $designerId);
        } catch (Exception $e) {
            return redirect()->route('admin.card.premium.create', $designerId)
                ->with('error_message', "Aborted. {$e->getMessage()}")
                ->withInput();
        }

        return redirect()->route('admin.card.premium.index', $designerId)
            ->with('message', 'New Card Created');
    }


    public function create($designerId)
    {
        $rowStyle = new CardPremium;
        $rowStyle->title = Input::old('title');
        $rowStyle->gender = Input::old('gender');

        //ambil card
        $cards = Input::old('card');
        if(isset($cards))
        {
            foreach($cards as $card){
                $rowStyle->type       = $card['type'];
                $rowStyle->video_url  = $card['video_url'];
                $rowStyle->photo_id   = $card['photo_id'];
                $rowStyle->description= $card['description'];
                $rowStyle->url_ori    = $card['url_ori'];
                $rowStyle->source     = $card['source'];
                $rowStyle->ordering   = $card['ordering'];
            }
        }
        else
        {
            $rowStyle->type       = NULL;
            $rowStyle->video_url  = NULL;
            $rowStyle->photo_id   = NULL;
            $rowStyle->description= NULL;
            $rowStyle->url_ori    = NULL;
            $rowStyle->source     = NULL;
            $rowStyle->ordering   = NULL;
        }


        $rowStyle->photo_id = Input::old('photo_id');

        if (Input::old('photo_id') !== NULL)
        {
            $media = Media::find(Input::old('photo_id'))->getMediaUrl();
            $photo_id = Input::old('photo_id');
            $photo = $media['medium'];
        }
        else
        {
            $photo_id = 0;
            $photo = NULL;
        }

        for($i=1;$i<=4;$i++){
            if(Input::old('photo_'.$i.'_id')){
                $rowStyle->{'photo_'.$i.'_id'} = Input::old('photo_'.$i.'_id');
                $media = Media::find(Input::old('photo_'.$i.'_id'))->getMediaUrl();
                $photo = $media['medium'];
            } else {
                $photo = null;
                $rowStyle->{'photo_'.$i.'_id'} = 0;
            }
            $rowStyle->{'photo_'.$i} = $photo;
        }

        $rowStyle->photo          = $photo;
        $rowStyle->photo_id       = $photo_id;
        $rowStyle->emotion_ids      = Input::old('emotion_ids');
        $rowStyle->keyword_ids      = Input::old('keyword_ids');

        $rowsGender = array('male', 'female');

        $rowsGenderView = mySelectPreprocessing(
            'single',
            'checked',
            'value',
            $rowsGender,
            $rowStyle->gender
        );

        $rowsEmotion = Tag::where('type', '=', 'emotion')
            ->get();

        $rowsEmotionView = mySelectPreprocessing(
            'multi',
            'selected',
            'key-value',
            $rowsEmotion,
            $rowStyle->emotion_ids
        );

        $rowsKeyword = Tag::where('type', '=', 'keyword')
            ->get();

        $rowsKeywordView = mySelectPreprocessing(
            'multi',
            'selected',
            'key-value',
            $rowsKeyword,
            $rowStyle->keyword_ids
        );

        $viewData = array(
            'boxTitle'    => 'Add Premium Media',
            'login_type'  => Auth::user()->role,
            'designerId'  => $designerId,
            'rowStyle'    => $rowStyle,
            'rowsGender'  => $rowsGenderView,
            'rowsEmotion' => $rowsEmotionView,
            'rowsKeyword' => $rowsKeywordView,
            'formAction'  => route("admin.card.premium.store", $designerId),
            'formMethod'  => 'POST',
            'formCancel'  => route("admin.card.premium.index", $designerId),
        );

        // if $designerId is zero mean this style created by admin, and admin has to choose the designer in form
        // if canceled back to admin page
        if ($designerId==0){
            $viewData['formCancel'] = route("admin.style.premium.index");
            $viewData['rowsDesigner'] = Room::select(DB::Raw('id as designer_id, name'))->get();
        }

        return View::make('admin.contents.style-premium.form', $viewData);
    }


    public function switchStatus(Request $request)
    {
        $room = Room::find($this->request->id);
        $status = (int) $this->request->status;

        if (is_null($room)) 
        {
            return [
                'success' => false,
                'message' => 'Terjadi Error! Silahkan refresh halaman dan coba lagi.'
            ];
        }

        // if it's a ACTIVATE action
        if ($status == 1)
        {
            $activation = $room->activatePremiumPhotos();
            
            if (!$activation['success'])
            {
                return [
                    'success' => false,
                    'message' => $activation['message']
                ];
            }

            return [
                'success' => true,
                'message' => 'Berhasil mengaktifkan foto premium!'
            ];
        } 

        // if it's a DEACTIVATE action
        else
        {
            // get cover_photo ID history from checkpoint
            $checkpoint = StyleCheckpoint::where('designer_id', $room->id)
                ->orderBy('id', 'desc')
                ->first();

            if (is_null($checkpoint))
            {
                return [
                    'success' => false,
                    'message' => 'Gagal menonaktifkan foto premium. Silakan coba set cover foto reguler secara manual, lalu ulangi kembali langkah ini.'
                ];
            }

            // check if cover_photo from checkpoint did have any description changes
            // eg. when reguler cover photo was switched manually by admin while premium photo was active
            $regularCoverPhoto = Card::where('photo_id', $checkpoint->photo_id)->withTrashed()->first();

            if (is_null($regularCoverPhoto))
            {
                return [
                    'success' => false,
                    'message' => 'Gagal mengambil data foto reguler. Silakan refresh halaman dan coba lagi.'
                ];
            }

            // if any changes are really committed or the card as old cover is somehow deleted:
            if (
                ! is_null($regularCoverPhoto->deleted_at) ||
                stripos($regularCoverPhoto->description, '-cover') === false
            ) {
                // then find the referenced cover_photo
                $regularCoverPhotoId = Card::getCoverPhotoId($room);
                if (is_null($regularCoverPhotoId))
                {
                    return [
                        'success' => false,
                        'message' => 'Gagal mengembalikan foto reguler. Silahkan cek apakah Kost/Apt ini mempunyai set foto reguler.'
                    ];
                }

                $checkpoint->photo_id = $regularCoverPhotoId;
            }

            try
            {
                // update room's photo_id
                $room->photo_id = $checkpoint->photo_id;
                $room->save();
                
                // delete checkpoint
                $checkpoint->delete();

                return [
                    'success' => true,
                    'message' => 'Berhasil menonaktifkan foto premium!'
                ];
            }
            catch (Exception $e)
            {
                return [
                    'success' => false,
                    'message' => $e->getMessage()
                ];
            }
        }

    }


    public function edit($designerId, $id)
    {
        $isCover = false;

        $card = CardPremium::where('id', $id)->with('photo')->first();
        if (is_null($card))
        {
            return back()->with('error_message', 'Media tidak ditemukan');
        }
        
        if ($card->type == 'image')
        {
            if (strpos($card->description, '-cover') !== FALSE) $isCover = true;

            if (!is_null($card->photo))
            {
                $media = $card->photo->getMediaUrl();
                $card->photo      = $media['medium'];
                $card->photo_real = $media['real'];
            }
        }

        $viewData = array(
            'boxTitle'    => 'Edit Media Booking',
            'login_type'  => Auth::user()->role,
            'is_cover'    => $isCover,
            'designerId'  => $designerId,
            'rowStyle'    => $card,
            'formAction'  => route("admin.card.premium.update", array($designerId, $card->id)),
            'formMethod'  => 'PUT',
            'formCancel'  => route("admin.card.premium.index", $designerId),
        );

        return view('admin.contents.style-premium.form', $viewData);
    }


    public function update($designerId, $id)
    {
        $cards = Input::get('cards');

        if(empty($cards)){
            return  redirect()->route("admin.card.premium.create", $designerId)
                ->with('error_message', 'Aborted')
                ->withInput();
        }

        try
        {
            CardPremium::updateCards($cards, $designerId, $id);
        }
        catch (Exception $e)
        {
            return  redirect()->route("admin.card.premium.edit", array($designerId, $id))
                ->with('error_message', "Aborted. {$e->getMessage()}")
                ->withInput();
        }

        return  redirect()->route("admin.card.premium.index", array($designerId))
            ->with('message', 'Style Updated');
    }


    public function destroy($designerId, $id)
    {
        CardPremium::destroy($id);

        Room::where('id', $designerId)
            ->update([
                'photo_count' => \DB::raw('photo_count - 1')
            ]);

        return  redirect()->route("admin.card.premium.index", array($designerId))
            ->with('message', 'Style deleted');
    }
    

    public function getRotate($styleId)
    {
        $style = CardPremium::find($styleId);
        $mediaObject = Media::findOrFail($style->photo_id);

        $newPhoto = $mediaObject->rotate();
        if(!is_null($newPhoto)) {
            $style->photo_id = $newPhoto['id'];
            $style->save();
        }

        return redirect()->route('admin.card.premium.index', $style->designer_id);
    }

    public function setCover($cardId)
    {
        $card = CardPremium::find($cardId);
        if (!is_null($card))
        {
            if (!CardPremium::getIsCoverable($card->description))
            {
                return back()->with('error_message', 'Hanya foto kamar yang bisa dijadikan cover photo.');
            }
        }

        $room = Room::find($card->designer_id);
        if (is_null($room))
        {
            return back()->with('error_message', 'Error: Kost tidak ditemukan');
        }
        
        // set whether it should be activated directly,
        // or just switching cover photo
        if ($card->photo_id != $room->photo_id && $room->isPremiumPhoto())
        {
            $room->assignCover('premium', $card->photo_id, true);
        }
        else
        {
            $room->assignCover('premium', $card->photo_id);
        }

        return redirect()->route('admin.card.premium.index', $room->id);
    }
}
