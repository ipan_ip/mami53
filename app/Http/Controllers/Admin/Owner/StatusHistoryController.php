<?php

namespace App\Http\Controllers\Admin\Owner;

use App\Entities\Owner\StatusHistory;
use App\Entities\Room\RoomOwner;
use App\Http\Controllers\Controller;
use App\Presenters\Owner\StatusHistoryPresenter;
use App\Repositories\Owner\StatusHistoryRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StatusHistoryController extends Controller
{
    protected $repository;

    public function __construct(StatusHistoryRepository $repository)
    {
        $this->middleware(function ($request, $next) {
            if (!Auth::user()->can('access-kost')) {
                return redirect('admin');
            }

            return $next($request);
        });

        $this->repository = $repository;

        \View::share('contentHeader', 'Stories Management');
        \View::share('user', Auth::user());
    }

    public function index(Request $request, $ownerId)
    {
        $data = $this->repository
            ->where('designer_owner_id', $ownerId)
            ->with('creator')
            ->orderBy('id', 'DESC')
            ->limit(10)
            ->offset($request->input('offset', 0))
            ->get();

        $data = (new StatusHistoryPresenter(StatusHistoryPresenter::LIST))->present($data);

        $total = StatusHistory::where('designer_owner_id', $ownerId)
            ->count();

        return ['total' => $total, 'data' => $data['data']];
    }
}
