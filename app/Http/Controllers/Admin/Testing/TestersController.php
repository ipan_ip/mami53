<?php

namespace App\Http\Controllers\Admin\Testing;

use App\Entities\Activity\Action;
use App\Entities\Activity\ActivityLog;
use App\Http\Controllers\Controller;
use App\Repositories\UserDataRepository;
use App\Services\UserService;
use Illuminate\Http\Request;
use Validator;

class TestersController extends Controller
{
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->middleware(function ($request, $next) {
            if (!\Auth::user()->can('access-testing')) {
                return redirect('admin');
            }

            return $next($request);
        });

        \View::share('contentHeader', 'Testers Management');
        \View::share('user', \Auth::user());

        $this->userService = $userService;
    }

    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'nullable|numeric',
            'name' => 'nullable|string',
            'phone_number' => 'nullable|numeric',
            'email' => 'nullable|email'
        ]);

        if ($validator->fails()) {
            return redirect('admin/testing/testers')
                        ->withErrors($validator)
                        ->withInput();
        }

        $users = $this->userService->getUsersFromAdmin($request);
        $viewData = [
            'userOwners' => $users,
        ];
        ActivityLog::LogIndex(Action::TESTING);
        return view('admin.contents.testing.testers.index', $viewData);
    }

    public function recyclePhone(Request $request, $id, UserDataRepository $userRepo)
    {
        $user = $userRepo->with('user_verification_account')->where('id', $id)->first();
        $result = $this->userService->recyclePhoneNumber($user);
        if (!$result) {
            return redirect()->action('Admin\Testing\TestersController@index')->with('error_message', 'Failed to recycle User ID ' . $id . '\'s phone number');
        }
        return redirect()->action('Admin\Testing\TestersController@index')->with('message', 'User ID ' . $id . '\'s phone number is recycled');
    }

    public function markAsTester(Request $request, $id, UserDataRepository $userRepo)
    {
        $user = $userRepo->where('id', $id)->first();
        $result = $this->userService->setAsTester($user, $request->input('flag'));
        if (!$result) {
            return redirect()->action('Admin\Testing\TestersController@index')->with('error_message', 'Failed to mark User ID ' . $id . ' as tester');
        }
        $flashMessageVerb = 'unmarked';
        if ($request->input('flag')) {
            $flashMessageVerb = 'marked';
        }
        return redirect()->action('Admin\Testing\TestersController@index')->with('message', 'User ID ' . $id . '  is ' . $flashMessageVerb . ' as tester');
    }
}
