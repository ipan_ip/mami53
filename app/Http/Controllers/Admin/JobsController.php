<?php

namespace App\Http\Controllers\Admin;

use App\Http\Helpers\RegexHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\Entities\Vacancy\Vacancy;
use Illuminate\Support\Facades\Redirect;
use Validator;
use App\Repositories\RoomRepositoryEloquent;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Jenssegers\Agent\Agent;
use App\Entities\Activity\Tracking;
use App\Http\Helpers\ApiHelper;
use App\Libraries\CSVParser;
use App\Libraries\SMSLibrary;
use App\Libraries\AreaFormatter;
use App\Entities\Area\AreaBigMapper;
use DB;
use App\User;
use App\Entities\User\LinkCode;
use Mail;
use App\Entities\Vacancy\Spesialisasi;
use App\Entities\Vacancy\Industry;
use App\Http\Helpers\UtilityHelper;
use App\Entities\Vacancy\CompanyProfile;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;

class JobsController extends Controller
{
    protected $validationMessages = [
        'place_name.required' => 'Nama Tempat Usaha harus diisi',
        'place_name.max' => 'Nama Tempat Usaha maksimal :max karakter',
        'address.max' => 'Alamat maksimal :max karakter',
        'latitude.required' => 'Latitude harus diisi',
        'latitude.numeric' => 'Latitude harus berupa angka',
        'longitude.required' => 'Latitude harus diisi',
        'longitude.numeric' => 'Latitude harus berupa angka',
        'city.max' => 'Kota maksimal :max karakter',
        'subdistrict.max' => 'Subdisctrict maksimal :max karakter',
        'jobs_name.required' => 'Nama Pekerjaan harus diisi',
        'jobs_name.max' => 'Nama Pekerjaan maksimal :max karakter',
        'position.max' => 'Posisi maksimal :max karakter',
        'salary.numeric' => 'Gaji harus berupa angka',
        'user_name.required' => 'Nama Penanggung Jawab harus diisi',
        'user_name.max' => 'Nama Penanggung Jawab maksimal :max karakter',
        'user_phone.required' => 'No. Telp. Penanggung Jawab harus diisi',
        'user_phone.max' => 'No. Telp. Penanggung Jawab maksimal :max karakter',
        'user_email.email' => 'Email Penanggung Jawab tidak valid',
        'agent_name.required' => 'Nama Penginput harus diisi',
        'agent_type.required' => 'Status Penginput harus diisi',
        'expired_date.date_format' => 'Format Tanggal Expired tidak valid (YYYY-MM-DD)'
    ];

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if(!Auth::user()->can('access-jobs')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        View::share('contentHeader', 'Stories Management');
        View::share('user', Auth::user());
    }

    public function index(Request $request)
    {
        $vacancys    = Vacancy::query()->with('vacancy_apply')->orderBy('id','asc');

        if($request->filled('q'))
        {
            $vacancys = $vacancys->where('name','like', '%' . $request->get('q') . '%');
        }

        if ($request->filled('data_input') AND $request->input('data_input') != "0") {
            $data_input = $request->input('data_input');
            $data_input = Vacancy::DATA_INPUT[$data_input];
            $vacancys = $vacancys->where(function($p) use($data_input) {
                $p->where('data_input', $data_input);
                if (in_array($data_input, ["user", "owner"])) {
                    $p->orWhereNull('data_input');
                }
            });
        }

        if ($request->filled('place_name')) {
            $vacancys = $vacancys->where('place_name', 'like', '%'.$request->input('place_name').'%');
        }


        if ($request->filled('job_type') && $request->job_type != '') {
            $vacancys = $vacancys->where('type', $request->job_type);
        }

        if ($request->filled('position')) {
            $vacancys = $vacancys->where('position', 'like', '%'.$request->input('position').'%');
        }

        if ($request->filled('status')) {
            $status = Vacancy::VACANCY_STATUS[$request->input('status')];
            $vacancys = $vacancys->where('status', $status);
        }

        $vacancys    = $vacancys->paginate(20);

        $viewData = array(
            'boxTitle'        => 'Jobs List',
            'vacancys'     => $vacancys,
            'status' => array_merge(['' => 'semua'], array_combine(array_keys(Vacancy::VACANCY_STATUS), Vacancy::VACANCY_STATUS)),
            'data_input' => Vacancy::DATA_INPUT,
            'jobTypes' => array_merge(['' => 'semua'], array_combine(Vacancy::VACANCY_TYPE, Vacancy::VACANCY_TYPE))
        );

        ActivityLog::LogIndex(Action::JOBS);

        return View::make('admin.contents.vacancy.index', $viewData);
    }


    public function getCompany(Request $request)
    {
        $project = CompanyProfile::where('name', 'LIKE', '%'.$request->input('name').'%')->limit(15)->get();

        $status = true;
        if (count($project) < 1) $status = false;

        return array("status" => $status, "data" => $project);
    }

    public function indexCompany(Request $request)
    {
        $company    = CompanyProfile::query()->orderBy('id','desc');

        if($request->filled('q'))
        {
            $company = $company->where('name','like', '%' . $request->get('q') . '%');
        }

        $company    = $company->paginate(20);

        $viewData = array(
            'boxTitle'        => 'Company profile List',
            'company'     => $company,
        );

        return View::make('admin.contents.vacancy.company.index', $viewData);
    }

    public function addNewCompany(Request $request)
    {
        $viewData = array(
            'boxTitle'        => 'Add new company profile',
            'industry' => Industry::list(),
        );

        return View::make('admin.contents.vacancy.company.new', $viewData);
    }

    public function companyEdit(Request $request, $id)
    {
        $company = CompanyProfile::where('id', $id)->first();
        if (is_null($company)) return redirect()->back()->with(['error_message' => "Data tidak ditemukan"]);

        $viewData = array(
            'boxTitle' => 'Company profile edit',
            'data'     => $company,
            'industry' => Industry::list(),
        );

        return View::make('admin.contents.vacancy.company.edit', $viewData);
    }

    public function companyUpdate(Request $request, $id)
    {
        $validator = Validator::make($request->all(),[
            'name'        => 'required',
            'industry'    => 'required',
            'address'     => 'required',
            'latitude'    => 'required',
            'longitude'   => 'required',
            'subdistrict' => 'required',
            'city'        => 'required',
        ], $this->validationMessages);

        if($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors())->withInput($request->all());
        }

        $value = $request->all();
        $city = AreaFormatter::format($value['city']);
        $subdistrict = AreaFormatter::format($value['subdistrict']);
        $areaBigMapper = (new AreaBigMapper)->bigCityMapper($city);

        $company = CompanyProfile::where('id', $id)->first();
        $company->name = $value['name'];
        $company->industry_id = $value['industry'];
        $company->address = $value['address'];
        $company->website = $value['website'];
        $company->size = $value['size'];
        $company->city = $city;
        $company->subdistrict = $subdistrict;
        $company->area_big = $areaBigMapper;
        $company->dresscode = $value["dresscode"];
        $company->language = $value["language"];
        $company->time = $value['time'];
        $company->subsidy = $value['subsidy'];
        $company->description = $value['description'];
        $company->phone = preg_replace(RegexHelper::numericOnly(), "", $value["phone"]);
        $company->email = $value['email'];
        $company->latitude = $value["latitude"];
        $company->longitude = $value["longitude"];
        $company->facilities = $value["facilities"];
        $company->photo_id = isset($value['photo_id']) ? $value['photo_id'] : null;
        $company->agent_name = $value['agent_name'];
        if (isset($value['live'])) {
            $company->is_active = 1;
        }
        //$company->logo = $value["company_logo_url"];
        $company->save();

        ActivityLog::LogUpdate(Action::JOBS, $id);
        
        return redirect('admin/jobs/company')->with(['message', 'Update berhasil']);
    }

    public function addNewPostCompany(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name'        => 'required',
            'industry'    => 'required',
            'address'     => 'required',
            'latitude'    => 'required',
            'longitude'   => 'required',
            'subdistrict' => 'required',
            'city'        => 'required',
            'industry'    => 'required',

        ], $this->validationMessages);

        if($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors())->withInput($request->all());
        }

        $value = $request->all();
        $city = AreaFormatter::format($value['city']);
        $subdistrict = AreaFormatter::format($value['subdistrict']);
        $areaBigMapper = (new AreaBigMapper)->bigCityMapper($city);

        $company = new CompanyProfile();
        $company->name = $value['name'];
        $company->industry_id = $value['industry'];
        $company->address = $value['address'];
        $company->website = $value['website'];
        $company->size = $value['size'];
        $company->city = $city;
        $company->subdistrict = $subdistrict;
        $company->area_big = $areaBigMapper;
        $company->dresscode = $value["dresscode"];
        $company->language = $value["language"];
        $company->time = $value['time'];
        $company->subsidy = $value['subsidy'];
        $company->description = $value['description'];
        $company->phone = preg_replace(RegexHelper::numericOnly(), "", $value["phone"]);
        $company->email = $value['email'];
        $company->latitude = $value["latitude"];
        $company->longitude = $value["longitude"];
        $company->facilities = $value["facilities"];
        $company->agent_name = $value["agent_name"];
        if ($request->filled('photo_id')) $company->photo_id = $value['photo_id'];
        if (isset($value['live'])) {
            $company->is_active = 1;
        }
        //$company->logo = $value["company_logo_url"];
        $company->save();
        return redirect('admin/jobs/company')->with(['message', 'berhasil']);
    }

    public function show($id)
    {
        return back()->withInput();
    }

    public function create(Request $request)
    {
        $agentType = ['scrap'];

        $spesialisasi = Spesialisasi::where('is_active', 1)->orderBy('name', 'asc')->get();

        $viewData = [
            "boxTitle"=> 'Add Job',
            "times"   => Vacancy::SALARY_TIME,
            "status"  => Vacancy::VACANCY_STATUS,
            "type"    => Vacancy::VACANCY_TYPE,
            'educationOptions' => Vacancy::EDUCATION_OPTION,
            'agentType' => $agentType,
            "spesialisasi" => $spesialisasi,
            "industry" => Industry::where('is_active', 1)->get() ,
            'groupOptions' => Vacancy::GROUP_OPTION
        ];

        return  View::make('admin.contents.vacancy.create', $viewData);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'place_name'    => 'required|max:190',
            'address'       => 'max:255',
            'latitude'      => 'required|numeric',
            'longitude'     => 'required|numeric',
            'city'          => 'max:100',
            'subdistrict'   => 'max:100',
            'jobs_name'     => 'required|max:190',
            'position'      => 'max:100',
            'salary'        => 'numeric',
            'salary_time'   => 'nullable',
            'type'          => 'nullable',
            'last_education'=> 'nullable',
            'user_name'     => 'nullable|max:190',
            'user_phone'    => 'nullable|max:190',
            'user_email'    => 'nullable',
            'detail_url'    => 'nullable|url',
            'duplicate'     => 'nullable',
            'expired_date'  => 'required|date_format:Y-m-d',
            'photo_id'      => 'nullable'
        ], $this->validationMessages);

        if($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors())->withInput($request->all());
        }

        $city = AreaFormatter::format($request->city);
        $subdistrict = AreaFormatter::format($request->subdistrict);
        $areaBigMapper = (new AreaBigMapper)->bigCityMapper($city);

        $agent = new Agent();
        $vacancy = new Vacancy;
        $vacancy->place_name    = $request->place_name;
        $vacancy->latitude      = $request->latitude;
        $vacancy->longitude     = $request->longitude;
        $vacancy->city          = $city;
        $vacancy->subdistrict   = $subdistrict;
        $vacancy->area_big      = is_array($areaBigMapper) ? null : $areaBigMapper;
        $vacancy->address       = $request->address;
        $vacancy->workplace     = $request->workplace;
        if (isset($request->workplace_status)) $vacancy->workplace_status = 1;
        else $vacancy->workplace_status = 0;
        $vacancy->name          = $request->jobs_name;
        $vacancy->position      = $request->position;
        $vacancy->type          = $request->type;
        $vacancy->description   = $request->description;
        $vacancy->salary        = $request->salary;
        $vacancy->max_salary    = $request->max_salary;
        $vacancy->salary_time   = $request->salary_time;
        $vacancy->last_education= $request->last_education;
        $vacancy->user_name     = $request->user_name;
        $vacancy->user_phone    = $request->user_phone;
        $vacancy->user_email    = $request->user_email;
        $vacancy->insert_from   = Tracking::checkDevice($agent);
        $vacancy->agent_name    = $request->agent_name;
        $vacancy->verificator   = $request->verificator;
        $vacancy->show_salary   = 1;
        $vacancy->data_input    = "admin";
        $vacancy->status        = 'waiting';
        if ($request->filled('spesialisasi')) $vacancy->spesialisasi_id = $request->input('spesialisasi');
        if ($request->filled('industry')) $vacancy->industry_id = $request->input('industry');
        $vacancy->detail_url    = $request->detail_url;
        $vacancy->apply_other_web = UtilityHelper::checkApplyOtherWeb([
            'user_email' => $request->user_email, 
            'user_phone' => $request->user_phone,
            'detail_url' => $request->detail_url
        ]);
        $vacancy->group         = 'niche';
        $vacancy->expired_date  = $request->expired_date;
        $vacancy->photo_id      = $request->photo_id;

        if ($request->filled('live')) {
            $vacancy->is_active = 1;
            $vacancy->status = 'verified';
        } else {
            $vacancy->is_active = 0;
            $vacancy->status = 'waiting';
        }

        $CheckIsIndex = Vacancy::ValidationIsIndex($vacancy, $vacancy);
    
        if ($CheckIsIndex) {
            $vacancy->is_indexed = 1;
        }
        
        $vacancy->save();

        ActivityLog::LogCreate(Action::JOBS, $vacancy->id);

        if ($request->filled('duplicate') && $request->duplicate == 1) {
            $duplicatedVacancy = $vacancy->replicate();
            $duplicatedVacancy->group = $vacancy->group == 'niche' ? 'general' : 'niche';
            $duplicatedVacancy->slug = SlugService::createSlug(Vacancy::class, 'slug', 
                $duplicatedVacancy->slugable_vacancy);
            $duplicatedVacancy->spesialisasi_id = null;
            $duplicatedVacancy->save();
        }

        return redirect()->route('admin.jobs.index')->with('message',  'Lowongan Kerja berhasil ditambahkan');
    }

    public function verify(Request $request, $id)
    {
        $vacancy = Vacancy::with('user')->where('id', $id)->first();

        if (is_null($vacancy) OR !$request->filled('for')) {
            return  Redirect::route('admin.jobs.index')
                ->with('message', 'Data tidak ditemukan');
        }

        // get area city and subdistrict from lat and long
        $address = $this->getAddress($vacancy);
        
        if ($address) {
            $vacancy->city = $address['city'];
            $vacancy->subdistrict = $address['subdistrict'];
            $vacancy->save();
        }

        if (is_null($vacancy->latitude)) {
            return  Redirect::route('admin.jobs.index')
                ->with('message', 'Lat Long belum ada');
        }

        if (!is_null($vacancy->city)){
            $areaBigMapper = (new AreaBigMapper)->bigCityMapper($vacancy->city);
            $vacancy->area_big = is_array($areaBigMapper) ? null : $areaBigMapper;
            $vacancy->workplace = is_array($areaBigMapper) ? null : $areaBigMapper;
            if (is_null($vacancy->spesialisasi_id)) $vacancy->spesialisasi_id = 80;
            $vacancy->save();
        }


        if ($request->input('for') == 'input_verificator') {
            $viewData = array(
                 "data"    => $vacancy,
                 "status"  => Vacancy::VACANCY_STATUS,
                 "type"    => Vacancy::VACANCY_TYPE,
                 "times"   => Vacancy::SALARY_TIME,
                 "boxTitle"=> 'Jobs Detail',
            );
            return  View::make('admin.contents.vacancy.verificator', $viewData);
        }

        //$slug = strtolower($vacancy->type)."-".SlugService::createSlug(Vacancy::class, 'slug', $vacancy->slug_vacancy);

        if ($request->input('for') == 'verify') $vacancy->status = 'verified';
        else if ($request->input('for') == 'reject') $vacancy->status = 'waiting'; 
        else $vacancy->status = 'reject';
        //$vacancy->slug = $slug;
        if ($request->input('for') == 'verify') $vacancy->is_active = 1;
        if ($request->input('for') == 'reject') $vacancy->is_active = 0;
        
        if ($request->filled('verificator')) $vacancy->verificator = $request->input('verificator');
        
        $CheckIsIndex = Vacancy::ValidationIsIndex($vacancy, $vacancy);
        $vacancy->is_indexed = 1;

        $vacancy->save();

        // offed by https://mamikos.atlassian.net/browse/PMS-671
        // if ($request->input('for') == 'verify') {

        //     if ($vacancy->data_input == 'scrap' AND $vacancy->is_active == 1) {
        //         if (SMSLibrary::validateIndonesianMobileNumber($vacancy->user_phone)) {
        //             // $linked = "https://mamikos.com/lowongan/".$vacancy->slug."?ver=".base64_encode($vacancy->id.",".$vacancy->data_input);
        //             $linked = "https://mamikos.com" . $vacancy->share_slug."?ver=".base64_encode($vacancy->id.",".$vacancy->data_input);
        //             $message = "MAMIKOS: Lowongan pekerjaan anda tampil di Mamikos, silakan buka link ini untuk konfirmasi ".$linked;
        //             SMSLibrary::send($vacancy->user_phone, $message);
        //         }
        //     } 
        // }

        return  Redirect::route('admin.jobs.index')
                ->with('message', 'Data berhasil diedit');
    }

    public function getAddress($vacancy)
    {
        try {
                $url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=".(double) $vacancy->latitude.",".(double) $vacancy->longitude."&sensor=false&key=AIzaSyB8gCZrWOXDcqOmrdNJtci8k1ZNcJ1EZAg";

                // Make the HTTP request
                $data = file_get_contents($url);
                // Parse the json response
                $jsondata = json_decode($data,true);
                //dd($jsondata['status']);
                if ($jsondata['status'] == 'OK') {
                    //$address = [];
                    $add                    = $jsondata['results'][0]['address_components'];
                    $address['city']        = isset($add[2]['long_name']) ? AreaFormatter::format($add[2]['long_name']) : "";
                    $address['subdistrict'] = isset($add[3]['long_name']) ? AreaFormatter::format($add[3]['long_name']) : "";
                    return $address;
                } else {
                    return false;
                }

        }

        //catch exception
        catch(Exception $e) {
          return false;
        }
    }

    public function edit(Request $request, $id)
    {
        $vacancy = Vacancy::where('id', $id)->first();

        if (is_null($vacancy)) {
            return  Redirect::route('admin.jobs.index')
                ->with('message', 'Data tidak ditemukan');
        }

        $spesialisasi = Spesialisasi::where('is_active', 1)->orderBy('group')->get();

        $spesialisasiGrouped = [];

        foreach ($spesialisasi as $spesial) {
            $spesialisasiGrouped[$spesial->group][] = $spesial;
        }

        $viewData = array(
             "data"    => $vacancy,
             "status"  => Vacancy::VACANCY_STATUS,
             "type"    => Vacancy::VACANCY_TYPE,
             "times"   => Vacancy::SALARY_TIME,
             'educationOptions' => Vacancy::EDUCATION_OPTION,
             "boxTitle"=> 'Jobs Detail',
             "spesialisasi" => $spesialisasiGrouped,
             "industry" => Industry::where('is_active', 1)->get(),
             'groupOptions' => Vacancy::GROUP_OPTION
        );

        return  View::make('admin.contents.vacancy.edit', $viewData);
    }

    public function update(Request $request, $id)
    {
        $vacancy = Vacancy::with('user')->where('id', $id)->first();

        if (is_null($vacancy)) {
            return  Redirect::route('admin.jobs.index')
                ->with('message', 'Data tidak ditemukan');
        }

        $validator = Validator::make($request->all(),[
            'place_name'    => 'required|max:190',
            'address'       => 'max:255',
            'latitude'      => 'required|numeric',
            'longitude'     => 'required|numeric',
            'city'          => 'max:100',
            'subdistrict'   => 'max:100',
            'jobs_name'     => 'required|max:190',
            'position'      => 'max:100',
            'salary'        => 'numeric',
            'salary_time'   => 'nullable',
            'type'          => 'nullable',
            'last_education'=> 'nullable',
            'user_name'     => 'nullable|max:190',
            'user_phone'    => 'nullable|max:190',
            'user_email'    => 'nullable',
            'detail_url'    => 'nullable|url',
            'duplicate'     => 'nullable',
            'expired_date'  => 'required|date_format:Y-m-d',
            'photo_id'      => 'nullable'
        ], $this->validationMessages);

        if($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors())->withInput($request->all());
        }

        $city = AreaFormatter::format($request->city);
        $subdistrict = AreaFormatter::format($request->subdistrict);
        $areaBigMapper = (new AreaBigMapper)->bigCityMapper($city);
        
        $vacancy->place_name = $request->input('place_name');
        $vacancy->address    = $request->input('address');
        $vacancy->workplace  = $request->input('workplace');
        if ($request->filled('workplace_status')) $vacancy->workplace_status = 1;
        else $vacancy->workplace_status = 0;
        $vacancy->latitude = $request->input('latitude');
        $vacancy->longitude = $request->input('longitude');
        $vacancy->city       = $city;
        $vacancy->subdistrict= $subdistrict;
        $vacancy->area_big   = is_array($areaBigMapper) ? null : $areaBigMapper;
        $vacancy->salary     = $request->input('salary');
        $vacancy->max_salary = $request->input('max_salary');
        $vacancy->salary_time= $request->input('salary_time');
        $vacancy->last_education = $request->input('last_education');
        $vacancy->name       = $request->input('jobs_name');
        $vacancy->type       = $request->input('type');
        $vacancy->description = $request->input('description');
        $vacancy->user_name = $request->input('user_name');
        $vacancy->user_phone = $request->input('user_phone');
        
        $company_id = (int) $request->input('company_profile');
        if ($company_id > 0) $vacancy->company_profile_id = $company_id;
        
        $vacancy->user_email = $request->input('user_email');
        $vacancy->agent_name = $request->input('agent_name');
        $vacancy->verificator   = $request->input('verificator');
        $vacancy->spesialisasi_id = $request->input('spesialisasi');
        if ($request->filled('industry')) $vacancy->industry_id = $request->input('industry');
        if ($request->filled('indexed')) {
            if ($request->input('indexed') == "on") $index = 1;
            else $index = 0;
            $checkIsIndex = Vacancy::ValidationIsIndex($request, $vacancy);
            $vacancy->is_indexed = $checkIsIndex;
        }

        $vacancy->detail_url = $request->detail_url;
        $vacancy->apply_other_web = UtilityHelper::checkApplyOtherWeb([
            'user_email' => $request->user_email, 
            'user_phone' => $request->user_phone,
            'detail_url' => $request->detail_url,
            'data_input' => $vacancy->data_input
        ]);
        $vacancy->group = 'niche';

        $vacancy->expired_date  = $request->expired_date;
        if ($request->expired_date >= date('Y-m-d')) $vacancy->is_expired = 0;

        $vacancy->photo_id      = $request->photo_id;

        if ($request->filled('live')) {
            $vacancy->status = 'verified';
            $vacancy->is_active = 1;

            // offed by https://mamikos.atlassian.net/browse/PMS-671
            // if ($vacancy->data_input == 'scrap' AND $vacancy->is_active == 1) {
            //     if (SMSLibrary::validateIndonesianMobileNumber($vacancy->user_phone)) {
            //         $linked = "https://mamikos.com/lowongan/".$vacancy->slug."?ver=".base64_encode($vacancy->id.",".$vacancy->data_input);
            //         $message = "MAMIKOS: Lowongan pekerjaan anda tampil di Mamikos, silakan buka link ini untuk konfirmasi ".$linked;
            //         SMSLibrary::send($vacancy->user_phone, $message);
            //     }
            // } 
            
        } else {
            $vacancy->status = "waiting";
            $vacancy->is_active = 0;
        }

        $vacancy->save();

        if ($request->filled('duplicate') && $request->duplicate == 1) {
            $duplicatedVacancy = $vacancy->replicate();
            $duplicatedVacancy->group = $vacancy->group == 'niche' ? 'general' : 'niche';
            $duplicatedVacancy->slug = SlugService::createSlug(Vacancy::class, 'slug', 
                $duplicatedVacancy->slugable_vacancy);
            $duplicatedVacancy->spesialisasi_id = null;
            $duplicatedVacancy->save();
        }
        
        ActivityLog::LogUpdate(Action::JOBS, $id);

        return  Redirect::route('admin.jobs.index')
                ->with('message', 'Berhasil edit');

    }

    public function import(Request $request)
    {
        $viewData = array(
            'boxTitle'     => 'Jobs Import'
        );

        return View::make('admin.contents.vacancy.import', $viewData);
    }

    public function verifySlugVacancy(Request $request, $id)
    {
        Vacancy::verifySlugVacancy($id);

        return redirect()->back()->with('message',"Slug has been Verified");
    }

    public function storeImportedVacancy(Request $request)
    {
        $this->validate($request, [
            'file_csv'=>'required|mimes:csv,txt'
        ]);

        $csv = $request->file_csv->path();

        $csvArray = CSVParser::csv_parse($csv, ['eol'=>"\r\n"]);

        $agent = new Agent();
        $agent = Tracking::checkDevice($agent);

        foreach ($csvArray AS $key => $value) {
            $vacancy = new Vacancy;
            $vacancy->place_name    = $value['place_name'];
            if (isset($value['latitude'])) $vacancy->latitude      = $value['latitude'];
            if (isset($value['longitude'])) $vacancy->longitude     = $value['longitude'];
            if (isset($value['area_city'])) $vacancy->city = ApiHelper::wordCheckCityOrDistrict($value['area_city']);
            if (isset($value['subdistrict'])) $vacancy->subdistrict   = ApiHelper::wordCheckCityOrDistrict($value['subdistrict']);
            $vacancy->address       = $value['address'];
            
            if (isset($value['workplace'])) $vacancy->workplace     = $value['workplace'];
            if (isset($value['workplace_status'])) $vacancy->workplace_status = $value['workplace_status'];

            $vacancy->name          = $value['jobs_name'];
            if (isset($value['agent_name'])) $vacancy->agent_name = $value['agent_name'];
            if (isset($value['position'])) $vacancy->position      = $value['position'];
            $vacancy->type          = $value['type'];
            $vacancy->description   = $value['description'];
            $vacancy->salary        = $value['salary'];
            $vacancy->max_salary    = $value['max_salary'];
            $vacancy->salary_time   = strtolower($value['salary_time']);
            $vacancy->user_name     = $value['user_name'];
            $vacancy->user_phone    = $value['user_phone'];
            $vacancy->user_email    = $value['user_email'];
            $vacancy->insert_from   = $agent;
            $vacancy->data_input    = "scrap koran";
            $vacancy->show_salary   = 1;
            $vacancy->is_active     = 0;
            $vacancy->status        = 'waiting';
            $vacancy->save();
        }
        return redirect()->back()->with('message',"Import success");
    }
    
    public function delete(Request $request, $id)
    {
        $vacancy = Vacancy::where('id', $id)->delete();

        ActivityLog::LogDelete(Action::JOBS, $id);

        return  Redirect::route('admin.jobs.index')
                ->with('message', 'Berhasil hapus');
    }

    public function createAccount(Request $request, $id)
    {
        $vacancy = Vacancy::where('id', $id)
                    ->first();

        $link = ApiHelper::random_string('alpha', 25);

        $viewData = array(
            'boxTitle'     => 'Account Detail',
            'user_phone'   => $vacancy->user_phone,
            'user_email'   => $vacancy->user_email,
            'link'         => $link
        );

        return view('admin.contents.vacancy.create-account', $viewData);        
    }
    public function storeVacancyAccount(Request $request, $id)
    {
        $vacancy = Vacancy::find($id);
        if (!is_null($vacancy->user_id) && $vacancy->user_id != 4) {
            return redirect()->route('admin.jobs.index')->with('error_message', "User sudah tersedia");
        }

        $checkPhoneNumber = SMSLibrary::validateIndonesianMobileNumber($request->input('user_phone'));
          
        $user = User::where('is_owner', 'true')
              -> where (function ($query) use ($request, $checkPhoneNumber) {
                if ($checkPhoneNumber){
                    $query =  $query->where('phone_number', $request->input('user_phone'));
                }
                if ($request->user_email != ""){
                    $query = $query->orWhere('email', $request->input('user_email'));
                }
            });

        $user = $user->count();

        if ($user > 0) return redirect()->route('admin.jobs.index')->with('error_message', "User sudah tersedia"); 

        DB::beginTransaction();

        $user = new User();
        $user->name         = $request->input('user_phone');
        $user->phone_number = $request->input('user_phone');
        $user->email        = $request->input('user_email');
        $user->is_verify    = 0;
        $user->is_owner     = 'true';
        $user->role         = 'user';
        $user->save();

        $LinkCode = array(
             "user_id" => $user->id,
             "link"    => $request->input('link'),
             "for"     => 'create_pass',
             "is_active" => 1
        );

        LinkCode::create($LinkCode);

        $vacancy->user_id = $user->id;
        $vacancy->save();

        DB::commit();

        if ($checkPhoneNumber) {
            $message = "MAMIKOS: No anda sudah terdaftar di akun Mamikos. Selanjutnya setting password anda di https://mamikos.com/p/".$request->input('link');
            SMSLibrary::send($request->input('user_phone'), $message, 'infobip', $user->id);
        } 
        else {
            $email = $request->input('user_email');
            Mail::send('emails.create-account-job', 
                [
                    'link'     => 'https://mamikos.com/p/'.$request->input('link'), 
                    'recipientName'=>$user->email
                ], function($message) use ($email)
            {
                $message->to($email)->subject('Setting Password di Akun Mamikos');
            });
        }
        return redirect()->route('admin.jobs.index')->with('message', "Berhasil membuat akun owner");
    }

    public function deleteCompany($id)
    {
        $company = CompanyProfile::with(["vacancy"])->where('id', $id)->first();
        if (is_null($company)) {
            return redirect()->back()->with(["message" => "Data tidak ditemukan"]);
        }
        $company->vacancy()->delete();
        $company->delete();

        return redirect()->back()->with(["message" => "Sukses hapus perusahaan"]);
    }

    public function redirectPage(Request $request, $vacancyId)
    {
        $vacancy = Vacancy::where('id', $vacancyId)->first();
        if (is_null($vacancy)) {
            return back()->with('message', 'Vacancy tidak ditemukan');
        }

        $viewData    = array(
            'boxTitle' => 'Vacancy redirect',
            'vacancy' => $vacancy
        ); 

        return view ("admin.contents.vacancy.redirect", $viewData);
    }

    public function storeRedirectData(Request $request, $vacancyId)
    {   
        $vacancy = Vacancy::where('id', $vacancyId)->first();
        if (empty($request->input('destination_id'))) {
            $vacancy->redirect_id = null;
            $vacancy->save();
            return redirect('admin/jobs')->with('message', 'Sukses update');
        }
        $vacancyDestination = Vacancy::where('id', $request->input('destination_id'))->first();
        if (is_null($vacancy) or is_null($vacancyDestination)) {
            return back()->with('error_message', 'Data lowongan tidak ditemukan');
        }

        $vacancy->redirect_id = $vacancyDestination->id;
        $vacancy->save();

        return redirect('admin/jobs')->with('message', 'Sukses update');
    }

}
