<?php

namespace App\Http\Controllers\Admin\MamiPay;

use App\Entities\Mamipay\MamipayOwner;
use Illuminate\Http\Request;
use App\Libraries\SMSLibrary;

use App\Http\Controllers\Controller;
use App\Entities\Mamipay\MamipayBankAccount;
use App\Entities\Mamipay\MamipayBankCity;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;

class MamipayOwnerController extends Controller
{
    protected $htmlBuilder;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            // TODO : separate kost owner and kost agent
            if(!\Auth::user()->can('access-kost-owner') && !\Auth::user()->can('access-kost-agent')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        \View::share('user', \Auth::user());
        \View::share('contentHeader', 'MamiPAY Management');
    }

    public function index()
    {
        $rowsOwner = MamipayOwner::orderBy('created_at','desc')->paginate(20);

        $viewData = [
            'rowsOwner'    => $rowsOwner,
            'boxTitle'     => 'MamiPAY Owner Management',
        ];

        ActivityLog::LogIndex(Action::MAMIPAY_OWNER);

        return view('admin.contents.mamipay-owner.index', $viewData);
    }

    public function add()
    {
        $bankList = MamipayBankAccount::where('source','flip')->get();     

        $viewData = [
            'boxTitle'  => 'Add Owner MamiPAY',
            'bankList'  => $bankList,
        ];

        return view('admin.contents.mamipay-owner.add', $viewData);
    }

    public function addSubmit(Request $request)
    {
        try {
            $result = MamipayOwnerAdminUtil::addMamipayOwner($request);

            if ($result->errorMsg->count()) {
                return back()->withErrors($result->errorMsg);
            }
        } catch (\Exception $e) {
            return back()->with('error_message',$e->getMessage());
        }

        return redirect()->route('admin.mamipay-owner.index')->with('message','Owner added!');
    }

    public function searchCity(Request $request)
    {
       $cityList = MamipayBankCity::where('source','flip')->where('city_name','like','%'.$request->input('city_name').'%')->limit(15)->get()->map(function($q){
            return [
                'id' => $q->city_code,
                'label' => $q->city_name,
                'value' => $q->city_name,
            ];
       });
       return response()->json($cityList);
    }

    public function approve(Request $request,$id)
    {
        $mamipayOwner = MamipayOwner::find($id);

        if($mamipayOwner){
            $mamipayOwner->status = MamipayOwner::STATUS_APPROVED;
            $mamipayOwner->save();
        }else{
            return back()->with('error_message','User not found!');
        }

        $smsData = array(
            'number'=>$mamipayOwner->user->phone_number,
            'message'=>'Selamat! Akun MamiPAY anda telah diversifikasi dan disetujui oleh tim MamiKos. Silakan masuk melalui aplikasi.'
        );

        SMSLibrary::smsVerification($smsData['number'], $smsData['message']);

        return back()->with('message','Mamipay '.$mamipayOwner->user->phone_number.' has been approved.');
        
    }
    
    public function reject(Request $request,$id)
    {
        $mamipayOwner = MamipayOwner::find($id);
        if($mamipayOwner){
            $mamipayOwner->status = MamipayOwner::STATUS_REJECTED;
            $mamipayOwner->save();
        }else{
            return back()->with('error_message','User not found!');
        }

        /*$smsData = array(
            'number'=>$mamipayOwner->user->phone_number,
            'message'=>'Akun mamipay Anda ditolak.'
        );

        SMSLibrary::smsVerification($smsData['number'], $smsData['message']);*/
        
        return back()->with('message','MamiPAY '.$mamipayOwner->user->phone_number.' has been rejected.');
        
    }

}

?>