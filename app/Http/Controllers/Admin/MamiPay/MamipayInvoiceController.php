<?php

namespace App\Http\Controllers\Admin\MamiPay;

use App\Entities\Activity\View;
use App\Entities\Mamipay\MamipayInvoice;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Libraries\SMSLibrary;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Transformers\Mamipay\AdminInvoiceTransformer;

use Validator;


class MamipayInvoiceController extends Controller
{
    private $filter = [];

    protected $bankList = array(
        "mandiri" => "Bank Mandiri",
        "bri" => "BRI (Bank Rakyat Indonesia)",
        "bni" => "BNI (Bank Negara Indonesia) & BNI Syariah",
        "bca" => "BCA (Bank Central Asia)",
        "bsm" => "Bank Syariah Mandiri",
        "cimb" => "CIMB Niaga & CIMB Niaga Syariah",
        "muamalat" => "Muamalat",
        "danamon" => "Bank Danamon",
        "permata" => "Bank Permata",
        "bii" => "Maybank Indonesia",
        "panin" => "Panin Bank",
        "uob" => "UOB Indonesia",
        "ocbc" => "OCBC NISP",
        "citibank" => "Citibank",
        "artha" => "Bank Artha Graha Internasional",
        "tokyo" => "Bank of Tokyo Mitsubishi UFJ",
        "dbs" => "DBS Indonesia",
        "standard_chartered" => "Standard Chartered Bank",
        "capital" => "Bank Capital Indonesia",
        "anz" => "ANZ Indonesia",
        "boc" => "Bank of China (Hong Kong) Limited",
        "bumi_arta" => "Bank Bumi Arta",
        "hsbc" => "HSBC Indonesia",
        "rabobank" => "Rabobank International Indonesia",
        "mayapada" => "Bank Mayapada",
        "bjb" => "BJB",
        "dki" => "Bank DKI Jakarta",
        "daerah_istimewa" => "BPD DIY",
        "jawa_tengah" => "Bank Jateng",
        "jawa_timur" => "Bank Jatim",
        "jambi" => "Bank Jambi",
        "sumut" => "Bank Sumut",
        "sumatera_barat" => "Bank Sumbar (Bank Nagari)",
        "riau_dan_kepri" => "Bank Riau Kepri",
        "sumsel_dan_babel" => "Bank Sumsel Babel",
        "lampung" => "Bank Lampung",
        "kalimantan_selatan" => "Bank Kalsel",
        "kalimantan_barat" => "Bank Kalbar",
        "kalimantan_timur" => "Bank Kaltim",
        "kalimantan_tengah" => "Bank Kalteng",
        "sulselbar" => "Bank Sulselbar",
        "sulut" => "Bank SulutGo",
        "nusa_tenggara_barat" => "Bank NTB",
        "bali" => "BPD Bali",
        "nusa_tenggara_timur" => "Bank NTT",
        "maluku" => "Bank Maluku",
        "papua" => "Bank Papua",
        "bengkulu" => "Bank Bengkulu",
        "sulawesi" => "Bank Sulteng",
        "sulawesi_tenggara" => "Bank Sultra",
        "nusantara_parahyangan" => "Bank Nusantara Parahyangan",
        "india" => "Bank of India Indonesia",
        "mestika_dharma" => "Bank Mestika Dharma",
        "sinarmas" => "Bank Sinarmas",
        "maspion" => "Bank Maspion Indonesia",
        "ganesha" => "Bank Ganesha",
        "icbc" => "ICBC Indonesia",
        "qnb_kesawan" => "QNB Indonesia",
        "btn" => "BTN (Bank Tabungan Negara)",
        "woori" => "Bank Woori Saudara",
        "tabungan_pensiunan_nasional" => "BTPN",
        "bri_syr" => "BRI (Bank Rakyat Indonesia) Syariah",
        "bjb_syr" => "BJB Syariah",
        "mega" => "Bank Mega",
        "bukopin" => "Bukopin",
        "jasa_jakarta" => "Bank Jasa Jakarta",
        "hana" => "KEB Hana Bank Indonesia",
        "mnc_internasional" => "Bank MNC Internasional",
        "agroniaga" => "BRI Agroniaga",
        "sbi_indonesia" => "SBI Indonesia",
        "royal" => "Bank Royal Indonesia",
        "nationalnobu" => "Nobu (Nationalnobu) Bank",
        "mega_syr" => "Bank Mega Syariah",
        "ina_perdana" => "Bank Ina Perdana",
        "sahabat_sampoerna" => "Bank Sahabat Sampoerna",
        "kesejahteraan_ekonomi" => "Bank Kesejahteraan Ekonomi",
        "bca_syr" => "BCA (Bank Central Asia) Syariah",
        "artos" => "Bank Artos Indonesia",
        "mayora" => "Bank Mayora Indonesia",
        "index_selindo" => "Bank Index Selindo",
        "victoria_internasional" => "Bank Victoria International",
        "agris" => "Bank Agris",
        "chinatrust" => "CTBC (Chinatrust) Indonesia",
        "commonwealth" => "Commonwealth Bank"
    );

    public function __construct(Request $request)
    {
        $this->middleware(function ($request, $next) {
            // TODO : separate kost owner and kost agent
            if(!\Auth::user()->can('access-kost-owner') && !\Auth::user()->can('access-kost-agent')) {
                return redirect('admin');
            }
            
            return $next($request);
        });
        
        $this->filter = $request->all();

        \View::share('user', \Auth::user());
        \View::share('contentHeader', 'MamiPAY Management');
    }

    public function index()
    {
        $viewData = $this->getPaidInvoice();

        ActivityLog::LogIndex(Action::MAMIPAY_PAID_INVOICE);

        return view('admin.contents.mamipay-owner.invoice', $viewData);
    }

    public function store()
    {
        if(!empty($this->filter['csv_type'])){
            if($this->filter['csv_type']=='flip'){
                $viewsData = $this->getPaidInvoice(false);
                return $this->bigFLipCSV($viewsData['rowExport']);
            }elseif($this->filter['csv_type']=='bni'){
                
            }
        }
    }

    public function getPaidInvoice($paginate=true)
    {
        
        $rowsInvoiceQuery = MamipayInvoice::where('status','paid')->orderBy('paid_at','desc');

        if(!empty($this->filter)){
            if(!empty($this->filter['paid_from']) && !empty($this->filter['paid_to'])){
                $from = date($this->filter['paid_from']);
                $to = date($this->filter['paid_to']);
                $rowsInvoiceQuery = $rowsInvoiceQuery->whereBetween('paid_at', [$from, $to]);
            }
        }

        if($paginate){
            $rowsInvoice = $rowsInvoiceQuery->paginate(20);
        }else{
            $rowsInvoice = $rowsInvoiceQuery->get();
        }

        $transformer =  new AdminInvoiceTransformer;

        $rows = array();
        $rowExport = array();
        $number = 0;

        foreach($rowsInvoice as $invoice){
            $number+=1;
            $row =  $transformer->transform($invoice);
            $rows[] = $row;

            $bankName = $row['bank_name'];
            
            if(!isset($this->bankList[strtolower($bankName)])){
                $matches = array_filter($this->bankList, function($var) use ($bankName) { return preg_match("/\b$bankName\b/i", $var); });
                if($matches){
                    $bankKeys = array_keys($matches);
                    $bankName = reset($bankKeys);
                }
            }else{
                $bankName = strtolower($bankName);
            }

            $rowExport[]=array(
                'no' => $number,
                'bank_tujuan' => $bankName,
                'nomor_rekening_tujuan' => $row['bank_account_number'],
                'nama_penerima' => $row['bank_account_owner'],
                'nominal' => $row['total_paid_amount'],
                'berita_transfer' => $row['invoice_number'],
            );
        }

        $viewData = [
            'activeFilter' => $this->filter,
            'rowExport' => $rowExport,
            'rowsEntity' => $rowsInvoice,
            'rows'    => $rows,
            'boxTitle'     => 'MamiPAY Invoice Management',
        ];

        return $viewData;
    }

    public function bigFLipCSV($dataTransact)
    {
        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=csv_transaksi_flip_".strtotime("now").".csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );

        $columns = array('No','Bank Tujuan','Nomor Rekening Tujuan','Nominal','Berita Transfer','Email Penerima (Opsional)','Nama Penerima (Opsional)');
             
        $callback = function() use ($dataTransact, $columns)
        {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach($dataTransact as $transact) {
                fputcsv($file, array($transact['no'],$transact['bank_tujuan'],$transact['nomor_rekening_tujuan'],$transact['nominal'],$transact['berita_transfer'],'',''));
            }

            fclose($file);
        };
        return response()->stream($callback, 200, $headers);
    }
}

?>