<?php

namespace App\Http\Controllers\Admin\MamiPay;

use App\Entities\Activity\Action;
use App\Entities\Activity\ActivityLog;
use App\Entities\Mamipay\MamipayOwner;
use App\Libraries\SMSLibrary;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use stdClass;

class MamipayOwnerAdminUtil
{
    /**
     * Common validation and process to add mamipay owner.
     *
     * @param Request $request
     * @return stdClass
     */
    public static function addMamipayOwner(Request $request): stdClass
    {
        $result = new stdClass;
        $requiredField = array(
            'owner_phone_number',
            'owner_name',
            'bank_account_owner',
            'bank_name',
            'bank_account_number'
        );

        $errorInformation = array(
            'owner_phone_number' => 'Owner phone number is required',
            'owner_name' => 'Owner name is required',
            'bank_account_owner' => 'Account name is required',
            'bank_name' => 'Bank name is required',
            'bank_account_number' => 'Account number is required'
        );

        $errorMsg = new MessageBag();

        $validated = array();
        foreach ($request->all() as $k => $value) {
            $validated[$k] = $value;
            if (in_array($k, $requiredField) && empty($value)) {
                $errorMsg->add($k, $errorInformation[$k]);
            }
        }

        if ($validated['owner_phone_number']) {
            $phoneFormatValid = SMSLibrary::validateDomesticIndonesianMobileNumber($validated['owner_phone_number']);
            if (!$phoneFormatValid) {
                $errorMsg->add('owner_phone_number', 'Owner phone number not valid');
            }

            $userByPhone = User::where('phone_number', $validated['owner_phone_number'])->where('is_owner', 'true')->first();

            if (!$userByPhone) {
                $errorMsg->add('owner_phone_number', 'Phone number not registered');
            } else {
                $existingOwner = MamipayOwner::where('user_id', $userByPhone->id)->first();

                if ($existingOwner) {
                    $errorMsg->add('owner_phone_number', 'Phone number already registered as MamiPAY owner');
                }
            }
        }

        $result->errorMsg = $errorMsg;

        if ($errorMsg->count()) {
            return $result;
        }

        $newOwner = new MamipayOwner;
        $newOwner->user_id = $userByPhone->id;
        $newOwner->status = 'approved';
        $newOwner->role = 'pemilik';
        $newOwner->bank_name = $validated['bank_name'];
        $newOwner->bank_account_number = $validated['bank_account_number'];
        $newOwner->bank_account_owner = $validated['bank_account_owner'];
        $newOwner->bank_account_city = $validated['bank_account_city'];

        $newOwner->save();

        ActivityLog::LogCreate(Action::MAMIPAY_OWNER, $newOwner->id);

        return $result;
    }
}
