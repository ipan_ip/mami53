<?php

namespace App\Http\Controllers\Admin\MamiPay;

use App\Entities\Activity\View;
use App\Entities\Mamipay\MamipayOwner;
use App\Entities\Mamipay\MamipayTenant;
use App\Entities\Media\Media;
use App\Entities\User\UserVerificationAccount;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Libraries\SMSLibrary;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Validator;

class MamipayUserVerificationIdentityController extends Controller
{
    protected $htmlBuilder;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if(!\Auth::user()->can('access-kost-owner') && !\Auth::user()->can('access-kost-agent')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        \View::share('user', \Auth::user());
        \View::share('contentHeader', 'MamiPAY Management');
    }

    public function index()
    {
        $mamipayTenants = MamipayTenant::with(['photoIdentifier', 'photoDocument', 'user'])->paginate(20);


        $viewData   = [
            'mamipayTenants' => $mamipayTenants,
        ];

        // dd($userTenant);
        // return $mamipayTenants;
        return view('admin.contents.mamipay-user.index', $viewData);
    }
    
}
