<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Slider\Slider;
use App\Entities\Slider\SliderPage;
use App\Http\Helpers\ApiResponse as Api;
use App\Http\Helpers\RegexHelper;
use Auth;
use App\Http\Controllers\Controller;
use Bugsnag;
use Exception;
use Illuminate\Http\Request;
use RuntimeException;
use View;

class SliderController extends Controller
{
    public const STATUS_ACTIVE = 'ACTIVE';
    public const STATUS_INACTIVE = 'NON ACTIVE';

    public function __construct()
    {
        $this->middleware(
            function ($request, $next)
            {
                if (!Auth::user()->can('access-slider')) {
                    return redirect('admin');
                }

                return $next($request);
            }
        );

        View::share('contentHeader', 'Slider Management');
        View::share('user', Auth::user());
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function index()
    {
        $viewData = [
            'boxTitle'  => 'Slider Control Panel',
            'searchUrl' => route('admin.slider.index'),
        ];

        return view('admin.contents.slider.index', $viewData);
    }

    /**
     * Getting all sliders data required by Bootstrap Table
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTableData(Request $request)
    {
        $searchQuery = $request->query('search');
        $sortBy      = $request->query('sort') === "" ? "id" : $request->query('sort');
        $sortOrder   = $request->query('order', 'desc');
        $offset      = (int)$request->query('offset');
        $limit       = (int)$request->query('limit');

        $sliders = Slider::getAjaxData($searchQuery, $sortBy, $sortOrder, $offset, $limit);

        if ($sliders->count() > 0) {
            foreach ($sliders as $slide) {
                // Active status
                if ($slide->is_active === 1) {
                    $slide->status = self::STATUS_ACTIVE;
                } else {
                    $slide->status = self::STATUS_INACTIVE;
                }

                // Generate endpoint URL
                $slide->endpoint = '/slide/' . $slide->endpoint;

                // Generate creator name
                $slide->creator_name = $slide->creator->name;

                // Last updated date
                $slide->last_updated = date('j F Y', strtotime($slide['updated_at']));
            }
        }

        return response()->json(
            [
                "data" => $sliders,
            ]
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array|bool[]
     */
    public function store(Request $request)
    {
        try {
            $data = [
                'name'     => $request->name,
                'endpoint' => $request->endpoint,
                'user_id'  => auth()->id(),
            ];

            if (!Slider::store($data)) {
                throw new RuntimeException('Failed saving slider data.');
            }

            return [
                'success' => true,
            ];
        } catch (Exception $e) {
            return [
                'success' => false,
                'message' => $e->getMessage(),
                'error'   => $e,
            ];
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function activate(Request $request)
    {
        try {
            $activate = Slider::changeStatus($request->all());

            if (!$activate) {
                return [
                    'success' => false,
                    'message' => 'Failed activating slider data. Please refresh page and try again.',
                ];
            }

            return [
                'success' => true,
                'title'   => 'Slider successfully activated',
                'message' => "Page will be refreshed after you clicked OK",
            ];
        } catch (Exception $e) {
            Bugsnag::notifyException($e);

            return [
                'success' => false,
                'message' => $e->getMessage(),
                'error'   => $e,
            ];
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function deactivate(Request $request)
    {
        try {
            $deactivate = Slider::changeStatus($request->all());

            if (!$deactivate) {
                throw new RuntimeException('Failed deactivating slider data. Please refresh page and try again.');
            }
        } catch (Exception $e) {
            Bugsnag::notifyException($e);

            return [
                'success' => false,
                'message' => $e->getMessage(),
            ];
        }

        return [
            'success' => true,
            'title'   => 'Slider successfully deactivated',
            'message' => "Page will be refreshed after you clicked OK",
        ];
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function remove(Request $request)
    {
        try {
            $slider = Slider::find((int)$request->id);

            if (is_null($slider)) {
                throw new RuntimeException('Failed fetching slider data. Please refresh page and try again.');
            }

            $slider->delete();
        } catch (Exception $e) {
            Bugsnag::notifyException($e);

            return [
                'success' => false,
                'message' => $e->getMessage(),
            ];
        }

        return [
            'success' => true,
            'title'   => 'Slider successfully removed',
            'message' => "Page will be refreshed after you clicked OK",
        ];
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function verify(Request $request)
    {
        try {
            // Allow only alphanumeric and spaces
            $isValidCharacters = preg_match(RegexHelper::alphanumericAndSpace(), $request->name) === 1;
            if (!$isValidCharacters) {
                throw new RuntimeException('Please use alphanumeric and space only!');
            }

            if (!Slider::validateName($request->name)) {
                throw new RuntimeException('This name already in use. Please use another name!');
            }

            $generatedEndpoint = Slider::generateEndpoint($request->name);

            return Api::responseData(
                [
                    'data' => [
                        'name'     => strtoupper(trim($request->name)),
                        'endpoint' => $generatedEndpoint,
                    ],
                ]
            );
        } catch (Exception $e) {
            Bugsnag::notifyException($e);

            return Api::responseError(
                $e->getMessage(),
                'BAD_REQUEST',
                401
            );
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array|bool[]
     */
    public function storePage(Request $request)
    {
        try {
            if (!SliderPage::store($request->all())) {
                throw new RuntimeException('Failed saving slide data.');
            }

            return [
                'success' => true,
            ];
        } catch (Exception $e) {
            return [
                'success' => false,
                'message' => $e->getMessage(),
                'error'   => $e,
            ];
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function removePage(Request $request)
    {
        try {
            $slide = SliderPage::find((int)$request->id);

            if (is_null($slide)) {
                throw new RuntimeException('Failed fetching slide data. Please refresh page and try again.');
            }

            $slide->delete();
        } catch (Exception $e) {
            Bugsnag::notifyException($e);

            return [
                'success' => false,
                'message' => $e->getMessage(),
            ];
        }

        return [
            'success' => true,
            'title'   => 'Slide page successfully removed',
            'message' => "Page will be refreshed after you clicked OK",
        ];
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function activatePage(Request $request)
    {
        try {
            $activate = SliderPage::changeStatus($request->all());

            if (!$activate) {
                throw new RuntimeException('Failed activating slide data. Please refresh page and try again.');
            }
        } catch (Exception $e) {
            Bugsnag::notifyException($e);

            return [
                'success' => false,
                'message' => $e->getMessage(),
                'error'   => $e,
            ];
        }

        return [
            'success' => true,
            'title'   => 'Slide page successfully activated',
            'message' => "Page will be refreshed after you clicked OK",
        ];
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function deactivatePage(Request $request)
    {
        try {
            $deactivate = SliderPage::changeStatus($request->all());

            if (!$deactivate) {
                throw new RuntimeException('Failed deactivating slide data. Please refresh page and try again.');
            }
        } catch (Exception $e) {
            Bugsnag::notifyException($e);

            return [
                'success' => false,
                'message' => $e->getMessage(),
            ];
        }

        return [
            'success' => true,
            'title'   => 'Slide page successfully deactivated',
            'message' => "Page will be refreshed after you clicked OK",
        ];
    }

    public function setCoverPage(Request $request)
    {
        try {
            $saveSorting = SliderPage::setAsCover($request->all());

            if (!$saveSorting) {
                throw new RuntimeException('Failed updating sorting order. Please refresh page and try again!');
            }
        } catch (Exception $e) {
            return [
                'success' => false,
                'message' => $e->getMessage(),
            ];
        }

        return [
            'success' => true,
            'title'   => 'New sorting order successfully updated',
            'message' => "Page will be refreshed after you clicked OK",
        ];
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array|bool[]
     */
    public function orderPage(Request $request)
    {
        $sortData = $request->all();

        try {
            $saveSorting = SliderPage::saveOrder($sortData);

            if (!$saveSorting) {
                throw new RuntimeException('Failed updating sorting order. Please refresh page and try again!');
            }
        } catch (Exception $e) {
            return [
                'success' => false,
                'message' => $e->getMessage(),
            ];
        }

        return [
            'success' => true,
            'title'   => 'New sorting order successfully updated',
            'message' => "Page will be refreshed after you clicked OK",
        ];
    }
}
