<?php

namespace App\Http\Controllers\Admin\Polling;

use App\Entities\Polling\Polling;
use App\Entities\Polling\PollingQuestion;
use App\Http\Controllers\Controller;
use App\Repositories\Polling\PollingQuestionRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PollingQuestionController extends Controller
{
    protected $repo;

    public function __construct(PollingQuestionRepository $pollingRepo)
    {
        $permissionError = 'You dont have permission to this page "'.request()->path().'".';
        $this->middleware(function ($request, $next) use ($permissionError) {
            if (!$request->user()->can(['access-polling'])) {
                return redirect('admin')->withErrors($permissionError);
            }

            return $next($request);
        });

        $this->repo = $pollingRepo;

        View::share('contentHeader', 'Polling Question Management');
        View::share('user', Auth::user());
    }

    public function index()
    {
        $questions = $this->repo->getQuestionPaginated(20, request()->all());
        $pollingDropdown = ['all' => 'Polling: All'] + Polling::getAsDropdown();
        $questionTypeDropdown = ['all' => 'Type: All'] + PollingQuestion::getTypesDropdown();
        $pageTitle = 'Manage Question';
        $data = [
            'boxTitle' => $pageTitle,
            'pageTitle' => $pageTitle,
            'questions' => $questions,
            'pollingDropdown' => $pollingDropdown,
            'questionTypeDropdown' => $questionTypeDropdown,
        ];

        return view('admin.contents.polling.question.index', $data);
    }

    public function create()
    {
        $pollingDropdown = Polling::getAsDropdown();
        $questionTypeDropdown = PollingQuestion::getTypesDropdown();
        $pageTitle = 'Add Question';
        $data = [
            'boxTitle' => $pageTitle,
            'pageTitle' => $pageTitle,
            'pollingDropdown' => $pollingDropdown,
            'questionTypeDropdown' => $questionTypeDropdown,
        ];

        return view('admin.contents.polling.question.create', $data);
    }

    public function store(Request $request)
    {
        $question = $this->createOrUpdate($request->all());
        // validation fails
        if ($question instanceof RedirectResponse) {
            return $question;
        }

        return redirect()->route('admin.polling.question.index')
            ->with('message', 'Polling Question created successfully.');
    }

    public function edit($id)
    {
        $question = $this->repo->getQuestionById($id);
        $questionTypeDropdown = PollingQuestion::getTypesDropdown();
        $pageTitle = 'Edit Question';
        $data = [
            'boxTitle' => $pageTitle,
            'pageTitle' => $pageTitle,
            'question' => $question,
            'questionTypeDropdown' => $questionTypeDropdown,
        ];

        return view('admin.contents.polling.question.edit', $data);
    }

    public function update($id, Request $request)
    {
        $question = $this->repo->getQuestionById($id);
        if (!$question) {
            abort(404);
        }

        $question = $this->createOrUpdate($request->all(), $question->id);
        // validation fails
        if ($question instanceof RedirectResponse) {
            return $question;
        }

        return redirect()
            ->route('admin.polling.question.index')
            ->with('message', 'Polling Question updated successfully.');
    }

    public function destroy($id)
    {
        $question = $this->repo->getQuestionById($id);
        if (!$question) {
            abort(404);
        }

        $this->repo->deleteQuestion($id);

        return redirect()
            ->route('admin.polling.question.index')
            ->with('message', 'Polling Question has been deleted successfully.');
    }

    private function createOrUpdate(array $data, $id = null)
    {
        $questionTypes = array_keys(PollingQuestion::getTypesDropdown());
        $validator = Validator::make($data, [
            'polling_id' => 'required',
            'question' => 'required',
            'type' => 'required|in:'. implode(',', $questionTypes),
        ], [], [
            'polling_id' => 'Polling',
            'question' => 'Question',
            'type' => 'Type',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        return $id
            ? $this->repo->updateQuestion($id, $data)
            : $this->repo->createQuestion($data);
    }
}
