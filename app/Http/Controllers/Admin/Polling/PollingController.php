<?php

namespace App\Http\Controllers\Admin\Polling;

use App\Entities\Polling\Polling;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use App\Repositories\Polling\PollingRepository;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PollingController extends Controller
{
    protected $repo;

    public function __construct(PollingRepository $pollingRepo)
    {
        $permissionError = 'You dont have permission to this page "'.request()->path().'".';
        $this->middleware(function ($request, $next) use ($permissionError) {
            if (!$request->user()->can(['access-polling'])) {
                return redirect('admin')->withErrors($permissionError);
            }

            return $next($request);
        });

        $this->repo = $pollingRepo;

        View::share('contentHeader', 'Polling Management');
        View::share('user', Auth::user());
    }

    public function index()
    {
        $pollings = $this->repo->getPollingPaginated(20, request()->all());
        $pageTitle = 'Manage Polling';
        $data = [
            'boxTitle' => $pageTitle,
            'pageTitle' => $pageTitle,
            'pollings' => $pollings,
        ];

        return view('admin.contents.polling.polling.index', $data);
    }

    public function create()
    {
        $pageTitle = 'Add Polling';
        $data = [
            'boxTitle' => $pageTitle,
            'pageTitle' => $pageTitle,
        ];

        return view('admin.contents.polling.polling.create', $data);
    }

    public function store(Request $request)
    {
        $polling = $this->createOrUpdate($request->all());
        // validation fails
        if ($polling instanceof RedirectResponse) {
            return $polling;
        }

        return redirect()->route('admin.polling.index')
            ->with('message', 'Polling created successfully.');
    }

    public function edit($id)
    {
        $polling = $this->repo->getPollingById($id);
        $pageTitle = 'Edit Polling: '. $polling->key;
        $data = [
            'boxTitle' => $pageTitle,
            'pageTitle' => $pageTitle,
            'polling' => $polling
        ];

        return view('admin.contents.polling.polling.edit', $data);
    }

    public function update($id, Request $request)
    {
        $polling = $this->repo->getPollingById($id);
        if (!$polling) {
            abort(404);
        }

        $polling = $this->createOrUpdate($request->all(), $polling->id);
        // validation fails
        if ($polling instanceof RedirectResponse) {
            return $polling;
        }

        return redirect()
            ->route('admin.polling.index')
            ->with('message', 'Polling updated successfully.');
    }

    public function destroy($id)
    {
        $polling = $this->repo->getPollingById($id);
        if (!$polling) {
            abort(404);
        }

        $this->repo->deletePolling($id);

        return redirect()
            ->route('admin.polling.index')
            ->with('message', 'Polling '. $polling->name .' deleted successfully.');
    }

    private function createOrUpdate(array $data, $id = null)
    {
        $ignoreId = $id ?: 'NULL';
        $pollingUnique = (new Polling)->getTable() .',key,'. $ignoreId;
        $validator = Validator::make($data, [
            'key' => 'required|alpha_dash|max:50|unique:'. $pollingUnique,
            'descr' => 'max:255',
        ], [], [
            'key' => 'Polling Name',
            'descr' => 'Polling Description',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        return $id
            ? $this->repo->updatePolling($id, $data)
            : $this->repo->createPolling($data);
    }
}
