<?php

namespace App\Http\Controllers\Admin\Polling;

use App\Entities\Polling\Polling;
use App\Entities\Polling\PollingQuestion;
use App\Entities\Polling\PollingQuestionOption;
use App\Http\Controllers\Controller;
use App\Repositories\Polling\UserPollingRepository;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserPollingController extends Controller
{
    protected $repo;

    public function __construct(UserPollingRepository $repo)
    {
        $permissionError = 'You dont have permission to this page "'.request()->path().'".';
        $this->middleware(function ($request, $next) use ($permissionError) {
            if (!$request->user()->can(['access-polling'])) {
                return redirect('admin')->withErrors($permissionError);
            }

            return $next($request);
        });

        $this->repo = $repo;

        View::share('contentHeader', 'User Polling Management');
        View::share('user', Auth::user());
    }

    public function index()
    {
        $userDropdown = ['all' => 'User: All'] + $this->getUserDropdown();
        $questionDropdown = ['all' => 'Question: All'] + PollingQuestion::getOptionableQuestion();
        $userPollings = $this->repo->getUserPaginated(20, request()->all());
        $pageTitle = 'User Polling';
        $data = [
            'boxTitle' => $pageTitle,
            'pageTitle' => $pageTitle,
            'userDropdown' => $userDropdown,
            'questionDropdown' => $questionDropdown,
            'userPollings' => $userPollings,
        ];

        return view('admin.contents.polling.user.index', $data);
    }

    private function getUserDropdown()
    {
        return User::has('user_pollings')
            ->get()
            ->pluck('name', 'id')
            ->toArray();
    }
}
