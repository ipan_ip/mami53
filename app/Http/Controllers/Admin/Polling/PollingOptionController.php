<?php

namespace App\Http\Controllers\Admin\Polling;

use App\Entities\Polling\Polling;
use App\Entities\Polling\PollingQuestion;
use App\Entities\Polling\PollingQuestionOption;
use App\Http\Controllers\Controller;
use App\Repositories\Polling\PollingOptionRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PollingOptionController extends Controller
{
    protected $repo;

    public function __construct(PollingOptionRepository $repo)
    {
        $permissionError = 'You dont have permission to this page "'.request()->path().'".';
        $this->middleware(function ($request, $next) use ($permissionError) {
            if (!$request->user()->can(['access-polling'])) {
                return redirect('admin')->withErrors($permissionError);
            }

            return $next($request);
        });

        $this->repo = $repo;

        View::share('contentHeader', 'Polling Option Management');
        View::share('user', Auth::user());
    }

    public function index()
    {
        $options = $this->repo->getOptionPaginated(20, request()->all());
        $questionDropdown = ['all' => 'Question: All'] + PollingQuestion::getOptionableQuestion();
        $pageTitle = 'Manage Question Option';
        $data = [
            'boxTitle' => $pageTitle,
            'pageTitle' => $pageTitle,
            'options' => $options,
            'questionDropdown' => $questionDropdown,
        ];

        return view('admin.contents.polling.option.index', $data);
    }

    public function create()
    {
        $questionDropdown = PollingQuestion::getOptionableQuestion();
        $settingDropdown = PollingQuestionOption::getSettingDropdown();
        $pageTitle = 'Add Option';
        $data = [
            'boxTitle' => $pageTitle,
            'pageTitle' => $pageTitle,
            'questionDropdown' => $questionDropdown,
            'settingDropdown' => $settingDropdown,
        ];

        return view('admin.contents.polling.option.create', $data);
    }

    public function store(Request $request)
    {
        $option = $this->createOrUpdate($request->all());
        // validation fails
        if ($option instanceof RedirectResponse) {
            return $option;
        }

        return redirect()->route('admin.polling.option.index')
            ->with('message', 'Polling Option created successfully.');
    }

    public function edit($id)
    {
        $option = $this->repo->getOptionById($id);
        $settingDropdown = PollingQuestionOption::getSettingDropdown();
        $pageTitle = 'Edit Question';
        $data = [
            'boxTitle' => $pageTitle,
            'pageTitle' => $pageTitle,
            'option' => $option,
            'settingDropdown' => $settingDropdown,
        ];

        return view('admin.contents.polling.option.edit', $data);
    }

    public function update($id, Request $request)
    {
        $option = $this->repo->getOptionById($id);
        if (!$option) {
            abort(404);
        }

        $option = $this->createOrUpdate($request->all(), $option->id);
        // validation fails
        if ($option instanceof RedirectResponse) {
            return $option;
        }

        return redirect()
            ->route('admin.polling.option.index')
            ->with('message', 'Polling Option updated successfully.');
    }

    public function destroy($id)
    {
        $option = $this->repo->getOptionById($id);
        if (!$option) {
            abort(404);
        }

        $this->repo->delete($id);

        return redirect()
            ->route('admin.polling.option.index')
            ->with('message', 'Polling Option has been deleted successfully.');
    }

    private function createOrUpdate(array $data, $id = null)
    {
        $validator = Validator::make($data, [
            'question_id' => 'required',
            'option' => 'required',
        ], [], [
            'question_id' => 'Question',
            'option' => 'Option',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        return $id
            ? $this->repo->updateOption($id, $data)
            : $this->repo->createOption($data);
    }
}
