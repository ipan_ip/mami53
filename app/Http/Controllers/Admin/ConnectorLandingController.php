<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\Landing\LandingConnector;
use Auth;
use View;
use Validator;
use Illuminate\Validation\Rule;

class ConnectorLandingController extends Controller
{
	public function __construct()
    {
        View::share('contentHeader', 'Connector Landing Management');
        View::share('user', Auth::user());
    }

	public function index(Request $request)
    {
        $rowsLanding    = LandingConnector::orderBy('id','desc');

        if($request->filled('q'))
        {
            $rowsLanding->where('slug','like', '%' . $request->get('q') . '%');
        }

        $rowsLanding    = $rowsLanding->paginate(10);
        $viewData = array(
            'boxTitle'        => 'Landing Connector',
            'rowsLanding'     => $rowsLanding,
        );

        return View::make('admin.contents.landing_connector.index', $viewData);
    }
}