<?php

namespace App\Http\Controllers\Admin;

use App\Http\Helpers\ApiHelper;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Entities\Config\AppConfig;
use Illuminate\Support\Facades\View;
use Auth;
use App\Entities\Room\Room;
use App\Entities\Room\RoomFilter;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;

class MitulaController extends Controller
{
    private $filterCountPair = [
        'mitula_area_1'=>'mitula_area_1_count',
        'mitula_area_2'=>'mitula_area_2_count',
        'mitula_area_3'=>'mitula_area_3_count',
        'mitula_area_4'=>'mitula_area_4_count',
        'mitula_area_5'=>'mitula_area_5_count'
    ];

    public function __construct() 
    {
        $this->middleware(function ($request, $next) {
            if(!Auth::user()->can('access-mitula-config')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        View::share('contentHeader', 'Stories Management');
        View::share('user', Auth::user());
    }

    public function config(Request $request)
    {

        $filterCountPair = $this->filterCountPair;

        $filters = AppConfig::whereIn('name', array_keys($filterCountPair))->get()->toArray();
        $outputCounts = AppConfig::whereIn('name', array_values($filterCountPair))->get()->toArray();

        $areas = Room::selectRaw('DISTINCT TRIM(area_city) as area')
                        ->whereNotNull('area_city')
                        ->where('area_city', '<>', '')
                        ->where('area_city', '<>', '-')
                        ->orderBy('area')
                        ->get()
                        ->pluck('area')
                        ->toArray();

        $boxTitle = 'Mitula Configuration';

        ActivityLog::LogIndex(Action::MITULA_CONFIG);

        return view('admin.contents.mitula.config', compact('filters', 'outputCounts', 'boxTitle', 'filterCountPair', 'areas'));
    }

    public function configUpdate(Request $request)
    {
        $validationRules = [];

        $i = 1;
        foreach ($this->filterCountPair as $key => $value) {
            if($i == 1) {
                $validationRules[$key] = 'required';
            }
            
            $validationRules[$value] = 'required_with:' . $key . '|numeric';

            $i++;
        }

        $this->validate($request, $validationRules);

        foreach($this->filterCountPair as $filter => $filterCount) {
            AppConfig::where('name', $filter)
                    ->update(['value'=>$request->input($filter)]);

            AppConfig::where('name', $filterCount)
                    ->update(['value'=>$request->input($filterCount)]);
        }

        return  redirect(url('admin/mitula/config'))
                ->with('message', 'Configuration saved');
    }

    public function preview(Request $request)
    {
        $filtersArea = AppConfig::whereIn('name', array_keys($this->filterCountPair))->get()->toArray();
        $outputCounts = AppConfig::whereIn('name', array_values($this->filterCountPair))->get()->toArray();

        $totalOutput = 0;
        foreach($outputCounts as $output) {
            if(is_null($output['value'])) continue;
            $totalOutput += (int)$output['value'];
        }

        if($totalOutput > 100) {
            return view('admin.contents.mitula.preview', ['rooms'=>[]]);
        }

        $filters =  [];
        $filters['rent_type'] = 2;
        $filters['disable_sorting'] = true;

        $roomFilter = new RoomFilter($filters);

        $i = 0;
        $rooms= [];
        foreach($this->filterCountPair as $filterName => $outputCount) {
            if(!is_null($filtersArea[$i]['value']) && $filtersArea[$i]['value'] != '') {

                $rooms[] = $roomFilter
                            ->doFilter(new Room)
                            ->where('area_city', 'like', '%' . $filtersArea[$i]['value'] . '%')
                            ->where('room_available', '>', 0)
                            ->with(['cards', 'cards.photo'])
                            ->orderBy('kost_updated_date', 'desc')
                            ->take($outputCounts[$i]['value'])
                            ->get();
            }

            $i++;
        }

        return view('admin.contents.mitula.preview', compact('rooms'));
    }
}
