<?php

namespace App\Http\Controllers\Admin;

use App\Http\Helpers\ApiHelper;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Entities\Config\AppConfig;
use Illuminate\Support\Facades\View;
use Auth;
use App\Entities\Activity\ChatAdmin;
use App\Entities\Consultant\Consultant;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;

class ChatController extends Controller
{

    private $adminNames;
    const PERMISSION_ACCESS_CHAT = 'access-chat';
    const PERMISSION_CHAT_ADMIN_CS = 'access-chat-';
    const PERMISSION_CHAT_CONSULTANT = 'access-consultant-chat-';
    
    public function __construct()
    {
        View::share('contentHeader', 'Stories Management');
        View::share('user', Auth::user());
    }

    public function index()
    {
        if(!Auth::user()->can($this::PERMISSION_ACCESS_CHAT)) return redirect('admin');

        // $chatAdmins = ChatAdmin::getAdminIdAndDisplayNames();
        $chatAdmins = [];
        $consultants = Consultant::all();

        $viewData = [
            'chatAdmins' => $chatAdmins,
            'consultants' => $consultants,
            'boxTitle' => 'Chat Admin and Consultant',
        ];

        ActivityLog::LogIndex(Action::CHAT_ROOM);

        return view('admin.contents.chat.index', $viewData);
    }

    public function chatConfig(Request $request)
    {
        if(!Auth::user()->can('access-cs-status')) return redirect('admin');

        $adminIds = ChatAdmin::getAdminIds();
        $keyNames = [];
        foreach($adminIds as $adminId) {
            $keyNames[] = 'cs_' . $adminId . '_on';
        }

        $configs = AppConfig::whereIn('name', $keyNames)
                        ->orderByRaw('FIELD(name, "' . implode('", "', $keyNames) . '")')
                        ->get()->toArray();

        $viewData = [
            'boxTitle' => 'CS Online Status',
            'configs' => $configs,
            'adminName' => ChatAdmin::getAdminIdAndDisplayNames()
        ];

        ActivityLog::LogIndex(Action::CS_STATUS);

        return view('admin.contents.chat.config', $viewData);
    }

    public function chatConfigUpdate(Request $request)
    {
        if(!Auth::user()->can('access-cs-status')) return redirect('admin');

        // $csNames = ['ines', 'ivana', 'sisca'];
        $csIds = ChatAdmin::getAdminIds();

        // foreach($csNames as $csName) {
        foreach($csIds as $csId) {
            if($request->filled('cs_' . $csId . '_on') && $request->input('cs_' . $csId . '_on') == 1) {
                AppConfig::where('name', 'cs_' . $csId . '_on')
                        ->update(['value'=>1]);
            } else {
                AppConfig::where('name', 'cs_' . $csId . '_on')
                        ->update(['value'=>0]);
            }
        }

        return  redirect(url('admin/chat/config'))
                ->with('message', 'Configuration saved');
    }
}
