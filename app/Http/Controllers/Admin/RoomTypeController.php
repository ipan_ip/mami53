<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Room\Element\Type;
use App\Presenters\RoomPresenter;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;

class RoomTypeController extends Controller
{
	protected $indexRoute = 'admin.room.type.index';
	protected $itemPerPage = 20;

	/**
	 * RoomTypeController constructor.
	 */
	public function __construct()
	{
		$this->middleware(function ($request, $next)
		{
			if (!Auth::user()->can('access-kost'))
			{
				return redirect('admin');
			}

			return $next($request);
		});

		View::share('contentHeader', 'Room Types Management');
		View::share('user', Auth::user());
	}

	public function index(Request $request, int $roomId)
	{
		$query = Type::with(['room', 'facilities', 'facility_setting', 'units'])
			->withCount('units')
			->withCount('facility_setting')
			->where('designer_id', $roomId);

		// Filter
		$query = $this->filter($request, $query);

		$rowsRoom = $query->paginate($this->itemPerPage);

		$rooms = (new RoomPresenter('type'))->present($rowsRoom);

		$viewData = [
			'boxTitle'     => 'Room Type Management',
			'rowsDesigner' => $rowsRoom,
			'rowsRoom'     => $rooms['data'],
			'parentRoomId' => $roomId,
			'searchUrl'    => url()->route($this->indexRoute, $roomId),
		];

		return view("admin.contents.stories.type", $viewData);
	}

	private function filter(Request $request, $query)
	{
		return $query;
	}
}
