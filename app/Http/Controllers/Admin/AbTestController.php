<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Activity\Action;
use App\Entities\Activity\ActivityLog;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\AbTest\ControlRequest;
use App\Services\AbTest\DashboardAPIService;
use Illuminate\Support\Facades\View;
use Auth;

class AbTestController extends Controller
{
    protected $dashboardAPIService;

    public function __construct(DashboardAPIService $dashboardAPIService)
    {
        $this->dashboardAPIService = $dashboardAPIService;
    }

    public function control(ControlRequest $request)
    {
        ActivityLog::Log(Action::AB_TEST, json_encode($request->toArray()));
        return $this->dashboardAPIService->post($request);
    }

    public function index(ControlRequest $request)
    {
        View::share('contentHeader', 'A/B Testing Management');
        View::share('user', Auth::user());
        
        return view("admin.contents.abtest.index");
    }
}