<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Premium\Bank;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Http\Helpers\ApiHelper;

class BankController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if(!Auth::user()->can('access-bank')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        View::share('contentHeader', 'Stories Management');
        View::share('user', Auth::user());
    }

   
    public function index()
    {
       $rowsBank = Bank::orderBy('id', 'desc')->get();
        $viewData = array(
            'boxTitle'     => 'Account Bank List',
            'deleteAction' => 'admin.tag.destroy',
            'rowsBank'      => $rowsBank,
        );

        ActivityLog::LogIndex(Action::BANK);

        return View::make('admin.contents.bank.index', $viewData);
    }

    public function create()
    {
        $bank = new Bank;

        $bank->name = Input::old('name');
        $bank->type = Input::old('type');
        $bank->parent_name = Input::old('parent_name');
        $bank->latitude_1 = Input::old('latitude_1');
        $bank->longitude_1 = Input::old('longitude_1');
        $bank->latitude_2 = Input::old('latitude_2');
        $bank->longitude_2 = Input::old('longitude_2');

        $viewData = array(
            'boxTitle'    => 'Insert Bank',
            'bank'      => $bank,
            'rowsActive' => array('1', '0'),
            'formAction'  => 'admin.bank.store',
            'formMethod'  => 'POST',
        );

        return View::make('admin.contents.bank.form', $viewData);

    }

    public function store(Request $request)
    {

    	$this->validate($request, array(
            'name'        => 'required',
            'account_name'=> 'required',
            'number'      => 'required',
            'is_active'   => 'required'
        ));

    	$isSuccess = Bank::create($request->input());
        if ($isSuccess) {
            ActivityLog::LogCreate(Action::BANK, $isSuccess->id);
      
            return  Redirect::route('admin.bank.index')
                ->with('message', 'New Area Created');
      
        } else {

            return  Redirect::route('admin.bank.create')
                ->with('error_message', 'Aborted')
                ->withInput();
        }

    }

    public function edit($id)
    {
        $bank = Bank::find($id);

        $viewData = array(
            'boxTitle'    => 'View or Edit Bank',
            'bank'        => $bank,
            'rowsActive'  => array('1', '0'),
            'formAction'  => array('admin.bank.update', $id),
            'formMethod'  => 'PUT',
        );
        return View::make('admin.contents.bank.form', $viewData);
    }

    public function update(Request $request, $id)
    {
    	$this->validate($request, array(
            'name'        => 'required',
            'account_name'=> 'required',
            'number'      => 'required',
            'is_active'   => 'required'
        ));

        $bank   = Bank::find($id);

    	$bank->name         = $request->input('name');
    	$bank->account_name  = $request->input('account_name');
    	$bank->number   = $request->input('number');
    	$bank->is_active  = $request->input('is_active');
    	$isSuccess = $bank->save();

        if ($isSuccess) {
            ActivityLog::LogUpdate(Action::BANK, $id);
            return  Redirect::route('admin.bank.index')
                ->with('message', 'New Area Created');
      
        } else {

            return  Redirect::route('admin.bank.create')
                ->with('error_message', 'Aborted')
                ->withInput();
        }

    }

    public function destroy($id)
    {
        Bank::destroy($id);

        ActivityLog::LogDelete(Action::BANK, $id);

        return  Redirect::route('admin.bank.index')
            ->with('message', 'Area deleted');
    }	

}
