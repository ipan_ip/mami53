<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\Landing\LandingVacancy;
use Auth;
use View;
use Validator;
use Illuminate\Validation\Rule;
use App\Libraries\CSVParser;
use App\Entities\Vacancy\Vacancy;
use App\Entities\Vacancy\Spesialisasi;
use App\Http\Helpers\UtilityHelper;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;

class JobsLandingController extends Controller
{

	protected $validationMessages = [
        'slug.required'=>'Slug harus diisi',
        'slug.alpha_dash' => 'Slug hanya boleh mengandung huruf, angka, dan dash (-)',
        'slug.max'=> 'Slug maksimal :max karakter',
        'slug.unique' => 'Slug sudah dipakai sebelumnya',
        'heading_1.required' => 'Heading 1 harus diisi',
        'heading_1.unique' => 'Heading 1 sudah dipakai sebelumnya',
        'heading_1.max' => 'Heading 1 maksimal :max karakter',
        'heading_2.max' => 'Heading 2 maksimal :max karakter',
        'keyword.required' => 'Keyword harus diisi',
        'keyword.max' => 'Keyword maksimal :max karakter',
        'latitude_1.required' => 'Latitude 1 harus diisi',
        'latitude_1.numeric' => 'Latitude 1 harus berupa angka',
        'longitude_1.required' => 'Longitude 1 harus diisi',
        'longitude_1.numeric' => 'Longitude 1 harus berupa angka',
        'latitude_2.required' => 'Latitude 2 harus diisi',
        'latitude_2.numeric' => 'Latitude 2 harus berupa angka',
        'longitude_2.required' => 'Longitude 2 harus diisi',
        'longitude_2.numeric' => 'Longitude 2 harus berupa angka',
        'price_min.numeric' => 'Harga Minimal harus berupa angka',
        'price_max.numeric' => 'Harga Maksimal harus berupa angka',
        'photo_cover.numeric' => 'Foto Cover harus berupa angka'
    ];

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if(!Auth::user()->can('access-landing-jobs')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        View::share('contentHeader', 'Jobs Landing Management');
        View::share('user', Auth::user());
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $rowsLanding    = LandingVacancy::with('parent')->orderByRaw(\DB::raw('IFNULL(redirect_id, 0) asc'));

        if($request->filled('q') && $request->get('q') != '')
        {
            $rowsLanding->where('slug','like', '%' . $request->get('q') . '%');
        }

        if ($request->filled('niche') && $request->get('niche') != '') {
            $rowsLanding->where('is_niche', $request->get('niche') == 'true' ? 1 : 0);
        }

        if ($request->filled('sort')) {
            $field = $request->get('sort');
            $direction = $request->filled('sort_dir') ? $request->get('sort_dir') : 'asc';

            $rowsLanding = $rowsLanding->orderBy($field, $direction);
        } else {
            $rowsLanding = $rowsLanding->orderBy('id', 'desc');
        }

        $rowsLanding    = $rowsLanding->paginate(10);

        $sortUrl = [
            'view_count' => UtilityHelper::modifyQueryStringForSorting($request->fullUrl(), 'view_count', 'array')
        ];

        parse_str(parse_url($request->fullUrl(), PHP_URL_QUERY), $currentQueryString);

        $viewData = array(
            'boxTitle'        => 'Landing List',
            'rowsLanding'     => $rowsLanding,
            'sortUrl'         => $sortUrl,
            'currentQueryString' => $currentQueryString
        );

        ActivityLog::LogIndex(Action::LANDING_JOBS);

        return View::make('admin.contents.landing_vacancy.index', $viewData);
    }

        /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $parentOptions = LandingVacancy::get();

        $viewData   = [
            'boxTitle'          => 'New Landing',
            'parentOptions'     => $parentOptions,
            'type'              => Vacancy::VACANCY_TYPE,
            'educationOptions'  => Vacancy::EDUCATION_OPTION,
            'spesialisasiOptions' => Spesialisasi::Active()->get(),
        ];
//dd($viewData);
        return View::make('admin.contents.landing_vacancy.create', $viewData);

    }

    public function store(Request $request)
    {       
        $validator = Validator::make($request->all(), 
                        [
                            'slug'=>'required|max:250|unique:landing_vacancy,slug',
                            'heading_1'=>'required|max:250|unique:landing_vacancy,heading_1',
                            'heading_2'=>'max:250',
                            'keyword'=>'required|max:250',
                            'latitude_1' => 'required|numeric',
                            'longitude_1' => 'required|numeric',
                            'latitude_2' => 'required|numeric',
                            'longitude_2' => 'required|numeric',
                            'price_min' => 'numeric',
                            'price_max' => 'numeric',
                        ], $this->validationMessages);

        if($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors());
        }

        $jobsLanding = new LandingVacancy;

        $jobsLanding->slug           = preg_replace('/\--+/', '-', str_replace(' ', '-', preg_replace('/\s+/', ' ',trim($request->get('slug')))));
        $jobsLanding->parent_id      = $request->get('parent_id') ? $request->get('parent_id') :null;
        $jobsLanding->heading_1      = $request->get('heading_1');
        $jobsLanding->heading_2      = $request->get('heading_2');
        $jobsLanding->latitude_1 	 = $request->get('latitude_1');
        $jobsLanding->longitude_1    = $request->get('longitude_1');
        $jobsLanding->latitude_2     = $request->get('latitude_2');
        $jobsLanding->longitude_2    = $request->get('longitude_2');
        $jobsLanding->price_min      = $request->get('price_min');
        $jobsLanding->price_max      = $request->get('price_max');
        $jobsLanding->keyword        = $request->get('keyword');
        $jobsLanding->description    = $request->get('description');
        $jobsLanding->type           = $request->get('type_landing');
        $jobsLanding->last_education = $request->get('last_education');
        $jobsLanding->is_niche       = $request->get('is_niche');
        $jobsLanding->spesialisasi_id= $request->get('spesialisasi');   
        $jobsLanding->save();

        ActivityLog::LogCreate(Action::LANDING_JOBS, $jobsLanding->id);

        return  redirect()->route('admin.jobs-landing.index')
            ->with('message', 'Landing Page successfully created');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $jobsLanding = LandingVacancy::find($id);

        if(!$jobsLanding) {
            return redirect()->back()->with('error_message', 'Landing tidak ditemukan');
        }

        $parentOptions = LandingVacancy::where('id', '<>', $id)->get();

        $photo = null;


        $viewData   = [
            'boxTitle'          => 'Edit Landing Jobs',
            'jobsLanding'       => $jobsLanding,
            //'photo'             => $photo,
            'parentOptions'     => $parentOptions,
            'type'              => Vacancy::VACANCY_TYPE,
            'educationOptions'  => Vacancy::EDUCATION_OPTION,
            'spesialisasiOptions' => Spesialisasi::Active()->get(),
        ];

        return View::make('admin.contents.landing_vacancy.edit', $viewData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $jobsLanding = LandingVacancy::find($id);

        if(!$jobsLanding) {
            return redirect()->back()->with('error_message', 'Landing tidak ditemukan');
        }

        $validator = Validator::make($request->all(), 
                        [
                            'slug'=>[
                                'required', 
                                'max:250', 
                                Rule::unique('landing_vacancy', 'slug')->ignore($id, 'id')->whereNull('deleted_at')
                            ],
                            'heading_1'=>[
                                'required',
                                'max:250',
                                Rule::unique('landing_vacancy', 'heading_1')->ignore($id, 'id')->whereNull('deleted_at')
                            ],
                            'heading_2'=>'max:250',
                            'keyword'=>'required|max:250',
                            'latitude_1' => 'required|numeric',
                            'longitude_1' => 'required|numeric',
                            'latitude_2' => 'required|numeric',
                            'longitude_2' => 'required|numeric',
                            'price_min' => 'numeric',
                            'price_max' => 'numeric',
                        ], $this->validationMessages);

        if($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors());
        }

        $jobsLanding->slug           = preg_replace('/\--+/', '-', str_replace(' ', '-', preg_replace('/\s+/', ' ',trim($request->get('slug')))));
        $jobsLanding->parent_id      = $request->get('parent_id') ? $request->get('parent_id') :null;
        $jobsLanding->heading_1      = $request->get('heading_1');
        $jobsLanding->heading_2      = $request->get('heading_2');
        
        $jobsLanding->latitude_1       = $request->get('latitude_1');
        $jobsLanding->longitude_1      = $request->get('longitude_1');
        $jobsLanding->latitude_2       = $request->get('latitude_2');
        $jobsLanding->longitude_2      = $request->get('longitude_2');
        $jobsLanding->keyword          = $request->get('keyword');
        $jobsLanding->description      = $request->get('description');
        $jobsLanding->type             = $request->get('type_landing');
        $jobsLanding->last_education   = $request->get('last_education');
        $jobsLanding->is_niche         = $request->get('is_niche');
        $jobsLanding->spesialisasi_id  = $request->get('spesialisasi');   
        $jobsLanding->save();

        ActivityLog::LogUpdate(Action::LANDING_JOBS, $id);
        
        return  redirect()->route('admin.jobs-landing.index')
            ->with('message', 'Landing Page successfully updated');
    }

        /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!Auth::user()->can('access-landing-jobs-delete')) return redirect ('/admin');

        LandingVacancy::find($id)->delete();

        ActivityLog::LogDelete(Action::LANDING_JOBS, $id);

        return  redirect()->route('admin.jobs-landing.index')
            ->with('message', 'Landing Page successfully deleted');
    }

    public function redirectLanding(Request $request, $id)
    {
        if(!Auth::user()->can('access-landing-jobs-redirect')) return redirect ('/admin');

        $landing = LandingVacancy::find($id);

        $viewData = array(
            'boxTitle'        => 'Edit Landing Redirection',
            'landing'         => $landing
        );

        return view('admin.contents.landing_vacancy.redirect',$viewData);
    }

    public function redirectLandingPost(Request $request, $id)
    {
        $landing = LandingVacancy::find($id);

        if(is_null($request->get('landing_destination')) || $request->get('landing_destination') == '') {
            $landing->redirect_id = NULL;
            $landing->save();

            return redirect()->route('admin.landing.index')->with('message', 'Redirect Landing sudah dibatalkan');
        }

        $landingDestination = LandingVacancy::find($request->get('landing_destination'));

        if(!$landingDestination) {
            return redirect()->back()->with('error_message', 'Landing tujuan tidak ditemukan');
        }

        $landing->redirect_id = $landingDestination->id;
        $landing->save();

        return redirect()->route('admin.jobs-landing.index')->with('message', 'Landing sudah di-redirect');
    }

    public function parseIndex(Request $request)
    {
        $viewData   = [
            'boxTitle'          => 'Parse Landing Job CSV',
        ];

        return view('admin.contents.landing_vacancy.parser', $viewData);
    }


    public function parseCSV(Request $request)
    {
        $this->validate($request, [
            'file_csv'=>'required|mimes:csv,txt'
        ]);

        $spesialisasi = Spesialisasi::where('is_active', 1)
                            ->orderBy('group')
                            ->get();

        $spesialisasiGrouped = [];

        foreach ($spesialisasi as $spesial) {
            $spesialisasiGrouped[$spesial->group][$spesial->id] = strtolower($spesial->name);
        }

        $csv = $request->file_csv->path();

        $csvArray = CSVParser::csv_parse($csv, ['eol'=>"\r\n"]);

        $failedRow = [];
        foreach($csvArray as $key=>$row) {
            $slug = strtolower(str_replace(' ', '-', trim($row['slug'])));

            $existingLanding = LandingVacancy::where('slug', $slug)
                                    ->orWhere('heading_1', $row['heading_1'])
                                    ->get();

            if(count($existingLanding) > 0) {
                $failedRow[] = 'Baris ' . ($key + 1) . ' gagal diinput karena slug atau heading_1 sudah ada sebelumnya';
                continue;
            }

            $landing = new LandingVacancy;
            $landing->slug = strtolower($slug);
            $landing->heading_1 = $row['heading_1'];
            $landing->heading_2 = $row['heading_2'];

            if(isset($row['description'])) {
                $landing->description = $row['description'];
            }
            
            $coordinate_1 = explode(',', $row['koordinat_1']);
            $coordinate_2 = explode(',', $row['koordinat_2']);

            $landing->latitude_1 = trim($coordinate_1[0]);
            $landing->longitude_1 = trim($coordinate_1[1]);
            $landing->latitude_2 = trim($coordinate_2[0]);
            $landing->longitude_2 = trim($coordinate_2[1]);

            $landing->keyword = $row['keywords'];
            $landing->price_min = $row['price_min'] == '' ? 0 : $row['price_min'];
            $landing->price_max = $row['price_max'] == '' ? 10000000 : $row['price_max'];

            $landing->type = $row['type'] != '' ? $row['type'] : null;

            $landing->last_education = $row['last_education'];

            $landing->is_niche = $row['is_niche'];

            $spesialisasiGroup = $row['is_niche'] == 1 ? 'niche' : 'general';

            if ($row['spesialisasi'] != '') {
                $speciality = array_search(strtolower($row['spesialisasi']), 
                    $spesialisasiGrouped[$spesialisasiGroup]);

                if ($speciality !== false) {
                    $landing->spesialisasi_id = $speciality;
                }
            }

            if (isset($row['redirect_id']) && $row['redirect_id'] != '') {
                $landing->redirect_id = $row['redirect_id'];
            }

            if (isset($row['parent_id']) && $row['parent_id'] != '') {
                $landing->parent_id = $row['parent_id'];
            }
            
            $landing->save();
        }

        return redirect()->back()
                        ->with(!empty($failedRow) ? 'errors' : 'message', 
                            !empty($failedRow) ? collect($failedRow) : 'Sukses input data');
    }

}
