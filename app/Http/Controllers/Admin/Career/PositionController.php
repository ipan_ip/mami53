<?php

namespace App\Http\Controllers\Admin\Career;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Entities\Career\Position;
use App\Http\Controllers\Controller;

class PositionController extends Controller {
    public function __construct()
    {
        \View::share('contentHeader', 'Position Management');
        \View::share('user' , Auth::user());
    }

    public function index(Request $request) {
        $positions = Position::withCount('vacancy')->paginate(20);
        return view('admin.contents.career.position.index', ['rowsPosition' => $positions]);
    }

    public function store(Request $request) {
        try
		{
            $validator = Validator::make($request->all(), [
                'name' => "required|unique:career_position,name"
            ]);

            if ($validator->fails()) {
                return [
                    'success' => false,
                    'message' => 'Position name could not be saved',
                    'error'   => $validator->errors(),
			    ];
            }

			Position::create($request->only('name'));
		}
		catch (Exception $e)
		{
			return [
				'success' => false,
				'message' => $e->getMessage(),
				'error'   => $e,
			];
		}

		return [
			'success' => true,
			'message' => "Position has been successfully created.",
        ];
    }

    public function update(Request $request, Position $position) {
        try
		{
            $validator = Validator::make($request->all(), [
                'name' => "required|unique:career_position,name"
            ]);

            if ($validator->fails()) {
                return [
                    'success' => false,
                    'message' => 'Position name could not be saved',
                    'error'   => $validator->errors(),
			    ];
            }

			$position->update($request->only('name'));
		}
		catch (Exception $e)
		{
			return [
				'success' => false,
				'message' => $e->getMessage(),
				'error'   => $e,
			];
		}

		return [
			'success' => true,
			'message' => "Position has been successfully updated.",
        ];
    }

    public function destroy(Request $request, Position $position) {
        try
		{
            if ($position->vacancy()->count() > 0) {
                return [
                    'success' => false,
                    'message' => 'Position name could not be deleted',
                    'error'   => 'Tidak bisa menghapus posisi yang memiliki lowongan',
			    ];
            }

			$position->delete();
		}
		catch (Exception $e)
		{
			return [
				'success' => false,
				'message' => $e->getMessage(),
				'error'   => $e,
			];
		}

		return [
			'success' => true,
			'message' => "Position has been successfully deleted.",
        ];
    }
}