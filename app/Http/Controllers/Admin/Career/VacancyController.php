<?php

namespace App\Http\Controllers\Admin\Career;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Entities\Career\Description;
use App\Entities\Career\Location;
use App\Entities\Career\Position;
use App\Entities\Career\Requirement;
use App\Entities\Career\Vacancy;
use App\Http\Controllers\Controller;

class VacancyController extends Controller {
    public function __construct()
    {
        \View::share('contentHeader', 'Vacancy Management');
        \View::share('user' , Auth::user());
    }

    public function index(Request $request) {
        $vacancy = Vacancy::with('locations')->with('position')->paginate(20);

        return view('admin.contents.career.vacancy.index', ['rowsVacancy' => $vacancy]);
    }

    public function create() {
        $viewData = [
            'locations' => Location::get(), 
            'positions' => Position::get()
        ];

        return view('admin.contents.career.vacancy.create', $viewData);
    }

    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'position' => 'required|integer|exists:career_position,id',
            'locations' => 'required|array',
            'locations.*' => 'required|integer|exists:career_location,id',
            'descriptions' => 'required|array',
            'descriptions.*' => 'required|string|max:255',
            'requirements' => 'required|array',
            'requirements.*' => 'required|string|max:255',
            'is_open' => 'sometimes|integer|max:1',
            'contact' => 'required|string|max:255',
            'level' => 'sometimes|string|max:255',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $requirements = collect($request->requirements);
        $requirements = $requirements->map(function ($item, $key) {
            return ['content' => $item];
        })->toArray();

        $descriptions = collect($request->descriptions);
        $descriptions = $descriptions->map(function ($item, $key) {
            return ['content' => $item];
        })->toArray();

        $locations = Location::whereIn('id', $request->locations)->get();

        $vacancy = new Vacancy($request->only(['is_open', 'contact', 'level']));
        $vacancy->career_position_id = $request->position;
        $vacancy->save();

        $vacancy->locations()->attach($locations);
        $vacancy->requirements()->createMany($requirements);
        $vacancy->description()->createMany($descriptions);

        return redirect()->route('admin.career.vacancy.index');
    }

    public function edit(Request $request, Vacancy $vacancy) {
        $viewData = [
            'locations' => Location::get(), 
            'positions' => Position::get(),
            'vacancy' => $vacancy
        ];

        return view('admin.contents.career.vacancy.edit', $viewData);
    }

    public function update(Request $request, Vacancy $vacancy) {
        $validator = Validator::make($request->all(), [
            'position' => 'required|integer|exists:career_position,id',
            'locations' => 'required|array',
            'locations.*' => 'required|integer|exists:career_location,id',
            'descriptions' => 'required|array',
            'descriptions.*' => 'required|string|max:255',
            'requirements' => 'required|array',
            'requirements.*' => 'required|string|max:255',
            'is_open' => 'sometimes|integer|max:1',
            'contact' => 'required|string|max:255',
            'level' => 'sometimes|string|max:255',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $requirements = collect($request->requirements);
        $requirements = $requirements->map(function ($item, $key) {
            return ['content' => $item];
        })->toArray();

        $descriptions = collect($request->descriptions);
        $descriptions = $descriptions->map(function ($item, $key) {
            return ['content' => $item];
        })->toArray();

        $locations = Location::whereIn('id', $request->locations)->get();

        $updatedData = $request->only(['is_open', 'contact', 'level']);
        $updatedData += ['career_position_id' => $request->position];
        $vacancy->update($updatedData);

        $vacancy->locations()->detach();
        $vacancy->locations()->attach($locations);

        $vacancy->requirements()->delete();
        $vacancy->requirements()->createMany($requirements);

        $vacancy->description()->delete();
        $vacancy->description()->createMany($descriptions);

        return redirect()->route('admin.career.vacancy.index');
    }

    public function toggle(Request $request, Vacancy $vacancy) {
        if ($vacancy->is_open == 1) {
            $vacancy->update(['is_open' => false]);
        }
        else {
            $vacancy->update(['is_open' => true]);
        }
        
        return redirect()->route('admin.career.vacancy.index');
    }

    public function destroy(Request $request, Vacancy $vacancy) {
        $vacancy->delete();

        return redirect()->route('admin.career.vacancy.index');
    }
}