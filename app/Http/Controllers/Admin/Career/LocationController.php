<?php

namespace App\Http\Controllers\Admin\Career;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Entities\Career\Location;
use App\Http\Controllers\Controller;

class LocationController extends Controller {
    public function __construct()
    {
        \View::share('contentHeader', 'Location Management');
        \View::share('user' , Auth::user());
    }

    public function index(Request $request) {
        $locations = Location::withCount('vacancy')->paginate(20);
        return view('admin.contents.career.location.index', ['rowsLocation' => $locations]);
    }

    public function store(Request $request) {
        try
		{
            $validator = Validator::make($request->all(), [
                'name' => "required|unique:career_location,name"
            ]);

            if ($validator->fails()) {
                return [
                    'success' => false,
                    'message' => 'Location name could not be saved',
                    'error'   => $validator->errors(),
			    ];
            }

			Location::create($request->only('name'));
		}
		catch (Exception $e)
		{
			return [
				'success' => false,
				'message' => $e->getMessage(),
				'error'   => $e,
			];
		}

		return [
			'success' => true,
			'message' => "Location has been successfully created.",
        ];
    }

    public function update(Request $request, Location $location) {
        try
		{
            $validator = Validator::make($request->all(), [
                'name' => "required|unique:career_location,name"
            ]);

            if ($validator->fails()) {
                return [
                    'success' => false,
                    'message' => 'Location name could not be saved',
                    'error'   => $validator->errors(),
			    ];
            }

			$location->update($request->only('name'));
		}
		catch (Exception $e)
		{
			return [
				'success' => false,
				'message' => $e->getMessage(),
				'error'   => $e,
			];
		}

		return [
			'success' => true,
			'message' => "Location has been successfully updated.",
        ];
    }

    public function destroy(Request $request, Location $location) {
        try
		{
            if ($location->vacancy()->count() > 0) {
                return [
                    'success' => false,
                    'message' => 'Location name could not be deleted',
                    'error'   => 'Tidak bisa menghapus lokasi yang memiliki lowongan',
			    ];
            }

			$location->delete();
		}
		catch (Exception $e)
		{
			return [
				'success' => false,
				'message' => $e->getMessage(),
				'error'   => $e,
			];
		}

		return [
			'success' => true,
			'message' => "Location has been successfully deleted.",
        ];
    }
}