<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;
use Auth;
use Carbon\Carbon;

use App\Entities\Booking\BookingPayment;
use App\Repositories\Booking\BookingPaymentRepository;

class BookingPaymentController extends Controller
{
	protected $repository;

	public function __construct(BookingPaymentRepository $repository)
    {
        $this->middleware(function ($request, $next) {
            if(!Auth::user()->can('access-booking-payment')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

    	$this->repository = $repository;

    	View::share('contentHeader', 'Booking Management');
        View::share('user', Auth::user());
    }

    public function index(Request $request)
    {
        $query = BookingPayment::with(['booking_user', 'bank']);

        if($request->filled('q') && $request->get('q') != '') {
            $query = $query->whereIn('booking_user_id', function($query) use ($request) {
                $query->select('id')
                    ->from('booking_user')
                    ->where('booking_code', 'like', '%' . $request->get('q') . '%');
            });
        }

    	$bookingPayments = $query->orderBy('created_at', 'desc')
    							->paginate(20);

    	$viewData = [];
    	$viewData['bookingPayments'] = $bookingPayments;
    	$viewData['boxTitle'] = 'Booking Payments';

    	return view('admin.contents.booking.booking_payments', $viewData);
    }

    public function verify(Request $request, $bookingPaymentId)
    {
    	
    	$bookingPayment = BookingPayment::with(['booking_user'])->find($bookingPaymentId);

    	if($bookingPayment->status == BookingPayment::PAYMENT_STATUS_VERIFIED) {
    		return redirect()->route('admin.booking.payments')->with('error_message', 'Pembayaran sudah terverifikasi sebelumnya');
    	}

    	if(!is_null($bookingPayment->booking_user->expired_date) && Carbon::parse($bookingPayment->booking_user->expired_date)->lt(Carbon::now())) {
    		return redirect()->route('admin.booking.payments')->with('error_message', 'Pembayaran tidak bisa diverifikasi karena sudah melewati tanggal checkin');
    	}

    	$otherPayments = BookingPayment::where('booking_user_id', $bookingPayment->booking_user_id)
    									->where('id', '<>', $bookingPayment->id)
    									->get();

    	$paymentVerification = $this->repository->verifyPayment($bookingPayment, $otherPayments);

    	if(is_string($paymentVerification)) {
    		return redirect()->route('admin.booking.payments')->with('message', $paymentVerification);
    	} else {
    		return redirect()->route('admin.booking.payments')->with('message', 'Pembayaran berhasil diverifikasi');
    	}
    }
}
