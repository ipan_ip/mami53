<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Chat\ChatCondition;
use App\Entities\Chat\Criteria;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\Activity\Question;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use App\Entities\Activity\ChatAnswer;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;
use DB;
use RuntimeException;
use Bugsnag;
use Exception;

class questionController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if(!Auth::user()->can('access-chat-question')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        View::share('contentHeader', 'Stories Management');
        View::share('user', Auth::user());
    }

   
    public function index()
    {
       $question = Question::orderBy('id', 'desc')->get();
        $viewData = array(
            'boxTitle'     => 'Question List',
            'deleteAction' => 'admin.tag.destroy',
            'questions'      => $question,
        );

        ActivityLog::LogIndex(Action::CHAT_QUESTION);

        return View::make('admin.contents.question.index', $viewData);
    }

    public function create()
    {
        $viewData = array(
            'boxTitle'    => 'Create Question',
            'formAction'  => array('admin.question.store'),
            'formMethod'  => 'POST'
        );
        return View::make('admin.contents.question.create', $viewData);
    }

    public function store(Request $request)
    {
        $question = new Question();
        $question->question = $request->input('question');
        $question->answer = $request->input('answer');
        $question->for = $request->input('for');
        $question->custom_action_key = $request->input('custom_action_key');
        $question->chat_for = 1;
        $question->save();
        ActivityLog::LogCreate(Action::CHAT_QUESTION, $question->id);
        return redirect('admin/question')->with('message', 'Question Created to '.$request->input('question'));
    }

    public function show($id)
    {
        $data = Question::with(
            [
                'chat_condition' => function ($q) {
                    $q->orderBy('order', 'asc');
                },
                'chat_answer'
            ]
        )
            ->where('id', $id)
            ->first();

        $criteria = Criteria::all();

        $viewData = array(
            'boxTitle'    => 'View or Edit Question',
            'formAction'  => array('admin.question.update', $data->id),
            'data'        => $data,
            'formMethod'  => 'PUT',
            'answer_condition' => ChatAnswer::CONDITION,
            'chat_criteria' => $criteria,
        );
        return View::make('admin.contents.question.form',$viewData);
    }

    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        $chatQuestion = Question::find($id);
        $chatQuestion->question = $request->input('question');
        $chatQuestion->answer = $request->input('default_answer');
        $chatQuestion->order = $request->input('order');
        $chatQuestion->custom_action_key = $request->input('custom_action_key');
        if ($request->filled('condition')) {
            $condition = $request->input('condition');
            $answer = $request->input('answer');

            $conditionCount = count($request->input('condition'));
            for ($i = 0; $i < $conditionCount; $i++) {
                if (empty($condition[$i])) {
                    continue;
                }
                $chatAnswer = ChatAnswer::where('chat_question_id', $chatQuestion->id)
                                    ->where('condition', $condition[$i])
                                    ->first();
                if (is_null($chatAnswer)) {
                    $chatAnswer = new ChatAnswer();
                    $chatAnswer->chat_question_id = $chatQuestion->id;
                    $chatAnswer->condition = $condition[$i];
                }
                $chatAnswer->answer = $answer[$i];
                $chatAnswer->save();
            }
        }
        $chatQuestion->save();
        DB::commit();

        ActivityLog::LogUpdate(Action::CHAT_QUESTION, $id);
        
        return redirect('admin/question')->with('message', 'Question Updated to '.$request->input('question')); 
    }

    public function changeStatus(Request $request, $id)
    {
        $question = Question::where('id', $id)->firstOrFail();
        if ($question->status == "true") $status = "false";
        else $status = "true";
        $question->status = $status;
        $question->save();
        return redirect('admin/question')->with('message', 'Sukses mengganti status pertanyaan chat.'); 
    }

    public function answerDeleted(Request $request, $id)
    {
        $answer = ChatAnswer::find($id);

        if (is_null($answer)) {
            return back()->with('error_message', 'Data tidak ditemukan');
        }

        $answer->delete();
        return back()->with('message', 'Berhasil hapus');
    }

    /**
     * Delete a Condition
     *
     * @param Request $request
     *
     * @return array
     */
    public function deleteCondition(Request $request)
    {
        try {
            if (!$request->filled('id')) {
                new RuntimeException('Failed fetching Condition. Please refresh page and try again.');
            }

            $condition = ChatCondition::find($request->id);

            if (is_null($condition)) {
                return [
                    'success' => false,
                    'message' => 'Unable to find Condition.',
                ];
            }

            ChatCondition::removeChatCondition($condition);

            return [
                'success' => true,
                'title' => 'Condition Successfully Removed!',
                'message' => "Page will be refreshed after you clicked OK",
            ];
        } catch (Exception $e) {
            Bugsnag::notifyException($e);

            return [
                'success' => false,
                'message' => $e->getMessage(),
                'error' => $e,
            ];
        }
    }

    /**
     * Update/Create a new chat Condition
     *
     * @param Request $request
     *
     * @return array
     */
    public function storeCondition(Request $request)
    {
        try {
            $question = Question::find((int) $request->chat_question_id);

            if (is_null($question)) {
                return [
                    'success' => false,
                    'message' => 'Unable to find Question.',
                ];
            }

            if ($request->filled('id')) {
                $condition = ChatCondition::find((int) $request->id);

                if (is_null($condition)) {
                    return [
                        'success' => false,
                        'message' => 'Unable to find Condition.',
                    ];
                }
                ChatCondition::updateChatCondition($request, $condition);
            } else {
                ChatCondition::createChatCondition($request, $question);
            }
            return [
                'success' => true,
                'title' => 'Condition Successfully Stored!',
                'message' => "Page will be refreshed after you clicked OK",
            ];
        } catch (Exception $e) {
            Bugsnag::notifyException($e);

            return [
                'success' => false,
                'message' => $e->getMessage(),
                'error' => $e,
            ];
        }
    }

    public function sortOrder(Request $request)
    {
        $orderData = $request->all();

        try {
            $saveOrder = ChatCondition::saveSortOrder($orderData);

            if (!$saveOrder) {
                return [
                    'success' => false,
                    'message' => "Gagal menyimpan urutan baru. Silakan refresh halaman dan coba lagi!",
                ];
            }

            return [
                'success' => true,
            ];
        } catch (Exception $e) {
            return [
                'success' => false,
                'message' => $e->getMessage(),
            ];
        }
    }
}
