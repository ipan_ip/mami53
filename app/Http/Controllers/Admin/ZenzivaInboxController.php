<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Auth;
use App\Libraries\Sms\Zenziva;
use Config;
use Carbon\Carbon;
use App\Entities\Message\ZenzivaReply;
use App\User;
use App\Entities\Room\Room;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;

class ZenzivaInboxController extends Controller
{
    public function __construct()
    {
        View::share('contentHeader', 'Zenziva Inbox Management');
        View::share('user', Auth::user());
    }

    public function index(Request $request)
    {
        $dateQuery = Carbon::now()->format('Y-m-d');

        if ($request->filled('q') && $request->q != '') {
            $dateQuery = $request->q;
        }

        $zenziva = new Zenziva(
            'mamikos',
            Config::get('services.zenziva.userkey'),
            Config::get('services.zenziva.passkey')
        );

        $inboxes = $zenziva->getInboxByDate($dateQuery, $dateQuery);

        $replyIds = ZenzivaReply::where('replied_at', '>=', $dateQuery . ' 00:00:01')
                        ->where('replied_at', '<=', $dateQuery . ' 23:59:59')
                        ->pluck('zenziva_id')->toArray();

        ActivityLog::LogIndex(Action::ZENZIVA_INBOX);

        return view('admin.contents.zenziva.index', [
            'inboxes' => $inboxes,
            'replyIds' => $replyIds,
            'boxTitle' => 'Zenziva Inbox'
        ]);
    }

    public function convertToDB(Request $request)
    {
        $existingReply = ZenzivaReply::where('zenziva_id', $request->zenziva_id)->first();

        if ($existingReply) {
            return redirect()->route('admin.zenziva-inbox.index')
                ->with('error_message', 'Data berhasil dimasukkan ke dalam database');
        }

        $phoneNumber = str_replace('+62', '0', $request->phone_number);

        $user = User::with('owners')
                    ->where('phone_number', $phoneNumber)
                    ->where('is_owner', 'true')
                    ->first();

        $room = null;

        if ($user) {

            foreach ($user->owners as $owner) {
                if (in_array($owner->status, ['verified', 'draft2'])) {
                    $room = Room::find($owner->designer_id);
                }
            }

        } else {
            
            $room = Room::where(function($query) use ($phoneNumber) {
                        $query->where('owner_phone', $phoneNumber)
                            ->orWhere('manager_phone', $phoneNumber);
                    })->where('is_active', 'true')
                    ->first();

        }

        $reply = new ZenzivaReply;
        $reply->zenziva_id = $request->zenziva_id;
        $reply->user_id = $user ? $user->id : null;
        $reply->phone_number = $phoneNumber;
        $reply->designer_id = $room ? $room->id : null;
        $reply->message = $request->message;
        $reply->replied_at = $request->date . ' ' . $request->time;
        $reply->save();

        return redirect()->route('admin.zenziva-inbox.index')
            ->with('message', 'Data berhasil dimasukkan ke dalam database');
    }
    
}
