<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\Landing\LandingApartment;
use Auth;
use View;
use App\Entities\Room\Element\Tag;
use DB;
use Validator;
use App\Libraries\CSVParser;
use Illuminate\Validation\Rule;
use App\Entities\Apartment\ApartmentProject;
use App\Http\Helpers\UtilityHelper;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;

class LandingApartmentController extends Controller
{
    protected $validationMessages = [
        'slug.required'=>'Slug harus diisi',
        'slug.alpha_dash' => 'Slug hanya boleh mengandung huruf, angka, dan dash (-)',
        'slug.max'=> 'Slug maksimal :max karakter',
        'slug.unique' => 'Slug sudah dipakai sebelumnya',
        'heading_1.required' => 'Heading 1 harus diisi',
        'heading_1.unique' => 'Heading 1 sudah dipakai sebelumnya',
        'heading_1.max' => 'Heading 1 maksimal :max karakter',
        'heading_2.max' => 'Heading 2 maksimal :max karakter',
        'keyword.required' => 'Keyword harus diisi',
        'keyword.max' => 'Keyword maksimal :max karakter',
        'latitude_1.required' => 'Latitude 1 harus diisi',
        'latitude_1.numeric' => 'Latitude 1 harus berupa angka',
        'longitude_1.required' => 'Longitude 1 harus diisi',
        'longitude_1.numeric' => 'Longitude 1 harus berupa angka',
        'latitude_2.required' => 'Latitude 2 harus diisi',
        'latitude_2.numeric' => 'Latitude 2 harus berupa angka',
        'longitude_2.required' => 'Longitude 2 harus diisi',
        'longitude_2.numeric' => 'Longitude 2 harus berupa angka',
        'price_min.numeric' => 'Harga Minimal harus berupa angka',
        'price_max.numeric' => 'Harga Maksimal harus berupa angka',
        'photo_cover.numeric' => 'Foto Cover harus berupa angka'
    ];

    protected $rentTypeOptions = [
        'Harian', 'Mingguan', 'Bulanan', 'Tahunan'
    ];

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if(!Auth::user()->can('access-landing-apartment')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        View::share('contentHeader', 'Landing Apartment Management');
        View::share('user', Auth::user());
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $rowsLanding    = LandingApartment::with('parent')->orderByRaw(\DB::raw('IFNULL(redirect_id, 0) asc'));

        if($request->filled('q'))
        {
            $rowsLanding = $rowsLanding->where('slug','like', '%' . $request->get('q') . '%');
        }

        if ($request->filled('sort')) {
            $field = $request->get('sort');
            $direction = $request->filled('sort_dir') ? $request->get('sort_dir') : 'asc';

            $rowsLanding = $rowsLanding->orderBy($field, $direction);
        } else {
            $rowsLanding = $rowsLanding->orderBy('id', 'desc');
        }

        $rowsLanding    = $rowsLanding->paginate(10);

        $sortUrl = [
            'view_count' => UtilityHelper::modifyQueryStringForSorting($request->fullUrl(), 'view_count', 'array')
        ];

        parse_str(parse_url($request->fullUrl(), PHP_URL_QUERY), $currentQueryString);


        $viewData = array(
            'boxTitle'        => 'List Landing Apartment',
            'rowsLanding'     => $rowsLanding,
            'sortUrl'         => $sortUrl,
            'currentQueryString' => $currentQueryString
        );

        ActivityLog::LogIndex(Action::LANDING_APARTMENT);

        return View::make('admin.contents.landing-apartment.index', $viewData);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $parentOptions = LandingApartment::get();
        $tagOptions = Tag::where('type', '<>', 'fac_project')->get();
        $rentTypeOptions = $this->rentTypeOptions;
        $unitTypeOptions = ApartmentProject::UNIT_TYPE;

        $viewData   = [
            'boxTitle'          => 'New Landing Apartment Page',
            'tagOptions'        => $tagOptions,
            'parentOptions'     => $parentOptions,
            'rentTypeOptions'   => $rentTypeOptions,
            'unitTypeOptions'   => $unitTypeOptions
        ];

        return View::make('admin.contents.landing-apartment.create', $viewData);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {       
        $validator = Validator::make($request->all(), 
                        [
                            'slug'             =>'required|max:250|unique:landing_apartment,slug',
                            'heading_1'        =>'required|max:250|unique:landing_apartment,heading_1',
                            'heading_2'        =>'max:250',
                            'keyword'          =>'required|max:250',
                            'latitude_1'       => 'required|numeric',
                            'longitude_1'      => 'required|numeric',
                            'latitude_2'       => 'required|numeric',
                            'longitude_2'      => 'required|numeric',
                            'price_min'        => 'numeric',
                            'price_max'        => 'numeric',
                            'photo_cover'      => 'nullable|numeric',
                            'unit_type'        => 'required',
                            'is_furnished'     => 'required'
                        ], $this->validationMessages);

        if($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors());
        }

        $landingApartment = new LandingApartment;

        $landingApartment->slug           = $request->get('slug');
        $landingApartment->parent_id      = $request->get('parent_id') ? $request->get('parent_id') :null;
        $landingApartment->heading_1      = $request->get('heading_1');
        $landingApartment->heading_2      = $request->get('heading_2');
        
        $landingApartment->latitude_1 = $request->get('latitude_1');
        $landingApartment->longitude_1 = $request->get('longitude_1');
        $landingApartment->latitude_2 = $request->get('latitude_2');
        $landingApartment->longitude_2 = $request->get('longitude_2');

        $landingApartment->price_min      = $request->get('price_min');
        $landingApartment->price_max      = $request->get('price_max');
        $landingApartment->rent_type      = $request->get('rent_type');

        $landingApartment->keyword        = $request->get('keyword');

        $landingApartment->description_1  = $request->get('description_1');
        $landingApartment->description_2  = $request->get('description_2');
        $landingApartment->description_3  = $request->get('description_3');

        if ( $request->unit_type == "-") {
            $landingApartment->unit_type  = NULL;    
        } else {
             $landingApartment->unit_type  = $request->get('unit_type');
        }
       
        if ( $request->is_furnished == "-") {
             $landingApartment->is_furnished  = NULL;    
        } else {
             $landingApartment->is_furnished  = $request->get('is_furnished');
        }


        // if ($request->filled('area_city') && $request->filled('sub_district')) { 
        //     $place = [$request->input('area_city'), $request->input('sub_district')];
        //     $landingApartment->place          = json_encode($place);
        // }

        $landingApartment->photo_id      = $request->get('photo_cover');
        $landingApartment->dummy_counter = $request->get('dummy_counter') != '' ? $request->get('dummy_counter') : null;

        $landingApartment->cleanNoFollowLinks();
        $landingApartment->save();

        ActivityLog::LogCreate(Action::LANDING_APARTMENT, $landingApartment->id);

        $tagIds = $request->get('tag_ids');
        $tagIds = (array) $tagIds;

        $landingApartment->tags()->sync($tagIds);

        return  redirect()->route('admin.landing-apartment.index')
            ->with('message', 'Landing Page successfully created');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $landingApartment = LandingApartment::find($id);

        if(!$landingApartment) {
            return redirect()->back()->with('error_message', 'Landing tidak ditemukan');
        }

        $landingApartment->tag_ids        = array_pluck($landingApartment->tags()->get(['tag.id'])->toArray(),'id');

        if ($landingApartment->place != null) {
            $place = json_decode($landingApartment->place, true);
            $landingApartment->area_city = $place[0];
            $landingApartment->sub_district = $place[1];
        } else {
            $landingApartment->area_city = "";
            $landingApartment->sub_district = "";
        }

        $parentOptions = LandingApartment::where('id', '<>', $id)->get();
        $tagOptions = Tag::where('type', '<>', 'fac_project')->get();
        $unitTypeOptions = ApartmentProject::UNIT_TYPE;


        $photo = null;

        if(!is_null($landingApartment->photo_id)) {
            $photo = $landingApartment->photo;
        }

        $viewData   = [
            'boxTitle'          => 'Edit Landing Apartment',
            'landingApartment'  => $landingApartment,
            'photo'             => $photo,
            'parentOptions'     => $parentOptions,
            'tagOptions'        => $tagOptions,
            'rentTypeOptions'   => $this->rentTypeOptions,
            'unitTypeOptions'   => $unitTypeOptions
        ];

        return View::make('admin.contents.landing-apartment.edit', $viewData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $landingApartment = LandingApartment::find($id);

        if(!$landingApartment) {
            return redirect()->back()->with('error_message', 'Landing tidak ditemukan');
        }

        $validator = Validator::make($request->all(), 
                        [
                            'slug'=>[
                                'required', 
                                'max:250', 
                                Rule::unique('landing_apartment', 'slug')->ignore($id, 'id')->whereNull('deleted_at')
                            ],
                            'heading_1'=>[
                                'required',
                                'max:250',
                                Rule::unique('landing_apartment', 'heading_1')->ignore($id, 'id')->whereNull('deleted_at')
                            ],
                            'heading_2'=>'max:250',
                            'keyword'=>'required|max:250',
                            'latitude_1' => 'required|numeric',
                            'longitude_1' => 'required|numeric',
                            'latitude_2' => 'required|numeric',
                            'longitude_2' => 'required|numeric',
                            'price_min' => 'numeric',
                            'price_max' => 'numeric',
                            'photo_cover' => 'nullable|numeric',
                            'unit_type'        => 'required',
                            'is_furnished'     => 'required'
                        ], $this->validationMessages);

        if($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors());
        }

        $landingApartment->slug           = $request->get('slug');
        $landingApartment->parent_id      = $request->get('parent_id') ? $request->get('parent_id') :null;
        $landingApartment->heading_1      = $request->get('heading_1');
        $landingApartment->heading_2      = $request->get('heading_2');
        
        $landingApartment->latitude_1 = $request->get('latitude_1');
        $landingApartment->longitude_1 = $request->get('longitude_1');
        $landingApartment->latitude_2 = $request->get('latitude_2');
        $landingApartment->longitude_2 = $request->get('longitude_2');

        $landingApartment->price_min      = $request->get('price_min');
        $landingApartment->price_max      = $request->get('price_max');
        $landingApartment->rent_type      = $request->get('rent_type');

        $landingApartment->keyword        = $request->get('keyword');

        $landingApartment->description_1  = $request->get('description_1');
        $landingApartment->description_2  = $request->get('description_2');
        $landingApartment->description_3  = $request->get('description_3');

        if ( $request->unit_type == "-") {
            $landingApartment->unit_type  = NULL;    
        } else {
             $landingApartment->unit_type  = $request->get('unit_type');
        }
       
        if ( $request->is_furnished == "-") {
             $landingApartment->is_furnished  = NULL;    
        } else {
             $landingApartment->is_furnished  = $request->get('is_furnished');
        }

        // if ($request->filled('area_city') && $request->filled('sub_district')) { 
        //     $place = [$request->input('area_city'), $request->input('sub_district')];
        //     $landingApartment->place          = json_encode($place);
        // } else {
        //     $landingApartment->place = NULL;
        // }

        $landingApartment->photo_id      = $request->get('photo_cover');
        $landingApartment->dummy_counter = $request->get('dummy_counter') != '' ? $request->get('dummy_counter') : null;

        $landingApartment->cleanNoFollowLinks();
        $landingApartment->save();

        $tagIds = $request->get('tag_ids');
        $tagIds = (array) $tagIds;

        $landingApartment->tags()->sync($tagIds);
        ActivityLog::LogUpdate(Action::LANDING_APARTMENT, $id);
        return  redirect()->route('admin.landing-apartment.index')
            ->with('message', 'Landing Page successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!Auth::user()->can('access-landing-apartment-delete')) return redirect ('/admin');

        LandingApartment::find($id)->delete();
        
        ActivityLog::LogDelete(Action::LANDING_APARTMENT, $id);

        return  redirect()->route('admin.landing-apartment.index')
            ->with('message', 'Landing Page successfully deleted');
    }


    public function parseIndex(Request $request)
    {
        $viewData   = [
            'boxTitle'          => 'Parse Landing CSV',
        ];

        return view('admin.contents.landing-apartment.parser', $viewData);
    }


    public function parseCSV(Request $request)
    {
        $this->validate($request, [
            'file_csv'=>'required|mimes:csv,txt'
        ]);

        $availableTags = Tag::get();
        $tagMapping = $availableTags->pluck('id', 'name')->toArray();
        $tagMapping = array_change_key_case($tagMapping, CASE_LOWER);
        $unitType = ApartmentProject::UNIT_TYPE;


        $csv = $request->file_csv->path();

        $csvArray = CSVParser::csv_parse($csv);

        // dd($csvArray);

        $failedRow = [];
        foreach($csvArray as $key=>$row) {
            $slug = str_replace(' ', '-', $row['url_slug']);

            $existingLanding = LandingApartment::where('slug', $slug)
                                    ->orWhere('heading_1', $row['heading_1'])
                                    ->get();

            if(count($existingLanding) > 0) {
                $failedRow[] = 'Baris ' . ($key + 1) . ' gagal diinput karena slug atau heading_1 sudah ada sebelumnya';
                continue;
            }

            $landing = new LandingApartment;
            $landing->slug = $slug;
            $landing->heading_1 = $row['heading_1'];
            $landing->heading_2 = $row['heading_2'];

            $coordinate_1 = explode(',', $row['koordinat_1']);
            $coordinate_2 = explode(',', $row['koordinat_2']);

            $landing->latitude_1 = trim($coordinate_1[0]);
            $landing->longitude_1 = trim($coordinate_1[1]);
            $landing->latitude_2 = trim($coordinate_2[0]);
            $landing->longitude_2 = trim($coordinate_2[1]);

            $landing->keyword = $row['keywords'];
            $landing->price_min = $row['price_min'] == '' ? 0 : $row['price_min'];
            $landing->price_max = $row['price_max'] == '' ? 10000000 : $row['price_max'];
            $landing->rent_type = array_flip($this->rentTypeOptions)[ucwords(trim($row['rent_type']))];

            if ($row['unit_type'] == ""){
                $landing->unit_type = NULL;    
            } else {
                $landing->unit_type = $unitType[$row['unit_type']];
            }

            if ($row['is_furnished'] == ""){
                $landing->is_furnished = NULL;    
            } else {
                $landing->is_furnished = $row['is_furnished'];
            }
            
            $landing->save();

            $explodedFilters = $row['filter_1'] != '' ? explode('|', $row['filter_1']) : [];
            $tagIds = [];
            if(!empty($explodedFilters)) {
                foreach($explodedFilters as $filter) {
                    $lowerCasedFilter = strtolower($filter);
                    if(isset($tagMapping[$lowerCasedFilter])) {
                        $tagIds[] = $tagMapping[$lowerCasedFilter];
                    }
                }
            }

            $landing->tags()->sync($tagIds);
        }

        return redirect()->back()
                        ->with(!empty($failedRow) ? 'errors' : 'message', 
                            !empty($failedRow) ? collect($failedRow) : 'Sukses input data');
    }

    public function redirectlanding(Request $request, $landingId)
    {
        if(!Auth::user()->can('access-landing-apartment-redirect')) return redirect ('/admin');
            
        $landingApartment = LandingApartment::find($landingId);

        $viewData = array(
            'boxTitle'        => 'Edit Landing Apartment Redirect',
            'landingApartment'         => $landingApartment
        );

        return view('admin.contents.landing-apartment.redirect',$viewData);
    }

    public function redirectlandingpost(Request $request, $landingId)
    {
        $landingApartment = LandingApartment::find($landingId);

        if(is_null($request->get('landing_destination')) || $request->get('landing_destination') == '') {
            $landingApartment->redirect_id = NULL;
            $landingApartment->save();

            return redirect()->route('admin.landing-apartment.index')->with('message', 'Redirect Landing sudah dibatalkan');
        }

        $landingDestination = LandingApartment::find($request->get('landing_destination'));

        if(!$landingDestination) {
            return redirect()->back()->with('error_message', 'Landing tujuan tidak ditemukan');
        }

        $landingApartment->redirect_id = $landingDestination->id;
        $landingApartment->save();

        return redirect()->route('admin.landing-apartment.index')->with('message', 'Landing sudah di-redirect');
    }
}
