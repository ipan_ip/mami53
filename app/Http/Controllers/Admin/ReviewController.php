<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Room\Room;
use App\Entities\Media\Media;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\Entities\Room\Element\Tag;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Http\Helpers\ApiHelper;
use App\Entities\Room\Review;
use App\Entities\Notif\AllNotif;
use App\Notifications\ReviewLive;
use App\Entities\Room\Element\StyleReview;
use App\Entities\Room\ReviewReply;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;
use Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Config;
use Notification;
use App\Notifications\ReviewReply AS ReviewReplyNotification;
use App\Events\NotificationAdded;
use Event;

class ReviewController extends Controller
{

    protected $review_status = ["semua", "waiting", "live"];

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if(!Auth::user()->can('access-kost-review')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        View::share('contentHeader', 'Stories Management');
        View::share('user', Auth::user());
    }

   
    public function index(Request $request)
    {
       $rowsReview = Review::with('room', 'user');

       $rowsReview = $this->filterIndex($request, $rowsReview);

       if ($request->filled('status') AND $request->input('status') != '0') {
            $rowsReview = $rowsReview->where('status', $this->review_status[$request->input('status')]);
       }

       $rowsReview = $rowsReview->orderBy('updated_at', 'desc')->paginate(20);
        $viewData = array(
            'boxTitle'      => 'Review List',
            'deleteAction'  => 'admin.tag.destroy',
            'rowsReview'    => $rowsReview,
            'statuso'       => $this->review_status,
        );

        ActivityLog::LogIndex(Action::KOST_REVIEW);
        
        return View::make('admin.contents.review.index', $viewData);
    }

    private function filterIndex(Request $request, $query)
    {
        if ($request->filled('q')) {
            $query = $this->searchIndex($request->input('q'), $query);
        }

        if ($request->filled('username')) {
            $query = $this->searchIndexUser($request->input('username'), $query);
        }

        return $query;
    }

    private function searchIndex($keyword, $query)
    {
        return $query->whereHas('room', function($query) use ($keyword) {
            $query->where('designer.name', 'like', '%' . $keyword . '%');
        });

    }

    private function searchIndexUser($keyword, $query)
    {
        return $query->whereHas('user', function($query) use ($keyword) {
            $query->where('user.name', 'like', '%' . $keyword . '%');
        });

    }

    public function show($id)
    {
    	$data = Review::with('room', 'user', 'style_review', 'style_review.photo', 'reply_review')->where('id', $id)->first();
        
        $viewData = array(
            'boxTitle'    => 'View or Edit Review',
            'rowsReviewTypes' => array('waiting','live','rejected','deleted'),
            'formAction'  => array('admin.review.update', $data->id),
            'data'        => $data,
            'formMethod'  => 'PUT'
        );

        return View::make('admin.contents.review.form',$viewData);
    }

    public function update(Request $request, $id)
    {
    	$review = Review::with('room', 'room.owners', 'user')->find($id);
        $review->status  = $request->input('type');
        $review->content = $request->input('content');
        $review->save();

        if ($request->input('type') == "live") {
            
            /* notif to app */ 
            $message = "Review anda sudah terverifikasi.";
            $scheme  = "room/". $review->room->song_id;
            
            $notif   = new AllNotif(); 

            /* notif app */
            $notif->notifToApp($message, $review->user_id, $scheme);

            /* one signal */
            $notif->sendNotif($review->user, new ReviewLive($review->room->slug));

            /* notification owner */
            if (count($review->room->owners) > 0) {
                $notificationEvent = [
                    "owner_id"    => $review->room->owners[0]->user_id,
                    "type"        => 'review',
                    "designer_id" => $review->designer_id,
                    "identifier"  => $review->id 
                ];

                Event::fire(new NotificationAdded($notificationEvent));
            }
            
        }         

        $this->forgetRatingCache($review->room->song_id);
        ActivityLog::LogUpdate(Action::KOST_REVIEW, $id);
        
        return redirect('admin/review')->with('message', 'Review Updated to '.$request->input('type')); 
    }

    public function deletePhoto(Request $request, $id)
    {
        StyleReview::where('id', $id)->delete();
        return back()->with('message','Owner record deleted');    
    }

    public function destroy(Request $request, $id)
    {
        $review = Review::with('room')->find($id);
        if (is_null($review)) {
            return redirect()->back()->with('error_message', 'Review unavailable');
        }

        if (!$review->room) {
            return redirect()->back()->with('error_message', 'Delete Failed, Room is not exist');
        }

        $songId = $review->room->song_id;
        $this->forgetRatingCache($songId);

        Review::where('id', $id)->delete();
        ActivityLog::LogDelete(Action::KOST_REVIEW, $id);

        return redirect('admin/review')->with('message', 'Success, Review Deleted '); 
    }

    public function reply(Request $request)
    {
        $rowsReview = ReviewReply::with('review', 'user')->orderBy('updated_at', 'desc')->get();
        $viewData = array(
            'boxTitle'        => 'Review Reply List',
            'deleteAction'    => 'admin.tag.destroy',
            'rowsReviewReply' => $rowsReview,
        );
        
        return View::make('admin.contents.review.reply', $viewData); 
    }

    public function replyConfirm($id)
    {
        $reply = ReviewReply::with('review', 'review.room', 'review.user')->where('id', $id)->first();
        
        if (isset($reply->review)) {

           //web notif
           Notification::send($reply->review->user, new ReviewReplyNotification($reply->review->room->slug));

           //sms notif
           $message = "Owner membalas review anda";
           (new AllNotif)->notifToApp($message, $reply->review->id, "room/".$reply->review->room->song_id);
        }

        $reply->status = '1';
        $reply->save();

        return back()->with('message','Success Confirm');
    }

    public function replyDeleted($id)
    {
        ReviewReply::where('id', $id)->delete();
        return back()->with('message','Success Deleted');
    }

    public function rotatePhotoReview($id)
    {
        $style       = StyleReview::find($id);
        $mediaObject = Media::findOrFail($style->photo_id);
       //  $medias      = $mediaObject->getMediaUrl();

       // foreach ($medias as $key => $media) {
       //      $url = Config::get('api.media.cdn_url');
       //      //$folderCacheConfig = Config::get('api.media.folder_cache');

       //      // $path = str_replace($url, public_path('/'), $media);
       //      $fileExists = false;

       //      $sourcePath = str_replace($url, '', $media);
       //      if(Config::get('filesystems.default') == 'upload') {
       //          $source = Storage::getDriver()->getAdapter()->getPathPrefix() . $sourcePath; 
       //          $file_exists = file_exists($source);
       //      } else {
       //          $source = Storage::url($sourcePath);
       //          $file_exists = Storage::exists($source);
       //      }
            
       //      if($file_exists) {
       //          $image = Image::make($source);
       //          $image->rotate(-90);

       //          $imgStream = $image->stream();

       //          Storage::put($sourcePath, $imgStream->__toString());

       //          $image->destroy();
       //      }

       //  }

        // Media::find($style->photo_id)->renameMedia();

        $newPhoto = $mediaObject->rotate();
        if(!is_null($newPhoto)) {
            $style->photo_id = $newPhoto['id'];
            $style->save();
        }

        return  back()->with('message','Success Rotate');
    }

    public function threadReview(Request $request, $id)
    {
        $review = Review::with(['room', 'user'])->where('id', $id)->first();
        
        $reviewReply = [];
        if (!is_null($review)) {
            $reviewReply = ReviewReply::with(['review', 'user'])->where('review_id', $review->id)->orderBy('id', 'asc')->get();
        }
        
        $viewData = [
            "review" => $review,
            "reply"  => $reviewReply,
            "boxTitle" => "Review "
        ];

        return View::make('admin.contents.review.thread', $viewData);  
    }

    public function changeStatusReview(Request $request, $id)
    {
        $type = $request->input('type');
        
        $review = Review::with('room', 'room.owners', 'user')->find($id);
        
        if (is_null($review)) {
            return back()->with("error_message", "Data tidak ditemukan");
        }
        
        if ($review->created_at == $review->updated_at) {
            $notificationType = "review";
        } else {
            $notificationType = "review_update";
        }
        $review->status  = $type;
        $review->save();

        $this->forgetRatingCache($review->room->song_id);

        if ($type == 'rejected') {
            return redirect()->back()->with(["message" => "Berhasil menolak review"]);
        }

        if ($type == "live") {
            
            /* notif to app */ 
            $message = "Review anda sudah terverifikasi.";
            $scheme  = "room/". $review->room->song_id;
            
            $notif   = new AllNotif(); 

            /* notif app */
            $notif->notifToApp($message, $review->user_id, $scheme);

            /* one signal */
            $notif->sendNotif($review->user, new ReviewLive($review->room->slug));

            /* notification owner */
            if (count($review->room->owners) > 0) {
                $notificationEvent = [
                    "owner_id"    => $review->room->owners[0]->user_id,
                    "type"        => $notificationType,
                    "designer_id" => $review->designer_id,
                    "identifier"  => $review->id 
                ];

                Event::fire(new NotificationAdded($notificationEvent));
            }
            
        }

        return back()->with('message', 'Review Updated to '.$type); 
    }

    /**
     * Forget cache for rating stuff
     * 
     * @param int $songId
     * @return void
     */
    private function forgetRatingCache(int $songId): void
    {
        \Cache::forget('avgrating:' . $songId);
        \Cache::forget('avgreview:' . $songId);
        \Cache::forget('review-count:' . $songId);
    }

}

