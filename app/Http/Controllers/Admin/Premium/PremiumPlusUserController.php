<?php
namespace App\Http\Controllers\Admin\Premium;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\Entities\Premium\PremiumPlusUser;
use App\Http\Requests\Admin\Premium\PremiumPlusUserRequest;
use App\Services\Premium\PremiumPlusUserService;
use DB;
use App\Entities\Premium\PremiumPlusInvoice;

class PremiumPlusUserController extends Controller
{
    private $service;
    public function __construct(PremiumPlusUserService $service)
    {
        $this->service = $service;
        $this->middleware(function ($request, $next) {
            if(!Auth::user()->can('access-pay-confirmation')) {
                return redirect('admin');
            }
            
            return $next($request);
        });
        
        View::share('user', Auth::user());
        View::share('contentHeader', 'Stories Management');
    }

    public function index(Request $request)
    {
        $premiumPlusUser = PremiumPlusUser::with('user', 'consultant')->orderBy('id', 'desc')->paginate(20);

        $viewData['boxTitle'] = "GP4 List";
        $viewData['premiumPlusUser'] = $premiumPlusUser;
        return view("admin.contents.premium-plus.index", $viewData);
    }

    public function store(PremiumPlusUserRequest $request)
    {
        $validator = $request->failedValidator;
        if (!is_null($validator) && $validator->fails()) {
            return back()->withErrors($validator)->withInput($request->input());
        }

        try {
            DB::beginTransaction();
            // $consultant = $request->getConsultant()->consultant;
            $owner = $request->getOwner();
            $postParams = $request->all();
            // $postParams['consultant_id'] = $consultant->id;
            $postParams['user_id'] = $owner->id;

            $premiumPlusUser = $this->service->save(new PremiumPlusUser(), $postParams);
            DB::commit();
            return redirect('admin/premium-plus')->with('message', 'Berhasil menambah akun GP4');
        } catch (\Exception $exception) {
            DB::rollBack();
            return back()->with('error_message', 'Gagal menambah akun GP4');
        }
    }

    public function create(Request $request)
    {
        $premiumPlusUser = new PremiumPlusUser();

        $premiumPlusUser->name = Input::old('name');
        $premiumPlusUser->phone_number = Input::old('phone_number');
        $premiumPlusUser->email = Input::old('email');
        $premiumPlusUser->consultant_id = Input::old('consultant_id');
        $premiumPlusUser->admin_phone = Input::old('admin_phone');
        $premiumPlusUser->designer_id = Input::old('designer_id');
        $premiumPlusUser->request_date = Input::old('request_date');
        $premiumPlusUser->start_date = Input::old('start_date');
        $premiumPlusUser->end_date = Input::old('end_date');
        $premiumPlusUser->activation_date = Input::old('activation_date');
        $premiumPlusUser->premium_rate = Input::old('premium_rate');
        $premiumPlusUser->static_rate = Input::old('static_rate');
        $premiumPlusUser->registered_room = Input::old('registered_room');
        $premiumPlusUser->guaranteed_room = Input::old('guaranteed_room');
        $premiumPlusUser->grace_period = Input::old('grace_period');
        $premiumPlusUser->releasable_room = 0;
        $premiumPlusUser->total_monthly_premium = 0;

        $viewData = [
            'boxTitle'          => 'GP4 Form',
            'premiumPlusUser'   => $premiumPlusUser,
            'formAction'        => 'admin.premium-plus.store',
            'formMethod'        => 'POST',
            'formState'         => 'add'
        ];

        return view("admin.contents.premium-plus.form", $viewData);
    }

    public function edit(Request $request, $id)
    {
        $premiumPlusUser = PremiumPlusUser::with([
                    'user', 
                    'consultant', 
                    'invoices' => function($query) {
                        $query->where('status', PremiumPlusInvoice::INVOICE_STATUS_PAID);
                    }
                ])
                ->find($id);

        if (is_null($premiumPlusUser)) {
            return redirect('/admin/premium-plus')->with('error_message', 'User not found');
        }

        // $premiumPlusUser->consultant_id = $premiumPlusUser->consultant->id;
        $premiumPlusUser->admin_phone = $premiumPlusUser->user->phone_number;

        $viewData = [
            'boxTitle'          => 'GP4 Edit',
            'premiumPlusUser'   => $premiumPlusUser,
            'formAction'        => array('admin.premium-plus.update', $premiumPlusUser->id),
            'formMethod'        => 'PUT',
            'formState'         => 'edit'
        ];

        return view('admin.contents.premium-plus.form', $viewData);
    }

    public function update(PremiumPlusUserRequest $request, $id)
    {
        $validator = $request->failedValidator;
        if (!is_null($validator) && $validator->fails()) {
            return back()->withErrors($validator)->withInput($request->input());
        }

        $premiumPlusUser = PremiumPlusUser::where('id', $id)->first();
        if (is_null($premiumPlusUser)) {
            return back()->with('error_message', 'Id not found');
        }

        try {
            DB::beginTransaction();
            // $consultant = $request->getConsultant()->consultant;
            $owner = $request->getOwner();
            $postParams = $request->all();
            // $postParams['consultant_id'] = $consultant->id;
            $postParams['user_id'] = $owner->id;
            
            $this->service->save($premiumPlusUser, $postParams);
            DB::commit();
            return redirect('admin/premium-plus')->with('message', 'Berhasil update akun GP4');
        } catch (\Exception $exception) {
            DB::rollBack();
            return back()->with('error_message', 'Gagal update akun GP4');
        }
    }
}