<?php
namespace App\Http\Controllers\Admin\Premium;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use App\Entities\Premium\PremiumPlusInvoice;

class PremiumPlusInvoiceController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if(!Auth::user()->can('access-pay-confirmation')) {
                return redirect('admin');
            }
            
            return $next($request);
        });
        View::share('user', Auth::user());
        View::share('contentHeader', 'Stories Management');
    }

    public function index(Request $request, $id)
    {
        $premiumPlusInvoice = PremiumPlusInvoice::with('premium_plus_user')
                                        ->where('premium_plus_user_id', $id)
                                        ->orderBy('id', 'asc')
                                        ->paginate(20);

        $viewData['boxTitle'] = "GP4 Invoice";
        $viewData['premiumPlusInvoice'] = $premiumPlusInvoice;
        return view("admin.contents.premium-plus.invoice", $viewData);
    }

}