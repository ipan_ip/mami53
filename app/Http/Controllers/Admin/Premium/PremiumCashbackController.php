<?php
namespace App\Http\Controllers\Admin\Premium;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use App\Entities\Premium\PremiumCashbackHistory;

class PremiumCashbackController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if(!Auth::user()->can('access-pay-confirmation')) {
                return redirect('admin');
            }
            
            return $next($request);
        });
        View::share('user', Auth::user());
        View::share('contentHeader', 'Stories Management');
    }

    public function index(Request $request)
    {
        $premiumCashbackHistory = PremiumCashbackHistory::with('user', 'balance_request');

        if ($request->filled('phone_number')) {
            $phoneNumber = $request->input('phone_number');
            $premiumCashbackHistory = $premiumCashbackHistory->whereHas('user', function($user) use($phoneNumber) {
                $user->where('phone_number', $phoneNumber);
            });
        }
        
        $premiumCashbackHistory = $premiumCashbackHistory->orderBy('id', 'desc')->paginate(20);
        $viewData['boxTitle'] = "Premium Cashback History";
        $viewData['premiumCashbackHistory'] = $premiumCashbackHistory;
        return view("admin.contents.premium.cashback.index", $viewData);
    }

}