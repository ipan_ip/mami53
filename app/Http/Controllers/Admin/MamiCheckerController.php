<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\User;
use App\Entities\User\UserChecker;
use App\Entities\User\UserCheckerTracker;
use App\Entities\Room\Room;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Carbon\Carbon;

class MamiCheckerController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            // TODO : separate kost owner and kost agent
            if (!\Auth::user()->can('access-mami-checker')) {
                return redirect('admin');
            }

            return $next($request);
        });

        \View::share('user', \Auth::user());
        \View::share('contentHeader', 'Mami Checker Management');
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(Request $request)
    {
        $query = UserChecker::with(['user', 'trackings', 'trackings.room'])
            ->orderBy('updated_at', 'desc');

        if ($request->filled('q')) {
            $query->whereHas('user', function($u) use ($request) {
                $u->where('name', 'LIKE', '%' . $request->q . '%');
            });
        }

        $rowsChecker = $query->paginate(20);

        foreach ($rowsChecker as $key => $row) {
            $row->photo_url = $row->user->photo->getMediaUrl();
        }

        $viewData = [
            'rowsChecker' => $rowsChecker,
            'boxTitle' => 'Mami Checker Management',
            'searchUrl' => route('admin.mami-checker.index'),
        ];

        ActivityLog::LogIndex(Action::MAMI_CHECKER);

        return view('admin.contents.mami-checker.index', $viewData);
    }

    public function storeProfile(Request $request)
    {
        try 
        {
            \DB::beginTransaction();

            $fromDB = false;

            // new userChecker from existing user
            if ($request->filled('from_db')) {
                $fromDB = true;

                $user = \App\User::find($request->id);
                if (is_null($user)) {
                    return [
                        'success' => false,
                        'message' => "Gagal mendapatkan data user. Silakan refresh halaman dan coba lagi.",
                    ];
                }
            } else {    
                // if in updating scope
                if ($request->filled('id'))
                {   
                    $checkerProfile = UserChecker::find($request->id);
                    if (is_null($checkerProfile)) {
                        return [
                            'success' => false,
                            'message' => "Gagal mendapatkan data profil Mamichecker. Silakan refresh halaman dan coba lagi.",
                        ];
                    }

                    $user = \App\User::find($checkerProfile->user_id);
                    if (is_null($user)) {
                        return [
                            'success' => false,
                            'message' => "Gagal mendapatkan data user. Silakan refresh halaman dan coba lagi.",
                        ];
                    }
                }
                // new fresh user
                else
                    {
                    $user = new \App\User;
                    // Set default password for all new MamiChecker's profile
                    $user->password = Hash::make(md5('mami-checker-2019'));
                    $user->role = 'user';
                }
            }

            // Update User table
            $user->name = $request->name;
            $user->email = $request->email;
            $user->phone_number = $request->phone;
            $user->gender = $request->gender;
            $user->work_place = $request->work_place;
            $user->address = $request->address;
            $user->photo_id = $request->photo_id;
            $user->is_verify = 1;
            $user->save();

            ActivityLog::LogCreate(Action::MAMI_CHECKER, $user->id);

            // if in updating scope
            if (!$request->filled('from_db') && $request->filled('id')) 
            {
                $checkerProfile->updated_at = Carbon::now();
                $checkerProfile->save();
            } 
            // if in adding scope
            else
            {
                // Update UserChecker table
                $checkerRegistration = UserChecker::register($user, $fromDB);
                if (is_null($checkerRegistration))
                {
                    return [
                        'success' => false,
                        'message' => "Gagal registrasi data Mamichecker baru. Silakan refresh halaman dan coba lagi.",
                    ];
                }
            }

            if (isset($checkerProfile) && !is_null($checkerProfile)) {
                ActivityLog::LogUpdate(Action::MAMI_CHECKER, $checkerProfile->id);
            }

            \DB::commit();

        } 
        catch (\Exception $e) 
        {
            Bugsnag::notifyException($e);

            return [
                'success' => false,
                'message' => "Terjadi error saat menyimpan data profil. Silakan refresh halaman dan coba lagi.",
                'error' => $e->getMessage()
            ];
        }

        return [
            'success' => true,
        ];
    }

    public function removeProfile(Request $request)
    {
        if (!$request->filled('id')) 
        {
            return [
                'success' => false,
                'message' => "Gagal menemukan data profil. Silakan refresh halaman dan coba lagi.",
            ];
        }

        try 
        {
            $userChecker = UserChecker::find($request->id);

            if (is_null($userChecker))
            {
                return [
                    'success' => false,
                    'message' => "Gagal menemukan data profil Mamichecker. Silakan refresh halaman dan coba lagi.",
                ];
            }

            $user = \App\User::find($userChecker->user_id);

            if (is_null($user)) 
            {
                return [
                    'success' => false,
                    'message' => "Gagal menemukan data user. Silakan refresh halaman dan coba lagi.",
                ];
            }

            // delete user from user table if origin is NOT 'profile-registration-db'
            if ($userChecker->from_db == 0) 
            {
                $user->delete();
            }

            // remove record
            $userChecker->delete();

        } 
        catch (\Exception $e) 
        {
            Bugsnag::notifyException($e);

            return [
                'success' => false,
                'message' => "Terjadi error saat menghapus data profil. Silakan refresh halaman dan coba lagi.",
                'error' => $e->getMessage()
            ];
        }

        ActivityLog::LogDelete(Action::MAMI_CHECKER, $request->id);

        return [
            'success' => true,
        ];
        
    }

    public function getAllCheckers(Request $request)
    {
        $checkers = UserChecker::with(['user' => function($u) {
            $u->orderBy('name');
        }]);

        if ($request->filled('q')) 
        {
            $checkers->whereHas('user', function($u) use ($request) {
                $u->where('name', 'LIKE', '%' . $request->q . '%');
            });
        }
        
        return $checkers->paginate(10);
    }

    public function getCheckerData(Request $request)
    {
        $checker = UserChecker::with(['user', 'trackings', 'trackings.room'])
        ->where('id', $request->id)->first();
        
        if (!is_null($checker))
        {
            $checker->photo_url = $checker->user->photo->getMediaUrl();
        }

        return $checker;
    }

    public function storeTracker(Request $request)
    {
        try 
        {
            $checker = UserChecker::find($request->checkerId);
            $room = Room::find($request->designerId);

            UserCheckerTracker::track($checker, $room, $request->action);
        }
        catch (\Exception $e) 
        {
            Bugsnag::notifyException($e);

            return [
                'success' => false,
                'message' => "Terjadi error saat mengupdate data checker. Silakan refresh halaman dan coba lagi.",
                'error' => $e->getMessage()
            ];
        }

        return [
            'success' => true,
        ];
    }

    public function removeTracker(Request $request)
    {
        try 
        {
            $storeTracking = UserCheckerTracker::untrack($request->id);
        }
        catch (\Exception $e) 
        {
            Bugsnag::notifyException($e);

            return [
                'success' => false,
                'message' => "Terjadi error saat mengupdate data checker. Silakan refresh halaman dan coba lagi.",
                'error' => $e->getMessage()
            ];
        }

        return [
            'success' => true,
        ];
    }
}
