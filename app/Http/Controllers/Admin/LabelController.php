<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Room\Element\Label;
use App\Entities\Room\Element\Tag;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class LabelController extends Controller
{
    protected $priority = [
        0 => 'Highest',
        1 => 'High',
        2 => 'Medium',
        3 => 'Low',
        4 => 'Lowest'
    ];

    public function __construct()
    {
        \View::share('user', \Auth::user());
        \View::share('contentHeader', 'Label Management');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $labels = Label::all();
        $tags = Tag::all()->pluck('name','id');
        $viewData = [
            'rowsLabel' => $labels,
            'boxTitle' => 'List Label',
            'tags' => $tags,
            'priority' => $this->priority
        ];

        return view('admin.contents.label.index', $viewData);
    }

    /**
     * Show the form for creating a new resource.
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $viewData = [
            'formAction' => 'admin.label.store',
            'formMethod' => 'POST',
            'label' => new Label
        ];
        return view('admin.contents.label.form', $viewData);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (is_null($request->get('tags'))) {
            $tags = "NULL";
        } else {
            $tags = json_encode($request->get('tags'));
        }

        $newLabel = [
            'name' => $request->get('name'),
            'tag' => $tags,
            'price_min' => $request->get('price_min'),
            'price_max' => $request->get('price_max'),
            'priority' => $request->get('priority')
        ];

        Label::create($newLabel);

        return redirect()->route('admin.label.index')->with('message','Label has successfully created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $label = Label::find($id);
        if ($label->tag != 'NULL') {
            $label->tag = json_decode($label->tag,true);    
        }
        $viewData = [
            'formAction' => ['admin.label.update',$label->id],
            'formMethod' => 'PUT',
            'label' => $label
        ];

        return view('admin.contents.label.form', $viewData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (is_null($request->get('tags'))) {
            $tags = "NULL";
        } else {
            $tags = json_encode($request->get('tags'));
        }

        $label = Label::find($id);

        $updateLabel = [
            'name' => $request->get('name'),
            'tag' => $tags,
            'price_min' => $request->get('price_min'),
            'price_max' => $request->get('price_max'),
            'priority' => $request->get('priority')
        ];

        foreach ($updateLabel as $index => $update) {
            $label->$index = $update;
        }

        $label->save();

        return redirect()->route('admin.label.index')->with('message','Label has successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $label = Label::find($id);
        $label->delete();

        return redirect()->route('admin.label.index')->with('message','Label has successfully deleted');
    }
}
