<?php

namespace App\Http\Controllers\Admin;

use App\Http\Helpers\ApiHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;
use Auth;

Use App\Entities\User\UserInputReward;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;


class RewardController extends Controller
{

    public function __construct() 
    {
        $this->middleware(function ($request, $next) {
            if(!Auth::user()->can('access-reward')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        View::share('contentHeader', 'User Rewards');
        View::share('user', Auth::user());
    }

    public function index(Request $request)
    {
        $query = UserInputReward::with('room');

        if($request->filled('q') && $request->get('q') != '') {
            $query->where('phone_number', 'like', '%' . $request->get('q') . '%');
        }

        $rewards = $query->orderBy('id', 'desc')->paginate(20);

        ActivityLog::LogIndex(Action::REWARD);

        return view('admin.contents.reward.index', ['rewards'=>$rewards, 'boxTitle'=>'Rewards']);
    }

    public function sent(Request $request, $rewardId)
    {
        $reward = UserInputReward::find($rewardId);

        if(!$reward) {
            return redirect()->back()->with('error_message', 'Data tidak ditemukan');
        }

        $reward->reward_sent = 1;
        $reward->save();

        ActivityLog::LogUpdate(Action::REWARD, $rewardId);

        return redirect()->back()->with('message', 'Data berhasil dirubah');

    }

    public function unsent(Request $request, $rewardId)
    {
        $reward = UserInputReward::find($rewardId);

        if(!$reward) {
            return redirect()->back()->with('error_message', 'Data tidak ditemukan');
        }

        $reward->reward_sent = 0;
        $reward->save();

        ActivityLog::LogUpdate(Action::REWARD, $rewardId);

        return redirect()->back()->with('message', 'Data berhasil dirubah');

    }
}
