<?php

namespace App\Http\Controllers\Admin\Tag;

use App\Entities\Room\Element\Type;
use App\Entities\Room\Room;
use App\Entities\Room\RoomFacility;
use App\Entities\Room\RoomFacilitySetting;
use App\Entities\Room\RoomTypeFacility;
use App\Entities\Room\RoomTypeFacilitySetting;
use App\Http\Controllers\Controller;
use Auth;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use RuntimeException;
use View;

class FacilityController extends Controller
{
	public function __construct()
	{
		$this->middleware(function ($request, $next)
		{
			if (!Auth::user()->can('access-kost'))
			{
				return redirect('admin');
			}

			return $next($request);
		});

		View::share('contentHeader', 'Add Facilities');
		View::share('user', Auth::user());
	}

	/**
	 * Show the form for adding new facility photos.
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
	 */
	public function create(int $id)
	{
		// Getting room data
		$room = Room::select(['id', 'name'])
			->with(['facility_setting'])
			->withCount('facilities')
			->find($id);

		if (is_null($room))
		{
			return Redirect::route('admin.room.index')->withErrors('Failed fetching room data. Please refresh the page and try again.');
		}

		// Redirect to Edit Page if facility photos exist
		if ($room->facilities_count > 0)
		{
			return Redirect::route('admin.room.facility.edit', $id);
		}

		$activatedTags = !is_null($room->facility_setting) ? explode(',', $room->facility_setting->active_tags) : [];

		// Getting facility structure
		$structure = RoomFacility::getFacilityStructure($activatedTags);

		$viewData = [
			'title'     => 'Facility Photo Management - <strong>' . $room->name . '</strong>',
			'roomId'    => $room->id,
			'structure' => $structure,
			'scope'     => 'adding',
		];

		return View::make('admin.contents.stories.partial.facility-form', $viewData);
	}

	public function typeCreate(int $id)
	{
		// Getting room data
		$roomType = Type::select(['id', 'name', 'designer_id'])
			->with(['room', 'facility_setting'])
			->withCount('facilities')
			->find($id);

		if (is_null($roomType))
		{
			return Redirect::route('admin.room.type.index')->withErrors('Failed fetching room type data. Please refresh the page and try again.');
		}

		// Redirect to Edit Page if facility photos exist
		if ($roomType->facilities_count > 0)
		{
			return Redirect::route('admin.room.type.facility.edit', $id);
		}

		$activatedTags = !is_null($roomType->facility_setting) ? explode(',', $roomType->facility_setting->active_tags) : [];

		// Getting facility structure
		$structure = RoomTypeFacility::getFacilityStructure($activatedTags);

		$viewData = [
			'title'        => 'Facility Photo Management - <strong>' . $roomType->room->name . '</strong> > <strong>' . $roomType->name . '</strong>',
			'roomId'       => $roomType->id,
			'parentRoomId' => $roomType->room->id,
			'structure'    => $structure,
			'scope'        => 'adding',
		];

		return View::make('admin.contents.stories.partial.type-facility-form', $viewData);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return array
	 */
	public function store(Request $request)
	{
		try
		{
			if (!RoomFacility::store($request->all()))
			{
				return [
					'success' => false,
					'message' => 'No new photos detected!',
				];
			}
			else
			{
				return [
					'success' => true,
					'message' => "Facility photos successfully saved.",
				];
			}
		}
		catch (Exception $e)
		{
			return [
				'success' => false,
				'message' => $e->getMessage(),
				'error'   => $e,
			];
		}
	}

	public function typeStore(Request $request)
	{
		try
		{
			if (!RoomTypeFacility::store($request->all()))
			{
				return [
					'success' => false,
					'message' => 'No new photos detected!',
				];
			}
			else
			{
				return [
					'success' => true,
					'message' => "Facility photos successfully saved.",
				];
			}
		}
		catch (Exception $e)
		{
			return [
				'success' => false,
				'message' => $e->getMessage(),
				'error'   => $e,
			];
		}
	}

	/**
	 * Show the form for editing facility photos
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
	 */
	public function edit(int $id)
	{
		// Getting room data
		$room = Room::select(['id', 'name'])
			->with(['facilities'])
			->withCount('facilities')
			->find($id);

		if (is_null($room))
		{
			return Redirect::route('admin.room.index')->withErrors('Failed fetching room data. Please refresh the page and try again.');
		}

		// Redirect to Add Page if facility photos DON'T exist
		if ($room->facilities_count < 1)
		{
			return Redirect::route('admin.room.facility.add', $id);
		}

		// Getting compiled facility structure
		$structure = RoomFacility::getCompiledFacilityStructure($id);

		$viewData = [
			'title'     => 'Facility Photo Management - Editing - <strong>' . $room->name . '</strong>',
			'roomId'    => $room->id,
			'structure' => $structure,
			'scope'     => 'editing',
		];

		return View::make('admin.contents.stories.partial.facility-form', $viewData);
	}

	public function typeEdit(int $id)
	{
		// Getting room data
		$roomType = Type::select(['id', 'name', 'designer_id'])
			->with(['room', 'facilities'])
			->withCount('facilities')
			->find($id);

		if (is_null($roomType))
		{
			return Redirect::route('admin.room.type.index')->withErrors('Failed fetching room type data. Please refresh the page and try again.');
		}

		// Redirect to Add Page if facility photos DON'T exist
		if ($roomType->facilities_count < 1)
		{
			return Redirect::route('admin.room.type.facility.add', $id);
		}

		// Getting compiled facility structure
		$structure = RoomTypeFacility::getCompiledFacilityStructure($id);

		$viewData = [
			'title'        => 'Facility Photo Management - Editing - <strong>' . $roomType->room->name . '</strong> > <strong>' . $roomType->name . '</strong>',
			'roomId'       => $roomType->id,
			'parentRoomId' => $roomType->room->id,
			'structure'    => $structure,
			'scope'        => 'editing',
		];

		return View::make('admin.contents.stories.partial.type-facility-form', $viewData);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return array
	 */
	public function update(Request $request)
	{
		try
		{
			$data = $request->all();

			RoomFacility::store($data);
		}
		catch (Exception $e)
		{
			return [
				'success' => false,
				'message' => $e->getMessage(),
				'error'   => $e,
			];
		}

		return [
			'success' => true,
			'message' => "Facility photos successfully updated.",
		];
	}

	public function remove(Request $request)
	{
		try
		{
			$id = (int) $request->id;

			if (!RoomFacility::remove($id))
			{
				return [
					'success' => false,
					'message' => 'Failed deleting photo. Please refresh the page and try again.',
				];
			}
			else
			{
				return [
					'success' => true,
					'message' => "Facility photos successfully deleted.",
				];
			}
		}
		catch (Exception $e)
		{
			return [
				'success' => false,
				'message' => $e->getMessage(),
				'error'   => $e,
			];
		}
	}

	public function getSetting(int $id)
	{
		// Getting room data
		$room = Room::select(['id', 'name'])
			->with('facility_setting')
			->find($id);

		if (is_null($room))
		{
			return Redirect::route('admin.room.index')->withErrors('Failed fetching room data. Please refresh the page and try again.');
		}

		// Getting compiled facility structure
		$structure = RoomFacility::getFacilityStructure();

		// Getting active tags
		$activeTags = [];
		if (!is_null($room->facility_setting))
		{
			if ($room->facility_setting->active_tags !== '')
			{
				$activeTags = explode(',', $room->facility_setting->active_tags);
			}
		}

		$viewData = [
			'title'      => '<i class=\'fa fa-wrench\'></i> Facility Setting - <strong>' . $room->name . '</strong>',
			'roomId'     => $room->id,
			'structure'  => $structure,
			'activeTags' => $activeTags,
			'scope'      => 'editing',
		];

		return View::make('admin.contents.stories.partial.facility-setting-form', $viewData);
	}

	public function typeGetSetting(int $id)
	{
		// Getting room data
		$roomType = Type::select(['id', 'name', 'designer_id'])
			->with(['room', 'facility_setting'])
			->find($id);

		if (is_null($roomType))
		{
			return Redirect::route('admin.room.index')->withErrors('Failed fetching room data. Please refresh the page and try again.');
		}

		// Getting compiled facility structure
		$structure = RoomTypeFacility::getFacilityStructure();

		// Getting active tags
		$activeTags = [];
		if (!is_null($roomType->facility_setting))
		{
			if ($roomType->facility_setting->active_tags !== '')
			{
				$activeTags = explode(',', $roomType->facility_setting->active_tags);
			}
		}

		$viewData = [
			'title'        => '<i class=\'fa fa-wrench\'></i> Room Type Facility Setting - <strong>' . $roomType->room->name . '</strong> > <strong>' . $roomType->name . '</strong>',
			'roomId'       => $roomType->id,
			'parentRoomId' => $roomType->room->id,
			'structure'    => $structure,
			'activeTags'   => $activeTags,
			'scope'        => 'editing',
		];

		return View::make('admin.contents.stories.partial.type-facility-setting-form', $viewData);
	}

	public function updateSetting(Request $request)
	{
		$data = $request->all();

		try
		{
			$room = Room::find($request->room_id);
			if (is_null($room))
			{
				throw new RuntimeException('Failed fetching room data. Please refresh the page and try again');
			}

			// Find existing settings
			$setting = RoomFacilitySetting::where('designer_id', $room->id)->first();
			if (is_null($setting))
			{
				$setting              = new RoomFacilitySetting;
				$setting->designer_id = $room->id;
			}

			// Compile activated tags
			$tags = [];
			foreach ($data as $key => $value)
			{
				if ($key != 'room_id')
				{
					$tags[] = $key;
				}
			}

			$setting->active_tags = trim(json_encode($tags), '[]');
			$setting->save();
		}
		catch (Exception $e)
		{
			return [
				'success' => false,
				'message' => $e->getMessage(),
				'error'   => $e,
			];
		}

		return [
			'success' => true,
		];
	}

	public function typeUpdateSetting(Request $request)
	{
		$data = $request->all();

		try
		{
			$roomType = Type::find($request->room_id);
			if (is_null($roomType))
			{
				throw new RuntimeException('Failed fetching room data. Please refresh the page and try again');
			}

			// Find existing settings
			$setting = RoomTypeFacilitySetting::where('designer_type_id', $roomType->id)->first();
			if (is_null($setting))
			{
				$setting                   = new RoomTypeFacilitySetting();
				$setting->designer_type_id = $roomType->id;
			}

			// Compile activated tags
			$tags = [];
			foreach ($data as $key => $value)
			{
				if ($key != 'room_id')
				{
					$tags[] = $key;
				}
			}

			$setting->active_tags = trim(json_encode($tags), '[]');
			$setting->save();
		}
		catch (Exception $e)
		{
			return [
				'success' => false,
				'message' => $e->getMessage(),
				'error'   => $e,
			];
		}

		return [
			'success' => true,
		];
	}
}
