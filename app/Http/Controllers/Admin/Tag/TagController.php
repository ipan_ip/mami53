<?php

namespace App\Http\Controllers\Admin\Tag;

use App\Entities\Activity\Action;
use App\Entities\Activity\ActivityLog;
use App\Entities\Media\Media;
use App\Entities\Room\Element\RoomTag;
use App\Entities\Room\Element\Tag;
use App\Entities\Room\Element\TagType;
use App\Http\Controllers\Controller;
use App\Http\Helpers\ApiHelper;
use Auth;
use Bugsnag;
use Exception;
use Illuminate\Http\Request;
use Input;
use Redirect;
use RuntimeException;
use View;

class TagController extends Controller
{
    const TAG_TYPE_DEFAULT = 'emotion';

    /**
     * TagController constructor.
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next)
        {
            if (!Auth::user()->can('access-tag'))
            {
                return redirect('admin');
            }

            return $next($request);
        });

        View::share('contentHeader', 'Facility Tag Management');
        View::share('user', Auth::user());
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index(Request $request)
    {
        $query = Tag::getSortedFac();

        if ($request->filled('q'))
        {
            $query->where('name', 'like', '%' . $request->q . '%');
        }

        $tags = $query->paginate(15);

        $viewData = [
            'boxTitle'     => 'Facility Tags Management',
            'deleteAction' => 'admin.tag.destroy',
            'rowsTag'      => $tags,
            'openModal'    => $request->filled('open_modal')
        ];

        ActivityLog::LogIndex(Action::TAG);

        return View::make('admin.contents.tag.index', $viewData);
    }

    public function create(Request $request)
    {
        return redirect()->action('Admin\Tag\TagController@index', [
            'open_modal' => true,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function store(Request $request)
    {
        try
        {
            $isEditing = $request->filled('id');

            // If it's an EDITING mode
            if ($isEditing)
            {
                $tag 			= Tag::find((int) $request->id);
                $tagTypeLabel 	= ($request->filled('label')) ? $request->label : $tag->type;
            }
            // Else, it's an ADDING mode
            else
            {
                $tag 			= new Tag;
                $tagTypeLabel 	= self::TAG_TYPE_DEFAULT;
            }

            $tag->name             = $request->name;
            $tag->type             = $tagTypeLabel;

            if ($request->file('icon'))
            {
                $icon = Media::postTagIcon($request->file('icon'), 'upload', Auth::id(), 'user_id', null, 'user_photo');
                if ($icon)
                {
                    $tag->photo_id = $icon['id'];
                }
            }

            if ($request->file('icon_small'))
            {
                $icon = Media::postTagIcon($request->file('icon_small'), 'upload', Auth::id(), 'user_id', null, 'user_photo');
                if ($icon)
                {
                    $tag->small_photo_id = $icon['id'];
                }
            }

            $isSuccess = $tag->save();

            //save type in tag_type table
            if (!$isEditing) {
                TagType::createTagType($request, $tag);
            } else {
                TagType::updateTagType($request, $tag);
            }

            if ($isSuccess)
            {
                ActivityLog::LogCreate(Action::TAG, $tag->id);
            }
            else
            {
                new RuntimeException("Failed " . $isEditing ? 'Updating' : 'Adding' . " facility type. Please refresh page and try again.");
            }
        }
        catch (Exception $e)
        {
            Bugsnag::notifyException($e);

            return [
                'success' => false,
                'message' => $e->getMessage(),
                'error'   => $e,
            ];
        }

        return [
            'success' => true,
        ];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $arrInputOld = ['name', 'type'];

        $this->validate($request, [
            'name' => 'required',
            'type' => 'required',
        ]);

        $rowTag = Tag::find($id);

        if ($request->file('icon'))
        {
            $icon = Media::postTagIcon($request->file('icon'), 'upload', Auth::id(), 'user_id', null, 'user_photo');
            if ($icon)
            {
                $rowTag->photo_id = $icon['id'];
            }
        }

        if ($request->file('icon_small'))
        {
            $icon = Media::postTagIcon($request->file('icon_small'), 'upload', Auth::id(), 'user_id', null, 'user_photo');
            if ($icon)
            {
                $rowTag->small_photo_id = $icon['id'];
            }
        }

        $rowTag->name           = Input::get('name');
        $rowTag->type           = Input::get('type');
        $rowTag->is_highlighted = 'true';
        $rowTag->count          = 0;

        $isSuccess = $rowTag->save();

        if ($isSuccess)
        {
            ActivityLog::LogUpdate(Action::TAG, $id);
            return Redirect::route('admin.tag.index')
                ->with('message', 'Tag Updated');
        }
        else
        {
            return Redirect::route('admin.tag.create')
                ->with('error_message', 'Aborted')
                ->withInput();
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function enable(Request $request)
    {
        try
        {
            $activate = Tag::enablePhotoUpload($request->id, true);

            if (!$activate)
            {
                new RuntimeException('Failed enabling photo upload. Please refresh page and try again.');
            }
        }
        catch (Exception $e)
        {
            Bugsnag::notifyException($e);

            return [
                'success' => false,
                'message' => $e->getMessage(),
                'error'   => $e,
            ];
        }

        return [
            'success' => true,
            'title'   => 'Photo upload successfully enabled',
            'message' => "Page will be refreshed after you clicked OK",
        ];
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function disable(Request $request)
    {
        try
        {
            $activate = Tag::enablePhotoUpload($request->id, false);

            if (!$activate)
            {
                new RuntimeException('Failed disabling photo upload. Please refresh page and try again.');
            }
        }
        catch (Exception $e)
        {
            Bugsnag::notifyException($e);

            return [
                'success' => false,
                'message' => $e->getMessage(),
                'error'   => $e,
            ];
        }

        return [
            'success' => true,
            'title'   => 'Photo upload successfully disabled',
            'message' => "Page will be refreshed after you clicked OK",
        ];
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function set(Request $request)
    {
        try
        {
            $activate = Tag::setAsTopFacility($request->id, true);

            if (!$activate)
            {
                new RuntimeException('Failed setting tag as Top Facility. Please refresh page and try again.');
            }
        }
        catch (Exception $e)
        {
            Bugsnag::notifyException($e);

            return [
                'success' => false,
                'message' => $e->getMessage(),
                'error'   => $e,
            ];
        }

        return [
            'success' => true,
            'title'   => 'Tag successfully set as Top Facility',
            'message' => "Page will be refreshed after you clicked OK",
        ];
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function unset(Request $request)
    {
        try
        {
            $activate = Tag::setAsTopFacility($request->id, false);

            if (!$activate)
            {
                new RuntimeException('Failed un-setting tag from Top Facility. Please refresh page and try again.');
            }
        }
        catch (Exception $e)
        {
            Bugsnag::notifyException($e);

            return [
                'success' => false,
                'message' => $e->getMessage(),
                'error'   => $e,
            ];
        }

        return [
            'success' => true,
            'title'   => 'Tag successfully unset from Top Facility',
            'message' => "Page will be refreshed after you clicked OK",
        ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function remove(Request $request)
    {
        try
        {
            if (!$request->filled('id'))
            {
                new RuntimeException('Failed fetching tag. Please refresh page and try again.');
            }

            // Look for rooms still using this tag
            $room_tag = RoomTag::where('tag_id', $request->id)->count();
            if ($room_tag > 0)
            {
                return [
                    'success' => false,
                    'message' => 'Unable to delete tag! Because it is in use.',
                ];
            }

            if (!Tag::remove($request->id))
            {
                new RuntimeException('Failed deleting tag. Please refresh page and try again.');
            }
        }
        catch (Exception $e)
        {
            Bugsnag::notifyException($e);

            return [
                'success' => false,
                'message' => $e->getMessage(),
                'error'   => $e,
            ];
        }

        TagType::where('tag_id', $request->id)->delete();

        return [
            'success' => true,
            'title'   => 'Tag Successfully Removed!',
            'message' => "Page will be refreshed after you clicked OK",
        ];
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function isUnique()
    {
        ApiHelper::isValidInput(
            Input::all(),
            [
                'fields' => 'required',
                'type'   => 'required|in:tag.name',
            ]
        );

        $fields = Input::get('fields');

        if (Input::get('type') === 'tag.name')
        {
            $tag = Tag::where('name', '=', $fields['name'])
                ->where('type', '=', $fields['type'])
                ->first();

            if ($tag === NULL)
            {
                return ApiHelper::responseData(['valid' => TRUE]);
            }
            else
            {
                return ApiHelper::responseData(['valid' => FALSE]);
            }
        }
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function setTopFacility($id)
    {
        $tag = Tag::find($id);
        if (is_null($tag))
        {
            return back()->with('error_message', 'Data tidak ditemukan');
        }

        $tag->is_top = $tag->is_top == 1 ? 0 : 1;
        $tag->save();

        return back()->with('message', 'Sukses');
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function sortOrder(Request $request)
    {
        $orderData = $request->all();

        try
        {
            $saveOrder = Tag::saveSortOrder($orderData);

            if (!$saveOrder)
            {
                return [
                    'success' => false,
                    'message' => "Gagal menyimpan urutan baru. Silakan refresh halaman dan coba lagi!",
                ];
            }

            return [
                'success' => true,
            ];
        }
        catch (Exception $e)
        {
            return [
                'success' => false,
                'message' => $e->getMessage(),
            ];
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function display(Request $request)
    {
        try {
            $activate = Tag::displayInList($request->id, true);

            if (!$activate) {
                new RuntimeException('Failed to display in List. Please refresh page and try again.');
            }
        } catch (Exception $e) {
            Bugsnag::notifyException($e);

            return [
                'success' => false,
                'message' => $e->getMessage(),
                'error' => $e,
            ];
        }

        return [
            'success' => true,
            'title' => 'Tag successfully displayed in list',
            'message' => "Page will be refreshed after you clicked OK",
        ];
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function undisplay(Request $request)
    {
        try {
            $activate = Tag::displayInList($request->id, false);

            if (!$activate) {
                new RuntimeException('Failed to undisplay in list. Please refresh page and try again.');
            }
        } catch (Exception $e) {
            Bugsnag::notifyException($e);

            return [
                'success' => false,
                'message' => $e->getMessage(),
                'error' => $e,
            ];
        }

        return [
            'success' => true,
            'title' => 'Tag successfully undisplay in list',
            'message' => "Page will be refreshed after you clicked OK",
        ];
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function addToFilter(Request $request)
    {
        try {
            $activate = Tag::setAsFacilityFilter($request->id, true);

            if (!$activate) {
                new RuntimeException('Failed to set as filter. Please refresh page and try again.');
            }
        } catch (Exception $e) {
            Bugsnag::notifyException($e);

            return [
                'success' => false,
                'message' => $e->getMessage(),
                'error' => $e,
            ];
        }

        return [
            'success' => true,
            'title' => 'Tag successfully added to filter',
            'message' => "Page will be refreshed after you clicked OK",
        ];
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function removeFromFilter(Request $request)
    {
        try {
            $activate = Tag::setAsFacilityFilter($request->id, false);

            if (!$activate) {
                new RuntimeException('Failed to remove from filter. Please refresh page and try again.');
            }
        } catch (Exception $e) {
            Bugsnag::notifyException($e);

            return [
                'success' => false,
                'message' => $e->getMessage(),
                'error' => $e,
            ];
        }

        return [
            'success' => true,
            'title' => 'Tag successfully unset from filter',
            'message' => "Page will be refreshed after you clicked OK",
        ];
    }

    public function deleteSecondLabel(Request $request)
    {
        try {
            $delete = TagType::deleteSecondTagType($request);

            if (!$delete) {
                new RuntimeException('Failed to delete Type. Please refresh page and try again.');
            }
        } catch (Exception $e) {
            Bugsnag::notifyException($e);

            return [
                'success' => false,
                'message' => $e->getMessage(),
                'error' => $e,
            ];
        }

        return [
            'success' => true,
            'title' => 'Type successfully deleted',
            'message' => "Page will be refreshed after you clicked OK",
        ];
    }
}
