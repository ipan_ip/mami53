<?php

namespace App\Http\Controllers\Admin\Tag;

use App\Entities\Activity\Action;
use App\Entities\Activity\ActivityLog;
use App\Entities\Room\Element\Tag;
use App\Entities\Room\Element\TagMaxRenter;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;
use Redirect;
use Validator;
use View;

class TagMaxRenterController extends Controller
{
	protected const DEFAULT_PAGINATION = 20;

	public function __construct()
	{
		$this->middleware(function ($request, $next) {
			if (!Auth::user()->can('access-tag')) {
				return redirect('admin');
			}

			return $next($request);
		});

		View::share('contentHeader', 'Manage Max Renter by Tag');
		View::share('user', Auth::user());
	}

	public function index(Request $request)
	{
		$query = TagMaxRenter::with('tag');

		if ($request->filled('q')) {
			$term = $request->q;
			$query->whereHas('tag', function ($queryTag) use ($term) {
				$queryTag->where('name', 'like', '%' . $term . '%');
			});
		}

		$tagMaxRenter = $query->paginate(self::DEFAULT_PAGINATION);

		$viewData = [
			'boxTitle'     => 'Manage Max Renter by Tag',
			'deleteAction' => 'admin.tag-max-renter.destroy',
			'tagMaxRenter' => $tagMaxRenter,
		];

		ActivityLog::LogIndex(Action::TAG_MAX_RENTER);

		return View::make('admin.contents.tag.max-renter.index', $viewData);
	}

    public function show(Request $request, KostLevel $level)
    {
        abort(404);
    }

    public function create()
    {
        $tags = Tag::all();
        $viewData = [
            'tags' => $tags,
            'boxTitle' => 'Add Tag Max Renter'
        ];
        return View::make('admin.contents.tag.max-renter.edit', $viewData);
    }

	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [
            'tag_id' => 'required',
            'max_renter' => 'required|numeric|min:1',
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withInput()->with('errors', $validator->errors());
        }

		$tagMaxRenter = new TagMaxRenter();
		$tagMaxRenter->tag_id = $request->tag_id;
		$tagMaxRenter->max_renter = $request->max_renter;
		$tagMaxRenter->save();

		ActivityLog::LogCreate(Action::TAG_MAX_RENTER, $tagMaxRenter->id);

        return Redirect::route('admin.tag-max-renter.index')->with('message', 'Max Renter successfully added.');
	}

    public function edit(TagMaxRenter $tagMaxRenter)
    {
        $tags = Tag::all();

        $viewData = [
			'tags' => $tags,
			'tagMaxRenter' => $tagMaxRenter,
            'boxTitle' => 'Edit Tag Max Renter'
        ];
        return View::make('admin.contents.tag.max-renter.edit', $viewData);
    }

	public function update(Request $request, TagMaxRenter $tagMaxRenter)
	{
		$validator = Validator::make($request->all(), [
            'tag_id' => 'required',
            'max_renter' => 'required|numeric|min:1',
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withInput()->with('errors', $validator->errors());
        }

		$tagMaxRenter->tag_id = $request->tag_id;
		$tagMaxRenter->max_renter = $request->max_renter;
		$tagMaxRenter->save();

		ActivityLog::LogUpdate(Action::TAG_MAX_RENTER, $tagMaxRenter->id);

        return Redirect::route('admin.tag-max-renter.index')->with('message', 'Max Renter successfully updated.');
	}

    public function destroy($id)
    {
		$tagMaxRenter = TagMaxRenter::find($id);
		if (is_null($tagMaxRenter)) {
			abort(404);
		}
        $tagMaxRenter->delete();

        ActivityLog::LogDelete(Action::TAG_MAX_RENTER, $id);

        return Redirect::route('admin.tag-max-renter.index')->with('message', 'Max Renter successfully deleted.');
    }
}
