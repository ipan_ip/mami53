<?php

namespace App\Http\Controllers\Admin\Tag;

use App\Entities\Activity\Action;
use App\Entities\Activity\ActivityLog;
use App\Entities\Facility\FacilityCategory;
use App\Http\Controllers\Controller;
use Auth;
use Bugsnag;
use Exception;
use Illuminate\Http\Request;
use RuntimeException;
use View;

class FacilityCategoryController extends Controller
{
	public function __construct()
	{
		$this->middleware(function ($request, $next)
		{
			if (!Auth::user()->can('access-tag'))
			{
				return redirect('admin');
			}

			ActivityLog::LogIndex(Action::TAG);

			return $next($request);
		});

		View::share('contentHeader', 'Facility Category Management');
		View::share('user', Auth::user());
	}

	/**
	 * @return \Illuminate\Contracts\View\View
	 */
	public function index()
	{
		$categories = FacilityCategory::with('creator')
			->orderBy('updated_at', 'desc')
			->get();

		$viewData = [
			'boxTitle'     => 'Facility Category Management',
			'deleteAction' => 'admin.tag-category.destroy',
			'categories'      => $categories,
		];

		return View::make('admin.contents.tag.category.index', $viewData);
	}

	/**
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return array
	 */
	public function store(Request $request)
	{
		try
		{
			FacilityCategory::store($request->all());
		}
		catch (Exception $e)
		{
			return [
				'success' => false,
				'message' => $e->getMessage(),
				'error'   => $e,
			];
		}

		return [
			'success' => true,
			'message' => "Facility Category successfully created.",
		];
	}

	/**
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return array
	 */
	public function remove(Request $request)
	{
		try
		{
			if (!$request->filled('id'))
			{
				new RuntimeException('Failed fetching facility category. Please refresh page and try again.');
			}

			// Look for rooms still using this tag
			$tagCategory = FacilityCategory::with('types')->find($request->id);
			if ($tagCategory->types->count())
			{
				if ($tagCategory->types->count() <= 1)
				{
					$msg = 'Failed to delete facility category. Because there is active facility type under it!<br/>Please remove it first.';
				}
				else
				{
					$msg = 'Failed to delete facility category. Because there are active facility types under it!<br/>Please remove them first.';
				}

				return [
					'success' => false,
					'message' => $msg,
				];
			}

			if (!FacilityCategory::remove($request->id))
			{
				new RuntimeException('Failed deleting facility. Please refresh page and try again.');
			}
		}
		catch (Exception $e)
		{
			Bugsnag::notifyException($e);

			return [
				'success' => false,
				'message' => $e->getMessage(),
				'error'   => $e,
			];
		}

		return [
			'success' => true,
			'title'   => 'Facility Category Successfully Removed!',
			'message' => "Page will be refreshed after you clicked OK",
		];
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
	 */
	public function getAllCategories()
	{
		return FacilityCategory::query()->pluck('name', 'id')->toArray();
	}



	/**
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return mixed
	 */
	public function getAllCategoriesInAjax(Request $request)
	{
		return FacilityCategory::orderBy('name')
			->pluck('id', 'name')
			->toArray();
	}
}
