<?php

namespace App\Http\Controllers\Admin\Tag;

use App\Entities\Activity\Action;
use App\Entities\Activity\ActivityLog;
use App\Entities\Facility\FacilityType;
use App\Http\Controllers\Controller;
use Auth;
use Bugsnag;
use Exception;
use Illuminate\Http\Request;
use RuntimeException;
use Validator;
use View;

class FacilityTypeController extends Controller
{
	public function __construct()
	{
		$this->middleware(function ($request, $next)
		{
			if (!Auth::user()->can('access-tag'))
			{
				return redirect('admin');
			}

			return $next($request);
		});

		View::share('contentHeader', 'Facility Type Management');
		View::share('user', Auth::user());
	}

	/**
	 * @return \Illuminate\Contracts\View\View
	 */
	public function index()
	{
		$rowsTag = FacilityType::with(['facility_category', 'creator'])
			->orderBy('updated_at', 'desc')
			->get();

		$viewData = [
			'boxTitle'     => 'Facility Type Management',
			'deleteAction' => 'admin.tag-type.destroy',
			'rowsTag'      => $rowsTag,
		];

		ActivityLog::LogIndex(Action::TAG);

		return View::make('admin.contents.tag.type.index', $viewData);
	}

	/**
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return array
	 */
	public function store(Request $request)
	{
		try
		{
			FacilityType::store($request->all());
		}
		catch (Exception $e)
		{
			return [
				'success' => false,
				'message' => $e->getMessage(),
				'error'   => $e,
			];
		}

		return [
			'success' => true,
			'message' => "Facility Type successfully created.",
		];
	}

	/**
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return array
	 */
	public function remove(Request $request)
	{
		try
		{
			if (!$request->filled('id'))
			{
				new RuntimeException('Failed fetching facility type. Please refresh page and try again.');
			}

			// Look for tags that are still using this tag type
			$tagType = FacilityType::with('facilities')->find($request->id);
			if ($tagType->facilities->count())
			{
				if ($tagType->facilities->count() <= 1)
				{
					$msg = 'Failed to delete tag type. Because there is active room facility under it!<br/>Please remove it first.';
				}
				else
				{
					$msg = 'Failed to delete tag type. Because there are active room facility under it!<br/>Please remove them first.';
				}

				return [
					'success' => false,
					'message' => $msg,
				];
			}

			if (!FacilityType::remove($request->id))
			{
				new RuntimeException('Failed deleting facility type. Please refresh page and try again.');
			}
		}
		catch (Exception $e)
		{
			Bugsnag::notifyException($e);

			return [
				'success' => false,
				'message' => $e->getMessage(),
				'error'   => $e,
			];
		}

		return [
			'success' => true,
			'title'   => 'Facility Type Successfully Removed!',
			'message' => "Page will be refreshed after you clicked OK",
		];
	}

	/**
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return mixed
	 */
	public function getAllTypesInAjax(Request $request)
	{
		return FacilityType::where('facility_category_id', (int) $request->parent)
			->orderBy('name')
			->pluck('id', 'name')
			->toArray();
	}

	public function sort(Request $request) {
		$validator = Validator::make($request->all(), [
			'order' => 'required',
		]);

		$validator->after(function ($validator) use ($request) {
			if ($request->order) {
				$orderById = explode(',', $request->order);

				foreach ($orderById as $id) {
					if (!FacilityType::where('id', $id)->exists()) {
						$validator->errors()->add('order', 'A tag sorted does not exist');
					}
				}

				if (FacilityType::whereNotIn('id', $orderById)->count() > 0) {
					$validator->errors()->add('order', 'A tag could not be found on the new sorted tag');
				}
			}
		});

		if ($validator->fails()) {
			return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
		}

		$orderById = explode(',', $request->order);
		foreach ($orderById as $order => $id) {
			FacilityType::where('id', $id)->update(['order' => ($order + 1)]);
		}

		return redirect()->back()->with("message","Order has been successfully updated");
	}
}
