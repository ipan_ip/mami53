<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\Room\Room;
use Carbon\Carbon;
use App\Entities\Room\Element\Verify;
use Illuminate\Support\Facades\View;
use Auth;
use DB;
use App\Entities\Room\Element\Card;

class AgentController extends Controller
{

	public function __construct()
    {
        View::share('contentHeader', 'Agent Images Management');
        View::share('user', Auth::user());
    }

    public function getListImages(Request $request, $phone)
    {
    	if (! $phone) {
            return 'error';
        }

        $rooms = Room::with('cards', 'cards.photo');
        if ($request->filled("type") AND $request->input("type") == "owner") {
            $rooms = $rooms->where("owner_phone", $phone)->orWhere("manager_phone", $phone);
        } else {
            $rooms = $rooms->whereAgentPhone($phone);
        }                    
        
        $rooms = $rooms->orderBy('id','desc')->paginate(20);

        return view('admin.contents.agent.images', array('rowsDesigner' => $rooms, 'phone' => $phone));
    }

    public function verifiedRoom($phone)
    {
        if (! $phone) {
            return 'error';
        }

        $designers = Room::whereAgentPhone($phone)
                             ->orderBy('id','desc')
                             ->get(['id','name','is_active','agent_name', 'owner_phone', 'manager_phone', 'expired_phone','agent_phone','created_at'])
                             ->toArray();

        $designerVerifies = Verify::select(DB::raw('max(id) as id, created_at, designer_id, action '))
                                          ->whereIn('designer_id', array_pluck($designers, 'id'))
                                          ->groupBy('designer_id', 'created_at', 'action')
                                          ->get()->toArray();

        $designerVerifiesCreatedIds = array();
        $carbon = new Carbon;
        foreach ($designerVerifies as $designerVerify) {
            $designerVerifiesCreatedIds[$designerVerify['designer_id']] = array(
                'created_at' => $designerVerify['created_at'],
                'action'     => $designerVerify['action'],
            );
        }
        
        $beforeWeek = null;

        foreach ($designers as $key => $designer) {
            if (isset($designerVerifiesCreatedIds[$designer['id']])) {
                $designers[$key]['verified_at'] = $designerVerifiesCreatedIds[$designer['id']]['created_at'];
                $designers[$key]['action'] = $designerVerifiesCreatedIds[$designer['id']]['action'];
            } else {
                $designers[$key]['verified_at'] = null;
                $designers[$key]['action'] = null;
            }
                if ($beforeWeek == $carbon->parse($designer['created_at'])->weekOfYear) {
                   $designers[$key]['week'] = null;    
                } else {
                   $designers[$key]['week'] = $carbon->parse($designer['created_at'])->weekOfYear;
                }
                $beforeWeek              = $carbon->parse($designer['created_at'])->weekOfYear;

                if (is_null($designer['owner_phone']) OR empty($designer['owner_phone']) OR $designer['owner_phone'] == '-') {
                    $designers[$key]['phone'] = $designer['manager_phone'];
                } else {
                    $designers[$key]['phone'] = $designer['owner_phone'];
                }
        }
        return view('admin.contents.agen.statistik', array('designers' => $designers, 'verified' => 0, 'unverified' => 0, 'notlisted' => 0));
    }
}
