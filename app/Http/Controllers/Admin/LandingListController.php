<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Room\Booking;
use App\Http\Helpers\ApiHelper;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Auth;
use Validator;
use App\Entities\Vacancy\Vacancy;
use App\Entities\Vacancy\Spesialisasi;
use App\Entities\Landing\LandingList;
use App\Entities\Aggregator\AggregatorPartner;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;


class LandingListController extends Controller
{
    protected $sortingJob = [
        'default' => 'via Mamikos',
        'new' => 'dari Lowongan Terbaru', 
        'old' => 'dari Lowongan Terlama'
    ];

    protected $sortingKost = [
        '-' => 'Acak',
        'asc' => 'Terendah ke Tertinggi',
        'desc' => 'Tertinggi ke Terendah',
        'availability' => 'Kosong ke Penuh'
    ];

    protected $filterListJob = [
        'city'          => 'Kota',
        'job_status'    => 'Status Pekerjaan', 
        'last_education'=> 'Pendidikan Terakhir', 
        'sorting'       => 'Urutan',
        'spesialisasi'  => 'Spesialisasi'
    ];

    protected $filterListKost = [
        'city'  => 'Kota',
        'gender' => 'Tipe Kost',
        'min_payment' => 'Jangka Waktu',
        'sorting' => 'Urutan'
    ];

    protected $filterListApartment = [
        'city'  => 'Kota',
        'furnished' => 'Perabotan',
        'min_payment' => 'Jangka Waktu',
        'sorting' => 'Urutan'
    ];

    protected $filterFurnished = [
        'all' => 'All',
        0 => 'Not Furnished',
        1 => 'Furnished',
        2 => 'Semi Furnished'
    ];

    protected $filterCityJob = [
        'all' => 'Semua Kota',
        'Jakarta Utara' => 'Jakarta Utara',
        'Jakarta Timur' => 'Jakarta Timur',
        'Jakarta Selatan' => 'Jakarta Selatan',
        'Jakarta Barat' => 'Jakarta Barat',
        'Yogyakarta' => 'Yogyakarta',
        'Bandung' => 'Bandung',
        'Surabaya' => 'Surabaya',
        'Bali' => 'Bali',
        'Semarang' => 'Semarang',
        'Malang' => 'Malang',
        'Medan' => 'Medan',
        'Makassar' => 'Makassar',
        'Palembang' => 'Palembang',
        'Batam' => 'Batam',
        'Pekanbaru' => 'Pekanbaru',
        'Balikpapan' => 'Balikpapan',
        'Padang' => 'Padang',
        'Pontianak' => 'Pontianak',
        'Manado' => 'Manado',
        'Denpasar' => 'Denpasar',
    ];

    public function __construct() 
    {
        $this->middleware(function ($request, $next) {
            if(!Auth::user()->can('access-landing-list-creator')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        View::share('contentHeader', 'Landing List Management');
        View::share('user', Auth::user());
    }

    public function index(Request $request)
    {
        $landingLists = LandingList::whereNotIn('type', Booking::LANDING_BOOKING_TYPES);

        if ($request->filled('q') && $request->get('q') !== "") {
            $landingLists = $landingLists->where('keyword', 'like', '%' . $request->q . '%');
        }

        $landingLists = $landingLists->paginate(20);

        $viewData    = array(
        	'boxTitle'       => 'Landing List Creator',
        	'landingLists'   => $landingLists
        ); 

        ActivityLog::LogIndex(Action::LANDING_LIST_CREATOR);

        return view ("admin.contents.landing-list-creator.index", $viewData);
    }

    public function createJob(Request $request)
    {
        $sorting      = $this->sortingJob;
        $filterList   = $this->filterListJob;
        $spesialisasi = Spesialisasi::where('is_active', 1)->get();
        $aggregator   = AggregatorPartner::where('is_active', 1)->get();

    	$viewData = [
    		'boxTitle'              => 'Create Landing List Creator Job',
            'type'                  => Vacancy::VACANCY_TYPE,
            'educationOptions'      => Vacancy::EDUCATION_OPTION,
            'sortingOptions'        => $sorting,
            'filterOptions'         => $filterList,
            'filterCities'          => $this->filterCityJob,
            'vacancySpesialisasi'   => $spesialisasi,
            'aggregatorPartner'     => $aggregator
    	];

    	return view ("admin.contents.landing-list-creator.create-job", $viewData);
    }

    public function storeJob(Request $request)
    {
    	$validator = Validator::make($request->all(), [
            'title'           => 'required',
            'slug'            => 'required',
            'job_status'      => 'required',
            'last_education'  => 'required',
            'partner_id'      => 'nullable',
            'sorting'         => 'required'
        ]);

        $slug = str_replace(' ', '', strtolower($request->slug));

        $validator->after(function($validator) use ($request, $slug) {
            if (!is_null($request->input('partner_id')) && $request->input('partner_id') != '') {
                $partnerExist = AggregatorPartner::find($request->input('partner_id'));

                if (!$partnerExist) {
                    $validator->errors()->add('partner_id', 'Aggregator Partner tidak ditemukan');
                }
            }

            if (!is_null($request->input('slug')) && $request->input('slug') != '') {
                $slugExist = LandingList::where('slug', $slug)
                                ->where('type', 'job')
                                ->first();

                if ($slugExist) {
                    $validator->errors()->add('slug', 'Slug sudah digunakan sebelumnya');
                }
            }
        });

        if($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        $active_filter = $request->active_filter;
        if (is_null($active_filter)) {
            $active_filter = [];
        }

        $LandingList                = new LandingList;
        $LandingList->type          = 'job';
        $LandingList->slug          = $slug;
        $LandingList->keyword       = $request->title;
        $LandingList->description   = $request->description;
        $LandingList->option        =json_encode([
            'job_status'     => $request->job_status,
            'last_education' => $request->last_education,
            'partner_id'     => $request->partner_id,
            'sorting'        => $request->sorting,
            'active_filter'  => $active_filter,
            'is_niche'       => is_null($request->is_niche) ? 0 : $request->is_niche,
            'spesialisasi'   => $request->spesialisasi,
            'city'           => $request->city
        ]);

        $LandingList-> save();

        ActivityLog::LogCreate(Action::LANDING_JOBS, $LandingList->id);

        return redirect()->route('admin.list-creator.index')->with('message', 'Sukses menambah landing list');

    }

    public function edit(Request $request, $id)
    {
        $sorting     = $this->sortingJob;
        $filterList  = $this->filterListJob;
        $LandingList = LandingList::find($id);
        $options     = json_decode($LandingList->option, true);
        $spesialisasi = Spesialisasi::where('is_active', 1)->get();
        $aggregator   = AggregatorPartner::where('is_active', 1)->get();

        $viewData = [
            'boxTitle'         => 'Edit Landing List Creator Job',
            'type'             => Vacancy::VACANCY_TYPE,
            'educationOptions' => Vacancy::EDUCATION_OPTION,
            'sortingOptions'   => $sorting,
            'filterOptions'    => $filterList,
            'filterCities'     => $this->filterCityJob,
            'landingList'      => $LandingList,
            'options'          => $options,
            'vacancySpesialisasi'   => $spesialisasi,
            'aggregatorPartner'     => $aggregator
        ];

        return view ("admin.contents.landing-list-creator.edit-job", $viewData);
    }

    public function update(Request $request, $id)
    {
       $LandingList = LandingList::find($id);

        if(!$LandingList) {
            return redirect()->back()->with('error_message', 'Data tidak ditemukan');
        }

        $validator = Validator::make($request->all(), [
            'title'           => 'required',
            'slug'            => 'required',
            'job_status'      => 'required',
            'last_education'  => 'required',
            'partner_id'      => 'nullable',
            'sorting'         => 'required'
        ]);

        $slug = str_replace(' ', '', strtolower($request->slug));

        $validator->after(function($validator) use ($request, $slug, $LandingList) {
            if (!is_null($request->input('partner_id')) && $request->input('partner_id') != '') {
                $partnerExist = AggregatorPartner::find($request->input('partner_id'));

                if (!$partnerExist) {
                    $validator->errors()->add('partner_id', 'Aggregator Partner tidak ditemukan');
                }
            }

            if (!is_null($request->input('slug')) && $request->input('slug') != '') {
                $slugExist = LandingList::where('slug', $slug)
                                ->where('type', 'job')
                                ->first();

                if ($slugExist && $slugExist->id != $LandingList->id) {
                    $validator->errors()->add('slug', 'Slug sudah digunakan sebelumnya');
                }
            }
        });

        if($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        $active_filter = $request->active_filter;
        if (is_null($active_filter)) {
            $active_filter = [];
        }

        $LandingList->type          = 'job';
        $LandingList->slug          = $slug;
        $LandingList->keyword       = $request->title;    
        $LandingList->description   = $request->description;    
        $LandingList->option        =json_encode([
            'job_status'     => $request->job_status,
            'last_education' => $request->last_education,
            'partner_id'     => $request->partner_id,
            'sorting'        => $request->sorting,
            'active_filter'  => $active_filter,
            'is_niche'       => is_null($request->is_niche) ? 0 : $request->is_niche,
            'spesialisasi'   => $request->spesialisasi,
            'city'           => $request->city
        ]);

        $LandingList-> save();
        
        ActivityLog::LogUpdate(Action::LANDING_LIST_CREATOR, $id);

        return redirect()->route('admin.list-creator.index')->with('message', 'Sukses mengupdate landing list creator');
    }

    public function createKost(Request $request)
    {
        $sorting      = $this->sortingKost;
        $filterList   = $this->filterListKost;

        $viewData = [
            'boxTitle'              => 'Create Landing List Creator Kost',
            'sortingOptions'        => $sorting,
            'filterOptions'         => $filterList,
            'filterCities'          => $this->filterCityJob,
        ];

        return view ("admin.contents.landing-list-creator.create-kost", $viewData);
    }

    public function storeKost(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title'           => 'required',
            'slug'            => 'required',
            'sorting'         => 'required',
            'latitude_1'      => 'required_with:longitude_1,latitude_2,longitude_2',
            'longitude_1'     => 'required_with:latitude_1,latitude_2,longitude_2',
            'latitude_2'      => 'required_with:latitude_1, longitude_1,longitude_2',
            'longitude_2'     => 'required_with:latitude_1,longitude_1,latitude_2',
        ]);

        $slug = str_replace(' ', '', strtolower($request->slug));

        $validator->after(function($validator) use ($request, $slug) {
            if (!is_null($request->input('slug')) && $request->input('slug') != '') {
                $slugExist = LandingList::where('slug', $slug)
                                ->where('type', 'kost')
                                ->first();

                if ($slugExist) {
                    $validator->errors()->add('slug', 'Slug sudah digunakan sebelumnya');
                }
            }
        });

        if($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        $active_filter = $request->active_filter;
        if (is_null($active_filter)) {
            $active_filter = [];
        }

        $location = [];

        if ($request->latitude_1 != '' && $request->longitude_1 != '' 
            && $request->latitude_2 != '' && $request->longitude_2 != '') {
            $location = [
                [$request->longitude_1, $request->latitude_1],
                [$request->longitude_2, $request->latitude_2]
            ];
        }

        $LandingList                = new LandingList;
        $LandingList->type          = 'kost';
        $LandingList->slug          = $slug;
        $LandingList->keyword       = $request->title;
        $LandingList->description   = $request->description;
        $LandingList->option        =json_encode([
            'sorting'        => $request->sorting,
            'active_filter'  => $active_filter,
            'city'           => $request->city,
            'location'       => $location
        ]);

        $LandingList-> save();

        ActivityLog::LogCreate(Action::LANDING_KOST, $LandingList->id);

        return redirect()->route('admin.list-creator.index')->with('message', 'Sukses menambah landing list');
    }

    public function editKost(Request $request, $id)
    {
        $sorting     = $this->sortingKost;
        $filterList  = $this->filterListKost;
        $LandingList = LandingList::find($id);
        $options     = json_decode($LandingList->option, true);

        $viewData = [
            'boxTitle'         => 'Edit Landing List Creator Kost',
            'sortingOptions'   => $sorting,
            'filterOptions'    => $filterList,
            'filterCities'     => $this->filterCityJob,
            'landingList'      => $LandingList,
            'options'          => $options
        ];

        return view ("admin.contents.landing-list-creator.edit-kost", $viewData);
    }

    public function updateKost(Request $request, $id)
    {
        $LandingList = LandingList::find($id);

        if(!$LandingList) {
            return redirect()->back()->with('error_message', 'Data tidak ditemukan');
        }

        $validator = Validator::make($request->all(), [
            'title'           => 'required',
            'slug'            => 'required',
            'sorting'         => 'required',
            'latitude_1'      => 'required_with:longitude_1,latitude_2,longitude_2',
            'longitude_1'     => 'required_with:latitude_1,latitude_2,longitude_2',
            'latitude_2'      => 'required_with:latitude_1, longitude_1,longitude_2',
            'longitude_2'     => 'required_with:latitude_1,longitude_1,latitude_2',
        ]);

        $slug = str_replace(' ', '', strtolower($request->slug));

        $validator->after(function($validator) use ($request, $slug, $LandingList) {
            if (!is_null($request->input('slug')) && $request->input('slug') != '') {
                $slugExist = LandingList::where('slug', $slug)
                                ->where('type', 'kost')
                                ->first();

                if ($slugExist && $slugExist->id != $LandingList->id) {
                    $validator->errors()->add('slug', 'Slug sudah digunakan sebelumnya');
                }
            }
        });

        if($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        $active_filter = $request->active_filter;
        if (is_null($active_filter)) {
            $active_filter = [];
        }

        $location = [];

        if ($request->latitude_1 != '' && $request->longitude_1 != '' 
            && $request->latitude_2 != '' && $request->longitude_2 != '') {
            $location = [
                [$request->longitude_1, $request->latitude_1],
                [$request->longitude_2, $request->latitude_2]
            ];
        }

        $LandingList->slug          = $slug;
        $LandingList->keyword       = $request->title;    
        $LandingList->description   = $request->description;    
        $LandingList->option        =json_encode([
            'sorting'        => $request->sorting,
            'active_filter'  => $active_filter,
            'city'           => $request->city,
            'location'       => $location
        ]);

        $LandingList-> save();

        ActivityLog::LogUpdate(Action::LANDING_LIST_CREATOR, $id);

        return redirect()->route('admin.list-creator.index')->with('message', 'Sukses mengupdate landing list creator');
    }


    public function createApartment(Request $request)
    {
        $sorting      = $this->sortingKost;
        $filterList   = $this->filterListKost;

        $viewData = [
            'boxTitle'              => 'Create Landing List Creator Apartment',
            'sortingOptions'        => $sorting,
            'filterOptions'         => $filterList,
            'filterCities'          => $this->filterCityJob,
            'filterFurnished'       => $this->filterFurnished
        ];

        return view ("admin.contents.landing-list-creator.create-apartment", $viewData);
    }

    public function storeApartment(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title'           => 'required',
            'slug'            => 'required',
            'sorting'         => 'required',
            'latitude_1'      => 'required_with:longitude_1,latitude_2,longitude_2',
            'longitude_1'     => 'required_with:latitude_1,latitude_2,longitude_2',
            'latitude_2'      => 'required_with:latitude_1, longitude_1,longitude_2',
            'longitude_2'     => 'required_with:latitude_1,longitude_1,latitude_2',
        ]);

        $slug = str_replace(' ', '', strtolower($request->slug));

        $validator->after(function($validator) use ($request, $slug) {
            if (!is_null($request->input('slug')) && $request->input('slug') != '') {
                $slugExist = LandingList::where('slug', $slug)
                                ->where('type', 'apartment')
                                ->first();

                if ($slugExist) {
                    $validator->errors()->add('slug', 'Slug sudah digunakan sebelumnya');
                }
            }
        });

        if($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        $active_filter = $request->active_filter;
        if (is_null($active_filter)) {
            $active_filter = [];
        }

        $location = [];

        if ($request->latitude_1 != '' && $request->longitude_1 != '' 
            && $request->latitude_2 != '' && $request->longitude_2 != '') {
            $location = [
                [$request->longitude_1, $request->latitude_1],
                [$request->longitude_2, $request->latitude_2]
            ];
        }

        $LandingList                = new LandingList;
        $LandingList->type          = 'apartment';
        $LandingList->slug          = $slug;
        $LandingList->keyword       = $request->title;
        $LandingList->description   = $request->description;
        $LandingList->option        =json_encode([
            'sorting'        => $request->sorting,
            'active_filter'  => $active_filter,
            'city'           => $request->city,
            'location'       => $location,
            'furnished'      => $request->furnished
        ]);

        $LandingList-> save();

        ActivityLog::LogCreate(Action::LANDING_APARTMENT, $LandingList->id);

        return redirect()->route('admin.list-creator.index')->with('message', 'Sukses menambah landing list');
    }

    public function editApartment(Request $request, $id)
    {
        $sorting     = $this->sortingKost;
        $filterList  = $this->filterListKost;
        $LandingList = LandingList::find($id);
        $options     = json_decode($LandingList->options, true);

        $viewData = [
            'boxTitle'         => 'Edit Landing List Creator Apartment',
            'sortingOptions'   => $sorting,
            'filterOptions'    => $filterList,
            'filterCities'     => $this->filterCityJob,
            'landingList'      => $LandingList,
            'options'          => $options,
            'filterFurnished'  => $this->filterFurnished
        ];

        return view ("admin.contents.landing-list-creator.edit-apartment", $viewData);
    }

    public function updateApartment(Request $request, $id)
    {
        $LandingList = LandingList::find($id);

        if(!$LandingList) {
            return redirect()->back()->with('error_message', 'Data tidak ditemukan');
        }

        $validator = Validator::make($request->all(), [
            'title'           => 'required',
            'slug'            => 'required',
            'sorting'         => 'required',
            'latitude_1'      => 'required_with:longitude_1,latitude_2,longitude_2',
            'longitude_1'     => 'required_with:latitude_1,latitude_2,longitude_2',
            'latitude_2'      => 'required_with:latitude_1, longitude_1,longitude_2',
            'longitude_2'     => 'required_with:latitude_1,longitude_1,latitude_2',
        ]);

        $slug = str_replace(' ', '', strtolower($request->slug));

        $validator->after(function($validator) use ($request, $slug, $LandingList) {
            if (!is_null($request->input('slug')) && $request->input('slug') != '') {
                $slugExist = LandingList::where('slug', $slug)
                                ->where('type', 'apartment')
                                ->first();

                if ($slugExist && $slugExist->id != $LandingList->id) {
                    $validator->errors()->add('slug', 'Slug sudah digunakan sebelumnya');
                }
            }
        });

        if($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        $active_filter = $request->active_filter;
        if (is_null($active_filter)) {
            $active_filter = [];
        }

        $location = [];

        if ($request->latitude_1 != '' && $request->longitude_1 != '' 
            && $request->latitude_2 != '' && $request->longitude_2 != '') {
            $location = [
                [$request->longitude_1, $request->latitude_1],
                [$request->longitude_2, $request->latitude_2]
            ];
        }

        $LandingList->slug          = $slug;
        $LandingList->keyword       = $request->title;    
        $LandingList->description   = $request->description;    
        $LandingList->option        =json_encode([
            'sorting'        => $request->sorting,
            'active_filter'  => $active_filter,
            'city'           => $request->city,
            'location'       => $location,
            'furnished'      => $request->furnished
        ]);

        $LandingList-> save();

        ActivityLog::LogUpdate(Action::LANDING_LIST_CREATOR, $id);

        return redirect()->route('admin.list-creator.index')->with('message', 'Sukses mengupdate landing list creator');
    }


    public function createSpecific(Request $request)
    {
        $viewData = [
            'boxTitle'              => 'Create Description for Specific Landing'
        ];

        return view ("admin.contents.landing-list-creator.create-specific", $viewData);
    }

    public function storeSpecific(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title'           => 'required',
            'slug'            => 'required',
            'description'     => 'required'
        ]);

        $slug = str_replace(' ', '', strtolower($request->slug));

        $validator->after(function($validator) use ($request, $slug) {
            if (!is_null($request->input('slug')) && $request->input('slug') != '') {
                $slugExist = LandingList::where('slug', $slug)
                                ->where('type', 'specific')
                                ->first();

                if ($slugExist) {
                    $validator->errors()->add('slug', 'Slug sudah digunakan sebelumnya');
                }
            }
        });

        if($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        $LandingList                = new LandingList;
        $LandingList->type          = 'specific';
        $LandingList->slug          = $slug;
        $LandingList->keyword       = $request->title;
        $LandingList->description   = $request->description;
        $LandingList->option        = '';
        $LandingList-> save();

        ActivityLog::LogCreate(Action::LANDING_LIST_CREATOR, $LandingList->id);

        return redirect()->route('admin.list-creator.index')->with('message', 'Sukses menambah landing list');
    }

    public function editSpecific(Request $request, $id)
    {
        $LandingList = LandingList::find($id);

        $viewData = [
            'boxTitle'         => 'Edit Description for Specific Landing',
            'landingList'      => $LandingList
        ];

        return view ("admin.contents.landing-list-creator.edit-specific", $viewData);
    }

    public function updateSpecific(Request $request, $id)
    {
        $LandingList = LandingList::find($id);

        if(!$LandingList) {
            return redirect()->back()->with('error_message', 'Data tidak ditemukan');
        }

        $validator = Validator::make($request->all(), [
            'title'           => 'required',
            'slug'            => 'required',
            'description'     => 'required'
        ]);

        $slug = str_replace(' ', '', strtolower($request->slug));

        $validator->after(function($validator) use ($request, $slug, $LandingList) {

            if (!is_null($request->input('slug')) && $request->input('slug') != '') {
                $slugExist = LandingList::where('slug', $slug)
                                ->where('type', 'specific')
                                ->first();

                if ($slugExist && $slugExist->id != $LandingList->id) {
                    $validator->errors()->add('slug', 'Slug sudah digunakan sebelumnya');
                }
            }
        });

        if($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        $LandingList->slug          = $slug;
        $LandingList->keyword       = $request->title;    
        $LandingList->description   = $request->description;    
        $LandingList-> save();

        ActivityLog::LogUpdate(Action::LANDING_LIST_CREATOR, $id);

        return redirect()->route('admin.list-creator.index')->with('message', 'Sukses mengupdate landing list creator');
    }

    public function destroy(Request $request, $id)
    {
        $landingList = LandingList::find($id);

        if (!$landingList) {
            return redirect()->back()->with('error_message', 'Landing List tidak ditemukan');
        }

        $landingList->delete();

        ActivityLog::LogDelete(Action::LANDING_LIST_CREATOR, $id);

        return redirect()->back()->with('message', 'Landing List berhasil dihapus');
    }

    public function createCompany()
    {
        $sorting      = $this->sortingKost;
        $filterList   = $this->filterListKost;

        $viewData = [
            'boxTitle'              => 'Create Landing List Creator Company',
            'sortingOptions'        => $sorting,
            'filterOptions'         => $filterList,
            'filterCities'          => $this->filterCityJob,
            'filterFurnished'       => $this->filterFurnished
        ];

        return view ("admin.contents.landing-list-creator.create-company", $viewData);
    }

    public function storeCompany(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title'           => 'required',
            'slug'            => 'required',
            'sorting'         => 'required',
            'latitude_1'      => 'required_with:longitude_1,latitude_2,longitude_2',
            'longitude_1'     => 'required_with:latitude_1,latitude_2,longitude_2',
            'latitude_2'      => 'required_with:latitude_1, longitude_1,longitude_2',
            'longitude_2'     => 'required_with:latitude_1,longitude_1,latitude_2',
        ]);

        $slug = str_replace(' ', '', strtolower($request->slug));

        $validator->after(function($validator) use ($request, $slug) {
            if (!is_null($request->input('slug')) && $request->input('slug') != '') {
                $slugExist = LandingList::where('slug', $slug)
                    ->where('type', 'company')
                    ->first();

                if ($slugExist) {
                    $validator->errors()->add('slug', 'Slug sudah digunakan sebelumnya');
                }
            }
        });

        if($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        $active_filter = $request->active_filter;
        if (is_null($active_filter)) {
            $active_filter = [];
        }

        $location = [];

        if ($request->latitude_1 != '' && $request->longitude_1 != ''
            && $request->latitude_2 != '' && $request->longitude_2 != '') {
            $location = [
                [$request->longitude_1, $request->latitude_1],
                [$request->longitude_2, $request->latitude_2]
            ];
        }

        $LandingList                = new LandingList;
        $LandingList->type          = 'company';
        $LandingList->slug          = $slug;
        $LandingList->keyword       = $request->title;
        $LandingList->description   = $request->description;
        $LandingList->option        =json_encode([
            'sorting'        => $request->sorting,
            'active_filter'  => $active_filter,
            'city'           => $request->city,
            'location'       => $location,
        ]);

        $LandingList-> save();

        return redirect()->route('admin.list-creator.index')->with('message', 'Sukses menambah landing list');

    }

    public function editCompany(Request $request, $id)
    {
        $sorting     = $this->sortingKost;
        $filterList  = $this->filterListKost;
        $LandingList = LandingList::find($id);
        $options     = json_decode($LandingList->options, true);

        $viewData = [
            'boxTitle'         => 'Edit Landing List Creator Company',
            'sortingOptions'   => $sorting,
            'filterOptions'    => $filterList,
            'filterCities'     => $this->filterCityJob,
            'landingList'      => $LandingList,
            'options'          => $options,
            'filterFurnished'  => $this->filterFurnished
        ];

        return view ("admin.contents.landing-list-creator.edit-company", $viewData);
    }

    public function updateCompany(Request $request, $id)
    {
        $LandingList = LandingList::find($id);

        if(!$LandingList) {
            return redirect()->back()->with('error_message', 'Data tidak ditemukan');
        }

        $validator = Validator::make($request->all(), [
            'title'           => 'required',
            'slug'            => 'required',
            'sorting'         => 'required',
            'latitude_1'      => 'required_with:longitude_1,latitude_2,longitude_2',
            'longitude_1'     => 'required_with:latitude_1,latitude_2,longitude_2',
            'latitude_2'      => 'required_with:latitude_1, longitude_1,longitude_2',
            'longitude_2'     => 'required_with:latitude_1,longitude_1,latitude_2',
        ]);

        $slug = str_replace(' ', '', strtolower($request->slug));

        $validator->after(function($validator) use ($request, $slug, $LandingList) {
            if (!is_null($request->input('slug')) && $request->input('slug') != '') {
                $slugExist = LandingList::where('slug', $slug)
                    ->where('type', 'apartment')
                    ->first();

                if ($slugExist && $slugExist->id != $LandingList->id) {
                    $validator->errors()->add('slug', 'Slug sudah digunakan sebelumnya');
                }
            }
        });

        if($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        $active_filter = $request->active_filter;
        if (is_null($active_filter)) {
            $active_filter = [];
        }

        $location = [];

        if ($request->latitude_1 != '' && $request->longitude_1 != ''
            && $request->latitude_2 != '' && $request->longitude_2 != '') {
            $location = [
                [$request->longitude_1, $request->latitude_1],
                [$request->longitude_2, $request->latitude_2]
            ];
        }

        $LandingList->slug          = $slug;
        $LandingList->keyword       = $request->title;
        $LandingList->description   = $request->description;
        $LandingList->option        =json_encode([
            'sorting'        => $request->sorting,
            'active_filter'  => $active_filter,
            'city'           => $request->city,
            'location'       => $location,
        ]);

        $LandingList-> save();

        ActivityLog::LogUpdate(Action::LANDING_LIST_CREATOR, $id);

        return redirect()->route('admin.list-creator.index')->with('message', 'Sukses mengupdate landing list creator');
    }

    public function redirectPage(Request $request, $landingCreatorId)
    {
        $landing = LandingList::where('id', $landingCreatorId)->first();
        if (is_null($landing)) {
            return redirect()->back()->with('errors', 'Landing not found');
        }

        $viewData    = array(
            'boxTitle' => 'Landing List Creator',
            'landing' => $landing
        ); 

        return view ("admin.contents.landing-list-creator.redirect", $viewData);
    }

    public function storeRedirectData(Request $request, $landingCreatorId)
    {
        $landing = LandingList::where('id', $landingCreatorId)->first();
        $landingDestination = LandingList::where('id', $request->input('landing_destination'))->first();

        if (empty($request->input('landing_destination'))) {
            $landing->redirect_id = null;
            $landing->save();
            return redirect('admin/list-creator')->with('message', 'Berhasil update');
        }

        if (is_null($landing) or is_null($landingDestination)) {
            return back()->with('error_message', 'Landing not found');
        }

        if ($landing->type != $landingDestination->type) {
            return back()->with('error_message', 'Tipe landing tidak sama');
        }
        $landing->redirect_id = $landingDestination->id;
        $landing->save();
        return redirect('admin/list-creator')->with('message', 'Berhasil update');
    }
}
