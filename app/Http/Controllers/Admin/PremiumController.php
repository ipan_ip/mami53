<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Http\Helpers\ApiHelper;
use App\Libraries\SMSLibrary;
use App\Entities\Premium\AccountConfirmation;
use App\Entities\Premium\PremiumPackage;
use App\Entities\Premium\PremiumRequest;
use App\Entities\Promoted\ViewPromote;
use App\User;
use App\Entities\Premium\BalanceRequest;
use App\Entities\Notif\NotifData;
use App\Entities\Notif\SendNotif;
use Notification;
use App\Entities\Room\RoomOwner;
use App\Entities\Room\Room;
use Cache;
use App\Presenters\PremiumRequestPresenter;
use App\Entities\User\Notification AS NotifOwner;
use App\Entities\Premium\Payment;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;
use Carbon\Carbon;
use App\Entities\Activity\AppSchemes;
use App\Repositories\Premium\AdminConfirmationRepository;

class PremiumController extends Controller
{

    protected $package_string = array("all package", "trial", "package", "balance");
    protected $verified = array("all status", "verified", "waiting");
    protected $repository;

    public function __construct(AdminConfirmationRepository $repository)
    {
        $this->repository = $repository;
        $this->middleware(function ($request, $next) {
            if(!Auth::user()->can('access-pay-confirmation')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        View::share('contentHeader', 'Stories Management');
        View::share('user', Auth::user());
    }

   
    public function index(Request $request)
    {

      //dd($request->all());
       $query = AccountConfirmation::with('user', 'user.owners', 'bank_account', 'premium_balance', 'premium_balance.premium_package', 'premium_request.premium_package');
       $query = $this->filterIndex($request, $query);

       if ($request->filled('package')) {
         if ($request->input('package') == "3") $query = $query->where('view_balance_request_id', '>', 0); 
         //$query = $query;
       }

       if ($request->filled('total')) $query = $query->where('total', $request->input('total'));
       if ($request->filled('name')) $query = $query->where('name', 'LIKE', '%'.$request->input('name').'%');

       $query = $query->orderBy('id', 'desc')->paginate(20);

       $data = (new PremiumRequestPresenter('confirmation'))->present($query);
       //dd($data['data']);
        $viewData = array(
            'boxTitle'     => 'Request Premium Account List',
            'deleteAction' => 'admin.tag.destroy',
            'rowsAccount'      => $data['data'],
            'rowPaginate'  => $query,
            'package' => $this->package_string,
            'verified' => $this->verified,
        );

        ActivityLog::LogIndex(Action::PAY_CONFIRMATION);

        return View::make('admin.contents.premium.index', $viewData);
    }

    private function filterIndex(Request $request, $query)
    {
        if ($request->filled('q')) {
            $query = $this->searchPhoneOwner($request->input('q'), $query);
        }

        if ($request->filled('package')) {
            if ($request->input('package') == '1') $query = $this->getTrialUser($request->input('package'), $query);
            if ($request->input('package') == "2") $query = $this->getTrialUser($request->input('package'), $query);
        }
        if ($request->filled('statuso')) {
            if ($request->input('statuso') == '1') $query->where('is_confirm', '1');
            else if ($request->input('statuso') == '2') $query->where('is_confirm', '0');
        }

        return $query;
    }

    private function getTrialUser($keyword, $query)
    {
        if ($keyword == "1") $premium_id = PremiumRequest::where('premium_package_id', 19)->get()->pluck('id')->toArray();
        if ($keyword == "2") $premium_id = PremiumRequest::where('premium_package_id', '<>', 19)->get()->pluck('id')->toArray();
        return $query->whereIn('premium_request_id', $premium_id);
    }

    private function searchPhoneOwner($keyword, $query)
    {
        if (strlen(trim($keyword)) > 0) {
           $userId = User::where('phone_number', 'LIKE', '%'.$keyword.'%')->get()->pluck('id')->toArray();
           /*$query = $query->user()->where('phone_number', 'LIKE', '%'.$keyword.'%');*/
           $query->whereIn('user_id', $userId);
        }
        return $query;
    }

    public function create()
    {
        return back()->withInput();
    }  

    public function show($id)
    {
        return back()->withInput();
    }

    public function edit($id)
    {
        if (! empty($id)) {
            $premium = AccountConfirmation::with('user', 'premium_request')->find($id);
            if (is_null($premium) === false && $premium instanceof AccountConfirmation) {
                $viewData = [
                    'boxTitle'    => 'View or Edit Account Confirmation',
                    'premium'        => $premium,
                    'rowsActive'  => ['1', '0'],
                    'formAction'  => [
                        'admin.premium.update', 
                        $id
                    ],
                    'formMethod'  => 'PUT',
                ];
                return View::make('admin.contents.premium.form', $viewData);
            }
        }

        return redirect()
            ->intended(route('admin.premium.index'))
            ->withErrors([
                'Error', 
                "Request Premium Account id: $id tidak ditemukan"
            ]);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, array(
            'is_confirm'  => 'required',
            'user_id'     => 'required'
        ));

        $premiumConfirmation = AccountConfirmation::with(['premium_request' => function($query) {
                                $query->where('expired_status', null);
                                },
                            'user',
                            'user.owners',
                            'premium_request.premium_package',
                            'premium_request.room',
                            'premium_balance' => function($query) {
                                    $query->whereNull('expired_status');
                                    $query->orWhere('expired_status', 'false');    
                                }
                            ]
                        )
                        ->where('id', $id)
                        //->where('is_confirm', AccountConfirmation::CONFIRMATION_WAITING)
                        ->first();
        
        if (is_null($premiumConfirmation)) {
            return redirect()->back()->with('error_message', 'Gagal konfirmasi pembayaran');
        }

        if ($premiumConfirmation->is_confirm === AccountConfirmation::CONFIRMATION_SUCCESS) {
            $this->repository->setConfirmation($premiumConfirmation, $request->all());
            return Redirect::route('admin.premium.index')->with('message', 'Sukses konfirmasi pembayaran');
        }

        $roomOwners = $premiumConfirmation->user->owners;
        if (count($roomOwners) == 0) {
            return  redirect()->back()->with('error_message', 'Pemilik belum punya kos');
        } else if (count($roomOwners) > 0) {
            $roomStatus = array_unique($roomOwners->pluck('status')->toArray());
            if (
                !in_array(RoomOwner::ROOM_VERIFY_STATUS, $roomStatus)
                ) {
                return redirect()->back()->with('error_message', 'Kos pemilik belum aktif');
            }
        }
        
        $this->repository->premiumConfirmation($premiumConfirmation, $request->all());

        return Redirect::route('admin.premium.index')->with('message', 'Sukses konfirmasi pembayaran');
    }

    public function destroy(Request $request, $id) 
    {
        $confirmation = AccountConfirmation::with('premium_request')->find($id);
        
        if (is_null($confirmation)) {
            return redirect()->back()->with('error_message', "Data konfirmasi tidak ditemukan");
        }

        $requestId = (int) $confirmation->premium_request_id;

        if ($requestId > 0) {
            $message = 'Sukses hapus konfirmasi';
            $premiumRequest = $confirmation->premium_request;

            if ($premiumRequest->status == "0") {
                $confirmationTotal = AccountConfirmation::requestConfirmation($requestId);
                if (count($confirmationTotal) == 1) {
                    $expiredDate = Carbon::today()->addDay(2)->format('Y-m-d');
                    $premiumRequest->expired_status = 'false';
                    $premiumRequest->expired_date = $expiredDate;
                    $premiumRequest->save();
                }
                $confirmation->delete();
                ActivityLog::LogDelete(Action::PAY_CONFIRMATION, $id);
            } else if (
                $premiumRequest->status == PremiumRequest::PREMIUM_REQUEST_SUCCESS
                && $confirmation->is_confirm == AccountConfirmation::CONFIRMATION_SUCCESS
                && $confirmation->premium_request_id > 0
            ) {
                $premiumConfirmation = AccountConfirmation::where('premium_request_id', $premiumRequest->id)->count();
                if ($premiumConfirmation > 1) {
                    $confirmation->delete();
                }
            } else if (
                $premiumRequest->status == PremiumRequest::PREMIUM_REQUEST_SUCCESS
                && $confirmation->is_confirm == AccountConfirmation::CONFIRMATION_WAITING
            ) {
                $confirmation->delete();
            } else {
                $message = 'Tidak dapat menghapus paket yang sudah terkonfirmasi';
            }
        } else {
            $message = 'Hapus hanya untuk pembelian paket';
        }

        return redirect()->back()->with('message', $message);
    }

}
