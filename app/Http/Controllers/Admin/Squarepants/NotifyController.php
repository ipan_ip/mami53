<?php

namespace App\Http\Controllers\Admin\Squarepants;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\Squarepants\Agents;
use App\Entities\Squarepants\NotifToApp;
use App\Http\Helpers\ApiResponse as Api;
use App\Entities\Squarepants\AgentData;
use App\Libraries\SMSLibrary;

class NotifyController extends Controller
{
    public function notifNow(Request $request) 
    {
    	$agent = Agents::where('email', $request->input('email'))->first();
    	if (is_null($agent)) {
    		return ["status" => false];
    	}

        if (strlen($request->input('phone')) < 8) {
            return ["status" => false];
        }

        $title = "No hapenya ini ";

        if (!$request->filled('search')) {
        	// store to DB
        	$data = AgentData::where('agent_call_app_id', $agent->id)->where('phone', $request->input('phone'))->first();
        	
        	if (is_null($data)) {
        		$data = new AgentData();
        		$data->agent_call_app_id = $agent->id;
        		$data->phone = $request->input('phone');
        		$data->identifier = $request->input('id');
        		$data->save();
        	} else {
        		$data->phone = $request->input('phone');
        		$data->identifier = $request->input('id');
                $data->updated_at = date("Y-m-d H:i:s");
        		$data->save();
        	}
            $title = $request->input('kos_name');
        }

    	$message = [
                        "title" => $title,
    				    "body" => $request->input('phone'),
    			    ];
    	$fcm_token = $agent->token;

    	(new NotifToApp)->send($fcm_token, $message);

    	return ['status' => true];
    }

    public function getListData(Request $request)
    {
        if (!$request->filled('email')) return Api::responseData(['status' => false, "data" => null]);
        $agent = Agents::where('email', $request->input('email'))->first();

        $data = AgentData::with('room')->where('agent_call_app_id', $agent->id)->whereDate('updated_at', date('Y-m-d'))->orderBy('id', 'desc')->get();
        $results = [];
        foreach ($data AS $key => $value) {
            $results[] = array(
                "name" => $value->room->name,
                'phone' => $value->phone
            );
        }

        return Api::responseData(['status' => true, "data" => $results]);
    }
}
