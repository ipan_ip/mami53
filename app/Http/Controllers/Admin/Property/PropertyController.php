<?php

namespace App\Http\Controllers\Admin\Property;

use App\Entities\Property\Property;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Property\PropertyIndexRequest;
use App\Http\Requests\Admin\Property\PropertyStoreRequest;
use App\Services\Level\PropertyLevelService;
use Auth;
use Illuminate\Support\Facades\View;
use App\Services\Property\PropertyService;

class PropertyController extends Controller
{
    private $service;
    private $pageTitle;

    public function __construct(PropertyService $propertyService)
    {
        $this->middleware(function ($request, $next) {
            if (!Auth::user()->can('access-kost-property')) {
                return redirect('admin');
            }

            return $next($request);
        });
        $this->service = $propertyService;
        $this->pageTitle = 'Property Management';
        View::share('contentHeader', 'Property Management');
        View::share('user', Auth::user());
    }

    public function index(
        PropertyIndexRequest $request,
        PropertyLevelService $propertyLevelService
    ) {
        $params = $request->validated();
        $properties = $this->service->getListPaginate($params);
        $viewData = [
            'boxTitle' => $this->pageTitle,
            'pageTitle' => $this->pageTitle,
            'properties' => $properties,
            'propertyLevels' => $propertyLevelService->getLevelDropdown()
        ];
        return view('admin.contents.property.index', $viewData);
    }

    public function show(Property $property)
    {
        $viewData = [
            'property' => $property,
            'boxTitle' => $this->pageTitle,
            'pageTitle' => $this->pageTitle,
        ];
        return view('admin.contents.property.show', $viewData);
    }

    public function create()
    {
        $viewData = [
            'boxTitle' => $this->pageTitle,
            'pageTitle' => $this->pageTitle,
        ];
        return view('admin.contents.property.create', $viewData);
    }

    public function store(PropertyStoreRequest $request)
    {
        $validator = $request->validator;
        $owner = $request->getOwner();

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        if ($this->service->isPropertyNameExist($request->property_name, $owner)) {
            return redirect()
                ->back()
                ->with('error_message', 'Anda sudah memiliki nama properti yang sama')
                ->withInput();
        }

        $property = $this->service->createProperty(
            $request->property_name,
            $owner
        );

        if ($property === null) {
            return redirect()->back()->with('error_message', 'Property gagal ditambahkan!');
        }

        return redirect()->route('admin.property.index')
            ->with(
                'message',
                sprintf(
                    'Property %s berhasil ditambahkan!',
                    $property->name
                )
            );
    }

    public function edit(Property $property)
    {
        $viewData = [
            'property' => $property,
            'ownerPhone' => $property->owner_user->phone_number ?? null,
            'boxTitle' => $this->pageTitle,
            'pageTitle' => $this->pageTitle,
        ];

        return view('admin.contents.property.edit', $viewData);
    }

    public function update(PropertyStoreRequest $request, Property $property)
    {
        $validator = $request->validator;
        $owner = $request->getOwner();

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        if ($this->service->isPropertyNameExist($request->property_name, $owner, $property->id)) {
            return redirect()
                ->back()
                ->with('error_message', 'Anda sudah memiliki nama properti yang sama')
                ->withInput();
        }

        $result = $this->service->updateProperty(
            $property,
            $request->property_name,
            $owner
        );

        if (!$result) {
            return redirect()->back()->with('error_message', 'Property gagal diperbarui!');
        }
        
        return redirect()->route('admin.property.index')
            ->with(
                'message',
                sprintf(
                    'Property %s berhasil diperbarui!',
                    $property->name
                )
            );
    }

    public function destroy(Property $property)
    {
        $propertyName = $property->name;

        $hasKost = $property->rooms()->count() > 0;
        $hasContract = $property->property_contracts()->count() > 0;

        if ($hasKost || $hasContract) {
            return redirect()->back()->with('error_message', 'Property gagal dihapus!');
        }

        $property->delete();

        return redirect()->route('admin.property.index')
            ->with(
                'message',
                sprintf(
                    'Property %s berhasil dihapus!',
                    $propertyName
                )
            );
    }
}
