<?php

namespace App\Http\Controllers\Admin\Property;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\Landing\LandingHouseProperty;
use Auth;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use App\Entities\Room\Element\Tag;
use App\Entities\Room\Element\Tagging;
use App\Entities\Landing\LandingHousePropertyRelation;
use App\Entities\Landing\LandingHousePropertyTag;
use Config;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;

class LandingHousePropertyController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if(!Auth::user()->can('access-landing-page')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        View::share('contentHeader', 'Stories Management');
        View::share('user', Auth::user());
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $landing = LandingHouseProperty::query();

        if ($request->filled('name')) {
            $landing->where(function($p) use($request) {
                $p->where('slug', $request->input('name'))
                    ->orWhere('heading_1', 'like', '%'.$request->input('name').'%');
            });
        }

        $landing = $landing->orderBy('id', 'desc')->paginate(20);
        
        $shareConfig = Config::get('services.share');
        $base = $shareConfig['base_url'];

        $viewData = array(
            'boxTitle' => 'List Landing',
            'landing' => $landing,
            'base_url' => $base
        );

        ActivityLog::LogIndex(Action::LANDING_HOUSE_PROPERTY);
        
        return View::make('admin.contents.landing-house-property.index', $viewData);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $rowLanding = new LandingHouseProperty;
        $rowLanding->slug           = Input::old('slug');
        $rowLanding->type           = Input::old('type','area');
        $rowLanding->parent_id      = Input::old('parent_id');
        $rowLanding->heading_1      = Input::old('heading_1');
        $rowLanding->heading_2      = Input::old('heading_2');
        $rowLanding->latitude       = Input::old('latitude');
        $rowLanding->longitude      = Input::old('longitude');
        $rowLanding->price_min      = Input::old('price_min');
        $rowLanding->price_max      = Input::old('price_max');
        $rowLanding->keyword        = Input::old('keyword');
        $rowLanding->rent_type      = Input::old('rent_type');
        $rowLanding->area_city      = Input::old('area_city');
        $rowLanding->area_subdistrict   = Input::old('area_subdistrict');
        $rowLanding->property_type = Input::old('property_type');

        $rowLanding->description  = Input::old('description');
        $rowLanding->coordinate_1   = Input::old('coordinate_1');
        $rowLanding->coordinate_2   = Input::old('coordinate_2');

        if (!is_null($rowLanding->coordinate_1)) {
            $coordinate1Splited = explode(',', $rowLanding->coordinate_1);

            $rowLanding->latitude_1  = $coordinate1Splited[0];
            $rowLanding->longitude_1 = $coordinate1Splited[1];
        }


        if (!is_null($rowLanding->coordinate_1)) {
            $coordinate2Splited = explode(',', $rowLanding->coordinate_2);

            $rowLanding->latitude_2  = $coordinate2Splited[0];
            $rowLanding->longitude_2 = $coordinate2Splited[1];
        }

        $rowsTagging   = Tagging::all();

        $viewData   = [
            'boxTitle' => 'New Landing Page',
            'rowLanding' => $rowLanding,
            'rowsTag' => Tag::get(),
            'rowsTagging' => $rowsTagging,
            'rowsOldTagging' => [],
            'formMethod' => 'POST',
            'action' => '/admin/house_property/landing',
        ];

        return View::make('admin.contents.landing-house-property.form', $viewData);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'slug'=>[
                Rule::unique('landing_house_property')->whereNull('deleted_at')
            ],
            'heading_1'=>[
                Rule::unique('landing_house_property')->whereNull('deleted_at')
            ]
        ]);

        $rowLanding                 = new LandingHouseProperty;

        $rowLanding->slug           = $request->get('slug');
        $rowLanding->type           = $request->get('type');
        $rowLanding->heading_1      = $request->get('heading_1');
        $rowLanding->heading_2      = $request->get('heading_2');
        
        if ($request->filled('coordinate_1') AND $request->filled('coordinate_2')) {
            
            $latLong1                   = explode(',', $request->get('coordinate_1'));
            $latLong2                   = explode(',', $request->get('coordinate_2'));

            $rowLanding->latitude_1     = min($latLong1[0],$latLong2[0]);
            $rowLanding->longitude_1    = min($latLong1[1],$latLong2[1]);

            $rowLanding->latitude_2     = max($latLong1[0],$latLong2[0]);
            $rowLanding->longitude_2    = max($latLong1[1],$latLong2[1]);

        }

        $rowLanding->price_min = $request->get('price_min');
        $rowLanding->price_max = $request->get('price_max');
        $rowLanding->rent_type = $request->get('rent_type');
        $rowLanding->keyword        = $request->get('keyword');
        $rowLanding->description  = $request->get('description');
        $rowLanding->dummy_counter  = $request->get('dummy_counter') != '' ? $request->get('dummy_counter') : null;
        $rowLanding->is_active = 1;
        $rowLanding->property_type = $request->get('property_type');
        $rowLanding->save();

        ActivityLog::LogCreate(Action::LANDING_HOUSE_PROPERTY, $rowLanding->id);

        $taggingIds = $request->input('tagging_ids');
        if ($taggingIds != null) {
            // process check tagging
            $temp   = [];
            foreach ($taggingIds as $tagging) {
                $checkTagging      = Tagging::where('name', $tagging)->first();
                $newLandingTagging = new LandingHousePropertyRelation();
                $newLandingTagging->landing_house_id = $rowLanding->id;

                // create new tagging
                if ($checkTagging == null) {
                    $newTagging = new Tagging();
                    $newTagging->name = $tagging;
                    $newTagging->save();

                    $newLandingTagging->landing_category = $newTagging->id;
                    $newLandingTagging->save();
                } else {
                    $checkLandingTagging    = LandingHousePropertyRelation::where('landing_house_id', $rowLanding->id)->where('landing_category', $checkTagging->id)->first();

                    if ($checkLandingTagging == null) {
                        $newLandingTagging->landing_category = $checkTagging->id;
                        $newLandingTagging->save();
                    }
                }


                $temp[] = $newLandingTagging;
            }

        }

        $tagIds  = $request->get('tag_ids');
        $tagIds  = (array) $tagIds;

        $rowLanding->tags()->sync($tagIds);

        return  redirect('/admin/house_property/landing')->with('message', 'Landing Page successfully created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect()->back()->with(['message', 'Try again']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!Auth::user()->can('access-landing-page-edit')) return redirect ('/admin');

        $rowLanding = LandingHouseProperty::find($id);

        $rowLanding->coordinate_1   = $rowLanding->latitude_1 . ',' . $rowLanding->longitude_1;
        $rowLanding->coordinate_2   = $rowLanding->latitude_2 . ',' . $rowLanding->longitude_2;
        $rowLanding->tag_ids        = array_pluck($rowLanding->tags()->get(['tag.id'])->toArray(),'id');

        $rowsTag = Tag::get();
        $rowsTag = mySelectPreprocessing(
            'multi',
            'selected',
            'key-value',
            $rowsTag,
            $rowLanding->tag_ids
        );

        if ($rowLanding->rent_type == null) {
            $rowLanding->rent_type = 2;
        }

        // Check Tagging
        $rowsTagging   = Tagging::all();
        $oldTagging    = LandingHousePropertyRelation::where('landing_house_id', $id)->get();
        $oldTaggingId  = [];
        foreach ($oldTagging as $tagging) {
            $oldTaggingId[] = $tagging->landing_category;
        }

        $viewData   = [
            'boxTitle'          => 'Edit Landing Page',
            'rowLanding'        => $rowLanding,
            'rowsTag'           => $rowsTag,
            'rowsTagging'       => $rowsTagging,
            'rowsOldTagging'    => $oldTaggingId,
            'method' => 'PUT',
            'action' => "/admin/house_property/landing/".$rowLanding->id,
        ];
        return View::make('admin.contents.landing-house-property.form', $viewData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'slug'=>[
                Rule::unique('landing_house_property')->ignore($id, 'id')->whereNull('deleted_at')
            ],
            'heading_1'=>[
                Rule::unique('landing_house_property')->ignore($id, 'id')->whereNull('deleted_at')
            ]
        ]);

        $rowLanding                 = LandingHouseProperty::findOrFail($id);

        $rowLanding->slug           = $request->get('slug');
        $rowLanding->type           = $request->get('type');
        $rowLanding->heading_1      = $request->get('heading_1');
        $rowLanding->heading_2      = $request->get('heading_2');
        $latLong1                   = explode(',', $request->get('coordinate_1'));
        $latLong2                   = explode(',', $request->get('coordinate_2'));
        $rowLanding->latitude_1     = min($latLong1[0],$latLong2[0]);
        $rowLanding->longitude_1    = min($latLong1[1],$latLong2[1]);
        $rowLanding->latitude_2     = max($latLong1[0],$latLong2[0]);
        $rowLanding->longitude_2    = max($latLong1[1],$latLong2[1]);
        $rowLanding->price_min      = $request->get('price_min');
        $rowLanding->price_max      = $request->get('price_max');
        $rowLanding->rent_type      = $request->get('rent_type');
        $rowLanding->keyword        = $request->get('keyword');
        $rowLanding->property_type = $request->get('property_type');
        $rowLanding->description  = $request->get('description');

        $taggingIds = $request->input('tagging_ids');

        if ($taggingIds != null) {
            LandingHousePropertyRelation::removeLandingPropertyRelation($id, $taggingIds);

            // process check tagging
            $temp   = [];
            foreach ($taggingIds as $tagging) {
                $checkTagging      = Tagging::where('name', $tagging)->first();
                $newLandingTagging = new LandingHousePropertyRelation();
                $newLandingTagging->landing_house_id = $id;

                // create new tagging
                if ($checkTagging == null) {
                    $newTagging = new Tagging();
                    $newTagging->name = $tagging;
                    $newTagging->save();

                    $newLandingTagging->landing_category = $newTagging->id;
                    $newLandingTagging->save();
                } else {
                    $checkLandingTagging    = LandingHousePropertyRelation::where('landing_house_id', $id)->where('landing_category', $checkTagging->id)->first();

                    if ($checkLandingTagging == null) {
                        $newLandingTagging->landing_category = $checkTagging->id;
                        $newLandingTagging->save();
                    }
                }


                $temp[] = $newLandingTagging;
            }

        } else {
            LandingHousePropertyRelation::removeAllLandingTagging($id);
        }

        $rowLanding->dummy_counter  = $request->get('dummy_counter') != '' ? $request->get('dummy_counter') : null;

        $rowLanding->save();

        $tagIds  = $request->get('tag_ids');
        $tagIds  = (array) $tagIds;
        $rowLanding->tags()->sync($tagIds);
        ActivityLog::LogUpdate(Action::LANDING_HOUSE_PROPERTY, $id);
        return  redirect('admin/house_property/landing')->with('message', 'Landing Page successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $landing = LandingHouseProperty::findOrFail($id);
        $landingRedirect = LandingHouseProperty::where('redirect_id', $id)->update(['redirect_id' => NULL]);
        $landing->delete();

        ActivityLog::LogDelete(Action::LANDING_HOUSE_PROPERTY, $id);

        return redirect()->back()->with(['message', 'Success delete']);
    }

    public function  redirectPage(Request $request, $id)
    {
        $landing = LandingHouseProperty::findOrFail($id);
        $viewData = array(
            'boxTitle' => 'List Landing',
            'landing' => $landing
        );
        
        return View::make('admin.contents.landing-house-property.redirect', $viewData);
    }

    public function storeLandingHousePropertyRedirection(Request $request, $id)
    {
        $landing = LandingHouseProperty::findOrFail($id);
        $landingDestinationId = (int) $request->input('landing_destination');

        if ($landingDestinationId > 0) {
            $landingDestination = LandingHouseProperty::findOrFail($landingDestinationId);

            if (($landing->id == $landingDestination->id) or ($landing->property_type != $landingDestination->property_type)) {
                return redirect()->back()->with(['message', 'Gagal mengganti tujuan landing baru']);
            }
        }

        $landing->redirect_id = $landingDestinationId > 0 ? $landingDestinationId : NULL;
        $landing->save();
        return Redirect('admin/house_property/landing')->with(['message', 'Berhasil mengganti tujuan landing']);
    }
}
