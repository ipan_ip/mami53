<?php

namespace App\Http\Controllers\Admin\Property;

use App\Entities\Property\Property;
use App\Entities\Property\PropertyContract;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\PropertyContract\PropertyContractStoreRequest;
use App\Services\Level\PropertyLevelService;
use App\Services\Property\PropertyContractService;
use Auth;
use Illuminate\Support\Facades\View;

class PropertyContractController extends Controller
{
    private $contractService;
    private $levelService;
    private $pageTitle;

    public function __construct(
        PropertyContractService $propertyContractService,
        PropertyLevelService $propertyLevelService
    ) {
        $this->middleware(function ($request, $next) {
            if (!Auth::user()->can('access-kost-property')) {
                return redirect('admin');
            }

            return $next($request);
        });
        $this->contractService = $propertyContractService;
        $this->levelService = $propertyLevelService;
        $this->pageTitle = 'Property Contract Management';
        View::share('contentHeader', 'Property Contract Management');
        View::share('user', Auth::user());
    }

    public function create(Property $property)
    {
        $viewData = [
            'boxTitle' => $this->pageTitle,
            'pageTitle' => $this->pageTitle,
            'property' => $property,
            'levelList' => $this->levelService->getLevelDropdown()
        ];

        return view('admin.contents.property-contract.create', $viewData);
    }

    public function store(PropertyContractStoreRequest $request, Property $property)
    {
        $params = $request->validated();
        $result = $this->contractService->createContract(
            Auth::user(),
            $property,
            $params
        );
        if ($result === null)
            return redirect()->back()->with('error_message', 'Kontrak gagal ditambahkan!');
        return redirect()->route('admin.property.index')
            ->with('message', 'Kontrak berhasil ditambahkan');
    }

    public function renew(Property $property)
    {
        $viewData = [
            'boxTitle' => $this->pageTitle,
            'pageTitle' => $this->pageTitle,
            'property' => $property,
            'propertyContract' => $property->property_active_contract,
            'levelList' => $this->levelService->getLevelDropdown()
        ];

        return view('admin.contents.property-contract.renew', $viewData);
    }

    public function storeRenew(PropertyContractStoreRequest $request, Property $property)
    {
        $params = $request->validated();
        $result = $this->contractService->terminateContract(
            Auth::user(),
            $property
        );
        $result = $this->contractService->createContract(
            Auth::user(),
            $property,
            $params
        );
        if ($result === null)
            return redirect()->back()->with('error_message', 'Kontrak gagal dibuat ulang!');
        return redirect()->route('admin.property.index')
            ->with('message', 'Kontrak berhasil dibuat ulang!');
    }

    public function terminate(Property $property)
    {
        $result = $this->contractService->terminateContract(
            Auth::user(),
            $property
        );
        if ($result->status == PropertyContract::STATUS_INACTIVE) {
            return redirect()->back()->with('message', 'Kontrak berhasil diberhentikan!');
        }
        return redirect()->back()->with('error_message', 'Kontrak gagal diberhentikan!');
    }
}
