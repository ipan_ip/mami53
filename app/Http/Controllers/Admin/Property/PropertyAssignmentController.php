<?php

namespace App\Http\Controllers\Admin\Property;

use App\Entities\Property\Property;
use App\Entities\Room\Room;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\View;
use App\Services\Room\RoomService;

class PropertyAssignmentController extends Controller
{
    private $roomService;
    private $pageTitle;

    public function __construct(RoomService $roomService)
    {
        $this->middleware(function ($request, $next) {
            if (!Auth::user()->can('access-kost-property')) {
                return redirect('admin');
            }

            return $next($request);
        });
        $this->roomService = $roomService;
        $this->pageTitle = 'Assign Property';
        View::share('contentHeader', 'Assign Property');
        View::share('user', Auth::user());
    }

    public function index(Property $property)
    {
        $kosts = $this->roomService->getRoomsByProperty($property);
        $viewData = [
            'boxTitle' => $this->pageTitle,
            'pageTitle' => $this->pageTitle,
            'kosts' => $kosts,
            'property' => $property
        ];
        return view('admin.contents.property-assignment.index', $viewData);
    }

    public function toggle(Property $property, Room $room)
    {
        $result = $this->roomService->togglePropertyAssignment($property, $room);
        if ($result)
            return redirect()->back()->with('message', 'Perubahan berhasil!');
        return redirect()->back()->with('error_message', 'Perubahan gagal!');
    }
}
