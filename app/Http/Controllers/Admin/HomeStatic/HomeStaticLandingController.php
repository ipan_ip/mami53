<?php

namespace App\Http\Controllers\Admin\HomeStatic;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;
use Auth;
use App\Entities\Landing\Landing;
use App\Entities\Landing\HomeStaticLanding;
use Validator;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;

class HomeStaticLandingController extends Controller
{

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!Auth::user()->can('access-home-static')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        View::share('contentHeader', 'Home Static Landing Management');
        View::share('user', Auth::user());
    }

    public function index(Request $request)
    {

        $query = HomeStaticLanding::with('landing_kost', 'parent');

        if ($request->filled('q') && $request->get('q') != '') {
            $query = $query->where('name', 'like', '%' . $request->get('q') . '%');
        }

        $landings = $query->orderBy('created_at', 'desc')->paginate(20);

        $viewData = array(
            'boxTitle'  => 'Home Static Landing',
            'landings'  => $landings,
        );

        ActivityLog::LogIndex(Action::LANDING_HOME_STATIC);

        return view("admin.contents.home-static-landing.index", $viewData);
    }

    public function create(Request $request)
    {
        $kostLandings = Landing::select(\DB::raw('id, concat(keyword," | ",slug) as name'))
            ->whereNull('redirect_id')
            ->get();

        $parentStaticLanding = HomeStaticLanding::whereNull('parent_id')->get();

        $viewData = [
            'boxTitle'              => 'Create Home Static Landing',
            'kostLandings'          => $kostLandings,
            'parentStaticLanding'   => $parentStaticLanding
        ];

        return view('admin.contents.home-static-landing.create', $viewData);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'landing_id' => 'required',
            'name'       => 'required|max:50'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()
                ->with('errors', $validator->errors());
        }

        $homeStaticLanding              = new HomeStaticLanding;
        $homeStaticLanding->type        = 'kost';
        $homeStaticLanding->landing_id  = $request->landing_id;
        $homeStaticLanding->parent_id   = $request->parent_id;
        $homeStaticLanding->name        = $request->name;
        $homeStaticLanding->save();

        ActivityLog::LogCreate(Action::LANDING_HOME_STATIC, $homeStaticLanding->id);

        return redirect()->route('admin.home-static-landing.index')->with('message', 'Data berhasil disimpan');
    }

    public function edit(Request $request, $id)
    {
        $kostLandings = Landing::select(\DB::raw('id, concat(keyword," | ",slug) as name'))
            ->whereNull('redirect_id')
            ->get();

        $parentStaticLanding = HomeStaticLanding::all();

        $homeStaticLanding   = HomeStaticLanding::find($id);

        $viewData = [
            'boxTitle'              => 'Edit Home Static Landing',
            'homeStaticLanding'     => $homeStaticLanding,
            'parentStaticLanding'   => $parentStaticLanding,
            'kostLandings'          => $kostLandings
        ];

        return view('admin.contents.home-static-landing.edit', $viewData);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'landing_id' => 'required',
            'name'       => 'required|max:50'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()
                ->with('errors', $validator->errors());
        }

        $homeStaticLanding = HomeStaticLanding::find($id);

        $homeStaticLanding->type        = 'kost';
        $homeStaticLanding->landing_id  = $request->landing_id;
        $homeStaticLanding->parent_id   = $request->parent_id;
        $homeStaticLanding->name        = $request->name;
        $homeStaticLanding->save();

        ActivityLog::LogUpdate(Action::LANDING_HOME_STATIC, $id);

        return redirect()->route('admin.home-static-landing.index')->with('message', 'Data berhasil dirubah');
    }

    public function delete(Request $request, $id)
    {
        $homeStaticLanding = HomeStaticLanding::find($id);

        HomeStaticLanding::where('parent_id', $id)->delete();

        $homeStaticLanding->delete();

        ActivityLog::LogDelete(Action::LANDING_HOME_STATIC, $id);

        return redirect()->route('admin.home-static-landing.index')->with('message', 'Data berhasil dihapus');
    }
}
