<?php

namespace App\Http\Controllers\admin\Vacancy;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\Entities\Vacancy\Spesialisasi;
use App\Entities\Vacancy\VacancySpesialisasiType;
use Illuminate\Support\Facades\Redirect;

class SpesialisasiController extends Controller
{
    protected $group = [
        'general' => 'General',
        'niche'   => 'Niche'
    ];

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if(!Auth::user()->can('access-jobs')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        View::share('contentHeader', 'Stories Management');
        View::share('user', Auth::user());
    }

    public function index(Request $request)
    {
        $spesialisasi    = Spesialisasi::with('vacancy_spesialisasi_type')
            ->orderBy('id','asc');

        if($request->filled('q'))
        {
            $spesialisasi->where('name','like', '%' . $request->get('q') . '%');
        }

        $spesialisasi    = $spesialisasi->paginate(20);

        $viewData = array(
            'boxTitle'        => 'Spesialisasi',
            'spesialisasi'     => $spesialisasi,
        );

        return View::make('admin.contents.vacancy.spesialisasi.index', $viewData);
    }

    public function create(Request $request)
    {
        $vacancyTypes = VacancySpesialisasiType::orderBy('name', 'asc')->pluck('name', 'id');

        $spesialisasi = new spesialisasi;
        $spesialisasi->name = Input::old('spesialisasi_name');
        $spesialisasi->vacancy_spesialisasi_type_id = Input::old('type');
        $spesialisasi->is_active = Input::old('is_active');
        $group = $this->group;

        $viewData = array(
            'boxTitle'    => 'Insert spesialisasi',
            'data'      => $spesialisasi,
            'rowsActive' => array('1', '0'),
            "type"    => ['general' => Spesialisasi::TYPE, 'niche'=>Spesialisasi::TYPE_NICHE],
            'formMethod'  => 'POST',
            'formAction'  => 'admin.spesialisasi.store',
            'groups' => $group,
            'vacancyTypes' => $vacancyTypes
        );

        return  View::make('admin.contents.vacancy.spesialisasi.form', $viewData);
    }

    public function store(Request $request)
    {
        $this->validate($request, array(
            'spesialisasi_name' => 'required',
            'type'              => 'required',
        ));

        $spesialisasi = new Spesialisasi();
        $spesialisasi->name = $request->input('spesialisasi_name');
        $spesialisasi->vacancy_spesialisasi_type_id = $request->input('type');
        // $spesialisasi->type = $request->input('type');
        $spesialisasi->group = $request->input('group');
        $spesialisasi->is_active = $request->filled('is_active') ? 1 : 0;
        $spesialisasi->save();

        if ($spesialisasi) {
            return Redirect::route('admin.spesialisasi.index')
                ->with('message', 'Created');
      
        } else {
            return Redirect::route('admin.spesialisasi.create')
                ->with('error_message', 'Aborted')
                ->withInput();
        }

    }

    public function edit(Request $request, $id)
    {
        $vacancyTypes = VacancySpesialisasiType::orderBy('name', 'asc')->pluck('name', 'id');

        $spesialisasi = Spesialisasi::where('id', $id)->first();
        $group = $this->group;

        if (is_null($spesialisasi)) {
            return  Redirect::route('admin.jobs.index')
                ->with('message', 'Data tidak ditemukan');
        }

        $viewData = array(
             "data"    => $spesialisasi,
             "type"    => ['general' => Spesialisasi::TYPE, 'niche'=>Spesialisasi::TYPE_NICHE],
             "boxTitle"=> 'Edit',
             'formAction'  => array('admin.spesialisasi.update', $id),
             'formMethod'  => 'PUT',
             'groups' => $group,
             'vacancyTypes' => $vacancyTypes
        );

        return  View::make('admin.contents.vacancy.spesialisasi.form', $viewData);
    }

    public function update(Request $request, $id)
    {
        $spesialisasi = Spesialisasi::find($id);

        $spesialisasi->vacancy_spesialisasi_type_id = $request->input('type');
        $spesialisasi->name = $request->input('spesialisasi_name');
        $spesialisasi->group = $request->input('group');
        $spesialisasi->is_active = $request->filled('is_active') ? 1 : 0;
        $spesialisasi->save();

        return  Redirect('admin/spesialisasi')
                ->with('message', 'Berhasil');
    }

    public function destroy(Request $request, $id)
    {
    	$spesialisasi = Spesialisasi::where('id', $id)->first();

        if (is_null($spesialisasi)) {
            return  Redirect('admin/spesialisasi')
                ->with('message', 'Data tidak ditemukan');
        }
        $spesialisasi->delete();
        return  Redirect('admin/spesialisasi')
                ->with('message', 'Berhasil menghapus data');
    }
}
