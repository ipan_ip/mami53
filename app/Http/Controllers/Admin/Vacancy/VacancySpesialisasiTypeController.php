<?php

namespace App\Http\Controllers\admin\Vacancy;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Facades\View;
use Auth;
use App\Entities\Vacancy\VacancySpesialisasiType;
use Validator;

class VacancySpesialisasiTypeController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if(!Auth::user()->can('access-jobs')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        View::share('contentHeader', 'Vacancy Type Management');
        View::share('user', Auth::user());
    }

    public function index(Request $request)
    {
        $vacancyTypes    = VacancySpesialisasiType::query()->orderBy('id','asc');

        if($request->filled('q'))
        {
            $vacancyTypes->where('name','like', '%' . $request->get('q') . '%');
        }

        $vacancyTypes    = $vacancyTypes->paginate(20);

        $viewData = array(
            'boxTitle'      => 'Vacancy Types',
            'vacancyTypes'  => $vacancyTypes,
        );

        return View::make('admin.contents.vacancy.types.index', $viewData);
    }

    public function create(Request $request)
    {
        $viewData = array(
            'boxTitle'  => 'Insert Vacancy Types'
        );

        return  View::make('admin.contents.vacancy.types.create', $viewData);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:250'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors());
        }

        $vacancyType = new VacancySpesialisasiType;
        $vacancyType->name = $request->name;
        $vacancyType->save();

        return redirect()->route('admin.vacancy-speciality-types.index')->with('message', 'Data berhasil ditambahkan');
    }

    public function edit(Request $request, $id)
    {
        $vacancyType = VacancySpesialisasiType::find($id);

        $viewData = array(
            'boxTitle'    => 'Insert Vacancy Types',
            'vacancyType' => $vacancyType
        );

        return  View::make('admin.contents.vacancy.types.edit', $viewData);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:250'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors());
        }

        $vacancyType = VacancySpesialisasiType::find($id);
        $vacancyType->name = $request->name;
        $vacancyType->save();

        return redirect()->route('admin.vacancy-speciality-types.index')->with('message', 'Data berhasil dirubah');
    }
}
