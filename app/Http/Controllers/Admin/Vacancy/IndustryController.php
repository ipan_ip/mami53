<?php

namespace App\Http\Controllers\admin\Vacancy;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\Entities\Vacancy\Industry;
use Illuminate\Support\Facades\Redirect;

class IndustryController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if(!Auth::user()->can('access-jobs')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        View::share('contentHeader', 'Stories Management');
        View::share('user', Auth::user());
    }

    public function index(Request $request)
    {
    	$industry    = Industry::query()->orderBy('id','asc');

        if($request->filled('q'))
        {
            $industry->where('name','like', '%' . $request->get('q') . '%');
        }

        $industry    = $industry->paginate(20);

        $viewData = array(
            'boxTitle'        => 'Industry',
            'industry'     => $industry,
        );

        return View::make('admin.contents.vacancy.industry.index', $viewData);
    }

    public function create(Request $request)
    {
        $industry = new Industry;
        $industry->name = Input::old('industry_name');
        $industry->is_active = Input::old('is_active');

        $viewData = array(
            'boxTitle'    => 'Insert industry',
            'data'      => $industry,
            'formMethod'  => 'POST',
            'formAction'  => 'admin.industry.store',
        );

        return  View::make('admin.contents.vacancy.industry.form', $viewData);
    }

    public function store(Request $request)
    {

        $this->validate($request, array(
            'industry_name'        => 'required',
        ));

        $industry = new Industry();
        $industry->name = $request->input('industry_name');
        if ($request->filled('is_active')) $industry->is_active = 1;
        else $industry->is_active = 0;
        $industry->save();

        if ($industry) {
      
            return  Redirect::route('admin.industry.index')
                ->with('message', 'Created');
      
        } else {

            return  Redirect::route('admin.industry.create')
                ->with('error_message', 'Aborted')
                ->withInput();
        }

    }

    public function edit(Request $request, $id)
    {
        $industry = Industry::where('id', $id)->first();

        if (is_null($industry)) {
            return  Redirect::route('admin.jobs.index')
                ->with('message', 'Data tidak ditemukan');
        }

        $viewData = array(
             "data"    => $industry,
             "boxTitle"=> 'Edit',
             'formAction'  => array('admin.industry.update', $id),
             'formMethod'  => 'PUT',
        );

        return  View::make('admin.contents.vacancy.industry.form', $viewData);
    }

    public function update(Request $request, $id)
    {
        $industry = Industry::where('id', $id)->first();
        if (is_null($industry)) {
            return  Redirect('admin/industry')
                ->with('message', 'Berhasil');
        }

        $industry->name = $request->input('industry_name');
        if ($request->filled('is_active')) $industry->is_active = 1;
        else $industry->is_active = 0;
        $industry->save();

        return  Redirect('admin/industry')
                ->with('message', 'Berhasil');
    }

    public function destroy(Request $request, $id)
    {
    	$industry = Industry::where('id', $id)->first();

        if (is_null($industry)) {
            return  Redirect('admin/industry')
                ->with('message', 'Data tidak ditemukan');
        }
        $industry->delete();
        return  Redirect('admin/industry')
                ->with('message', 'Berhasil menghapus data');
    }
}
