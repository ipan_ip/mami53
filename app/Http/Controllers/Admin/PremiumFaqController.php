<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use App\Entities\Premium\PremiumFaq;
use Illuminate\Support\Facades\Input;
use App\Repositories\Premium\PremiumFaqRepository;

class PremiumFaqController extends Controller
{
    protected $isActive = [0, 1];
    protected $repository;
    public function __construct(PremiumFaqRepository $repository)
    {
        $this->middleware(function ($request, $next) {
            if(!Auth::user()->can('access-pay-confirmation')) {
                return redirect('admin');
            }
            
            return $next($request);
        });
        View::share('contentHeader', 'Stories Management');
        View::share('user', Auth::user());
        $this->repository = $repository;
    }

   
    public function index(Request $request)
    {
        $faqList = PremiumFaq::orderBy('id', 'desc')->get();
        $viewData = array(
            'boxTitle'     => 'FAQ List',
            'deleteAction' => 'admin.premium.faq.destroy',
            'faqList'      => $faqList,
        );

        return View::make('admin.contents.premium-faq.index', $viewData);
    }

    public function create()
    {
        $faq = new PremiumFaq;

        $faq->question = Input::old('question');
        $faq->answer = Input::old('answer');
        $faq->is_active = Input::old('is_active');

        $viewData = array(
            'boxTitle' => 'Insert FAQ',
            'faq' => $faq,
            'rowsActive' => $this->isActive,
            'formAction' => '/admin/premium/faq',
            'formMethod'  => 'POST',
        );

        return View::make('admin.contents.premium-faq.form', $viewData);
    }

    public function store(Request $request)
    {
        $this->validate($request, array(
            'question' => 'required',
            'answer' => 'required',
            'is_active' => 'required'
        ));
        
        $this->repository->store($request->all());
        return redirect('/admin/premium/faq')->with('message', 'Sukses menambah FAQ');
    }

    public function edit($id)
    {
        $faq = PremiumFaq::where('id', $id)->first();
        if (is_null($faq)) {
            return redirect()->back()->with('error_message', 'Data tida tidak ditemukan');
        }

        $viewData = array(
            'boxTitle' => 'Edit FAQ',
            'faq' => $faq,
            'rowsActive' => $this->isActive,
            'formAction' => '/admin/premium/faq/'.$faq->id,
            'formMethod'  => 'PUT',
        );

        return View::make('admin.contents.premium-faq.form', $viewData);
    }

    public function update(Request $request, $id)
    {
    	$this->validate($request, array(
            'question' => 'required',
            'answer' => 'required',
            'is_active' => 'required'
        ));

        $this->repository->edit($request, $id);
        return redirect('/admin/premium/faq')->with('message', 'Sukses edit FAQ');
    }

    public function destroy($id)
    {
        $faq = $this->repository->destroy($id);
        
        if (is_null($faq)) {
            $message = "Data tidak ditemukan";
        } else {
            $message = "Sukses menghapus data";
        }

        return back()->with('message', $message);
    }

}
