<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Entities\Landing\LandingMetaOg;
use App\Repositories\Landing\LandingMetaOgRepositoryEloquent;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\{
    Auth,
    Input,
    Validator,
    View
};
use Illuminate\Validation\Rule;
use Illuminate\Validation\Validator as ValidatorReturnType;
use Illuminate\View\View as customView;

class LandingMetaOgController extends Controller
{
    public $validationMessages = [
        'page_type.required' => 'Page Type field must be chosen',
        'image.url' => 'Format url image doesn`t correct'
    ];

    const INDEX_TITLE = 'Landing Meta';
    const CREATE_TITLE = 'New Landing Meta';
    const EDIT_TITLE = 'Edit Landing Meta';

    const SAVE_SUCCESS = 'Record success to saved.';
    const SAVE_FAIL = 'Sorry, record fail to save.';
    const UPDATE_SUCCESS = 'Record success to update.';
    const UPDATE_FAIL = 'Sorry, record fail to update.';

    private $repo;

    public function __construct()
    {
        View::share('contentHeader', 'User Account Management');
        View::share('user', Auth::user());

        $this->repo = app()->make(LandingMetaOgRepositoryEloquent::class);
    }

    /**
     * Index list meta page
     * 
     * @return customView
     */
    public function index(): customView
    {
        $data = [
            'boxTitle'           => self::INDEX_TITLE,
            'rows'               => $this->repo->index(),
            'sortUrl'            => null,
            'currentQueryString' => null
        ];

        return view('admin.contents.landing-metaog.index', $data);
    }

    /**
     * Create meta page
     * 
     * @return customView
     */
    public function create(): customView
    {
        $landingMetaOg = new LandingMetaOg;

        $landingMetaOg->title = Input::old('title');
        $landingMetaOg->description = Input::old('description');
        $landingMetaOg->keywords = Input::old('keywords');
        $landingMetaOg->image = Input::old('image');
        $landingMetaOg->is_active = (int) Input::old('is_active');

        $data = [
            'boxTitle'           => self::CREATE_TITLE,
            'pageTypes'          => LandingMetaOg::PAGE_TYPES,
            'row'                => $landingMetaOg,
            'formUrl'            => route('admin.landing.meta.save'),
            'formCancel'         => route('admin.landing.meta.index'),
            'formMethod'         => 'POST',
            'keywords'           => LandingMetaOg::LIST_KEYWORDS
        ];

        return view('admin.contents.landing-metaog.form', $data);
    }

    /**
     * Save data
     * 
     * @param Request $request
     * 
     * @return RedirectResponse
     */
    public function save(Request $request): RedirectResponse
    {
        $validator = $this->setValidation($request);

        if ($validator->fails()) {
            return redirect()->back()
                ->withInput()
                ->withErrors($validator->errors());
        }

        if ($this->repo->save($request)) {
            return redirect()->route('admin.landing.meta.index')->with('message', self::SAVE_SUCCESS);
        }

        return redirect()->route('admin.landing.meta.index')->withErrors(self::SAVE_FAIL);
    }

    /**
     * Edit page
     * 
     * @param int $id
     * 
     * @return customView
     */
    public function edit(int $id): customView
    {
        $data = [
            'boxTitle'           => self::EDIT_TITLE,
            'pageTypes'          => LandingMetaOg::PAGE_TYPES,
            'row'                => $this->repo->findById($id),
            'formUrl'            => route('admin.landing.meta.update', ['id' => $id]),
            'formCancel'         => route('admin.landing.meta.index'),
            'formMethod'         => 'POST',
            'keywords'           => LandingMetaOg::LIST_KEYWORDS
        ];

        return view('admin.contents.landing-metaog.form', $data);
    }

    /**
     * Update existed record
     * 
     * @param Request $request
     * @param int $id
     * 
     * @return RedirectResponse
     */
    public function update(Request $request, $id): RedirectResponse
    {
        $validator = $this->setValidation($request);

        if ($validator->fails()) {
            return redirect()->back()
                ->withInput()
                ->withErrors($validator->errors());
        }

        if ($this->repo->edit($request, $id)) {
            return redirect()->route('admin.landing.meta.index')->with('message', self::UPDATE_SUCCESS);
        }

        return redirect()->route('admin.landing.meta.index')->withErrors(self::UPDATE_FAIL);
    }

    /**
     * Set validation
     * 
     * @param Request $request
     * 
     * @return Validator
     */
    private function setValidation(Request $request): ValidatorReturnType
    {
        return Validator::make(
            $request->all(),
            [
                'page_type' => [
                    'required',
                    Rule::in(LandingMetaOg::PAGE_TYPES),
                ],
                'image' => [
                    'url',
                ]
            ],
            $this->validationMessages
        );
    }
}