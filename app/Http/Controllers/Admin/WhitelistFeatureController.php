<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Feature\WhitelistFeature;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\WhitelistFeatureRequest;
use App\Repositories\WhitelistFeatureRepository;


class WhitelistFeatureController extends Controller
{
    protected $repository;

    public function __construct(WhitelistFeatureRepository $repository)
    {
        $this->repository = $repository;

        View::share('contentHeader', 'Stories Management');
        View::share('user', Auth::user());
    }

    /**
     * Display a listing of the resource.
     *
     * @return view
     */
    public function index()
    {
        $whitelistFeatures = $this->repository->all();
        $viewData = [
            'boxTitle' => 'Whitelist Features',
            'whitelistFeatures' => $whitelistFeatures,
            'deleteAction'  => 'admin.whitelist-features.destroy',
        ];
        ActivityLog::LogIndex(Action::WHITELIST_FEATURE);
        return View::make('admin.contents.whitelist-feature.index', $viewData);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return view
     */
    public function create(Request $request)
    {
        $whitelistFeature = new WhitelistFeature;
        $whitelistFeature->name = $request->old('name');
        $whitelistFeature->user_id = $request->old('user_id');
        $viewData = array(
            'boxTitle'      => 'Whitelist Features',
            'whitelistFeature'  => $whitelistFeature,
            'formAction'    => 'admin.whitelist-features.store',
            'formMethod'    => 'POST',
        );
        return View::make('admin.contents.whitelist-feature.form', $viewData);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\WhitelistFeatureRequest $request
     * @return view
     */
    public function store(WhitelistFeatureRequest $request)
    {
        $input = $request->validated();
        $whitelistFeature = new WhitelistFeature;
        $whitelistFeature->name = $input['name'];
        $whitelistFeature->user_id = $input['user_id'];
        $result = $this->repository->save($whitelistFeature);
        if ($result[0]) {
            ActivityLog::LogCreate(Action::WHITELIST_FEATURE, $result[2]);
            return Redirect::route('admin.whitelist-features.index')
                ->with('message', 'Whitelist Feature created');

        } else {
            ActivityLog::LogError(Action::WHITELIST_FEATURE, $result[1]);
            return Redirect::route('admin.whitelist-features.create')
                ->with('error_message', 'Aborted. ' . $result[1])
                ->withInput();

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return view
     */
    public function edit($id)
    {
        $whitelistFeature = $this->repository->find($id);
        $viewData = array(
            'boxTitle'      => 'Whitelist Features',
            'whitelistFeature'  => $whitelistFeature,
            'formAction'    => ['admin.whitelist-features.update', $id],
            'formMethod'    => 'PUT',
        );

        return View::make('admin.contents.whitelist-feature.form', $viewData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\WhitelistFeatureRequest $request
     * @param  int  $id
     * @return view
     */
    public function update(WhitelistFeatureRequest $request, $id)
    {
        $input = $request->validated();
        $whitelistFeature = $this->repository->find($id);
        $whitelistFeature->name = $input['name'];
        $whitelistFeature->user_id = $input['user_id'];
        $result = $this->repository->save($whitelistFeature);
        if ($result[0]) {
            ActivityLog::LogUpdate(Action::WHITELIST_FEATURE, $result[2]);
            return Redirect::route('admin.whitelist-features.index')
                ->with('message', 'Whitelist Feature updated');

        } else {
            ActivityLog::LogError(Action::WHITELIST_FEATURE, $result[1]);
            return Redirect::route('admin.whitelist-features.edit', $id)
                ->with('error_message', 'Aborted. ' . $result[1])
                ->withInput();

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return view
     */
    public function destroy($id)
    {
        $whitelistFeature = $this->repository->find($id);
        $name = $whitelistFeature->name;
        $userId = $whitelistFeature->user_id;
        $result = $this->repository->delete($id);
        if ($result) {
            ActivityLog::LogDelete(Action::WHITELIST_FEATURE, $id);
            return Redirect::route('admin.whitelist-features.index')
                ->with('message', 'Whitelist Feature "' . $name . '" with user id ' . $userId . ' has been deleted');
        }
        return Redirect::route('admin.whitelist-features.index')
            ->with('error_message', 'Whitelist Feature "' . $name . '" with user id ' . $userId . ' failed to delete');
    }
}
