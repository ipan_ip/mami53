<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use View;
use Auth;
use DB;
use App\Entities\Voucher\VoucherProgram;
use App\Entities\Voucher\VoucherRecipient;
use App\Entities\Voucher\VoucherRecipientFile;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;
use Validator;
use App\User;
use App\Http\Helpers\Shortcode;
use App\Libraries\SMSLibrary;

class VoucherProgramController extends Controller
{
    protected $validationMessages = [
        'name.required'=>'Nama harus diisi',
        'name.max'=>'Nama maksimum :max karakter',
        'email_subject.max'=>'Subject Email maksimal :max karakter',
        'sms_template.max'=>'SMS Template maksimal :max karakter'
    ];

    protected $recipientValidationMessages = [
        'voucher_code.required'=>'Kode Voucher harus diisi',
        'voucher_code.max'=>'Kode Voucher maksimal :max karakter',
        'email.required'=>'Email harus diisi',
        'email.max'=>'Email maksimal :max karakter',
        'email.email'=>'Format Email tidak valid',
        'phone_number.max'=>'Nomor Telepon maksimal :max karakter'
    ];

    protected $importValidationMessages = [
        'user_type.required_if' => 'Tipe User harus diisi',
        'file.mimes'=>'Format file Anda tidak valid'
    ];

	public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if(!Auth::user()->can('access-voucher-program')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        View::share('contentHeader', 'Voucher Program Management');
        View::share('user', Auth::user());
    }

    public function index(Request $request)
    {
        $voucherPrograms = null;

        if($request->filled('q') && $request->get('q') != '') {
            $voucherPrograms = VoucherProgram::where('name', 'like', '%' . $request->get('q') . '%')->paginate(20);
        } else {
            $voucherPrograms = VoucherProgram::paginate(20);
        }

        $viewData = [];
        $viewData['voucherPrograms'] = $voucherPrograms;
        $viewData['boxTitle'] = 'Voucher Program';

        ActivityLog::LogIndex(Action::VOUCHER_PROGRAM);

        return view('admin.contents.voucher.index', $viewData);
    }

    public function create(Request $request)
    {
        $viewData = [];
        $viewData['boxTitle'] = 'Tambah Voucher Program';

        return view('admin.contents.voucher.create', $viewData);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
                        'name'=>'required|max:150',
                        'email_subject'=>'max:255',
                        'sms_template'=>'max:255'
                    ], $this->validationMessages);

        if($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors())->withInput();
        }

        $voucherProgram = new VoucherProgram;
        $voucherProgram->name = $request->name;
        $voucherProgram->email_subject = $request->email_subject;
        $voucherProgram->email_template = $request->email_template;
        $voucherProgram->sms_template = $request->sms_template;
        $voucherProgram->status = VoucherProgram::STATUS_CREATED;
        $voucherProgram->save();

        ActivityLog::LogCreate(Action::VOUCHER_PROGRAM, $voucherProgram->id);

        return redirect()->route('admin.voucher.index')->with('message', 'Voucher Program berhasil dibuat');
    }

    public function edit(Request $request, $id)
    {
        $voucherProgram = VoucherProgram::find($id);

        $viewData = [];
        $viewData['boxTitle'] = 'Edit Voucher Program';
        $viewData['voucherProgram'] = $voucherProgram;

        return view('admin.contents.voucher.edit', $viewData);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
                        'name'=>'required|max:150',
                        'email_subject'=>'max:255',
                        'sms_template'=>'max:255'
                    ], $this->validationMessages);

        if($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors())->withInput();
        }

        $voucherProgram = VoucherProgram::find($id);

        $voucherProgram->name = $request->get('name');
        $voucherProgram->email_subject = $request->get('email_subject');
        $voucherProgram->email_template = $request->get('email_template');
        $voucherProgram->sms_template = $request->get('sms_template');
        $voucherProgram->save();

        ActivityLog::LogUpdate(Action::VOUCHER_PROGRAM, $id);
        
        return redirect()->route('admin.voucher.index')->with('message', 'Voucher Program berhasil dirubah');
    }

    public function runNotifier(Request $request, $id)
    {
        $voucherProgram = VoucherProgram::find($id);

        if(!$voucherProgram) {
            return redirect()->back()->with('error_message', 'Data tidak ditemukan');
        }

        $voucherProgram->status = VoucherProgram::STATUS_RUNNING;
        $voucherProgram->save();

        return redirect()->back()->with('message', 'Proses notifikasi sedang dijalankan');
    }

    public function pauseNotifier(Request $request, $id)
    {
        $voucherProgram = VoucherProgram::find($id);

        if(!$voucherProgram) {
            return redirect()->back()->with('error_message', 'Data tidak ditemukan');
        }

        $voucherProgram->status = VoucherProgram::STATUS_PAUSED;
        $voucherProgram->save();

        return redirect()->back()->with('message', 'Proses notifikasi ditunda');
    }

    public function stopNotifier(Request $request, $id)
    {
        $voucherProgram = VoucherProgram::find($id);

        if(!$voucherProgram) {
            return redirect()->back()->with('error_message', 'Data tidak ditemukan');
        }

        $voucherProgram->status = VoucherProgram::STATUS_STOPPED;
        $voucherProgram->save();

        return redirect()->back()->with('message', 'Proses notifikasi dihentikan');
    }

    public function testEmail(Request $request, $id)
    {
        $voucherProgram = VoucherProgram::find($id);

        if(!$voucherProgram) {
            return redirect()->back()->with('error_message', 'Data tidak ditemukan');
        }

        $viewData = [];
        $viewData['boxTitle'] = 'Kirim Test Email';
        $viewData['voucherProgram'] = $voucherProgram;

        return view('admin.contents.voucher.testemail', $viewData);
    }

    public function testEmailSubmit(Request $request, $id)
    {
        $voucherProgram = VoucherProgram::find($id);

        if(!$voucherProgram) {
            return redirect()->back()->with('error_message', 'Data tidak ditemukan');
        }

        $validator = Validator::make($request->all(),
                        [
                            'destination'=>'required|email'
                        ],
                        [
                            'destination.required'=>'Email Tujuan harus diisi',
                            'destination.email'=>'Format Email Tujuan tidak valid'
                        ]);

        if($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors())->withInput();
        }

        $emailTemplate = $voucherProgram->email_template;
        $emailTemplate = str_replace(['[VOUCHERMAMIKOS]'], 
                            '[voucher]', $emailTemplate);

        $shortcodeFactory = new Shortcode;
        $emailTemplate = $shortcodeFactory->compile($emailTemplate);

        $destination = $request->get('destination');

        \Mail::send('emails.voucher-reward', ['template' => $emailTemplate], function($message) use ($destination, $voucherProgram)
        {
            $message->to($destination)->subject($voucherProgram->email_subject != '' ? $voucherProgram->email_subject : 'Voucher dari MamiKos!');
        });

        return redirect()->back()->withInput()->with('message', 'Test email sudah dikirim. Silakan periksa Inbox Anda');
    }

    public function testSMSSubmit(Request $request, $id)
    {
        $voucherProgram = VoucherProgram::find($id);

        if(!$voucherProgram) {
            return redirect()->back()->with('error_message', 'Data tidak ditemukan');
        }

        $validator = Validator::make($request->all(),
                        [
                            'destination_phone'=>'required'
                        ],
                        [
                            'destination_phone.required'=>'Nomor Telepon Tujuan harus diisi',
                        ]);

        if($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors())->withInput();
        }

        $smsTemplate = $voucherProgram->sms_template;
        $smsTemplate = str_replace(['[VOUCHERMAMIKOS]'], 
                            '[voucher]', $smsTemplate);

        $shortcodeFactory = new Shortcode;
        $smsTemplate = $shortcodeFactory->compile($smsTemplate);

        $smsStatus = SMSLibrary::send(SMSLibrary::phoneNumberCleaning($request->destination_phone), $smsTemplate);

        if($smsStatus == 'Message Sent') {
            return redirect()->back()->withInput()->with('message', 'Test SMS sudah dikirim. Silakan periksa kotak masuk Anda');
        } else {
            return redirect()->back()->withInput()->with('error_message', 'Test SMS gagal dikirim, silakan cek ulang nomor tujuan');
        }

        
    }

    public function recipient(Request $request, $voucherProgramId)
    {
        $voucherProgram = VoucherProgram::find($voucherProgramId);

        $query = VoucherRecipient::where('voucher_program_id', $voucherProgramId);
        if($request->filled('q') && $request->get('q') != '') {
            $query = $query->where(function($query) use ($request){
                                $query->where('email', 'like', '%' . $request->get('q') . '%')
                                    ->orWhere('voucher_code', 'like', '%' . $request->get('q') . '%')
                                    ->orWhere('phone_number', 'like', '%' . $request->get('q') . '%');
                            });
        }

        $recipients = $query->paginate(20);

        $viewData = [];
        $viewData['voucherProgram'] = $voucherProgram;
        $viewData['recipients'] = $recipients;
        $viewData['boxTitle'] = 'Voucher Program Recipient';

        return view('admin.contents.voucher.recipients', $viewData);
    }

    public function recipientCreate(Request $request, $voucherProgramId)
    {
        $voucherProgram = VoucherProgram::find($voucherProgramId);

        $viewData = [];
        $viewData['voucherProgram'] = $voucherProgram;
        $viewData['boxTitle'] = 'Voucher Program Recipient';

        return view('admin.contents.voucher.recipient-create', $viewData);
    }

    public function recipientStore(Request $request, $voucherProgramId)
    {
        $validator = Validator::make($request->all(),
                            [
                                'voucher_code'=>'required|max:150',
                                'email'=>'email|max:150',
                                'phone_number'=>'max:100'
                            ], $this->recipientValidationMessages);

        if($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors())->withInput();
        }

        $recipient = new VoucherRecipient;
        $recipient->voucher_program_id = $voucherProgramId;
        $recipient->voucher_code = $request->voucher_code;
        $recipient->email = $request->email;
        $recipient->phone_number = $request->phone_number;
        $recipient->status = VoucherRecipient::STATUS_CREATED;
        $recipient->save();

        return redirect()->route('admin.voucher.recipient.index', $voucherProgramId)->with('message', 'Penerima voucher berhasil ditambahkan');
    }

    public function recipientEdit(Request $request, $voucherProgramId, $recipientId)
    {
        $voucherProgram = VoucherProgram::find($voucherProgramId);

        $recipient = VoucherRecipient::where("voucher_program_id", $voucherProgramId)
                                    ->where('id', $recipientId)
                                    ->first();

        $viewData = [];
        $viewData['voucherProgram'] = $voucherProgram;
        $viewData['recipient'] = $recipient;
        $viewData['boxTitle'] = 'Voucher Program Recipient';

        return view('admin.contents.voucher.recipient-edit', $viewData);
    }

    public function recipientUpdate(Request $request, $voucherProgramId, $recipientId)
    {
        $validator = Validator::make($request->all(),
                            [
                                'voucher_code'=>'required|max:150',
                                'email'=>'email|max:150',
                                'phone_number' => 'max:100'
                            ], $this->recipientValidationMessages);

        if($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors())->withInput();
        }

        $recipient = VoucherRecipient::where("voucher_program_id", $voucherProgramId)
                                    ->where('id', $recipientId)
                                    ->first();

        $recipient->voucher_code = $request->voucher_code;
        $recipient->email = $request->email;
        $recipient->phone_number = $request->phone_number;
        $recipient->save();

        return redirect()->route('admin.voucher.recipient.index', $voucherProgramId)->with('message', 'Penerima voucher berhasil dirubah');
    }


    public function recipientImport(Request $request, $voucherProgramId)
    {
        $voucherProgram = VoucherProgram::find($voucherProgramId);

        $viewData = [];
        $viewData['voucherProgram'] = $voucherProgram;
        $viewData['boxTitle'] = 'Voucher Program Recipient';

        return view('admin.contents.voucher.recipient-import', $viewData);
    }


    public function recipientImportSubmit(Request $request, $voucherProgramId)
    {
        $validator = Validator::make($request->all(),
                    [
                        'use_file_email'=>'nullable',
                        'user_type'=>'required_if:use_file_email,1',
                        'join_start'=>'date_format:Y-m-d',
                        'join_end'=>'date:Y-m-d|after:join_start',
                        'file'=>'mimes:csv,txt'
                    ], $this->importValidationMessages);

        if($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors())->withInput();
        }

        $options = [
            'use_file_email'=>$request->get('use_file_email')
        ];

        if($request->get('use_file_email') != 1) {
            $options['user_type'] = $request->get('user_type');
            $options['sort_type'] = $request->get('sort_type');
            $options['sort_direction'] = $request->get('sort_direction');

            if($request->get('join_start') != '') {
                $options['join_start'] = $request->get('join_start');
            }

            if($request->get('join_end') != '') {
                $options['join_end'] = $request->get('join_end');
            }
        }

        $uploadedFile = VoucherRecipientFile::upload($request->file('file'), $voucherProgramId, $options);

        if($uploadedFile === FALSE) {
            return redirect()->back()->with('error_message', 'File gagal diupload')->withInput();
        }

        return redirect()->route('admin.voucher.recipient.index', $voucherProgramId)->with('message', 'Voucher sedang di-import, silakan tunggu.');
    }


    public function recipientStatusImport(Request $request, $voucherProgramId)
    {
        $voucherProgram = VoucherProgram::find($voucherProgramId);

        $recipientFiles = VoucherRecipientFile::where('voucher_program_id', $voucherProgramId)
                        ->get();

        $viewData = [];
        $viewData['voucherProgram'] = $voucherProgram;
        $viewData['recipientFiles'] = $recipientFiles;
        $viewData['boxTitle'] = 'Voucher Program Recipient Import Status';

        return view('admin.contents.voucher.recipient-status-import', $viewData);
    }


    public function recipientSendNotif(Request $request, $voucherProgramId, $recipientId)
    {
        $voucherProgram = VoucherProgram::find($voucherProgramId);

        $recipient = VoucherRecipient::where('voucher_program_id', $voucherProgramId)
                                    ->where('id', $recipientId)
                                    ->first();

        if(!$recipient) {
            return redirect()->back()->with('error_message', 'Data tidak ditemukan');
        }

        if(is_null($recipient->email) || $recipient->email == '') {
            return redirect()->back()->with('error_message', 'Email kosong');
        }

        $emailTemplate = $voucherProgram->email_template;
        $emailTemplate = str_replace(['[VOUCHERMAMIKOS]'], 
                            '[voucher code="' . $recipient->voucher_code . '"]', $emailTemplate);

        $shortcodeFactory = new Shortcode;
        $emailTemplate = $shortcodeFactory->compile($emailTemplate);

        $destination = $recipient->email;;

        \Mail::send('emails.voucher-reward', ['template' => $emailTemplate], function($message) use ($destination, $voucherProgram)
        {
            $message->to($destination)->subject($voucherProgram->email_subject != '' ? $voucherProgram->email_subject : 'Voucher dari MamiKos!');
        });

        return redirect()->back()->with('message', 'Email notifikasi berhasil dikirim');
    }
}