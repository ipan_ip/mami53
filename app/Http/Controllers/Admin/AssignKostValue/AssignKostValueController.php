<?php

namespace App\Http\Controllers\Admin\AssignKostValue;

use App\Entities\Level\KostLevel;
use App\Entities\Level\KostLevelValue;
use App\Entities\Level\KostLevelValueMapping;
use App\Entities\Room\KostValue;
use App\Entities\Room\Room;
use App\Http\Controllers\Web\MamikosController;
use App\Presenters\KostLevelValuePresenter;
use App\Presenters\RoomPresenter;
use App\Repositories\RoomRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

class AssignKostValueController extends MamikosController
{
    protected $title = 'Assign Kost Benefit Management';
    protected $repository;

    public const DEFAULT_LIMIT = 10;

    public function __construct(RoomRepository $repository)
    {
        $this->middleware(function ($request, $next) {
            
            if (!Auth::user()->can('access-kost-level')) {
                return redirect('admin');
            }

            return $next($request);
        });

        $this->repository = $repository;

        View::share('user', Auth::user());
        View::share('contentHeader', $this->title);
    }

    /**
     * Index page of assign kost benefit
     * 
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $benefits = KostLevelValue::getBenefit(
            self::DEFAULT_LIMIT, $request->all()
        );

        $viewData = [
            'boxTitle' => $this->title,
            'benefits' => $benefits,
        ];

        return view('admin.contents.assign-kost-value.index', $viewData);
    }

    /**
     * Create page for assign or update kost to benefit
     * 
     * @param int $songId
     * @return \Illuminate\View\View
     */
    public function create($songId)
    {
        $room = Room::where('song_id', $songId)->first();
        if (! $room) {
            return redirect()
                ->route('admin.assign-kost-value.index')
                ->with('messages', 'Room is not exist');
        }

        $level = $room->level_info;
        $kostLevel = KostLevel
            ::with(['values.photo', 'values.photoSmall'])
            ->find($level['id']);

        // get benefit from kost_level of room
        $kostLevelBenefit = $kostLevel->values;
        $benefitsFromLevel = (new KostLevelValuePresenter('list'))
            ->present($kostLevelBenefit);

        // get benefit from designer_kost_value (ala carte benefit)
        $kostBenefit = $room->kost_values;
        $benefitsFromKost = (new KostLevelValuePresenter('list'))
            ->present($kostBenefit);

        // get available benefit
        $availbleBenefitOption = KostLevelValue::select(['id', 'name'])
            ->whereNotIn('id', $kostLevelBenefit->pluck('id'))
            ->whereNotIn('id', $kostBenefit->pluck('id'))
            ->get();

        $viewData = [
            'room' => $room,
            'boxTitle' => $this->title,
            'availbleBenefitOption' => $availbleBenefitOption,
            'benefitsFromLevel' => $benefitsFromLevel['data'],
            'benefitsFromKost' => $benefitsFromKost['data'],
        ];

        return view('admin.contents.assign-kost-value.create', $viewData);
    }

    /**
     * Method to process post add benefit
     * 
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postAddBenefit(Request $request, $songId)
    {
        if (! $request->has('benefit_id')) {
            return redirect()->back()
                ->with('error_message', 'Benefit id not exist');
        }

        $benefitId = $request->input('benefit_id');

        // search benefit
        $benefit = KostLevelValue::find($benefitId);
        if (is_null($benefit)) {
            return redirect()->back()
                ->with('error_message', 'Benefit is not exist');
        }

        // search room
        $room = Room::where('song_id', $songId)->first();
        if (! $room) {
            return redirect()->back()
                ->with('error_message', 'room is not exist');
        }


        $level = $room->level_info;
        
        // check not duplicate from kost_level benefit
        $nDuplicateFromKostLevelBenefit = KostLevelValueMapping::getTotalBenefitKosLevelByIds(
            $level['id'],
            $benefitId
        );

        // check room benefit will not duplicate
        $nDuplicateFromCustomBenefit = KostValue::getTotalBenefitKosByIds(
            $room->id, 
            $benefitId
        );

        // benefit not allowed to be duplicate
        if (
            $nDuplicateFromCustomBenefit > 0
            || $nDuplicateFromKostLevelBenefit > 0
        ) {
            return redirect()->back()
                ->with('error_message', 'benefit sudah tersedia');
        }
        
        $newBenefit = new KostValue();
        $newBenefit->designer_id = $room->id;
        $newBenefit->kost_level_value_id = $benefit->id;
        $newBenefit->save();

        return redirect()->back()->with('success_message', 'add benefit success');
    }

    /**
     * Api to retrieve kost with benefit
     * 
     * @param Request $request
     * @return LengthAwarePaginator|Collection|mixed
     * @throws RepositoryException
     */
    public function getAllData(Request $request)
    {
        $this->repository->setPresenter(new RoomPresenter('list-kost-benefit'));

        $limit      = $request->get('limit', self::DEFAULT_LIMIT);

        $response   = $this->repository->with(['level', 'level_info']);

        $count      = $response->count();
        $response   = $response->paginate($limit);

        $response['total']  = $count;
        $response['rows']   = $response['data'];

        unset($response['data']);
        return $response;
    }

    /**
     * Remove benefit
     * 
     * @param int $songId
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postRemoveBenefit(Request $request, $songId)
    {
        // find benefit
        if (! $request->has('benefit_id')) {
            return response()->json([
                'success' => false,
                'message' => 'Benefit id is not exist',
            ]);
        }

        // find room
        $room = Room::where('song_id', $songId)->first();
        if (! $room) {
            return response()->json([
                'success' => false,
                'message' => 'Rooom is not exist found',
            ]);
        }

        $benefitId = $request->input('benefit_id');
        $room->kost_values()->detach($benefitId);

        return response()->json(['success' => true]);
    }

    /**
     * Verify valid room or no by url
     * @param Request $request
     */
    public function verify(Request $request)
    {
        $existedRoom = Room::with('photo')
            ->where('slug', $request->slug)
            ->where('is_active', 'true')
            ->first();

        if (is_null($existedRoom)) {
            return [
                'success' => false,
                'message' => "Kost tidak ditemukan!"
            ];
        }

        $existedRoom->photo_url = !is_null($existedRoom->photo) 
            ? $existedRoom->photo->getMediaUrl() 
            : [];

        return [
            'success' => true,
            'room'    => $existedRoom
        ];
    }
}