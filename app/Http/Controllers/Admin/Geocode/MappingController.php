<?php

namespace App\Http\Controllers\Admin\Geocode;

use App\Entities\Activity\Action;
use App\Entities\Activity\ActivityLog;
use App\Entities\Area\AreaGeolocation;
use App\Http\Controllers\Api\Search\SuggestionController;
use App\Http\Controllers\Web\MamikosController;
use Auth;
use Illuminate\Http\Request;
use View;

class MappingController extends MamikosController
{
    private const PAGE_TITLE = 'Area Mapping';

    public function __construct()
    {
        $this->middleware(
            function ($request, $next) {
                if (!Auth::user()->can('access-geocode')) {
                    return redirect('admin');
                }

                return $next($request);
            }
        );

        View::share('contentHeader', self::PAGE_TITLE);
        View::share('user', Auth::user());
    }

    public function index(Request $request)
    {
        $data = [];

        if ($request->filled('keyword')) {
            $response = SuggestionController::GetSuggestions($request->keyword);
            if (!empty($response['areas'])) {
                $data = $response['areas'];
            }
        }

        $viewData = [
            'title' => self::PAGE_TITLE,
            'rows' => $data,
        ];

        ActivityLog::LogIndex(Action::AREA_GEOCODE_MAPPING);

        return view('admin.contents.geocode.mapping', $viewData);
    }

    public function getBoundary(int $id)
    {
        $geo = AreaGeolocation::query()
            ->where('id', $id)
            ->first();

        return json_encode($geo->geolocation);
    }
}
