<?php

namespace App\Http\Controllers\Admin\Geocode;

use App\Entities\Activity\Action;
use App\Entities\Activity\ActivityLog;
use App\Entities\Area\AreaGeolocation;
use App\Http\Controllers\Web\MamikosController;
use Auth;
use Illuminate\Http\Request;
use View;

class IndexController extends MamikosController
{
    private const PAGE_TITLE = 'Area Geocode Master';
    private const DEFAULT_LIMIT = 15;

    protected $model;

    public function __construct(AreaGeolocation $model)
    {
        $this->middleware(
            function ($request, $next) {
                if (!Auth::user()->can('access-geocode')) {
                    return redirect('admin');
                }

                return $next($request);
            }
        );

        $this->model = $model;

        View::share('contentHeader', 'Area Geocode Management');
        View::share('user', Auth::user());
    }

    public function index(Request $request)
    {
        $datas = $this->model->getWithPagination(self::DEFAULT_LIMIT, $request->all());

        $viewData = [
            'title' => self::PAGE_TITLE,
            'rows' => $datas,
            'searchUrl' => route('admin.geocode.index', '#ucode'),
        ];

        ActivityLog::LogIndex(Action::AREA_GEOCODE);

        return view('admin.contents.geocode.index', $viewData);
    }
}
