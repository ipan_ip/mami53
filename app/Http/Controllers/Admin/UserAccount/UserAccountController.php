<?php

namespace App\Http\Controllers\Admin\UserAccount;

use App\Entities\User\UserRole;
use App\Repositories\UserDataRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use View;
use Auth;

class UserAccountController extends Controller
{
    private $userRepository;

    protected const PAGINATION_LIMIT = 15;

    public function __construct(UserDataRepository $userRepository)
    {
        $this->userRepository = $userRepository;
        View::share('contentHeader', 'User Account');
        View::share('user', Auth::user());
    }

    public function indexView()
    {
        return view('admin.contents.user.account');
    }


    public function indexData(Request $request)
    {
        $userList = $this->userRepository->with('social')
            ->leftJoin('user_verification_account', 'user.id', 'user_id');

        if (!empty($request->input('user_id'))) {
            $userList = $userList->where('user.id', $request->input('user_id'));
        }

        if (!empty($request->input('name'))) {
            $userList = $userList->whereRaw('match (name) against ("' . $request->input('name') . '" IN BOOLEAN MODE)');
        }

        if (!empty($request->input('phone_number'))) {
            $userList = $userList->where('phone_number', $request->input('phone_number'));
        }

        if (!empty($request->input('email'))) {
            $userList = $userList->where('email', $request->input('email'));
        }

        $total = $userList->count();
        $userList = $userList->select(
            'user.id',
            'name',
            'email',
            'phone_number',
            'user.created_at',
            'job',
            'work_place',
            'birthday',
            'gender',
            'is_owner',
            'is_tester',
            'is_verify_phone_number',
            'is_verify_email',
            'identity_card',
            'role'
        )
            ->limit($request->input('limit', self::PAGINATION_LIMIT))
            ->offset($request->input('offset', 0))
            ->get()
            ->map(
                function ($item) {
                    $item->register_from = $item->registerWith();

                    return $item;
                }
            );

        return response()->json(['total' => $total, 'rows' => $userList]);
    }

    public function destroy(Request $request, int $id)
    {
        $user = User::find($id);

        if ($user->role !== UserRole::Administrator) {
            return response()->json(['success' => false]);
        }

        $user->consultant()->delete();
        $user->delete();
        return response()->json(['success' => true]);
    }
}
