<?php

namespace App\Http\Controllers\Admin;

use App\Http\Helpers\ApiResponse;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Entities\Activity\ChatAdmin;
use App\Entities\Activity\CSResolved;
use Validator;

class CSResolvedController extends Controller
{

    public function logResolved(Request $request)
    {
        $idsString = implode(',', ChatAdmin::getAdminIds());
        $validator = Validator::make($request->all(), [
                'cs_id'=>'required|in:'.$idsString,
                'group_id'=>'required'
            ], [
                'cs_id.required' => 'CS ID harus diisi',
                'cs_id.in' => 'CS ID tidak valid',
                'group_id.required' => 'Group ID harus diisi'
            ]);

        if($validator->fails()) {
            return ApiResponse::responseData([
                'status'=>false,
                'meta'=> [
                    'message'=>implode('. ', $validator->errors()->all()),
                    'messages'=> $validator->errors()->all()
                ]
            ]);
        }

        $existingLog = CSResolved::where('cs_id', $request->cs_id)
                                ->where('group_id', $request->group_id)
                                ->first();

        if($existingLog) {
            return ApiResponse::responseData([
                'status'=>false
            ]);
        }

        $csResolved = new CSResolved;
        $csResolved->cs_id = $request->cs_id;
        $csResolved->group_id = $request->group_id;
        $csResolved->save();

        return ApiResponse::responseData([
            'status'=>true
        ]);
    }
}
