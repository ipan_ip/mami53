<?php

namespace App\Http\Controllers\Admin\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Entities\Media\Media;
use App\Entities\User\UserRole;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!\Auth::user()->can('access-area') && !\Auth::user()->can('access-mami-checker')) {
                return redirect('admin');
            }

            return $next($request);
        });

        \View::share('contentHeader', 'User Management');
        \View::share('user', \Auth::user());
    }

    public function index(Request $request)
    {
        // This gonna be a future feature :)
        return $request;
    }

    public function getSingleUser(Request $request)
    {
        $user = User::where('id', $request->input('id'))->first();
        return $user;
    }

    public function getUsersForChecker(Request $request)
    {
        $checkers = User::WhereDoesntHave('checker_role')
            ->with(['photo'])
            ->whereNotNull('name')
            ->where('name', '<>', '')
            ->where('is_verify', '=', '1')
            ->where('role', '<>', UserRole::Administrator)
            ->where('is_owner', '<>', 'false')
            ->orderBy('name');

        if ($request->filled('search')) $checkers->where('name', 'LIKE', '%' . $request->input('search') . '%');
        
        return $checkers->paginate(10);
    }

    public function getConsultantCandidates(Request $request)
    {
        $consultants = User::with(['photo'])
                        ->whereNotNull('name')
                        ->where('name', '<>', '')
                        ->where('role', UserRole::Administrator)
                        ->orderBy('name');

        if ($request->filled('search')) $consultants->where('name', 'LIKE', '%' . $request->input('search') . '%');

        return $consultants->paginate(10);
    }

    public function getOwners(Request $request)
    {
        $checkers = User::select('id', 'name', 'phone_number', 'is_verify', 'is_owner')
            ->whereNotNull('name')
            ->where('name', '<>', '')
            ->where('is_verify', '1')
            ->where('is_owner', 'true')
            ->orderBy('name');

        if ($request->filled('search')) {
            $checkers->where('name', 'LIKE', '%' . $request->input('search') . '%')
                ->orWhere('phone_number', 'LIKE', '%' . $request->input('search') . '%');
        }
        
        return $checkers->paginate(10);
    }
}
