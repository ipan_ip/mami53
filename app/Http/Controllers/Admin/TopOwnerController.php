<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Owner\TopOwnerHistory;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;
use App\Http\Controllers\Controller;
use App\User;
use DB;
use Illuminate\Http\Request;

class TopOwnerController extends Controller
{
    protected $inputTypes = array();

    protected $htmlBuilder;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!\Auth::user()->can('access-top-owner')) {
                return redirect('admin');
            }

            return $next($request);
        });

        \View::share('user', \Auth::user());
        \View::share('contentHeader', 'Top Owner Management');
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(Request $request)
    {
        $query = RoomOwner::with(['user', 'room' => function ($q) {
            $q->with(['avg_review'])
                ->withCount(['report_summary', 'report_summary_within_month', 'avg_review as review']);
        }])
            ->withCount(['room'])
            ->where(function ($q) {
                $q->where('owner_status', 'LIKE', 'Pemilik%');
                $q->orWhere('owner_status', 'LIKE', 'Pengelola%');
            })
            ->where('status', 'verified')
            ->whereNull('deleted_at')
            ->groupBy('user_id')
            ->orderBy('updated_at', 'desc');

        // Apply Filters
        $query = $this->filter($request, $query);

        $rowsOwner = $query->paginate(20);

        // Calculate average of total rating
        foreach($rowsOwner as $row) {
            $totalAvgRating = 0;
            if (count($row->room->avg_review) > 0) 
            {
                foreach ($row->room->avg_review as $review) {
                    $totalAvgRating += (int)round(($review->cleanliness + $review->comfort + $review->safe + $review->price + $review->room_facility + $review->public_facility)/6);
                }
                $totalAvgRating = round($totalAvgRating / count($row->room->avg_review));
            }
            $row->average_rating = $totalAvgRating;
        }

        $sortFilter = [
            'default' => "Tanpa Pengurutan",
            'report_summary_count' => "Total Report Terbanyak",
            'report_summary_within_month_count' => "Report Terbanyak Bulan Ini",
            'review_count' => "Total Review Terbanyak",
            'average_rating' => "Rating Terbaik",
        ];

        $statisticFilter = [
            "0" => "Semua Statistik",
            "1" => "Total Report > 5",
            "2" => "Report Bulan Ini > 0",
            "3" => "Total Review > 0",
            "4" => "Rating Dibawah 3",
            "5" => "Rating Diatas 3",
        ];

        $statusFilter = [
            'all' => 'Semua Status',
            'true' => '"Pemilik Top"',
            'false' => 'Non "Pemilik Top"',
        ];

        $viewData = [
            'rowsOwner' => $rowsOwner,
            'boxTitle' => null,
            'searchUrl' => route('admin.top-owner.index'),
            'sortFilter' => $sortFilter,
            'statisticFilter' => $statisticFilter,
            'statusFilter' => $statusFilter,
        ];

        ActivityLog::LogIndex(Action::OWNER_TOP);

        return view('admin.contents.top-owner.index', $viewData);
    }

    public function changeStatus($userId)
    {
        if (isset($userId)) {
            try {
                $owner = User::query()->where('id', $userId)->first();

                $newStatus = $owner->is_top_owner == 'true' ? 'false' : 'true';
                $owner->is_top_owner = $newStatus;
                $owner->save();

                // Store (de)activation history
                $topOwnerHistory = new TopOwnerHistory;
                $topOwnerHistory->store($userId, $newStatus == 'true' ? 'active' : 'deactive');

                return back()->with('message', 'Status berhasil diupdate');

            } catch (Exception $e) {
                return back()->with('error', $e);
            }
        }

        return back()->with('error', "ID owner tidak berhasil ditemukan");
    }

    private function filter(Request $request, $query)
    {
        if ($request->filled('q') or $request->filled('owner_name')) {
            // ini_set('memory_limit', '512M');
        }

        if ($request->filled('owner_name') && $request->input('owner_name')) {
            $user = User::where('name', 'like', '%' . $request->input('owner_name') . '%')
                ->orWhere('id', $request->input('owner_name'))
                ->get()
                ->pluck('id');

            $query->whereIn('user_id', $user);
        }

        if ($request->filled('phone_owner') && $request->input('phone_owner')) {
            $user = User::where('phone_number', 'like', '%' . $request->input('phone_owner') . '%')->get()->pluck('id');

            if (count($user) == 0) {
                $room = Room::where('owner_phone', 'like', '%' . $request->input('phone_owner') . '%')->get()->pluck('id');
                $query->whereIn('designer_id', $room);
            } else {
                $query->whereIn('user_id', $user);
            }
        }

        return $query;
    }
}
