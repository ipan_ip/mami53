<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Http\Helpers\ApiHelper;

use App\Entities\Premium\PremiumRequest;
use App\Entities\Premium\BalanceRequest;
use App\Entities\Premium\AccountConfirmation;
use App\Entities\Room\RoomOwner;
use App\User;
use App\Presenters\PremiumRequestPresenter;
use App\Entities\Premium\Bank;
use App\Entities\Premium\PremiumPackage;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;
use App\Repositories\PremiumRepositoryEloquent;
use App\Http\Helpers\ApiResponse as Api;
use App\Entities\Promoted\ViewPromote;
use DB;
use Carbon\Carbon;
use Validator;
use App\Services\Premium\PremiumRequestService;
use App\Http\Helpers\PhoneNumberHelper;

class RequestController extends Controller
{
    protected $service;

    public function __construct(PremiumRequestService $service)
    {
        $this->middleware(function ($request, $next) {
            if(!Auth::user()->can('access-premium-account-request')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        View::share('contentHeader', 'Stories Management');
        View::share('user', Auth::user());

        $this->service = $service;
    }

   
    public function index(Request $request)
    {
       $query = PremiumRequest::with([
           'user', 
           'premium_package', 
           'account_confirmation', 
           'view_promote', 
           'view_balance_request',
           'premium_executive_user'
        ]);

       $query = $this->filterIndex($request, $query);

       $query = $query->orderBy('id', 'desc')->paginate(20);

       $data = (new PremiumRequestPresenter)->present($query);

        $viewData = array(
            'boxTitle'     => 'Request Premium Account List',
            'deleteAction' => 'admin.tag.destroy',
            'rowsRequest'  => /*$query->items()*/$data['data'],
            'rowPaginate'  => $query,
            'statuso'      => array('Package Status', 'confirm', 'waiting')
        );

        ActivityLog::LogIndex(Action::PREMIUM_ACCOUNT_REQUEST);

        return View::make('admin.contents.request.index', $viewData);
    }


    private function filterIndex(Request $request, $query)
    {
        if ($request->filled('q')) {
            $query = $this->searchPhoneOwner($request->input('q'), $query);
        }

        if ($request->filled('statuso')) {
            if ($request->input('statuso') == '1') $query->where('status', '1');
            else if ($request->input('statuso') == '2') $query->where('status', '0');
        }

        if ($request->filled('price_total')) {
            $query->where('total', 'LIKE', '%'.$request->input('price_total').'%');
        }

        return $query;
    }

    private function searchPhoneOwner($keyword, $query)
    {
        $userId = User::where('phone_number', 'LIKE', '%'.PhoneNumberHelper::sanitizeNumber($keyword).'%')
                            ->get()
                            ->pluck('id')
                            ->toArray();

        $query->whereIn('user_id', $userId);
        
        return $query;
    }

    public function balance(Request $request)
    {
       $query = BalanceRequest::with('premium_request', 'premium_request.user', 'account_confirmation');
       
       $query = $this->filterBalanceIndex($request, $query);

       if ($request->filled('get')) {
          $query = $query->where('premium_request_id', $request->input('get'));
       }

       $query = $query->orderBy('id', 'desc')->paginate(20);

       $data = (new PremiumRequestPresenter('balance'))->present($query);

        $viewData = array(
            'boxTitle'     => 'Request Buy Balance List',
            'deleteAction' => 'admin.tag.destroy',
            'rowsRequest'  => $data['data'],
            'rowPaginate'  => $query,
            'statuso'      => array('Package Status', 'confirm', 'waiting')
        );
        return View::make('admin.contents.request.balance', $viewData);
    }

    public function filterBalanceIndex($request, $query)
    {

        if ($request->filled('q')) {
            $query = $this->searchPhoneOwner($request->input('q'), $query);
        }

        if ($request->filled('statuso')) {
            if ($request->input('statuso') == '1') $query->where('status', 1);
            else if ($request->input('statuso') == '2') $query->where('status', 0);
        }
       return $query;
    }

    public function kostOwner($id)
    {
        $owner = RoomOwner::with('room', 'room.view_promote')->where('user_id', $id)->get();
        $premiumRequest = PremiumRequest::where('user_id', $id)->where('status', '1')->orderBy('id', 'desc')->first();

        $viewData = array(
            'boxTitle'     => 'Kost List',
            'deleteAction' => '',
            'kost'         => $owner,
            'ownerId' => $id,
            'premium_request' => $premiumRequest
        );
        
        return View::make('admin.contents.request.kost', $viewData);
    }

    public function deleteRoomPromotion(Request $request, $id)
    {
        DB::beginTransaction();
        $message = "Gagal hapus kos promosi";
        if ($request->filled('owner_id')) {
            if ($request->input('owner_id') == 0) {
                $roomPromotion = ViewPromote::with('room')->where('id', $id)->first();
                if (!is_null($roomPromotion)) {
                    $roomPromotion->room->is_promoted = 'false';
                    $roomPromotion->room->save();
                    $roomPromotion->delete();
                    $message = "Berhasil hapus promosi kos";
                }
            } else {
                $premiumRequest = PremiumRequest::with(['view_promote' => function($p) use($id) {
                                              $p->where('designer_id', $id);
                                          }])->where('user_id', $request->input('owner_id'))
                                          ->where('status', '1')
                                          ->orderBy('id', 'desc')
                                          ->first();
                if (!is_null($premiumRequest) and count($premiumRequest->view_promote) > 0) {
                    $roomPromotion = $premiumRequest->view_promote()->with('room')->first();
                    $roomPromotion->room->is_promoted = 'false';
                    $roomPromotion->room->save();
                    $roomPromotion->delete();
                    $message = "Berhasil hapus promosi kos";
                }
            }
        }
        DB::commit();
        return back()->with('message', $message);
    }

    public function confirmation(Request $request, $id)
    {
        if ($request->filled('for') == PremiumPackage::BALANCE_TYPE) {
           $premiumRequest = BalanceRequest::with('premium_request', 'premium_request.user')->find($id);
           $requestType = PremiumPackage::BALANCE_TYPE;
        } else {
           $requestType = PremiumPackage::PACKAGE_TYPE;
           $premiumRequest = PremiumRequest::with('user')->find($id);
        }

        if (is_null($premiumRequest)) {
            return back()->with('message', 'Pembelian paket tidak ditemukan.');
        }

        $bank = Bank::select('id', 'name', 'account_name', 'number')->active()->get();
        $viewData = array(
            'boxTitle' => 'Confirm Premium Request',
            'request' => $premiumRequest,
            'bank' => $bank,
            'for' => $requestType,
            'formMethod'  => 'POST',
        );
        return View::make('admin.contents.request.confirmation', $viewData);
    }

    public function confirmationuser(Request $request, $requestId)
    {
        $for = $request->input('for');

        if ($for == 'balance') {
            $AccountConfirm = AccountConfirmation::where('view_balance_request_id', $requestId)->count();
        } else {
            $AccountConfirm = AccountConfirmation::where('premium_request_id', $requestId)->count();
        }

        if ($AccountConfirm > 0) { 
            return Redirect::to('admin/request')->with('message', 'Gagal, owner sudah melakukan konfirmasi');
        }
        
        if ($for == 'balance') {
            $BalanceRequest = BalanceRequest::with('premium_request', 'premium_request.user')->find($requestId);
            if (is_null($BalanceRequest)) {
                return Redirect::to('admin/request/balance')->with('message', 'Gagal, owner belum pernah topup balance');
            }

            $BalanceRequest->expired_date = null;
            $BalanceRequest->expired_status = null;
            $BalanceRequest->save();
            $userId = $BalanceRequest->premium_request->user_id;
            $PremiumRequestId = 0;
            $BalanceId = $BalanceRequest->id;
        } else { 
            $PremiumRequest = PremiumRequest::find($requestId);
            if (is_null($PremiumRequest)) {
                return Redirect::to('admin/request')->with('message', 'Gagal, owner bukan premium');
            }

            $PremiumRequest->expired_date   = null;
            $PremiumRequest->expired_status = null;
            $PremiumRequest->save();
            $userId = $PremiumRequest->user->id;
            $PremiumRequestId = $PremiumRequest->id; 
            $BalanceId = 0;
        }

        $confirm = new AccountConfirmation();
        $confirm->user_id                 = $userId;
        $confirm->premium_request_id      = $PremiumRequestId;
        $confirm->view_balance_request_id = $BalanceId;
        $confirm->bank                    = $request->input('bank_name');
        $confirm->name                    = $request->input('bank_name_account');
        $confirm->total                   = $request->input('jumlah');
        $confirm->transfer_date           = $request->input('transfer_date');
        $confirm->bank_account_id         = $request->input('bank_id');
        
        if ($PremiumRequestId > 0 && $PremiumRequest->status == PremiumRequest::PREMIUM_REQUEST_SUCCESS) {
            $isConfirm = AccountConfirmation::CONFIRMATION_SUCCESS;
        } else {
            $isConfirm = AccountConfirmation::CONFIRMATION_WAITING;
        }

        $confirm->is_confirm = $isConfirm;
        $confirm->save();

        ActivityLog::LogUpdate(Action::PREMIUM_ACCOUNT_REQUEST, $requestId);

        return  Redirect::to('admin/request')
                    ->with('message', 'Konfirmasi Berhasil, silakan ke menu Pay Confirmation untuk konfirmasi selanjutnya');
    }

    public function destroyBalance(Request $request, $id)
    {
        DB::beginTransaction();
        $balance = BalanceRequest::with('account_confirmation')->where('id', $id)->first(); 

        if (is_null($balance)) {
            return back()->with('error_message', 'Data tidak ditemukan');
        }

        if (count($balance->account_confirmation) > 0) {
            try {
              $balance->account_confirmation()->delete(); 
            } catch (Exception $e) {
              Bugsnag::notifyException($e);
            }
        }

        $balance->delete();
        
        DB::commit();

        return  Redirect::to('admin/request/balance')
                ->with('message', 'Delete Balance Request Berhasil');
    }

    public function edit($id)
    {
        $PremiumRequest = PremiumRequest::find($id);

        $viewData = array(
            'boxTitle'    => 'Edit Premium Account Request',
            'request'     => $PremiumRequest,
            'rowsActive'  => array('1', '0'),
            'formAction'  => array('admin.request.update', $id),
            'formMethod'  => 'PUT',
        );
        return View::make('admin.contents.request.form', $viewData);
    }

    public function deleted(Request $request, $id)
    {
        $premiumRequest = PremiumRequest::with('account_confirmation', 'view_promote', 'payment')
                ->where('id', $id)
                ->first();

        if (is_null($premiumRequest)) {
            return redirect()->back()->with('error_message', 'Data tidak ditemukan');
        }

        DB::beginTransaction();
        if ($request->filled('type') and $request->input('type') == 'all') {
            $deletePremiumRequest = $this->deletePremiumRequest($premiumRequest);
            $message = $deletePremiumRequest['message'];
        } else {
            
            if ($premiumRequest->status == '1') {
                return back()->with('message', 'Permintaan premium ini sudah di verifikasi');
            }

            if ($premiumRequest->account_confirmation != null) {
                $premiumRequest->account_confirmation()->delete();
            }

            if (!is_null($premiumRequest->payment)) {
                $premiumRequest->payment()->delete();
            }

            $premiumRequest->delete();

            ActivityLog::LogDelete(Action::PREMIUM_ACCOUNT_REQUEST, $id);

            $message = 'Delete Premium Request Berhasil';
        }
        DB::commit();
        return  redirect()->back()->with('message', $message);
    }

    public function deletePremiumRequest($premiumRequest)
    {
        $lastPremiumRequest = PremiumRequest::with('account_confirmation', 
                                                'view_promote',
                                                'premium_package',
                                                'user'
                                        )
                                        ->where('user_id', $premiumRequest->user_id)
                                        ->where('status', '1')
                                        ->orderBy('id', 'desc')
                                        ->first();

        if ($lastPremiumRequest->id != $premiumRequest->id) {
            return [
                "status" => False,
                "message" => "Gagal hapus premium request"
            ];
        }

        $beforeLastPremiumRequest = PremiumRequest::where('id', '<>', $lastPremiumRequest->id)
                            ->where('user_id', $premiumRequest->user_id)
                            ->orderBy('id', 'desc')
                            ->first();
                            
        if (is_null($beforeLastPremiumRequest)) {

            $this->deleteLastPremiumRequest($lastPremiumRequest);
            $lastPremiumRequest->view_promote->delete();
            $user = $lastPremiumRequest->user;
            $user->date_owner_limit = null;
            $user->save();

            return [
                "status" => true,
                "message" => "Berhasil hapus premium request",
            ];
        }

        $premiumPackage = $lastPremiumRequest->premium_package;
        if (count($lastPremiumRequest->view_promote) > 0) {
            $promoteActive = $lastPremiumRequest->view_promote->where('is_active', 1)->count();
            if ($promoteActive > 0) {
                return [
                    "status" =>  false,
                    "message" => "Gagal! Tolong matikan dulu iklan yang aktif."
                ];
            }

            if ($premiumPackage->view > 0) {
                $viewPromoteTotal = $lastPremiumRequest->view_promote->sum('total');
                $viewPromoteHistory = $lastPremiumRequest->view_promote->sum('history');
                if ($viewPromoteTotal > $beforeLastPremiumRequest->view or $viewPromoteHistory > $beforeLastPremiumRequest->view) {
                    return [
                        "status" => false,
                        "message" => "Gagal! karena owner sudah alokasi melebihi total sebelumnya yaitu ".$beforeLastPremiumRequest->view
                    ];
                }
            }

            $beforeLastPremiumRequest->used = $lastPremiumRequest->used;
            $beforeLastPremiumRequest->allocated = $lastPremiumRequest->alocated;
            $beforeLastPremiumRequest->save();
            $lastPremiumRequest->view_promote()->update(['premium_request_id' => $beforeLastPremiumRequest->id]);
            $this->deleteLastPremiumRequest($lastPremiumRequest);     
        } else {
            $this->deleteLastPremiumRequest($lastPremiumRequest);
        }

        $premiumOwnerExpired = $lastPremiumRequest->user->date_owner_limit;
        $premiumOwnerExpiredDate = Carbon::createFromFormat('Y-m-d', $premiumOwnerExpired)->addDays(-$premiumPackage->total_day);
        $user = $lastPremiumRequest->user;
        $user->date_owner_limit = $premiumOwnerExpiredDate;
        $user->save();
        
        return [
            "status" => true, 
            "message" => "Sukses hapus premium request"
        ];
    }

    public function deleteLastPremiumRequest($lastPremiumRequest)
    {
        $lastPremiumRequest->account_confirmation->delete();
        $lastPremiumRequest->status = '0';
        $lastPremiumRequest->used = 0;
        $lastPremiumRequest->allocated = 0;
        $lastPremiumRequest->expired_date = date("Y-m-d", strtotime(date("Y-m-d")." +3 days"));
        $lastPremiumRequest->expired_status = 'false';
        $lastPremiumRequest->save();
    }

    public function show(Request $request, $id)
    {
      if ($id == 'balance') {
        return $this->balance($request);
      }  
      return  Redirect::route('admin.request.index');
    }

    public function update(Request $request, $id)
    {
    	$this->validate($request, array(
            'status'=> 'required'
        ));

        $PremiumRequest   = PremiumRequest::find($id);
    	$PremiumRequest->status  = $request->input('status');
    	$isSuccess = $PremiumRequest->save();

        if ($isSuccess) {
            ActivityLog::LogUpdate(Action::PREMIUM_ACCOUNT_REQUEST, $id);
            return  Redirect::route('admin.request.index')
                ->with('message', 'Edit Status Premium Request');
      
        } else {

            return  Redirect::route('admin.request.create')
                ->with('error_message', 'Aborted')
                ->withInput();
        }

    }

    public function newRequest(Request $request)
    {
        if (!$request->filled('for') OR !in_array($request->input('for'), ["balance", "package"])) {
            return redirect()->back();
        }

        if ($request->input('for') == "package") $for = ["package", "apartment", "kos"];
        else $for = [$request->input('for')];

        $thisMonthDate = Carbon::now()->subDays(30)->format('Y-m-d');
        $package = PremiumPackage::whereIn('for', $for)
                            ->where('is_active', "1")
                            ->whereDate('sale_limit_date', '<=', date("Y-m-d"))
                            ->whereDate('sale_limit_date', '>=', $thisMonthDate)
                            ->get();

        $viewData = array(
            'boxTitle'     => 'Create Request',
            'oldpackage'      =>  $package,
            'for'          => $request->input('for')
        );

        return View::make('admin.contents.request.new', $viewData);
    }

    public function newPostRequest(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone_number' => 'required',
            'for' => 'required|in:'.PremiumPackage::BALANCE_TYPE.",".PremiumPackage::PACKAGE_TYPE
        ],[
            'phone_number.required' => 'No hp pemilik tidak boleh kosong',
            'for.required' => 'Tipe paket tidak boleh kosong'
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $user = $this->getUser($request->input('phone_number'));
        if (is_null($user)) {
            return back()->with(['error_message', 'No pemilik tidak ditemukan']);
        }

        if (
            $request->filled('oldpackage') && 
            $request->input('oldpackage') != 0
        ) {
            $packageId = $request->input('oldpackage');
        } else {
            $packageId = $request->input('package');
        }

        if ($request->input('for') === PremiumPackage::PACKAGE_TYPE) {
            $peId = null;
            if ($request->filled('pe_id')) {
                $peId = $request->input('pe_id');
            }

            $response = $this->service->premiumPackageRequest([
                'package_id' => $packageId,
                'pe_id' => $peId
            ], $user);

            $redirectPage = 'admin/request';
        } else {
            $response = $this->service->premiumBalanceRequest([
                'package_id' => $packageId
            ], $user);
            
            $redirectPage = 'admin/request/balance';
        }

        if (!$response['action']) {
            return back()->with('error_message', 'Gagal melakukan pembelian paket');
        }

        return redirect($redirectPage)->with('message', 'Pembelian paket premium berhasil');
    }

    public function getUser($phone)
    {
        $user = User::where('phone_number', $phone)
                    ->where('is_owner', User::IS_OWNER)
                    ->first();
        return $user;
    }

    public function packageList(Request $request)
    {
        $user = $this->getUser($request->input('phone'));
        $requestType = $request->input('for');

        if (is_null($user)) {
            return Api::responseData([
                "status" => false, 
                "user" => $user
            ]);
        }

        if (
            $requestType == PremiumPackage::BALANCE_TYPE
            && $user->date_owner_limit <= date('Y-m-d')
        ) {
            return Api::responseData([
                "status" => false, 
                "user" => $user
            ]); 
        }
        
        $roomRepository = new PremiumRepositoryEloquent(app());
        $package = $roomRepository->getPremiumPackageVersion($requestType, $user, ["v" => "2"]);

        return Api::responseData($package);
    }

    public function premiumExtendStatus(Request $request, $id)
    {
        $premiumRequest = PremiumRequest::where('id', $id)->first();
        if (is_null($premiumRequest)) {
            return redirect()->back()->with(['error_message' => "Data tidak ditemukan"]);
        }

        $premiumRequest->expired_status = 'false';
        $premiumRequest->expired_date = date("Y-m-d", strtotime(date("Y-m-d")."+3 days"));
        $premiumRequest->save();
        return redirect()->back()->with(['message' => "Berhasil"]);
    }

    public function allocationFix(Request $request, $id)
    {
        $premiumRequest = PremiumRequest::with('view_promote')
                        ->where('user_id', $id)
                        ->where('status', '1')
                        ->orderBy('id', 'desc')
                        ->first();

        if (is_null($premiumRequest)) {
            return back()->with('error_message', 'Data tidak ditemukan');
        }

        if (count($premiumRequest->view_promote) == 0) {
            return back()->with('error_message', 'Belum ada kos yang di promosikan');
        }

        DB::beginTransaction();
        $allocation = 0;
        $viewTotal = 0;
        foreach ($premiumRequest->view_promote as $key => $value) {
            if ($value->is_active == 1) $allocation += $value->total;
            else $allocation += $value->history;

            if ($value->is_active == 1) $viewTotal += $value->used + $value->history;
            else $viewTotal += $value->history;
        }
        $premiumRequest->used = $viewTotal;
        $premiumRequest->allocated = $allocation;
        $premiumRequest->save();
        DB::commit();
        return back()->with('message', 'Sukses');
    }

    public function requestUpdate(Request $request, $id)
    {
        $premiumRequest = PremiumRequest::where('user_id', $id)->where('id', $request->input('request_id'))->first();
        if (is_null($premiumRequest)) {
            return back()->with('message', 'Data tidak ditemukan');
        }

        $premiumRequest->view = $request->input('view');
        $premiumRequest->used = $request->input('used');
        $premiumRequest->allocated = $request->input('allocated');
        $premiumRequest->save();
        ActivityLog::LogUpdate(Action::PREMIUM_ACCOUNT_REQUEST, $id);
        return back()->with('message', 'Sukses');
    }
}
