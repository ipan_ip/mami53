<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\Entities\Refer\ReferralTracking;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;

class McaController extends Controller
{

	public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if(!Auth::user()->can('access-mca')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        \View::share('contentHeader', 'Stories Management');
        \View::share('user' , Auth::user());
    }

    public function index(Request $request)
    {
    	$tracking = ReferralTracking::with('user', 'room', 'referrer', 'referrer.user')->get();

    	$viewData = array(
            'boxTitle'     => 'MCA List',
            'deleteAction' => 'admin.referrer.destroy',
            'tracking'     => $tracking,
        );

        ActivityLog::LogIndex(Action::MCA);
        
        return View::make('admin.contents.referrer.mca', $viewData);
    }

    public function create(Request $request)
    {
    	return Redirect()->back();
    }

    public function store(Request $request)
    {
    	return Redirect()->back();
    }

    public function edit(Request $request, $id)
    {
    	return Redirect()->back();
    }

    public function verify(Request $request, $id)
    {
    	$tracking = ReferralTracking::where('id', $id)->where('reward_status', 'waiting')->first();
    	if (is_null($tracking)) return redirect()->back()->with('error_message', 'Data tidak ditemukan.');
    	$tracking->reward_status = 'live';
    	$tracking->save();
    	return redirect()->back()->with('message', 'Sukses.');
    }
}
