<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Notif\NotificationWhatsappReports;
use App\Entities\Notif\NotificationWhatsappTemplate;
use App\Http\Controllers\Controller;
use App\Http\Helpers\RegexHelper;
use App\Http\Helpers\UserEventsHelper;
use Auth;
use Bugsnag;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use RuntimeException;
use View;
use WhatsAppBusiness;

class WhatsAppNotificationController extends Controller
{
	public function __construct()
	{
		$this->middleware(function ($request, $next)
		{
			if (!Auth::user()->can('access-notification'))
			{
				return redirect('admin');
			}

			return $next($request);
		}
		);

		View::share('contentHeader', 'WhatsApp Notification');
		View::share('user', Auth::user());
	}

	/**
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function index()
	{
		$templates = NotificationWhatsappTemplate::select();

		$rowsData = $templates->paginate(20);

		foreach ($rowsData as $row)
		{
			$row->name          = strtoupper($row->name);
			$row->content       = $this->formatContent($row->content);
			$row->on_event      = (new UserEventsHelper)->getSingleEvent($row->target_user, $row->on_event);
			$row->schedule_date = !is_null($row->schedule_date) ? Carbon::createFromFormat('Y-m-d', $row->schedule_date)->format('d M Y') : "-";
			$row->schedule_time = !is_null($row->schedule_time) ? Carbon::createFromFormat('H:i:s', $row->schedule_time)->format('H:i') : "-";

			// Whatsapp Label status
			if (!$row->is_active)
			{
				$row->whatsapp_label = 'label-grey';
			}
			else
			{
				if ($row->sms_active && $row->sms_mode === 2)
				{
					$row->whatsapp_label = 'label-grey';
				}
				else
				{
					$row->whatsapp_label = 'label-success';
				}
			}

			// SMS Label status
			if (!$row->is_active)
			{
				$row->sms_label = 'label-grey';
			}
			else
			{
				if (!$row->sms_active)
				{
					$row->sms_label = 'label-grey';
				}
				else
				{
					if ($row->sms_mode === 1)
					{
						$row->sms_label = 'label-info';
					}
					else
					{
						$row->sms_label = 'label-success';
					}
				}
			}
		}

		$viewData = [
			'rowsData'  => $rowsData,
			'boxTitle'  => 'WhatsApp & SMS Notification Management',
			'searchUrl' => route('admin.wa-notification.index'),
		];

		return view('admin.contents.wa-notification.index', $viewData);
	}

	/**
	 * @param string $content
	 *
	 * @return string
	 */
	private function formatContent(string $content): string
	{
		$content = trim($content);

		// Remove double curly brackets
		$content = preg_replace_callback(RegexHelper::surroundByDoubleCurlyBrackets(), function ($words)
		{
			foreach ($words as $word)
			{
				return preg_replace('/{{2}/', '[', preg_replace('/}{2}/', ']', strtoupper($word)));
			}
		}, $content
		);

		// Replace asterisk with html <strong>
		$content = preg_replace_callback(RegexHelper::surroundByAsterisks(), function ($words)
		{
			foreach ($words as $word)
			{
				return preg_replace('/\*/', '</strong>', preg_replace('/^((?:(?:.*?\*){0}.*?))\*/', '<strong>', $word));
			}
		}, $content
		);

		// Replace underscore with html <i>
		$content = preg_replace_callback(RegexHelper::surroundByUnderscores(), function ($words)
		{
			foreach ($words as $word)
			{
				return preg_replace('/_/', '</i>', preg_replace('/^((?:(?:.*?_){0}.*?))_/', '<i>', $word));
			}
		}, $content
		);

		// Replace tilde with html <del>
		$content = preg_replace_callback(RegexHelper::surroundByTildes(), function ($words)
		{
			foreach ($words as $word)
			{
				return preg_replace('/~/', '</del>', preg_replace('/^((?:(?:.*?~){0}.*?))~/', '<del>', $word));
			}
		}, $content
		);

		// Replace triple backtick with html <code>
		$content = preg_replace_callback(RegexHelper::surroundByTripleBackticks(), function ($words)
		{
			foreach ($words as $word)
			{
				return preg_replace('/`{3}/', '</code>', preg_replace('/^((?:(?:.*?`{3}){0}.*?))`{3}/', '<code>', $word));
			}
		}, $content
		);

		return nl2br($content);
	}

	/**
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return array
	 */
	public function store(Request $request)
	{
		try
		{
			$store = NotificationWhatsappTemplate::store($request->all());

			if (isset($store['milestone']))
			{
				$message = "First notification dispatch will be on <br/><br/><span class='label label-success'>" . Carbon::createFromFormat('Y-m-d H:m:i', $store['milestone'])->format('D, d M Y @ H:m') . "</span>";
			}
			else
			{
				$message = "Page will be refreshed after you clicked OK";
			}

			return [
				'success' => true,
				'message' => $message,
			];
		}
		catch (Exception $e)
		{
			return [
				'success' => false,
				'message' => $e->getMessage(),
				'error'   => $e,
			];
		}
	}

	/**
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return array
	 */
	public function update(Request $request)
	{
		try
		{
			if (!NotificationWhatsappTemplate::update($request->all()))
			{
				new RuntimeException('Failed updating template data. Please refresh page and try again.');
			}
		}
		catch (Exception $e)
		{
			Bugsnag::notifyException($e);

			return [
				'success' => false,
				'message' => $e->getMessage(),
			];
		}

		return [
			'success' => true,
		];
	}

	/**
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return array
	 */
	public function sendTest(Request $request)
	{
		$data = $request->all();

		try
		{
			$template = NotificationWhatsappTemplate::find((int) $data['template_id']);
			$sending  = NotificationWhatsappTemplate::send($template, $data, true);

			if (!$sending)
			{
				return [
					'success' => false,
					'message' => $sending['message'],
				];
			}
		}
		catch (Exception $e)
		{
			Bugsnag::notifyException($e);

			return [
				'success' => false,
				'message' => $e->getMessage(),
				'error'   => $e,
			];
		}

		return [
			'success' => true,
			'message' => 'Testing notification sent to ' . $data['number'],
		];
	}

	/**
	 * @param \Illuminate\Http\Request $request
	 * @param int $id
	 *
	 * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
	 */
	public function getReports(Request $request, int $id)
	{
		try
		{
			$query = NotificationWhatsappReports::with('template')->where('template_id', $id);

			if (!$query->get()->count())
			{
				return back()->with('error_message', 'Invalid template ID');
			}

			// Apply Filters
			$query = $this->filter($request, $query);

			$rowsData = $query->paginate(20);

			$sortFilter = [
				'updated_at' => "By Update Date",
				'created_at' => "By Creation Date",
				'message_id' => "By Message ID",
				'status_id'  => "By Status",
			];

			$statusFilter = [
				'0' => 'All Status',
				'1' => 'Pending',
				'2' => 'Undeliverable',
				'3' => 'Delivered',
				'4' => 'Expired',
				'5' => 'Rejected',
			];

			$viewData = [
				'templateData' => NotificationWhatsappTemplate::select(['id', 'name'])->where('id', $id)->first(),
				'rowsData'     => $rowsData,
				'boxTitle'     => 'WhatsApp & SMS Notification Reports',
				'searchUrl'    => route('admin.wa-notification.reports', $id),
				'sortFilter'   => $sortFilter,
				'statusFilter' => $statusFilter,
			];

			return view('admin.contents.wa-notification.reports', $viewData);
		}
		catch (Exception $e)
		{
			Bugsnag::notifyException($e);

			return [
				'success' => false,
				'message' => $e->getMessage(),
				'error'   => $e,
			];
		}
	}

	/**
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return array
	 */
	public function getReportsSynced(Request $request)
	{
		try
		{
			$reports = NotificationWhatsappReports::with('template')
				->where('template_id', $request->template_id)
				->where('updated_at', '>=', Carbon::today()->subDays(30)->format('Y-m-d H:i:s'))
				->orderBy('updated_at', 'desc')
				->get();

			foreach ($reports as $report)
			{
				if (!$report->message_id || is_null($report->message_id))
				{
					// Just skip it and move to next report data
					continue;
				}

				NotificationWhatsappReports::getUpdatedReport($report->message_id);
			}
		}
		catch (Exception $e)
		{
			Bugsnag::notifyException($e);

			return [
				'success' => false,
				'message' => $e->getMessage(),
				'error'   => $e,
			];
		}

		return [
			'success' => true,
			'message' => "All reports successfully synced!",
		];
	}

	public function refreshReport(Request $request)
	{
		try
		{
			if (!$request->filled('id'))
			{
				return [
					'success' => false,
					'message' => "Missing ID! Please refresh page and try again.",
				];
			}

			$report = NotificationWhatsappReports::find($request->id);

			if (is_null($report))
			{
				return [
					'success' => false,
					'message' => "Failed fetching report data. Please refresh page and try again.",
				];
			}

			if (!$report->message_id || is_null($report->message_id))
			{
				return [
					'success' => false,
					'message' => "Missing Message ID. Please refresh page and try again.",
				];
			}

			if (!NotificationWhatsappReports::getUpdatedReport($report->message_id, true))
			{
				return [
					'success' => false,
					'message' => "Couldn't get report data from InfoBip. This message could be expired and/or no longer available.",
				];
			}
		}
		catch (Exception $e)
		{
			Bugsnag::notifyException($e);

			return [
				'success' => false,
				'message' => $e->getMessage(),
				'error'   => $e,
			];
		}

		return [
			'success' => true,
			'message' => "<h5>Message<br>" . $report->message_id . "<br>successfully updated!</h5>",
		];
	}

	/**
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return array
	 */
	public function enable(Request $request)
	{
		try
		{
			$activate = NotificationWhatsappTemplate::enableSms($request->all());

			if (!$activate)
			{
				new RuntimeException('Failed enabling SMS feature. Please refresh page and try again.');
			}
		}
		catch (Exception $e)
		{
			Bugsnag::notifyException($e);

			return [
				'success' => false,
				'message' => $e->getMessage(),
				'error'   => $e,
			];
		}

		return [
			'success' => true,
			'title'   => 'SMS successfully enabled',
			'message' => "Page will be refreshed after you clicked OK",
		];
	}

	/**
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return array
	 */
	public function disable(Request $request)
	{
		try
		{
			$deactivate = NotificationWhatsappTemplate::enableSms($request->all());

			if (!$deactivate)
			{
				new RuntimeException('Failed disabling SMS feature. Please refresh page and try again.');
			}
		}
		catch (Exception $e)
		{
			Bugsnag::notifyException($e);

			return [
				'success' => false,
				'message' => $e->getMessage(),
			];
		}

		return [
			'success' => true,
			'title'   => 'SMS successfully disabled',
			'message' => "Page will be refreshed after you clicked OK",
		];
	}

	/**
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return array
	 */
	public function activate(Request $request)
	{
		try
		{
			$activate = NotificationWhatsappTemplate::changeStatus($request->all());

			if (!$activate)
			{
				return [
					'success' => false,
					'message' => 'Failed activating template data. Please refresh page and try again.',
				];
			}

			/** @var string $message */
			$message = isset($activate['milestone']) ? "<h5>First notification dispatch will be on <br/><br/><span class='label label-success'>" . Carbon::createFromFormat('Y-m-d H:i:s', $activate['milestone'])->format('D, d M Y @ H:i') . "</span></h5>" : "Page will be refreshed after you clicked OK";

			return [
				'success' => true,
				'title'   => 'Template successfully activated',
				'message' => $message,
			];
		}
		catch (Exception $e)
		{
			Bugsnag::notifyException($e);

			return [
				'success' => false,
				'message' => $e->getMessage(),
				'error'   => $e,
			];
		}
	}

	/**
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return array
	 */
	public function deactivate(Request $request)
	{
		try
		{
			$deactivate = NotificationWhatsappTemplate::changeStatus($request->all());

			if (!$deactivate)
			{
				new RuntimeException('Failed deactivating template data. Please refresh page and try again.');
			}
		}
		catch (Exception $e)
		{
			Bugsnag::notifyException($e);

			return [
				'success' => false,
				'message' => $e->getMessage(),
			];
		}

		return [
			'success' => true,
			'title'   => 'Template successfully deactivated',
			'message' => "Page will be refreshed after you clicked OK",
		];
	}

	/**
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return array
	 */
	public function remove(Request $request)
	{
		try
		{
			if (!$request->filled('id'))
			{
				new RuntimeException('Failed fetching template data. Please refresh page and try again.');
			}

			if (!NotificationWhatsappTemplate::remove($request->id))
			{
				new RuntimeException('Failed deleting template data. Please refresh page and try again.');
			}
		}
		catch (Exception $e)
		{
			Bugsnag::notifyException($e);

			return [
				'success' => false,
				'message' => $e->getMessage(),
				'error'   => $e,
			];
		}

		return [
			'success' => true,
			'title'   => 'Template Data Removed!',
			'message' => "Page will be refreshed after you clicked OK",
		];
	}

	/**
	 * @return array
	 */
	public function checkScenario()
	{
		try
		{
			$check = WhatsAppBusiness::checkScenario();

			if (is_null($check))
			{
				return [
					'success' => false,
					'message' => '<h5>No SCENARIO is existing!<br/>Please contact developer!</h5>',
				];
			}

			return [
				'success' => true,
				'title'   => 'Active OMNI Scenario detected:',
				'message' => '<code>' . json_encode($check) . '</code>',
			];
		}
		catch (Exception $e)
		{
			Bugsnag::notifyException($e);

			return [
				'success' => false,
				'message' => $e->getMessage(),
				'error'   => $e,
			];
		}
	}

	/**
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return mixed
	 */
	public function getUserEvents(Request $request)
	{
		return (new UserEventsHelper)->getAllEvents($request->user_type);
	}

	/**
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return array
	 */
	public function updateSms(Request $request)
	{
		$data = $request->all();

		try
		{
			$template = NotificationWhatsappTemplate::find($data['id']);

			if (is_null($template))
			{
				return [
					'success' => false,
					'message' => "Failed fetching template data. Please refresh the page and try again.",
				];
			}

			$template->sms_content = $data['content'];
			$template->save();
		}
		catch (Exception $e)
		{
			Bugsnag::notifyException($e);

			return [
				'success' => false,
				'message' => $e->getMessage(),
				'error'   => $e,
			];
		}

		return [
			'success' => true,
		];
	}

	public function resetSms(Request $request)
	{
		$data = $request->all();

		try
		{
			$template = NotificationWhatsappTemplate::find($data['id']);

			if (is_null($template))
			{
				return [
					'success' => false,
					'message' => "Failed fetching template data. Please refresh the page and try again.",
				];
			}

			$template->sms_content = null;

			if ($template->sms_mode > 0)
			{
				// Set back to Synced mode
				$template->sms_mode = 0;
			}

			$template->save();
		}
		catch (Exception $e)
		{
			Bugsnag::notifyException($e);

			return [
				'success' => false,
				'message' => $e->getMessage(),
				'error'   => $e,
			];
		}

		return [
			'success' => true,
		];
	}

	/**
	 * @param \Illuminate\Http\Request $request
	 * @param \Illuminate\Database\Eloquent\Builder $query
	 *
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	private function filter(Request $request, Builder $query)
	{
		if ($request->filled('q') or $request->filled('owner_name'))
		{
			$query->where(function ($q) use ($request)
			{
				$q->where('message_id', 'like', '%' . $request->q . '%')
					->orWhere('to', 'like', '%' . $request->q . '%');
			});
		}

		if ($request->filled('date'))
		{
			$date = Carbon::createFromFormat('d/m/Y', $request->date)->toDateString();

			$query->whereDate('created_at', '=', $date);
		}

		if ($request->filled('sort'))
		{
			$query->orderBy($request->sort, 'desc');
		}

		if ($request->filled('status'))
		{
			$status = (int) $request->status;
			if ($status > 0)
			{
				$query->where('status_group_id', $status);
			}
		}

		return $query;
	}
}
