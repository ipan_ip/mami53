<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Promoted\Promoted;
use App\Entities\Promoted\PromotedList;
use App\Entities\Room\Room;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Redirect;
use App\Http\Helpers\ApiHelper;
use Validator;


class PromotedController extends Controller
{

    protected $indexRoute   = 'admin.promoted.index';
    protected $createRoute  = 'admin.promoted.create';
    protected $storeRoute   = 'admin.promoted.store';
    protected $editRoute    = 'admin.promoted.edit';
    protected $updateRoute  = 'admin.promoted.update';
    protected $deleteRoute  = 'admin.promoted.destroy';
    protected $publishRoute = 'admin.promoted.publish';


    protected $validationMessages = [
        'top_kost_1.required' => 'Room ID harus diisi',
        'top_kost_1.numeric' => 'Room ID harus berupa angka',
        'period_end.date' => 'Format tanggal tidak valid',
        'period_end.required_if' => 'Tanggal Akhir Periode harus diisi'
    ];

    protected $pinnedType = [
        ''          => 'Tanpa Tipe',
        'fav'       => 'Favourite',
        'review'    => 'Review'
    ];

    public function __construct()
    {

        $this->middleware(function ($request, $next) {
            if(!Auth::user()->can('access-promoted-kost')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        View::share('contentHeader', 'Stories Management');
        View::share('user' , Auth::user());
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = PromotedList::with('room', 'room.owners', 'room.owners.user');
        if(!empty($request->get('q'))) {
            $search = $request->get('q');
            $query = $query->whereHas('room', function($query) use ($search) {
                $query->where('designer.name', 'like', '%' . $search . '%');
            });
        } 

        $promotedRooms = $query->orderBy('id', 'desc')->paginate(20);

        $viewData = array(
            'boxTitle'      => 'Lists Recommendations',
            'promotedRooms'     => $promotedRooms,
        );

        ActivityLog::LogIndex(Action::KOST_PROMOTED);

        return View::make('admin.contents.promoted.index', $viewData);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $viewData = array(
            'boxTitle'       => 'Insert Promoted',
            'pinnedTypeOptions' => $this->pinnedType    
        );

        return View::make('admin.contents.promoted.create', $viewData);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),
                            [
                                'top_kost_1'=>'required|numeric',
                                'period_end'=>'nullable|required_if:pinned_type,fav,review'
                            ], $this->validationMessages);

        if($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors())->withInput();
        }

        $room = Room::find($request->top_kost_1);

        if(!$room) {
            return redirect()->back()->with('error_message', 'Kost tidak ditemukan')->withInput();
        }

        $pinnedType = $request->input('pinned_type') == '' ? null : $request->input('pinned_type');

        $promotedExist = PromotedList::where('designer_id', $request->top_kost_1)
                                    ->where('type', $pinnedType)
                                    ->first();

        if($promotedExist) {
            return redirect()->back()->with('error_message', 'Kost sudah dipromosikan')->withInput();
        }

        $promotedList = new PromotedList;
        $promotedList->designer_id = $request->top_kost_1;
        $promotedList->type = $pinnedType;
        $promotedList->period_end = $request->period_end;
        $promotedList->save();

        ActivityLog::LogCreate(Action::KOST_PROMOTED, $promotedList->id);

        return redirect()->route('admin.promoted.index')->with('message', 'Kost berhasil dipromosikan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $promotedList = PromotedList::with('room')->find($id);

        if(!$promotedList) {
            return redirect()->back()->with('error_message', 'Kost tidak ditemukan');
        }

        $viewData = array(
            'boxTitle'       => 'Edit Promoted',
            'promotedList'   => $promotedList,
            'pinnedTypeOptions' => $this->pinnedType
        );

        return View::make('admin.contents.promoted.edit', $viewData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $promotedList = PromotedList::find($id);

        if(!$promotedList) {
            return redirect()->back()->with('error_message', 'Data tidak ditemukan')->withInput();
        }

        $validator = Validator::make($request->all(),
                            [
                                'top_kost_1'=>'required|numeric',
                                'period_end'=>'nullable|required_if:pinned_type,fav,review'
                            ], $this->validationMessages);

        if($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors())->withInput();
        }

        $room = Room::find($request->top_kost_1);

        if(!$room) {
            return redirect()->back()->with('error_message', 'Kost tidak ditemukan')->withInput();
        }

        $pinnedType = $request->input('pinned_type') == '' ? null : $request->input('pinned_type');

        $promotedExist = PromotedList::where('designer_id', $request->top_kost_1)
                                    ->where('type', $pinnedType)
                                    ->where('id', '<>', $id)
                                    ->first();

        if($promotedExist) {
            return redirect()->back()->with('error_message', 'Kost sudah dipromosikan')->withInput();
        }

        $promotedList->designer_id = $request->top_kost_1;
        $promotedList->type = $pinnedType;
        $promotedList->period_end = $request->period_end;
        $promotedList->save();

        ActivityLog::LogUpdate(Action::KOST_PROMOTED, $id);
        return redirect()->route('admin.promoted.index')->with('message', 'Data berhasil dirubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $promotedList = PromotedList::find($id);

        if(!$promotedList) {
            return redirect()->back()->with('error_message', 'Data tidak ditemukan');
        }

        $promotedList->delete();

        ActivityLog::LogDelete(Action::KOST_PROMOTED, $id);

        return redirect()->back()->with('message', 'Data berhasil dihapus');
    }

    public function searchName(Request $request)
    {
        $query = $request->get('search');
        $temp = [];

        if ($query != NULL) {
            $roomWithId = Room::active()->where('id',$query)->get();

            if (count($roomWithId) < 1) {
                $roomWithName = Room::active()->where('name','LIKE','%'.$query.'%')->get();

                $temp = $roomWithName;
            } else {
                $temp = $roomWithId;
            }
        }

        $result = array();
        foreach ($temp as $key => $t) {
            $result[$key] = array(
                "id" => $t->id,
                "name" => $t->name,
            );
        }
        $response = array(
            "count_result" => count($result),
            "search_result" => $result,
        );

        return ApiHelper::responseData($response);
    }

}
