<?php

namespace App\Http\Controllers\Admin\Api\Kost;

use App\Entities\Generate\ListingReason;
use App\Entities\Generate\RejectReason;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Api\Owner\Traits\StandardResponse;
use App\Services\Room\RejectService;
use App\Transformers\Admin\KostRejectHistoryListTransformer;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class RejectController extends BaseController
{
    use StandardResponse;

    /**
     * get history reject reason kost
     *
     * @param integer $roomId
     * @param \App\Transformers\Admin\KostRejectHistoryListTransformer $transformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function getHistory(int $roomId, KostRejectHistoryListTransformer $transformer): JsonResponse
    {
        return $this->response([
            'data' => $transformer->transform(
                ListingReason::with('user')
                    ->where('listing_id', $roomId)
                    ->where('from', 'room')
                    ->get()
            )
        ]);
    }

    /**
     * reject kost
     *
     * @param integer $roomId
     * @param \Illuminate\Http\Request $request
     * @param \App\Services\Room\RejectService $service
     * @return \Illuminate\Http\JsonResponse
     */
    public function reject(
        int $roomId,
        Request $request,
        RejectService $service
    ): JsonResponse {
        $validator = Validator::make(
            $request->all(),
            [
                'reject_reason_ids'             => 'nullable|array',
                'reject_reason_ids.*'           => 'nullable|integer',
                'reject_reason_other'           => 'array',
                'reject_reason_other.*.id'      => 'required|integer|min:1',
                'reject_reason_other.*.content' => 'required|string|min:1|max:200',
            ]
        );

        try {
            $validator->validate();

            $room = Room::with(['owners', 'verification'])->findOrFail($roomId);
            $roomOwner = $room->owners->first();

            if (
                $roomOwner->status != RoomOwner::ROOM_UNVERIFY_STATUS &&
                ! in_array($roomOwner->status, RoomOwner::ROOM_STATUS_VERIFIED_OR_WAITING)
            ) {
                throw ValidationException::withMessages([
                    'id' => 'Kos tidak bisa direject.'
                ]);
            }

            [$reasonIds, $reasonOthers] = $this->parseDataFromRequest(
                (array) $request->input('reject_reason_ids'),
                (array) $request->input('reject_reason_other')
            );

            if (empty($reasonIds) && empty($reasonOthers)) {
                throw ValidationException::withMessages([
                    'reject_reason_ids' => 'Alasan ditolak harus diisi.'
                ]);
            }

            $listingReason = $service->reject(
                $room,
                $reasonIds,
                $reasonOthers
            );

            return $this->response([]);
        } catch (ValidationException $exception) {
            return $this->response([], $exception, 100422);
        } catch (ModelNotFoundException $exception) {
            return $this->response([], $exception, 100404);
        } catch (Exception $exception) {
            return $this->response([], $exception, 100400);
        }
    }

    private function parseDataFromRequest(array $reasonIds, array $reasonOthers): array
    {
        $rejectReasons = RejectReason::all();
        $rejectReasonIds = $rejectReasons->where('content', '!=', null)->pluck('id')->toArray();
        $rejectReasonOtherIds = $rejectReasons->where('content', null)->pluck('id')->toArray();

        $newReasonIds = array_intersect($reasonIds, $rejectReasonIds);
        
        $newReasonOthers = [];
        foreach ($reasonOthers as $reasonOther) {
            if (in_array($reasonOther['id'], $rejectReasonOtherIds)) {
                $newReasonOthers[$reasonOther['id']] = $reasonOther['content'];
            }
        }

        return [$newReasonIds, $newReasonOthers];
    }
}
