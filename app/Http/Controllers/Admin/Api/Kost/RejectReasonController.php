<?php

namespace App\Http\Controllers\Admin\Api\Kost;

use App\Entities\Generate\RejectReason;
use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Api\Owner\Traits\StandardResponse;
use App\Services\Room\RejectService;
use App\Transformers\Admin\RejectReasonGroupTransformer;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class RejectReasonController extends BaseController
{
    use StandardResponse;

    /**
     * get list reject reason
     *
     * @param \App\Transformers\Admin\RejectReasonGroupTransformer $transformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(RejectReasonGroupTransformer $transformer): JsonResponse
    {
        return $this->response([
            'data' => $transformer->transform(
                RejectReason::all()
            )
        ]);
    }

    /**
     * get wording preview
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Services\Room\RejectService $service
     * @return \Illuminate\Http\JsonResponse
     */
    public function preview(Request $request, RejectService $service): JsonResponse
    {
        $validator = Validator::make(
            $request->all(),
            [
                'reject_reason_ids'             => 'nullable|array',
                'reject_reason_ids.*'           => 'nullable|integer',
                'reject_reason_other'           => 'array',
                'reject_reason_other.*.id'      => 'required|integer|min:1',
                'reject_reason_other.*.content' => 'required|string|min:1|max:200',
            ]
        );

        try {
            $validator->validate();

            [$reasonIds, $reasonOthers] = $this->parseDataFromRequest(
                (array) $request->input('reject_reason_ids'),
                (array) $request->input('reject_reason_other')
            );

            if (empty($reasonIds) && empty($reasonOthers)) {
                throw ValidationException::withMessages([
                    'reject_reason_ids' => 'Alasan ditolak harus diisi.'
                ]);
            }

            return $this->response([
                'data' => ['reason' => $service->getRejectRemark($reasonIds, $reasonOthers)]
            ]);
        } catch (ValidationException $exception) {
            return $this->response([], $exception, 100422);
        } catch (Exception $exception) {
            return $this->response([], $exception, 100400);
        }
    }

    private function parseDataFromRequest(array $reasonIds, array $reasonOthers): array
    {
        $rejectReasons = RejectReason::all();
        $rejectReasonIds = $rejectReasons->where('content', '!=', null)->pluck('id')->toArray();
        $rejectReasonOtherIds = $rejectReasons->where('content', null)->pluck('id')->toArray();

        $newReasonIds = array_intersect($reasonIds, $rejectReasonIds);
        
        $newReasonOthers = [];
        foreach ($reasonOthers as $reasonOther) {
            if (in_array($reasonOther['id'], $rejectReasonOtherIds)) {
                $newReasonOthers[$reasonOther['id']] = $reasonOther['content'];
            }
        }

        return [$newReasonIds, $newReasonOthers];
    }
}
