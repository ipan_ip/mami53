<?php

namespace App\Http\Controllers\Admin\Popup;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Entities\Owner\Popup;
use App\Entities\Classes\ImageUploaded;
use File;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;

class PopupController extends Controller
{
    protected $isActive = [0,1];
    public function __construct()
    {
        View::share('contentHeader', 'Stories Management');
        View::share('user', Auth::user());
    }

    public function index(Request $request)
    {
        $popup = Popup::orderBy('id', 'desc')->paginate(20);
        $data = [
            "data" => $popup,
            "boxTitle" => "Manage popup"
        ];

        ActivityLog::LogIndex(Action::KOST_OWNER);

        return view('admin.contents.popup.index', $data);
    }

    public function create(Request $request)
    {
        $popup = new Popup();
        $popup->scheme = Input::old('scheme');
        $popup->title = Input::old('title');
        $popup->url = Input::old('url');
        $popup->content = Input::old('content');
        $popup->is_active = Input::old('is_active');

        $data = [
            "popup" => $popup,
            "boxTitle" => "Create new popup",
            "action" => "admin.popup.store",
            "method" => "POST",
            "is_active" => $this->isActive
        ];

        return view('admin.contents.popup.form', $data);
    }

    public function store(Request $request)
    {
        $popup = new popup();
        $popup->for = 'owner';
        $popup->scheme = $request->input('scheme');
        $popup->title = $request->input('title');
        $popup->url = $request->input('url');
        $popup->content = $request->input('content');
        $popup->is_active = $request->input('is_active');
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $path  = public_path('uploads/cache/data/popup');
            $image_name = (new ImageUploaded($image, $path, []))->perform();
            $popup->image = $image_name;
        }
        $popup->save();

        ActivityLog::LogCreate(Action::KOST_OWNER, $popup->id);

        return redirect("admin/owner/popup")->with(["message" => "Success upload"]);
    }

    public function edit(Request $request, $id)
    {
        $popup = Popup::where('id', $id)->first();
        if (is_null($popup)) {
            return redirect()->back()->with(["error_message" => "Data tidak ditemukan"]);
        }
        //dd(File::exists());
        $data = [
            "popup" => $popup,
            "boxTitle" => "Create new popup",
            "action" => array("admin.popup.update",$popup->id),
            "method" => "PUT",
            "image" => "/uploads/cache/data/popup/".$popup->image,
            "is_active" => $this->isActive
        ];

        return view('admin.contents.popup.form', $data); 
    }

    public function update(Request $request, $id)
    {
        $popup = Popup::where('id', $id)->first();
        if (is_null($popup)) {
            return redirect()->back()->with(["error_message" => "Data tidak ditemukan"]);
        }
        $popup->scheme = $request->input('scheme');
        $popup->title = $request->input('title');
        $popup->url = $request->input('url');
        $popup->content = $request->input('content');
        $popup->is_active = $request->input('is_active');
        if ($request->hasFile('image')) {
            if (!is_null($popup->image)) File::delete(public_path("/uploads/cache/data/popup/".$popup->image));
            $image = $request->file('image');
            $path  = public_path('uploads/cache/data/popup');
            $image_name = (new ImageUploaded($image, $path, []))->perform();
            $popup->image = $image_name;
        }
        $popup->save();

        return redirect("admin/owner/popup")->with(["message" => "Success edit"]);
    }

    public function destroy(Request $request, $id)
    {
        $popup = Popup::where('id', $id)->first();
        if (is_null($popup)) {
            return redirect()->back()->with(["error_message" => "Data tidak ditemukan"]);
        }
        if (!is_null($popup->image)) File::delete(public_path("/uploads/cache/data/popup/".$popup->image));
        $popup->delete();
        return redirect("admin/owner/popup")->with(["message" => "Success delete"]);
    }
}
