<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Auth;
use App\Http\Controllers\Admin\BaseController as Controller;
use App\Entities\Notif\Notif;
use App\Entities\Media\Media;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;
use App\User;

class NotifController extends Controller
{

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if(!Auth::user()->can('access-notification')) {
                return redirect('admin');
            }
            
            return $next($request);
        });
        
        \View::share('contentHeader', 'Stories Management');
        \View::share('user' , Auth::user());
    }

    public function index()
    {
        $rowsNotif = Notif::all();

        $viewData = array(
            'boxTitle'  => 'List Notif',
            'rowsNotif' => $rowsNotif,

            'editAction'     =>  'admin.notif.edit',
            'deleteAction'   =>  'admin.notif.destroy',
            'createAction'   =>  'admin.notif.create',
            'sendAction'     =>  'admin.notif.send.id',
        );

        ActivityLog::LogIndex(Action::NOTIFICATION);

        return view('admin.contents.notif.index', $viewData);
    }

    public function create(Request $request)
    {
        $rowNotif = new \StdClass;

        $rowNotif->id               = 0;
        $rowNotif->photo            = null;
        $rowNotif->title            = $request->old('title');
        $rowNotif->message          = $request->old('message');
        $rowNotif->scheme           = $request->old('scheme');
        $rowNotif->photo_id         = $request->old('photo_id');
        $rowNotif->user_parameter   = $request->old('user_parameter');
        $rowNotif->scheme_param     = '';

        if ($rowNotif->photo_id !== null) {
            $media = Media::find($rowNotif->photo_id);
            $rowNotif->photo = $media == null ? null : $media->getMediaUrl()['medium'];
        }

        $rowsScheme = Notif::getSchemeList();
        $rowsScheme = self::setArrayOption($rowsScheme, true);

        $userCategories = array('all' => 'Semua', 'specific_user' => 'Specific User');
        $userCategories = self::setArrayOption($userCategories, true);
        $userCategories = $this->mySelectPreprocessing('single', 'selected', 'key-value', $userCategories, '');

        $rowsUser = User::select(['id', 'name'])->take(10)->get();

        $rowsUser = $this->mySelectPreprocessing('single', 'selected', 'key-value', $rowsUser, '');

        $viewData = array(
            'boxTitle' => 'Insert Notif',
            'formType' => 'create',
            'rowNotif' => $rowNotif,
            'schemeType' => '',
            'rowsScheme' => $rowsScheme,
            'rowsUserList' => $rowsUser,
            'rowsUserCategory' => $userCategories,
            'authorType' => 'admin',
            'showUserList' => false,
            'showDesignerList' => false,
            'formAction' => url()->route('admin.notif.store'),
            'formMethod' => 'post'
        );

        return view('admin.contents.notif.temp', $viewData);
    }

    /**
     * Save notif resource
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $notif = new Notif;
        $notif->title = $request->input('title');
        $notif->message = $request->input('message');
        $notif->scheme = $request->input('scheme');
        $notif->photo_id = $request->input('photo_id');
        $notif->user_category = $request->input('user_category');

        if ($notif->scheme == 'url') {
            $notif->scheme_param = $request->input('url');
        } elseif ($notif->scheme == 'room/{id}') {
            $notif->scheme_param = $request->input('designer_id');
        } elseif ($notif->scheme = 'search_kost?params={params}') {
            $notif->scheme_param = $request->input('scheme_param_filter');
        } elseif ($notif->scheme = 'list_kost?params={params}') {
            $notif->scheme_param = $request->input('scheme_param_filter');
        }

        if ($notif->user_category == 'specific_user') {
            $notif->user_parameter = json_encode($request->input('user_ids'));
        }

        $notif->save();

        ActivityLog::LogCreate(Action::NOTIFICATION, $notif->id);

        return redirect()->route('admin.notif.index')
                         ->with('message', 'Notif Successfully Updated');
    }

    public function edit(Request $request, $notifId)
    {
        $rowNotif = Notif::find($notifId);

        if ($rowNotif->photo_id !== null) {
            $media = Media::find($rowNotif->photo_id);
            $rowNotif->photo = $media == null ? null : $media->getMediaUrl()['medium'];
        }

        $rowsScheme = Notif::getSchemeList();
        $rowsScheme = self::setArrayOption($rowsScheme, true);

        $userCategories = array('all' => 'Semua', 'specific_user' => 'Specific User');
        $userCategories = self::setArrayOption($userCategories, true);
        $userCategories = $this->mySelectPreprocessing('single', 'selected', 'key-value', $userCategories, '');


        $rowsUser = User::select(['id', 'name'])->take(10)->get();
        $rowsUser = $this->mySelectPreprocessing('single', 'selected', 'key-value', $rowsUser, '');

        $viewData = array(
            'boxTitle' => 'Insert Notif',
            'formType' => 'create',
            'rowNotif' => $rowNotif,
            'schemeType' => '',
            'rowsScheme' => $rowsScheme,
            'rowsUserList' => $rowsUser,
            'rowsUserCategory' => $userCategories,
            'authorType' => 'admin',
            'showUserList' => false,
            'showDesignerList' => false,
            'formAction' => url()->route('admin.notif.update', $rowNotif->id),
            'formMethod' => 'put'
        );

        return view('admin.contents.notif.temp', $viewData);
    }

    /**
     * Notif
     * @param  Request $request [description]
     * @param  [type]  $id      [description]
     * @return [type]           [description]
     */
    public function update(Request $request, $id)
    {
        $notif = Notif::find($id);
        $notif->title = $request->input('title');
        $notif->message = $request->input('message');
        $notif->scheme = $request->input('scheme');
        $notif->photo_id = $request->input('photo_id');
        $notif->user_category = $request->input('user_category');

        if ($notif->scheme == 'url') {
            $notif->scheme_param = $request->input('url');
        } elseif ($notif->scheme == 'room/{id}') {
            $notif->scheme_param = $request->input('designer_id');
        } elseif ($notif->scheme = 'search_kost?params={params}') {
            $notif->scheme_param = $request->input('scheme_param_filter');
        } elseif ($notif->scheme = 'list_kost?params={params}') {
            $notif->scheme_param = $request->input('scheme_param_filter');
        }

        if ($notif->user_category == 'specific_user') {
            $notif->user_parameter = json_encode($request->input('user_ids'));
        }

        $notif->save();
        ActivityLog::LogUpdate(Action::NOTIFICATION, $id);
        return redirect()->route('admin.notif.index')
                         ->with('message', 'Notif Successfully Updated');
    }

    public function destroy(Request $request, $id)
    {
        Notif::find($id)->delete();

        ActivityLog::LogDelete(Action::NOTIFICATION, $id);

        return redirect()->route('admin.notif.index')
                         ->with('message', 'Notif Successfully Deleted');
    }

    public function sendNotif($id)
    {
        return redirect()->route('admin.notif.index')->with('message', 'Deprecated');
    }
}
