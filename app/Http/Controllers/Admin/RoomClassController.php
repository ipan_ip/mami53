<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Owner\TopOwnerHistory;
use App\Entities\Room\Room;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;

class RoomClassController extends Controller
{
    protected $inputTypes = array();

    protected $htmlBuilder;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!\Auth::user()->can('access-room-class')) {
                return redirect('admin');
            }

            return $next($request);
        });

        \View::share('user', \Auth::user());
        \View::share('contentHeader', 'Room Class Management');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Room::select(['id', 'song_id', 'name', 'class', 'is_active', 'kost_updated_date'])
            ->with('tags')
            ->whereNull('apartment_project_id');

        // Apply Filters
        $query = $this->filter($request, $query);

        // Sorting
        $query->orderBy('kost_updated_date', 'desc');

        // dd($query->getQuery()->toRawSql());

        $rowsRoom = $query->paginate(20);

        foreach ($rowsRoom as $row) {
            // Remove duplicated tags
            $_tmp = array();
            foreach($row->tags as $key => $value) {
                if(!array_key_exists($value->id, $_tmp)) {
                    $_tmp[$value->id] = $value;
                }
            }
            $row->tags = array_values($_tmp);

            // Humanize updated date
            $row->kost_updated_date = Carbon::parse($row->kost_updated_date)->format('d M Y');
        }

        // dd(json_encode($rowsRoom));

        $statusFilter = [
            99 => "Tampilkan Semua Class",
            0 => "Tampilkan Kos Tanpa Class",
            1 => "Tampilkan Basic",
            11 => "Tampilkan Basic Plus",
            2 => "Tampilkan Eksklusif",
            21 => "Tampilkan Eksklusif Plus",
            22 => "Tampilkan Eksklusif Max",
            3 => "Tampilkan Residence",
            31 => "Tampilkan Residence Plus",
            32 => "Tampilkan Residence Max"
        ];

        $viewData = [
            'rowsRoom' => $rowsRoom,
            'boxTitle' => null,
            'searchUrl' => route('admin.room-class.index'),
            'statusFilter' => $statusFilter,
        ];

        ActivityLog::LogIndex(Action::KOST_CLASSES);

        return view('admin.contents.room-class.index', $viewData);
    }

    private function filter(Request $request, $query)
    {
        if ($request->filled('q')) {
            $query->where(function($q) use ($request) {
                $q->where('name', 'like', '%' . $request->input('q') . '%')->orWhere('song_id', 'like', '%' . $request->input('q') . '%');
            });
        }

        // Class Filter
        if ($request->filled('class') && $request->input('class') != 99) {
            $query->where('class', $request->input('class'));
        }

        return $query;
    }

    public function recalculateClass($roomId)
    {   
        if (isset($roomId)) {
            try {
                $room = Room::query()->where('id', $roomId)->first();
                $response = $room->updateClass();
                
                if ($response['old_class'] != $response['new_class']) {
                    $room->class = $response['new_class'];
                    $room->storeClassHistory("admin-recalculate");
                    $room->save();

                    return back()->with('message', 'Class berhasil dikalkulasi ulang. Class baru telah diterapkan.');
                } else {
                    return back()->with('message', 'Class berhasil dikalkulasi ulang. Tidak ada perubahan pada Class!');
                }
                
            } catch (Exception $e) {
                return back()->with('error', $e);
            }
        }

        return back()->with('error', "ID kos tidak berhasil ditemukan");
    }

    public function resetClass($roomId)
    {
        if (isset($roomId)) {
            try {
                $room = Room::query()->where('id', $roomId)->first();
                $room->class = 0;
                $room->storeClassHistory("admin-reset");
                $room->save();

                return back()->with('message', 'Class berhasil direset');
            } catch (Exception $e) {
                return back()->with('error', $e);
            }
        }

        return back()->with('error', "ID kos tidak berhasil ditemukan");
    }
}
