<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Premium\PremiumPackage;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Http\Helpers\ApiHelper;
use Carbon\Carbon;
use DB;

class PaketController extends Controller
{
    protected $packageType = [
        'All', 
        PremiumPackage::TRIAL_TYPE,
        PremiumPackage::BALANCE_TYPE,
        PremiumPackage::PACKAGE_TYPE
    ];

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if(!Auth::user()->can('access-premium-package')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        View::share('contentHeader', 'Stories Management');
        View::share('user', Auth::user());
    }

   
    public function index(Request $request)
    {
        $premiumPackage = PremiumPackage::query();

        if (
            $request->filled('q')
            && $request->get('q') != ''
        ) {
            $premiumPackage = $premiumPackage->where('name', 'like', '%' . $request->get('q') . '%');
        }

        if (
            $request->filled('type') 
            && $request->get('type') > 0
        ) {
            $premiumPackage = $premiumPackage->where('for', $this->packageType[$request->get('type')]);
        }

        $premiumPackage = $premiumPackage->orderBy('id', 'desc')->paginate(20);

        $viewData = array(
            'boxTitle'       => 'Account Bank List',
            'deleteAction'   => 'admin.paket.destroy',
            'PremiumPackage' => $premiumPackage,
            'packageType' => $this->packageType
        );

        ActivityLog::LogIndex(Action::PREMIUM_PACKAGE);

        return View::make('admin.contents.paket.index', $viewData);
    }

    public function create()
    {
        $PremiumPackage = new PremiumPackage;

        $PremiumPackage->name            = Input::old('name');
        $PremiumPackage->price           = Input::old('price');
        $PremiumPackage->sale_price      = Input::old('sale_price');
        $PremiumPackage->special_price   = Input::old('special_price');
        $PremiumPackage->sale_limit_date = Input::old('sale_limit_date');
        $PremiumPackage->bonus           = Input::old('bonus');
        $PremiumPackage->total_day      = Input::old('total_day');
        $PremiumPackage->is_active       = Input::old('is_active');
        $PremiumPackage->view           = Input::old('view');
        $PremiumPackage->for             = Input::old('for');
        $PremiumPackage->start_date      = Input::old('start_date');
        $PremiumPackage->feature        = Input::old('feature');

        $viewData = array(
            'boxTitle'    => 'Insert Paket Premium',
            'paket'       => $PremiumPackage,
            'rowsActive'  => array('1', '0'),
            'rowFor'      => array('balance', 'package', 'trial'),
            'formAction'  => 'admin.paket.store',
            'formMethod'  => 'POST',
        );

        return View::make('admin.contents.paket.form', $viewData);

    }

    public function store(Request $request)
    {

        $this->validate($request, array(
            'name'        => 'required',
            'price'       => 'required',
            'is_active'   => 'required'
        ));

        $dataInput = $request->input();
        /*$dataInput['views']           = $request->input('price');*/
        //$dataInput['sale_limit_date'] = (new Carbon())->parse($request->input('sale_limit_date'))->format('Y-m-d H:i:s');

        $isSuccess = PremiumPackage::create($dataInput);
        if ($isSuccess) {
            ActivityLog::LogCreate(Action::PREMIUM_PACKAGE, $isSuccess->id);

            return  Redirect::route('admin.paket.index')
                ->with('message', 'New Package Created');
      
        } else {

            return  Redirect::route('admin.paket.create')
                ->with('error_message', 'Aborted')
                ->withInput();
        }

    }

    public function edit($id)
    {
        $PremiumPackage = PremiumPackage::find($id);

        $viewData = array(
            'boxTitle'    => 'View or Edit Premium Package',
            'paket'       => $PremiumPackage,
            'rowsActive'  => array('1', '0'),
            'rowFor'      => array('balance', 'package', 'trial'),
            'formAction'  => array('admin.paket.update', $id),
            'formMethod'  => 'PUT',
        );
        return View::make('admin.contents.paket.form', $viewData);
    }

    public function update(Request $request, $id)
    {
        
        $this->validate($request, array(
            'name'        => 'required',
            'price'       => 'required',
            'is_active'   => 'required'
        ));

        $PremiumPackage   = PremiumPackage::find($id);
        
        //$sale_limit_date = (new Carbon())->parse($request->input('sale_limit_date'))->format('Y-m-d H:i:s');
    
        $PremiumPackage->name           = $request->input('name');
        $PremiumPackage->price          = $request->input('price');
        $PremiumPackage->sale_price     = $request->input('sale_price');

        $PremiumPackage->special_price  = $request->input('special_price');
        
        $PremiumPackage->sale_limit_date= $request->input('sale_limit_date');
        $PremiumPackage->bonus          = $request->input('bonus');
        $PremiumPackage->total_day     = $request->input('total_day');
        $PremiumPackage->is_active      = $request->input('is_active');
        $PremiumPackage->view          = $request->input('view');
        $PremiumPackage->for            = $request->input('for');
        $PremiumPackage->start_date     = $request->input('start_date');
        $PremiumPackage->feature       = $request->input('feature');

        $isSuccess = $PremiumPackage->save();

        if ($isSuccess) {
            ActivityLog::LogUpdate(Action::PREMIUM_PACKAGE, $id);
            return  Redirect::route('admin.paket.index')
                ->with('message', 'New Premium Package Created');
      
        } else {

            return  Redirect::route('admin.paket.create')
                ->with('error_message', 'Aborted')
                ->withInput();
        }

    }

    public function destroy($id)
    {
        PremiumPackage::destroy($id);

        return  Redirect::route('admin.paket.index')
            ->with('message', 'Premium Package deleted');
    }

    public function recommendationChange(Request $request, $id)
    {
        DB::beginTransaction();
        PremiumPackage::where('is_recommendation', 1)->update(['is_recommendation' => 0]);
        $premium = PremiumPackage::Active()->where('id', $id)->first();
        if (!is_null($premium)) {
            $premium->is_recommendation = 1;
            $premium->save();
        }
        DB::commit();
        if (is_null($premium)) {
            return redirect()->back()->with(["error_message" => "Data tidak ditemukan"]);
        }
        return redirect()->back()->with(["message" => "Berhasil"]);
    }

}
