<?php

namespace App\Http\Controllers\Admin;

use App\Entities\User\HostilityReason;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;
use App\Http\Requests\Admin\SetHostileScoreRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class HostileOwnerController extends Controller
{
    protected $inputTypes = array();

    protected $htmlBuilder;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!\Auth::user()->can('access-hostile-owner')) {
                return redirect('admin');
            }

            return $next($request);
        });

        \View::share('user', \Auth::user());
        \View::share('contentHeader', 'Hostile Owner');
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(Request $request)
    {
        $query = User::select('*')
            ->with(['room_owner_verified', 'room_owner', 'hostility_reason'])
            ->withCount('room_owner_verified as verified_room_count')
            ->withCount('room_owner as total_room_count')
            ->where('hostility', '>', 0)
            ->orderBy('hostility', 'desc')
            ->orderBy('updated_at', 'desc');

        // Apply Filters
        $query = $this->filter($request, $query);

        $rowsOwner = $query->paginate(20);

        $sortFilter = [
            'default' => "Tanpa Pengurutan",
            'report_summary_count' => "Total Report Terbanyak",
            'report_summary_within_month_count' => "Report Terbanyak Bulan Ini",
            'review_count' => "Total Review Terbanyak",
            'average_rating' => "Rating Terbaik",
        ];

        $viewData = [
            'rowsOwner' => $rowsOwner,
            'searchUrl' => route('admin.hostile-owner.index'),
            'sortFilter' => $sortFilter,
        ];

        ActivityLog::LogIndex(Action::OWNER_HOSTILE);

        return view('admin.contents.hostile-owner.index', $viewData);
    }

    private function filter(Request $request, $query)
    {
        if ($request->filled('owner_name')) {
            $ownerName = $request->input('owner_name');
            $query->where('name', 'LIKE', '%' . $ownerName . '%');
        }

        if ($request->filled('owner_phone_number')) {
            $ownerPhoneNumber = $request->input('owner_phone_number');
            $query->where('phone_number', $ownerPhoneNumber);
        }

        if ($request->filled('owner_email')) {
            $ownerEmail = $request->input('owner_email');
            $query->where('email', 'LIKE', '%' . $ownerEmail . '%');
        }

        return $query;
    }

    public function show(Request $request, $id)
    {
        $owner = User::where('is_owner', 'true')
            ->where('id', $id)
            ->first();

        if (is_null($owner)) {
            return [
                'success' => false,
                'message' => "Owner tidak ditemukan!",
            ];
        }

        if ($owner->hostility > 0) {
            return [
                'success' => false,
                'message' => "Owner ini sudah masuk di daftar Hostile Owner",
            ];
        }

        return [
            'success' => true,
            'data' => $owner,
        ];
    }

    public function store(SetHostileScoreRequest $request)
    {
        if ($request->isValidationFailed()) {
            return [
                'success' => false,
                'message' => $request->failedValidator->errors()->first()
            ];
        }

        $admin = Auth::user();

        // Synchronize hostility score to rooms
        $request->owner->synchronizeHostility($request->score);
        $request->owner->hostility_reason()->create([
            'reason' => $request->reason,
            'created_by' => $admin->id,
            'updated_by' => $admin->id
        ]);

        ActivityLog::LogCreate(Action::OWNER_HOSTILE, $request->owner->id);

        return [
            'success' => true
        ];
    }

    public function update(SetHostileScoreRequest $request)
    {
        if ($request->isValidationFailed()) {
            return [
                'success' => false,
                'message' => $request->failedValidator->errors()->first()
            ];
        }

        $admin = Auth::user();

        // Synchronize hostility score to rooms
        $request->owner->synchronizeHostility($request->score);
        $request->owner->hostility_reason()->update([
            'reason' => $request->reason,
            'updated_by' => $admin->id
        ]);

        ActivityLog::LogUpdate(Action::OWNER_HOSTILE, $request->owner->id);

        return [
            'success' => true
        ];
    }

    public function destroy($id)
    {
        $owner = User::find($id);
        $owner->synchronizeHostility(0);
        $owner->hostility_reason()->delete(); // Delete hostility score reason

        ActivityLog::LogUpdate(Action::OWNER_HOSTILE, $owner->id);

        return [
            'success' => true
        ];
    }

    public function updateHostileReason(Request $request, $id)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'reason' => 'required|string|enum_value:' . HostilityReason::class
            ]
        );

        if ($validator->fails()) {
            return [
                'success' => false,
                'message' => $validator->errors()->first()
            ];
        }

        $owner = User::where([['is_owner', '=', 'true'], ['id', '=', $id]])->first();

        if (is_null($owner)) {
            return [
                'success' => false,
                'message' => 'Owner tidak ditemukan! Coba lagi nanti'
            ];
        }

        if($owner->hostility <= 0){
            return [
                'success' => false,
                'message' => 'Hostility score owner harus lebih besar dari 0'
            ];
        }

        $admin = Auth::user();
        if (is_null($owner->hostile_reason)) {
            $owner->hostility_reason()->create(
                [
                    'reason' => $request->reason,
                    'created_by' => $admin->id,
                    'updated_by' => $admin->id
                ]
            );
        } else {
            $owner->hostility_reason()->update(
                [
                    'reason' => $request->reason,
                    'updated_by' => $admin->id
                ]
            );
        }

        return [
            'success' => true
        ];
    }
}
