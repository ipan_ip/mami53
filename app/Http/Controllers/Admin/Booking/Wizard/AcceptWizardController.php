<?php
namespace App\Http\Controllers\admin\Booking\Wizard;

use App\Entities\Room\Element\RoomUnit;
use App\Enums\Mamipay\MamipayRoomPriceComponentType;
use App\Presenters\BookingUserPresenter;
use App\Presenters\Mamipay\MamipayRoomPriceComponentPresenter;
use App\Repositories\Booking\BookingUserRepository;
use App\Repositories\Mamipay\MamipayRoomPriceComponentRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\Booking\BookingUser;
use Carbon\Carbon;
use Config;
use GuzzleHttp\Client;
use Auth;
use App\User;

class AcceptWizardController extends Controller
{
    private function getNumberSteps()
    {
        $steps = [];
        for ($x=1; $x <=4; $x++) 
        {
            $steps[]=['title'=>$x];
        }
        return $steps;
    }

    private function composeRequestHeader($method,$targetPath,$uid)
    {

        $xGitTime = Carbon::now()->timestamp;
        $authString = $method.' '.'api/v1/'.$targetPath.' '.$xGitTime;

        $key = Config::get('api.secret_key');
        $authKeyToken = hash_hmac('sha256', $authString, $key);

        $header = [
            'X-GIT-PF' => 'web',
            'Authorization' => 'GIT '.$authKeyToken.':'.base64_encode('WEB-'.$uid) ,
            'X-GIT-Time' => $xGitTime,
            'Content-Type' => 'application/json'
        ];

        return $header;
    }

    public function detailBooking(BookingUser $booking, BookingUserRepository $bookingUserRepository)
    {
        if ($booking->status !== 'booked') {
            return redirect('admin/booking/users?#booking-users');
        }

        // room price component repository
        $roomPriceComponentRepo = app()->make(MamipayRoomPriceComponentRepository::class);

        // getting room unit
        $room = optional($booking->booking_designer)->room;
        $roomUnit = $room ? $bookingUserRepository->getRoomUnitList($room) : null;
        $tranformRoomUnit = (new BookingUserPresenter('room-unit-list'))->present($roomUnit);

        $sortingRoomUnit = $this->sortingRoomLevel($tranformRoomUnit);
        if ($sortingRoomUnit !== null) {
            $tranformRoomUnit = $sortingRoomUnit;
        }

        $priceComponetList = null;
        if (optional($room)->id !== null) {
            // get data
            $priceComponentData = $roomPriceComponentRepo->getListByRoomId($room->id);
            // map data response
            $priceComponetList = $this->mapListResponseAdditionalPrice($priceComponentData);
        }

        // multiplication additional price
        $multiplicationAdditionalPrices = [
            BookingUser::DAILY_TYPE             => 1,
            BookingUser::WEEKLY_TYPE            => 1,
            BookingUser::MONTHLY_TYPE           => 1,
            BookingUser::QUARTERLY_TYPE         => 3,
            BookingUser::SEMIANNUALLY_TYPE      => 6,
            BookingUser::YEARLY_TYPE            => 12
        ];

        $multiplication = 1;
        if (optional($booking)->rent_count_type !== null) {
            $multiplication = isset($multiplicationAdditionalPrices[$booking->rent_count_type]) ? $multiplicationAdditionalPrices[$booking->rent_count_type] : 1;
        }

        $viewData=[
            'contentHeader' => 'Harga Sewa dan Tanggal Penagihan Tetap (Prorata)',
            'steps' => $this->getNumberSteps(),
            'booking' => $booking,
            'room_units' => $tranformRoomUnit['data'] ?? null,
            'room_price_component' => $priceComponetList ?? null,
            'fine_types' => [
                'day' => 'Hari',
                'week' => 'Minggu',
                'month' => 'Bulan'
            ],
            'multiplication_additional_price' => $multiplication
        ];

        return view('admin.contents.booking.wizard.user.accept_detail', $viewData);
    }

    public function acceptBooking(Request $request,BookingUser $booking)
    {
        $requestArray = $request->all();

        unset($requestArray['_token']);

        switch ($requestArray['rent_type']) 
        {
            case 'daily':
                $requestArray['rent_type'] = 'day';
                break;
            case 'weekly':
                $requestArray['rent_type'] = 'week';
                break;
            case 'monthly':
                $requestArray['rent_type'] = 'month';
                break;
            case 'quarterly':
                $requestArray['rent_type'] = '3_month';
                break;
            case 'semiannually':
                $requestArray['rent_type'] = '6_month';
                break;
            case 'yearly':
                $requestArray['rent_type'] = 'year';
                break;
            default:
                $requestArray['rent_type'] = 'month';
                break;
        }

        $requestArray['room_id'] = $booking->designer->song_id;

        $gender = $booking->user->gender ? $booking->user->gender : 'male';

        if ($gender == 'perempuan') {
            $gender = 'female';
        } else {
            $gender = 'male';
        }

        $requestArray['gender'] = $gender;

        $requestArray['occupation'] = $booking->user->job ? $booking->user->job : '-';

        $requestArray['marital_status'] = $booking->user->marital_status;

        $clearArray = array_filter($requestArray);

        $clearArray['parent_name'] = '';
        $clearArray['parent_phone_number'] = '';
        $clearArray['owner_accept'] = true;

        if (!empty($clearArray['costs'])) {
            $additionalCosts = [];

            $fixedCosts = !empty($clearArray['costs']['fixed']) ? $clearArray['costs']['fixed'] : false;
            $otherCosts = !empty($clearArray['costs']['other']) ? $clearArray['costs']['other'] : false;

            if ($fixedCosts) {
                foreach ($fixedCosts as $cost) {
                    $additionalCosts[]=array(
                        'field_title' => $cost['name'],
                        'field_value' => $cost['value'],
                        'cost_type' => 'fixed'
                    );
                }
            }

            if ($otherCosts) {
                foreach ($otherCosts as $cost) {
                    $additionalCosts[]=array(
                        'field_title' => $cost['name'],
                        'field_value' => $cost['value'],
                        'cost_type' => 'other'
                    );
                }
            }

            $clearArray['additional_costs']=$additionalCosts;
        }

        // detech room number
        if (!empty($request->designer_room_id)) {
            $roomUnit = RoomUnit::find($request->designer_room_id);
            if (!$roomUnit)
                return redirect()->back()->with('error_message', 'No kamar tidak valid.');

            $clearArray['room_number'] = $roomUnit->name;
        }

        $user = Auth::user();
        $ownerId = $booking->designer->owner_id;

        if (!$ownerId) {
            $ownerPhone = $booking->designer->owner_phone;
            $ownerByPhone = $ownerPhone ? User::where('phone_number',$ownerPhone)->where('is_owner','true')->first() : null;
            $ownerId = $ownerByPhone ? $ownerByPhone->id : $user->id;
        }

        $client = new Client([
            'base_uri' => Config::get('api.mamipay.url'),
            'headers' => $this->composeRequestHeader('POST','owner/booking/accept/'.$booking->id,$user->id)
        ]);

        $reponseCall = $client->post('owner/booking/accept/'.$booking->id,['body' => json_encode($clearArray)]);
        $responseResult = json_decode($reponseCall->getBody()->getContents());    

        if (!$responseResult->status) {
            return redirect()->back()->with('error_message', $responseResult->meta->message);
        }

        return redirect('admin/booking/users?#booking-users')->with('message','Booking berhasil diterima');
    }

    /**
     * Mapping data sorting room level with higher priority goldplus
     * @param $roomUnits
     * @return array|null
     */
    private function sortingRoomLevel($roomUnits): ?array
    {
        try {
            if (isset($roomUnits['data'])) {
                $collect = collect($roomUnits['data']);
                $gpAvailable = $collect
                    ->where('available', true)
                    ->where('is_gold_plus', true)
                    ->sortBy('level_name');

                $nonGpAvailable = $collect
                    ->where('available', true)
                    ->where('is_gold_plus', false);

                $gpDisable = $collect
                    ->where('disable', true)
                    ->where('is_gold_plus', true)
                    ->sortBy('level_name');

                $nonGpDisable = $collect
                    ->where('disable', true)
                    ->where('is_gold_plus', false);

                $return = collect()->merge($gpAvailable)
                    ->merge($nonGpAvailable)
                    ->merge($gpDisable)
                    ->merge($nonGpDisable);

                if ($return->count() > 0) {
                    return [
                        'data' => $return->toArray()
                    ];
                }

                return null;
            }

            return null;

        } catch (\Exception $e) {
            return null;
        }
    }

    private function mapListResponseAdditionalPrice($lists)
    {
        $response = [];
        try {
            foreach (MamipayRoomPriceComponentType::KEYS as $type) {
                // get data by component
                $data = $lists->where('component', $type)->where('is_active', 1);
                $tranform = (new MamipayRoomPriceComponentPresenter('data'))->present($data)['data'];
                if ($type == MamipayRoomPriceComponentType::ADDITIONAL) {
                    $response[$type] = count($tranform) > 0 ? $tranform : null;
                } else {
                    $response[$type] = isset($tranform[0]) ? $tranform[0] : null;
                }
            }
        } catch (\Exception $e) {
            $response = null;
        }
        return $response;
    }
}