<?php
namespace App\Http\Controllers\admin\Booking\Wizard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Entities\Booking\BookingOwnerRequestEnum;
use App\Entities\Room\RoomOwner;
use App\Entities\Room\BookingOwnerRequest;
use App\Entities\Booking\BookingDesigner;
use App\Http\Helpers\ApiResponse;
use App\Channel\MoEngage\MoEngageEventDataApi;
use App\Notifications\Booking\BookingOwnerRequest as BookingApprove;
use App\Entities\User\Notification as NotifOwner;
use App\Http\Controllers\Admin\MamiPay\MamipayOwnerAdminUtil;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Notification;

use DB;
use Exception;
use Illuminate\Http\JsonResponse;

class RequestWizardController extends Controller
{

    const ERR_CODE_CHECK_USER_BY_PHONE_NUMBER_NO_USER_FOUND = 1;
    const ERR_CODE_CHECK_USER_BY_PHONE_NUMBER_NOT_MAMIPAY_USER_YET = 2;
    const ERR_CODE_CHECK_USER_BY_PHONE_NUMBER_NO_PROPERTY_OWNED = 3;

    public function checkUser($userId)
    {
        $user = User::find($userId);

        if (!$user->mamipay_owner) {
            return ApiResponse::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Owner '.$user->phone_number.' belum terdaftar sebagai MamiPAY Owner'
                ]
            ]);
        }

        return ApiResponse::responseData([
            'status' => true,
            'meta' => [
                'message' => 'Owner telah terdaftar sebagai MamiPAY Owner'
            ]
        ]);
    }

    public function checkUserByPhoneNumber(string $phoneNumber): JsonResponse
    {
        $user = User::with([
            'room_owner' => function($roomOwnerQuery) {
                $roomOwnerQuery->OwnerTypeOnly();
            }
        ])->ownerOnly()->where('phone_number', $phoneNumber)->first();

        if (empty($user)) {
            return ApiResponse::responseData([
                'status' => false,
                'meta' => [
                    'message' => __('api.tenant.error.not_found'),
                ],
                'error_code' => self::ERR_CODE_CHECK_USER_BY_PHONE_NUMBER_NO_USER_FOUND
            ]);
        }

        if ($user->room_owner->isEmpty()) {
            // This owner doesn't own any property.
            return ApiResponse::responseData([
                'status' => false,
                'meta' => [
                    'message' => __('api.mamipay.owner.error.no_property_owned', ['phoneNumber' => $user->phone_number]),
                ],
                'error_code' => self::ERR_CODE_CHECK_USER_BY_PHONE_NUMBER_NO_PROPERTY_OWNED
            ]);
        }

        if (!$user->mamipay_owner) {
            return ApiResponse::responseData([
                'status' => false,
                'meta' => [
                    'message' => __('api.mamipay.owner.error.not_registered', ['phoneNumber' => $user->phone_number]),
                ],
                'error_code' => self::ERR_CODE_CHECK_USER_BY_PHONE_NUMBER_NOT_MAMIPAY_USER_YET
            ]);
        }

        return ApiResponse::responseData([
            'status' => true,
            'meta' => [
                'message' => __('api.mamipay.owner.success.registered'),
            ],
            'user_id' => $user->id,
            'error_code' => 0
        ]);
    }

    public function addMamipayOwner(Request $request)
    {
        try {
            $response = MamipayOwnerAdminUtil::addMamipayOwner($request);
            if ($response && $response->errorMsg->count()) {
                return ApiResponse::responseData([
                    'status' => false,
                    'meta' => [
                        'message' => __('api.mamipay.owner.error.add_failed'),
                    ],
                    'error_message' => $response->errorMsg,
                ]);
            } else {
                return ApiResponse::responseData([
                    'status' => true,
                    'meta' => [
                        'message' => __('api.mamipay.owner.success.add_success'),
                    ]
                ]);
            }
        } catch (Exception $e) {
            return ApiResponse::responseData([
                'status' => false,
                'meta' => [
                    'message' => __('api.mamipay.owner.error.add_unknown'),
                ]
            ]);
        };
    }

    public function checkKost(Request $request)
    {

        $kostIds = $request->input('kost_ids');
        $offset = $request->input('offset') ? $request->input('offset') : 0;
        $limit = $request->input('limit') ? $request->input('limit') : 5;
       
        $validatedRoom = [];
        $errorRoom = [];

        foreach ($kostIds as $id) {
            $roomOwner = RoomOwner::where('designer_id',$id)->first();
            
            if (!$roomOwner->room->is_active || ($roomOwner->status != 'verified')) {
                $errorRoom[$id]= !$roomOwner->room->is_active ? 'Kost is not active' : 'Kost not verified';
            }

            $price = $roomOwner->room->price();

            $validatedRoom[]=array(
                'kost_name' => $roomOwner->room->name,
                'kost_status' => isset($errorRoom[$id]) ? $errorRoom[$id] : '',
                'price' => [
                    "daily" => $price->getPrice("daily"),
                    "weekly" => $price->getPrice("weekly"),
                    "monthly" => $price->getPrice("monthly"),
                    "quarterly" => $price->getPrice("quarterly"),
                    "semiannualy" => $price->getPrice("semiannually"),
                    "yearly" => $price->getPrice("annually"),
                ],
                'kost_id' => $id
            );
        }

        $roomPaginated = array_slice($validatedRoom,$offset,$limit);

        return ApiResponse::responseData([
            'status' => empty($errorRoom) ? true : false,
            'meta' => [
                'message' => empty($errorRoom) ? '' : 'Harap perbaiki kost yang gagal terlebih dahulu sebelum melakukan request'
            ],
            'validated_rooms' => $roomPaginated,
            'active_page' => ceil($offset/$limit) + 1,
            'total_item' => count($kostIds)
        ]);
    }

    public function getRoom(Request $request, $userId)
    {
        $offset = $request->input('offset') ? $request->input('offset') : 0;
        $limit = $request->input('limit') ? $request->input('limit') : 5;
        
        $roomOwner = RoomOwner::where('user_id',$userId)->orderBy('id','desc');
        $roomCount = $roomOwner->count();
        $roomLimit = $roomOwner->offset($offset)->limit($limit)->get();
        
        if ($roomCount) {
            $roomArray = array();
            
            foreach ($roomLimit as $roomData)
            {
                $bookingRequest = $roomData->room->booking_owner_requests()->orderBy('id','desc')->first();
                
                $requestStatus = $bookingRequest ? $bookingRequest->status : false;
                
                $roomArray[]=$roomData->room()->get()->map(function ($room) use ($requestStatus, $roomData) {
                    $isRoomOwnerVerified = $roomData->status == 'verified';
                    $isBooking = true; // Will disable the checkbox by default.
                    $labelInfo = '';
                    if (!$isRoomOwnerVerified) {
                        $labelInfo = 'Kost not verified';
                    } else if ($room->is_booking) {
                        $labelInfo = 'Active';
                    } else if ($requestStatus && in_array($requestStatus,[BookingOwnerRequest::BOOKING_REJECT,BookingOwnerRequest::BOOKING_NOT_ACTIVE])) {
                        $labelInfo = 'Not Active';
                        $isBooking = false;
                    } else {
                        $isBooking = false;
                        $labelInfo = 'Requested';
                    }
                    return [
                        'id' => $room->id,
                        'name' => $room->name,
                        'label_info' => $labelInfo,
                        'is_booking' => $isBooking,
                    ];
                })->toArray()[0];
                
            }
            
            return ApiResponse::responseData([
                'status' => true,
                'rooms' => $roomArray,
                'active_page' => ceil($offset/$limit) + 1,
                'total_item' => $roomCount
            ]);

        } else{
            return ApiResponse::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Data room tidak ditemukan',
                ]
            ]);
        }

        return ApiResponse::responseData([
            'status' => true,
            'meta' => [
                'message' => 'Owner telah terdaftar sebagai MamiPAY Owner'
            ]
        ]);
    }

    public function createBookingRequest(Request $request)
    {
        $kostIds = $request->input('kost_ids');
        $userId = $request->input('user_id');

        $createdIds = [];

        try {
            DB::beginTransaction();
            foreach ($kostIds as $id) {
                $currentRequest = BookingOwnerRequest::where('user_id', $userId)->where('designer_id', $id)->first();

                if (is_null($currentRequest)) {
                    $request = new BookingOwnerRequest();
                    $request->designer_id = $id;
                    $request->user_id = $userId;
                    $request->status = BookingOwnerRequest::BOOKING_APPROVE;
                    $request->requested_by = 'admin';
                    $request->registered_by = BookingOwnerRequestEnum::ADMIN;
                    $request->save();
                    
                    $room = $request->room;

                    if ($room) {
                        $room->is_booking = true;
                        $room->updateSortScore();
                        $room->generateUniqueCode();
                        $room->save();

                        BookingDesigner::insertData($request, $room);
                    }

                    $currentRequest = $request;
                    
                    $createdIds[]=$id;
                } elseif (in_array($currentRequest->status,[BookingOwnerRequest::BOOKING_REJECT,BookingOwnerRequest::BOOKING_NOT_ACTIVE])) {
                    $currentRequest->status = BookingOwnerRequest::BOOKING_APPROVE;
                    $currentRequest->save();
                    
                    $room = $currentRequest->room;
                    
                    if ($room) {
                        $room->is_booking = true;
                        $room->updateSortScore();
                        $room->generateUniqueCode();
                        $room->save();

                        BookingDesigner::insertData($request, $room);
                    }

                    $createdIds[]=$id;
                }

                $message = "Mamikos";
                $messageDetail = "Permintaan booking sukses diterima";
                $notifType = "booking_approved";

                $moEngage = new MoEngageEventDataApi();
                $moEngage->reportBookingActivated($currentRequest);

                if (count($currentRequest->room->owners) > 0) {
                    $owner = $currentRequest->room->owners[0]->user;
                    Notification::send($owner, new BookingApprove(["message" => $message, "message_detail" => $messageDetail]));
                    NotifOwner::NotificationStore(["user_id" => $owner->id, 
                                                "designer_id" => $currentRequest->designer_id, 
                                                "type" => $notifType,
                                                "identifier" => $currentRequest->id,
                                            ]);
                }
                
            }
            DB::commit();
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
            return ApiResponse::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Terjadi kesalahan.'
                ]
            ]);
        }        

        return ApiResponse::responseData([
            'status' => true,
            'meta' => [
                'message' => 'Request dengan id '.implode(',',$createdIds).' telah dibuat.'
            ]
        ]);
    }
}