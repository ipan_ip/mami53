<?php

namespace App\Http\Controllers\Admin\Booking\RejectReason;

use App\Entities\Activity\Action;
use App\Entities\Activity\ActivityLog;
use Illuminate\Http\Request;
use App\Http\Controllers\Web\MamikosController;
use App\Repositories\Booking\RejectReason\BookingRejectReasonRepository;
use Auth;
use Validator;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use View;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class BookingRejectReasonController extends MamikosController
{
    protected $title = 'Booking Reject Reason';
    protected $repository;    
    protected $message = [
        'description.required' => 'Reject Reason tidak boleh kosong',
    ];


    public function __construct(BookingRejectReasonRepository $repository)
    {
        $this->middleware(function ($request, $next) {
            if (!Auth::user()->can('access-data-booking')) {
                return redirect('admin');
            }

            return $next($request);
        });

        $this->repository = $repository;

        View::share('contentHeader', 'Booking Management');
        View::share('user', Auth::user());
    }

    public function index(Request $request)
    {        
        $rejectReasons = $this->repository->getList($request);
        
        $viewData = [
            'boxTitle' => $this->title,
            'rejectReasons' => $rejectReasons
        ];

        return view('admin.contents.booking.reject-reason.index', $viewData);
    }

    public function create()
    {
        ActivityLog::Log(Action::BOOKING_REJECT_REASON, 'Access form create page');

        return view('admin.contents.booking.reject-reason.form');
    }

    public function store(Request $request)
    {
        try {
            $this->validateRejectReason($request);

            $result = $this->repository->create($request->all());
            
            if ($result == null) throw new CustomErrorException('Reject Reason gagal terbuat, coba lagi');
            ActivityLog::Log(Action::BOOKING_REJECT_REASON, 'Created a Reject Reason');
            return redirect('admin/booking/reject-reason')->with('message', 'Reject Reason Berhasil Dibuat');

        } catch (Exception $e) {
            Bugsnag::notifyException($e);
            return back()->with('error_message', $e->getMessage());
        }
    }

    public function edit(int $id)
    {
        try{
            $rejectReason = $this->repository->find($id);
            
            if(!$rejectReason) throw new Exception('Reject Reason Not Found');
            
            ActivityLog::Log(Action::BOOKING_REJECT_REASON, 'Access form edit page');
            $data['rejectReason'] = $rejectReason;
            return view('admin.contents.booking.reject-reason.form', $data);

        } catch (Exception $exception) {
            Bugsnag::notifyException($exception);
            return redirect()->back()->with('error_message', $exception->getMessage());
        }
    }

    public function update(Request $request, int $id)
    {
        try {
            $this->validateRejectReason($request);
            
            $rejectReason = $this->repository->update($request->all(), $id);
            
            if (!$rejectReason) throw new Exception('Reject Reason Gagal di Update');

            ActivityLog::Log(Action::BOOKING_REJECT_REASON, 'Updated a Reject Reason');

            return redirect('admin/booking/reject-reason')->with('message', 'Reject Reason Updated');

        } catch (Exception $exception) {
            Bugsnag::notifyException($exception);
            return redirect()->back()->with('error_message', $exception->getMessage());
        }
    }

    public function destroy(int $id)
    {
        try {
            $this->repository->delete($id);

            ActivityLog::Log(Action::BOOKING_REJECT_REASON, 'Delete Booking Reject Reason');

            return redirect('admin/booking/reject-reason')->with('message', 'Booking Reject Reason Success deleted');
        } catch (ModelNotFoundException $exception){
            return redirect()->back()->with('error_message', 'Booking Reject Reason ID Not Found');
        } catch (Exception $exception) {
            Bugsnag::notifyException($exception);
            return redirect()->back()->with('error_message', $exception->getMessage());
        }
    }

    public function validateRejectReason(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'is_affecting_acceptance' => 'required',
            'description' => 'required',
            'is_active' => 'required',
            'type' => 'required',
            'make_kost_not_available' => 'required',
        ], $this->message);

        if ($validator->fails()) return back()->withErrors($validator)->withInput();

        return $validator;
    }
}
