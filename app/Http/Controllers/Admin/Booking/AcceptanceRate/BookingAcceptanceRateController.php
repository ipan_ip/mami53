<?php

namespace App\Http\Controllers\Admin\Booking\AcceptanceRate;

use App\Entities\Activity\Action;
use App\Entities\Activity\ActivityLog;
use App\Http\Controllers\Web\MamikosController;
use App\Presenters\Booking\BookingAcceptanceRatePresenter;
use App\Repositories\Booking\AcceptanceRate\BookingAcceptanceRateRepository;
use App\Http\Helpers\ApiResponse as Api;
use App\Jobs\Booking\BookingAcceptanceRateCalculation;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use View;
use Auth;

class BookingAcceptanceRateController extends MamikosController
{
    protected $title = 'Booking Acceptance Rate';
    protected $repository;

    public const DEFAULT_LIMIT = 10;

    public function __construct(BookingAcceptanceRateRepository $repository)
    {
        View::share('user', Auth::user());
        View::share('contentHeader', $this->title);

        $this->repository = $repository;
    }

    public function index()
    {
        $viewData = ['boxTitle' => $this->title];
        return view('admin.contents.booking.acceptance-rate.index', $viewData);
    }

    /**
     * @return LengthAwarePaginator|Collection|mixed
     * @throws RepositoryException
     */
    public function getAllData(Request $request)
    {
        $this->repository->setPresenter(new BookingAcceptanceRatePresenter());

        $limit      = $request->get('limit', self::DEFAULT_LIMIT);
        $response   = $this->repository->with(['room.owners']);
        $count      = $response->count();
        $response   = $response->paginate($limit);

        $response['total']  = $count;
        $response['rows']   = $response['data'];

        unset($response['data']);
        return $response;
    }

    /**
     * updateStatus to update is_active bar 
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function updateStatus(int $status)
    {
        if ($status !== 1 && $status !== 0) {
            return Api::responseError('Not allowed status', 'Bad request', 400);
        }

        if ($status === 1) {
            $result = $this->repository->activate();
            ActivityLog::LogIndex(Action::BOOKING_ACCEPTANCE_ACTIVATION);
        } else {
            $result = $this->repository->deactivate();
            ActivityLog::LogIndex(Action::BOOKING_ACCEPTANCE_DEACTIVATION);
        }

        return Api::responseData([
            'affected' => $result,
            'code' => 200,
        ]);
    }

    /** 
     * Recalculate booking_acceptance_rate each BBK Kos 
     * 
     * @return JsonResponse
     */
    public function recalculate()
    {
        BookingAcceptanceRateCalculation::dispatch();
        ActivityLog::LogIndex(Action::BOOKING_ACCEPTANCE_RECALCULATE);
        return Api::responseData(['message' => 'job success submitted','code' => 200]);
    }

}
