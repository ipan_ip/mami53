<?php

namespace App\Http\Controllers\admin\Booking;

use App\Entities\Booking\BookingOwner;
use App\Entities\Notif\NotificationWhatsappTemplate;
use App\Repositories\Booking\Owner\OwnerBookingRepository;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\Room\BookingOwnerRequest;
use Illuminate\Database\Eloquent\Builder;
use View;
use Auth;
use Notification;
use App\Notifications\Booking\BookingOwnerRequest as BookingApprove;
use App\Entities\User\Notification as NotifOwner;
use App\Entities\Booking\BookingDesigner;
use DB;
use App\Entities\Room\Room;
use App\Entities\Mamipay\MamipayBankAccount;
use App\Entities\Mamipay\MamipayBankCity;
use Validator;
use App\User;
use App\Entities\Mamipay\MamipayOwner;
use App\Notifications\Booking\BookingOwnerRequestByAdmin;
use App\Entities\Booking\BookingOwnerRequestEnum;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;
use App\Channel\MoEngage\MoEngageEventDataApi;
use App\Entities\Generate\ListingReason;
use App\Entities\Level\KostLevel;
use App\Entities\Level\RoomLevel;
use App\Events\EligibleEarnPoint;
use App\Services\Room\BookingOwnerRequestService;
use App\Repositories\Level\KostLevelRepository;
use App\Repositories\Room\RoomUnitRepository;
use Carbon\Carbon;
use RuntimeException;

class OwnerBookingController extends Controller
{

    protected $message = [
        'owner_name.required' => 'Nama pemilik tidak boleh kosong',
        'owner_phone.required' => 'Nomor hp pemilik tidak boleh kosong',
        'rek_name.required' => 'Nama pemilik rekening tidak boleh kosong',
        'rek_number.required' => 'No. Rekening tidak boleh kosong',
        'bank_code.required' => 'Nama bank tidak boleh kosong',
    ];

    protected $repository;
    protected $kostLevelRepo;
    protected $roomUnitRepo;
    protected $moEngageEventDataApi;

    public function __construct(OwnerBookingRepository $repository, KostLevelRepository $kostLevelRepo, RoomUnitRepository  $roomUnitRepo, MoEngageEventDataApi $moEngageEventDataApi)
    {
        $this->middleware(function ($request, $next) {
            if (!Auth::user()->can('access-booking-payment')) {
                return redirect('admin');
            }

            return $next($request);
        });

        $this->repository = $repository;
        $this->kostLevelRepo = $kostLevelRepo;
        $this->roomUnitRepo = $roomUnitRepo;

        $this->moEngageEventDataApi = $moEngageEventDataApi;

        View::share('contentHeader', 'Booking Management');
        View::share('user', Auth::user());
    }

    public function listBookingRequest(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'from' => 'date_format:d-m-Y',
            'to' => 'date_format:d-m-Y',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        $query = BookingOwnerRequest::with([
            'user' => function ($query) {
                $query->withCount([
                    'booking_owner_requests' => function ($query) {
                        $query->where('status', BookingOwnerRequest::BOOKING_WAITING);
                    },
                    // 'booking_owner_requests AS approve_count' => function ($query) {
                    //     $query->where('status', BookingOwnerRequest::BOOKING_APPROVE);
                    // }
                ]);
            },
            'booking_room',
            'mamipay_owner_profile.bank_city',
            'verificator',
            'room' => function ($query) {
                $query->withCount('bbk_reject_reason');
            },
        ]);

        $filters = $request->only([
            'area_city',
            'room_name',
            'owner_phone',
            'from',
            'to',
            'registered_by',
            'property_status',
        ]);
        
        $query = $this->filter($query, $filters);

        $sortBy = $request->filled('sort_by') ? $request->input('sort_by') : 'request_date_desc';
        $query = $this->sort($query, $sortBy);

        // For mamipay activation form helper
        $bankList = MamipayBankAccount::where('source', 'flip')->get();

        $bookingRequest = $query->paginate(20);
        $data['boxTitle'] = "Booking request from owner";
        $data['booking_request'] = $bookingRequest;
        $data['bankList'] = $bankList;

        ActivityLog::LogIndex(Action::BOOKING_OWNER_REQUEST);

        return view('admin.contents.booking.booking_request', $data);
    }

    /**
     *  Filter query to BookingOwnerRequest by Room's area city.
     *
     *  @param Request $request
     *  @param Builder $query Query to BookingOwnerRequest
     *
     *  @return Builder
     */
    private function filterByAreaCity(Builder $query, string $areaCity): Builder
    {
        $query = $query->whereHas('room', function ($query) use ($areaCity) {
            $query->where('area_city', 'LIKE', '%' . $areaCity . '%')
                ->orWhere('area_subdistrict', 'LIKE', '%' . $areaCity . '%');
        });

        return $query;
    }

    /**
     *  Filter query to BookingOwnerRequest by Room's name
     *
     *  @param Builder $query Query to BookingOwnerRequest
     *  @param string $name Room name to query
     *
     *  @return Builder
     */
    private function filterByRoomName(Builder $query, string $name): Builder
    {
        $query = $query->whereHas('room', function ($query) use ($name) {
            $query->where('name', 'LIKE', '%' . $name . '%');
        });

        return $query;
    }

    /**
     *  Filter query to BookingOwnerRequest by Room's owner phone number
     *
     *  @param Builder $query Query to BookingOwnerRequest
     *  @param str $ownerNumber Owner number to search
     *
     *  @return Builder
     */
    private function filterByOwnerNumber(Builder $query, string $ownerNumber): Builder
    {
        $query = $query->whereHas('user', function ($query) use ($ownerNumber) {
            $query->where('phone_number', $ownerNumber);
        });

        return $query;
    }

    /**
     *  Filter query to BookingOwnerRequest by Room's status
     *
     *  @param Builder $query Query to BookingOwnerRequest
     *  @param str $status Status of Room
     */
    private function filterByPropertyStatus(Builder $query, string $status): Builder
    {
        switch ($status) {
            case 'verified':
                $query = $query->where('status', BookingOwnerRequestEnum::STATUS_APPROVE);
                break;
            case 'not_verified':
                $query = $query->where('status', BookingOwnerRequestEnum::STATUS_WAITING);
                break;
            case 'not_active':
                $query = $query->where('status', BookingOwnerRequestEnum::STATUS_NOT_ACTIVE);
                break;
            case 'reject':
                $query =  $query->where('status', BookingOwnerRequestEnum::STATUS_REJECT);
                break;
        }

        return $query;
    }

    /**
     *  Filter query to BookingOwnerRequest if filter parameter is specified in the request
     *
     *  @param Builder $query Query to BookingOwnerRequest
     *  @param array $filters Filters selected
     *
     *  @return Builder
     */
    public function filter(Builder $query, array $filters): Builder
    {
        if (!empty($filters['area_city'])) {
            $query = $this->filterByAreaCity($query, $filters['area_city']);
        }

        if (!empty($filters['room_name'])) {
            $query = $this->filterByRoomName($query, $filters['room_name']);
        }

        if (!empty($filters['owner_phone'])) {
            $phoneNumber = (string) $filters['owner_phone'];
            $query = $this->filterByOwnerNumber($query, $phoneNumber);
        }

        if (!empty($filters['from'])) {
            $from = Carbon::parse($filters['from']);

            $query->requestedAfter($from);
        }

        if (!empty($filters['to'])) {
            $to = Carbon::parse($filters['to']);

            $query->requestedBefore($to);
        }

        if (!empty($filters['registered_by']) && $filters['registered_by'] !== 'all') {
            $query->where('registered_by', $filters['registered_by']);
        }

        if (!empty($filters['property_status'])) {
            $query = $this->filterByPropertyStatus($query, $filters['property_status']);
        }

        return $query;
    }

    /**
     *  Sort $query to BookingOwnerRequest according to $request
     *
     *  @param Builder $query Query to BookingOwnerRequest
     *  @param string $by Sort request by
     *
     *  @return Builder
     */
    private function sort(Builder $query, string $by): Builder
    {
        switch ($by) {
            case 'request_date_asc':
                $query = $query->orderBy('created_at');
                break;
            case 'request_date_desc':
                $query = $query->orderBy('created_at', 'desc');
                break;
            case 'updated_date_asc':
                $query = $query->orderBy('updated_at');
                break;
            case 'updated_date_desc':
                $query = $query->orderBy('updated_at', 'desc');
                break;
            case 'registered_by_asc':
                // The long query below is due to registered_by column being an enum
                $query = $query->orderByRaw('registered_by = \'admin\' DESC')
                    ->orderByRaw('registered_by = \'consultant\' DESC')
                    ->orderByRaw('registered_by = \'system\' DESC');
                break;
            case 'registered_by_desc':
                $query = $query->orderByRaw('registered_by = \'system\' DESC')
                    ->orderByRaw('registered_by = \'consultant\' DESC')
                    ->orderByRaw('registered_by = \'admin\' DESC');
                break;
            default:
                $query = $query->orderBy('created_at', 'desc');
                break;
        }

        return $query;
    }

    public function deactivateRoom(Request $request, $id)
    {
        $bookingRequest = BookingOwnerRequest::with('room.unique_code')->where('status', 'approve')->where('id', $id)->first();
        if (is_null($bookingRequest)) {
            $message = "Request booking tidak ditemukan";
            return back()->with('message', $message);
        }

        DB::beginTransaction();
        $room = $bookingRequest->room;
        if (!is_null($room)) {
            // Remove unique code
            if (!is_null($room->unique_code)) {
                $room->unique_code->delete();
            }

            $room->is_booking = 0;
            $room->updateSortScore();
            $room->save();

            $bookingRoom = BookingDesigner::where('designer_id', $room->id)->first();
            if (!is_null($bookingRoom)) {
                $bookingRoom->deactivate();
            }
        }

        $bookingRequest->status = "not_active";
        $bookingRequest->save();
        DB::commit();

        ActivityLog::LogUpdate(Action::BOOKING_OWNER_REQUEST, $id);

        $message = "Booking tidak aktif";
        return back()->with('message', $message);
    }

    public function requestConfirm(Request $request, $id)
    {
        $message = "Data tidak ditemukan";
        /** @var BookingOwnerRequest  */
        $ownerRequest = BookingOwnerRequest::with('room', 'room.owners')->where('id', $id)->where('status', 'waiting')->first();
        if (is_null($ownerRequest)) {
            return back()->withInput(['message', $message]);
        }

        DB::beginTransaction();
        $status = ["approve", "reject"];

        if ($request->filled('status') and in_array($request->input('status'), $status)) {
            $notif = false;
            if ($request->input('status') == "approve") {
                $ownerRequest->status = "approve";
                $ownerRequest->verificator()->associate(Auth::user());
                $ownerRequest->save();

                $room = $ownerRequest->room;
                $room->is_booking = 1;
                $room->updateSortScore();
                $room->generateUniqueCode();
                $room->save();

                BookingDesigner::insertData($request, $room);

                $this->assignKostAndRoomLevelRegular($room);

                // Fire point event
                event(
                    new EligibleEarnPoint(
                        $ownerRequest->user,
                        'owner_join_bbk',
                        [
                            $ownerRequest->room->name
                        ],
                        null,
                        $ownerRequest->room->level->first()
                    )
                );

                $message = "Mamikos";
                $messageDetail = "Permintaan booking sukses diterima";
                $notif = true;
                $notifType = "booking_approved";

                $this->moEngageEventDataApi->reportBookingActivated($ownerRequest);

                // Send WA notification
                // Ref task: https://mamikos.atlassian.net/browse/KOS-12496
                $notificationData = [
                    'owner_id' => $ownerRequest->user_id,
                    'owner_name' => $ownerRequest->user->name,
                    'room_name' => $room->name,
                    'ad_link' => config('app.url') . '/room/' . $room->slug,
                ];
                NotificationWhatsappTemplate::dispatch('triggered', 'account_activated', $notificationData);

            } else if ($request->input('status') == "reject") {
                $ownerRequest->status = "reject";
                $ownerRequest->verificator()->associate(Auth::user());
                $ownerRequest->save();
                $message = "Permintaan aktifkan booking ditolak";
                $messageDetail = "Permintaan ditolak, fitur booking belum tersedia di daerah anda";
                $notif = true;
                $notifType = "booking_rejected";
            }

            if ($notif == true) {
                if (count($ownerRequest->room->owners) > 0) {
                    $user = $ownerRequest->room->owners[0]->user;
                    Notification::send($user, new BookingApprove(["message" => $message, "message_detail" => $messageDetail]));
                    NotifOwner::NotificationStore(["user_id" => $user->id,
                        "designer_id" => $ownerRequest->designer_id,
                        "type" => $notifType,
                        "identifier" => $ownerRequest->id,
                    ]);
                }

            }
        }
        DB::commit();

        ActivityLog::LogUpdate(Action::BOOKING_OWNER_REQUEST, $id);

        return back()->withInput(['message', $message]);
    }

    /**
     * Approve all request BBK
     *
     * @param \App\Services\Room\BookingOwnerRequestService $service
     * @param int $userId
     * @return View
     */
    public function requestBulkApprove(BookingOwnerRequestService $service, int $userId)
    {
        try {
            $service->bulkApprove($userId);
        } catch (Exception $e) {
            $bbkRequests = BookingOwnerRequest::with('room')
                ->where('user_id', $userId)
                ->where('status', BookingOwnerRequest::BOOKING_WAITING)
                ->get();

            Bugsnag::notifyException($e, function ($report) use ($bbkRequests) {
                $report->setSeverity('info');
                $report->setMetaData([
                    'bbk_request' => $bbkRequests
                ]);
            });

            return back()->withErrors($e->getMessage());
        }

        return back()->with('message', 'Permintaan booking sukses diterima');
    }

    /**
     * Reject request BBK
     *
     * @param \App\Services\Room\BookingOwnerRequestService $service
     * @param Request $request
     * @param int $roomId
     * @return View
     */
    public function requestReject(BookingOwnerRequestService $service, Request $request, int $roomId)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'reject_remark_text'=>'max:140'
            ],
            [
                'reject_remark_text.max'=>'Alasan ditolak maksimal :max karakter'
            ]
        );

        if ($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors())->withInput($request->all());
        }

        $rejectRemark = $request->input('reject_remark');
        if ($rejectRemark == 'textarea') {
            $rejectRemark = $request->input('reject_remark_text');
        }
        $rejectRemarkLength = strlen(trim($rejectRemark));

        $room = Room::with('owners', 'owners.user')->find($roomId);

        if ($rejectRemarkLength == 0) {
            return redirect()->back()->with('error_message', 'Alasan ditolak harus diisi');
        }

        if (is_null($room)) {
            return redirect()->back()->with('error_message', 'Kost tidak ditemukan');
        }

        if (!$room->owners->count()) {
            return redirect()->back()->with('error_message', 'Pemilik kost tidak ditemukan');
        }

        try {
            if (isset($request->action) && $request->action == 'bulk') {
                $service->bulkReject($room->owners->first()->user_id, $rejectRemark);
            } else {
                $service->singleReject($roomId, $rejectRemark);
            }
        } catch (Exception $e) {
            Bugsnag::notifyException($e, function ($report) use ($room) {
                $report->setSeverity('info');
                $report->setMetaData([
                    'room' => $room
                ]);
            });

            return back()->withErrors($e->getMessage());
        }

        return back()->with('message', 'Permintaan aktifkan booking ditolak');
    }

    protected function assignKostAndRoomLevelRegular(Room $kost)
    {
        $user = Auth::user();
        // Assign Kost Level Regular to Kost/Room
        $kostRegularLevel = KostLevel::regularLevel();
        if ($kostRegularLevel && !$kost->level->first() && !$kost->isMamirooms()) {
            $this->kostLevelRepo->changeLevel($kost, $kostRegularLevel, $user, true);
        }

        // Assign Room Level Reguler to RoomUnit
        $roomRegularLevel = RoomLevel::regularLevel()->first();
        if ($roomRegularLevel) {
            $this->roomUnitRepo->assignRoomLevelToAllUnits($kost->id, $roomRegularLevel, $user, true);
        }
    }

    public function create(Request $request)
    {
        $data['boxTitle'] = "Booking request from owner";
        $data['banks'] = MamipayBankAccount::orderBy('bank_name', 'asc')->get();
        $data['bank_cities'] = MamipayBankCity::orderBy('city_name', 'asc')->get();
        return view('admin.contents.booking.booking_request_admin', $data);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'owner_name' => 'required',
            'owner_phone' => 'required',
            'rek_name' => 'required',
            'rek_number' => 'required',
            'bank_code' => 'required'
        ], $this->message);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $user = User::with(['room_owner' => function ($p) {
            $p->where('owner_status', 'like', '%Kos%')
                ->where('status', 'verified');
        }])->where('phone_number', $request->input('owner_phone'))
            ->where('is_owner', 'true')
            ->first();

        if (is_null($user) or count($user->room_owner) < 1) {
            return back()->with('error_message', 'Owner tidak ditemukan');
        }

        DB::beginTransaction();
        foreach ($user->room_owner as $key => $value) {
            $bookingOwnerRequest = BookingOwnerRequest::where('designer_id', $value->designer_id)->first();
            if (is_null($bookingOwnerRequest) == false)
                continue;
            $bookingOwnerRequest = new BookingOwnerRequest();
            $bookingOwnerRequest->user_id = $user->id;
            $bookingOwnerRequest->designer_id = $value->designer_id;
            $bookingOwnerRequest->status = 'waiting';
            $bookingOwnerRequest->requested_by = 'admin';
            $bookingOwnerRequest->registered_by = BookingOwnerRequestEnum::ADMIN;
            $bookingOwnerRequest->save();
            ActivityLog::LogCreate(Action::BOOKING_OWNER_REQUEST, $bookingOwnerRequest->id);
        }

        $mamipayOwner = MamipayOwner::where('user_id', $user->id)->first();
        if (is_null($mamipayOwner)) {
            $mamipayOwner = new MamipayOwner();
            $mamipayOwner->user_id = $user->id;
            $mamipayOwner->status = 'approved';
            $mamipayOwner->role = 'pemilik';
            $mamipayOwner->bank_name = $request->input('bank_code');
            $mamipayOwner->bank_account_number = $request->input('rek_number');
            $mamipayOwner->bank_account_owner = $request->input('rek_name');
            $mamipayOwner->bank_account_city = $request->input('bank_city_code');
            $mamipayOwner->referral_code_agent = 'admin';
            $mamipayOwner->save();
        }

        DB::commit();

        $messageDetail = "Akun MamiPAY Anda telah aktif. Silakan hubungi CS apabila Anda tidak melakukan permintaan aktivasi ini.";
        Notification::send($user, new BookingOwnerRequestByAdmin(["message" => "Mamipay", "message_detail" => $messageDetail]));

        return redirect('admin/booking/owner/request')->with('message', 'Sukses request booking');
    }

    public function markRegisteredBy(Request $request, $id, $by)
    {
        if (!in_array($by, [BookingOwnerRequestEnum::SYSTEM, BookingOwnerRequestEnum::ADMIN, BookingOwnerRequestEnum::CONSULTANT])) {
            abort(422);
        }

        $bookingRequest = BookingOwnerRequest::findOrFail($id);
        $bookingRequest->registered_by = $by;
        $bookingRequest->save();

        return redirect('admin/booking/owner/request');
    }

    /**
     * Get list instant booking
     * URL: /admin/booking/owner/instant-booking
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function instantBooking(Request $request)
    {
        $keywords = [
            'room_name' => $request->get('room_name'),
            'phone_number' => $request->get('phone_number'),
            'created_at' => $request->get('created_at'),
            'sort_by' => $request->get('sort_by'),
        ];

        $instantBooks = $this->repository->getListInstantBooking($keywords);

        $viewData = [
            'boxTitle' => 'Instant Booking',
            'instantBooks' => $instantBooks
        ];

        return view('admin.contents.booking.instant-booking.index', $viewData);
    }

    /**
     * Create page instant booking
     * URL: /admin/booking/owner/instant-booking/create
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createInstantBooking()
    {
        ActivityLog::Log(Action::INSTANT_BOOKING_OWNER, 'Access form create page');

        return view('admin.contents.booking.instant-booking.create');
    }

    /**
     * Store instant booking
     * URL: /admin/booking/owner/instant-booking/store
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function storeInstantBooking(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'owner_id' => 'required',
                'rent_count' => 'required',
            ], $this->message);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }

            $user = User::find($request->owner_id);
            if (!$user->isOwner()) throw new Exception('User is not registered as owner');

            $isBookingAllRooms = BookingOwner::isBookingAllRoom($user);
            if (!$isBookingAllRooms) throw new Exception('Users are not registered with the booking feature');

            $additionalPrices = [];
            if (!empty($request->descriptions) && !empty($request->prices)) {
                foreach ($request->descriptions as $descKey => $description) {
                    foreach ($request->prices as $priceKey => $price) {
                        if ((isset($price) && isset($description)) && ($descKey == $priceKey)) {
                            $data['key'] = $description;
                            $data['value'] = $price;
                        }
                    }
                    $additionalPrices[] = $data;
                }
            }

            $params = [
                'rent_counts' => $request->rent_count,
                'additional_prices' => $additionalPrices,
                'is_instant_booking' => $request->status == 'true' ? true : false,
            ];

            $result = $this->repository->storeInstantBook($request->owner_id, $params);

            if (!$result['status']) throw new Exception($result['message']);

            return redirect('admin/booking/owner/instant-booking')->with('message', $result['message']);

        } catch (Exception $e) {
            return back()->with('error_message', $e->getMessage());
        }
    }

    /**
     * @param integer $id
     * @param string $status
     * @return \Illuminate\Http\RedirectResponse
     */
    public function setStatusInstantBooking(int $id,  string $status)
    {
        try {
            $bookingOwner = BookingOwner::find($id);
            if (!$bookingOwner) throw new Exception($bookingOwner['message']);

            $changeStatus = $status == 'active' ? true : false;
            $result = $this->repository->setStatus($id, $changeStatus);

            ActivityLog::LogCreate(Action::INSTANT_BOOKING_OWNER, $result['data']->id);

            return redirect('admin/booking/owner/instant-booking')->with('message', $result['message']);
        } catch (Exception $e) {
            return back()->with('error_message', $e->getMessage());
        }
    }

    /**
     * Edit page instant booking
     * URL: /admin/booking/owner/instant-booking/edit/{id}
     *
     * @param $id
     * @return mixed
     * @throws Exception
     */
    public function editInstantBooking(int $id)
    {
        try {
            $bookingOwner = $this->repository->getById($id);
            if (!$bookingOwner['status']) throw new Exception($bookingOwner['message']);

            $instantBooking = $bookingOwner['data'];

            // get rent counts rooms owner
            $rentCounts = $this->repository->getRentCountsActiveRooms($instantBooking->owner_id);

            $result = [
                'id' => $instantBooking->id,
                'owner_id' => $instantBooking->owner_id,
                'owner_name' => !empty($instantBooking->user) ? $instantBooking->user->name : null,
                'rent_counts' => !empty($instantBooking->rent_counts) ? unserialize($instantBooking->rent_counts) : null,
                'additional_prices' => !empty($instantBooking->additional_prices) ? unserialize($instantBooking->additional_prices) : null,
                'is_instant_booking' => $instantBooking->is_instant_booking,
                'rent_count_rooms' => !empty($rentCounts['data']) ? $rentCounts['data'] : null
            ];

            $data['boxTitle'] = "Instant booking owner";
            $data['instantBooking'] = $result;

            ActivityLog::Log(Action::INSTANT_BOOKING_OWNER, 'Access form edit page');

            return view('admin.contents.booking.instant-booking.edit', $data);

        } catch (Exception $exception) {
            Bugsnag::notifyException($exception);
            return redirect()->back()->with('error_message', $exception->getMessage());
        }
    }

    /**
     * Update instant booking
     * URL: /admin/booking/owner/instant-booking/update/{id}
     *
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function updateInstantBooking(Request $request, int $id)
    {
        try {
            $validator = Validator::make($request->all(), [
                'rent_counts' => 'required'
            ], $this->message);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }

            $additionalPrices = [];
            if ($request->descriptions && $request->prices) {
                foreach ($request->descriptions as $descKey => $description) {
                    foreach ($request->prices as $priceKey => $price) {
                        if ((isset($price) && isset($description)) && ($descKey == $priceKey)) {
                            $data['key'] = $description;
                            $data['value'] = $price;
                        }
                    }

                    $additionalPrices[] = $data;
                }
            }

            $params = [
                'rent_counts' => $request->rent_counts,
                'additional_prices' => $additionalPrices,
                'is_instant_booking' => $request->status == 'true' ? true : false,
            ];

            $instantBooking = $this->repository->updateInstantBook($id, $params);

            if (!$instantBooking['status']) throw new Exception($instantBooking['message']);

            return redirect('admin/booking/owner/instant-booking')->with('message', $instantBooking['message']);
        } catch (Exception $exception) {
            Bugsnag::notifyException($exception);
            return redirect()->back()->with('error_message', $exception->getMessage());
        }
    }

    public function processUploadInstantBooking(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'file' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        $file = $request->file('file');
        try {
            if (!$file->isValid()) {
                throw new Exception('File ' . $file->getClientOriginalName() . ' is invalid.');
            }

            $path = $file->getRealPath();
            $this->processInstantBookingFromCSV($path);

        } catch (Exception $exception) {
            Bugsnag::notifyException($exception);

            return redirect()->back()->with('errors', $validator->getMessageBag()->add('file', 'Failed to upload file.'));
        }

        return redirect()->back()->with('message', 'Upload file success. Will process it in background.');
    }

    public function processInstantBookingFromCSV($path)
    {
        try {
            $file = file_get_contents($path);
            $array = array_map("str_getcsv", explode("\n", $file));

            $result = [];
            foreach ($array as $item) {
                $ownerPhoneNumber = !empty($item[1]) ? $item[1] : null;
                $owner = User::where('phone_number', $ownerPhoneNumber)->where('is_owner', 'true')->first();
                if (!is_null($owner)) {
                    $tempData['owner_id'] = $owner->id;
                    $tempData['rent_counts'] = !empty($item[3]) ? (explode(",",$item[3])) : null;
                    $tempData['is_active'] = 1;
                    $tempData['is_instant_booking'] = true;

                    // Additional Prices
                    if (!empty($item[2])) {
                        $tempData['additional_prices']  = [];
                        $additionalPrices = (explode(",",$item[2]));
                        foreach ($additionalPrices as $price) {
                            $explodePrice = (explode(":",$price));
                            $temp['key']    = $explodePrice[0];
                            $temp['value']  = $explodePrice[1];

                            $tempData['additional_prices'][] = $temp;
                        }
                    } else {
                        $tempData['additional_prices'] = null;
                    }

                    // insert to booking_owner table
                    $this->repository->storeInstantBook($owner->id, $tempData);
                    $result[] = $tempData;
                }

                continue;
            }
        } catch (Exception $e) {
            $exception = new RuntimeException("failed to register owner instant booking - ".$e->getMessage());
            Bugsnag::notifyException($exception);
            return null;
        }
    }

    public function rentCountActiveRoomsOwner($ownerId)
    {
        try {
            $owner = User::find($ownerId);
            if (!$owner)  throw new Exception('User not found');
            if (!$owner->isOwner()) throw new Exception('User is not registered as owner');

            $isBookingAllRooms = BookingOwner::isBookingAllRoom($owner);
            if (!$isBookingAllRooms) throw new Exception('Owner are not registered with the booking feature');

            $rentCountsRooms = $this->repository->getRentCountsActiveRooms($ownerId);

            return response()->json([
                'status' => true,
                'message' => $rentCountsRooms['message'],
                'data' => $rentCountsRooms['data']
            ]);

        } catch (Exception $exception) {
            Bugsnag::notifyException($exception);

            return response()->json([
                'status' => false,
                'message' => $exception->getMessage()
            ]);
        }
    }

    /**
     * Get list log reject reason
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listRejectReason($id)
    {
        $room = Room::find($id);

        if (!$room) {
            return redirect()->back()->with('error_message', 'Kos tidak ditemukan');
        }

        $bbkRequest = BookingOwnerRequest::with([
            'user' => function ($query) {
                $query->withCount([
                    'booking_owner_requests' => function ($query) {
                        $query->where('status', BookingOwnerRequest::BOOKING_WAITING);
                    }
                ]);
            }
        ])
        ->where('designer_id', $id)
        ->first();

        $rejectReasonHistory = ListingReason::with('user')
            ->where('listing_id', $room->id)
            ->where('from', 'bbk')
            ->get();

        $viewData = [
            'room' => $room,
            'boxTitle' => 'BBK Reject Reason - ' . $room->name,
            'reject_reason' => $rejectReasonHistory,
            'reject_reason_select' => __('notification.admin.bbk-reject-remark'),
            'bbkRequest' => $bbkRequest,
        ];

        return view('admin.contents.booking.bbk_reject_remark', $viewData);
    }
}
