<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Entities\Refer\Referrer;
use Illuminate\Support\Facades\View;
use App\Http\Helpers\ApiHelper;
use App\User;
use DB;
use App\Libraries\SMSLibrary;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;

class ReferrerController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if(!Auth::user()->can('access-referral')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        View::share('contentHeader', 'Stories Management');
        View::share('user', Auth::user());
    }

    public function index(Request $request)
    {
    	$referrer = Referrer::with('user')->orderBy('created_at', 'desc')->get();
        $viewData = array(
            'boxTitle'     => 'Referrer List',
            'deleteAction' => 'admin.referrer.destroy',
            'referrer'      => $referrer,
        );

        ActivityLog::LogIndex(Action::REFERRAL);
        
        return View::make('admin.contents.referrer.index', $viewData);
    }

    public function create()
    {
        $referrer = new Referrer;

        $referrer->phone_number = Input::old('phone_number');
        $referrer->code    = Input::old('code');

        $code = ApiHelper::random_string('alpha', 5);

        $viewData = array(
            'boxTitle'    => 'Insert Referrer',
            'rowsActive'  => array('1', '0'),
            'formAction'  => 'admin.referrer.store',
            'formMethod'  => 'POST',
            'code'        => $code,
            'data'        => $referrer
        );

        return View::make('admin.contents.referrer.form', $viewData);
    }

    public function store(Request $request)
    {
        if (empty($request->input('phone_number')) || empty($request->input('code'))) return redirect()->back()->with('error_message', 'Semua field harus terisi.');
        
        $user = User::where('phone_number', 'LIKE', '%'.$request->input('phone_number').'%')->where('is_owner', 'false')->first();

        if (is_null($user)) return redirect()->back()->with('error_message', 'No hp tidak tersedia di Mamikos.');

        $referrer = Referrer::where('user_id', $user->id)->first();

        if (!is_null($referrer)) return redirect()->back()->with('error_message', 'User sudah memiliki kode referral.');

        DB::beginTransaction();
        $refer = new Referrer();
        $refer->user_id = $user->id;
        $refer->code    = $request->input('code');
        $refer->status  = 1;
        $refer->type    = 'mca';
        $refer->save();
        ActivityLog::LogCreate(Action::REFERRAL, $refer->id);
        DB::commit();

        if ($refer) {
            SMSLibrary::smsVerification($user->phone_number, "Mamikos: Kode referral anda adalah ".$request->input('code'));
        }

        return redirect('admin/referrer')->with('message', 'Berhasil.');
    }

    public function edit(Request $request, $id)
    {
        return redirect()->back();
    }

    public function update(Request $request)
    {
        return redirect()->back();
    }

}
