<?php

namespace App\Http\Controllers\Admin\Level;

use App\Entities\Activity\Action;
use App\Entities\Activity\ActivityLog;
use App\Entities\Level\KostLevel;
use App\Entities\Level\LevelBenefit;
use App\Entities\Level\LevelCriteria;
use App\Http\Controllers\Controller;
use App\Http\Helpers\KostLevelValueHelper;
use App\Repositories\Level\RoomLevelRepository;
use http\Exception\RuntimeException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Validation\Rule;
use Validator;

class KostLevelController extends Controller
{
    const KOST_LEVEL_INDEX_URL = 'admin/kost-level/level#kost-level';

    /**
     *  Instance of room level repository
     *
     *  @var RoomLevelRepository
     */
    protected $roomLevelRepository;

    public function __construct(RoomLevelRepository $roomLevelRepository)
    {
        $this->middleware(function ($request, $next) {
            if (!Auth::user()->can('access-kost-level')) {
                return redirect('admin');
            }

            return $next($request);
        });

        $this->roomLevelRepository = $roomLevelRepository;

        View::share('contentHeader', 'Kost Level Management');
        View::share('user', Auth::user());
    }

    public function index(Request $request)
    {
        $query = KostLevel::with(['benefits', 'criterias', 'values', 'room_level']);

        if ($request->filled('q')) {
            $query->where('name', 'like', '%' . $request->input('q') . '%');
        }

        $level = $query->orderBy('order', 'asc')->paginate(20);

        ActivityLog::LogIndex(Action::KOST_LEVEL);

        $viewData = [
            'level' => $level,
            'boxTitle' => 'Manage Kost Level'
        ];
        return view('admin.contents.kost-level.level.index', $viewData);
    }

    public function show(Request $request, KostLevel $level)
    {
        abort(404);
    }

    public function create()
    {
        $isRegularAvail = KostLevel::canMakeRegularLevel();
        $isPartnerAvail = KostLevel::canMakePartnerLevel();
        $order = KostLevel::getAvailableOrder();

        $viewData = [
            'order' => $order,
            'isRegularAvail' => $isRegularAvail,
            'isPartnerAvail' => $isPartnerAvail,
            'boxTitle' => 'Create Kost Level',
            'roomLevels' => $this->roomLevelRepository->select('id', 'name')->doesntHave('kost_level')->get()
        ];
        return view('admin.contents.kost-level.level.create', $viewData);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:100',
            'order' => 'required',
            'is_regular' => 'required',
            'is_hidden' => 'required',
            'is_partner' => 'required',
            'has_value' => 'required',
            'join_count' => 'present|numeric|min:0',
            'charging_fee' => 'present|numeric|min:0',
            'charging_type' => 'required|in:percentage,amount',
            'charge_for_booking' => 'nullable|boolean',
            'charge_for_consultant' => 'nullable|boolean',
            'charge_for_owner' => 'nullable|boolean',
            'charge_invoice_type' => 'required|in:all,per_contract,per_tenant',
            'room_level_id' => ['required', 'integer', 'exists:room_level,id'],
            'notes' => 'max:255',
            'key' => [
                'nullable',
                'max:64',
                'alpha_dash',
                Rule::unique('kost_level', 'key')->whereNull('deleted_at')
            ],
        ]);

        if ($validator->fails()) {
            return response()->json(
                [
                    'success' => false,
                    'message' => $validator->errors()
                ]
            );
        }

        if ($this->failedAdditionalValidation($validator, $request->all())) {
            return response()->json(
                [
                    'success' => false,
                    'message' => $validator->errors()
                ]
            );
        }

        if (
            $request->filled('has_value')
            && $request->has_value == '1'
        ) {
            if (empty($request->level_values)) {
                return response()->json(
                    [
                        'success' => false,
                        'message' => "Please select Level Values!"
                    ]
                );
            }
        }

        try {
            $level = new KostLevel();
            $level->name = $request->input('name');
            $level->key = !empty($request->input('key')) ? $request->input('key') : null;
            $level->order = $request->input('order');
            $level->is_regular = $request->input('is_regular');
            $level->is_hidden = $request->input('is_hidden');
            $level->is_partner = $request->input('is_partner');
            $level->has_value = $request->input('has_value');
            $level->notes = $request->input('notes');
            $level->tnc = $request->input('tnc');
            $level->join_count = $request->input('join_count');
            $level->charging_fee = $request->input('charging_fee');
            $level->charging_type = $request->input('charging_type');
            $level->charge_for_booking = $request->input('charge_for_booking') ? true : false;
            $level->charge_for_consultant = $request->input('charge_for_consultant') ? true : false;
            $level->charge_for_owner = $request->input('charge_for_owner') ? true : false;
            $level->charge_invoice_type = $request->input('charge_invoice_type');
            $level->save();

            $roomLevel = $this->roomLevelRepository->find($request->room_level_id);
            $roomLevel->kost_level_id = $level->id;
            $roomLevel->save();

            $benefits = $request->input('benefits');
            $criterias = $request->input('criterias');
            $this->handleCreateBenefit($benefits, $level);
            $this->handleCreateCriteria($criterias, $level);

            /*
             * Store "Kost_level_values" assignment
             *
             * Ref task: https://mamikos.atlassian.net/browse/BG-2997
             */
            if ($request->has_value == '1') {
                $helper = new KostLevelValueHelper();
                if (!$helper->assignValuesToKostLevel($level, $request->level_values)) {
                    throw new RuntimeException('Failed to assign Kost Level values');
                }
            }

            ActivityLog::LogCreate(Action::KOST_LEVEL, $level->id);
        } catch (\Exception $exception) {
            return response()->json(
                [
                    'success' => false,
                    'message' => $exception->getMessage()
                ]
            );
        }

        return response()->json(
            [
                'success' => true,
                'message' => "Level successfully added."
            ]
        );
    }

    private function handleCreateBenefit($benefits, KostLevel $level)
    {
        foreach ($benefits as $inputBenefit) {
            $inputBenefit = trim($inputBenefit);
            if (empty($inputBenefit)) {
                continue;
            }
            $benefit = new LevelBenefit();
            $benefit->level_id = $level->id;
            $benefit->text = $inputBenefit;
            $benefit->save();
        }
    }

    private function handleCreateCriteria($criterias, KostLevel $level)
    {
        foreach ($criterias as $inputCriteria) {
            $inputCriteria = trim($inputCriteria);
            if (empty($inputCriteria)) {
                continue;
            }
            $criteria = new LevelCriteria();
            $criteria->level_id = $level->id;
            $criteria->text = $inputCriteria;
            $criteria->save();
        }
    }

    public function edit(KostLevel $level)
    {
        $isRegularAvail = KostLevel::canMakeRegularLevel();
        $isPartnerAvail = KostLevel::canMakePartnerLevel();
        $order = KostLevel::getAvailableOrder();

        $level->loadMissing(
            [
                'room_level',
                'values'
            ]
        );

        $order[] = $level->order;
        sort($order);

        $viewData = [
            'level' => $level,
            'order' => $order,
            'isRegularAvail' => $isRegularAvail,
            'isPartnerAvail' => $isPartnerAvail,
            'boxTitle' => 'Edit Kost Level',
            'roomLevels' => $this->roomLevelRepository->select('id', 'name')
                ->where(function ($query) use ($level) {
                    $query->where('kost_level_id', $level->id)
                        ->orDoesntHave('kost_level');
                })
                ->get()
        ];
        return view('admin.contents.kost-level.level.edit', $viewData);
    }

    public function update(Request $request, KostLevel $level)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:100',
            'order' => 'required',
            'is_regular' => 'required',
            'is_hidden' => 'required',
            'is_partner' => 'required',
            'has_value' => 'required',
            'join_count' => 'present|numeric|min:0',
            'charging_fee' => 'present|numeric|min:0',
            'charging_type' => 'required|in:percentage,amount',
            'charge_for_booking' => 'nullable|boolean',
            'charge_for_consultant' => 'nullable|boolean',
            'charge_for_owner' => 'nullable|boolean',
            'charge_invoice_type' => 'required|in:all,per_contract,per_tenant',
            'notes' => 'max:255',
            'room_level_id' => ['required', 'integer', 'exists:room_level,id'],
            'key' => [
                'nullable',
                'max:64',
                'alpha_dash',
                Rule::unique('kost_level', 'key')->ignore($level->id, 'id')->whereNull('deleted_at')
            ],
        ]);

        if ($validator->fails()) {
            return response()->json(
                [
                    'success' => false,
                    'message' => $validator->errors()
                ]
            );
        }

        if ($this->failedAdditionalValidation($validator, $request->all())) {
            return response()->json(
                [
                    'success' => false,
                    'message' => $validator->errors()
                ]
            );
        }

        if (
            $request->filled('has_value')
            && $request->has_value == '1'
        ) {
            if (empty($request->level_values)) {
                return response()->json(
                    [
                        'success' => false,
                        'message' => "Please select Level Values!"
                    ]
                );
            }
        }

        try {
            $level->name = $request->input('name');
            $level->key = !empty($request->input('key')) ? $request->input('key') : null;
            $level->order = $request->input('order');
            $level->is_regular = $request->input('is_regular');
            $level->is_hidden = $request->input('is_hidden');
            $level->is_partner = $request->input('is_partner');
            $level->has_value = $request->input('has_value');
            $level->notes = $request->input('notes');
            $level->tnc = $request->input('tnc');
            $level->join_count = $request->input('join_count');
            $level->charging_fee = $request->input('charging_fee');
            $level->charging_type = $request->input('charging_type');
            $level->charge_for_booking = $request->input('charge_for_booking') ? true : false;
            $level->charge_for_consultant = $request->input('charge_for_consultant') ? true : false;
            $level->charge_for_owner = $request->input('charge_for_owner') ? true : false;
            $level->charge_invoice_type = $request->input('charge_invoice_type');
            $level->save();

            // Set old room level back
            $level->load('room_level');
            $level->room_level()->update(
                [
                    'kost_level_id' => null
                ]
            );

            $roomLevel = $this->roomLevelRepository->find($request->room_level_id);
            $roomLevel->kost_level_id = $level->id;
            $roomLevel->save();

            $benefits = $request->input('benefits');
            $benefitIds = $request->input('benefit_id');
            $criterias = $request->input('criterias');
            $criteriaIds = $request->input('criteria_id');
            $benefits = $this->handleUpdateBenefit($benefitIds, $benefits);
            $this->handleDeleteBenefit($benefitIds, $level);
            $this->handleCreateBenefit($benefits, $level);
            $criterias = $this->handleUpdateCriteria($criteriaIds, $criterias);
            $this->handleDeleteCriteria($criteriaIds, $level);
            $this->handleCreateCriteria($criterias, $level);

            /*
             * Sync "Kost_level_values" assignment
             *
             * Ref task: https://mamikos.atlassian.net/browse/BG-2997
             */
            $levelValues = $request->filled('level_values') ? $request->level_values : [];
            $helper = new KostLevelValueHelper();
            if (!$helper->assignValuesToKostLevel($level, $levelValues)) {
                throw new RuntimeException('Failed to assign Kost Level values');
            }

            ActivityLog::LogUpdate(Action::KOST_LEVEL, $level->id);
        } catch (\Exception $exception) {
            return response()->json(
                [
                    'success' => false,
                    'message' => $exception->getMessage()
                ]
            );
        }

        return response()->json(
            [
                'success' => true,
                'message' => "Level successfully updated."
            ]
        );
    }

    private function failedAdditionalValidation(&$validator, array $data)
    {
        if ($data['charging_type'] == 'percentage') {
            if ($data['charging_fee'] > 100) {
                $validator->errors()->add('charging_fee', 'The charging fee cannot be greater than 100');
                return true;
            }
        }

        return false;
    }

    private function handleUpdateBenefit($benefitIds, $benefits)
    {
        if (isset($benefitIds)) {
            $benefitCounter = 0;
            foreach ($benefitIds as $benefitId) {
                if (empty($benefitId)) {
                    $benefitCounter++;
                    continue;
                }
                $benefit = LevelBenefit::find($benefitId);
                $inputBenefit = trim($benefits[$benefitCounter]);
                unset($benefits[$benefitCounter]);
                if (empty($inputBenefit)) {
                    $benefit->delete();
                    $benefitCounter++;
                    continue;
                }
                $benefit->text = $inputBenefit;
                $benefit->save();
                $benefitCounter++;
            }
            return $benefits;
        } else {
            return [];
        }
    }

    private function handleDeleteBenefit($benefitIds, KostLevel $level)
    {
        if (isset($benefitIds)) {
            $dbIds = LevelBenefit::getIdsOfLevel($level);
            $benefitIds = array_filter($benefitIds);
            $idToDelete = array_diff($dbIds, $benefitIds);
            foreach ($idToDelete as $benefitId) {
                $benefit = LevelBenefit::find($benefitId);
                $benefit->delete();
            }
        }
    }

    private function handleUpdateCriteria($criteriaIds, $criterias)
    {
        if (isset($benefitIds)) {
            $criteriaCounter = 0;
            foreach ($criteriaIds as $criteriaId) {
                if (empty($criteriaId)) {
                    $criteriaCounter++;
                    continue;
                }
                $criteria = LevelCriteria::find($criteriaId);
                $inputCriteria = trim($criterias[$criteriaCounter]);
                unset($criterias[$criteriaCounter]);
                if (empty($inputCriteria)) {
                    $criteria->delete();
                    $criteriaCounter++;
                    continue;
                }
                $criteria->text = $inputCriteria;
                $criteria->save();
                $criteriaCounter++;
            }
            return $criterias;
        } else {
            return [];
        }
    }

    private function handleDeleteCriteria($criteriaIds, KostLevel $level)
    {
        if (isset($benefitIds)) {
            $dbIds = LevelCriteria::getIdsOfLevel($level);
            $criteriaIds = array_filter($criteriaIds);
            $idToDelete = array_diff($dbIds, $criteriaIds);
            foreach ($idToDelete as $criteriaId) {
                $criteria = LevelCriteria::find($criteriaId);
                $criteria->delete();
            }
        }
    }

    public function destroy($id)
    {
        $level = KostLevel::find($id);
        if (is_null($level)) {
            abort(404);
        }

        $level->key = null;
        $level->order = 0;
        $level->save();
        $level->delete();
        $level->benefits()->delete();
        $level->criterias()->delete();

        ActivityLog::LogDelete(Action::KOST_LEVEL, $id);

        return redirect()->to(self::KOST_LEVEL_INDEX_URL)->with('message', 'Level successfully deleted.');
    }
}
