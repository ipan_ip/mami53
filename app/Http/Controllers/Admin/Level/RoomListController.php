<?php

namespace App\Http\Controllers\Admin\Level;

use App\Entities\Activity\Action;
use App\Entities\Activity\ActivityLog;
use App\Entities\Level\RoomLevel;
use App\Entities\Level\RoomLevelHistory;
use App\Entities\Room\Room;
use App\Entities\Room\Element\RoomUnit;
use App\Http\Controllers\Controller;
use App\Repositories\Room\RoomUnitRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Validator;

class RoomListController extends Controller
{
    private const KOST_LIST_LEVEL_INDEX_ROUTE = 'admin.kost-list.index';
    private const ROOM_LIST_LEVEL_INDEX_ROUTE = 'admin.room-list.index';

    private $roomUnitRepository;

    public function __construct(RoomUnitRepository $roomUnitRepository)
    {
        $this->middleware(function ($request, $next) {
            if (!Auth::user()->can('access-kost-level')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        $this->roomUnitRepository = $roomUnitRepository;

        View::share('contentHeader', 'Room Level Management');
        View::share('user', Auth::user());
    }

    public function index(Request $request, $id)
    {
        $room = Room::select(['id', 'song_id', 'room_count', 'room_available'])->where('song_id', $id)->first();
        if (is_null($room)) {
            return redirect()->to(route(self::KOST_LIST_LEVEL_INDEX_ROUTE). '#kost-level')->with('errors', Validator::make([], [])->getMessageBag()->add('kost', 'Invalid Kost'));
        } 
        
        $total = RoomUnit::where('designer_id', $room->id)->count();
        if ($total < 1) {
            $this->roomUnitRepository->backFillRoomUnit($room);
        }

        $limit = (int) $request->get('limit', RoomUnitRepository::PER_PAGE);

        $roomUnits = $this->roomUnitRepository->getRoomUnitListPaginated($room->id, $request->all(), $limit);

        ActivityLog::LogIndex(Action::ROOM_LIST_LEVEL);

        $viewData = [
            'room' => $room,
            'roomUnits' => $roomUnits,
            'roomLevelList' => RoomLevel::orderBy('order', 'ASC')->get()->keyBy('id'),
            'boxTitle' => 'Room List'
        ];
        return view('admin.contents.room-level.room-list.index', $viewData);
    }

    public function edit($id, $roomId)
    {
        $room = Room::select(['id', 'song_id'])->where('song_id', $id)->first();
        if (is_null($room)) {
            return redirect()->to(route(self::KOST_LIST_LEVEL_INDEX_ROUTE). '#kost-level')->with('errors', Validator::make([], [])->getMessageBag()->add('kost', 'Invalid Kost'));
        } 

        $roomUnit = RoomUnit::where('id', $roomId)
                    ->where('designer_id', $room->id)->first();
        if (is_null($roomUnit)) {
            return redirect()->to(route(self::ROOM_LIST_LEVEL_INDEX_ROUTE, ['id' => $id]). '#kost-level')->with('errors', Validator::make([], [])->getMessageBag()->add('room', 'Invalid Room'));
        } 

        $viewData = [
            'room' => $room,
            'roomUnit' => $roomUnit,
            'roomLevelList' => RoomLevel::orderBy('order', 'ASC')->get()->keyBy('id'),
            'boxTitle' => 'Room List'
        ];
        return view('admin.contents.room-level.room-list.edit', $viewData);
    }

    public function update(Request $request, $id, $roomId)
    {
        $validator = Validator::make($request->all(), [
            'room_level_id' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        $room = Room::select(['id'])->where('song_id', $id)->first();
        if (is_null($room)) {
            return redirect()->to(route(self::KOST_LIST_LEVEL_INDEX_ROUTE). '#kost-level')->with('errors', $validator->getMessageBag()->add('kost', 'Invalid Kost'));
        } 

        $roomUnit = RoomUnit::where('id', $roomId)
                    ->where('designer_id', $room->id)->first();
        if (is_null($roomUnit)) {
            return redirect()->to(route(self::ROOM_LIST_LEVEL_INDEX_ROUTE, ['id' => $id]). '#kost-level')->with('errors', $validator->getMessageBag()->add('kost', 'Invalid Room'));
        } 

        $level = RoomLevel::find($request->input('room_level_id'));
        if (is_null($level)) {
            return redirect()->to(route(self::ROOM_LIST_LEVEL_INDEX_ROUTE, ['id' => $id]). '#kost-level')->with('errors', $validator->getMessageBag()->add('level', 'Invalid Level'));
        }

        $this->roomUnitRepository->updateRoomLevel($roomUnit, $level, Auth::user());

        ActivityLog::LogUpdate(Action::ROOM_LIST_LEVEL, $roomId);

        return redirect()->to(route(self::ROOM_LIST_LEVEL_INDEX_ROUTE, ['id' => $id]). '#kost-level')->with('message', 'Room was successfully updated.');
    }

    public function assignAll(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'room_level_id' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        $room = Room::select(['id'])->where('song_id', $id)->first();
        if (is_null($room)) {
            return redirect()->to(route(self::KOST_LIST_LEVEL_INDEX_ROUTE). '#kost-level')->with('errors', $validator->getMessageBag()->add('kost', 'Invalid Kost'));
        }         

        $level = RoomLevel::find($request->input('room_level_id'));
        if (is_null($level)) {
            return redirect()->to(route(self::ROOM_LIST_LEVEL_INDEX_ROUTE, ['id' => $id]). '#kost-level')->with('errors', $validator->getMessageBag()->add('level', 'Invalid Level'));
        }

        $this->roomUnitRepository->assignRoomLevelToAllUnits($room->id, $level, Auth::user());

        ActivityLog::LogUpdate(Action::ROOM_LIST_LEVEL, $id);

        return redirect()->to(route(self::ROOM_LIST_LEVEL_INDEX_ROUTE, ['id' => $id]). '#kost-level')->with('message', 'All Rooms were successfully updated.');
    }

    public function history($id, $roomId)
    {
        $room = Room::with(['owners', 'owners.user'])->where('song_id', $id)->first();
        if (is_null($room)) {
            return redirect()->to(route(self::KOST_LIST_LEVEL_INDEX_ROUTE). '#kost-level')->with('errors', Validator::make([], [])->getMessageBag()->add('kost', 'Invalid Kost'));
        }

        $roomUnit = RoomUnit::where('id', $roomId)->where('designer_id', $room->id)->first();
        if (is_null($roomUnit)) {
            return redirect()->to(route(self::ROOM_LIST_LEVEL_INDEX_ROUTE, ['id' => $id]). '#kost-level')->with('errors', Validator::make([], [])->getMessageBag()->add('room', 'Invalid Room'));
        }

        $levelHistories = RoomLevelHistory::with(['room_level', 'user'])->where('room_id', $roomId)->orderBy('created_at', 'desc')->paginate();
        if (!count($levelHistories)) {
            return redirect()->to(route(self::ROOM_LIST_LEVEL_INDEX_ROUTE, ['id' => $id]). '#kost-level')->with('errors', Validator::make([], [])->getMessageBag()->add('history', 'No History Found'));
        }

        $viewData = [
            'room' => $room,
            'roomUnit' => $roomUnit,
            'levelHistories' => $levelHistories,
            'boxTitle' => 'Room Level History'
        ];
        return view('admin.contents.room-level.room-list.history', $viewData);
    }
}