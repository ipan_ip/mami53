<?php

namespace App\Http\Controllers\Admin\Level;

use App\Criteria\Consultant\PaginationCriteria;
use App\Criteria\Property\PropertyContract\OwnerNameCriteria;
use App\Criteria\Property\PropertyContract\OwnerPhoneCriteria;
use App\Criteria\Property\PropertyContract\PropertyNameCriteria;
use App\User;
use Illuminate\View\View;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Helpers\ApiHelper;
use App\Presenters\RoomPresenter;
use Illuminate\Http\JsonResponse;
use App\Services\Room\RoomService;
use Illuminate\Support\Facades\DB;
use App\Entities\Property\Property;
use App\Http\Controllers\Controller;
use App\Repositories\RoomRepository;
use Illuminate\Support\Facades\Auth;
use App\Entities\Level\PropertyLevel;
use Illuminate\Support\Facades\Validator;
use phpDocumentor\Reflection\Types\Self_;
use App\Presenters\Room\RoomUnitPresenter;
use App\Entities\Property\PropertyContract;
use App\Entities\Property\PropertyContractDetail;
use App\Http\Requests\Admin\PropertyPackage\AddPropertyRequest;
use App\Repositories\Room\RoomUnitRepository;
use App\Repositories\Level\KostLevelRepository;
use App\Repositories\Level\RoomLevelRepository;
use App\Presenters\Level\PropertyLevelPresenter;
use App\Repositories\Property\PropertyRepository;
use App\Presenters\Level\PropertyPackagePresenter;
use App\Services\Property\PropertyContractService;
use App\Repositories\Level\PropertyLevelRepository;
use App\Repositories\Property\PropertyContractRepository;
use App\Http\Requests\Admin\PropertyPackage\AssignKostToPropertyRequest;
use App\Http\Requests\Admin\PropertyPackage\PropertyPackageStoreRequest;
use App\Criteria\Room\ActiveCriteria;
use App\Repositories\Consultant\PotentialPropertyRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Entities\Consultant\PotentialProperty;
use App\Entities\Consultant\PotentialOwner;
use App\Repositories\GoldPlus\SubmissionRepository;
use App\Entities\GoldPlus\Enums\SubmissionStatus;
use App\Entities\Level\RoomLevelHistory;
use App\Entities\Room\Element\RoomUnit;
use App\Jobs\Property\SetKostAndRoomUnitLevelToRegular;
use App\Presenters\Property\PropertyContractPresenter;

class PropertyPackageController extends Controller
{
    /**
     *  Instance of property level repository
     *
     * @var PropertyLevelRepository
     */
    protected $propertyLevelRepository;

    /**
     *  Instance of room unit repository
     *
     * @var RoomUnitRepository
     */
    protected $roomUnitRepository;

    /**
     *  Instance of kost repository
     *
     * @var RoomRepository
     */
    protected $roomRepository;

    /**
     *  Instance of property repository
     *
     * @var PropertyRepository
     */
    protected $propertyRepository;

    /**
     *  Instance of kost level repository
     *
     * @var KostLevelRepository
     */
    protected $kostLevelRepository;

    /**
     *  Instance of room level repository
     *
     * @var RoomLevelRepository
     */
    protected $roomLevelRepository;

    /**
     *  Instance of potential property repository
     *
     * @var PotentialPropertyRepository
     */
    protected $potentialPropertyRepository;

    /**
     *  Instance of GP submission repository
     *
     * @var SubmissionRepository
     */
    protected $gpSubmissionRepo;

    private $propertyContractRepository;

    /**
     *  Instance of room service
     *
     * @var RoomService
     */
    protected $roomService;

    /**
     *  Instance of property contract service
     *
     * @var PropertyContractService
     */
    protected $service;

    protected const PAGINATION_LIMIT = 15;

    private const SEARCH_LIST_BY_OWNER_PHONE = 'owner_phone';
    private const SEARCH_LIST_BY_OWNER_NAME = 'owner_name';
    private const SEARCH_LIST_BY_PROPERTY_NAME = 'property_name';

    protected const STATUS_DELETE = 'delete';
    protected const STATUS_TERMINATE = 'terminated';

    public function __construct(
        PropertyContractRepository $propertyContractRepository,
        PropertyLevelRepository $propertyLevelRepository,
        RoomUnitRepository $roomUnitRepository,
        PropertyContractService $service,
        KostLevelRepository $kostLevelRepository,
        RoomRepository $roomRepository,
        RoomLevelRepository $roomLevelRepository,
        PropertyRepository $propertyRepository,
        RoomService $roomService,
        PotentialPropertyRepository $potentialPropertyRepository,
        SubmissionRepository $gpSubmissionRepo
    ) {
        \View::share('contentHeader', 'Property Package');
        $this->propertyContractRepository = $propertyContractRepository;
        $this->propertyLevelRepository = $propertyLevelRepository;
        $this->roomUnitRepository = $roomUnitRepository;
        $this->service = $service;
        $this->kostLevelRepository = $kostLevelRepository;
        $this->roomRepository = $roomRepository;
        $this->roomLevelRepository = $roomLevelRepository;
        $this->propertyRepository = $propertyRepository;
        $this->roomService = $roomService;
        $this->potentialPropertyRepository = $potentialPropertyRepository;
        $this->gpSubmissionRepo = $gpSubmissionRepo;

    }

    public function index()
    {
        $propertyLevels = PropertyLevel::select('id', 'name')
            ->get();

        return view('admin.contents.property-package.index', compact('propertyLevels'));
    }

    public function indexList(Request $request)
    {
        $data = $this->propertyContractRepository
            ->setPresenter(new PropertyContractPresenter(PropertyContractPresenter::PROPERTY_PACKAGE_INDEX))
            ->with(
                'property_level',
                'properties',
                'properties.owner_user'
            )
            ->withCount('properties');
        $total = PropertyContract::query();

        if ($request->filled('level')) {
            $data->where('property_level_id', $request->input('level'));
            $total->where('property_level_id', $request->input('level'));
        }

        if ($request->filled('search')) {
            $criteria = $this->searchContract($request->search_by, $request->search);
            $data->pushCriteria($criteria);
            $criteria->query($total);
        }

        if ($request->filled('status')) {
            $data->where('status', $request->input('status'));
            $total->where('status', $request->input('status'));
        }

        $data = $data->orderBy('updated_at', 'desc')
            ->pushCriteria(new PaginationCriteria($request->input('limit', self::PAGINATION_LIMIT), $request->input('offset', 0)))
            ->get();

        return response()->json(['total' => $total->count(), 'rows' => $data['data']]);
    }

    protected function searchContract(string $by, string $keyword)
    {
        switch ($by) {
            case self::SEARCH_LIST_BY_OWNER_NAME:
                $criteria = new OwnerNameCriteria($keyword);
                break;
            case self::SEARCH_LIST_BY_OWNER_PHONE:
                $criteria = new OwnerPhoneCriteria($keyword);
                break;
            case self::SEARCH_LIST_BY_PROPERTY_NAME:
                $criteria = new PropertyNameCriteria($keyword);
                break;
        }

        return $criteria;
    }

    public function createView($phone_number = null)
    {
        return view('admin.contents.property-package.create-property-contract', ['phone_number' => $phone_number]);
    }

    public function updateView($propertyContractId)
    {
        return view(
            'admin.contents.property-package.update-property-contract',
            ['propertyContractId' => $propertyContractId]
        );
    }

    public function assignView($ownerId, $propertyId, $propertyContractId)
    {
        return view(
            'admin.contents.property-package.assign-kos',
            ['ownerId' => $ownerId, 'propertyId' => $propertyId, 'propertyContractId' => $propertyContractId]
        );
    }

    public function detailView(PropertyContract $propertyContract): View
    {
        return view(
            'admin.contents.property-package.package-detail',
            ['propertyContractId' => $propertyContract->id, 'propertyId' => $propertyContract->property_id]
        );
    }

    /**
     *  Get all property levels available for contract creation dropdown
     *
     * @return JsonResponse
     */
    public function getLevels(): JsonResponse
    {
        $data = $this->propertyLevelRepository
            ->setPresenter(
                new PropertyLevelPresenter(
                    PropertyLevelPresenter::SELECT_LEVEL_DROPDOWN
                )
            )
            ->orderBy('name')
            ->get();

        return ApiHelper::responseData(
            [
                'data' => $data['data']
            ]
        );
    }

    public function searchOwnerByPhone($phoneNumber)
    {
        $owner = User::where('is_owner', 'true')
            ->where('phone_number', $phoneNumber)
            ->orderBy('updated_at', 'desc')
            ->first(['id', 'name']);

        if (!$owner) {
            return ApiHelper::responseData(['message' => 'Owner tidak dapat ditemukan'], false, false, 404);
        }

        $properties = Property::where('owner_user_id', $owner->id)
            ->whereDoesntHave(
                'property_contracts',
                function ($contract) {
                    $contract->where('status', 'active');
                }
            )
            ->get(['id', 'name'])->toArray();

        return ApiHelper::responseData(
            [
                'data' => [
                    'owner_id' => $owner->id,
                    'owner_name' => $owner->name,
                    'properties' => $properties
                ]
            ]
        );
    }

    /**
     *  Get list of room for a given kost
     *
     * @param Request $request
     * @param int $designerId
     *
     * @return JsonResponse
     */
    public function getRoomLists(Request $request, int $designerId): JsonResponse
    {
        $rooms = $this->roomUnitRepository
            ->select(
                'id',
                'name',
                'floor',
                'occupied',
                'room_level_id'
            )
            ->where('designer_id', $designerId)
            ->with('level:id,name')
            ->orderBy('name', 'asc')
            ->limit(50);

        if (!is_null($request->offset) && !empty($request->offset)) {
            $rooms->offset($request->offset);
        }

        $rooms = $rooms->get();
        $rooms = (new RoomUnitPresenter(RoomUnitPresenter::ADMIN_LEVEL_LIST))->present($rooms);

        return ApiHelper::responseData(
            [
                'data' => $rooms['data']
            ]
        );
    }

    /**
     *  Save created property contract
     *
     * @param PropertyPackageStoreRequest $request
     *
     * @return JsonResponse
     */
    public function store(PropertyPackageStoreRequest $request): JsonResponse
    {
        if ($request->isValidationFailed()) {
            return ApiHelper::responseData(
                ['messages' => $request->failedValidator->errors()],
                false,
                false,
                501
            );
        }

        $package = [
            'property_level_id' => $request->property_level_id,
            'joined_at' => $request->join_date,
            'property_id' => $request->property_id,
            'is_autocount' => !($request->custom_package),
        ];

        if ($request->custom_package) {
            $package['total_package'] = $request->total_package;
        } else {
            $package['total_package'] = 0;
        }

        $package = $this->service->createPackage($package, Auth::user());

        return ApiHelper::responseData(
            [
                'data' => $package
            ]
        );
    }

    /**
     *  Update property contract
     *
     * @param Request $request
     * @param PropertyContract $propertyContract
     *
     * @return JsonResponse
     */
    public function update(Request $request, PropertyContract $propertyContract): JsonResponse
    {
        $validator = Validator::make(
            $request->all(),
            [
                'join_date' => 'required|date_format:Y-m-d H:i:s',
                'custom_package' => 'required|boolean',
                'total_package' => 'required_if:custom_package,true|integer',
                'upgrade_to_gp2' => 'required|boolean',
            ]
        );

        if ($validator->fails()) {
            return ApiHelper::responseData(
                ['messages' => $validator->errors()],
                false,
                false,
                501
            );
        }

        $params = [
            'joined_at' => $request->join_date,
            'is_autocount' => !($request->custom_package),
            'total_package' => $request->total_package,
            'updated_by' => Auth::id()
        ];

        $package = $this->service->updatePackage($propertyContract, $params);

        $propertyContract->load('property_level');
        $isGP1Contract = in_array(
            $propertyContract->property_level->name,
            [
                PropertyLevel::GOLDPLUS_1,
                PropertyLevel::GOLDPLUS_1_PROMO
            ]
        );

        if ($request->upgrade_to_gp2 && $isGP1Contract) {
            $this->service->upgradeToGoldplus2($propertyContract, Auth::user());
        }

        return ApiHelper::responseData(
            [
                'data' => $package,
                'message' => 'Successfully updated'
            ]
        );
    }

    /**
     *  Get kost list which will be assigned to the property
     *
     * @param Request $request
     * @param User $owner
     * @param Property $property
     *
     * @return JsonResponse
     */
    public function kostListAssignToProperty(Request $request, User $owner, Property $property): JsonResponse
    {
        $data = Room::whereHas(
            'owners',
            function ($query) use ($owner) {
                $query->where('user_id', $owner->id)
                    ->where('status', RoomOwner::ROOM_VERIFY_STATUS)
                    ->where(
                        function ($q) {
                            $q->where('owner_status', RoomOwner::STATUS_TYPE_KOS_OWNER)
                                ->orWhere('owner_status', RoomOwner::STATUS_TYPE_KOS_OWNER_OLD);
                        }
                    );
            }
        )->where(
            function ($q) use ($property) {
                $q->where('property_id', $property->id)
                    ->orWhere('property_id', null);
            }
        );

        $total = $data->count();

        $data = $data->offset($request->input('offset', 0))
            ->limit($request->input('limit', self::PAGINATION_LIMIT))
            ->get();

        return response()->json(
            [
                'total' => $total,
                'rows' => $data
            ]
        );
    }

    public function kostListByProperty(Request $request, Property $property, PropertyContract $propertyContract): JsonResponse
    {
        $contractId = $propertyContract->id;
        $kosts = $this->roomRepository
            ->where('property_id', $property->id)
            ->active()
            ->with(
                [
                    'room_unit',
                    'property',
                    'property.property_contracts' => function ($contract) use ($contractId) {
                        $contract->where('id', $contractId);
                    },
                    'property.property_contracts.property_level',
                    'property.property_contracts.property_level.kost_level',
                    'property.property_contracts.property_level.kost_level.room_level',
                    'level',
                ]
            )
            ->orderBy('name', 'asc');

        $total = $kosts->count();

        $kosts = $kosts->offset($request->input('offset', 0))
            ->limit($request->input('limit', 50))
            ->get();

        $kosts = (new RoomPresenter('package-kost-list'))->present($kosts);

        return ApiHelper::responseData(
            [
                'total' => $total,
                'data' => $kosts['data']
            ]
        );
    }

    public function getPropertyPackageDetail(PropertyContract $propertyContract)
    {
        $property = $propertyContract->properties()->first();
        $data = [
            'owner_name' => $property ? $property->owner_user->name : '',
            'owner_phone_number' => $property ? $property->owner_user->phone_number : '',
            'package_name' => $propertyContract->property_level->name,
            'status' => $propertyContract->status,
            'custom_package' => !($propertyContract->is_autocount),
            'total_package' => $propertyContract->total_package,
            'join_date' => $propertyContract->joined_at,
            'created_at' => $propertyContract->created_at,
            'assigned_by' => $propertyContract->assigned_by_user ? $propertyContract->assigned_by_user->name : '-',
            'ended_at' => $propertyContract->ended_at,
            'ended_by' => $propertyContract->ended_by_user ? $propertyContract->ended_by_user->name : '-',
            'updated_by' => $propertyContract->updated_by_user ? $propertyContract->updated_by_user->name : '-',
            'properties' => $propertyContract->properties()->get(['id', 'name'])->toArray()
        ];

        return ApiHelper::responseData(
            [
                'data' => $data
            ]
        );
    }

    /**
     *  Toggle assign/unassign kost to property
     *
     * @param AssignKostToPropertyRequest $request
     *
     * @return JsonResponse
     */
    public function toggleAssignment(AssignKostToPropertyRequest $request): JsonResponse
    {
        if ($request->isValidationFailed()) {
            return ApiHelper::responseData(
                ['messages' => $request->failedValidator->errors()],
                false,
                false,
                501
            );
        }

        $property = $this->propertyRepository->find($request->property_id);
        $kost = $this->roomRepository->find($request->kost_id);

        $this->roomService->togglePropertyAssignment($property, $kost);

        return ApiHelper::responseData([]);
    }

    /**
     *  Assign all rooms of a kost to the level
     *
     * @param int $designerId
     *
     * @return JsonResponse
     */
    public function assignAllRooms(int $designerId, int $propertyContractId): JsonResponse
    {
        $kost = $this->roomRepository
            ->popCriteria(ActiveCriteria::class)
            ->with(
                [
                    'level',
                    'property',
                    'property.property_contracts' => function ($contract) use ($propertyContractId) {
                        $contract->where('id', $propertyContractId);
                    },
                    'property.property_contracts.property_level',
                    'property.property_contracts.property_level.kost_level',
                    'property.property_contracts.property_level.kost_level.room_level'
                ]
            )
            ->find($designerId);

        $property = $kost->property;

        if (is_null($property)) {
            return ApiHelper::responseData(['message' => 'Kost belum diassign ke properti.'], false, false, 501);
        }

        $contract = $property->property_contracts->first();

        if (is_null($contract)) {
            return ApiHelper::responseData(['message' => 'Properti tidak memiliki kontrak aktif.'], false, false, 501);
        }

        if ((!$contract->is_manually_created) && $contract->status !== PropertyContract::STATUS_ACTIVE) {
            return ApiHelper::responseData(['message' => 'Properti tidak memiliki kontrak aktif.'], false, false, 501);
        }

        $propertyLevel = $contract->property_level;
        $kostLevel = $propertyLevel->kost_level;

        if (is_null($kostLevel)) {
            return ApiHelper::responseData(
                ['message' => 'Properti level belum diassign kost level.'],
                false,
                false,
                501
            );
        }

        $roomLevel = $this->roomLevelRepository->where('kost_level_id', $kostLevel->id)->first();

        if (is_null($roomLevel)) {
            return ApiHelper::responseData(['message' => 'Kost level belum diassign room level.'], false, false, 501);
        }

        $this->kostLevelRepository->changeLevel($kost, $kostLevel, Auth::user(), true);

        $this->roomUnitRepository->assignRoomLevelToAllUnits($designerId, $roomLevel, Auth::user());

        $this->service->calculateTotalRoomAndPackage($contract);

        return ApiHelper::responseData();
    }

    public function unassignAllRooms(int $designerId, int $propertyContractId): JsonResponse
    {
        $kost = $this->roomRepository
            ->popCriteria(ActiveCriteria::class)
            ->with([
                'property.property_contracts' => function ($contract) use ($propertyContractId) {
                    $contract->where('id', $propertyContractId);
                }
            ])
            ->find($designerId);

        $property = $kost->property;

        if (is_null($property)) {
            return ApiHelper::responseData(['message' => 'Kost belum diassign ke properti.'], false, false, 501);
        }

        $contract = $property->property_contracts->first();

        if (is_null($contract)) {
            return ApiHelper::responseData(['message' => 'Properti tidak memiliki kontrak aktif.'], false, false, 501);
        }

        if ((!$contract->is_manually_created) && $contract->status !== PropertyContract::STATUS_ACTIVE) {
            return ApiHelper::responseData(['message' => 'Properti tidak memiliki kontrak aktif.'], false, false, 501);
        }

        $regularKostLevel = $this->kostLevelRepository->where('is_regular', true)->first();

        if (is_null($regularKostLevel)) {
            return ApiHelper::responseData(
                ['message' => 'Regular kost level belum dibuat.'],
                false,
                false,
                501
            );
        }

        $regularRoomLevel = $this->roomLevelRepository->where('is_regular', true)->first();

        if (is_null($regularRoomLevel)) {
            return ApiHelper::responseData(['message' => 'Regular room level belum dibuat.'], false, false, 501);
        }

        $this->kostLevelRepository->changeLevel($kost, $regularKostLevel, Auth::user(), true);

        $oldRoom = $this->roomUnitRepository
            ->where('designer_id', $designerId)
            ->with('level')
            ->first();

        if (!is_null($oldRoom)) {
            $roomUnitIds = $this->roomUnitRepository
                ->where('designer_id', $designerId)
                ->get()
                ->pluck('id')
                ->toArray();
            $oldRoomLevel = $oldRoom->level;
            RoomLevelHistory::addRecord($roomUnitIds, $regularRoomLevel, $oldRoomLevel, Auth::user());
        }

        $this->roomUnitRepository->where('designer_id', $designerId)
            ->update(['room_level_id' => $regularRoomLevel->id]);

        $this->service->calculateTotalRoomAndPackage($contract);

        return ApiHelper::responseData();
    }

    /**
     *  Get all properties that is not in a contract
     *
     * @param Request $request
     * @param int $ownerUserId
     *
     * @return JsonResponse
     */
    public function getOwnersUnmappedProperties(Request $request, int $ownerUserId): JsonResponse
    {
        $properties = $this->propertyRepository
            ->select('id', 'name')
            ->where('owner_user_id', $ownerUserId)
            ->whereDoesntHave(
                'property_contracts',
                function ($contract) {
                    $contract->where('status', 'active');
                }
            )
            ->get()
            ->toArray();

        return ApiHelper::responseData(['data' => $properties]);
    }

    /**
     *  Add property to property package
     *
     * @param AddPropertyRequest $request
     *
     * @return JsonResponse
     */
    public function addProperty(AddPropertyRequest $request): JsonResponse
    {
        if ($request->isValidationFailed()) {
            return ApiHelper::responseData(
                ['messages' => $request->failedValidator->errors()],
                false,
                false,
                501
            );
        }

        $isExists = PropertyContractDetail::where('property_contract_id', $request->property_contract_id)
            ->where('property_id', $request->property_id)
            ->exists();

        if ($isExists) {
            return ApiHelper::responseData([]);
        }

        $detail = new PropertyContractDetail();
        $detail->property_contract_id = $request->property_contract_id;
        $detail->property_id = $request->property_id;
        $detail->save();

        // Sync Potential Property followup_status
        $this->potentialPropertyRepository->updateFollowupStatusByContract($detail->property_contract);

        return ApiHelper::responseData([]);
    }

    public function terminate(PropertyContract $propertyContract)
    {
        $kostLevelReguler = $this->kostLevelRepository
            ->with('room_level')
            ->where('is_regular', true)
            ->first();

        if (is_null($kostLevelReguler)) {
            return ApiHelper::responseData(['message' => 'Kost level reguler tidak ditemukan'], false, false, 428);
        }

        SetKostAndRoomUnitLevelToRegular::dispatch($propertyContract, Auth::user());

        $userId = Auth::user()->id;
        $this->propertyContractRepository->terminateContract($propertyContract, $userId);

        // Sync Potential Property followup_status
        $this->potentialPropertyRepository
            ->updateFollowupStatusByContract($propertyContract, PotentialProperty::FOLLOWUP_STATUS_REJECTED);
        $this->setGPSubmissionStatusDone($propertyContract);

        return ApiHelper::responseData(
            [
                'status' => true,
                'message' => 'Contract successfully terminated'
            ]
        );
    }

    public function destroy(PropertyContract $propertyContract)
    {
        $kostLevelReguler = $this->kostLevelRepository
            ->where('is_regular', true)
            ->first();

        if (is_null($kostLevelReguler)) {
            return ApiHelper::responseData(['message' => 'Kost level reguler tidak ditemukan'], false, false, 428);
        }

        $roomLevelRegular = $kostLevelReguler->room_level ? $kostLevelReguler->room_level->id : null;

        $roomUnitIds = DB::table('kost_level_map')
                ->select('designer_room.id')
                ->join('designer', 'designer.song_id', 'kost_level_map.kost_id')
                ->join('properties', 'designer.property_id', 'properties.id')
                ->join(
                    'property_contract_detail',
                    function ($join) use ($propertyContract) {
                        $join->on('property_contract_detail.property_id', 'properties.id')
                            ->where('property_contract_id', $propertyContract->id);
                    }
                )
                ->join('designer_room', 'designer.id', 'designer_id')
                ->get()
                ->pluck('id')
                ->toArray();

        if (isset($roomUnitIds[0])) {
            $oldRoom = RoomUnit::with('level')->find($roomUnitIds[0]);
            $oldRoomLevel = $oldRoom->level;
            RoomLevelHistory::addRecord($roomUnitIds, $kostLevelReguler->room_level, $oldRoomLevel, Auth::user());
        }

        DB::table('kost_level_map')
            ->join('designer', 'designer.song_id', 'kost_level_map.kost_id')
            ->join('properties', 'designer.property_id', 'properties.id')
            ->join(
                'property_contract_detail',
                function ($join) use ($propertyContract) {
                    $join->on('property_contract_detail.property_id', 'properties.id')
                        ->where('property_contract_id', $propertyContract->id);
                }
            )
            ->join('designer_room', 'designer.id', 'designer_id')
            ->update(['designer_room.room_level_id' => $roomLevelRegular]);

        foreach ($propertyContract->properties as $property) {
            foreach ($property->rooms as $room) {
                $this->kostLevelRepository->changeLevel($room, $kostLevelReguler, Auth::user(), true);
            }
        }

        $userId = Auth::user()->id;
        $this->propertyContractRepository->update(
            ['deleted_by' => $userId, 'deleted_at' => Carbon::now()],
            $propertyContract->id
        );

        // Sync Potential Property followup_status
        $this->potentialPropertyRepository
            ->updateFollowupStatusByContract($propertyContract, PotentialProperty::FOLLOWUP_STATUS_REJECTED);
        $this->setGPSubmissionStatusDone($propertyContract);

        return ApiHelper::responseData(
            [
                'status' => true,
                'message' => 'Contract successfully deleted'
            ]
        );
    }

    public function storeProperty(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'name' => 'required',
                'owner_phone_number' => 'required|exists:user,phone_number'
            ]
        );

        if ($validator->fails()) {
            return ApiHelper::responseData(
                ['messages' => $validator->errors()],
                false,
                false,
                501
            );
        }

        $owner = User::where('phone_number', $request->input('owner_phone_number'))
            ->where('is_owner', 'true')
            ->first();

        if (is_null($owner)) {
            return ApiHelper::responseData(['message' => 'Owner not found'], false, false, 501);
        }

        $property = $this->propertyRepository->createProperty($request->input('name'), $owner);


        return ApiHelper::responseData(
            [
                'message' => 'Property successfully created',
                'property' => $property
            ]
        );
    }

    public function removeProperty(PropertyContract $propertyContract, Request $request)
    {
        $validation = Validator::make($request->all(), ['property_id' => 'required|int|exists:properties,id']);
        $propertyId = $request->input('property_id');

        $kosts = Room::whereHas('property', function ($query) use ($request) {
            $query->where('id', $request->input('property_id'));
        })
        ->get();

        $kostLevelReguler = $this->kostLevelRepository
            ->where('is_regular', true)
            ->first();

        if (is_null($kostLevelReguler)) {
            return ApiHelper::responseData(['message' => 'Kost level reguler tidak ditemukan'], false, false, 428);
        }

        $roomLevelRegular = $kostLevelReguler->room_level ? $kostLevelReguler->room_level->id : null;

        DB::table('kost_level_map')
            ->join('designer', 'designer.song_id', 'kost_level_map.kost_id')
            ->join('properties', 'designer.property_id', 'properties.id')
            ->join(
                'property_contract_detail',
                function ($join) use ($propertyContract, $propertyId) {
                    $join->on('property_contract_detail.property_id', 'properties.id')
                        ->where('property_contract_id', $propertyContract->id)
                        ->where('property_contract_detail.property_id', $propertyId);
                }
            )
            ->join('designer_room', 'designer.id', 'designer_id')
            ->update(['designer_room.room_level_id' => $roomLevelRegular]);

        foreach ($kosts as $kost) {
            $this->kostLevelRepository->changeLevel($kost, $kostLevelReguler, Auth::user(), true);
        }

        $propertyContract->property_contract_detail()->where('property_id', $request->input('property_id'))->delete();

        $this->service->calculateTotalRoomAndPackage($propertyContract);

        return ApiHelper::responseData(
            [
                'status' => true,
                'message' => 'Property successfully removed from contract'
            ]
        );

    }

    /**
     *  Set GP Submission status Done based on Property Contract
     *
     *  @param PropertyContract $contract
     *  @return void
     */
    private function setGPSubmissionStatusDone(PropertyContract $contract): void
    {
        $user = $this->propertyContractRepository->findUserOwnerByContract($contract);

        if (!is_null($user)) {
            $this->gpSubmissionRepo->updateSubmissionStatusByUserId($user->id, SubmissionStatus::DONE);
        }
    }

}

