<?php

namespace App\Http\Controllers\Admin\Level;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use App\Entities\Activity\Action;
use App\Entities\Activity\ActivityLog;
use App\Entities\Level\RoomLevel;
use App\Repositories\Level\RoomLevelRepository;
use Illuminate\Validation\Rule;
use Validator;

class RoomLevelController extends Controller
{
    const ROOM_LEVEL_INDEX_URL = 'admin/kost-level/room-level#kost-level';

    private $repo;

    public function __construct(RoomLevelRepository $repo)
    {
        $this->middleware(function ($request, $next) {
            if (!Auth::user()->can('access-kost-level')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        $this->repo = $repo;

        View::share('contentHeader', 'Room Level Management');
        View::share('user', Auth::user());
    }

    public function index(Request $request)
    {
        $limit = (int) $request->get('limit', RoomLevelRepository::PER_PAGE);
        
        $level = $this->repo->getRoomLevelList($request->filled('q') ? $request->input('q') : '', $limit);

        ActivityLog::LogIndex(Action::ROOM_LEVEL);

        $viewData = [
            'level' => $level,
            'boxTitle' => 'Manage Room Level',
            'chargingTypeOptions' => RoomLevel::getChargingTypeOptions(),
            'chargeInvoiceTypeOptions' => RoomLevel::getChargeInvoiceTypeOptions(),
        ];
        return view('admin.contents.room-level.level.index', $viewData);
    }

    public function show(Request $request, $id)
    {
        abort(404);
    }

    public function create()
    {
        $order = $this->repo->getAvailableOrders();

        $viewData = [
            'order' => $order,
            'isRegularAvail' => !$this->repo->hasRegularLevel(),
            'chargingTypeOptions' => RoomLevel::getChargingTypeOptions(),
            'chargeInvoiceTypeOptions' => RoomLevel::getChargeInvoiceTypeOptions(),
            'boxTitle' => 'Create Room Level'
        ];
        return view('admin.contents.room-level.level.add_edit', $viewData);
    }

    public function store(Request $request)
    {
        $data = $request->all();

        $validator = $this->makeRequestValidator($data);
        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        if ($this->failedAdditionalValidation($validator, $data)) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        $roomLevel = $this->repo->createRoomLevel($data);

        ActivityLog::LogCreate(Action::ROOM_LEVEL, $roomLevel->id);

        return redirect()->to(self::ROOM_LEVEL_INDEX_URL)->with('message', 'Room Level was successfully added.');
    }

    public function edit($id)
    {
        $level = $this->repo->getById($id);
        if (is_null($level)) {
            abort(404);
        }

        $order = $this->repo->getAvailableOrders();
        $order[] = $level->order;
        sort($order);

        $viewData = [
            'level' => $level,
            'order' => $order,
            'isRegularAvail' => !$this->repo->hasRegularLevel(),
            'chargingTypeOptions' => RoomLevel::getChargingTypeOptions(),
            'chargeInvoiceTypeOptions' => RoomLevel::getChargeInvoiceTypeOptions(),
            'boxTitle' => 'Edit Room Level'
        ];
        return view('admin.contents.room-level.level.add_edit', $viewData);
    }

    public function update(Request $request, $id)
    {
        $level = $this->repo->getById($id);
        if (is_null($level)) {
            abort(404);
        }

        $data = $request->all();
        
        $validator = $this->makeRequestValidator($data, $id);
        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        if ($this->failedAdditionalValidation($validator, $data)) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        $level = $this->repo->updateRoomLevel($level, $data);

        ActivityLog::LogUpdate(Action::ROOM_LEVEL, $level->id);

        return redirect()->to(self::ROOM_LEVEL_INDEX_URL)->with('message', 'Room Level was successfully updated.');
    }

    public function destroy($id)
    {
        $roomLevel = $this->repo->getById($id);
        if (is_null($roomLevel)) {
            abort(404);
        }

        $roomLevel->key = null;
        $roomLevel->order = 0;
        $roomLevel->save();
        $roomLevel->delete();

        ActivityLog::LogDelete(Action::ROOM_LEVEL, $id);

        return redirect()->to(self::ROOM_LEVEL_INDEX_URL)->with('message', 'Room Level was successfully deleted.');
    }

    private function makeRequestValidator(array $data, $id = null)
    {
        return Validator::make($data, [
            'name' => 'required|max:100',
            'order' => 'required',
            'is_regular' => 'required',
            'is_hidden' => 'required',
            'charging_fee' => 'present|numeric|min:0',
            'charging_type' => 'required|in:'. implode(",",array_keys(RoomLevel::getChargingTypeOptions())),
            'charge_for_owner' => 'nullable|boolean',
            'charge_for_consultant' => 'nullable|boolean',
            'charge_for_booking' => 'nullable|boolean',
            'charge_invoice_type' => 'required|in:'. implode(",",array_keys(RoomLevel::getChargeInvoiceTypeOptions())),
            'notes' => 'max:255',
            'charging_name' => 'nullable',
            'key' => [
                'nullable',
                'max:64',
                'alpha_dash',
                Rule::unique('room_level', 'key')->ignore($id, 'id')->whereNull('deleted_at')
            ],
        ]);
    }

    private function failedAdditionalValidation(&$validator, array $data)
    {
        if ($data['charging_type'] == RoomLevel::CHARGING_TYPE_PERCENTAGE) {
            if ($data['charging_fee'] > 100) {
                $validator->errors()->add('charging_fee', 'The charging fee cannot be greater than 100');
                return true;
            }
        }

        return false;
    }
}
