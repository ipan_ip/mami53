<?php

namespace App\Http\Controllers\Admin\Level;

use App\Entities\Activity\Action;
use App\Entities\Activity\ActivityLog;
use App\Entities\Consultant\ConsultantRoom;
use App\Entities\GoldPlus\Enums\SubmissionStatus;
use App\Entities\GoldPlus\Package;
use App\Entities\GoldPlus\Submission;
use App\Entities\Level\KostLevel;
use App\Entities\Level\LevelHistory;
use App\Entities\Room\Room;
use App\Http\Controllers\Controller;
use App\Http\Helpers\GoldplusConsultantHelper;
use App\Libraries\KostLevelImporter;
use App\Repositories\Level\KostLevelRepository;
use App\Repositories\Room\RoomUnitRepository;
use App\User;

use Bugsnag;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel as Importer;
use Storage;
use Validator;

class KostListController extends Controller
{
    protected $storagePath = 'imported';
    protected $repository;
    protected $roomUnitRepository;
    protected $levelImporter;

    const KOST_LIST_LEVEL_INDEX_URL = 'admin/kost-level/kost-list#kost-level';

    public function __construct(KostLevelRepository $repository, RoomUnitRepository $roomUnitRepository, KostLevelImporter $levelImporter)
    {
        $this->middleware(function ($request, $next) {
            if (!Auth::user()->can('access-kost-level')) {
                return redirect('admin');
            }

            return $next($request);
        });

        if (!Storage::disk('local')->exists($this->storagePath)) {
            Storage::disk('local')->makeDirectory($this->storagePath);
        }

        $this->repository = $repository;
        $this->roomUnitRepository = $roomUnitRepository;
        $this->levelImporter = $levelImporter;

        View::share('contentHeader', 'Kost Level Management');
        View::share('user', Auth::user());
    }

    public function index(Request $request)
    {
        $query = Room::with(['level', 'owners', 'owners.user'])->where('is_active', 'true');

        if ($request->filled('kost-name')) {
            $query->where('name', 'like', '%' . $request->input('kost-name') . '%');
        }

        $ownerWithName = [];
        if ($request->filled('owner-name')) {
            $ownerWithName = User::where('is_owner', 'true')
                ->where('name', 'like', '%' . $request->input('owner-name') . '%')
                ->pluck('id')
                ->toArray();
            $query->whereHas('owners', function ($q) use ($ownerWithName) {
                $q->whereIn('user_id', $ownerWithName);
            });
        }

        if ($request->filled('owner-phone')) {
            $ownerWithPhone = User::where('is_owner', 'true')
                ->where('phone_number', 'like', '%' . $request->input('owner-phone') . '%')
                ->pluck('id')
                ->toArray();
            if (!empty($ownerWithName)) {
                $ownerWithPhone = array_intersect($ownerWithName, $ownerWithPhone);
            }
            $query->whereHas('owners', function ($q) use ($ownerWithPhone) {
                $q->whereIn('user_id', $ownerWithPhone);
            });
        }

        if ($request->filled('level-id') && $request->input('level-id') > 0) {
            if ($request->input('level-id') == 100) {
                $query->whereDoesntHave('level');
            } else {
                $query->whereHas('level', function ($q) use ($request) {
                    $q->where('id', $request->input('level-id'));
                });
            }
        }

        $kost = $query->orderBy('updated_at', 'desc')->paginate(20);

        $kostLevelList = KostLevel::orderBy('order', 'asc')->get()->keyBy('id');
        $kostLevel = KostLevel::withTrashed()->get()->keyBy('id');

        ActivityLog::LogIndex(Action::KOST_LIST_LEVEL);

        $viewData = [
            'kost' => $kost,
            'kostLevel' => $kostLevel,
            'kostLevelList' => $kostLevelList,
            'boxTitle' => 'Kost List'
        ];
        return view('admin.contents.kost-level.kost-list.index', $viewData);
    }

    public function show(Request $request, Room $room)
    {
        abort(404);
    }

    public function create()
    {
        abort(404);
    }

    public function store(Request $request)
    {
        abort(404);
    }

    public function edit($id)
    {
        $kost = Room::with(['level', 'room_unit', 'owners', 'owners.user'])->where('song_id', $id)->first();
        if (is_null($kost)) {
            return redirect()->to(self::KOST_LIST_LEVEL_INDEX_URL)->with('errors', Validator::make([], [])->getMessageBag()->add('kost', 'Invalid Kost'));
        }

        $roomUnit = $kost->room_unit()->first();

        $kostLevelList = KostLevel::orderBy('order', 'asc')->get()->keyBy('id');
        $kostLevel = KostLevel::withTrashed()->get()->keyBy('id');

        $viewData = [
            'kost' => $kost,
            'isChargeByRoom' => !is_null($roomUnit) ? $roomUnit->is_charge_by_room : false,
            'kostLevel' => $kostLevel,
            'kostLevelList' => $kostLevelList,
            'boxTitle' => 'Kost List'
        ];
        return view('admin.contents.kost-level.kost-list.edit', $viewData);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'level' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        /** @var Room */
        $kost = Room::with(['level', 'owners'])->where('song_id', $id)->first();
        if (is_null($kost)) {
            return redirect()->to(self::KOST_LIST_LEVEL_INDEX_URL)->with('errors', $validator->getMessageBag()->add('kost', 'Invalid Kost'));
        }
        $level = KostLevel::find($request->input('level'));
        if (is_null($level)) {
            return redirect()->to(self::KOST_LIST_LEVEL_INDEX_URL)->with('errors', $validator->getMessageBag()->add('level', 'Invalid Level'));
        }

        if ($request->has('is_force') && $request->is_force == '1') {
            $this->repository->changeLevel($kost, $level, Auth::user(), true);

            $isChargeByRoom = (bool) $request->input('is_charge_by_room');
            $this->roomUnitRepository->updateRoomUnitChargeType($kost->id, $isChargeByRoom);
        } else {
            $this->repository->flagLevel($kost, $level, Auth::user());
        }

        if ($this->isCurrentlyGp($kost)) {
            if (!in_array($request->level, KostLevel::getGoldplusLevelIdsByLevel())) {
                // Downgrade from GP.
                // Will check the gp submission data and deactivating them (if any)
                $this->updateGpSubmission($kost, $request->level);
            }

            if (
                in_array($kost->goldplus_level_id, KostLevel::getGoldplusLevelIdsByGroups(3, 4)) &&
                !in_array($request->level, KostLevel::getGoldplusLevelIdsByGroups(3, 4))
            ) {
                // Downgrade from GP3/GP4.
                // Will remove the auto-generated consultant room mapping
                $this->removeConsultantRoomMappingGoldplus($kost);
            }
        }

        if (in_array($request->level, KostLevel::getGoldplusLevelIdsByGroups(3, 4))) {
            // Upgrading to GP3/GP4.
            // Will auto-generate or auto-update consultant room mapping
            $this->upsertConsultantRoomMappingGoldplus($kost);
        }

        ActivityLog::LogUpdate(Action::KOST_LIST_LEVEL, $id);

        return redirect()->back()->with('record_updated', true);
    }

    private function removeConsultantRoomMappingGoldplus(Room $room): void
    {
        $room->consultants()->detach();
    }

    private function upsertConsultantRoomMappingGoldplus(Room $room): void
    {
        $room->consultants()->detach();
        $room->consultants()->attach(GoldplusConsultantHelper::getRandomGoldplusConsultantId());
    }

    /**
     * Check is the room currently has gp level.
     *
     * @param Room $room
     * @return boolean
     */
    public function isCurrentlyGp(Room $room): bool
    {
        return in_array(optional($room->level_info)['id'], KostLevel::getGoldplusLevelIdsByLevel());
    }

    /**
     * Will remove the gp submission request of the room.
     * This function will reduce/replace the listing_ids field if there are matching room id in there.
     * In addition to reducing/replacing, the entry will be deleted once the new listing_ids is empty.
     *
     * @param Room $room
     * @param int $newGoldplusLevelId
     * @return void
     */
    private function updateGpSubmission(Room $room, int $newGoldplusLevelId): void
    {
        if (empty($ownerIds = $room->owners->pluck('user_id')->toArray())) {
            return;
        }
        $gpSubmissions = Submission::with(['package'])->whereNotDone()->whereIn('user_id', $ownerIds)->get();
        foreach ($gpSubmissions as $gpSubmission) {
            /** @var Submission $gpSubmission */
            if ($gpSubmission->is_all_listings_processed) {
                continue;
            }
            /** @var Room[]|\Illuminate\Database\Eloquent\Collection */
            $rooms = Room::with(['level'])
                ->active()
                ->whereIn('id', $gpSubmission->listing_ids_array)
                ->whereHas('owners', function ($owners) use ($gpSubmission) {
                    $owners->where('user_id', $gpSubmission->user_id);
                })->whereHas('level', function ($level) {
                    $level->whereIn('id', KostLevel::getGoldplusLevelIdsByLevel());
                })->get();
            // Checking is all listing ids is not gp anymore
            if ($rooms->isEmpty()) {
                // All rooms already not gp.
                // Will mark the gp submission as done.
                $gpSubmission->status = SubmissionStatus::DONE;
                $gpSubmission->save();
            } elseif ($gpSubmission->status != SubmissionStatus::PARTIAL) {
                // The downgrade is ongoing.
                // Marking as partial.
                $gpSubmission->status = SubmissionStatus::PARTIAL;
                $gpSubmission->save();
            }
        }
    }

    public function destroy($id)
    {
        abort(404);
    }

    public function removeFlag($id)
    {
        $kost = Room::with(['level'])->where('song_id', $id)->first();
        if (is_null($kost)) {
            return redirect()->to('admin/kost-level/kost-list/'.$id.'/edit#kost-level')->with('errors', $validator->getMessageBag()->add('kost', 'Invalid Kost'));
        }

        $this->repository->removeFlag($kost, Auth::user());

        return redirect()->to('admin/kost-level/kost-list/'.$id.'/edit#kost-level')->with('message', 'Flag successfully removed.');
    }

    public function history(Request $request, $id)
    {
        $kost = Room::with(['owners', 'owners.user'])->where('song_id', $id)->first();
        if (is_null($kost)) {
            return redirect()->to(self::KOST_LIST_LEVEL_INDEX_URL)->with('errors', Validator::make([], [])->getMessageBag()->add('kost', 'Invalid Kost'));
        }
        $levelHistory = LevelHistory::with(['kost_level', 'user', 'room'])->where('kost_id', $id)->orderBy('created_at', 'desc')->get();
        if (!count($levelHistory)) {
            return redirect()->to(self::KOST_LIST_LEVEL_INDEX_URL)->with('errors', Validator::make([], [])->getMessageBag()->add('history', 'No History Found'));
        }

        $viewData = [
            'kost' => $kost,
            'levelHistory' => $levelHistory,
            'boxTitle' => 'Kost Level History'
        ];
        return view('admin.contents.kost-level.kost-list.history', $viewData);
    }

    public function upload()
    {
        $viewData = [
            'boxTitle' => 'Upload CSV'
        ];
        return view('admin.contents.kost-level.kost-list.upload', $viewData);
    }

    public function processCsv(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'level_csv' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        $file = $request->file('level_csv');
        $this->levelImporter->setUser(Auth::user());
        try {
            if (!$file->isValid()) {
                throw new Exception('File ' . $file->getClientOriginalName() . ' is invalid.');
            }

            $extension = $file->getClientOriginalExtension();
            $filename = 'kostlevel-' . date('Ymd-His') . '-' . Str::uuid() . '.' . $extension;
            $path = Storage::disk('local')->putFileAs($this->storagePath, $file, $filename);

            Importer::queueImport($this->levelImporter, Storage::disk('local')->path($path));
            // Importer::import($this->levelImporter, Storage::disk('local')->path($path));
        } catch (Exception $exception) {
            Bugsnag::notifyException($exception);

            return redirect()->back()->with('errors', $validator->getMessageBag()->add('level_csv', 'Failed to upload file.'));
        }

        return redirect()->to(self::KOST_LIST_LEVEL_INDEX_URL)->with('message', 'Upload file success. Will process it in background.');
    }
}
