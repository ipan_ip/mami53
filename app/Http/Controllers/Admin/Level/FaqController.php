<?php

namespace App\Http\Controllers\Admin\Level;

use App\Entities\Activity\Action;
use App\Entities\Activity\ActivityLog;
use App\Entities\Level\LevelFaq;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Validator;

class FaqController extends Controller
{
    const FAQ_LEVEL_INDEX_URL = 'admin/kost-level/faq#kost-level';

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!Auth::user()->can('access-kost-level')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        View::share('contentHeader', 'Kost Level Management');
        View::share('user', Auth::user());
    }

    public function index(Request $request)
    {
        $query = LevelFaq::query();

        if ($request->filled('q')) {
            $query->where('question', 'like', '%' . $request->input('q') . '%');
            $query->orWhere('answer', 'like', '%' . $request->input('q') . '%');
        }

        $faq = $query->paginate(20);

        ActivityLog::LogIndex(Action::FAQ_LEVEL);

        $viewData = [
            'faq' => $faq,
            'boxTitle' => 'Manage Level FAQ'
        ];
        return view('admin.contents.kost-level.faq.index', $viewData);
    }

    public function show(Request $request, LevelFaq $faq)
    {
        abort(404);
    }

    public function create()
    {
        $viewData = [
            'boxTitle' => 'Create Level FAQ'
        ];
        return view('admin.contents.kost-level.faq.create', $viewData);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'question' => 'required',
            'answer' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        $faq = new LevelFaq();
        $faq->question = $request->input('question');
        $faq->answer = $request->input('answer');
        $faq->save();

        ActivityLog::LogCreate(Action::FAQ_LEVEL, $faq->id);

        return redirect()->to(self::FAQ_LEVEL_INDEX_URL)->with('message', 'FAQ successfully added.');
    }

    public function edit(LevelFaq $faq)
    {
        $viewData = [
            'faq' => $faq,
            'boxTitle' => 'Edit Level FAQ'
        ];
        return view('admin.contents.kost-level.faq.edit', $viewData);
    }

    public function update(Request $request, LevelFaq $faq)
    {
        $validator = Validator::make($request->all(), [
            'question' => 'required',
            'answer' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        $faq->question = $request->input('question');
        $faq->answer = $request->input('answer');
        $faq->save();

        ActivityLog::LogUpdate(Action::FAQ_LEVEL, $faq->id);

        return redirect()->to(self::FAQ_LEVEL_INDEX_URL)->with('message', 'FAQ successfully updated.');
    }

    public function destroy($id)
    {
        $faq = LevelFaq::find($id);
        if (is_null($faq)) {
            abort(404);
        }

        $faq->delete();

        ActivityLog::LogDelete(Action::FAQ_LEVEL, $id);

        return redirect()->to(self::FAQ_LEVEL_INDEX_URL)->with('message', 'FAQ successfully deleted.');
    }
}
