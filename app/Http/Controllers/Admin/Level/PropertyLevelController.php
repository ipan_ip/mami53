<?php

namespace App\Http\Controllers\Admin\Level;

use App\Entities\Level\PropertyLevel;
use App\Entities\Property\Property;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Level\PropertyLevelIndexRequest;
use App\Http\Requests\Admin\Level\PropertyLevelStoreRequest;
use App\Repositories\Level\KostLevelRepository;
use App\Services\Level\PropertyLevelService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use App\Entities\GoldPlus\Package;

class PropertyLevelController extends Controller
{
    private $service;

    /**
     *  Instance of Kost Level Repository
     *
     *  @var KostLevelRepository
     */
    protected $kostLevelRepository;

    public function __construct(PropertyLevelService $propertyLevelService, KostLevelRepository $kostLevelRepository)
    {
        $this->middleware(function ($request, $next) {
            if (!Auth::user()->can('access-kost-property')) {
                return redirect('admin');
            }

            return $next($request);
        });
        $this->service = $propertyLevelService;
        $this->kostLevelRepository = $kostLevelRepository;
        $this->pageTitle = 'Property Level Management';
        View::share('contentHeader', 'Property Level Management');
        View::share('user', Auth::user());
    }

    public function index(PropertyLevelIndexRequest $request)
    {
        $params = $request->validated();
        $propertyLevels = $this->service->getListPaginate($params, ['kost_level']);
        $viewData = [
            'boxTitle' => $this->pageTitle,
            'pageTitle' => $this->pageTitle,
            'propertyLevels' => $propertyLevels
        ];
        return view('admin.contents.property-level.index', $viewData);
    }

    public function create()
    {
        $viewData = [
            'boxTitle' => $this->pageTitle,
            'pageTitle' => $this->pageTitle,
            'kostLevels' => $this->kostLevelRepository->select('id', 'name')->doesntHave('property_level')->get()
        ];

        return view('admin.contents.property-level.create', $viewData);
    }

    public function store(PropertyLevelStoreRequest $request)
    {
        $params = $request->validated();
        $property = $this->service->createPropertyLevel($params);

        if ($property === null) {
            return redirect()->back()->with('error_message', 'Property gagal ditambahkan!');
        }

        return redirect()->route('admin.property.level.index')
            ->with(
                'message',
                sprintf(
                    'Property %s berhasil ditambahkan!',
                    $property->name
                )
            );
    }

    public function edit(PropertyLevel $propertyLevel)
    {
        $propertyLevel->load('kost_level');

        $viewData = [
            'propertyLevel' => $propertyLevel,
            'boxTitle' => $this->pageTitle,
            'pageTitle' => $this->pageTitle,
            'kostLevels' => $this->kostLevelRepository->select('id', 'name')
                ->where(function ($query) use ($propertyLevel) {
                    $query->where('property_level_id', $propertyLevel->id)
                        ->orDoesntHave('property_level');
                })
                ->get(),
            'goldplusPackage' => Package::all(),
        ];

        return view('admin.contents.property-level.edit', $viewData);
    }

    public function update(PropertyLevelStoreRequest $request, PropertyLevel $propertyLevel)
    {
        $params = $request->validated();
        $result = $this->service->updatePropertyLevel(
            $propertyLevel,
            $params
        );

        //Update property level
        $this->service->updatePropertyLevelId($propertyLevel, $params['goldplus_package_id']);

        if (!$result)
            return redirect()->back()->with('error_message', 'Property Level gagal diperbarui!');
        return redirect()->route('admin.property.level.index')
            ->with(
                'message',
                sprintf(
                    'Property Level %s berhasil diperbarui!',
                    $propertyLevel->name
                )
            );
    }

    public function destroy(PropertyLevel $propertyLevel)
    {
        $result = $this->service->destroy($propertyLevel);
        if (!$result)
            return redirect()->back()->with('error_message', 'Property Level gagal dihapus!');
        return redirect()->back()->with('message', 'Property Level berhasil dihapus!');
    }
}
