<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Entities\Owner\SurveySatisfaction;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;

class OwnerSurveyController extends Controller
{

	public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if(!Auth::user()->can('owner-survey')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        \View::share('contentHeader', 'Stories Management');
        \View::share('user' , Auth::user());
    }

    public function listSurveyOwner(Request $request)
    {
        $survey_owner = SurveySatisfaction::with('user', 'room')->orderBy('id', 'desc')->paginate(20);

        ActivityLog::LogIndex(Action::OWNER_SURVEY);

    	return View("admin.contents.owner.survey", ["survey" => $survey_owner, "boxTitle" => "List owner survey"]);
    }
}
