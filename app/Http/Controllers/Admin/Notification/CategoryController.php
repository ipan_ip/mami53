<?php

namespace App\Http\Controllers\Admin\Notification;

use App\Entities\Activity\Action;
use App\Entities\Activity\ActivityLog;
use App\Entities\Notif\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Validator;

class CategoryController extends Controller
{
    const CATEGORY_INDEX_URL = 'admin/notification-list/category#notification-list';

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!Auth::user()->can('access-notification-list')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        View::share('contentHeader', 'Notification Categories Management');
        View::share('user', Auth::user());
    }

    public function index(Request $request)
    {
        $query = Category::query();

        if ($request->filled('q')) {
            $query->where('name', 'like', '%' . $request->input('q') . '%');
        }

        $categories = $query->orderBy('order', 'asc')->paginate(20);

        ActivityLog::LogIndex(Action::NOTIFICATION_CATEGORY);

        $viewData = [
            'categories' => $categories,
            'boxTitle' => 'Manage Notification Categories'
        ];
        return view('admin.contents.notification-list.category.index', $viewData);
    }

    public function orderList()
    {
        $categories = Category::orderBy('order', 'asc')->get();

        $viewData = [
            'categories' => $categories,
            'boxTitle' => 'Manage Notification Categories Order'
        ];
        return view('admin.contents.notification-list.category.order', $viewData);
    }

    public function setOrder(Request $request)
    {
        $rawInput = $request->input('actions');
        $values = array_filter(explode('|', $rawInput));

        $categories = [];
        foreach ($values as $value) {
            $action = explode(',', $value);
            $curId = $action[0];
            $targetId = $action[1];
            $curUpdate = $action[2];
            $targetUpdate = 0 - $curUpdate;

            if (!isset($categories[$curId])) $categories[$curId] = 0;
            if (!isset($categories[$targetId])) $categories[$targetId] = 0;
            $categories[$curId] += $curUpdate;
            $categories[$targetId] += $targetUpdate;
        }
        foreach ($categories as $id => $update) {
            if ($update > 0) Category::find($id)->increment('order', $update);
            if ($update < 0) Category::find($id)->decrement('order', abs($update));
        }

        ActivityLog::Log(Action::NOTIFICATION_CATEGORY, 'Change order');

        return redirect()->to(self::CATEGORY_INDEX_URL)->with('message', 'Categories order successfully updated.');
    }

    public function show(Request $request, Category $category)
    {
        abort(404);
    }

    public function create()
    {
    	$viewData = [
    		'boxTitle' => 'Create Notification Categories'
    	];
    	return view('admin.contents.notification-list.category.create', $viewData);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
    		'name' => 'required|max:50',
            'icon' => 'max:250'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        $category = new Category();
        $category->name = $request->input('name');
        $category->icon = $request->input('icon');
        $category->order = Category::max('order') + 1;
        $category->save();

        ActivityLog::LogCreate(Action::NOTIFICATION_CATEGORY, $category->id);

        return redirect()->to(self::CATEGORY_INDEX_URL)->with('message', 'Category successfully added.');
    }

    public function edit(Category $category)
    {
    	$viewData = [
            'category' => $category,
    		'boxTitle' => 'Edit Notification Categories'
    	];
    	return view('admin.contents.notification-list.category.edit', $viewData);
    }

    public function update(Request $request, Category $category)
    {
        $validator = Validator::make($request->all(), [
    		'name' => 'required|max:50',
            'icon' => 'max:250'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        $category->name = $request->input('name');
        $category->icon = $request->input('icon');
        $category->save();

        ActivityLog::LogUpdate(Action::NOTIFICATION_CATEGORY, $category->id);

        return redirect()->to(self::CATEGORY_INDEX_URL)->with('message', 'Category successfully updated.');
    }

    public function destroy($id)
    {
        $category = Category::find($id);
        if (is_null($category)) {
            abort(404);
        }

        $category->order = 0;
        $category->save();
        $category->delete();

        ActivityLog::LogDelete(Action::NOTIFICATION_CATEGORY, $id);

        return redirect()->to(self::CATEGORY_INDEX_URL)->with('message', 'Category successfully deleted.');
    }
}
