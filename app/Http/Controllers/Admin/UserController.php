<?php 

namespace App\Http\Controllers\Admin;

use App\Http\Helpers\ApiHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Auth;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Entities\Mamipay\MamipayMedia;
use App\Entities\User\UserVerificationAccount;
use App\Entities\Activity\ActivityLog;
use App\Presenters\ActivityLogPresenter;
use App\Http\Controllers\Admin\Component\ActivityLogSorterTrait;
use App\Http\Controllers\Admin\Component\ActivityLogFilterTrait;
use Carbon\Carbon;
use App\Entities\Activity\Action;

class UserController extends Controller  {

    use ActivityLogSorterTrait, ActivityLogFilterTrait;

	public function __construct() 
    {
        View::share('contentHeader', 'User Account Management');
        View::share('user', Auth::user());
    }

    public function home(Request $request)
    {
        $threeDaysAgo = Carbon::now()->subDays(3)->hour(0)->minute(0)->second(0);

        $activityLogs = ActivityLog::query()->where('created_at', '>', $threeDaysAgo);

        $activityLogs = $this->sortActivityLog($activityLogs, $request);

        $activityLogs = $this->filterActivityLog($activityLogs, $request);

        $activityLogs = $activityLogs->paginate(15);

        $userIds = $activityLogs->where('causer_type', 'App\User')->pluck('causer_id')->toArray();
        $rowsLog = (new ActivityLogPresenter('list', $userIds))->present($activityLogs);

        $viewData = [
            'activityLogs' => $activityLogs,
            'rowsLog' => $rowsLog['data'],
        ];

        return view('admin.contents.user.dashboard', $viewData);

    }

    public function changePassword(Request $request)
    {
        $viewData = [
            'boxTitle'          => 'Change Password'
        ];

        return view('admin.contents.user.change-password', $viewData);
    }

    public function updatePassword(Request $request)
    {
    	 $validator = Validator::make($request->all(), [
                'password'              => 'required|min:8',
                'password_confirmation' => 'required_with:password|same:password|min:8',
            ]);

        if($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        $user 				= Auth::user();
        $user->password     = Hash::make(md5($request->input('password')));
        $user->save();

        ActivityLog::LogUpdate(Action::PASSWORD, Auth::user()->id);

        return redirect()->route('admin.account.change-password')->with('message', 'Password berhasil diubah');
    }
}
