<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;
use Auth;
use Validator;

use App\Repositories\Marketplace\MarketplaceInputRepository;
use App\Entities\Marketplace\MarketplaceProduct;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;


class MarketplaceProductController extends Controller
{
	protected $repository;

    protected $validationMessages = [
        'title.required' => 'Judul harus diisi',
        'title.max' => 'Judul maksimal :max karakter',
        'contact_phone.required' => 'Nomor Kontak harus diisi',
        'contact_phone.regex' => 'Format Nomor Kontak tidak valid (08xxxxxxxx)',
        'price_regular.required' => 'Harga Reguler harus diisi',
        'price_regular.numeric' => 'Harga Reguler harus berupa angka',
        'price_sale.numeric' => 'Harga Diskon harus berupa angka',
        'price_unit.max' => 'Satuan harga maksimal :max karakter',
        'latitude.required' => 'Latitude harus diisi',
        'latitude.numeric' => 'Latitude harus berupa angka',
        'longitude.required' => 'Longitude harus diisi',
        'longitude.numeric' => 'Longitude harus berupa angka',
        'address.max' => 'Alamat maksimal :max karakter',
        'photo_cover.required' => 'Foto Cover harus diisi'
    ];

	function __construct(MarketplaceInputRepository $repository)
	{
        $this->middleware(function ($request, $next) {
            if(!Auth::user()->can('access-marketplace-product')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

		$this->repository = $repository;

		View::share('contentHeader', 'Marketplace Management');
        View::share('user', Auth::user());
	}

	public function index(Request $request)
	{
		$viewData = [];

        $query = MarketplaceProduct::with(['styles', 'styles.photo']);
            
        if($request->filled('q') && $request->get('q') != '') {
            $query = $query->where('title', 'like', '%' . $request->get('q') . '%');
        }

        if($request->filled('status') && $request->get('status') != '') {
            $query = $query->where('status', $request->get('status'));
        }

    	$marketplaceProducts = $query->orderBy('created_at', 'desc')
    								->paginate(20);

    	$viewData = [];
    	$viewData['marketplaceProducts'] = $marketplaceProducts;
    	$viewData['boxTitle'] = 'Produk Marketplace';
        $viewData['statusOptions'] = MarketplaceProduct::STATUS_LABEL_ADMIN;

        ActivityLog::LogIndex(Action::MARKETPLACE_PRODUCT);

    	return view('admin.contents.marketplace.products', $viewData);
	}

    public function edit(Request $request, $id)
    {
        $viewData = [];

        $marketplaceProduct = MarketplaceProduct::with(['styles', 'styles.photo'])->find($id);

        if(!$marketplaceProduct) {
            return redirect()->back()->with('error_message', 'Produk tidak ditemukan');
        }

        $photoCover = null;
        $photoOthers = [];

        $styles = $marketplaceProduct->styles;
        if($styles) {
            foreach($styles as $style) {
                if($style->is_highlight == 1) {
                    $photoCover = $style->photo;
                } else {
                    $photoOthers[] = $style->photo;
                }
            }
        }

        $viewData['marketplaceProduct'] = $marketplaceProduct;
        $viewData['photoCover'] = $photoCover;
        $viewData['photoOthers'] = $photoOthers;
        $viewData['boxTitle'] = 'Edit Produk Marketplace';
        $viewData['statusOptions'] = [
            MarketplaceProduct::STATUS_VERIFIED => 'Aktif',
            MarketplaceProduct::STATUS_REJECTED => 'Ditolak',
            MarketplaceProduct::STATUS_INACTIVE => 'Nonaktif'
        ];

        return view('admin.contents.marketplace.product-edit', $viewData);
    }

    public function update(Request $request, $id)
    {
        $marketplaceProduct = MarketplaceProduct::find($id);

        if(!$marketplaceProduct) {
            return redirect()->back()->with('error_message', 'Produk tidak ditemukan');
        }

        $validator = Validator::make($request->all(),
            [
                'title'=>'required|max:190',
                'description'=>'nullable',
                'contact_phone' => 'regex:/^(08[1-9]\d{7,10})$/',
                'price_regular' => 'required|numeric',
                'price_sale' => 'nullable|numeric',
                'price_unit' => 'nullable|max:50',
                'latitude' => 'required|numeric',
                'longitude' => 'required|numeric',
                'address' => 'max:250',
                'photo_cover' => 'required',
                'photo_other' => 'nullable',
                // 'is_active' => 'nullable'
                'status' => 'nullable'
            ], $this->validationMessages);

        if($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator->errors());
        }

        $params = [
            'title' => $request->title,
            'description' => $request->description,
            'contact_phone' => $request->contact_phone,
            'price_regular' => $request->price_regular,
            'price_sale' => $request->price_sale,
            'price_unit' => $request->price_unit,
            'latitude' => $request->latitude,
            'longitude' => $request->longitude,
            'address' => $request->address,
            // 'is_active' => is_null($request->is_active) ? 0 : $request->is_active,
            'is_active' => $request->status == MarketplaceProduct::STATUS_VERIFIED ? 1 : 0,
            'status' => $request->status != '' ? $request->status : MarketplaceProduct::STATUS_WAITING,
            'photos' => [
                'cover' => $request->photo_cover,
                'other' => $request->photo_other
            ]
        ];

        $this->repository->updateProductByAdmin($marketplaceProduct, $params);

        ActivityLog::LogUpdate(Action::MARKETPLACE_PRODUCT, $id);
        
        return redirect(route('admin.marketplace.product'))->with('message', 'Produk berhasi di-update');
    }

    public function delete(Request $request, $id)
    {
        $marketplaceProduct = MarketplaceProduct::find($id);

        if(!$marketplaceProduct) {
            return redirect()->back()->with('error_message', 'Produk tidak ditemukan');
        }

        $marketplaceProduct->delete();

        ActivityLog::LogDelete(Action::MARKETPLACE_PRODUCT, $id);

        return redirect(route('admin.marketplace.product'))->with('message', 'Produk berhasi dihapus');
    }
}