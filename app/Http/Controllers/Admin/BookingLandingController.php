<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Http\Controllers\Controller;
use Auth;
use App\Repositories\Landing\LandingListRepository;
use App\Repositories\Booking\BookingDesignerRepository;
use App\Repositories\Booking\BookingDesignerPriceComponentRepositoryEloquent;
use App\Repositories\Booking\BookingUserRoomRepositoryEloquent;
use App\Entities\Booking\BookingDesigner;
use App\Entities\Room\Room;
use App\Entities\Landing\LandingList;
use Validator;
use Carbon\Carbon;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;
use App\Entities\Room\Element\Tag;
use Illuminate\View\View as ViewView;

class BookingLandingController extends Controller
{
    protected $repository;

    public function __construct(BookingDesignerRepository $repository)
    {
        $this->middleware(function ($request, $next) {
            if(!Auth::user()->can('access-landing-booking')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        $this->repository = $repository;

    	View::share('contentHeader', 'Booking Landing');
        View::share('user', Auth::user());
    }

    public function index(Request $request): \Illuminate\View\View
    {
        $landingLists = LandingList::getIndexBookingLanding(["booking", "booking-specific"]);

        if($request->filled('q') && $request->get('q') != '') {
            $landingLists->where(function($q) use ($request) {
                $q->where('slug', 'like', '%' . $request->get('q') . '%')
                    ->orWhere('keyword', 'like', '%' . $request->get('q') . '%')
                    ->orWhere('option', 'like', '%' . $request->get('q') . '%');
            });
        }

        $landings = $landingLists->paginate(100);
        
        foreach($landings as $landing) 
        {
            $options = json_decode($landing->option, TRUE);
            $landing->article = $options['article'];
            $landing->title = $options['title'];
            $landing->location = $options['location'];
            $landing->type = $landing->type;
            $landing->room_count = Room::getBookableRoomWithinArea($landing->location);
        }

        $viewData = [];
        $viewData['landings'] = $landings;

        ActivityLog::LogIndex(Action::LANDING_BOOKING);

        return view('admin.contents.booking.booking_landing', $viewData);
    }

    public function store(Request $request)
    {
        $storeLanding = LandingList::store('booking', $request->all());

        if (!$storeLanding) 
        {
            return [
                'success' => false,
                'message' => "Gagal menyimpan data landing. Silahkan refresh halaman coba lagi.",
            ];
        }

        ActivityLog::LogCreate(Action::LANDING_BOOKING, $storeLanding->id);

        return [
            'success' => true,
            'data' => $storeLanding,
        ];
    }

    public function storeLandingSpecific(Request $request)
    {
        $landingparent = LandingList::find($request->input('parent_id'));
        if (is_null($landingparent)) {
            return back()->with('error_message', 'Landing parent tidak ditemukan');
        }
        $storeLanding = LandingList::store('booking-specific', $request->all());

        if (!$storeLanding) {
            return back()->with('error_message', 'Gagal menyimpan data');
        }
        return redirect('/admin/booking/landing')->with('message', 'Berhasil menyimpan data landing');
    }

    public function update(Request $request)
    {
        $updateLanding = LandingList::updateData($request->id, 'booking', $request->all());

        if (!$updateLanding) 
        {
            return [
                'success' => false,
                'message' => "Gagal mengupdate data landing. Silahkan refresh halaman dan coba lagi.",
            ];
        }
        
        ActivityLog::LogUpdate(Action::LANDING_BOOKING, $updateLanding->id);

        return [
            'success' => true,
            'data' => $updateLanding,
        ];
    }

    public function updateMainLanding(Request $request)
    {
        $landing = LandingList::where('type', 'booking')
            ->where('slug', '=', '')
            ->pluck('id');

        if(!is_null($landing)) $landingId = $landing[0];

        try {
            $updateLanding = LandingList::updateData($landingId, 'booking-parent', $request->all());

            if (!$updateLanding) 
            {
                return [
                    'success' => false,
                    'message' => "Gagal menyimpan data parent landing. Silahkan coba lagi.",
                ];
            }

            ActivityLog::LogUpdate(Action::LANDING_BOOKING, $updateLanding->id);

            return [
                'success' => true,
                'data' => $updateLanding,
            ];
            
        } catch (Exception $e) {
            return [
                'success' => false,
                'message' => $e->getMessage()
            ];
        }
    }

    public function activate(Request $request)
    {
        try
        {
            // set new 'is_active' status
            $storeLanding = LandingList::where('id', $request->id)->first();
            $storeLanding->is_active = $request->status;
            $storeLanding->save();

            return [
                'success' => true
            ];
        }
        catch (Exception $e) 
        {
            return [
                'success' => false,
                'message' => $e->message,
            ];
        }
    }

    public function setAsDefault(Request $request)
    {
        try
        {
            // disable default landing
            $defaultLanding = LandingList::where('slug', $request->slug)->where('is_default', 1)->first();
            if (!is_null($defaultLanding))
            {
                $defaultLanding->is_default = 0;
                $defaultLanding->save();
            }

            // set new defaut landing
            $storeLanding = LandingList::where('id', $request->id)->first();
            $storeLanding->is_default = 1;
            $storeLanding->save();

            return [
                'success' => true
            ];
        }
        catch (Exception $e) 
        {
            return [
                'success' => false,
                'message' => $e->message,
            ];
        }
    }

    public function remove(Request $request)
    {
        try
        {
            $storeLanding = LandingList::where('id', $request->id)->delete();
            
            ActivityLog::LogDelete(Action::LANDING_BOOKING, $request->id);

            return [
                'success' => true
            ];
        }
        catch (Exception $e) 
        {
            return [
                'success' => false,
                'message' => $e->message,
            ];
        }
    }

    public function getCityGeocode(Request $request)
    {
        $geocode = LandingList::getGeocodeByCityName($request->input('city'));
        return response()->json($geocode);
    }

    public function getAjaxData()
    {
        $cities = LandingList::getCitiesAjax('is_booking', 1);
        return response()->json($cities);
    }

    public function getMainLanding()
    {
        $landing = LandingList::where('type', 'booking')
            ->where('slug', '=', '')
            ->first();

        if(!is_null($landing))
        {
            $option = json_decode($landing->option, TRUE);
            $landing->option = $option;
        }

        return response()->json($landing);
    }

    public function sortOrder(Request $request)
    {
        $sortData = $request->all();

        try
        {
            $saveSorting = LandingList::saveSortOrder($sortData);

            if (!$saveSorting)
            {
                return [
                    'success' => false,
                    'message' => "Gagal menyimpan urutan baru. Silakan refresh halaman dan coba lagi!",
                ];
            }

            return [
                'success' => true
            ];
        }
        catch (Exception $e) 
        {
            return [
                'success' => false,
                'message' => $e->message,
            ];
        }
    }

    /**
     * Access edit booking landing specific page
     */
    public function editSpecific(int $id, LandingListRepository $landingListRepository): ViewView
    {
        $landingSpecific = $landingListRepository->getLandingBookingSpecificById($id);

        if (is_null($landingSpecific)) {
            return back()->with('error_message', 'Landing tidak ditemukan');
        }

        $landingLists = $landingListRepository->getAllBookingLanding(["booking"]);
        $rowsConcern = Tag::where('type', '<>', 'fac_project')->get();

        $viewData['landingLists'] = $landingLists;
        $viewData['boxTitle'] = "Add new booking specific";
        $viewData['cities'] = $landingListRepository->getCitiesAjax('is_booking', 1);
        $viewData['areaType'] = LandingList::AREA;
        $viewData['rowsConcern'] = $rowsConcern;
        $viewData['genderOptions'] = LandingList::GENDER;
        $viewData['rentType'] = LandingList::RENT_TYPE;
        $viewData['filter'] = json_decode($landingSpecific->option, TRUE);
        $viewData['landing'] = $landingSpecific;

        return view('admin.contents.booking.booking_landing_specific_edit', $viewData);
    }

    /**
     * Update landing booking specific
     */
    public function updateSpecific(Request $request, $id)
    {
        $updateLanding = LandingList::updateData($id, 'booking-specific', $request->all());

        if (!$updateLanding) {
            return back()->with('error_message', 'Gagal update landing');
        }
        return redirect('admin/booking/landing')->with('message', 'Berhasil update landing');
    }

    public function specific(Request $request)
    {
        $landingLists = LandingList::getAllBookingLanding(["booking"])->get();
        $rowsConcern = Tag::where('type', '<>', 'fac_project')->get();

        $viewData['landingLists'] = $landingLists;
        $viewData['boxTitle'] = "Add new booking specific";
        $viewData['cities'] = LandingList::getCitiesAjax('is_booking', 1);
        $viewData['areaType'] = LandingList::AREA;
        $viewData['rowsConcern'] = $rowsConcern;
        $viewData['genderOptions'] = LandingList::GENDER;
        $viewData['rentType'] = LandingList::RENT_TYPE;
        
        return view('admin.contents.booking.booking_landing_specific', $viewData);
    }
}