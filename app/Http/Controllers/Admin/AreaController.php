<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Http\Helpers\ApiHelper;

use App\Entities\Area\Area;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;
use Cache;

class AreaController extends Controller
{
    
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if(!Auth::user()->can('access-area')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        View::share('contentHeader', 'Stories Management');
        View::share('user', Auth::user());
    }

   
    public function index()
    {
       $rowsArea = Area::orderBy('id', 'desc')->get();

        $viewData = array(
            'boxTitle'     => 'Area List',
            'deleteAction' => '',
            'rowsArea'      => $rowsArea,
        );

        ActivityLog::LogIndex(Action::AREA);

        return View::make('admin.contents.areas.index', $viewData);
    }

    public function create()
    {
        $rowsArea = new Area;

        $rowsArea->name = Input::old('name');
        $rowsArea->type = Input::old('type');
        $rowsArea->parent_name = Input::old('parent_name');
        $rowsArea->latitude_1 = Input::old('latitude_1');
        $rowsArea->longitude_1 = Input::old('longitude_1');
        $rowsArea->latitude_2 = Input::old('latitude_2');
        $rowsArea->longitude_2 = Input::old('longitude_2');

        $viewData = array(
            'boxTitle'    => 'Insert Area',
            'rowsArea'      => $rowsArea,
            'rowsAreaType' => array('campus', 'area', 'mrt/halte'),
            'formAction'  => 'admin.areas.store',
            'formMethod'  => 'POST',
        );

        return View::make('admin.contents.areas.form', $viewData);

    }

    public function store(Request $request)
    {

    	$this->validate($request, array(
            'name'        => 'required',
            'type'        => 'required',
            'parent_name' => 'required',
            'latitude_1'  => 'required',
            'longitude_1' => 'required',
            'latitude_2'  => 'required',
            'longitude_2' => 'required'
        ));
        
        $rowArea   = new Area;

    	$rowArea->type         = $request->input('type');
    	$rowArea->name         = $request->input('name');
    	$rowArea->parent_name  = $request->input('parent_name');
    	$rowArea->latitude_1   = $request->input('latitude_1');
    	$rowArea->longitude_1  = $request->input('longitude_1');
    	$rowArea->latitude_2   = $request->input('latitude_2');
    	$rowArea->longitude_2  = $request->input('longitude_2');
        $rowArea->ordering     = $request->input('ordering') != '' ? $request->input('ordering') : null;
        $isSuccess = $rowArea->save();
        
        ActivityLog::LogCreate(Action::AREA, $rowArea->id);

        if ($isSuccess) {
            Cache::forget('top-campus');
            Cache::forget('top-area');
      
            return  Redirect::route('admin.areas.index')
                ->with('message', 'New Area Created');
      
        } else {

            return  Redirect::route('admin.areas.create')
                ->with('error_message', 'Aborted')
                ->withInput();
        }

    }


    public function edit($id)
    {
        $rowsArea = Area::find($id);

        $viewData = array(
            'boxTitle'    => 'View or Edit Area',
            'rowsArea'      => $rowsArea,
            'rowsAreaType' => array('area', 'campus', 'mrt/halte'),
            'formAction'  => array('admin.areas.update', $id),
            'formMethod'  => 'PUT',
        );
        return View::make('admin.contents.areas.form', $viewData);
    }

    public function update(Request $request, $id)
    {
    	$this->validate($request, array(
            'name'        => 'required',
            'type'        => 'required',
            'parent_name' => 'required',
            'latitude_1'  => 'required',
            'longitude_1' => 'required',
            'latitude_2'  => 'required',
            'longitude_2' => 'required'
        ));

        $rowArea   = Area::find($id);

    	$rowArea->type         = $request->input('type');
    	$rowArea->name         = $request->input('name');
    	$rowArea->parent_name  = $request->input('parent_name');
    	$rowArea->latitude_1   = $request->input('latitude_1');
    	$rowArea->longitude_1  = $request->input('longitude_1');
    	$rowArea->latitude_2   = $request->input('latitude_2');
    	$rowArea->longitude_2  = $request->input('longitude_2');
        $rowArea->ordering     = $request->input('ordering') != '' ? $request->input('ordering') : null;
    	$isSuccess = $rowArea->save();

        if ($isSuccess) {
            Cache::forget('top-campus');
            Cache::forget('top-area');
            ActivityLog::LogUpdate(Action::AREA, $id);
            return  Redirect::route('admin.areas.index')
                ->with('message', 'New Area Created');
      
        } else {

            return  Redirect::route('admin.areas.create')
                ->with('error_message', 'Aborted')
                ->withInput();
        }

    }

    public function destroy($id)
    {
        Area::destroy($id);

        Cache::forget('top-campus');
        Cache::forget('top-area');

        ActivityLog::LogDelete(Action::AREA, $id);

        return  Redirect::route('admin.areas.index')
            ->with('message', 'Area deleted');
    }	

}
