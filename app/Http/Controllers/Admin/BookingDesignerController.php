<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;
use Auth;

use App\Repositories\Booking\BookingDesignerRepository;
use App\Repositories\Booking\BookingDesignerPriceComponentRepositoryEloquent;
use App\Repositories\Booking\BookingUserRoomRepositoryEloquent;
use App\Entities\Booking\BookingDesigner;
use App\Entities\Room\Room;
use Validator;
use Carbon\Carbon;

class BookingDesignerController extends Controller
{
	protected $repository;

    protected $validationMessages = [
        'name.required'=>'Nama kamar harus diisi',
        'name.max'=>'Nama kamar maksimal :max karakter',
        'room_id.required'=>'Room ID harus diisi',
        'type.required'=>'Tipe kamar harus diisi',
        'type.in'=>'Tipe kamar tidak dikenali',
        'max_guest.required'=>'Jumlah tamu harus diisi',
        'max_guest.numeric'=>'Jumlah tamu harus berupa angka',
        'available_room.required'=>'Jumlah kamar harus diisi',
        'available_room.numeric'=>'Jumlah kamar harus berupa angka',
        'available_room.min'=>'Jumlah Kamar minimal :min',
        'minimum_stay.required'=>'Lama sewa minimal harus diisi',
        'minimum_stay.numeric'=>'Lama sewa minimal harus berupa angka',
        'price.required'=>'Harga harus diisi',
        'price.numeric'=>'Harga harus berupa angka',
        'sale_price.numeric'=>'Harga diskon harus berupa angka',
        'extra_guest_price'=>'Harga tambahan per tamu harus berupa angka'
    ];

    public function __construct(BookingDesignerRepository $repository)
    {
        $this->middleware(function ($request, $next) {
            if(!Auth::user()->can('access-booking-rooms')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

    	$this->repository = $repository;
    	View::share('contentHeader', 'Booking Management');
        View::share('user', Auth::user());
    }

    public function index(Request $request)
    {
        $query = Room::with('booking_designers')->where('is_booking', 1);

        if($request->filled('q') && $request->get('q') != '') {
            $query = $query->where('name', 'like', '%' . $request->get('q') . '%');
        }

    	$roomsBookingOn = $query->paginate(20);

        $viewData = [];
        $viewData['rooms'] = $roomsBookingOn;
        $viewData['boxTitle'] = 'Rooms with Booking On';

        return view('admin.contents.booking.booking_rooms', $viewData);
    }

    public function type(Request $request, $designerId)
    {
        $room = Room::with('booking_designers')->find($designerId);

        if(!$room) {
            return redirect()->back()->with('error_message', 'Kost tidak ditemukan');
        }

        $bookingDesigners = $room->booking_designers;

        $viewData = [];
        $viewData['room'] = $room;
        $viewData['bookingDesigners'] = $bookingDesigners;
        $viewData['boxTitle'] = 'Rooms for Booking';

        return view('admin.contents.booking.booking_rooms_type', $viewData);
    }

    public function create(Request $request, $designerId)
    {
        $room = Room::with(['tags', 'tags.photo', 'tags.photoSmall', 'minMonth'])->find($designerId);

        if(!$room) {
            return redirect()->back()->with('error_message', 'Kost tidak ditemukan');
        }

        $viewData = [];
        $viewData['boxTitle'] = 'Add Booking Room';
        $viewData['room'] = $room;

        $roomFacility = $room->facility();
        $facilityRoom = array_values(array_unique($roomFacility->room));

        $viewData['defaultGuest'] = 1;
        if(in_array('Sekamar berdua', $facilityRoom)) {
            $viewData['defaultGuest'] = 2;
        } elseif(in_array('Sekamar bertiga', $facilityRoom)) {
            $viewData['defaultGuest'] = 3;
        }

        $minMonth = str_replace('Min. ', '', $room->minMonth);
        $minMonthArray = explode(' ', $minMonth);
        $viewData['defaultStay'] = 1;
        if(count($minMonthArray) > 1) {
            if($minMonthArray[1] == 'Bln') {
                $viewData['defaultStay'] = $minMonthArray[0];
            } elseif($minMonthArray[1] == 'Thn') {
                $viewData['defaultStay'] = 12;
            }
        }
        

        $viewData['bookingType'] = [
            BookingDesigner::BOOKING_TYPE_DAILY,
            BookingDesigner::BOOKING_TYPE_MONTHLY
        ];
        return view('admin.contents.booking.create_booking_room', $viewData);
    }


    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), 
            [
                'name'=>'required|max:190',
                'room_id'=>'required',
                'type'=>'required|in:' . BookingDesigner::BOOKING_TYPE_DAILY . ',' . BookingDesigner::BOOKING_TYPE_MONTHLY ,
                'description'=>'nullable',
                'max_guest'=>'required|numeric',
                'available_room'=>'required|numeric|min:1',
                'minimum_stay'=>'required|numeric',
                'price'=>'required|numeric',
                'sale_price'=>'numeric',
                'extra_guest_price'=>'numeric'
            ], $this->validationMessages);

        if($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors())->withInput($request->all());
        }

        $room = Room::find($request->room_id);

        if(!$room) {
            return redirect()->back()->with('error_message', 'Kost tidak ditemukan');
        }

        $params = [
            'name'=>$request->name,
            'type'=>$request->type,
            'description'=>$request->description,
            'max_guest'=>$request->max_guest,
            'available_room'=>$request->available_room,
            'minimum_stay'=>$request->minimum_stay,
            'price'=>$request->price,
            'sale_price'=>$request->sale_price,
            'extra_guest_price'=>$request->extra_guest_price
        ];

        $this->repository->addBookingDesigner($room, $params);

        return redirect(route('admin.booking.rooms.type', $room->id))->with('message', 'Kamar berhasil ditambahkan');
    }


    public function edit(Request $request, $designerId, $bookingDesignerId)
    {
        $bookingDesigner = BookingDesigner::where('designer_id', $designerId)
                                        ->where('id', $bookingDesignerId)
                                        ->with('room')
                                        ->first();


        if(!$bookingDesigner) {
            return redirect()->back()->with('error_message', 'Tipe kamar tidak ditemukan');
        }

        $bookingUserPriceComponentRepository = new BookingDesignerPriceComponentRepositoryEloquent(app());
        $priceComponents = $bookingUserPriceComponentRepository->getAllPriceComponents($bookingDesigner);

        $viewData = [];
        $viewData['boxTitle'] = 'Edit Booking Room';
        $viewData['bookingDesigner'] = $bookingDesigner;
        $viewData['priceComponents'] = $priceComponents;

        $viewData['bookingType'] = [
            BookingDesigner::BOOKING_TYPE_DAILY,
            BookingDesigner::BOOKING_TYPE_MONTHLY
        ];

        return view('admin.contents.booking.edit_booking_room', $viewData);
    }

    public function update(Request $request, $designerId, $bookingDesignerId)
    {
        $bookingDesigner = BookingDesigner::where('designer_id', $designerId)
                                        ->where('id', $bookingDesignerId)
                                        ->with('room')
                                        ->first();


        if(!$bookingDesigner) {
            return redirect()->back()->with('error_message', 'Tipe kamar tidak ditemukan');
        }

        $validator = Validator::make($request->all(), 
            [
                'name'=>'required|max:190',
                'type'=>'required|in:' . BookingDesigner::BOOKING_TYPE_DAILY . ',' . BookingDesigner::BOOKING_TYPE_MONTHLY ,
                'description'=>'nullable',
                'max_guest'=>'required|numeric',
                'available_room'=>'required|numeric|min:1',
                'minimum_stay'=>'required|numeric',
                'price'=>'required|numeric',
                'sale_price'=>'numeric',
                'extra_guest_price'=>'numeric'
            ], $this->validationMessages);

        if($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors())->withInput($request->all());
        }


        $params = [
            'name'=>$request->name,
            'type'=>$request->type,
            'description'=>$request->description,
            'max_guest'=>$request->max_guest,
            'available_room'=>$request->available_room,
            'minimum_stay'=>$request->minimum_stay,
            'price'=>$request->price,
            'sale_price'=>$request->sale_price,
            'extra_guest_price'=>$request->extra_guest_price,
            'is_active'=> !is_null($request->is_active) ? $request->is_active : 0
        ];

        $this->repository->updateBookingDesigner($bookingDesigner, $params);

        return redirect(route('admin.booking.rooms.type', $bookingDesigner->room->id))->with('message', 'Kamar berhasil dirubah');

    }

    public function calendar(Request $request, $designerId, $bookingDesignerId)
    {
        $bookingDesigner = BookingDesigner::where('designer_id', $designerId)
                                        ->where('id', $bookingDesignerId)
                                        ->with('room')
                                        ->first();

        $bookingDesignerRoomIds = collect($bookingDesigner->booking_designer_room)->pluck('id')->toArray();


        if(!$bookingDesigner) {
            return redirect()->back()->with('error_message', 'Tipe kamar tidak ditemukan');
        }

        $month = $request->filled('month') ? $request->get('month') : date('m', time());
        $year = $request->filled('year') ? $request->get('year') : date('Y', time());

        $bookingUserRoomRepository = new BookingUserRoomRepositoryEloquent(app());
        $occupiedRooms = $bookingUserRoomRepository->getOccupiedRooms($year . '-' . $month . '-01', $year . '-' . $month . '-' . date('t', strtotime($year . '-' . $month . '-01')), $bookingDesignerRoomIds, ['booking_user']);

        $eventSources = [];
        if(count($occupiedRooms) > 0) {
            foreach ($occupiedRooms as $key => $occupiedRoom) {
                $eventSources[] = [
                    'id'=>$occupiedRoom->id,
                    'title'=>$occupiedRoom->booking_user->booking_code . ' Checkin',
                    'start'=>strtotime($occupiedRoom->checkin_date . ' ' . $occupiedRoom->booking_user::CHECKIN_HOUR) * 1000,
                    'end'=>strtotime($occupiedRoom->checkin_date . ' 23:59') * 1000,
                    'class'=>'event-' . $occupiedRoom->id
                ];

                if(Carbon::parse($occupiedRoom->checkin_date)->diffInDays(Carbon::parse($occupiedRoom->checkout_date)) >= 1) {
                    $eventSources[] = [
                        'id'=>$occupiedRoom->id,
                        'title'=>$occupiedRoom->booking_user->booking_code,
                        'start'=>strtotime('+1 day', strtotime($occupiedRoom->checkin_date . ' 00:00')) * 1000,
                        'end'=>strtotime('-1 day', strtotime($occupiedRoom->checkout_date . ' 23:00')) * 1000,
                        'class'=>'event-' . $occupiedRoom->id
                    ];
                }

                $eventSources[] = [
                    'id'=>$occupiedRoom->id,
                    'title'=>$occupiedRoom->booking_user->booking_code . ' Checkout',
                    'start'=>strtotime($occupiedRoom->checkout_date . ' 00:00') * 1000,
                    'end'=>strtotime($occupiedRoom->checkout_date . ' ' . $occupiedRoom->booking_user::CHECKOUT_HOUR) * 1000,
                    'class'=>'event-' . $occupiedRoom->id
                ];
            }
        }


        $viewData = [];
        $viewData['boxTitle'] = 'Booking Calendar : ' . $bookingDesigner->room->name . ' - ' . $bookingDesigner->name; 
        $viewData['bookingDesigner'] = $bookingDesigner;
        $viewData['eventSources'] = $eventSources;
        $viewData['eventClasses'] = collect($eventSources)->pluck('class', 'id')->toArray();
        $viewData['currentMonth'] = $month;
        $viewData['currentYear'] = $year;
        $viewData['previousMonth'] = str_pad((int)$month == 1 ? 12 : (int)$month - 1, 2, '0', STR_PAD_LEFT);
        $viewData['previousYear'] = (int)$month == 1 ? $year - 1 : $year;
        $viewData['nextMonth'] = str_pad((int)$month == 12 ? 1 : (int)$month + 1, 2, '0', STR_PAD_LEFT);
        $viewData['nextYear'] = (int)$month == 12 ? $year + 1 : $year;

        return view('admin.contents.booking.calendar', $viewData);
    }
}