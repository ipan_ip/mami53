<?php
namespace App\Http\Controllers\Admin\Component\Room;

use App\Entities\Activity\Call;
use App\Entities\Notif\Notif;
use App\Entities\Room\Room;
use App\Services\Thanos\ThanosService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Exception;
use App\Entities\Promoted\ViewPromote;
use App\Entities\Promoted\HistoryPromote;
use App\Entities\Premium\PremiumRequest;
use App\User;
use App\Entities\User\LinkCode;
use App\Entities\Room\RoomOwner;
use App\Libraries\SMSLibrary;
use DB;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use App\Entities\Media\RenameQueue;
use App\Http\Helpers\ApiHelper;
use App\Entities\User\Notification as NotifOwner;
use App\Entities\Room\ExpiredBy;
use App\Entities\Activity\TrackingFeature;
use App\Entities\Premium\OwnerFreePackage;
use Notification;
use App\Notifications\Premium\FreeApprove;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;
use App\Entities\Room\Element\RoomUnit;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Repositories\Premium\PremiumAllocationRepositoryEloquent;
use App\Http\Requests\Premium\PremiumAllocationRequest;
use Illuminate\Support\Facades\Request as FacadesRequest;

trait RoomSetterTrait
{
    /**
     * Activate / Make room visible to users.
     *
     * @param int $id
     * @return RedirectResponse
     * @see /admin/stories/{id}/activate
     */
    public function verify($id, $notify = true)
    {
        $room = Room::with(['owners', 'owners.user', 'referral_tracking' => function($p) {
                    $p->where('reward_status', 'waiting');
                }, 'referral_tracking.user'])->where('id', $id)->first();

        if (is_null($room)) {
            return redirect()
                ->route('admin.room.index')
                ->with('message', "Gagal mendapatkan data Kos.");
        }

        if(!is_null($room->apartment_project_id)) {
            $apartmentProject = $room->apartment_project;
            if($apartmentProject->is_active == 0) {
                return redirect()->route('admin.room.index')
                            ->with('error_message',"Apartemen Project sedang tidak aktif.");
            }
        }
        
        $owner = $room->owners->whereIn('owner_status', RoomOwner::OWNER_TYPES)->first();
        
        if (!is_null($owner)) {
            if (
                $owner->status === RoomOwner::ROOM_EARLY_EDIT_STATUS
                || $owner->status === RoomOwner::ROOM_EDIT_STATUS
                || $owner->status === RoomOwner::LABEL_ROOM_OWNER_EDITED
            ) {
                return redirect()
                    ->route('admin.room.index')
                    ->with('message',"Kost proses edit draft silakan verifikasi lewat menu kost owner.");
            }
        }

        Room::verifyDesigner($room, $notify, $owner);

        $verification = $room->verification;
        if (! is_null($verification)) {
            $verification->is_ready = false;
            $verification->save();
        }

        // auto verify phone, address, and visited 
        // #growthsprint1
        $room->is_verified_phone   = 1;
        $room->is_verified_address = 1;

        if(strtolower($room->agent_status) == 'agen' OR strtolower($room->agent_status) == 'agent' OR $room->agent_status == 'scrap' OR strtolower($room->agent_status) == 'lainnya' OR strtolower($room->agent_status) == 'anak kos' OR strtolower($room->agent_status) == 'anak kost') {
            
            if ($room->agent_status != 'scrap' AND strtolower($room->agent_status) != 'lainnya') $room->is_visited_kost     = 1;
            else $room->is_visited_kost = 0;
            
            $room->save();
        }

        if (!is_null($room->referral_tracking)) {
            $message = "MAMIKOS: Selamat kost yang anda upload terverifikasi. Anda berhak dapat pulsa 15ribu ditunggu dalam 2-3 hari kerja.";
            
            $send = false;
            if (isset($room->referral_tracking->user)) {
               $send = true; 
            } 
            if ($send == true) SMSLibrary::send($room->referral_tracking->user->phone_number, $message, 'infobip', $room->referral_tracking->user->id);
        }

        if (isset($owner->user)) {    
            NotifOwner::NotificationStore(["user_id" => $owner->user_id, 
                                           "designer_id" => $owner->designer_id,
                                           "type" => "room_verif"
                                           ]
                                         );
        }

        // auto create owner if room is created by scrap, agen, or lainnya
        if (in_array($room->agent_status, ["scrap", "Agen", "Lainnya"])) $this->createOwnerAccount($room);

        // auto verify owner
        if ($room->agent_status == 'Agen' AND $room->agent_id > 0) {
            if (count($room->owners) > 0) {
                $owner_status = $room->owners[0];
                $owner_status->status = "verified";
                $owner_status->save();
            }
        }

        // Premium request  after adding apartment
        if (!is_null($room->apartment_project_id) AND isset($owner->user)) {
            $checkFreePackage = OwnerFreePackage::checkSubmitData($owner->user);
            if ($checkFreePackage) {
                $premium_request = PremiumRequest::premiumInsertAuto([
                    "user" => $owner->user,
                    "premium_package_id" => $checkFreePackage->premium_package_id,
                ]);
                
                if ($premium_request) {
                    $checkFreePackage->status = "success";
                    $checkFreePackage->save();
                    Notification::send($owner->user, new FreeApprove);
                }
            }
        }
           
        return redirect()->route('admin.room.index')->with('message',"Room has been Verified");
    }

    /**
     * Deactivate room using thanos
     *
     * @param $roomId
     */
    public function snap(int $roomId, ThanosService $thanosService)
    {
        try {
            $user = "bang.kerupux";
            if (!is_null(Auth::user())) {
                $user = Auth::user()->id;
            }
            $thanos = $thanosService->snap($roomId, $user, \Illuminate\Support\Facades\Request::ip());
            return redirect()->route('admin.room.index')->with('message', $thanos->room->name . " berhasil dinonaktifkan ");
        } catch (Exception $exception) {
            Bugsnag::notifyException(
                $exception,
                function ($report) use ($roomId) {
                    $report->setMetaData([
                        'designer_id' => $roomId
                    ]);
                });
        }
    }

    /**
     * Reactivate room that has been snapped by thanos
     *
     * @param $roomId
     */
    public function revertSnap(int $roomId, ThanosService $thanosService)
    {
        $room   = Room::with('hidden_by_thanos')->where('id', $roomId)->first();
        $thanos = $room->hidden_by_thanos;

        if (is_null($thanos)) {
            return redirect()->route('admin.room.index')->withErrors($room->name . " is already active.");
        }

        try {
            $user = Auth::user();
            $thanos = $thanosService->revert(
                $roomId,
                is_null($user) ? 'bang.kerupux' : $user->name,
                FacadesRequest::ip()
            );

            return redirect()->route('admin.room.index')->with('message', $room->name . " berhasil diaktifkan");
        } catch (Exception $e) {
            Bugsnag::notifyException($e, function ($report) use ($roomId, $thanos) {
                $report->setSeverity('error');
                $report->setMetaData([
                    'designer_id'   => $roomId,
                    'thanos_id'     => $thanos->id
                ]);
            });

            return redirect()->route('admin.room.index')->withErrors("proses gagal");
        }
    }

    public function claimed($user, $room)
    {
        $owner_status = 'Pemilik Kos';

        if (!is_null($room->apartment_project_id) AND $room->apartment_project_id > 0) $owner_status = 'Pemilik Apartemen';

        $owner = new RoomOwner();
        $owner->user_id = $user->id;
        $owner->designer_id   = $room->id;
        $owner->owner_status  = $owner_status;
        if ($room->is_active == 'false') $owner->status = 'draft2';
        else $owner->status = 'verified';
        $owner->save();

        return true;
    }

    public function createdAccountAction($phone)
    {
        $user = new User();
        $user->name         = $phone;
        $user->phone_number = $phone;
        $user->is_owner     = 'true';
        $user->role         = 'user'; 
        $user->save();

        $randomstring = ApiHelper::random_string('alpha', 25);
        $LinkCode = array(
           "user_id" => $user->id,
           "link"    => $randomstring,
           "for"     => 'create_pass',
           "is_active" => 1
        );

        LinkCode::create($LinkCode);

        if ($user) { 
            $user->notify(
                new \App\Notifications\OwnerAccountCreated($randomstring)
            );
        }
        return $user;
    }

    public function createOwnerAccount($room)
    {
        $checkPhoneNumber = SMSLibrary::validateIndonesianMobileNumber($room->owner_phone);  
        
        if ($checkPhoneNumber) {
            
            $user = User::where('phone_number', $room->owner_phone)
                         ->where('is_owner', 'true')
                         ->first();

            $room = Room::with('owners')->where('id', $room->id)->first();

            if (count($room->owners) > 0) return false;

            DB::beginTransaction();
              
            if (!is_null($user)) {
                $this->claimed($user,$room);
            } else {
                $newUser = $this->createdAccountAction($room->owner_phone);
                $this->claimed($newUser, $room);
            }
            
            DB::commit();

            return true;
        } else {
            return false;
        }
    }


    public function verifyslug($id){
        Room::verifySlugDesigner($id);

        return redirect()->route('admin.room.index')->with('message',"Slug has been Verified");
    }

    public function canBooking($id)
    {
        $room = Room::canBooking($id, 1);

        return redirect()->route('admin.room.index')->with('message',"Add boking success for " . $room->name . ".");
    }

    public function premiumAllocation(Request $request, $id)
    {
        $allocation = ViewPromote::where('designer_id', $id)->find($request->input('allocation_id'));
        if (is_null($allocation)) {
            return back()->with('error_message', 'Data tidak ditemukan');
        }
        $allocation->total = $request->input('total');
        $allocation->used = $request->input('used');
        $allocation->history = $request->input('history');
        $allocation->save();
        return back()->with('message', 'Sukses');
    }

    public function getPromotion($id)
    {
        $date = Date('Y-m-d');
        $room = Room::with([
                        'view_promote',
                        'view_promote.premium_request',
                        'owners' => function($query) {
                            $query->where('status', '!=', Room::UNVERIFIED);
                        },
                        'ad_history' => function($p) use($date) {
                            $p->whereDate('date', $date);
                        }
                    ]
                )->where('id', $id)
                ->first();

        if (is_null($room)) {
            return back()->with('message', 'Room tidak ditemukan');
        }

        $roomOwner = $room->owners()
                    ->with('user')
                    ->first();

        if (is_null($roomOwner)) {
            return back()->with('message', 'Room tidak ditemukan');
        }

        $roomPromotionTotal = $room->view_promote()->count();
        if ($roomPromotionTotal > 1) {
            $viewData = array(
                'boxTitle'        => 'Promote Untuk ' . isset($room->name) ? $room->name : null,
                'room' => $room->view_promote()->with('room', 'premium_request')->get(),
                'promotion_total' => $roomPromotionTotal,
            );
            return view("admin.contents.stories.promotion", $viewData);
        }

        $roomPromote = $room->view_promote()->with('history_promote')->first();
        $total = 0;
        $historyPromote = null;

        if (!is_null($roomPromote)) {
            $historyPromote = $roomPromote->history_promote()->get();
            $total = $roomPromote->is_active == 1 ? $roomPromote->total : $roomPromote->daily_budget;
            $premiumRequest = $roomPromote->premium_request;
        } else {
            $premiumRequest = PremiumRequest::lastActiveRequest($roomOwner->user);
        }

        $viewData = array(
            'boxTitle'        => 'Promote Untuk ' . isset($room->name) ? $room->name : null,
            'useres'          => $roomOwner,
            'premiumRequest' => $premiumRequest,
            'view_promote'    => $roomPromote,
            'history_promote' => $historyPromote,
            'rowsRoom'        => $room,
            'total'           => $total,
            'view_promote_for'=> 'click',
            'promotion_total' => 1,
        );

        return view("admin.contents.stories.promotion", $viewData);
    }

    public function deactiveViewPromote($deactive)
    {
        if ($deactive->is_active == 1) {
           $checkUsedNow = $deactive->total - $deactive->history;
           $allocate     = $checkUsedNow - $deactive->used;

           /*$nowUsed  = $deactive->history + $deactive->used;
           $allocate = $deactive->total - $nowUsed;*/

           $this->endAllocatedViews($deactive->premium_request, $allocate); 

        } else {
           $allocate = $deactive->total - $deactive->history;
           $this->endAllocatedViews($deactive->premium_request, $allocate); 
        } 

        $deactive->is_active = 0;
        $deactive->history   = $deactive->history + $deactive->used;
        $deactive->save();

        /* finish history update */
        $this->updateEndHistory($deactive); 
        
    }

    public function endAllocatedViews($premium_request, $total) 
    {
        $allocate = $premium_request;
        $allocate->allocated = $allocate->allocated - $total;
        $allocate->save();

        return true;
    }

    public function updateEndHistory($deactive)
    {
         $historyPromote = HistoryPromote::Where('view_promote_id', $deactive->id)
                                          ->where('session_id', $deactive->session_id)
                                          ->first();

         $historyPromote->end_view  = $deactive->total - $deactive->used;
         $historyPromote->end_date  = date("Y-m-d H:i:s");
         $historyPromote->used_view = $deactive->used;
         $historyPromote->save(); 
         
         return true;
    }

    public function getHistoryPromote($deactive, $status)
    {
          
          if ($status == 1) {
             $deactive = $deactive->history + $deactive->used;  
          }
      
          return $deactive;
    }

    public function toHistoryPromote($data)
    {
        $insert = array(
                "start_view"      => $data->total,
                "start_date"      => date("Y-m-d H:i:s"),
                "view_promote_id" => $data->id,
                "session_id"      => $data->session_id
            );
        return HistoryPromote::create($insert);
    }

    public function getHistoryUsedView($id)
    {
         return HistoryPromote::Where('view_promote_id', $id)
                                          ->get();
    }

    public function endViewPromote($roomId)
    {
        $deactive = ViewPromote::with('history_promote', 'premium_request')->where('designer_id', $roomId)->orderBy('created_at', 'desc')->first();

        if ($deactive->is_active == 0) {
            return $deactive;
        }

        $historyToAllocated   = $deactive->used + $deactive->history;
        
        $deactive->is_active = 0;
        $deactive->history   = $deactive->history + $deactive->used;
        $deactive->save(); 
        
        /* finish history update */
        $this->updateEndHistory($deactive);
        
        $allocate = $deactive->total - $historyToAllocated;

        $this->endAllocatedViews($deactive->premium_request, $allocate); 

        return $deactive;
    }

/*    public function endViewPromoteTwo($roomId)
    {
        $deactive = ViewPromote::with('history_promote', 'premium_request')->where('designer_id', $roomId)->orderBy('created_at', 'desc')->first();
        
        $deactive->is_active = 0;
        $deactive->history   = $deactive->history + $deactive->used;
        $deactive->save(); 
        
        $this->updateEndHistory($deactive);
        
        $allocate = $deactive->total - $deactive->used;

        $this->endAllocatedViews($deactive->premium_request, $allocate); 

        return $deactive;
    }*/

    public function deactiveKost($id)
    {
        $deactive = ViewPromote::with('history_promote', 'premium_request')->where('designer_id', $id)->orderBy('id', 'desc')->first();
        
        $room = Room::with('view_promote')->where('id', $deactive->designer_id)->first();
        
        if ($deactive->is_active == 1) {
          $this->deactiveViewPromote($deactive);

          $room->is_promoted = 'false';
          $room->updateSortScore();
          $room->save();

          return redirect('admin/room/promotion/'. $id)->with('message',"Success nonaktifkan kost promoted");
        }
        
        if ( $deactive->total % 200 != 0) {
           return redirect('admin/room/promotion/'. $id)->with('message',"Harus kelipatan 200");
        }  

       $PremiumRequest = PremiumRequest::with('view_promote')
                                        ->where('id', $deactive->premium_request_id)
                                        ->where('status', '1')
                                        ->orderBy('id', 'desc')
                                        ->first();
       
       if ($PremiumRequest->view_promote == null) {
          $countViewPromote = array();
       } else {
          $countViewPromote = $PremiumRequest->view_promote()->get();
       }

       if ( count($countViewPromote) > 0) {

          if($deactive->total > $PremiumRequest->view) {
                return [ 'data' => array(), 'status' => false, "message" => "Gagal, melebihi view yang tersedia." ];
            }
           
           if($PremiumRequest->allocated == 0) {
              $resultFinish = $PremiumRequest->view - $PremiumRequest->used;
           } else {
              $resultFinish = $PremiumRequest->view - $PremiumRequest->allocated;
           }
           

           $checkAvailViewPromote = $room->view_promote()->where('premium_request_id', $PremiumRequest->id)->first();

           if (is_null($checkAvailViewPromote)) {
                
                if ( $resultFinish >= $deactive->total ) {
                  
                     $data = array(
                         "designer_id" => $room->id,
                         "premium_request_id" =>  $PremiumRequest->id,
                         "total"              => $deactive->total,
                         "now_view"           => 0,
                         "is_active"          => 1,
                         "session_id"         => 1
                        );
                     
                     /* allocated add */
                     $PremiumRequest->allocated = $PremiumRequest->allocated + $deactive->total;
                     $PremiumRequest->save();

                     $toDBViewPromote = ViewPromote::create($data);

                     $room->is_promoted = 'true';
                     $room->updateSortScore();
                     $room->save();

                     $this->toHistoryPromote($toDBViewPromote);
                     

                     $toDBViewPromote->history = $this->getHistoryPromote($toDBViewPromote, 1);
                     return redirect('admin/room/promotion/'. $id)->with('message',"Success kost promoted 1");
              
                } else {
                     return redirect('admin/room/promotion/'. $id)->with('message',"Gagal, melebihi view yang tersedia");
                } 
           }

            if($deactive->total == $checkAvailViewPromote->total) {
                $resultFinish = $resultFinish + $checkAvailViewPromote->total;
                $buy = $deactive->total;
            } else {
                $buy = $deactive->total - $checkAvailViewPromote->total;
            }
            
            $getHistory = $this->getHistoryUsedView($checkAvailViewPromote->id);

            if($checkAvailViewPromote->is_active == 1) {
                $checkUsedView = $checkAvailViewPromote->history + $checkAvailViewPromote->used;
            } else {
                $checkUsedView = $checkAvailViewPromote->history;
            }
            
           /* if history == pinned views */  
           if ($checkUsedView == $deactive->total) {

                 if ($deactive->is_active == 0) {
                     return redirect('admin/room/promotion/'. $id)->with('message',"Tambah view untuk bisa promoted");
                 }

                 $deactive = $this->endViewPromote($room->id);
                 
                 $room->is_promoted = 'false';
                 $room->updateSortScore();
                 $room->save();
                 
                 $deactive->history = $this->getHistoryPromote($deactive, 0);
                 return redirect('admin/room/promotion/'. $id)->with('message',"Success kost promoted 2");
           }
           
          
             /*dd($buy . " - " . $resultFinish  . " - " . $checkUsedView . " - " . $checkAvailViewPromote->used . " - " . $deactive->total );*/
           if ($resultFinish >= $buy AND $checkUsedView < $deactive->total) { 
    
                if($checkUsedView + $resultFinish < ($checkAvailViewPromote->is_active == 1 ? $buy :  $deactive->total)) {
                    return redirect('admin/room/promotion/'. $id)->with('message',"Gagal, melebihi view yang tersedia");
                }
       
                /* history end */
                if ($deactive->total != $checkAvailViewPromote->total) {
                    
                     $this->updateEndHistory($checkAvailViewPromote);

                }
                  
              if (isset($checkAvailViewPromote)) {
                 //update

                 if($checkAvailViewPromote->is_active == 1) {
                    $historyViews = $getHistory->sum('start_view');
                    $toAllocate   = $buy;
                 } else {
                    $historyViews = $checkAvailViewPromote->history;
                    $toAllocate   = $deactive->total - $historyViews;
                 }

                 if ($checkAvailViewPromote->is_active == 0) {
                    /*update to premium request */
                    $PremiumRequest->allocated = $PremiumRequest->allocated + $toAllocate; 
                    $PremiumRequest->save(); 
                 }
                 
                 $addtoHistory = $checkAvailViewPromote->used;

                 $checkAvailViewPromote->total = $deactive->total;

                 if ($checkAvailViewPromote->is_active == 1) {
                    $checkAvailViewPromote->history    = $checkAvailViewPromote->history + $addtoHistory; 
                 }

                 $checkAvailViewPromote->is_active  = 1;
                 $checkAvailViewPromote->used  = 0;
                 $checkAvailViewPromote->session_id = $checkAvailViewPromote->session_id + 1;
                 $checkAvailViewPromote->save();  

                   

                 $this->toHistoryPromote($checkAvailViewPromote);
                 $toDBViewPromote = $checkAvailViewPromote; 
              } else {
                 
                 $data = array(
                     "designer_id" => $room->id,
                     "premium_request_id" =>  $PremiumRequest->id,
                     "total"              => $deactive->total,
                     "now_view"           => 0,
                     "is_active"          => 1,
                     "session_id"         => 1
                    );
                 $toDBViewPromote = ViewPromote::create($data);
                 
                 /*update to premium request */
                 $PremiumRequest->allocated = $PremiumRequest->allocated + $buy; 
                 $PremiumRequest->save(); 

                 $this->toHistoryPromote($toDBViewPromote);
              }

             $room->is_promoted = 'true';
             $room->updateSortScore();
             $room->save();



             $toDBViewPromote->history = $this->getHistoryPromote($toDBViewPromote, 1);

             return redirect('admin/room/promotion/'. $id)->with('message',"Success kost promoted 3");
           } else {
              return redirect('admin/room/promotion/'. $id)->with('message',"Gagal, melebihi view yang tersedia");
           }

       } else {
           
           if ($PremiumRequest->view >= $deactive->total) {

                $room->is_promoted = 'true';
                $room->updateSortScore();
                $room->save();

               $data = array(
                         "designer_id" => $room->id,
                         "premium_request_id" =>  $PremiumRequest->id,
                         "total"              => $deactive->total,
                         "now_view"           => 0,
                         "is_active"          => 1,
                         "session_id"         => 1, 
                        );
             $toDBViewPromote = ViewPromote::create($data);

             $PremiumRequest->allocated = $PremiumRequest->allocated + $data['total']; 
             $PremiumRequest->save(); 
             

             $this->toHistoryPromote($toDBViewPromote);

             $room->is_promoted = 'true';
             $room->updateSortScore();
             $room->save();


               $toDBViewPromote->history = $this->getHistoryPromote($toDBViewPromote, 1);
               return redirect('admin/room/promotion/'. $id)->with('message',"Success kost promoted 4");
            
            } else {
               return redirect('admin/room/promotion/'. $id)->with('message',"Gagal, melebihi view yang tersedia");
            }
       }    

    }

    public function canPinned($user_id, $data, $designer_id)
    {
        $viewPromote = ViewPromote::with('premium_request')->where('designer_id', $designer_id)->orderBy('id', 'desc')->first();
        
        if (isset($viewPromote)) {
            
            $PremiumRequest = $viewPromote->premium_request()->orderBy('id', 'desc')->first();

            if ($data['status'] == 1 AND $viewPromote->is_active == 1) {
                if ($PremiumRequest->user_id == $user_id) {
                    return true;
                } else {    
                    return false;
                }
            } else if ($data['status'] == 0) {
                if ($PremiumRequest->user_id == $user_id) {
                    return true;
                } else {    
                    return false;
                }
            } 
          
        } else {
            return true;
        }
      return true;
    }

    public function endViewPromoteTwo($roomId)
    {
        $deactive = ViewPromote::with('history_promote', 'premium_request')->where('designer_id', $roomId)->orderBy('id', 'desc')->first();
        
        $deactive->is_active = 0;
        $deactive->save(); 
        
        /* finish history update */
        //$this->updateEndHistory($deactive);
        
        //$allocate = $deactive->total - $deactive->used;

        //$this->endAllocatedViews($deactive->premium_request, $allocate); 

        return $deactive;
    }


    public function pinnedKostAdmin(PremiumAllocationRequest $request)
    {
        $validator = $request->failedValidator;
        if (!is_null($validator) && $validator->fails()) {
            return back()->with('error_message', implode(",", $validator->errors()->all()));
        }

        $data = $request->all();
        $songId = $data['song_id'];
        $roomOwner = RoomOwner::with('room')
                            ->where('user_id', $data['user_id'])
                            ->whereHas('room', function($q) use($songId) {
                                $q->where('song_id', $songId);
                            })
                            ->first();

        if (is_null($roomOwner)) {
            return back()->with('message', 'Pemilik tidak ditemukan');
        }

        $premiumAllocationRepository = new PremiumAllocationRepositoryEloquent(app());
        $allocation = $premiumAllocationRepository->premiumAllocation([
            "allocation" => $data['allocation']
        ], $roomOwner);

        return back()->with('message', $allocation['message']);
    }
    
    public function pinnedDeactivate(Request $request, $id)
    {
        $roomPromote = ViewPromote::where('id', $id)->first();

        if (is_null($roomPromote)) {
            $message = "Gagal mematikan iklan premium";
        } else {
            $premiumAllocationRepository = new PremiumAllocationRepositoryEloquent(app());
            $premiumAllocationRepository->deactivatePremiumPromote($roomPromote);
            $message = "Berhasil mematikan iklan premium";
        }

        return back()->with('message', $message);
    }

    public function cantBooking($id)
    {
        $room = Room::canBooking($id, 2);

        return redirect()->route('admin.room.index')->with('message',"Hide boking success for " . $room->name . ".");
    }

    /**
     * Verify Tanpa SMS will actually make is_active = true and expired_phone = 1
     */
    public function verifyTanpaSms($idRoom)
    {
        $room = Room::with('owners')->where('id', $idRoom)->first();

        if (!is_null($room->apartment_project_id)) {
            $apartmentProject = $room->apartment_project;
            if ($apartmentProject->is_active == 0) {
                return redirect()->route('admin.room.index')
                    ->with('error_message', 'Apartemen Project sedang tidak aktif.');
            }
        }
        
        $owner = $room->owners->whereIn('owner_status', RoomOwner::OWNER_TYPES)->first();
        
        if (!is_null($owner)) {
            if ($owner->status == 'draft1' || $owner->status == 'draft2' || $owner->status == 'edited') {
                return redirect()->route('admin.room.index')
                    ->with('message', 'Kost proses edit draft silakan verifikasi lewat menu kost owner.');
            }
        }

        Room::verifyTanpaSms($room);

        return redirect()->route('admin.room.index')->with('message', 'Verified without sms');
    }

    public function justVerifyRoom($idRoom){
        // $room = Room::justVerifyRoom($idRoom);
        
        // $message = "Verified without sms";
        // if ($room == false) {
        //     $message = "Owner lagi proses edit, silakan verifikasi di menu kost owner";
        // } 
        // return redirect()->route('admin.room.index')->with('message', $message);

        return $this->verify($idRoom, false);
    }

    public function getListSurvey($id)
    {
      $room = Room::with('survey','survey.user')->where('id', $id)->first();

      $viewData = array(
          'boxTitle'        => 'Survey Untuk ' . isset($room->name) ? $room->name : null,
          'rowsRoom'        => $room,
      );

      return view("admin.contents.stories.survey", $viewData);
    }

    /**
     * Deactivate / Make room not visible to users.
     *
     * @see /admin/stories/{id}/deactivate
     * @param int $id
     * @return Response
     */
    public function unverify($id)
    {
        Room::unverifyDesigner($id);

        return redirect()->route('admin.room.index')->with('message',"Stories has been Unverified");
    }

    /**
     * Mark story status to full. So user know it.
     *
     * @param int $id
     * @return Response
     * @see GET /admin/stories/{id}/full
     */
    public function markAsFull($id)
    {
        try {
            $kos = Room::findOrFail($id);
            if ($kos !== null) {
                $kos->status = (string) Room::KOST_STATUS_FULL;
                $kos->room_available = 0;
                $kos->kost_updated_date = date('Y-m-d H:i:s');
                $kos->updateSortScore();
                $kos->save();

                $kos->loadMissing('room_unit_empty');
                if ($kos->room_unit_empty->count() > 0) {
                    $kos->room_unit_empty->map(
                        function (RoomUnit $unit) {
                            $unit->occupied = true;
                            $unit->save();
                            return $unit;
                        }
                    );
                }
            }
        }
        catch (Exception $e)
        {
            ActivityLog::LogException(Action::KOST_MARK_AS_FULL, $e);
            return redirect()->route('admin.room.index')->with('error_message',$e->getMessage());
        }

        ActivityLog::LogUpdate(Action::KOST_MARK_AS_FULL, $id);
        return redirect()->route('admin.room.index')->with('message',"$kos->name successfully marked as full.");
    }

    /**
     * Mark story status to available.
     *
     * @param int $id
     * @return Response
     * @see GET /admin/stories/{id}/full
     */
    public function markAsAvailable($id)
    {
        try {
            $story = Room::findOrFail($id);
            $story->status = "0";
            $story->kost_updated_date = date('Y-m-d H:i:s');
            $story->save();
        } catch (Exception $e) {
            return redirect()->route('admin.room.index')->with('error_message',$e->getMessage());
        }
        return redirect()->route('admin.room.index')->with('message',"$story->name successfully marked as full.");
    }

    /**
     * Give a flag to this room that tell that this owner's phone is no longer active
     */
    public function activatePhone(Request $request, $id)
    {
        $room = Room::findOrFail($id);
        $room->togglePhoneActive();
        $room->renewLastUpdate();

        return back();
    }

    /**
     * Update count of available room.
     */
    public function availableCount(Request $request, $id)
    {
        $number = $request->input('number');

        $room = Room::findOrFail($id);
        $room->setAvailableCount($number);
        $room->renewLastUpdate();


        return back();
    }

    /**
     * Updating price, include monthly, weekly, daily, and yearly.
     */
    public function updatePrice(Request $request, $id)
    {
        $room = Room::findOrFail($id);
        $room->price_daily = $request->input('price_daily');
        $room->price_weekly = $request->input('price_weekly');
        $room->price_monthly = $request->input('price_monthly');
        $room->price_yearly = $request->input('price_yearly');

        $room->save();
        $room->renewLastUpdate();

        return back();
    }

    /**
     * Updating gender type of room.
     * Gender should be 0 -> Campur, 1 -> Putra, 2 -> Putri
     */
    public function updateGender(Request $request, $id, $gender)
    {
        $room = Room::findOrFail($id);
        $room->gender = $gender;
        $room->save();
        $room->renewLastUpdate();

        return back();
    }

    public function updateTwo(Request $request, $id)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'room_count'=>'required|integer|lte:' . Room::MAX_ROOM_COUNT,
            ],
            $this->validationMessages
        );

        $room = Room::with('owners')->where('id', $id)->first();
        $validator->after(function ($validator) use ($request, $room) {
            $roomAvailable  = $request->input('room_available');
            $roomCount      = $request->input('room_count');
            if ($roomAvailable > $roomCount) {
                $validator->errors()->add('room_available', 'Jumlah kamar tersedia tidak boleh melebihi jumlah kamar total');
            } else {
                $activeContract         = $room->getBookedAndActiveContract();
                $activeContractCount    = $activeContract->count();
                if ($roomAvailable > ($roomCount - $activeContractCount)) {
                    $validator->errors()->add('room_available', 'Total kamar terisi tidak boleh kurang dari total kamar dengan kontrak.');
                }
            }
        });

        if ($validator->fails()) {
            return back()->withErrors($validator->errors()->all());
        }

        $room->admin_remark   = $request->input('admin_remark');
        $room->price_daily    = $request->input('price_daily', 0) ? : 0;
        $room->price_weekly   = $request->input('price_weekly', 0) ? : 0;
        $room->price_monthly  = $request->input('price_monthly', 0) ? : 0;
        $room->price_yearly   = $request->input('price_yearly', 0) ? : 0;
        $room->gender         = $request->input('gender', 0);
        $room->expired_phone  = $request->input('expired_phone', 0);
        $room->manager_phone  = $request->input('manager_phone', $room->manager_phone);
        $room->owner_phone    = $request->input('owner_phone', $room->owner_phone);
        $room->room_count     = $request->input('room_count', 0) ? : 0;

        $room->setAvailableCount($request->input('room_available', 0) ? : 0);

        $room->is_verified_phone   = $request->input('is_verified_phone');
        $room->is_verified_address = $request->input('is_verified_address');
        $room->is_verified_kost    = $request->input('is_verified_kost');
        $room->is_visited_kost     = $request->input('is_visited_kost');

        $room->renewLastUpdate();
        $room->save();


        // this for the purpose of set all reply status of this designer to false
        // so they will be sent an update chat
        if ($request->input('expired_phone') == 0) {
            Call::unrepliedCallStatus($room->id);
        }

        if ($request->input('expired_phone') == "1") {
           if (count($room->owners) > 0) {
              // add to expired by
              ExpiredBy::insertExpired($room->owners[0], "admin");
              // delete owner
              $room->owners()->delete();               
           }
           
        }

        if ($request->input('expired_phone') == "0") {
            ExpiredBy::deleteExpired($room);
        }

        TrackingFeature::insertTo([
            'type' => 'room-update-admin',
            'identifier' => \Auth::user()->id
        ]);

        ActivityLog::LogUpdate(Action::KOST_ADDITIONAL, $id);

        return back();
    }

    public function storeOwner(Request $request, $roomId)
    {
        $checkPhoneNumber = SMSLibrary::validateDomesticIndonesianMobileNumber($request->input('phone_number'));  
        
        if ($checkPhoneNumber) {
      
            $user = User::where('phone_number', $request->input('phone_number'))
                      ->where('is_owner', 'true')
                      ->count();

            if ($user > 0) return redirect()->back()->with('message', "User sudah tersedia");
       
            DB::beginTransaction();

            $user = new User();
            $user->name         = $request->input('phone_number');
            $user->phone_number = $request->input('phone_number');
            $user->is_owner     = 'true';
            $user->role         = 'user'; 
            $user->save();

            $LinkCode = array(
                "user_id" => $user->id,
                "link"    => $request->input('link'),
                "for"     => 'create_pass',
                "is_active" => 1
            );

            LinkCode::create($LinkCode);

            $room = Room::find($roomId);

            $owner_status = 'Pemilik Kos';
            if (!is_null($room->apartment_project_id) AND $room->apartment_project_id > 0) $owner_status = 'Pemilik Apartemen';
         
            $owner = new RoomOwner();
            $owner->user_id = $user->id;
            $owner->designer_id   = $room->id;
            $owner->owner_status  = $owner_status;

            if ($room->is_active == 'false') $owner->status = 'draft2';
            else $owner->status = 'verified';
            
            $owner->Save();
         
            DB::commit();

            if ($owner) { 
                $message = "MAMIKOS: No anda sudah terdaftar di akun Mamikos. Selanjutnya setting password anda di https://mamikos.com/p/".$request->input('link'); 
                SMSLibrary::send($request->input('phone_number'), $message, 'infobip', $user->id);
            }

            return redirect()->back()->with('message', "Berhasil membuat akun owner");
        } else {
            return redirect()->back()->with('message', "No hp tidak valid");
        }
    
    }

    /**
     * Insert Room to rename queue
     * This queue will be processed by schedule
     * 
     * @param Request $request
     * @param int $roomId
     */
    public function renamePhoto(Request $request, $roomId)
    {
        $renamedRoom = RenameQueue::where('designer_id', $roomId)->first();

        if($renamedRoom) {
            return redirect()->back()->with('error_message', 'Foto telah di-rename');
        }

        $renameQueue = RenameQueue::insertQueue($roomId);

        return redirect()->back()->with('message', 'Foto berhasil di-rename');
    }


    /**
     * SEt room as Verified by Mamikos Team
     *
     * @param int $id
     * @return Response
     */
    public function verifyByMamikos($id)
    {
        $room = Room::where('id', $id)->first();
        $room->is_verified_by_mamikos = 1;
        $room->save();

        // store notification data
        $owner = $room->owners->whereIn('owner_status', RoomOwner::OWNER_TYPES)->first();
        if (isset($owner->user)) {    
            NotifOwner::NotificationStore([
                                            "user_id" => $owner->user_id, 
                                            "designer_id" => $owner->designer_id,
                                            "type" => "room_verif_mamikos"
                                        ]);
        }
           
        return redirect()->route('admin.room.index')->with('message', "Room successfuly set as Verified by Mamikos");
    }

    /**
     * SEt room as Unverified by Mamikos Team
     *
     * @param int $id
     * @return Response
     */
    public function unverifyByMamikos($id)
    {
        $room = Room::where('id', $id)->first();
        $room->is_verified_by_mamikos = 0;
        $room->save();

        // store notification data
        $owner = $room->owners->whereIn('owner_status', RoomOwner::OWNER_TYPES)->first();
        if (isset($owner->user)) {    
            NotifOwner::NotificationStore([
                                            "user_id" => $owner->user_id, 
                                            "designer_id" => $owner->designer_id,
                                            "type" => "room_unverif_mamikos"
                                        ]);
        }
           
        return redirect()->route('admin.room.index')->with('message', "Room successfuly set back as Unverified by Mamikos");
    }
}
