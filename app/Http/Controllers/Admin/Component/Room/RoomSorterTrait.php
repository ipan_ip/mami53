<?php

namespace App\Http\Controllers\Admin\Component\Room;

use Illuminate\Http\Request;

use DB;

trait RoomSorterTrait {
    private function sortByCreationDate(Request $request, $rooms) {
        if ($request->filled('o')) {

            $order = $request->input('o');

            switch ($order) {
                case 'fc':
                    $rooms->orderBy('created_at', 'asc');
                    break;
                case 'lu':
                    $rooms->orderBy('updated_at', 'desc');
                    break;
                case 'fu':
                    $rooms->orderBy('updated_at', 'asc');
                    break;
                default:
                    $rooms->orderBy('created_at', 'desc');
                    break;
            }
        } else {
            $rooms->orderBy('id', 'desc');
        }

        return $rooms;
    }

    private function sortByActivityStatus(Request $request, $rooms) {
        $sortByActiveStatus = ((int) $request->input('active') == 1);

        if ($request->filled('active') AND $sortByActiveStatus) {
            $activeStatus = ["", "true","false"];

            $activeStatusSelected = ((int) $request->input('active'));

            $activeStatusSelected = $activeStatus[$activeStatusSelected];
            $rooms = $rooms->orderBy(DB::raw('IF(is_active = '.$activeStatusSelected.', 0, 1)'));
        }

        return $rooms;
    }
}