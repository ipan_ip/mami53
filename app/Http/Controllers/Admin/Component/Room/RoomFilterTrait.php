<?php

namespace App\Http\Controllers\Admin\Component\Room;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

use App\Entities\Room\Room;

use App\Entities\Room\RoomType;

trait RoomFilterTrait {
    protected $inputTypes = array("Semua", "Agen", "Pemilik Kos", "Pengelola Kos", "Lainnya", "Anak Kos", "Kos Duplikat", 
                                'Pemilik Apartemen', 'Pengelola Apartemen', 'Agen Apartemen', 'Gojek', 'scrap', 'scrap google', 'scrap koran');

    private function filterByName(Request $request, $query)
    {
        if (
            $request->has('q')
            && !empty(trim($request->q))
        ) {
            $query->search(trim($request->q));
        }

        return $query;
    }

    private function filterIndex(Request $request, $query)
    {
        if ($request->filled('input-type') && $request->input('input-type')) {
            if ($request->input('input-type') == "6") {
                $query->whereDuplicate();
            }
            else {
                $query->where('agent_status', 'like', $this->inputTypes[$request->input('input-type')].'%');
            }
        }

        if ($request->input('agent')) {
            $query->where('agent_name', 'like', '%'. $request->input('agent').'%');
        }

        $query->createdBetween($request->input('from'), $request->input('to'));

        if($request->filled('room')) {
            $query = $query->where('song_id', $request->get('room'));
        }

        return $query;
    }

    private function filterRoomByType($request, $rooms) {
        $selected = $request->input('room-type') ?? RoomType::All;

        switch ($selected) {
            case RoomType::All:
                break;
            case RoomType::MamiRooms:
                $rooms = $rooms->where('is_mamirooms', true);
                break;
            case RoomType::Booking:
                $rooms = $rooms->where('is_booking', true);
                break;
            case RoomType::Testing:
                $rooms = $rooms->where('is_testing', true);
                break;
            case RoomType::Promoted:
                $rooms = $rooms->where('is_promoted', 'true');
                break;
            case RoomType::Regular:
                $rooms->whereRegular();
                break;
            case RoomType::Deleted:
                $rooms->onlyTrashed();
        }

        return $rooms;
    }

    private function filterRoomByLevel(Request $request, Builder $rooms)
    {
        $level = $request->get('kost-level');
        $rooms = $rooms->when($level, function ($q) use ($level) {
            $q->whereHas('level', function ($q) use ($level) {
                $q->where('id', $level);
            });
        });

        return $rooms;
    }

    private function filterRoomByStatus(Request $request,Builder $rooms)
    {
        $selected = $request->input('active');

        switch ($selected) {
            case Room::STATUS_INACTIVE:
                $rooms->whereHas('hidden_by_thanos');
                break;
            case Room::STATUS_WAITING_FOR_VERIFICATION:
                $rooms->where('is_active', 'false');
                break;
        }

        return $rooms;
    }

    private function filterByAreaCity(Request $request, Builder $query)
    {
        if (
            $request->has('area_city')
            && !is_null($request->area_city)
        ) {
            $query = $query->where('area_city', $request->area_city);
        }

        return $query;
    }
}