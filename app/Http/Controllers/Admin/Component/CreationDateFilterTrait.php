<?php

namespace App\Http\Controllers\Admin\Component;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

trait CreationDateFilterTrait {
    private function filterQueryByCreationDate(Builder $query, Request $request): Builder {
        if ($request->filled('from')) {
            $date = trim((string) $request->get('from'));

            if (preg_match('/[\d]{2}\-[\d]{2}-[\d]{4}/i', $date)) {
                $from = Carbon::createFromFormat("d-m-Y", $date)->startOfDay();
                $query = $query->where(
                    'created_at',
                    '>=',
                    $from
                );
            }
        }

        if ($request->filled('to')) {
            $date = trim((string) $request->get('to'));

            if (preg_match('/[\d]{2}\-[\d]{2}-[\d]{4}/i', $date)) {
                $to = Carbon::createFromFormat("d-m-Y", $date)->addDay(1)->startOfDay();
                $query = $query->where(
                    'created_at',
                    '<',
                    $to
                );
            }

        }

        return $query;
    }
}
