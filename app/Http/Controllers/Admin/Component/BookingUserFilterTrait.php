<?php

namespace App\Http\Controllers\Admin\Component;

use App\Entities\Booking\BookingUser;

trait BookingUserFilterTrait {
    private $kostTypeFilter = [
        0 => null,
        1 => 'mamirooms',
        2 => 'testing',
        3 => 'booking',
        4 => 'flash_sale'
    ];

    private $bookingStatusFilter = [
        0 => null,
        1 => BookingUser::BOOKING_STATUS_BOOKED,
        2 => BookingUser::BOOKING_STATUS_CONFIRMED,
        3 => BookingUser::BOOKING_STATUS_REJECTED,
        4 => BookingUser::BOOKING_STATUS_CANCELLED,
        5 => BookingUser::BOOKING_STATUS_CANCEL_BY_ADMIN,
        6 => BookingUser::BOOKING_STATUS_EXPIRED,
        7 => BookingUser::BOOKING_STATUS_EXPIRED_DATE,
        8 => BookingUser::BOOKING_STATUS_EXPIRED_BY_OWNER,
        9 => BookingUser::BOOKING_STATUS_TERMINATED,
        10 => BookingUser::BOOKING_STATUS_VERIFIED,
        11 => BookingUser::BOOKING_STATUS_FINISHED,
        12 => BookingUser::BOOKING_STATUS_CHECKED_IN,
    ];
}
