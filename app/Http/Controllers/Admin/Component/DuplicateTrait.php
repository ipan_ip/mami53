<?php
namespace App\Http\Controllers\Admin\Component;

use App\Repositories\PropertyInputRepositoryEloquent;
use App\Entities\Room\Element\RoomPriceComponent;

trait DuplicateTrait
{
	public function priceComponentInput($apartmentUnit, $apartment)
	{
		$PropertyInput = new PropertyInputRepositoryEloquent(app());
		
		$groupedPriceComponents = $PropertyInput->groupPriceComponents($apartment->price_components, 'price_name');

		if (isset($groupedPriceComponents['maintenance'])) {		
			$PropertyInput->savePriceComponent($apartmentUnit, RoomPriceComponent::PRICE_NAME_MAINTENANCE, 'Biaya Maintenance', $groupedPriceComponents['maintenance']->price_reguler);
		}

		if (isset($groupedPriceComponents['parking'])) {
			$PropertyInput->savePriceComponent($apartmentUnit, RoomPriceComponent::PRICE_NAME_PARKING, 'Biaya Tambahan Parkir Mobil', $groupedPriceComponents['parking']->price_reguler);
		}
	}
}