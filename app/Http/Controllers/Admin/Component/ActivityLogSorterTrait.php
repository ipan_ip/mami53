<?php

namespace App\Http\Controllers\Admin\Component;

use Illuminate\Http\Request;

trait ActivityLogSorterTrait {
    private $sortOptions = [
        'last created'          => 0,
        'first created'         => 1,
    ];

    private function sortActivityLog($activityLogs, Request $request) {
        if ($request->has('sort')) {
            $selectedSort = $request->sort;

            switch ($selectedSort) {
                case $this->sortOptions['first created']: return $activityLogs->orderBy('created_at');
            }
        }

        return $activityLogs->orderBy('created_at', 'desc');
    }
}