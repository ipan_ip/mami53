<?php

namespace App\Http\Controllers\Admin\Component;

trait BookingRejectFilterTrait {
    private $typeFilter = [
        0 => null,
        1 => 'cancel',
        2 => 'reject',
    ];

}
