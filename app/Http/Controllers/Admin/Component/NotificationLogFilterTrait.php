<?php

namespace App\Http\Controllers\Admin\Component;

use Jenssegers\Mongodb\Eloquent\Builder;

use Illuminate\Http\Request;

use App\Entities\Notif\NotificationChannel;
use App\Http\Controllers\Admin\Component\CreationDateFilterTrait;

trait NotificationLogFilterTrait {
    use CreationDateFilterTrait;

    private static $CHANNEL_FILTER = [
        0 => 'all',
        1 => NotificationChannel::Email,
        2 => NotificationChannel::SMS,
        3 => NotificationChannel::Push,
        4 => NotificationChannel::Whatsapp,
    ];

    private function filterLogByNotificationID(Builder $log, Request $request) {
        $id = $request->get('notification_id');

        if (empty($id)) {
            return $log;
        }

        return $log->where('notification_id', $id);
    }

    private function filterLogByChannel(Builder $log, Request $request): Builder {
        $channel = self::$CHANNEL_FILTER[$request->get('notification_channel') ?? 0];

        if ($channel == 'all') {
            return $log;
        }

        return $log->where('channel', $channel);
    }

    private  function filterLogByCreationDate(Builder $log, Request $request): Builder {
        return $this->filterQueryByCreationDate($log, $request);
    }
}