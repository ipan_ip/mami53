<?php

namespace App\Http\Controllers\Admin\Component;

use Illuminate\Http\Request;

use Carbon\Carbon;

use App\Entities\Activity\ActivityLog;
use App\Http\Controllers\Admin\Component\CreationDateFilterTrait;

trait ActivityLogFilterTrait {
    use CreationDateFilterTrait;

    private function filterActivityLog($activityLogs, Request $request) {
        $isSearchFilled = $request->has('keyword');

        if ($isSearchFilled) {
            $activityLogs = $this->searchActivityLog($activityLogs, $request->keyword);
        }
        
        $activityLogs = $this->filterQueryByCreationDate($activityLogs, $request);

        return $activityLogs;
    }

    public function searchActivityLog($activityLogs, $keyword) {
        $isActivityLogId = ActivityLog::where('id', $keyword)->exists();

        if ($isActivityLogId) {
            return $activityLogs->where('id', $request);
        }
        else {
            $keyword = "%$keyword%";

            return $activityLogs
                ->where('log_name', 'like', $keyword)
                ->orWhere('description', 'like', $keyword)
                ->orWhere('subject_id', 'like', $keyword)
                ->orWhere('causer_id', 'like', $keyword)
                ->orWhere('created_at', 'like', $keyword);
        }
    }
}