<?php

namespace App\Http\Controllers\Admin\Component;

trait BookingUserSorterTrait {
    private $bookingSortOptions = [
        0 => null,
        1 => 'first_requested',
        2 => 'last_checkin',
        3 => 'first_checkin',
        4 => 'first_checkout_date',
        5 => 'last_checkout_date'
    ];
}