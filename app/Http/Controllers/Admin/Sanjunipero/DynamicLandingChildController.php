<?php

namespace App\Http\Controllers\Admin\Sanjunipero;

use App\Entities\Sanjunipero\DynamicLandingChild;
use App\Entities\Sanjunipero\DynamicLandingParent;
use App\Http\Controllers\Controller;
use App\Http\Helpers\ApiResponse as Api;
use App\Repositories\Sanjunipero\DynamicLandingChildRepositoryEloquent;
use App\Repositories\Sanjunipero\DynamicLandingParentRepositoryEloquent;
use App\Rules\Sanjunipero\SlugChild;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\{
    Auth,
    Input,
    Validator,
    View
};
use Illuminate\View\View as CustomView;


class DynamicLandingChildController extends Controller
{
    public $validationMessages = [
        'slug.required' => 'Slug field must be filled',
        'parent_id.required' => 'Parent field must be filled',
        'area_name.required' => 'Area Name field must be filled',
        'coordinate_1.required' => 'Coordinate 1 field must be filled',
        'coordinate_2.required' => 'Coordinate 2 field must be filled'
    ];

    const INDEX_TITLE = 'Child Sanjunipero';
    const CREATE_TITLE = 'New Child Sanjunipero';
    const EDIT_TITLE = 'Edit Child Sanjunipero';

    const SAVE_SUCCESS = 'Record success to saved.';
    const SAVE_FAIL = 'Sorry, record fail to save.';
    const UPDATE_SUCCESS = 'Record success to update.';
    const UPDATE_FAIL = 'Sorry, record fail to update.';

    const AREA_TYPES = [DynamicLandingChild::AREA_TYPE, DynamicLandingChild::CAMPUS_TYPE];

    public function __construct() {
        View::share('contentHeader', 'User Account Management');
        View::share('user', Auth::user());
    }

    /**
     * Index list parent page
     * 
     * @param Request $request
     * @param DynamicLandingParentRepositoryEloquent $parentRepo
     * @param DynamicLandingChildRepositoryEloquent $childRepo
     * 
     * @return CustomView
     */
    public function index(
        Request $request,
        DynamicLandingParentRepositoryEloquent $parentRepo,
        DynamicLandingChildRepositoryEloquent $childRepo
    ): CustomView {
        $parent_id = $request->get('parent');

        $parents = $parentRepo->getIdTitleHeaderSlug();
        $rows = $childRepo->index();
        if (! empty($parent_id)) {
            $parent = $parentRepo->getWithChildById($parent_id);
            if ($parent instanceof DynamicLandingParent) {
                $rows = $parent->children;
            }
        }

        $data = [
            'boxTitle'           => self::INDEX_TITLE,
            'rows'               => $rows,
            'parents'            => $parents,
            'parent_id'          => $parent_id,
            'sortUrl'            => null,
            'currentQueryString' => null
        ];

        return view('admin.contents.sanjunipero.child.index', $data);
    }

    /**
     * Create new child landing page
     * 
     * @param DynamicLandingParentRepositoryEloquent $parentRepo
     * 
     * @return CustomView
     */
    public function create(
        DynamicLandingParentRepositoryEloquent $parentRepo
    ): CustomView {
        $dynamicLandingChild = new DynamicLandingChild;
        $parents = $parentRepo->getIdTitleHeaderSlug();
        $dynamicLandingChild->parent_id = mySelectPreprocessing(
            'single',
            'selected',
            'key-value',
            $parents,
            Input::old('parent_id')
        );
        $dynamicLandingChild->area_types = self::AREA_TYPES;

        $dynamicLandingChild->area_type = Input::old('area_type');
        $dynamicLandingChild->area_name = Input::old('area_name');
        $dynamicLandingChild->slug = Input::old('slug');
        $dynamicLandingChild->meta_desc = Input::old('meta_desc');
        $dynamicLandingChild->meta_keywords = Input::old('meta_keywords');
        $dynamicLandingChild->is_active = (int) Input::old('is_active');
        $dynamicLandingChild->coordinate_1 = Input::old('coordinate_1');
        $dynamicLandingChild->coordinate_2 = Input::old('coordinate_2');

        if (! empty(Input::old('coordinate_1')) && ! empty(Input::old('coordinate_2'))) {
            list($dynamicLandingChild->latitude_1, $dynamicLandingChild->longitude_1) = explode(',', Input::old('coordinate_1'));
            list($dynamicLandingChild->latitude_2, $dynamicLandingChild->longitude_2) = explode(',', Input::old('coordinate_2'));
        }

        $data = [
            'boxTitle'           => self::CREATE_TITLE,
            'row'                => $dynamicLandingChild,
            'formUrl'            => route('admin.sanjunipero.child.save'),
            'formCancel'         => route('admin.sanjunipero.child.index'),
            'formMethod'         => 'POST'
        ];

        return view('admin.contents.sanjunipero.child.form', $data);
    }

    /**
     * Save data
     * 
     * @param Request $request
     * @param DynamicLandingChildRepositoryEloquent $childRepo
     * 
     * @return RedirectResponse
     */
    public function save(
        Request $request,
        DynamicLandingChildRepositoryEloquent $childRepo
    ): RedirectResponse {
        $parentId = (int) $request->get('parent_id');
        $validator = Validator::make(
            $request->all(),
            [
                'slug' => [
                    'required',
                    new SlugChild($parentId, null, 'save')
                ],
                'parent_id' => [
                    'required'
                ],
                'area_name' => [
                    'required'
                ],
                'coordinate_1' => [
                    'required'
                ],
                'coordinate_2' => [
                    'required'
                ]
            ],
            $this->validationMessages
        );

        if ($validator->fails()) {
            return redirect()->back()
                ->withInput()
                ->withErrors($validator->errors());
        }

        if ($childRepo->save($request)) {
            return redirect()->route('admin.sanjunipero.child.index')->with('message', self::SAVE_SUCCESS);
        }

        return redirect()->route('admin.sanjunipero.child.index')->withErrors(self::SAVE_FAIL);
    }

    /**
     * Edit child landing page
     * 
     * @param DynamicLandingParentRepositoryEloquent $parentRepo
     * @param DynamicLandingChildRepositoryEloquent $childRepo
     * @param int $id
     * 
     * @return CustomView
     */
    public function edit(
        DynamicLandingParentRepositoryEloquent $parentRepo,
        DynamicLandingChildRepositoryEloquent $childRepo,
        int $id
    ): CustomView {
        $parents = $parentRepo->getIdTitleHeaderSlug();
        $data = $childRepo->findById($id);
        $data->parent_id = mySelectPreprocessing(
            'single',
            'selected',
            'key-value',
            $parents,
            $data->parent_id
        );
        $data->area_types = self::AREA_TYPES;
        $data->coordinate_1 = $data->latitude_1.','.$data->longitude_1;
        $data->coordinate_2 = $data->latitude_2.','.$data->longitude_2;

        $data = [
            'boxTitle'           => self::EDIT_TITLE,
            'row'                => $data,
            'formUrl'            => route('admin.sanjunipero.child.update', ['id' => $id]),
            'formCancel'         => route('admin.sanjunipero.child.index'),
            'formMethod'         => 'POST'
        ];

        return view('admin.contents.sanjunipero.child.form', $data);
    }

    /**
     * Update record child landing page
     * 
     * @param Request $request
     * @param DynamicLandingChildRepositoryEloquent $childRepo
     * @param int $id
     * 
     * @return RedirectResponse
     */
    public function update(
        Request $request,
        DynamicLandingChildRepositoryEloquent $childRepo,
        int $id
    ): RedirectResponse {
        $parentId = (int) $request->get('parent_id');
        $validator = Validator::make(
            $request->all(),
            [
                'slug' => [
                    'required',
                    new SlugChild($parentId, $id, 'update')
                ],
                'parent_id' => [
                    'required'
                ],
                'area_name' => [
                    'required'
                ],
                'coordinate_1' => [
                    'required'
                ],
                'coordinate_2' => [
                    'required'
                ]
            ],
            $this->validationMessages
        );

        if ($validator->fails()) {
            return redirect()->back()
                ->withInput()
                ->withErrors($validator->errors());
        }

        if ($childRepo->edit($request, $id)) {
            return redirect()->route('admin.sanjunipero.child.index')->with('message', self::UPDATE_SUCCESS);
        }

        return redirect()->route('admin.sanjunipero.child.index')->withErrors(self::UPDATE_FAIL);
    }

    /**
     * Activate child landing page with certain id
     * 
     * @param DynamicLandingChildRepositoryEloquent $childRepo
     * @param int $id
     * 
     * @return JsonResponse
     */
    public function activate(
        DynamicLandingChildRepositoryEloquent $childRepo,
        int $id
    ): JsonResponse {
        $dynamicLandingChild = $childRepo->activate($id);

        if ($dynamicLandingChild instanceof DynamicLandingChild) {
            return Api::responseData([
                "status" => true,
                "data" => $dynamicLandingChild,
                "message" => self::UPDATE_SUCCESS
            ]);
        }

        return Api::responseData([
            "status" => false,
            "data" => null,
            "message" => self::UPDATE_FAIL
        ]);
    }

    /**
     * Deactivate child landing page
     * 
     * @param DynamicLandingChildRepositoryEloquent $childRepo
     * @param int $Id
     * 
     * @return JsonResponse
     */
    public function deactivate(
        DynamicLandingChildRepositoryEloquent $childRepo,
        int $id
    ): JsonResponse {
        $dynamicLandingChild = $childRepo->deactivate($id);
        
        if ($dynamicLandingChild instanceof DynamicLandingChild) {
            return Api::responseData([
                "status" => true,
                "data" => $dynamicLandingChild,
                "message" => self::UPDATE_SUCCESS
            ]);
        }

        return Api::responseData([
            "status" => false,
            "data" => null,
            "message" => self::UPDATE_FAIL
        ]);
    }

}
