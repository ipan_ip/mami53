<?php

namespace App\Http\Controllers\Admin\Sanjunipero;

use App\Entities\Media\Media;
use App\Entities\Sanjunipero\DynamicLandingParent;
use App\Http\Controllers\Controller;
use App\Http\Helpers\ApiResponse as Api;
use App\Repositories\Sanjunipero\DynamicLandingParentRepositoryEloquent;
use App\Rules\Sanjunipero\EmptyArray;
use App\Rules\Sanjunipero\ImageHeader;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\{
    Auth,
    Validator,
    View
};
use Illuminate\Validation\Validator as ValidatorReturnType;
use Illuminate\View\View as CustomView;
use stdClass;

class DynamicLandingParentController extends Controller
{
    public $validationMessages = [
        'slug.required' => 'Slug field must be filled',
        'type_kost.required' => 'Type Kost field must be filled',
        'title_tag.required' => 'Title Tag field must be filled',
        'title_header.required' => 'Title Header field must be filled',
        'image_url.url' => 'Format url image doesn`t correct'
    ];

    const INDEX_TITLE = 'Parent Sanjunipero';
    const CREATE_TITLE = 'New Parent Sanjunipero';
    const EDIT_TITLE = 'Edit Parent Sanjunipero';

    const SAVE_SUCCESS = 'Record success to saved.';
    const SAVE_FAIL = 'Sorry, record fail to save.';
    const UPDATE_SUCCESS = 'Record success to update.';
    const UPDATE_FAIL = 'Sorry, record fail to update.';

    public function __construct()
    {
        View::share('contentHeader', 'User Account Management');
        View::share('user', Auth::user());
    }

    /**
     * Index list parent page
     * 
     * @param DynamicLandingParentRepositoryEloquent $parentRepo
     * 
     * @return CustomView
     */
    public function index(
        DynamicLandingParentRepositoryEloquent $parentRepo
    ): CustomView {
        $data = [
            'boxTitle'           => self::INDEX_TITLE,
            'rows'               => $parentRepo->index(),
            'sortUrl'            => null,
            'currentQueryString' => null
        ];

        return view('admin.contents.sanjunipero.parent.index', $data);
    }

    /**
     * Create new parent landing page
     * 
     * @return CustomView
     */
    public function create(): CustomView
    {
        $dynamicLandingParent = new DynamicLandingParent;

        $dynamicLandingParent->slug = old('slug');
        $dynamicLandingParent->type_kost = array_map(function($type) {
            $selected = '';
            $oldTypeKost = old('type_kost');
            if (!empty($oldTypeKost)) {
                foreach ($oldTypeKost as $val) {
                    if ($val === $type) {
                        $selected = 'selected';
                    }
                }
            }
            return (object) ['name' => $type, 'selected' => $selected];
        }, DynamicLandingParent::TYPE_KOST);
        $dynamicLandingParent->title_tag = old('title_tag');
        $dynamicLandingParent->title_header = old('title_header');
        $dynamicLandingParent->subtitle_header = old('subtitle_header');
        $dynamicLandingParent->meta_desc = old('meta_desc');
        $dynamicLandingParent->meta_keywords = old('meta_keywords');
        $dynamicLandingParent->image_url = old('image_url');
        $dynamicLandingParent->faq_question = old('faq_question');
        $dynamicLandingParent->faq_answer = old('faq_answer');
        $dynamicLandingParent->is_active = (int) old('is_active');
        $dynamicLandingParent = $this->setTempImage($dynamicLandingParent, 'create');

        $data = [
            'boxTitle'           => self::CREATE_TITLE,
            'row'                => $dynamicLandingParent,
            'formUrl'            => route('admin.sanjunipero.parent.save'),
            'formCancel'         => route('admin.sanjunipero.parent.index'),
            'formMethod'         => 'POST'
        ];

        return view('admin.contents.sanjunipero.parent.form', $data);
    }

    /**
     * Save data
     * 
     * @param Request $request
     * @param DynamicLandingParentRepositoryEloquent $parentRepo
     * 
     * @return RedirectResponse
     */
    public function save(
        Request $request,
        DynamicLandingParentRepositoryEloquent $parentRepo
    ): RedirectResponse {
        $validator = $this->setValidation($request, 'save');

        if ($validator->fails()) {
            return redirect()->back()
                ->withInput()
                ->withErrors($validator->errors());
        }

        if ($parentRepo->save($request)) {
            return redirect()->route('admin.sanjunipero.parent.index')->with('message', self::SAVE_SUCCESS);
        }

        return redirect()->route('admin.sanjunipero.parent.index')->withErrors(self::SAVE_FAIL);
    }

    /**
     * Edit page
     * 
     * @param DynamicLandingParentRepositoryEloquent $parentRepo
     * @param int $id
     * 
     * @return CustomView
     */
    public function edit(
        DynamicLandingParentRepositoryEloquent $parentRepo,
        int $id
    ): CustomView {
        $data = $parentRepo->findById($id);

        $data->type_kost = $this->preProcessingMultiTypeKost(
            DynamicLandingParent::TYPE_KOST,
            explode(',', $data->type_kost)
        );

        $data = $this->setTempImage($data, 'edit');

        $data = [
            'boxTitle'           => self::EDIT_TITLE,
            'row'                => $data,
            'formUrl'            => route('admin.sanjunipero.parent.update', ['id' => $id]),
            'formCancel'         => route('admin.sanjunipero.parent.index'),
            'formMethod'         => 'POST'
        ];

        return view('admin.contents.sanjunipero.parent.form', $data);
    }

    /**
     * Update record
     * 
     * @param Request $request
     * @param DynamicLandingParentRepositoryEloquent $parentRepo
     * @param int $id
     * 
     * @return RedirectResponse
     */
    public function update(
        Request $request,
        DynamicLandingParentRepositoryEloquent $parentRepo,
        int $id
    ): RedirectResponse {
        $validator = $this->setValidation($request);

        if ($validator->fails()) {
            return redirect()->back()
                ->withInput()
                ->withErrors($validator->errors());
        }

        if ($parentRepo->edit($request, $id)) {
            return redirect()->route('admin.sanjunipero.parent.index')->with('message', self::UPDATE_SUCCESS);
        }

        return redirect()->route('admin.sanjunipero.parent.index')->withErrors(self::UPDATE_FAIL);
    }

    /**
     * Activate record with certain id
     * 
     * @param DynamicLandingParentRepositoryEloquent $parentRepo
     * @param int $id
     * 
     * @return JsonResponse
     */
    public function activate(
        DynamicLandingParentRepositoryEloquent $parentRepo,
        int $id
    ): JsonResponse {
        $dynamicLandingParent = $parentRepo->activate($id);

        if ($dynamicLandingParent instanceof DynamicLandingParent) {
            return Api::responseData([
                "status" => true,
                "data" => $dynamicLandingParent,
                "message" => self::UPDATE_SUCCESS
            ]);
        }

        return Api::responseData([
            "status" => false,
            "data" => null,
            "message" => self::UPDATE_FAIL
        ]);
    }

    /**
     * Deactivate record
     * 
     * @param DynamicLandingParentRepositoryEloquent $parentRepo
     * @param int $id
     * 
     * @return JsonResponse
     */
    public function deactivate(
        DynamicLandingParentRepositoryEloquent $parentRepo,
        int $id
    ): JsonResponse {
        $dynamicLandingParent = $parentRepo->deactivate($id);
        
        if ($dynamicLandingParent instanceof DynamicLandingParent) {
            return Api::responseData([
                "status" => true,
                "data" => $dynamicLandingParent,
                "message" => self::UPDATE_SUCCESS
            ]);
        }

        return Api::responseData([
            "status" => false,
            "data" => null,
            "message" => self::UPDATE_FAIL
        ]);
    }

    /**
     * Preprocess Type Kost to make it appear in multi-select
     * 
     * @param array $types
     * @param array @result
     * 
     * @return stdClass
     */
    private function preProcessingMultiTypeKost(array $types, array $result): stdClass
    {
        return (object) array_map(function($type) use ($result) {
            $selected = '';
            if (in_array($type, $result)) {
                $selected = 'selected';
            }
            return (object) ['name' => $type, 'selected' => $selected];
        }, $types);
    }

    /**
     * Set validation
     * 
     * @param Request $request
     * @param string $method
     * 
     * @return Validator
     */
    private function setValidation(Request $request, string $method = 'update'): ValidatorReturnType
    {
        return Validator::make(
            $request->all(),
            [
                'slug' => $this->setSlugValidationRule($method),
                'type_kost' => [
                    'required'
                ],
                'title_tag' => [
                    'required'
                ],
                'title_header' => [
                    'nullable',
                    'string'
                ],
                'image_url' => [
                    'url'
                ],
                'faq_question' => [
                    new EmptyArray
                ],
                'faq_answer' => [
                    new EmptyArray
                ],
                'desktop_header_media_id' => [
                    new ImageHeader($request)
                ],
                'mobile_header_media_id' => [
                    new ImageHeader($request)
                ]
            ],
            $this->validationMessages
        );
    }

    /**
     * Set rule for slug
     * 
     * @param string $method
     * 
     * @return array
     */
    private function setSlugValidationRule(string $method): array
    {
        if ($method === 'save') {
            return [
                    'required',
                    'unique:dynamic_landing_parents,slug'
                ];
        }

        return ['required'];
    }

    /**
     * Set temp image when validation fail
     * 
     * @param DynamicLandingParentRepositoryEloquent $data
     * @param string $event
     * 
     * @return DynamicLandingParent
     */
    private function setTempImage(
        DynamicLandingParent $data,
        string $event='create'
    ): DynamicLandingParent {
        if ($event === 'create') {
            if (!empty(old('desktop_header_media_id'))) {
                $media = Media::find(old('desktop_header_media_id'));
                $desktop = $media->getMediaUrl();

                $data->desktop_header_media_id = old('desktop_header_media_id');
                $data->desktop_header_photo = $desktop['medium'];
            }

            if (!empty(old('mobile_header_media_id'))) {
                $media = Media::find(old('mobile_header_media_id'));
                $mobile = $media->getMediaUrl();

                $data->mobile_header_media_id = old('mobile_header_media_id');
                $data->mobile_header_photo = $mobile['medium'];
            }

            return $data;
        }

        $sizeType = 'small';
        if (!empty($data->desktop_header_media_id)) {
            $data->desktop_header_media_id = old('desktop_header_media_id') ?? $data->desktop_header_media_id;
            $data->desktop_header_photo = DynamicLandingParent::setSanjuniperoSize(
                $data->desktop_header_photo->getMediaUrl(),
                Media::SANJUNIPERO_HEADER_DESKTOP
            )[$sizeType];
        }

        if (!empty($data->mobile_header_media_id)) {
            $data->mobile_header_media_id = old('mobile_header_media_id') ?? $data->mobile_header_media_id;
            $data->mobile_header_photo = DynamicLandingParent::setSanjuniperoSize(
                $data->mobile_header_photo->getMediaUrl(),
                Media::SANJUNIPERO_HEADER_MOBILE
            )[$sizeType];
        }

        return $data;
    }

}
