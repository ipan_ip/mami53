<?php

namespace App\Http\Controllers\Admin\RewardLoyalty;

use App\Entities\Activity\Action;
use App\Entities\Activity\ActivityLog;
use App\Entities\Reward\RewardType;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Validator;

class RewardTypeController extends Controller
{
    private const INDEX_ROUTE = 'admin.reward-loyalty.type.index';
    private const PER_PAGE = 20;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!Auth::user()->can('access-reward-loyalty-core')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        View::share('contentHeader', 'Reward Type Management');
        View::share('user', Auth::user());
    }

    public function index(Request $request)
    {
        $query = RewardType::query();

        if ($request->filled('q')) {
            $query->where('key', 'like', '%' . $request->input('q') . '%');
        }

        $type = $query->paginate(self::PER_PAGE);

        ActivityLog::LogIndex(Action::REWARD_TYPE);

        $viewData = [
            'type' => $type,
            'boxTitle' => 'Manage Reward Type'
        ];
        return view('admin.contents.reward-loyalty.type.index', $viewData);
    }

    public function show(Request $request, RewardType $type)
    {
        abort(404);
    }

    public function create()
    {
        $viewData = [
            'boxTitle' => 'Create Reward Type'
        ];
        return view('admin.contents.reward-loyalty.type.edit', $viewData);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'key' => 'required|max:50|alpha_dash',
            'name' => 'required|max:50'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        $exist = RewardType::where('key', $request->input('key'))->withTrashed()->first();
        if (!is_null($exist)) {
            return redirect()->back()->withInput()->with('errors', $validator->getMessageBag()->add('key', 'Key already exist.'));
        }

        $type = new RewardType();
        $type->fill($request->all());
        $type->save();

        ActivityLog::LogCreate(Action::REWARD_TYPE, $type->id);

        return redirect()->route(self::INDEX_ROUTE, ['#reward-loyalty'])->with('message', 'Reward Type successfully added.');
    }

    public function edit(RewardType $type)
    {
        $viewData = [
            'type' => $type,
            'boxTitle' => 'Edit Reward Type'
        ];
        return view('admin.contents.reward-loyalty.type.edit', $viewData);
    }

    public function update(Request $request, RewardType $type)
    {
        $validator = Validator::make($request->all(), [
            'key' => 'required|max:50|alpha_dash',
            'name' => 'required|max:50'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        $exist = RewardType::where('key', $request->input('key'))->withTrashed()->first();
        if (!is_null($exist) && $exist->id !== $type->id) {
            return redirect()->back()->withInput()->with('errors', $validator->getMessageBag()->add('key', 'Key already exist.'));
        }

        $type->fill($request->all());
        $type->save();

        ActivityLog::LogUpdate(Action::REWARD_TYPE, $type->id);

        return redirect()->route(self::INDEX_ROUTE, ['#reward-loyalty'])->with('message', 'Reward Type successfully updated.');
    }

    public function destroy($id)
    {
        $type = RewardType::find($id);
        if (is_null($type)) {
            abort(404);
        }

        $type->delete();

        ActivityLog::LogDelete(Action::REWARD_TYPE, $id);

        return redirect()->route(self::INDEX_ROUTE, ['#reward-loyalty'])->with('message', 'Reward Type successfully deleted.');
    }
}
