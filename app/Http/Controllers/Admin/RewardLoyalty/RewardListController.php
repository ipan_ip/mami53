<?php

namespace App\Http\Controllers\Admin\RewardLoyalty;

use App\Entities\Level\KostLevel;
use App\Entities\Media\Media;
use App\Entities\Reward\Reward;
use App\Entities\Reward\RewardQuota;
use App\Entities\Reward\RewardRedeem;
use App\Entities\Reward\RewardRedeemStatusHistory;
use App\Entities\Reward\RewardTargetConfig;
use App\Entities\Reward\RewardType;
use App\Http\Controllers\Controller;
use App\Repositories\Reward\RewardRepository;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\MessageBag;

class RewardListController extends Controller
{
    private const PER_PAGE = 20;

    protected $repo;

    public function __construct(RewardRepository $repo)
    {
        $this->middleware(function ($request, $next) {
            if (!$request->user()->can(['access-reward-loyalty', 'view-reward-loyalty'])) {
                return redirect('admin')->withErrors('You dont have permission to this page "'.request()->path().'".');
            }
            
            return $next($request);
        });

        $this->middleware(function ($request, $next) {
            if (!$request->user()->can(['access-reward-loyalty'])) {
                return redirect('admin')->withErrors('You dont have permission to this page "'.request()->path().'".');
            }

            return $next($request);
        })->only(['create', 'store', 'edit', 'update', 'updateRedeemStatus']);

        $this->repo = $repo;

        View::share('contentHeader', 'Reward List Management');
        View::share('user', Auth::user());
    }

    public function index(Request $request)
    {
        $rewards = $this->repo->getListForAdmin(self::PER_PAGE, $request->all());
        $userTargets = Reward::getUserTargets();
        $pageTitle = 'Reward List Management';
        $viewData = [
            'boxTitle' => $pageTitle,
            'pageTitle' => $pageTitle,
            'rewards' => $rewards,
            'user_targets' => $userTargets
        ];

        return view('admin.contents.reward-loyalty.list.index', $viewData);
    }

    public function show(Reward $reward, Request $request)
    {
        $redeems = $this->repo->getRewardRedeemList($reward, self::PER_PAGE, $request->all());
        $pageTitle = 'Redemption of Reward "'. $reward->name .'"';
        $viewData = [
            'boxTitle'        => $pageTitle,
            'pageTitle'       => $pageTitle,
            'reward'          => $reward,
            'redeemedCount'   => $reward->getRedeemedCount(),
            'redeems'         => $redeems,
            'status_dropdown' => RewardRedeem::getStatusDropdown(),
        ];

        return view('admin.contents.reward-loyalty.list.show', $viewData);
    }

    public function rewardHistory(Reward $reward)
    {
        $histories = $this->repo->getRewardRevisions($reward);
        $pageTitle = 'Reward History ['. $reward->name .']';
        $viewData = [
            'boxTitle' => $pageTitle,
            'pageTitle' => $pageTitle,
            'reward' => $reward,
            'histories' => $histories
        ];
        return view('admin.contents.reward-loyalty.list.reward_history', $viewData);
    }

    public function redeemHistory(Reward $reward, $redeemId)
    {
        $redeem = RewardRedeem::findOrFail($redeemId);
        $histories = $redeem->status_history()->with('user')->latest()->paginate(self::PER_PAGE);
        $pageTitle = 'Redeem History ['. $redeem->user->name .']';
        $viewData = [
            'boxTitle' => $pageTitle,
            'pageTitle' => $pageTitle,
            'reward' => $reward,
            'redeem' => $redeem,
            'histories' => $histories
        ];
        return view('admin.contents.reward-loyalty.list.redeem_history', $viewData);
    }

    public function updateRedeemStatus(Reward $reward, $redeemId, Request $request)
    {
        $redeem = RewardRedeem::findOrFail($redeemId);
        $statusList = implode(',', RewardRedeem::getStatusList());
        $validator = Validator::make($request->all(), [
            'status' => 'required|in:'. $statusList,
            'notes' => 'max:30|required_if:status,'. RewardRedeem::STATUS_FAILED,
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        try {
            $this->repo->updateRedeemStatus(
                $reward,
                $redeem,
                $request->get('status'),
                $request->user(),
                $request->get('notes')
            );
        } catch (\Throwable $th) {
            $errMessage = $th->getMessage() ?? 'An error has been occurred.';
            return redirect()->route('admin.reward-loyalty.list.show', $reward->id)
                ->with('error_message', $errMessage);
        }

        return redirect()->route('admin.reward-loyalty.list.show', $reward->id)
            ->with('message', 'Reward was updated successfully.');
    }

    public function create()
    {
        $typeDropdown = RewardType::getAsDropdown();
        $userTargets = Reward::getUserTargets();
        $ownerSegments = RewardTargetConfig::getOwnerSegments();
        $gpLevels = KostLevel::getGpLevelKeyIds();
        $pageTitle = 'Add Reward';
        $viewData = [
            'boxTitle' => $pageTitle,
            'pageTitle' => $pageTitle,
            'type_dropdown' => $typeDropdown,
            'user_targets' => $userTargets,
            'owner_segments' => $ownerSegments,
            'gp_levels' => $gpLevels,
            'media_type' => Media::REWARD_TYPE,
        ];
        return view('admin.contents.reward-loyalty.list.create', $viewData);
    }

    public function store(Request $request)
    {
        $validator = $this->getValidator($request);
        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        $request = $this->fixRequest($request);

        $this->repo->createReward($request->all());

        return redirect()->route('admin.reward-loyalty.list.index')
            ->with('message', 'Reward successfully added.');
    }

    public function edit(Reward $reward)
    {
        $reward->load('quotas', 'redeems', 'target_configs');
        $reward->start_date = Carbon::parse($reward->start_date)->toDateString();
        $reward->end_date = Carbon::parse($reward->end_date)->toDateString();
        $reward->quota = [
            'total' => optional($reward->quotas->where('type', 'total')->first())->value,
            'daily' => optional($reward->quotas->where('type', 'daily')->first())->value,
            'total_user' => optional($reward->quotas->where('type', 'total_user')->first())->value,
            'daily_user' => optional($reward->quotas->where('type', 'daily_user')->first())->value,
        ];

        $typeDropdown = RewardType::getAsDropdown();
        $userTargets = Reward::getUserTargets();
        $ownerSegments = RewardTargetConfig::getOwnerSegments();
        $gpLevels = KostLevel::getGpLevelKeyIds();
        $pageTitle = 'Update Reward "'. $reward->name .'"';
        $viewData = [
            'reward' => $reward,
            'redeemedCount' => $reward->getRedeemedCount(),
            'boxTitle' => $pageTitle,
            'pageTitle' => $pageTitle,
            'type_dropdown' => $typeDropdown,
            'user_targets' => $userTargets,
            'owner_segments' => $ownerSegments,
            'gp_levels' => $gpLevels,
            'media_type' => Media::REWARD_TYPE,
        ];
        return view('admin.contents.reward-loyalty.list.edit', $viewData);
    }
    
    public function update(Reward $reward, Request $request)
    {
        $validator = $this->getValidator($request);
        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        // Make sure total quota value higher than redeemed count
        $redeemedCount = $reward->getRedeemedCount();
        if ($redeemedCount > $request->input('quota.total')) {
            return redirect()->back()->withInput()->with(
                'errors', (new MessageBag)->add('quota.total', 'Total Quota should higher than redeemed count')
            );
        }

        $request = $this->fixRequest($request);

        $this->repo->updateReward($reward, $request->all());

        return redirect()->route('admin.reward-loyalty.list.index')
            ->with('message', 'Reward "'. $reward->name .'" successfully updated.');
    }

    protected function getValidator(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:55',
            'description' => 'required',
            'start_date' => 'required|date',
            'end_date' => 'required|date',
            'quota.total' => 'required|gt:0',
            'quota.total_user' => 'required|gt:0',
            'quota.daily' => 'lte:quota.total|gt:0',
            'quota.daily_user' => 'lte:quota.total_user|gt:0',
            'type_id' => 'required|not_in:0',
            'redeem_value' => 'required|gt:0',
            'user_csv' => 'file|mimes:csv,txt',
            'media_id' => 'required_if:is_published,1',
            'tnc' => 'required_if:is_published,1',
            'howto' => 'required_if:is_published,1',
            'user_target' => 'required|in:'. implode(",", array_keys(Reward::getUserTargets())),
            'sequence' => 'required|integer|min:0',
        ], [
            'name.max' => 'Reward Name length must be less than or equal to 55 characters',
            'quota.total.gt' => 'Total Quota must be greater than 0',
            'quota.total_user.gt' => 'Total Each User Quota must be greater than 0',
            'quota.daily.lte' => 'Daily Quota must be less than or equal to Total Quota',
            'quota.daily_user.lte' => 'Daily Each User Quota must be less than or equal to Total Each User Quota',
            'quota.daily.gt' => 'Daily Quota must be greater than 0',
            'quota.daily_user.gt' => 'Daily Each User Quota must be greater than 0',
            'redeem_value.gt' => 'Redemption Point must be greater than 0',
        ], [
            'name' => 'Reward Name',
            'description' => 'Description',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'quota.total' => 'Total Quota',
            'quota.total_user' => 'Total Each User Quota',
            'quota.daily' => 'Daily Quota',
            'quota.daily_user' => 'Daily Each User Quota',
            'type_id' => 'Reward Type',
            'redeem_value' => 'Redeemption Point',
            'user_csv' => 'Targeted User',
            'media_id' => 'Image',
            'tnc' => 'Terms and Conditions',
            'howto' => 'How to Use',
            'user_target' => 'Target',
            'sequence' => 'Sequence/Order on Reward List',
        ]);
        
        return $validator;
    }

    protected function fixRequest(Request $request)
    {
        $startDate = Carbon::parse(request('start_date'))->startOfDay()->toDateTimeString();
        $endDate = Carbon::parse(request('end_date'))->endOfDay()->toDateTimeString();
        $request->offsetSet('start_date', $startDate);
        $request->offsetSet('end_date', $endDate);
        $request->offsetSet('redeem_currency', 'point');
        $request->offsetSet('is_active', $request->get('is_active', 0));
        $request->offsetSet('is_published', $request->get('is_published', 0));
        $request->offsetSet('is_testing', $request->get('is_testing', 0));

        $userCsv = $request->file('user_csv');
        if (optional($userCsv)->isValid()) {
            $request->offsetSet('target_user_ids', $this->extractUserIdsCsv($userCsv));
        }

        return $request;
    }

    protected function extractUserIdsCsv(UploadedFile $userCsv)
    {
        $userIds = [];
        if ($userCsv) {
            $file = fopen($userCsv, 'r');
            if ($file) {
                while (($data = fgetcsv($file)) !== false) {
                    $userIds[] = $data[0];
                }
            }
            fclose($file);
        }

        array_shift($userIds);

        // Get valid user ids
        return User::whereIn('id', $userIds)
            ->get()
            ->pluck('id')
            ->toArray();
    }
}
