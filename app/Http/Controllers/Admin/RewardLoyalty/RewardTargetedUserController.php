<?php

namespace App\Http\Controllers\Admin\RewardLoyalty;

use App\Entities\Reward\Reward;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RewardTargetedUserController extends Controller
{
    /**
     * Define an instance of Reward Targeted User controller.
     * 
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!$request->user()->can(['access-reward-loyalty', 'view-reward-loyalty'])) {
                return redirect('admin')->withErrors('You dont have permission to this page "'.request()->path().'".');
            }
            
            return $next($request);
        });

        $this->middleware(function ($request, $next) {
            if (!$request->user()->can(['access-reward-loyalty'])) {
                return redirect('admin')->withErrors('You dont have permission to this page "'.request()->path().'".');
            }

            return $next($request);
        })->only(['remove']);
    }

    /**
     * Remove all reward targeted users.
     * 
     * @param  int  $rewardId
     * @param  Request  $request
     * @return mixed
     */
    public function remove(int $rewardId, Request $request)
    {
        $reward = Reward::findOrFail($rewardId);
        $reward->target()->detach();

        return redirect()->to(route('admin.reward-loyalty.list.edit', $rewardId).'#reward-loyalty')
            ->with('message', 'Reward was updated successfully.');
    }
}
