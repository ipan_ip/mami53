<?php

namespace App\Http\Controllers\Admin\RolePermission;

use App\Http\Helpers\ApiHelper;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Auth;
use App\Entities\Entrust\Permission;
use App\Entities\Entrust\Role;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;
use Validator;
use Illuminate\Validation\Rule;

class PermissionController extends Controller
{

    public function __construct() 
    {
        $this->middleware(function ($request, $next) {
            if(!Auth::user()->can('access-permission')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        View::share('contentHeader', 'Permission Management');
        View::share('user', Auth::user());
    }

    public function index(Request $request)
    {
        $query = Permission::query();

        if($request->filled('q') && $request->get('q') != '') {
            $query = $query->where('name', 'like', '%' . $request->get('q') . '%')
                        ->orWhere('display_name', 'like', '%' . $request->get('q') . '%');
        }

        $permissions = $query->paginate(20);

        $viewData    = array(
        	'boxTitle'    =>  'List Permissions',
        	'permissions' => $permissions
        ); 

        ActivityLog::LogIndex(Action::PERMISSION);

        return view ("admin.contents.permission.index", $viewData);
    }

    public function create(Request $request)
    {
    	$viewData = [
    		'boxTitle'    => 'Create Permission'
    	];

    	return view('admin.contents.permission.create', $viewData);
    }

    public function store(Request $request)
    {
    	$validator = Validator::make($request->all(), [
    		'name'          => 'required|unique:user_permission|max:100',
            'display_name'  => 'required|max:250'
        ]);

        if($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        $permission 				= new Permission;
        $permission->name 			= $request->input('name');
        $permission->display_name   = $request->input('display_name');
        $permission->save();

        ActivityLog::LogCreate(Action::PERMISSION, $permission->id);

        return redirect()->route('admin.permission.index')->with('messages', 'Permission berhasil ditambahkan');
    }

    public function edit(Request $request, $id)
    {
        $permission = Permission::find($id);

        $viewData = [
            'boxTitle'        => 'Edit Permission',
            'permission'      => $permission
        ];

        return view('admin.contents.permission.edit', $viewData);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
        	'name'          => [
        		'required',
                Rule::unique('user_permission')->ignore($id, 'id'),
                'max:100'
            ],
            'display_name'  => 'required|max:250'
        ]);

        if($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        $permission = Permission::find($id);

        $permission->name         = $request->input('name');
        $permission->display_name = $request->input('display_name');
        $permission->save();

        ActivityLog::LogUpdate(Action::PERMISSION, $id);

        return redirect()->route('admin.permission.index')->with('message', 'Permission berhasil dirubah');
    }
}
