<?php

namespace App\Http\Controllers\Admin\RolePermission;

use App\Http\Helpers\ApiResponse;
use App\Entities\Activity\ActivityLog;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Auth;
use App\Entities\Entrust\Role;
use App\Entities\Entrust\Permission;
use Validator;
use Illuminate\Validation\Rule;
use App\User;
use Illuminate\Support\Facades\Hash;
use Cache;
use Illuminate\Cache\TaggableStore;
use Config;
use App\Libraries\ArtisanHelper;
use App\Entities\User\UserRole;
use RuntimeException;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use App\Entities\Activity\Action;

class RoleController extends Controller
{

    public function __construct() 
    {
        $this->middleware(function ($request, $next) {
            if(!Auth::user()->can('access-role')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        View::share('contentHeader', 'Role Management');
        View::share('user', Auth::user());
    }

    public function index(Request $request)
    {
        $roles = Role::query();
        $roles->searchByUserNameOrEmail($request->input('user'));
        $roles->searchByPermission($request->input('permission'));
        $roles = $roles->paginate(20);

        $viewData = array(
            'boxTitle'  => 'List Roles',
            'roles'     => $roles,
        );

        ActivityLog::LogIndex(Action::ROLE);

        return view("admin.contents.roles.index", $viewData);
    }

    public function create(Request $request)
    {
        $viewData = [
            'boxTitle'          => 'Create Role'
        ];

        return view('admin.contents.roles.create', $viewData);
    }

    public function show(Request $request)
    {
        abort(404);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'          => 'required|unique:user_role_2|max:100',
            'display_name'  => 'required|max:250'
        ]);

        if($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        $role = new Role;
        $role->name = $request->input('name');
        $role->display_name = $request->input('display_name');
        $role->save();

        ActivityLog::LogCreate(Action::ROLE, $role->id);

        return redirect()->route('admin.role.index')->with('message', 'Role berhasil ditambahkan');
    }

    public function edit(Request $request, $id)
    {
        $role = Role::findOrFail($id);

        $viewData = [
            'boxTitle'  => 'Edit Role',
            'role'      => $role
        ];

        return view('admin.contents.roles.edit', $viewData);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name'          => [
                'required',
                Rule::unique('user_role_2')->ignore($id, 'id'),
                'max:100'
            ],
            'display_name'  => 'required|max:250'
        ]);

        if($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        $role = Role::findOrFail($id);

        $role->name = $request->input('name');
        $role->display_name = $request->input('display_name');
        $role->save();

        ActivityLog::LogUpdate(Action::ROLE, $id);

        return redirect()->route('admin.role.index')->with('message', 'Role berhasil dirubah');
    }

    public function roleUsers(Request $request, $roleId)
    {
        $role = Role::findOrFail($roleId);

        $users = $role->users()->get();

        $viewData = [
            'boxTitle'  => 'User Role for ' . $role->display_name,
            'role'      => $role,
            'users'     => $users
        ];

        return view('admin.contents.roles.users', $viewData);
    }

    public function attachRole(Request $request, $roleId)
    {
        $role = Role::findOrFail($roleId);

        $viewData = [
            'boxTitle'  => 'Attach Role for ' . $role->display_name,
            'role'      => $role,
        ];

        return view('admin.contents.roles.attach', $viewData);
    }

    public function createRole(Request $request, $roleId)
    {
        $role = Role::findOrFail($roleId);

        $viewData = [
            'boxTitle'  => 'Create User Role for ' . $role->display_name,
            'role'      => $role,
        ];

        return view('admin.contents.roles.users-create', $viewData);
    }

    public function createRoleStore(Request $request, $roleId)
    {
        $validator = Validator::make($request->all(), [
                'name'                  => 'required|max:255',
                'email'                 => 'required|email|unique:user|max:150',
                'phone_number'          => 'nullable|numeric|unique:user',
                'password'              => 'required|confirmed|min:8',
            ]);

        if($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        $user                 = new User;
        $user->name           = $request->input('name');
        $user->email          = $request->input('email');
        $user->phone_number   = $request->input('phone_number');
        $user->password       = Hash::make(md5($request->input('password')));
        $user->is_verify      = '0';
        $user->role           = UserRole::Administrator;
        $user->save();

        ActivityLog::LogCreate(Action::ADMIN_USER, $user->id);

        $role = Role::findOrFail($roleId);
        $user->attachRole($role);

        if(Cache::getStore() instanceof TaggableStore) {
            Cache::tags(Config::get('entrust.role_user_table'))->flush();
        }

        return redirect()->route('admin.role.users', $role->id)->with('message', 'User berhasil ditambahkan');
    }

    public function attachRolePost(Request $request, $roleId)
    {
        $validator = Validator::make($request->all(), [
                'user_id' => [
                    'required',
                    Rule::unique('user_role')->where(function ($query) use ($roleId) {
                        return $query->where('role_id', $roleId);
                    }),
                ],
            ]);

        if($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        $role = Role::findOrFail($roleId);

        $user = User::findOrFail($request->user_id);

        $user->attachRole($role);

        if(Cache::getStore() instanceof TaggableStore) {
            Cache::tags(Config::get('entrust.role_user_table'))->flush();
        }

        return redirect()->route('admin.role.users', $role->id)->with('message', 'User berhasil ditambahkan');
    }

    public function detachRole(Request $request, $roleId, $userId)
    {
        $role = Role::findOrFail($roleId);

        $user = User::findOrFail($request->user_id);

        $user->detachRole($role);

        if(Cache::getStore() instanceof TaggableStore) {
            Cache::tags(Config::get('entrust.role_user_table'))->flush();
        }

        return redirect()->route('admin.role.users', $role->id)->with('message', 'User berhasil dihapus');
    }

     public function changePasswordUser(Request $request, $roleId, $userId)
    {
        $role = Role::findOrFail($roleId);
        $user = User::findOrFail($userId);

        $viewData = [
            'boxTitle'  => 'Change password for ' . $role->display_name,
            'role'      => $role,
            'user'      => $user
        ];

        return view('admin.contents.roles.change-password', $viewData);
    }

    public function updatePasswordUser(Request $request, $roleId, $userId)
    {
       $validator = Validator::make($request->all(), [
                'password'              => 'required|min:8',
                'password_confirmation' => 'required_with:password|same:password|min:8',
            ]);

        if($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        $user                 = User::findOrFail($userId);
        $user->password       = Hash::make(md5($request->input('password')));
        $user->save();

        $role = Role::findOrFail($roleId);

        ActivityLog::LogUpdate(Action::ROLE, $userId);

        return redirect()->route('admin.role.users', $role->id)->with('message', 'Password berhasil diubah');
    }

    public function getUserForRole(Request $request)
    {
        if($request->params == '') {
            return ApiResponse::responseData([
                'users' => []
            ]);
        }

        // we MUST limit to role=administrator
        $users = User::where(
            function($q) use($request) {
                $q->where('name', 'like', '%' . $request->params . '%')
                ->orWhere('email', 'like', '%' . $request->params . '%')
                ->orWhere('phone_number', '%' . $request->params . '%');
            })
            ->whereDoesntHave('roles', function ($query) use ($request) {
                $query->where('role_id', $request->role_id);
            })
            ->where('role', UserRole::Administrator)
            ->take(10)
            ->get();

        $userArray = [];

        if($users) {
            foreach($users as $user) {
                $userArray[$user->id] = $user->id . ' | ' . $user->name . ' | ' . $user->email . ' | ' . $user->phone_number;
            }
        }

        return ApiResponse::responseData([
            'users' => $userArray
        ]);
    }

    public function permissionRoles(Request $request, $roleId)
    {
        $role = Role::findOrFail($roleId);

        $permissions = $role->perms()->get();

        $viewData = [
            'boxTitle'      => 'Permission Roles for ' . $role->name,
            'permissions'   => $permissions,
            'role'          => $role
        ];

        return view('admin.contents.roles.permissions', $viewData);
    }

    public function attachPermission(Request $request, $roleId)
    {
        $role = Role::findOrFail($roleId);
        $attachedPermission = $role->perms()->get()->pluck('id')->toArray();
        $allPermissions = Permission::whereNotIn('id', $attachedPermission)->get();

        $viewData = [
            'boxTitle'      => 'Attach Permission Roles for ' . $role->name,
            'permissions'   => $allPermissions,
            'role'          => $role
        ];

        return view('admin.contents.roles.attach-permission', $viewData);
    }

    public function attachPermissionPost(Request $request, $roleId)
    {
        $role = Role::findOrFail($roleId);

        $permission = Permission::findOrFail($request->permission_id);

        try {
            $role->attachPermission($permission);
        } catch(\Exception $e) {
            return redirect()->back()->with('error_message', 'Permission telah ditambahkan sebelumnya');
        }    

        return redirect()->route('admin.role.permissions', $role->id)->with('message', 'Permission berhasil ditambahkan');
        
    }

    public function detachPermission(Request $request, $roleId, $permissionId)
    {
        $permission = Permission::findOrFail($permissionId);

        $role = Role::findOrFail($roleId);

        $role->detachPermission($permission);

        return redirect()->route('admin.role.permissions', $role->id)->with('message', 'Permission berhasil dihapus');
    }

    public function duplicate(Request $request, $roleId)
    {
        $role = Role::findOrFail($roleId);

        $availableRoles = Role::where('id', '<>', $roleId)->get();

        $viewData = [
            'boxTitle'      => 'Duplicate Permission Roles for ' . $role->name,
            'availableRoles'=> $availableRoles,
            'role'          => $role
        ];

        return view('admin.contents.roles.duplicate', $viewData);
    }

    public function duplicatePost(Request $request, $roleId)
    {
        $role = Role::findOrFail($roleId);

        $sourceRole = Role::findOrFail($request->input('role_id'));

        $sourcePermissions = $sourceRole->perms()->get();

        if($sourcePermissions) {
            foreach($sourcePermissions as $sourcePermission) {
                $role->attachPermission($sourcePermission);
            }
        }

        return redirect()->route('admin.role.permissions', $role->id)->with('message', 'Permission telah berhasi diduplikat');
    }

    public function destroy(Request $request, Role $role) {
        if ($role->user_count !== 0) {
            return [
                'sucess' => false,
                'message' => 'Role yang ingin dihapus tidak boleh memiliki user',
            ];
        }

        try {
            $role->delete();
        }
        catch(\Exception $e) {
            Bugsnag::notifyException($e);
        }
        
        ActivityLog::LogDelete(Action::ROLE, $role->id);

        return [
            'success' => true,
            'message' => 'Role berhasil dihapus',
        ];
    }

    public function flushRoleAndPermission(Request $request)
    {
        $response = [];
        $response['result'] = false;

        try
        {
            ArtisanHelper::clearCache();

            $response['result'] = true;

            ActivityLog::Log(Action::FLUSH_ROLE_PERMISSION, "Flushed roles and permissions success");
        }
        catch (RuntimeException $e)
        {
            ActivityLog::Log(Action::FLUSH_ROLE_PERMISSION, "Flushed roles and permissions fail");
            $response['errorMessage'] = $e->getMessage();
        }

        return response()->json($response);
    }
}
