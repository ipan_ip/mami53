<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\Room\Room;
use App\Entities\Room\Element\Report;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;
use View;
use DB;
use Auth;

class ReportController extends Controller
{

	public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if(!Auth::user()->can('access-kost-report')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        View::share('contentHeader', 'Room Report Management');
        View::share('user', Auth::user());
    }

    public function index(Request $request)
    {
        $sortTypes = [
            'date_desc'=>'Tanggal Terbaru',
            'date_asc'=>'Tanggal Terlama',
            'count_desc'=>'Jumlah Terbanyak',
            'count_asc'=>'Jumlah Tersedikit'
        ];

        $queryReportedRoomIds = Report::select(DB::raw('DISTINCT designer_id'));

        if($request->filled('sort') && $request->get("sort") != '') {
            list($sortCol, $sortDir) = explode('_', $request->get('sort'));

            if($sortCol == 'date') {
                $queryReportedRoomIds = $queryReportedRoomIds->orderBy('created_at', $sortDir);
            }
        } else {
            $queryReportedRoomIds = $queryReportedRoomIds->orderBy('created_at', 'desc');
        }

        $reportedRoomIds = $queryReportedRoomIds->groupBy('designer_id')
                                ->pluck('designer_id')
                                ->toArray();

        $query = Room::select('id', 'name', 'song_id')
                    ->withCount('report_summary')
                    ->whereIn('id', $reportedRoomIds);

        if($request->filled('q') && $request->get('q') != '') {
            $query = $query->where('name', 'like', '%' . $request->get('q') . '%');
        }

        if($request->filled('sort') && $request->get("sort") != '') {
            list($sortCol, $sortDir) = explode('_', $request->get('sort'));

            if($sortCol == 'count') {
                $query = $query->orderBy('report_summary_count', $sortDir);
            }
        } else {
            $query = $query->orderBy(DB::raw('FIELD(id, ' . implode(',', $reportedRoomIds) . ')'));
        }

        $rooms = $query->paginate(20);

        $viewData = array(
            'boxTitle'   => 'List laporan kost',
            'rooms'      => $rooms,
            'sortTypes'  => $sortTypes
        );

        ActivityLog::LogIndex(Action::KOST_REPORT);

        return view('admin.contents.reports.index', $viewData);
    }

    public function show(Request $request, $roomId)
    {
        $room = Room::find($roomId);

        if(!$room) {
            return redirect()->back()->with('error_message',  'Ruangan tidak ditemukan');
        }

        $reports = Report::where('designer_id', $room->id)
                        ->whereNull('report_parent_id')
                        ->orderBy('created_at', 'desc')
                        ->with(['user', 'children'])
                        ->paginate(20);


        $summary = Report::select('report_type', DB::raw('COUNT(id) as `total`'))
                        ->where('designer_id', $room->id)
                        ->groupBy('report_type')
                        ->pluck('total', 'report_type')
                        ->toArray();

        $viewData = array(
            'boxTitle'  => 'List laporan ' . $room->name,
            'room'      => $room,
            'reports'   => $reports,
            'summary'   => $summary
        );

        return view('admin.contents.reports.show', $viewData);
    }

    public function setFollowedUp(Request $request, $reportId)
    {
        $reports = Report::where('id', $reportId)
                        ->orWhere('report_parent_id', $reportId)
                        ->orderBy('created_at', 'desc')
                        ->get();

        if($reports) {
            foreach ($reports as $key => $report) {
                $report->status = Report::REPORT_STATUS_FOLLOWED_UP;
                $report->save();
            }
        }

        return redirect()->back()->with('message', 'Report sudah ditandai di-follow up');
    }
}
