<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Http\Helpers\ApiHelper;
use Config;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;

use App\Entities\ShortlinkGeneral\ShortlinkGeneral;

class ShortlinkGeneralController extends Controller
{
    /**
     * @var array
     */
    protected $rules = array(
        'admin.tag.store' => array(
            'name' => 'required|min:3|max:30',
            'type' => 'required|in:keyword,emotion,concern,theme',
        ),
        'admin.tag.update' => array(
            'name' => 'required|min:6|max:30',
        )
    );

    /**
     * ShortlinkGeneralController constructor.
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if(!Auth::user()->can('access-tag')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        View::share('contentHeader', 'Stories Management');
        View::share('user', Auth::user());
    }

    public function index()
    {
        $shortlinks = ShortlinkGeneral::orderBy('created_at', 'desc')->get();
        $baseUrl = config('app.short_url');

        $viewData = array(
            'boxTitle'      => 'Lists Shortlink',
            'deleteAction'  => 'admin.shortlink.destroy',
            'rowsShortlink' => $shortlinks,
            'baseUrl'       => $baseUrl,
        );

        ActivityLog::LogIndex(Action::SHORTLINK);

        return View::make('admin.contents.shortlink.index', $viewData);
    }

    public function create(Request $request)
    {
        $shortlink                  = new ShortlinkGeneral;
        $shortlink->original_url    = $request->old('original_url');
        $shortlink->short_url       = $request->old('short_url');

        $viewData = array(
            'boxTitle'      => 'Create Shortlink',
            'rowShortlink'  => $shortlink,
            'formAction'    => 'admin.shortlink.store',
            'formMethod'    => 'POST',
        );

        return View::make('admin.contents.shortlink.form', $viewData);
    }

    public function store(Request $request)
    {
        $this->validate($request, array(
            'original_url'  => 'required',
            'short_url'     => 'required'
        ));

        $slug = $request->short_url;
        $existingSlug = ShortlinkGeneral::where('short_url', $slug)->first();

        if ($existingSlug) {
            return Redirect::route('admin.shortlink.create')
            ->with('error_message', 'URL was registered')
            ->withInput();
            
        } else {
            $shortlink                 = new ShortlinkGeneral;
            $shortlink->original_url   = $request->original_url;
            $shortlink->short_url      = $slug;
            $isSuccess                 = $shortlink->save();

            ActivityLog::LogCreate(Action::SHORTLINK, $shortlink->id);
    
            if ($isSuccess) {
                return Redirect::route('admin.shortlink.index')
                    ->with('message', 'New Short Link Created');

            } else {
                return Redirect::route('admin.shortlink.create')
                    ->with('error_message', 'Aborted')
                    ->withInput();

            }
        }
    }
    
    public function edit($id)
    {
        $shortlink = ShortlinkGeneral::find($id);

        $viewData = array(
            'boxTitle'      => 'View or Edit Short Link',
            'rowShortlink'  => $shortlink,
            'formAction'    => array('admin.shortlink.update', $id),
            'formMethod'    => 'PUT',
        );

        return View::make('admin.contents.shortlink.form', $viewData);
    }

    public function update(Request $request, $id)
    {
        $arrInputOld = array('original_url', 'short_url');

        $this->validate($request, array(
            'original_url'  => 'required',
            'short_url'     => 'required'
        ));

        $shortlink                 = ShortlinkGeneral::find($id);
        $shortlink->original_url   = $request->original_url;
        $shortlink->short_url      = $request->short_url;
        $isSuccess                 = $shortlink->save();

        if ($isSuccess) {
            ActivityLog::LogUpdate(Action::SHORTLINK, $id);
            return Redirect::route('admin.shortlink.index')
                ->with('message', 'Short Link Updated');
        } else {
            return Redirect::route('admin.shortlink.create')
                ->with('error_message', 'Aborted')
                ->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ShortlinkGeneral::destroy($id);

        ActivityLog::LogDelete(Action::SHORTLINK, $id);

        return Redirect::route('admin.shortlink.index')
                ->with('message', 'Short Link Deleted');
    }
}
