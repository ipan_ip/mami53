<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\Entities\Vacancy\VacancyApply;
use Illuminate\Support\Facades\Redirect;
use Config;
use Response;

class JobsApplyController extends Controller
{
    public function __construct()
    {
        View::share('contentHeader', 'Stories Management');
        View::share('user', Auth::user());
    }

    public function list(Request $request, $id)
    {
    	$applys    = VacancyApply::query()->with('vacancy')->where('vacancy_id', $id)->orderBy('updated_at','desc');

        if($request->filled('q'))
        {
            $applys->where('name','like', '%' . $request->get('q') . '%');
        }

        $applys    = $applys->paginate(10);

        $viewData = array(
            'boxTitle'        => 'Jobs Apply List',
            'applys'     => $applys,
        );

        return View::make('admin.contents.vacancy.apply.list', $viewData);
    }

    public function viewCv(Request $request, $id)
    {
    	$apply = VacancyApply::where('id', $id)->first();
		$path  = Config::get('api.media.folder_cv').'/'.$apply->file;

		return Response::make(file_get_contents($path), 200, [
		    'Content-Type' => 'application/pdf',
		    'Content-Disposition' => 'inline; filename="'.$apply->file.'"'
		]);
    }

    public function applyList(Request $request)
    {
    	$applys    = VacancyApply::query()->with('vacancy')->orderBy('updated_at','desc');

        if($request->filled('q'))
        {
            $applys->where('name','like', '%' . $request->get('q') . '%');
        }

        $applys    = $applys->paginate(10);

        $viewData = array(
            'boxTitle'   => 'Jobs Apply List',
            'applys'     => $applys,
        );

        return View::make('admin.contents.vacancy.apply.all', $viewData);
    }
}
