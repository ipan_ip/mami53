<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;
use Validator;
use Auth;

use App\Entities\Event\PhotoboothEvent;
use App\Entities\Event\PhotoboothSubscriber;
use App\Entities\Config\AppConfig;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;

class PhotoboothEventController extends Controller
{

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if(!Auth::user()->can('access-photobooth')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        View::share('contentHeader', 'Photobooth Event Management');
        View::share('user', Auth::user());
    }

    public function index(Request $request)
    {
        $query = PhotoboothEvent::with('subscribers');

        if ($request->filled('q') && $request->get('q') != '') {
            $query = $query->where('university', 'like', '%' . $request->get('q') . '%');
        }

        $photoboothEvents = $query->orderBy('created_at', 'desc')->paginate(20);

        $viewData = array(
            'photoboothEvents' => $photoboothEvents,
            'boxTitle' => 'Photobooth Events'
        );

        ActivityLog::LogIndex(Action::PHOTOBOOTH);

        return view("admin.contents.photobooth.index", $viewData);
    }

    public function create(Request $request)
    {
        $viewData = [];
        $viewData['boxTitle'] = 'Add Photobooth Event';

        return view('admin.contents.photobooth.create', $viewData);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'start_date' => 'required|date_format:Y-m-d',
            'end_date' => 'required|date_format:Y-m-d|after:start_date',
            'university' => 'required|max:190',
            'quota' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        $event = new PhotoboothEvent;
        $event->start_date = $request->start_date;
        $event->end_date = $request->end_date;
        $event->university = $request->university;
        $event->quota = $request->quota;
        $event->save();

        ActivityLog::LogCreate(Action::PHOTOBOOTH, $event->id);

        return redirect()->route('admin.photobooth.index')->with('message', 'Event berhasil ditambahkan');
    }

    public function edit(Request $request, $id)
    {
        $photoboothEvent = PhotoboothEvent::find($id);

        if (!$photoboothEvent) {
            return redirect()->route('admin.photobooth.index')->with('error_message', 'Data tidak ditemukan');
        }

        $viewData = [
            'boxTitle'    => 'Edit Photobooth Event',
            'photoboothEvent' => $photoboothEvent
        ];

        return view('admin.contents.photobooth.edit', $viewData);
    }

    public function update(Request $request, $id)
    {
        $photoboothEvent = PhotoboothEvent::find($id);

        if (!$photoboothEvent) {
            return redirect()->route('admin.photobooth.index')->with('error_message', 'Data tidak ditemukan');
        }

        $validator = Validator::make($request->all(), [
            'start_date' => 'required|date_format:Y-m-d',
            'end_date' => 'required|date_format:Y-m-d|after:start_date',
            'university' => 'required|max:190',
            'quota' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        $photoboothEvent->start_date = $request->start_date;
        $photoboothEvent->end_date = $request->end_date;
        $photoboothEvent->university = $request->university;
        $photoboothEvent->quota = $request->quota;
        $photoboothEvent->save();

        ActivityLog::LogUpdate(Action::PHOTOBOOTH, $id);

        return redirect()->route('admin.photobooth.index')->with('message', 'Event berhasil dirubah');
    }

    public function subscribers(Request $request, $id)
    {
        $photoboothEvent = PhotoboothEvent::find($id);

        if (!$photoboothEvent) {
            return redirect()->route('admin.photobooth.index')->with('error_message', 'Data tidak ditemukan');
        }

        $subscribers = PhotoboothSubscriber::where('photobooth_event_id', $photoboothEvent->id)
                        ->paginate(20);

        $viewData = array(
            'subscribers' => $subscribers,
            'boxTitle' => 'Photobooth Events'
        );

        return view("admin.contents.photobooth.subscribers", $viewData);
    }
}
