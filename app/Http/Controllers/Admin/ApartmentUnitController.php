<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use View;
use App\Entities\Apartment\ApartmentProject;
use App\Entities\Room\Room;
use Auth;
use Validator;
use App\Repositories\PropertyInputRepository;
use App\Entities\Room\Element\Tag;
use App\Libraries\SMSLibrary;
use Cache;
use App\Entities\Generate\Currency;
/**
* 
*/
class ApartmentUnitController extends Controller
{

	protected $validationMessages = [
		'unit_id.required' => 'ID unit harus diisi',
		'project_id.required' => 'ID Apartment Project harus diisi',
		'project_id.numeric' => 'ID Apartment Project harus berupa angka',
		'unit_type.required' => 'Tipe Unit harus diisi',
		'unit_type_name.required_if' => 'Nama tipe unit harus diisi jika bertipe Lainnya',
		'unit_type_name.max' => 'Nama tipe unit maksimal :max karakter',
		'unit_number.required'=>'Nomor Unit harus diisi',
		'unit_number.max'=>'Nomor Unit maksimal :max karakter',
		'floor.required' => 'Lantai harus diisi',
		'maintenance_price.numeric' => 'Biaya Maintenance harus berupa angka',
		'parking_price.numeric' => 'Biaya Parkir harus berupa angka'
	];

	protected $apartmentValidationMessages = [
		'project_id.required_without'=>'Nama Apartment harus diisi',
		'project_name.required_without'=>'Nama Apartment harus diisi',
		'project_name.max'=>'Nama Apartment maksimal :max karakter',
		'unit_name.required'=>'Nama Unit harus diisi',
		'unit_name.max'=>'Nama Unit maksimal :max karakter',
		'unit_number.required' => 'Nomor Unit harus diisi',
		'unit_type.required'=>'Tipe Unit harus diisi',
		'unit_type_name.required_if' => 'Nama Tipe Unit harus diisi jika Tipe Unit adalah Lainnya',
		'unit_type_name.max'=>'Nama Tipe Unit maksimal :max karakter',
		'floor.required'=>'Lantai harus diisi',
		'floor.numeric'=>'Lantai harus berupa angka',
		'unit_size.required'=>'Luas Unit harus diisi',
		'unit_size.numeric'=>'Luas Unit harus berupa angka',
		'min_payment.required'=>'Minimal Pembayaran harus diisi',
		'min_payment.numeric'=>'Minimal Pembayaran harus berupa angka',
		'price_yearly.numeric'=>'Harga Sewa Tahunan harus berupa angka',
		'price_monthly.required'=>'Harga Sewa Bulanan harus diisi',
		'price_monthly.numeric'=>'Harga Sewa Bulanan harus berupa angka',
		'price_daily.numeric'=>'Harga Sewa Harian harus berupa angka',
		'price_weekly.numeric'=>'Harga Sewa Mingguan harus berupa angka',
		'maintenance_price.numeric'=>'Biaya Maintenance harus berupa angka',
		'parking_price.numeric'=>'Biaya Tambahan Parkir harus berupa angka',
		'facilities.required'=>'Fasilitas unit harus diisi',
		'is_furnished.required'=>'Pilihan Furnished harus diisi',
		'fac_room.required_if'=>'Fasilitas Kamar harus diisi',
		'owner_name.required'=>'Nama pemilik/pengelola harus diisi',
		'owner_name.min'=>'Nama pemilik/pengelola minimal :min karakter',
		'owner_name.max'=>'Nama pemilik/pengelola maksimal :max karakter',
		'photo_cover.required' => 'Foto Cover harus diisi',
		'input_as.required' => 'Tipe penginput harus diisi',
		'input_name.required' => 'Nama Penginput harus diisi'
	];

	protected $unitTypeOptions = [
			'1-Room Studio',
			'1 BR',
			'2 BR',
			'3 BR',
			'4 BR',
			'Lainnya'
		];
	
	protected $repository;


	public function __construct(PropertyInputRepository $repository)
	{
		$this->middleware(function ($request, $next) {
            if(!Auth::user()->can('access-apartment-project-unit')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

		View::share('contentHeader', 'Apartment Unit Management');
        View::share('user', Auth::user());

        $this->repository = $repository;
	}

    private function searchIndex($keyword, $query)
    {
    	$room = Room::find($keyword);

        if ($room != null) {
            return $query->where('id', $room->id);
        }

        return $query->where(function($q) use ($keyword){
            $keyword = "%$keyword%";

            $q  ->where('name', 'like', $keyword)
                ->orWhere('phone', 'like', $keyword)
                ->orWhere('owner_phone', 'like', $keyword)
                ->orWhere('manager_phone', 'like', $keyword)
                ->orWhere('agent_phone', 'like', $keyword)
                ->orWhere('agent_name', 'like', $keyword)
                ->orWhere('song_id', 'like', $keyword)
                ->orWhere('area_city', 'like', $keyword);
        });
    }

	public function list(Request $request, $projectId)
	{
		$apartmentProject = ApartmentProject::find($projectId);

		$apartmentUnits = Room::with(['photo'])->where('apartment_project_id', $apartmentProject->id);

        // Filter
        if ($request->filled('q')) {
            $apartmentUnits = $this->searchIndex($request->input('q'), $apartmentUnits);
        }

        if ($request->filled('phone')) {
        	$keyword = $request->input('phone');
        	$apartmentUnits = $apartmentUnits->where(function($q) use ($keyword){
            $keyword = "%$keyword%";

	            $q  ->where('owner_phone', 'like', $keyword)
	                ->orWhere('agent_phone', 'like', $keyword)
	                ->orWhere('manager_phone', 'like', $keyword);
        	});
        }

        if ($request->filled('phone') AND SMSLibrary::validateIndonesianMobileNumber($request->input('phone'))) {
            $telp_now = true;
        }

		$apartmentUnits = $apartmentUnits->paginate(20);

		$viewData = array(
            'boxTitle'        	=> 'List Apartment Unit (' . $apartmentProject->name . ')',
            'apartmentProject'	=> $apartmentProject,
            'apartmentUnits'	=> $apartmentUnits,
            'telp_now'        	=> isset($telp_now) ? true : false,
        );

        return view("admin.contents.apartments.unit-list", $viewData);
	}

	public function create(Request $request, $projectId)
	{
		$apartmentProject = ApartmentProject::find($projectId);

		$unitTypeOptions = $this->unitTypeOptions;

		$minPaymentOptions = Tag::where('type', 'keyword')->get()->pluck('name', 'id');
		$facilityOptions = Tag::where('type', '<>', 'keyword')
							->where('type', '<>', 'fac_project')
							->get()->pluck('name', 'id');

		$inputAsOptions = ['scrap'];

		$viewData = array(
            'boxTitle'        	=> 'Add Apartment Unit',
            'typeOptions'		=> $unitTypeOptions,
            'project'			=> $apartmentProject,
            'minPaymentOptions'	=> $minPaymentOptions,
            'facilityOptions'	=> $facilityOptions,
            'inputAsOptions'	=> $inputAsOptions,
            'furnishedOptions'  => ApartmentProject::UNIT_CONDITION
        );

        return view("admin.contents.apartments.unit-create", $viewData);
	}

	public function store(Request $request, $projectId)
	{
		$apartmentProject = ApartmentProject::find($projectId);

		$validator = Validator::make($request->all(), 
			[
				'unit_name'=>'required|max:250',
				'unit_number' => 'required',
				'unit_type'=>'required',
				'unit_type_name' => 'nullable|required_if:unit_type,Lainnya|max:100',
				'floor'=>'numeric',
				'unit_size'=>'numeric',
				'min_payment'=>'nullable|numeric',
				'price_daily'=>'nullable|numeric',
				'price_weekly' => 'nullable|numeric',
				'price_monthly'=>'required|numeric',
				'price_yearly'=>'nullable|numeric',
				'price_shown'=>'nullable',
				'maintenance_price'=>'nullable|numeric',
				'parking_price'=>'nullable|numeric',
                'is_furnished'=>'required',
				'facilities'=>'required',
				'owner_name'=>'nullable|min:4|max:100',
				'photo_cover'=>'required',
				'photo_bedroom' => 'nullable',
				'photo_bathroom' => 'nullable',
				'photo_other' => 'nullable',
				'input_as' => 'required',
				'input_name' => 'required'
			], $this->apartmentValidationMessages);

		$validator->after(function($validator) use ($request)
        {	
			if (!$request->filled('price_weekly') AND !$request->filled('price_monthly') AND !$request->filled('price_daily') AND !$request->filled('price_yearly')) {
                $validator->errors()->add('price', 'Harga sewa harus diisi salah satu');
            }

            $char_min = 4;
            if ($request->filled('price_type') AND $request->input('price_type') == 'usd') {
                $price_type = 'usd';
                $char_min = 1;
            }

            $monthly = null;
            if ($request->filled('price_monthly') AND $request->input('price_monthly') == 0) $monthly = null;
            else $monthly = $request->input('price_monthly');

            if (strlen($monthly) == 0) $monthly = null;

            if (!is_null($monthly) AND strlen($request->input('price_monthly')) < $char_min) {
                $validator->errors()->add('price_monthly', 'Harga Sewa Bulanan harus minimal '.$char_min.' karakter');
            }

            $daily = null;
            if ($request->filled('price_daily') AND $request->input('price_daily') == 0) $daily = null;
            else $daily = $request->input('price_daily');

            if (strlen($daily) == 0) $daily = null;

            if ( !is_null($daily) AND strlen($request->input('price_daily')) < $char_min ) {
                $validator->errors()->add('price_daily', 'Harga Sewa Harian harus minimal '.$char_min.' karakter');
            }
            
            $weekly = null;
            if ($request->filled('price_weekly') AND $request->input('price_weekly') == 0) $weekly = null;
            else $weekly = $request->input('price_weekly');

            if (strlen($weekly) == 0) $weekly = null;

            if ( !is_null($weekly) AND strlen($request->input('price_weekly')) < $char_min ) {
                $validator->errors()->add('price_weekly', 'Harga Sewa Mingguan harus minimal '.$char_min.' karakter');
            }

            $yearly = null;
            if ($request->filled('price_yearly') AND $request->input('price_yearly') == 0) $yearly = null;
            else $yearly = $request->input('price_yearly');

            if (strlen($yearly) == 0) $yearly = null;

            if ( !is_null($yearly) AND strlen($request->input('price_yearly')) < $char_min) {
                $validator->errors()->add('price_yearly', 'Harga Sewa Tahunan harus minimal '.$char_min.' karakter');
            }
            
            if (is_null($monthly) AND is_null($yearly) AND is_null($daily) AND is_null($weekly)) {
                $validator->errors()->add('price', 'Harga sewa harus diisi salah satu');
			}
			
        });

		$validator->after(function($validator) use ($request, $apartmentProject) {
			if($request->project_id > 0) {
				$unitNumberExist = Room::where('apartment_project_id', $apartmentProject->id)
										->where('unit_number', $request->unit_number)
										->first();

				if($unitNumberExist) {
					$validator->errors()->add('unit_number', 'Nomor Unit telah digunakan sebelumnya');
				}
			}
		});

		if($validator->fails()) {
			return redirect()->back()->withErrors($validator->errors())->withInput($request->all());
		}

		if ($request->input('indexed') == null) {
            $isIndexed = 0;
        } else {
            $isIndexed = 1;
		}
		
		//get input
        if ($request->filled('price_type') AND $request->input('price_type') == 'usd') {
            $usdPrice = Cache::get("currencyusd");
            
            if (is_null($usdPrice)) {
                $usdPrice = Currency::getCurrencyData('usd');
            }

            $price_monthly = $request->input('price_monthly') * $usdPrice;
            $price_weekly = $request->input('price_weekly') * $usdPrice;
            $price_yearly = $request->input('price_yearly') * $usdPrice;
            $price_daily = $request->input('price_daily') * $usdPrice;

            $price_monthly_usd = $request->input('price_monthly');
            $price_weekly_usd = $request->input('price_weekly');
            $price_yearly_usd = $request->input('price_yearly');
            $price_daily_usd = $request->input('price_daily');

            $price_type = true;
		}
		

		$params = [
			'name'         =>$request->unit_name,
			'unit_number'  =>$request->unit_number,
			'price_daily'=> isset($price_daily) ? $price_daily : $request->price_daily,
			'price_weekly'=> isset($price_weekly) ? $price_weekly : $request->price_weekly,
			'price_monthly'=> isset($price_monthly) ? $price_monthly : $request->price_monthly,
			'price_yearly'=> isset($price_yearly) ? $price_yearly : $request->price_yearly,
			'price_daily_usd' =>  isset($price_daily_usd) ? $price_daily_usd : 0,
			'price_weekly_usd' => isset($price_weekly_usd) ? $price_weekly_usd : 0,
			'price_monthly_usd' => isset($price_monthly_usd) ? $price_monthly_usd : 0,
			'price_yearly_usd' => isset($price_yearly_usd) ? $price_yearly_usd : 0,
			'price_shown'=>!is_null($request->price_shown) && $request->price_shown != '' ?
				implode('|', $request->price_shown) : null,
			'floor'        =>$request->floor,
			'unit_type'    =>$request->unit_type,
			'unit_type_name'=>$request->unit_type_name,
			'unit_size'     =>$request->unit_size,
			'is_furnished'	=> (int)$request->is_furnished,
			'facilities'    =>$request->facilities,
			'min_payment'   =>!is_null($request->min_payment) ? [$request->min_payment] : [],
			'owner_name'    =>$request->owner_name,
			'owner_phone'	=>$request->owner_phone,
			'password'      =>$request->password,
			'is_indexed'    =>$isIndexed,
			'description'   => trim(strip_tags($request->description)),
			'photos'			=> [
				'cover' => $request->photo_cover,
				'bedroom' => $request->photo_bedroom,
				'bath' => $request->photo_bathroom,
				'other' => $request->photo_other,
			],
			'input_as'		=>$request->input_as,
			'input_name'	=>$request->input_name,
			'price_type' => isset($price_type) ? $price_type : 'idr',
			'is_admin' => true,
		];

		$this->repository->saveNewApartmentUnitByAdmin($apartmentProject, $params);

		return redirect(route('admin.apartment.unit.list', $apartmentProject->id))->with('message', 'Unit berhasil ditambahkan');
	}

	public function edit(Request $request, $unitId)
	{
		$apartmentUnit = Room::with(['apartment_project', 'price_components'])->find($unitId);
		$apartment_active = ApartmentProject::select(['id', 'name'])
							->where('is_active', 1)
							->orWhere('id', $apartmentUnit->apartment_project_id)
							->get();

		$unitTypeOptions = $this->unitTypeOptions;

		$apartmentUnitPriceComponents = $this->repository->groupPriceComponents($apartmentUnit->price_components, 'price_name');

		$viewData = array(
            'boxTitle'        	=> 'Edit Apartment Unit',
            'apartmentProject'	=> $apartmentUnit->apartment_project,
            'apartmentUnit'		=> $apartmentUnit,
            'apartmentUnitPriceComponents' => $apartmentUnitPriceComponents,
            'typeOptions'		=> $unitTypeOptions,
            'maintenance_price' => isset($apartmentUnitPriceComponents['maintenance']->price_reguler) ? $apartmentUnitPriceComponents['maintenance']->price_reguler : "",
            'parking_price' => isset($apartmentUnitPriceComponents['parking']->price_reguler) ? $apartmentUnitPriceComponents['parking']->price_reguler : "",
            'apartment_active'  => $apartment_active,
            'furnishedOptions'  => ApartmentProject::UNIT_CONDITION
        );

        return view("admin.contents.apartments.unit-edit", $viewData);
	}

	public function update(Request $request)
	{
		$validator = Validator::make($request->all(),
			[
				'unit_id' => 'required',
				'project_id' => 'required|numeric',
				'unit_type' => 'required',
				'unit_type_name' => 'nullable|required_if:unit_type,Lainnya|max:100',
				'unit_number'=>'required|max:100',
				'floor' => 'required',
				'maintenance_price' => 'nullable|numeric',
				'parking_price' => 'nullable|numeric',
				'price_shown' => 'nullable'
			],
			$this->validationMessages);

		$validator->after(function($validator) use ($request) {
			$projectExist = ApartmentProject::find($request->project_id);

			if(!$projectExist) {
				$validator->errors()->add('project_id', 'ID Apartment Project tidak ditemukan');
			}

			$unitNumberExist = Room::where('apartment_project_id', $request->project_id)
									->where('unit_number', $request->unit_number)
									->first();

			if($unitNumberExist && $unitNumberExist->id != $request->unit_id) {
				$validator->errors()->add('unit_number', 'Nomor Unit telah digunakan sebelumnya');
			}
		});

		if($validator->fails()) {
			return redirect()->back()->with('errors', $validator->errors())->withInput($request->all());
		}

		$apartmentUnit = Room::with(['apartment_project', 'price_components'])->find($request->unit_id);

		$params = [
			'apartment_project_id' => $request->project_id,
			'unit_type' => $request->unit_type,
			'unit_type_name' => $request->unit_type_name,
			'unit_number' => $request->unit_number,
			'floor' => $request->floor,
			'is_furnished' => !is_null($request->is_furnished) ? $request->is_furnished : 0,
			'maintenance_price' => $request->maintenance_price,
			'parking_price' => $request->parking_price,
			'price_shown' => !is_null($request->price_shown) && $request->price_shown != '' ?
				implode('|', $request->price_shown) : null,
		];

		$this->repository->updateApartmentUnitByAdmin($apartmentUnit, $params);


		return redirect(route('admin.apartment.unit.list', $request->project_id))->with('message', 'Apartment Unit berhasil diupdate');
	}

}