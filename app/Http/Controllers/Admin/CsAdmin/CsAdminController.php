<?php

namespace App\Http\Controllers\Admin\CsAdmin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Carbon\Carbon;
use SendBird;
use Auth;
use Validator;
use Redirect;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

use App\Repositories\UserDataRepository;
use App\User;
use App\Entities\Room\Room;
use App\Entities\CsAdmin\CsAdmin;
use App\Entities\Activity\ActivityLog;
use App\Http\Helpers\UtilityHelper;
use App\Entities\Activity\Action;

class CsAdminController extends Controller
{
    const PERMISSION_ACCESS_CS = 'access-cs';
    const PERMISSION_CHAT_ADMIN_CS = 'access-chat-';

    public function __construct(UserDataRepository $userRepository)
    {
        $this->middleware(function ($request, $next) {
            if (!\Auth::user()->can($this::PERMISSION_ACCESS_CS)) {
                return redirect('admin');
            }

            return $next($request);
        });

        $this->userRepository = $userRepository;

        \View::share('user', \Auth::user());
        \View::share('contentHeader', 'CS Admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(Request $request)
    {
        // Activity Log
        ActivityLog::LogIndex(Action::CS_ADMIN);

        $query = CsAdmin::with('user')->orderBy('updated_at', 'desc');

        if ($request->filled('name')) {
            $query->where('name', 'LIKE', '%' . $request->key . '%');
        }

        $csAdmins = $query->paginate(20); 
        foreach ($csAdmins as $key => $row) {
            if (!is_null($row->user)) {
                if ($row->user->photo) {
                    $row->photo_url = $row->user->photo->getMediaUrl();
                }
            }
        }
        
        $viewData = [
            'csAdmins' => $csAdmins,
            'boxTitle' => 'CS Admin',
            'searchUrl' => route('admin.cs-admin.index'),
        ];

        return view('admin.contents.cs-admin.index', $viewData);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
    		'user_id' => 'required',
    		'name' => 'required',
        ]);

        if($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        $userId = $request->user_id;
        $user = User::find($userId);

        $existingCSAdmin = CsAdmin::where('user_id', $userId)->first();
        if (!is_null($existingCSAdmin)) {
            return redirect()->back()->with('error_message', 'User already registered');
        }

        $newCSAdmin = CsAdmin::register($request);
        if (is_null($newCSAdmin)) {
            return redirect()->back()->with('error_message', 'Failed created');
        }

        $csPhoto = null;
        if ($request->hasFile('photo')) {
            $csPhoto = $this->storePhoto($request->photo, $user);
        }

        // Activity Log
        ActivityLog::LogCreate(Action::CS_ADMIN, $newCSAdmin->id);

        $profileUrl = !empty($user->photo->getMediaUrl()) ? $user->photo->getMediaUrl()['medium'] : '';
        $sendBird = SendBird::createUserWithAccessToken($userId, $newCSAdmin->name, $profileUrl);
        if (is_null($sendBird)) {
            return redirect()->back()->with('message', 'Successfully created but failed register to SendBird');
        }

        $newCSAdmin->access_token = $sendBird['access_token'];
        $newCSAdmin->update();

        return redirect()->back()->with('message', 'Successfully created');
    }

    public function storePhoto($photo, $user)
    {
        $datas = [
            "photo" => $photo,
        ];

        try {
            $uploadPhoto = $this->userRepository->uploadProfilePhoto($datas, $user);

            return $uploadPhoto;
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);

            return null;
        }
    }

    public function update(Request $request, $id)
    {
        $csAdmin = CsAdmin::find($id);
        $user = User::find($csAdmin->user_id);
        if (is_null($csAdmin)) {
            return redirect()->back()->with('error_message', 'CS Admin not found');
        }

        try {
            $csAdmin->name = $request->name;
            $csAdmin->email = $request->email;
            $csAdmin->access_default = $request->access_default;
            $csAdmin->update();

            $csPhoto = null;
            if ($request->hasFile('photo')) {
                $csPhoto = $this->storePhoto($request->photo, $user);
            }

            $profileUrl = !empty($user->photo->getMediaUrl()) ? $user->photo->getMediaUrl()['medium'] : '';
            $sendBird = SendBird::updateUser($csAdmin->user_id, $csAdmin->name, $profileUrl);
            if (is_null($sendBird)) {
                return redirect()->back()->with('message', 'Successfully updated but failed updated data SendBird');
            }

            // Activity Log
            ActivityLog::LogUpdate(Action::CS_ADMIN, $id);

            return redirect()->back()->with('message', 'Successfully updated');
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
        }
    }

    public function destroy($id)
    {
        $csAdmin = CsAdmin::find($id);
        if (is_null($csAdmin)) {
            return redirect()->back()->with('error_message', 'CS Admin not found');
        }

        $csAdmin->delete();

        // Activity Log
        ActivityLog::LogDelete(Action::CS_ADMIN, $id);
        
        return redirect()->back()->with('message', 'Successfully deleted');        
    }

    public function getRoomChat($csAdminId)
    {
        $csAdmin = CsAdmin::find($csAdminId);
        if (is_null($csAdmin)) {
            return redirect()->back()->with('error_message', 'CS Admin not found');
        }

        $permissionNameCS = $this::PERMISSION_CHAT_ADMIN_CS.strtolower($csAdmin->name);
        if(!Auth::user()->can($permissionNameCS)) {
            return redirect()->back()->with('error_message', 'You have no access, please check your permission');
        }

        $roomChatUrl = $csAdmin->getRoomChatUrl();
        
        // Activity Log
        ActivityLog::Log(Action::CS_ADMIN, "Open Chat Room CS Admin - ".$csAdmin->user_id);

        if (is_null($roomChatUrl)) {
            return redirect()->back()->with('error_message', 'Failed to open chat room, please check your permission');
        }

        return redirect()->to($roomChatUrl);
    }
}