<?php

namespace App\Http\Controllers\Admin\Forum;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;
use Auth;
use Validator;
use Illuminate\Validation\Rule;

use App\Entities\Forum\ForumCategory;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;

class ForumCategoryController extends Controller
{

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!Auth::user()->can('access-forum-category')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        View::share('contentHeader', 'Forum Category Management');
        View::share('user', Auth::user());
    }

    public function index(Request $request)
    {
        $categories = ForumCategory::paginate(20);

        $viewData = array(
            'boxTitle'  => 'List Kategori',
            'categories'     => $categories,
        );

        ActivityLog::LogIndex(Action::FORUM_CATEGORY);

        return view("admin.contents.forum.category.index", $viewData);
    }

    public function create(Request $request)
    {
        $viewData = [
            'boxTitle'    => 'Create Forum Category'
        ];

        return view('admin.contents.forum.category.create', $viewData);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'slug' => 'required|unique:forum_category',
            'name' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        $category = new ForumCategory;
        $category->name = $request->name;
        $category->slug = strtolower($request->slug);
        $category->save();

        ActivityLog::LogCreate(Action::FORUM_CATEGORY, $category->id);

        return redirect()->route('admin.forum-category.index')->with('messages', 'Forum Kategori berhasil ditambahkan');
    }

    public function edit(Request $request, $id)
    {
        $category = ForumCategory::find($id);

        if(!$category) {
            return redirect()->back()->with('error_message', 'Data tidak ditemukan');
        }

        $viewData = [
            'boxTitle'       => 'Edit Forum Category',
            'category'       => $category,
        ];

        return view('admin.contents.forum.category.edit', $viewData);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'slug'=>[
                'required',
                Rule::unique('forum_category')->ignore($id, 'id')->whereNull('deleted_at')
            ],
            'name' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        $category = ForumCategory::find($id);
        $category->name = $request->name;
        $category->slug = $request->slug;
        $category->save();

        ActivityLog::LogUpdate(Action::FORUM_CATEGORY, $id);

        return redirect()->route('admin.forum-category.index')->with('messages', 'Forum Kategori berhasil dirubah');
    }
}
