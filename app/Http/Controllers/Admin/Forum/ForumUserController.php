<?php

namespace App\Http\Controllers\Admin\Forum;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;
use Auth;
use Carbon\Carbon;
use App\Entities\Entrust\Role;
use App\User;

use Cache;
use Illuminate\Cache\TaggableStore;
use Config;
use App\Entities\Forum\ForumPoint;
use App\Entities\Forum\ForumUser;
use App\Entities\Forum\ForumCategory;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;
use Validator;

class ForumUserController extends Controller
{

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!Auth::user()->can('access-forum-user')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        View::share('contentHeader', 'Forum User Management');
        View::share('user', Auth::user());
    }

    public function seniorList(Request $request)
    {
        $role = Role::where('name', 'forum_senior')->first();

        $users = $role->users()->with(['forum_user', 'forum_points'])->paginate(20);

        $viewData = array(
            'boxTitle'  => 'List Kakak',
            'users'     => $users,
        );

        ActivityLog::LogIndex(Action::FORUM_SENIOR_USER);

        return view("admin.contents.forum.user.senior_index", $viewData);
    }

    public function createSenior(Request $request)
    {
        $categories = ForumCategory::get();
        $yearStartOptions = range(date('Y'), 2005);

        $viewData = array(
            'boxTitle'  => 'Tambah Kakak',
            'categories' => $categories,
            'yearStartOptions' => $yearStartOptions
        );

        return view("admin.contents.forum.user.senior_create", $viewData);
    }

    public function storeSenior(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'nim' => 'required',
            'university' => 'required',
            'major' => 'required',
            'year_start' => 'required',
            'university_city' => 'required',
            'category_id' => 'required',
            // 'official' => 'nullable'
        ]);

        $user = $request->user_id ? User::find($request->user_id) : null;

        $validator->after(function($validator) use ($request, $user) {

            if (!$user) {
                $validator->errors()->add('user_id', 'User tidak ditemukan');
            } else {
                if ($user->hasRole('forum_junior')) {
                    $validator->errors()->add('user_id', 'User sudah terdaftar sebagai adik');
                } elseif ($user->hasRole('forum_senior')) {
                    $validator->errors()->add('user_id', 'User sudah terdaftar sebagai kakak, silakan jadikan official melalui menu di listing kakak');
                } elseif ($user->hasRole('forum_senior_official')) {
                    $validator->errors()->add('user_id', 'User sudah terdaftar sebagai kakak official');
                }
            }

            $semester = ForumUser::countSemester($request->year_start);

            if ($semester <= 2) {
                $validator->errors()->add('user_id', 'User denga tahun masuk ' . $request->year_start . 
                    ' seharusnya masih jadi adik');
            }

        });

        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }


        $user->job = 'kuliah';
        $user->work_place = $request->university;
        $user->position = $request->major;
        $user->city = $request->university_city;

        $semester = ForumUser::countSemester($request->year_start);

        $user->semester = $semester;

        $user->save();

        $forumUser = new ForumUser;
        $forumUser->user_id = $user->id;
        $forumUser->register_as = 'college_student';

        $forumUser->university = $request->university;
        $forumUser->major = $request->major;
        $forumUser->destination = $request->university_city;
        $forumUser->identifier_number = $request->nim;
        $forumUser->year_start = $request->year_start;

        $forumUser->save();

        ActivityLog::LogCreate(Action::FORUM_SENIOR_USER, $forumUser->id);

        $roleSenior = Role::where('name', 'forum_senior')->first();
        $user->attachRole($roleSenior);

        $roleSeniorOfficial = Role::where('name', 'forum_senior_official')->first();
        $user->attachRole($roleSeniorOfficial);

        $user->forum_categories()->attach($request->category_id);

        return redirect()->route('admin.forum.user.senior-list')
            ->with('messages', 'Kakak official berhasil ditambahkan');
    }

    public function makeSeniorOfficial(Request $request, $userId)
    {
        $role = Role::where('name', 'forum_senior_official')->first();

        $user = User::find($userId);

        $user->attachRole($role);

        if(Cache::getStore() instanceof TaggableStore) {
            Cache::tags(Config::get('entrust.role_user_table'))->flush();
        }

        ActivityLog::LogUpdate(Action::FORUM_SENIOR_USER, $userId);

        return redirect()->route('admin.forum.user.senior-list')
            ->with('messages', 'User sudah berubah jadi official');
    }

    public function makeSeniorUnofficial(Request $request, $userId)
    {
        $role = Role::where('name', 'forum_senior_official')->first();

        $user = User::find($userId);

        $user->detachRole($role);

        if(Cache::getStore() instanceof TaggableStore) {
            Cache::tags(Config::get('entrust.role_user_table'))->flush();
        }

        ActivityLog::LogUpdate(Action::FORUM_SENIOR_USER, $userId);

        return redirect()->route('admin.forum.user.senior-list')
            ->with('messages', 'User sudah berubah jadi official');
    }

    public function points(Request $request, $userId)
    {
        $user = User::find($userId);

        $points = ForumPoint::where('user_id', $userId)->paginate(20);

        $viewData = array(
            'boxTitle'  => 'List Point',
            'user'      => $user,
            'points'    => $points
        );

        return view("admin.contents.forum.user.points", $viewData);
    }

    public function redeem(Request $request, $userId)
    {
        $user = User::with('forum_points')->find($userId);

        $viewData = array(
            'boxTitle'  => 'Redeem Point',
            'user'      => $user
        );

        return view("admin.contents.forum.user.redeem", $viewData);
    }

    public function storeForumPoint(Request $request, $userId)
    {
        $user = User::with('forum_points')->find($userId);

        $totalPoint = $user->forum_points->sum('point_value');

        $validator = Validator::make($request->all(), [
            'points' => 'required|max:' . $totalPoint,
            'description' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        $point = new ForumPoint;
        $point->user_id = $user->id;
        $point->type = ForumPoint::TYPE_REDEEM;
        $point->reference_type = null;
        $point->reference_id = null;
        $point->point_value = -$request->points;
        $point->description = $request->description;
        $point->save();

        ActivityLog::LogUpdate(Action::FORUM_SENIOR_USER, $userId);

        return redirect()->route('admin.forum.user.senior-list')
            ->with('messages', 'User sudah berubah jadi official');
    }
}
