<?php

namespace App\Http\Controllers\Admin\Forum;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;
use Auth;
use Carbon\Carbon;
use App\Entities\Entrust\Role;
use App\User;

use Cache;
use Config;
use App\Entities\Forum\ForumThread;
use App\Entities\Forum\ForumAnswer;
use App\Entities\Forum\ForumReport;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;
use Validator;

use App\Events\Forum\AnswerActivated;
use App\Events\Forum\AnswerDeactivated;
use App\Events\Forum\ThreadActivated;
use App\Events\Forum\ThreadDeactivated;

class ForumReportController extends Controller
{

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!Auth::user()->can('access-forum-report')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        View::share('contentHeader', 'Forum Thread Management');
        View::share('user', Auth::user());
    }

    public function index(Request $request)
    {
        $query = ForumReport::with(['thread', 'answer']);

        if ($request->filled('q') && $request->get('q') != '') {
            $query = $query->where('description', 'like', '%' . $request->get('q') . '%');
        }

        $reports = $query->orderBy('created_at', 'desc')->paginate(20);

        $viewData = array(
            'boxTitle'  => 'List Report',
            'reports'   => $reports,
        );

        ActivityLog::LogIndex(Action::FORUM_REPORT);

        return view("admin.contents.forum.report.index", $viewData);
    }
}
