<?php

namespace App\Http\Controllers\Admin\Forum;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;
use Auth;
use Carbon\Carbon;
use App\Entities\Entrust\Role;
use App\User;

use Cache;
use Config;
use App\Entities\Forum\ForumThread;
use App\Entities\Forum\ForumAnswer;
use Validator;

use App\Events\Forum\AnswerActivated;
use App\Events\Forum\AnswerDeactivated;

class ForumAnswerController extends Controller
{

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!Auth::user()->can('access-forum-thread')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        View::share('contentHeader', 'Forum Thread Management');
        View::share('user', Auth::user());
    }

    public function list(Request $request, $threadId)
    {
        $query = ForumAnswer::with('thread')
                    ->where('thread_id', $threadId);

        if ($request->filled('q') && $request->get('q') != '') {
            $query = $query->where('answer', 'like', '%' . $request->get('q') . '%');
        }

        $answers = $query->orderBy('created_at', 'desc')->paginate(20);

        $viewData = array(
            'boxTitle'  => 'List Jawaban',
            'answers'   => $answers,
        );

        return view("admin.contents.forum.answer.list", $viewData);
    }

    public function activate(Request $request, $answerId)
    {
        $answer = ForumAnswer::with('thread')->find($answerId);

        $answer->is_active = 1;
        $answer->save();

        event(new AnswerActivated($answer->thread, $answer));

        return redirect()->route('admin.forum-answer.list', $answer->thread_id)
            ->with('messages', 'Jawaban sudah diaktifkan');
    }

    public function deactivate(Request $request, $answerId)
    {
        $answer = ForumAnswer::with('thread')->find($answerId);

        $answer->is_active = 0;
        $answer->save();

        event(new AnswerDeactivated($answer->thread, $answer));

        return redirect()->route('admin.forum-answer.list', $answer->thread_id)
            ->with('messages', 'Jawaban sudah dinonaktifkan');
    }
}
