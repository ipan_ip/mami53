<?php

namespace App\Http\Controllers\Admin\Forum;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;
use Auth;
use Carbon\Carbon;
use App\Entities\Entrust\Role;
use App\User;

use Cache;
use Config;
use App\Entities\Forum\ForumThread;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;
use Validator;

use App\Events\Forum\ThreadActivated;
use App\Events\Forum\ThreadDeactivated;

class ForumThreadController extends Controller
{

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!Auth::user()->can('access-forum-thread')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        View::share('contentHeader', 'Forum Thread Management');
        View::share('user', Auth::user());
    }

    public function index(Request $request)
    {
        $query = ForumThread::with('category');

        if ($request->filled('q') && $request->get('q') != '') {
            $query = $query->where('title', 'like', '%' . $request->get('q') . '%');
        }

        $threads = $query->orderBy('created_at', 'desc')->paginate(20);

        $viewData = array(
            'boxTitle'  => 'List Pertanyaan',
            'threads'     => $threads,
        );

        ActivityLog::LogIndex(Action::FORUM_THREAD);

        return view("admin.contents.forum.thread.index", $viewData);
    }

    public function edit(Request $request, $threadId)
    {
        $thread = ForumThread::find($threadId);

        $viewData = array(
            'boxTitle'  => 'Edit Pertanyaan',
            'thread'     => $thread,
        );

        return view("admin.contents.forum.thread.edit", $viewData);
    }

    public function update(Request $request, $threadId)
    {
        $validator = Validator::make($request->all(), [
            'question' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        $thread = ForumThread::find($threadId);

        $thread->title = $request->question;
        $thread->save();

        ActivityLog::LogUpdate(Action::FORUM_THREAD, $threadId);

        return redirect()->route('admin.forum-thread.index')
            ->with('messages', 'Pertanyaan sudah dirubah');
    }

    public function activate(Request $request, $threadId)
    {
        $thread = ForumThread::find($threadId);

        $thread->is_active = 1;
        $thread->save();

        event(new ThreadActivated($thread));

        return redirect()->route('admin.forum-thread.index')
            ->with('messages', 'Pertanyaan sudah diaktifkan');
    }

    public function deactivate(Request $request, $threadId)
    {
        $thread = ForumThread::find($threadId);

        $thread->is_active = 0;
        $thread->save();

        event(new ThreadDeactivated($thread));

        return redirect()->route('admin.forum-thread.index')
            ->with('messages', 'Pertanyaan sudah dinonaktifkan');
    }
}
