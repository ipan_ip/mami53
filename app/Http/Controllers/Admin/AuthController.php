<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Validator;
use Auth;
use Session;
use Hash;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Helpers\ApiHelper;
use App\User;
use App\Entities\Activity\ActivityLog;
use App\Entities\User\UserRole;
use App\Entities\Activity\Action;

class AuthController extends Controller
{
    public function getIndex()
    {
        // redirect to login if not from bang.kerupux.com
        if($_SERVER['HTTP_HOST'] != 'bang.kerupux.com' && config('app.env') == 'production') {
            return redirect('/login');
        }

        return view('guest.contents.login');
    }

    public function adminLogin(Request $request)
    {
        $validator = Validator::make($request->all(), [
                'email'    => 'required',
                'password' => 'required|min:8',
            ]);

        $request->flash();

        if ($validator->fails()) {
            return redirect()->to('auth')
                             ->withErrors($validator)
                             ->with('error_message', 'Please recheck your input.');
        }

        $email    = $request->input('email');
        $password = $request->input('password');

        // Check if he is login with email or phone number
        if ( ! filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $email = $this->getEmailFromPhoneNumber($email, $password);
        }

        if(is_null($email)) {
            return  redirect()->to('auth')
                        ->with('error_message', 'Your email/password combination was incorrect')
                        ->with('error_type', 1)
                        ->withInput();
        }

        if (Auth::attempt(array('email' => $email, 'password' => md5($password)), true)) {
            $user = Auth::user();

            // check role
            if ($user->role != UserRole::Administrator) {
                Auth::logout();
                
                return  redirect()->to('auth')
                        ->with('error_message', 'You do not have access')
                        ->with('error_type', 1)
                        ->withInput();
            }

            // Get user photo_url
            if (!is_null($user->photo)) {
                $photoData = User::photo_url($user->photo);
                $user->photo_url = $photoData['small'];
            } else {
                // *** Notes! This feature is being used since v.1.1.2, and this command must be executed at first: "php artisan storage:link" ***
                $user->photo_url =  env("APP_URL", "https://mamikos.com") . "/storage/placeholder.png";
            }

            return $this->successLogin($user);

        } else {

            return  redirect()->to('auth')
                        ->with('error_message', 'Your email/password combination was incorrect')
                        ->with('error_type', 1)
                        ->withInput();
        }
    }

    private function successLogin(User $user)
    {
        if ($user->role != UserRole::Administrator)
        {
            $user->logout();
            abort(403);
        }

        $flashMessage = sprintf("Welcome back, %s", $user->name);

        ActivityLog::Log(Action::LOGIN, 'logged in');

        return redirect()
            ->route('admin.dashboard.home')
            ->with('message', $flashMessage);
    }

    private function getEmailFromPhoneNumber($phoneNumber, $password)
    {
        $userOwnerPhone     = User::where('phone_number', $phoneNumber)->first();

        if ($userOwnerPhone) {
            return $userOwnerPhone->email;
        }

        // If user doesn't exist yet, we search through the room table
        // that have this phone number.
        // $designerOwnerPhone = Designer::where('owner_phone', $phoneNumber)
        //                               ->where('token', $password)
        //                               ->first();

        // if ($designerOwnerPhone) {
        //     // After we found the designer, create user from that designer
        //     // and return the email.
        //     return $designerOwnerPhone->generateUser()->email;
        // }

        return null;
    }

    public function logout()
    {
        $user = Auth::user();

        ActivityLog::Log(Action::LOGOUT, 'logged out');

        Auth::logout();

        return redirect()->to('/auth');
    }

    public function forgotPassword()
    {
        return view('guest.contents.password');
    }

    public function postPassword(Request $request)
    {
        if ($request->get('email')) {

            $phone  = $request->input('email');
            $user   = User::where('phone_number', $phone)->first();

            if ($user !== null) {

                $newPassword        = ApiHelper::random_string('easy', 10);
                $user->password     = Hash::make($newPassword);
                $user->save();

                $message = sprintf("Password terbaru anda untuk login admin mamikos adalah %s", $newPassword);

                SMSLibrary::send($phone, $message, 'infobip', $user->id);
            }
        }

        return redirect()->to('auth')
                         ->with('message', 'SMS untuk password baru telah dikirim ke nomor anda');
    }
}
