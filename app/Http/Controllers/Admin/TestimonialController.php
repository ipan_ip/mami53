<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Room\Room;
use App\Entities\Testimonial\Testimonial;
use App\User;
use Auth;
use Bugsnag;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use RuntimeException;
use View;

class TestimonialController extends Controller
{
    private const ITEM_PER_PAGE = 15;
    private const INDEX_ROUTE = 'admin.testimonial.index';

    /**
     * TestimonialController constructor.
     */
    public function __construct()
    {
        $this->middleware(
            function ($request, $next)
            {
                if (!Auth::user()->can('access-testimonial')) {
                    return redirect('admin');
                }

                return $next($request);
            }
        );

        View::share('contentHeader', 'Owner Testimony Management');
        View::share('user', Auth::user());
    }

    public function index(Request $request)
    {
        $query = Testimonial::with('photo');

        $testimonies = $query->paginate(self::ITEM_PER_PAGE);

        foreach ($testimonies as $testimony) {
            if (!is_null($testimony->photo)) {
                $testimony->media_url = $testimony->photo->getMediaUrl();
            }
        }

        $viewData = [
            'boxTitle'  => 'Owner Testimony Management',
            'rowsData'  => $testimonies,
            'searchUrl' => url()->route(self::INDEX_ROUTE),
        ];

        return view("admin.contents.testimonial.index", $viewData);
    }

    public function store(Request $request)
    {
        try {
            $store = Testimonial::store($request->all());

            if (!$store) {
                throw new RuntimeException('Failed saving testimonial data.');
            }

            return [
                'success' => true,
                'message' => '',
            ];
        } catch (Exception $e) {
            return [
                'success' => false,
                'message' => $e->getMessage(),
                'error'   => $e,
            ];
        }
    }

    public function activate(Request $request)
    {
        try {
            $activate = Testimonial::changeStatus($request->all());

            if (!$activate) {
                return [
                    'success' => false,
                    'message' => 'Failed activating testimony data. Please refresh page and try again.',
                ];
            }

            return [
                'success' => true,
                'title'   => 'Testimony successfully activated',
                'message' => "Page will be refreshed after you clicked OK",
            ];
        } catch (Exception $e) {
            Bugsnag::notifyException($e);

            return [
                'success' => false,
                'message' => $e->getMessage(),
                'error'   => $e,
            ];
        }
    }

    public function deactivate(Request $request)
    {
        try {
            $deactivate = Testimonial::changeStatus($request->all());

            if (!$deactivate) {
                throw new RuntimeException('Failed deactivating testimony data. Please refresh page and try again.');
            }
        } catch (Exception $e) {
            Bugsnag::notifyException($e);

            return [
                'success' => false,
                'message' => $e->getMessage(),
            ];
        }

        return [
            'success' => true,
            'title'   => 'Testimony successfully deactivated',
            'message' => "Page will be refreshed after you clicked OK",
        ];
    }

    public function remove(Request $request)
    {
        try {
            $testimony = Testimonial::find((int)$request->id);

            if (is_null($testimony)) {
                throw new RuntimeException('Failed fetching testimony data. Please refresh page and try again.');
            }

            // Check active status first
            if ($testimony->is_active === 1) {
                throw new RuntimeException('Please deactivate the owner testimony first');
            }

            $testimony->delete();
        } catch (Exception $e) {
            Bugsnag::notifyException($e);

            return [
                'success' => false,
                'message' => $e->getMessage(),
            ];
        }

        return [
            'success' => true,
            'title'   => 'Testimony successfully removed',
            'message' => "Page will be refreshed after you clicked OK",
        ];
    }

    public function getOwnerNameAjax(Request $request)
    {
        try {
            $request     = $request->all();
            $suggestions = [];

            $owners = User::where('name', 'like', '%' . $request['query'] . '%')
                ->where('is_owner', 'true')
                ->limit(10)
                ->pluck('name');

            if (!$owners) {
                return [];
            }

            foreach ($owners as $owner) {
                $suggestions[] = $owner;
            }

            return [
                'suggestions' => $suggestions,
            ];
        } catch (Exception $e) {
            return [
                'success' => false,
                'message' => $e->getMessage(),
                'error'   => $e,
            ];
        }
    }

    public function getKostNameAjax(Request $request)
    {
        try {
            $request     = $request->all();
            $suggestions = [];

            $rooms = Room::where('name', 'like', '%' . $request['query'] . '%')
                ->limit(10)
                ->pluck('name');

            if (!$rooms) {
                return [];
            }

            foreach ($rooms as $room) {
                $suggestions[] = $room;
            }

            return [
                'suggestions' => $suggestions,
            ];
        } catch (Exception $e) {
            return [
                'success' => false,
                'message' => $e->getMessage(),
                'error'   => $e,
            ];
        }
    }
}
