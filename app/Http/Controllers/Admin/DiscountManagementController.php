<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\Booking\BookingDiscount;
use App\Entities\Room\Room;
use App\Entities\Media\Media;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;
use App\Entities\FlashSale\FlashSale;
use App\Entities\Room\Element\Price;
use App\Http\Helpers\DiscountPriceTypeHelper;
use App\Jobs\Booking\BookingDiscountRemoveAll;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Repositories\Booking\BookingDiscountRepository;

class DiscountManagementController extends Controller
{
    protected $repository;

    // for filtering
    protected $roomTypeFilter = array(
        0 => "Semua", 
        1 => 'Mamirooms Kost',
        2 => 'Booking Kost',
    );

    public function __construct(BookingDiscountRepository $repository)
    {
        $this->middleware(function ($request, $next) {
            if(!\Auth::user()->can('access-discount-management')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        $this->repository = $repository;

    	\View::share('contentHeader', 'Discount Management');
        \View::share('user', \Auth::user());
    }

    public function index(Request $request)
    {
        $roomType   = $request->input('room-type');
        $query      = ($request->filled('q') && $request->q != '') ? $request->q : ''; 
        $discounts  = $this->repository->getAllData($roomType, $query);
        $discounts  = $this->repository->formatDataDiscountToList($discounts);

        $viewData = array(
            'boxTitle'        => 'Discount Management',
            'roomTypeFilter'  => $this->roomTypeFilter,
            'discounts'       => $discounts
        );

        ActivityLog::LogIndex(Action::DISCOUNT_MANAGEMENT);
        return view('admin.contents.booking.discount_management', $viewData);
    }

    private function extractDiscountProportionData($data)
    {
        $isSourceMamikos          = $data['discount_source'] === 'mamikos';
        $isSourceOwner            = $data['discount_source'] === 'owner';

        if ($isSourceOwner) {
            // for data discount source owner
            $data['discount_value']         = $data['discount_value_owner'];
            $data['discount_type']          = $data['discount_type_owner'];
            $data['discount_value_mamikos'] = 0;
            return $data;
        }

        if ($isSourceMamikos) {
            // for data discount source mamikos
            $data['discount_value']         = $data['discount_value_mamikos'];
            $data['discount_type']          = $data['discount_type_mamikos'];
            $data['discount_value_owner']   = 0;
            return $data;
        }

        // for data discount source mamikos_and_owner
        switch (true) {
            case $data['discount_type_mamikos'] == 'percentage' && $data['discount_type_owner'] == 'percentage':
                $data['discount_type']  = 'percentage';
                $data['discount_value'] = $data['discount_value_owner'] + $data['discount_value_mamikos'];
                break;
            case $data['discount_type_mamikos'] == 'nominal' && $data['discount_type_owner'] == 'nominal':
                $data['discount_type']  = 'nominal';
                $data['discount_value'] = $data['discount_value_owner'] + $data['discount_value_mamikos'];
                break;
            default:
                $data['real_price']     = (int) $data['real_price'];
                $data['markup_value']   = (int) $data['markup_value'];
                $data['discount_type']  = 'nominal';
                $data['discount_value'] = 0; // default

                if ($data['discount_type_owner'] == 'percentage') {
                    $price = $this->getPriceAfterMarkup($data['markup_value'], $data['markup_type'], $data['real_price']);
                    $data['discount_value'] += ($price * $data['discount_value_owner']) / 100;
                }
                if ($data['discount_type_mamikos'] == 'percentage') {
                    $price = $this->getPriceAfterMarkup($data['markup_value'], $data['markup_type'], $data['real_price']);
                    $data['discount_value'] += ($price * $data['discount_value_mamikos']) / 100;
                }
                if ($data['discount_type_owner'] == 'nominal') {
                    $data['discount_value'] += $data['discount_value_owner'];
                }
                if ($data['discount_type_mamikos'] == 'nominal') {
                    $data['discount_value'] += $data['discount_value_mamikos'];
                }
                break;
        }

        return $data;
    }

    public function store(Request $request)
    {   
        $validator = Validator::make($request->all(), [
            'designer_id'           => 'required|numeric',
            'price_type'            => 'required',
            'discount_source'       => ['required', Rule::in(['mamikos', 'owner', 'mamikos_and_owner'])],
            'discount_type_mamikos' => Rule::in(['percentage', 'nominal']),
            'discount_type_owner'   => Rule::in(['percentage', 'nominal']),
        ]);

        if ($validator->fails()) {
            return [
                'success' => false,
                'message' => "Gagal menyimpan data discount. Mohon cek input anda.",
            ];
        }

        if (
            ($request->discount_source == 'mamikos' && $request->discount_value_mamikos == 0)
            && ($request->discount_source == 'owner' && $request->discount_value_owner == 0)
        ) {
            return [
                'success' => false,
                'message' => "Gagal menyimpan data discount. diskon tidak boleh kosong.",
            ];
        }

        try {
            $isEditing  = $request->filled('id') ? true : false;
            $data       = $this->extractDiscountProportionData($request->all());
            $discount   = BookingDiscount::store($data);

            if (!$discount) {
                return [
                    'success' => false,
                    'message' => "Gagal menyimpan data discount. Silahkan refresh halaman dan coba lagi.",
                ];
            }

            // Set room's related price to discounted
            if (!$this->setToDiscountPrice($discount, $isEditing)) {
                return [
                    'success' => false,
                    'message' => 'Gagal mengupdate status discount. Silahkan refresh halaman dan coba lagi.',
                ];
            }

            ActivityLog::LogCreate(Action::DISCOUNT_MANAGEMENT, $discount->id);
            return [
                'success' => true,
                'data' => $discount,
            ];
        } catch (Exception $e) {
            return [
                'success' => false,
                'message' => "Gagal menyimpan data discount. Silahkan refresh halaman dan coba lagi.",
                'errMessage' => $e->getMessage()
            ];
        }
    }

    public function updateStatus(Request $request)
    {
        try
        {
            $discount = BookingDiscount::find($request->id);
            if (is_null($discount))
            {
                return [
                    'success' => false,
                    'message' => 'Gagal mengupdate status discount. Silahkan refresh halaman dan coba lagi.',
                ]; 
            }

            $room = Room::find($discount->designer_id);
            if (is_null($room))
            {
                return [
                    'success' => false,
                    'message' => 'Gagal mendapatkan data kos. Silahkan refresh halaman dan coba lagi.',
                ]; 
            }

            // command to activate
            if ($request->status == 1)
            {
                $price = $room->price();

                // prevent activation when real price is 0
                if ($price->getPrice($discount->price_type) == 0)
                {
                    return [
                        'success' => false,
                        'message' => 'Discount tidak dapat diaktivasi.<br/>Kos ini tidak mempunyai harga ' . $price->getPriceType($discount->price_type),
                    ];
                }

                // Revert room's related price to discounted
                if (!$this->setToDiscountPrice($discount))
                {
                    // cancel changes
                    $discount = BookingDiscount::deactivate($request->id);
    
                    return [
                        'success' => false,
                        'message' => 'Gagal set harga diskon kost/apt. Silahkan refresh halaman dan coba lagi.',
                    ];
                }
            }
            // command to deactivate
            else
            {
                // Revert room's related price to original
                if (!$this->setToOriginalPrice($discount))
                {
                    // cancel changes
                    $discount = BookingDiscount::activate($request->id);
    
                    return [
                        'success' => false,
                        'message' => 'Gagal mengembalikan harga kost/apt. Silahkan refresh halaman dan coba lagi.',
                    ];
                }
            }

            $discount->is_active = (int) $request->status;
            $discount->save();

            return [
                'success' => true
            ];

        }
        catch (Exception $e) 
        {
            return [
                'success' => false,
                'message' => $e->message,
            ];
        }
    }

    /**
     * Remove discount
     * @param \Illuminate\Http\Request $request
     **/
    public function remove(Request $request)
    {
        try {
            $discount = BookingDiscount::where('id', $request->id)->first();
            if (is_null($discount)) {
                return [
                    'success' => false,
                    'message' => "Gagal menemukan data discount. Silahkan refresh halaman dan coba lagi."
                ];
            }

            if (!$this->setToOriginalPrice($discount)) {
                return [
                    'success' => false,
                    'message' => "Gagal mengembalikan harga kost/apt. Silahkan refresh halaman dan coba lagi.",
                ];
            }

            $discount->delete();
            ActivityLog::LogDelete(Action::DISCOUNT_MANAGEMENT, $request->id);

            return ['success' => true];
        } catch (Exception $e) {
            return [
                'success' => false,
                'message' => $e->message,
            ];
        }
    }

    /**
     * Remove all discount using jobs
     * @param \Illuminate\Http\Request $request
     **/
    public function removeAll()
    {
        if (!empty(FlashSale::getCurrentlyRunningId())) {
            return [
                'success' => false,
                'message' => 'Gagal menghapus semua discount karena terdapat FlashSale yang sedang berjalan'
            ];
        }

        BookingDiscountRemoveAll::dispatchNow();
        ActivityLog::Log(Action::DISCOUNT_MANAGEMENT, 'Delete all booking discount');
        return ['success' => true];
    }

    private function setToOriginalPrice(BookingDiscount $discount)
    {
        // Revert back room's price
        $entity = Room::where('id', $discount->designer_id)->first();
        $priceType = DiscountPriceTypeHelper::getRoomPriceType($discount->price_type);
        if (!is_null($entity)) 
        {
            $entity[$priceType] = $discount->price;
            $entity->save();

            return true;
        }

        return false;
    }

    private function setToDiscountPrice(BookingDiscount $discount, $isEditing = false)
    {
        // Revert back room's price
        $entity = Room::where('id', $discount->designer_id)->first();
        $priceType = DiscountPriceTypeHelper::getRoomPriceType($discount->price_type);
        if (!is_null($entity)) 
        {
            $discount->updatePrice($entity, $isEditing);

            $entity[$priceType] = $discount->getListingPrice();
            $entity->save();

            return true;
        }

        return false;
    }

    public function verifyRoom(Request $request)
    {
        $existedRoom = Room::with('photo')
            ->where('slug', $request->slug)
            ->where('is_active', 'true')
            ->first();

        if (is_null($existedRoom)) {
            return [
                'success' => false,
                'message' => "Kost tidak ditemukan!"
            ];
        }
        
        if ($existedRoom->price_monthly == 0) {
            return [
                'success' => false,
                'message' => "Kost tidak mempunyai harga sewa bulanan!"
            ];
        }

        if ($existedRoom->is_booking != 1) {
            return [
                'success' => false,
                'message' => "Kost tidak mempunyai fitur \"Bisa Booking\"!"
            ];
        }

        $media  = Media::find($existedRoom->photo_id);
        $existedRoom->photo_url = !is_null($existedRoom->photo) ? $existedRoom->photo->getMediaUrl() : [];

        // get room's available prices:
        $roomPrices                         = $existedRoom->price();
        $roomPrices->dailyPrice             = $roomPrices->getPrice('daily');
        $roomPrices->weeklyPrice            = $roomPrices->getPrice('weekly');
        $roomPrices->monthlyPrice           = $roomPrices->getPrice('monthly');
        $roomPrices->quarterlyPrice         = $roomPrices->getPrice('quarterly');
        $roomPrices->semiannuallyPrice      = $roomPrices->getPrice('semiannually');
        $roomPrices->annuallyPrice          = $roomPrices->getPrice('annually');
        $roomPrices->dailyPriceUsd          = $roomPrices->getPrice('daily_usd');
        $roomPrices->weeklyPriceUsd         = $roomPrices->getPrice('weekly_usd');
        $roomPrices->monthlyPriceUsd        = $roomPrices->getPrice('monthly_usd');
        $roomPrices->annuallyPriceUsd       = $roomPrices->getPrice('annually_usd');

        // find discounts which already applied
        $appliedDiscounts = BookingDiscount::getDiscounts($existedRoom->id);
        $availableSlot = [];

        if (!empty($appliedDiscounts)) {
            // if NO discount exists for the room:
            // check availability for new discount
            if ($roomPrices->dailyPrice != 0 && !in_array('daily', $appliedDiscounts)) $availableSlot[] = 'price_daily';
            if ($roomPrices->weeklyPrice != 0 && !in_array('weekly', $appliedDiscounts)) $availableSlot[] = 'price_weekly';
            if ($roomPrices->monthlyPrice != 0 && !in_array('monthly', $appliedDiscounts)) $availableSlot[] = 'price_monthly';
            if ($roomPrices->quarterlyPrice != 0 && !in_array('quarterly', $appliedDiscounts)) $availableSlot[] = 'price_quarterly';
            if ($roomPrices->semiannuallyPrice != 0 && !in_array('semiannually', $appliedDiscounts)) $availableSlot[] = 'price_semiannually';
            if ($roomPrices->annuallyPrice != 0 && !in_array('annually', $appliedDiscounts)) $availableSlot[] = 'price_yearly';
            if ($roomPrices->dailyPriceUsd != 0 && !in_array('daily_usd', $appliedDiscounts)) $availableSlot[] = 'price_daily_usd';
            if ($roomPrices->weeklyPriceUsd != 0 && !in_array('weekly_usd', $appliedDiscounts)) $availableSlot[] = 'price_weekly_usd';
            if ($roomPrices->monthlyPriceUsd != 0 && !in_array('monthly_usd', $appliedDiscounts)) $availableSlot[] = 'price_monthly_usd';
            if ($roomPrices->annuallyPriceUsd != 0 && !in_array('annually_usd', $appliedDiscounts)) $availableSlot[] = 'price_yearly_usd';
        } else {
            // if room HAS some active discounts
            // check availability for new discount
            if ($roomPrices->dailyPrice != 0) $availableSlot[] = 'price_daily';
            if ($roomPrices->weeklyPrice != 0) $availableSlot[] = 'price_weekly';
            if ($roomPrices->monthlyPrice != 0) $availableSlot[] = 'price_monthly';
            if ($roomPrices->quarterlyPrice != 0) $availableSlot[] = 'price_quarterly';
            if ($roomPrices->semiannuallyPrice != 0) $availableSlot[] = 'price_semiannually';
            if ($roomPrices->annuallyPrice != 0) $availableSlot[] = 'price_yearly';
            if ($roomPrices->dailyPriceUsd != 0) $availableSlot[] = 'price_daily_usd';
            if ($roomPrices->weeklyPriceUsd != 0) $availableSlot[] = 'price_weekly_usd';
            if ($roomPrices->monthlyPriceUsd != 0) $availableSlot[] = 'price_monthly_usd';
            if ($roomPrices->annuallyPriceUsd != 0) $availableSlot[] = 'price_yearly_usd';
        }
        
        return [
            'success'       => true,
            'room'          => $existedRoom,
            'available'     => $availableSlot
        ];
    }

    private function filterRoomByType($request, $discounts) {
        $selected = $this->roomTypeFilter[$request->input('room-type') ?? 0];

        if ($selected == 'Mamirooms Kost') {
            $discounts = $discounts->whereHas('room', function($q) {
                $q->where('is_mamirooms', true);
            });
        }
        else if ($selected == 'Booking Kost') {
            $discounts = $discounts->whereHas('room', function($q) {
                $q->where('is_booking', true)->where('is_mamirooms', false);
            });
        }

        return $discounts;
    }

    private function getPriceAfterMarkup($markupValue, $markupType, $realPrice)
    {
        if ($markupType === 'percentage') {
            $priceAfterMarkup = ($realPrice * ($markupValue/100)) + $realPrice;
        } else {
            $priceAfterMarkup = $realPrice + $markupValue;
        }

        return $priceAfterMarkup;
    }

}
