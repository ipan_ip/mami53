<?php

namespace App\Http\Controllers\Admin\Competitor;

use App\Entities\Activity\Action;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\AdminAccess;
use App\Entities\Fake\Faker;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Validator;

class CompetitorUserController extends Controller
{
    private const INDEX_ROUTE = 'admin.competitor.user.index';
    private const PER_PAGE = 20;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!Auth::user()->can(AdminAccess::COMPETITOR)) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        View::share('contentHeader', 'Competitor User Management');
        View::share('user', Auth::user());
    }

    public function index(Request $request)
    {
        ActivityLog::LogIndex(Action::COMPETITOR_USER);

        $query = Faker::query();

        if ($request->filled('q')) {
            $query->where('comment', 'like', '%' . $request->input('q') . '%');
        }
        
        $compUser = $query->paginate(self::PER_PAGE);

        $viewData = [
            'compUser' => $compUser,
            'boxTitle' => 'Manage Competitor User'
        ];
        return view('admin.contents.competitor.user.index', $viewData);
    }

    public function show(Request $request, $id)
    {
        abort(404);
    }

    public function create()
    {
        $viewData = [
            'boxTitle' => 'Create Competitor User'
        ];
        return view('admin.contents.competitor.user.edit', $viewData);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required|numeric',
            'comment' => 'required|max:100'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        $exist = Faker::where('user_id', $request->input('user_id'))->withTrashed()->first();
        if (!is_null($exist)) {
            return redirect()->back()->withInput()->with('errors', $validator->getMessageBag()->add('user_id', 'User already marked as Competitor.'));
        }

        $compUser = new Faker();
        $compUser->user_id = $request->input('user_id');
        $compUser->comment = $request->input('comment');
        $compUser->save();

        ActivityLog::LogCreate(Action::COMPETITOR_USER, $compUser->id);

        return redirect()->route(self::INDEX_ROUTE, ['#competitor'])->with('message', 'Competitor User successfully added.');
    }

    public function edit($id)
    {
        $compUser = Faker::find($id);
        if (is_null($compUser)) {
            abort(404);
        }

        $viewData = [
            'compUser' => $compUser,
            'boxTitle' => 'Edit Competitor User'
        ];
        return view('admin.contents.competitor.user.edit', $viewData);
    }

    public function update(Request $request, $id)
    {
        $compUser = Faker::find($id);
        if (is_null($compUser)) {
            abort(404);
        }

        $validator = Validator::make($request->all(), [
            'user_id' => 'required|numeric',
            'comment' => 'required|max:100'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        $exist = Faker::where('user_id', $request->input('user_id'))->withTrashed()->first();
        if (!is_null($exist) && $exist->id !== $compUser->id) {
            return redirect()->back()->withInput()->with('errors', $validator->getMessageBag()->add('user_id', 'User already marked as Competitor.'));
        }

        $compUser->user_id = $request->input('user_id');
        $compUser->comment = $request->input('comment');
        $compUser->save();

        ActivityLog::LogUpdate(Action::COMPETITOR_USER, $compUser->id);

        return redirect()->route(self::INDEX_ROUTE, ['#competitor'])->with('message', 'Competitor User successfully updated.');
    }

    public function destroy($id)
    {
        $compUser = Faker::find($id);
        if (is_null($compUser)) {
            abort(404);
        }

        $compUser->delete();

        ActivityLog::LogDelete(Action::COMPETITOR_USER, $id);

        return redirect()->route(self::INDEX_ROUTE, ['#point'])->with('message', 'Competitor User successfully deleted.');
    }
}
