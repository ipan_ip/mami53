<?php

namespace App\Http\Controllers\Admin\Competitor;

use App\Entities\Activity\Action;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\AdminAccess;
use App\Entities\Fake\FakeInfo;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Validator;

class FakePhoneController extends Controller
{
    private const INDEX_ROUTE = 'admin.competitor.fake-phone.index';
    private const PER_PAGE = 20;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!Auth::user()->can(AdminAccess::COMPETITOR)) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        View::share('contentHeader', 'Fake Phone Management');
        View::share('user', Auth::user());
    }

    public function index(Request $request)
    {
        ActivityLog::LogIndex(Action::COMPETITOR_FAKE_PHONE);

        $query = FakeInfo::phone();

        if ($request->filled('q')) {
            $query->where('info', 'like', '%' . $request->input('q') . '%');
            $query->orWhere('comment', 'like', '%' . $request->input('q') . '%');
        }
        
        $fakePhone = $query->paginate(self::PER_PAGE);

        $viewData = [
            'fakePhone' => $fakePhone,
            'boxTitle' => 'Manage Fake Phone'
        ];
        return view('admin.contents.competitor.fake-phone.index', $viewData);
    }

    public function show(Request $request, $id)
    {
        abort(404);
    }

    public function create()
    {
        $viewData = [
            'boxTitle' => 'Create Fake Phone'
        ];
        return view('admin.contents.competitor.fake-phone.edit', $viewData);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'info' => 'required|max:100',
            'comment' => 'required|max:100'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        $exist = FakeInfo::where('info', $request->input('info'))->first();
        if (!is_null($exist)) {
            return redirect()->back()->withInput()->with('errors', $validator->getMessageBag()->add('info', 'Info already exist.'));
        }

        $fakePhone = new FakeInfo();
        $fakePhone->info_type = FakeInfo::TYPE_PHONE;
        $fakePhone->info = $request->input('info');
        $fakePhone->comment = $request->input('comment');
        $fakePhone->save();

        ActivityLog::LogCreate(Action::COMPETITOR_FAKE_PHONE, $fakePhone->id);

        return redirect()->route(self::INDEX_ROUTE, ['#competitor'])->with('message', 'Fake Phone successfully added.');
    }

    public function edit($id)
    {
        $fakePhone = FakeInfo::find($id);
        if (is_null($fakePhone)) {
            abort(404);
        }

        $viewData = [
            'fakePhone' => $fakePhone,
            'boxTitle' => 'Edit Fake Phone'
        ];
        return view('admin.contents.competitor.fake-phone.edit', $viewData);
    }

    public function update(Request $request, $id)
    {
        $fakePhone = FakeInfo::find($id);
        if (is_null($fakePhone)) {
            abort(404);
        }

        $validator = Validator::make($request->all(), [
            'info' => 'required|max:100',
            'comment' => 'required|max:100'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        $exist = FakeInfo::where('info', $request->input('info'))->first();
        if (!is_null($exist) && $exist->id !== $fakePhone->id) {
            return redirect()->back()->withInput()->with('errors', $validator->getMessageBag()->add('info', 'Info already exist.'));
        }

        $fakePhone->info = $request->input('info');
        $fakePhone->comment = $request->input('comment');
        $fakePhone->save();

        ActivityLog::LogUpdate(Action::COMPETITOR_FAKE_PHONE, $fakePhone->id);

        return redirect()->route(self::INDEX_ROUTE, ['#competitor'])->with('message', 'Fake Phone successfully updated.');
    }

    public function destroy($id)
    {
        $fakePhone = FakeInfo::find($id);
        if (is_null($fakePhone)) {
            abort(404);
        }

        $fakePhone->delete();

        ActivityLog::LogDelete(Action::COMPETITOR_FAKE_PHONE, $id);

        return redirect()->route(self::INDEX_ROUTE, ['#competitor'])->with('message', 'Fake Phone successfully deleted.');
    }
}
