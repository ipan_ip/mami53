<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use View;
use Auth;
use Validator;
use App\Libraries\CSVParser;
use App\Entities\Room\Room;
use App\Entities\Area\AreaBigMapper;
use App\Entities\Landing\Landing;
use App\Entities\Landing\LandingApartment;
use App\Entities\Landing\LandingVacancy;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;
use App\Http\Helpers\ApiResponse;
use Cache;

class AreaBigMapperController extends Controller
{
    protected $validationMessages = [
        'area_city.required'=>'Area City harus diisi',
        'area_city.max'=>'Area City maksimal :max karakter',
        'area_big.max'=>'Area Big maksimal :max karakter',
        'province.max'=>'Provinsi maksimal :max karakter'
    ];
    
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if(!Auth::user()->can('access-area-big-mapper')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        View::share('contentHeader', 'Area Big Mapper Management');
        View::share('user', Auth::user());
    }

   
    public function index(Request $request)
    {
        $query = AreaBigMapper::query();

        if($request->filled('q') && $request->get('q') != '') {
            $query = $query->where('area_city', 'like', '%' . $request->get('q') . '%')
                        ->orWhere('area_big', 'like', '%' . $request->get('q') . '%')
                        ->orWhere('province', 'like', '%' . $request->get('q') . '%');
        }

        $mappers = $query->orderBy('province')
                        ->paginate(20);

        $viewData = array(
            'boxTitle'  => 'Area Mapper List',
            'mappers'   => $mappers,
        );

        ActivityLog::LogIndex(Action::AREA_BIG_MAPPER);

        return View::make('admin.contents.area-big-mapper.index', $viewData);
    }

    public function create(Request $request)
    {
        $viewData = array(
            'boxTitle'  => 'Create Area Mapper',
            'provinceList' => $this->provinceList
        );
        
        return View::make('admin.contents.area-big-mapper.create', $viewData);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), 
                        [
                            'area_city'=>'required|max:100',
                            'area_big'=>'max:100',
                            'province'=>'max:100'
                        ], $this->validationMessages);

        if($validator->fails()) {
            return redirect()->back()->with('error_message', $validator->errors())->withInput();
        }

        $existingMapper = AreaBigMapper::where('area_city', $request->area_city)
            ->where('area_big', $request->area_big)
            ->first();
        if($existingMapper) {
            return redirect()->back()->with('error_message', 'Area City telah ada sebelumnya')->withInput();
        }

        $mapper = new AreaBigMapper;
        $mapper->area_city = $request->area_city;
        $mapper->area_big = $request->area_big;
        $mapper->province = $request->province;
        $mapper->save();

        ActivityLog::LogCreate(Action::AREA_BIG_MAPPER, $mapper->id);

        Cache::forget('area-big-list');
        Cache::forget('area-city-list');

        return redirect()->route('admin.area-big-mapper.index')->with('message', 'Area Big Mapper berhasil ditambahkan');
    }

    public function edit(Request $request, $mapperId)
    {
        $mapper = AreaBigMapper::find($mapperId);

        if(!$mapper) {
            return redirect()->back()->with('error_message', 'Data tidak ditemukan');
        }

        $landings = Landing::where('area_city', $mapper->area_city)->get();
        $landingsApartment = LandingApartment::where('area_city', $mapper->area_city)->get();
        $landingsJob = LandingVacancy::where('area_city', $mapper->area_city)->get();

        $viewData = array(
            'boxTitle'  => 'Edit Area Mapper',
            'mapper'    => $mapper,
            'landings'  => $landings,
            'landingsApartment' => $landingsApartment,
            'landingsJob' => $landingsJob,
            'provinceList' => $this->provinceList
        );
        
        return View::make('admin.contents.area-big-mapper.edit', $viewData);
    }

    public function update(Request $request, $mapperId)
    {
        $mapper = AreaBigMapper::find($mapperId);

        if(!$mapper) {
            return redirect()->back()->with('error_message', 'Data tidak ditemukan');
        }

        $validator = Validator::make($request->all(), 
                        [
                            'area_city'=>'required|max:100',
                            'area_big'=>'max:100',
                            'province'=>'max:100'
                        ], $this->validationMessages);

        if($validator->fails()) {
            return redirect()->back()->with('error_message', $validator->errors())->withInput();
        }

        $existingMapper = AreaBigMapper::where('area_city', $request->area_city)
                                    ->where('area_big', $request->area_big)
                                    ->where('id', '<>', $mapperId)
                                    ->first();
        if($existingMapper) {
            return redirect()->back()->with('error_message', 'Area City telah ada sebelumnya');
        }

        $mapper->area_city = $request->area_city;
        $mapper->area_big = $request->area_big;
        $mapper->province = $request->province;
        $mapper->save();

        Cache::forget('area-big-list');
        Cache::forget('area-city-list');

        ActivityLog::LogUpdate(Action::AREA_BIG_MAPPER, $mapperId);
        
        return redirect()->route('admin.area-big-mapper.index')->with('message', 'Area Big Mapper berhasil dirubah');
    }

    public function updateAssociateLanding(Request $request, $mapperId)
    {
        $mapper = AreaBigMapper::find($mapperId);

        if(!$mapper) {
            return redirect()->back()->with('error_message', 'Data tidak ditemukan');
        }

        if($request->filled('subdistrict')) {
            $landingIds = $request->input('landing_id');

            foreach($request->input('subdistrict') as $key => $subdistrict) {
                if($landingIds[$key] == '') continue;

                $landing = Landing::find($landingIds[$key]);

                if(!$landing) continue;

                $landing->area_city = $mapper->area_city;
                $landing->area_subdistrict = $subdistrict;
                $landing->save();
            }
        }

        return redirect()->back()->with('message', 'Associate Landing berhasil dirubah');
    }

    public function removeAssociateLanding(Request $request, $landingId)
    {
        $landing = Landing::find($landingId);

        if(!$landing) {
            return redirect()->back()->with('error_message', 'Landing tidak ditemukan');
        }

        $landing->area_city = null;
        $landing->area_subdistrict = null;
        $landing->save();

        return redirect()->back()->with('message', 'Associate Landing berhasil dirubah');
    }

    public function updateAssociateLandingApartment(Request $request, $mapperId)
    {
        $mapper = AreaBigMapper::find($mapperId);

        if(!$mapper) {
            return redirect()->back()->with('error_message', 'Data tidak ditemukan');
        }

        if($request->filled('subdistrict_apartment')) {
            $landingIds = $request->input('landing_id_apartment');

            foreach($request->input('subdistrict_apartment') as $key => $subdistrict) {
                if($landingIds[$key] == '') continue;

                $landing = LandingApartment::find($landingIds[$key]);

                if(!$landing) continue;

                $landing->area_city = $mapper->area_city;
                $landing->area_subdistrict = $subdistrict;
                $landing->save();
            }
        }

        return redirect()->back()->with('message', 'Associate Landing Apartment berhasil dirubah');
    }

    public function removeAssociateLandingApartment(Request $request, $landingId)
    {
        $landing = LandingApartment::find($landingId);

        if(!$landing) {
            return redirect()->back()->with('error_message', 'Landing Apartment tidak ditemukan');
        }

        $landing->area_city = null;
        $landing->area_subdistrict = null;
        $landing->save();

        return redirect()->back()->with('message', 'Associate Landing Apartment berhasil dirubah');
    }

    public function updateAssociateLandingJobs(Request $request, $mapperId)
    {
        $mapper = AreaBigMapper::find($mapperId);

        if(!$mapper) {
            return redirect()->back()->with('error_message', 'Data tidak ditemukan');
        }

        if($request->filled('subdistrict_jobs')) {
            $landingIds = $request->input('landing_id_jobs');

            foreach($request->input('subdistrict_jobs') as $key => $subdistrict) {
                if($landingIds[$key] == '') continue;

                $landing = LandingVacancy::find($landingIds[$key]);

                if(!$landing) continue;

                $landing->area_city = $mapper->area_city;
                $landing->area_subdistrict = $subdistrict;
                $landing->save();
            }
        }

        return redirect()->back()->with('message', 'Associate Landing berhasil dirubah');
    }

    public function removeAssociateLandingJobs(Request $request, $landingId)
    {
        $landing = LandingVacancy::find($landingId);

        if(!$landing) {
            return redirect()->back()->with('error_message', 'Landing tidak ditemukan');
        }

        $landing->area_city = null;
        $landing->area_subdistrict = null;
        $landing->save();

        return redirect()->back()->with('message', 'Associate Landing berhasil dirubah');
    }

    public function destroy(Request $request, $mapperId)
    {
        $mapper = AreaBigMapper::find($mapperId);

        if(!$mapper) {
            return redirect()->back()->with('error_message', 'Data tidak ditemukan');
        }

        $mapper->delete();

        ActivityLog::LogDelete(Action::AREA_BIG_MAPPER, $mapperId);

        return redirect()->route('admin.area-big-mapper.index')->with('message', 'Area Big Mapper berhasil dihapus');
    }

    /**
     * Sync area big mapper in room with this data
     *
     * @param int $mapperId
     */
    public function sync(Request $request, $mapperId)
    {
        $mapper = AreaBigMapper::find($mapperId);

        if(!$mapper) {
            return redirect()->back()->with('error_message', 'Data tidak ditemukan');
        }

        Room::where('area_city', $mapper->area_city)
            ->update([
                'area_big'=>!is_null($mapper->area_big) && $mapper->area_big != '' ? $mapper->area_big : (!is_null($mapper->province) && $mapper->province != '' ? $mapper->province : $mapper->area_city)
            ]);
                
        return redirect()->route('admin.area-big-mapper.index')->with('message', 'Data Area Big di Kost / Apartemen berhasil diupdate');
    }

    public function parse(Request $request)
    {
        $viewData   = [
            'boxTitle'          => 'Parse City and Province CSV',
        ];

        return view('admin.contents.area-big-mapper.parser', $viewData);
    }

    public function parsePost(Request $request)
    {
        $this->validate($request, [
            'file_csv'=>'required|mimes:csv,txt'
        ]);

        $csv = $request->file_csv->path();

        $csvArray = CSVParser::csv_parse($csv, ['eol'=>"\r\n"]);

        $existingMappers = AreaBigMapper::select(\DB::raw('LOWER(province) as `province`, LOWER(area_city) as `area_city`'))
                                ->get()->pluck('province', 'area_city')->toArray();
        $existingCities = array_keys($existingMappers);

        $failedRow = [];
        foreach($csvArray as $key=>$row) {
            try {
                if(in_array(strtolower($row['area_city']), $existingCities)) {
                    if(is_null($existingMappers[strtolower($row['area_city'])]) || $existingMappers[strtolower($row['area_city'])] == '') {
                        AreaBigMapper::where('area_city', $row['area_city'])
                                    ->update([
                                        'province'=>$row['province']
                                    ]);
                    } else {
                        $failedRow[] = 'Gagal input karena Provinsi sudah terisi : baris ' . $key;
                        continue;
                    }
                } else {
                    $newMapper = new AreaBigMapper;
                    $newMapper->area_city = $row['area_city'];
                    $newMapper->province = $row['province'];
                    $newMapper->save();
                }
            } catch (\Exception $e) {
                $failedRow[] = 'Gagal diimport : ' . $key ;
            }
            
        }

        return redirect()->back()
                        ->with(!empty($failedRow) ? 'errors' : 'message', 
                            !empty($failedRow) ? collect($failedRow) : 'Sukses input data');
    }


    public function landingSuggestion(Request $request)
    {
        $params = $request->params;

        $landings = Landing::whereNull('redirect_id')
                            ->where(function($query) use ($params) {
                                $query->where('slug', 'like', '%' . $params . '%')
                                    ->orWhere('heading_1', 'like', '%' . $params . '%');
                            })
                            ->take('10')
                            ->get()
                            ->pluck('heading_1', 'id');

        return ApiResponse::responseData([
            'landings'=>$landings
        ]);
    }

    public function suggestionCityRegency(Request $request)
    {
        $params = $request->params;

        $landings = AreaBigMapper::where('area_city', 'like', '%' . $params . '%')
                            ->take('10')
                            ->get()
                            ->pluck('area_city', 'id');

        return ApiResponse::responseData([
            'landings'=>$landings
        ]);
    }

    public function landingSuggestionApartment(Request $request)
    {
        $params = $request->params;

        $landings = LandingApartment::whereNull('redirect_id')
                            ->where(function($query) use ($params) {
                                $query->where('slug', 'like', '%' . $params . '%')
                                    ->orWhere('heading_1', 'like', '%' . $params . '%');
                            })
                            ->take('10')
                            ->get()
                            ->pluck('heading_1', 'id');

        return ApiResponse::responseData([
            'landings'=>$landings
        ]);
    }

    public function landingSuggestionJobs(Request $request)
    {
        $params = $request->params;

        $landings = LandingVacancy::whereNull('redirect_id')
                            ->where(function($query) use ($params) {
                                $query->where('slug', 'like', '%' . $params . '%')
                                    ->orWhere('heading_1', 'like', '%' . $params . '%');
                            })
                            ->take('10')
                            ->get()
                            ->pluck('heading_1', 'id');

        return ApiResponse::responseData([
            'landings'=>$landings
        ]);
    }

    protected $provinceList = [
        'Banten',
        'Jawa Barat',
        'DKI Jakarta',
        'Jawa Tengah',
        'DI Yogyakarta',
        'Jawa Timur',
        'Aceh',
        'Sumatera Utara',
        'Sumatera Barat',
        'Riau',
        'Jambi',
        'Sumatera Selatan',
        'Bengkulu',
        'Lampung',
        'Kepulauan Bangka Belitung',
        'Kepulauan Riau',
        'Kalimantan Barat',
        'Kalimantan Tengah',
        'Kalimantan Selatan',
        'Kalimantan Timur',
        'Sulawesi Utara',
        'Sulawesi Tengah',
        'Sulawesi Selatan',
        'Sulawesi Tenggara',
        'Gorontalo',
        'Sulawesi Barat',
        'Bali',
        'Nusa Tenggara Timur',
        'Nusa Tenggara Barat',
        'Maluku',
        'Maluku Utara',
        'Papua',
        'Papua Barat',
        'Kalimantan Utara',
    ];


}
