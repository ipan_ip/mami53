<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Media\Media;
use App\Entities\Room\Element\Tag;
use App\Entities\Room\Room;
use App\Http\Controllers\Controller;
//use Goutte\Client;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Session;
use App\Entities\Room\Element\Card;
use App\Entities\User\UserCheckerTracker;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;
use Illuminate\Http\Request as HttpRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Storage;


class CardController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Group Controller
    |--------------------------------------------------------------------------
    |
    | Used to authenticate whether it is user or administrator
    | and redirect to apropriate page
    |
    */

    protected $baseRoute = '';

    /**
     * The layout that should be used for responses.
     */
    protected $layout = 'embed.layouts.main';

    protected $rules = array(
        'admin.stories.style.store' => array(
            'title'      => 'required|min:3|max:30',
        ),
        'admin.stories.style.update' => array(
            'title' => 'required|min:3|max:30',
        ),
        'stories.style.store' => array(
            'title'      => 'required|min:3|max:30',
        ),
        'stories.style.update' => array(
            'title' => 'required|min:3|max:30',
        )
    );

    public function __construct()
    {
//        parent::__construct();
//        if (Request::is('admin/*'))
//        {
//            $this->baseRoute = 'admin.';
//            $this->layout = 'admin.layouts.main';
//            $this->login_type = 'admin';
//            $this->user = (object) array('name'=>'admin');
//        }
//        else if (Request::is('user/*'))
//        {
//            $this->baseRoute = 'user.';
//            $this->layout = 'embed.layouts.main';
//            $this->login_type = 'user';
//            $this->user = Session::get('embed.designer.data');
//        }
        View::share('user' , Auth::user());
        View::share('contentHeader', 'Media Management');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($designerId = null, $is_verified = false)
    {
        if(Request::is('admin/card')) $viewData['designerId'] = 0;

        if (!is_null($designerId)) 
        {
            $room = Room::find($designerId);
            $roomName = !is_null($room) ? $room->name : '';
        } 

        $cards = Card::with('photo')
            ->where('designer_id', '=', $designerId)
            ->where('type', '<>', 'video');

        if ($is_verified) $cards->where('is_verified', '=', "N");

        $cards = $cards->get();

        // prevent calling grouping function from view 
        $cards = $cards->map(function($card) {
            $card->photo_group_name = $card->photo_group;
            return $card;
        });

        $viewData = array(
            'login_type'  => Auth::user()->role,
            'boxTitle'  => 'Media List: <strong>' . $roomName . '</strong>',
            'designerId' => $designerId,
            'designer' => $room,
            'rowsStyle' => $cards,
            'answerAction'  => "admin.card.get",
            'deleteAction' => "admin.card.destroy",
            'editAction' => "admin.card.edit",
            'createAction' => "admin.card.create",
            'rotateAction' => "admin.card.rotate",
            'coverAction' => "admin.card.cover",
            'previewAction' => "admin.card.preview",
            'photoGroupUpdate' => route("admin.card.update.photo-group"),
            'cancelAction' => url()->previous(),
            'photoGroup' => Card::PHOTO_GROUP_NAME,
        );

        return View::make('admin.contents.style.index', $viewData);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create($designerId)
    {
        $rowStyle = new Card;

        $rowStyle->title      = Input::old('title');
        $rowStyle->gender      = Input::old('gender');

        //ambil card
        $cards = Input::old('card');
        if(isset($cards))
        {
            foreach($cards as $card){
                $rowStyle->type       = $card['type'];
                $rowStyle->video_url  = $card['video_url'];
                $rowStyle->video_by_mamichecker = $card['video_by_mamichecker'];
                $rowStyle->photo_id   = $card['photo_id'];
                $rowStyle->description= $card['description'];
                $rowStyle->photo_group_name = $card['photo_group'];
                $rowStyle->url_ori    = $card['url_ori'];
                $rowStyle->source     = $card['source'];
                $rowStyle->ordering   = $card['ordering'];
            }
        }
        else
        {
            $rowStyle->type       = NULL;
            $rowStyle->video_url  = NULL;
            $rowStyle->video_by_mamichecker = NULL;
            $rowStyle->photo_id   = NULL;
            $rowStyle->description= NULL;
            $rowStyle->photo_group_name = NULL;
            $rowStyle->url_ori    = NULL;
            $rowStyle->source     = NULL;
            $rowStyle->ordering   = NULL;
        }


        $rowStyle->photo_id = Input::old('photo_id');

        if (Input::old('photo_id') !== NULL)
        {
            $media = Media::find(Input::old('photo_id'))->getMediaUrl();
            $photo_id = Input::old('photo_id');
            $photo = $media['medium'];
        }
        else
        {
            $photo_id = 0;
            $photo = NULL;
        }

        for($i=1;$i<=4;$i++){
            if(Input::old('photo_'.$i.'_id')){
                $rowStyle->{'photo_'.$i.'_id'} = Input::old('photo_'.$i.'_id');
                $media = Media::find(Input::old('photo_'.$i.'_id'))->getMediaUrl();
                $photo = $media['medium'];
            } else {
                $photo = null;
                $rowStyle->{'photo_'.$i.'_id'} = 0;
            }
            $rowStyle->{'photo_'.$i} = $photo;
        }

        $rowStyle->photo          = $photo;
        $rowStyle->photo_id       = $photo_id;
        $rowStyle->emotion_ids      = Input::old('emotion_ids');
        $rowStyle->keyword_ids      = Input::old('keyword_ids');

        $rowsGender = array('male', 'female');

        $rowsGenderView = mySelectPreprocessing(
            'single',
            'checked',
            'value',
            $rowsGender,
            $rowStyle->gender
        );

        $rowsEmotion = Tag::where('type', '=', 'emotion')
            ->get();

        $rowsEmotionView = mySelectPreprocessing(
            'multi',
            'selected',
            'key-value',
            $rowsEmotion,
            $rowStyle->emotion_ids
        );

        $rowsKeyword = Tag::where('type', '=', 'keyword')
            ->get();

        $rowsKeywordView = mySelectPreprocessing(
            'multi',
            'selected',
            'key-value',
            $rowsKeyword,
            $rowStyle->keyword_ids
        );

        $viewData = array(
            'boxTitle'    => 'Add Media',
            'login_type'  => Auth::user()->role,
            'designerId'  => $designerId,
            'rowStyle'    => $rowStyle,
            'rowsGender'  => $rowsGenderView,
            'rowsEmotion' => $rowsEmotionView,
            'rowsKeyword' => $rowsKeywordView,
            'formAction'  => route("admin.card.store", $designerId),
            'formMethod'  => 'POST',
            'formCancel'  => url()->previous(),
            'photoGroup' => Card::PHOTO_GROUP_NAME,
        );

        // if $designerId is zero mean this style created by admin, and admin has to choose the designer in form
        // if canceled back to admin page
        if ($designerId==0){
            $viewData['formCancel'] = route("admin.style.index");
            $viewData['rowsDesigner'] = Room::select(DB::Raw('id as designer_id, name'))->get();
        }

        return View::make('admin.contents.style.form', $viewData);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store($designerId)
    {
        $cards = Input::get('cards');

        if (empty($cards)) {
            return redirect()->route('admin.card.create', $designerId)
                ->with('error_message', 'Aborted')
                ->withInput();
        }

        if (($cards[0]['type'] == 'image' || $cards[0]['type'] == 'quiz') && empty($cards[0]['photo_id'])) {
            return redirect()->route('admin.card.create', $designerId)
            ->with('error_message', 'Photo has not been uploaded yet')
            ->withInput();
        }

        if( $cards[0]['type'] == 'image' && !array_key_exists($cards[0]['group'],Card::PHOTO_GROUP_NAME)){
            return redirect()->route('admin.card.create', $designerId)
            ->with('error_message', 'Select Valid Photo Group')
            ->withInput();
        }

        DB::beginTransaction();

        try {
            $success = Card::register($cards, $designerId);
            if(!$success){
                DB::rollback();
                return redirect()->route('admin.card.create', $designerId)
                ->with('error_message', "Error Occurred")
                ->withInput();
            }

            if($cards[0]['type'] == 'image'){
                Room::where('id', $designerId)
                    ->update([
                        'photo_count' => \DB::raw('IFNULL(photo_count, 0) + 1')
                    ]);
            }
            
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->route('admin.card.create', $designerId)
                ->with('error_message', "Aborted. {$e->getMessage()}")
                ->withInput();
        }

        return redirect()->route('admin.card.index', $designerId)
            ->with('message', 'New Card Created');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return View
     */
    public function edit($designerId, $id)
    {
        $isCover = false;

        $card = Card::where('id', $id)->with('photo')->first();
        if (is_null($card))
        {
            return back()->with('error_message', 'Media tidak ditemukan');
        }

        if ($card->type == 'image')
        {
            if (strpos($card->description, '-cover') !== FALSE) $isCover = true;

            if (!is_null($card->photo))
            {
                $media = $card->photo->getMediaUrl();
                $card->photo      = $media['medium'];
                $card->photo_real = $media['real'];
            }

            // prevent calling grouping function from view 
            $card->photo_group_name = $card->photo_group;
        }

        $viewData = array(
            'boxTitle'    => 'Edit Media',
            'login_type'  => Auth::user()->role,
            'is_cover'    => $isCover,
            'designerId'  => $designerId,
            'rowStyle'    => $card,
            'formAction'  => route("admin.card.update", array($designerId, $card->id)),
            'formMethod'  => 'PUT',
            'formCancel'  => url()->previous(),
            'photoGroup' => Card::PHOTO_GROUP_NAME,
        );

        return view('admin.contents.style.form', $viewData);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($designerId, $id)
    {
        $cards = Input::get('cards');

        $style = Card::find($id);
        if (!$style instanceof Card) {
            return  redirect()->route('admin.card.index', [$designerId])
            ->with('message', 'Style Not Found');
        }

        if(empty($cards)){
            return  redirect()->route("admin.card.create", $designerId)
                ->with('error_message', 'Aborted')
                ->withInput();
        }

        if (($cards[0]['type'] == 'image' || $cards[0]['type'] == 'quiz') && empty($cards[0]['photo_id'])) {
            return redirect()->route('admin.card.create', $designerId)
            ->with('error_message', 'Photo has not been uploaded yet')
            ->withInput();
        }

        if( $cards[0]['type'] == 'image' && !array_key_exists($cards[0]['group'],Card::PHOTO_GROUP_NAME)){
            return redirect()->route('admin.card.create', $designerId)
            ->with('error_message', 'Select Valid Photo Group')
            ->withInput();
        }
        
        DB::beginTransaction();

        try
        {
            $oldType = $style->type;
            $success = Card::updateCards($cards, $designerId, $id);
            if(!$success){
                DB::rollback();
                return redirect()->route('admin.card.edit', array($designerId, $id))
                ->with('error_message', "Error Occurred")
                ->withInput();
            }
            if($oldType == 'image' && $cards[0]['type'] != 'image'){
                Room::where('id', $designerId)->where('photo_count', '>', 0)
                    ->update([
                        'photo_count' => \DB::raw('photo_count - 1')
                    ]);
            }

            if($oldType != 'image' && $cards[0]['type'] == 'image'){
                Room::where('id', $designerId)
                    ->update([
                        'photo_count' => \DB::raw('photo_count + 1')
                    ]);
            }
            
            DB::commit();

        }
        catch (Exception $e)
        {
            DB::rollback();
            return  redirect()->route("admin.card.edit", array($designerId, $id))
                ->with('error_message', "Aborted. {$e->getMessage()}")
                ->withInput();
        }

        ActivityLog::LogUpdate(Action::HOUSE_PROPERTY, $id);

        return  redirect()->route("admin.card.index", array($designerId))
            ->with('message', 'Style Updated');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($designerId, $id)
    {
        $style = Card::find($id);
        if (!$style instanceof Card) {
            return  redirect()->route('admin.card.index', [$designerId])
            ->with('message', 'Style Not Found');
        }

        DB::beginTransaction();

        try {
            $type = $style->type;
            $success = $style->delete();

            if(!$success){
                return  redirect()->route('admin.card.index', [$designerId])
                ->with('message', 'Error occurred');
            }

            if($type=='image' ){
                Room::where('id', $designerId)->where('photo_count', '>', 0)
                    ->update([
                        'photo_count' => \DB::raw('photo_count - 1')
                    ]);
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return  redirect()->route('admin.card.index', [$designerId])
            ->with('message', 'Error occurred');
        }

        return  redirect()->route('admin.card.index', [$designerId])
            ->with('message', 'Style deleted');
    }

    // public function getRotate($styleId)
    // {
    //     $style = Card::find($styleId);
    //     $medias = Media::findOrFail($style->photo_id)->getMediaUrl();

    //     foreach ($medias as $key => $media) {
    //         $url = Config::get('api.media.cdn_url');

    //         $path = str_replace($url, public_path('/'), $media);

    //         if (file_exists($path)) {
    //             $image = Image::make($path);
    //             $image->rotate(-90)->save($path);
    //         }
    //     }

    //     Media::find($style->photo_id)->renameMedia();

    //     return redirect()->route('admin.card.index', $style->designer_id);
    // }

    public function getRotate($styleId)
    {
        $style = Card::find($styleId);
        $mediaObject = Media::findOrFail($style->photo_id);
        // $medias = $mediaObject->getMediaUrl();

        // foreach ($medias as $key => $media) {
        //     $url = Config::get('api.media.cdn_url');
        //     $folderCacheConfig = Config::get('api.media.folder_cache');

        //     // $path = str_replace($url, public_path('/'), $media);
        //     $fileExists = false;

        //     $sourcePath = str_replace($url, '', $media);
        //     if(Config::get('filesystems.default') == 'upload') {
        //         $source = Storage::getDriver()->getAdapter()->getPathPrefix() . $sourcePath; 
        //         $file_exists = file_exists($source);
        //     } else {
        //         $source = Storage::url($sourcePath);
        //         $file_exists = Storage::exists($source);
        //     }
            
        //     if($file_exists) {
        //         $image = Image::make($source);
        //         $image->rotate(-90);

        //         $imgStream = $image->stream();

        //         Storage::put($sourcePath, $imgStream->__toString());

        //         $image->destroy();
        //     }

        // }

        // Media::find($style->photo_id)->renameMedia();
        $newPhoto = $mediaObject->rotate();
        if(!is_null($newPhoto)) {
            $style->photo_id = $newPhoto['id'];
            $style->save();
        }

        return redirect()->route('admin.card.index', $style->designer_id);
    }

    public function setCover($cardId)
    {
        $card = Card::find($cardId);
        
        if (! $card instanceof Card) {
            return back()->with('error_message', 'ID card photo tidak ditemukan/tidak valid');
        }

        if (! $card->getAllowAsCoverAttribute()) {
            return back()->with('error_message', 'Hanya foto dalam kamar yang bisa dijadikan cover photo.');
        }

        if (empty($card->designer_id)) {
            return back()->with('error_message', 'Error: Kost tidak ditemukan');
        }

        $room = Room::find($card->designer_id);
        if (is_null($room))
        {
            return back()->with('error_message', 'Error: Kost tidak ditemukan');
        }
        
        // set whether it should be activated directly,
        // or just switching cover photo
        if ($card->photo_id != $room->photo_id && !$room->isPremiumPhoto())
        {
            $room->assignCover('reguler', $card->photo_id, true);
        }
        else
        {
            $room->assignCover('reguler', $card->photo_id);
        }

        return redirect()->route('admin.card.index', $room->id);
    }

    public function getPreview($styleId)
    {
        $card = Card::findOrFail($styleId);
        if (
            $card->photo
            && isset($card->photo->getMediaUrl()['real'])
        ) {
            return redirect($card->photo->getMediaUrl()['real']);
        } else {
            return null;
        }
    }

     /**
     * Update Photo Group.
     *
     * @return Response
     */
    public function updatePhotoGroup(HttpRequest $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|exists:designer_style,id',
            'group' => 'required|in:building_front,building_inside,building_roadview,room_front,room_inside,bathroom,other',
        ]);

        $validator->after(function ($validator) use ($request) {
            $card = Card::find($request->id);

            if (! $card instanceof Card) {
                $validator->errors()->add('id', 'Foto tidak ditemukan.');
            } elseif ($card->is_cover || strpos($card->description, 'cover') !== false) {
                $validator->errors()->add('id', 'Tidak bisa merubah grup dari foto cover.');
            }
        });

        if ($validator->fails()) {
            return [
                'success' => false,
                'message' => implode(', ', array_flatten($validator->errors()->messages())),
                'error' => $validator->errors()
            ];
        }
        
        try {
            Card::updatePhotoGroup($request->id, $request->group);
        } catch (Exception $e) {
            return [
                'success' => false,
                'message' => "Terjadi error saat merubah Photo Group. Silakan refresh halaman dan coba lagi.",
                'error' => $e->getMessage()
            ];
        }

        ActivityLog::LogUpdate(Action::KOST_PHOTO, $request->id);

        return [
            'success' => true,
            'message' => "Photo Group Berhasil dirubah",
        ];
    }
}
