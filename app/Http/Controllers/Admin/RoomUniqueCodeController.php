<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Bugsnag;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel as Importer;
use App\Http\Controllers\Controller;
use App\Entities\Room\Room;
use App\Entities\Room\Element\UniqueCode;
use App\Entities\Area\AreaCity;
use App\Entities\Area\AreaCityNormalized;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;
use App\Libraries\AreaCityCodeImporter;
use App\Presenters\RoomPresenter;
use RuntimeException;
use View;

class RoomUniqueCodeController extends Controller
{
    protected $storagePath = 'area_unique_code';

    public function __construct()
    {
        $this->middleware(
            function ($request, $next) {
                if (!Auth::user()->can('access-kost') && !Auth::user()->can('access-booking')) {
                    return redirect('admin');
                }

                return $next($request);
            }
        );

        View::share('user', Auth::user());
        View::share('contentHeader', 'Unique Code Management');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return Factory|\Illuminate\View\View
     * @throws Exception
     */
    public function index(Request $request)
    {
        $query = Room::select(
            [
                'id',
                'song_id',
                'name',
                'area_city',
                'photo_id',
                'kost_updated_date',
                'is_active',
                'is_mamirooms',
                'created_at',
                'updated_at'
            ]
        )
            ->with(
                [
                    'photo_by' => function ($t) {
                        $t->orderBy('id', 'desc');
                    },
                    'normalized_city',
                    'view_promote',
                    'photo',
                    'active_discounts',
                    'attachment',
                    'checkings',
                    'unique_code'
                ]
            )
            ->whereHas('unique_code');

        // Apply Filters
        $query = $this->filter($request, $query);

        $rowsRoom = $query
            ->orderBy('updated_at', 'desc')
            ->paginate(10);

        $rooms = (new RoomPresenter('ucode'))->present($rowsRoom);

        $viewData = [
            'rowsDesigner' => $rowsRoom,
            'rowsRoom' => $rooms['data'],
            'boxTitle' => 'Kost Booking with Codes List',
            'searchUrl' => route('admin.room.ucode.index'),
        ];

        ActivityLog::LogIndex(Action::BOOKING_CODE);

        return view('admin.contents.unique-code.index', $viewData);
    }

    private function filter(Request $request, $query)
    {
        if ($request->filled('q')) {
            $query->where(
                function ($q) use ($request) {
                    $q->where('id', '=', $request->input('q'))
                        ->orWhere('song_id', '=', $request->input('q'))
                        ->orWhere('name', 'like', '%' . $request->input('q') . '%');
                }
            );
        }

        return $query;
    }

    /**
     * Display a code setting for every city.
     *
     * @param Request $request
     *
     * @return Factory|\Illuminate\View\View
     */
    public function setting(Request $request)
    {
        $cityCodes = AreaCity::select()
            ->orderBy('updated_at', 'desc')
            ->where('code', '<>', '')
            ->where(
                function ($q) use ($request) {
                    if ($request->filled('q')) {
                        $q->where('name', 'like', '%' . $request->q . '%');
                    }
                    if ($request->filled('c')) {
                        $q->where('code', $request->c);
                    }
                }
            )
            ->get()
            ->groupBy('code', true);

        // Getting bookable rooms count
        foreach ($cityCodes as $key => $values) {
            foreach ($values as $value) {
                $value->total_booking_rooms_active = AreaCityNormalized::where('normalized_city', $value->name)
                    ->whereHas(
                        'room',
                        function ($q) {
                            $q->where('is_booking', 1)
                                ->where('is_active', 'true')
                                ->where('expired_phone', 0);
                        }
                    )
                    ->count();

                $value->total_booking_rooms_nonactive = AreaCityNormalized::where('normalized_city', $value->name)
                    ->whereHas(
                        'room',
                        function ($q) {
                            $q->where('is_booking', 1)
                                ->where(
                                    function ($q) {
                                        $q->where('is_active', 'false')->orWhere('expired_phone', 1);
                                    }
                                );
                        }
                    )
                    ->count();
            }
        }

        $viewData = [
            'rowsCode' => $cityCodes,
            'boxTitle' => 'Kost Code Setting',
            'searchUrl' => route('admin.room.ucode.setting'),
        ];
        return view('admin.contents.unique-code.setting', $viewData);
    }

    /**
     * Add new city code
     *
     * @param Request $request
     *
     * @return array
     */
    public function add(Request $request)
    {
        try {
            $citiesArray = $request->cities;

            // updating each city
            AreaCity::whereIn('id', $citiesArray)->update(['code' => $request->code]);

            foreach ($citiesArray as $id) {
                ActivityLog::LogCreate(Action::AREA_CITY_CODE, $id);
            }
        } catch (Exception $e) {
            return [
                "success" => false,
                "data" => $e->getMessage(),
            ];
        }

        return [
            "success" => true,
            "data" => $request->all(),
        ];
    }

    /**
     * Update cities within spesific code
     *
     * @param Request $request
     *
     * @return array
     */
    public function update(Request $request)
    {
        try {
            $citiesArray = $request->cities;
            $targetCode = $request->code;

            // remove code from cities which is removed from current BBK area code
            $removedCities = AreaCity::where('code', $targetCode)
                ->whereNotIn('id', $citiesArray)
                ->pluck('name');

            if ($removedCities->count()) {
                $removalTargetRooms = Room::whereHas(
                    'normalized_city',
                    function ($q) use ($removedCities) {
                        $q->whereIn('normalized_city', $removedCities);
                    }
                )
                    ->pluck('id');

                if ($removalTargetRooms->count()) {
                    UniqueCode::whereIn('designer_id', $removalTargetRooms)->delete();
                }

                // reset each city's 'is_generated' status
                $removedCities = AreaCity::whereIn('name', $removedCities);
                $removedCities->update(['code' => null, 'is_generated' => 0]);

                foreach ($removedCities->pluck('id') as $id) {
                    ActivityLog::LogDelete(Action::AREA_CITY_CODE, $id);
                }
            }

            // remove code from cities which is added to current BBK area code
            $addedCities = AreaCity::whereIn('id', $citiesArray)
                ->where(
                    function ($q) use ($targetCode) {
                        $q->WhereNull('code')->orWhere('code', '<>', $targetCode);
                    }
                )
                ->pluck('name');

            if ($addedCities->count()) {
                $addingTargetRooms = Room::whereHas(
                    'normalized_city',
                    function ($q) use ($addedCities) {
                        $q->whereIn('normalized_city', $addedCities);
                    }
                )
                    ->with('unique_code')
                    ->where('is_booking', 1)
                    ->pluck('id');

                if ($addingTargetRooms->count()) {
                    UniqueCode::whereIn('designer_id', $addingTargetRooms)->delete();
                }

                // reset each city's 'is_generated' status
                $addedCities = AreaCity::whereIn('name', $addedCities);
                $addedCities->update(['code' => $targetCode, 'is_generated' => 0]);

                foreach ($addedCities->pluck('id') as $id) {
                    ActivityLog::LogCreate(Action::AREA_CITY_CODE, $id);
                }
            }
        } catch (Exception $e) {
            return [
                "success" => false,
                "data" => $e->getMessage(),
            ];
        }

        return [
            "success" => true,
        ];
    }

    /**
     * Generate codes for all booking kost within specific city
     *
     * @param Request $request
     *
     * @return array
     */
    public function generateCode(Request $request)
    {
        try {
            $room = Room::find($request->id);

            if (is_null($room)) {
                return [
                    "success" => false,
                    "data" => "Gagal mendapatkan data kos",
                ];
            }

            $room->generateUniqueCode();

            // Only add active Kos to room index
            if ($room->isActive()) {
                $room->addToElasticsearch();
            }
        } catch (Exception $e) {
            Bugsnag::notifyException($e);

            return [
                "success" => false,
                "data" => $e->getMessage(),
            ];
        }

        return [
            "success" => true,
        ];
    }

    public function bulkGenerateCodes()
    {
        try {
            $chunks =  Room::with(
                [
                    'unique_code'
                ]
            )
                ->where('is_booking', 1)
                ->get()
                ->chunk(100);

            foreach ($chunks as $chunk) {
                if (!$chunk->count()) {
                    return [
                        "success" => false,
                        "data" => "Failed fetching Kos data",
                    ];
                }

                foreach ($chunk as $room) {
                    $room->generateUniqueCode();

                    $room = null;
                    unset($room);
                }
            }

            return response()->json(
                [
                    "success" => true,
                    "message" => 'All booking codes successfully regenerated',
                ]
            );
        } catch (Exception $e) {
            Bugsnag::notifyException($e);

            return response()->json(
                [
                    "success" => false,
                    "data" => $e->getMessage(),
                ]
            );
        }
    }

    public function getCitiesAjax()
    {
        return AreaCity::orderBy('name')->get()->toArray();
    }

    public function getUnassignedCitiesAjax()
    {
        return AreaCity::orderBy('name')->whereNull('code')->get()->toArray();
    }

    public function getCodesAjax()
    {
        return AreaCity::groupBy('code')->pluck('code')->toArray();
    }
}
