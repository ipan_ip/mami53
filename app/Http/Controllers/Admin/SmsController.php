<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Activity\Sms;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;
use App\Libraries\SMSLibrary;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;

class SmsController extends Controller
{
    protected $layout     = 'admin.layouts.main';

    public function __construct()
    {
        $this->middleware('auth');
        
        $this->middleware(function ($request, $next) {
            if(!Auth::user()->can('access-sms-broadcast')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        View::share('contentHeader', 'Cards Management');
        View::share('user', Auth::user());
    }

    public function index()
    {
        $boxTitle      = 'SMS Blast List';
        $editRoute     = 'admin.sms.edit';
        $deleteRoute   = 'admin.sms.destroy';
        $createAction  = route('admin.sms.create');
        $rowsSms       = Sms::orderBy('id','desc')->get();

        $viewData      = compact(['createAction','editRoute','deleteRoute','boxTitle','rowsSms']);

        ActivityLog::LogIndex(Action::SMS_BROADCAST);

        return View::make('admin.contents.call.sms.index', $viewData);
    }

    public function create()
    {
        $boxTitle       = 'Compose New Message';
        $cities         = Sms::getAvailableCity();
        $citiesSelected = [];
        $rowSms         = new Sms;
        $actionMethod   = 'POST';
        $actionUrl      = route('admin.sms.store');


        $viewData   = compact(['boxTitle','cities','citiesSelected','rowSms','actionMethod','actionUrl']);

        return View::make('admin.contents.call.sms.create', $viewData);
    }

    public function store()
    {
        $sms            = new Sms;
        $sms->message   = Input::get('message','message empty');
        $sms->city      = static::jsonizeCity(Input::get('city',[]));
        $sms->save();

        ActivityLog::LogCreate(Action::SMS_BROADCAST, $sms->id);

        return redirect()->route('admin.sms.index')->with('message','Message has been composed successfully');
    }

    public function edit($messageId)
    {
        $boxTitle       = 'Edit Message';
        $rowSms         = Sms::findOrFail($messageId);
        $cities         = Sms::getAvailableCity();
        $citiesSelected = json_decode($rowSms->city);
        $actionMethod   = 'PUT';
        $actionUrl      = route('admin.sms.update',[$messageId]);

        $viewData   = compact(['boxTitle','cities','citiesSelected','rowSms','actionMethod','actionUrl']);

        return View::make('admin.contents.call.sms.form', $viewData);
    }

    public function update(Request $request,$messageId)
    {
        $sms            = Sms::findOrFail($messageId);
        $sms->message   = Input::get('message','message empty');
        $sms->city      = static::jsonizeCity($request->get('city',[]));
        $sms->save();

        ActivityLog::LogUpdate(Action::SMS_BROADCAST, $messageId);

        return redirect()->route('admin.sms.index')->with('message','Message has been edited successfully');
    }

    public function getConfirm($messageId)
    {
        $message        = Sms::find($messageId);
        $rowsDesigner   = $message->getDesigners();
        $phoneNumbers   = $message->getPhoneNumbers();

        $boxTitle       = $message->message;
        $numberCount    = sizeof($phoneNumbers);
        $sendAction     = url('admin/sms/send/' . $messageId);
        $sendMethod     = 'POST';

        $viewData      = compact(['message','rowsDesigner','boxTitle','numberCount','sendAction','sendMethod']);

        return View::make('admin.contents.call.sms.confirm', $viewData);
    }

    public function sendSms($messageId)
    {
        $message        = Sms::find($messageId);
        $phoneNumbers   = $message->getPhoneNumbers();

        // $phoneNumbers   = ['085708049545'];
        foreach($phoneNumbers as $key => $phoneNumber)
        {
            SMSLibrary::send($phoneNumber, $message->message);
        }
        return redirect()->route('admin.sms.index');

    }

    private static function jsonizeCity($cityArray)
    {
        $cities     = NULL;

        if($cityArray)
        {
            $cities     = (array) $cityArray;
            $cities     = array_filter($cities, function($city){
                return $city != "";
            });
            $cities     = json_encode(array_values($cities));
        }

        return $cities;
    }
}
