<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use View;
use Auth;
use DB;
use App\Entities\NotificationSurvey\NotificationSurvey;
use App\Entities\NotificationSurvey\NotificationSurveyRecipient;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;
use Validator;
use App\User;
use App\Http\Helpers\Shortcode;
use App\Libraries\SMSLibrary;
use Notification;
use App\Notifications\NotificationSurveySender;

class NotificationSurveyController extends Controller
{

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if(!Auth::user()->can('access-notification-survey')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        View::share('contentHeader', 'Notification Survey Management');
        View::share('user', Auth::user());
    }

    public function index(Request $request)
    {
        $notificationSurveys = null;

        if($request->filled('q') && $request->get('q') != '') {
            $notificationSurveys = NotificationSurvey::where('name', 'like', '%' . $request->get('q') . '%')->paginate(20);
        } else {
            $notificationSurveys = NotificationSurvey::paginate(20);
        }

        $viewData = [];
        $viewData['notificationSurveys'] = $notificationSurveys;
        $viewData['boxTitle'] = 'Notification Survey';

        ActivityLog::LogIndex(Action::NOTIFICATION_SURVEY);

        return view('admin.contents.notification-survey.index', $viewData);
    }

    public function create(Request $request)
    {
        $viewData = [];
        $viewData['boxTitle'] = 'Tambah Notification Survey';

        return view('admin.contents.notification-survey.create', $viewData);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
                        'name'=>'required|max:150',
                        'content_sms'=>'max:250',
                        'content_app'=>'max:250',
                        'scheme_app'=>'max:250',
                    ]);

        if($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors())->withInput();
        }

        $notificationSurvey = new NotificationSurvey;
        $notificationSurvey->name = $request->name;
        $notificationSurvey->content_sms = $request->content_sms;
        $notificationSurvey->content_app = $request->content_app;
        $notificationSurvey->scheme_app = $request->scheme_app;
        $notificationSurvey->save();

        ActivityLog::LogCreate(Action::NOTIFICATION_SURVEY, $notificationSurvey->id);

        return redirect()->route('admin.notification-survey.index')->with('message', 'Notification Survey berhasil dibuat');
    }

    public function edit(Request $request, $id)
    {
        $notificationSurvey = NotificationSurvey::find($id);

        $viewData = [];
        $viewData['boxTitle'] = 'Edit Notification Survey';
        $viewData['notificationSurvey'] = $notificationSurvey;

        return view('admin.contents.notification-survey.edit', $viewData);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
                        'name'=>'required|max:150',
                        'content_sms'=>'max:250',
                        'content_app'=>'max:250',
                        'scheme_app'=>'max:250',
                    ]);

        if($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors())->withInput();
        }

        $notificationSurvey = NotificationSurvey::find($id);

        $notificationSurvey->name = $request->name;
        $notificationSurvey->content_sms = $request->content_sms;
        $notificationSurvey->content_app = $request->content_app;
        $notificationSurvey->scheme_app = $request->scheme_app;
        $notificationSurvey->save();

        ActivityLog::LogUpdate(Action::NOTIFICATION_SURVEY, $id);

        return redirect()->route('admin.notification-survey.index')->with('message', 'Notification Survey berhasil dirubah');
    }

    public function runNotifier(Request $request, $id)
    {
        $notificationSurvey = NotificationSurvey::find($id);

        if(!$notificationSurvey) {
            return redirect()->back()->with('error_message', 'Data tidak ditemukan');
        }

        $notificationSurvey->state = NotificationSurvey::STATE_RUNNING;
        $notificationSurvey->save();

        return redirect()->back()->with('message', 'Proses notifikasi sedang dijalankan');
    }

    public function pauseNotifier(Request $request, $id)
    {
        $notificationSurvey = NotificationSurvey::find($id);

        if(!$notificationSurvey) {
            return redirect()->back()->with('error_message', 'Data tidak ditemukan');
        }

        $notificationSurvey->state = NotificationSurvey::STATE_PAUSED;
        $notificationSurvey->save();

        return redirect()->back()->with('message', 'Proses notifikasi ditunda');
    }

    public function stopNotifier(Request $request, $id)
    {
        $notificationSurvey = NotificationSurvey::find($id);

        if(!$notificationSurvey) {
            return redirect()->back()->with('error_message', 'Data tidak ditemukan');
        }

        $notificationSurvey->state = NotificationSurvey::STATE_STOPPED;
        $notificationSurvey->save();

        return redirect()->back()->with('message', 'Proses notifikasi dihentikan');
    }

    public function testNotification(Request $request, $id)
    {
        $notificationSurvey = NotificationSurvey::find($id);

        if(!$notificationSurvey) {
            return redirect()->back()->with('error_message', 'Data tidak ditemukan');
        }

        $viewData = [];
        $viewData['boxTitle'] = 'Kirim Test Notifikasi';
        $viewData['notificationSurvey'] = $notificationSurvey;

        return view('admin.contents.notification-survey.testnotif', $viewData);
    }

    public function testNotificationSubmit(Request $request, $id)
    {
        $notificationSurvey = NotificationSurvey::find($id);

        if(!$notificationSurvey) {
            return redirect()->back()->with('error_message', 'Data tidak ditemukan');
        }

        $validator = Validator::make($request->all(),
                        [
                            'user_id'=>'required'
                        ]);

        if($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors())->withInput();
        }

        $user = User::find($request->user_id);

        if (!$user) {
            return redirect()->back()->with('error_message', 'User tidak ditemukan')->withInput();
        }

        Notification::send($user, new NotificationSurveySender($notificationSurvey));

        return redirect()->back()->withInput()->with('message', 'Test notifikasi sudah dikirim. Silakan periksa device Anda');
    }

    public function testSMSSubmit(Request $request, $id)
    {
        $voucherProgram = VoucherProgram::find($id);

        if(!$voucherProgram) {
            return redirect()->back()->with('error_message', 'Data tidak ditemukan');
        }

        $validator = Validator::make($request->all(),
                        [
                            'destination_phone'=>'required'
                        ],
                        [
                            'destination_phone.required'=>'Nomor Telepon Tujuan harus diisi',
                        ]);

        if($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors())->withInput();
        }

        $smsTemplate = $voucherProgram->sms_template;
        $smsTemplate = str_replace(['[VOUCHERMAMIKOS]'], 
                            '[voucher]', $smsTemplate);

        $shortcodeFactory = new Shortcode;
        $smsTemplate = $shortcodeFactory->compile($smsTemplate);

        $smsStatus = SMSLibrary::send(SMSLibrary::phoneNumberCleaning($request->destination_phone), $smsTemplate);

        if($smsStatus == 'Message Sent') {
            return redirect()->back()->withInput()->with('message', 'Test SMS sudah dikirim. Silakan periksa kotak masuk Anda');
        } else {
            return redirect()->back()->withInput()->with('error_message', 'Test SMS gagal dikirim, silakan cek ulang nomor tujuan');
        }

        
    }

    public function recipient(Request $request, $notificationSurveyId)
    {
        $notificationSurvey = NotificationSurvey::find($notificationSurveyId);

        $query = NotificationSurveyRecipient::with('user')
            ->where('notification_survey_id', $notificationSurveyId);
        if($request->filled('q') && $request->get('q') != '') {
            $query = $query->where('user_id', $request->q);
        }

        $recipients = $query->paginate(20);

        $viewData = [];
        $viewData['notificationSurvey'] = $notificationSurvey;
        $viewData['recipients'] = $recipients;
        $viewData['boxTitle'] = 'Notification Survey Recipient';

        return view('admin.contents.notification-survey.recipients', $viewData);
    }

    public function recipientCreate(Request $request, $notificationSurveyId)
    {
        $notificationSurvey = NotificationSurvey::find($notificationSurveyId);

        $viewData = [];
        $viewData['notificationSurvey'] = $notificationSurvey;
        $viewData['boxTitle'] = 'Notification Survey Recipient';

        return view('admin.contents.notification-survey.recipient-create', $viewData);
    }

    public function recipientStore(Request $request, $notificationSurveyId)
    {
        $validator = Validator::make($request->all(),
                            [
                                'user_ids'=>'required',
                            ]);

        if($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors())->withInput();
        }

        $userIds = explode(',', $request->user_ids);

        $recipientData = [];
        foreach ($userIds as $userId) {

            if ($userId == '') continue;

            $recipientData[] = [
                'user_id' => (int)$userId,
                'notification_survey_id' => $notificationSurveyId,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ];
        }

        NotificationSurveyRecipient::insert($recipientData);

        return redirect()->route('admin.notification-survey.recipient.index', $notificationSurveyId)->with('message', 'Penerima survey berhasil ditambahkan');
    }

    public function recipientEdit(Request $request, $notificationSurveyId, $recipientId)
    {
        $notificationSurvey = NotificationSurvey::find($notificationSurveyId);

        $recipient = NotificationSurveyRecipient::where("notification_survey_id", $notificationSurveyId)
                                    ->where('id', $recipientId)
                                    ->first();

        $viewData = [];
        $viewData['notificationSurvey'] = $notificationSurvey;
        $viewData['recipient'] = $recipient;
        $viewData['boxTitle'] = 'Notification Survey Recipient';

        return view('admin.contents.notification-survey.recipient-edit', $viewData);
    }

    public function recipientUpdate(Request $request, $notificationSurveyId, $recipientId)
    {
        $validator = Validator::make($request->all(),
                            [
                                'user_id'=>'required',
                            ]);

        if($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors())->withInput();
        }

        $recipient = NotificationSurveyRecipient::where("notification_survey_id", $notificationSurveyId)
                                    ->where('id', $recipientId)
                                    ->first();

        $recipient->user_id = $request->user_id;
        $recipient->save();

        ActivityLog::LogUpdate(Action::NOTIFICATION_SURVEY, $recipientId);

        return redirect()->route('admin.notification-survey.recipient.index', $notificationSurveyId)->with('message', 'Penerima survey berhasil dirubah');
    }


    public function recipientSendNotif(Request $request, $voucherProgramId, $recipientId)
    {
        $voucherProgram = VoucherProgram::find($voucherProgramId);

        $recipient = VoucherRecipient::where('voucher_program_id', $voucherProgramId)
                                    ->where('id', $recipientId)
                                    ->first();

        if(!$recipient) {
            return redirect()->back()->with('error_message', 'Data tidak ditemukan');
        }

        if(is_null($recipient->email) || $recipient->email == '') {
            return redirect()->back()->with('error_message', 'Email kosong');
        }

        $emailTemplate = $voucherProgram->email_template;
        $emailTemplate = str_replace(['[VOUCHERMAMIKOS]'], 
                            '[voucher code="' . $recipient->voucher_code . '"]', $emailTemplate);

        $shortcodeFactory = new Shortcode;
        $emailTemplate = $shortcodeFactory->compile($emailTemplate);

        $destination = $recipient->email;;

        \Mail::send('emails.voucher-reward', ['template' => $emailTemplate], function($message) use ($destination, $voucherProgram)
        {
            $message->to($destination)->subject($voucherProgram->email_subject != '' ? $voucherProgram->email_subject : 'Voucher dari MamiKos!');
        });

        return redirect()->back()->with('message', 'Email notifikasi berhasil dikirim');
    }
}