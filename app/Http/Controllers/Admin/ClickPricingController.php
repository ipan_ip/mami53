<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;
use Auth;
use Validator;
use Illuminate\Validation\Rule;
use Cache;

use App\Entities\Premium\ClickPricing;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;

/**
* 
*/
class ClickPricingController extends Controller
{

    protected $validationMessages = [
        'area.required' => 'Area harus diisi',
        'area.unique' => 'Area sudah digunakan sebelumnya',
        'lower_limit_price.required' => 'Batas Bawah harus diisi',
        'lower_limit_price.numeric' => 'Batas Bawah harus berupa angka',
        'higher_limit_price.required' => 'Batas Atas harus diisi',
        'higher_limit_price.numeric' => 'Batas Atas harus berupa angka',
        'property_median_price.required' => 'Batas Tengah Harga Bulanan harus diisi',
        'property_median_price.numeric' => 'Batas Tengah Harga Bulanan harus berupa angka',
        'low_price.required' => 'Harga Bawah harus diisi',
        'low_price.numeric' => 'Harga Bawah harus berupa angka',
        'high_price.required' => 'Harga Atas harus diisi',
        'high_price.numeric' => 'Harga Atas harus berupa angka'
    ];

    protected $for = ["click", "chat"];
    
    function __construct()
    {
        $this->middleware(function ($request, $next) {
            if(!Auth::user()->can('access-click-pricing')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        View::share('contentHeader', 'Click Pricing Management');
        View::share('user', Auth::user());
    }

    public function index(Request $request)
    {
        $query = ClickPricing::query();

        if($request->filled('q') && $request->get('q') != '') {
            $query = $query->where('area_city', 'like', '%' . $request->get('q') . '%');
        }

        $clickPricings = $query->orderBy('created_at', 'desc')
                                ->paginate(20);

        $viewData = [];
        $viewData['clickPricings'] = $clickPricings;
        $viewData['boxTitle'] = 'Click Pricing';

        ActivityLog::LogIndex(Action::CLICK_PRICING);

        return view('admin.contents.click-pricing.index', $viewData);
    }

    public function create(Request $request)
    {
        $viewData = [];
        $viewData['boxTitle'] = 'Add Click Pricing';
        $viewData['for'] = $this->for;

        return view('admin.contents.click-pricing.create', $viewData);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), 
            [
                'area' => [
                    'required',
                    Rule::unique('premium_click_pricing', 'area_city')->whereNull('deleted_at')
                ],
                'lower_limit_price' => 'required|numeric',
                'higher_limit_price' => 'required|numeric',
                'property_median_price' => 'required|numeric',
                'low_price' => 'required|numeric',
                'high_price' => 'required|numeric'
            ], $this->validationMessages);

        $validator->after(function ($validator) use ($request) {
            if($request->low_price < $request->lower_limit_price) {
                $validator->errors()->add('low_price', 'Harga Bawah tidak boleh di bawah Batas Bawah');
            } elseif($request->low_price > $request->higher_limit_price) {
                $validator->errors()->add('low_price', 'Harga Bawah tidak boleh di atas Batas Atas');
            }

            if($request->high_price < $request->lower_limit_price) {
                $validator->errors()->add('high_price', 'Harga Atas tidak boleh di bawah Batas Bawah');
            } elseif($request->high_price > $request->higher_limit_price) {
                $validator->errors()->add('high_price', 'Harga Atas tidak boleh di atas Batas Atas');
            }
        });

        if($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator->errors());
        }

        $pricing = new ClickPricing;
        $pricing->area_city = $request->input('area');
        $pricing->lower_limit_price = $request->input('lower_limit_price');
        $pricing->higher_limit_price = $request->input('higher_limit_price');
        $pricing->property_median_price = $request->input('property_median_price');
        $pricing->low_price = $request->input('low_price');
        $pricing->high_price = $request->input('high_price');
        $pricing->for = $request->input('for');
        $pricing->save();

        ActivityLog::LogCreate(Action::CLICK_PRICING, $pricing->id);

        $cacheKey = str_replace(' ', '', strtolower($pricing->area_city));
        if ($pricing->for == 'chat') $cacheName = 'chatpricing:';
        else $cacheName = 'clickpricing:';

        Cache::put($cacheName . $cacheKey, $pricing, 60 * 24);

        return redirect(route('admin.click-pricing.index'))->with('message', 'Pricing berhasil ditambahkan');
    }

    public function edit(Request $request, $id)
    {
        $clickPricing = ClickPricing::find($id);

        if(!$clickPricing) {
            return redirect()->back()->with('error_message', 'Data tidak ditemukan');
        }

        $viewData = [];
        $viewData['clickPricing'] = $clickPricing;
        $viewData['boxTitle'] = 'Edit Click Pricing';
        $viewData['for'] = $this->for;

        return view('admin.contents.click-pricing.edit', $viewData);
    }

    public function update(Request $request, $id)
    {
        $clickPricing = ClickPricing::find($id);

        if ($clickPricing->for != $request->input('for')) {
            if ($clickPricing->for == 'chat') $cacheNameForget = 'chatpricing:';
            else $cacheNameForget = 'clickpricing:';
            Cache::forget($cacheNameForget . str_replace(' ', '', strtolower($clickPricing->area_city)));
        }

        if(!$clickPricing) {
            return redirect()->back()->with('error_message', 'Data tidak ditemukan');
        }

        $validator = Validator::make($request->all(),
            [
                'area' => [
                    'required',
                    Rule::unique('premium_click_pricing', 'area_city')->ignore($id, 'id')->whereNull('deleted_at')
                ],
                'lower_limit_price' => 'required|numeric',
                'higher_limit_price' => 'required|numeric',
                'property_median_price' => 'required|numeric',
                'low_price' => 'required|numeric',
                'high_price' => 'required|numeric'
            ], $this->validationMessages);

        $validator->after(function ($validator) use ($request) {
            if($request->low_price < $request->lower_limit_price) {
                $validator->errors()->add('low_price', 'Harga Bawah tidak boleh di bawah Batas Bawah');
            } elseif($request->low_price > $request->higher_limit_price) {
                $validator->errors()->add('low_price', 'Harga Bawah tidak boleh di atas Batas Atas');
            }

            if($request->high_price < $request->lower_limit_price) {
                $validator->errors()->add('high_price', 'Harga Atas tidak boleh di bawah Batas Bawah');
            } elseif($request->high_price > $request->higher_limit_price) {
                $validator->errors()->add('high_price', 'Harga Atas tidak boleh di atas Batas Atas');
            }
        });

        if($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator->errors());
        }

        $clickPricingOriginal = $clickPricing->getOriginal();

        $clickPricing->area_city = $request->input('area');
        $clickPricing->lower_limit_price = $request->input('lower_limit_price');
        $clickPricing->higher_limit_price = $request->input('higher_limit_price');
        $clickPricing->property_median_price = $request->input('property_median_price');
        $clickPricing->low_price = $request->input('low_price');
        $clickPricing->high_price = $request->input('high_price');
        $clickPricing->for = $request->input('for');
        $clickPricing->save();

        if ($clickPricing->for == 'chat') $cacheName = 'chatpricing:';
        else $cacheName = 'clickpricing:';

        if($clickPricingOriginal['area_city'] != $clickPricing->area_city) {
            Cache::forget($cacheName . str_replace(' ', '', strtolower($clickPricingOriginal['area_city'])));
        }

        Cache::put($cacheName . str_replace(' ', '', strtolower($clickPricing->area_city)), $clickPricing, 60 * 24);

        ActivityLog::LogUpdate(Action::CLICK_PRICING, $id);
        
        return redirect(route('admin.click-pricing.index'))->with('message', 'Pricing berhasil dirubah');
    }

    public function destroy(Request $request, $id)
    {
        $clickPricing = ClickPricing::find($id);

        if(!$clickPricing) {
            return redirect()->back()->with('error_message', 'Data tidak ditemukan');
        }

        if ($clickPricing->for == 'chat') $cacheName = 'chatpricing:';
        else $cacheName = 'clickpricing:';

        Cache::forget($cacheName . str_replace(' ', '', strtolower($clickPricing->area_city)));

        $clickPricing->delete();

        ActivityLog::LogDelete(Action::CLICK_PRICING, $id);

        return redirect(route('admin.click-pricing.index'))->with('message', 'Pricing berhasil dihapus');
    }
}