<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Landing\LandingTagging;
use App\Entities\Room\Element\Tagging;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Entities\Landing\Landing;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use App\Entities\Room\Element\Tag;
use Illuminate\Support\Facades\DB;
use App\Entities\Media\Media;
use App\Entities\Media\MediaHandler;
use App\Entities\Activity\ActivityLog;
use Illuminate\Validation\Rule;
use Validator;
use App\Libraries\CSVParser;
use App\Http\Helpers\UtilityHelper;
use App\Entities\Activity\Action;

class LandingController extends Controller
{
    protected $validationMessages = [
        'slug.required' => 'Slug harus diisi',
        'slug.unique' => 'Slug sudah digunakan sebelumnya',
        'heading_1.required' => 'Heading 1 harus diisi',
        'heading_1.unique' => 'Heading 1 sudah digunakan sebelumnya',
    ];

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if(!Auth::user()->can('access-landing-page')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        View::share('contentHeader', 'Stories Management');
        View::share('user', Auth::user());
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $rowsLanding    = Landing::with('parent')/*->where('redirect_id', null)*/;

        if(Input::has('q'))
        {
            $rowsLanding->where('slug','like', '%' . Input::get('q') . '%');
        }

        if (Input::has('sort')) {
            $field = Input::get('sort');
            $direction = Input::has('sort_dir') ? Input::get('sort_dir') : 'asc';

            $rowsLanding = $rowsLanding->orderBy($field, $direction);
        } else {
            $rowsLanding = $rowsLanding->orderBy('id', 'desc');
        }
        
        $rowsLanding    = $rowsLanding->orderByRaw(\DB::raw('IFNULL(redirect_id, 0) asc'))
                            ->paginate(10);

        $sortUrl = [
            'view_count' => UtilityHelper::modifyQueryStringForSorting($request->fullUrl(), 'view_count', 'array')
        ];

        parse_str(parse_url($request->fullUrl(), PHP_URL_QUERY), $currentQueryString);
        
        $viewData = array(
            'boxTitle'        => 'List Landing',
            'deleteAction'    => "admin.landing.destroy",
            'rowsLanding'     => $rowsLanding,
            'sortUrl'         => $sortUrl,
            'currentQueryString' => $currentQueryString
        );

        ActivityLog::LogIndex(Action::LANDING_PAGE);

        return View::make('admin.contents.landing.index', $viewData);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $rowLanding = new Landing;

        $rowLanding->slug           = Input::old('slug');
        $rowLanding->type           = Input::old('type','area');
        $rowLanding->parent_id      = Input::old('parent_id');
        $rowLanding->photo_1_id     = Input::old('photo_1_id');
        $rowLanding->photo_2_id     = Input::old('photo_2_id');
        $rowLanding->heading_1      = Input::old('heading_1');
        $rowLanding->heading_2      = Input::old('heading_2');
        $rowLanding->latitude       = Input::old('latitude');
        $rowLanding->longitude      = Input::old('longitude');
        $rowLanding->distance       = Input::old('distance');
        $rowLanding->price_min      = Input::old('price_min');
        $rowLanding->price_max      = Input::old('price_max');
        $rowLanding->keyword        = Input::old('keyword');
        $rowLanding->tag_ids        = Input::old('tag_ids');
        $rowLanding->gender         = Input::old('gender');
        $rowLanding->rent_type      = Input::old('rent_type');
        
        $rowLanding->area_city      = Input::old('area_city');
        $rowLanding->sub_district   = Input::old('sub_district');

        $rowLanding->description_1  = Input::old('description_1');
        $rowLanding->description_2  = Input::old('description_2');
        $rowLanding->description_3  = Input::old('description_3');

        $rowLanding->photo_1        = 'http://lorempixel.com/400/200/city/1/Mamikos';
        $rowLanding->photo_2        = 'http://lorempixel.com/400/200/city/2/Mamikos';

        $rowLanding->coordinate_1   = Input::old('coordinate_1');
        $rowLanding->coordinate_2   = Input::old('coordinate_2');

        if (!is_null($rowLanding->coordinate_1)) {
            $coordinate1Splited = explode(',', $rowLanding->coordinate_1);

            $rowLanding->latitude_1  = $coordinate1Splited[0];
            $rowLanding->longitude_1 = $coordinate1Splited[1];
        }


        if (!is_null($rowLanding->coordinate_1)) {
            $coordinate2Splited = explode(',', $rowLanding->coordinate_2);

            $rowLanding->latitude_2  = $coordinate2Splited[0];
            $rowLanding->longitude_2 = $coordinate2Splited[1];
        }
        

        $rowLanding->use_amp = Input::old('use_amp');

        $rowsLanding = Landing::select(DB::raw('id,concat(keyword," | ",slug) as name'))->get();
        $rowsLanding = mySelectPreprocessing(
            'single',
            'selected',
            'key-value',
            $rowsLanding,
            $rowLanding->parent_id
        );

        $rowsTag  = Tag::get();
        $rowsTag  = mySelectPreprocessing(
            'multi',
            'selected',
            'key-value',
            $rowsTag,
            $rowLanding->tag_ids
        );

        $rowsTagging   = Tagging::all();

        $viewData   = [
            'boxTitle'          => 'New Landing Page',
            'rowLanding'        => $rowLanding,
            'rowsTag'           => $rowsTag,
            'rowsTagging'       => $rowsTagging,
            'rowsOldTagging'    => [],
            'rowsLanding'       => $rowsLanding,
            'formMethod'        => 'POST',
            'formUrl'           => route('admin.landing.store'),
            'formCancel'        => route('admin.landing.index')
        ];

        return View::make('admin.contents.landing.form', $viewData);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(), 
            [
                'slug' => [
                    'required',
                    Rule::unique('landing', 'slug')->whereNull('deleted_at')
                ],
                'heading_1' => [
                    'required',
                    Rule::unique('landing', 'heading_1')->whereNull('deleted_at')
                ]
            ],
            $this->validationMessages
        );

        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator->errors());
        }

        $rowLanding                 = new Landing;

        $rowLanding->slug           = $request->get('slug');
        $rowLanding->type           = $request->get('type');
        $rowLanding->parent_id      = $request->get('parent_id') ? $request->get('parent_id') :null;
        $rowLanding->heading_1      = $request->get('heading_1');
        $rowLanding->heading_2      = $request->get('heading_2');
        
        if ($request->filled('coordinate_1') AND $request->filled('coordinate_2')) {
            
            $latLong1                   = explode(',', $request->get('coordinate_1'));
            $latLong2                   = explode(',', $request->get('coordinate_2'));

            $rowLanding->latitude_1     = min($latLong1[0],$latLong2[0]);
            $rowLanding->longitude_1    = min($latLong1[1],$latLong2[1]);

            $rowLanding->latitude_2     = max($latLong1[0],$latLong2[0]);
            $rowLanding->longitude_2    = max($latLong1[1],$latLong2[1]);

        }

        $rowLanding->price_min      = $request->get('price_min');
        $rowLanding->price_max      = $request->get('price_max');
        $rowLanding->rent_type      = $request->get('rent_type');

        $rowLanding->photo_1_id     = $request->get('photo_1_id') ? $request->get('photo_1_id') : 61339;
        $rowLanding->photo_2_id     = $request->get('photo_2_id') ? $request->get('photo_2_id') : 61340;

        $rowLanding->keyword        = $request->get('keyword');

        $rowLanding->description_1  = $request->get('description_1');
        $rowLanding->description_2  = $request->get('description_2');
        $rowLanding->description_3  = $request->get('description_3');
        $rowLanding->user_id        = Auth::user()->id;
        // gender should be int array or null in DB
        $rowLanding->gender         = Landing::convertGenderRequestToGender($request->get('gender'));

        $rowLanding->use_amp        = $request->filled('use_amp') ? $request->get('use_amp') : 0;
        $rowLanding->dummy_counter  = $request->get('dummy_counter') != '' ? $request->get('dummy_counter') : null;

        $banner = null;
        if ($request->file('image')) {
            $banner = Media::postMedia($request->file('image'), 'url', null, null, null, 'style_photo', true);
            $rowLanding->image = $banner['id'];
        }

        $rowLanding->cleanNoFollowLinks();
        $rowLanding->save();
        $rowLanding->removeWatermark();

        $taggingIds = $request->input('tagging_ids');
        if ($taggingIds != null) {
            // process check tagging
            $temp   = [];
            foreach ($taggingIds as $tagging) {
                $checkTagging      = Tagging::where('name', $tagging)->first();
                $newLandingTagging = new LandingTagging();
                $newLandingTagging->landing_id = $rowLanding->id;

                // create new tagging
                if ($checkTagging == null) {
                    $newTagging = new Tagging();
                    $newTagging->name = $tagging;
                    $newTagging->save();

                    $newLandingTagging->landing_category_id = $newTagging->id;
                    $newLandingTagging->save();
                } else {
                    $checkLandingTagging    = LandingTagging::where('landing_id', $rowLanding->id)->where('landing_category_id', $checkTagging->id)->first();

                    if ($checkLandingTagging == null) {
                        $newLandingTagging->landing_category_id = $checkTagging->id;
                        $newLandingTagging->save();
                    }
                }


                $temp[] = $newLandingTagging;
            }

        }

        $tagIds  = $request->get('tag_ids');
        $tagIds  = (array) $tagIds;

        $rowLanding->tags()->sync($tagIds);

        ActivityLog::LogCreate(Action::LANDING_PAGE, $rowLanding->id);

        return  redirect()->route('admin.landing.index')
            ->with('message', 'Landing Page successfully created');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!Auth::user()->can('access-landing-page-edit')) return redirect ('/admin');

        $rowLanding = Landing::findOrFail($id);

        $rowLanding->coordinate_1   = (float) $rowLanding->latitude_1 . ',' . (float) $rowLanding->longitude_1;
        $rowLanding->coordinate_2   = (float) $rowLanding->latitude_2 . ',' . (float) $rowLanding->longitude_2;

        if ($rowLanding->photo_1_id != null) {
           $photo_1                 = Media::find($rowLanding->photo_1_id)->getMediaUrl();
           $rowLanding->photo_1     = $photo_1['medium'];
        }

        if ($rowLanding->photo_2_id != null) {
            $photo_2                = Media::find($rowLanding->photo_2_id)->getMediaUrl();
            $rowLanding->photo_2    = $photo_2['medium'];
        }

        $rowLanding->tag_ids        = array_pluck($rowLanding->tags()->get(['tag.id'])->toArray(),'id');
        $rowLanding->gender         = Landing::renderGenderForDataForm($rowLanding->gender);

        $rowsLanding                = Landing::select(DB::raw('id,concat(keyword," | ",slug) as name'))->get();
        $rowsLanding                = mySelectPreprocessing(
            'single',
            'selected',
            'key-value',
            $rowsLanding,
            $rowLanding->parent_id
        );

        $rowsTag = Tag::get();
        $rowsTag = mySelectPreprocessing(
            'multi',
            'selected',
            'key-value',
            $rowsTag,
            $rowLanding->tag_ids
        );

        if ($rowLanding->rent_type == null) {
            $rowLanding->rent_type = 2;
        }

        // Check Tagging
        $rowsTagging   = Tagging::all();
        $oldTagging    = LandingTagging::where('landing_id', $id)->whereNull("deleted_at")->get();
        $oldTaggingId  = [];
        foreach ($oldTagging as $tagging) {
            $oldTaggingId[] = $tagging->landing_category_id;
        }

        $viewData   = [
            'boxTitle'          => 'Edit Landing Page',
            'rowLanding'        => $rowLanding,
            'rowsTag'           => $rowsTag,
            'rowsTagging'       => $rowsTagging,
            'rowsOldTagging'    => $oldTaggingId,
            'rowsLanding'       => $rowsLanding,
            'formMethod'        => 'PUT',
            'formUrl'           => route('admin.landing.update', $id),
            'formCancel'        => route('admin.landing.index')
        ];

        return View::make('admin.contents.landing.form', $viewData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'slug'=>[
                Rule::unique('landing')->ignore($id, 'id')->whereNull('deleted_at')
            ],
            'heading_1'=>[
                Rule::unique('landing')->ignore($id, 'id')->whereNull('deleted_at')
            ]
        ]);

        $rowLanding                 = Landing::find($id);

        $rowLanding->slug           = $request->get('slug');
        $rowLanding->type           = $request->get('type');
        $rowLanding->parent_id      = $request->get('parent_id') ? $request->get('parent_id') :null;
        $rowLanding->heading_1      = $request->get('heading_1');
        $rowLanding->heading_2      = $request->get('heading_2');

        $latLong1                   = explode(',', $request->get('coordinate_1'));
        $latLong2                   = explode(',', $request->get('coordinate_2'));

        $rowLanding->latitude_1     = min($latLong1[0],$latLong2[0]);
        $rowLanding->longitude_1    = min($latLong1[1],$latLong2[1]);

        $rowLanding->latitude_2     = max($latLong1[0],$latLong2[0]);
        $rowLanding->longitude_2    = max($latLong1[1],$latLong2[1]);

        $rowLanding->photo_1_id     = $request->get('photo_1_id') ? $request->get('photo_1_id') : 61339;
        $rowLanding->photo_2_id     = $request->get('photo_2_id') ? $request->get('photo_2_id') : 61340;

        $rowLanding->price_min      = $request->get('price_min');
        $rowLanding->price_max      = $request->get('price_max');
        $rowLanding->rent_type      = $request->get('rent_type');

        $rowLanding->keyword        = $request->get('keyword');

        $rowLanding->description_1  = $request->get('description_1');
        $rowLanding->description_2  = $request->get('description_2');
        $rowLanding->description_3  = $request->get('description_3');
        
        // gender should be int array or null in DB
        $rowLanding->gender         = Landing::convertGenderRequestToGender($request->get('gender'));

        $taggingIds = $request->input('tagging_ids');

        if ($taggingIds != null) {
            LandingTagging::removeLandingTagging($id, $taggingIds);

            // process check tagging
            $temp   = [];
            foreach ($taggingIds as $tagging) {
                $checkTagging      = Tagging::where('name', $tagging)->first();
                $newLandingTagging = new LandingTagging();
                $newLandingTagging->landing_id = $id;

                // create new tagging
                if ($checkTagging == null) {
                    $newTagging = new Tagging();
                    $newTagging->name = $tagging;
                    $newTagging->save();

                    $newLandingTagging->landing_category_id = $newTagging->id;
                    $newLandingTagging->save();
                } else {
                    $checkLandingTagging    = LandingTagging::where('landing_id', $id)->where('landing_category_id', $checkTagging->id)->first();

                    if ($checkLandingTagging == null) {
                        $newLandingTagging->landing_category_id = $checkTagging->id;
                        $newLandingTagging->save();
                    }
                }


                $temp[] = $newLandingTagging;
            }

        } else {
            LandingTagging::removeAllLandingTagging($id);
        }

        $rowLanding->use_amp        = $request->filled('use_amp') ? $request->get('use_amp') : 0;
        $rowLanding->dummy_counter  = $request->get('dummy_counter') != '' ? $request->get('dummy_counter') : null;

        $banner = null;
        if ($request->file('image')) {
            $banner = Media::postMedia($request->file('image'), 'url', null, null, null, 'style_photo', true);
            $rowLanding->image = $banner['id'];
        }
        
        $rowLanding->cleanNoFollowLinks();
        $rowLanding->save();
        $rowLanding->removeWatermark();

        $tagIds  = $request->get('tag_ids');
        $tagIds  = (array) $tagIds;
        $rowLanding->tags()->sync($tagIds);
        
        ActivityLog::LogUpdate(Action::LANDING_PAGE, $id);

        return  redirect()->route('admin.landing.index')
            ->with('message', 'Landing Page successfully updated');
    }

    public function show(Request $request, $id)
    {
        return redirect('admin/landing/'.$id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!Auth::user()->can('access-landing-page-delete')) return redirect ('/admin');

        Landing::find($id)->delete();

        ActivityLog::LogDelete(Action::LANDING_PAGE, $id);

        return  redirect()->route('admin.landing.index')
            ->with('message', 'Landing Page successfully deleted');
    }


    public function parseIndex(Request $request)
    {
        $viewData   = [
            'boxTitle'          => 'Parse Landing CSV',
        ];

        return view('admin.contents.landing.parser', $viewData);
    }


    public function parseCSV(Request $request)
    {
        $this->validate($request, [
            'file_csv'=>'required|mimes:csv,txt'
        ]);

        $availableTags = Tag::get();
        $tagMapping = $availableTags->pluck('id', 'name')->toArray();
        $tagMapping = array_change_key_case($tagMapping, CASE_LOWER);

        // $tagMapping = [
        //     'Ac'=>13,
        //     'Parkir Mobil'=>22,
        //     'Kamar Mandi Dalam'=>1,
        //     'Karyawan'=>82,
        //     'Karyawati'=>82
        // ];

        $rentTypeMapping = [
            'kost harian' => 0,
            'kost mingguan' => 1,
            'kost bulanan' => 2,
            'kost tahunan' => 3
        ];

        $csv = $request->file_csv->path();

        $csvArray = CSVParser::csv_parse($csv, ['eol'=>"\r\n"]);

        // dd($csvArray);

        $failedRow = [];
        foreach($csvArray as $key=>$row) {
            $slug = str_replace(' ', '-', $row['url_slug']);

            $existingLanding = Landing::where('slug', $slug)
                                    ->orWhere('heading_1', $row['heading_1'])
                                    ->get();

            if(count($existingLanding) > 0) {
                $failedRow[] = 'Baris ' . ($key + 1) . ' gagal diinput karena slug atau heading_1 sudah ada sebelumnya';
                continue;
            }

            $landing = new Landing;
            $landing->slug = $slug;
            $landing->type = strtolower($row['type']);
            $landing->heading_1 = $row['heading_1'];
            $landing->heading_2 = $row['heading_2'];

            if(isset($row['description_1'])) {
                $landing->description_1 = $row['description_1'];
            }

            if(isset($row['description_2'])) {
                $landing->description_2 = $row['description_2'];
            }

            if(isset($row['description_3'])) {
                $landing->description_3 = $row['description_3'];
            }
            

            $coordinate_1 = explode(',', $row['koordinat_1']);
            $coordinate_2 = explode(',', $row['koordinat_2']);

            $landing->latitude_1 = trim($coordinate_1[0]);
            $landing->longitude_1 = trim($coordinate_1[1]);
            $landing->latitude_2 = trim($coordinate_2[0]);
            $landing->longitude_2 = trim($coordinate_2[1]);

            $landing->keyword = $row['keywords'];
            $landing->price_min = $row['price_min'] == '' ? 0 : $row['price_min'];
            $landing->price_max = $row['price_max'] == '' ? 10000000 : $row['price_max'];

            $genders = explode('|', $row['gender']);

            if(count($genders) > 0) {
                $genderOption = [];

                if($row['filter_1'] == 'Karyawati') $genderOption[] = '2';

                foreach($genders as $gender) {
                    if(strtolower($gender) == 'wanita') {
                        $genderOption[] = '2';
                    } elseif(strtolower($gender) == 'pria') {
                        $genderOption[] = '1';
                    } elseif(strtolower($gender) == 'campur') {
                        $genderOption[] = '0';
                    }
                }

                $landing->gender = '[' . implode(',', $genderOption) . ']';
            } else {
                $landing->gender = '[0]';
            }

            // $landing->gender = '[' . ($row['filter_1'] == 'Karyawati' || strtolower($row['gender']) == 'wanita' ? '2' : (strtolower($row['gender']) == 'pria' ? '1' :  '0')) .  ']';

            $lowerCasedRentType = strtolower($row['rent_type']);
            $landing->rent_type = isset($rentTypeMapping[$lowerCasedRentType]) ? $rentTypeMapping[$lowerCasedRentType] : '2';
            
            $landing->save();

            $explodedFilters = $row['filter_1'] != '' ? explode('|', $row['filter_1']) : [];
            $tagIds = [];
            if(!empty($explodedFilters)) {
                foreach($explodedFilters as $filter) {
                    $lowerCasedFilter = strtolower($filter);
                    if(isset($tagMapping[$lowerCasedFilter])) {
                        $tagIds[] = $tagMapping[$lowerCasedFilter];
                    }
                }
            }

            $landing->tags()->sync($tagIds);

            // $tagId = isset($tagMapping[$row['filter_1']]) ? $tagMapping[$row['filter_1']] : '';
            // if($tagId != '') {
            //     $landing->tags()->sync([$tagId]);
            // }
        }

        return redirect()->back()
                        ->with(!empty($failedRow) ? 'errors' : 'message', 
                            !empty($failedRow) ? collect($failedRow) : 'Sukses input data');
    }

    public function redirectlanding(Request $request, $landingId)
    {
        if(!Auth::user()->can('access-landing-page-redirect')) return redirect ('/admin');

        $listLanding = Landing::getLandingWithoutRedirection();
        $viewData = array(
            'boxTitle'    => 'Edit Landing Redirect',
            'landingId'   => $landingId,
            'listLanding' => $listLanding
        );

        return view('admin.contents.landing.redirect', $viewData);
    }

    public function redirectlandingpost(Request $request, $landingId)
    {
        $landing = Landing::find($landingId);

        if(is_null($request->get('landing_destination')) || $request->get('landing_destination') == '') {
            $landing->redirect_id = NULL;
            $landing->save();

            return redirect()->route('admin.landing.index')->with('message', 'Redirect Landing sudah dibatalkan');
        }

        $landingDestination = Landing::find($request->get('landing_destination'));

        if(!$landingDestination) {
            return redirect()->back()->with('error_message', 'Landing tujuan tidak ditemukan');
        }

        $landing->redirect_id = $landingDestination->id;
        $landing->save();

        return redirect()->route('admin.landing.index')->with('message', 'Landing sudah di-redirect');
    }
}
