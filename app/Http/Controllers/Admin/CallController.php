<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Activity\Call;
use App\Entities\Activity\CallReply;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use App\Libraries\Sms\Zenziva;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;

class CallController extends Controller
{
    /**
     * CallController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if(!\Auth::user()->can('access-call')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        View::share('contentHeader', 'Calls Management');
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $calls = Call::query()->with('replies');

        if ($request->filled('q')) {
            $keyword  =  $request->get('q');

            $calls->where('designer_id', '!=', '0')
                ->where(function($query) use ($keyword){
                    $query->where('designer_id', '=', $keyword)
                        ->orWhere('phone', 'like', '%' . $keyword . '%')
                        ->orWhere('name', 'like', '%' . $keyword . '%')
                        ->orWhereHas('room', function($q) use ($keyword){
                            $q->where('name', 'like', '%' . $keyword . '%');
                        });
                });
        }

        if ($request->filled('from') && $request->filled('to')
            && !empty($request->input('from')) && !empty($request->input('to'))){
            $from = $request->input('from');
            $to = $request->input('to');

            $now = Carbon::now();

            try {
                $from = Carbon::createFromFormat('d-m-Y H:i:s', $from . ' 00:00:00')->format('Y-m-d H:i:s');
                $to = Carbon::createFromFormat('d-m-Y H:i:s', $to . ' 23:59:59')->format('Y-m-d H:i:s');

                $calls = $calls->whereBetween('created_at', array($from, $to));
            } catch (Exception $e) {
                return Redirect::route('admin.call.index')->with('error_message', 'Aborted: ' . $e->getMessage());
            }
        }

        $calls->where('chat_admin_id', NULL)
              ->orderBy('created_at', 'desc');
        $calls->with('room');

        $calls = $calls->paginate(20);

        $viewData = array(
            'user' => ['name' => 'foo', 'nick_name' => 'bar', 'role' => 'admin'],
            'bar'  => 'qux',
            'calls' => $calls,
            'currentPath' => url('/')
        );

        ActivityLog::LogIndex(Action::CALL);

        return View::make("admin.contents.call.index", $viewData);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getReply($id)
    {
        try {
            $call = Call::with('replies')->findOrFail($id);
            $reply = new CallReply;
            $reply->created_at = Carbon::now();

            $userPhone = $call->phone;

            if (empty($userPhone)) {
                throw new Exception("User Phone Number is empty in this call record.");
            }
        } catch (Exception $e) {
            return Redirect::route('admin.call.index')->with('error_message', $e->getMessage());
        }

        $viewData = array(
            'user' => ['name' => 'foo', 'nick_name' => 'bar', 'role' => 'admin'],
            'boxTitle' => 'Reply Message',
            'call'    => $call,
            'reply' => $reply,
        );

        // $this->layout->content = View::make("admin.contents.call.index", $viewData);
        return View::make("admin.contents.call.reply", $viewData);
    }

    /**
     * Send sms to user number related with call data.
     */
    public function sendReply($id)
    {
        $number = null;
        $validator = Validator::make(Input::all(), array(
            'message' => 'required|min:3|max:160',
        ));

        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }

        try {
            $call = Call::findOrFail($id);

            $zenziva = new Zenziva(
                'mamikos',
                Config::get('services.zenziva.userkey'),
                Config::get('services.zenziva.passkey')
            );

            $number = $call->phone;
            $message = Input::get('message');

            $response = $zenziva->sendSms($number, $message);

            $status = $response['message']['status'];

            if ($status != Zenziva::SMS_SENT) {
                if ($status == Zenziva::SMS_NOT_SENT_DUE_INVALID_NUMBER) {
                    throw new Exception("SMS not sent due invalid number.");
                }

                throw new Exception("Sorry, SMS not sent.");
            } elseif ($status == Zenziva::SMS_SENT) {
                ActivityLog::LogCreate(Action::CALL, $id);

                CallReply::create(array(
                    'call_id' => $id,
                    'message' => $message,
                ));
            }
        } catch (Exception $e) {
            return Redirect::route('admin.call.reply', $id)
                ->with('error_message', $e->getMessage());
        }


        $pageRedirect = Input::get('page',1);

        return Redirect::route('admin.call.index', array('page' => $pageRedirect,'from' => Input::get('from'),'to' => Input::get('to')))->with('message', 'Sms sent to ' . $number);
    }


}
