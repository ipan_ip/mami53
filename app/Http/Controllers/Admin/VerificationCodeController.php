<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use View;
use Auth;
use App\Entities\Activity\ActivationCode;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;


class VerificationCodeController extends Controller
{
	
	function __construct()
	{
		$this->middleware(function ($request, $next) {
            if(!Auth::user()->can('access-verification-code')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

		View::share('contentHeader', 'Kode Verifikasi');
        View::share('user', Auth::user());
	}

	public function index(Request $request)
	{
		$query = ActivationCode::with('delivery_report')->withTrashed()->orderBy('created_at', 'desc');

		if($request->filled('phone') && $request->get('phone') != '') {
			$query = $query->where('phone_number', 'like', '%' . $request->get('phone') . '%');
		}
		$verificationCodes = $query->paginate(20);

		$viewData = array(
            'boxTitle'     			=> 'List Kode Verifikasi',
            'verificationCodes'     => $verificationCodes,
		);
		
		ActivityLog::LogIndex(Action::VERIFICATION_CODE);

        return view('admin.contents.verification.index', $viewData);
	}
}