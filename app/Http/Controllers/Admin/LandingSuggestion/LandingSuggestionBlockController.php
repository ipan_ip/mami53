<?php

namespace App\Http\Controllers\Admin\LandingSuggestion;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;
use Auth;
use Carbon\Carbon;
use App\Entities\Entrust\Role;
use App\User;
use Validator;
use App\Entities\Landing\LandingSuggestionBlock;
use App\Entities\Landing\LandingSuggestionList;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;

class LandingSuggestionBlockController extends Controller
{

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!Auth::user()->can('access-landing-suggestion')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        View::share('contentHeader', 'Landing Suggestion Management');
        View::share('user', Auth::user());
    }

    public function index(Request $request)
    {
        $query = LandingSuggestionBlock::query();
        if ($request->filled('q') && $request->get('q') != '') {
            $query = $query->where('name', 'like', '%'.$request->get('q').'%')->orWhere('id', '=', $request->get('q'));
        }

        $landingSuggestionBlocks = $query->orderBy('created_at', 'desc')->paginate(20);

        $viewData = array(
            'boxTitle'                => 'List Landing Suggestion Area',
            'landingSuggestionBlocks' => $landingSuggestionBlocks,
        );

        ActivityLog::LogIndex(Action::LANDING_SUGGESTION);

        return view("admin.contents.landing-suggestion.index", $viewData);
    }

    public function create(Request $request)
    {
        $viewData = array(
            'boxTitle'  => 'Tambah Landing Suggestion Area'
        );

        return view("admin.contents.landing-suggestion.create", $viewData);
    }

    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:190',
            'latitude_1' => 'required',
            'longitude_1' => 'required',
            'latitude_2' => 'required',
            'longitude_2' => 'required',
            'landing_ids.*' => 'required',
            'aliases.*' => 'max:250'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors())->withInput();
        }

        $suggestionBlock = new LandingSuggestionBlock;
        $suggestionBlock->name = $request->name;
        $suggestionBlock->latitude_1 = $request->latitude_1;
        $suggestionBlock->longitude_1 = $request->longitude_1;
        $suggestionBlock->latitude_2 = $request->latitude_2;
        $suggestionBlock->longitude_2 = $request->longitude_2;
        $suggestionBlock->save();

        ActivityLog::LogCreate(Action::LANDING_SUGGESTION, $suggestionBlock->id);

        foreach ($request->landing_ids as $key => $landingId) {
            $suggestionItem = new LandingSuggestionList;
            $suggestionItem->landing_block_id = $suggestionBlock->id;
            $suggestionItem->reference_type = 'landing_rooms';
            $suggestionItem->reference_id = $landingId;
            $suggestionItem->alias = isset($request->aliases[$key]) ? $request->aliases[$key] : null;
            $suggestionItem->save();
        }

        return redirect()->route('admin.landing-suggestion.index')->with('message', 'Landing Suggestion berhasil ditambahkan');
    }

    public function edit(Request $request, $id)
    {
        $suggestionBlock = LandingSuggestionBlock::with('landing_rooms')->find($id);

        $viewData = array(
            'boxTitle'  => 'Edit Landing Suggestion Area',
            'suggestionBlock' => $suggestionBlock
        );

        return view("admin.contents.landing-suggestion.edit", $viewData);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:190',
            'latitude_1' => 'required',
            'longitude_1' => 'required',
            'latitude_2' => 'required',
            'longitude_2' => 'required',
            'landing_ids.*' => 'required',
            'aliases.*' => 'max:250'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors())->withInput();
        }

        $suggestionBlock = LandingSuggestionBlock::with('landing_rooms')->find($id);
        $suggestionBlock->name = $request->name;
        $suggestionBlock->latitude_1 = $request->latitude_1;
        $suggestionBlock->longitude_1 = $request->longitude_1;
        $suggestionBlock->latitude_2 = $request->latitude_2;
        $suggestionBlock->longitude_2 = $request->longitude_2;
        $suggestionBlock->save();

        $landingRoomExistingIds = $suggestionBlock->landing_rooms->pluck('id')->toArray();

        $landingRoomNewIds = [];

        foreach ($request->landing_ids as $key => $landingId) {
            if (in_array($landingId, $landingRoomExistingIds)) {
                $suggestionItem = LandingSuggestionList::where('reference_type', 'landing_rooms')
                                    ->where('reference_id', $landingId)
                                    ->where('landing_block_id', $suggestionBlock->id)
                                    ->first();

                $suggestionItem->alias = isset($request->aliases[$key]) ? $request->aliases[$key] : null;
                $suggestionItem->save();

            } else {
                $suggestionItem = new LandingSuggestionList;
                $suggestionItem->landing_block_id = $suggestionBlock->id;
                $suggestionItem->reference_type = 'landing_rooms';
                $suggestionItem->reference_id = $landingId;
                $suggestionItem->alias = isset($request->aliases[$key]) ? $request->aliases[$key] : null;
                $suggestionItem->save();
            }

            $landingRoomNewIds[] = $landingId;
        }

        foreach ($landingRoomExistingIds as $existingId) {
            if (!in_array($existingId, $landingRoomNewIds)) {
                LandingSuggestionList::where('reference_type', 'landing_rooms')
                    ->where('reference_id', $existingId)
                    ->where('landing_block_id', $suggestionBlock->id)
                    ->delete();
            }
        }
        
        ActivityLog::LogUpdate(Action::LANDING_SUGGESTION, $id);

        return redirect()->route('admin.landing-suggestion.index')->with('message', 'Landing Suggestion berhasil dirubah');
    }

    public function delete(Request $request, $id)
    {
        $suggestionBlock = LandingSuggestionBlock::with('landing_rooms')->find($id);

        LandingSuggestionList::where('landing_block_id', $suggestionBlock->id)
            ->delete();

        $suggestionBlock->delete();

        ActivityLog::LogDelete(Action::LANDING_SUGGESTION, $id);

        return redirect()->route('admin.landing-suggestion.index')->with('message', 'Landing Suggestion berhasil dihapus');
    }
}
