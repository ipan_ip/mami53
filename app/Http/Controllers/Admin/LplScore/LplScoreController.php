<?php

namespace App\Http\Controllers\Admin\LplScore;

use App\Criteria\LplScore\AdminCriteria;
use App\Entities\Activity\Action;
use App\Entities\Activity\ActivityLog;
use App\Http\Controllers\Web\MamikosController;
use App\Http\Helpers\ApiResponse as Api;
use App\Presenters\LplScorePresenter;
use App\Repositories\LplScore\LplScoreRepositoryEloquent;
use Auth;
use Bugsnag;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Application;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Prettus\Repository\Exceptions\RepositoryException;
use Validator;
use View;

class LplScoreController extends MamikosController
{
    protected $title = 'LPL Criteria Management';
    protected $repository;

    public const DEFAULT_LIMIT = 50;

    public function __construct(LplScoreRepositoryEloquent $repository)
    {
        $this->middleware(
            function ($request, $next) {
                if (!Auth::user()->can('access-lpl-score')) {
                    return redirect('admin');
                }

                return $next($request);
            }
        );

        View::share('contentHeader', $this->title);
        View::share('user', Auth::user());

        $this->repository = $repository;
    }

    /**
     * @return Factory|Application|RedirectResponse|\Illuminate\View\View
     */
    public function index()
    {
        $viewData = ['boxTitle' => $this->title];
        ActivityLog::LogIndex(Action::LPL_SCORE);

        return view('admin.contents.lpl-score.index', $viewData);
    }

    /**
     * @return Factory|Application|RedirectResponse|\Illuminate\View\View
     */
    public function calculator()
    {
        $viewData = ['boxTitle' => $this->title];
        ActivityLog::LogIndex(Action::LPL_SCORE_CALC);

        return view('admin.contents.lpl-score.calculator', $viewData);
    }

    /**
     * @param Request $request
     * @return LengthAwarePaginator|Collection|mixed
     * @throws RepositoryException
     */
    public function getAllData(Request $request)
    {
        $criterias = $this->repository
            ->orderBy('order', 'desc')
            ->get();

        $ranks = $criterias->unique('order')->pluck('order')->toArray();
        $allCriterias = $criterias->toArray();
        
        $criteriaGroupByRank = [];
        foreach ($ranks as $rank) {
            $filtered = array_values(array_filter($allCriterias, function ($value) use ($rank) {
                return $value['order'] === $rank;
            }));

            $row['items'] = $filtered;
            $row['rank'] = $rank;

            $criteriaGroupByRank[] = $row;
        }

        $response['total'] = count($criteriaGroupByRank);
        $response['rows'] = $criteriaGroupByRank;

        return $response;
    }

    /**
     * @param $id
     * @return LengthAwarePaginator|Collection|mixed
     */
    public function getData($id)
    {
        return $this->repository->find($id);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function updateData(Request $request)
    {
        try {
            $validator = Validator::make(
                $request->all(),
                [
                    'id' => 'required|numeric',
                    'name' => 'required|string',
                    'description' => 'required|string'
                ]
            );

            if ($validator->fails()) {
                return $this->sendErrorResponse('Missing or malformed request!');
            }

            $this->repository->update(
                [
                    'name' => trim($request->name),
                    'description' => e(trim($request->description))
                ],
                (int) $request->id
            );
        } catch (Exception $exception) {
            Bugsnag::notifyException($exception);

            return $this->sendErrorResponse(
                $exception->getMessage(),
                'Internal Server Error',
                500
            );
        }

        return Api::responseData(
            [
                'updated' => true
            ]
        );
    }

    public function reorderData(Request $request)
    {
        try {
            if (!$request->filled('_data')) {
                return $this->sendErrorResponse('Missing or malformed request!');
            }

            ActivityLog::LogIndex(Action::LPL_SCORE_REORDER);

            $this->repository->reorderData($request->_data);

        } catch (Exception $exception) {
            Bugsnag::notifyException($exception);

            return $this->sendErrorResponse(
                $exception->getMessage(),
                'Internal Server Error',
                500
            );
        }

        return Api::responseData(
            [
                'queued' => true
            ]
        );
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getHistory(Request $request)
    {
        try {
            $response = $this->repository->getAllHistory();

            return Api::responseData(
                [
                    'data' => $response
                ]
            );
        } catch (Exception $e) {
            Bugsnag::notifyException($e);

            return $this->sendErrorResponse(
                $e->getMessage(),
                'Internal Server Error',
                500
            );
        }
    }

    /**
     * @param $message
     * @param string $severity
     * @param int $code
     * @return JsonResponse
     */
    public function sendErrorResponse($message, $severity = 'Bad Request', $code = 400)
    {
        return Api::responseError(
            $message,
            $severity,
            $code
        );
    }
}
