<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Activity\Action;
use App\Entities\Activity\ActivityLog;
use App\Entities\Owner\Loyalty;
use App\Http\Controllers\Controller;
use App\Libraries\LoyaltyImporter;
use App\User;
use Auth;
use Bugsnag;
use Exception;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel as Importer;
use Illuminate\Support\Str;
use Storage;
use View;

class OwnerLoyaltyController extends Controller
{
	protected $storagePath = 'imported';

	public function __construct()
	{
		$this->middleware(function ($request, $next)
		{
			if (!Auth::user()->can('access-owner-loyalty'))
			{
				return redirect('admin');
			}

			return $next($request);
		});

		if (!Storage::disk('local')->exists($this->storagePath))
		{
			Storage::disk('local')->makeDirectory($this->storagePath);
		}

		View::share('user', Auth::user());
		View::share('contentHeader', 'Owner Loyalty Management');
	}

	/**
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function index(Request $request)
	{
		$query = Loyalty::with(['importer'])
			->orderBy('data_date', 'desc')
			->orderBy('created_at', 'desc')
			->orderBy('updated_at', 'desc');

		if ($request->filled('q'))
		{
			$query->where(function ($q) use ($request)
			{
				$q->where('owner_phone_number', 'like', '%' . $request->q . '%');
			});
		}

		$rowsOwner = $query->paginate(20);

		foreach ($rowsOwner as $row)
		{
			// get related owner data
			$formattedOwnerPhoneNumber = substr($row->owner_phone_number, 0, 1) == '0' ? $row->owner_phone_number : '0' . $row->owner_phone_number;
			$row->user                 = User::where('phone_number', $formattedOwnerPhoneNumber)->first();

			// compile expiration date
			$expiryDate           = Loyalty::getExpiryDate($row);
			$row->expiration_date = is_null($expiryDate) ? '-' : $expiryDate->format('d M Y');
		}

		$viewData = [
			'rowsOwner' => $rowsOwner,
			'boxTitle'  => 'Owner Loyalty Management',
			'searchUrl' => route('admin.loyalty'),
		];

		ActivityLog::LogIndex(Action::OWNER_LOYALTY);

		return view('admin.contents.loyalty.index', $viewData);
	}

	/**
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return array
	 */
	public function update(Request $request)
	{
		$error = null;

		try
		{
			$ownerLoyalty = Loyalty::find($request->id);
			if (is_null($ownerLoyalty))
			{
				$error = "Gagal menemukan data Owner Loyalty. Silakan refresh halaman dan coba lagi.";
			}

			// Find owner data
			$owner = User::find($request->user_id);
			if (is_null($owner))
			{
				$error = "Gagal mendapatkan data owner. Silakan refresh halaman dan coba lagi.";
			}

			if (!is_null($error))
			{
				return [
					'success' => false,
					'message' => $error,
				];
			}

			// update record
			$ownerLoyalty->owner_phone_number = $owner->phone_number;
			$ownerLoyalty->save();

		}
		catch (Exception $e)
		{
			Bugsnag::notifyException($e);

			return [
				'success' => false,
				'message' => "<h4>Terjadi error saat menghapus data Owner Loyalty</h4>" . $e->getMessage(),
				'error'   => $e->getMessage(),
			];
		}

		return [
			'success' => true,
		];
	}

	/**
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return array
	 */
	public function remove(Request $request)
	{
		try
		{
			$ownerLoyalty = Loyalty::find($request->id);

			if (is_null($ownerLoyalty))
			{
				return [
					'success' => false,
					'message' => "Gagal menemukan data Owner Loyalty. Silakan refresh halaman dan coba lagi.",
				];
			}

			// remove record
			$ownerLoyalty->delete();

			ActivityLog::LogDelete(Action::OWNER_LOYALTY, $request->id);

		}
		catch (Exception $e)
		{
			Bugsnag::notifyException($e);

			return [
				'success' => false,
				'message' => "<h4>Terjadi error saat menghapus data Owner Loyalty</h4>" . $e->getMessage(),
			];
		}

		return [
			'success' => true,
		];
	}

	/**
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return array
	 */
	public function import(Request $request)
	{
		$file = $request->loyalty_file;

		try
		{
			if (!$file->isValid())
			{
				throw new Exception("File " . $file->getClientOriginalName() . " is invalid");
			}

			$extension = $file->getClientOriginalExtension();
			$filename   = Str::uuid() . '.' . $extension;
			$path      = Storage::disk('local')->putFileAs($this->storagePath, $file, $filename);

			Importer::queueImport(new LoyaltyImporter(), Storage::disk('local')->path($path));
		}
		catch (Exception $e)
		{
			Bugsnag::notifyException($e);

			return [
				'success' => false,
				'message' => "<h4>Terjadi error saat mengimpor dokumen</h4>" . $e->getMessage(),
			];
		}

		return [
			'success' => true,
		];
	}
}
