<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Room\Element\Card;
use Illuminate\Http\Request;

use App\Entities\Media\Media;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Helpers\ApiResponse as Api;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Intervention\Image\Facades\Image;
use App\Entities\Room\Room;
use Carbon\Carbon;
use App\Entities\Media\MediaHandler;
use App\Entities\Room\Element\RoomTermDocument;
use File;

class MediaController extends Controller
{
    /**
     * Exclude watermarking for below image types:
     * 1. User related image
     * 2. Vacancy photo
     * 3. Event photo
     * 4. Slide image
     *
     * @var string[]
     */
    protected $nonWatermarkedTypes = [
        Media::USER_PHOTO_TYPE_2,
        Media::COMPANY_PHOTO_TYPE,
        Media::EVENT_PHOTO_TYPE,
        Media::SLIDE_PHOTO_TYPE,
        Media::FLASH_SALE_PHOTO_TYPE,
        Media::SANJUNIPERO_HEADER_DESKTOP,
        Media::SANJUNIPERO_HEADER_MOBILE
    ];

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeMedia(Request $request)
    {
        $file = $request->hasFile('media') ? $request->file('media') : $request->file('file');

        $mediaTypeInput = $request->input('media_type');
        $mediaType      = is_array($mediaTypeInput) ? $mediaTypeInput[0] : $mediaTypeInput;

        $option = [];

        // Determine if it should keep PNG formatting
        if (in_array($mediaType,
            [
                Media::EVENT_PHOTO_TYPE,
                Media::SLIDE_PHOTO_TYPE,
                Media::FLASH_SALE_PHOTO_TYPE
            ]
        )) {
            $option['keep_png'] = true;
        }

        // by default, there always be a watermark
        $needWatermark = true;
        if (
            $request->filled('need_watermark') ||
            // some old application is sending `watermarking` instead of `need_watermark`)
            $request->filled('watermarking')
        ) {
            // if request is explicitly excluding watermark
            if ($request->need_watermark === 'false' || $request->watermarking === 'false') {
                $needWatermark = false;
            }

            // extra parameter for watermarking_type:
            // "regular" or "premium"
            if ($request->filled('watermarking_type')) {
                $option['watermarking_type'] = $request->watermarking_type;
            }
        }

        // if media_type is nonWatermarkedTypes
        if (in_array($mediaType, $this->nonWatermarkedTypes) !== false) {
            $needWatermark = false;
        }

        if ($request->filled("propertyId")) {
            $option = [
                "propertyId"   => $request->input("propertyId"),
                "room_type"    => "villa",
                "from"         => "admin",
                "type"         => $request->input('type'),
                'propertyName' => $request->input('propertyName'),
            ];
        }

        $media = Media::postMedia(
            $file,
            'upload',
            Auth::id(),
            null,
            null,
            $mediaType,
            $needWatermark,
            null,
            true,
            $option
        );

        return Api::responseData(compact('media'));
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public static function deleteCard($id)
    {
        $card = Card::find($id);

        if (is_null($card)) {
            return Redirect::back()->with('message', 'Data tidak ditemukan');
        }

        $roomId = $card->designer_id;
        $card->delete();

        Room::where('id', $roomId)
            ->update(
                [
                    'photo_count' => \DB::raw('photo_count - 1'),
                ]
            );

        return Redirect::back()->with('message', 'Operation Successful !');
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public static function deleteRoomTermMedia($id)
    {
        $termDocument = RoomTermDocument::where('media_id', $id);

        if (is_null($termDocument)) {
            return Redirect::back()->with('message', 'Data tidak ditemukan');
        }

        $termDocument->delete();

        return Redirect::back()->with('message', 'Foto peraturan kos berhasil dihapus');
    }

    public function deleteCardRound($roomId, $id)
    {
        $media = Media::find($id);
        $media->delete();

        Room::where('id', $roomId)
            ->update(
                [
                    'photo_round_id' => null,
                ]
            );

        return Redirect::back()->with('message', 'Operation Successful !');
    }

    public function deleteUserPhoto($id)
    {
        $media = Media::find($id);
        $media->delete();

        return Api::responseData(compact('id'));
    }

    public function getImageReal($mediaId)
    {
        try {
            $fileLocation = MediaHandler::getOriginalImage($mediaId);

            if (is_null($fileLocation)) {
                return redirect()->back()->with('error_message', 'File tidak ditemukan');
            }

            return Image::make($fileLocation)->response('jpg');
        } catch (\Exception $e) {
            // in some cases, there will be permission issue that cannot be handled via code
            return redirect()->back()->with('error_message', $e->getMessage());
        }
    }

    public function deletePhoto(Request $request, $id)
    {
        $media = Media::where('id', $id)->first();
        if (is_null($media)) {
            return Api::responseData(["status" => false]);
        }
        $media->delete();
        return Api::responseData(['status' => true]);
    }

    public function listPhotos(Request $request)
    {
        $rooms = Room::query()
            ->with(
                [
                    'cards' => function ($q)
                    {
                        $q->where('type', 'image');
                    },
                    'cards.photo',
                ]
            )
            ->whereHas(
                'cards',
                function ($q) use ($request)
                {
                    if (!$request->filled('start_date') && !$request->filled('end_date')) {
                        // if no time span set, then fetch data strted from last month
                        $q->whereDate('created_at', '>=', Carbon::today()->subMonth());
                    }

                    if ($request->filled('start_date')) {
                        $q->whereDate('created_at', '>=', $request->input('start_date'));
                    }

                    if ($request->filled('end_date')) {
                        $q->whereDate('created_at', '<=', $request->input('end_date'));
                    }
                }
            )
            ->whereNull('apartment_project_id')
            ->orderBy('kost_updated_date', 'desc');

        if ($request->filled('q') && $request->q != '') {
            $rooms = $rooms->where('name', 'like', '%' . $request->q . '%')
                ->orWhere('id', $request->q);
        }

        $rooms = $rooms->paginate(20);

        $viewData = [
            'boxTitle'      => 'List images Kost',
            'data'          => $rooms,
            'contentHeader' => 'Stories Management',
            'user'          => Auth::user(),
        ];

        return view("admin.contents.stories.images", $viewData);
    }

    public function generateWatermark(Request $request)
    {
        try {
            $media = Media::find($request->media_id);

            if (is_null($media)) {
                return [
                    "success" => false,
                    "message" => "Failed fetching media data. Please refresh page and try again.",
                ];
            }

            $media->regenerateWatermark();
        } catch (\Exception $e) {
            return [
                "success" => false,
                "message" => $e->getMessage(),
            ];
        }

        return [
            "success" => true,
        ];
    }
}
