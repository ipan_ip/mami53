<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\Promoted\Promotion;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Entities\Notif\AllNotif;
use App\Notifications\PromoApproved;
use App\Entities\Room\RoomOwner;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;
use App\Entities\Activity\AppSchemes;
use Notification;
use App\Http\Requests\Admin\Promo\PromoVerifyRequest;

class PromoController extends Controller
{
	public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if(!Auth::user()->can('access-promo-owner')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        View::share('contentHeader', 'Stories Management');
        View::share('user', Auth::user());
    }

    public function index(Request $request)
    {
        $data  = Promotion::with('room');

        if ($request->filled('q') && $request->get('q') != '') {
            $queryStr = '%' . $request->get('q') . '%';

            $data = $data->whereIn('designer_id', function($query) use ($request, $queryStr) {
                $query->select('id')
                    ->from('designer')
                    ->where('name', 'like', $queryStr);
            })
            ->orWhere('title', 'like', $queryStr)
            ->orWhere('content', 'like', $queryStr);
        }

        $data = $data->orderBy('updated_at', 'desc')->paginate(20);

    	$viewData = array(
            'boxTitle'     => 'Review List',
            'deleteAction' => 'admin.tag.destroy',
            'promotion'    => $data,
        );

        ActivityLog::LogIndex(Action::PROMO_OWNER);
        
        return View::make('admin.contents.promotion.index', $viewData);
    }

    public function show($id)
    {
        $promo = Promotion::with('room')->find($id);
        
        $viewData = array(
            'boxTitle'    => 'View or Edit Promotion',
            'promo'       => $promo,
            'rowsActive'  => array('true', 'false'),
            'formAction'  => array('admin.promo.update', $id),
            'formMethod'  => 'PUT',
        );
        return View::make('admin.contents.promotion.form', $viewData);
    }

    public function update(Request $request, $id)
    {
        $promo = Promotion::with(['room.owners' => function($q) {
                        $q->whereIn('owner_status', RoomOwner::OWNER_TYPES);
                    }])->where('id', $id)
                    ->first();
        
        if (is_null($promo)) {
            return redirect('admin/promo')->with('message', 'Promo tidak ditemukan');
        }

        $promo->content      = $request->input('content');  
        $promo->verification = $request->input('is_confirm');
        $promo->save();

        if ($promo->verification == Promotion::VERIFY && count($promo->room->owners) > 0) {
            Notification::send($promo->room->owners[0]->user, new PromoApproved($promo->room->song_id));
        }
        ActivityLog::LogUpdate(Action::PROMO_OWNER, $id);
        return redirect('admin/promo')->with('message', 'Promo is updated.'); 
    
    }


    public function verifyWithButton(PromoVerifyRequest $request, $id)
    {
        $params = $request->validated();
        $promo = $request->getPromo();
        
        $promo->verification = $request->input('is_confirm');
        $promo->save();

        if ($params['is_confirm'] == Promotion::UNVERIFY) {
            return redirect()->back()->with("message", "Berhasil mengganti status");
        }

        if (
            $promo->verification == Promotion::VERIFY 
            && count($promo->room->owners) > 0
        ) {
            Notification::send($promo->room->owners[0]->user, new PromoApproved($promo->room->song_id));
        }

        return redirect('admin/promo')->with('message', 'Promo is updated.'); 
    }

    public function unverifyPromo(Request $request, $id)
    {
        $promo = Promotion::with(['room.owners' => function($q) {
                        $q->whereIn('owner_status', RoomOwner::OWNER_TYPES);
                    }])->where('id', $id)
                    ->first();
        
        $promo->verification = Promotion::IGNORE;
        $promo->save();

        return redirect('admin/promo')->with('message', 'Promo is updated.');
    }

    public function store(Request $request)
    {
        abort(404);
    }

    public function delete($id)
    {
        Promotion::where('id', $id)->delete();

        ActivityLog::LogDelete(Action::PROMO_OWNER, $id);

        return redirect('admin/promo')->with('message', 'Promo is deleted.'); 
    }    
}
