<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Activity\Call;
use App\Entities\Room\Element\RoomTag;
use App\Entities\Room\TraitAttributeAccessor;
use App\Http\Controllers\Admin\Component\Room\RoomSetterTrait;
use App\Http\Controllers\Admin\Component\DuplicateTrait;
use App\Http\Helpers\KostLevelValueHelper;
use App\Presenters\RoomPresenter;
use App\Repositories\RoomRepositoryEloquent;
use App\Services\Thanos\ThanosService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Entities\Room\Room;
use App\Entities\Room\Element\Tag;
use App\Entities\Room\Element\Card;
use App\User;
use App\Http\Helpers\ApiHelper;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Validation\Validator;
use Illuminate\Support\Facades\URL;
use App\Entities\Room\TraitFieldGenerator;
use App\Entities\Room\HistorySlug;
use App\Entities\Room\ContactUser;
use App\Http\Helpers\ApiResponse as Api;
use App\Entities\Media\Media;
use Config;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image as Image;
use App\Entities\Media\MediaHandler;
use App\Entities\Media\RenameQueue;
use App\Libraries\CSVParser;
use App\Entities\Apartment\ApartmentProject;
use Cviebrock\EloquentSluggable\Services\SlugService;
use App\Libraries\AreaFormatter;
use App\Entities\Area\AreaBigMapper;
use App\Libraries\SMSLibrary;
use DB;
use App\Entities\Generate\ListingReason;
use Illuminate\Support\Facades\Cache;
use App\Entities\Generate\Currency;
use App\Entities\Room\RoomPriceType;
use App\Entities\User\UserChecker;
use App\Entities\User\UserCheckerTracker;
use App\Libraries\YouTubeLibrary;
use Exception;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use App\Http\Controllers\Admin\Component\Room\RoomFilterTrait;
use App\Http\Controllers\Admin\Component\Room\RoomSorterTrait;
use App\Entities\User\UserRole;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;
use App\Entities\Level\KostLevel;
use App\Http\Helpers\ApiResponse;
use Illuminate\Http\JsonResponse;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Room\RoomOwner;
use App\Http\Helpers\RequestInputHelper;
use App\Http\Helpers\LplScoreHelper;
use App\Jobs\KostIndexer\IndexKostJob;
use App\Services\Owner\Kost\DuplicateService;

class RoomController extends Controller
{
    use RoomSetterTrait, DuplicateTrait, TraitFieldGenerator, TraitAttributeAccessor, RoomFilterTrait, RoomSorterTrait;

    protected $baseRoute    = '';
    protected $verifiedSorting = ["All", "Active - Not active", "Waiting for verification", "Inactive"];
    protected $login_type   = 'admin';
    protected $itemPerPage  = 15;

    protected $indexRoute   = 'admin.room.index';
    protected $createRoute  = 'admin.room.create';
    protected $storeRoute   = 'admin.room.store';
    protected $editRoute    = 'admin.room.edit';
    protected $updateRoute  = 'admin.room.update';
    protected $deleteRoute  = 'admin.room.destroy';

    protected $updateRedirect = 'admin.room.redirectkostpost';

    protected $validationMessages = [
        'room_count.lte' => 'Jumlah kamar tidak bisa lebih dari ' . Room::MAX_ROOM_COUNT,
    ];
    const ROOM_DUPLICATE_UPPER_LIMIT = 5;
    const ROOM_DUPLICATE_LOWER_LIMIT = 1;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if(!Auth::user()->can('access-kost')) {
                return redirect('admin');
            }

            return $next($request);
        });

        \View::share('contentHeader', 'Stories Management');
        \View::share('user' , Auth::user());
    }

    public function index(Request $request)
    {
        $query = Room::query()
            ->with([
                'photo_by' => function ($t) {
                    $t->orderBy('id', 'desc');
                },
                'minMonth',
                'level',
                'view_promote',
                'owners',
                'apartment_project',
                'photo',
                'agents',
                'tracking_update',
                'active_discounts',
                'attachment',
                'checkings',
                'facilities',
                'types',
                'cards'
            ])
            ->withCount(['facility_setting', 'types']);

        $cities = Room::whereNotNull('area_city')
            ->where('area_city', '<>', '')
            ->where('area_city', '<>', '-')
            ->groupBy('area_city')
            ->orderBy('area_city')
            ->pluck('area_city');

        // Filter
        $query = $this->filterByName($request, $query);
        $query = $this->filterIndex($request, $query);
        $query = $this->filterRoomByStatus($request, $query);
        $query = $this->filterRoomByType($request, $query);
        $query = $this->filterRoomByLevel($request, $query);
        $query = $this->filterByAreaCity($request, $query);

        //$telp_now = false;
        if ($request->filled('q') AND SMSLibrary::validateIndonesianMobileNumber($request->input('q'))) {
            $telp_now = true;
        }

        // Order By
        $orderByDate = array(
            'lc' => 'created_at',
            'fc' => 'created_at',
            'lu' => 'updated_at',
            'fu' => 'updated_at',
        );

        $orderByMost = array(
            'm' => 'mobile_view' ,
            'w' => 'web_view',
            'l' => 'like', // l stand for like
            's' => 'share',
            'c' => 'comment'
        );

        $query = $this->sortByActivityStatus($request, $query);

        $query = $this->sortByCreationDate($request, $query);

        $rowsRoom = $query->paginate($this->itemPerPage);

        $rooms = (new RoomPresenter('index'))->present($rowsRoom);

        $kostLevelDropdown = KostLevel::get()->pluck('name', 'id')->all();
        $kostLevelDropdown[0] = 'Semua';
        ksort($kostLevelDropdown);

        $viewData = array(
            'boxTitle'        => 'List Kost',
            'login_type'      => $this->login_type,
            'createRoute'     => $this->createRoute,
            'editRoute'       => $this->editRoute,
            'deleteAction'    => $this->deleteRoute,
            'rowsDesigner'    => $rowsRoom,
            'rowsRoom'        => $rooms['data'],
            'searchUrl'       => url()->route($this->indexRoute),
            'startDate'       => $request->input('st'),
            'endDate'         => $request->input('en'),
            'redirectHere'    => \Request::url() . '?' . http_build_query($_GET),
            'sortLists'       => $this->getSortList(),
            'inputBy'         => $this->inputTypes,
            'soringActive'    => $this->verifiedSorting,
            'telp_now'        => isset($telp_now) ? true : false,
            'status'          => ["All", "Active", "Not active"],
            'kostLevelDropdown' => $kostLevelDropdown,
            'areaCities'      => $cities
        );

        ActivityLog::LogIndex(Action::KOST);

        return view("admin.contents.stories.index", $viewData);
    }

    public function indexTwo(Request $request)
    {
        $request->validate([
            'from' => 'nullable|date',
            'to' => 'nullable|date',
        ]);

        $query = Room::with([
            'expired_by',
            'view_promote',
            'owners',
            'view_promote.premium_request.user',
            'hidden_by_thanos'
        ])->withCount([
            'room_unit_empty',
            'room_unit_occupied',
            'room_unit_empty as room_unit_empty_with_contract_count' => function ($query) {
                $query->whereHas('active_contract');
            }
        ]);

        $query->search($request->input('q'));
        $query->createdBetween($request->input('from'), $request->input('to'));

        $rowsRoom = $query->with('photo')->paginate($this->itemPerPage);

        $rooms = (new RoomPresenter('index-two'))->present($rowsRoom);

        $viewData = array(
            'boxTitle'        => 'List Kost',
            'login_type'      => $this->login_type,
            'createRoute'     => $this->createRoute,
            'editRoute'       => $this->editRoute,
            'deleteAction'    => $this->deleteRoute,
            'rowsDesigner'    => $rowsRoom,
            'rowsRoom'        => $rooms['data'],
            'searchUrl'       => url()->route($this->indexRoute),
            'startDate'       => $request->input('st'),
            'endDate'         => $request->input('en'),
            'redirectHere'    => \Request::url() . '?' . http_build_query($_GET),
            'sortLists'       => $this->getSortList(),
            'inputBy'         => $this->inputTypes,
            'gender'          => array("campur", "putra", "putri")

        );

        ActivityLog::LogIndex(Action::KOST_ADDITIONAL);

        return view("admin.contents.stories.index-two", $viewData);
    }

    // Add video page
    public function addVideoTour($roomId)
    {
        if (is_null($roomId))
        {
            return back()->with('error_message', 'Kost / apartemen tidak ditemukan');
        }

        $room = Room::find($roomId);

        if (is_null($room))
        {
            return back()->with('error_message', 'Kost / apartemen tidak ditemukan');
        }

        $viewData = array(
            'login_type'  => Auth::user()->role,
            'boxTitle'  => 'Add Video Tour: <strong>' . $room->name . '</strong>',
            'designerId'  => $roomId,
            'formAction'  => route("admin.room.video.store", $roomId),
            'formMethod'  => 'POST',
            'formCancel'  => url()->previous(),
        );

        return view('admin.contents.stories.partial.video-form', $viewData);
    }

    // Update/Edit Video Page
    public function updateVideoTour($roomId)
    {
        if (is_null($roomId))
        {
            return back()->with('error_message', 'Kost / apartemen tidak ditemukan');
        }

        $room = Room::find($roomId);

        if (is_null($room))
        {
            return back()->with('error_message', 'Kost / apartemen tidak ditemukan');
        }

        $video = Card::where('designer_id', $roomId)
                    ->where('type', 'video')
                    ->first();

        if (is_null($video))
        {
            return back()->with('error_message', 'Video tidak ditemukan');
        }

        // Determine video status (by_mamichecker or not)
        $roomHasChecked = UserCheckerTracker::with(['checker.user'])
            ->where('designer_id', $roomId)
            ->where('action', 'video-tour')
            ->orderBy('id', 'desc')
            ->first();

        if (!is_null($roomHasChecked))
        {
            $video->by_mamichecker = true;
            $video->tracker_id = $roomHasChecked->id;
        }

        $viewData = array(
            'login_type'  => Auth::user()->role,
            'boxTitle'    => 'Edit Video Tour: <strong>' . $room->name . '</strong>',
            'login_type'  => Auth::user()->role,
            'tracker'     => $roomHasChecked,
            'checker'     => $roomHasChecked ? $roomHasChecked->checker : null,
            'video'       => $video,
            'designerId'  => $roomId,
            'formAction'  => route("admin.room.video.store", $roomId),
            'formMethod'  => 'POST',
            'formCancel'  => url()->previous(),
        );

        ActivityLog::LogUpdate(Action::KOST, $roomId);

        return view('admin.contents.stories.partial.video-form', $viewData);
    }

    public function storeVideoTour(Request $request)
    {
        if ($request->filled('trackerId') && (int)$request->trackerId > 0)
            $updateMode = true;
        else
            $updateMode = false;

        if (!$request->filled('videoUrl')) $errorMessage = "Gagal mengolah URL video! Silakan refresh halaman dan coba lagi.";

        if (!$request->filled('checkerId')) $errorMessage = "Gagal mengolah data profil checker! Silakan refresh halaman dan coba lagi.";

        if (!$request->filled('roomId')) $errorMessage = "Gagal mendapatkan ID Kost! Silakan refresh halaman dan coba lagi.";

        // extract video ID form URL (only Youtube URL is supported!)
        $youtubeVideoId = YouTubeLibrary::extractVideoIdFrom($request->input('videoUrl'));
        if (is_null($youtubeVideoId)) $errorMessage = "URL video tidak valid! Cek kembali URL video yang Anda isikan.";

        // Card object
        $cards = array();

        // store video tour data
        try
        {
            // in Editing/updating mode
            if ($updateMode)
            {
                $cardData = Card::find($request->cardId);

                if (is_null($cardData))
                {
                    $errorMessage = "Gagal mendapatkan Data Video! Silakan refresh halaman dan coba lagi";
                }
                else
                {
                    $cards[] = [
                        'type'          => $cardData->type,
                        'description'   => $cardData->description,
                        'source'        => $cardData->source,
                        'ordering'      => $cardData->ordering,
                        'source_url'    => $request->videoUrl,
                        'video'         => 'https://www.youtube.com/watch?v=' . $youtubeVideoId
                    ];

                    // Update card
                    Card::updateCards($cards, $request->roomId, $cardData->id);

                    // Update tracker
                    UserCheckerTracker::updateCheckerId($request->trackerId, $request->checkerId);
                }
            }
            // in Adding mode
            else
            {
                $cards[] = [
                    'type'          => 'video',
                    'description'   => 'Video Tour',
                    'source'        => 'By Admin Kost Booking',
                    'ordering'      => '0',
                    'source_url'    => $request->videoUrl,
                    'video'         => 'https://www.youtube.com/watch?v=' . $youtubeVideoId
                ];

                // Store card
                Card::register($cards, $request->roomId);

                // Store checker info
                $checker = UserChecker::find($request->checkerId);
                if (is_null($checker)) $errorMessage = "Gagal mengolah data profil checker! Silakan refresh halaman dan coba lagi.";

                if (!isset($errorMessage))
                {
                    $room = Room::find($request->roomId);
                    if (is_null($room)) $errorMessage = "Gagal mendapatkan ID Kost! Silakan refresh halaman dan coba lagi.";
                }

                if (!isset($errorMessage))
                {
                    UserCheckerTracker::track($checker, $room, 'video-tour');
                }
            }
        }
        catch (Exception $e)
        {
            $errorMessage = "Error: " . $e->getMessage();
        }

        if (!isset($errorMessage))
        {
            return [
                'success' => true
            ];
        }

        return [
            'success' => false,
            'message' => $errorMessage
        ];
    }

    public function removeVideoTour(Request $request)
    {
        try
        {
            // remove tracker
            UserCheckerTracker::untrack($request->trackerId);
        }
        catch (Exception $e)
        {
            $errorMessage = "Error: " . $e->getMessage();
        }

        if (!isset($errorMessage))
        {
            try
            {
                // remove card
                $cardData = Card::find($request->cardId);
                $cardData->delete();
            }
            catch (Exception $e)
            {
                $errorMessage = "Error: " . $e->getMessage();
            }
        }

        if (!isset($errorMessage))
        {
            return [
                'success' => true
            ];
        }

        return [
            'success' => false,
            'message' => $errorMessage
        ];
    }

    public function createOwner(Request $request, $roomId)
    {
        $room = Room::with('owners', 'owners.user')
                    ->where('id', $roomId)
                    ->first();

        if (count($room->owners) > 0) return redirect()->route('admin.room.index')->with('message', 'sudah punya user');

        $user = User::where('phone_number', 'LIKE', '%'.$room->owner_phone.'%')->where('is_owner', 'true')->count();

        if ($user > 0) return redirect()->route('admin.room.index')->with('message', 'sudah punya akun');

        $link = ApiHelper::random_string('alpha', 25);

        $viewData = array(
            'boxTitle'     => 'Owner Detail',
            'deleteAction' => 'admin.tag.destroy',
            'room'         => $room,
            'link'         => $link
        );

        return view('admin.contents.stories.owner', $viewData);
    }

    public function historyOwner()
    {
        return view("admin.contents.stories.history");
    }

    public function CallOwner(Request $request, $id)
    {
       $calls = ContactUser::with('user')
                             ->where('designer_id', $id)
                             ->orderBy('id', 'desc')
                             ->get();

        if (count($calls) == 0) $calls = null;

        $carbon = Carbon::now();

        $viewData = array(
            'boxTitle'        => 'List History Call',
            'login_type'      => $this->login_type,
            'createRoute'     => $this->createRoute,
            'editRoute'       => $this->editRoute,
            'deleteAction'    => $this->deleteRoute,
            'searchUrl'       => url()->route($this->indexRoute),
            'startDate'       => $request->input('st'),
            'endDate'         => $request->input('en'),
            'redirectHere'    => \Request::url() . '?' . http_build_query($_GET),
            'sortLists'       => $this->getSortList(),
            'inputBy'         => $this->inputTypes,
            'calls'           => $calls,
            'carbon'          => $carbon
        );

        return view("admin.contents.stories.calls", $viewData);
    }

    private function getSortList()
    {
        return array(
            'lc' => 'Last Created',
            'fc' => 'First Created',
            'lu' => 'Last Updated',
            'fu' => 'First Updated',
            'm'  => 'Most Mobile View',
            'w'  => 'Most Web View',
            'l'  => 'Most Likes',
            'c'  => 'Most Comments',
            's'  => 'Most Shares',
        );
    }

    public function create(Request $request)
    {

        $rowsConcern = Tag::where('type', '<>', 'fac_project')->get();

        $genderOptions = [
            0 => 'Campur',
            1 => 'Pria',
            2 => 'Wanita'
        ];

        $viewData = array(
            'boxTitle'       => 'Insert Room',
            'rowsConcern'    => $rowsConcern,
            'agent_status'   => $this->inputTypes,
            'formAction'     => $this->storeRoute,
            'formMethod'     => 'POST',
            'formCancel'     => URL::route($this->indexRoute),
            'genderOptions'  => $genderOptions
        );

        return view('admin.contents.stories.create', $viewData);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = validator($request->all(), array(
            'name' => 'required|max:250',
            'price_daily' => 'numeric',
            'price_weekly' => 'numeric',
            'price_monthly' => 'required|numeric',
            'price_yearly' => 'numeric',
            'room_count' => 'required|numeric',
            'room_available' => 'required|numeric',
            'address' => 'required',
            'longitude' => 'required|numeric',
            'latitude' => 'required|numeric',
            'owner_name' => 'required_without:manager_name',
            'owner_phone' => 'required_with:owner_name',
            'manager_name' => 'required_without:owner_name',
            'manager_phone' => 'required_with:manager_name',
            'agent_name' => 'required',
            'area_city' => 'required'
        ));

        $validator->after(function($validator) use ($request)
        {
            if ( (int)$request->price_daily != 0 && strlen($request->price_daily) < 4 )
            {
                $validator->errors()->add('price_daily', 'The price daily minimal 4 character.');
            }

            if ( (int)$request->price_weekly != 0 && strlen($request->price_weekly) < 4 )
            {
                $validator->errors()->add('price_weekly', 'The price weekly minimal 4 character.');
            }

            if ( (int)$request->price_monthly != 0 && strlen($request->price_monthly) < 4 )
            {
                $validator->errors()->add('price_monthly', 'The price monthly minimal 4 character.');
            }

            if ( (int)$request->price_yearly != 0 && strlen($request->price_yearly) < 4 )
            {
                $validator->errors()->add('price_yearly', 'The price yearly minimal 4 character.');
            }

            if ((int) $request->room_count < 1) {
                $validator->errors()->add('room_count', 'Jumlah kamar minimal 1');
            }

            if ((int) $request->room_count >500 ) {
                $validator->errors()->add('room_count', 'Jumlah kamar tidak boleh lebih dari 500');
            }

            if ((int) $request->room_count < (int) $request->room_available) {
                $validator->errors()->add('room_count', 'Jumlah kamar harus lebih besar dari jumlah kamar tersedia');
            }
        });

        $validator->after(function($validator) use ($request) {
            if($request->name != '') {
                $roomExist = Room::where('name', $request->name)->first();
                if($roomExist) {
                    $validator->errors()->add('name', 'Nama sudah pernah digunakan sebelumnya');
                }
            }
        });

        if ($validator->fails()) {
            return redirect()->route('admin.room.create')->withErrors($validator)->withInput();
        }

        //check for indexed type
        if (is_null($request->input('indexed'))) {
            $isIndexed = 0;
        } else {
            $isIndexed = 1;
        }

        $price_daily = RequestInputHelper::getNumericValue($request->input('price_daily'));
        $price_monthly = RequestInputHelper::getNumericValue($request->input('price_monthly'));
        $price_weekly = RequestInputHelper::getNumericValue($request->input('price_weekly'));
        $price_yearly = RequestInputHelper::getNumericValue($request->input('price_yearly'));
        $price_quarterly = RequestInputHelper::getNumericValue($request->input('3_month'));
        $price_semiannually = RequestInputHelper::getNumericValue($request->input('6_month'));

        $areaBig = NULL;
        $areaCity = NULL;
        if (!empty($request->input('area_city'))) {
            $areaCity = AreaFormatter::format($request->input('area_city'));
            $areaBig = $areaCity;
        }

        $areaSubdistrict = NULL;
        if (!empty($request->input('area_subdistrict'))) {
            $areaSubdistrict =  AreaFormatter::format($request->input('area_subdistrict'));
        }

        $agentStatus = null;
        if (!empty($request->input('agent_status'))) {
            if ($request->input('agent_status') == 'Semua') {
                $agentStatus = null;
            } else {
                $agentStatus = $request->input('agent_status');
            }
        }

        $designerDetail = array(
            'name'              => $request->input('name'),
            'size'              => $request->input('size'),
            'gender'            => $request->input('gender'),
//            'type'              => $request->input('type'),
            'floor'             => $request->input('floor'),
            'price'             => $request->input('price_monthly'),
            'address'           => $request->input('address'),
            'longitude'         => $request->input('longitude'),
            'latitude'          => $request->input('latitude'),
            'description'       => trim(strip_tags($request->input('description'))),
            'price_daily'       => $price_daily,
            'price_weekly'      => $price_weekly,
            'price_monthly'     => $price_monthly,
            'price_yearly'      => $price_yearly,
            'price_quarterly'   => $price_quarterly,
            'price_semiannually' => $price_semiannually,
            'fac_room_other'    => substr($request->input('fac_room_other'), 0, 500),
            'fac_bath_other'    => substr($request->input('fac_bath_other'), 0, 500),
            'fac_share_other'   => substr($request->input('fac_share_other'), 0, 500),
            'fac_near_other'    => substr($request->input('fac_near_other'), 0, 500),
            'price_remark'      => substr($request->input('price_remark'), 0, 500),
            'remark'            => $request->input('remarks'),
            'agent_name'        => $request->input('agent_name'),
            'verificator'       => $request->input('verificator'),
            'room_count'        => $request->input('room_count'),
            'room_available'    => $request->input('room_available'),
            'status'            => $request->input('room_available') > 0 ? 2 : 0,
            'remark'            => $request->input('remarks'),
            'animal'            => $request->input('animal'),
            'owner_name'        => $request->input('owner_name'),
            'owner_phone'       => $request->input('owner_phone'),
            'manager_name'      => $request->input('manager_name'),
            'manager_phone'     => $request->input('manager_phone'),
            'area_subdistrict'  => $areaSubdistrict,
            'area_city'         => $areaCity,
            'area_big'          => $areaBig,
            'office_phone'      => '',
            'kost_updated_date' => date('Y-m-d H:i:s'),
            'is_active'         => 'false',
            'is_indexed'        => $isIndexed,
            'is_booking'        => $request->input('is_booking'),
            'youtube_id'        => $request->input('youtube_id'),
            'agent_status'      => $agentStatus,
            'guide'             => $request->input('guide'),
            'kost_updated_date' => Carbon::now()->format('Y-m-d H:i:s'),
            'is_promoted'       => 'false',
            'is_booking'        => 0,
            'photo_count'       => 0,
            'view_count'        => 0,
            'expired_phone'     => 0,
            'is_mamirooms'      => ($request->input('mamirooms') == '1'),
            'is_testing'        => ($request->input('testing') == '1'),
        );

        // storing room
        $newRoom = new Room;
        foreach ($designerDetail as $index => $value){
            $newRoom->$index = $value;
            // Room Class
            $newRoom->class        = $newRoom->calculateClass($request->get('concern_ids'));
        }
        $newRoom->generateSongId();
        $newRoom->save();

        // Store class history
        $newRoom->storeClassHistory("admin-add-room");

        /* Set Kost level "Mamirooms" */
        if ($newRoom->is_mamirooms) {
            KostLevelValueHelper::assignMamiroomsLevelToKos($newRoom);
        }

        // storing tag
        $designerTag = $request->get('concern_ids');
        if(isset($designerTag) && count($designerTag) > 0) {
            $newRoom->syncTags($designerTag);
        }

        // linking room owner if any
        $owner = User::ownerOnly()->where('phone_number', $request->owner_phone)->first();
        if (!empty($owner)) {
            $roomOwner = new RoomOwner();
            $roomOwner->user_id = $owner->id;
            $roomOwner->status = RoomOwner::ROOM_ADD_STATUS;
            $roomOwner->owner_status = RoomOwner::STATUS_TYPE_KOS_OWNER;
            $newRoom->owners()->save($roomOwner);
        }

        ActivityLog::LogCreate(Action::KOST, $newRoom->id);

        if ($newRoom) {
            return redirect()->route('admin.room.show', $newRoom->id)->with("message","Room data has been successfully inserted");
        } else {
            return redirect()->route('admin.room.create')->with("error_message","Error, looks something went wrong")->withInput();
        }
    }

    /**
     * This function is deprecated (2018-01-18)
     * Use App\Libraries\AreaFormatted instead
     *
     */
    public function sanitizeCityOrDistrictName($word, $data)
    {
        $dataAnswer = preg_split("[ ]", $word);

        $dataAnswerCount = count($dataAnswer);
        for ($i=0; $i < $dataAnswerCount; $i++) {

            if (array_key_exists($dataAnswer[$i], $data)) {
                $dataAnswer[$i] = $data[$dataAnswer[$i]];
            }

            $fin[] = $dataAnswer[$i];
        }

        return $fin;
    }

    /**
     * Show the form to upload cover, thumbnail and cards
     *
     * @param int $id
     * @return Response
     * */
    public function show($id)
    {
        $room = Room::find($id);
        if (is_null($room)) return back()->with('error', 'Kos tidak tersedia');

        $photos = Card::categorizedCards($room, 'edit');
        $photo360 = $room->photo_360;

        return view('admin.contents.stories.show', compact('room','photos', 'photo360'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function submit(Request $request, $id)
    {
        // photos id
        $photosId = array(
            'cover' => $request->get('cover'),
            'bangunan' => $request->get('building'),
            'kamar' => $request->get('room'),
            'kamar-mandi' => $request->get('bathroom'),
            'lainnya' => $request->get('other'),
        );

        $photosRoundId = $request->get('round');

        $room = Room::find($id);

        // check for card if exists
        // $card = Card::showList($room);

        // if card is not existed yet so it assumed it was a new room
        // if ($card == null) {
        //     $room->generateSongId();
        //     $room->generateLocationId();
        //     $room->generateCityCode();

        //     $message = 'Room has successfully inserted';
        // } else {
            $message = 'Room has succesfully updated';
        // }

        if ($photosId != null){
            $photoCount = $this->addPhotosToRoom($room, $photosId);

            if(!is_null($room->photo_count)) {
                $room->photo_count += $photoCount;
            } else {
                $room->photo_count = $photoCount;
            }
        }

        if ($photosRoundId != null) {
            $room->photo_round_id = $photosRoundId[0];
        }

        $room->updateSortScore();
        $room->save();

        return redirect()->route($this->indexRoute)->with('message',$message);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */

    public function edit(Request $request, $id)
    {
        $room = Room::find($id);
        if (is_null($room->apartment_project_id)) {
            return redirect('admin/kost/' . $room->id . '/edit');
        }

        $rowDesigner = Room::with([
            'listing_reason' => function($p) {
                $p->orderBy('id', 'desc');
            },
            'room_price_type',
            'agents',
            'cards'
        ])->where('id', $id)->first();

        if (is_null($rowDesigner)) return back()->with('message', 'Kos tidak tersedia');

        $showPhotoDuplicate = false;
        if (count($rowDesigner->cards) == 0 AND !is_null($rowDesigner->duplicate_from)) $showPhotoDuplicate = true;

        $show_agent_input = true;
        $agen_data = [$rowDesigner->agent_phone, $rowDesigner->agent_name];
        if ($rowDesigner->agent_id > 0) {
            $show_agent_input = false;
            $agen_data = [$rowDesigner->agents->phone_number, $rowDesigner->agents->name];
        }

        if (is_null($rowDesigner)) return redirect()->route($this->indexRoute)->with('message', "Kost tidak tersedia");

        $MainKos = null;
        $isPhotoPreviouslyDuplicated = false;
        if (!is_null($rowDesigner->duplicate_from)) {
            $MainKos = Room::select('id', 'name')->where('id', $rowDesigner->duplicate_from)->first();
            $isPhotoPreviouslyDuplicated = true;
        }

        $kostParent = null;
        if (is_null($MainKos)) {
            $kostParent = Room::select('id', 'name')->where('duplicate_from', $rowDesigner->id)->get();
        }

        if ($rowDesigner == null) return abort(404);

        if ($request->is('admin/*')) {
            $currentRoute = 'admin.';
        } else {
            $currentRoute = 'kost-admin.';
        }
        // User
        $rowsUser = User::take(10)->get();
        $rowsUserView = mySelectPreprocessing(
            'single',
            'selected',
            'key-value',
            $rowsUser,
            $rowDesigner->user_id
        );

        // Address
        $rowDesigner->coordinate = null;
        if ($rowDesigner->latitude !== null) {
            $rowDesigner->coordinate = $rowDesigner->latitude . ',' . $rowDesigner->longitude;
        }

        // Tag
        $rowDesigner->concern_ids  = RoomTag::where('designer_id', '=', $id)->pluck('tag_id');

        $rowsConcern = Tag::where('type', '<>', 'fac_project')->get();
        $rowsConcernView = mySelectPreprocessing(
            'multi',
            'selected',
            'key-value',
            $rowsConcern,
            $rowDesigner->concern_ids
        );

        $rowsGender = array('male', 'female');
        $rowsGenderView = mySelectPreprocessing(
            'single',
            'checked',
            'value',
            $rowsGender,
            $rowDesigner->gender
        );

        $pricingusd = false;
        if (isset($rowDesigner->room_price_type) AND $rowDesigner->room_price_type->is_active == 1) {
            $pricing = [
                "price_monthly" => !is_null($rowDesigner->getDiscountRealPrice('monthly_usd')) ? $rowDesigner->getDiscountRealPrice('monthly_usd') : $rowDesigner->price_monthly_usd,
                "price_weekly"  => !is_null($rowDesigner->getDiscountRealPrice('weekly_usd')) ? $rowDesigner->getDiscountRealPrice('weekly_usd') : $rowDesigner->price_weekly_usd,
                "price_daily" => !is_null($rowDesigner->getDiscountRealPrice('daily_usd')) ? $rowDesigner->getDiscountRealPrice('daily_usd') : $rowDesigner->price_daily_usd,
                "price_yearly" => !is_null($rowDesigner->getDiscountRealPrice('annually_usd')) ? $rowDesigner->getDiscountRealPrice('annually_usd') : $rowDesigner->price_yearly_usd,
            ];
            $pricingusd = true;
        }

        if (!$pricingusd) {
            $pricing = [
                "price_yearly" => !is_null($rowDesigner->getDiscountRealPrice('annually')) ? $rowDesigner->getDiscountRealPrice('annually') : $rowDesigner->price_yearly,
                "price_monthly" => !is_null($rowDesigner->getDiscountRealPrice('monthly')) ? $rowDesigner->getDiscountRealPrice('monthly') : $rowDesigner->price_monthly,
                "price_weekly" => !is_null($rowDesigner->getDiscountRealPrice('weekly')) ? $rowDesigner->getDiscountRealPrice('weekly') : $rowDesigner->price_weekly,
                "price_daily" => !is_null($rowDesigner->getDiscountRealPrice('daily')) ? $rowDesigner->getDiscountRealPrice('daily') : $rowDesigner->price_daily
            ];
        }

        if (is_null($rowDesigner->apartment_project_id) or $rowDesigner->apartment_project_id == 0) {
            $isApartment = false;
        } else {
            $isApartment = true;
        }

         //$landingsSuggestion = AreaBigMapper::where('area_city', 'like', '%'.$request->area_city.'%')->take(10)->get();
        $viewData = array(
            'boxTitle'    => 'View or Edit Stories',
            'rowDesigner' => $rowDesigner,
            'isApartment' => $isApartment,
            'show_agent_input' => $show_agent_input,
            'other_price' => [
                '3_month' => $rowDesigner->price_quarterly,
                '6_month' => $rowDesigner->price_semiannually
            ],
            'agent_data' => $agen_data,
            'showBtnDuplicatePhoto' => $showPhotoDuplicate,
            'mainKos'     => $MainKos,
            'kostParent'  => $kostParent,
            'rowsUser'    => $rowsUserView,
            'rowsConcern' => $rowsConcernView,
            'manager_phone' => SMSLibrary::validateDomesticIndonesianMobileNumber($rowDesigner->manager_phone),
            'owner_phone' => SMSLibrary::validateDomesticIndonesianMobileNumber($rowDesigner->owner_phone),
            'rowsGender'  => $rowsGenderView,
            'formAction'  => array("{$currentRoute}room.update", $id),
            'formMethod'  => 'PUT',
            'pricing' => $pricing,
            'price_type' => $pricingusd,
            'data_all'    => $request->all(),
            'status_agent'=> $this->inputTypes,
            'formCancel'  => URL::route("{$currentRoute}room.index"),
            'typeOptions' => ['1-Room Studio', '1 BR', '2 BR', '3 BR', '4 BR', 'Lainnya'],
            'furnishedOptions' => ApartmentProject::UNIT_CONDITION,
            'isPhotoPreviouslyDuplicated' => $isPhotoPreviouslyDuplicated,
           // 'landingsSuggestion' => $landingsSuggestion
        );

        return view("admin.contents.stories.form", $viewData);
    }

    /**
     * Room destroy function
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function destroy(Request $request,$id)
    {
        $room = Room::find($id);
        if (is_null($room)) {
            return redirect()->route('admin.room.index')->with('message', 'Room not found');
        }

        try {
            $deactivate = $room->deactivate("admin");
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
        }

        if ($deactivate !== true) {
            $message = $deactivate;
        } else {
            $message = 'Room has been succesfully deleted';
            ActivityLog::LogDelete(Action::KOST, $id);
        }

        return redirect()->route('admin.room.index')->with('message', $message);
    }

    public function redirectkost(Request $request, $id){
        //$designer_id = Room::find($id)->song_id;
        //$aa = HistorySlug::where('designer_id',$designer_id)->get();

        $viewData = array(
            'boxTitle'        => 'Edit Redirect Slug',
            'login_type'      => $this->login_type,
            'createRoute'     => $this->createRoute,
            'editRoute'       => $this->editRoute,
            'deleteAction'    => $this->deleteRoute,
            'searchUrl'       => url()->route($this->indexRoute),
            'startDate'       => $request->input('st'),
            'endDate'         => $request->input('en'),
            'redirectHere'    => \Request::url() . '?' . http_build_query($_GET),
            'sortLists'       => $this->getSortList(),
            'inputBy'         => $this->inputTypes,
            'actionnya'       => $this->updateRedirect,
            'designer_id'     => $id
        );

        return view('admin.contents.stories.changeredirectid',$viewData);
    }

    public function redirectkostpost(Request $request){
        $designer_id_lama = $request->input('designer_id_lama');
        $slu = HistorySlug::where('designer_id', $designer_id_lama)->get();
        foreach($slu AS $hh){
        $slug = HistorySlug::find($hh->id);
        $slug->designer_id = $request->input('designer_id');
        $slug->save();
        }
        return redirect()->route('admin.room.index')->with('message', 'Room has been succesfully redirect slug');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        ini_set('max_execution_time', 300);

        $validator = validator($request->all(), array(
            'name' => 'required',
            'address' => 'required',
            'longitude' => 'required',
            'latitude' => 'required',
            'area_city' => 'required',
            'price_daily' => 'numeric',
            'price_weekly' => 'numeric',
            'price_monthly' => 'required|numeric',
            'price_yearly' => 'numeric',
            'room_count' => 'required|integer|lte:' . Room::MAX_ROOM_COUNT,
        ), $this->validationMessages);

        $rowDesigner = Room::with(['agents'])->where('id', $id)->first();
        $validator->after(function ($validator) use ($request, $rowDesigner) {
            $nominalLength = 4;
            if (
                $request->filled('price_type')
                && $request->input('price_type') === 'usd'
            ) {
                $priceType = 'usd';
                $nominalLength = 1;
            }

            if ($request->input('price_monthly') > 0 && strlen($request->input('price_monthly')) < $nominalLength) {
                $validator->errors()->add('price_monthly', 'Harga Sewa Bulanan harus minimal '.$nominalLength.' karakter');
            }

            if ($request->input('price_daily') > 0 && strlen($request->input('price_daily')) < $nominalLength ) {
                $validator->errors()->add('price_daily', 'Harga Sewa Harian harus minimal '.$nominalLength.' karakter');
            }

            if ($request->input('price_weekly') > 0 && strlen($request->input('price_weekly')) < $nominalLength ) {
                $validator->errors()->add('price_weekly', 'Harga Sewa Mingguan harus minimal '.$nominalLength.' karakter');
            }

            if ($request->input('price_yearly') > 0 && strlen($request->input('price_yearly')) < $nominalLength) {
                $validator->errors()->add('price_yearly', 'Harga Sewa Tahunan harus minimal '.$nominalLength.' karakter');
            }

            if (
                $request->input('price_yearly') === 0
                && $request->input('price_monthly') === 0
                && $request->input('price_weekly') === 0
                && $request->input('price_daily') === 0
            ) {
                $validator->errors()->add('price', 'Harga sewa harus diisi salah satu');
            }

            if (is_null($rowDesigner)) {
                $validator->errors()->add('name', 'Room tidak ditemukan.');
            } else {
                $roomAvailable  = $request->input('room_available');
                $roomCount      = $request->input('room_count');
                if ($roomAvailable > $roomCount) {
                    $validator->errors()->add('room_available', 'Jumlah kamar tersedia tidak boleh melebihi jumlah kamar total');
                } else {
                    $activeContract         = $rowDesigner->getBookedAndActiveContract();
                    $activeContractCount    = $activeContract->count();
                    if ($roomAvailable > ($roomCount - $activeContractCount)) {
                        $validator->errors()->add('room_available', 'Total kamar terisi tidak boleh kurang dari total kamar dengan kontrak');
                    }
                }
            }
        });

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        if ($request->input('act') === 'duplikat') {
            return $this->duplicate($request->all());
        }

        if ($request->input('act') === 'duplikat_photo') {
            return $this->duplicateWithPhoto($request->all(), $id);
        }

        // replicate the original one, to perform multiple checks
        $originalDesigner = $rowDesigner->replicate();

        // Save history slug before get updated
        $rowDesigner->archiveOldSlug();

        // Get user selected
        $userId = $request->get('user_id');
        if ($userId == 'Select a User') {
            $user = Auth::user()->id;
        } else {
            $user = $userId;
        }

        //get input
        if ($request->filled('price_type') && $request->input('price_type') === 'usd') {
            $usdPrice = Cache::get("currencyusd");

            if (is_null($usdPrice)) {
                $usdPrice = Currency::getCurrencyData('usd');
            }

            $priceMonthly = $request->input('price_monthly') * $usdPrice;
            $priceWeekly = $request->input('price_weekly') * $usdPrice;
            $priceYearly = $request->input('price_yearly') * $usdPrice;
            $priceDaily = $request->input('price_daily') * $usdPrice;

            $priceMonthlyUsd = $request->input('price_monthly');
            $priceWeeklyUsd = $request->input('price_weekly');
            $priceYearlyUsd = $request->input('price_yearly');
            $priceDailyUsd = $request->input('price_daily');

            $priceType = true;
        }

        if (!is_numeric($request->input('room_available'))) {
            $roomAvailable = 0;
        } else {
            $roomAvailable = abs($request->input('room_available'));
        }

        if (!is_numeric($request->input('room_count'))) {
           $roomCount = 0;
        } else {
           $roomCount = $request->input('room_count');
        }

        $areaCity = NULL;
        $areaBig = NULL;
        if (!empty($request->input('area_big'))) {
            $areaBig = AreaFormatter::format($request->input('area_big'));
        }

        if (!empty($request->input('area_city'))) {
            $areaCity = AreaFormatter::format($request->input('area_city'));
        }

        $areaSubdistrict = NULL;
        if (!empty($request->input('area_subdistrict'))) {
           $areaSubdistrict =  AreaFormatter::format($request->input('area_subdistrict'));
        }

        $agentStatus = null;
        if ($request->input('agent_status') == 'Semua') {
            $agentStatus = null;
        } else {
            $agentStatus = $request->input('agent_status');
        }

        if (!is_null($rowDesigner->apartment_project_id) and $rowDesigner->apartment_project_id > 0) {
            $rowDesigner->unit_type = $request->input('unit_type') != 'Lainnya' ? $request->input('unit_type') : $request->input('unit_type_name');
            $rowDesigner->unit_number = $request->input('unit_number');
            $rowDesigner->furnished = (int) $request->input('is_furnished');
        }

        $rowDesigner->name          = $request->input('name');
        $rowDesigner->size          = $request->input('size');
        $rowDesigner->gender        = $request->input('gender');
        $rowDesigner->floor         = $request->input('floor');
        $rowDesigner->price_monthly = $request->input('price');
        $rowDesigner->address       = $request->input('address');
        $rowDesigner->longitude     = $request->input('longitude');
        $rowDesigner->latitude      = $request->input('latitude');

        $rowDesigner->price_daily = isset($priceDaily) ? $priceDaily : $request->input('price_daily');
        $rowDesigner->price_weekly = isset($priceWeekly) ? $priceWeekly : $request->input('price_weekly');
        $rowDesigner->price_monthly = isset($priceMonthly) ? $priceMonthly : $request->input('price_monthly');
        $rowDesigner->price_yearly = isset($priceYearly) ? $priceYearly : $request->input('price_yearly');
        $rowDesigner->price_quarterly = $request->input('3_month');
        $rowDesigner->price_semiannually = $request->input('6_month');

        $rowDesigner->price_daily_usd = isset($priceDailyUsd) ? $priceDailyUsd : null;
        $rowDesigner->price_weekly_usd = isset($priceWeeklyUsd) ? $priceWeeklyUsd : null;
        $rowDesigner->price_monthly_usd = isset($priceMonthlyUsd) ? $priceMonthlyUsd : null;
        $rowDesigner->price_yearly_usd = isset($priceYearlyUsd) ? $priceYearlyUsd : null;

        $rowDesigner->description = trim(strip_tags($request->input('description')));
        $rowDesigner->fac_room_other = substr($request->input('fac_room_other'), 0, 500);
        $rowDesigner->fac_bath_other = substr($request->input('fac_bath_other'), 0, 500);
        $rowDesigner->fac_share_other = substr($request->input('fac_share_other'), 0, 500);
        $rowDesigner->fac_near_other = substr($request->input('fac_near_other'), 0, 500);
        $rowDesigner->remark = $request->input('remarks');
        $rowDesigner->price_remark = substr($request->input('price_remark'), 0, 500);
        $rowDesigner->room_count = $roomCount;
        $rowDesigner->status = $request->input('status');
        $rowDesigner->remark = $request->input('remarks');
        $rowDesigner->admin_remark = $request->input('admin_remark');
        $rowDesigner->user_id = $user;
        $rowDesigner->animal = $request->input('animal');

        if ($rowDesigner->agent_id < 1) {
            $rowDesigner->agent_name = $request->input('agent_name');
            $rowDesigner->agent_email = $request->input('agent_email');
            $rowDesigner->agent_phone = $request->input('agent_phone');
        }

        $rowDesigner->verificator = $request->input('verificator');
        $rowDesigner->owner_name = $request->input('owner_name');
        $rowDesigner->owner_phone = $request->input('owner_phone');
        $rowDesigner->manager_name = $request->input('manager_name');
        $rowDesigner->manager_phone = $request->input('manager_phone');
        $rowDesigner->area_subdistrict = $areaSubdistrict;
        $rowDesigner->area_city = $areaCity;

        if (
            $originalDesigner->area_big === $request->input('area_big')
            && ($originalDesigner->area_city != $request->input('area_city'))
        ) {
            $rowDesigner->area_big = $areaBig;
        } else {
            $rowDesigner->area_big = AreaFormatter::format($request->input('area_big'));
        }

        $rowDesigner->office_phone = $this->getOfficePhone($request->input('area_city'));
        $rowDesigner->kost_updated_date = date('Y-m-d H:i:s');
        $rowDesigner->room_available = $roomAvailable;
        $rowDesigner->youtube_id = $request->input('youtube_id');
        $rowDesigner->agent_status = $agentStatus;
        $rowDesigner->guide        = $request->input('guide');
        $rowDesigner->is_mamirooms = $request->input('mamirooms');
        $rowDesigner->is_testing   = $request->input('testing');

        // Room Class
        $rowDesigner->class = $rowDesigner->calculateClass($request->get('concern_ids'));

        if ($rowDesigner->is_indexed == 0 && $rowDesigner->apartment_project_id > 0) {
            $checkIndexedRoom = Room::indexedStatus($rowDesigner, $request->all());
            if (!$checkIndexedRoom) $rowDesigner->is_indexed = 1;
        }

        if (is_null($rowDesigner->apartment_project_id)) {
            $rowDesigner->is_indexed = 1;
        }

        if ($request->filled('indexed') && $rowDesigner->apartment_project_id == 0) {
            $rowDesigner->is_indexed = 1;
        } else if ($rowDesigner->apartment_project_id == 0) {
            $rowDesigner->is_indexed = 0;
        }

        // Store class history
        $rowDesigner->storeClassHistory("admin-update-room");

        $rowDesigner->updateSortScore();
        $isSuccess = $rowDesigner->save();

        // If there's any active discount,
        // then recalculate discount
        $rowDesigner->recalculateAllActiveDiscounts();

        if ($request->filled('price_type')) $priceType = $request->input('price_type');
        else $priceType = 'idr';
        RoomPriceType::updateData($rowDesigner, $priceType);

        if (strlen(trim($request->input('agent_note'))) > 0) {
            $reason = ListingReason::where('listing_id', $rowDesigner->id)->where('from', 'room')->first();

            if (is_null($reason)) {
                $reason = new ListingReason();
                $reason->from = "room";
                $reason->listing_id = $rowDesigner->id;
                $reason->content    = $request->input('agent_note');
                $reason->save();
            } else {
                $reason->content = $request->input('agent_note');
                $reason->save();
            }
        }

        // this for the purpose of set all reply status of this designer to false
        // so they will be sent an update chat
        Call::unrepliedCallStatus($rowDesigner->id);

        // storing tag
        $designerTag = $request->get('concern_ids');
        if ($designerTag != null) {
            RoomTag::where('designer_id', $id)->delete();

            foreach ($designerTag as $tag) {
                RoomTag::store($tag, $id);
            }
        }

        /* Set Kost level "Mamirooms" */
        if ($rowDesigner->is_mamirooms) {
            KostLevelValueHelper::assignMamiroomsLevelToKos($rowDesigner);
        } else {
            $level = $rowDesigner->level_info;

            // if previous state is_mamirooms = true
            // then we update to is_mamirooms = false
            // then we need to delete kost level mamirooms
            if (
                !is_null($level)
                && $level['name'] == KostLevelValueHelper::STRING_MAMIROOMS_1
                && $originalDesigner->is_mamirooms
            ) {
                $rowDesigner->level()->detach();
            }
        }
        IndexKostJob::dispatch($id);

        // Revert Thanos
        try {
            $thanos = app()->make(ThanosService::class);
            $thanos->revert($rowDesigner->id, "bang.kerupux", \Illuminate\Support\Facades\Request::ip());
        } catch (Exception $exception) {
        }

        if ($request->input('act') == 'just_photo') return $this->photosDuplicateAction($rowDesigner);

        if ($isSuccess) {
            ActivityLog::LogUpdate(Action::KOST, $id);
            return Redirect::route('admin.room.show', $rowDesigner->id)->with('message','Data kost berhasil diupdate');
        } else {
            return route($this->editRoute, $rowDesigner->id)->with('error_message','Data kost gagal diupdate');
        }
    }

    public function duplicate($roomData, $for = null)
    {
        $room = array_only($roomData, [
                    "name", "apartment_project_id","description","address","gender","photo_id", "room_count",
                    "latitude","longitude","size","extend","price","photo_id",
                    "owner_name","owner_phone","manager_name","manager_phone","phone","office_phone",
                    "area","resident_remark","price_daily","price_weekly","price_monthly","price_yearly","price_remark",
                    "fac_room","fac_room_other","fac_bath","fac_bath_other","fac_share","fac_share_other","fac_near",
                    "fac_near_other","fac_parking","payment_duration","remark","description","agent_name",
                    "agent_phone","agent_status","agent_email","area_city","area_subdistrict","status",
                    "floor","deposit","animal", "room_available"]);

        if (empty($room['apartment_project_id'])) $room['apartment_project_id'] = null;

        $room['deposit'] = 0;
        //$room['name'] = $room['name'] ." Duplicate";

        if (!$room['price_daily'] || $room['price_daily'] == '-' ) {
            $room['price_daily'] = 0;
        }
        if (!$room['price_monthly'] || $room['price_monthly'] == '-' ) {
            $room['price_monthly'] = 0;
        }
        if (!$room['price_weekly'] || $room['price_weekly'] == '-' ) {
            $room['price_weekly'] = 0;
        }
        if (!$room['price_yearly'] || $room['price_yearly'] == '-' ) {
            $room['price_yearly'] = 0;
        }

        $data = array(
            'Kabupaten' => "",
            'Kota'      => "",
            'Kecamatan' => ""
            );

        if (!empty($room['area_city'])) {
          $room['area_city'] = AreaFormatter::format($room['area_city']);
        }

        if (!empty($room['area_subdistrict'])) {
           $room['area_subdistrict'] =  AreaFormatter::format($room['area_subdistrict']);
        }

        $room['price_daily'] = intval($room['price_daily']);
        $room['price_monthly'] = intval($room['price_monthly']);
        $room['price_weekly'] = intval($room['price_weekly']);
        $room['price_yearly'] = intval($room['price_yearly']);

        $newRoom = Room::create($room);
        $newRoom->generateSongId();

        if (!empty($roomData['concern_ids'])) {
            $tagIds = (array) $roomData['concern_ids'];

            if (! empty($tagIds)) {
                $newRoom->attachTags($tagIds);
            }
        }

       // $this->addCards($roomData['cards'], $newRoom->id);
        if ($for == 'photos') return $newRoom;
        return redirect('admin/room');
    }

    public function duplicateWithPhoto($roomData, $id)
    {
       $room          = Room::with(['apartment_project'])->find($id);

       /*  if (!is_null($room->duplicate_from)) {
           return Redirect('/admin/room')->with('message','Kos tidak bisa diduplikate');
        }*/

        $roomDuplicate = $this->duplicate($roomData, 'photos');
/*      $mediaIds      = $room->cards()->get();

        $medias        = Media::whereIn('id', $mediaIds->pluck('photo_id')->toArray())->get();

        $sizeNames     = Media::sizeByType('style_photo');
        $coverId       = 0;
        $mediaWithDescription = $mediaIds->pluck('description', 'photo_id')->toArray();

        $roomNameNew = $roomDuplicate->slug;
        $roomSlug    = $room->slug;
        $roomName    = $room->name;

        foreach($medias as $media) {
            $newMedia = $this->photosDuplicate($media);

            if(is_null($newMedia)) {
                continue;
            }

            $description = $mediaWithDescription[$media->id];
            if (strpos($description, 'bangunan-tampak-depan-cover') !== false) {
                $description = $roomNameNew.'-bangunan-tampak-depan-cover';
                $coverId     = $newMedia['id'];
            } else if (strpos($description, 'bangunan-ruang-utama') !== false) {
                $description = $roomNameNew.'-bangunan-ruang-utama';
            } else if (strpos($description, 'dalam-kamar') !== false) {
                $description = $roomNameNew.'-dalam-kamar';
            } else if (strpos($description, 'dalam-kamar-mandi') !== false) {
                $description = $roomNameNew.'-dalam-kamar-mandi';
            } else {
                $hideOldDescription = str_replace($roomSlug, "", $description);
                $hideOldDescription = str_replace($roomName, "", $description);
                $description        = $roomNameNew."-".$hideOldDescription;
            }

            $Style              = new Card();
            $Style->designer_id = $roomDuplicate->id;
            $Style->description = $description;
            $Style->photo_id    = $newMedia['id'];
            $Style->save();

        }*/

            $roomNow                        = Room::find($roomDuplicate->id);
            $roomNow->apartment_project_id  = $room->apartment_project_id;
            $roomNow->unit_type             = $room->unit_type;
            $roomNow->floor                 = $room->floor;
            $roomNow->unit_number           = $room->unit_number;
            $roomNow->furnished             = $room->furnished;

           /* if ($coverId != 0) {
               $roomNow->photo_id       = $coverId;
            }*/
            $roomNow->duplicate_from = $id;
            $roomNow->save();

            if ($roomNow->apartment_project_id > 0) {
                $roomNow->generateApartmentUnitCode();
                $this->priceComponentInput($roomNow, $room);
            }
        //return redirect('admin/card/'.$roomDuplicate->id);
            return redirect()->route($this->editRoute, $roomDuplicate->id);
    }

    /**
     * Duplicating the photos of certain Room.
     *
     * @param int $id Room id (the duplicated one).
     */
    public function photosDuplicateAction(Room $room, ?Room $sourceRoom = null)
    {
        if (is_null($room->duplicate_from)) {
            return redirect('/admin/room')->with('message', 'Kos tidak bisa diduplikate');
        }

        if (is_null($sourceRoom)) {
            $sourceRoom = Room::with('cards')->where('id', $room->duplicate_from)->first();
        }

        $service = app()->make(DuplicateService::class);

        $service->replicateKostPhoto($room, $sourceRoom);
        $service->replicateRoomPhoto($room, $sourceRoom);
        $service->replicateCoverPhoto($room, $sourceRoom);

        return redirect('admin/card/'.$room->id);
    }

    public function photosDuplicate(Media $media)
    {
        $originalFound = false;
        $originalFile = '';
        $hasWatermark = false;
        $theFile = '';

        // get real image from /uploads folder
        $originalFilePath = $media->file_path . '/' . $media->file_name;

        if(Storage::exists($originalFilePath)) {
            $originalFound = true;
            $theFile = Storage::get($originalFilePath);
        }

        // if real image not present get the already converted image
        if(!$originalFound) {
            $sizeNames = Media::sizeByType('style_photo');

            list($width, $height) = explode('x', $sizeNames['large']);
            $largeTypePhoto = Media::getCachePath($media, $width, $height);

            if(Storage::exists($largeTypePhoto)) {
                $originalFound = true;
                $hasWatermark = true;
                $theFile = Storage::get($largeTypePhoto);
            }
        }

        // if no file found, then skip resizing
        if(!$originalFound) {
            return;
        }

        $originalExtension = pathinfo($originalFilePath, PATHINFO_EXTENSION);

        return Media::postMedia($theFile, 'upload', null, null, null, 'style_photo', $hasWatermark ? false : true, null, false,
            [
                'original_extension'=>$originalExtension
            ]);
    }

    /**
     *  Allow arbitrary number of kost duplicate to be created
     *
     *  @param Request $request
     *  @param int $id Id of kost to be duplicated
     *
     *  @return JsonResponse
     */
    public function createRoomDuplicates(Request $request, int $id): JsonResponse
    {
        $room = Room::with(['apartment_project', 'owners'])->find($id);

        if (is_null($room)) {
            return ApiResponse::responseData(['status' => false]);
        }

        $duplicateToCreate = (int) $request->duplicate_count;

        if ($duplicateToCreate > self::ROOM_DUPLICATE_UPPER_LIMIT) {
            return ApiResponse::responseData([
                'status' => false,
                'meta' => [
                    'message' => __('api.room.duplicate.error.more_than_limit', ['limit' => self::ROOM_DUPLICATE_UPPER_LIMIT])
                ]
            ]);
        } elseif ($duplicateToCreate < self::ROOM_DUPLICATE_LOWER_LIMIT) {
            return ApiResponse::responseData([
                'status' => false,
                'meta' => [
                    'message' => __('api.room.duplicate.error.less_than_limit', ['limit' => self::ROOM_DUPLICATE_LOWER_LIMIT])
                ]
            ]);
        }

        $isWithPhoto = $request->is_with_photo;
        $duplicates = [];

        $roomData = $room->toArray();
        $roomData['concern_ids'] = $room->tags()->select('tag.id as tag_id')->get()->makeHidden('pivot')->toArray();

        DB::beginTransaction();

        for ($i = 0; $i < $duplicateToCreate; $i++) {
            if (!$isWithPhoto) {
                $roomData['photo_id'] = null;
            }

            $roomDuplicate = $this->duplicate($roomData, 'photos');
            $roomDuplicate->area_big = $room->area_big;
            $roomDuplicate->guide = $room->guide;
            
            if (is_null($room->apartment_project_id)) {
                $roomDuplicate->property_id = $room->property_id;
                $roomDuplicate->unit_type = $room->unit_type;
            }
            
            if ($isWithPhoto) {
                $roomDuplicate->apartment_project_id = $room->apartment_project_id;
                $roomDuplicate->unit_type = $room->unit_type;
                $roomDuplicate->floor = $room->floor;
                $roomDuplicate->unit_number = $room->unit_number;
                $roomDuplicate->furnished = $room->furnished;
                $roomDuplicate->duplicate_from = $id;
                $roomDuplicate->save(); // Need to save here because @photosDuplicateAction will refetch the room data and check duplicated_from info.

                $this->photosDuplicateAction($roomDuplicate, $room);
            }

            $roomDuplicate->name = ($roomDuplicate->name . ' duplikat ' . (string) ($i + 1));
            $this->duplicateRoomOwner($room, $roomDuplicate);

            $roomDuplicate->save();

            $duplicates[] = [
                'id' => $roomDuplicate->id,
                'name' => $roomDuplicate->name
            ];

            if ($roomDuplicate->apartment_project_id > 0) {
                $roomDuplicate->generateApartmentUnitCode();
                $this->priceComponentInput($roomDuplicate, $room);
            }
        }
        DB::commit();

        return ApiResponse::responseData(['status' => true, 'duplicates' => $duplicates]);
    }

    /**
     *  Bulk room verification for duplicate kost feature
     *
     *  @param Request $request
     *
     *  @return JsonResponse
     */
    public function bulkVerify(Request $request): JsonResponse
    {
        $toVerify = $request->to_verify;
        foreach ($toVerify as $id) {
            $this->verify($id);
        }

        return ApiResponse::responseData(['status' => true]);
    }

    /**
     *  Bulk room just verify for duplicate kost feature
     *
     *  @param Request $request
     *
     *  @return JsonResponse
     */
    public function bulkJustVerify(Request $request): JsonResponse
    {
        $toVerify = $request->to_verify;
        foreach ($toVerify as $id) {
            $this->justVerifyRoom($id);
        }

        return ApiResponse::responseData(['status' => true]);
    }

    /**
     *  Bulk room slug verification for duplicate kost feature
     *
     *  @param Request $request
     *
     *  @return JsonResponse
     */
    public function bulkVerifySlug(Request $request): JsonResponse
    {
        $toVerify = $request->to_verify;
        foreach ($toVerify as $id) {
            Room::verifySlugDesigner($id);
        }

        return ApiResponse::responseData(['status' => true]);
    }

    /**
     * @param Room $room
     * @param $photosId
     */
    public function addPhotosToRoom(Room $room, $photosId)
    {
        $photoCount = 0;

        foreach ($photosId as $category => $value) {
            if ($value != null && $value != 0) {
                $photoCategoryCount = $this->addPhotoByCategory($room, $category, $value);
                $photoCount += $photoCategoryCount;
            }
        }

        return $photoCount;
    }

    /**
     * @param Room $room
     * @param $category
     * @param $photoId
     */
    public function addPhotoByCategory(Room $room, $category, $photoId)
    {
        $photoDescriptionSuffix = [
            'cover'=>'bangunan-tampak-depan-cover',
            'bangunan' => 'bangunan-ruang-utama',
            'kamar' => 'dalam-kamar',
            'kamar-mandi' => 'dalam-kamar-mandi'
        ];

        $photoCount = 0;

        foreach ($photoId as $id) {
            $media = Media::where('id', $id)->first();
            if (is_null($media)) {
                continue;
            }
            $newCard = new Card;
            $newCard->designer_id = $room->id;

            $description = isset($photoDescriptionSuffix[$category]) ? $room->name . '-' . $photoDescriptionSuffix[$category] : $category;
            $newCard->description = $description;

            $newCard->type = 'image';
            $newCard->photo_id = $id;
            $newCard->sub_type = 'text';
            $newCard->save();

            $photoCount++;

            if ($category == 'cover' && $id != null) {
                $room->photo_id = $id;
                $room->save();
            } else if ($category == 'round') {
                $room->photo_round_id = $id;
                $room->save();
            }
        }

        return $photoCount;
    }

    /**
     * Get office phone number based on area city
     * @return mixed|string
     */
    public function getOfficePhone($areaCity)
    {
        $restrictedString = array('Kota ','Kabupaten ');
        $cityName = str_replace($restrictedString, "", $areaCity);

        /**
         * Changes : 087839647533 -> 087705466177
         * 			 087839647538 -> 087705466166
         */
        $phoneArray = array(
            "Yogyakarta"    => "087839647530",
            "Sleman"        => "087705466177",
            "Bantul"        => "087705466166",
            "Depok"         => "087705466177",
            "Jakarta Barat" => "087705466177",
            "Jakarta Pusat" => "087839647530",
            "Jakarta Selatan" => "087705466166",
            "Jakarta Timur" => "087839647530",
            "Jakarta Utara" => "087705466166"
        );

        if (! isset($phoneArray[$cityName])) {
            $phoneNumber = "087839647530";
        } else {
            $phoneNumber = $phoneArray[$cityName];
        }

        return $phoneNumber;
    }

    public function getInfoForCS(Request $request, $songId)
    {
        $user = Auth::user();

        if (is_null($user) ||
            $user->role != UserRole::Administrator) {
            return Api::responseData([
                'status'=>false,
                'meta'=>[
                    'message'=>'Log In Required'
                ]
            ]);
        }

        $room = Room::with(['tags', 'minMonth'])
                    ->select(['id', 'song_id', 'name', 'address', 'manager_name', 'manager_phone',
                            'owner_name', 'owner_phone', 'kost_updated_date', 'room_available',
                            'latitude', 'longitude', 'price_daily', 'price_weekly', 'price_monthly', 'price_yearly', 'slug'])
                    ->where('song_id', $songId)
                    ->first();

        if(!$room) {
            return Api::responseData([
                'status'=>false,
                'meta'=>[
                    'message'=>'Kamar tidak ditemukan'
                ]
            ]);
        }

        $tagIds = $room->tags->pluck('id')->toArray();

        $owners = $room->owners;
        $owner = null;
        if(count($owners) > 0) {
            $owner = $owners->first();
        }

        return Api::responseData([
            'room'=>[
                'name'=>$room->name,
                'address'=>$room->address,
                'manager_name'=>$room->manager_name,
                'manager_phone' => $room->manager_phone,
                'owner_name'=> $room->owner_name,
                'owner_phone' => $room->owner_phone,
                'owner_account_phone' => !is_null($owner) ? $owner->user->phone_number : '',
                'last_update' => date('Y-m-d H:i:s', strtotime($room->kost_updated_date)),
                'room_available' => $room->room_available,
                'latitude'=>$room->latitude,
                'longitude'=>$room->longitude,
                'price_daily'=>$room->price_daily != 0 ? number_format($room->price_daily, 0, ',', '.') : 0,
                'price_weekly'=>$room->price_weekly != 0 ? number_format($room->price_weekly, 0, ',', '.') : 0,
                'price_monthly'=>$room->price_monthly != 0 ? number_format($room->price_monthly, 0, ',', '.') : 0,
                'price_yearly'=>$room->price_yearly != 0 ? number_format($room->price_yearly, 0, ',', '.') : 0,
                'map_link'=>'https://maps.google.com/maps?z=7&q=' . $room->latitude . ','. $room->longitude,
                'room_link'=>$room->share_url,
                'couple_allowed'=>in_array(60, $tagIds) ? 'ya' : '-',
                'double_allowed'=>in_array(76, $tagIds) ? 'ya' : '-',
                'min_month'=>$room->minMonth,
                'is_booking' => $room->is_booking == 1 ? "Bisa" : "Tidak",
            ]
        ]);
    }

    public function availableName(Request $request)
    {
        $roomId = $request->input('room');
        $nameProposed = $request->input('name');

        $currentRoom = null;
        if(!is_null($roomId)) {
            $currentRoom = Room::find($roomId);
        }

        $query = Room::where('name', $nameProposed);

        if($currentRoom) {
            $query = $query->where('id', '<>', $roomId);

            if(!is_null($currentRoom->apartment_project_id)) {

                return Api::responseData([
                    'available' => true
                ]);
            }
        }

        $room = $query->first();

        return Api::responseData([
            'available' => is_null($room) ? true : false,
        ]);
    }

    public function renameParser(Request $request)
    {
        $viewData   = [
            'boxTitle'          => 'Parse Room to be renamed',
        ];

        return view('admin.contents.stories.rename-parser', $viewData);
    }

    public function renameParserPost(Request $request)
    {
        $this->validate($request, [
            'file_csv'=>'required|mimes:csv,txt'
        ]);

        $csv = $request->file_csv->path();

        $csvArray = CSVParser::csv_parse($csv, ['eol'=>"\r\n"]);

        $failedRow = [];
        foreach($csvArray as $key=>$row) {

            try {
                $renamedRoom = RenameQueue::where('designer_id', $row['room_id'])->first();

                if($renamedRoom) {
                    $failedRow[] = 'Foto telah di-rename : ' . $row['room_id'] ;
                    continue;
                }

                $renameQueue = RenameQueue::insertQueue($row['room_id']);

            } catch (\Exception $e) {
                $failedRow[] = 'Gagal diimport : ' . $row['room_id'] ;
            }

        }

        return redirect()->back()
                        ->with(!empty($failedRow) ? 'errors' : 'message',
                            !empty($failedRow) ? collect($failedRow) : 'Sukses input data');
    }

    public function kostToApartmentMigrate(Request $request, $id)
    {
        $room = Room::find($id);

        if(!$room) {
            return redirect()->back()->with('error_message', 'Kamar tidak ditemukan');
        }

        $apartmentProjects = ApartmentProject::where('is_active', 1)->get();

        $unitTypeOptions = [
            '1-Room Studio',
            '1 BR',
            '2 BR',
            '3 BR',
            '4 BR',
            'Lainnya'
        ];

        $viewData   = [
            'boxTitle'          => 'Migrate kost to apartment',
            'room'               => $room,
            'apartmentProjects' => $apartmentProjects,
            'typeOptions'       => $unitTypeOptions,
            'furnishedOptions'  => ApartmentProject::UNIT_CONDITION
        ];

        return view('admin.contents.stories.migrate', $viewData);
    }

    public function updateKostToApartment(Request $request, $id)
    {
        $validator = validator($request->all(),
                        [
                            'name' => 'required',
                            'project_id' => 'required',
                            'unit_size' => 'required',
                            'unit_number' => 'required',
                            'unit_type' => 'required',
                            'unit_type_name' => 'nullable|required_if:unit_type,Lainnya|max:100',
                            'maintenance_price' => 'numeric',
                            'parking_price' => 'numeric'
                        ],
                        [
                            'name.required' => 'Nama harus diisi',
                            'project_id.required' => 'Apartment Project harus diisi',
                            'unit_size.required' => 'Ukuran Unit harus diisi',
                            'unit_type.required' => 'Tipe Unit harus diisi',
                            'unit_number.required' => 'Nomor Unit harus diisi',
                            'unit_type_name.required_if' => 'Nama Tipe Unit harus diisi',
                            'maintenance_price.numeric' => 'Biaya maintenance harus berupa angka',
                            'parking_price' => 'Biaya Parkir harus berupa angka'
                        ]);

        $validator->after(function($validator) use ($request, $id) {
            $existingUnitNumber = Room::where('unit_number', $request->unit_number)
                                    ->where('apartment_project_id', $request->project_id)
                                    ->where('id', '<>', $id)
                                    ->first();

            if($existingUnitNumber) {
                $validator->errors()->add('unit_number', 'Nomor Unit telah digunakan sebelumnya');
            }
        });

        if($validator->fails()) {
            return redirect()->back()->withInput()->with('errors', $validator->errors());
        }

        $room = Room::find($id);

        if(!$room) {
            return redirect()->back()->with('error_message', 'Kamar tidak ditemukan');
        }

        $apartmentProject = ApartmentProject::find($request->project_id);

        // archive slug, since it will be replaced to the new one
        $room->archiveOldSlug();

        $room->latitude = $apartmentProject->latitude;
        $room->longitude = $apartmentProject->longitude;
        $room->area_city = $apartmentProject->area_city;
        $room->area_subdistrict = $apartmentProject->area_subdistrict;
        $room->area_big = $apartmentProject->area_subdistrict;
        $room->name = $request->name;
        $room->apartment_project_id = $request->project_id;
        $room->size = $request->unit_size;
        $room->unit_type = $request->unit_type != 'Lainnya' ? $request->unit_type : $request->unit_type_name;
        $room->floor = $request->floor;
        $room->unit_number = $request->unit_number;
        $room->room_count = 1;
        $room->room_available = 1;
        $room->furnished = !is_null($request->is_furnished) ? $request->is_furnished : 0;

        $room->slug = SlugService::createSlug(Room::class, 'slug', $room->name_slug);

        $room->updateSortScore();
        $room->save();

        $room->generateApartmentUnitCode();


        $propertyInputRepository = new \App\Repositories\PropertyInputRepositoryEloquent(app());

        if($request->maintenance_price > 0) {
            $propertyInputRepository->savePriceComponent($room, \App\Entities\Room\Element\RoomPriceComponent::PRICE_NAME_MAINTENANCE, 'Biaya Maintenance', $request->maintenance_price);
        }

        if($request->parking_price > 0) {
            $propertyInputRepository->savePriceComponent($room, \App\Entities\Room\Element\RoomPriceComponent::PRICE_NAME_PARKING, 'Biaya Maintenance', $request->parking_price);
        }

        ActivityLog::LogUpdate(Action::KOST, $id);

        return redirect()->route('admin.room.index')->with('message', 'Kost berhasil di-migrasi ke apartment');
    }

    public function setMamirooms(Request $request, Room $room, $action) {
        try {
            $room->is_mamirooms = $action != 'unset';
            $room->kost_updated_date = Carbon::now();
            $room->save();

            if ($action == 'set') {
                /* Assign Kos Level "Mamirooms" */
                if (!KostLevelValueHelper::assignMamiroomsLevelToKos($room)) {
                    return redirect()->route('admin.room.index')->with('error', "Mamirooms level is not set!");
                }
            } else {
                /* Unassign Kos Level "Mamirooms" */
                $room->level()->detach();
                IndexKostJob::dispatch($room->id);
            }

            return redirect()->route('admin.room.index')->with('message', 'Successfully set as Mamirooms');
        } catch (Exception $exception) {
            return redirect()->route('admin.room.index')->with('error', $exception->getMessage());
        }
    }

    public function setTesting(Request $request, Room $room, $action) {
        if ($action == 'unset') {
            $room->is_testing = false;
        }
        else {
            $room->is_testing = true;
        }

        $room->kost_updated_date = Carbon::now();
        $room->save();

        return redirect()->route('admin.room.index');
    }

    public function getRooms(Request $request, RoomRepositoryEloquent $repository)
    {
        return $repository->getRoomsByNameOrId($request->input('search'));
    }

    /**
     * Duplicate the room owner of $room into $duplicateRoom
     *
     * @param Room $room. Parent room (the source)
     * @param Room $duplicateRoom. Duplicated room (the target)
     * @return void
     */
    private function duplicateRoomOwner(Room $room, Room $duplicateRoom)
    {
        $duplicatedRoomOwners = [];
        if (!empty($room->owners) && !is_null($duplicateRoom)) {
            foreach ($room->owners as $owner) {
                $roomOwner = new RoomOwner();
                $roomOwner->user_id = $owner->user_id;
                $roomOwner->owner_status = $owner->owner_status;
                $roomOwner->status = $owner->status;
                $duplicatedRoomOwners[] = $roomOwner;
            }
        }
        $duplicateRoom->owners()->saveMany($duplicatedRoomOwners);
    }

    /**
     *  Manual action to trigger recalculate room.sort_score without job queue
     *
     *  @param Request $request
     *  @param int $id Id of kost
     *
     *  @return JsonResponse
     */
    public function recalculateRoomScore(int $id): JsonResponse
    {
        $room = Room::find($id);

        if (is_null($room)) {
            return ApiResponse::responseData([
                'status' => false,
                'message' => 'kamar tidak ditemukan'
            ]);
        }

        LplScoreHelper::recalculateRoomScore($room);
        $criteriaTracking = LplScoreHelper::getCriteriaForTracking($room);
        $room = Room::select(['name', 'sort_score'])->find($id);

        return ApiResponse::responseData([
            'status'  => true,
            'name' => $room->name,
            'sort_score' => $room->sort_score,
            'tracking' => $criteriaTracking,
        ]);
    }

    /**
     *  Set kost status to ADD
     *  Used in page admin agents
     *
     *  @param int $roomId
     *  @return \Illuminate\Http\Response
     */
    public function setStatusAdd($roomId)
    {
        $room = Room::with('owners')->find($roomId);

        if (is_null($room)) {
            return redirect()->back()->with('error_message', 'Kos tidak ditemukan');
        }

        if ($room->is_booking) {
            return redirect()->back()->with('error_message', 'Kos sudah terdaftar Bisa Booking/BBK');
        }

        $owner = $room->owners->first();

        if (is_null($owner)) {
            return redirect()->back()->with('error_message', 'Kos tidak memiliki owner');
        }

        if ($owner->status != RoomOwner::ROOM_VERIFY_STATUS) {
            return redirect()->back()->with('error_message', 'Kos tidak aktif');
        }

        $owner->status = RoomOwner::ROOM_ADD_STATUS;
        $owner->save();

        $room->is_active            = 'false';
        $room->is_verified_phone    = 0;
        $room->is_verified_address  = 0;
        $room->save();

        $room->deleteFromElasticsearch();

        return redirect()->back()->with('message', 'Set status kos to ADD berhasil');
    }

    /**
     *  Get price filter of room
     *
     *  @param int $id Id of kost
     *  @return JsonResponse
     */
    public function getPriceFilter(int $id): JsonResponse
    {
        $room = Room::with('price_filter')->find($id);

        if (is_null($room)) {
            return ApiResponse::responseData([
                'status' => false,
                'message' => 'kamar tidak ditemukan'
            ]);
        }

        return ApiResponse::responseData([
            'status'  => true,
            'data' => $room->price_filter
        ]);
    }
}
