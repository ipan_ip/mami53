<?php 

namespace App\Http\Controllers\Admin;

use App\Entities\User\UserMedia;
use App\Http\Helpers\ApiHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Auth;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Entities\Mamipay\MamipayMedia;
use App\Entities\User\UserVerificationAccount;
use App\Entities\Activity\ActivityLog;
use App\Entities\Notif\SendNotif;
use App\Entities\Notif\NotifData;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Exception;
use App\Entities\Activity\Action;
use App\Libraries\Notifications\SendNotifWrapper;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

class UserIdentityCardController extends Controller  
{
    private $sendNotifWrapper;
	public function __construct(SendNotifWrapper $sendNotifWrapper)
    {
        View::share('contentHeader', 'User Identity Card');
        View::share('user', Auth::user());
        $this->sendNotifWrapper = $sendNotifWrapper;
    }

    public function getVerifyIdentityCard(Request $request)
    {
        $perPage = $request->get('per_page', 20);
        
        if (is_numeric($perPage) === false) {
            $perPage = 20;
        }

        $userVerificationsQuery = UserVerificationAccount::with(
            [
                'user',
                'user_medias'
            ])
            ->whereNotNull('identity_card')
            ->orderBy('updated_at', 'DESC');
        
        $userVerificationsQuery = $this->filterUserVerifications($request, $userVerificationsQuery);
        $userVerificationsQuery = $this->filterUser($request, $userVerificationsQuery);
        $userVerificationsQuery = $this->filterUserMedia($request, $userVerificationsQuery);

        $userVerifications = $userVerificationsQuery->paginate($perPage);

        $viewData = [
            'boxTitle' => 'Verification Account',
            'datas' => $userVerifications
        ];

        ActivityLog::LogIndex(Action::USER_VERIFICATION);

        return view('admin.contents.user.list-verification-identity', $viewData);
    }

    /**
     * For filtering query of user verifications. Will remove the date filter from request object if the range is not valid.
     *
     * @param Request $request
     * @param Builder $userVerificationsQuery
     * @return Builder
     */
    private function filterUserVerifications(Request $request, Builder $userVerificationsQuery): Builder
    {
        if (!empty($request->input('status'))) {
            $userVerificationsQuery->where('identity_card', $request->input('status'));
        }
        if (!empty($request->input('from_submit_date')) && !empty($request->input('to_submit_date'))) {
            $from = Carbon::parse($request->input('from_submit_date'))->startOfDay();
            $to = Carbon::parse($request->input('to_submit_date'))->endOfDay();
            if ($from->lt($to)) {
                $userVerificationsQuery
                    ->whereDate('created_at', '>=', $from)
                    ->whereDate('created_at', '<=', $to);
            } else {
                $request->request->remove('from_submit_date');
                $request->request->remove('to_submit_date');
            }
        }
        if (!empty($request->input('from_verification_date')) && !empty($request->input('to_verification_date'))) {
            $from = Carbon::parse($request->input('from_verification_date'))->startOfDay();
            $to = Carbon::parse($request->input('to_verification_date'))->endOfDay();
            if ($from->lt($to)) {
                $userVerificationsQuery
                    ->where('identity_card', UserVerificationAccount::IDENTITY_CARD_STATUS_VERIFIED)
                    ->whereDate('updated_at', '>=', $from)
                    ->whereDate('updated_at', '<=', $to);
            } else {
                $request->request->remove('from_verification_date');
                $request->request->remove('to_verification_date');
            }
        }
        return $userVerificationsQuery;
    }

    /**
     * For filtering query of user verificatoin based on user information.
     *
     * @param Request $request
     * @param Builder $userVerificationsQuery
     * @return Builder
     */
    private function filterUser(Request $request, Builder $userVerificationsQuery): Builder
    {
        $isOwner = $request->input('is_owner');
        $q = $request->input('q');
        if ((!empty($isOwner)) || (!empty($q))) {
            $userVerificationsQuery->whereHas('user', function($query) use ($q, $isOwner) {
                if (!empty($q)) {
                    $query->where(function($groupedCondition) use ($q) {
                        $groupedCondition->where('email', 'LIKE', '%'.$q.'%')
                            ->orwhere('name', 'LIKE', '%'.$q.'%');
                    });
                }
                if (!empty($isOwner)) {
                    $query->where('is_owner', $isOwner);
                }
            });
        }
        return $userVerificationsQuery;
    }

    /**
     * For filtering query of user verification based on it's user media.
     *
     * @param Request $request
     * @param Builder $userVerificationsQuery
     * @return Builder
     */
    private function filterUserMedia(Request $request, Builder $userVerificationsQuery): Builder
    {
        $uploadFrom = $request->input('upload_from');
        $identityType = $request->input('identity_type');
        if ((!empty($uploadFrom)) || (!empty($identityType))) {
            $userVerificationsQuery->whereHas('user_medias', function($query) use ($identityType, $uploadFrom) {
                $query->latestIdentityMediaOfEachUser();
                if (!empty($identityType)) {
                    $query->where('type', $identityType);
                }
                if (!empty($uploadFrom)) {
                    $query->where('from', $uploadFrom);
                }
            });
        }
        return $userVerificationsQuery;
    }

    public function getIdentityCardDetail($id)
    {
        $userRequest  = UserVerificationAccount::with('user')->find($id);
        if (is_null($userRequest)) {
            return redirect()->route('admin.account.verification-identity-card')->with('messages', 'Data tidak ditemukan');
        }

        $user = $userRequest->user;
        $userMedia = $user->user_medias;
        
        $photoIdentity = "";
        $photoSelfieIdentity = "";
        $type = "";
        $uploadFrom = "";
        foreach ($userMedia as $photo) {
            $type = $photo->type;

            if ($photo['description'] == "photo_identity" || $photo['description'] == "photo_ktp")
                $photoIdentity = $photo->getMediaUrl()['medium'];
            if ($photo['description'] == "selfie_identity" || $photo['description'] == "selfie_ktp")
                $photoSelfieIdentity = $photo->getMediaUrl()['medium'];
            
            if ($photo['type'] == "booking")
                $uploadFrom = "Booking";
            else $uploadFrom = "User Profile";
        }
        
        $viewData = [
            'boxTitle'       => 'Verification Account Detail',
            'id'             => $id,
            'user'           => $user,
            'userRequest'    => $userRequest,
            'media'          => $userMedia,
            'type'           => $type,
            'photoIdentity'  => $photoIdentity,
            'photoSelfieIdentity' => $photoSelfieIdentity,
            'uploadFrom'     => $uploadFrom
        ];

        return view('admin.contents.user.detail-verification-identity', $viewData);
    }

    public function verificationIdentityCard(int $id, Request $request)
    {
        $status = $request->input('status');
        $verifyAccount = UserVerificationAccount::find($id);
        if (is_null($verifyAccount)) {
            return redirect()->route('admin.account.verification-identity-card')->with('messages', 'Data tidak ditemukan');
        }

		switch ($status) {
			case $verifyAccount::IDENTITY_CARD_STATUS_VERIFIED:
				$verifyAccount->identity_card = $verifyAccount::IDENTITY_CARD_STATUS_VERIFIED;
				$verifyAccount->update();

				$this->notifVerify($verifyAccount);
				break;
            case $verifyAccount::IDENTITY_CARD_STATUS_REJECTED:
                $validator = Validator::make($request->all(), [
                    'reject_reason' => 'required|string',
                ]);
                if ($validator->fails()) {
                    $errorMessage = implode("\n", $validator->errors()->all());
                    return redirect()->route('admin.account.verification-identity-card')->with('error_message', $errorMessage);
                }
                $verifyAccount->reject_reason = $request->input('reject_reason');
				$verifyAccount->identity_card = $verifyAccount::IDENTITY_CARD_STATUS_REJECTED;
				$verifyAccount->update();

				// Delete the last indentity card
				$this->deleteExistingIdentityByUserID($verifyAccount->user_id);

				$this->notifVerify($verifyAccount);
				break;
			case $verifyAccount::IDENTITY_CARD_STATUS_CANCELLED:
				$verifyAccount->identity_card = $verifyAccount::IDENTITY_CARD_STATUS_CANCELLED;
				$verifyAccount->update();

				// Delete the last indentity card
				$this->deleteExistingIdentityByUserID($verifyAccount->user_id);

				$this->notifVerify($verifyAccount);
				break;
			default:
				$verifyAccount->identity_card = $verifyAccount::IDENTITY_CARD_STATUS_WAITING;
				$verifyAccount->update();
				break;
		}

		ActivityLog::LogUpdate(Action::USER_VERIFICATION, $id);

        return redirect()->route('admin.account.verification-identity-card')->with('messages', 'Berhasil melakukan verifikasi');
    }

    private function notifVerify(UserVerificationAccount $userVerificationAccount)
    {
        try {
            $userId = $userVerificationAccount->user_id;
            $title = __("notification.user-identity.{$userVerificationAccount->identity_card}.title");
            $scheme = __("notification.user-identity.{$userVerificationAccount->identity_card}.scheme");
            $bodyCopyPrefix = '';
            if ($userVerificationAccount->identity_card == UserVerificationAccount::IDENTITY_CARD_STATUS_REJECTED) {
                // Rejected has custom body based on it's reason
                $bodyCopyPrefix = '.other';
                if (!empty($userVerificationAccount->reject_reason) &&
                    in_array($userVerificationAccount->reject_reason, UserVerificationAccount::REJECT_REASON_ALL_PREDEFINED)
                ) {
                    $bodyCopyPrefix = ".{$userVerificationAccount->reject_reason}";
                }
            }
            $body = __("notification.user-identity.{$userVerificationAccount->identity_card}.body{$bodyCopyPrefix}");

            $notifData = NotifData::buildFromParam(
                $title,
                $body,
                $scheme,
                ''
            );
            return $this->sendNotifWrapper->sendToUserIds($notifData, (array) $userId, false);
        } catch (\Exception $e) {
            Bugsnag::notifyError("Failed to send KYC notification.", $e->getMessage());
            return redirect()->route('admin.account.verification-identity-card')->with('error_message', $e->getMessage());
        }
    }

    public function deleteExistingIdentityByUserID($userId)
	{
		// Delete the last indentity card
		$userMedia = new UserMedia();
		$identityCardIds = $userMedia->getIdentityCardId($userId);
		if (empty($identityCardIds)) {
			return null;
		}

		$userMedia->destroy($identityCardIds);
	}

}
