<?php

namespace App\Http\Controllers\Admin\Room;

use App\Entities\Activity\Action;
use App\Entities\Activity\ActivityLog;
use App\Entities\Area\Province;
use App\Entities\Generate\ListingReason;
use App\Entities\Media\Media;
use App\Entities\Property\Property;
use App\Entities\Room\Element\Tag;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Enums\Mamipay\MamipayRoomPriceComponentType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Helpers\ApiHelper;
use App\Http\Requests\Admin\Room\CreateKostRequest;
use App\Http\Requests\Admin\Room\UpdateKostRequest;
use App\Libraries\AreaFormatter;
use App\Services\Owner\Kost\InputService;
use App\Services\Owner\Kost\PriceService;
use App\Transformers\Owner\Common\MediaTransformer;
use App\Transformers\Owner\Input\Kost\PriceTransformer;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

class KostController extends Controller
{
    private $inputService;
    private $priceService;

    public $agentStatus = [
        'Semua',
        'Agen',
        'Pemilik Kos',
        'Pengelola Kos',
        'Lainnya',
        'Anak Kos',
        'Kos Duplikat',
        'Pemilik Apartemen',
        'Pengelola Apartemen',
        'Agen Apartemen',
        'Gojek',
        'scrap',
        'scrap google',
        'scrap koran'
    ];

    public function __construct(InputService $inputService, PriceService $priceService)
    {
        $this->middleware(function ($request, $next) {
            if (! Auth::user()->can('access-kost')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        View::share('contentHeader', 'Stories Management');
        View::share('user', Auth::user());

        $this->inputService = $inputService;
        $this->priceService = $priceService;
    }

    public function create()
    {
        $facilities = $this->getAllFacilities();

        $facilityShare = $facilities->where('type', 'fac_share');
        $facilityRoom = $facilities->where('type', 'fac_room');
        $facilityBath = $facilities->where('type', 'fac_bath');
        $facilityPark = $facilities->where('type', 'fac_park');
        $minPayment = $facilities->where('type', 'keyword');
        $facilityOther = $facilities->whereNotIn('type', ['fac_share', 'fac_room', 'fac_bath', 'fac_park', 'keyword']);

        $kosRule = $this->getAllKosRule();

        $viewData = [
            'isStateCreate'     => true,
            'agentStatus'       => $this->agentStatus,
            'facilityShare'     => $facilityShare,
            'facilityRoom'      => $facilityRoom,
            'facilityBath'      => $facilityBath,
            'facilityPark'      => $facilityPark,
            'minPayment'        => $minPayment,
            'facilityOther'     => $facilityOther,
            'kosRule'           => $kosRule,
            'provinceList'     => $this->getProvinceList(),
            'checkPropertyNameUrl'  => null,
            'checkUnitTypeyUrl'  => null,
        ];

        return view('admin.contents.kost.form', $viewData);
    }

    public function store(CreateKostRequest $request)
    {
        $validator = $request->validator;

        $validator->after(function ($validator) use ($request) {
            $nameExist = Room::where('name', $request->name)->first();
            if ($nameExist) {
                $validator->errors()->add('name', 'Nama kos telah digunakan sebelumnya.');
            }
            if ($request->room_count < $request->room_available) {
                $validator->errors()->add(
                    'room_available',
                    'Jumlah kamar tersedia tidak boleh melebihi jumlah kamar total.'
                );
            }
            if (! empty($request->fine_price)
                && (
                    ($request->fine_type == 'day' && $request->fine_length > 31) ||
                    ($request->fine_type == 'week' && $request->fine_length > 4) ||
                    ($request->fine_type == 'month' && $request->fine_length > 12)
                )
            ) {
                $validator->errors()->add(
                    'room_available',
                    'Durasi biaya denda melebihi batas maksimal.'
                );
            }
        });

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        $kostData = $this->parseKostDataFromRequest($request);

        $owner = User::ownerOnly()->where('phone_number', $request->owner_phone)->first();
        if ($owner instanceof User && ! empty($request->property_name)) {
            $property = $this->findOrCreateProperty($owner, $request);

            if (! $property) {
                return redirect()
                ->back()
                ->withErrors('Tipe kamar dari properti ini telah digunakan sebelumnya.')
                ->withInput();
            }

            $kostData['property_id'] = $property->id;
            $kostData['unit_type'] = $request->unit_type;
        }

        $room = new Room(array_merge(
            [
                'add_from' => 'admin',
                'is_active' => 'false',
                'is_indexed' => 0,
                'is_promoted' => 'false',
                'photo_count' => 0,
                'view_count' => 0,
                'expired_phone' => 0,
                'is_booking' => 0,
                'is_visited_kost' => 0,
                'kost_updated_date' => Carbon::now(),
            ],
            $kostData
        ));

        $room->generateSongId();
        $room->save();

        $this->saveAdditionalKostData($room, $request);

        
        if ($owner instanceof User) {
            $this->createRoomOwner($room, $owner);
        }

        ActivityLog::LogCreate(Action::KOST, $room->id);

        return redirect('/admin/card/' . $room->id)->with('message', 'Kos berhasil ditambahkan');
    }

    public function edit(int $id)
    {
        $room = Room::with([
            'tags',
            'room_term.room_term_document.media',
            'mamipay_price_components.price_component_additionals',
            'address_note' => function ($query) {
                $query->with('province');
            },
            'property',
            'owners',
            'listing_reason' => function ($p) {
                $p->orderBy('id', 'desc');
            },
        ])
        ->find($id);

        if (is_null($room)) {
            return redirect('/admin/room/')
                ->withErrors('Kost not found');
        }

        $properties = null;
        $selectedProperty = null;
        if (! is_null($room->owners->first())) {
            $properties = Property::where('owner_user_id', $room->owners->first()->user_id)->get();
            $selectedProperty = $room->property;
        }

        $kosRulePhotos = $this->getKosRulePhotos($room);

        $additionalPrice = $this->getAdditionalPriceData($room);

        $facilities = $this->getAllFacilities();

        $facilityShare = $facilities->where('type', 'fac_share');
        $facilityRoom = $facilities->where('type', 'fac_room');
        $facilityBath = $facilities->where('type', 'fac_bath');
        $facilityPark = $facilities->where('type', 'fac_park');
        $minPayment = $facilities->where('type', 'keyword');
        $facilityOther = $facilities->whereNotIn('type', ['fac_share', 'fac_room', 'fac_bath', 'fac_park', 'keyword']);

        $kosRule = $this->getAllKosRule();

        $selectedTags = $room->tags->pluck('id')->toArray();

        $photo360 = $this->getRoundPhoto($room);

        $show_agent_input = true;
        $agen_data = [$room->agent_phone, $room->agent_name];
        if ($room->agent_id > 0) {
            $show_agent_input = false;
            $agen_data = [$room->agents->phone_number, $room->agents->name];
        }

        $viewData = [
            'isStateCreate'     => false,
            'agentStatus'       => $this->agentStatus,
            'facilityShare'     => $facilityShare,
            'facilityRoom'      => $facilityRoom,
            'facilityBath'      => $facilityBath,
            'facilityPark'      => $facilityPark,
            'minPayment'        => $minPayment,
            'facilityOther'     => $facilityOther,
            'kosRule'           => $kosRule,
            'room'              => $room,
            'selectedTags'      => $selectedTags,
            'rulePhotos'        => $kosRulePhotos,
            'additionalPrice'   => $additionalPrice,
            'properties'        => $properties,
            'photo360'          => $photo360,
            'show_agent_input'  => $show_agent_input,
            'agent_data'        => $agen_data,
            'provinceList'     => $this->getProvinceList(),
            'selectedProperty'  => $selectedProperty,
            'checkPropertyNameUrl'  => route('admin.room.agent.check-property-name', [$room->id]),
            'checkUnitTypeyUrl'  => route('admin.room.agent.check-unit-type', [$room->id]),
        ];

        return view('admin.contents.kost.form', $viewData);
    }

    public function update(UpdateKostRequest $request, int $id)
    {
        $validator = $request->validator;

        $property = Property::with('rooms')->find($request->property_id);

        $validator->after(function ($validator) use ($request, $id, $property) {
            $nameExist = Room::where('name', $request->name)
                ->where('id', '!=', $id)
                ->first();
            if ($nameExist) {
                $validator->errors()->add('name', 'Nama kos telah digunakan sebelumnya.');
            }

            if (! is_null($property)) {
                if ($property->has_multiple_type && empty($request->unit_type)) {
                    $validator->errors()->add('unit_type', 'Tipe kamar dari properti ini harus diisi.');
                }

                $sameUnitType = $property->rooms->firstWhere('unit_type', $request->unit_type);
                if (! is_null($sameUnitType) && $sameUnitType->id != $id) {
                    $validator->errors()->add('unit_type', 'Tipe kamar dari properti ini telah digunakan sebelumnya.');
                }
            }

            if (! empty($request->fine_price)
                && (
                    ($request->fine_type == 'day' && $request->fine_length > 31) ||
                    ($request->fine_type == 'week' && $request->fine_length > 4) ||
                    ($request->fine_type == 'month' && $request->fine_length > 12)
                )
            ) {
                $validator->errors()->add(
                    'room_available',
                    'Durasi biaya denda melebihi batas maksimal.'
                );
            }
        });

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        $room = Room::find($id);

        if (is_null($room)) {
            return redirect('/admin/room/')
                ->withErrors('Kost not found');
        }

        $kostData = $this->parseKostDataFromRequest($request);

        foreach ($kostData as $key => $value) {
            $room->{$key} = $value;
        };

        if (! is_null($property)) {
            $room->property_id = $request->input('property_id');
            $room->unit_type = $request->input('unit_type');
        }

        if ($room->agent_id < 1) {
            $room->agent_email = $request->input('agent_email');
            $room->agent_phone = $request->input('agent_phone');
        }

        $room->kost_updated_date = Carbon::now();
        $room->save();

        $this->saveAdditionalKostData($room, $request);

        if (! is_null($property) && ! $property->has_multiple_type && ! empty($request->unit_type)) {
            $property->has_multiple_type = true;
            $property->save();
        }

        $this->updateAgentNote($room, (string) $request->input('agent_note'));

        ActivityLog::LogUpdate(Action::KOST, $room->id);

        return redirect()->back()->with('message', 'Kos berhasil diupdate');
    }

    private function parseTagDataFromRequest(Request $request): array
    {
        $minPayment = $request->input('min_payment') == 0 ? [] : [$request->input('min_payment')];
        $facShare = $request->has('facility_share_ids') ? $request->input('facility_share_ids') : [];
        $facRoom = $request->has('facility_room_ids') ? $request->input('facility_room_ids') : [];
        $facBath = $request->has('facility_bath_ids') ? $request->input('facility_bath_ids') : [];
        $facPark = $request->has('facility_park_ids') ? $request->input('facility_park_ids') : [];
        $facOther = $request->has('facility_other_ids') ? $request->input('facility_other_ids') : [];
        $kosRule = $request->has('kos_rule_ids') ? $request->input('kos_rule_ids') : [];

        return array_merge(
            $minPayment,
            $facShare,
            $facRoom,
            $facBath,
            $facPark,
            $facOther,
            $kosRule
        );
    }

    private function parseKostDataFromRequest(Request $request): array
    {
        $cleanCity = trim(
            ApiHelper::removeEmoji(
                $request->input('area_city')
            )
        );
        $cleanSubdistrict = trim(
            ApiHelper::removeEmoji(
                $request->input('area_subdistrict')
            )
        );
        $cleanAddress = trim(
            ApiHelper::removeEmoji(
                $request->input('address')
            )
        );
        $cleanGuide = trim(
            ApiHelper::removeEmoji(
                $request->input('guide')
            )
        );

        return [
            'gender'                => $request->input('gender'),
            'name'                  => $request->input('name'),
            'description'           => $request->input('description'),
            'building_year'         => $request->input('building_year') == 0 ? null : $request->input('building_year'),
            'remark'                => $request->input('remark'),
            'price_daily'           => (int) $request->input('price_daily'),
            'price_weekly'          => (int) $request->input('price_weekly'),
            'price_monthly'         => (int) $request->input('price_monthly'),
            'price_yearly'          => (int) $request->input('price_yearly'),
            'price_quarterly'       => (int) $request->input('price_quarterly'),
            'price_semiannually'    => (int) $request->input('price_semiannually'),
            'price_remark'          => $request->input('price_remark'),
            'size'                  => $request->input('size'),
            'room_count'            => $request->input('room_count'),
            'room_available'        => $request->input('room_available'),
            'address'               => $cleanAddress,
            'longitude'             => $request->input('longitude'),
            'latitude'              => $request->input('latitude'),
            'area_city'             => $cleanCity,
            'area_subdistrict'      => $cleanSubdistrict,
            'area_big'              => AreaFormatter::format($cleanCity),
            'guide'                 => $cleanGuide,
            'owner_name'            => $request->input('owner_name'),
            'owner_phone'           => $request->input('owner_phone'),
            'manager_name'          => $request->input('manager_name'),
            'manager_phone'         => $request->input('manager_phone'),
            'agent_name'            => $request->input('agent_name'),
            'agent_status'          => $request->input('agent_status'),
            'verificator'           => $request->input('verificator'),
            'is_mamirooms'          => ($request->input('mamirooms') == '1'),
            'is_testing'            => ($request->input('testing') == '1'),
            'is_indexed'            => ($request->input('indexed') == '1'),
            'fac_room_other'        => $request->input('fac_room_other'),
            'fac_bath_other'        => $request->input('fac_bath_other'),
            'fac_share_other'       => $request->input('fac_share_other'),
            'fac_near_other'        => $request->input('fac_near_other'),
            'admin_remark'          => $request->input('admin_remark'),
            'youtube_id'            => $request->input('youtube_id'),
            'photo_round_id'        => $request->input('photo_round_id'),
        ];
    }

    /**
     * parse request on price step to create kost
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    private function parsePriceDataFromRequest(Request $request): array
    {
        $additionalCost = (array) $request->input('additional_cost');
        foreach ($additionalCost as &$additional) {
            $additional['is_active'] = $request->input('additional_cost_active');
        }

        $depositFee = $request->input('deposit') == 0 ? null : [
            'price' => $request->input('deposit'),
            'is_active' => $request->input('deposit_active'),
        ];

        $downPayment = $request->input('dp') == 0 ? null : [
            'percentage' => $request->input('dp'),
            'is_active' => $request->input('dp_active'),
        ];

        $fine = $request->input('fine_price') == 0 ? null : [
            'price' => $request->input('fine_price'),
            'duration_type' => $request->input('fine_type'),
            'length' => $request->input('fine_length'),
            'is_active' => $request->input('fine_active'),
        ];

        return [
            'additional_cost' => $additionalCost,
            'deposit' => $depositFee,
            'dp' => $downPayment,
            'fine' => $fine,
        ];
    }

    private function saveAdditionalKostData(Room $room, Request $request)
    {
        $this->saveTagData($room, $request);

        $this->saveAdditionalPriceData($room, $request);

        $this->saveTermData($room, $request);
        
        $this->saveAddressData($room, $request);
    }

    private function saveTagData(Room $room, Request $request)
    {
        $tagData = $this->parseTagDataFromRequest($request);

        $room->tags()->sync($tagData);
    }

    private function saveAdditionalPriceData(Room $room, Request $request)
    {
        $priceData = $this->parsePriceDataFromRequest($request);

        $this->priceService->saveAdditionalCostData($room, $priceData['additional_cost']);
        $this->priceService->savePriceComponent($room, $priceData['deposit'], MamipayRoomPriceComponentType::DEPOSIT);
        $this->priceService->savePriceComponent($room, $priceData['dp'], MamipayRoomPriceComponentType::DP);
        $this->priceService->savePriceComponent($room, $priceData['fine'], MamipayRoomPriceComponentType::FINE);
    }

    private function saveTermData(Room $room, Request $request)
    {
        $photoRules = is_null($request->input('kos_rule_photos')) ? [] : $request->input('kos_rule_photos');

        $this->inputService->syncRoomTermPhotos($photoRules, $room);
    }

    private function saveAddressData(Room $room, Request $request)
    {
        $cleanCity = trim(
            ApiHelper::removeEmoji(
                $request->input('area_city')
            )
        );
        $cleanSubdistrict = trim(
            ApiHelper::removeEmoji(
                $request->input('area_subdistrict')
            )
        );

        $this->inputService->saveAddressNote(
            $room->id,
            AreaFormatter::provinceFormatter(
                $request->input('province')
            ),
            $cleanCity,
            $cleanSubdistrict,
            $request->input('guide')
        );
    }

    private function createRoomOwner(Room $room, User $user)
    {
        $roomOwner = new RoomOwner();
        $roomOwner->user_id = $user->id;
        $roomOwner->designer_id = $room->id;
        $roomOwner->status = RoomOwner::ROOM_ADD_STATUS;
        $roomOwner->owner_status = RoomOwner::STATUS_TYPE_KOS_OWNER;
        $roomOwner->save();
    }

    private function findOrCreateProperty(User $user, Request $request)
    {
        $property = Property::where('name', $request->property_name)
            ->where('owner_user_id', $user->id)
            ->first();

        if ($property instanceof Property) {
            $property->loadMissing('rooms');
            if (($property->has_multiple_type
                    && (empty($request->unit_type) || $property->rooms->firstWhere('unit_type', $request->unit_type)))
                || (! $property->has_multiple_type && ($property->rooms->count() > 0 && empty($request->unit_type)))
            ) {
                return false;
            }
            
            if (! $property->has_multiple_type && ! empty($request->unit_type)) {
                $property->has_multiple_type = true;
                $property->save();
            }
        } else {
            $property = new Property([
                'name' => $request->property_name,
                'has_multiple_type' => ! empty($request->unit_type),
                'owner_user_id' => $user->id,
            ]);
            $property->save();
        }
        
        return $property;
    }

    private function getAllFacilities()
    {
        return Tag::where('type', '!=', 'fac_project')
        ->whereNotIn('id', Tag::IGNORED_IDS)
        ->whereDoesntHave('types', function ($query) {
            $query->where('name', 'kos_rule');
        })
        ->get();
    }

    private function getAllKosRule()
    {
        return Tag::whereHas('types', function ($query) {
            $query->where('name', 'kos_rule');
        })->get();
    }

    private function getKosRulePhotos(Room $room): array
    {
        if (is_null($room->room_term) || is_null($room->room_term->room_term_document)) {
            return [];
        }

        $mediaTransformer = app()->make(MediaTransformer::class);

        $collection = $room->room_term->room_term_document;

        return $collection
            ->map(function ($roomTermPhoto) use ($mediaTransformer) {
                if ($roomTermPhoto->media instanceof Media) {
                    return $mediaTransformer->transform($roomTermPhoto->media);
                }
            })
            ->values()
            ->toArray();
    }

    private function getRoundPhoto(Room $room): ?array
    {
        if (is_null($room->photo_round)) {
            return null;
        }

        $mediaTransformer = app()->make(MediaTransformer::class);

        return $mediaTransformer->transform($room->photo_round);
    }

    private function getAdditionalPriceData(Room $room): array
    {
        $priceTransformer = app()->make(PriceTransformer::class);

        $additionalPrice = $priceTransformer->transform($room);
        unset($additionalPrice['_id']);
        unset($additionalPrice['price']);
        unset($additionalPrice['min_payment']);
        unset($additionalPrice['is_price_flash_sale']);

        return $additionalPrice;
    }

    private function getProvinceList(): Collection
    {
        return Province::select('id', 'name')->get();
    }

    private function updateAgentNote(Room $room, string $agentNote)
    {
        if (strlen(trim($agentNote)) > 0) {
            $reason = ListingReason::where('listing_id', $room->id)->where('from', 'room')->first();

            if (is_null($reason)) {
                $reason = new ListingReason();
                $reason->from = "room";
                $reason->listing_id = $room->id;
            }

            $reason->content = $agentNote;
            $reason->save();
        }
    }
}
