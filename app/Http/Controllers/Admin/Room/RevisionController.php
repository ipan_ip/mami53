<?php

namespace App\Http\Controllers\Admin\Room;

use App\Entities\Revision\Revision;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Http\Controllers\Controller;
use App\Presenters\RoomPresenter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RevisionController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!Auth::user()->can('access-kost')) {
                return redirect('admin');
            }

            return $next($request);
        });

        \View::share('contentHeader', 'Stories Management');
        \View::share('user', Auth::user());
    }

    public function index(Request $request, $id)
    {
        $room = Room::where('id', $id)
            ->with(['owners'])
            ->first();

        if (is_null($room)) {
            return redirect()->back()->with('error_message', 'Data tidak ditemukan');
        }

        $revisionHistory = Revision::with('user')
            ->where('revisionable_type', Room::class)
            ->where('revisionable_id', $id)
            ->orderBy('id', 'DESC')
            ->limit(10)
            ->offset($request->input('offset', 0))
            ->get();
        $total = Revision::where('revisionable_type', Room::class)
            ->where('revisionable_id', $id)
            ->count();

        $revisionHistory = (new RoomPresenter(RoomPresenter::REVISION_INDEX))->present($revisionHistory);

        return [
            'total' => $total,
            'owner_id' => $room->owners()->first()->id,
            'data' => $revisionHistory['data']
        ];
    }
}
