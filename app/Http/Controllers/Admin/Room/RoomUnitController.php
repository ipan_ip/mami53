<?php

namespace App\Http\Controllers\Admin\Room;

use App\Entities\Log\RoomUnitRevision;
use App\Exceptions\RoomAllotmentException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use App\Services\RoomUnit\RoomUnitService;
use App\Repositories\Room\RoomUnitRepository;
use App\Entities\Room\Room;
use Illuminate\Support\Facades\Validator;
use Exception;

class RoomUnitController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!Auth::user()->can('access-kost') && !Auth::user()->can('access-kost-additional')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        View::share('contentHeader', 'Stories Management');
        View::share('user', Auth::user());
    }

    /**
     * Display room units from a kost.
     *
     * @param  \App\Repositories\Room\RoomUnitRepository  $roomUnitRepository
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function index(RoomUnitRepository $roomUnitRepository, $id)
    {
        $room = Room::with('room_unit')->find($id);

        if (is_null($room)) {
            return redirect('/admin')->withErrors('Kos tidak ditemukan');
        }

        if ($room->room_unit->count() == 0) {
            try {
                $roomUnitRepository->backFillRoomUnit($room);
            } catch (Exception $e) {
                // do nothing
            }
        }

        $roomUnits = $roomUnitRepository->with('active_contract.tenant')->where('designer_id', $id)->get();
        $transformedUnits = (new \App\Presenters\Room\RoomUnitPresenter('detail-tenant'))->present($roomUnits);

        return view('admin.contents.stories.room-unit', [
            'units' => $transformedUnits['data'],
            'room' => $room
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'new_unit' => 'required|integer|between:1,20',
        ]);

        $room = Room::find($id);
        $validator->after(function ($validator) use ($request, $room) {
            if (is_null($room)) {
                $validator->errors()->add('new_unit', 'Kos tidak ditemukan');
            } elseif ($request->input('new_unit') + $room->room_count > Room::MAX_ROOM_COUNT) {
                $validator->errors()->add('new_unit', 'Jumlah kamar tidak bisa lebih dari ' . Room::MAX_ROOM_COUNT);
            }
        });

        if ($validator->fails()) {
            return redirect('/admin/room/' . $id . '/room-unit/')->withErrors($validator);
        }

        $oldCount = $room->room_count;
        $oldAvailable = $room->room_available;
        $room->room_count = $oldCount + $request->input('new_unit');
        $room->room_available = $oldAvailable + $request->input('new_unit');
        $room->save();

        return redirect('/admin/room/' . $id . '/room-unit/')->with('message', 'Kamar berhasil ditambahkan');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Services\RoomUnit\RoomUnitService  $service
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteBulk(Request $request, RoomUnitService $service, $id)
    {
        $validator = Validator::make($request->all(), [
            'bulk_delete' => 'required',
        ]);

        $room = Room::find($id);
        $validator->after(function ($validator) use ($request, $room) {
            if (is_null($room)) {
                $validator->errors()->add('new_unit', 'Kos tidak ditemukan');
            }
        });

        if ($validator->fails()) {
            return redirect('/admin/room/' . $id . '/room-unit/')->withErrors($validator);
        }

        $deletedList = $request->input('bulk_delete');

        try {
            $result = $service->updateBulk($room->song_id, [], [], $deletedList);
            $successCount = count($result['success']);
        } catch (\Exception $e) {
            return redirect('/admin/room/' . $id . '/room-unit/')->withErrors($e->getMessage());
        }

        return redirect('/admin/room/' . $id . '/room-unit/')->with('message', $successCount . ' Kamar berhasil dihapus');
    }

    /**
     * @param Request $request
     * @param  \App\Repositories\Room\RoomUnitRepository  $roomUnitRepository
     * @param RoomUnitService $service
     * @param $id
     * @param $unit
     * @return \Illuminate\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateRow(
        Request $request,
        RoomUnitRepository $roomUnitRepository,
        RoomUnitService $service,
        $id,
        $unitId
    ) {
        $validator = Validator::make($request->all(), [
            'name'      => 'required|string|min:1|max:50|not_regex:/^ +$/i',
            'floor'     => 'string|min:1|max:50|not_regex:/^ +$/i',
            'occupied'  => 'required',
        ]);

        $unit = $roomUnitRepository->with(['room'])->find($unitId);
        $validator->after(function ($validator) use ($request, $unit) {
            if (is_null($unit)) {
                $validator->errors()->add('name', 'Room Unit tidak ditemukan');
            }
        });

        if ($validator->fails()) {
            return redirect('/admin/room/' . $id . '/room-unit/')->withErrors($validator);
        }

        $params = [
            'name'      => $request->input('name'),
            'floor'     => isset($request['floor']) ? $request->input('floor') : '',
            'occupied'  => $request->input('occupied'),
        ];
        try {
            $service->updateSingleUnit($unit->room, $unit, $params);
        } catch (QueryException $queryException) {
            return redirect('/admin/room/' . $id . '/room-unit/')->withErrors("Nama sudah pernah digunakan");
        } catch (\Exception $e) {
            return redirect('/admin/room/' . $id . '/room-unit/')->withErrors($e->getMessage());
        }

        return redirect('/admin/room/' . $id . '/room-unit/')->with('message', 'Kamar berhasil di-update');
    }

    /**
     * @param RoomUnitService $service
     * @param $id
     * @param $unit
     * @return \Illuminate\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deleteRow(
        RoomUnitService $service,
        $id,
        $unitId
    ) {
        try {
            $service->deleteRoomUnit($unitId);
        } catch (\Exception $e) {
            return redirect('/admin/room/' . $id . '/room-unit/')->withErrors($e->getMessage());
        }
        return redirect('/admin/room/' . $id . '/room-unit/')->with('message', 'Kamar berhasil dihapus');
    }

    /**
     * Sync designer with room unit.
     *
     * @param  \App\Repositories\Room\RoomUnitRepository  $roomUnitRepository
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function syncData(RoomUnitRepository  $roomUnitRepository, int $id)
    {
        $room = Room::withCount([
            'room_unit_empty as room_unit_empty_with_contract_count' => function ($query) {
                $query->whereHas('active_contract');
            }
        ])->find($id);

        if (is_null($room)) {
            return redirect('/admin/room/two')->withErrors('Kamar tidak ditemukan');
        }

        if ($room->room_unit_empty_with_contract_count > 0) {
            $roomUnitRepository->occupyRoomUnitWithContract($id);
        }

        $room = Room::withCount(
            'room_unit_empty',
            'room_unit_occupied'
        )->find($id);

        $roomUnitCount = $room->room_unit_empty_count + $room->room_unit_occupied_count;
        $roomUnitAvailable = $room->room_unit_empty_count;

        if ($roomUnitCount == 0) {
            $roomUnitRepository->backFillRoomUnit($room);
        }

        if (
            $room->room_count != $roomUnitCount ||
            $room->room_available != $roomUnitAvailable
        ) {
            $room->room_count = $roomUnitCount;
            $room->room_available = $roomUnitAvailable;
            $room->save();
        }

        return redirect('/admin/room/two')->with('message', 'Sync Room Allotment berhasil');
    }
}
