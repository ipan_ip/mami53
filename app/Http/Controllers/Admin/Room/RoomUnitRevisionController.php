<?php

namespace App\Http\Controllers\Admin\Room;

use App\Entities\Log\RoomUnitRevision;
use App\Entities\Room\Room;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RoomUnitRevisionController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!Auth::user()->can('access-kost') && !Auth::user()->can('access-kost-additional')) {
                return redirect('admin');
            }

            return $next($request);
        });
    }
    public function index(Request $request, $id)
    {
        $room = Room::where('id', $id)->first();

        if (is_null($room)) {
            return redirect('/admin/room/' . $id . '/room-unit/')->withErrors('Kamar tidak ditemukan');
        }

        $history = RoomUnitRevision::where('designer_id', $room->id)
            ->limit(10)
            ->offset((int) $request->input('offset', 0))
            ->orderBy('_id', 'desc')
            ->get();
        $total = RoomUnitRevision::where('designer_id', $room->id)->count();
        $userIds = $history->pluck('user_id')
            ->unique()
            ->values()
            ->all();
        $users = User::whereIn('id', $userIds)->get();
        $history = $history->map(function ($log, $key) use ($users) {
            $log = $log->toArray();
            $user = $users->where('id', $log['user_id'])->first();
            $log['update_by'] = [
                'id' => !is_null($user) ? $log['user_id'] : null,
                'name' => !is_null($user) ? $user->name : null,
                'email' => !is_null($user) ? $user->email : null,
                'is_owner' => !is_null($user) ? ($user->is_owner === 'true') : null
            ];

            return $log;
        });

        return [
            'total' => $total,
            'kostName' => $room->name,
            'revisionHistory' => $history
        ];
    }
}
