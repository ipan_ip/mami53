<?php

namespace App\Http\Controllers\Admin\Room;

use App\Entities\Property\Property;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Helpers\RegexHelper;
use App\User;
use Illuminate\Support\Facades\Validator;

class PropertyController extends Controller
{
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'property_name' => 'required|string|regex:' . RegexHelper::validPropertyName(),
            'user_id' => 'required|integer',
            'has_multiple_type' => 'required|boolean',
        ]);

        $user = User::find($request->input('user_id'));
        if (! $user instanceof User) {
            return redirect()->back()->withErrors('User tidak ditemukan');
        }

        $validator->after(function ($validator) use ($request) {
            $propertyExist = Property::where('name', $request->property_name)
                ->where('owner_user_id', $request->user_id)
                ->first();
            if ($propertyExist) {
                $validator->errors()->add('name', 'Nama properti telah digunakan sebelumnya.');
            }
        });

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $property = new Property([
            'name' => $request->property_name,
            'has_multiple_type' => $request->has_multiple_type,
            'owner_user_id' => $request->user_id,
        ]);
        $property->save();

        return redirect()->back()->with('message', 'Properti berhasil ditambahkan');
    }

    public function update(Request $request, int $id)
    {
        $validator = Validator::make($request->all(), [
            'property_name' => 'required|string|regex:' . RegexHelper::validPropertyName(),
            'has_multiple_type' => 'required|integer|in:0,1',
        ]);

        $property = Property::find($id);
        if (! $property instanceof Property) {
            return redirect()->back()->withErrors('Properti tidak ditemukan');
        }

        $validator->after(function ($validator) use ($request, $id) {
            $propertyExist = Property::where('name', $request->property_name)
                ->where('owner_user_id', $request->user_id)
                ->where('id', '!=', $id)
                ->first();
            if ($propertyExist) {
                $validator->errors()->add('name', 'Nama properti telah digunakan sebelumnya.');
            }
        });

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $property->name = $request->property_name;
        if ($request->has_multiple_type) {
            $property->has_multiple_type = $request->has_multiple_type;
        }
        $property->save();

        return redirect()->back()->with('message', 'Properti berhasil diperbarui.');
    }
}
