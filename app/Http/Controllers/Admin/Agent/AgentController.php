<?php

namespace App\Http\Controllers\Admin\Agent;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Validator;
use App\Entities\Room\Room;
use App\Libraries\SMSLibrary;
use DB;
use App\Presenters\RoomPresenter;
use Carbon\Carbon;
use App\Entities\Agent\Agent;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;
use App\Entities\Property\Property;
use App\Entities\Room\RoomOwner;
use App\Http\Controllers\Admin\Component\Room\RoomSetterTrait;
use App\Http\Helpers\RegexHelper;
use App\Repositories\Property\PropertyRepository;
use App\Repositories\RoomRepository;
use App\Services\Owner\Kost\DataChecker\RoomAllotment;
use App\User;
use Exception;
use Illuminate\Support\Facades\DB as FacadesDB;

class AgentController extends Controller
{
    use RoomSetterTrait;

    protected $validatorMessage = [
        "name.required" => "Nama tidak boleh kosong",
        "address.required" => "Alamat tidak boleh kosong",
        "phone_number.required" => "No hp tidak boleh kosong"
    ];


    protected $inputTypes   = array("All", "Agen");
    protected $baseRoute    = '';
    protected $verifiedSorting = ["All", "Active - Not active", "New", "Last"];
    protected $agent_from =  ["All", "JKT"];
    protected $login_type   = 'admin';
    protected $itemPerPage  = 20;

    protected $indexRoute   = 'admin.room.index';
    protected $createRoute  = 'admin.room.create';
    protected $storeRoute   = 'admin.room.store';
    protected $editRoute    = 'admin.room.edit';
    protected $updateRoute  = 'admin.room.update';
    protected $deleteRoute  = 'admin.room.destroy';

    public function __construct()
    {
        View::share('contentHeader', 'Stories Management');
        View::share('user', Auth::user());
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Room::with(
            [
                'minMonth',
                'view_promote',
                'owners.user',
                'view_promote.premium_request.user',
                'apartment_project',
                'photo',
                'listing_reason' => function ($p) {
                    $p->orderBy('id', 'desc');
                },
                'hidden_by_thanos',
                'photo_by' => function ($t) {
                    $t->orderBy('id', 'desc');
                },
                'attachment',
                'level',
                'premium_cards',
                'facilities',
            ]
        );

        $agent_data = null;
        if ($request->filled('agent_from') AND $request->input('agent_from') == "1" AND $request->filled('agent') AND strlen($request->input('agent')) > 0) {
            $agent_data = Agent::where('phone_number', 'like', '%'.$request->input('agent').'%')
                                ->orWhere('name', 'like', '%'.$request->input('agent').'%')
                                ->orWhere('unique_code', $request->input('agent'))
                                ->first();
        }

        // Filter
        $query = $this->filterIndex($request, $query, $agent_data);

        //$telp_now = false;
        if ($request->filled('q') AND SMSLibrary::validateIndonesianMobileNumber($request->input('q'))) {
            $telp_now = true;
        }


        // Order By
        $orderByDate = array(
            'lc' => 'created_at',
            'fc' => 'created_at',
            'lu' => 'updated_at',
            'fu' => 'updated_at',
        );

        $orderByMost = array(
            'm' => 'mobile_view' ,
            'w' => 'web_view',
            'l' => 'like', // l stand for like
            's' => 'share',
            'c' => 'comment'
        );

        if ($request->filled('active') AND (int) $request->input('active') == 1) {
            $actibe  = (int) $request->input('active');
            $verifiedStatus = ["", "true","false"];
            
            //$queryOrder = "CASE WHEN is_active = 'true' THEN 1 ";
            //$queryOrder .= "WHEN is_active = 'false' THEN 2 ";
            //$queryOrder .= "ELSE 3 END";
            //$query->orderByRaw($queryOrder);

            $byy = $verifiedStatus[(int) $request->input('active')];
            $query = $query->orderBy(DB::raw('IF(is_active = '.$byy.', 0, 1)'));
        }        

        if ($request->filled('active') AND (int) $request->input('active') == 2) {
            $query = $query->orderBy('id', 'desc');
        } else if ($request->filled('active') AND (int) $request->input('active') == 3) {
            $query = $query->orderBy('id', 'asc');
        } else {
            $query->orderBy('id', 'desc');
        }

        $notlistedcount = 0;
        $unverifiedcount = 0;
        $verifiedcount = 0;

        if (strlen($request->input('owner_phone')) == 0) {
            $notlistedcount = $this->counterdata($request->all(), 'expired_phone', '1', $agent_data);
            $unverifiedcount = $this->counterdata($request->all(), 'is_active', 'false', $agent_data);
            $verifiedcount = $this->counterdata($request->all(), 'is_active', 'true', $agent_data);
        }

        $rowsRoom = $query->paginate($this->itemPerPage);

        $rooms = (new RoomPresenter('index'))->present($rowsRoom);

        $viewData = array(
            'boxTitle'        => 'List Agent Kost',
            'agent_from'      => $this->agent_from,
            'login_type'      => $this->login_type,
            'createRoute'     => $this->createRoute,
            'editRoute'       => $this->editRoute,
            'deleteAction'    => $this->deleteRoute,
            'rowsDesigner'    => $rowsRoom,
            'rowsRoom'        => $rooms['data'],
            'paginate'        => $rooms['meta']['pagination'],
            'searchUrl'       => url()->route($this->indexRoute),
            'startDate'       => $request->input('st'),
            'endDate'         => $request->input('en'),
            'redirectHere'    => \Request::url() . '?' . http_build_query($_GET),
            'sortLists'       => $this->getSortList(),
            'inputBy'         => $this->inputTypes,
            'notlisted'   => 0,
            'verified'    => 0,
            'unverified'    => 0,
            'unverifiedcount' => $unverifiedcount,
            'verifiedcount' => $verifiedcount,
            'notlistedcount' => $notlistedcount,
            'soringActive'    => $this->verifiedSorting,
            'telp_now'        => isset($telp_now) ? true : false,
        );

        ActivityLog::LogIndex(Action::AGENT);

        return view('admin.contents.agent.index', $viewData);
    }

    public function counterdata($request, $column, $value, $agent_data)
    {
        $room  = Room::where($column, $value);

        if (!is_null($agent_data) AND isset($request['agent_from']) AND $request['agent_from'] == '1') {
            $room = $room->where('agent_id', $agent_data->id);
        } else if (is_null($agent_data) AND isset($request['agent_from']) AND $request['agent_from'] == '1') {
            $room = $room->where('agent_id', '>', 0);
        } else if (isset($request['agent_from']) AND $request['agent_from'] == '0') {
            if (isset($request['agent']) AND is_numeric($request['agent'])) $room = $room->where('agent_phone', 'like', "%".trim($request['agent']."%"));
            else if (isset($request['agent'])) $room = $room->where('agent_name', 'like', '%'. $request['agent'].'%');
        }

        if (isset($request['from']) AND strlen($request['from']) > 0) {
            $from = Carbon::createFromFormat("d-m-Y", $request['from'])->setTime(0,0,0);
            $room = $room->where('created_at', '>', $from);
        }

        if (isset($request['to']) AND strlen($request['to']) > 0) {
            $to = Carbon::createFromFormat("d-m-Y", $request['to'])->addDay()->setTime(0,0,0);
            $room = $room->where('created_at', '<', $to);
        }

        return $room->count();
    }

        private function getSortList()
    {
        return array(
            'lc' => 'Last Created',
            'fc' => 'First Created',
            'lu' => 'Last Updated',
            'fu' => 'First Updated',
            'm'  => 'Most Mobile View',
            'w'  => 'Most Web View',
            'l'  => 'Most Likes',
            'c'  => 'Most Comments',
            's'  => 'Most Shares',
        );
    }

    private function filterIndex(Request $request, $query, $agent_data = null)
    {
        //if ($request->filled('agent')) {
          //  $query = $this->searchIndex($request->input('agent'), $query);
        //}

        if ($request->filled('input-type') && $request->input('input-type') && $request->input('input-type') == "1") {
            if ($request->input('input-type') == "6") {
               $query->whereNotNull('duplicate_from');
            } else {
                $query->where('agent_status', 'like', $this->inputTypes[$request->input('input-type')].'%');
            }
        }

        if (!is_null($agent_data) AND $request->input('agent_from') == '1') {
            $query = $query->where('agent_id', $agent_data->id);
        } else if (is_null($agent_data) AND $request->input('agent_from') == '1') {
            $query = $query->whereNotNull('agent_id');
        } else if ($request->input('agent_from') == '0' AND $request->input('input-type') == '1') {
            if ($request->filled('agent') AND is_numeric($request->input('agent'))) $query = $query->where('agent_phone', 'like', "%".trim($request->input('agent')."%"));
            else if ($request->filled('agent')) $query = $query->where('agent_name', 'like', '%'. $request->input('agent').'%');
        }

        if ($request->filled('owner_phone') AND strlen($request->input('owner_phone')) > 5) {
            $owner_phone = $request->input('owner_phone');
            $query = $query->where(function($p) use($owner_phone) {
                $p->where('owner_phone', 'like', '%'.$owner_phone.'%')
                    ->orWhere('manager_phone', 'like', '%'.$owner_phone.'%')
                    ->orWhere('name', 'like', '%'.$owner_phone.'%')
                    ->orWhere('id', 'like', '%'.$owner_phone.'%')
                    ->orWhere('song_id', 'like', '%'.$owner_phone.'%');
            });
        }

        if ($request->filled('from')) {
            $from = Carbon::createFromFormat("d-m-Y", $request->get('from'))->setTime(0,0,0);
            $query->where('created_at', '>=', $from);
        }

        if ($request->filled('to')) {
            $to = Carbon::createFromFormat("d-m-Y", $request->get('to'))->addDay()->setTime(0,0,0);
            $query->where('created_at', '<=', $to);
        }

        if($request->filled('room')) {
            $query = $query->where('song_id', $request->get('room'));
        }

        return $query;
    }

    private function searchIndex($keyword, $query)
    {
        // if ($keyword == 'recommended') {
        //     return $query->whereNotNull('recommended_at');
        // }

        // $room = Room::whereCityCode($keyword)->first();

        // if ($room != null) {
        //     return $query->where('id', $room->id);
        // }

        $room = Room::find($keyword);

        if ($room != null) {
            return $query->where('id', $room->id);
        }

        return $query->where(function($q) use ($keyword){
            $keyword = "%$keyword%";

            $q  ->where('name', 'like', $keyword)
                ->orWhere('phone', 'like', $keyword)
                ->orWhere('owner_phone', 'like', $keyword)
                ->orWhere('manager_phone', 'like', $keyword)
                ->orWhere('agent_phone', 'like', $keyword)
                ->orWhere('song_id', 'like', $keyword)
                ->orWhere('area_city', 'like', $keyword);
        });

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     *  Verify kost from agent menu verification
     *
     *  @param Request $request
     *  @param Request $id
     *  @param RoomRepository $roomRepository
     *  @param PropertyRepository $propertyRepository
     *  @param RoomAllotment $roomAllotment
     *
     *  @return JsonResponse
     */
    public function agentVerify(Request $request, $id, RoomRepository $roomRepository, PropertyRepository $propertyRepository, RoomAllotment $roomAllotment)
    {
        $room = Room::with('owners')->find($id);

        if (is_null($room) || !($room instanceof Room)) {
            return [
                'success' => false,
                'messages' => 'Room tidak ditemukan',
            ];
        }

        if (empty($request->property_id)) {
            return [
                'success' => false,
                'messages' => 'Nama Properi Tidak Boleh Kosong',
            ];
        }

        $owner = $room->owners()->first();
        if (is_null($owner)) {
            $owner = $roomRepository->createOwnerAndRoomOwner($room);
        }

        if (!$propertyRepository->isPropertyExist($request->property_id, $owner->user)) {
            return [
                'success' => false,
                'messages' => 'Pilih Nama Properti yang valid',
            ];
        }

        if ($request->has_multiple_type && empty($request->unit_type)) {
            return [
                'success' => false,
                'messages' => 'Tipe Kamar tidak boleh kosong',
            ];
        }

        if ($roomRepository->isUnitTypeExist($request->property_id, $request->unit_type, $room->id)) {
            return [
                'success' => false,
                'messages' => 'Tipe Kamar tidak boleh sama',
            ];
        }
        
        // Checking has_multiple_type
        if (!empty($request->unit_type) && !$request->has_multiple_type) {
            return [
                'success' => false,
                'messages' => 'Jika ingin mengisi Tipe Kamar maka Centang Harus diaktifkan',
            ];
        }

        // if ($room->photo_count == 0) {
        //     return [
        //         'success' => false,
        //         'messages' => 'Mohon lengkapi data foto kos dan pastikan telah sesuai',
        //     ];
        // }

        // $ungroupedCard = $room->cards()->where(['type' => 'image', 'group' => null])->first();
        // if (!is_null($ungroupedCard)) {
        //     return [
        //         'success' => false,
        //         'messages' => 'Ada Foto yang belum digrup silahkan Atur Foto terlebih dahulu',
        //     ];
        // }

        if (!$roomAllotment->check($room)) {

            $room->room_count = 1;
            $room->room_available = 1;
            $success = $room->save();

            if (!$success) {
                return [
                    'success' => false,
                    'messages' => 'Terjadi kesalahan saat melakukan verifikasi, silahkan refresh browser anda.',
                ];
            }
        }

        FacadesDB::beginTransaction();
        try {
            if ((int) $room->song_id == 0) {
                $room->generateSongId();
            }
            $room->property_id = $request->property_id;
            $room->unit_type = $request->unit_type ?? null;
            $room->save();
            
            $selectedProperty =  Property::find($request->property_id);
            if (!$selectedProperty->has_multiple_type && $request->has_multiple_type) {
                $selectedProperty->has_multiple_type = $request->has_multiple_type;
                $selectedProperty->save();
            }

            $this->verify($room->id);
            FacadesDB::commit();
            return [
                'success' => true,
                'messages' => 'Room has been Verified',
            ];
        } catch (\Exception $e) {
            FacadesDB::rollback();
            return [
                'success' => false,
                'messages' => 'Terjadi kesalahan saat melakukan verifikasi, silahkan refresh browser anda.',
            ];
        }
    }

    /**
     * Get listing owner properties by room id
     *
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function getRoomProperties($id, RoomRepository $roomRepository)
    {
        $room = Room::with(['owners', 'property'])->find($id);
        if($room->owners()->first() instanceof RoomOwner){
            $owner = $room->owners()->first()->user;
        }else{
            $owner = User::with('property')->ownerOnly()->where('phone_number', $room->owner_phone)->first();
            if($owner instanceof User){
                $roomRepository->createRoomOwner($room, $owner);
            }
        }
        $properties = $owner->property ?? [];

        $selectedProperty = $room->property;
        $selectedPropertyId = $selectedProperty instanceof Property ? $selectedProperty->id : null;
        $hasMultipleType = $selectedProperty instanceof Property ? $selectedProperty->has_multiple_type : false;

        return [
            'success' => true,
            'data' => $properties,
            'selected_property' => $selectedPropertyId,
            'has_multiple_type' => $hasMultipleType,
            'unit_type' => $room->unit_type,
        ];
    }

    /**
     * Add owner properties by room id
     *
     *  @param Request $request
     *  @param PropertyRepository $propertyRepository
     *  @param RoomRepository $roomRepository
     *
     *  @return JsonResponse
     */
    public function addRoomProperty(Request $request, PropertyRepository $propertyRepository, RoomRepository $roomRepository)
    {
        $room = Room::with('owners')->find($request->id);

        if (is_null($room) || !($room instanceof Room)) {
            return [
                'success' => false,
                'messages' => 'Room tidak ditemukan',
            ];
        }

        if (empty($request->name)) {
            return [
                'success' => false,
                'messages' => 'Nama Properti Tidak Boleh Kosong',
            ];
        }

        if (! preg_match(RegexHelper::validPropertyName(), $request->name)) {
            return [
                'success' => false,
                'messages' => 'The property name format is invalid.',
            ];
        }

        $owner = $room->owners()->first();
        if (is_null($owner)) {
            $owner = $roomRepository->createOwnerAndRoomOwner($room);
        } else {
            if ($propertyRepository->isPropertyNameExist($request->name, $owner->user)) {
                return [
                    'success' => false,
                    'messages' => 'Anda sudah memiliki nama properti yang sama',
                ];
            }
        }

        try {

            $property = new Property;
            $property->name = $request->name;
            $property->owner_user_id = $owner->user->id;
            $property->has_multiple_type = false;
            $success = $property->save();
            
            if (is_null($success)) 
            return [
                'success' => false,
                'messages' => 'Terjadi kesalahan saat menambahkan Nama Properti, atur Pemilik Kost agar menjadi owner',
            ];              
        } catch (\Exception $e) {
            return [
                'success' => false,
                'messages' => 'Terjadi kesalahan saat menambahkan Nama Properti, silahkan refresh browser anda.',
            ];
        }

        return [
            'success' => true,
            'messages' => 'Nama Properti berhasil ditambahkan.',
        ];
    }

    /**
     * Check if Propery Name is Exist
     *
     *  @param Request $request
     *  @param $id
     *  @param PropertyRepository $propertyRepository
     *
     *  @return JsonResponse
     */
    public function checkPropertyName(Request $request, $id, PropertyRepository $propertyRepository)
    {
        $room = Room::with('owners')->find($id);

        if (is_null($room)) {
            return [
                'success' => false,
                'messages' => 'Room tidak ditemukan',
            ];
        }

        $owner = $room->owners()->first();
        if (is_null($owner) || is_null($owner->user)) {
            return [
                'success' => true,
            ];
        }

        if (empty($request->name)) {
            return [
                'success' => false,
                'messages' => 'Nama Properti tidak boleh kosong',
            ];
        }

        if (! preg_match(RegexHelper::validPropertyName(), $request->name)) {
            return [
                'success' => false,
                'messages' => 'The property name format is invalid.',
            ];
        }

        $propertyId = empty($request->property_id) ? 0 : $request->property_id;
        if ($propertyRepository->isPropertyNameExist($request->name, $owner->user, $propertyId )) {
            return [
                'success' => false,
                'messages' => 'Anda sudah memiliki nama properti yang sama',
            ];
        }

        return [
            'success' => true,
        ];
    }

    /**
     * Check if Unit Type is Exist
     *
     *  @param Request $request
     *  @param $id
     *  @param RoomRepository $roomRepository
     *
     *  @return JsonResponse
     */
    public function checkUnitType(Request $request, $id, RoomRepository $roomRepository)
    {
        $room = Room::with('owners')->find($id);

        if (is_null($room)) {
            return [
                'success' => false,
                'messages' => 'Room tidak ditemukan',
            ];
        }

        $owner = $room->owners()->first();
        if (is_null($owner) || is_null($owner->user)) {
            return [
                'success' => true,
            ];
        }

        if (empty($request->name)) {
            return [
                'success' => false,
                'messages' => 'Tipe Kamar tidak boleh kosong',
            ];
        }

        if ($roomRepository->isUnitTypeExist($request->id_property, $request->name, $room->id)) {
            return [
                'success' => false,
                'messages' => 'Tipe Kamar tidak boleh sama',
            ];
        }

        return [
            'success' => true,
        ];
    }

    /**
     * Get detail property
     *
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function getDetailProperty($id)
    {
        $property = Property::find($id);

        return $property;
    }
}
