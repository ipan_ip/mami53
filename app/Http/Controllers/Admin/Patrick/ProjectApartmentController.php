<?php

namespace App\Http\Controllers\Admin\Patrick;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\Room\Element\Tag;
use Illuminate\Support\Facades\View;
use Auth;
use Validator;
use App\Entities\Apartment\ApartmentProject;
use App\Entities\Apartment\ApartmentProjectStyle;
use App\Entities\Generate\DummyProject;
use App\Http\Helpers\ApiHelper;
use App\Entities\Media\Media;
use DB;
use App\Libraries\AreaFormatter;

class ProjectApartmentController extends Controller
{

	protected $validationMessages = [
		'project_id.required'		=> 'Project ID harus diisi',
		'name.required'				=> 'Nama Project harus diisi',
		'name.max'					=> 'Nama Project Maksimum :max karakter',
		'name.min'					=> 'Nama Project Maksimum :min karakter',
		'address.max'				=> 'Alamat maksimal :max karakter',
		'description.max'			=> 'Deskripsi maksimal :max karakter',
		'latitude.required'			=> 'Latitude harus diisi',
		'latitude.numeric'			=> 'Latitude harus berupa angka',
		'longitude.required'		=> 'Longitude harus diisi',
		'longitude.numeric'			=> 'Longitude harus berupa angka',
		'subdistrict.max'			=> 'Subdistrik maksimal :max karakter',
		'area_city.max'				=> 'Area City maksimal :max karakter',
		'area_city.required'		=> 'Area City harus diisi',
		'phone_number_building.max'	=> 'Nomor Telepon Gedung maksimal :max karakter',
		'phone_number_marketing.max'=> 'Nomor Telepon Marketing maksimal :max karakter',
		'phone_number_other.max'	=> 'Nomor Telepon Lainnya maksimal :max karakter',
		'developer.max'				=> 'Pengembang maksimal :max karakter',
		'unit_count.numeric'		=> 'Jumlah Unit harus berupa angka',
		'floor_count.numeric'		=> 'Jumlah Lantai harus berupa angka',
		'size.numeric'				=> 'Luas harus berupa angka',
	];

	public function __construct()
	{
		View::share('contentHeader', 'Apartment Project Management');
        View::share('user', Auth::user());
	}

    public function add(Request $request)
	{
		$tags = Tag::where('type', 'fac_project')->get();

		$viewData = [
			'boxTitle'        	=> 'Edit Apartment Project',
			'tags'				=> $tags,
			'request'			=> $request->all(),
			'linkName'			=> str_replace(' ', '+', $request->input('name'))
		];

		return view('admin.contents.patrick.apartemen.project-edit', $viewData);
	}

	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'name'			=> 'required|min:5|max:190',
			'address'		=> 'max:250',
			'description'	=> 'nullable',
			'latitude'		=> 'required|numeric',
			'longitude'		=> 'required|numeric',
			'area_city'		=> 'max:250',
			'subdistrict'	=> 'max:250',
			'phone_number_building'	=> 'max:100',
			'phone_number_marketing'=> 'max:100',
			'phone_number_other'	=> 'max:100',
			'developer'				=> 'max:250',
			'unit_count'			=> 'nullable|numeric',
			'floor_count'			=> 'nullable|numeric',
			'size'					=> 'nullable|numeric',
		], $this->validationMessages);


		if($validator->fails()) {
			return redirect()->back()->with('errors', $validator->errors())->withInput($request->all());
		}

		$params = [
			'name'			=> $request->name,
			'address'		=> $request->address,
			'description'	=> $request->description,
			'latitude'		=> $request->latitude,
			'longitude'		=> $request->longitude,
			'area_city'		=> $request->area_city,
			'area_subdistrict'		=> $request->subdistrict,
			'tags'					=> $request->facilities,
			'phone_number_building'	=> $request->phone_number_building,
			'phone_number_marketing'=> $request->phone_number_marketing,
			'phone_number_other'	=> $request->phone_number_marketing,
			'developer'				=> $request->developer,
			'unit_count'			=> $request->unit_count,
			'floors'				=> $request->floor_count,
			'size'					=> $request->size,
			'images'				=> $request->images,
			'download'				=> empty($request->download) ? null : $request->download,
			'live'					=> empty($request->live) ? null : $request->live	
		];

		$this->saveNewApartmentProjectByAdmin($params);
		return redirect('admin/apartment/project')->with('message', 'Berhasil');
	}


	public function saveNewApartmentProjectByAdmin($params)
	{
		DB::beginTransaction();

		$areaType = array(
            'Kabupaten' => "",
            'Kota'      => "",
            'Kecamatan' => ""
            );

		$apartmentProject = new ApartmentProject;
		$apartmentProject->name 		= $params['name'];
		$apartmentProject->address 		= $params['address'];
		$apartmentProject->description 	= $params['description'];
		$apartmentProject->latitude 	= $params['latitude'];
		$apartmentProject->longitude 	= $params['longitude'];
		$apartmentProject->area_city 		= AreaFormatter::format($params['area_city']);
		$apartmentProject->area_subdistrict = AreaFormatter::format($params['area_subdistrict']);
		$apartmentProject->phone_number_building 	= $params['phone_number_building'];
		$apartmentProject->phone_number_marketing 	= $params['phone_number_marketing'];
		$apartmentProject->phone_number_other 		= $params['phone_number_other'];
		$apartmentProject->developer 				= $params['developer'];
		$apartmentProject->unit_count 				= $params['unit_count'];
		$apartmentProject->floor					= $params['floors'];
		$apartmentProject->size 					= $params['size'];
		if (!is_null($params['live'])) $apartmentProject->is_active = 1;
		$apartmentProject->save();

		$apartmentProject->generateApartmentCode();

		if (!is_null($params['tags'])) {
			$apartmentProject->tags()->sync($params['tags']);
		}

		if (!is_null($params['download'])) {
			$media = $this->downloadImageProject($params['images']);
			$this->styleProjectStore($apartmentProject, $media['id']);
		}

		$this->dummyInsert($apartmentProject);

		DB::commit();

		return $apartmentProject;
	}

	public function downloadImageProject($images)
    {
        $watermarking = true;
        if ($images == "http://songturu.mamikos.com/uploads/file/noimages.png") {
            $watermarking = false;
        }
    	$media = Media::postMedia($images, 'url', null, null, null, 'style_photo', $watermarking, null, false);
    	return $media;
    }

	public function dummyInsert($params)
	{
		$apartmentProjectDummy = new DummyProject();
		$apartmentProjectDummy->name 				= $params->name;
		$apartmentProjectDummy->address 			= $params->address;
		$apartmentProjectDummy->description 		= $params->description;
		$apartmentProjectDummy->latitude 			= $params->latitude;
		$apartmentProjectDummy->longitude 			= $params->longitude;
		$apartmentProjectDummy->area_city 			= $params->area_city;
		$apartmentProjectDummy->area_subdistrict 	= $params->area_subdistrict;
		$apartmentProjectDummy->phone_number_building 	= $params->phone_number_building;
		$apartmentProjectDummy->phone_number_marketing 	= $params->phone_number_marketing;
		$apartmentProjectDummy->phone_number_other 		= $params->phone_number_other;
		$apartmentProjectDummy->developer 				= $params->developer;
		$apartmentProjectDummy->unit_count 				= $params->unit_count;
		$apartmentProjectDummy->floor 					= $params->floors;
		$apartmentProjectDummy->size 					= $params->size;
		$apartmentProjectDummy->project_id 				= $params->id;
		$apartmentProjectDummy->project_code			= $params->project_code;
		$apartmentProjectDummy->save();
		return $apartmentProjectDummy;
	}

	public function styleProjectStore($project, $photoId)
	{
		if ($photoId == 0) return null;
		$style = new ApartmentProjectStyle;
		$style->apartment_project_id 	= $project->id;
		$style->photo_id 				= $photoId;
		$style->title 					= $project->name . '-cover';
		$style->save();
		return $style;
	}

}
