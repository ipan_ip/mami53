<?php

namespace App\Http\Controllers\Admin\Patrick;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Auth;
use App\Entities\Generate\DummyDataApartemen;
use App\Entities\Room\Element\Tag;
use App\Entities\Apartment\ApartmentProject;
use App\Entities\Room\Element\RoomPriceComponent;
use App\Entities\Room\Room;
use Validator;
use App\Http\Controllers\Admin\Component\Room\RoomSetterTrait;
use DB;
use App\Http\Helpers\ApiHelper;
use App\Entities\Media\Media;
use App\Entities\Room\Element\Card;
use Carbon\Carbon;
use App\Libraries\AreaFormatter;
use Cache;
use App\Entities\Room\RoomPriceType;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;
use App\Jobs\KostIndexer\IndexKostJob;

class ApartemenController extends Controller
{

    use RoomSetterTrait;

	protected $inputTypes   = array("Semua", "Agen", "Pemilik Kos", "Pengelola Kos", "Lainnya", "Anak Kos", "Kos Duplikat", 
                                'Pemilik Apartemen', 'Pengelola Apartemen', 'Agen Apartemen', 'Gojek', 'scrap');

	protected $unitTypeOptions = [
		'1-Room Studio',
		'1 BR',
		'2 BR',
		'3 BR',
		'4 BR',
		'Lainnya'
	];


    protected $apartmentValidationMessages = [
        'project_id.required_without'=>'Nama Apartment harus diisi',
        'project_name.required_without'=>'Nama Apartment harus diisi',
        'project_name.max'=>'Nama Apartment maksimal :max karakter',
        'unit_name.required'=>'Nama Unit harus diisi',
        'unit_name.max'=>'Nama Unit maksimal :max karakter',
        'unit_number.required' => 'Nomor Unit harus diisi',
        'unit_type.required'=>'Tipe Unit harus diisi',
        'unit_type_name.required_if' => 'Nama Tipe Unit harus diisi jika Tipe Unit adalah Lainnya',
        'unit_type_name.max'=>'Nama Tipe Unit maksimal :max karakter',
        'floor.required'=>'Lantai harus diisi',
        'floor.numeric'=>'Lantai harus berupa angka',
        'unit_size.required'=>'Luas Unit harus diisi',
        'unit_size.numeric'=>'Luas Unit harus berupa angka',
        'min_payment.required'=>'Minimal Pembayaran harus diisi',
        'min_payment.numeric'=>'Minimal Pembayaran harus berupa angka',
        'price_monthly.required'=>'Harga Sewa harus diisi',
        'price_monthly.numeric'=>'Harga Sewa harus berupa angka',
        'maintenance_price.numeric'=>'Biaya Maintenance harus berupa angka',
        'parking_price.numeric'=>'Biaya Tambahan Parkir harus berupa angka',
        'facilities.required'=>'Fasilitas unit harus diisi',
        'is_furnished.required'=>'Pilihan Furnished harus diisi',
        'fac_room.required_if'=>'Fasilitas Kamar harus diisi',
        'owner_name.required'=>'Nama pemilik/pengelola harus diisi',
        'owner_name.min'=>'Nama pemilik/pengelola minimal :min karakter',
        'owner_name.max'=>'Nama pemilik/pengelola maksimal :max karakter',
        'photo_cover.required' => 'Foto Cover harus diisi',
        'input_as.required' => 'Tipe penginput harus diisi',
        'input_name.required' => 'Nama Penginput harus diisi'
    ];

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if(!Auth::user()->can('access-patrick-apartment')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        View::share('contentHeader', 'Patrick Apartemen Management');
        View::share('user', Auth::user());
    }

    public function getListDummyApartemen(Request $request)
    {
    	$query = DummyDataApartemen::query()->orderBy('id', 'asc');
    
        $data = $query->paginate(20);

        $viewData = [
            'data'    => $data,
            'boxTitle'     => 'Patrick Management'
        ];

        ActivityLog::LogIndex(Action::PATRICK_APARTMENT);

        return view('admin.contents.patrick.apartemen.index', $viewData);
    }

    public function edit(Request $request, $id)
    {
    	$data = DummyDataApartemen::where('id', $id)->where('is_live', 0)->first();

    	if (is_null($data)) return redirect()->back()->with('message','Apartemen sudah live');

		$unitTypeOptions = $this->unitTypeOptions;

		$minPaymentOptions = Tag::where('type', 'keyword')->get()->pluck('name', 'id');
		$facilityOptions = Tag::where('type', '<>', 'keyword')
							->where('type', '<>', 'fac_project')
							->get()->pluck('name', 'id');

		$inputAsOptions = ['scrap'];

		$viewData = array(
            'boxTitle'        	=> 'Add Apartment Unit',
            'typeOptions'		=> $unitTypeOptions,
            'minPaymentOptions'	=> $minPaymentOptions,
            'facilityOptions'	=> $facilityOptions,
            'inputAsOptions'	=> $inputAsOptions,
            'data'				=> $data
        );

        return view("admin.contents.patrick.apartemen.edit", $viewData);
    }

    public function getNameProject(Request $request)
    {
    	$project = ApartmentProject::where('name', 'LIKE', '%'.$request->input('name').'%')->limit(15)->get();

        $status = true;
        if (count($project) < 1) $status = false;

    	return array("status" => $status, "data" => $project);
    }

    public function updateTempApartemenDummy($request)
    {
        DB::beginTransaction();
        $dummy = DummyDataApartemen::where('id', $request['id_dummy'])->first();
        $dummy->unit_type   = $request['unit_type'];
        $dummy->unit_number = $request['unit_number'];
        $dummy->name        = $request['unit_name'];
        $dummy->floor       = $request['floor'];
        $dummy->size        = $request['unit_size'];
        $dummy->price       = $request['price_monthly'];
        $dummy->phone_number= $request['owner_phone'];
        $dummy->save();
        DB::commit();
        return $dummy;
    }

    public function storeUnit(Request $request)
    {
        $this->updateTempApartemenDummy($request->all());
        if ($request->filled('save_temp')) {
            return redirect()->back()->withInput($request->all());
        }
        
        $projectId = $request->input('project_id');
        $apartmentProject = ApartmentProject::find($projectId);

        $validator = Validator::make($request->all(), 
            [
                'unit_name'         => 'required|max:250',
                'unit_number'       => 'required',
                'unit_type'         => 'required',
                'unit_type_name'    => 'nullable|required_if:unit_type,Lainnya|max:100',
                'floor'             => 'numeric',
                'unit_size'         => 'numeric',
                'min_payment'       => 'nullable|numeric',
                'price_monthly'     => 'required|numeric',
                'maintenance_price' => 'nullable|numeric',
                'parking_price'     => 'nullable|numeric',
                'owner_name'        => 'nullable|min:1|max:100',
                'input_as'          => 'required',
                'input_name'        => 'required'
            ], $this->apartmentValidationMessages);

        $validator->after(function($validator) use ($request, $apartmentProject) {
            if (is_null($apartmentProject)) {
                $validator->errors()->add('message', 'Project id harus berupa angka dan tidak boleh kosong');
            }

            if($request->project_id > 0) {
                $unitNumberExist = Room::where('apartment_project_id', $apartmentProject->id)
                                        ->where('unit_number', $request->unit_number)
                                        ->first();

                if($unitNumberExist) {
                    $validator->errors()->add('unit_number', 'Nomor Unit telah digunakan sebelumnya');
                }
            }
        });

        if($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput($request->all());
        }

        $liveOn = false;
        if ($request->filled('live')) $liveOn = true;

        $params = [
            'name'         =>$request->unit_name,
            'unit_number'  =>$request->unit_number,
            'price_monthly'=>$request->price_monthly,
            'floor'        =>$request->floor,
            'unit_type'    =>$request->unit_type,
            'unit_type_name'=>$request->unit_type_name,
            'unit_size'     =>$request->unit_size,
            'facilities'    =>$request->facilities,
            'min_payment'   =>!is_null($request->min_payment) ? [$request->min_payment] : [],
            'owner_name'    =>$request->owner_name,
            'owner_phone'   =>$request->owner_phone,
            'password'      =>$request->password,
            'photos'            => [
                'cover' => $request->photo_cover,
                'bedroom' => $request->photo_bedroom,
                'bath' => $request->photo_bathroom
            ],
            'input_as'      =>$request->input_as,
            'input_name'    =>$request->input_name,
            'furnished'     =>empty($request->furnished) ? 0 : 1,
            'lives' => $liveOn,
            'price_type' => $request->price_type,
        ];

        $room = $this->saveNewApartmentUnitByAdmin($apartmentProject, $params, $request->all());

        if ($room) {
            DummyDataApartemen::where('id', $request->input('id_dummy'))->update(["is_live" => 1]);
            //$this->updateTempApartemenDummy($request->all());
        }

        if ($request->filled('live')) {
            if ($apartmentProject->is_active == 1) {
                $this->verify($room->id, false);
            }
        }

        if ($request->filled('is_duplicate')) {
            $url = 'admin/room/'.$room->id.'/edit?ref=patrick';
            return redirect($url)->with('message', "Duplicate form");
        }

        ActivityLog::LogUpdate(Action::PATRICK_APARTMENT, $apartmentProject->id);

        return redirect('admin/patrick/apartemen/list')->with('message', 'Unit berhasil ditambahkan');
    }

    public function saveNewApartmentUnitByAdmin(ApartmentProject $apartmentProject, $params, $request)
    {
        DB::beginTransaction();

        $areaType = array(
            'Kabupaten' => "",
            'Kota'      => "",
            'Kecamatan' => ""
            );

        $room = new Room;
        $room->apartment_project_id = $apartmentProject->id;
        $room->name                 = ApiHelper::removeEmoji($params['name']);
        $room->unit_number          = $params['unit_number'];
        $room->address              = $apartmentProject->address;
        $room->latitude             = $apartmentProject->latitude;
        $room->longitude            = $apartmentProject->longitude;
        $room->floor                = $params['floor'];
        $room->unit_type            = $params['unit_type'] != 'Lainnya' ? $params['unit_type'] : $params['unit_type_name'];
        $room->gender               = 0; // set gender to campur
        $room->room_available       = 1; // room available is always 1
        $room->room_count           = 1;
        $room->status               = 1;
        $room->is_active            = 'false';
        $room->is_indexed           = 0;
        $room->is_promoted          = 'false';
        $room->area_city            = AreaFormatter::format($apartmentProject->area_city);
        $room->area_subdistrict     = AreaFormatter::format($apartmentProject->area_subdistrict);
        $room->area_big = $apartmentProject->area_city != '' ? AreaFormatter::format($apartmentProject->area_city) : '';

        $room->owner_name   = $params['owner_name'];
        $room->owner_phone  = $params['owner_phone'];
        $room->agent_status = $params['input_as'];
        $room->agent_name   = $params['input_name'];
        $room->description  = $request['description'];
        
        $room->expired_phone    = 0;
        $room->is_booking       = 0;
        $room->price_daily      = 0;
        $room->price_weekly     = 0;
        
        $price_monthly = $params['price_monthly'];
        if ($params['price_type'] == 'usd') {
            $room->price_monthly_usd = $price_monthly;
            $usdPrice = Cache::get('currencyusd');
            if (is_null($usdPrice)) {
                $usdPrice = RoomPriceType::getOnePriceType('usd');
            }
            $price_monthly = $price_monthly * $usdPrice;
        }
        $room->price_monthly    = $price_monthly;
        $room->price_yearly     = 0;
        $room->Furnished        = $params['furnished'];
        // $room->agent_status = $params['input_as'];
        $room->size = $params['unit_size'];
        if ($params['lives']) $room->live_at = date('Y-m-d H:i:s');
        
        $photoIdAvailable = false;
        $photo_count = 0;
        if (isset($request['download']) AND !empty($request['images'])) {
            $media = $this->downloadImage($request['images']);
            if (isset($media['id'])) $room->photo_id = $media['id'];
            $photoIdAvailable = true;
            $photo_count = 1;
        }

        $room->kost_updated_date = Carbon::now()->format('Y-m-d H:i:s');
        $room->add_from = 'desktop';
        $room->photo_count = $photo_count;
        $room->updateSortScore();
        $room->save();

        // generate room code
        $room->generateApartmentUnitCode();

        // generate songId
        $room->generateSongId();

        if ($photoIdAvailable) {
            $renameImage = $this->renameImage($room);
        }

        // input price component
        if(isset($params['maintenance_price']) && $params['maintenance_price'] > 0) {
            $this->savePriceComponent($room, RoomPriceComponent::PRICE_NAME_MAINTENANCE, 'Biaya Maintenance', $params['maintenance_price']);
        }

        if(isset($params['parking_price']) && $params['parking_price'] > 0) {
            $this->savePriceComponent($room, RoomPriceComponent::PRICE_NAME_PARKING, 'Biaya Tambahan Parkir Mobil', $params['parking_price']);
        }

        // save tags
        if (is_null($params['facilities'])) {
            $inputTags = $params['min_payment'];
        } else {
            $inputTags = array_merge($params['facilities'], $params['min_payment']);
        }
        
        $room->syncTags($inputTags);
        DB::commit();

        return $room;
    }

    public function downloadImage($images)
    {
        $media = Media::postMedia($images, 'url', null, null, null, 'style_photo', true, null, false);
        return $media;
    }

    public function renameImage($room)
    {
        $Style              = new Card();
        $Style->designer_id = $room->id;
        $Style->description = "bangunan-tampak-depan-cover";
        $Style->photo_id    = $room->photo_id;
        $Style->save();

        return $Style;
    }

    public function savePriceComponent(Room $room, $price_name, $price_label, $price_reguler, $sale_price =0)
    {
        $priceComponent = new RoomPriceComponent;
        $priceComponent->price_name = $price_name;
        $priceComponent->price_label = $price_label;
        $priceComponent->designer_id = $room->id;
        $priceComponent->price_reguler = $price_reguler;
        $priceComponent->sale_price = $sale_price;
        $priceComponent->save();

        return $priceComponent;
    }

    public function checkPhoneKostTemp(Request $request)
    {
        $data = null;
        $phone = null;
        if ($request->filled('phone')) {
            $data = DummyDataApartemen::where('phone_number', $request->input('phone'))->orWhere('description', 'LIKE', '%'+$request->input('phone')+'%')->get();
            $phone = $request->input('phone');
        }

        $viewData = [
            'data'          => $data,
            'phone'         => $phone,
            'boxTitle'      => 'Patrick Management'
        ];

        return view('admin.contents.patrick.apartemen.check-phone', $viewData);
    }

    public function fromGoogle(Request $request)
    {
        $word = "apartemen";
        if ($request->filled("word")) $word = $request->input('word');
        
        $viewData = [
            "word"     => $word,
            'data'     => null,
            'status'   => false,
            'request'  => $request->all(),
            'boxTitle' => 'Patrick Google Management'
        ];

        return view('admin.contents.patrick.apartemen.google', $viewData);
    }

    public function searchFromGoogle(Request $request)
    {
        $word = str_replace(' ', '+', $request->input('word'));
        //dd($word);
        $urlGoogleClean = "https://maps.googleapis.com/maps/api/place/nearbysearch/json";
        $keyGoogle      = "key=AIzaSyB8gCZrWOXDcqOmrdNJtci8k1ZNcJ1EZAg";

        if ($request->filled('pagetoken')) {
            $urlGoogle = file_get_contents($urlGoogleClean."?pagetoken=".$request->input('pagetoken')."&".$keyGoogle);
        } else {
            $urlGoogle = file_get_contents($urlGoogleClean."?location=".$request->input('latitude').",".$request->input('longitude')."&radius=".$request->input('radius')."&type=apartemen&keyword=".$word."&".$keyGoogle);
        }
        
        $data = json_decode($urlGoogle, true);
        $status = false;
        if ($data['status'] == 'OK') $status = true;

        $nextPage = null;
        if (isset($data['next_page_token'])) $nextPage = $data['next_page_token'];

        $viewData = [
            "data"     => $data,
            "status"   => $status,
            "next"     => $nextPage,
            "request"  => $request->all(),
            "boxTitle" => "Result Data",
            "word"     => $request->input('word')
        ];

        return view('admin.contents.patrick.apartemen.google', $viewData);
    }
}
