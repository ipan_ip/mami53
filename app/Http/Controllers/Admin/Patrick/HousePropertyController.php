<?php

namespace App\Http\Controllers\Admin\Patrick;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Auth;
use App\Entities\Generate\DummyHouseProperty;
use App\Entities\Room\Element\Tag;
use Validator;
use App\Entities\HouseProperty\HouseProperty;
use App\Libraries\AreaFormatter;
use App\Entities\Media\Media;
use App\Entities\HouseProperty\HousePropertyMedia;
use DB;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;

class HousePropertyController extends Controller
{
    protected $validatorMessages = [

    ];

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if(!Auth::user()->can('access-patrick-kost')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        View::share('contentHeader', 'Agent Images Management');
        View::share('user', Auth::user());
    }

    public function getList(Request $request)
    {
        $query = DummyHouseProperty::query()->orderBy('id', 'asc');
    
        $data = $query->paginate(20);

        $viewData = [
            'data'    => $data,
            'boxTitle'     => 'Patrick Management'
        ];

        ActivityLog::LogIndex(Action::PATRICK_HOUSE);

        return view('admin.contents.patrick.house_property.index', $viewData);
    }

    public function getEdit(Request $request, $id)
    {
        $housePropertyDummy = DummyHouseProperty::where('status', 'waiting')->where('id', $id)->first();
        
        if (is_null($housePropertyDummy)) {
            return back()->with('message', 'Data tidak ditemukan');
        }

        $tags = Tag::where('type', '<>', 'fac_project')->get();
        $viewData = [
            "data" => $housePropertyDummy,
            "tag" => $tags,
            "boxTitle" => "Edit ".$housePropertyDummy->name
        ];
        return view('admin.contents.patrick.house_property.edit', $viewData);
    }

    public function storeHouseProperty(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:10',
            'size' => 'nullable',
            'price_daily' => 'nullable|numeric',
            'price_weekly' => 'nullable|numeric',
            'price_monthly' => 'nullable|numeric',
            'price_yearly' => 'nullable|numeric',
            'address' => 'required|max:200',
            'latitude' => 'required',
            'longitude' => 'required',
            'area_city' => 'required',
            'area_subdistrict' => 'required',
            'bed_room' => 'numeric',
            'bed_total' => 'numeric',
            'guest_max' => 'numeric',
            'bath_total' => 'numeric',
            'parking' => 'required',
            'other_cost_information' => 'nullable'
        ], $this->validatorMessages);

        $validator->after(function($validator) use ($request) {
            if($request->name != '') {
                $roomExist = HouseProperty::where('name', $request->name)->first();
                if($roomExist) {
                    $validator->errors()->add('name', 'Nama sudah pernah digunakan sebelumnya');
                }
            }
        });

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        DB::beginTransaction();
        $this->dummyUpdate($request->all(), $id);
        $this->insertToHouseProperty($request->all());
        DB::commit();

        ActivityLog::LogUpdate(Action::PATRICK_HOUSE, $id);
        
        return redirect('/admin/patrick/house_property/list')->with(["message", "Success"]);
    }

    public function insertToHouseProperty($request)
    {
        $houseProperty = new HouseProperty();
        $houseProperty->name = $request['name'];
        $houseProperty->type = $request['type'];
        $houseProperty->address = $request['address'];
        $houseProperty->latitude = $request['latitude'];
        $houseProperty->longitude = $request['longitude'];
        if (strlen($request['area_city']) > 0) {
            $houseProperty->area_city = $this->areaCityFormatter($request['area_city']);
            $houseProperty->area_big = $request['area_city'];
        }
        $houseProperty->area_subdistrict = $request['area_subdistrict'];
        $houseProperty->price_daily = $request['price_daily'];
        $houseProperty->price_weekly = $request['price_weekly'];
        $houseProperty->price_monthly = $request['price_monthly'];
        $houseProperty->price_yearly = $request['price_yearly'];
        $houseProperty->bed_room = $request['bed_room'];
        $houseProperty->size = $request['size'];
        $houseProperty->guest_max = $request['guest_max'];
        $houseProperty->bed_total = $request['bed_total'];
        $houseProperty->bath_total = $request['bath_total'];
        $houseProperty->parking = $request['parking'];
        $houseProperty->other_cost_information = $request['other_cost_information'];
        $houseProperty->other_information = $request['other_information'];
        $houseProperty->description = $request['description'];
        $houseProperty->owner_name = $request['owner_name'];
        $houseProperty->owner_phone = $request['owner_phone'];
        $houseProperty->manager_name = $request['manager_name'];
        $houseProperty->manager_phone = $request['manager_phone'];
        $houseProperty->agent_name = $request['agent_name'];
        $houseProperty->live_at = date('Y-m-d H:i:s');
        $houseProperty->floor = $request['floor'];
        $houseProperty->building_year = isset($request['building_year']) ? $request['building_year'] : Null;
        $houseProperty->is_available = $request['is_available'];
        if (isset($request['live'])) {
            $houseProperty->is_active = 1;
        }
        $houseProperty->song_id = HouseProperty::generateSongId();
        $houseProperty->save();

        if (isset($request['download']) AND isset($request['images'])) {
            $postOptions = [
                        "identifier" => $houseProperty->id, 
                        "room_type" => "villa",
                        "from" => "agent"
                    ];
            $watermarking = true;
            $media = Media::postMedia($request['images'], 'url', null, null, null, 'style_photo', $watermarking, null, false, $postOptions);
            $cover = $this->mediaInsert($media, $houseProperty);
            $houseProperty->cover_id = $cover->id;
            $houseProperty->save();
        }

        if (isset($request['concern_ids'])) {
            if(count($request['concern_ids']) > 0) {
                $houseProperty->house_property_tags()->sync($request['concern_ids']);
            }
        }
        return $houseProperty;
    }

    public function mediaInsert($media, $house_property)
    {
        $housePropertyMedia = new HousePropertyMedia();
        $housePropertyMedia->house_property_id = $house_property->id;
        $housePropertyMedia->description = $house_property->name."-bangunan-tampak-depan-cover";
        $housePropertyMedia->upload_identifier = $media['upload_identifier_type'];
        $housePropertyMedia->upload_identifier_type = $media['upload_identifier_type'];
        $housePropertyMedia->file_name = $media['file_name'];
        $housePropertyMedia->file_path = $media['file_path'];
        $housePropertyMedia->type = $media['type'];
        $housePropertyMedia->media_format = $media['media_format'];
        $housePropertyMedia->save();
        return $housePropertyMedia;
    }

    public function areaCityFormatter($area_city)
    {
        return AreaFormatter::format($area_city);
    }

    public function dummyUpdate($request, $id)
    {
        $dummyHouseProperty = DummyHouseProperty::where('id', $id)->firstOrFail();
        $dummyHouseProperty->name = $request['name'];
        $dummyHouseProperty->address = $request['address'];
        $dummyHouseProperty->latitude = $request['latitude'];
        $dummyHouseProperty->longitude = $request['longitude'];
        if (strlen($request['area_city']) > 0) {
            $dummyHouseProperty->area_city = $this->areaCityFormatter($request['area_city']);
            $dummyHouseProperty->area_big = $request['area_city'];
        }
        $dummyHouseProperty->area_subdistrict = $request['area_subdistrict'];
        //$dummyHouseProperty->area_big = $
        $dummyHouseProperty->price_daily = $request['price_daily'];
        $dummyHouseProperty->price_weekly = $request['price_weekly'];
        $dummyHouseProperty->price_monthly = $request['price_monthly'];
        $dummyHouseProperty->price_yearly = $request['price_yearly'];
        $dummyHouseProperty->bed_room = $request['bed_room'];
        $dummyHouseProperty->size = $request['size'];
        $dummyHouseProperty->guest_max = $request['guest_max'];
        $dummyHouseProperty->bed_total = $request['bed_total'];
        $dummyHouseProperty->bath_total = $request['bath_total'];
        $dummyHouseProperty->parking = $request['parking'];
        $dummyHouseProperty->other_cost_information = $request['other_cost_information'];
        $dummyHouseProperty->other_information = $request['other_information'];
        $dummyHouseProperty->description = $request['description'];
        $dummyHouseProperty->owner_name = $request['owner_name'];
        $dummyHouseProperty->owner_phone = $request['owner_phone'];
        $dummyHouseProperty->manager_name = $request['manager_name'];
        $dummyHouseProperty->manager_phone = $request['manager_phone'];
        $dummyHouseProperty->agent_name = $request['agent_name'];
        $dummyHouseProperty->status = 'live';
        $dummyHouseProperty->save();
        return $dummyHouseProperty;
    }

    public function import(Request $request)
    {
        $request->validate([
            'json_file' => 'required|file|max:4024',
        ]);

        $file = $request->file('json_file')->getRealPath();
        $fileContent = json_decode(file_get_contents($file));

        DB::beginTransaction();
        foreach ($fileContent as $key => $value) {
            $dummyHouseProperty = new DummyHouseProperty();
            $dummyHouseProperty->name = isset($value->name) ? $value->name: "";
            $dummyHouseProperty->address = isset($value->address) ? $value->address: "";
            $dummyHouseProperty->latitude = isset($value->latitude) ? $value->latitude: "";
            $dummyHouseProperty->longitude = isset($value->longitude) ? $value->longitude: "";
            $dummyHouseProperty->area_subdistrict = isset($value->area_subdistrict) ? $value->area_subdistrict: "";
            $dummyHouseProperty->area_city = isset($value->area_city) ? $value->area_city: "";
            $dummyHouseProperty->area_big = isset($value->area_big) ? $value->area_big: "";
            $dummyHouseProperty->price_daily = isset($value->price_daily) ? $value->price_daily: "";
            $dummyHouseProperty->price_weekly = isset($value->price_weekly) ? $value->price_weekly: "";
            $dummyHouseProperty->price_monthly = isset($value->price_monthly) ? $value->price_monthly: "";
            $dummyHouseProperty->price_yearly = isset($value->price_yearly) ? $value->price_yearly: "";
            $dummyHouseProperty->bed_room = isset($value->bed_room) ? $value->bed_room: "";
            $dummyHouseProperty->size = isset($value->size) ? $value->size: "";
            $dummyHouseProperty->guest_max = isset($value->guest_max) ? $value->guest_max: "";
            $dummyHouseProperty->bed_total = isset($value->bed_total) ? $value->bed_total: "";
            $dummyHouseProperty->bath_total = isset($value->bath_total) ? $value->bath_total: "";
            $dummyHouseProperty->type = isset($value->type) ? $value->type: "";
            $dummyHouseProperty->parking = isset($value->parking) ? $value->parking: "";
            $dummyHouseProperty->other_cost_information = isset($value->other_cost_information) ? $value->other_cost_information: "";
            $dummyHouseProperty->other_information = isset($value->other_information) ? $value->other_information: "";
            $dummyHouseProperty->description = isset($value->description) ? $value->description: "";
            $dummyHouseProperty->owner_name = isset($value->owner_name) ? $value->owner_name: "";
            $dummyHouseProperty->owner_phone = isset($value->owner_phone) ? $value->owner_phone: "";
            $dummyHouseProperty->manager_name = isset($value->manager_name) ? $value->manager_name: "";
            $dummyHouseProperty->manager_phone = isset($value->manager_phone) ? $value->manager_phone: "";
            $dummyHouseProperty->agent_name = isset($value->agent_name) ? $value->agent_name: "";
            $dummyHouseProperty->status = 'waiting';
            $dummyHouseProperty->web_url = isset($value->web_url) ? $value->web_url: "";
            $dummyHouseProperty->cover = isset($value->cover) ? $value->cover: "";
            $dummyHouseProperty->save();

            ActivityLog::LogCreate(Action::PATRICK_HOUSE, $dummyHouseProperty->id);
        }

        DB::commit();

        return back()->with('message', 'Berhasil import data');
    }
}
