<?php

namespace App\Http\Controllers\Admin\Patrick;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Auth;
use DB;
use App\Entities\Generate\DummyData;
use App\Entities\Room\Element\Tag;
use App\Entities\Room\Room;
use Carbon\Carbon;
use App\Entities\Media\Media;
use App\Entities\Room\Element\Card;
use App\User;
use App\Libraries\CSVParser;
use Validator;
use App\Http\Helpers\ApiResponse as Api;
use App\Entities\Generate\ListingReason;
use App\Libraries\AreaFormatter;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;

class ProjectController extends Controller
{
	protected $inputTypes   = array("Semua", "Agen", "Pemilik Kos", "Pengelola Kos", "Lainnya", "Anak Kos", "Kos Duplikat", 
                                'Pemilik Apartemen', 'Pengelola Apartemen', 'Agen Apartemen', 'Gojek', 'scrap', 'scrap google');
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if(!Auth::user()->can('access-patrick-kost')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        View::share('contentHeader', 'Agent Images Management');
        View::share('user', Auth::user());
    }

    public function index(Request $request)
    {
    	$query = DummyData::query()->orderBy('id', 'asc');
    
        $data = $query->paginate(20);

        $viewData = [
            'data'    => $data,
            'boxTitle'     => 'Patrick Management'
        ];

        ActivityLog::LogIndex(Action::PATRICK_KOST);

        return view('admin.contents.patrick.index', $viewData);
    }

    public function edit(Request $request, $id)
    {
    	$data = DummyData::where('id', $id)->where('is_live', 0)->first();

    	if (is_null($data)) return redirect()->back()->with('message','Kost sudah live');

    	$rowsConcern = Tag::where('type', '<>', 'fac_project')->get();

        $genderOptions = [
            0 => 'Campur',
            1 => 'Pria',
            2 => 'Wanita'
        ];

    	$viewData = [
    		'boxTitle'       => 'Edit data',
    		'agent_status' => $this->inputTypes,
    		'data'      => $data,
    		'tag' 		=> $rowsConcern,
    		'gender' 	=> $genderOptions 
    	];

    	return view('admin.contents.patrick.edit', $viewData);
    }

    public function LiveData(Request $request, $id)
    {
        //dd($request->all());

        if ($request->filled('is_not_listing')) {
            $reason = ListingReason::where('listing_id', $id)->where('from', 'dummy')->first();

            if (is_null($reason)) {
                $reason = new ListingReason();
                $reason->from = "dummy";
                $reason->listing_id = $id;
                $reason->content    = $request->input('content_not_listing');
                $reason->save();

                $dummy = DummyData::where('id', $id)->first();
                $dummy->checking = 'yes';
                $dummy->save();

                return redirect()->back()->with('message', 'Berhasil');
            }

            $reason->content = $request->input('content_not_listing');
            $reason->save();
            return redirect()->back()->with('message', 'Berhasil');
        }

    	$validator = validator($request->all(), array(
            'name' 				=> 'required|max:250',
            'price_daily' 		=> 'numeric',
            'price_weekly' 		=> 'numeric',
            'price_monthly' 	=> 'required|numeric',
            'price_yearly' 		=> 'numeric',
            'room_count' 		=> 'required|numeric',
            'room_available' 	=> 'required|numeric',
            'address' 			=> 'required',
            'longitude' 		=> 'required|numeric',
            'latitude' 			=> 'required|numeric',
            'owner_name' 		=> 'required_without:manager_name',
            'owner_phone' 		=> 'required_with:owner_name',
            'manager_name' 		=> 'required_without:owner_name',
            'manager_phone'   	=> 'required_with:manager_name',
            'agent_name'      	=> 'required'
        ));

        $validator->after(function($validator) use ($request) {
            if($request->name != '') {
                $roomExist = Room::where('name', $request->name)->first();
                if($roomExist) {
                    $validator->errors()->add('name', 'Nama sudah pernah digunakan sebelumnya');
                }
            }
        });

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        DB::beginTransaction();

        $room = new Room();
        //check for indexed type
        if ($request->input('indexed') == null) {
            $room->is_indexed = 0;
        } else {
            $room->is_indexed = 1;
        }

        if (!$request->input('price_daily') || $request->input('price_daily') == '-' || !is_numeric($request->input('price_daily'))) {
            $room->price_daily = 0;
        } else {
            $room->price_daily = $request->input('price_daily');
        }

        if (!$request->input('price_monthly') || $request->input('price_monthly') == '-' || !is_numeric($request->input('price_monthly'))) {
            $room->price_monthly = 0;
        } else {
            $room->price_monthly = $request->input('price_monthly');
        }

        if (!$request->input('price_weekly') || $request->input('price_weekly') == '-' || !is_numeric($request->input('price_weekly'))) {
            $room->price_weekly = 0;
        } else {
            $room->price_weekly = $request->input('price_weekly');
        }

        if (!$request->input('price_yearly') || $request->input('price_yearly') == '-' || !is_numeric($request->input('price_yearly'))) {
            $room->price_yearly = 0;
        } else {
            $room->price_yearly = $request->input('price_yearly');
        }

        $data = array(
            'Kabupaten' => "",
            'Kota'      => "",
            'Kecamatan' => ""
        );

        $area_big = NULL;
        $area_city = NULL;
        if (!empty($request->input('area_city'))) {
            $room->area_city = AreaFormatter::format($request->input('area_city'));
            $room->area_big = $area_city;
        }

        $area_subdistrict = NULL;
        if (!empty($request->input('area_subdistrict'))) {
            $room->area_subdistrict =  AreaFormatter::format($request->input('area_subdistrict'));
        }
        
        $agent_status = null; 
        if (!empty($request->input('agent_status'))) {
            if ($request->input('agent_status') == 'Semua') {
                $room->agent_status = null;
            } else {
                $room->agent_status = $request->input('agent_status');
            }
        }

        $room->name             = $request->input('name');
        $room->size             = $request->input('size');
        $room->gender           = $request->input('gender');
        $room->floor            = $request->input('floor');
        $room->price            = $request->input('price_monthly');
        $room->address          = $request->input('address');
        $room->longitude        = $request->input('longitude');
        $room->latitude         = $request->input('latitude');
        $room->description      = $request->input('description');
        $room->fac_room_other   = substr($request->input('fac_room_other'), 0, 500);
        $room->fac_bath_other   = substr($request->input('fac_bath_other'), 0, 500);
        $room->fac_share_other  = substr($request->input('fac_share_other'), 0, 500);
        $room->fac_near_other   = substr($request->input('fac_near_other'), 0, 500);
        $room->price_remark     = substr($request->input('price_remark'), 0, 500);
        $room->remark           = $request->input('remarks');
        $room->agent_name       = $request->input('agent_name');
        $room->room_count       = $request->input('room_count');
        $room->room_available   = $request->input('room_available');
        $room->status           = $request->input('room_available') > 0 ? 2 : 0;
        $room->remark           = $request->input('remarks');
        $room->animal           = $request->input('animal');
        $room->owner_name       = $request->input('owner_name');
        $room->owner_phone      = $request->input('owner_phone');
        $room->manager_name     = $request->input('manager_name');
        $room->manager_phone    = $request->input('manager_phone');

	    $room->office_phone       	= '';
	    $room->kost_updated_date 	= date('Y-m-d H:i:s');
	    
	    $room->guide            	= $request->input('guide');
	    $room->kost_updated_date	= Carbon::now()->format('Y-m-d H:i:s');
	    $room->is_promoted      	= 'false';
	    $room->is_booking       	= 0;

        if ($request->filled('is_verify_address')) $room->is_verified_address = 1;
        if ($request->filled('is_verify_phone')) $room->is_verified_phone = 1;

	    $photoIdAvailable = false;
        $photo_count = 0;
	    if ($request->filled('download') AND !empty($request->filled('images'))) {
	    	$media = $this->downloadImage($request->input('images'));
	    	if (isset($media['id'])) $room->photo_id = $media['id'];
	    	$photoIdAvailable = true;
            $photo_count = 1;
	    }
        if ($request->filled('live')) $room->live_at = date('Y-m-d H:i:s');
        $room->photo_count = $photo_count;
        $room->updateSortScore();
	    $room->save();

	    if ($request->filled('live')) {
	    	Room::verifyDesigner($room, true, null);
		}

		if ($photoIdAvailable) {
			$renameImage = $this->renameImage($room);
		}

		// storing tag
		$designerTag = $request->get('concern_ids');
		if(isset($designerTag) && count($designerTag) > 0) {
			$room->syncTags($designerTag);
		}

        if ($request->filled('create_owner')) {
        	$claim = false;
        	if ($request->filled('claim')) $claim = true;

        	User::createOwner($room->owner_phone, $room, $claim);
    	}
        
        DB::commit();

        if ($room) {
        	$room->generateSongId();
        	//DummyData::where('id', $id)->update(['is_live' => 1]);
            $this->updateDummyData($id, $room);

            if ($request->filled('is_duplicate')) {
                $url = 'admin/room/'.$room->id.'/edit?ref=patrick';
                return redirect($url)->with('message', "Duplicate form");
            }

            ActivityLog::LogUpdate(Action::PATRICK_KOST, $id);

            return redirect('admin/patrick/list')->with("message","Room data has been successfully inserted");
            
        } else {
            return redirect()->back()->with("error_message","Error, looks something went wrong")->withInput();
        }

    }

    public function updateDummyData($id, $room)
    {
        $dummy = DummyData::where('id', $id)->first();
        $dummy->is_live         = 1;
        $dummy->designer_id     = $room->id;
        $dummy->name            = $room->name;
        $dummy->address         = $room->address;
        $dummy->area_city       = $room->area_city;
        $dummy->subdistrict     = $room->area_subdistrict;
        $dummy->latitude        = $room->latitude;
        $dummy->longitude       = $room->longitude;
        $dummy->room_count      = $room->room_count;
        $dummy->room_available  = $room->room_available;
        $dummy->gender          = $room->gender;
        $dummy->price           = $room->price;
        $dummy->save();
        return $dummy;
    }

    public function downloadImage($images)
    {
        $watermarking = true;
        if ($images == "http://songturu.mamikos.com/uploads/file/noimages.png") {
            $watermarking = false;
        }
    	$media = Media::postMedia($images, 'url', null, null, null, 'style_photo', $watermarking, null, false);
    	return $media;
    }

    public function renameImage($room)
    {
	    $Style              = new Card();
        $Style->designer_id = $room->id;
        $Style->description = "bangunan-tampak-depan-cover";
        $Style->photo_id    = $room->photo_id;
        $Style->save();

        return $Style;
    }

    public function checkingEdit(Request $request, $id)
    {
        $dummy = DummyData::where('id', $id)->first();
        if (is_null($dummy)) {
            return redirect()->back()->with("error_message","Error, looks something went wrong")->withInput();
        }

        $dummy->checking = "yes";
        $dummy->save();
        return redirect()->back()->with("message","Checking successfully");
    }

    public function scrapKoran($data)
    {
        DB::beginTransaction();
        foreach ($data AS $key => $value) {
            $room = new Room();
            $room->owner_phone = $value["phone"];
            $room->description = $value["description"];
            $room->address     = $value["address"];
            $room->name        = $value["name"];
            $room->gender      = $value["gender"];
            $room->room_available = $value["room_available"];
            $room->room_count     = $value["room_count"];
            $room->price_monthly  = $value["price_monthly"];
            $room->price_yearly   = $value["price_yearly"];
            $room->agent_name     = $value["agent_name"];
            $room->agent_phone    = $value["agent_phone"];
            $room->agent_status   = "scrap koran";
            $room->save();
        }
        DB::commit();
        return true;
    }

    public function fromGoogle(Request $request)
    {
        $viewData = [
            'data'     => null,
            'status'   => false,
            'request'  => $request->all(),
            'boxTitle' => 'Patrick Google Management'
        ];

        return view('admin.contents.patrick.google', $viewData);
    }

    public function getFromGoogle(Request $request)
    {
        $urlGoogleClean = "https://maps.googleapis.com/maps/api/place/nearbysearch/json";
        $keyGoogle      = "key=AIzaSyB8gCZrWOXDcqOmrdNJtci8k1ZNcJ1EZAg";

        if ($request->filled('pagetoken')) {
            $urlGoogle = file_get_contents($urlGoogleClean."?pagetoken=".$request->input('pagetoken')."&".$keyGoogle);
        } else {
            $urlGoogle = file_get_contents($urlGoogleClean."?location=".$request->input('latitude').",".$request->input('longitude')."&radius=".$request->input('radius')."&type=kost&keyword=kost&".$keyGoogle);
        }
        
        $data = json_decode($urlGoogle, true);
        
        $status = false;
        if ($data['status'] == 'OK') $status = true;

        $nextPage = null;
        if (isset($data['next_page_token'])) $nextPage = $data['next_page_token'];

        $viewData = [
            "data"     => $data,
            "status"   => $status,
            "next"     => $nextPage,
            "request"  => $request->all(),
            "boxTitle" => "Result Data"
        ];

        return view('admin.contents.patrick.google', $viewData);
    }

    public function editFromGoogle(Request $request)
    {
            $urlToInfomation = str_replace(" ", "+", strtolower($request['name']));
            $rowsConcern = Tag::where('type', '<>', 'fac_project')->get();
            $genderOptions = [
                0 => 'Campur',
                1 => 'Pria',
                2 => 'Wanita'
            ];

            $viewData = [
                'boxTitle'  => 'Edit data',
                'agent_status' => $this->inputTypes,
                'data'      => $request->all(),
                'getdata'   => "https://www.google.co.id/search?q=".$urlToInfomation,
                'tag'       => $rowsConcern,
                'gender'    => $genderOptions 
            ];

            return view('admin.contents.patrick.editgoogle', $viewData);
    }

    public function livesKost(Request $request)
    {
        $validator = validator($request->all(), array(
            'name'              => 'required|max:250',
            'price_daily'       => 'numeric',
            'price_weekly'      => 'numeric',
            'price_monthly'     => 'required|numeric',
            'price_yearly'      => 'numeric',
            'room_count'        => 'required|numeric',
            'room_available'    => 'required|numeric',
            'address'           => 'required',
            'longitude'         => 'required|numeric',
            'latitude'          => 'required|numeric',
            'owner_name'        => 'required_without:manager_name',
            'owner_phone'       => 'required_with:owner_name',
            'manager_name'      => 'required_without:owner_name',
            'manager_phone'     => 'required_with:manager_name',
            'agent_name'        => 'required'
        ));

        $validator->after(function($validator) use ($request) {
            if($request->name != '') {
                $roomExist = Room::where('name', $request->name)->first();
                if($roomExist) {
                    $validator->errors()->add('name', 'Nama sudah pernah digunakan sebelumnya');
                }
            }
        });

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        DB::beginTransaction();

        $room = new Room();
        //check for indexed type
        if ($request->input('indexed') == null) {
            $room->is_indexed = 0;
        } else {
            $room->is_indexed = 1;
        }

        if (!$request->input('price_daily') || $request->input('price_daily') == '-' || !is_numeric($request->input('price_daily'))) {
            $room->price_daily = 0;
        } else {
            $room->price_daily = $request->input('price_daily');
        }

        if (!$request->input('price_monthly') || $request->input('price_monthly') == '-' || !is_numeric($request->input('price_monthly'))) {
            $room->price_monthly = 0;
        } else {
            $room->price_monthly = $request->input('price_monthly');
        }

        if (!$request->input('price_weekly') || $request->input('price_weekly') == '-' || !is_numeric($request->input('price_weekly'))) {
            $room->price_weekly = 0;
        } else {
            $room->price_weekly = $request->input('price_weekly');
        }

        if (!$request->input('price_yearly') || $request->input('price_yearly') == '-' || !is_numeric($request->input('price_yearly'))) {
            $room->price_yearly = 0;
        } else {
            $room->price_yearly = $request->input('price_yearly');
        }

        $area_big = NULL;
        $area_city = NULL;
        if (!empty($request->input('area_city'))) {
            $room->area_city = AreaFormatter::format($request->input('area_city'));
            $room->area_big = $area_city;
        }

        $area_subdistrict = NULL;
        if (!empty($request->input('area_subdistrict'))) {
            $room->area_subdistrict =  AreaFormatter::format($request->input('area_subdistrict'));
        }
        
        $agent_status = null; 
        if (!empty($request->input('agent_status'))) {
            if ($request->input('agent_status') == 'Semua') {
                $room->agent_status = null;
            } else {
                $room->agent_status = $request->input('agent_status');
            }
        }

        $room->name             = $request->input('name');
        $room->size             = $request->input('size');
        $room->gender           = $request->input('gender');
        $room->floor            = $request->input('floor');
        $room->price            = $request->input('price_monthly');
        $room->address          = $request->input('address');
        $room->longitude        = $request->input('longitude');
        $room->latitude         = $request->input('latitude');
        $room->description      = $request->input('description');
        $room->fac_room_other   = substr($request->input('fac_room_other'), 0, 500);
        $room->fac_bath_other   = substr($request->input('fac_bath_other'), 0, 500);
        $room->fac_share_other  = substr($request->input('fac_share_other'), 0, 500);
        $room->fac_near_other   = substr($request->input('fac_near_other'), 0, 500);
        $room->price_remark     = substr($request->input('price_remark'), 0, 500);
        $room->remark           = $request->input('remarks');
        $room->agent_name       = $request->input('agent_name');
        $room->room_count       = $request->input('room_count');
        $room->room_available   = $request->input('room_available');
        $room->status           = $request->input('room_available') > 0 ? 2 : 0;
        $room->remark           = $request->input('remarks');
        $room->animal           = $request->input('animal');
        $room->owner_name       = $request->input('owner_name');
        $room->owner_phone      = $request->input('owner_phone');
        $room->manager_name     = $request->input('manager_name');
        $room->manager_phone    = $request->input('manager_phone');

        $room->office_phone         = '';
        $room->kost_updated_date    = date('Y-m-d H:i:s');
        
        $room->guide                = $request->input('guide');
        $room->kost_updated_date    = Carbon::now()->format('Y-m-d H:i:s');
        $room->is_promoted          = 'false';
        $room->is_booking           = 0;

        if ($request->filled('is_verify_address')) $room->is_verified_address = 1;
        if ($request->filled('is_verify_phone')) $room->is_verified_phone = 1;

        $photoIdAvailable = false;
        $photo_count = 0;
        if ($request->filled('download') AND !empty($request->filled('images'))) {
            $media = $this->downloadImage($request->input('images'));
            if (isset($media['id'])) $room->photo_id = $media['id'];
            $photoIdAvailable = true;
            $photo_count = 1;
        }
        $room->photo_count = $photo_count;
        $room->updateSortScore();
        $room->save();

        if ($request->filled('live')) {
            Room::verifyDesigner($room, true, null);
        }

        if (!is_null($photoIdAvailable)) {
            $renameImage = $this->renameImage($room);
        }

        // storing tag
        $designerTag = $request->get('concern_ids');
        if(isset($designerTag) && count($designerTag) > 0) {
            $room->syncTags($designerTag);
        }

        if ($request->filled('create_owner')) {
            $claim = false;
            if ($request->filled('claim')) $claim = true;

            User::createOwner($room->owner_phone, $room, $claim);
        }
        
        DB::commit();

        if ($room) {
            $room->generateSongId();
            return redirect('admin/patrick/list')->with("message","Room data has been successfully inserted");
        } else {
            return redirect()->back()->with("error_message","Error, looks something went wrong")->withInput();
        }
    }

    public function checkRoom(Request $request)
    {
        $room = Room::where('name', $request->input('name'))->first();

        return Api::responseData([
            'available' => is_null($room) ? true : false,
        ]);
    }

    public function checkData(Request $request)
    {
        $viewData = [
            'data'      => '',
            'boxTitle'  => 'Patrick Management'
        ];

        return view('admin.contents.patrick.importir', $viewData);
    }

    public function checkFromNoHp(Request $request)
    {
        $room = Room::select('id', 'name', 'is_active')->where('owner_phone', $request->input('phone'))->get()->toArray();
        if (count($room) < 1) $status = false;
        else $status = true;
        return ["data" => $room, "status" => $status];
    }


}
