<?php

namespace App\Http\Controllers\Admin\Patrick;

use App\Http\Helpers\RegexHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Auth;
use App\Entities\Vacancy\VacancyDummy;
use App\Entities\Vacancy\Vacancy;
use Validator;
use App\Http\Helpers\ApiHelper;
use Jenssegers\Agent\Agent;
use App\Entities\Activity\Tracking;
use Cviebrock\EloquentSluggable\Services\SlugService;
use App\Entities\Area\AreaBigMapper;
use App\Entities\Vacancy\DummyCompanyProfile;
use DB;
use App\Entities\Vacancy\CompanyProfile;
use App\Entities\Vacancy\Spesialisasi;
use App\Entities\Vacancy\Industry;
use App\Http\Helpers\UtilityHelper;
use App\Libraries\AreaFormatter;
use App\Entities\Media\Media;
use App\Entities\Generate\Content;
use App\Entities\Activity\ActivityLog;
use App\Entities\Activity\Action;

class VacancyController extends Controller
{
    protected $validationMessages = [
        'place_name.required' => 'Nama Tempat Usaha harus diisi',
        'place_name.max' => 'Nama Tempat Usaha maksimal :max karakter',
        'address.max' => 'Alamat maksimal :max karakter',
        'latitude.required' => 'Latitude harus diisi',
        'latitude.numeric' => 'Latitude harus berupa angka',
        'longitude.required' => 'Latitude harus diisi',
        'longitude.numeric' => 'Latitude harus berupa angka',
        'city.max' => 'Kota maksimal :max karakter',
        'subdistrict.max' => 'Subdisctrict maksimal :max karakter',
        'jobs_name.required' => 'Nama Pekerjaan harus diisi',
        'jobs_name.max' => 'Nama Pekerjaan maksimal :max karakter',
        'position.max' => 'Posisi maksimal :max karakter',
        'salary.numeric' => 'Gaji harus berupa angka',
        'user_name.required' => 'Nama Penanggung Jawab harus diisi',
        'user_name.max' => 'Nama Penanggung Jawab maksimal :max karakter',
        'user_phone.required' => 'No. Telp. Penanggung Jawab harus diisi',
        'user_phone.max' => 'No. Telp. Penanggung Jawab maksimal :max karakter',
        'user_email.email' => 'Email Penanggung Jawab tidak valid',
        'agent_name.required' => 'Nama Penginput harus diisi',
        'agent_type.required' => 'Status Penginput harus diisi'
    ];

	public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if(!Auth::user()->can('access-patrick-vacancy')) {
                return redirect('admin');
            }
            
            return $next($request);
        });

        View::share('contentHeader', 'Stories Management');
        View::share('user', Auth::user());
    }

    public function getListDummyVacancy(Request $request)
    {
    	$vacancys    = VacancyDummy::query()->orderBy('id','asc');

        if($request->filled('q'))
        {
            $vacancys->where('name','like', '%' . $request->get('q') . '%');
        }

        $vacancys    = $vacancys->paginate(20);

        $viewData = array(
            'boxTitle'        => 'Jobs Dummy List',
            'vacancys'     => $vacancys,
        );

        ActivityLog::LogIndex(Action::PATRICK_VACANCY);

        return View::make('admin.contents.patrick.vacancy.index', $viewData);
    }

    public function getCompanyList(Request $request)
    {
        $company    = DummyCompanyProfile::query()->orderBy('id','asc');

        if($request->filled('q'))
        {
            $company->where('name','like', '%' . $request->get('q') . '%');
        }

        $company    = $company->paginate(20);

        $viewData = array(
            'boxTitle'        => 'Company Dummy List',
            'company'     => $company,
        );

        return View::make('admin.contents.patrick.vacancy.company', $viewData);
    }

    public function importPage(Request $request)
    {
        $viewData = [
                'boxTitle' => "Vacancy import",
        ];
        return view::make('admin.contents.patrick.vacancy.import', $viewData);
    }

    public function posyImport(Request $request)
    {
        $request->validate([
            'json_file' => 'required|file|max:4024',
        ]);

        $file = $request->file('json_file')->getRealPath();
        $fileContent = json_decode(file_get_contents($file));
        
        if (is_null($fileContent)) {
            return back()->with('error_message', 'Gagal import data');
        }
        
        foreach ($fileContent as $key => $value) {

            $website = isset($value->website) ? " website: ".$value->website : "";
            $company_size = isset($value->company_size) ? " Banyaknya pekerja: ".$value->company_size : "";
            $work_time = isset($value->work_time) ? " Jam kerja: ".$value->work_time : "";
            $uniform = isset($value->uniform) ? " Pakaian kerja: ".$value->uniform : "";
            $tunjangan = isset($value->tunjangan) ? " Tunjangan: ".$value->tunjangan : "";

            $pt_name    = empty($value->pt_name) ? "-" : (new Content())->cleanFromHtmlTag($value->pt_name);
            $title      = empty($value->title) ? "-" : $value->title;
            $description= empty($value->job_description) ? "-" : $value->job_description;
            $phone      = empty($value->telp) ? "-" : $value->telp;
            $address    = empty($value->address) ? "-" : $value->address;

            $dummy = new VacancyDummy();
            $dummy->place_name  = trim($pt_name);
            $dummy->name        = $title." di ".trim($pt_name);
            $dummy->position    = $title;
            $dummy->url         = $value->slug;
            $dummy->description = $description.$website.$company_size.$work_time.$uniform.$tunjangan;
            $dummy->salary = isset($value->salary) ? $value->salary : 0;

            if (isset($value->max_salary)) $dummy->max_salary = $value->max_salary;
            
            if (!empty($value->timese)) {
                $locations = (new Content())->getLatLong($value->timese);
                $dummy->latitude  = $locations[0];
                $dummy->longitude = $locations[1];
            }

            $dummy->user_phone  = $phone;
            $dummy->address     = $address;
            $dummy->is_active   = 0;
            $dummy->save();

            ActivityLog::LogCreate(Action::PATRICK_VACANCY, $dummy->id);
        }
        return back()->with('message', 'Success');
    }

    public function companyEdit(Request $request, $id)
    {
        $data = DummyCompanyProfile::where('id', $id)->where('is_live', 0)->first();

        if (is_null($data)) return redirect()->back()->with('message','Company sudah live');

        $viewData = array(
             "data"    => $data,
             "industry"  => Industry::list(),
             "boxTitle"=> 'Company Edited',
             "description" => utf8_encode($data->description)
        );

        return view('admin.contents.patrick.vacancy.company-edit', $viewData);
    }

    public function companyUpdate(Request $request, $id)
    {
        DB::beginTransaction();
        $company = $this->insertToCompanyProfile($request->all());
        $this->dummyUpdateCompanyProfile($company, $request->all(), $id);
        DB::commit();
        return redirect('admin/patrick/vacancy/company')->with(['message' => "Berhasil"]);
    }

    public function insertToCompanyProfile($value)
    {
        $city = AreaFormatter::format($value['city']);
        $subdistrict = AreaFormatter::format($value['subdistrict']);
        $areaBigMapper = (new AreaBigMapper)->bigCityMapper($city);

        $company = new CompanyProfile();
        $company->name = $value['name'];
        $company->industry_id = $value['industry'];
        $company->address = $value['address'];
        $company->website = $value['website'];
        $company->size = $value['size'];
        $company->city = $city;
        $company->subdistrict = $subdistrict;
        $company->area_big = $areaBigMapper;
        $company->dresscode = $value["dresscode"];
        $company->language = $value["language"];
        $company->time = $value['time'];
        $company->subsidy = $value['subsidy'];
        $company->description = $value['description'];
        $company->phone = preg_replace(RegexHelper::numericOnly(), "", $value["phone"]);
        $company->email = $value['email'];
        $company->latitude = $value["latitude"];
        $company->longitude = $value["longitude"];
        $company->facility = $value["facilities"];
        $company->agent_name = $value['agent_name'];
        if (isset($value['live'])) {
            $company->is_active = 1;
        }
        
        if (strlen($value['logo']) > 4) {
            $media = $this->downloadImage($value['logo']);
            if (isset($media['id'])) $company->photo_id = $media['id'];
        }

        //$company->logo = $value["company_logo_url"];
        $company->save();

        return $company;
    }

    public function downloadImage($images)
    {
        $watermarking = false;
        if ($images == "http://songturu.mamikos.com/uploads/file/noimages.png") {
            $watermarking = false;
        }
        $media = Media::postMedia($images, 'url', null, null, null, 'company_photo', $watermarking, null, false);
        return $media;
    }

    public function dummyUpdateCompanyProfile($companyLive, $value, $id)
    {
        $company = DummyCompanyProfile::where('id', $id)->first();
        $company->name = $value['name'];
        //$company->industry_id = $value['industry'];
        $company->address = $value['address'];
        $company->website = $value['website'];
        $company->size = $value['size'];
        $company->company_profile_id = $companyLive->id;
        $company->is_live = 1;
        $company->dresscode = $value["dresscode"];
        $company->language = $value["language"];
        $company->time = $value['time'];
        $company->subsidy = $value['subsidy'];
        $company->description = $value['description'];
        $company->phone = preg_replace(RegexHelper::numericOnly(), "", $value["phone"]);
        $company->email = $value['email'];
        $company->latitude = $value["latitude"];
        $company->longitude = $value["longitude"];
        $company->facility = $value["facilities"];
       
        $company->save();
        return $company;
    }

    public function editDummyVacancy(Request $request, $id)
    {
        $data = VacancyDummy::where('id', $id)->where('is_active', 0)->first();

        if (is_null($data)) return redirect()->back()->with('message','Kost sudah live');

        $spesialisasi = Spesialisasi::where('is_active', 1)->orderBy('name', 'asc')->get();

        $viewData = array(
             "data"    => $data,
             "status"  => Vacancy::VACANCY_STATUS,
             "type"    => Vacancy::VACANCY_TYPE,
             "times"   => Vacancy::SALARY_TIME,
             "educations" => Vacancy::EDUCATION_OPTION,
             "boxTitle"=> 'Jobs Edited',
             "spesialisasi" => $spesialisasi,
            "industry" => Industry::where('is_active', 1)->get() ,
            'groupOptions' => Vacancy::GROUP_OPTION
        );

        return view('admin.contents.patrick.vacancy.edit', $viewData);
    }


    public function stores(Request $request, $id_dummy) 
    {
        //dd($request->all());
        $this->dummyUpdate($request->all(), $id_dummy);
        $validator = Validator::make($request->all(),[
            'place_name'    => 'required|max:190',
            'address'       => 'max:255',
            'latitude'      => 'required|numeric',
            'longitude'     => 'required|numeric',
            'city'          => 'max:100',
            'subdistrict'   => 'max:100',
            'jobs_name'     => 'required|max:190',
/*            'position'      => 'max:100',
*/            'salary'        => 'numeric',
            'salary_time'   => 'nullable',
            'type'          => 'nullable',
            'user_name'     => 'nullable|max:190',
            'user_phone'    => 'nullable|max:190',
            'user_email'    => 'nullable',
        ], $this->validationMessages);

        if($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors())->withInput($request->all());
        }

        $company_id = (int) $request->input('company_profile');       
        $is_index = Vacancy::ValidationIsIndex($request);

        $area_city = ApiHelper::sanitizeCityOrDistrictName($request->city);
        $areaBigMapper = $this->areaBigMapper($area_city);
        $subdistrict = ApiHelper::sanitizeCityOrDistrictName($request->subdistrict);
        $agent                  = new Agent();
        $vacancy                = new Vacancy;
        $vacancy->place_name    = $request->place_name;
        $vacancy->latitude      = $request->latitude;
        $vacancy->longitude     = $request->longitude;
        $vacancy->city          = $area_city;
        $vacancy->subdistrict   = $subdistrict;
        $vacancy->area_big      = $areaBigMapper;
        $vacancy->address       = $request->address;
        $vacancy->name          = $request->jobs_name;
/*      $vacancy->position      = $request->position;*/
        if ($company_id > 0) $vacancy->company_profile_id = $company_id;

        $vacancy->last_education = $request->education;
        $vacancy->show_salary   = 1; 
        $vacancy->type          = $request->type;
        $vacancy->description   = $request->description;
        $vacancy->salary        = $request->salary;
        $vacancy->max_salary    = $request->max_salary;
        $vacancy->salary_time   = $request->salary_time;
        $vacancy->user_name     = $request->user_name;
        $vacancy->user_phone    = $request->user_phone;
        $vacancy->user_email    = $request->user_email;
        $vacancy->insert_from   = Tracking::checkDevice($agent);
        $vacancy->data_input    = "scrap";
        $vacancy->detail_url    = $request->detail_url;
        $vacancy->group = 'niche';
        if (strlen($request->expired_date) > 4) $vacancy->expired_date  = $request->expired_date;

        $checkotherweb = UtilityHelper::checkApplyOtherWeb(["user_email" => $request->user_email, "user_phone" => $request->user_phone, "detail_url" => $request->detail_url]);
        if ($checkotherweb) $vacancy->apply_other_web = 1;
        
        $vacancy->workplace     = $request->input('workplace');
        $vacancy->workplace_status = $request->filled('workplace_status') ? $request->input('workplace_status') : 0;
        $vacancy->agent_name    = strtolower($request->agent_name);
        if ($request->filled('live')) {
            $vacancy->is_active = 1;
            $vacancy->status = 'verified';
            $vacancy->is_indexed = $is_index;
        } else {
            $vacancy->status = "waitting";
        }
        if ($request->filled('spesialisasi')) $vacancy->spesialisasi_id = $request->input('spesialisasi');
        if ($request->filled('industry')) $vacancy->industry_id = $request->input('industry');
        $vacancy->save();

        if ($request->filled('live')) {
            $this->verifyIfLiveChecklist($vacancy->id);
        }
//dd($vacancy);
        return redirect('admin/patrick/vacancy/list')->with('message',  'Lowongan Kerja berhasil ditambahkan');
    }

    public function areaBigMapper($area_city)
    {
        if (strlen($area_city) == 0) return null;
        $mapper = AreaBigMapper::where('area_city', $area_city)->first();
        if (is_null($mapper)) return null;
        return $mapper->area_big;
    }

    public function dummyUpdate($request, $id)
    {
        $vacancy                = VacancyDummy::where('id', $id)->first();
        $vacancy->place_name    = isset($request['place_name']) ? $request['place_name'] : null;
        $vacancy->latitude      = isset($request['latitude']) ? $request['latitude'] : null;
        $vacancy->longitude     = isset($request['longitude']) ? $request['longitude'] : null;

        if (isset($request['city'])) {
            $vacancy->city = ApiHelper::sanitizeCityOrDistrictName($request['city']);
        }

        if (isset($request['subdistrict'])) {
            $vacancy->subdistrict = ApiHelper::sanitizeCityOrDistrictName($request['subdistrict']);
        }

        $vacancy->address       = isset($request['address']) ? $request['address'] : null;
        $vacancy->name          = isset($request['jobs_name']) ? $request['jobs_name'] : null;
//        $vacancy->position      = $request['position'];
        $vacancy->type          = isset($request['type']) ? $request['type'] : null;
        $vacancy->description   = isset($request['description']) ? $request['description'] : null;
        $vacancy->salary        = isset($request['salary']) ? $request['salary'] : null;
        $vacancy->max_salary    = isset($request['max_salary']) ? $request['max_salary'] : null;
        $vacancy->salary_time   = isset($request['salary_time']) ? $request['salary_time'] : null;
        $vacancy->user_name     = isset($request['user_name']) ? $request['user_name'] : null;
        $vacancy->user_phone    = isset($request['user_phone']) ? $request['user_phone'] : null;
        $vacancy->user_email    = isset($request['user_email']) ? $request['user_email'] : null;
        $vacancy->insert_from   = 'desktop';
        $vacancy->data_input    = "scrap";
        if (isset($request['live'])) {
            $vacancy->is_active = 1;
            $vacancy->status    = 'verified';
        }
        $vacancy->save();
        return $vacancy;
    }

    public function verifyIfLiveChecklist($id)
    {
        $vacancy = Vacancy::where('id', $id)->first();
        //$slug = strtolower($vacancy->type)."-".SlugService::createSlug(Vacancy::class, 'slug', $vacancy->slug_vacancy, ['unique' => true]);
        //$vacancy->slug = $slug;
        $vacancy->save();
        return $vacancy;
    }

    public function deleteVacancy($id)
    {
        $vacancy = VacancyDummy::where('id', $id)->delete();
        return redirect()->back()->withInput(['message', 'Berhasil hapus']);
    }

    public function checkFromNoHpVacancy(Request $request)
    {
        $vacancy = Vacancy::select('id', 'name', 'is_active')->where('user_phone', $request->input('phone'))->get()->toArray();
        if (count($vacancy) < 1) $status = false;
        else $status = true;
        return ["data" => $vacancy, "status" => $status];
    }
}
