<?php

namespace App\Http\Controllers\Admin\Patrick;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Auth;
use Validator;
use App\Entities\File\ScrapFile;

class ExportController extends Controller
{
	protected $agent_name = ["septarin", "shabrina f", "tantri sm", "tika", "arin", "helfi", "riris", "milawati h", "nana", "kiki", "dinda", "ipeh", "erin", "erma"];

	protected $validatorMessage = [
		"agent_name.required" 	=> "Nama agen tidak boleh kosong",
		"file_csv.required"		=> "File tidak boleh kosong",
		"file_csv.mimes"		=> "File harus CSV"
	];

	public function __construct()
    {
        View::share('contentHeader', 'Patrick Apartemen Management');
        View::share('user', Auth::user());
    }

    public function index(Request $request)
    {
    	$viewData = [
            'data'    => '',
            'boxTitle'     => 'Patrick Export Management'
        ];

        return view('admin.contents.patrick.export.index', $viewData);
    }

    public function addFile(Request $request)
    {
    	$agent_name = $this->agent_name;
    	$validator = Validator::make($request->all(), 
            [
                'agent_name'        => 'required',
                'file_csv'    		=> 'required'
            ], $this->validatorMessage);

        $validator->after(function($validator) use ($request, $agent_name) {
           $agent = strtolower($request->input('agent_name'));
           if (!in_array($agent, $agent_name)) {
           		$validator->errors()->add('message', 'Nama agent tidak sesui');
           }
        });

        if($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput($request->all());
        }

       $response = ScrapFile::storeData($request->all());
       return redirect()->back()->with('message','Sukses');
    }
}
