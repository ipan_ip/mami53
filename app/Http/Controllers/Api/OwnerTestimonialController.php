<?php

namespace App\Http\Controllers\Api;

use App\Http\Helpers\ApiResponse as Api;
use App\Repositories\TestimonialRepository;


class OwnerTestimonialController extends BaseController
{

    /**
     * @var TestimonialRepository
     */
    protected $repository;

    /**
     * OwnerTestimonialController constructor.
     *
     * @param \App\Repositories\TestimonialRepository $repository
     */
    public function __construct(TestimonialRepository $repository)
    {
        $this->repository = $repository;
    }


    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTestimony()
    {
        $testimony = $this->repository->getListTestimony();

        return Api::responseData(
            [
                'testimonies' => $testimony,
            ]
        );
    }

}


