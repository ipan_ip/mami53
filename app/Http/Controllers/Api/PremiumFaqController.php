<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Helpers\ApiResponse as Api;
use App\Repositories\Premium\PremiumFaqRepository;
use App\User;
use Illuminate\Support\Facades\Validator;

class PremiumFaqController extends BaseController
{
    protected $repository;
    public function __construct(PremiumFaqRepository $repository)
    {
        $this->repository = $repository;
        parent::__construct();
    }

    public function getPremiumFaq(Request $request)
    {
        $limit = $request->filled('limit') ? $request->input('limit') : 20;
        $faqList = $this->repository->getPremiumFaq($limit);
        return Api::responseData($faqList);
    }

}