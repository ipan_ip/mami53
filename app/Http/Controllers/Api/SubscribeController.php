<?php
namespace App\Http\Controllers\Api;

use App\Http\Helpers\ApiResponse as Api;
use Illuminate\Http\Request;
use App\Repositories\RoomRepositoryEloquent;
use App\Entities\User\UserFindRoom;
use App\Entities\User\UserFindApartment;
use App\Entities\DownloadExam\DownloadExamInput;
use App\Entities\Alarm\UserFilter;
use Validator;

class SubscribeController extends BaseController
{
    protected $repository;

    public function subscribeFilter(Request $request)
    {
        $user = $this->user();
        $device = $this->device();

        $source = $request->filled('source') ? $request->get('source') : 'filter';

        if ($request->filled('location') AND !is_null($request->input('location'))) $alarmDetail = $alarmDetail = $request->all('filters', 'location', 'phone', 'email');
        else $alarmDetail = $request->all('filters', 'job', 'area', 'phone', 'email');

        $roomRepository = new RoomRepositoryEloquent(app());

        // log raw alarm
        $alarm = $roomRepository->saveFilterToAlarm($user, $device, $alarmDetail);

        // if only request has user_detail parameter
        if($request->filled('user_detail')) {
            UserFindRoom::saveFromSubscribe($user, $request->input('user_detail'));
            $source = 'user_find_room';
        }

        // log new filter
        UserFilter::saveOrUpdate(
            $user ? $user : null, 
            $alarmDetail, 
            'room', 
            $source, 
            $device
        );
        
        return Api::responseData([ 'result' => $alarm ]);
    }

    /**
    * Handle subscribe filter from download soal
    */
    public function subscribeDownload(Request $request)
    {
        $user = $this->user();

        $alarmDetail = $request->all('filters', 'location', 'phone', 'email');

        $source = $request->filled('source') ? $request->get('source') : 'filter';

        $roomRepository = new RoomRepositoryEloquent(app());

        $alarm = $roomRepository->saveFilterToAlarm($user, null, $alarmDetail);

        $inputResult = null;

        if($request->filled('user_detail')) {
            $userDetail = $request->input('user_detail');
            
            $source = $userDetail['type'];

            if(isset($userDetail['type']) && $userDetail['type'] == 'test-kerja') {
                $userDetail['landing'] = '20000';
            } elseif(isset($userDetail['type']) && $userDetail['type'] == 'download-bbm') {
                $userDetail['landing'] = '30000';
                $userDetail['name'] = $user->name;
                $userDetail['email'] = $user->email;
                $userDetail['phone'] = $user->phone_number;
            } elseif(isset($userDetail['type']) && $userDetail['type'] == 'seleksi-mandiri') {
                $userDetail['landing'] = '40000';
                $userDetail['name'] = $user->name;
                $userDetail['email'] = $user->email;
                $userDetail['phone'] = $user->phone_number;
            } elseif(isset($userDetail['type']) && $userDetail['type'] == 'daftar-ulang') {
                $userDetail['landing'] = '50000';
                $userDetail['name'] = $user->name;
                $userDetail['email'] = $user->email;
                $userDetail['phone'] = $user->phone_number;
            } elseif(isset($userDetail['type']) && $userDetail['type'] == 'landing-commuter') {
                $userDetail['landing'] = '60000';
                $userDetail['name'] = $user->name;
                $userDetail['email'] = $user->email;
                $userDetail['phone'] = $user->phone_number;
            } else {
                $userDetail['landing'] = '10000';
                $source = 'sbmptn';
            }
            
            $userDetail['notes'] = '';
            $userDetail['start_date'] = null;
            $userDetail['duration'] = 1;
            $inputResult = UserFindRoom::saveFromSubscribe($user, $userDetail);
        } elseif($request->filled('form_detail')) {
            $userId = is_null($user) ? NULL : $user->id;

            $formDetail = $request->input('form_detail');
            $examInput = new DownloadExamInput;
            $examInput->download_exam_id = $formDetail['exam_id'];
            $examInput->user_id = $userId;
            $examInput->name = $formDetail['name'];
            $examInput->current_location = $formDetail['current_location'];
            $examInput->destination = $formDetail['destination'];

            if(isset($formDetail['university'])) {
                $examInput->university = $formDetail['university'];
            }
            
            $examInput->gender = $formDetail['gender'];
            $examInput->email = $formDetail['email'];
            $examInput->phone_number = $formDetail['phone_number'];
            $examInput->save();

            $inputResult = $examInput;

            $source = 'download_exam_' . $formDetail['exam_id'];
        }

        UserFilter::saveOrUpdate($user, $alarmDetail, 'room', $source);
        
        return Api::responseData([ 'result' => $alarm ]);
    }


    public function subscribeDownloadUpdate(Request $request)
    {
        $type = $request->filled('type') ? $request->input('type') : 'download_exam';

        $user = $this->user();

        if(!$user) {
            return Api::responseData([
                'status'=>false,
                'meta' => [
                    'message' => 'Silakan login terlebih dahulu'
                ]
            ]);
        }

        if($type == 'download_exam') {
            $downloadExamInput = DownloadExamInput::find($request->input('_seq'));

            if(!$downloadExamInput) {
                return Api::responseData([
                    'status' => false,
                    'meta' => [
                        'message' => 'Data tidak ditemukan'
                    ]
                ]);
            }

            if(!is_null($downloadExamInput->user_id)) {
                return Api::responseData([
                    'status' => false,
                    'meta' => [
                        'message' => 'Data sudah diupdate sebelumnya.'
                    ]
                ]);
            }

            $downloadExamInput->user_id = $user->id;
            $downloadExamInput->save();
        }

        return Api::responseData(['status'=>true]);

    }

    public function findMeApartment(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'city' => 'required',
            'furnished' => 'required',
            'unit_type' => 'required',
            'rent_type' => 'required',
            'price_min' => 'required',
            'price_max' => 'required',
            'name' => 'required',
            'email' => 'required|email',
            'phone_number' => 'required|regex:/^(08[1-9]\d{7,11})$/'
        ], [
            'city.required' => 'Nama Kota harus diisi',
            'furnished.required' => 'Fasilitas Kamar harus diisi',
            'unit_type.required' => 'Tipe Kamar harus diisi',
            'rent_type.required' => 'Jangka Waktu harus diisi',
            'price_min.required' => 'Harga Minimal harus diisi',
            'price_max.required' => 'Harga Maksimal harus diisi',
            'name.required' => 'Nama harus diisi',
            'email.required' => 'Email harus diisi',
            'email.email' => 'Email tidak valid',
            'phone_number.required' => 'Nomor telepon harus diisi',
            'phone_number.regex' => 'Format Nomor Telepon tidak valid (08xxxxxxxx)'
        ]);

        if ($validator->fails()) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message'=>implode('. ', $validator->errors()->all())
                ]
            ]);
        }

        $user = $this->user();

        $userFindApartment = new UserFindApartment;
        $userFindApartment->user_id = !is_null($user) ? $user->id : null;
        $userFindApartment->name = $request->name;
        $userFindApartment->email = $request->email;
        $userFindApartment->phone_number = $request->phone_number;
        $userFindApartment->city = $request->city;
        $userFindApartment->furnished = $request->furnished;
        $userFindApartment->unit_type = $request->unit_type;
        $userFindApartment->rent_type = $request->rent_type;
        $userFindApartment->price_min = $request->price_min;
        $userFindApartment->price_max = $request->price_max;
        $userFindApartment->note = $request->notes;
        $userFindApartment->save();
        
        return Api::responseData([ 'result' => $userFindApartment ]);
    }

}
