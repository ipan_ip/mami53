<?php

namespace App\Http\Controllers\Api\Related;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseController;
use App\Http\Helpers\ApiResponse as Api;
use App\Repositories\Jobs\JobsRepository;
use App\Entities\Room\Room;
use App\Entities\Vacancy\Vacancy;
use App\Entities\Vacancy\VacancyFilters;
use Cache;
use Validator;

class RelatedController extends BaseController
{

    protected $repository;

	public function __construct(JobsRepository $repository)
    {
        $this->repository = $repository;

        parent::__construct();
    }

    public function getRelated(Request $request, $id)
    {
        if (!$request->filled('type')) {
            return Api::responseData(["status" => false, "vacancy" => null]);
        }

        $type = ["room", "jobs"];
        if (!in_array($request->input('type'), $type)) {
            return Api::responseData(["status" => false, "vacancy" => null]);
        }

        if ($request->input('type') == 'room') {
            $data = Room::where('song_id', $id)->first();

            if (is_null($data)) {
                return Api::responseData(["status" => false, "vacancy" => null]);
            }

            $place = [$data->area_city, $data->area_subdistrict];
            
        } else if ($request->input('type') == 'jobs') {
            $data = Vacancy::where('id', $id)->first();

            if (is_null($data)) {
                return Api::responseData(["status" => false, "vacancy" => null]);
            }
            $place = [$data->city, $data->subdistrict];
        } 

        $vacancy = null;
        $room = null;

        $cacheIdentifier = strtolower($data->area_city."-".$data->area_subdistrict);
        $getVacancyCache = Cache::get("vacancy:".$cacheIdentifier);
        if (!is_null($getVacancyCache)) {
            shuffle($getVacancyCache);
            $vacancy = array_slice($getVacancyCache, 15);
        }

        if (($request->input('type') == 'room' OR $request->input('type') == 'jobs') AND is_null($getVacancyCache)) {
            $this->repository->setPresenter(new \App\Presenters\JobsPresenter('list', $this->user()));
            $filters['place']    = $place;
            $this->repository->pushCriteria(new \App\Criteria\Jobs\MainFilterCriteria($filters));
            $vacancy = $this->repository->getJobsItemListNotWithPagination(new VacancyFilters($filters));
            
            if (count($vacancy) > 0) {
                Cache::put("vacancy:".$cacheIdentifier, $vacancy, 60*120);
                shuffle($vacancy);
                $vacancy = array_slice($vacancy, 15);
            }
            $vacancy = $vacancy;
        }

        if ($request->input('type') == "room") {
            $roomtype = "apartment";
            if (is_null($data->apartment_project_id) OR $data->apartment_project_id < 1) $roomtype = "kos";
        } else {
            $roomtype = "jobs";
        }

        $room = $this->repository->getRelatedRoom($place, $roomtype, $cacheIdentifier);
        $response = array_merge(["vacancy" => $vacancy], $room);

        return Api::responseData($response);
    }


    public function relatedWeb(Request $request)
    {
        ini_set('max_execution_time', 100000);

        $validator = Validator::make($request->all(), [
            'location.*' => 'required'
        ]);

        $validator->after(function ($validator) use($request) {
            $req = $request->all();
            if (!isset($req['location']) OR count($req['location']) != 2) {
                $validator->errors()->add('location', 'Location is not found');
            }

            if ((isset($req['location'][0]) AND count($req['location'][0]) !=2) OR (isset($req['location'][1])  AND count($req['location'][1]) !=2)) {
                $validator->errors()->add('location', 'Location is not found');
            }
        });        

        if ($validator->fails()) {
            return Api::responseData(["status" => false, "kost" => null, "apartment" => null, "vacancy" => null]);
        }
        
        $filters = $request->input('filters');
        
        if (!empty($filters['place'])){
            $filters['place'] = $filters['place']; 
        } else {
            $filters['place'] = null;
        }
        
        $filters['location']    = $request->input('location');
        $filters['sorting']     = $request->input('sorting');
        $filters['titik_tengah'] = $this->getTitikTengah($filters['location']);
        
        if (!isset($filters['property_type'])) $filters['property_type'] = "jobs";

        $response = $this->repository->webRelated($filters);

        $vacancy = null;
        if (!is_null($response['identifier'])) {
            $getVacancyCache = Cache::get("vacancy:".$response['identifier']);
            if (!is_null($getVacancyCache)) {
                shuffle($getVacancyCache);
                $datavacancy = array_chunk($getVacancyCache, 5);
                if (count($datavacancy) > 0) $vacancy = $datavacancy[0];
            }
            $vacancy = $vacancy;
        }

        if (($filters['property_type'] == 'apartment' OR $filters['property_type'] == 'kost') AND is_null($vacancy)) {
            $this->repository->setPresenter(new \App\Presenters\JobsPresenter('list', $this->user()));
            $this->repository->pushCriteria(new \App\Criteria\Jobs\MainFilterCriteria($filters));
            $vacancy = $this->repository->getJobsItemListNotWithPagination(new VacancyFilters($filters));
            
            if (count($vacancy) > 0) {
                Cache::put("vacancy:".$response['identifier'], $vacancy, 60*120);
                shuffle($vacancy);
                $datavacancy = array_chunk($vacancy, 5);
                if (count($datavacancy) > 0) $vacancy = $datavacancy[0];
            }
            $vacancy = $vacancy;
        }

        return Api::responseData(["kost" => $response["kost"], "apartment" => $response['apartment'], "vacancy" => $vacancy]);
    }

    public function getTitikTengah($location){
        $latitude = ($location[0][0] + $location[1][0]) / 2;
        $longitude = ($location[0][1] + $location[1][1]) / 2;
        return $latitude." , ".$longitude;
    }

}
