<?php

namespace App\Http\Controllers\Api;

use App\Http\Helpers\ApiResponse as Api;
use App\Http\Middleware\ApiMiddleware;
use App\Repositories\SliderRepository;
use Illuminate\Http\Request;

class SliderController extends BaseController
{
    /**
     * @var SliderRepository
     */
    protected $repository;

    public function __construct(SliderRepository $repository)
    {
        parent::__construct();

        $this->repository = $repository;
    }

    public function getSliders(Request $request)
    {
        $sliders = $this->repository->getSliderList();

        return Api::responseData(
            [
                'sliders' => $sliders,
            ]
        );
    }

    public function getSlider(Request $request)
    {
        $endpoint = $request->endpoint;

        $slider = $this->repository->getSlider($endpoint);

        if (empty($slider)) {
            return Api::responseData(
                [
                    'status' => false,
                    'meta'   => [
                        'message' => 'Invalid endpoint!',
                    ],
                ],
                404
            );
        }

        return Api::responseData(
            [
                'slider' => $slider,
            ]
        );
    }
}
