<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Helpers\ApiResponse as Api;
use App\Entities\Landing\LandingSuggestionBlock;
use Validator;

class LandingSuggestionBlockController extends BaseController
{
    public function suggest(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'latitude' => 'required',
            'longitude' => 'required'
        ]);

        if ($validator->fails()) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'messages' => 'gagal menemukan data'
                ]
            ]);
        }

        $landingSuggestions = LandingSuggestionBlock::with('landing_rooms')
                                ->where('latitude_1', '<', $request->latitude)
                                ->where('latitude_2', '>', $request->latitude)
                                ->where('longitude_1', '<', $request->longitude)
                                ->where('longitude_2', '>', $request->longitude)
                                ->get();

        if (count($landingSuggestions) > 0) {

            $closestDistance = 0;
            $closestSuggestion = null;

            foreach($landingSuggestions as $landingSuggestion) {
                $latitudeDistance = ($request->latitude - $landingSuggestion->latitude_1) / 2;
                $longitudeDistance = ($request->longitude - $landingSuggestion->longitude_1) / 2;

                $distance = abs(($latitudeDistance + $longitudeDistance) / 2);

                if ($closestDistance == 0) {
                    $closestDistance = $distance;
                    $closestSuggestion = $landingSuggestion;
                } elseif ($closestDistance != 0 && $distance < $closestDistance) {
                    $closestDistance = $distance;
                    $closestSuggestion = $landingSuggestion;
                }
            }

            $suggestionList = [];

            if (count($closestSuggestion->landing_rooms) > 0) {
                foreach ($closestSuggestion->landing_rooms as $landing) {
                    $suggestionList[] = [
                        'slug' => $landing->slug,
                        'alias' => $landing->pivot->alias,
                    ];
                }
            }

            return Api::responseData([
                'data' => [
                    'name' => $closestSuggestion->name,
                    'landing_rooms' => $suggestionList
                ]
            ]);

        } else {
            return Api::responseData([
                'data' => [
                    'name' => '',
                    'landing_rooms' => []
                ]
            ]);
        }


    }
}
