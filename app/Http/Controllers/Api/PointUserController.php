<?php

namespace App\Http\Controllers\Api;

use App\Entities\Level\KostLevel;
use App\Entities\Point\Point;
use App\Entities\Point\PointHistory;
use App\Entities\Point\PointSetting;
use App\Entities\Room\Room;
use App\Http\Helpers\ApiResponse as Api;
use App\Presenters\PointPresenter;
use App\Repositories\OwnerDataRepository;
use App\Repositories\Point\PointRepository;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class PointUserController extends BaseController
{
    private $repository;
    private $presenter;
    private $ownerRepo;

    protected const DEFAULT_PAGINATION = 10;

    public function __construct(PointRepository $repository, PointPresenter $presenter, OwnerDataRepository $ownerRepo)
    {
        $this->repository = $repository;
        $this->presenter = $presenter;
        $this->ownerRepo = $ownerRepo;
    }

    public function total(): JsonResponse
    {
        $user = $this->user();

        if (empty($user)) {
            return Api::responseUserFailed();
        }
        
        $pointUser = $user->point_user()->first();
        $point = (int) (optional($pointUser)->total);
        if ($pointUser) {
            if ($user->isOwner() && !$pointUser->isEligibleEarnPointOwner()) {
                $point = null;
                $pointUser = null;
            } elseif (!$user->isOwner() && !$pointUser->isEligibleEarnPointTenant()) {
                $point = null;
                $pointUser = null;
            }
        }

        $nearExpiredDate = null;
        $nearExpiredPoint = null;
        if (!is_null($pointUser) && !is_null($pointUser->expired_date)) {
            $nearExpired = $this->repository->getUserPointCountByDate($user)->first();
            
            $nearExpiredDate = optional($nearExpired)->expired_date;
            $nearExpiredPoint = !is_null($nearExpired) ? (int) $nearExpired->total_point : null;
            $nearExpiredPoint = ($point < $nearExpiredPoint) ? $point : $nearExpiredPoint;
        }

        return Api::responseData([
            'point' => $point,
            'near_expired_point' => $nearExpiredPoint,
            'near_expired_date' => $nearExpiredDate
        ]);
    }

    public function expiry(): JsonResponse
    {
        $user = $this->user();
        if (empty($user)) {
            return Api::responseUserFailed();
        }

        $pointUser = $user->point_user()->first();

        if ($pointUser && !$pointUser->isEligibleEarnPointTenant()) {
            return Api::responseData([
                'point' => null,
                'expiry_in' => null,
                'near_expiry' => null
            ]);
        }

        $point = Point::wherePointDefault($user)->first();
        $expiryValue = (int) optional($point)->expiry_value;

        $this->presenter->setResponseType(PointPresenter::RESPONSE_TYPE_EXPIRY_DETAIL);
        $this->repository->setPresenter($this->presenter);
        $nearExpiry = $this->repository->getUserPointCountByDate($user);

        return Api::responseData([
            'point' => (int) optional($pointUser)->total,
            'expiry_in' => trim($this->getExpiredIn($expiryValue)),
            'near_expiry' => $nearExpiry['data']
        ]);
    }

    public function tnc(): JsonResponse
    {
        $user = $this->user();
        if (empty($user)) {
            return Api::responseUserFailed();
        }
        
        $tnc = trim(optional(Point::wherePointDefault($user)->first())->tnc);
        return Api::responseData(compact('tnc'));
    }

    public function activity(Request $request): JsonResponse
    {
        $user = $this->user();
        if (empty($user)) {
            return Api::responseUserFailed();
        }
        
        $userType = $request->get('target', 'owner');
        if (!in_array($userType, ['owner', 'tenant'])) {
            $userType = 'owner';
        }
        
        $this->presenter->setResponseType(PointPresenter::RESPONSE_TYPE_ACTIVITY_LIST);
        $this->repository->setPresenter($this->presenter);
        $activity = $this->repository->getActivityListByUserType($userType);

        return Api::responseData($activity);
    }

    public function scheme(Request $request): JsonResponse
    {
        $user = $this->user();
        if (empty($user)) {
            return Api::responseUserFailed();
        }

        $userType = $request->get('target', 'owner');
        if (!in_array($userType, ['owner', 'tenant'])) {
            $userType = 'owner';
        }

        $goldplusIds = KostLevel::getGpLevelIds();

        $userLargestLevelSub = KostLevel::whereLevelsBelongToOwner($goldplusIds, $user->id);
        $activeSegment = Point::joinSub($userLargestLevelSub, 'user_level', function ($q) {
                $q->on('point.kost_level_id', '=', 'user_level.id');
            })
            ->select([
                'point.type',
                'user_level.order',
            ])
            ->where('point.target', Point::TARGET_OWNER)
            ->where('point.is_published', 1)
            ->orderBy('user_level.order', 'DESC')
            ->first();
        $segments = Point::leftJoin('kost_level', function ($q) {
                $q->on('point.kost_level_id', '=', 'kost_level.id');
            })
            ->select([
                'point.id',
                'point.type',
                'point.title',
                'point.value',
                'point.kost_level_id',
                \DB::raw('COALESCE(kost_level.order, 0) sequence'),
            ])
            ->where('point.target', $userType)
            ->where('point.is_published', 1)
            ->orderBy('sequence', 'ASC')
            ->get();
        $activities = $this->repository->getActivityListByUserType($userType);
        $settings = $this->repository->getSettingList();
        
        $user->load(['room_owner_verified.room']);
        $roomCount = $this->ownerRepo->getRoomAvailability(
            $user->room_owner_verified,
            function (Room $room) {
                return $room['is_booking'];
            }
        )['total'];

        $result = [];
        foreach ($segments as $segment) {
            $segmentItem = [
                'type' => (string) $segment->type,
                'title' => (string) $this->getPointSegmentTitle($segment),
                'is_active' => ((is_null($activeSegment) && in_array($segment->type, [Point::DEFAULT_TYPE, Point::DEFAULT_TYPE_TENANT])) || 
                                (!is_null($activeSegment) && $segment->type === $activeSegment->type)),
                'activities' => []
            ];

            foreach ($activities as $activity) {
                $item = [
                    'title' => (string) $activity->title,
                    'description' => (string) $activity->description,
                    'point_details' => null
                ];

                foreach ($settings as $setting) {
                    if ($setting->point_id === $segment->id && $setting->activity_id === $activity->id) {
                        $minPoint = $segment->value * $setting->received_each;
                        $maxPoint = $setting->limit;
                        if ($setting->limit_type === PointSetting::LIMIT_ONCE) {
                            $maxPoint = $minPoint;
                        } elseif ($setting->limit === PointSetting::LIMIT_ROOM_COUNT) {
                            $maxPoint = $roomCount * $setting->received_each;
                        }

                        $item['point_details'] = [
                            'min_point' => (int) $minPoint,
                            'max_point' => (int) $maxPoint,
                            'limit_type' => (string) ($setting->limit === PointSetting::LIMIT_ROOM_COUNT ? '' : PointSetting::limitTypeOptions()[$setting->limit_type])
                        ];
                        
                        break;
                    }
                }

                $segmentItem['activities'][] = $item;
            }
            
            $result[] = $segmentItem;
        }
        
        return Api::responseData([
            'data' => $result
        ]);
    }

    public function history(Request $request): JsonResponse
    {
        $user = $this->user();
        if (empty($user)) {
            return Api::responseUserFailed();
        }

        $limit = (int) $request->get('limit', self::DEFAULT_PAGINATION);
        $filterByType = (string) $request->get('type', PointHistory::HISTORY_TYPE_ALL);

        $this->presenter->setResponseType(PointPresenter::RESPONSE_TYPE_HISTORY_LIST);
        $this->repository->setPresenter($this->presenter);
        $history = $this->repository->getPointHistoryByUser($user, $limit, $filterByType);

        $pointUser = $user->point_user()->first();
        $isBlacklisted = false;
        if ($pointUser) {
            if ($user->isOwner()) {
                $isBlacklisted = !$pointUser->isEligibleEarnPointOwner();
            } elseif (!$user->isOwner()) {
                $isBlacklisted = !$pointUser->isEligibleEarnPointTenant();
            }
        }

        $response = $this->transformDataPagination($history) + [
            'is_blacklisted' => $isBlacklisted
        ];
        
        return Api::responseData($response);
    }

    private function transformDataPagination($data): array
    {
        $pagination = $data['meta']['pagination'];
        $transformedData = [
            'data' => $data['data'],
            'has_next' => ($pagination['current_page'] < $pagination['total_pages']),
            'current_page' => $pagination['current_page'],
            'limit' => $pagination['per_page'],
            'total_pages' => $pagination['total_pages']
        ];

        return $transformedData;
    }

    private function getPointSegmentTitle(Point $segment): string
    {
        if ($segment->type === Point::DEFAULT_TYPE || $segment->type === Point::DEFAULT_TYPE_TENANT) {
            return 'Booking Langsung';
        } elseif ($segment->type === Point::MAMIROOMS_TYPE_TENANT) {
            return 'Mamirooms';
        } elseif (empty($segment->title)) {
            return strtoupper($segment->type);
        }

        return $segment->title;
    }

    private function getExpiredIn(int $expiryValue): string
    {
        if ($expiryValue <= 0) {
            return '';
        }

        $expiryUnit = array_get(Point::expiryUnitOptions(), Point::EXPIRY_UNIT_MONTH);
        return "{$expiryValue} {$expiryUnit}";
    }
}
