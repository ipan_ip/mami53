<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use SendBird;
use Validator;

use App\User;
use App\Http\Helpers\ApiResponse as Api;
use App\Repositories\UserDataRepository;
use App\Entities\Room\Room;
use App\Libraries\SMSLibrary;
use App\Http\Helpers\RegexHelper;
use Illuminate\Http\JsonResponse;
use App\Jobs\MoEngage\SendMoEngageUserPropertiesQueue;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Exception;

class UserDataController extends BaseController
{
    protected $kostValidationMessages = [
        "kost_time.required"   => "Waktu masuk kos tidak boleh kosong",
        "kost_time.date"       => "Waktu masuk kos tidak boleh kosong",
        "id.required"          => "Kos tidak boleh kosong",  
        "birthday.required"    => "Tanggal lahir tidak boleh kosong",
        "birthday.date"        => "Tanggal lahir tidak sesui format",
        "gender.required"      => "Jenis kelamin tidak boleh kosong",
        "gender.in"            => "Jenis kelamin tidak sesuai",
        "education.required"   => "Pendidikan terakhir tidak boleh kosong",
        "city.required"        => "Kota asal tidak boleh kosong",
        "jobs.required"        => "Jenis user tidak boleh kosong",
        "name.required"        => "Nama user tidak boleh kosong",
        "name.min"             => "Nama user min :min karakter",
        "name.max"             => "Nama user max :max karakter",
        "name.regex"           => "Mohon masukkan karakter alfabet",
        "for_param.required"   => "Tipe edit tidak boleh kosong",
        "email.email"          => "Email tidak sesuai format",
        "phone.required"       => "No tidak boleh kosong",
        "media.required"       => "Gagal upload media",
        "media.mimes"          => "Jenis file tidak diperkenankan, harap menggunakan file JPG atau PNG",
        "media.max"            => "Ukuran file tidak boleh melebihi 2MB",
        "photo.required"       => "Gagal upload media",
        "photo.mimes"          => "Jenis file tidak diperkenankan, harap menggunakan file JPG atau PNG",
        "photo.max"            => "Ukuran file tidak boleh melebihi 2MB"
    ];

	public function __construct(UserDataRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;
    }

    /**
     * Check is tenant is user social (user that login/register from social account)
     * The social account are Facebook OR Google
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function isTenantUserSocial(): JsonResponse
    {
        $user = $this->user();
        if (empty($user)) {
            throw new \Exception(__('api.tenant.error.not_found'));
        }

        if ($user->is_owner === 'true') {
            return Api::responseUserFailed([
                'data' => null
            ]);
        }

        return Api::responseData(['status' => $this->repository->isUserSocial($user->id)]);
    }

    public function profile()
    {
        $user = $this->user();
        
	    if ($user->is_owner == 'true') return Api::responseUserFailed(array("data" => null));

	    $response = $this->repository->getProfileUser($user, 'app');

	    return Api::responseData($response);	
    }

    public function getEdit(Request $request)
    {
        $user = $this->user();
        if ($user->is_owner == 'true') return Api::responseUserFailed(array("data" => null));

        $response = $this->repository->getEditUser($user);

        return Api::responseData($response);
    }

    public function postUpdate(Request $request)
    {
        $user = $this->user();
        if ($user->is_owner == 'true') return Api::responseUserFailed(array("data" => null, "message" => array("User tidak ditemukan")));

        // form validation
        $validator = Validator::make($request->all(), [
                'name'     => 'required|min:3|max:50',
                'birthday' => 'required|date',
                'gender'   => 'required|in:male,female',
                //'education'=> 'required',
                //'city'     => 'required',
                'jobs'     => 'required|in:kuliah,kerja,lainnya',
            ], $this->kostValidationMessages);    

        if($validator->fails()) {
            $response = [
                "status"  => false,
                "message" => $validator->errors()->all(),
                "data"    => null 
            ];
            return Api::responseData($response);
        }

        $response = $this->repository->postUpdate($request->all(), $user);

        if (! is_null($response)) {
            //ADD USER PROPERTIES FOR MoEngage Tracking
            try {
                SendMoEngageUserPropertiesQueue::dispatch(
                    (int) $user->id,
                    ['first_name' => $user->name]
                )->delay(now()->addMinutes(15));
            } catch (Exception $e) {
                Bugsnag::notifyException($e);
            }
        }

        SendBird::updateUser($user->id, $response['data']->name, $response['data']->photo['small']);

        return Api::responseData($response);
    }

    public function changeProfilePhoto(Request $request)
    {
        $user = $this->user();

        if (!$user) return Api::responseUserFailed(['data' => null, 'message' => ['User tidak ditemukan']]);

        // form validation
        if (isset($request['photo'])) {
            $validator = Validator::make($request->all(), [
                'photo'  => 'required|mimes:jpeg,bmp,png,jpg|max:2048', // Max size: 2MB
            ], $this->kostValidationMessages); 

            if ($validator->fails()) {
                $response = [
                    'status'  => false,
                    'message' => $validator->errors()->all(),
                    'data'    => null 
                ];
                return Api::responseData($response);
            }   
        }

        if ((isset($request['media']))) {
            $validator = Validator::make($request->all(), [
                'media'  => 'required|mimes:jpeg,bmp,png,jpg|max:2048', // Max size: 2MB
            ], $this->kostValidationMessages);   

            if ($validator->fails()) {
                $response = [
                    'status'  => false,
                    'message' => $validator->errors()->all(),
                    'data'    => null 
                ];
                return Api::responseData($response);
            }
        }

        $response = $this->repository->uploadProfilePhoto($request->all(), $user);
        if (isset($response['media']->url)) {
          SendBird::updateUser($user->id, '', $response['media']->url['small']);
        }

        return Api::responseData($response); 
    }

    public function getUserJobsApply(Request $request)
    {
        $user = $this->user();
        if (is_null($user)) return Api::responseData(["status" => false, "data" => null]);
        $response = $this->repository->getApply($user);
        return Api::responseData($response);
    }

    public function checkinAction(Request $request)
    {
        $user = $this->user();
        
        // form validation
        $validator = Validator::make($request->all(), [
                'kost_time'  => 'required|date',
                'id' => 'required'
            ], $this->kostValidationMessages);    

        $room = Room::where('song_id', $request->input('id'))->first();

        $validator->after(function($validator) use ($room, $user) {
                if (is_null($user) or $user->is_owner == 'true') {
                    $validator->errors()->add('user', 'User tidak ditemukan');
                }

                if (is_null($room)) {
                    $validator->errors()->add('id', 'Data kos tidak ditemukan');
                }
        });  

        if($validator->fails()) {
            $response = [
                "status"  => false,
                "message" => $validator->errors()->all(),
                "data"    => null 
            ];
            return Api::responseData($response);
        }
        
        $data = array_merge(array("designer_id" => $room->id), $request->all());

        $response = $this->repository->checkinAction($data, $user);
        return Api::responseData($response);
    }

    public function getCheckUserIncomplete(Request $request)
    {
        $user = $this->user();
        if (is_null($user)) return Api::responseUserFailed(array("data" => null, "message" => array("User tidak ditemukan")));

        $response = $this->repository->checkUserIncomplete($user, $request->all());
        return Api::responseData($response);    
    }  

    public function simplyEdit(Request $request)
    {
        $user = $this->user();
        if (is_null($user) or $user->is_owner == 'true') return Api::responseUserFailed(array("meta" => array("message" => "User tidak ditemukan")));

        // Filter for email data: 
        if (isset($request->email))
        {   
            // Replace any [space] with [underscore] & convert all characters to lowercase
            $filteredEmail = strtolower(str_replace(' ', '_', $request->email));
            $request->merge(['email' => $filteredEmail]);
        }
        
        $validator = Validator::make($request->all(), [
                'for_param'  => 'required|in:survey,call,fav,review,recommendation,report,afterlogin,rekomendasi',
                'email'      => 'email'
            ], $this->kostValidationMessages);

        if($validator->fails()) {
            $response = [
                "status"  => false,
                "meta" => [
                    "message" => "Gagal update data",
                    "reason" => $validator->errors()->all()
                ],
                "data"    => null 
            ];
            return Api::responseData($response);
        }

        if ($request->filled('phone')) {
            $checkValidPhone = SMSLibrary::validateIndonesianMobileNumber($request->input('phone'));
            if (!$checkValidPhone) {
                return Api::responseUserFailed(array("meta" => array("message" => "No hp tidak sesuai format")));
            }
        }

        $needPhone = ["call", "review"];   
        if (in_array($request->input('for_param'), $needPhone)) {
            if (!$request->filled('phone') OR SMSLibrary::validateIndonesianMobileNumber($request->input('phone')) == false) {
                return Api::responseUserFailed(array("meta" => array("message" => "No hp tidak sesuai format")));
            }   
        }

        $needEmail = ["recommendation", "report"];
        if (in_array($request->input('for_param'), $needEmail)) {
            if (!$request->filled('email') OR filter_var($request->input('email'), FILTER_VALIDATE_EMAIL) == false) {
               return Api::responseUserFailed(array("meta" => array("message" => "Email tidak sesuai format")));    
            }
        }

        $this->repository->updateUserPhone($user, $request);

        return Api::responseData(array("meta" => array("message" => "Data berhasil update")));
    }

    public function changePhoneNumber(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'phone' => 'required'
        ], $this->kostValidationMessages);
        
        $dataPost = $request->all();
        $user     = $this->user();

        if($validator->fails()) {
            $response = [
                "status"  => false,
                "meta"   => [
                    "message" => "Nomor tidak boleh kosong"
                ]
            ];
            return Api::responseData($response);
        }
        
        $isValid = SMSLibrary::validateIndonesianMobileNumber($dataPost['phone']);
        if (!$isValid || is_null($user)) {
            $response = [
                'status' => false,
                "meta"   => [
                    "message" => "Maaf, Nomor tidak valid, silahkan gunakan nomor yang lain"
                ]
            ];

            return Api::responseData($response);
        }
        
        $response = $this->repository->changePhoneNumber($request->all(),$user);
        
        return Api::responseData(array(
            "status" => $response['status'], 
            "meta"   => [
                "message" => $response['message']
            ]
        ));
    } 


    public function setOccupation(Request $request)
    {
        $user = $this->user();

        if (!$user) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Silakan login terlebih dahulu'
                ]
            ]);
        }

        $occupation = in_array($request->input('occupation'), ['kerja', 'kuliah']) ? 
            $request->input('occupation') : 'lainnya';

        $user->job = $occupation;
        if ($occupation == 'lainnya') {
            $user->description = $request->input('occupation');
        }
        $user->save();

        return Api::responseData([
            'status' => true
        ]);
    }

    public function changeName(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            ['name' => ('required|max:50|regex:' . RegexHelper::name())],
            $this->kostValidationMessages
        );

        $user = $this->user();

        if (!$user) {
            return Api::responseUserFailed(['meta' => ['message' => 'User tidak ditemukan']]);
        }

        if ($validator->fails()) {
            $response = [
                'status'  => false,
                'meta' => [
                    'message' => $validator->errors()->first('name')
                ]
            ];
            return Api::responseData($response);
        }

        $response = $this->repository->changeName($request->all(), $user);
        SendBird::updateUser($user->id, $response['name']);

        return Api::responseData([
            'status' => true,
            'meta'   => [
                'message' => $response['message']
            ]
        ]);
    }

    public function changeEmail(Request $request)
    {
        // Filter for email data: 
        if (isset($request->email))
        {   
            // Replace any [space] with [underscore] & convert all characters to lowercase
            $filteredEmail = strtolower(str_replace(' ', '_', $request->email));
            $request->merge(['email' => $filteredEmail]);
        }

        $validator = Validator::make($request->all(),[
            'email' => 'email'
        ], $this->kostValidationMessages);
        
        $user     = $this->user();

        if($validator->fails()) {
            $response = [
                "status"  => false,
                'meta' => [
                    'message' => 'Maaf. Email tidak valid. Silahkan gunakan alamat email yang lain'
                ]
            ];
            return Api::responseData($response);
        }
        
        $response = $this->repository->changeEmail($request->all(),$user);

        return Api::responseData(array(
            "status" => true, 
            "meta"   => [
                "message" => $response['message']
            ]
        ));
    }

    public function updateTerms(Request $request) 
    {
        if (isset($request->agree)) {
            $user = $this->user();
            $currentTcStatus = $user->agree_on_tc;
            $newTcStatuc = $request->agree ? 'true' : 'false';

            if ($currentTcStatus != $newTcStatuc) 
            {
                $response = $this->repository->changeTcStatus($newTcStatuc, $user);
            }
            else 
            {
                $response = [
                    "status" => false,
                    'meta' => [
                        'message' => 'User sudah memperbarui status T&C Agreement'
                    ]
                ];
            }
        }

        return Api::responseData(array(
            "status" => $response['status'], 
            "meta"   => [
                "message" => (!empty($response['message'])) ? $response['message'] : $response['meta']['message']
            ]
        ));
    }

    public function issueChatToken($userId)
    {
        $response = [
            'status'  => false,
            'meta' => ['message' => '']
        ];

        // Based on the tinker test, $userId == 0 is enough
        // >>> $userIdString = "0"
        // => "0"
        // >>> $userId = 0
        // => 0
        // >>> $userIdString == $userId
        // => true
        // >>> $userIdString === $userId
        // => false
        if (empty($userId) || $userId == 0) {
            $response['meta']['message'] = 'User ID tidak boleh kosong';
            return Api::responseData($response);
        }
        $user = User::find($userId);
        if (empty($user)) {
            $response['meta']['message'] = 'User ID tidak valid';
            return Api::responseData($response);
        }

        $user->tryCreateChatUser();
        $response['status'] = true;
        $response['meta']['message'] = 'Berhasil issue chat token';
        return Api::responseData($response);
    }
}
