<?php

namespace App\Http\Controllers\Api\Squarepants;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Entities\Squarepants\Agents;
use App\Http\Helpers\ApiResponse as Api;
use App\Entities\Squarepants\NotifToApp;

class AppController extends Controller
{
	protected $validationMessages = [
		"email.required" => "Email tidak boleh kosong",
		"token.required" => "Token tidak boleh kosong",
	];

    public function agentRegister(Request $request)
    {
    	$validator = Validator::make($request->all(), 
            [
                'email'   => 'required',
                'token'   => 'required'
            ], $this->validationMessages);

        if($validator->fails()) {
            return Api::responseData([
                    'status'   => false,
                    'messages' => $validator->errors()->all()
                ]);
        }

        $agent = Agents::where('email', $request->input('email'))->first();

        if (is_null($agent)) {
        	$this->insertAgent($request->all());
        } else {
        	$agent->email = $request->input('email');
        	$agent->token = $request->input('token');
        	$agent->save();
        }

        //(new NotifToApp)->Send("sas", "sds");

        return Api::responseData(["status" => true, "messages" => ["Berhasil mendaftar"]]);
    }

    public function insertAgent($request)
    {
    	$agent = new Agents();
    	$agent->email = $request['email'];
    	$agent->token = $request['token'];
    	$agent->save();
    	return true;
    }
}
