<?php

namespace App\Http\Controllers\Api;

use App\Http\Helpers\ApiResponse as Api;
use App\Services\Polling\PollingService;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class UserPollingController extends BaseController
{
    private $pollingService;

    public function __construct(PollingService $pollingService)
    {
        $this->pollingService = $pollingService;
    }

    public function detail(string $key): JsonResponse
    {
        $user = $this->user();

        if (empty($user)) {
            return Api::responseUserFailed();
        }

        $hasSubmitted = $this->pollingService->hasSubmittedPolling($key, $user);
        $polling['data'] = [];

        if (!$hasSubmitted) {
            $polling = $this->pollingService->getPollingDetail($key);
            if (!$polling) {
                return Api::responseError(false, false, 200, 404);
            }
        }

        $polling['data'] += [
            'has_submitted' => $hasSubmitted
        ];

        return Api::responseData($polling);
    }

    public function submit(string $key, Request $request): JsonResponse
    {
        $user = $this->user();

        if (empty($user)) {
            return Api::responseUserFailed();
        }

        $submitResponse = $this->pollingService->submitPolling($key, $user, $request->all());

        return Api::responseData([
            'status'        => $submitResponse['status'],
            'meta'          => [
                'message'   => $submitResponse['message']
            ],
            'action_code'   => $submitResponse['action_code']
        ]);
    }
}