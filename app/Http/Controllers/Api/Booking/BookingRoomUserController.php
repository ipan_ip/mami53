<?php

namespace App\Http\Controllers\Api\Booking;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseController;
use App\Http\Helpers\ApiResponse as Api;
use Validator;
use Carbon\Carbon;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Booking\BookingUser;
use App\Presenters\BookingUserPresenter;
Use App\User;
use App\Http\Helpers\BookingInvoiceHelper;
use App\Presenters\Mamipay\MamipayContractPresenter;
use App\Repositories\Booking\BookingUserRepository;
use App\Repositories\Contract\ContractRepository;

class BookingRoomUserController extends BaseController
{
    public function getRoomWithContract(BookingUserRepository $bookingUserRepo, ContractRepository $contractRepository, MamipayContractPresenter $mamipayContractPresenter)
    {
        $user = $this->user();

        if (is_null($user)) {
            return Api::responseData([
                'status' =>false,
                'meta'   => [
                    'message' => 'Data user tidak ditemukan'
                ]
            ]);
        }

        $bookingUser = $bookingUserRepo->getMyRoom($user->id);

        if ($bookingUser) {
            $transformData = (new BookingUserPresenter('my-room'))->present($bookingUser);

            $contract = MamipayContract::with(['invoices.additional_costs'])->find($bookingUser->contract_id);
            if (!$contract) {
                return Api::responseData([
                    'status' => false,
                    'meta'   => [
                        'message' => 'Data contract tidak ditemukan'
                    ]
                ]);
            }

            $invoices = BookingInvoiceHelper::invoice($contract->invoices);
            $years = BookingInvoiceHelper::years($invoices);

            $transformData['data']['invoice'] = $invoices;
            $transformData['data']['years'] = $years;

            return Api::responseData([
                'data'   => $transformData['data']
            ]);

        } else {
            // Check is there any consultant contract
            $contract = $contractRepository->getMyRoomContract($user->id);
            if (empty($contract)) {
                return Api::responseData([
                    'status' => false,
                    'meta'   => [
                        'message' => 'Data booking tidak ditemukan'
                    ]
                ]);
            }

            $transformData = $mamipayContractPresenter->present($contract);
            $invoices = BookingInvoiceHelper::invoice($contract->invoices);
            $transformData['data']['invoice'] = $invoices;
            $transformData['data']['years'] = BookingInvoiceHelper::years($invoices);

            return Api::responseData([
                'data'   => $transformData['data']
            ]);
        }
    }

}
