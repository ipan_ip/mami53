<?php

namespace App\Http\Controllers\Api\Booking;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseController;
use App\Http\Helpers\ApiResponse as Api;

use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use RuntimeException;

use App\Entities\Booking\BookingUser;

class BookingStatusController extends BaseController
{
    public function getListStatus()
    {
        $status = [
            BookingUser::BOOKING_STATUS_BOOKED,
            BookingUser::BOOKING_STATUS_CONFIRMED,
            BookingUser::BOOKING_STATUS_PAID,
            BookingUser::BOOKING_STATUS_VERIFIED,
            BookingUser::BOOKING_STATUS_CHECKED_IN,
            BookingUser::BOOKING_STATUS_CANCELLED,
            BookingUser::BOOKING_STATUS_CANCEL_BY_ADMIN,
            BookingUser::BOOKING_STATUS_REJECTED,
            BookingUser::BOOKING_STATUS_EXPIRED,
            BookingUser::BOOKING_STATUS_EXPIRED_DATE,
            BookingUser::BOOKING_STATUS_TERMINATED,
            BookingUser::BOOKING_STATUS_FINISHED,
        ];

        return Api::responseData([
            'data' => $status
        ]);
    }
}
