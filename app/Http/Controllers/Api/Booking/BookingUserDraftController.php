<?php
namespace App\Http\Controllers\Api\Booking;

use App\Entities\Booking\BookingUserDraft;
use App\Exceptions\CustomErrorException;
use App\Http\Controllers\Api\BaseController;
use App\Http\Helpers\ApiResponse as Api;
use App\Http\Requests\Booking\BookingUserDraftRequest;
use App\Jobs\SendPushNotificationQueue;
use App\Presenters\Booking\BookingUserDraftPresenter;
use App\Repositories\Booking\BookingUserDraftRepository;
use Illuminate\Http\JsonResponse;
use Exception;
use Bugsnag;
use Illuminate\Http\Request;

class BookingUserDraftController extends BaseController
{
    protected $repository;

    public function __construct(BookingUserDraftRepository $repo)
    {
        $this->repository = $repo;
        parent::__construct();
    }

    public function index(Request $request): JsonResponse
    {
        // force default response to false
        $response = $this->makeDefaultResponse();
        try {
            $user = $this->user();
            if (is_null($user)) throw new CustomErrorException('Data user tidak ditemukan');

            // params filter and other
            $params = [];
            $params['limit']    = $request->limit ?? 5;
            $params['order']    = $request->order ?? 'checkin';
            $params['sort']     = $request->sort ? strtoupper($request->sort) : 'ASC';
            $draft = $this->repository->listingViewedAndDraft($user, $params);
            $tranform = (new BookingUserDraftPresenter('list'))->present($draft);

            // set draft to has been read
            $this->repository->setDraftHasBeenRead($user);

            $response = [
                'data' => [
                    'data'          => $tranform['data'] ?? null,
                    'current_page'  => $draft->currentPage(),
                    'from'          => $draft->firstItem(),
                    'last_page'     => $draft->lastPage(),
                    'next_page_url' => $draft->nextPageUrl(),
                    'per_page'      => $draft->perPage(),
                    'prev_page_url' => $draft->previousPageUrl(),
                    'to'            => $draft->count(),
                    'total'         => $draft->total(),
                    'has_more'      => $draft->hasMorePages()
                ]
            ];

        } catch (CustomErrorException $e) {
            $response['meta']['message'] = $e->getMessage();
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
            $response['meta']['message'] = $e->getMessage();
        }
        return Api::responseData($response);
    }

    /**
     * Store draft-booking
     * @param BookingUserDraftRequest $request
     * @return JsonResponse
     */
    public function store(BookingUserDraftRequest $request): JsonResponse
    {
        // force default response to false
        $response = $this->makeDefaultResponse();
        try {
            $validator = $request->failedValidator;
            if ($validator && $validator->fails()) throw new CustomErrorException(implode(', ',$validator->errors()->all()));

            $user = $this->user();
            if (is_null($user)) throw new CustomErrorException('Data user tidak ditemukan');

            // get room owner
            $roomOwner = $this->repository->getRoomOwner((int) $request->room_id);

            $params = [
                'designer_id'           => $request->room_id,
                'user_id'               => $user->id,
                'designer_owner_id'     => $roomOwner ? $roomOwner->id : null,
                'rent_count_type'       => $request->rent_count_type ?? null,
                'duration'              => $request->duration ?? null,
                'checkin'               => $request->checkin ?? null,
                'checkout'              => $request->checkout ?? null,
                'tenant_name'           => $request->contact_name ?? null,
                'tenant_phone'          => $request->contact_phone ?? null,
                'tenant_job'            => $request->contact_job ?? null,
                'tenant_introduction'   => $request->contact_introduction ?? null,
                'tenant_work_place'     => $request->contact_work_place ?? null,
                'tenant_description'    => $request->contact_work_place ?? null,
                'tenant_gender'         => $request->contact_gender ?? null,
                'total_renter_count'    => $request->total_renter ?? null,
                'is_married'            => $request->is_married ?? false,
                'status'                => BookingUserDraft::DRAFT_CREATED,
                'read'                  => false,
                'session_id'            => $request->session_id ?? null
            ];

            $draft = $this->repository->createOrUpdate($params);
            if ($draft == null) throw new CustomErrorException('Draft gagal terbuat, coba lagi');

            // mapping is first draft or not
            $isFirstCreated = $this->repository->totalOfDraftWithTrashedAndStatus($draft, [BookingUserDraft::DRAFT_CREATED, BookingUserDraft::ALREADY_BOOKING]);
            $draft->first_draft = $isFirstCreated === 1 ? true : false;

            if ($draft->created_at == $draft->updated_at) {
                // prepare save notification data
                $notifData = [];
                $notifData['user_id'] = $user->id;
                $notifData['title'] = 'Ada Booking Kos yang masih menunggu nih!';
                $notifData['type'] = 'booking_draft';
                $notifData['read'] = 'false';
                $notifData['url'] = '/user/booking/draft';
                $notifData['schema'] = 'bang.kerupux.com://list_booking?draft=true';
                $notifData['designer_id'] = $request->room_id;

                // save notification
                $this->repository->createNotificationCenter($notifData);

                // send notification with queue job
                SendPushNotificationQueue::dispatch(__('notification.booking-draft.created'), $user->id, false);
            }

            $response = [
                'data' => $draft
            ];

        } catch (CustomErrorException $e) {
            $response['meta']['message'] = $e->getMessage();
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
            $response['meta']['message'] = $e->getMessage();
        }
        return Api::responseData($response);
    }

    /**
     * Get data homepage draft booking
     * @return JsonResponse
     */
    public function homepageShortcut(): JsonResponse
    {
        // force default response to false
        $response = $this->makeDefaultResponse();
        try {
            $user = $this->user();
            if (is_null($user)) throw new CustomErrorException('Data user tidak ditemukan');

            $draftBooking = $this->repository->getHomepageShortcutData($user);
            $draftBookingTranform = null;
            if ($draftBooking['data'] !== null)
                $draftBookingTranform = (new BookingUserDraftPresenter('homepage-shortcut'))->present($draftBooking['data']);

            $response = [
                'data' => [
                    'total' => $draftBooking['total'],
                    'room' => $draftBookingTranform
                ]
            ];

        } catch (CustomErrorException $e) {
            $response['meta']['message'] = $e->getMessage();
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
            $response['meta']['message'] = $e->getMessage();
        }
        return Api::responseData($response);
    }

    /**
     * Destroy draft-booking
     * @param int|null $id
     * @return JsonResponse
     */
    public function destroy(int $id = null): JsonResponse
    {
        // force default response to false
        $response = $this->makeDefaultResponse();
        try {
            $user = $this->user();
            if (is_null($user)) throw new CustomErrorException('Data user tidak ditemukan');

            $draft = $this->repository->findDraft($id);
            if (!$draft) throw new CustomErrorException('Room tidak ditemukan');

            if ($user->id !== $draft->user_id) throw new CustomErrorException('Akses di tolak');

            $this->repository->delete($id);

            $response = [
                'meta' => [
                    'message' => 'Draft booking berhasil dihapus'
                ]
            ];

        } catch (CustomErrorException $e) {
            $response['meta']['message'] = $e->getMessage();
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
            $response['meta']['message'] = $e->getMessage();
        }
        return Api::responseData($response);
    }

    /**
     * View room listing history
     * @param Request $request
     * @return JsonResponse
     */
    public function roomHistoryViewed(Request $request): JsonResponse
    {
        // force default response to false
        $response = $this->makeDefaultResponse();
        try {
            $user = $this->user();
            if (is_null($user)) throw new CustomErrorException('Data user tidak ditemukan');

            // params filter and other
            $params = [];
            $params['limit']    = $request->limit ?? 5;
            $params['order']    = $request->order ?? 'viewed_at';
            $params['sort']     = $request->sort ? strtoupper($request->sort) : 'ASC';
            $history    = $this->repository->listingRoomHistory($user, $params);
            $tranform   = (new BookingUserDraftPresenter('list-room-history'))->present($history);

            $response = [
                'data' => [
                    'data'          => $tranform['data'] ?? null,
                    'current_page'  => $history->currentPage(),
                    'from'          => $history->firstItem(),
                    'last_page'     => $history->lastPage(),
                    'next_page_url' => $history->nextPageUrl(),
                    'per_page'      => $history->perPage(),
                    'prev_page_url' => $history->previousPageUrl(),
                    'to'            => $history->count(),
                    'total'         => $history->total(),
                    'has_more'      => $history->hasMorePages()
                ]
            ];

            // set draft to has been read
            $this->repository->setNewLastSeenToReaded($user);

        } catch (CustomErrorException $e) {
            $response['meta']['message'] = $e->getMessage();
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
            $response['meta']['message'] = $e->getMessage();
        }
        return Api::responseData($response);
    }

    /**
     * Delete viewed room
     * @param int|null $roomId
     * @return JsonResponse
     */
    public function destroyViewedRoom(int $roomId = null): JsonResponse
    {
        // force default response to false
        $response = $this->makeDefaultResponse();
        try {
            $user = $this->user();
            if (is_null($user)) throw new CustomErrorException('Data user tidak ditemukan');

            $viewed = $this->repository->findHistoryViewed($user, $roomId);
            if (!$viewed) throw new CustomErrorException('Room tidak ditemukan');

            $this->repository->deleteHistoryViewed($user, $roomId);

            $response = [
                'meta' => [
                    'message' => 'Kos Baru Dilihat berhasil dihapus'
                ]
            ];

        } catch (CustomErrorException $e) {
            $response['meta']['message'] = $e->getMessage();
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
            $response['meta']['message'] = $e->getMessage();
        }
        return Api::responseData($response);
    }

    /**
     * View badge notification
     * @param Request $request
     * @return JsonResponse
     */
    public function notification(): JsonResponse
    {
        // force default response to false
        $response = $this->makeDefaultResponse();
        try {
            $user = $this->user();
            if (is_null($user)) throw new CustomErrorException('Data user tidak ditemukan');

            $totalOfDraft = $this->repository->totalOfDraftNotYetSeen($user);
            $totalOfLastSeen = $this->repository->totalOfNewLastSeen($user);

            $response = [
                'data' => [
                    'badge' => ($totalOfDraft === 0 && $totalOfLastSeen === 0) ? false : true,
                    'total_of_draft' => $totalOfDraft,
                    'total_of_last_seen' => $totalOfLastSeen
                ]
            ];
        } catch (CustomErrorException $e) {
            $response['meta']['message'] = $e->getMessage();
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
            $response['meta']['message'] = $e->getMessage();
        }
        return Api::responseData($response);
    }

    /**
     * Function to return default response
     * @return array
     */
    private function makeDefaultResponse(): array
    {
        return [
            'status' => false,
            'meta' => ['message' => 'General Error']
        ];
    }
}
