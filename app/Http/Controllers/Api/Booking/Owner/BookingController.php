<?php

namespace App\Http\Controllers\Api\Booking\Owner;

use App\Entities\Booking\BookingOwner;
use App\Exceptions\CustomErrorException;
use App\Repositories\Booking\Owner\OwnerBookingRepository;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseController;
use Config;
use Validator;
use Auth;
use RuntimeException;
use App\Http\Helpers\ApiResponse as Api;
use App\Jobs\MoEngage\SendMoEngageUserPropertiesQueue;
use Exception;

class BookingController extends BaseController
{
    protected $validationMessages = [
        'rent_counts.required' => 'Durasi sewa tidak boleh kosong',
        'additional_prices.*.value.integer' => 'Format Harga tidak sesuai'
    ];

    public function registerInstantBook(Request $request, OwnerBookingRepository $repository)
    {
        try {
            $user = $this->user();
            if (is_null($user)) throw new CustomErrorException('Data User tidak ditemukan');

            if (!$user->isOwner()) throw new CustomErrorException('Maaf Anda tidak terdaftar sebagai owner');

            $isBookingAllRooms = BookingOwner::isBookingAllRoom($user);
            if (!$isBookingAllRooms) throw new CustomErrorException('Maaf Anda tidak aktif fitur booking');

            // Validate request
            $validator = Validator::make($request->all(),
                [
                    'rent_counts' => 'required',
                    'additional_prices.*.value' => ['integer']
                ], $this->validationMessages);
            if ($validator->fails()) throw new CustomErrorException(implode(', ',$validator->messages()->all()));

            $additionalPrices = $request->input('additional_prices');

            $params = [
                'rent_counts'        => $request->input('rent_counts'),
                'additional_prices'  => $additionalPrices,
                'is_instant_booking' => (bool) $request->input('is_instant_booking'),
                'owner_id'           => $user->id
            ];

            $bookingOwner = $repository->storeInstantBook($user->id, $params);
            if (!$bookingOwner['status']) {
                // try to update data data
                $bookingOwner = $repository->updateInstantBook($bookingOwner['data']->id, $params);
                if (!$bookingOwner['status']) throw new CustomErrorException('You are not registered the booking feature');
            }

            $result = $bookingOwner['data'];
            $result['rent_counts'] = (!empty($result->rent_counts)) ? unserialize($result->rent_counts) : null;
            $result['additional_prices'] = (!empty($result->additional_prices)) ? unserialize($result->additional_prices) : null;

            //ADD USER PROPERTIES FOR MoEngage Tracking
            try {
                SendMoEngageUserPropertiesQueue::dispatch(
                    (int) $user->id,
                    ['is_instant_booking' => (bool) $result->is_instant_booking]
                )->delay(now()->addMinutes(15));
            } catch (Exception $e) {
                Bugsnag::notifyException($e);
            }

            $response = [
                'status' => true,
                'data' => $result
            ];

        } catch (CustomErrorException $e) {
            $response['status'] = false;
            $response['meta']['message'] = $e->getMessage();
        }  catch(\Throwable $e) {
            $exception = new RuntimeException("Failed to create instant booking - ".$e->getMessage());
            Bugsnag::notifyException($exception);
            $response['meta']['message'] = 'Failed to create instant booking';
        }

        return Api::responseData($response);
    }
}
