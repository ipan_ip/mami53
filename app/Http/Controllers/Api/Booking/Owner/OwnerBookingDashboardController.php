<?php

namespace App\Http\Controllers\Api\Booking\Owner;

use App\Entities\Booking\BookingDesigner;
use App\Entities\Booking\BookingUser;
use App\Entities\Dbet\DbetLink;
use App\Entities\Room\BookingOwnerRequest;
use App\Exceptions\CustomErrorException;
use App\Http\Controllers\Api\BaseController;
use App\Http\Helpers\ApiResponse as Api;
use App\Repositories\Booking\BookingUserRepository;
use App\Repositories\Contract\ContractRepository;
use App\Repositories\Dbet\DbetLinkRegisteredRepository;
use App\Repositories\RoomRepository;
use App\User;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Illuminate\Http\JsonResponse;
use Exception;
use Illuminate\Http\Request;

/**
 * Class OwnerBookingDashboardController
 * @package App\Http\Controllers\Api\Booking\Owner
 */
class OwnerBookingDashboardController extends BaseController
{
    const BOOKING_REQUEST_NOTIF_TYPE = 'booking_request';

    protected $repository;

    /**
     * OwnerTenantController constructor.
     */
    public function __construct(
        BookingUserRepository $bookingUserRepository
    ) {
        $this->repository = $bookingUserRepository;
        parent::__construct();
    }

    public function index(Request $request): JsonResponse
    {
        $response = $this->makeDefaultResponse();
        try {
            $user = $this->user();
            if (!$user)
                throw new CustomErrorException('User tidak ditemukan');

            // repository
            $contractRepo   = app(ContractRepository::class);
            $dbetRepo       = app(DbetLinkRegisteredRepository::class);

            // default data
            $totalOffBookingPending = 0;
            $totalContractSubmission = 0;

            // get room ids
            $roomIds = $this->getRoomIds($user);
            if (count($roomIds) !== 0) {
                $bookingDesignerIds = BookingDesigner::whereIn('designer_id', $roomIds)->pluck('id')->toArray();
                if (count($bookingDesignerIds) !== 0) {
                    $totalOffBookingPending = $this->repository->getTotalOwnerBookingWithStatus(
                        BookingUser::BOOKING_STATUS_BOOKED,
                        $bookingDesignerIds
                    );
                }

                // get total submission
                $totalContractSubmission = $dbetRepo
                    ->whereIn('designer_id', $roomIds)
                    ->where('status', DbetLink::PENDING)
                    ->count();
            }

            // get total unpaid invoice
            $totalOffUnpaidInvoices = $contractRepo->countOwnerCurrentMonthUnpaidInvoices($user->id);

            // response data
            $response = [
                'status' => true,
                'data' => [
                    'unread_booking_pending_badge'  => ($totalOffBookingPending === 0 ? false : true),
                    'unread_unpaid_invoice_badge'   => ($totalOffUnpaidInvoices === 0 ? false : true),
                    'unread_contract_submission_badge' => ($totalContractSubmission === 0 ? false : true),
                    'total_off_booking_pending'     => $totalOffBookingPending,
                    'total_off_unpaid_invoice'      => $totalOffUnpaidInvoices,
                    'total_off_contract_submission' => $totalContractSubmission
                ]
            ];

        } catch (CustomErrorException $e) {
            $response['meta']['message'] = $e->getMessage();
        } catch (Exception $e) {
            $response['meta']['message'] = $e->getMessage();
            Bugsnag::notifyException($e);
        }
        return Api::responseData($response);
    }

    private function getRoomIds(User $user): array
    {
        // repository
        $roomRepo       = app(RoomRepository::class);
        // get room bbk
        $rooms = $roomRepo
            ->select('id')
            ->whereNull('apartment_project_id')
            ->whereHas('owners', function($ownerQuery) use($user){
                $ownerQuery
                    ->where('user_id', $user->id)
                    ->whereIn('status', ['add', 'verified', 'draft2']);
            })
            ->where('is_booking',1)
            ->whereHas('booking_owner_requests',function($q){
                $q->where('status', BookingOwnerRequest::BOOKING_APPROVE);
            })
            ->get();

        // default data
        $roomIds                        = [];
        if ($rooms) {
            $roomIds = $rooms->pluck('id')->toArray();
        }

        return $roomIds;
    }

    /**
     * Function to return default response
     * @return array
     */
    private function makeDefaultResponse(): array
    {
        return [
            'status' => false,
            'meta' => ['message' => 'General Error']
        ];
    }
}