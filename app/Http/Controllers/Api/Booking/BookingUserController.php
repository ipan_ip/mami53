<?php

namespace App\Http\Controllers\Api\Booking;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseController;
use App\Http\Helpers\ApiResponse as Api;
use Validator;
use App\Repositories\Booking\BookingUserRepository;
use App\Presenters\BookingUserPresenter;
use App\Presenters\BookingUserPriceComponentPresenter;
use Carbon\Carbon;

use App\Entities\Booking\BookingUser;
use App\Entities\Booking\BookingDesigner;
use App\Entities\Room\Room;
use Auth;
use App\Entities\Notif\SendNotif;

class BookingUserController extends BaseController
{
    protected $repository;

    protected $validationMessages = [
        'type.required'=>'Tipe booking harus diisi',
        'checkin.required'=>'Tanggal Checkin harus diisi',
        'checkin.date_format'=>'Format Tanggal Checkin salah',
        'checkin.after'=>'Tanggal Checkin harus lebih dari tanggal kemarin',
        'checkout.required'=>'Tanggal Checkout harus diisi',
        'checkout.required_if'=>'Tanggal Checkout harus diisi',
        'checkout.date_format'=>'Format Tanggal Checkout salah',
        'checkout.after'=>'Tanggal Checkout harus lebih dari tanggal Checkin',
        'duration.required'=>'Lama tinggal harus diisi',
        'duration.required_if'=>'Lama tinggal harus diisi',
        'duration.numeric'=>'Lama tinggal harus berupa angka',
        'room_total.required'=>'Jumlah kamar harus diisi',
        'room_total.numeric'=>'Jumlah kamar harus berupa angka',
        'guest_total.required'=>'Jumlah orang harus diisi',
        'guest_total.numeric'=>'Jumlah orang harus berupa angka',
        'contact_name.required'=>'Nama kontak harus diisi',
        'contact_name.max'=>'Nama kontak maksimal :max karakter',
        'contact_phone.required'=>'Nomor telepon kontak harus diisi',
        'contact_phone.max'=>'Nomor telepon kontak maksimal :max karakter',
        'contact_email.required'=>'Email kontak harus diisi',
        'contact_email.email'=>'Format Email kontak salah',
        'cancel_reason.required'=>'Alasan Batal harus diisi',
        'cancel_reason.min'=>'Alasan Batal minimal :min karakter',
        'cancel_reason.max'=>'Alasan Batal maksimal :max karakter',
        'guest.*.name.required'=>'Nama tamu harus diisi',
        'guest.*.name.min'=>'Nama tamu minimal :min karakter',
        'guest.*.name.max'=>'Nama tamu maksimal :max karakter',
        'guest.*.email.required'=>'Email tamu harus diisi',
        'guest.*.email.email'=>'Format email tamu salah',
        'guest.*.phone_number.required'=>'Nomor Telepon tamu harus diisi',
        'guest.*.phone_number.min'=>'Nomor Telepon tamu minimal :min karakter',
        'guest.*.phone_number.max'=>'Nomor Telepon tamu maksimal :max karakter',
        'guest.birthday.required'=>'Tanggal lahir tamu harus diisi',
        'guest.birthday.date_format'=>'Format tanggal lahir tamu salah'
    ];


    public function __construct(BookingUserRepository $repository)
    {
        $this->repository = $repository;

        parent::__construct();
    }

    /**
     * Calculate Total Price before submitting booking
     *
     * @param Request       Laravel request
     */
    public function calculate(Request $request, $roomId, $roomTypeId)
    {
        // check if room exist
        $room = Room::where('song_id', $roomId)->first();

        if(!$room) {
            return Api::responseData([
                    'status'=>false,
                    'messages'=>['Kamar tidak ditemukan']
                ]);
        }

        // check if room type exist and active
        $bookingDesigner = BookingDesigner::where('designer_id', $room->id)
                                        ->where('id', $roomTypeId)
                                        ->active()
                                        ->first();

        if(!$bookingDesigner) {
            return Api::responseData([
                    'status'=>false,
                    'messages'=>['Tipe Kamar tidak ditemukan']
                ]);
        }

        // Validate request
        $validator = Validator::make($request->all(), 
            [
                'type'=>'required',
                'checkin'=>'required|date_format:Y-m-d|after:yesterday',
                'checkout'=>'required_if:type,' . BookingDesigner::BOOKING_TYPE_DAILY . '|date_format:Y-m-d|after:checkin',
                'duration'=>'required_if:type,' . BookingDesigner::BOOKING_TYPE_MONTHLY . '|numeric',
                'room_total'=>'required|numeric',
                'guest_total'=> 'required|numeric',
            ], $this->validationMessages);

        
        $validator->after(function($validator) use ($request, $bookingDesigner) {
            // recheck booking type
            if($request->get('type') != $bookingDesigner->type) {
                $validator->errors()->add('type', 'Tipe Kamar tidak sesuai dengan tipe booking');
            }
        });

        if($validator->fails()) {
            return Api::responseData([
                    'status'=>false,
                    'messages'=>$validator->errors()->all()
                ]);
        }

         $duration = $request->type == BookingDesigner::BOOKING_TYPE_MONTHLY ? 
                            $request->duration : 
                            Carbon::parse($request->checkin)->diffInDays(Carbon::parse($request->checkout));

        $params = [
            'guest_total' => $request->get('guest_total'),
            'stay_duration' => $duration,
            'room_total' => $request->get('room_total')
        ];

        $calculatedPriceComponents = $this->repository->calculatePriceComponents($bookingDesigner, $params);

        $priceComponents = [];
        foreach($calculatedPriceComponents as $key => $price) {
            if(!is_null($price)) {
                $priceComponents[$key] = (new BookingUserPriceComponentPresenter('list'))->present($price)['data'];
            }
        }

        return Api::responseData([
            'data'=>$priceComponents
        ]);
    }

    /**
     * Book a room
     *
     * @param Request       request
     * @param int           room id
     * @param int           room type id
     *
     * @return Array        response
     */
    public function book(Request $request, $roomId, $roomTypeId)
    {
        $user = $this->user();

        // check booking hour (06.00 - 22.00)
        $startBookingTime = Carbon::now()->setTime(6, 0, 0);
        $endBookingTime = Carbon::now()->setTime(22, 0, 0);
        if(!Carbon::now()->between($startBookingTime, $endBookingTime)) {
            return Api::responseData([
                'status'=>false,
                'messages'=>['Untuk sementara booking hanya bisa dilakukan antara jam 6 pagi sampai jam 10 malam']
            ]);
        }

        // check if room exist
        $room = Room::where('song_id', $roomId)->first();

        if(!$room) {
            return Api::responseData([
                    'status'=>false,
                    'messages'=>['Kamar tidak ditemukan']
                ]);
        }

        if($room->is_booking != 1) {
            return Api::responseData([
                'status'=>false,
                'messages'=>['Fitur booking tidak aktif untuk kost ini']
            ]);
        }

        // check if room type exist and active
        $bookingDesigner = BookingDesigner::where('designer_id', $room->id)
                                        ->where('id', $roomTypeId)
                                        ->active()
                                        ->first();

        if(!$bookingDesigner) {
            return Api::responseData([
                    'status'=>false,
                    'messages'=>['Tipe Kamar tidak ditemukan']
                ]);
        }

        // Validate request
        $validator = Validator::make($request->all(), 
            [
                'type'=>'required',
                'checkin'=>'required|date_format:Y-m-d|after:yesterday',
                'checkout'=>'required_if:type,' . BookingDesigner::BOOKING_TYPE_DAILY . '|date_format:Y-m-d|after:checkin',
                'duration'=>'required_if:type,' . BookingDesigner::BOOKING_TYPE_MONTHLY . '|numeric',
                'room_total'=>'required|numeric',
                'guest_total'=> 'required|numeric',
                'contact_name'=>'required|max:190',
                'contact_phone'=>'required|max:100',
                'contact_email'=>'required|email',
                // 'special_request'=>'nullable',
                'guest.*.name'=>'required|min:4|max:50',
                'guest.*.email'=>'required|email',
                'guest.*.birthday'=>'required|date_format:Y-m-d',
                'guest.*.phone_number'=>'required|min:8|max:15'
            ], $this->validationMessages);

        
        $validator->after(function($validator) use ($request, $bookingDesigner) {
            // recheck booking type
            if($request->get('type') != $bookingDesigner->type) {
                $validator->errors()->add('type', 'Tipe Kamar tidak sesuai dengan tipe booking');
            }

            // recheck checkin date
            $nowDate = Carbon::now()->setTime(0, 0, 0);
            if($nowDate->diffInDays(Carbon::parse($request->get('checkin'))->setTime(0, 0, 0), false) < 2) {
                $validator->errors()->add('checkin', 'Tanggal Chekin minimal 2 hari dari tanggal hari ini.');
            }

            // recheck guest total
            if(ceil($request->get('guest_total') / $request->get('room_total')) > $bookingDesigner->max_guest) {
                $validator->errors()->add('guest_total', '1 kamar hanya boleh ditempati ' . $bookingDesigner->max_guest . ' orang');
            }

            if($request->get('type') == BookingDesigner::BOOKING_TYPE_DAILY && count($request->get('guest')) < 1) {
                $validator->errors()->add('guest_total', 'Data tamu harus diisi');
            } elseif($request->get('type') == BookingDesigner::BOOKING_TYPE_MONTHLY && count($request->get('guest')) != $request->get('guest_total')) {
                $validator->errors()->add('guest_total', 'Jumlah data tamu harus sesuai dengan jumlah tamu');
            }
        });

        if($validator->fails()) {
            return Api::responseData([
                    'status'=>false,
                    'messages'=>$validator->errors()->all()
                ]);
        }

        $duration = $request->type == BookingDesigner::BOOKING_TYPE_MONTHLY ? 
                            $request->duration : 
                            Carbon::parse($request->checkin)->diffInDays(Carbon::parse($request->checkout));

        if($duration < $bookingDesigner->minimum_stay) {
            return Api::responseData([
                    'status'=>false,
                    'messages'=>['Lama sewa minimum ' . $bookingDesigner->minimum_stay . ' ' . BookingDesigner::BOOKING_UNIT[$bookingDesigner->type]]
                ]);
        }

        $params = [
            'type'=>$request->type,
            'checkin'=>$request->checkin,
            'checkout'=>$request->type == BookingDesigner::BOOKING_TYPE_DAILY ? 
                            $request->checkout : 
                            Carbon::parse($request->checkin)->addMonths($request->duration)->format('Y-m-d'),
            'duration'=>$duration,
            'room_total'=>$request->room_total,
            'guest_total'=>$request->guest_total,
            'contact_name'=>$request->contact_name,
            'contact_phone'=>$request->contact_phone,
            'contact_email'=>$request->contact_email,
            'special_request'=>$request->special_request,
            'guest'=>$request->guest
        ];

        // $this->repository->setPresenter(new BookingUserPresenter('detail'));

        // main booking process
        $bookingUser = $this->repository->bookRoom($bookingDesigner, $user, $params);

        if($bookingUser === false) {
            return Api::responseData([
                    'status'=>false,
                    'messages'=>['Booking Gagal, silakan coba lagi']
                ]);
        }

        $bookingUser = (new BookingUserPresenter('detail'))->present($bookingUser);

        return Api::responseData([
            'data'=>$bookingUser['data']
        ]);
    }

    /**
     * Get booking detail by booking code
     * 
     * @param Request           request
     * @param string            Booking Code
     *
     * @return Array            Formatted booking detail
     */
    public function detail(Request $request, $bookingCode)
    {
        $user = $this->user();

        // get booking data by booking code and user
        $bookingUser = $this->repository->with(['booking_designer', 'booking_designer.room', 
                                            'booking_designer.room.owners', 
                                            'booking_designer.room.owners.user', 'payments'])
                                        ->findWhere([
                                            ['booking_code', '=', $bookingCode],
                                            ['user_id', '=', $user->id]
                                        ]);

        if(count($bookingUser) <= 0) {
            return Api::responseData([
                'status'=>false,
                'messages'=>['Booking tidak ditemukan']
            ]);
        }

        // set presenter and parse result to presenter
        $this->repository->setPresenter(new BookingUserPresenter('detail'));
        $bookingUser = $this->repository->parserResult(collect($bookingUser)->first());

        return Api::responseData([
            'data'=>$bookingUser['data']
        ]);
    }

    /**
     * Cancel Booking (Only for bulanan type)
     *
     * @param Request       $request
     * @param string        Booking Code
     *
     * @return boolean      Cancel Status
     */
    public function cancel(Request $request, $bookingCode)
    {
        $user = $this->user();

        // only allow cancel for own booking
        $bookingUser = BookingUser::where('booking_code', $bookingCode)
                                ->where('user_id', $user->id)
                                ->first();

        if(!$bookingUser) {
            return Api::responseData([
                'status'=>false,
                'messages'=>['Booking tidak ditemukan']
            ]);
        }

        if($bookingUser->status != BookingUser::BOOKING_STATUS_VERIFIED) {
            return Api::responseData([
                'status'=>false,
                'messages'=>['Booking tidak dapat dibatalkan']
            ]);
        }

        $validator = Validator::make($request->all(), [
            'cancel_reason'=>'required|min:20|max:500'
        ], $this->validationMessages);

        if($validator->fails()) {
            return Api::responseData([
                    'status'=>false,
                    'messages'=>$validator->errors()->all()
                ]);
        }

        $cancelReason = $request->get('cancel_reason');
        $cancelStatus = $this->repository->cancelBooking($bookingUser, $cancelReason);

        if($cancelStatus) {
            return Api::responseData([
                'data'=>[
                    'cancel_status'=>$cancelStatus
                ]
            ]);    
        } else {
            return Api::responseData([
                'status'=>false,
                'messages'=>['Pembatalan Booking tidak bisa dilakukan']
            ]);
        }

    }


    /**
     * Get user's booking history
     */
    public function history(Request $request)
    {
        $user = $this->user();

        $params = [];
        if($request->filled('limit')) {
            $params['limit'] = $request->get('limit');
        }

        if($request->filled('offset')) {
            $params['offset'] = $request->get('offset');
        }

        $history = $this->repository->getHistory($user, $params);

        $bookingUsers = (new BookingUserPresenter('list'))->present($history['bookingUsers']);

        return Api::responseData([
            'data'=>$bookingUsers['data'],
            'count'=>$history['bookingUsersTotal']
        ]);
    }

    /**
     * Get booking voucher
     *
     * @param Request       Laravel request
     * @param string        Booking Code
     *
     * @return Object       Api response
     */
    public function getVoucher(Request $request, $bookingCode)
    {
        $user = $this->user();

        // only allow cancel for own booking
        $bookingUser = BookingUser::where('booking_code', $bookingCode)
                                ->where('user_id', $user->id)
                                ->where('status', BookingUser::BOOKING_STATUS_VERIFIED)
                                ->first();

        if(!$bookingUser) {
            return Api::responseData([
                'status'=>false,
                'messages'=>['Booking tidak ditemukan']
            ]);
        }

        $voucherPath = $this->repository->getVoucher($bookingUser);

        return Api::responseData([
            'data'=>[
                'voucher'=>$voucherPath
            ]
        ]);
    }


    /**
     * Get booking receipt
     *
     * @param Request           Laravel request
     * @param string            Booking Code
     *
     * @return Object           Api response
     */
    public function getReceipt(Request $request, $bookingCode)
    {
        $user = $this->user();

        // only allow cancel for own booking
        $bookingUser = BookingUser::where('booking_code', $bookingCode)
                                ->where('user_id', $user->id)
                                ->where('status', BookingUser::BOOKING_STATUS_VERIFIED)
                                ->first();

        if(!$bookingUser) {
            return Api::responseData([
                'status'=>false,
                'messages'=>['Booking tidak ditemukan']
            ]);
        }

        $receiptPath = $this->repository->getReceipt($bookingUser);

        return Api::responseData([
            'data'=>[
                'voucher'=>$receiptPath
            ]
        ]);
    }

    /**
     * Get number of active booking
     */
    public function counter(Request $request)
    {
        $user = $this->user();

        if(is_null($user)) {
            return Api::responseData([
                'status'=>false,
                'meta'=> [
                    'message'=>'User tidak ditemukan'
                ]
            ]);
        }

        $counter = $this->repository->counter($user);

        return Api::responseData([
            'data'=>['counter'=>$counter]
        ]);
    }
}
