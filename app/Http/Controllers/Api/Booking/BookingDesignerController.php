<?php

namespace App\Http\Controllers\Api\Booking;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseController;
use App\Http\Helpers\ApiResponse as Api;
use App\Repositories\Booking\BookingDesignerRepository;
use App\Presenters\BookingDesignerPresenter;
use App\Transformers\Booking\BookingDesignerDetailTransformer;
use Validator;
use Carbon\Carbon;

use App\Entities\Room\Room;
use App\Entities\Booking\BookingDesigner;

class BookingDesignerController extends BaseController
{

	protected $repository;

    // Validation Message
    protected $validationMessages = [
        'name.required'=>'Nama kamar harus diisi',
        'name.max'=>'Nama kamar maksimal :max karakter',
        'room_id.required'=>'Room ID harus diisi',
        'type.required'=>'Tipe kamar harus diisi',
        'type.in'=>'Tipe kamar tidak dikenali',
        'max_guest.required'=>'Jumlah tamu harus diisi',
        'max_guest.numeric'=>'Jumlah tamu harus berupa angka',
        'available_room.required'=>'Jumlah kamar harus diisi',
        'available_room.numeric'=>'Jumlah kamar harus berupa angka',
        'minimum_stay.required'=>'Lama sewa minimal harus diisi',
        'minimum_stay.numeric'=>'Lama sewa minimal harus berupa angka',
        'price.required'=>'Harga harus diisi',
        'price.numeric'=>'Harga harus berupa angka',
        'sale_price.numeric'=>'Harga diskon harus berupa angka',
        'is_active.boolean'=>'Is Active hanya bisa benar atau salah',
        'start_date.required'=>'Tanggal Mulai harus diisi',
        'start_date.date_format'=>'Format Tanggal Mulai salah',
        'end_date.required'=>'Tanggal Selesai harus diisi',
        'end_date.required_if'=>'Tanggal Selesai harus diisi',
        'end_date.date_format'=>'Format Tanggal Selesai salah',
        'total_room.required'=>'Jumlah Kamar harus diisi',
        'total_room.numeric'=>'Jumlah Kamar harus berupa angka',
        'duration.required_if'=>'Durasi harus diisi'
    ];

    public function __construct(BookingDesignerRepository $repository)
    {
        $this->repository = $repository;

        parent::__construct();
    }

    /**
     * Get booking type
     * 
     * @param Request           Request
     * @param int|string        if room id is provided, then it will return types for specific rooms
     */
    public function getType(Request $request, $roomId = '')
    {
        $room = null;

        if($roomId != '') {
            $room = Room::active()->where('song_id', $roomId)->first();

            if(!$room) {
                return Api::responseData([
                    'status'=>false,
                    'messages'=>['Kamar tidak ditemukan']
                ]);
            }
        }

        $availableTypes = $this->repository->getAvailableBookingType($room);

        return Api::responseData([
            'data'=>$availableTypes
        ]);
        
    }


    /**
     * Save new booking room
     */
    public function store(Request $request)
    {
        $user = $this->user();

        if($user->is_owner == 'false') {
            return Api::responseData([
                    'status'=>false,
                    'messages'=>['Hanya owner yang bisa menambah kamar untuk booking']
                ]);
        }

        $validator = Validator::make($request->all(), 
            [
                'name'=>'required|max:190',
                'room_id'=>'required',
                'type'=>'required|in:' . BookingDesigner::BOOKING_TYPE_DAILY . ',' . BookingDesigner::BOOKING_TYPE_MONTHLY ,
                'description'=>'nullable',
                'max_guest'=>'required|numeric',
                'available_room'=>'required|numeric',
                'minimum_stay'=>'required|numeric',
                'price'=>'required|numeric',
                'sale_price'=>'numeric',
            ], $this->validationMessages);

        if($validator->fails()) {
            return Api::responseData([
                    'status'=>false,
                    'messages'=>$validator->errors()->all()
                ]);
        }

        $room = Room::with('owners')->where('song_id', $request->get('room_id'))->first();

        if(!$room) {
            return Api::responseData([
                    'status'=>false,
                    'messages'=>['Kost tidak ditemukan']
                ]);
        }

        if($room->is_booking != 1) {
            return Api::responseData([
                    'status'=>false,
                    'messages'=>['Anda belum mengaktifkan fitur booking']
                ]);
        }

        $owner = $room->owners->first();

        if($owner->user_id != $user->id) {
            return Api::responseData([
                    'status'=>false,
                    'messages'=>['Anda bukan pemilik kost ini.']
                ]);
        }

        $params = [
            'name'=>$request->name,
            'type'=>$request->type,
            'description'=>$request->description,
            'max_guest'=>$request->max_guest,
            'available_room'=>$request->available_room,
            'minimum_stay'=>$request->minimum_stay,
            'price'=>$request->price,
            'sale_price'=>$request->sale_price
        ];

        $this->repository->setPresenter(new BookingDesignerPresenter('detail'));

        $bookingDesigner = $this->repository->addBookingDesigner($room, $params);
        
        return Api::responseData([
            'data'=>$bookingDesigner['data']
        ]);
    }

    /**
     * Update booking room
     */
    public function update(Request $request, $bookingDesignerId)
    {
        $user = $this->user();

        if($user->is_owner == 'false') {
            return Api::responseData([
                    'status'=>false,
                    'messages'=>['Hanya owner yang bisa menambah kamar untuk booking']
                ]);
        }

        $validator = Validator::make($request->all(), 
            [
                'name'=>'required|max:190',
                'description'=>'nullable',
                'max_guest'=>'required|numeric',
                'available_room'=>'required|numeric',
                'minimum_stay'=>'required|numeric',
                'price'=>'required|numeric',
                'sale_price'=>'numeric',
                'is_active'=>'boolean'
            ], $this->validationMessages);

        if($validator->fails()) {
            return Api::responseData([
                    'status'=>false,
                    'messages'=>$validator->errors()->all()
                ]);
        }

        $bookingDesigner = $this->repository->find($bookingDesignerId);

        if(!$bookingDesigner) {
            return Api::responseData([
                    'status'=>false,
                    'messages'=>['Kamar tidak ditemukan']
                ]);
        }

        if($bookingDesigner->room->is_booking != 1) {
            return Api::responseData([
                    'status'=>false,
                    'messages'=>['Anda belum mengaktifkan fitur booking']
                ]);
        }

        $this->repository->setPresenter(new BookingDesignerPresenter('detail'));

        $params = [
            'name'=>$request->name,
            'description'=>$request->description,
            'max_guest'=>$request->max_guest,
            'available_room'=>$request->available_room,
            'minimum_stay'=>$request->minimum_stay,
            'price'=>$request->price,
            'sale_price'=>$request->sale_price
        ];

        if($request->filled('is_active')) {
            $paras['is_active'] = $request->is_active;
        }

        $bookingDesigner = $this->repository->updateBookingDesigner($bookingDesigner, $params);

        return Api::responseData([
            'data'=>$bookingDesigner['data']
        ]);
    }


    /**
     * Get schedule based on start date and end date
     *
     * @param Request           request
     * @param int               room id
     * @param date              Start Date (Y-m-d)
     * @param date              End Date (Y-m-d)
     */
    public function getSchedule(Request $request)
    {
        $validator = Validator::make($request->all(), 
            [
                'room_id'=>'required',
                'start_date'=>'required|date_format:Y-m-d',
                'end_date'=>'required|date_format:Y-m-d',
                'type'=>'nullable'
            ], $this->validationMessages);

        if($validator->fails()) {
            return Api::responseData([
                    'status'=>false,
                    'messages'=>$validator->errors()->all()
                ]);
        }

        $roomId = $request->get('room_id');
        $startDate = $request->get('start_date');
        $endDate = $request->get('end_date');
        $type = $request->filled('type') ? $request->get('type') : '';

        if(strtotime($startDate) > strtotime($endDate)) {
            return Api::responseData([
                'status'=>false,
                'messages'=>['Tanggal mulai harus lebih kecil dari tanggal selesai']
            ]);
        }

        $room = Room::active()->where('song_id', $roomId)->first();

        if(!$room) {
            return Api::responseData([
                'status'=>false,
                'messages'=>['Kamar tidak ditemukan']
            ]);
        }

        if($room->is_booking != 1) {
            return Api::responseData([
                'status'=>false,
                'messages'=>['Fitur booking tidak aktif untuk kost ini']
            ]);
        }

        $schedule = $this->repository->getSchedule($room, $startDate, $endDate, $type);

        return Api::responseData([
            'data'=>[
                'schedule'=>$schedule
            ]
        ]);
    }


    /**
     * Search available room on chosen criteria
     *
     * @param Request
     */
    public function searchAvailable(Request $request)
    {
        // Validate input
        $validator = Validator::make($request->all(), 
            [
                'room_id'=>'required',
                'start_date'=>'required|date_format:Y-m-d',
                'end_date'=>'required_if:type,' . BookingDesigner::BOOKING_TYPE_DAILY . '|date_format:Y-m-d',
                'type'=>'nullable',
                'duration'=>'required_if:type,' . BookingDesigner::BOOKING_TYPE_MONTHLY,
                'total_room'=>'required|numeric'
            ], $this->validationMessages);

        if($validator->fails()) {
            return Api::responseData([
                    'status'=>false,
                    'messages'=>$validator->errors()->all()
                ]);
        }

        $roomId = $request->get('room_id');
        $type = $request->filled('type') ? $request->get('type') : '';
        $startDate = $request->get('start_date');
        if($type == BookingDesigner::BOOKING_TYPE_MONTHLY) {
            $endDate = Carbon::parse($startDate)->addMonths($request->get('duration'))->format('Y-m-d');
        } else {
            $endDate = $request->get('end_date');
        }
        $totalRoom = $request->get('total_room');
        

        if(strtotime($startDate) > strtotime($endDate)) {
            return Api::responseData([
                'status'=>false,
                'messages'=>['Tanggal mulai harus lebih kecil dari tanggal selesai']
            ]);
        }

        // Find designer room
        $room = Room::active()->where('song_id', $roomId)->first();

        if(!$room) {
            return Api::responseData([
                'status'=>false,
                'messages'=>['Kamar tidak ditemukan']
            ]);
        }

        if($room->is_booking != 1) {
            return Api::responseData([
                'status'=>false,
                'messages'=>['Fitur booking tidak aktif untuk kost ini']
            ]);
        }

        // get available room
        $availableRooms = $this->repository->searchAvailableRooms($room, $startDate, $endDate, $type, $totalRoom);

        return Api::responseData([
            'data'=>$availableRooms
        ]);
    }

}
