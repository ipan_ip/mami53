<?php

namespace App\Http\Controllers\Api\Booking;

use App\Entities\Booking\StatusChangedBy;
use App\Exceptions\CustomErrorException;
use App\Http\Helpers\RentCountHelper;
use App\Http\Requests\Booking\BookingUserCancelRequest;
use App\Http\Requests\Booking\BookingUserRequest;
use App\Jobs\MoEngage\SendMoEngageReportUserBookingSubmittedQueue;
use App\Jobs\ProcessNotificationDispatch;
use App\Presenters\Booking\BookingUserDraftPresenter;
use App\Repositories\Booking\BookingUserDraftRepository;
use App\Repositories\Booking\BookingUserDraftRepositoryEloquent;
use App\Services\Booking\BookingService;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseController;
use App\Http\Helpers\ApiResponse as Api;
use App\Http\Helpers\GenderHelper;
use App\Http\Helpers\BookingUserHelper;

use Auth;
use Notification;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use RuntimeException;
use Exception;

use App\Repositories\Booking\BookingUserRepository;
use App\Repositories\UserDataRepository;
use App\Entities\Mamipay\MamipayTenant;
use App\Entities\Booking\BookingUser;
use App\Entities\Booking\BookingDesigner;
use App\Entities\Booking\BookingUserTracker;
use App\Entities\User\UserVerificationAccount;
use App\Presenters\BookingRoomPresenter;
use App\Presenters\BookingUserPresenter;
use App\Entities\Room\Room;
use App\Libraries\SMSLibrary;
use App\Http\Helpers\BookingNotificationHelper;
use App\Entities\User\Notification as UserNotif;
use App\Entities\Notif\Category as NotifCategory;
use App\Entities\Notif\NotificationWhatsappTemplate;


class NewBookingUserController extends BaseController
{
    protected $repository;

    const DEFAULT_GENDER_ANDROID_MIN_VERSION = '1163110035';

    const ERROR_SYSTEM_MESSAGE = 'Ups, Ada Kendala di Sistem Kami';

    public function __construct(
        BookingUserRepository $repository,
        UserDataRepository $userRepository,
        BookingService $bookingService
    ) {
        parent::__construct();

        $this->repository = $repository;
        $this->userRepository = $userRepository;
        $this->bookingService = $bookingService;
    }

    /**
     * Display the specified room.
     * @param int $roomId
     * @param BookingUserDraftRepository $bookingUserDraftRepository
     * @return JsonResponse
     */
    public function detailRoom(int $roomId, BookingUserDraftRepository $bookingUserDraftRepository): JsonResponse
    {
        // force default response to false
        $response = $this->makeDefaultResponse();
        try {
            $user = $this->user();
            if (is_null($user)) throw new CustomErrorException('Data user tidak ditemukan');

            $room = Room::with(
                [
                    'photo',
                    'booking_designers',
                    'tags',
                    'level'
                ])
                ->where('id', $roomId)
                ->orWhere('song_id', $roomId)
                ->first();

            if (!$room) throw new CustomErrorException('Data Kos tidak ditemukan');

            $roomDetail = (new BookingRoomPresenter('detail'))->present($room);
            if (is_null($roomDetail['data']['booking_designer']))
                throw new CustomErrorException('Maaf ada kendala saat melakukan booking, silahkan kontak CS Kami');

            // When kost only have rent count "daily"
            if (is_null($roomDetail['data']['rent_count_type']))
                throw new CustomErrorException('Mohon maaf, booking belum tersedia untuk harian. Kembali lain waktu atau cari kos lain');

            $job = $user->job;
            if ($job == BookingUser::CONTACT_JOB_KULIAH) {
                $job = BookingUser::CONTACT_JOB_MAHASISWA;
            }

            // added draft booking
            $draftBooking = $bookingUserDraftRepository->getDraftByUserAndRoom($user, $room);
            $draftBookingTranform = null;
            if ($draftBooking !== null)
                $draftBookingTranform = (new BookingUserDraftPresenter('detail'))->present($draftBooking);

            /*
             * Jira card: https://mamikos.atlassian.net/browse/MB-1252
             *
             * this is only a temporary handler,
             * if the user version under 3.11.0 has been reduced or does not exist at all we can delete it
             */
            $androidVersion = 0;
            if (!is_null(app()->device)) {
                $androidVersion = isset(app()->device->app_version_code) ? app()->device->app_version_code : 0;
            }
            $androidMinVersion = self::DEFAULT_GENDER_ANDROID_MIN_VERSION;
            $defaultGender = $androidVersion < $androidMinVersion ? User::GENDER_MALE : null;

            $result = [
                'room' => $roomDetail['data'],
                'user' => [
                    'name'                  => $user->name,
                    'phone_number'          => !is_null($user->phone_number) ? $user->phone_number : "",
                    'email'                 => $user->email,
                    'address'               => $user->address,
                    'job'                   => $job,
                    'work'                  => $user->position,
                    'work_place'            => $user->work_place,
                    'description'           => $user->description,
                    'introduction'          => $user->introduction,
                    'last_education'        => $user->education,
                    'semester'              => $user->semester,
                    'gender'                => !empty($user->gender) ? GenderHelper::getUserGenderReplace($user->gender) : $defaultGender,
                    'identity_card_verify'  => isset($user->user_verification_account) ? $user->user_verification_account->identity_card : null,
                    'phone_number_verify'   => isset($user->user_verification_account) ? $user->user_verification_account->is_verify_phone_number : null,
                    'email_verify'          => isset($user->user_verification_account) ? $user->user_verification_account->is_verify_email : null,
                    'complete_profile'      => $user->checkCompleteProfile(),
                ],
                'draft' => $draftBookingTranform !== null ? $draftBookingTranform['data'] : null
            ];

            $response = [
                'data' => $result
            ];
        } catch (CustomErrorException $e) {
            $response['meta']['message'] = $e->getMessage();
        } catch (Exception $e) {
            $exception = new RuntimeException('failed to load detail room - ' . $e->getMessage());
            Bugsnag::notifyException($exception);
            $response['meta']['message'] = self::ERROR_SYSTEM_MESSAGE;
        }
        return Api::responseData($response);
    }

    /**
     * Store a newly user booking.
     * @param BookingUserRequest $request
     * @param int $roomId
     * @param int $roomTypeId
     * @return JsonResponse
     */
    public function store(BookingUserRequest $request, int $roomId, int $roomTypeId): JsonResponse
    {
        // force default response to false
        $response = $this->makeDefaultResponse();
        try {
            $validator = $request->failedValidator;
            if ($validator && $validator->fails()) throw new CustomErrorException(implode(", ",$validator->errors()->all()));

            $user = $this->user();
            if (is_null($user)) throw new CustomErrorException('Data user tidak ditemukan');

            // check if room exist
            $room = Room::with(
                [
                    'owners',
                    'owners.user',
                    'tags'
                ])
                ->where('song_id', $roomId)
                ->first();

            if (!$room) throw new CustomErrorException('Kamar tidak ditemukan');

            if ($room->is_booking != 1) throw new CustomErrorException('Fitur booking tidak aktif untuk kost ini');

            // check room not have owner
            if (!isset($room->owners->first()->user)) {
                throw new CustomErrorException('Silahkan hubungi CS untuk melanjutkan proses booking');
            }

            // check gender kost
            $gender = $room->gender == 1 ? 'male' : ($room->gender == 2 ? 'female' : 'all');
            if ($gender != $request->input('contact_gender') && $gender != 'all') throw new CustomErrorException('Gagal, periksa kembali tipe kost Anda.');

            // check if room type exist and active
            $bookingDesigner = BookingDesigner::bookingRoomActive($room->id, $roomTypeId);
            if (!$bookingDesigner) throw new CustomErrorException('Tipe Kamar tidak ditemukan');

            // checking data max renter
            if ( !empty($request->total_renter) && $request->total_renter !== 1 )  {
                // max renter
                $tagIds     = $room->tags->pluck('id')->toArray() ?? [];
                $maxRenter  = BookingUserHelper::getMaxRenterByTagIds($tagIds);
                // check total renter from client > avalaible total renter on the room
                if ( isset($maxRenter['max']) && ($request->total_renter > $maxRenter['max']) ) {
                    throw new CustomErrorException('Mohon maaf, jumlah penyewa melibihi jumlah yang ditetapkan');
                }
            }

            $tenantEmail = !empty($request->contact_email) ? $request->contact_email : !empty($user->email) ? $user->email : null;

            // Check User Booking Status
            $bookingUser = new BookingUser();
            $checkUserBookingActive = $bookingUser->bookingUserActive($user);
            $existingSameRoomActive = $bookingUser->existingRoomBooked($user, $room);

            if ($checkUserBookingActive->count() || $existingSameRoomActive->count())
                throw new CustomErrorException('Mohon maaf, untuk saat ini belum bisa melakukan booking, karena masih terdapat booking yang aktif');

            // Check User Mamipay Contract Status Active
            $tenantContractActive = MamipayTenant::tenantContractActive($user->id);
            if ($tenantContractActive) throw new CustomErrorException('Mohon maaf, Untuk saat ini belum bisa melakukan booking, karena masih terdapat kontrak yang aktif');

            // Check Valid Phone Number
            $isValidPhoneNumber = SMSLibrary::validateDomesticIndonesianMobileNumber($request->contact_phone);
            if (!$isValidPhoneNumber) throw new CustomErrorException('Mohon maaf, No Handphone tidak valid');

            $phoneNumberAvailable = $user->isPhoneNumberAvailable($request->contact_phone);
            if ($phoneNumberAvailable == false) throw new CustomErrorException('Nomor sudah digunakan');

            $isValidParentPhoneNumber = null;
            if ($request->contact_parent_phone) {
                $isValidParentPhoneNumber = SMSLibrary::validateDomesticIndonesianMobileNumber($request->contact_parent_phone);
                if (!$isValidParentPhoneNumber) throw new CustomErrorException('Mohon maaf, No Handphone tidak valid');
            }

            $job = $request->contact_job;
            if ($job == 'mahasiswa') $job = 'kuliah';

            $params = [
                'song_id'               => $room->song_id,
                'checkin'               => $request->checkin,
                'checkout'              => $request->checkout,
                'duration'              => $request->duration,
                'rent_count_type'       => $request->rent_count_type,
                'contact_identity'      => $request->contact_identity,
                'contact_name'          => $request->contact_name,
                'contact_phone'         => $request->contact_phone,
                'contact_job'           => $job,
                'contact_email'         => $tenantEmail,
                'contact_gender'        => $request->contact_gender,
                'contact_parent_name'   => isset($request->contact_parent_name) ? $request->contact_parent_name : null,
                'contact_parent_phone'  => $isValidParentPhoneNumber,
                'contact_introduction'  => $request->contact_introduction,
                'created_by'            => StatusChangedBy::TENANT,
                'is_married'            => $request->is_married,
                'total_renter'          => $request->total_renter,
                'session_id'            => $request->session_id ?? null,
                'is_flash_sale'         => $request->is_flash_sale == true ? true : false,
                'flash_sale'            => $request->getFlashSale()
            ];

            $result = $this->repository->newBookRoom($bookingDesigner, $params, $user, $room);

            // Check Identity Card
            UserVerificationAccount::validateIdentityCardStatus($user);

            BookingUserTracker::report($user->id, $result->id, $roomId);

            if ($result === false) throw new CustomErrorException('Booking Gagal, silakan coba lagi');

            // Update User Profile
            $this->updateUserProfile($request, $user);

            // get owner rooms
            $owner = $room->owners->first();

            // Open Chat Room
            $channelUrl = $this->bookingService->sendChatAfterBookingSuccess($room, $user, $result);
            $result['chat'] = [
                'group_channel_url' => $channelUrl,
                'owner_id'          => isset($owner->user_id) ? $owner->user_id : null,
                'tenant_id'         => $user->id
            ];

            // Send Notification
            $this->sendNotification($room, $user, $result);
            // Send WhatsApp notification to Owner
            $this->sendWhatsApp($room, $request);
            // Update data draft booking
            $bookingUserDraftRepository = new BookingUserDraftRepositoryEloquent(app());
            $bookingUserDraftRepository->updateWhenCreatedBooking($user, $room);

            // send MoEngage tracker
            $interface = BookingUserHelper::getInterfaceForMoEngage();
            SendMoEngageReportUserBookingSubmittedQueue::dispatch($result, $interface);

            // Send an email to the owner when the tenant submit booking
            if (!empty($owner->user)) {
                $this->bookingService->sendEmailWhenSubmitBookingToOwner($owner->user, $result, $room->name);
            }

            $response = [
                'data' => $result
            ];

        } catch (CustomErrorException $e) {
            $response['meta']['message'] = $e->getMessage();
        } catch (Exception $e) {
            $exception = new RuntimeException('failed to create booking - ' . $e->getMessage());
            Bugsnag::notifyException($exception);
            $response['meta']['message'] = self::ERROR_SYSTEM_MESSAGE;
        }
        return Api::responseData($response);
    }

    /**
     * Function to update user profile
     * @param BookingUserRequest $request
     * @param User $user
     * @return array|null
     */
    private function updateUserProfile(BookingUserRequest $request, User $user)
    {
        try {
            $job = $request->contact_job;
            if ($job == 'mahasiswa') $job = 'kuliah';

            $data = [
                'name'          => $request->contact_name,
                'gender'        => $request->contact_gender,
                'birthday'      => !is_null($request->contact_birthday) ? $request->contact_birthday : $user->birthday,
                'jobs'          => $job,
                'work_place'    => !is_null($request->contact_work_place) ? $request->contact_work_place : '',
                'description'   => !is_null($request->contact_work_place) ? $request->contact_work_place : '',
                'introduction'  => $request->contact_introduction,
            ];

            $existingUpdate = $this->userRepository->postUpdate($data, $user);

            return $existingUpdate;
        } catch(Exception $e) {
            $exception = new RuntimeException('failed to update user profile - ' . $e->getMessage());
            Bugsnag::notifyException($exception);
            return null;
        }
    }

    /**
     * Display a listing of the user booking.
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        // force default response to false
        $response = $this->makeDefaultResponse();
        try {
            $user = $this->user();
            if (is_null($user))
                throw new CustomErrorException('Data user tidak ditemukan');

            $query = BookingUser::with([
                'user',
                'booking_user_flash_sale',
                'booking_user_flash_sale.booking_user',
                'booking_user_flash_sale.flash_sale',
                'booking_designer',
                'booking_designer.room' => function($room) {
                    $room->withTrashed();
                },
                'booking_designer.room.tags',
                'booking_designer.room.tags.types',
                'booking_designer.room.tags.photo',
                'booking_designer.room.tags.photoSmall',
                'booking_designer.room.photo',
                'booking_designer.room.level',
                'price_components',
                'booking_user_rooms',
                'booking_designer.chat' => function($chat) use($user) {
                    $chat->where('user_id', $user->id)
                        ->where('chat_group_id', '!=', null)
                        ->orderBy('id', 'ASC');
                },
                'contract',
                'invoices'
            ])->where('user_id', $user->id);

            $query->filterByMultipleStatus($request->status);
            $query->sort($request->sort);

            $bookingUsers   = $query->paginate(5);

            $transformData  = (new BookingUserPresenter('list'))->present($bookingUsers);

            $data = [
                'data'          => $transformData['data'],
                'current_page'  => $bookingUsers->currentPage(),
                'from'          => $bookingUsers->firstItem(),
                'last_page'     => $bookingUsers->lastPage(),
                'next_page_url' => $bookingUsers->nextPageUrl(),
                'per_page'      => $bookingUsers->perPage(),
                'prev_page_url' => $bookingUsers->previousPageUrl(),
                'to'            => $bookingUsers->count(),
                'total'         => $bookingUsers->total(),
                'has-more'      => $bookingUsers->hasMorePages()
            ];

            if (!$transformData)
                throw new CustomErrorException('Data booking tidak ditemukan');

            $response = [
                'data' => $data
            ];

        } catch (CustomErrorException $e) {
            $response['meta']['message'] = $e->getMessage();
        } catch (Exception $e) {
            $exception = new RuntimeException('failed to get booking list - ' . $e->getMessage());
            Bugsnag::notifyException($exception);
            $response['meta']['message'] = self::ERROR_SYSTEM_MESSAGE;
        }
        return Api::responseData($response);
    }

    /**
     * Display the specified user booking.
     * @param int $bookId
     * @return JsonResponse
     */
    public function show(int $bookId): JsonResponse
    {
        // force default response to false
        $response = $this->makeDefaultResponse();
        try {
            $user = $this->user();
            if (is_null($user))
                throw new CustomErrorException('Data user tidak ditemukan');

            $bookingUser = BookingUser::with([
                    'user',
                    'booking_user_flash_sale',
                    'booking_user_flash_sale.booking_user',
                    'booking_user_flash_sale.flash_sale',
                    'booking_designer',
                    'booking_designer.room' => function($room) {
                        $room->withTrashed();
                    },
                    'booking_designer.room.tags',
                    'booking_designer.room.tags.photo',
                    'booking_designer.room.tags.photoSmall',
                    'booking_designer.room.photo',
                    'booking_status_changes'
                ])
                ->where('user_id', $user->id)
                ->find($bookId);

            if (!$bookingUser)
                throw new CustomErrorException('Data booking tidak ditemukan');

            $transformData = (new BookingUserPresenter('detail'))->present($bookingUser);
            if (!$transformData)
                throw new CustomErrorException('Data booking tidak ditemukan');

            $response = [
                'data' => $transformData['data']
            ];

        } catch (CustomErrorException $e) {
            $response['meta']['message'] = $e->getMessage();
        } catch (Exception $e) {
            $exception = new RuntimeException('failed to load detail room - ' . $e->getMessage());
            Bugsnag::notifyException($exception);
            $response['meta']['message'] = self::ERROR_SYSTEM_MESSAGE;
        }
        return Api::responseData($response);
    }

    /**
     * Cancel user booking
     * @param BookingUserCancelRequest $request
     * @param $bookingCode
     * @return JsonResponse
     */
    public function cancelBook(BookingUserCancelRequest $request, $bookingCode): JsonResponse
    {
        // force default response to false
        $response = $this->makeDefaultResponse();
        try {
            $validator = $request->failedValidator;
            if ($validator && $validator->fails()) throw new CustomErrorException(implode(", ",$validator->errors()->all()));

            $user = $this->user();
            if (is_null($user)) throw new CustomErrorException('Data user tidak ditemukan');

            // only allow cancel for own booking
            $newBookingUser = new BookingUser();
            $bookingUser    = $newBookingUser->getByBookingCode($bookingCode, $user->id);

            if (!$bookingUser) throw new CustomErrorException('Booking tidak ditemukan');

            if ($bookingUser->status == BookingUser::BOOKING_STATUS_VERIFIED) throw new CustomErrorException('Booking tidak dapat dibatalkan');

            // check if room exist
            $room = Room::with(['owners', 'owners.user'])->where('song_id', $bookingUser->designer_id)->first();
            if (!$room) throw new CustomErrorException('Kamar tidak ditemukan');

            $cancelReason = $request->get('cancel_reason');
            $params = [
                'reason' => $cancelReason,
                'reason_id' => $request->reason_id ?? null
            ];
            $cancelStatus = $this->repository->cancelBooking($bookingUser, $params);

            if (!$cancelStatus) throw new CustomErrorException('Pembatalan Booking tidak bisa dilakukan');

            // Send Notification
            $notificationHelper = BookingNotificationHelper::getInstance();
            $notificationHelper->notifCancelBookingToOwner($room, $bookingUser->id, $cancelReason);
            $notificationHelper->sendSmsCancelBookingToOwner($room, $cancelReason);
            $notificationHelper->notifCancelBookingToTenant($user->id, $bookingUser->id);

            $notificationCategory = NotifCategory::getNotificationCategory('booking');
            if (!is_null($notificationCategory)) {
                $userNotification = new UserNotif();
                $userNotification->user_id      = $user->id;
                $userNotification->title        = 'Booking berhasil dibatalkan';
                $userNotification->type         = 'booking_cancelled';
                $userNotification->read         = 'false';
                $userNotification->url          = '/booking';
                $userNotification->scheme       = 'bang.kerupux.com://main_search';
                $userNotification->designer_id  = $room->id;
                $userNotification->category_id  = $notificationCategory->id;
                $userNotification->save();
            }

            $response = [
                'meta'   => [
                    'message' => 'Pembatalan Booking telah berhasil'
                ],
                'data' => [
                    'cancel_status' => $cancelStatus
                ]
            ];

        } catch (CustomErrorException $e) {
            $response['meta']['message'] = $e->getMessage();
        } catch (Exception $e) {
            $exception = new RuntimeException('failed to cancel booking - ' . $e->getMessage());
            Bugsnag::notifyException($exception);
            $response['meta']['message'] = self::ERROR_SYSTEM_MESSAGE;
        }
        return Api::responseData($response);
    }

    /**
     * Function to send notification SMS and Push Notif
     * @param Room $room
     * @param User $user
     * @param Object $booking
     * @return bool|null
     */
    private function sendNotification(Room $room, User $user, $booking)
    {
        try {
            $notificationHelper = BookingNotificationHelper::getInstance();
            $notificationHelper->notifSuccessBookingToTenant($user->id, $booking->id);
            $notificationHelper->notifSuccessBookingToOwner($room, $booking->id);
            $notificationHelper->sendSmsSuccessBookingToOwner($room);
            return true;
        } catch(\Exception $e) {
            $exception = new RuntimeException('failed send notification - ' . $e->getMessage());
            Bugsnag::notifyException($exception);
            return null;
        }
    }

    /**
     * Function to send whatsapp message
     * @param Room $room
     * @param BookingUserRequest $request
     * @return bool|null
     */
    private function sendWhatsApp(Room $room, BookingUserRequest $request)
    {
        try {
            $roomOwner = $room->owners->first();
            $rentType = RentCountHelper::getFormatRentCount($request->duration, $request->rent_count_type);
            $checkinDate = Carbon::createFromFormat('Y-m-d', $request->checkin)->formatLocalized('%e %B %Y');
            $notificationData = [
                'owner_name'    => $roomOwner->user->name ?: 'Pemilik Kos',
                'owner_id'      => $roomOwner->user->id,
                'phone_number'  => $roomOwner->user->phone_number,
                'room_name'     => trim($room->name),
                'tenant_name'   => $request->contact_name,
                'duration'      => $rentType,
                'check_in_date' => trim($checkinDate),
                'link'          => config('app.url').'/ownerpage/manage/all/booking'
            ];

            $notificationWATemplate = NotificationWhatsappTemplate::where('is_active', true)->where('name', 'hanging_booking_realtime_2')->first();
            // send notification to whatsApp with queue job
            if (!is_null($notificationWATemplate)) {
                ProcessNotificationDispatch::dispatch($notificationWATemplate, $notificationData);
            }

            return true;
        } catch(Exception $e) {
            $exception = new RuntimeException('failed send WhatsApp - ' . $e->getMessage());
            Bugsnag::notifyException($exception);
            return null;
        }
    }

    /**
     * Function to return default response
     * @return array
     */
    private function makeDefaultResponse()
    {
        return [
            'status' => false,
            'meta' => ['message' => 'General Error']
        ];
    }
}
