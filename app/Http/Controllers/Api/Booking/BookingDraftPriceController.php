<?php

namespace App\Http\Controllers\Api\Booking;

use App\Entities\Room\Room;
use App\Exceptions\CustomErrorException;
use App\Http\Controllers\Api\BaseController;
use App\Http\Helpers\ApiResponse as Api;
use App\Presenters\Mamipay\MamipayRoomPriceComponentPresenter;
use App\Repositories\RoomRepository;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class BookingDraftPriceController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the user booking.
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request, $songId): JsonResponse
    {
        // force default response to false
        $response = $this->makeDefaultResponse();
        try {
            // repository
            $roomRepo = app()->make(RoomRepository::class);

            // get room data
            $room = $roomRepo->with([
                'mamipay_price_components.price_component_additionals',
                'discounts'
            ])->where('song_id', $songId)->first();
            if (!$room)
                throw new CustomErrorException('Kost tidak ditemukan');

            // transform data
            $transformData  = (new MamipayRoomPriceComponentPresenter('draft-price'))->present($room);

            // response
            $response = $transformData;

        } catch (CustomErrorException $e) {
            $response['meta']['message'] = $e->getMessage();
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
            $response['meta']['message'] = $e->getMessage();
        }
        return Api::responseData($response);
    }

    /**
     * Function to return default response
     * @return array
     */
    private function makeDefaultResponse()
    {
        return [
            'status' => false,
            'meta' => ['message' => 'General Error']
        ];
    }
}