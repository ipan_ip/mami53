<?php

namespace App\Http\Controllers\Api\Booking;

use App\Entities\Room\Element\Price;
use App\Entities\Room\Element\Tag;
use App\Entities\Room\Room;
use App\Http\Helpers\BookingUserHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseController;
use App\Http\Helpers\ApiResponse as Api;
use App\Http\Helpers\RentCountHelper;

use Validator;
use Carbon\Carbon;
use Auth;

class BookingRentCountController extends BaseController
{
    const PRICE_WEEKLY = 1;

    public function getRentCount($rentCountType)
    {
        $rentCounts = RentCountHelper::getRentCount($rentCountType);

        return Api::responseData([
            'data' => $rentCounts
        ]);
    }

    public function getRentCountByRoom($rentCount, $roomId)
    {
        // temporarily disable, because we have many side effect.
        return $this->getRentCount($rentCount);

        $room = Room::where('id', $roomId)->orWhere('song_id', $roomId)->first();
        if (!$room) {
            return Api::responseData([
                'status' => false,
                'meta'   => [
                    'message'  => 'Kamar tidak ditemukan'
                ]
            ]);
        }

        $rentCountTags = (new Tag)->getTagRentCount();
        $roomTags = $room->tags->pluck('id')->toArray();
        if (empty($roomTags)) {
            return Api::responseData([
                'status' => false,
                'meta'   => [
                    'message'  => 'Minimum sewa tidak tersedia'
                ],
                'data' => null
            ]);
        }

        $rentCounts = [];
        foreach ($rentCountTags as $key => $value) {
            if (in_array($key, $roomTags)) {
                $slicing = explode(" ",$value);
                $rentCounts[$key] = $slicing[1];
            }
        }
        sort($rentCounts);

        $price = $room->price();
        $priceWeekly = $price->priceTitleFormats($this::PRICE_WEEKLY, true);

        if (empty($rentCounts) || !is_null($priceWeekly)) {
            return $this->getRentCount($rentCount);
        }

        $durations = RentCountHelper::getMinimumDuration($rentCounts[0], $rentCount);

        return Api::responseData([
            'data' => $durations
        ]);
    }

    public function getCheckoutDate(Request $request)
    {
        $checkoutDate = BookingUserHelper::getCheckoutDate($request->rent_count_type, $request->checkin_date, $request->duration);

        return Api::responseData([
            'data' => $checkoutDate
        ]);
    }
}