<?php

namespace App\Http\Controllers\Api\Booking;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseController;
use App\Http\Helpers\BookingInvoiceHelper;
use App\Http\Helpers\ApiResponse as Api;
use Validator;
use Auth;
use App\User;
use Carbon\Carbon;
use Notification;
use App\Presenters\BookingContractPresenter;
use App\Presenters\BookingInvoicePresenter;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayInvoice;
use Jenssegers\Date\Date;

class BookingContractController extends BaseController
{
    protected $validationMessages = [
        'year.required'=>'Tahun harus diisi',
        'contract_id.required'=>'Kontrak harus diisi',
    ];

    public function getContract($contractId)
    {
        $contract       = MamipayContract::find($contractId);
        $contractKost   = $contract->kost;
        $contractTenant = $contract->tenant;

        $contractDetail = (new BookingContractPresenter('detail'))->present($contract);

        $result = [
            'contract' => $contractDetail['data'],
            'tenant'   => $contractTenant
        ];

        return Api::responseData([
            'data' => $contractDetail['data']
        ]);
    }

    public function invoiceDetail($invoiceId)
    {
        $invoice = MamipayInvoice::with('contract')->find($invoiceId);

        if (!$invoice) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Data Pembayaran tidak ditemukan'
                ]
            ], 404);
        }

        $contract = $invoice->contract;
        $tenant = $contract->tenant;
        $invoiceFromMamipay = MamipayInvoice::getDetailExternal($invoiceId);

        $invoiceUrl = null;
        $timeline = null;
        $amountString = "-";
        $amount = null;
        if ($invoiceFromMamipay) {            
            $amount = $invoiceFromMamipay->data->total_paid_amount;
            $amountString = 'Rp. ' . number_format($invoiceFromMamipay->data->total_paid_amount, 0, ',', '.');
            if (is_null($amount)) {
                $amount = $invoiceFromMamipay->data->amount;
                $amountString = 'Rp. ' . number_format($invoiceFromMamipay->data->amount, 0, ',', '.');
            }

            if ($invoice->manual_reminder || $invoice->status == "paid") {
                $invoiceUrl = BookingInvoiceHelper::withPlatformParam($invoice->shortlink_url);
                $timeline = $invoice->timeline;
            }
        }
        
        $result = [
            'name' => $invoice->name,
            'scheduled_month' => Carbon::parse($invoice->scheduled_date)
                ->format('F Y'),
            'tenant_name' => $tenant->name,
            'room_number' => $contract->type_kost->room_number,
            'paid_amount' => $amount,
            'paid_amount_string'  => $amountString,
            'paid_status_formatted' => $invoice->payment_status_formatted,
            'paid_status' => $invoice->status,
            'paid_description' => $invoiceFromMamipay->data->payment_description,
            'timeline' => $timeline,
            'cost_components'=> $invoice->price_component, //need to keep this because old app already consume it.
            'detail_components'=> $invoiceFromMamipay->data->cost_components,
            'invoice_number' => $invoice->invoice_number,
            'invoice_link' => $invoiceUrl,
            'manual_reminder' => $invoice->manual_reminder,
            'last_payment' => $invoice->manual_reminder,
            'scheduled_date' => Carbon::parse($invoice->scheduled_date)->format('d M'),
            'scheduled_date_raw' => $invoice->scheduled_date
        ];

        return Api::responseData([
            'data' => $result
        ]);
    }

    public function filterInvoiceByYear(Request $request)
    {
        $invoices = MamipayInvoice::whereYear('scheduled_date', $request->year)
                                ->where('contract_id', $request->contract_id)
                                ->get();

        if ($invoices->count() > 0) {
            $invoicesTransformData = (new BookingInvoicePresenter('list'))->present($invoices);

            return Api::responseData([
                'data' => $invoicesTransformData['data']
            ]);

        } else {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Data Invoice tidak ditemukan'
                ]
            ], 404);
        }
    }

    public function filterInvoiceByYearFromExternal(Request $request)
    {
        // Validate request
        $validator = Validator::make($request->all(), 
            [
                'contract_id' => 'required',
                'year'        => 'required'
            ], $this->validationMessages);

        if($validator->fails()) {
            return Api::responseData([
                'status' => false,
                'meta'   => [
                    'message'  => implode(", ",$validator->messages()->all())
                ]
            ]);
        }

        $invoices = MamipayInvoice::getPaymentScheduleExternal($request->contract_id, $request->year);

        $result = [];
        foreach ($invoices->schedules as $invoice) {
            $descriptionStatus = null;
            $scheduledDate = Carbon::parse($invoice->scheduled_date_raw);
            $paidAt = Carbon::parse($invoice->paid_at);
            $checkScheduledPaid = $paidAt->diffInDays($scheduledDate, false);

            $statusPayment = null;
            if ($checkScheduledPaid < 0) {
                $statusPayment = "Pembayaran terlambat ". abs($checkScheduledPaid) ." hari";
            } else {
                if ($invoice->payment_status == MamipayInvoice::PAYMENT_STATUS_PAID) {
                    $statusPayment = MamipayInvoice::ONTIME_PAYMENT;
                } else {
                    $statusPayment = $invoice->payment_status_formatted;
                }
            }

            $credentials = [
                '_id'            => $invoice->id,
                'name'           => $invoice->name,
                'invoice_number' => $invoice->invoice_number,
                'shortlink'      => $invoice->invoice_link,
                'scheduled_date' => Date::parse($invoice->scheduled_date_raw)->format('d F Y H:i'),
                'paid_at'        => $invoice->paid_at,
                'amount'         => $invoice->amount,
                'amount_string'  => 'Rp. ' . number_format($invoice->amount, 0, ',', '.'),
                'status'         => ($invoice->payment_status == MamipayInvoice::PAYMENT_STATUS_NOT_IN_MAMIPAY) ? MamipayInvoice::PAYMENT_STATUS_PAID : $invoice->payment_status,
                'late_payment'   => abs($checkScheduledPaid),
                'status_payment' => $statusPayment,
                'manual_reminder' => $invoice->manual_reminder,
                'last_payment' => true,
            ];

            $result[] = $credentials;
        }

        if ($result) {
            return Api::responseData([
                'data' => $result
            ]);

        } else {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Data Invoice tidak ditemukan'
                ]
            ], 404);
        }
    }
    
}
