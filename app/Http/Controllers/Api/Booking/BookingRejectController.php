<?php

namespace App\Http\Controllers\Api\Booking;

use Auth;
use Notification;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use RuntimeException;
use Exception;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Api\BaseController;
use App\Http\Helpers\ApiResponse as Api;
use App\Repositories\Booking\RejectReason\BookingRejectReasonRepository;


class BookingRejectController extends BaseController
{
    protected $repository;

    public function __construct(BookingRejectReasonRepository $repository)
    {
        $this->repository = $repository;
        parent::__construct();
    }

    /**
     * Get all reason by type
     *
     * @param string $type
     * @return JsonResponse
     */
    public function getAllByType(string $type): JsonResponse
    {
        try {
            $reasons = $this->repository->getAllByType($type);
            $response = [
                'data' => $reasons
            ];

        } catch(Exception $e) {;
            $exception = new RuntimeException('failed to get data - ' . $e->getMessage());
            Bugsnag::notifyException($exception);
            $response['status'] = false;
            $response['meta']['message'] = $e->getMessage();

        }

        return Api::responseData($response);
    }

}