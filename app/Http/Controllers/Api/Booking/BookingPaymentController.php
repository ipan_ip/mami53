<?php

namespace App\Http\Controllers\Api\Booking;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseController;
use App\Http\Helpers\ApiResponse as Api;
use App\Repositories\Booking\BookingPaymentRepository;
use App\Presenters\BookingPaymentPresenter;
use Validator;

use App\Entities\Booking\BookingPayment;
use App\Entities\Booking\BookingUser;
use App\Entities\Premium\Bank;

class BookingPaymentController extends BaseController
{
    protected $repository;

    protected $validationMessages = [
        'bank_destination.required'=>'Bank Tujuan harus diisi',
        'bank_destination.numeric'=>'Bank Tujuan harus berupa angka',
        'bank_source.required'=>'Bank Asal harus diisi',
        'bank_source.max'=>'Bank Asal maksimal :max karakter',
        'account_name.required'=>'Nama Pemilik Rekening harus diisi',
        'account_name.max'=>'Nama Pemilik Rekening maksimal :max karakter',
        'account_number.required'=>'Nomor Rekening harus diisi',
        'account_number.max'=>'Nomor Rekening maksimal :max karakter',
        'total_payment.required'=>'Total pembayaran harus diisi',
        'total_payment.numeric'=>'Total pembayaran harus berupa angka',
        'payment_date.required'=>'Tanggal Pembayaran harus diisi',
        'payment_date.date_format'=>'Format Tanggal Pembayaran salah'
    ];

    public function __construct(BookingPaymentRepository $repository)
    {
        $this->repository = $repository;

        parent::__construct();
    }

    /**
     * Confirm Payment
     *
     * @param Request           request
     * @param string            Booking Code
     */
    public function confirmation(Request $request, $bookingCode)
    {
        $user = $this->user();

        $bookingUser = BookingUser::where('booking_code', $bookingCode)
                                ->where('user_id', $user->id)
                                ->first();

        if(!$bookingUser) {
            return Api::responseData([
                'status'=>false,
                'messages'=>['Booking tidak ditemukan']
            ]);
        }

        $validator = Validator::make($request->all(),[
            'bank_destination'=>'required|numeric',
            'bank_source'=>'required|max:50',
            'account_name'=>'required|max:100',
            'account_number'=>'nullable|max:50',
            'total_payment'=>'required|numeric',
            'payment_date'=>'required|date_format:Y-m-d'
        ], $this->validationMessages);

        $validator->after(function($validator) use ($request) {
            $bankAccount = Bank::where('is_active', 1)
                                ->where('id', $request->bank_destination)
                                ->first();

            if(!$bankAccount) {
                $validator->errors()->add('bank_destination', 'Bank Tujuan tidak ditemukan');
            }
        });

        if($validator->fails()) {
            return Api::responseData([
                    'status'=>false,
                    'messages'=>$validator->errors()->all()
                ]);
        }

        // confirmation can't be processed if booking status is not confirmed
        if(!in_array($bookingUser->status, [
                BookingUser::BOOKING_STATUS_CONFIRMED,
                BookingUser::BOOKING_STATUS_PAID
            ])) {
            return Api::responseData([
                    'status'=>false,
                    'messages'=>['Konfirmasi pembayaran tidak dapat dilakukan. Pastikan admin sudah mengkonfirmasi ketersediaan kamar.']
                ]);
        }

        $params = [
            'bank_account_id'=>$request->get('bank_destination'),
            'bank_source'=>$request->get('bank_source'),
            'account_name'=>$request->get('account_name'),
            'account_number'=>$request->get('account_number'),
            'total_payment'=>$request->get('total_payment'),
            'payment_date'=>$request->get('payment_date')
        ];

        $this->repository->setPresenter(new BookingPaymentPresenter('detail'));

        $payment = $this->repository->paymentConfirmation($bookingUser, $params);

        return Api::responseData([
            'data'=>$payment['data']
        ]);
    }
}