<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\OwnerDataRepository;
use App\Http\Helpers\ApiResponse as Api;
use Validator;
use Cache;
use Mail;
use App\User;
use App\Entities\Owner\OwnerRegisterRequest;

class OwnerRegisterRequestController extends BaseController
{
    public function registerRequest(Request $request)
    {
        $bankAccountName = $request->filled('bank_account_name') ? $request->input('bank_account_name') : null;
        $bankAccountNumber = $request->filled('bank_account_number') ? $request->input('bank_account_number') : null;

        $credentials = [
            'name' => $request->name,
            'email' => $request->email,
            'phone_number' => $request->phone_number,
            'user_id' => $request->user_id,
            'room_name' => $request->room_name,
            'room_slug' => $request->room_slug,
            'status' => 'request',
            'bank_account_name' => $bankAccountName,
            'bank_account_number' => $bankAccountNumber
        ];

        $registration = OwnerRegisterRequest::create($credentials);

        if ($registration) {
            return Api::responseData([
                'status' => true,
                'meta'   => [
                    'message' => 'Success registration request'
                ]
            ]);
        } else {
            return Api::responseData([
                'status' => false,
                'meta'   => [
                    'message' => 'Failed registration request'
                ]
            ]);
        }
    }
    
}
