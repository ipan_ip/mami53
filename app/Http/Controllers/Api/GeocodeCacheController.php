<?php

namespace App\Http\Controllers\Api;

use App\Http\Helpers\ApiResponse;
use Illuminate\Http\Request;
use App\Entities\Geocode\GeocodeCache;
use App\Entities\Geocode\GeocodeCacheProcessed;
use Validator;

/**
* 
*/
class GeocodeCacheController extends BaseController
{
    
    public function saveGeocodeCache(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'lat' => 'required|numeric',
            'lng' => 'required|numeric',
            'city' => 'nullable|max:150',
            'subdistrict' => 'nullable|max:150'
        ]);

        if($validator->fails()) {
            return ApiResponse::responseData([
                'status' => false,
                'meta' => [
                    'message' => implode('. ', $validator->errors()->all())
                ]
            ]);
        }

        $geocodeCache = new GeocodeCache;
        $geocodeCache->lat = $request->lat;
        $geocodeCache->lng = $request->lng;
        // $geocodeCache->place_id = $request->place_id;
        $geocodeCache->postal_code = is_null($request->postal_code) ? '-' : $request->postal_code;
        $geocodeCache->country = is_null($request->country) ? '-' : $request->country;
        $geocodeCache->city = is_null($request->city) ? '-' : $request->city;
        $geocodeCache->subdistrict = is_null($request->subdistrict) ? '-' : $request->subdistrict;
        $geocodeCache->status = GeocodeCache::STATUS_NEW;
        $geocodeCache->save();

        return ApiResponse::responseData([
            'status' => true
        ]);
    }

    public function getGeocodeCache(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'lat' => 'required|numeric',
            'lng' => 'required|numeric'
        ]);

        if($validator->fails()) {
            return ApiResponse::responseData([
                'status' => false,
                'meta' => [
                    'message' => implode('. ', $validator->errors()->all())
                ]
            ]);
        }

        $cache = (new GeocodeCacheProcessed)->getCacheByLatLng($request->lat, $request->lng);

        $output = null;

        if($cache) {
            $output = $cache->toArray();
            unset($output['id']);
            unset($output['created_at']);
            unset($output['updated_at']);
            unset($output['deleted_at']);

            $output['city'] = $output['city'] == '-' ? null : $output['city'];
            $output['subdistrict'] = $output['subdistrict'] == '-' ? null : $output['subdistrict'];
        }
        else {
            // To prevent calling Google Geocode (it's expensive!!), just give jakarta info
            $output = [ 
                "lat_1" => -6.142438933765712,
                "lng_1" => 106.78177965804935,
                "lat_2" => -6.142438933765712,
                "lng_2" => 106.78177965804935,
                "city" => "jakarta",
                "subdistrict" => "jakarta"
            ];
        }

        return ApiResponse::responseData([
            'status'=>true,
            'cache' => $output
        ]);


    }
    
}