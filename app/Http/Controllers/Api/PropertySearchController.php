<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Session;

use App\Entities\Property\PropertyType;
use App\Http\Helpers\ApiResponse;
use App\Libraries\KostSearch\KostSearcher;
use App\Entities\User\UserSearchHistoryTemp;
use App\Repositories\RoomRepository;
use App\Presenters\RoomPresenter;
use App\Transformers\Room\ListTransformer;
use App\Http\Controllers\Api\RoomController;
use App\Http\Helpers\OpenSslEncryptor;

class PropertySearchController extends BaseController
{
    public function __construct(RoomRepository $repository)
    {
        $this->repository = $repository;
    }

    public function searchKost(Request $request)
	{
        // Log Search History
        $filters = $request->input('filters');
        (new UserSearchHistoryTemp())->storeHistoryFilter(
            $filters,
            false,
            $this->user(),
            $this->device(),
            Session::getId()
        );

        $searcher = new KostSearcher(PropertyType::Kost());

        $result = $searcher->searchByRequest($request);

        $rooms = empty($result->items) === true
            ? []
            : $result->items->map(function($item) {
                return (new ListTransformer)->transform($item);
            });

        $response = [
            'source' => 'es',
            'total' => $result->total, 
            'has-more' => ($result->total > ($request->offset + $request->limit)),
            'rooms' => $rooms
        ];

        if (in_array($request->getPathInfo(), RoomController::ANTISCRAPING_ENCRYPTION['encryptable_routes'])
            && isset($response['rooms'])) {
            $jsonString = json_encode($response['rooms']);
            $response['rooms'] = OpenSslEncryptor::encrypt(
                $jsonString,
                RoomController::ANTISCRAPING_ENCRYPTION['key'],
                RoomController::ANTISCRAPING_ENCRYPTION['iv']
            );
        }

        return ApiResponse::responseData($response);
    }
}