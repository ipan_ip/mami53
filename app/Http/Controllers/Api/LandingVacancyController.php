<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Helpers\ApiResponse as Api;
use App\Entities\Landing\LandingVacancy;

class LandingVacancyController extends BaseController
{
    public function showFilter(Request $request, $slug)
    {
        $landing = LandingVacancy::where('slug', $slug)->first();

        if(!$landing) {
            return Api::responseData([
                'landing' => null
            ]);
        }

        return Api::responseData([
            'landing' => [
                'location' => $landing->location,
                'filters' => $landing->filters
            ]
        ]);
    }
}
