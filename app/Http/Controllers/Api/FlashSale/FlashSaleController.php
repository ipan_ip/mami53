<?php

namespace App\Http\Controllers\Api\FlashSale;

use App\Criteria\FlashSale\ActiveCriteria;
use App\Criteria\FlashSale\RunningCriteria;
use App\Criteria\FlashSale\UpcomingCriteria;
use App\Http\Controllers\Api\BaseController;
use App\Http\Helpers\ApiResponse as Api;
use App\Http\Helpers\FeatureFlagHelper;
use App\Presenters\FlashSalePresenter;
use App\Repositories\FlashSale\FlashSaleRepositoryEloquent;
use Illuminate\Http\JsonResponse;
use Prettus\Repository\Exceptions\RepositoryException;
use Illuminate\Http\Request;

class FlashSaleController extends BaseController
{
    public const BAD_REQUEST_MESSAGE = 'Missing or malformed Flash Sale data ID!';

    /**
     * @var FlashSaleRepositoryEloquent
     */
    protected $repository;

    public function __construct(FlashSaleRepositoryEloquent $repository)
    {
        parent::__construct();

        $this->repository = $repository;
    }

    /**
     * @return JsonResponse
     * @throws RepositoryException
     */
    public function getAllData()
    {
        $this->repository->setPresenter(new FlashSalePresenter('api-index'));
        $this->repository->pushCriteria(new ActiveCriteria());

        $response = FeatureFlagHelper::isSupportFlashSale() ?
            $this->repository
                ->with(
                    [
                        'areas.landings.landing'
                    ]
                )
                ->all() :
            null;

        $totalFinished = 0;
        $totalRunning = 0;
        $totalUpcoming = 0;

        if ($response['data']) {
            foreach ($response['data'] as $data) {
                if (!$data['is_running'] && !$data['is_finished']) {
                    $totalUpcoming++;
                } elseif ($data['is_running']) {
                    $totalRunning++;
                } else {
                    $totalFinished++;
                }
            }
        }

        return Api::responseData(
            [
                'total_finished' => $totalFinished,
                'total_running' => $totalRunning,
                'total_upcoming' => $totalUpcoming,
                'datas' => $response['data']
            ]
        );
    }

    /**
     * @param $string
     * @return JsonResponse
     * @throws RepositoryException
     */
    public function getData($string = '')
    {
        if (empty($string)) {
            return $this->sendErrorResponse(self::BAD_REQUEST_MESSAGE);
        }

        $this->repository->setPresenter(new FlashSalePresenter('api-specific'));
        $this->repository->pushCriteria(new ActiveCriteria());

        $response = $this->repository
            ->with(
                [
                    'areas.landings.landing'
                ]
            )
            ->scopeQuery(
                function ($query) use ($string) {
                    if (is_numeric($string)) {
                        return $query->where('id', (int)$string);
                    } else {
                        return $query->where('name', 'like', $string);
                    }
                }
            )
            ->first();

        return Api::responseData(
            [
                'data' => !empty($response['data']) ? $response['data'] : null
            ]
        );
    }

    /**
     * Get running flashSale
     * 
     * @param  Request $request
     * @return JsonResponse
     * @throws RepositoryException
     */
    public function getRunningData(Request $request)
    {
        $this->repository->setPresenter(new FlashSalePresenter('api-single'));
        $this->repository->pushCriteria(new RunningCriteria());

        $response = FeatureFlagHelper::isSupportFlashSale() ?
            $this->repository
                ->with(
                    [
                        'areas.landings.landing'
                    ]
                )
                ->first() :
            null;

        $response = !empty($response['data']) ? $response['data'] : null;

        return Api::responseData(['data' => $response]);
    }

    /**
     * @return JsonResponse
     * @throws RepositoryException
     */
    public function getUpcomingData()
    {
        $this->repository->setPresenter(new FlashSalePresenter('api-single'));
        $this->repository->pushCriteria(new UpcomingCriteria());

        $response = $this->repository
            ->with(
                [
                    'areas.landings.landing'
                ]
            )
            ->first();

        return Api::responseData(
            [
                'data' => !empty($response) ? $response['data'] : null
            ]
        );
    }

    /**
     * @param $message
     * @param string $severity
     * @param int $code
     * @return JsonResponse
     */
    public function sendErrorResponse($message, $severity = 'Bad Request', $code = 400)
    {
        return Api::responseError(
            $message,
            $severity,
            $code
        );
    }

}
