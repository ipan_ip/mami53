<?php

namespace App\Http\Controllers\Api\FakeDoor;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseController;
use App\Http\Helpers\ApiResponse as Api;
use Validator;
use App\Entities\FakeDoor\FakeDoorRentHouse;

class FakeDoorRentHouseController extends BaseController
{

    protected $validationMessages = [
        'type.required' => 'Tipe harus diisi',
        'area.required' => 'Area harus diisi',
        'area.max' => 'Area maksimal :max karakter',
        'email.required' => 'Email harus diisi',
        'email.email'   => 'Format email tidak valid',
        'email.max' => 'Email maksimal :max karakter'
    ];

    public function __construct()
    {
        parent::__construct();
    }

    public function submitSurvey(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'type' => 'required',
            'area' => 'required|max:250',
            'email' => 'required|email|max:190'
        ], $this->validationMessages);

        if($validator->fails()) {
            return Api::responseData([
                'status'=>'false',
                'meta' => [
                    'message' => implode('. ', $validator->errors()->all()),
                    'messages' => $validator->errors()->all()
                ]
            ]);
        }

        $survey = new FakeDoorRentHouse;
        $survey->type = $request->type;
        $survey->email = $request->email;
        $survey->area = $request->area;
        $survey->save();

        return Api::responseData([
            'status' => true
        ]);
    }
}