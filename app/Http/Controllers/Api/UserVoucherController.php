<?php

namespace App\Http\Controllers\Api;

use App\Entities\Booking\BookingUser;
use App\Http\Helpers\ApiResponse as Api;
use App\Presenters\UserVoucherPresenter;
use App\Repositories\Voucher\VoucherRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class UserVoucherController extends BaseController
{
    private $repository;
    private $presenter;

    protected const TYPE_ACTIVE = 'active';
    protected const TYPE_USED = 'used';
    protected const TYPE_EXPIRED = 'expired';

    protected const DEFAULT_PAGINATION = 10;

    public function __construct(VoucherRepository $repository, UserVoucherPresenter $presenter)
    {
        $this->repository = $repository;
        $this->presenter = $presenter;
    }

    public function index(Request $request): JsonResponse
    {
        $this->user = $this->user();
        if (empty($this->user) || $this->user->isOwner()) {
            return Api::responseUserFailed();
        }
        $limit = (int) $request->get('limit', self::DEFAULT_PAGINATION);
        $type = $request->get('type', self::TYPE_ACTIVE);
        $this->repository->setPresenter($this->presenter);

        $kostId = null;
        $kostCity = null;
        $bookingUser = $this->getLastBooking();
        if (!empty($bookingUser)) {
            $kostId = $bookingUser->designer->id;
            $kostCity = $bookingUser->designer->area_city;
        }

        if (
            is_null(optional($this->user->mamipay_tenant)->id) &&
            is_null($this->user->email) &&
            is_null($kostId) &&
            is_null($kostCity) &&
            is_null($this->user->job) &&
            is_null($this->user->work_place)
        ) {
            return $this->emptyResponse($limit);
        }

        switch ($type) {
            case self::TYPE_USED:
                if (is_null(optional($this->user->mamipay_tenant)->id)) {
                    return $this->emptyResponse($limit);
                }
                $voucherList = $this->repository->getUsedUserVoucherList(
                    $limit,
                    optional($this->user->mamipay_tenant)->id,
                    $this->user->email,
                    $kostId,
                    $kostCity,
                    $this->user->job,
                    $this->user->work_place
                );
                break;
            case self::TYPE_EXPIRED:
                $voucherList = $this->repository->getExpiredUserVoucherList(
                    $limit,
                    optional($this->user->mamipay_tenant)->id,
                    $this->user->email,
                    $kostId,
                    $kostCity,
                    $this->user->job,
                    $this->user->work_place
                );
                break;
            case self::TYPE_ACTIVE:
            default:
                $voucherList = $this->repository->getActiveUserVoucherList(
                    $limit,
                    optional($this->user->mamipay_tenant)->id,
                    $this->user->email,
                    $kostId,
                    $kostCity,
                    $this->user->job,
                    $this->user->work_place
                );
        }

        $response = $this->transformDataPagination($voucherList);
        return Api::responseData($response);
    }

    public function count(): JsonResponse
    {
        $this->user = $this->user();
        if (empty($this->user) || $this->user->isOwner()) {
            return Api::responseUserFailed();
        }

        $kostId = null;
        $kostCity = null;
        $bookingUser = $this->getLastBooking();
        if (!empty($bookingUser)) {
            $kostId = $bookingUser->designer->id;
            $kostCity = $bookingUser->designer->area_city;
        }

        if (
            is_null(optional($this->user->mamipay_tenant)->id) &&
            is_null($this->user->email) &&
            is_null($kostId) &&
            is_null($kostCity) &&
            is_null($this->user->job) &&
            is_null($this->user->work_place)
        ) {
            $count = 0;
        } else {
            $count = $this->repository->countActiveUserVoucher(
                optional($this->user->mamipay_tenant)->id,
                $this->user->email,
                $kostId,
                $kostCity,
                $this->user->job,
                $this->user->work_place
            );
        }
        return Api::responseData(compact('count'));
    }

    public function show(int $id): JsonResponse
    {
        $this->user = $this->user();
        if (empty($this->user) || $this->user->isOwner()) {
            return Api::responseUserFailed();
        }

        $this->presenter->setResponseType(UserVoucherPresenter::RESPONSE_TYPE_DETAIL);
        $this->repository->setPresenter($this->presenter);

        $voucher = $this->repository->getById($id, $this->user->email);
        if (!$voucher) {
            return Api::responseError(false, false, 200, 404);
        }
        return Api::responseData($voucher);
    }

    private function getLastBooking(): ?BookingUser
    {
        $bookingUser = BookingUser::with('designer')
            ->where('user_id', $this->user->id)
            ->whereIn('status', [BookingUser::BOOKING_STATUS_VERIFIED, BookingUser::BOOKING_STATUS_CHECKED_IN])
            ->orderBy('created_at', 'desc')
            ->first();
        return $bookingUser;
    }

    private function transformDataPagination($data): array
    {
        $pagination = $data['meta']['pagination'];
        $transformedData = [
            'data' => $data['data'],
            'has_next' => ($pagination['count'] == $pagination['per_page']),
            'current_page' => $pagination['current_page'],
            'limit' => $pagination['per_page']
        ];

        return $transformedData;
    }

    private function emptyResponse(int $limit = self::DEFAULT_PAGINATION): JsonResponse
    {
        $response = [
            'data' => [],
            'has_next' => false,
            'current_page' => 1,
            'limit' => $limit
        ];
        return Api::responseData($response);
    }
}
