<?php

namespace App\Http\Controllers\Api;

use App\Http\Helpers\ApiResponse as Api;
use App\Entities\Level\FlagEnum;
use App\Entities\Level\HistoryActionEnum;
use App\Entities\Room\Room;
use App\Repositories\Level\KostLevelRepository;
use Illuminate\Http\Request;

class KostLevelController extends BaseController
{
    private $repository;
    private $kost;
    private $passValidate = false;

    public function __construct(KostLevelRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        $kostLevel = $this->repository->getAll()->toArray();
        $levelFaq = $this->repository->getFaq()->toArray();

        $response = [];
        foreach ($kostLevel as $levelItem) {
            $benefits = [];
            $criterias = [];
            $tnc = '';
            if (!empty($levelItem['benefits'])) {
                $benefits = array_column($levelItem['benefits'], 'text');
            }
            if (!empty($levelItem['criterias'])) {
                $criterias = array_column($levelItem['criterias'], 'text');
            }
            if (!empty($levelItem['tnc'])) {
                $tnc = $levelItem['tnc'];
            }

            $response['benefits'][] = [
                'level' => [
                    'id' => $levelItem['id'],
                    'name' => $levelItem['name'],
                    'is_regular' => (bool) $levelItem['is_regular'],
                    'join_count' => (int) $levelItem['join_count'],
                    'charging_fee' => (int) $levelItem['charging_fee']
                ],
                'data' => $benefits
            ];
            $response['criterias'][] = [
                'level' => [
                    'id' => $levelItem['id'],
                    'name' => $levelItem['name'],
                    'is_regular' => (bool) $levelItem['is_regular'],
                    'join_count' => (int) $levelItem['join_count'],
                    'charging_fee' => (int) $levelItem['charging_fee']
                ],
                'data' => $criterias
            ];
            $response['tnc'][] = [
                'level' => [
                    'id' => $levelItem['id'],
                    'name' => $levelItem['name'],
                    'is_regular' => (bool) $levelItem['is_regular'],
                    'join_count' => (int) $levelItem['join_count'],
                    'charging_fee' => (int) $levelItem['charging_fee']
                ],
                'data' => $tnc
            ];
        }
        $response['faq'] = $levelFaq;
        return Api::responseData($response);
    }

    public function approve($kostId)
    {
        $this->user = $this->user();
        if (is_null($this->user) || $this->user->is_owner == 'false') {
            return $this->buildFailedResponse();
        }

        $response = $this->validateRequestFromNotif($kostId);
        if (!$this->passValidate) {
            return $response;
        }

        $this->repository->approveUpgrade($this->kost, $this->user);
        return Api::responseData(['status' => true]);
    }

    public function reject($kostId)
    {
        $this->user = $this->user();
        if (is_null($this->user) || $this->user->is_owner == 'false') {
            return $this->buildFailedResponse();
        }

        $response = $this->validateRequestFromNotif($kostId);
        if (!$this->passValidate) {
            return $response;
        }

        $this->repository->removeFlag($this->kost, $this->user, HistoryActionEnum::ACTION_REJECT);
        return Api::responseData(['status' => true]);
    }

    public function ack($kostId)
    {
        $this->user = $this->user();
        if (is_null($this->user) || $this->user->is_owner == 'false') {
            return Api::responseData(['status' => false]);
        }

        $response = $this->validateRequestFromNotif($kostId, FlagEnum::FLAG_DOWNGRADED);
        if (!$this->passValidate) {
            return $response;
        }

        $this->repository->removeFlag($this->kost, $this->user);
        return Api::responseData(['status' => true]);
    }

    private function validateRequestFromNotif($kostId, $flagToCheck = FlagEnum::FLAG_UPGRADE)
    {
        $this->kost = Room::with(['level', 'owners', 'owners.user'])->where('song_id', $kostId)->first();
        if (is_null($this->kost)) {
            return $this->buildFailedResponse('Kost tidak ditemukan.');
        }
        $kostOwner = $this->kost->owners->first();
        if (is_null($kostOwner) || $kostOwner->user->id !== $this->user->id) {
            return $this->buildFailedResponse('Kost tidak ditemukan.');
        }

        $message = 'Kost tidak dapat naik level.';
        if ($flagToCheck == FlagEnum::FLAG_DOWNGRADED) {
            $message = 'Kost tidak turun level.';
        }

        $kostLevel = $this->kost->level->first();
        if (is_null($kostLevel)) {
            return $this->buildFailedResponse($message);
        }
        $kostFlagLevel = $kostLevel->pivot->flag;
        if (is_null($kostFlagLevel) || $kostFlagLevel != $flagToCheck) {
            return $this->buildFailedResponse($message);
        }

        $this->passValidate = true;
    }

    private function buildFailedResponse($message = '')
    {
        if (!empty($message)) {
            return Api::responseError($message, false, 200);
        }
        return Api::responseUserFailed();
    }

    public function flagCount()
    {
        $this->user = $this->user();
        if (is_null($this->user) || $this->user->is_owner == 'false') {
            return Api::responseData(['status' => false]);
        }

        $count = $this->repository->countOwnerFlaggedKost($this->user);
        return Api::responseData(['count' => $count]);
    }

    public function kostList(Request $request)
    {
        $this->user = $this->user();
        if (is_null($this->user) || $this->user->is_owner == 'false') {
            return Api::responseData(['status' => false]);
        }

        $name = (string) $request->get('name');
        $level = (int) $request->get('level');

        $kostList = $this->repository->getOwnerActiveKostList($this->user, $name, $level);
        $response = [];
        foreach ($kostList as $kost) {
            $transformedKost = [
                '_id' => $kost->song_id,
                'room_title' => $kost->name,
                'level_info' => $kost->level_info,
            ];
            if (!is_null($transformedKost['level_info']['flag'])) {
                array_unshift($response, $transformedKost);
            } else {
                $response[] = $transformedKost;
            }
        }
        return Api::responseData(['rooms' => $response]);
    }
}
