<?php

namespace App\Http\Controllers\Api\Card;

use App\Http\Controllers\Api\BaseController;
use App\Http\Helpers\ApiResponse as Api;
use App\Http\Helpers\PhotoCategoryHelper;
use App\Repositories\RoomRepository;

class CardController extends BaseController
{
    /**
     * @var RoomRepository
     */
    protected $repository;

    public function __construct(RoomRepository $repository)
    {
        $this->repository = $repository;
        parent::__construct();
    }

    /**
     * get photo with category for detail kost
     * 
     * @param int $songId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCardsWithCategory(int $songId)
    {
        $room = $this->repository->where('song_id', $songId)->first();
        if (is_null($room)) {
            return Api::responseData([
                'status' => false,
                'meta' => ['message' => 'Data kost tidak ditemukan'],
            ]);
        }

        $cards = PhotoCategoryHelper::getListCardWithCategory($room);
        $populatedCards = PhotoCategoryHelper::populatePhotoPerCategory($cards);

        return Api::responseData(['data' => $populatedCards]);
    }

}