<?php
/**
 * Created by PhpStorm.
 * User: sandiahsan
 * Date: 23/10/18
 * Time: 10.38
 */

namespace App\Http\Controllers\Api;

use App\Entities\Activity\ActivationCode;
use App\Entities\Activity\Tracking;
use App\Http\Helpers\ApiHelper;
use App\Http\Helpers\ApiResponse;
use App\Libraries\SMSLibrary;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Jenssegers\Agent\Agent;
use Validator;

class ForgotPasswordController extends BaseController
{
    public function getLastVerificationCodeAccount(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone_number'	=> 'required|regex:/^(08[1-9]\d{7,10})$/',

        ]);

        if($validator->fails()) {
            return ApiResponse::responseData([
                'status' => false,
                'meta'   => [
                    'message' => $validator->errors()
                ]
            ]);

        } else {
            $accountVerificationCode = ActivationCode::select('phone_number', 'code_hashed', 'expired_at')
                ->where('phone_number', $request->input('phone_number'))
                ->orderBy('created_at', 'desc')
                ->first();

            if ($accountVerificationCode) {
                return ApiResponse::responseData([
                    'status' => true,
                    'data'   => $accountVerificationCode
                ]);

            } else {
                return ApiResponse::responseData([
                    'status' => false,
                    'meta'   => [
                        'message' => "Account not found"
                    ]
                ]);
            }

        }
    }

    public function storeVerificationCode($request)
    {
        $agent          = new Agent();
        $phoneNumber    = $request->input('phone_number');
        $isRegistered   = User::isRegisteredOwner($phoneNumber);

        $verificationCode   = ApiHelper::random_string('alnum', '4');
        $codeHashed         = md5($verificationCode);
        $expiredAt          = Carbon::now()->addHours(2)->format('Y-m-d H:i:s');
        $device             = Tracking::checkDevice($agent);

        // if user request verification code again, check created at > 3 minutes send new code
        if ($isRegistered) {
            // set expired_at, phone_number, device, code, code_hashed, for(page)
            $credentials = [
                'phone_number' => $request->input('phone_number'),
                'device'       => $device,
                'code'         => $verificationCode,
                'for'          => 'forgot_password',
                'code_hashed'  => $codeHashed,
                'expired_at'   => $expiredAt
            ];

            // save verification code (verification_code table)
            $codeRequest = ActivationCode::create($credentials);

            if ($codeRequest) {
                $message    = "Verification code reset password : $verificationCode";

                // send message
                SMSLibrary::smsVerification($phoneNumber, $message);

                return ApiResponse::responseData([
                    'status' => true,
                    'data'   => [
                        'code'          => $codeHashed,
                        'is_registered' => $isRegistered
                    ]
                ]);

            } else {
                return ApiResponse::responseData([
                    'status' => false,
                    'meta'   => [
                        'message' => 'Failed to create data activation code'
                    ]
                ]);

            }

        } else {
            return ApiResponse::responseData([
                'status' => false,
                'meta'   => [
                    'message'     => 'Sorry, Your number is not registered',
                    'error_type'  => 'invalid_number'
                ]
            ]);
        }

    }

    /**
     *  Check whether phone number is registered as owner
     *
     *  @deprecated
     */
    public function checkAccount(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone_number'	=> 'required|regex:/^(08[1-9]\d{7,10})$/',

        ]);

        if($validator->fails()) {
            return ApiResponse::responseData([
                'status' => false,
                'meta'   => [
                    'message' => $validator->errors()
                ]
            ]);

        } else {
            $isRegistered   = User::isRegisteredOwner($request->input('phone_number'));
            if (!$isRegistered) {
                return ApiResponse::responseData([
                    'status' => false,
                    'meta'   => [
                        'message'     => 'Sorry, Your number is not registered',
                        'error_type'  => 'invalid_number'
                    ]
                ]);
            }

            // Check last request verification code by number phone
            $checkLastRequestVerificationCode = ActivationCode::select('phone_number', 'code_hashed', 'expired_at')
                ->where('phone_number', $request->input('phone_number'))
                ->where('created_at', '>', Carbon::now()->subMinutes(3)->toDateTimeString())
                ->orderBy('created_at', 'desc')
                ->first();

            if ($checkLastRequestVerificationCode != null) {
                return ApiResponse::responseData([
                    'status' => false,
                    'meta'   => [
                        'message'       => 'Sorry, you cannot request a verification code, please wait for a while',
                        'error_type'    => 'code_delay'
                    ]
                ]);

            } else {
                $LastRequestVerificationCode = ActivationCode::where('phone_number', $request->input('phone_number'))
                                ->orderBy('created_at', 'desc')
                                ->first();

                if ($LastRequestVerificationCode) {
                    $LastRequestVerificationCode->delete();
                }

                // proses store verification code
                return $this->storeVerificationCode($request);
            }
        }
    }

    public function checkRequestVerificationCodeAgain($request)
    {

        $checkTimeVerificationCreate = ActivationCode::where('phone_number', $request['phone_number'])
                                                    ->where('code_hashed', $request['code_hashed'])
                                                    ->where('created_at', '>', Carbon::now()->subMinutes(3)->toDateTimeString())
                                                    ->first();

        return $checkTimeVerificationCreate;
    }

    /**
     *  Check if owner forgot password's verification code is correct
     *
     *  @deprecated
     */
    public function checkVerificationCode(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone_number'  => 'required|regex:/^(08[1-9]\d{7,10})$/',

        ]);

        if($validator->fails()) {
            return ApiResponse::responseData([
                'status' => false,
                'meta'   => [
                    'message' => $validator->errors()
                ]
            ]);

        } else {
            $phoneNumber            = $request->input('phone_number');
            $verificationCode       = $request->input('verification_code');
            $verificationCodeHashed = md5($verificationCode);

            // check phone number and code valid
            $activationCodes = ActivationCode::where('phone_number', $phoneNumber)
                                                    ->whereRaw('expired_at > NOW()')
                                                    ->where('for', 'forgot_password')
                                                    ->where('code_hashed', $verificationCodeHashed)
                                                    ->first();
            if ($activationCodes) {
                return ApiResponse::responseData([
                    'status' => true,
                    'meta'   => [
                        'message' => 'Your verification code is valid',

                    ]
                ]);

            } else {
                return ApiResponse::responseData([
                    'status' => false,
                    'meta'   => [
                        'message'       => 'Your verification code is invalid',
                        'error_type'    => 'invalid_code'
                    ]
                ]);

            }
        }
    }

    /**
     *  Validate owner forgot password request and change the password
     *
     *  @deprecated
     */
    public function resetPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone_number'      => 'nullable|regex:/^(08[1-9]\d{7,10})$/',
            'verification_code' => 'required',
            'new_password'      => 'required|min:6',
            'confirm_password'  => 'required|min:6'
        ], [
            'phone_number.regex'           => 'Format No HP tidak valid (08xxxxxxxx)',
            'strcode.required'             => 'Field tidak lengkap',
            'new_password.required'        => 'Password Baru harus diisi',
            'new_password.min'             => 'Password Baru minimal :min karakter',
            'confirm_password.required'    => 'Konfirmasi Password harus diisi',
            'confirm_password.min'         => 'Konfirmasi Password minimal :min karakter'
        ]);

        $validator->after(function ($validator) use($request) {
            if ($request->input('new_password') != $request->input('confirm_password')) {
                $validator->errors()->add('confirm_password', 'Password baru dan konfirmasi password baru berbeda');
            }
        });

        if($validator->fails()) {
            return ApiResponse::responseData([
                'status' => false,
                'meta'   => [
                    'message' => implode(', ', $validator->errors()->all())
                ]
            ]);

        } else {
            $phoneNumber            = $request->input('phone_number');
            $verificationCode       = $request->input('verification_code');
            $verificationCodeHashed = md5($verificationCode);


            $activationCodes = ActivationCode::where('phone_number', $phoneNumber)
                            ->whereRaw('expired_at > NOW()')
                            ->where('for', 'forgot_password')
                            ->where('code_hashed', $verificationCodeHashed)
                            ->first();

            if ($activationCodes) {
                $passwordHash   = Hash::make($request->input('new_password'));
                $user           = User::where('phone_number', $phoneNumber)->where('is_owner','true')->first();
                $user->token    = $passwordHash;
                $user->password = $passwordHash;
                $user->save();

                if ($user) {
                    $activationCodes->deleted_at = Carbon::now();
                    $activationCodes->update();

                    return ApiResponse::responseData([
                        'status' => true,
                        'meta'   => [
                            'message' => 'Change password successfully',
                        ]
                    ]);

                } else {
                    return ApiResponse::responseData([
                        'status' => false,
                        'meta'   => [
                            'message' => 'Failed to change password'
                        ]
                    ]);

                }
            } else {
                return ApiResponse::responseData([
                    'status' => false,
                    'meta'   => [
                        'message'    => 'Your verification code is invalid',
                        'error_type' => 'invalid_code'

                    ]
                ]);

            }

        }
    }
}