<?php
namespace App\Http\Controllers\Api;

use App\Http\Helpers\ApiResponse;
use Illuminate\Http\Request;
use Exception;
use Bugsnag;

use App\Http\Helpers\ApiResponse as Api;
use App\Http\Helpers\ApiHelper;
use App\Http\Requests;
use App\Repositories\AppRepository;
use App\Entities\Device\UserDevice;
use Mail;
use App\Libraries\SMSLibrary;
use App\Http\Helpers\UtilityHelper;
use App\Entities\Activity\ChatAdmin;
use App\Entities\Device\DeviceHistory;

class AppController extends BaseController
{
    protected $repository;

    public function __construct(AppRepository $repository)
    {
        $this->repository = $repository;
    }

    public function tryUpdateAppVersionCode($appVersionCode)
    {
        try
        {
            $device = $this->device();
            if (is_null($device))
                return;

            if ($device->app_version_code != $appVersionCode)
            {
                $device->app_version_code = $appVersionCode;
                $device->save();

                DeviceHistory::store($device->id, $appVersionCode, 'update');
            }
        }
        catch (Exception $e)
        {
            Bugsnag::notifyException($e);
        }
    }

    public function getVersion(Request $request)
    {
        $this->validate($request, ['app_version_code' => 'required|numeric']);

        $currentUser = $this->user();
        $isOwner = ($currentUser != null && $currentUser->is_owner == 'true');        
        $adminId = $isOwner ? ChatAdmin::getDefaultAdminIdForOwner() : ChatAdmin::getRandomAdminIdForTenants();

        $owner = $this->repository->getInformationOwner($currentUser);  
        
        $popupPlayStore = true;
        if (in_array(date('D'), ['Sat', 'Sun'])) $popupPlayStore = false;

        $response = array( 'version'           => $this->repository->getAppVersion($request),
                        'web_version'       => "20160610",
                        'filter_version'    => "20190219", // updated by using release 1.0.2 release date
                        'chat_question'     => "20190814",
                        'title'             => '',
                        'is_native'         => true,
                        'cs_id'             => (string) $adminId,
                        'premium_buy'       => $owner,
                        'playstore_popup'   => $popupPlayStore
                    );

        $appVersionCode = $request['app_version_code'];
        $this->tryUpdateAppVersionCode($appVersionCode);
        
        if ($request->filled('v')) {
            if ($request->input('v') == '2') return $this->appVersionV2($response);
        }
      
        return Api::responseData($response);
    }

    public function appVersionV2($response)
    {
        $maps_popup = (object) [
                "title"     => "Barang Bekas di Kost JADI DUIT ! ASIK !",
                "content"   => "Sekarang di Mamikos kamu bisa jual barang bekas di Kost kamu loh !",
                "image"     => UtilityHelper::popupImagesRandom(),
                "type"      => "marketplace"
            ];
            
        return Api::responseData([
            'version' => $response['version'],
            'web_version'       => $response['web_version'],
            'filter_apt_version' => "20171228",
            'filter_kst_version' => "20190219", // updated by using release 1.0.2 release date
            'filter_mp_version'  => "20180122",
            'chat_question'     => $response['chat_question'],
            'title'             => '',
            'is_native'         => $response['is_native'],
            'cs_id'             => $response['cs_id'],
            'premium_buy'       => $response['premium_buy'],
            'playstore_popup'   => $response['playstore_popup'],
            'map_popup'         => $maps_popup
        ]);
    }

    public function getTotal(Request $request)
    {
        return Api::responseData(array('total' => $this->getCountUser()));
    }

    /**
     * @param Request $request
     */
    public function putDevice(Request $request)
    {
        ApiHelper::isValidInput(
            $request->all(),
            array(
                'app_notif_token'   => 'required',
                'app_version_code'  => 'required|numeric',
            )
        );

        if (! $this->device()) {
            return Api::responseData(array('device' => null));
        }

        return Api::responseData(
            array(
                'device' => UserDevice::patch(
                    $this->device()->id,
                    'device_id',
                    $request->input('app_notif_token'),
                    $request->input('app_version_code')
                ),
            )
        );
    }


    /**
     * @param Request $request
     */
    public function putDeviceOld(Request $request)
    {
        ApiHelper::isValidInput(
            $request->all(),
            array(
                'app_notif_token'   => 'required|alpha_dash',
                'app_version_code'  => 'required|numeric',
            )
        );

        return ApiHelper::responseData(
            array(
                'device' => UserDevice::patch(
                    $this->user()->id,
                    'user_id',
                    $request->input('app_notif_token'),
                    $request->input('app_version_code')
                ),
            )
        );
    }

    public function postCampaign(Request $request)
    {
        $this->repository->savePostCampaign($request);

        return Api::responseData(array('success' => true));
    }

    public function getIsDeviceLastLogin(Request $request)
    {
        $this->validate($request, array(
                'device_identifier' => 'required|alpha_num',
                'device_platform'   => 'required|in:android,ios',
            ));

        return ApiHelper::responseData(
            array(
                'is_device_last_login' => UserDevice::isLastLogin(
                    $this->user()->id,
                    $request->input('device_identifier'),
                    $request->input('device_platform')
                ),
            )
        );
    }

    public function putDeviceLastLogin(Request $request)
    {
        $userLastLogin = $this->repository->setLastLogin($request, auth()->user);

        return Api::responseData(array('device' => $userLastLogin));
    }

    public function getPopup(Request $request)
    {
        $popUp = $this->repository->latestAndEnabledPopup(auth()->user());

        return ApiResponse::responseData(array('popup' => $popUp));
    }

    public function putDisablePopup(Request $request)
    {
        $popUp = $this->repository->setEnableAppPopup(auth()->user(), 'false');

        return Api::responseData(array('popup' => $popUp));
    }

    public function putNotifSetting(Request $request)
    {
        $deviceId = $this->device()->id;

        $app_notif_all = $request->input('all', true);
        $app_notif_by_reference = $request->input('status_changed', true);
        $app_notif_status_changed = $request->input('by_reference', true);

        if ($app_notif_all === true) {
            $app_notif_all = "true";
        } elseif ($app_notif_all === false){
            $app_notif_all = "false";
        }

        if ($app_notif_status_changed === true) {
            $app_notif_status_changed = "true";
        } elseif ($app_notif_status_changed === false){
            $app_notif_status_changed = "false";
        }

        if ($app_notif_by_reference === true) {
            $app_notif_by_reference = "true";
        } elseif ($app_notif_by_reference === false){
            $app_notif_by_reference = "false";
        }

        $userDevice = UserDevice::find($deviceId);

        $userDevice->app_notif_all = $app_notif_all;
        $userDevice->app_notif_status_changed = $app_notif_status_changed;
        $userDevice->app_notif_by_reference = $app_notif_by_reference;

        $result = $userDevice->save();

        return ApiResponse::responseData(['result' => $result]);
    }

    public function sendDownloadLink(Request $request)
    {
        ApiHelper::isValidInput(
            $request->all(),
            array(
                'user_email'   => 'required|email'
            )
        );

        $user_email = $request->input('user_email');

        Mail::send('emails.download-app', [], function($message) use ($user_email)
        {
            $message->to($user_email)->subject('Unduh Aplikasi Mobile MamiKos');
        });

        if($request->input('user_phone') != '') {
            $smsMessage = "Klik https://mamikos.com/app/android untuk Android atau https://mamikos.com/app/ios untuk iOS";
            SMSLibrary::send(SMSLibrary::phoneNumberCleaning($request->input('user_phone')), $smsMessage);
        }

        return ApiResponse::responseData(['status'=>true]);
    }
}
