<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;

class BaseController extends Controller
{
    protected $device = null;
    protected $user = null;

    public function __construct()
    {

    }

    protected function user()
    {
        // Temporary solution if API has no device token,
        // then it should try to use session instead.
        // So the web will be able to make api call
        // For the future, we will convert this to use native Laravel authentication using guard.
        $user = app()->user;

        if(is_null($user)) {
            $user = Auth::user();
        }

        return $user;
    }

    protected function device()
    {
        return app()->device;
    }
}
