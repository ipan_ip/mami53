<?php

namespace App\Http\Controllers\Api;

use App\Http\Helpers\ApiResponse as Api;
use App\Presenters\Notification\CategoryPresenter;
use App\Presenters\Notification\NotificationPresenter;
use App\Repositories\Notification\NotificationRepository;

use Illuminate\Http\Request;

class NotificationController extends BaseController
{
    private $repository;
    private $defaultCountResponse = ['category_id' => 0, 'unread' => 0];

    public function __construct(NotificationRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $failedAuthStatus = $this->checkFailedAuthStatus();
        if (!is_null($failedAuthStatus)) {
            return Api::responseData(['status' => $failedAuthStatus]);
        }

        $this->repository->setPresenter(NotificationPresenter::class);
        $limit = (int) $request->get('limit', 10);
        $categoryId = (int) $request->post('category', 0);
        $notifications = $this->repository->getByUser($this->user, $categoryId, $limit);
        $this->repository->setAsRead($this->user, $categoryId);

        $response = $this->transformDataPagination($notifications);
        return Api::responseData($response);
    }

    public function categories(Request $request)
    {
        $this->repository->setPresenter(CategoryPresenter::class);
        $limit = (int) $request->get('limit', 10);
        $categories = $this->repository->categoryList($limit);
        $categoryAll = [
            'id' => 0,
            'name' => 'All',
            'icon' => '',
            'order' => 0
        ];
        array_unshift($categories['data'], $categoryAll);

        $response = $this->transformDataPagination($categories);
        return Api::responseData($response);
    }

    public function counter()
    {
        $failedAuthStatus = $this->checkFailedAuthStatus();
        if (!is_null($failedAuthStatus)) {
            return Api::responseData(['status' => $failedAuthStatus]);
        }

        $counter = $this->repository->getUserUnreadCount($this->user);
        $data = [$this->defaultCountResponse];
        if (count($counter)) {
            $data = $counter->toArray();
            $totalCount = 0;
            $index = 0;
            foreach ($data as $count) {
                $totalCount += $count['unread'];
                if ($count['category_id'] === 0) {
                    unset($data[$index]);
                }
                $index++;
            }
            $countAll = [
                'category_id' => 0,
                'unread' => $totalCount
            ];
            array_unshift($data, $countAll);
        }
        $response = [
            'data' => $data
        ];
        return Api::responseData($response);
    }

    public function read(Request $request)
    {
        $failedAuthStatus = $this->checkFailedAuthStatus();
        if (!is_null($failedAuthStatus)) {
            return Api::responseData(['status' => $failedAuthStatus]);
        }

        $categoryId = (int) $request->post('category', 0);
        $this->repository->setAsRead($this->user, $categoryId);

        $response = [
            'data' => $this->defaultCountResponse
        ];
        return Api::responseData($response);
    }

    private function checkFailedAuthStatus(): ?bool
    {
        $this->user = $this->user();
        $this->device = $this->device();
        if (is_null($this->user)) {
            // workaround requested by mobile team, app will crash if status=false
            if (!is_null($this->device)) {
                return true;
            }
            return false;
        }
        return null;
    }

    private function transformDataPagination($data)
    {
        $pagination = $data['meta']['pagination'];
        $transformedData = [
            'data' => $data['data'],
            'has_next' => ($pagination['current_page'] < $pagination['total_pages']),
            'current_page' => $pagination['current_page'],
            'limit' => $pagination['per_page']
        ];

        return $transformedData;
    }
}
