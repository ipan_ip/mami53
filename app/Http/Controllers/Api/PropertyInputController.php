<?php
namespace App\Http\Controllers\Api;

use App\Http\Helpers\ApiResponse;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Helpers\ApiHelper;
use App\Repositories\PropertyInputRepository;
use Validator;
use Mail;
use App\Presenters\RoomPresenter;
use App\Entities\Room\Room;
use App\Entities\Media\Media;
use App\Entities\Room\Element\Price;
use App\Entities\Room\Element\Tag;
use App\Entities\User\UserInputReward;
use App\Entities\Premium\PremiumPackage;
use Jenssegers\Agent\Agent;
use App\Entities\Activity\Tracking;
use App\Entities\Refer\Referrer;
use App\Entities\User\History;
use App\Entities\Marketplace\MarketplaceProduct;
use App\Entities\Owner\SurveySatisfaction;
use App\Entities\Activity\TrackingFeature;
use Cache;
use App\Entities\Generate\Currency;
use App\Entities\Premium\OwnerFreePackage;
use App\Entities\Activity\PhotoBy;
use App\Entities\Owner\Activity;
use App\Entities\Owner\ActivityType;
use App\Entities\Mamipay\MamipayOwner;
use App\Http\Helpers\RegexHelper;
use App\Repositories\Room\RoomUnitRepository;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Room\RoomOwner;
use App\Jobs\Owner\MarkActivity;
use Carbon\Carbon;

class PropertyInputController extends BaseController
{
	const MAX_ROOM_COUNT = 500;

	protected $kostValidationMessages = [
		'input_as.required'=>'Tipe penginput data harus diisi',
		'input_as.in'=>'Tipe penginput tidak valid',
		'latitude.required'=>'Latitude harus diisi',
		'longitude.required'=>'Longitude harus diisi',
		'address.max'=>'Alamat maksimal :mas karakter',
		'name.required'=>'Nama Kost harus diisi',
		'name.min'=>'Nama kost minimal :min karakter',
		'name.max'=>'Nama kost maksimal :max karakter',
		'gender.required'=>'Tipe penghuni kost harus diisi',
		'gender.in'=>'Tipe penghuni kost tidak sesuai',
		'room_size.required'=>'Luas kamar harus diisi',
		'room_size.max'=>'Luas kamar maksimal :max karakter',
		'room_available.required'=>'Jumlah kamar kosong harus diisi',
		'room_available.required_unless'=>'Jumlah kamar kosong harus diisi',
		'room_available.integer'=>'Jumlah kamar kosong harus berupa angka',
		'room_count.required'=>'Jumlah Kamar harus diisi',
		'room_count.required_unless'=>'Jumlah Kamar harus diisi',
		'room_count.required_if'=>'Jumlah Kamar harus diisi',
		'room_count.integer'=>'Jumlah Kamar harus berupa angka',
		'room_count.lte' => 'Jumlah kamar tidak bisa lebih dari ' . self::MAX_ROOM_COUNT,
		'price_daily.numeric'=>'Harga harian harus berupa angka',
		'price_monthly.required'=>'Harga bulanan harus diisi',
		'price_monthly.numeric'=>'Harga bulanan harus berupa angka',
		'price_weekly.numeric'=>'Harga mingguan harus berupa angka',
		'price_yearly.numeric'=>'Harga tahunan harus berupa angka',
		'fac_property.required'=>'Fasilitas kost harus diisi',
		'fac_room.required'=>'Fasilitas kamar harus diisi',
		'fac_bath.required'=>'Tipe kamar mandi harus diisi',
		'price_remark.max'=>'Keterangan Harga maksimal :max karakter',
		'remarks'=>'Keterangan Lain maksimal :max karakter',
		'description.max'=>'Deskripsi maksimal :max karakter',
		'agent_name.required'=>'Nama Penginput harus diisi',
		'agent_name.max'=>'Nama Agen maksimal :max karakter',
		'agent_phone.required'=>'Nomor Telepon Penginput harus diisi',
		'agent_phone.max'=>'Nomor Telepon Agen maksimal :max karakter',
		'agent_phone.regex' => 'Format Nomor Telepon Penginput tidak valid (08xxxxxxxx)',
		'agent_email.email'=>'Email Agen tidak valid',
		'agent_email.max'=>'Email Agen maksimal :max karakter',
		'owner_name'=>'required|min:3|max:100',
		'owner_name.required'=>'Nama pemilik/pengelola harus diisi',
		'owner_name.min'=>'Nama pemilik/pengelola minimal :min karakter',
		'owner_name.max'=>'Nama pemilik/pengelola maksimal :max karakter',
		'owner_name.required_if'=>'Nama Pemilik harus diisi',
		'owner_email.email'=>'Format email pemilik/pengelola tidak valid',
		'owner_phone.required_if'=>'Nomor Telepon pemilik harus diisi',
		'owner_phone.regex' => 'Format Nomor Telepon Pemilik tidak valid (08xxxxxxxx)',
		'manager_name.min' => 'Nama Pengelola minimal :min karakter',
		'manager_name.max' => 'Nama Pengelola maksimal :max karakter',
		'manager_phone.regex' => 'Format Nomor Telepon Pengelola tidak valid (08xxxxxxxx)',
		'password.required'=>'Password harus diisi',
		'password.min'=>'Password minimal :min karakter',
		'input_source.max'=>'Source Input maksimal :max karakter',
		'photos.cover.required' => 'Foto harus diisi',
		'youtube_link.url'=>'Link Youtube tidak valid',
		'youtube_link.max'=>'Link Youtube maksimal :max karakter'
	];

	protected $apartmentValidationMessages = [
		'input_as.required'=>'Tipe penginput data harus diisi',
		'input_as.in'=>'Tipe penginput tidak valid',
		'project_id.required_without'=>'Nama Apartment harus diisi',
		'project_name.required_without'=>'Nama Apartment harus diisi',
		'project_name.max'=>'Nama Apartment maksimal :max karakter',
		'unit_name.required'=>'Nama Unit harus diisi',
		'unit_name.max'=>'Nama Unit maksimal :max karakter',
		'unit_number.required' => 'Nomor Unit harus diisi',
		'unit_type.required'=>'Tipe Unit harus diisi',
		'unit_type_name.required_if' => 'Nama Tipe Unit harus diisi jika Tipe Unit adalah Lainnya',
		'unit_type_name.max'=>'Nama Tipe Unit maksimal :max karakter',
		'floor.required'=>'Lantai harus diisi',
		'floor.numeric'=>'Lantai harus berupa angka',
		'unit_size.required'=>'Luas Unit harus diisi',
		'unit_size.numeric'=>'Luas Unit harus berupa angka',
		'min_payment.required'=>'Minimal Pembayaran harus diisi',
		'min_payment.numeric'=>'Minimal Pembayaran harus berupa angka',
		'price_daily.numeric' => 'Harga Harian harus berupa angka',
		'price_weekly.numeric' => 'Harga Mingguan harus berupa angka',
		'price_monthly.required'=>'Harga Bulanan harus diisi',
		'price_monthly.numeric'=>'Harga Bulanan harus berupa angka',
		'price_yearly.numeric' => 'Harga Tahunan harus berupa angka',
		'price_shown.required' => 'Harga yang ditampilkan harus diisi',
		'maintenance_price.numeric'=>'Biaya Maintenance harus berupa angka',
		'parking_price.numeric'=>'Biaya Tambahan Parkir harus berupa angka',
		'description.max'=>'Deskripsi maksimal :max karakter',
		'fac_unit.required'=>'Fasilitas unit harus diisi',
		'is_furnished.required'=>'Pilihan Furnished harus diisi',
		'fac_room.required_unless'=>'Fasilitas Kamar harus diisi',
		'photos'=>'nullable',
		'owner_name.required'=>'Nama pemilik/pengelola harus diisi',
		'owner_name.min'=>'Nama pemilik/pengelola minimal :min karakter',
		'owner_name.max'=>'Nama pemilik/pengelola maksimal :max karakter',
		'owner_email.email'=>'Format email pemilik/pengelola tidak valid',
		'password.required'=>'Password harus diisi',
		'password.min'=>'Password minimal :min karakter',
		'input_source.max'=>'Source Input maksimal :max karakter',
		'youtube_link.url'=>'Link Youtube tidak valid',
		'youtube_link.max'=>'Link Youtube maksimal :max karakter'
	];

	protected $kostSimpleValidationMessage = [
		'address.max'=>'Alamat maksimal :mas karakter',
		'name.required'=>'Nama Kost harus diisi',
		'name.min'=>'Nama kost minimal :min karakter',
		'name.max'=>'Nama kost maksimal :max karakter',
		'owner_phone.required'=>'Nomor Telepon pemilik harus diisi',
		'owner_phone.regex' => 'Format Nomor Telepon pemilik tidak valid (08xxxxxxxx)',
		'photos.required' => 'Foto harus diisi',
		'photos.cover.required' => 'Foto bangunan depan harus diisi',
		'agent_name.required' => 'Nama Penginput harus diisi',
		'agent_name.max' => 'Nama Penginput maksimal :max karakter',
		'agent_phone.required' => 'Nomor Telepon Penginput harus diisi',
		'agent_phone.max' => 'Nomor Telepon Penginput maksimal :max karakter',
		'agent_phone.regex' => 'Nomor Telepon Penginput tidak valid (08xxxxxxxx)',
		'review.required' => 'Review tidak boleh kosong',
		'referral_code.required' => 'Kode referral tidak boleh kosong',
		'latitude.required'=>'Latitude harus diisi',
		'longitude.required'=>'Longitude harus diisi',
		'price.required' => 'Harga tidak boleh kosong',
		'input_source.max'=>'Source Input maksimal :max karakter'
	];

	protected $dataStatuses = [
		'add', 
		'draft', 
		'draft1', 
		'draft2', 
		'verified', 
		'unverified'
	];

	protected $repository;

	public function __construct(PropertyInputRepository $repository)
	{
		$this->repository = $repository;
	}
	
	/**
	 * Validate inconsistence conditions flow in backend
	 * 
	 * @param int $autoBbk
	 * 
	 * @return bool
	 */
	private function isValidInputAutoBbk(int $autoBbk): bool
	{
		$user = $this->user();

		//Define is user mamipay
		$isMamipay = MamipayOwner::where('user_id', $user->id)
			->where('status', MamipayOwner::STATUS_APPROVED)->exists();

		return ($autoBbk === 1 && false === $isMamipay) ? false : true;
	}

	/**
	 * Main entry to input kost process
	 *
	 * @param Request $request 			Laravel Request
	 *
	 * @return Array 	API response
	 */
	public function inputKost(Request $request)
	{
		$user = $this->user();

        //Check is user data is empty?
        if (empty($user)) {
            return ApiResponse::responseError(
                false,
                false,
                401,
                401
            );
        }

		$request->merge(array_map('trim', $request->all('agent_email', 'owner_email')));

		// form validation
		$validator = Validator::make($request->all(), [
				'input_as'      => 'required|in:Pemilik Kos,Pengelola Kos,Anak Kos,Agen,Lainnya',
				'latitude'      => 'required',
				'longitude'     => 'required',
				'city'          => 'nullable',
				'subdistrict'   => 'nullable',
				'address'       => 'nullable|max:250',
				'name'          => 'required|min:5|max:250',
				'gender'        => 'required|in:0,1,2',
				'room_size'     => 'required|max:50',
				'room_available'=> 'required_unless:input_as,Anak Kos,Lainnya|integer',
				'room_count'    => 'required_unless:input_as,Anak Kos,Lainnya|integer|lte:' . self::MAX_ROOM_COUNT,
				'price_daily'   => 'nullable|numeric',
				'price_monthly' => 'required|numeric',
				'price_weekly'  => 'nullable|numeric',
				'price_yearly'  => 'nullable|numeric',
				'fac_property'  => 'nullable',
				'fac_room'      => 'nullable',
				'fac_bath'      => 'required',
				'price_remark'  => 'max:500',
				'remarks'       => 'max:1000',
				'description'   => 'max:1000',
				'photos'        => 'nullable',
				'youtube_link'	=> 'url|max:300',
				'manager_name'	=> 'nullable|min:3|max:100',
				'manager_phone'	=> 'nullable',
				'owner_name'    => 'required_if:input_as,Anak Kos,Lainnya|min:3|max:100',
				'owner_phone'   => 'required_if:input_as,Anak Kos,Lainnya',
				'owner_email'   => 'nullable|email',
				'password'      => 'nullable|min:6',
				'agent_email'   => 'email|max:50',
				'agent_name'    => 'max:50',
				'agent_phone'   => 'max:50',
				'input_source'	=> 'max:190',
				'autobbk'		=> 'nullable|in:1,0'
			], $this->kostValidationMessages);

		$autoBbk = (int) $request->get('autobbk', 0);

		$validator->after(function($validator) use ($request, $autoBbk)
        {
			if (false === $this->isValidInputAutoBbk($autoBbk)) {
				$validator->errors()->add('autobbk', __('api.auto_bbk.error.invalid_autobbk_activation'));
			}

            if ( (int)$request->price_daily != 0 && strlen($request->price_daily) < 4 )
            {
                $validator->errors()->add('price_daily', 'Harga Sewa Harian harus minimal 4 karakter');
            }

            if ( (int)$request->price_weekly != 0 && strlen($request->price_weekly) < 4 )
            {
                $validator->errors()->add('price_weekly', 'Harga Sewa Mingguan harus minimal 4 karakter');
            }

            if ( (int)$request->price_monthly != 0 && strlen($request->price_monthly) < 4 )
            {
                $validator->errors()->add('price_monthly', 'Harga Sewa Bulanan harus minimal 4 karakter');
            }

            if ( (int)$request->price_yearly != 0 && strlen($request->price_yearly) < 4 )
            {
                $validator->errors()->add('price_yearly', 'Harga Sewa Tahunan harus minimal 4 karakter');
            }
        });

		$need_password = false;

		$validator->after(function($validator) use ($request, $user, &$need_password) {
			// make sure no duplicate name
			$nameExist = Room::where('name', $request->name)->first();
			if($nameExist) {
				$validator->errors()->add('name', 'Nama kost telah digunakan sebelumnya');
			}

			if(!in_array($request->get('input_as'), ['Anak Kos', 'Agen', 'Lainnya'])) {
				if(!is_null($user)) {
					if($user->is_owner == 'false') {
						$validator->errors()->add('owner_name', 'Anda harus menjadi owner untuk bisa menambah data');
					}

					// if user password empty then assume that this user register with auto register
					if(is_null($user->password) && $request->owner_name == '') {
						$need_password = true;
						$validator->errors()->add('owner_name', 'Nama Pemilik harus diisi');
					}

					if(is_null($user->password) && $request->password == '') {
						$need_password = true;
						$validator->errors()->add('password', 'Password harus diisi');
					}
				} else {
					$validator->errors()->add('input_as', 'Anda harus login untuk menambah data kost');
				}
			}

			$roomCount = $request->get('room_count');
			$roomAvailable = $request->get('room_available');
			
            if ($roomCount < $roomAvailable) {
            	$validator->errors()->add('room_count', 'Jumlah kamar harus lebih besar dari jumlah kamar tersedia');
            }
		});

		if($validator->fails()) {
			$response = [
				'status'=>false,
                'messages'=>$validator->errors()->all()
			];

			if($need_password) {
				$response['need_password'] = true;
			}

			return ApiResponse::responseData($response);
		}

		// get referer of the source, check if there is campaign available
        $campaignSource = '';
        if(isset($_SERVER['HTTP_REFERER'])) {
        	parse_str(parse_url($_SERVER['HTTP_REFERER'], PHP_URL_QUERY), $refererQs);
        	if(isset($refererQs['utm_source'])) {
        		$campaignSource = $refererQs['utm_source'];
        	} elseif(isset($_COOKIE['campaign_source'])) {
        		$campaignSource = $_COOKIE['campaign_source'];
        	}
        }
        
        $agent = new Agent();
        $add_from = Tracking::checkDevice($agent);

		$params = [
			'input_as'=>$request->input_as,
			'name'=>$request->name,
			'address'=>$request->address,
			'latitude'=>$request->latitude,
			'longitude'=>$request->longitude,
			'gender'=>$request->gender,
			'room_available'=> isset($request->room_available) ? $request->room_available : 0,
			'room_count'=>$request->room_count,
			'city'=>$request->city,
			'subdistrict'=>$request->subdistrict,
			'price_daily'=>$request->price_daily,
			'price_weekly'=>$request->price_weekly,
			'price_monthly'=>$request->price_monthly,
			'price_yearly'=>$request->price_yearly,
			'price_quarterly' => $request['3_month'],
			'price_semiannually' => $request['6_month'],
			'room_size'=>$request->room_size,
			'photos'=>$request->photos,
			'youtube_link'=>$request->youtube_link,
			'fac_property'=>!is_null($request->fac_property) ? $request->fac_property : [],
			'fac_room'=>!is_null($request->fac_room) ? $request->fac_room : [],
			'fac_bath'=>$request->fac_bath,
			'price_remark'=>$request->price_remark,
			'remarks'=>$request->remarks,
			'description'=>$request->description,
			'manager_name'=>$request->manager_name,
			'manager_phone'=>$request->manager_phone,
			'owner_name'=>$request->owner_name,
			'owner_email'=>$request->owner_email,
			'owner_phone'=>$request->owner_phone,
			'password'=> !is_null($request->password) ? $request->password : '',
			'agent_name'=>$request->agent_name,
			'agent_email'=>$request->agent_email,
			'agent_phone'=>$request->agent_phone,
			'add_from'   => $add_from,
			'input_source'=>$request->input_source,
			'campaign_source' => $campaignSource,
			'building_year' => $request->building_year,
			'autobbk' => $autoBbk,
		];

		// save new kost
		$newRoom = $this->repository->saveNewKost($user, $params);

		if ($request->input('input_as') == 'Agen' AND $request->filled('selfie_photo') AND $request->input('selfie_photo') != 0) {
			PhotoBy::updateTo($request->input('selfie_photo'), $newRoom->id);
		}

		if ($request->filled('checkin') AND !is_null($user)) {
			if ($request->input('checkin') == true) History::autoCheckIn($newRoom, $user);
		}

		// re-load the room since, the data returned above is not complete yet
		$newRoom = Room::with(['tags.photo', 'tags.photoSmall'])->find($newRoom->id);
		
		// set presenter
		$roomData = (new RoomPresenter('after-input'))->present($newRoom);

		// add dialog message
		$dialogMessage = '';
		$trialPackage = null;
		if($request->get('input_as') != 'Anak Kos' && $request->get('input_as') != 'Lainnya') {
			$dialogMessage = "Terima kasih telah mendaftarkan kost Anda di MamiKos!";
			if(empty($newRoom->cards)) {
				$dialogMessage .= "\nLengkapi foto kost agar iklan bisa segera tayang";
			}

			if(!is_null($user) && $user->is_owner == 'true' && count($user->premium_request()->get()) <= 0) {
				// count room ownership
				if(count($user->room_owner()->get()) == 1) {
					$trial = PremiumPackage::getActiveTrial();

					if($trial) {
						$trialPackage = [
							'id'              => $trial->id,
							'name'            => $trial->name,
							'price'           => 'Rp. ' . number_format($trial->price, 0, '.', ','),
							'sale_price'      => 'Rp. ' . number_format($trial->sale_price, 0, '.', '.'),
							'bonus'           => $trial->bonus,
							'days_count'      => $trial->total_day,
							'views'           => $trial->view
						];
					}
				}
			}
		} else {
			if($params['input_source'] == 'input-scoreboard') {
				$dialogMessage = 'Selamat kamu mendapat 2 point! Kami akan memverifikasi data yang kamu submit. Setelah terverifikasi, point akan kami rubah menjadi 10 point. Semangat ya!!';
			} else {
				$dialogMessage = 'Kami akan memverifikasi data yang kamu submit. Setelah terverifikasi, kami akan mengirimkan pulsa sebesar Rp. 25.000 untukmu.';
			}
		}
		
		$promoInputNeeded = false;
		// invalidate campaign source cookie
		if(isset($_COOKIE['campaign_source'])) {
			setcookie('campaign_source', '', time() - 3600, '/');
		}

		return ApiResponse::responseData([
			'data'=>[
				'room'=>$roomData['data'],
				'dialog_message'=>$dialogMessage,
				'promo_input' => $promoInputNeeded,
				'trial_package' => $trialPackage
			],
		]);
	}

	/**
	 * Update kost process
	 *
	 * @param Request $request
	 * @param RoomUnitRepository $roomUnitRepository
	 * @param int $songId
	 *
	 */
	public function updateKost(Request $request, RoomUnitRepository $roomUnitRepository, $songId)
	{
		// must be an owner
		$user = $this->user();

		if(!$user) {
			$response = [
				'status'=>false,
                'messages'=>['Silakan login terlebih dahulu']
			];

			return ApiResponse::responseData($response);
		}

		$request->merge(array_map('trim', $request->all('agent_email')));

		$room = Room::with(['owners', 'owners.user','tags.photo', 'tags.photoSmall', 'cards' => function($q) {
			$q->where('type', '<>', 'video');
		}])
					->where('song_id', $songId)
					->first();

		// check if room found
		if(!$room) {
			$response = [
				'status'=>false,
                'messages'=>['Kamar tidak ditemukan']
			];

			return ApiResponse::responseData($response);
		}

		$owner = null;
		// check if user is the owner of this room
		if(count($room->owners) <= 0 || is_null($room->owners)) {
			$response = [
				'status'=>false,
                'messages'=>['Anda bukan pemilik dari kost ini']
			];

			return ApiResponse::responseData($response);
		} else {
			foreach($room->owners as $roomOwner) {
				if(in_array($roomOwner->status, ['add', 'edited', 'draft', 'draft1', 'draft2', 'verified', 'unverified'])) {
					$owner = $roomOwner;
					break;
				}
			}
			
            if (!($owner instanceof RoomOwner) || $owner->user_id != $user->id) {
                $response = [
                    'status' => false,
                    'messages' => ['Anda bukan pemilik dari kost ini']
                ];

                return ApiResponse::responseData($response);
            }
		}

		// form validation
		$validator = Validator::make($request->all(), [
				// 'input_as'=>'required|in:Pemilik Kos,Pengelola Kos,Anak Kos',
				'latitude'=>'required',
				'longitude'=>'required',
				'city'=>'nullable',
				'subdistrict'=>'nullable',
				'address'=>'max:250',
				'name'=>'required|min:5|max:250',
				'gender'=>'required|in:0,1,2',
				'room_size'=>'required|max:50',
				'room_available'=>'required|integer',
				'room_count'=>'required|integer|lte:' . self::MAX_ROOM_COUNT,
				'price_daily'=>'nullable|numeric',
				'price_monthly'=>'required|numeric',
				'price_weekly'=>'nullable|numeric',
				'price_yearly'=>'nullable|numeric',
				'price_remark'=>'max:500',
				'remarks'=>'max:1000',
				'description'=>'max:1000',
				'fac_property'=>'nullable',
				'fac_room'=>'nullable',
				'fac_bath'=>'required',
				'concern_ids'=>'nullable',
				'photos'=>'nullable',
				'youtube_link'	=> 'url|max:300',
				'owner_name'=>'nullable|max:100',
				'owner_phone'=> 'nullable',
				// 'owner_email'=>'nullable|email',
				// 'password'=>'nullable|min:6'
				'manager_name'=> 'nullable|max:100',
				'manager_phone' => 'nullable',
				'agent_email'=>'email|max:50',
				'agent_name'=>'max:50',
				'agent_phone'=>'max:50'
			], $this->kostValidationMessages);

		$validator->after(function($validator) use ($request)
        {
            if ( (int)$request->price_daily != 0 && strlen($request->price_daily) < 4 )
            {
                $validator->errors()->add('price_daily', 'Harga Sewa Harian harus minimal 4 karakter');
            }

            if ( (int)$request->price_monthly != 0 && strlen($request->price_monthly) < 4 )
            {
                $validator->errors()->add('price_monthly', 'Harga Sewa Bulanan harus minimal 4 karakter');
            }

            if ( (int)$request->price_weekly != 0 && strlen($request->price_weekly) < 4 )
            {
                $validator->errors()->add('price_weekly', 'Harga Sewa Mingguan harus minimal 4 karakter');
            }

            if ( (int)$request->price_yearly != 0 && strlen($request->price_yearly) < 4 )
            {
                $validator->errors()->add('price_yearly', 'Harga Sewa Tahunan harus minimal 4 karakter');
            }
        });

		$flashSale = $room->getFlashSaleRentType();
		$originalPrice = $room->getRealPrices();
		$validator->after(function($validator) use ($request, $room, $flashSale, $originalPrice)
		{
			if ($flashSale[Price::STRING_DAILY] && $request->price_daily != $originalPrice['price_daily']) {
				$validator->errors()->add('price_daily', 'Promo ngebut aktif, tidak bisa update');
			} elseif ($flashSale[Price::STRING_WEEKLY] && $request->price_weekly != $originalPrice['price_weekly']) {
				$validator->errors()->add('price_weekly', 'Promo ngebut aktif, tidak bisa update');
			} elseif ($flashSale[Price::STRING_MONTHLY] && $request->price_monthly != $originalPrice['price_monthly']) {
				$validator->errors()->add('price_monthly', 'Promo ngebut aktif, tidak bisa update');
			} elseif ($flashSale[Price::STRING_ANNUALLY] && $request->price_yearly != $originalPrice['price_yearly']) {
				$validator->errors()->add('price_yearly', 'Promo ngebut aktif, tidak bisa update');
			} elseif ($flashSale[Price::STRING_QUARTERLY] && $request['3_month'] != $originalPrice['price_3_month']) {
				$validator->errors()->add('3_month', 'Promo ngebut aktif, tidak bisa update');
			} elseif ($flashSale[Price::STRING_SEMIANNUALLY] && $request['6_month'] != $originalPrice['price_6_month']) {
				$validator->errors()->add('6_month', 'Promo ngebut aktif, tidak bisa update');
			}
		});

		$validator->after(function($validator) use ($request, $user, $songId, $room) {
			// make sure no duplicate name
			$nameExist = Room::where('name', $request->name)
							->where('song_id', '<>', $songId)
							->first();

			if($nameExist) {
				$validator->errors()->add('name', 'Nama kost telah digunakan sebelumnya');
			}

			if(!is_null($user)) {
				if($user->is_owner == 'false') {
					$validator->errors()->add('owner_name', 'Anda harus menjadi owner untuk bisa merubah data');
				}
			} else {
				$validator->errors()->add('input_as', 'Anda harus login untuk menambah data kost');
			}

			if($request->owner_phone != '0' && $request->owner_phone != '-' && !is_null($request->owner_phone) && $request->owner_phone != '') {
				$owner = trim(preg_replace("/[^0-9,.]/", "", $request->owner_phone));
				if(!preg_match(RegexHelper::phoneNumberFormat(), $owner)) {
					$validator->errors()->add('owner_phone', 'Format Nomor Telepon Pemilik tidak valid (08xxxxxxxx)');
				}
			}
			
			if($request->manager_phone != '0' && $request->manager_phone != '-' && !is_null($request->manager_phone) && $request->manager_phone != '') {
				$manager = trim(preg_replace("/[^0-9,.]/", "", $request->manager_phone));
				if(!preg_match(RegexHelper::phoneNumberFormat(), $manager)) {
					$validator->errors()->add('manager_phone', 'Format Nomor Telepon Pengelola tidak valid (08xxxxxxxx)');
				}
			}

			if ($request->room_count < $request->room_available) {
            	$validator->errors()->add('room_count', 'Jumlah kamar harus lebih besar dari jumlah kamar tersedia');
			} else {
				$activeContract         = $room->getBookedAndActiveContract();
                $activeContractCount    = $activeContract->count();
				if ($request->room_available > ($request->room_count - $activeContractCount)) {
					$validator->errors()->add('room_available', 'Total kamar terisi tidak boleh kurang dari total kamar dengan kontrak.');
				}
			}
		});

		if($validator->fails()) {
			$response = [
				'status'=>false,
                'messages'=>$validator->errors()->all()
			];

			return ApiResponse::responseData($response);
		}

		$params = [
			// 'input_as'=>$request->input_as,
			'name' => $request->name,
			'address' => $request->address,
			'latitude' => $request->latitude,
			'longitude' => $request->longitude,
			'gender' => $request->gender,
			'room_available' => $request->room_available,
			'room_count' => $request->room_count,
			'city' =>$request->city,
			'subdistrict' => $request->subdistrict,
			'price_daily' => $request->price_daily,
			'price_weekly' => $request->price_weekly,
			'price_monthly' => $request->price_monthly,
			'price_yearly' => $request->price_yearly,
			'price_quarterly' => $request['3_month'],
			'price_semiannually' => $request['6_month'],
			'price_remark' => $request->price_remark,
			'remarks' => $request->remarks,
			'description' => $request->description,
			'room_size' => $request->room_size,
			'photos' => $request->photos,
			// 'youtube_link'=>$request->filled('youtube_link') ? $request->youtube_link : false,
			'fac_property' => !is_null($request->fac_property) ? $request->fac_property : [],
			'fac_room' => $request->fac_room,
			'fac_bath' => !is_null($request->fac_bath) ? $request->fac_bath : [],
			'concern_ids' => !is_null($request->concern_ids) ? $request->concern_ids : [],
			'owner_name' => $request->owner_name,
			// 'owner_email'=>$request->input_as != 'Anak Kos' ? $request->owner_email : '',
			'owner_phone' => trim(preg_replace("/[^0-9,.]/", "", $request->owner_phone)),
			// 'password'=>$request->input_as != 'Anak Kos' ? $request->password : ''
			'manager_name' => $request->manager_name,
			'manager_phone' => trim(preg_replace("/[^0-9,.]/", "", $request->manager_phone)),
			'agent_name' => $request->agent_name,
			'agent_email' => $request->agent_email,
			'agent_phone' => $request->agent_phone,
			'building_year' => $request->building_year
		];

		if ($request->filled('youtube_link') || $request->youtube_link == '') {
			$params['youtube_link'] = $request->youtube_link;
		}

		// save new kost
		$roomCountOld = $room->room_count;
		$roomAvailableOld = $room->room_available;
		$updatedRoom = $this->repository->updateKostByOwner($room, $owner, $params);

		//trancking update
		TrackingFeature::insertTo(["type" => "room-update", "identifier" => $room->id]);

		$responseSurvey = ["status" => true, "show_survey" => false];
		if ($roomAvailableOld > 0 AND $request->room_available == 0) {
			$dataUpdate = [	"user_id" 		=> $user->id,
					 		"identifier" 	=> $room->id,
					 		"for" 			=> "update_room",
					 		"content" 		=> null
						  ];

			$responseSurvey = SurveySatisfaction::insertOrUpdate($dataUpdate);
		}

		MarkActivity::dispatch($user->id, ActivityType::TYPE_UPDATE_PROPERTY);
		// re-load the room since, the data returned above is not complete yet
		// $newRoom = Room::with(['tags.photo', 'tags.photoSmall'])->find($newRoom->id);
		
		// set presenter
		$roomData = (new RoomPresenter('after-input'))->present($updatedRoom);

		return ApiResponse::responseData([
			'data'=>[
				'room'			=> $roomData['data'],
				'dialog_message'=>'Edit Kost berhasil, silakan tunggu verifikasi admin (2x24 jam).',
				'show_survey' 	=> $responseSurvey["show_survey"],
				'activity'	    => [
					"logged_in_at" => Carbon::now()->toDateTimeString(),
					"updated_property_at" => Carbon::now()->toDateTimeString(),
				]
			],
		]);
		
	}

	/**
	 * Main entry to input apartment process
	 *
	 * @param Request $request 			Laravel request
	 *
	 * @return Array 		API response
	 */
	public function inputApartment(Request $request)
	{
		//dd(Cache::get("currencyusd"));
		// must be an owner
		$user = $this->user();

		$request->merge(array_map('trim', $request->all('owner_email')));

		if($user->is_owner == 'false') {
			return ApiResponse::responseData([
                     'status'=>false,
                     'messages'=>['Anda harus menjadi owner untuk bisa menambah data']
                 ]);
		}

		// form validation
		$validator = Validator::make($request->all(), [
				'input_as'=>'required|in:Pemilik Kos,Pengelola Kos,Agen,Pemilik Apartemen,Pengelola Apartemen,Agen Apartemen,Agen Kos',
				'project_id'=>'required_without:project_name',
				'project_name'=>'required_without:project_id|max:190',
				'unit_name'=>'required|max:250',
				'unit_number' => 'required',
				'unit_type'=>'required',
				'unit_type_name' => 'nullable|required_if:unit_type,Lainnya|max:100',
				'floor'=>'required|numeric',
				'unit_size'=>'required|numeric',
				'min_payment'=>'nullable|numeric',
				'price_daily' => 'numeric',
				'price_weekly' => 'numeric',
				'price_monthly'=>'numeric',
				'price_yearly' => 'numeric',
				'price_shown' => 'nullable',
				'maintenance_price'=>'nullable|numeric',
				'parking_price'=>'nullable|numeric',
				'description'=>'nullable|max:900',
				'fac_unit'=>'required',
				'is_furnished'=>'required',
				'fac_room'=>'required_if:is_furnished,1',
				'photos'=>'nullable',
				'youtube_link'	=> 'url|max:300',
				'owner_name'=>'nullable|min:4|max:100',
				'owner_email'=>'nullable|email',
				'password'=>'nullable|min:6',
				'input_source'	=> 'max:190'
			], $this->apartmentValidationMessages);

		$validator->after(function($validator) use ($request)
        {
        	if (!$request->filled('price_weekly') AND !$request->filled('price_monthly') AND !$request->filled('price_daily') AND !$request->filled('price_yearly')) {
        		$validator->errors()->add('price', 'Harga sewa harus diisi salah satu');
			}

			$char_min = 4;
			if ($request->filled('price_type') AND $request->input('price_type') == 'usd') {
				$price_type = 'usd';
				$char_min = 1;
			}

			if ($request->filled('price_monthly') AND $request->input('price_monthly') == 0) $monthly = null;
			else $monthly = $request->input('price_monthly');

            if (!is_null($monthly) AND strlen($request->input('price_monthly')) < $char_min) {
                $validator->errors()->add('price_monthly', 'Harga Sewa Bulanan harus minimal '.$char_min.' karakter');
            }

			if ($request->filled('price_daily') AND $request->input('price_daily') == 0) $daily = null;
			else $daily = $request->input('price_daily');

            if ( !is_null($daily) AND strlen($request->input('price_daily')) < $char_min ) {
                $validator->errors()->add('price_daily', 'Harga Sewa Harian harus minimal '.$char_min.' karakter');
            }
			
			if ($request->filled('price_weekly') AND $request->input('price_weekly') == 0) $weekly = null;
			else $weekly = $request->input('price_weekly');

            if ( !is_null($weekly) AND strlen($request->input('price_weekly')) < $char_min ) {
                $validator->errors()->add('price_weekly', 'Harga Sewa Mingguan harus minimal '.$char_min.' karakter');
            }

			if ($request->filled('price_yearly') AND $request->input('price_yearly') == 0) $yearly = null;
			else $yearly = $request->input('price_yearly');

            if ( !is_null($yearly) AND strlen($request->input('price_yearly')) < $char_min) {
                $validator->errors()->add('price_yearly', 'Harga Sewa Tahunan harus minimal '.$char_min.' karakter');
			}
			
			if (is_null($monthly) AND is_null($yearly) AND is_null($daily) AND is_null($weekly)) {
				$validator->errors()->add('price', 'Harga sewa harus diisi salah satu');
			}

        });

		$need_password = false;

		$validator->after(function($validator) use ($request, $user, &$need_password) {
			if(($request->project_id == 0 || is_null($request->project_id)) && ($request->project_name == '' || is_null($request->project_name))) {
				$validator->errors()->add('project_name', 'Nama Apartment harus diisi');
			}

			if($request->project_id > 0) {
				$unitNumberExist = Room::where('apartment_project_id', $request->project_id)
										->where('unit_number', $request->unit_number)
										->first();

				if($unitNumberExist) {
					$validator->errors()->add('unit_number', 'Nomor Unit telah digunakan sebelumnya');
				}
			}

			// if user password empty then assume that this user register with auto register
			if(is_null($user->password) && $request->owner_name == '') {
				$need_password = true;
				$validator->errors()->add('owner_name', 'Nama Pemilik harus diisi');
			}

			if(is_null($user->password) && $request->password == '') {
				$need_password = true;
				$validator->errors()->add('password', 'Password harus diisi');
			}

			if(is_array($request->price_shown)) {
				foreach($request->price_shown as $priceShown) {
					$priceShownValue = 'price_' . $priceShown;
					if(is_null($request->$priceShownValue) || $request->$priceShownValue <= 0) {
						$validator->errors()->add('price_shown', 'Jumlah Harga yang akan ditampilkan harus diisi');
						break;
					}
				}
			}
		});

		// we don't need to validate the same name input because apartment could have same exact name

		if($validator->fails()) {
			$response = [
				'status'=>false,
                'messages'=>$validator->errors()->all()
			];

			if($need_password) {
				$response['need_password'] = true;
			}
			return ApiResponse::responseData($response);
		}

		// get the user agent of source
		$agent    = new Agent();
        $add_from = Tracking::checkDevice($agent);

        // get referer of the source, check if there is campaign available
        $campaignSource = '';
        if(isset($_SERVER['HTTP_REFERER'])) {
        	parse_str(parse_url($_SERVER['HTTP_REFERER'], PHP_URL_QUERY), $refererQs);
        	if(isset($refererQs['utm_source'])) {
        		$campaignSource = $refererQs['utm_source'];
        	} elseif(isset($_COOKIE['campaign_source'])) {
        		$campaignSource = $_COOKIE['campaign_source'];
        	}
        }
		$usdPrice = Cache::get("currencyusd");
		if (is_null($usdPrice)) {
			$usdPrice = Currency::where("name", "usd")->first()->exchange_rate;
			Cache::put("currencyusd", $usdPrice, 60 * 24);
		}
		
		if ($request->filled('price_type') AND $request->input('price_type') == 'usd') {
			$dailyPrice = $request->price_daily * $usdPrice;
			$weeklyPrice = $request->price_weekly * $usdPrice;
			$monthlyPrice = $request->price_monthly * $usdPrice;
			$yearlyPrice = $request->price_yearly * $usdPrice;

			$priceDailyUsd = $request->price_daily;
			$priceWeeklyUsd = $request->price_weekly;
			$priceMonthlyUsd = $request->price_monthly;
			$priceYearlyUsd = $request->price_yearly;

			$price_type = $request->input('price_type');
			
		}

		// FREE PACKAGE FROM BANNER
		if ($request->filled('banner_type') AND $request->filled('ids') AND in_array($request->input('banner_type'), OwnerFreePackage::TYPE)) {
			OwnerFreePackage::insertOrUpdateToDB(["type" => $request->input('banner_type'), 
												"ids" => $request->input('ids'),
												"user" => $this->user()
											]);
		}

		$params = [
			'input_as'     =>$request->input_as,
			'name'         =>$request->unit_name,
			'unit_number'  =>$request->unit_number,
			'price_daily'  => isset($dailyPrice) ? round($dailyPrice) : $request->price_daily,
			'price_weekly' => isset($weeklyPrice) ? round($weeklyPrice) : $request->price_weekly,
			'price_monthly'=> isset($monthlyPrice) ? round($monthlyPrice) : $request->price_monthly,
			'price_yearly' => isset($yearlyPrice) ? round($yearlyPrice) : $request->price_yearly,
			'price_daily_usd' =>  isset($priceDailyUsd) ? $priceDailyUsd : 0,
			'price_weekly_usd' => isset($priceWeeklyUsd) ? $priceWeeklyUsd : 0,
			'price_monthly_usd' => isset($priceMonthlyUsd) ? $priceMonthlyUsd : 0,
			'price_yearly_usd' => isset($priceYearlyUsd) ? $priceYearlyUsd : 0,
			'price_shown'  =>is_array($request->price_shown) ? implode('|', $request->price_shown) : null,
			'description'  =>$request->description,
			'floor'        =>$request->floor,
			'unit_type'    =>$request->unit_type,
			'unit_type_name'=>$request->unit_type_name,
			'unit_size'     =>$request->unit_size,
			'is_furnished'	=>$request->is_furnished,
			'fac_unit'      =>$request->fac_unit,
			'fac_room'      =>!is_null($request->fac_room) ? $request->fac_room : [],
			'min_payment'   =>!is_null($request->min_payment) ? [$request->min_payment] : [],
			'owner_name'    =>$request->owner_name,
			'owner_email'   =>$request->owner_email,
			'password'      =>$request->password,
			'photos'        =>$request->photos,
			'youtube_link'	=>$request->youtube_link,
			'add_from'      => $add_from,
			'campaign_source'=>$campaignSource,
			'input_source'	=>$request->input_source,
			'price_type' => isset($price_type) ? $price_type : 'idr',
		];

		if($request->filled('project_id') && $request->project_id != '' && $request->project_id != 0 && !is_null($request->project_id)) {
			$params['project_id'] = $request->project_id;
		} else {
			$params['project_name'] = $request->project_name;
		}

		if($request->filled('maintenance_price')) {
			$params['maintenance_price'] = $request->maintenance_price;
		}

		if($request->filled('parking_price')) {
			$params['parking_price'] = $request->parking_price;
		}

		// save data
		$newRoom = $this->repository->saveNewApartmentUnit($user, $params);
		$newRoom = Room::with(['tags.photo', 'tags.photoSmall'])->find($newRoom->id);
		
		$roomData = (new RoomPresenter('after-input'))->present($newRoom);

		$dialogMessage = "Terima kasih telah mendaftarkan apartment Anda di MamiKos!";
		if(empty($newRoom->cards)) {
			$dialogMessage .= "\nLengkapi foto kost agar iklan bisa segera tayang";
		}

		// invalidate campaign source cookie
		if(isset($_COOKIE['campaign_source'])) {
			setcookie('campaign_source', '', time() - 3600, '/');
		}

		return ApiResponse::responseData([
			'data'=>[
				'room'=>$roomData['data'],
				'dialog_message'=>$dialogMessage
			],
		]);

	}

	public function updateApartment(Request $request, $songId)
	{
		// must be an owner
		$user = $this->user();

		$request->merge(array_map('trim', $request->all('agent_email')));

		$room = Room::with(['owners', 'owners.user','tags.photo', 'tags.photoSmall'])
					->where('song_id', $songId)
					->first();

		// if room data not found
		if (is_null($room))
		{
			$response = [
				'status'	=> false,
                'messages'	=> [
					'Kamar tidak ditemukan'
				]
			];

			return ApiResponse::responseData($response);
		}

		$owner = null;
		// check if user is the owner of this room
		if (!$room->owners->count()) 
		{
			$response = [
				'status'	=> false,
                'messages'	=> [
					'Anda bukan pemilik dari apartemen ini'
				]
			];

			return ApiResponse::responseData($response);
		}

		foreach ($room->owners as $roomOwner) 
		{
			if (in_array($roomOwner->status, $this->dataStatuses) !== false) 
			{
				$owner = $roomOwner;
				break;
			}
		}

		// if related owner data remains not found
		if (is_null($owner) || $owner->user_id != $user->id) 
		{
			$response = [
				'status'	=> false,
				'messages'	=> [
					'Anda bukan pemilik dari apartemen ini'
				]
			];

			return ApiResponse::responseData($response);
		}

		$validator = Validator::make($request->all(), [
				'unit_name'=>'required|max:250',
				'unit_number' => 'required',
				'unit_type'=>'required',
				'unit_type_name' => 'nullable|required_if:unit_type,Lainnya|max:100',
				'floor'=>'required|numeric',
				'unit_size'=>'required|numeric',
				'min_payment'=>'nullable|numeric',
				'price_daily'=>'nullable|numeric',
				'price_weekly'=>'nullable|numeric',
				'price_monthly'=>'nullable|numeric',
				'price_yearly'=>'nullable|numeric',
				'price_shown'=>'nullable',
				'description'=>'nullable|max:900',
				'maintenance_price'=>'nullable|numeric',
				'parking_price'=>'nullable|numeric',
				'fac_unit'=>'required',
				'is_furnished'=>'required',
				'fac_room'=>'required_unless:is_furnished,0',
				'photos'=>'nullable',
				'youtube_link'	=> 'url|max:300'
			], $this->apartmentValidationMessages);

		$validator->after(function($validator) use ($request)
        {
            if (!$request->filled('price_weekly') AND !$request->filled('price_monthly') AND !$request->filled('price_daily') AND !$request->filled('price_yearly')) {
        		$validator->errors()->add('price', 'Harga sewa harus diisi salah satu');
			}

			$char_min = 4;
			if ($request->filled('price_type') AND $request->input('price_type') == 'usd') {
				$price_type = 'usd';
				$char_min = 1;
			}

			if ($request->filled('price_monthly') AND $request->input('price_monthly') == 0) $monthly = null;
			else $monthly = $request->input('price_monthly');

            if (!is_null($monthly) AND strlen($request->input('price_monthly')) < $char_min) {
                $validator->errors()->add('price_monthly', 'Harga Sewa Bulanan harus minimal '.$char_min.' karakter');
            }

			if ($request->filled('price_daily') AND $request->input('price_daily') == 0) $daily = null;
			else $daily = $request->input('price_daily');

            if ( !is_null($daily) AND strlen($request->input('price_daily')) < $char_min ) {
                $validator->errors()->add('price_daily', 'Harga Sewa Harian harus minimal '.$char_min.' karakter');
            }
			
			if ($request->filled('price_weekly') AND $request->input('price_weekly') == 0) $weekly = null;
			else $weekly = $request->input('price_weekly');

            if ( !is_null($weekly) AND strlen($request->input('price_weekly')) < $char_min ) {
                $validator->errors()->add('price_weekly', 'Harga Sewa Mingguan harus minimal '.$char_min.' karakter');
            }

			if ($request->filled('price_yearly') AND $request->input('price_yearly') == 0) $yearly = null;
			else $yearly = $request->input('price_yearly');

            if ( !is_null($yearly) AND strlen($request->input('price_yearly')) < $char_min) {
                $validator->errors()->add('price_yearly', 'Harga Sewa Tahunan harus minimal '.$char_min.' karakter');
			}
			
			if (is_null($monthly) AND is_null($yearly) AND is_null($daily) AND is_null($weekly)) {
				$validator->errors()->add('price', 'Harga sewa harus diisi salah satu');
			}
            
        });

		$validator->after(function($validator) use ($request, $room) {
			$unitNumberExist = Room::where('apartment_project_id', $room->apartment_project_id)
									->where('unit_number', $request->unit_number)
									->where('song_id', '<>', $room->song_id)
									->first();

			if($unitNumberExist) {
				$validator->errors()->add('unit_number', 'Nomor Unit telah digunakan sebelumnya');
			}

			if(is_array($request->price_shown)) {
				foreach($request->price_shown as $priceShown) {
					$priceShownValue = 'price_' . $priceShown;
					if(is_null($request->$priceShownValue) || $request->$priceShownValue <= 0) {
						$validator->errors()->add('price_shown', 'Jumlah Harga yang akan ditampilkan harus diisi');
						break;
					}
				}
			}
		});

		if($validator->fails()) {
			return ApiResponse::responseData([
				'status'=>false,
                'messages'=>$validator->errors()->all()
			]);
		}

		$usdPrice = Cache::get("currencyusd");
		if (is_null($usdPrice)) {
			$usdPrice = Currency::where("name", "usd")->first()->exchange_rate;
			Cache::put("currencyusd", $usdPrice, 60 * 24);
		}
		
		if ($request->filled('price_type') AND $request->input('price_type') == 'usd') {
			$dailyPrice = $request->price_daily * $usdPrice;
			$weeklyPrice = $request->price_weekly * $usdPrice;
			$monthlyPrice = $request->price_monthly * $usdPrice;
			$yearlyPrice = $request->price_yearly * $usdPrice;

			$priceDailyUsd = $request->price_daily;
			$priceWeeklyUsd = $request->price_weekly;
			$priceMonthlyUsd = $request->price_monthly;
			$priceYearlyUsd = $request->price_yearly;
			$priceType = $request->input('price_type');
		}

		$params = [
			'name'=>$request->unit_name,
			'unit_number'=> $request->unit_number,
			'price_daily'=> isset($dailyPrice) ? round($dailyPrice) : $request->price_daily,
			'price_weekly'=> isset($weeklyPrice) ? round($weeklyPrice) : $request->price_weekly,
			'price_monthly'=> isset($monthlyPrice) ? round($monthlyPrice) : $request->price_monthly,
			'price_yearly'=> isset($yearlyPrice) ? round($yearlyPrice) : $request->price_yearly,
			'price_daily_usd' =>  isset($priceDailyUsd) ? $priceDailyUsd : 0,
			'price_weekly_usd' => isset($priceWeeklyUsd) ? $priceWeeklyUsd : 0,
			'price_monthly_usd' => isset($priceMonthlyUsd) ? $priceMonthlyUsd : 0,
			'price_yearly_usd' => isset($priceYearlyUsd) ? $priceYearlyUsd : 0,
			'price_shown' =>is_array($request->price_shown) ? implode('|', $request->price_shown) : null,
			'description' => $request->description,
			'floor'=>$request->floor,
			'unit_type'=>$request->unit_type,
			'unit_type_name'=>$request->unit_type_name,
			'unit_size'=>$request->unit_size,
			'fac_unit'=>$request->fac_unit,
			'fac_room'=>!is_null($request->fac_room) ? $request->fac_room : [],
			'is_furnished'=>$request->is_furnished,
			'min_payment'=>!is_null($request->min_payment) ? [$request->min_payment] : [],
			'photos'=>$request->photos,
			'price_type' => isset($priceType) ? $priceType : 'idr',
			// 'youtube_link'=>$request->youtube_link
		];

		if ($request->filled('youtube_link') || $request->youtube_link == '') {
			$params['youtube_link'] = $request->youtube_link;
		}

		if($request->filled('maintenance_price')) {
			$params['maintenance_price'] = $request->maintenance_price;
		}

		if($request->filled('parking_price')) {
			$params['parking_price'] = $request->parking_price;
		}

		$updatedRoom = $this->repository->updateApartmentUnitByOwner($room, $owner, $params);

		//trancking update
		TrackingFeature::insertTo(["type" => "room-update", "identifier" => $room->id]);


		// set presenter
		$roomData = (new RoomPresenter('after-input'))->present($updatedRoom);

		return ApiResponse::responseData([
			'data'=>[
				'room'=>$roomData['data'],
				'dialog_message' => 'Edit Apartemen berhasil, silakan tunggu verifikasi admin (2x24 jam).'
			],
		]);
	}


	/**
	 * Input Kost API with simple form
	 * Used by gojek driver
	 */
	public function inputKostSimple(Request $request)
	{
		$validator = Validator::make($request->all(), 
			[
				'address'       => 'required|max:250',
				'name'          => 'required|min:5|max:250',
				'owner_phone'   => 'required|regex:/^(08[1-9]\d{7,10})$/',
				'photos'		=> 'required',
				'photos.cover'	=> 'required',
				'agent_name'	=> 'required|max:100',
				'agent_phone'	=> 'required|max:12|regex:/^(08[1-9]\d{7,10})$/',
				'input_source'	=> 'max:190'
			], $this->kostSimpleValidationMessage);

		$validator->after(function($validator) use ($request) {
			// make sure no duplicate name
			$nameExist = Room::where('name', $request->name)->first();
			if($nameExist) {
				$validator->errors()->add('name', 'Nama kost telah digunakan sebelumnya, silakan gunakan nama lain');
			}
		});

		if($validator->fails()) {
			$response = [
				'status'=>false,
                'messages'=>$validator->errors()->all()
			];

			return ApiResponse::responseData($response);
		}

		$params = [
			'name' => $request->name,
			'address' => $request->address,
			'owner_phone' => $request->owner_phone,
			'photos' => $request->photos,
			'input_as' => 'Gojek',
			'agent_name' => $request->agent_name,
			'agent_phone' => $request->agent_phone,
			'input_source' => $request->input_source
		];

		$newRoom = $this->repository->saveNewKostSimple($params);
		
		return ApiResponse::responseData([
			'status' => true,
			'message' => ''
		]);
	}

	public function inputKostReferral(Request $request)
	{
		$validator = Validator::make($request->all(), 
			[
				'address'       => 'required|max:250',
				'name'          => 'required|min:5|max:250',
				'owner_phone'   => 'required|regex:/^(08[1-9]\d{7,10})$/',
				'photos'		=> 'required',
				'photos.cover'	=> 'required',
				'agent_name'	=> 'required|max:100',
				'agent_phone'	=> 'required|max:14|regex:/^(08[1-9]\d{7,10})$/',
				'review'        => 'required',
				'referral_code' => 'required',
				'latitude'      => 'required',
				'longitude'     => 'required',
				'price_daily'   => 'nullable|numeric',
				'price_weekly'  => 'nullable|numeric',
				'price_monthly' => 'nullable|numeric',
				'price_yearly'  => 'nullable|numeric',
				'input_source'	=> 'max:190'
			], $this->kostSimpleValidationMessage);

		$validator->after(function($validator) use ($request)
        {
            if ( (int)$request->price_daily != 0 && strlen($request->price_daily) < 4 )
            {
                $validator->errors()->add('price_daily', 'Harga Sewa Harian harus minimal 4 karakter');
            }

            if ( (int)$request->price_weekly != 0 && strlen($request->price_weekly) < 4 )
            {
                $validator->errors()->add('price_weekly', 'Harga Sewa Mingguan harus minimal 4 karakter');
            }

            if ( (int)$request->price_monthly != 0 && strlen($request->price_monthly) < 4 )
            {
                $validator->errors()->add('price_monthly', 'Harga Sewa Bulanan harus minimal 4 karakter');
            }

            if ( (int)$request->price_yearly != 0 && strlen($request->price_yearly) < 4 )
            {
                $validator->errors()->add('price_yearly', 'Harga Sewa Tahunan harus minimal 4 karakter');
            }
        });
        
        $checkReferralCode = Referrer::where('code', $request->input('referral_code'))->first();

		$validator->after(function($validator) use ($request, $checkReferralCode) {
			// make sure no duplicate name
			$nameExist = Room::where('name', $request->name)->first();
			if($nameExist) {
				$validator->errors()->add('name', 'Nama kost telah digunakan sebelumnya, silakan gunakan nama lain');
			}

			if (is_null($checkReferralCode)) {
				$validator->errors()->add('referral_code', 'Kode referral tidak sesuai');	
			}
		});

		if($validator->fails()) {
			$response = [
				'status'=>false,
                'messages'=>$validator->errors()->all()
			];

			return ApiResponse::responseData($response);
		}

		$params = [
			'name'        => $request->name,
			'address'     => $request->address,
			'owner_phone' => $request->owner_phone,
			'photos'      => $request->photos,
			'input_as'    => 'Referral',
			'agent_name'  => $request->agent_name,
			'agent_phone' => $request->agent_phone,
			'review'      => $request->review,
			'referrer'    => $checkReferralCode,
			'user'        => $this->user(),
			'latitude'    => $request->latitude,
			'longitude'   => $request->longitude,
			'price'       => $request->price,
			'city'        => $request->city,
			'subdistrict' => $request->subdistrict,
			'price_daily' => $request->price_daily,
			'price_weekly'=> $request->price_weekly,
			'price_monthly' => $request->price_monthly,
			'price_yearly'  => $request->price_yearly
		];

		$newRoom = $this->repository->saveNewKostReferral($params);
		
		return ApiResponse::responseData([
			'status' => true,
			'message' => ''
		]);
	}

	/** 
	 * General function to do simple input 
	 * #growthsprint1
	 */
	public function inputKostGeneral(Request $request)
	{
		$validator = Validator::make($request->all(), 
			[
				'address'       => 'required|max:250',
				'name'          => 'required|min:5|max:250',
				'owner_phone'   => 'required',
				'photos.cover'	=> 'nullable',
				'agent_name'	=> 'required',
				'agent_phone'	=> 'required',
				'latitude'      => 'nullable|numeric',
				'longitude'     => 'nullable|numeric',
				'price_daily'	=> 'nullable|numeric',
				'price_weekly'	=> 'nullable|numeric',
				'price_monthly' => 'nullable|numeric',
				'price_yearly'	=> 'nullable|numeric',
				'input_source'	=> 'max:190'
			], $this->kostValidationMessages);

		$validator->after(function($validator) use ($request)
        {
            if ( (int)$request->price_daily != 0 && strlen($request->price_daily) < 4 )
            {
                $validator->errors()->add('price_daily', 'Harga Sewa Harian harus minimal 4 karakter');
            }

            if ( (int)$request->price_weekly != 0 && strlen($request->price_weekly) < 4 )
            {
                $validator->errors()->add('price_weekly', 'Harga Sewa Mingguan harus minimal 4 karakter');
            }

            if ( (int)$request->price_monthly != 0 && strlen($request->price_monthly) < 4 )
            {
                $validator->errors()->add('price_monthly', 'Harga Sewa Bulanan harus minimal 4 karakter');
            }

            if ( (int)$request->price_yearly != 0 && strlen($request->price_yearly) < 4 )
            {
                $validator->errors()->add('price_yearly', 'Harga Sewa Tahunan harus minimal 4 karakter');
            }
        });

		$validator->after(function($validator) use ($request) {
			// make sure no duplicate name
			$nameExist = Room::where('name', $request->name)->first();
			if($nameExist) {
				$validator->errors()->add('name', 'Nama kost telah digunakan sebelumnya, silakan gunakan nama lain');
			}
		});

		if($validator->fails()) {
			$response = [
				'status'=>false,
                'messages'=>$validator->errors()->all()
			];

			return ApiResponse::responseData($response);
		}

		$params = [
			'name'        => $request->name,
			'address'     => $request->address,
			'owner_phone' => $request->owner_phone,
			'photos'      => $request->photos,
			'input_as'    => 'Lainnya',
			'agent_name'  => $request->agent_name,
			'agent_phone' => $request->agent_phone,
			'latitude'    => $request->latitude,
			'longitude'   => $request->longitude,
			'city'        => $request->city,
			'subdistrict' => $request->subdistrict,
			'price_daily' => $request->price_daily,
			'price_weekly'=> $request->price_weekly,
			'price_monthly' => $request->price_monthly,
			'price_yearly'  => $request->price_yearly,
			'input_source'	=> $request->input_source
		];

		$user = $this->user();

		$newRoom = $this->repository->saveNewKostGeneral($user, $params);
		
		return ApiResponse::responseData([
			'status' => true,
			'message' => ''
		]);
	}


	/**
	 * Get Tags group for form input
	 *
	 * @param Request $request 			Laravel Request
	 * @param string $type 				Request type
	 *
	 * @return Array 					API Response 
	 */
	public function getTagsGroup(Request $request, $type = 'kost')
	{
		$tags = [];
		$tags['photo_example'] = 'https://mamikos.com/uploads/cache/data/style/2017-03-14/dp7jjH2u-360x480.jpg';

		if ($type == 'marketplace') {
			$tags["type"] = MarketplaceProduct::TYPE_PRODUCT;
			$tags["condition"] = MarketplaceProduct::CODITION_PRODUCT;
			$tags["unit"] = MarketplaceProduct::PRICE_UNIT;

			return ApiResponse::responseData([
				'data'=>$tags
			]);
		}

		$minPaymentOptions = Tag::where('type', 'keyword')->get();
		foreach($minPaymentOptions as $option) {
			$tags['min_payment'][] = [
				'tag_id'=>$option->id,
				'name'=>$option->name
			];
		}
			
		if($type == 'kost') {
			$tags['kost_facilities'] = [
				['tag_id'=>15, 'name'=>'WiFi'],
				['tag_id'=>59, 'name'=>'Akses Kunci 24 Jam'],
				['tag_id'=>60, 'name'=>'Bisa Pasutri'],
				['tag_id'=>22, 'name'=>'Parkir Mobil']
			];

			$tags['room_facilities'] = [
				['tag_id'=>13, 'name'=>'AC'],
				['tag_id'=>58, 'name'=>'Kipas Angin'],
				['tag_id'=>45, 'name'=>'TV'],
				['tag_id'=>8, 'name'=>'Air Panas']
			];

			$tags['bath_facilities'] = [
				['tag_id'=>1, 'name'=>'Dalam'],
				['tag_id'=>2, 'name'=>'Luar']
			];
		} elseif($type == 'apartment') {
			$tags['unit_type'] = [
				'1-Room Studio',
				'1 BR',
				'2 BR',
				'3 BR',
				'4 BR',
				'Lainnya'
			];

			$tags['unit_facilities'] = [
				['tag_id'=>13, 'name'=>'AC'],
				['tag_id'=>15, 'name'=>'WiFi'],
				['tag_id'=>87, 'name'=>'Access Card']
			];

			$tags['room_facilities'] = [
				['tag_id'=>10, 'name'=>'Bed'],
				['tag_id'=>12, 'name'=>'TV'],
				['tag_id'=>79, 'name'=>'TV Kabel'],
				['tag_id'=>80, 'name'=>'Sofa'],
				['tag_id'=>19, 'name'=>'Kulkas'],
				['tag_id'=>27, 'name'=>'Dapur'],
				['tag_id'=>31, 'name'=>'Mesin Cuci'],
				['tag_id'=> 8, 'name'=>'Air Panas'],
				['tag_id'=>18, 'name'=>'Dispenser'],
				['tag_id'=>149, 'name'=>'Meja Makan'],
				['tag_id'=>150, 'name'=>'Microwave']
			];

			$tags['room_facilities_semi_furnished'] = [
				['tag_id'=>10, 'name'=>'Bed'],
				['tag_id'=>12, 'name'=>'TV'],
				['tag_id'=>79, 'name'=>'TV Kabel'],
				['tag_id'=>80, 'name'=>'Sofa'],
				['tag_id'=>19, 'name'=>'Kulkas'],
				['tag_id'=>27, 'name'=>'Dapur'],
				['tag_id'=>31, 'name'=>'Mesin Cuci'],
				['tag_id'=> 8, 'name'=>'Air Panas']
			];
		}

		return ApiResponse::responseData([
			'data'=>$tags
		]);
	}

	/**
	 * Function to get custom input_as
	 * #growthsprint1
	 */
	public function getInputAs(Request $request)
	{
		$result = [
			[
				'identifier'	=> 3,
				'type' 			=> 'special',
				'title' 		=> 'Tambah Kos Dapat "PULSA"',
				'value'			=> 'Anak Kos'
			],
			[
				'identifier'	=> 0,
				'type' 			=> 'normal',
				'title' 		=> 'Pemilik Kos',
				'value'			=> 'Pemilik Kos'
			],
			[
				'identifier'	=> 1,
				'type' 			=> 'normal',
				'title' 		=> 'Pengelola Kos',
				'value'			=> 'Pengelola Kos'
			],
			[
				'identifier'	=> 2,
				'type' 			=> 'normal',
				'title' 		=> 'Agen',
				'value'			=> 'Agen'
			],
			[
				'identifier'	=> 3,
				'type' 			=> 'normal',
				'title' 		=> 'Anak Kos',
				'value'			=> 'Anak Kos'
			],
			[
				'identifier'	=> 4,
				'type' 			=> 'normal',
				'title' 		=> 'Lainnya',
				'value'			=> 'Lainnya'
			]
		];

		return ApiResponse::responseData([
			'data' => $result
		]);
	}

	public function registerRewardNumber(Request $request)
	{
		$validator = Validator::make($request->all(),
						[
							'room_id'=>'required|numeric',
							'phone_number'=>'required|regex:'.RegexHelper::phoneNumberFormat(),
							'name'=>'max:150'
						],
						[
							'room_id.required'=>'ID kost harus diisi',
							'room_id.numeric'=>'ID kost harus berupa angka',
							'phone_number.required'=>'Nomor HP harus diisi',
							'phone_number.regex'=>'Format nomor HP tidak valid (08xxxxxxxx)',
							'name.max'=>'Nama maksimal :max karakter'
						]);



		if($validator->fails()) {
			return ApiResponse::responseData([
                     'status'=>false,
                     'messages'=>$validator->errors()->all()
                 ]);
		}

		$existingReward = UserInputReward::where('designer_id', $request->room_id)->first();
		if($existingReward) {
			return ApiResponse::responseData([
					'status'=>false,
					'messages'=>['Anda tidak dapat mendaftarkan nomor HP berbeda untuk inputan yang sama']
				]);
		}

		$params = [
			'room_id'=>$request->room_id,
			'phone_number'=>$request->phone_number,
			'name'=>$request->name
		];

		$reward = UserInputReward::setRewardNumber($params);

		return ApiResponse::responseData([
				'data'=>[
					'reward_id'=>$reward->id,
					'phone_number'=>$reward->phone_number,
					'name'=>$reward->name
				]
			]);
	}

	public function requestInputPost(Request $request)
	{
		$validator = Validator::make($request->all(),
			[
				'name'=>'required',
				'phone_number'=>'required|regex:/^(08[1-9]\d{7,10})$/',
				'email'=>'required|email'
			], 
			[
				'name.required' => 'Nama harus diisi',
				'phone_number.required'=>'Nomor Telepon harus diisi',
				'phone_number.regex' => 'Format Nomor Telepon tidak valid (08xxxxxxxx)',
				'email.required' => 'Email harus diisi',
				'email.email' => 'Format Email tidak valid'
			]);

		if($validator->fails()) {
			return ApiResponse::responseData([
				'status' => false,
				'meta'=> [
					'message'=>implode('. ', $validator->errors()->all()),
					'messages'=>$validator->errors()->all()
				]
			]);
		}


		Mail::send('emails.request-input-apartment', [
				'name'=>$request->get('name'),
				'phone_number'=>$request->get('phone_number'),
				'email'=>$request->get('email')
			], function($message)
        {
            $message->to('team@mamikos.com')->subject('Permintaan Bantuan Upload Apartemen');
        });

		return ApiResponse::responseData(['status'=>true]);
	}

	public function getApartmentProjectTags(Request $request)
	{
		$tags = Tag::where('type', '=', 'fac_project')->get();

		$projectTags = [];

		foreach($tags as $tag) {
			$projectTags[] = [
				'tag_id' => $tag->id,
				'name' => $tag->name
			];
		}

		return ApiResponse::responseData([
			'tags' => $projectTags
		]);
	}

	public function checkExistingInput(Request $request)
	{
		$criteria = $request->input('criteria');

		if(is_null($criteria) || $criteria == '') {
			return ApiResponse::responseData([
				'rooms'=> []
			]);
		}

		$rooms = $this->repository->getExistingInput($criteria);

		$roomData = (new RoomPresenter('existing-input'))->present($rooms);

		return ApiResponse::responseData([
			'rooms'=> $roomData['data']
		]);
	}

	public function checkExistingPhoto(Request $request, $roomId)
	{
		$room = Room::with('cards')
					->where('song_id', $roomId)
					->first();

		if(!$room) {
			return ApiResponse::responseData([
				'status' => false,
				'meta' => [
					'message' => 'Kamar tidak ditemukan'
				]
			]);
		}

		$cards = \App\Entities\Room\Element\Card::showList($room, 'edit');

		return ApiResponse::responseData([
			'name' => $room->name,
			'photos' => $cards
		]);
	}
}