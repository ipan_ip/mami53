<?php

namespace App\Http\Controllers\Api\Auth;

use App\Entities\Activity\ActivationCode;
use App\Entities\Activity\ActivationCodeType;
use App\Entities\Activity\Enums\VerificationMethod;
use App\Http\Controllers\Api\BaseController;
use App\Http\Helpers\ApiResponse;
use App\Http\Requests\OwnerForgetPasswordCheckRequest;
use App\Http\Requests\OwnerForgetPasswordCodeRequest;
use App\Http\Requests\OwnerForgetPasswordRequest;
use App\Http\Requests\OwnerEditPhoneCodeRequest;
use App\Http\Requests\OwnerEditPhoneRequest;
use App\Http\Requests\ForgetPasswordIdentifierRequest;
use App\Notifications\PhoneNumberVerification;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Notification;

/**
 *  Handle authentication related process for owner
 *
 */
class OwnerController extends BaseController
{
    protected $lastCodeRequested;

    /**
     *  Enable editing of user's profile phone
     *
     *  @param Request $request
     *
     *  @return JsonResponse
     */
    public function editPhone(OwnerEditPhoneRequest $request): JsonResponse
    {
        $validator = $request->failedValidator;

        if (!is_null($validator) && $validator->fails()) {
            return ApiResponse::responseData([
                'status' => false,
                'meta' => [
                    'message' => $validator->errors()->all()
                ]
            ]);
        }

        $owner = $this->user();

        if (is_null($owner)) {
            return ApiResponse::responseData([
                'status' => false,
                'message' => 'Not signed in'
            ]);
        }

        $owner->update(['phone_number' => $request->phone_number]);

        ActivationCode::deleteLastCode($request->get('phone_number'));

        return ApiResponse::responseData(['status' => true]);
    }

    /**
     *  Request verification code for owner to change phone number
     *
     *  @param OwnerEditPhoneCodeRequest $request
     *
     *  @return JsonResponse
     */
    public function editPhoneRequest(OwnerEditPhoneCodeRequest $request): JsonResponse
    {
        $validator = $request->failedValidator;

        if (!is_null($validator) && $validator->fails()) {
            return ApiResponse::responseData([
                'status' => false,
                'meta' => [
                    'message' => $validator->errors()->all()
                ]
            ]);
        }

        $phoneNumber = $request->phone_number;
        $this->lastCodeRequested = ActivationCode::where('phone_number', $phoneNumber)
            ->where('for', ActivationCodeType::OWNER_EDIT_PHONE_NUMBER)
            ->latest()
            ->first();

        $minutesPerRequest = 1; // Minutes before new request is allowed
        if ($this->isTooFrequent($minutesPerRequest)) {
            return ApiResponse::responseData([
                'status' => false,
                'meta' => [
                    'message' => __('api.owner.verification.code.too_frequent', ['minutes' => '01.00']) // The minute are formatted like this to conform with UX.
                ]
            ]);
        }

        $this->deletePreviousCode($phoneNumber);
        $activationCode = ActivationCode::generateActivationCode(
            $phoneNumber,
            ActivationCodeType::OWNER_EDIT_PHONE_NUMBER
        );
        $activationCode->save();
        $this->sendVerificationCode((new User(['phone_number' => $phoneNumber])), $activationCode); // Create a placeholder model User for the phone number.

        return ApiResponse::responseData([
            'status' => true,
            'meta' => [
                'message' => __('api.owner.verification.code.success')
            ]
        ]);
    }

    /**
     *  Retrieve available user owner identifier to select the verification method
     *
     *  @param ForgetPasswordIdentifierRequest $request
     *
     *  @return JsonResponse
     */
    public function forgetPasswordIdentifier(ForgetPasswordIdentifierRequest $request): JsonResponse
    {
        $validator = $request->failedValidator;

        if (!is_null($validator) && $validator->fails()) {
            return ApiResponse::responseData([
                'message' => $validator->errors()->first(),
                'contact' => null,
            ], false, false);
        }

        $validateUser = $this->validateUserIdentifier($request);
        if ($validateUser instanceof JsonResponse) {
            return $validateUser;
        }
        
        $data = $this->userIdentifierData($validateUser);

        return ApiResponse::responseData([
            'status' => true,
            'data' => $data
        ]);
    }

    /**
     *  Forget password for owner by authenticating through OTP
     *
     *  @param OwnerForgetPasswordRequest $request
     *
     *  @return JsonResponse
     */
    public function forgetPassword(OwnerForgetPasswordRequest $request): JsonResponse
    {
        $validator = $request->failedValidator;

        if (!is_null($validator) && $validator->fails()) {
            return ApiResponse::responseData([
                'status' => false,
                'meta' => [
                    'message' => $validator->errors()->all()
                ],
                'is_forbidden' => false
            ]);
        }

        $user = User::where('phone_number', $request->phone_number)->get();
        $response = $this->validateUser($user);
        if (!is_null($response)) {
            return $response;
        }

        $user->first()->update(['password' => Hash::make($request->password)]);

        ActivationCode::deleteLastCode($request->get('phone_number'));

        return ApiResponse::responseData([
            'status' => true,
            'meta' => [
                'message' => 'success'
            ],
            'is_forbidden' => false
        ]);
    }

    /**
     *  Request verification code for owner to change password
     *
     *  @param Request $request
     *
     *  @return JsonResponse
     */
    public function forgetPasswordRequest(OwnerForgetPasswordCodeRequest $request): JsonResponse
    {
        $validator = $request->failedValidator;

        if (!is_null($validator) && $validator->fails()) {
            return ApiResponse::responseData([
                'status' => false,
                'meta' => [
                    'message' => $validator->errors()->first()
                ],
                'is_forbidden' => false
            ]);
        }

        $user = User::where('phone_number', $request->phone_number)->get();
        $response = $this->validateUser($user);
        if (!is_null($response)) {
            return $response;
        }

        $phoneNumber = $request->phone_number;

        $this->lastCodeRequested = ActivationCode::where('phone_number', $phoneNumber)
            ->where('for', ActivationCodeType::OWNER_FORGET_PASSWORD)
            ->latest()
            ->first();

        $minutesPerRequest = 1; // Minutes before new request is allowed
        if ($this->isTooFrequent($minutesPerRequest)) {
            return ApiResponse::responseData([
                'status' => false,
                'meta' => [
                    'message' => __('api.owner.verification.code.too_frequent', ['minutes' => '01.00']) // The minute are formatted like this to conform with UX.
                ],
                'is_forbidden' => false
            ]);
        }

        $this->deletePreviousCode($phoneNumber);
        $activationCode = ActivationCode::generateActivationCode(
            $phoneNumber,
            ActivationCodeType::OWNER_FORGET_PASSWORD
        );
        $activationCode->save();
        $this->sendVerificationCode((new User(['phone_number' => $phoneNumber])), $activationCode); // Create a placeholder model User for the phone number.

        return ApiResponse::responseData([
            'status' => true,
            'meta' => [
                'message' => __('api.owner.verification.code.success')
            ],
            'is_forbidden' => false
        ]);
    }

    /**
     *  Check whether verification code submitted againts a phone number is valid
     *
     *  This function check if verification code match a given phone number. This will not
     *  change owner password but simply validate whether the request was malicious for frotend
     *
     *  This method is case-sensitive to the verification code
     *
     *  @param OwnerForgetPasswordCheckRequest
     *  @see OwnerController::editPasswordRequest
     */
    public function forgetPasswordCheck(OwnerForgetPasswordCheckRequest $request): JsonResponse
    {
        $validator = $request->failedValidator;

        if (!is_null($validator) && $validator->fails()) {
            return ApiResponse::responseData([
                'status' => false,
                'meta' => [
                    'message' => $validator->errors()->all()
                ],
                'is_forbidden' => false
            ]);
        }

        $user = User::where('phone_number', $request->phone_number)->get();
        $response = $this->validateUser($user);
        if (!is_null($response)) {
            return $response;
        }

        $phoneNumber = $request->phone_number;

        $code = ActivationCode::where([
            ['expired_at', '>', Carbon::now()],
            ['phone_number', '=', $phoneNumber],
            ['for', '=', ActivationCodeType::OWNER_FORGET_PASSWORD]
        ])
        ->whereRaw('BINARY `code`=?', $request->verification_code) // Case-sensitive search so that H71E != H71e
        ->first();

        if (is_null($code)) {
            return ApiResponse::responseData([
                'status' => false,
                'meta' => [
                    'message' => __('api.owner.verification.code.invalid')
                ],
                'is_forbidden' => false
            ]);
        }

        return ApiResponse::responseData([
            'status' => true,
            'meta' => [
                'message' => __('api.owner.verification.code.success')
            ],
            'is_forbidden' => false
        ]);
    }

    private function validateUser($user)
    {   
        if ($user->where('is_owner', 'true')->count() > 0 && $user->where('is_owner', 'false')->count() > 0) {
            $title = __('api.input.forget_password.failed');
            $message = __('api.input.forget_password.tenant.duplicate');
        } else if ($user->where('is_owner', 'true')->count() > 1) {
            $title = __('api.input.forget_password.failed');
            $message = __('api.input.forget_password.owner.duplicate');
        } else if ($user->count() === 0) {
            $title = __('api.input.forget_password.send_failed');
            $message = __('api.input.forget_password.not_found');
        } else if ($user->where('is_owner', 'false')->count() === 1 && !is_null($user->first()->social)) {
            $title = __('api.input.forget_password.send_failed');
            $message = __('api.input.forget_password.social');
        } else if ($user->where('is_owner', 'false')->count() > 0) {
            $title = __('api.input.forget_password.send_failed');
            $message = __('api.input.forget_password.wrong_section');
        } else {
            return;
        }

        return ApiResponse::responseData([
            'status' => false,
            'meta' => [
                'message' => $message,
            ],
            'title' => $title,
            'is_forbidden' => true // Indicate this special case of error
        ]);
    }

    /**
     *  Check if code request was too frequent
     *
     *  @param string $phoneNumber Receiver of the code
     *  @param int $minutesPerRequest Number of minutes user need to wait before a new request
     *
     *  @return bool
     */
    private function isTooFrequent(int $minutesPerRequest)
    {
        $isAlreadyRequested = !empty($this->lastCodeRequested);
        
        // Check if user already has a code and is requesting a new one
        if ($isAlreadyRequested) {
            if ($this->lastCodeRequested->created_at->diffInMinutes(Carbon::now(), false) < $minutesPerRequest) {
                return true;
            }
        }

        return false;
    }

    /**
     *  Delete existing code sent
     *
     *  @param string $phoneNumber Receiver of the previous code
     *
     *  @return void
     */
    private function deletePreviousCode(string $phoneNumber): void
    {
        $isAlreadyRequested = !empty($this->lastCodeRequested);

        if ($isAlreadyRequested) {
            // If code has not expired but user requested new code, delete the old one;
            ActivationCode::deleteLastCode($phoneNumber);
        }
    }

    /**
     *  Send veritifcation code to user
     *
     *  @param User $user Receiver of the notification
     *  @param ActivationCode $activationCode Code to send
     *  @return void
     */
    private function sendVerificationCode(User $user, ActivationCode $activationCode): void
    {
        Notification::send($user, new PhoneNumberVerification($activationCode));
    }

    /**
     *  Validate user duplication and user as owner
     *
     *  @param ForgetPasswordIdentifierRequest $request
     *  @return object
     */
    private function validateUserIdentifier(ForgetPasswordIdentifierRequest $request): object
    {
        $phoneNumber = $request->phone_number ?? null;
        $phoneCount = !is_null($phoneNumber) ? User::where('phone_number', $phoneNumber)->count() : 0;

        if ($phoneCount > 1) {
            return ApiResponse::responseData([
                'message' => __('api.input.forget_password.duplicate.message'),
                'contact' => [
                    'cta_message' => __('api.input.forget_password.duplicate.cta_message'),
                    'wa_number' => __('api.input.forget_password.duplicate.wa_number'),
                    'pretext' => __('api.input.forget_password.duplicate.pretext', ['identifier' => $phoneNumber ?? $email]),
                ]
            ], false, false);
        }

        if (!is_null($phoneNumber)) {
            $user = User::where('phone_number', $request->phone_number)->where('is_owner', 'true')->first();
            $message = __('api.input.phone_number.owner_not_found');
        }

        if (is_null($user)) {
            return ApiResponse::responseData([
                'message' => $message,
                'contact' => null,
            ], false, false);
        }

        return $user;
    }

    /**
     *  Retrieve and format user identifier data for verification method options
     *
     *  @param User $user
     *  @return array
     */
    private function userIdentifierData(User $user): array
    {
        $data = [
            array_merge(VerificationMethod::SMS, ['value' => $user->phone_number]),
            array_merge(VerificationMethod::WA, ['value' => $user->phone_number])
        ];

        return $data;
    }
}
