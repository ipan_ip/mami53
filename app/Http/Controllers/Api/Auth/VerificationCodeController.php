<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Api\BaseController;
use App\Http\Helpers\ApiResponse;
use App\Http\Requests\Auth\Verification\BaseVerificationRequest;
use Illuminate\Http\JsonResponse;
use App\Services\Activity\ActivationCodeService;
use Exception;
use App\Entities\Activity\ActivationCode;
use App\Entities\Activity\ActivationCodeType;
use App\Http\Requests\Auth\Verification\CheckVerificationRequest;
use App\Enums\Activity\ActivationCodeVia;
use App\Exceptions\TranslationableException;
use Illuminate\Contracts\Support\MessageBag;

/**
 * Controller for API related to verification code.
 */
class VerificationCodeController extends BaseController
{
    private $activationCodeService;

    public function __construct(ActivationCodeService $activationCodeService)
    {
        $this->activationCodeService = $activationCodeService;
    }

    /**
     * Request/resend OTP in new flow.
     *
     * @param BaseVerificationRequest $request
     * @return JsonResponse
     */
    public function request(BaseVerificationRequest $request): JsonResponse
    {
        if ($request->isValidationFailed()) {
            return ApiResponse::responseData([
                'status' => false,
                'meta' => [
                    'message' => $this->formatMessageBag($request->failedValidator->getMessageBag()),
                ]
            ]);
        }

        $activationCode = $this->activationCodeService->getActivationCode(
            $request->getFor(),
            $request->destination,
            $request->getVia()
        );

        if (!is_null($activationCode)) {
            return $this->requestResend($activationCode);
        }
        $activationCode = $this->activationCodeService->createActivationCode(
            $request->getFor(),
            $request->destination,
            $request->getVia()
        );
        $this->activationCodeService->sendActivationCode($activationCode);
        return ApiResponse::responseData($this->formatVerificationCodeForRequest($activationCode));
    }

    private function requestResend(ActivationCode $activationCode): JsonResponse
    {
        try {
            $this->activationCodeService->resendActivationCode($activationCode);
            return ApiResponse::responseData($this->formatVerificationCodeForRequest($activationCode));
        } catch (TranslationableException $e) {
            return ApiResponse::responseData([
                'status' => false,
                'meta' => [
                    'message' => $e->getTranslation() ?? $e->getMessage(),
                ]
            ]);
        } catch (Exception $e) {
            return ApiResponse::responseData([
                'status' => false,
                'meta' => [
                    'message' => $e->getMessage(),
                ]
            ]);
        }
    }

    private function formatVerificationCodeForRequest(ActivationCode $activationCode): array
    {
        return [
            'verification_code' => [
                'can_resend_at' => $this->activationCodeService->getAllowedResendTime($activationCode)->timestamp,
            ],
        ];
    }

    public function formatMessageBag(MessageBag $messageBag): string
    {
        return $messageBag->first();
    }

    /**
     * Checking the OTP inputted with the one in records.
     *
     * @param CheckVerificationRequest $request
     * @return JsonResponse
     */
    public function check(CheckVerificationRequest $request): JsonResponse
    {
        if ($request->isValidationFailed()) {
            return ApiResponse::responseData([
                'status' => false,
                'meta' => [
                    'message' => $this->formatMessageBag($request->failedValidator->getMessageBag()),
                ],
            ]);
        }

        return ApiResponse::responseData([
            'status' => true,
        ]);
    }
}
