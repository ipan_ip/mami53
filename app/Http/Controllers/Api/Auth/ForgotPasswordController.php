<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Api\BaseController;
use App\Http\Helpers\ApiResponse;
use App\Http\Requests\Auth\Verification\BaseVerificationRequest;
use Illuminate\Http\JsonResponse;
use App\Services\Activity\ActivationCodeService;
use App\Http\Requests\Auth\PasswordResetRequest;
use Illuminate\Contracts\Support\MessageBag;
use App\Entities\Activity\ActivationCode;
use App\Enums\Activity\ActivationCodeVia;
use App\User;
use Illuminate\Support\Facades\Hash;
use App\Entities\Activity\ActivationCodeType;

/**
 * Controller for API related to forgot password.
 */
class ForgotPasswordController extends BaseController
{
    private $activationCodeService;

    public function __construct(ActivationCodeService $activationCodeService)
    {
        $this->activationCodeService = $activationCodeService;
    }

    /**
     * Submit new password and code verification
     *
     * @param PasswordResetRequest $request
     * @return JsonResponse
     */
    public function passwordReset(PasswordResetRequest $request): JsonResponse
    {
        if ($request->isValidationFailed()) {
            return ApiResponse::responseData([
                'status' => false,
                'message' => $this->formatMessageBag($request->failedValidator->getMessageBag()),
            ]);
        }

        $user = User::where('phone_number', $request->destination)
            ->orWhere('email', $request->destination)
            ->first();

        if (is_null($user)) {
            return ApiResponse::responseData([
                'status' => false,
                'message' => __('api.input.forget_password.not_found'),
            ]);
        }

        $user->update(['password' => Hash::make($request->password)]);

        $this->activationCodeService->deleteLatestActivationCode(
                $request->getFor(),
                $request->getVia(),
                $request->destination
            );

        return ApiResponse::responseData([
            'status' => true,
        ]);
    }

    public function formatMessageBag(MessageBag $messageBag): string
    {
        return $messageBag->first();
    }
}