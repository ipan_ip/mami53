<?php

namespace App\Http\Controllers\Api\Auth;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Notification;
use App\Entities\Activity\ActivationCode;
use App\Entities\Activity\ActivationCodeType;
use App\Entities\Activity\Tracking;
use App\Entities\Activity\Enums\VerificationMethod;
use App\Entities\Device\UserDevice;
use App\Entities\Notif\SettingNotif;
use App\Entities\User\UserLoginTracker;
use App\Entities\User\UserSocial;
use App\Entities\User\UserVerificationAccount;
use App\Http\Controllers\Api\BaseController;
use App\Http\Helpers\ApiHelper;
use App\Http\Helpers\ApiResponse;
use App\Http\Requests\TenantChangePasswordRequest;
use App\Http\Requests\TenantLoginRequest;
use App\Http\Requests\TenantForgetPasswordCheckRequest;
use App\Http\Requests\TenantForgetPasswordCodeRequest;
use App\Http\Requests\TenantForgetPasswordRequest;
use App\Http\Requests\TenantRegistrationRequest;
use App\Http\Requests\TenantVerificationCheckRequest;
use App\Http\Requests\TenantVerificationCodeRequest;
use App\Http\Requests\ForgetPasswordIdentifierRequest;
use App\Jobs\MoEngage\SendMoEngageTenantProperties;
use App\Notifications\PhoneNumberVerification;
use App\Presenters\UserPresenter;
use App\Repositories\Mamipay\MamipayTenantRepository;
use App\User;
use Carbon\Carbon;
use Jenssegers\Agent\Agent;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Exception;

/**
 *  Control tenant specific authentication related process
 *
 */
class TenantController extends BaseController
{
    protected $lastCodeRequested;
    protected $mamipayTenantRepository;

    public function __construct(MamipayTenantRepository $mamipayTenantRepository)
    {
        $this->mamipayTenantRepository = $mamipayTenantRepository;
    }

    /**
     *  Handle tenant registration for the new flow
     *
     *  @param Request $request
     *
     *  @return JsonResponse
     */
    public function register(TenantRegistrationRequest $request): JsonResponse
    {
        $errors = $request->errors; // Only available if validation failed

        if (!is_null($errors)) {
            return ApiResponse::responseData([
                'status' => false,
                'messages' => $errors,
            ]);
        }

        $deviceDetail = [
            'identifier' => $request->input('device_identifier'),
            'uuid' => $request->input('device_uuid'),
            'platform' => $request->input('device_platform'),
            'platform_version_code' => $request->input('device_platform_version_code'),
            'model' => $request->input('device_model'),
            'email' => $request->input('device_email'),
            'app_version_code' => $request->input('app_version_code'),
        ];

        $user = User::create([
            'phone_number' => $request->phone_number,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'name' => $request->name,
            'role' => 'user',
            'is_owner' => 'false'
        ]);

        $this->mamipayTenantRepository->syncMamipayTenantWithUserTenant($user);

        Auth::loginUsingId($user->id);

        $userDataCheck = User::checkUserIncompleteAfterLogin($user, 'afterlogin'); // TODO: If you have time please refactor User::checkUserIncompleteAfterLogin

        $data = [
            'auth' => [
                'type'       => 'register',
                'user_id'    => $user->id,
                'gender'     => $user->gender,
            ],
            'profile'    => $user->profileFormat(),
            'phone'      => $userDataCheck['phone'],
            'email'      => $userDataCheck['email'],
            'show_edit'  => $userDataCheck['show_edit'],
            'chat_setting' => SettingNotif::getSingleUserNotificationSetting(
                $user->id,
                'app_chat',
                ($user->is_owner == 'true' ? true : false)
            ),
            'created_at' => (isset($user->created_at) ? $user->created_at : null)
        ];

        $this->loginOnDevice($deviceDetail, $user);
        $this->handleVerification($user);

        try {
            SendMoEngageTenantProperties::dispatch((int) $user->id);
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
        }

        ActivationCode::deleteLastCode($request->phone_number);

        return ApiResponse::responseData($data);
    }

    /**
     *  Handle tenant login request
     *
     *  @param TenantLoginRequest $request
     *
     *  @return JsonResponse
     */
    public function login(TenantLoginRequest $request): JsonResponse
    {
        $validator = $request->failedValidator;

        if (!is_null($validator) && $validator->fails()) {
            return ApiHelper::validationError($validator->errors()->first());
        }

        $user = $request->user;

        if (!Hash::check($request->password, $user->password)) {
            $message = $request->filled('phone_number') ? __('api.auth.tenant.phone_number.invalid') : __('api.auth.tenant.email.invalid');

            return ApiResponse::responseData([ 'message' => $message], false, false, 200, $message);
        }

        $device = [
            'identifier' => $request->device_identifier,
            'uuid' => $request->device_uuid,
            'platform' => $request->device_platform,
            'platform_version_code' => $request->device_platform_version_code,
            'model' => $request->device_model,
            'email' => $request->device_email,
            'app_version_code' => $request->app_version_code,
        ];

        Auth::loginUsingId($user->id);
        $this->loginOnDevice($device, $user);

        try {
            SendMoEngageTenantProperties::dispatch((int) $user->id);
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
        }

        $data = (new UserPresenter('login'))->present($user)['data'];
        return ApiResponse::responseData($data);
    }

    /**
     *  Request verification code for tenant registration
     *
     *  @param TenantVerificationCodeRequest $request
     *
     *  @return JsonResponse
     */
    public function requestVerification(TenantVerificationCodeRequest $request): JsonResponse
    {
        $validator = $request->failedValidator;

        if (!is_null($validator) && $validator->fails()) {
            return ApiResponse::responseData([
                'status' => false,
                'meta' => [
                    'message' => $validator->errors()->first()
                ]
            ]);
        }

        $phoneNumber = $request->phone_number;
        $this->lastCodeRequested = ActivationCode::where('phone_number', $phoneNumber)
            ->where('for', ActivationCodeType::TENANT_VERIFICATION)
            ->latest()
            ->first();

        $minutesPerRequest = 1; // Minutes before new request is allowed
        if ($this->isTooFrequent($minutesPerRequest)) {
            return ApiResponse::responseData([
                'status' => false,
                'meta' => [
                    'message' => __('api.tenant.verification.code.too_frequent', ['minutes' => '01.00']) // The minute are formatted like this to conform with UX.
                ]
            ]);
        }

        $this->deletePreviousCode($phoneNumber);
        $activationCode = ActivationCode::generateActivationCode($phoneNumber, ActivationCodeType::TENANT_VERIFICATION);
        $activationCode->save();
        $this->sendVerificationCode((new User(['phone_number' => $phoneNumber])), $activationCode); // Create a placeholder model User for the phone number.

        return ApiResponse::responseData([
            'status' => true,
            'meta' => [
                'message' => __('api.tenant.verification.code.success')
            ]
        ]);
    }

    /**
     *  Check whether verification code submitted againts a phone number is valid
     *
     *  This function check if verification code match a given phone number. This will not
     *  verify the owner but simply check the validity of its phone number verification code.
     *
     *  This method is case-sensitive to the verification code
     *
     *  @param TenantVerificationCheckRequest $request
     *
     *  @return JsonResponse
     *
     *  @see OwnerController::postRequestVerification
     */
    public function verificationCheck(TenantVerificationCheckRequest $request): JsonResponse
    {
        $validator = $request->failedValidator;

        if (!is_null($validator) && $validator->fails()) {
            return ApiResponse::responseData([
                'status' => false,
                'meta' => [
                    'message' => $validator->errors()->all()
                ]
            ]);
        }

        $code = ActivationCode::where([
            ['phone_number', '=', $request->phone_number],
            ['for', '=', ActivationCodeType::TENANT_VERIFICATION]
        ])
            ->whereNotExpired()
            ->whereCode($request->input('code', ''));

        if (!$code->exists()) {
            return ApiResponse::responseData([
                'status' => false,
                'meta' => [
                    'message' => __('api.tenant.verification.code.invalid')
                ]
            ]);
        }

        return ApiResponse::responseData([
            'status' => true,
            'meta' => [
                'message' => __('api.tenant.verification.code.success')
            ]
        ]);
    }

    /**
     *  Retrieve available user tenant identifier to select the verification method
     *
     *  @param ForgetPasswordIdentifierRequest $request
     *
     *  @return JsonResponse
     */
    public function forgetPasswordIdentifier(ForgetPasswordIdentifierRequest $request): JsonResponse
    {
        $validator = $request->failedValidator;

        if (!is_null($validator) && $validator->fails()) {
            return ApiResponse::responseData([
                'message' => $validator->errors()->first(),
                'contact' => null,
            ], false, false);
        }

        $validateUser = $this->validateUserIdentifier($request);
        if ($validateUser instanceof JsonResponse) {
            return $validateUser;
        }

        $data = $this->userIdentifierData($validateUser);
        
        return ApiResponse::responseData([
            'status' => true,
            'data' => $data
        ]);
    }

    /**
     *  Request verification code for tenant's forget password
     *
     *  @param Request $request
     *
     *  @return JsonResponse
     */
    public function forgetPasswordRequest(TenantForgetPasswordCodeRequest $request): JsonResponse
    {
        $validator = $request->failedValidator;
        $phoneNumber = $request->phone_number;

        if (!is_null($validator) && $validator->fails()) {
            $messages = [];

            foreach ($validator->errors()->getMessages() as $key => $message) {
                $messages[$key] = $message[0];
            }

            return ApiResponse::responseData([
                'status' => false,
                'messages' => $messages,
                'is_forbidden' => false
            ]);
        }

        $user = User::where('phone_number', $request->phone_number)->get();
        $response = $this->validateUser($user);
        if (!is_null($response)) {
            return $response;
        }

        $this->lastCodeRequested = ActivationCode::where('phone_number', $phoneNumber)
            ->where('for', ActivationCodeType::TENANT_FORGET_PASSWORD)
            ->latest()
            ->first();

        $minutesPerRequest = 1; // Minutes before new request is allowed
        if ($this->isTooFrequent($minutesPerRequest)) {
            return ApiResponse::responseData([
                'status' => false,
                'meta' => [
                    'message' => __('api.tenant.verification.code.too_frequent', ['minutes' => '01.00']) // The minute are formatted like this to conform with UX.
                ],
                'is_forbidden' => false
            ]);
        }

        $this->deletePreviousCode($phoneNumber);
        $activationCode = ActivationCode::generateActivationCode(
            $phoneNumber,
            ActivationCodeType::TENANT_FORGET_PASSWORD
        );
        $activationCode->save();
        $this->sendVerificationCode((new User(['phone_number' => $phoneNumber])), $activationCode); // Create a placeholder model User for the phone number.

        return ApiResponse::responseData([
            'status' => true,
            'meta' => [
                'message' => __('api.tenant.verification.code.success')
            ],
            'is_forbidden' => false
        ]);
    }

    /**
     *  Check validity of tenant's forget password verification code
     *
     *  @param Request $request
     *
     *  @return JsonResponse
     */
    public function forgetPasswordCheck(TenantForgetPasswordCheckRequest $request): JsonResponse
    {
        $validator = $request->failedValidator;
        $phoneNumber = $request->phone_number;

        if (!is_null($validator) && $validator->fails()) {
            $messages = [];

            foreach ($validator->errors()->getMessages() as $key => $message) {
                $messages[$key] = $message[0];
            }

            return ApiResponse::responseData([
                'status' => false,
                'messages' => $messages,
                'is_forbidden' => false
            ]);
        }

        $code = ActivationCode::where([
            ['phone_number', '=', $phoneNumber],
            ['for', '=', ActivationCodeType::TENANT_FORGET_PASSWORD]
        ])
            ->whereNotExpired()
            ->whereCode($request->input('verification_code', ''));

        if (!$code->exists()) {
            return ApiResponse::responseData([
                'status' => false,
                'messages' => [
                    'verification_code' => __('api.tenant.verification.code.invalid')
                ],
                'is_forbidden' => false
            ]);
        }

        $user = User::where('phone_number', $request->phone_number)->get();
        $response = $this->validateUser($user);
        if (!is_null($response)) {
            return $response;
        }

        return ApiResponse::responseData([
            'status' => true,
            'meta' => [
                'message' => __('api.tenant.verification.code.success')
            ],
            'is_forbidden' => false
        ]);
    }

    /**
     *  Change tenant's password as part of forget password process
     *
     *  @param Request $request
     *
     *  @return JsonResponse
     */
    public function forgetPassword(TenantForgetPasswordRequest $request): JsonResponse
    {
        $validator = $request->failedValidator;

        if (!is_null($validator) && $validator->fails()) {
            $messages = [];

            foreach ($validator->errors()->getMessages() as $key => $message) {
                $messages[$key] = $message[0];
            }

            return ApiResponse::responseData([
                'status' => false,
                'messages' => $messages,
                'is_forbidden' => false
            ]);
        }

        $user = User::where('phone_number', $request->phone_number)->get();
        $response = $this->validateUser($user);
        if (!is_null($response)) {
            return $response;
        }

        $user->first()->update(['password' => Hash::make($request->password)]);

        ActivationCode::deleteLastCode($request->get('phone_number'));

        return ApiResponse::responseData([
            'status' => true,
            'meta' => [
                'message' => 'success'
            ],
            'is_forbidden' => false
        ]);
    }

    private function validateUser($user)
    {
        if ($user->where('is_owner', 'false')->count() > 1 || 
            $user->where('is_owner', 'true')->count() > 0 && 
            $user->where('is_owner', 'false')->count() > 0) {
            $title = __('api.input.forget_password.failed');
            $message = __('api.input.forget_password.tenant.duplicate');
        } else if ($user->where('is_owner', 'true')->count() > 0) {
            $title = __('api.input.forget_password.send_failed');
            $message = __('api.input.forget_password.wrong_section');
        } else if ($user->count() === 0) {
            $title = __('api.input.forget_password.send_failed');
            $message = __('api.input.forget_password.not_found');
        } else if (!is_null($user->first()->social)) {
            $title = __('api.input.forget_password.send_failed');
            $message = __('api.input.forget_password.social');
        } else {
            return;
        }

        return ApiResponse::responseData([
            'status' => false,
            'meta' => [
                'message' => $message,
            ],
            'title' => $title,
            'is_forbidden' => true // Indicate this special case of error
        ]);
    }

    /**
     * Parse error message to single flat array
     * 
     * @param array $errorMesssage
     * 
     * @return array
     */
    private function parseErrorMessage(array $errorMessage): array
    {
        $validationError = [];
        foreach ($errorMessage as $key => $val) {
            $msgVal = [];
            foreach ($val as $v) {
                $msgVal[] = $v;
            }
            $validationError[$key] = $val[0];
        }

        return $validationError;
    }

     /**
     * Due for backward compatibility UG squad create this function
     * It's consumed by web application (not Android + iOS)
     * 
     * The Web App just need response array from meta object
     * But the Android and App need outside from meta object
     * 
     * @param array $validationError
     * 
     * @return array
     */
    private function parseErrorMessageForWebApp(\Illuminate\Support\MessageBag $errorMessage): array
    {
        $validationError = [];
        foreach ($errorMessage->getMessages() as $key => $val) {
            $msgVal = [];
            foreach ($val as $v) {
                $msgVal[] = $v;
            }
            $validationError[$key] = [$val[0]];
        }

        return $validationError;
    }

    /**
     *  Change tenant password
     *
     *  @param TenantChangePasswordRequest $request
     *
     *  @return JsonResponse
     */
    public function changePassword(TenantChangePasswordRequest $request): JsonResponse
    {
        $validator = $request->failedValidator;

        if (!is_null($validator) && $validator->fails()) {
            $validatorError = $this->parseErrorMessageForWebApp($validator->errors());
            return ApiResponse::responseData([
                'status' => false,
                'meta' => [
                    'messages' => $validatorError,
                ],
                'message' => $this->parseErrorMessage($validator->errors()->getMessages()),
            ]);
        }

        $user = $this->user();
        $user->password = Hash::make($request->new_password);
        $user->save();

        return ApiResponse::responseData(['status' => true]);
    }

    /**
     *  Update user device data and log user on the device
     *
     *  @param array $deviceDetail UserDevice detail data
     *  @param User $user User to log in as
     *
     *  @return UserDevice|null
     */
    private function loginOnDevice(array $deviceDetail, User $user)
    {
        $userDevice = empty(ApiHelper::activeDeviceId()) ? null : ApiHelper::activeDeviceId();
        $userDevice = UserDevice::insertOrUpdate($deviceDetail, $user->id, $userDevice);

        if (!empty($userDevice->id)) {
            $device = UserDevice::find($userDevice->id);
            $device->login_status = 'login';
            $device->save();

            $this->trackLogin($userDevice, $user);

            $user->last_login = $userDevice->id;
            $user->save();
        }

        return $userDevice;
    }

    /**
     *  Track user login on user device
     *
     *  @param UserDevice $userDevice Device the user logged in on
     *  @param User $user User logged in as
     *
     *  @return void
     */
    private function trackLogin(UserDevice $userDevice, User $user)
    {
        $agent = new Agent();
        $deviceType = Tracking::checkDevice($agent);
        UserLoginTracker::saveTracker($user, $deviceType, $userDevice, $agent->device());
    }

    /**
     *  Check if code request was too frequent
     *
     *  @param string $phoneNumber Receiver of the code
     *  @param int $minutesPerRequest Number of minutes user need to wait before a new request
     */
    private function isTooFrequent(int $minutesPerRequest)
    {
        $isAlreadyRequested = !empty($this->lastCodeRequested);
        
        // Check if user already has a code and is requesting a new one
        if ($isAlreadyRequested) {
            if ($this->lastCodeRequested->created_at->diffInMinutes(Carbon::now(), false) < $minutesPerRequest) {
                return true;
            }
        }
    }

    /**
     *  Delete existing code sent
     *
     *  @param string $phoneNumber Receiver of the previous code
     *
     *  @return void
     */
    private function deletePreviousCode(string $phoneNumber)
    {
        $isAlreadyRequested = !empty($this->lastCodeRequested);

        if ($isAlreadyRequested) {
            // If code has not expired but user requested new code, delete the old one;
            ActivationCode::deleteLastCode($phoneNumber);
        }
    }

    /**
     *  Send veritifcation code to user
     *
     *  @param User $user Receiver of the notification
     *  @param ActivationCode $activationCode Code to send
     *  @return void
     */
    private function sendVerificationCode(User $user, ActivationCode $activationCode)
    {
        Notification::send($user, new PhoneNumberVerification($activationCode));
    }

    /**
     *  Handle verificationg user and disposal of verification code
     *
     *  @param User
     *
     *  @return void
     */
    private function handleVerification(User $user)
    {
        $userVerification = new UserVerificationAccount();
        $userVerification->is_verify_phone_number = true;
        $userVerification->is_verify_owner = false;
        $user->user_verification_account()->save($userVerification);
        ActivationCode::deleteLastCode($user->phone_number);
    }

    /**
     *  Validate user duplication and user as tenant
     *
     *  @param ForgetPasswordIdentifierRequest $request
     *  @return object
     */
    private function validateUserIdentifier(ForgetPasswordIdentifierRequest $request): object
    {
        $phoneNumber = $request->phone_number ?? null;
        $phoneCount = !is_null($phoneNumber) ? User::where('phone_number', $phoneNumber)->count() : 0;

        if ($phoneCount > 1) {
            return ApiResponse::responseData([
                'message' => __('api.input.forget_password.duplicate.message'),
                'contact' => [
                    'cta_message' => __('api.input.forget_password.duplicate.cta_message'),
                    'wa_number' => __('api.input.forget_password.duplicate.wa_number'),
                    'pretext' => __('api.input.forget_password.duplicate.pretext', ['identifier' => $phoneNumber]),
                ]
            ], false, false);
        }

        if (!is_null($phoneNumber)) {
            $user = User::where('phone_number', $request->phone_number)->where('is_owner', 'false')->first();
            $message = __('api.input.phone_number.tenant_not_found');
        }

        if (is_null($user)) {
            return ApiResponse::responseData([
                'message' => $message,
                'contact' => null,
            ], false, false);
        }

        return $user;
    }

    /**
     *  Retrieve and format user identifier data for verification method options
     *
     *  @param User $user
     *  @return array
     */
    private function userIdentifierData(User $user): array
    {
        $data = [
            array_merge(VerificationMethod::SMS, ['value' => $user->phone_number]),
            array_merge(VerificationMethod::WA, ['value' => $user->phone_number])
        ];

        return $data;
    }
}
