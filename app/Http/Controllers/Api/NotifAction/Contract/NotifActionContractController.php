<?php
namespace App\Http\Controllers\Api\NotifAction\Contract;

use App\Channel\MoEngage\MoEngageEventDataApi;
use App\Entities\Mamipay\MamipayInvoice;
use App\Exceptions\CustomErrorException;
use App\Http\Controllers\Api\BaseController;
use App\Http\Helpers\ApiResponse as Api;
use App\Http\Helpers\BookingUserHelper;
use App\Jobs\MoEngage\SendMoEngageReportNotificationActionMarkPaymentBillQueue;
use App\Jobs\MoEngage\SendMoEngageReportNotificationActionPaymentReminderQueue;
use App\Jobs\MoEngage\SendMoEngageReportNotificationActionShowListBillQueue;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Illuminate\Http\JsonResponse;
use Exception;
use Illuminate\Http\Request;

class NotifActionContractController extends BaseController
{

    public function showListBill(Request $request, $invoiceId): JsonResponse
    {
        // force default response to false
        $response = $this->makeDefaultResponse();
        try {
            $user = $this->user();
            if (is_null($user))
                throw new CustomErrorException('User Not Found');

            $invoice = MamipayInvoice::with(['contract.type_kost.room'])->find($invoiceId);
            if (!$invoice)
                throw new CustomErrorException('Invoice Not Found');

            $contract = optional($invoice)->contract;
            if ($contract === null)
                throw new CustomErrorException('Contract Not Found');

            if ($contract->owner_id !== $user->id)
                throw new CustomErrorException('Access Denied');

            $interface = BookingUserHelper::getInterfaceForMoEngage();

            SendMoEngageReportNotificationActionShowListBillQueue::dispatch($invoice, $user, $interface);

            $response = [
                'status' => true
            ];

        } catch (CustomErrorException $e) {
            $response['meta']['message'] = $e->getMessage();
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
            $response['meta']['message'] = $e->getMessage();
        }
        return Api::responseData($response);
    }

    public function markPayment(Request $request, $invoiceId): JsonResponse
    {
        // force default response to false
        $response = $this->makeDefaultResponse();
        try {
            $user = $this->user();
            if (is_null($user))
                throw new CustomErrorException('User Not Found');

            $invoice = MamipayInvoice::with(['contract.type_kost.room'])->find($invoiceId);
            if (!$invoice)
                throw new CustomErrorException('Invoice Not Found');

            $contract = optional($invoice)->contract;
            if ($contract === null)
                throw new CustomErrorException('Contract Not Found');

            if ($contract->owner_id !== $user->id)
                throw new CustomErrorException('Access Denied');

            $interface = BookingUserHelper::getInterfaceForMoEngage();

            SendMoEngageReportNotificationActionMarkPaymentBillQueue::dispatch($invoice, $user, $interface);

            $response = [
                'status' => true
            ];

        } catch (CustomErrorException $e) {
            $response['meta']['message'] = $e->getMessage();
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
            $response['meta']['message'] = $e->getMessage();
        }
        return Api::responseData($response);
    }

    public function reminderPayment(Request $request, $invoiceId): JsonResponse
    {
        // force default response to false
        $response = $this->makeDefaultResponse();
        try {
            $user = $this->user();
            if (is_null($user))
                throw new CustomErrorException('User Not Found');

            $invoice = MamipayInvoice::with(['contract.type_kost.room'])->find($invoiceId);
            if (!$invoice)
                throw new CustomErrorException('Invoice Not Found');

            $contract = optional($invoice)->contract;
            if ($contract === null)
                throw new CustomErrorException('Contract Not Found');

            if ($contract->owner_id !== $user->id)
                throw new CustomErrorException('Access Denied');

            $interface = BookingUserHelper::getInterfaceForMoEngage();

            SendMoEngageReportNotificationActionPaymentReminderQueue::dispatch($invoice, $user, $interface);

            $response = [
                'status' => true
            ];

        } catch (CustomErrorException $e) {
            $response['meta']['message'] = $e->getMessage();
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
            $response['meta']['message'] = $e->getMessage();
        }
        return Api::responseData($response);
    }

    /**
     * Function to return default response
     * @return array
     */
    private function makeDefaultResponse(): array
    {
        return [
            'status' => false,
            'meta' => ['message' => 'General Error']
        ];
    }
}