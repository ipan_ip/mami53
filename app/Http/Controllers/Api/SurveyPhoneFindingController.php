<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseController;
use App\Http\Helpers\ApiResponse as Api;
use Validator;
use App\Entities\Feedback\SurveyPhoneFinding;

/**
 * #growthsprint1
 * Currently doesn't use repository pattern, since this will be only single function at the moment
 */
class SurveyPhoneFindingController extends BaseController
{

    protected $validationMessages = [
        'is_difficult.required' => 'Jawaban harus diisi',
    ];

    public function __construct()
    {
        parent::__construct();
    }

    public function submitSurvey(Request $request)
    {   
        $validator = Validator::make($request->all(), [
            'phone_found' => 'required|bool'
        ], $this->validationMessages);

        if($validator->fails()) {
            return Api::responseData([
                'status'=>false,
                'meta' => [
                    'message' => implode('. ', $validator->errors()->all()),
                    'messages' => $validator->errors()->all()
                ]
            ]);
        }

        $user = $this->user();

        $survey = new SurveyPhoneFinding;
        $survey->user_id = $user ? $user->id : null;
        $survey->phone_found = $request->phone_found;
        $survey->save();

        return Api::responseData([
            'status' => true
        ]);
    }
}