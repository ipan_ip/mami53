<?php

namespace App\Http\Controllers\Api;

use App\Entities\Level\KostLevel;
use App\Http\Controllers\Api\BaseController;
use App\Http\Helpers\ApiResponse;
use App\Http\Requests\Owner\ListKostsGoldplusRequest;
use App\Presenters\RoomPresenter;
use App\Repositories\RoomRepository;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Illuminate\Http\JsonResponse;
use RuntimeException;

/**
 * Controller to handle api related to goldplus.
 */
class GoldplusController extends BaseController
{
    protected $roomRepository;

    public function __construct(RoomRepository $roomRepository)
    {
        parent::__construct();
        $this->roomRepository = $roomRepository;
    }

    /**
     * API to return the list of owned goldplus kost.
     *
     * @param ListKostsGoldplusRequest $request
     */
    public function getListKostsGoldplus(ListKostsGoldplusRequest $request): JsonResponse
    {
        $user = $this->user();
        if (empty($user)) {
            Bugsnag::notifyException(new RunTimeException('User data is null.'));
            return ApiResponse::responseData([], false, false, 401);
        }

        if ($request->isValidationFailed()) {
            return ApiResponse::responseData([
                'messages' => $request->failedValidator->errors(),
            ], false, false, 400);
        }

        if (empty($request->input('gp_status'))) {
            // since iOS 3.14.0 intermittently send empty string instead of 0 :sad:
            // we need to replace the request value with 0
            $request->merge([
                'gp_status' => 0,
            ]);
        }

        // return the index of gp level in zero-based index, so we should increase the index by 1 
        // if the value is found.
        $level = array_search($request->input('gp_status'), KostLevel::getGoldplusLevelIds());
        if ($level !== false) {
            $level += 1;
        } else {
            $level = 0;
        }

        $gpStatus = KostLevel::getGoldplusLevelIdsByLevel($level);
        $rooms = $this->roomRepository->getGoldplusKosts($user->id, $gpStatus);
        
        return ApiResponse::responseData([
            'status' => true,
            'data' => (new RoomPresenter('goldplus-list'))->present($rooms)['data'],
            'has_more' => $rooms->hasMorePages(),
        ]);
    }

    /**
     * Get the list of available goldplus level filters for list of goldplus kost.
     */
    public function getGoldplusFilterOptions(): JsonResponse
    {
        $response = [
            __('api.goldplus.filter-all'),
        ];
        foreach (__('api.goldplus.filters') as $goldPlusLevelId => $translation) {
            $response[] = [
                'key' => (int) $goldPlusLevelId,
                'value' => $translation['value'],
            ];
        }
        return ApiResponse::responseData([
            'status' => true,
            'data' => $response,
        ]);
    }

    /**
     * Get list of wording for GP Businness Onboard
     * 
     * @return JsonResponse
     */
    public function getGoldplusOnboardingWords(): JsonResponse
    {
        $user = $this->user();
        if (empty($user)) {
            return ApiResponse::responseData([
                'status' => false,
                'meta'   => [
                    'response_code' => 200,
                    'code'          => 333,
                    'severity'      => 'Error',
                    'message'       => 'Authorization: Sign-in is required.'
                ]
            ], false, false, 401);
        }

        return ApiResponse::responseData([
            'status' => true,
            'data' => __('api.goldplus.onboarding-gp-business'),
        ]);
    }
}
