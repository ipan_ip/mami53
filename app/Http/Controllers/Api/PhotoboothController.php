<?php
namespace App\Http\Controllers\Api;

use App\Http\Helpers\ApiResponse as Api;
use Illuminate\Http\Request;
use Validator;
use Auth;

use App\Entities\Event\PhotoboothSubscriber;
use App\Entities\Event\PhotoboothEvent;
use App\Entities\Media\Media;
use Carbon\Carbon;


class PhotoboothController extends BaseController
{
    public function register(Request $request)
    {
        $user = $this->user();

        if (!$user) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Silakan login terlebih dulu'
                ]
            ]);
        }

        $photoboothEvent = PhotoboothEvent::with('subscribers')->find($request->university);

        if (!$photoboothEvent) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Event Photobooth tidak ditemukan'
                ]
            ]);
        } elseif($photoboothEvent->start_date->gt(Carbon::now())) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Event Photobooth belum dimulai'
                ]
            ]);
        } elseif($photoboothEvent->end_date->lt(Carbon::now())) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Event Photobooth telah selesai'
                ]
            ]);
        } elseif($photoboothEvent->subscriber_count >= $photoboothEvent->quota) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Quota photobooth telah habis'
                ]
            ]);
        }

        $validator = Validator::make($request->all(),[
            'name' => 'required|max:250',
            'email' => 'required|email|max:190',
            'phone_number' => 'required|max:190',
            'university' => 'required'
        ], [
            'name.required' => 'Nama harus diisi',
            'name.max' => 'Nama maksimal :max karakter',
            'email.required' => 'Email harus diisi',
            'email.email' => 'Format email tidak valid',
            'email.max' => 'Email maksimal :max karakter',
            'phone_number.required' => 'Nomor Telepon harus diisi',
            'phone_number.max' => 'Nomor Telepon maksimal :max karakter',
            'university.required' => 'Nama Universitas harus diisi'
        ]);

        $validator->after(function($validator) use ($request, $user) {
            $existingSubscriber = PhotoboothSubscriber::where('user_id', $user->id)
                                        ->whereNull('photo_id')
                                        ->first();

            if ($existingSubscriber) {
                $validator->errors()->add('name', 'Anda sudah melakukan registrasi sebelumnya');
            }
        });

        if($validator->fails()) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => implode(', ', $validator->errors()->all()),
                    'messages' => $validator->errors()->all()
                ]
            ]);
        }

        $subscriber = new PhotoboothSubscriber;
        $subscriber->user_id = $user->id;
        $subscriber->photobooth_event_id = $photoboothEvent->id;
        $subscriber->name = $request->name;
        $subscriber->email = $request->email;
        $subscriber->phone_number = $request->phone_number;
        $subscriber->university = $photoboothEvent->university;
        $subscriber->save();

        $subscriber->generateVoucher();

        return Api::responseData([
            'status' => true,
            'subscriber' => [
                'name' => $subscriber->name,
                'email' => $subscriber->email,
                'phone_number' => $subscriber->phone_number,
                'university' => $subscriber->university,
                'voucher_code' => $subscriber->voucher_code
            ]
        ]);
    }

    public function upload(Request $request)
    {
        $subscriber = PhotoboothSubscriber::where('voucher_code', 
                            is_null($request->voucher_code) ? 'x' : $request->voucher_code)
                        ->first();

        if (!$subscriber) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Kode Voucher tidak ditemukan.',
                    'messages' => ['Kode Voucher tidak ditemukan.']
                ]
            ]);
        }

        $validator = Validator::make($request->all(), [
            'photo' => 'required|image',
            'voucher_code' => 'required'
        ]);

        $validator->after(function($validator) use ($request, $subscriber) {
            if ($subscriber && !is_null($subscriber->photo_id)) {
                $validator->erros()->add('photo', 'Kode Voucher ini sudah mempunyai foto');
            }
        });

        if ($validator->fails()) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => implode(', ', $validator->errors()->all()),
                    'messages' => $validator->errors()->all()
                ]
            ]);
        }

        $newPhoto = Media::postMedia(
            $request->photo,
            'upload',
            null,
            null,
            null,
            'photobooth',
            false,
            null,
            false
        );

        if ($newPhoto['id'] == 0) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Foto gagal diupload',
                    'messages' => ['Foto gagal diupload']
                ]
            ]);
        }

        $subscriber->photo_id = $newPhoto['id'];
        $subscriber->save();

        return Api::responseData([
            'status' => true
        ]);
    }
}
