<?php

namespace App\Http\Controllers\Api\Search;

use Bugsnag;
use Exception;
use Illuminate\Http\Request;
use Agent;

use App\Entities\Search\InputKeyword;
use App\Http\Controllers\Api\BaseController;
use App\Http\Helpers\ApiResponse as Api;
use App\Libraries\ElasticsearchApi;
use App\Entities\Tracker\SearchKeyword;

class SuggestionController extends BaseController
{
    public const DataSourceNone = 'none';
    public const DataSourceClassic = 'classic';
    public const DataSourceElasticSearch = 'es';

    protected $response;

    public function suggest(Request $request)
    {
        $this->logSearchKeyword($request->q, $request);

        $this->response = [
            'keyword'           => $request->q,
            'optimizedKeyword'  => $request->q,
            'areas'             => [],
            'areaDataSource'    => self::DataSourceNone,
            'landings'          => [],
            'landingDataSource' => self::DataSourceNone,
            'rooms'             => [],
            'roomDataSource'    => self::DataSourceNone,
        ];

        try {
            $this->response = self::GetSuggestions($request->q);
        } catch (Exception $exception) {
            Bugsnag::notifyException($exception);
        }

        return Api::responseData(
            [
                'data' => $this->response,
            ]
        );
    }

    private function logSearchKeyword(string $keyword, Request $request)
    {
        // Logging is OPTIONAL, it should be silent all the time
        try
        {
            $keyword = trim($keyword);
            if (empty($keyword)) 
                return;

            $user = $this->user();
            $userId = $user === null ? 0 : $user->id;

            $route = $request->getPathInfo();
            
            $device = $this->device();
            if (is_null($device) === true) // request not from app
            {
                $os = Agent::platform();
                $version = $os ? Agent::version($os) : '';
            }
            else // request from app
            {
                $os = $device->platform;
                $version = $device->app_version_code;
            }

            // Log Search Keyword
            SearchKeyword::log($keyword, $route, $os, $version, $userId);
        }
        catch (Exception $e)
        {
            Bugsnag::notifyError('Info', $e->getMessage());
        }
    }

    public static function GetSuggestions($keyword)
    {
        $keyword = strtolower(trim($keyword));

        // Response data initial state
        $responseData = [
            'keyword'           => $keyword,
            'optimizedKeyword'  => $keyword,
            'areas'             => [],
            'areaDataSource'    => self::DataSourceNone,
            'landings'          => [],
            'landingDataSource' => self::DataSourceNone,
            'rooms'             => [],
            'roomDataSource'    => self::DataSourceNone,
        ];

        // Quickly return initial response if there's no keyword at all!
        if (!$keyword) {
            return $responseData;
        }

        ////////////////////////////////////////////////////////////////////////////
        // 1-1. Suggest Areas looking up in DB
        ////////////////////////////////////////////////////////////////////////////

        $cachedKeyword = InputKeyword::with('suggestions.geolocation')
            ->where('keyword', 'LIKE', $keyword)
            ->first();

        if ($cachedKeyword) {
            $suggestions = [];

            // Add +1 to `total` column
            $cachedKeyword->total = $cachedKeyword->total + 1;
            $cachedKeyword->save();

            foreach ($cachedKeyword->suggestions as $suggestion) {
                $result = [
                    'title'     => $suggestion->suggestion,
                    'area'      => $suggestion->area,
                    'latitude'  => $suggestion->latitude,
                    'longitude' => $suggestion->longitude,
                    'place_id'  => $suggestion->place_id,
                ];
                $suggestions[] = $result;
            }

            $responseData['areas']          = $suggestions;
            $responseData['areaDataSource'] = self::DataSourceClassic;
        }


        // Optimize the $keyword first, before further processing
        $optimizedKeyword                 = self::OptimizeKeyword($keyword);
        $responseData['optimizedKeyword'] = $optimizedKeyword;

        // Quickly return initial response if there's no optimizedKeyword at all!
        if (!$optimizedKeyword) {
            return $responseData;
        }

        ////////////////////////////////////////////////////////////////////////////
        // 1-2. Suggest Areas using Elastic Search
        ////////////////////////////////////////////////////////////////////////////

        // Initiate ElasticSearch helper instance
        $elasticSearchApi = new ElasticsearchApi();

        if (empty($responseData['areas'])) {
            // 1st Formula ~ Using AND operator
            $searchFormulaType1 = '{
                "query": {
                    "bool": {
                        "must": {
                            "multi_match": {
                                "query": "' . $optimizedKeyword . '",
                                "fuzziness": "AUTO",
                                "fields": [
                                    "keyword^10",
                                    "suggestion"
                                ],
                                "type": "most_fields",
                                "operator": "AND"
                            }
                        }
                    }
                },
                "size": "5"
            }';

            try {
                $areaRequest = $elasticSearchApi->search('keyword', $searchFormulaType1);

                if ($areaRequest['hits']['total'] > 0) {
                    $suggestions = [];

                    foreach ($areaRequest['hits']['hits'] as $suggestion) {
                        $suggestions[] = [
                            'keyword'   => $suggestion['_source']['keyword'],
                            'title'     => $suggestion['_source']['suggestion'],
                            'area'      => $suggestion['_source']['area'],
                            'latitude'  => $suggestion['_source']['latitude'],
                            'longitude' => $suggestion['_source']['longitude'],
                            'place_id'  => $suggestion['_source']['place_id'],
                        ];
                    }

                    $responseData['areas']          = $suggestions;
                    $responseData['areaDataSource'] = self::DataSourceElasticSearch . " (type 1)";
                } // 2nd Formula ~ Using OR operator
                else {
                    $searchFormulaType2 = '{
                        "query": {
                            "bool": {
                                "must": {
                                    "multi_match": {
                                        "query": "' . $optimizedKeyword . '",
                                        "fuzziness": "3",
                                        "fields": [
                                            "keyword^10",
                                            "suggestion^2",
                                            "area"
                                        ],
                                        "type": "most_fields",
                                        "operator": "OR"
                                    }
                                }
                            }
                        },
                        "size": "5"
                    }';

                    try {
                        $areaRequest = $elasticSearchApi->search('keyword', $searchFormulaType2);
                        $suggestions = [];

                        if ($areaRequest['hits']['total'] > 0) {
                            foreach ($areaRequest['hits']['hits'] as $suggestion) {
                                $suggestions[] = [
                                    'keyword'   => $suggestion['_source']['keyword'],
                                    'title'     => $suggestion['_source']['suggestion'],
                                    'area'      => $suggestion['_source']['area'],
                                    'latitude'  => $suggestion['_source']['latitude'],
                                    'longitude' => $suggestion['_source']['longitude'],
                                    'place_id'  => $suggestion['_source']['place_id'],
                                ];
                            }
                        }

                        $responseData['areas']          = $suggestions;
                        $responseData['areaDataSource'] = self::DataSourceElasticSearch . " (type 2)";
                    } catch (Exception $e) {
                        Bugsnag::notifyException($e);
                    }
                }
            } catch (Exception $e) {
                Bugsnag::notifyException($e);
            }
        }

        ////////////////////////////////////////////////////////////////////////////
        // 2. Suggest Landings
        ////////////////////////////////////////////////////////////////////////////
        $words = explode(" ", $optimizedKeyword);
        $terms = '[';
        $i     = 1;
        foreach ($words as $k) {
            if ($i < count($words)) {
                $terms .= '{"term": {"tags": "' . $k . '"}},';
            } else {
                $terms .= '{"term": {"tags": "' . $k . '"}}';
            }
            $i++;
        }
        $terms .= ']';

        $searchFormula = '{
            "query": {
                "bool": {
                    "must": ' . $terms . '
                }
            },
            "size": "5"
        }';

        try {
            $landingRequest = $elasticSearchApi->search('landing', $searchFormula);
            $suggestions    = [];

            if ($landingRequest['hits']['total'] > 0) {
                foreach ($landingRequest['hits']['hits'] as $suggestion) {
                    $suggestions[] = [
                        'title'        => $suggestion['_source']['title'],
                        'slug'         => $suggestion['_source']['slug'],
                        'tags'         => $suggestion['_source']['tags'],
                        'coordinate'   => $suggestion['_source']['coordinate'],
                        'price'        => $suggestion['_source']['price'],
                        'array_price'  => $suggestion['_source']['array_price'],
                        'gender'       => $suggestion['_source']['gender'],
                        'array_gender' => $suggestion['_source']['array_gender'],
                        // 'highlight' => $suggestion->highlight->title,
                    ];
                }
            }

            $responseData['landings']          = $suggestions;
            $responseData['landingDataSource'] = self::DataSourceElasticSearch;
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
        }

        ////////////////////////////////////////////////////////////////////////////
        // 3. Suggest Rooms
        ////////////////////////////////////////////////////////////////////////////

        // Search by code first
        $searchFormula = '{
                "query": {
                    "bool": {
                        "should": [ 
                            {
                                "match": {
                                    "code": "' . $optimizedKeyword . '"
                                }
                            }
                        ]
                    }
                },
                "size": "1"
            }';

        try {
            $suggestions = [];
            $roomRequest = $elasticSearchApi->search('room', $searchFormula);

            /*
             * To support ElasticSearch v7.9.2
             * Ref Task: https://mamikos.atlassian.net/browse/BG-3605
             */
            $isAPIVersion7 = is_array($roomRequest['hits']['total']);
            $count = $isAPIVersion7 ? (int) $roomRequest['hits']['total']['value'] : (int) $roomRequest['hits']['total'];

            if ($count < 1) {
                // Then search by multi_match method
                $searchFormula2 = '{ 
                        "query": {
                            "bool": {
                                "must": {
                                    "multi_match": {
                                        "query": "' . $optimizedKeyword . '",
                                        "fuzziness": "AUTO",
                                        "fields": [
                                            "title^5",
                                            "slug"
                                        ],
                                        "type": "most_fields",
                                        "operator": "AND"
                                    }
                                }
                            }
                        },
                        "size": "5"
                    }';

                $roomRequest = $elasticSearchApi->search('room', $searchFormula2);
            }

            $count = $isAPIVersion7 ? (int) $roomRequest['hits']['total']['value'] : (int) $roomRequest['hits']['total'];

            if ($count > 0) {
                foreach ($roomRequest['hits']['hits'] as $suggestion) {
                    $suggestions[] = [
                        'title' => $suggestion['_source']['title'],
                        'slug' => $suggestion['_source']['slug'],
                        'room_id' => $suggestion['_source']['room_id'],
                        'apartment_id' => $suggestion['_source']['apartment_id'],
                        'gender' => $suggestion['_source']['gender'],
                        'property_type' => $suggestion['_source']['type'],
                        'code' => isset($suggestion['_source']['code']) ? $suggestion['_source']['code'] : ''
                        // 'highlight' => $suggestion->highlight->title,
                    ];
                }
            }

            $responseData['rooms']          = $suggestions;
            $responseData['roomDataSource'] = self::DataSourceElasticSearch;
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
        }

        return $responseData;
    }

    private static function OptimizeKeyword($keyword)
    {
        // Sanitize query
        // a. First, replace all special chars with whitespaces
        // b. Then, remove double or more whitespaces if any
        $keyword = preg_replace(['/\s{2,}/', '/[\t\n]/'], ' ', preg_replace('/[^ \w-]/', ' ', $keyword));

        $keyword = self::RemoveIgnorableWordsFrom($keyword);

        $keyword = trim($keyword);
        return $keyword;
    }

    // Ignored Keywords Filter: Keyword will be stripped down if is listed
    private static function RemoveIgnorableWordsFrom($keyword)
    {
        try {
            $path              = storage_path() . "/json/ignored-keywords.json";
            $ignorableKeywords = json_decode(file_get_contents($path), true);

            /*
             * Extra ignorable-keywords ONLY for production
             * Ref Task: https://mamikos.atlassian.net/browse/BG-2164
             */
            if (config('app.env') === 'production') {
                $extraPath = storage_path() . "/json/ignored-keywords-prod.json";
                $ignorableKeywordsForProduction = json_decode(file_get_contents($extraPath), true);
                $ignorableKeywords = array_merge($ignorableKeywords, $ignorableKeywordsForProduction);
            }

            $words        = explode(" ", $keyword);
            $allowedWords = array_diff($words, $ignorableKeywords);

            return implode(' ', $allowedWords);
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
        }

        return $keyword;
    }
}
