<?php

namespace App\Http\Controllers\Api\Search;

use App\Entities\Search\InputKeyword;
use App\Http\Controllers\Api\BaseController;
use App\Http\Helpers\ApiResponse as Api;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class LocationSuggestionController extends BaseController
{
    /**
     * @param Request $request
     * @return JsonResponse|null
     */
    public function suggestAutocomplete(Request $request)
    {
        if (
            !$request->filled('criteria')
            || empty($request->criteria)
        ) {
            $this->sendResponse(null);
        }

        $keyword = InputKeyword::with('suggestions')
            ->where('keyword', (string)$request->criteria)
            ->first();

        if (is_null($keyword)) {
            return $this->sendResponse(null);
        }

        $response = $keyword->getSuggestions();

        return $this->sendResponse($response);
    }

    private function sendResponse($returnData)
    {
        return Api::responseData(
            [
                'data' => $returnData
            ]
        );
    }
}