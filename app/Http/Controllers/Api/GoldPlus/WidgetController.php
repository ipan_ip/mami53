<?php

namespace App\Http\Controllers\Api\GoldPlus;

use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use App\Repositories\RoomRepository;
use App\Entities\GoldPlus\Submission;
use App\Entities\Level\KostLevel;
use App\Http\Helpers\ApiResponse as Api;
use App\Http\Controllers\Api\BaseController;
use App\Entities\Owner\Goldplus\OwnerGoldplusStatistic;

/**
 * class WidgetController
 * 
 * This class purpose for Controller to handle GoldPlus Widget.
 * 
 * @author Ricky Setiawan<ricky.s@mamiteam.com>
 */
class WidgetController extends BaseController
{
    protected $roomRepository;

    public function __construct(RoomRepository $roomRepository)
    {
        parent::__construct();
        $this->roomRepository = $roomRepository;
    }

    /**
     * API to return the list of accumulated statistic record of GoldPlus Kost 
     * from yesterday 00:00:00 until today 00:00:00
     * 
     * @return JsonResponse
     */
    public function getWidget(): JsonResponse
    {
        $user = $this->user();
        if (is_null($user)) {
            throw new \RuntimeException('User data accidentally was null');   
        }
        
        $today = Carbon::parse(date('Y-m-d'));
        $yesterday = $today->copy()->subDay();
        $statistics = ['visit', 'unique_visit', 'chat', 'favorite'];

        $data = OwnerGoldplusStatistic::where('created_at', '>=', $yesterday)
            ->where('created_at', '<=', $today)
            ->where('owner.user_id', $user->id)
            ->get();
            
        $filteredData = [];
        foreach ($statistics as $statistic) {
            $filteredData[$statistic] = $data
                ->filter(function ($item) use($statistic) {
                    return $item->statistic['key'] === $statistic;
                })->map(function ($item) {
                    return $item->statistic;
                });
        }

        $response = [];
        foreach ($filteredData as $statistic => $items) {
            $total = 0;
            foreach ($items as $item) {
                $total += $item['total'];
            }
            $response[] = [
                'label' => __('api.goldplus.widget.label.' . $statistic),
                'value' => $total,
            ];
        }

        $date = now()->subDay();

        $day = $date->format('j');
        $month = __('library.locale.id.M.' . $date->format('n'));
        $year = $date->format('Y');

        $date = implode(' ', [$day, $month, $year]);

        return Api::responseData([
            'status' => true,
            'data' => [
                'date' => $date,
                'statistic' => $response,
            ],
        ]);
    }

    /**
     * API to return the number of goldplus kost count information
     * 
     * @return JsonResponse
     */
    public function getWidgetRoomCount(): JsonResponse
    {
        $user = $this->user();
        if (is_null($user) || empty($user)) {
            throw new \RuntimeException(__('goldplus-statistic.error.runtime_exception.user_id_was_null'));   
        }

        $goldplusRooms = collect($this->roomRepository->getGoldplusAcquisitionKosts($user->id, 0)->items());

        $goldplusRoomsActive = $goldplusRooms->filter(function ($room) {
            if (!is_null($room->potential_property_onreview)) {
                return !is_null($room->level->whereIn('id', KostLevel::getNewGoldplusLevelIds())->first());
            }
            return !is_null($room->level->whereIn('id', KostLevel::getGoldplusLevelIdsByLevel())->first());
        })->count();
            
        $goldplusRoomsInReview = $goldplusRooms->filter(function ($room) {
            if (!is_null($room->potential_property_onreview)) {
                if (is_null($room->level->whereIn('id', KostLevel::getGoldplusLevelIdsByLevel())->first())) {
                    return true;
                }
                return is_null($room->level->whereIn('id', KostLevel::getNewGoldplusLevelIds())->first());
            }
            return false;
        })->count();

        $widgets['active'] = [
            'label' => __('api.goldplus.widget.label.active-goldplus'),
            'key' => 1,
            'value' => $goldplusRoomsActive,
            'button_visible' => false
        ];
        $widgets['in-review'] = [
            'label' => __('api.goldplus.widget.label.in-review-goldplus'),
            'key' => 2,
            'value' => $goldplusRoomsInReview,
            'button_visible' => true
        ];

        if ($goldplusRoomsInReview <= 0) {
            unset($widgets['in-review']);
        }

        foreach ($widgets as $widget) {
            $data[] = [
                'label' => $widget['label'],
                'key' => $widget['key'],
                'value' => $widget['value'],
                'button_visible' => $widget['button_visible']
            ];
        };

        return Api::responseData([
            'status' => true,
            'data' => $data,
        ]);
    }
}
