<?php

namespace App\Http\Controllers\Api\GoldPlus;

use App\Entities\GoldPlus\Enums\KostStatus;
use RuntimeException;
use App\Http\Helpers\ApiResponse;
use App\Presenters\RoomPresenter;
use Illuminate\Http\JsonResponse;
use App\Repositories\RoomRepository;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use App\Http\Controllers\Api\BaseController;
use App\Http\Requests\Owner\ListKostsGoldplusAcquisitionRequest;

class GoldPlusAcquisitionController extends BaseController
{
    protected $roomRepository;

    public function __construct(RoomRepository $roomRepository)
    {
        parent::__construct();
        $this->roomRepository = $roomRepository;
    }

    /**
     * API to return the list of owned goldplus kost with onreview goldplus submission.
     *
     * @param ListKostsGoldplusAcquisitionRequest $request
     */
    public function getListKostsGoldplus(ListKostsGoldplusAcquisitionRequest $request): JsonResponse
    {
        $user = $this->user();
        if (empty($user)) {
            Bugsnag::notifyException(new RunTimeException('User data is null.'));
            return ApiResponse::responseData([], false, false, 401);
        }

        if ($request->isValidationFailed()) {
            return ApiResponse::responseData([
                'messages' => $request->failedValidator->errors(),
            ], false, false, 400);
        }

        $rooms = $this->roomRepository->getGoldplusAcquisitionKosts($user->id, request()->input('status'));
        if (empty($rooms)) {
            throw new \RuntimeException('Error getting room list return empty');
        }

        $data = (new RoomPresenter('goldplus-acquisition-list'))->present($rooms)['data'];

        $button = $this->roomRepository->getButtonSubmitGP($user->id);

        return ApiResponse::responseData([
            'status' => true,
            'data' => $data,
            'button' => $button,
            'has_more' => $rooms->hasMorePages(),
        ]);
    }

    /**
     * Get the list of available filters for list of goldplus acquisition kost.
     */
    public function getGoldplusFilterOptions(): JsonResponse
    {
        $user = $this->user();
        if (empty($user)) {
            Bugsnag::notifyException(new RunTimeException('User data is null.'));
            return ApiResponse::responseData([], false, false, 401);
        }

        $unpaidStatus = KostStatus::UNPAID;
        $totalUnpaidKost = $this->roomRepository->getGoldplusAcquisitionKosts($user->id, $unpaidStatus['key'])->total();
        $unpaidStatus['is_new_badge'] = false;
        if ($totalUnpaidKost > 0) {
            $unpaidStatus['is_new_badge'] = true;
        }

        $response = [
            array_merge(KostStatus::ALL, ['is_new_badge' => false]),
            array_merge(KostStatus::ACTIVE, ['is_new_badge' => false]),
            $unpaidStatus,
            array_merge(KostStatus::ON_REVIEW, ['is_new_badge' => false]),
        ];
        
        return ApiResponse::responseData([
            'status' => true,
            'data' => $response,
        ]);
    }
}
