<?php

namespace App\Http\Controllers\Api\GoldPlus;

use App\Entities\Consultant\PotentialOwner;
use App\Http\Requests\GoldPlus\CreateSubmissionRequest;
use App\Http\Controllers\Api\BaseController;
use App\Http\Helpers\ApiResponse;
use App\Services\GoldPlus\CreateNewSubmission;
use App\Services\GoldPlus\GoldPlusSubmissionError;

use Illuminate\Http\Request;

class SubmissionController extends BaseController
{
    protected $submitSrv;

    public function __construct(CreateNewSubmission $submitSrv)
    {
        parent::__construct();
        $this->submitSrv = $submitSrv;
    }

    public function getOnboardingContent(Request $request)
    {
        $onboarding = __('api.goldplus.onboarding');

        $title = $onboarding['title'];
        $data = $onboarding['items'];

        return ApiResponse::responseData([
            'title' => $title,
            'data' => $data
        ]);
    }

    public function getTncContent(Request $request)
    {
        $path = resource_path('views/goldplus/terms-and-conditions.html');
        $html = '';

        if (file_exists($path)) {
           $html = file_get_contents($path) ;
        }

        return ApiResponse::responseData(['html_content' => $html]);
    }

    public function store(CreateSubmissionRequest $request)
    {
        if ($request->isValidationFailed()) {
            return ApiResponse::badRequest($request->failedValidator->errors());
        }

        try {
            $listing_ids = is_null($request->input('listing_ids')) ? [] : $request->input('listing_ids');
            $result = $this->submitSrv->submit($this->user(), $request->input('code'), $listing_ids);
        } catch (GoldPlusSubmissionError $e) {
            return ApiResponse::badRequest($e->getMessage(), 200);
        }

        $submission = $result['submission']->toArray();
        $submission['listing_ids'] = explode(',', $result['submission']->listing_ids);
        $submission['property_contract_id'] = $result['property_contract']->id ?? null;
        ksort($submission);
        return ApiResponse::responseData(['submission' => $submission]);
    }
}
