<?php
namespace App\Http\Controllers\Api\GoldPlus;

use App\Entities\GoldPlus\Package;
use App\Http\Controllers\Api\BaseController;
use App\Http\Helpers\ApiResponse;
use App\Transformers\GoldPlus\PackageIndexApiTransformer;

class PackageController extends BaseController
{
    protected $packageRepo;
    protected $indexApiTrans;

    public function __construct(PackageIndexApiTransformer $indexApiTrans)
    {
        parent::__construct();
        $this->indexApiTrans = $indexApiTrans;
    }

    public function index()
    {
        $packages = Package::where('active', true)->get()->reverse()->values();
        $transformed = $this->transformPackages($packages);

        return ApiResponse::responseData([
            'packages' => $transformed
        ]);
    }

    private function transformPackages($packages)
    {
        $transformed = [];
        foreach ($packages as $package) {
            $transformed[] = $this->indexApiTrans->transform($package);
        }
        return $transformed;
    }
}