<?php

namespace App\Http\Controllers\Api\GoldPlus;

use App\Entities\Room\Room;
use App\Entities\Room\Element\Price;
use App\Http\Controllers\Api\BaseController;
use App\Http\Helpers\ApiHelper as Api;
use App\Repositories\GoldplusStatistic\OwnerGoldplusStatisticRepository;
use App\Repositories\RoomRepository;
use App\User;
use Illuminate\Http\JsonResponse;

class UnsubscribeController extends BaseController
{
    protected const RADIUS = 3000;

    protected $ownerGoldplusStatisticRepository;
    protected $roomRepository;

    public function __construct(OwnerGoldplusStatisticRepository $ownerGoldplusStatisticRepository, RoomRepository $roomRepository)
    {
        parent::__construct();
        
        $this->ownerGoldplusStatisticRepository = $ownerGoldplusStatisticRepository;
        $this->roomRepository = $roomRepository;
    }

    public function getStatistic(): JsonResponse
    {
        $user = $this->user();
        if (empty($user) || is_null($user)) {
            throw new RuntimeException(
                __('goldplus-statistic.error.runtime_exception.user_id_was_null')
            );
        }

        $resultGoldplusStatistic = $this->ownerGoldplusStatisticRepository
            ->getBestKostStatisticData(
                $user->id
            );

        $room = $this->getRoom($user, $resultGoldplusStatistic ? $resultGoldplusStatistic['id'] : null);
        if (!$room) {
            return Api::responseData([
                'data' => null
            ]);
        }

        $price = $room->price();
        $priceTypeIndex = $price->getAvailablePriceTypeIndex();
        $priceType = Price::getPriceTypeByIndex($priceTypeIndex);
        $priceType = $priceType === Price::STRING_ANNUALLY ? Price::STRING_YEARLY : $priceType;

        return Api::responseData([
            'data' => [
                'id' => $room->song_id,
                'room_title' => $room->name,
                'price_title' => $price->priceTitleFormat($priceTypeIndex, false),
                'data_price' => $room->getNearbyPriceByRadius(self::RADIUS)[$priceType],
                'gp_statistic' => $resultGoldplusStatistic ? $resultGoldplusStatistic['statistic'] : null
            ]
        ]);
    }

    private function getRoom(User $user, ?int $songId): ?Room
    {
        if ($songId) {
            return $this->roomRepository->where('song_id', $songId)->first();
        }
        
        return $this->getMostExpensiveRoom($user);
    }

    private function getMostExpensiveRoom(User $user): ?Room
    {
        return $this->roomRepository->whereHas('owners', function ($q) use ($user) {
                $q->where('user_id', $user->id);
            })
            ->whereNull('apartment_project_id')
            ->where('is_testing', 0)
            ->where(function($q) {
                $q->where('name', '!=', 'mamites')
                    ->orWhere('name', '!=', 'mamitest');
            })
            ->orderByRaw(\DB::raw('GREATEST(price_daily, price_weekly, price_monthly, price_quarterly, price_semiannually, price_yearly) DESC'))
            ->orderBy('created_at', 'DESC')
            ->first();
    }
}