<?php

namespace App\Http\Controllers\Api\Midtrans;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseController;
use App\Veritrans\Midtrans;
use App\Veritrans\Veritrans;
use Auth;
use App\Entities\Premium\Payment;
use App\Entities\Premium\PremiumPackage;
use Validator;
use App\Entities\Classes\MidtransToken;
use App\User;
use DB;
use App\Entities\Classes\MidtransFinish;
use App\Entities\Classes\Premium\Confirmation;
use App\Http\Helpers\ApiResponse as Api;
use Config;
use App\Http\Requests\Premium\PremiumPlusTokenRequest;
use App\Http\Requests\Premium\SnapFinishRequest;
use App\Services\Premium\PremiumPlusInvoiceService;

class SnapController extends BaseController
{

    protected $validatorMessage = [
        "request_id.required" => "Pembayaran tidak bisa dilakukan",
        "package_id.required" => "Pembelian paket premium gagal"
    ];

    private $premiumPlusInvoiceService;

    public function __construct(PremiumPlusInvoiceService $premiumPlusInvoiceService)
    {   
        Midtrans::$serverKey = Config::get('services.midtrans.server_key');
        Midtrans::$isProduction = Config::get('services.midtrans.is_production');
        $this->premiumPlusInvoiceService = $premiumPlusInvoiceService;
    }

    public function postToken(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'package_id' => 'required'
        ], $this->validatorMessage);

        if($validator->fails()) {
            return Api::responseData([
                "status" => false,
                "token" => null
            ]);
        }
        
        $user = $this->user();
        if (is_null($user)) {
            return Api::responseData([
                "status" => false,
                "token" => null
            ]);
        }

        $requestType = PremiumPackage::PACKAGE_TYPE;
        if ($request->filled('request_type')) {
            $requestType = $request->input('request_type');
        }
        
        $data = [
            "package_id"    => $request->input('package_id'),
            "request_type"  => $requestType
        ];
        
        DB::beginTransaction();
        $token = (new MidtransToken($user, $data))->perform();
        DB::commit();

        $status = is_null($token) ? false : true; 
        return Api::responseData([
            "status" => $status,
            "token" => $token
        ]);
    }

    public function createTokenGp(PremiumPlusTokenRequest $request)
    {
        $validator = $request->failedValidator;
        if (!is_null($validator) && $validator->fails()) {
            return back()->withErrors($validator)->withInput($request->input());
        }

        try {
            DB::beginTransaction();
            $token = $this->premiumPlusInvoiceService->getToken($request->all(), $this->user());
            DB::commit();
            return Api::responseData($token);
        } catch (\Exception $exception) {
            DB::rollBack();
            return Api::responseData([
                'status' => false,
                'message' => 'Gagal melakukan pembayaran'
            ]);
        }
    }

    public function finish(SnapFinishRequest $request)
    {
        $validator = $request->failedValidator;
        if (!is_null($validator) && $validator->fails()) {
            return Api::responseData([
                'status' => false,
                'message' => $validator->errors()->first(),
            ]);
        }
        
        $result = $request->all();
        $user = $this->user();
        DB::beginTransaction();
        (new MidtransFinish($result, $user, true))->finish();
        DB::commit();
        return Api::responseData(["status" => true]);
    }
}
