<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Helpers\ApiResponse as Api;
use App\Entities\LandingContent\LandingContentSubscriber;
use App\Repositories\RoomRepositoryEloquent;
use App\Entities\Alarm\UserFilter;

class LandingContentController extends BaseController
{
    public function subscribe(Request $request)
    {
        $user = $this->user();

        if (!filter_var($request->input('email'), FILTER_VALIDATE_EMAIL)) {
            return Api::responseData(["status" => false]);
        }

        $roomRepository = new RoomRepositoryEloquent(app());

        if ($request->filled('location') AND !is_null($request->input('location'))) $alarmDetail = $alarmDetail = $request->all('filters', 'location', 'phone', 'email');
        else $alarmDetail = $request->all('filters', 'job', 'area', 'phone', 'email');

        $alarm = $roomRepository->saveFilterToAlarm($user, null, $alarmDetail);

        $userDetail = $request->input('user_detail');
        
        $subscriber = new LandingContentSubscriber;
        $subscriber->landing_content_id = $request->seq;
        if (!is_null($user)) $subscriber->user_id = $user->id;
        $subscriber->name = $request->name;
        $subscriber->email = $request->email;
        $subscriber->phone_number = $request->phone;
        $subscriber->location = substr($userDetail['location'], 0, 250);
        $subscriber->save();
        if ($user) {
            UserFilter::saveOrUpdate($user, $alarmDetail, 'room', 'landing_content_' . $request->seq);
        }
        return Api::responseData([
                                "status" => true, 
                                "result" => $alarm,
                                "search" => "job=".$request->input('job')."&area=".$request->input('area') 
                            ]);
    }
}
