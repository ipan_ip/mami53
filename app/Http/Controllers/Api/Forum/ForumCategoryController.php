<?php

namespace App\Http\Controllers\Api\Forum;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseController;
use App\Http\Helpers\ApiResponse as Api;
use App\Entities\Forum\ForumCategory;
use App\Presenters\ForumCategoryPresenter;

class ForumCategoryController extends BaseController
{
    public function list(Request $request)
    {
        $categories = ForumCategory::with(['threads', 'answers'])->get();


        $categories = (new ForumCategoryPresenter('list'))->present($categories);

        return Api::responseData([
            'categories' => $categories['data']
        ]);
    }

    public function option(Request $request)
    {
        $categories = ForumCategory::get();

        $categories = (new ForumCategoryPresenter('option'))->present($categories);

        return Api::responseData([
            'categories' => $categories['data']
        ]);
    }

    public function show(Request $request, $slug)
    {
        $category = ForumCategory::where('slug', $slug)
                        ->first();

        if (!$category) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Kategori tidak ditemukan'
                ]
            ]);
        }

        $category = (new ForumCategoryPresenter('detail'))->present($category);

        return Api::responseData([
            'category' => $category['data']
        ]);
    }
}