<?php

namespace App\Http\Controllers\Api\Forum;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseController;
use App\Http\Helpers\ApiResponse as Api;
use App\Repositories\Forum\ForumThreadRepository;
use App\Repositories\Forum\ForumAnswerRepository;
use App\Presenters\ForumAnswerPresenter;
use App\Events\Forum\AnswerAdded;
use App\Entities\Forum\ForumAnswer;
use Validator;
use App\Http\Helpers\UtilityHelper;

class ForumAnswerController extends BaseController
{
    protected $repository;
    protected $threadRepository;

    public function __construct(ForumAnswerRepository $repository, 
        ForumThreadRepository $threadRepository)
    {
        $this->repository = $repository;
        $this->threadRepository = $threadRepository;

        parent::__construct();
    }

    public function getThreadAnswers(Request $request, $threadSlug)
    {
        $user = $this->user();

        $thread = $this->threadRepository->getBySlug($threadSlug);

        if (!$thread) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Pertanyaan tidak ditemukan'
                ]
            ]);
        }

        $answers = $this->repository->getAnswers($thread);

        $answers = (new ForumAnswerPresenter('list', $user))->present($answers);
        $pagination = $answers['meta']['pagination'];

        return Api::responseData([
            'answers' => $answers['data'],
            'pagination' => [
                'current_page' => $pagination['current_page'],
                'next_page' => $pagination['current_page'] + 1,
                'has_more' => $pagination['current_page'] < $pagination['total_pages']
            ]
        ]);
    }

    public function postAnswer(Request $request, $threadSlug)
    {
        $user = $this->user();

        if (!$user) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Silakan login terlebih dahulu'
                ]
            ]);
        }

        if (!($user->hasRole('forum_junior') || $user->hasRole('forum_senior'))) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Silakan registrasi di forum terlebih dahulu'
                ]
            ]);
        }

        $thread = $this->threadRepository->getBySlug($threadSlug);

        if (!$thread) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Pertanyaan tidak ditemukan'
                ]
            ]);
        }

        $validator = Validator::make($request->all(), [
            'answer' => 'required'
        ], [
            'answer.required' => 'Jawaban harus diisi'
        ]);

        if ($validator->fails()) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => implode('. ', $validator->errors()->all())
                ]
            ]);
        }

        $params = [
            'answer' => UtilityHelper::linkify($request->answer, ['http'], ['target' => '_blank'])
        ];

        $answer = $this->repository->answerThread($thread, $params, $user);

        event(new AnswerAdded($thread, $answer));

        $answers = $this->repository->getAnswers($thread);

        return Api::responseData([
            'status' => true,
            'last_page' => $answers->lastPage()
        ]);
    }

    public function voteUp(Request $request, $answerId)
    {
        $user = $this->user();

        if (!$user) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Silakan login terlebih dahulu'
                ]
            ]);
        }

        if (!($user->hasRole('forum_junior') || $user->hasRole('forum_senior'))) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Silakan registrasi di forum terlebih dahulu'
                ]
            ]);
        }

        $answer = ForumAnswer::find($answerId);

        if (!$answer) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Jawaban tidak ditemukan'
                ]
            ]);
        }

        if ($answer->user_id == $user->id) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Tidak dapat menyukai jawaban sendiri'
                ]
            ]);
        }

        $vote = $this->repository->voteUp($answer, $user);

        if ($vote === false) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Gagal menyukai jawaban'
                ]
            ]);
        }

        $answer = (new ForumAnswerPresenter('vote', $user))->present($answer);

        return Api::responseData([
            'answer_data' => $answer['data']
        ]);
    }

    public function voteDown(Request $request, $answerId)
    {
        $user = $this->user();

        if (!$user) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Silakan login terlebih dahulu'
                ]
            ]);
        }

        if (!($user->hasRole('forum_junior') || $user->hasRole('forum_senior'))) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Silakan registrasi di forum terlebih dahulu'
                ]
            ]);
        }

        $answer = ForumAnswer::find($answerId);

        if (!$answer) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Jawaban tidak ditemukan'
                ]
            ]);
        }

        if ($answer->user_id == $user->id) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Tidak dapat tidak menyukai jawaban sendiri'
                ]
            ]);
        }

        $vote = $this->repository->voteDown($answer, $user);

        if ($vote === false) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Gagal untuk tidak menyukai jawaban'
                ]
            ]);
        }

        $answer = (new ForumAnswerPresenter('vote', $user))->present($answer);

        return Api::responseData([
            'answer_data' => $answer['data']
        ]);
    }

    public function report(Request $request, $answerId)
    {
        $user = $this->user();

        if (!$user) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Silakan login terlebih dahulu'
                ]
            ]);
        }

        // if (!($user->hasRole('forum_junior') || $user->hasRole('forum_senior'))) {
        //     return Api::responseData([
        //         'status' => false,
        //         'meta' => [
        //             'message' => 'Silakan registrasi di forum terlebih dahulu'
        //         ]
        //     ]);
        // }

        $answer = ForumAnswer::find($answerId);

        if (!$answer) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Jawaban tidak ditemukan'
                ]
            ]);
        }

        if ($answer->isReportedBy($user)) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Anda sudah melaporkan jawaban ini'
                ]
            ]);
        }

        $validator = Validator::make($request->all(), [
            'report' => 'required'
        ], [
            'report.required' => 'Deskrispi laporan harus diisi'
        ]);

        if ($validator->fails()) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => implode('. ', $validator->errors()->all())
                ]
            ]);
        }

        $this->repository->report($answer, $user, $request->report);

        return Api::responseData([
            'status' => true
        ]);
    }
}