<?php

namespace App\Http\Controllers\Api\Forum;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseController;
use App\Http\Helpers\ApiResponse as Api;
use Validator;
use App\User;
use Carbon\Carbon;
use App\Entities\Entrust\Role;
use App\Entities\Forum\ForumUser;
use App\Presenters\ForumUserPresenter;

class ForumUserController extends BaseController
{

    protected $validationMessages = [
        'register_as.required' => 'Jenis User harus diisi',
        'register_as.in' => 'Jenis User tidak valid',
        'hometown.required_if' => 'Kota Asal harus diisi',
        'hometown.max' => 'Kota Asal maksimal :max karakter',
        'destination.required_if' => 'Kota Tujuan harus diisi',
        'destination.max' => 'Kota Tujuan maksimal :max karakter',
        'nim.required_if' => 'NIM harus diisi',
        'nim.max' => 'NIM maksimal :max karakter',
        'university.required_if' => 'Nama Universitas harus diisi',
        'university.max' => 'Nama Universitas maksimal :max karakter',
        'major.required_if' => 'Jurusan harus diisi',
        'major.max' => 'Jurusan maksimal :max karakter',
        'year_start.required_if' => 'Tahun Masuk harus diisi',
        'year_start.date_format' => 'Format Tahun Masuk tidak valid',
        'university_city.required_if' => 'Kota Universitas harus diisi',
        'university_city.max' => 'Kota Universitas maksimal :max karakter'
    ];

    public function getDataForRegistration(Request $request)
    {
        $user = $this->user();

        if (!$user) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Silakan login terlebih dahulu'
                ]
            ]);
        }

        $user = (new ForumUserPresenter('register'))->present($user);

        return Api::responseData([
            'user' => $user['data']
        ]);
    }

    public function register(Request $request)
    {
        $user = $this->user();

        if (!$user) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Silakan login terlebih dahulu'
                ]
            ]);
        }

        if ($user->hasRole('forum_junior') || $user->hasRole('forum_senior')) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Anda sudah terdaftar di forum'
                ]
            ]);
        }

        $validator = Validator::make($request->all(), [
            'register_as'=>'required|in:student,college_student',
            'hometown' => 'required_if:register_as,student|max:150',
            'destination' => 'required_if:register_as,student|max:150',
            'nim' => 'required_if:register_as,college_student|max:20',
            'university' => 'required_if:register_as,college_student|max:150',
            'major' => 'required_if:register_as,college_student|max:100',
            'year_start' => 'required_if:register_as,college_student',
            'university_city' => 'required_if:register_as,college_student|max:150',
            'photo_id' => 'nullable'
        ], $this->validationMessages);

        $validator->after(function($validator) use ($request) {
            $semester = ForumUser::countSemester($request->year_start);

            if ($request->register_as == 'college_student' && $semester > 2) {
                if (is_null($request->category_ids) || empty($request->category_ids)) {
                    $validator->errors()->add('category_ids', 'Pilihan subforum harus diisi');
                }
            }
        });

        if ($validator->fails()) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => implode('. ', $validator->errors()->all())
                ]
            ]);
        }

        $roleName = '';
        $semester = 0;

        if ($request->register_as == 'student') {
            // test if profile is still working
            $user->job  = 'pelajar';
            $user->city = $request->destination;

            $roleName = 'forum_junior';

        } elseif ($request->register_as == 'college_student') {
            $user->job          = 'kuliah';
            $user->work_place   = $request->university;
            $user->position     = $request->major;
            $user->city         = $request->university_city;

            $semester = ForumUser::countSemester($request->year_start);

            $user->semester = $semester;

            if ($semester > 2) {
                $roleName = 'forum_senior';
            } else {
                $roleName = 'forum_junior';
            }
            
        }

        $user->photo_id = $request->photo_id;

        $user->save();

        $forumUser = new ForumUser;
        $forumUser->user_id = $user->id;
        $forumUser->register_as = $request->register_as;

        if ($request->register_as == 'student') {
            $forumUser->hometown = $request->hometown;
            $forumUser->destination = $request->city;
        } elseif ($request->register_as == 'college_student') {
            $forumUser->university = $request->university;
            $forumUser->major = $request->major;
            $forumUser->destination = $request->university_city;
            $forumUser->identifier_number = $request->nim;
            $forumUser->year_start = $request->year_start;
        }

        $forumUser->save();

        $role = Role::where('name', $roleName)->first();
        $user->attachRole($role);

        if ($semester > 2) {
            $user->forum_categories()->attach($request->category_ids);
        }
        
        return Api::responseData([
            'status' => true
        ]);
    }

    public function listUniversities(Request $request)
    {
        return Api::responseData([
            'universities' => ForumUser::UNIVERSITY_OPTIONS
        ]);
    }

    public function determineRoleFromYearStart(Request $request)
    {
        $year = $request->year;

        $semester = ForumUser::countSemester($year);
        
        return Api::responseData([
            'role' => $semester > 2 ? 'senior' : 'junior'
        ]);
    }
}