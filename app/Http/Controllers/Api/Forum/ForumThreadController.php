<?php

namespace App\Http\Controllers\Api\Forum;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseController;
use App\Http\Helpers\ApiResponse as Api;
use Validator;
use App\Repositories\Forum\ForumThreadRepository;
use App\Presenters\ForumThreadPresenter;
use App\Events\Forum\ThreadCreated;
use App\Entities\Forum\ForumCategory;


class ForumThreadController extends BaseController
{
    protected $repository;

    protected $validationMessages = [
        'category.required' => 'Nama Kota harus diisi',
        'question.required' => 'Pertanyaan harus diisi',
        'question.min' => 'Pertanyaan minimal :min karakter'
    ];

    public function __construct(ForumThreadRepository $repository)
    {
        $this->repository = $repository;

        parent::__construct();
    }

    public function list(Request $request, $categorySlug)
    {
        $category = ForumCategory::where('slug', $categorySlug)->first();

        if (!$category) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Kategori tidak ditemukan'
                ]
            ]);
        }

        $threads = $this->repository->listByCategory($category);

        $threads = (new ForumThreadPresenter('list', $this->user()))->present($threads);

        $pagination = $threads['meta']['pagination'];

        return Api::responseData([
            'threads' => $threads['data'],
            'pagination' => [
                'current_page' => $pagination['current_page'],
                'next_page' => $pagination['current_page'] + 1,
                'has_more' => $pagination['current_page'] < $pagination['total_pages']
            ]
        ]);
    }

    public function createThread(Request $request)
    {
        $user = $this->user();

        if (!$user) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Silakan login terlebih dahulu'
                ]
            ]);
        }

        // if (!$user->hasRole('forum_junior')) {
        //     return Api::responseData([
        //         'status' => false,
        //         'meta' => [
        //             'message' => 'Hanya Adik yang boleh mengajukan pertanyaan'
        //         ]
        //     ]);
        // }

        $validator = Validator::make($request->all(), [
            'category' => 'required',
            'question'  => 'required|min:10'
        ], $this->validationMessages);

        if ($validator->fails()) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => implode('. ', $validator->errors()->all())
                ]
            ]);
        }

        $params = [
            'question' => $request->question,
            'category' => $request->category
        ];

        $thread = $this->repository->createNewThread($params, $user);

        event(new ThreadCreated($thread));

        $thread = (new ForumThreadPresenter('detail-simple'))->present($thread);

        return Api::responseData([
            'status' => true,
            'thread' => $thread['data']
        ]);
    }

    public function showThread(Request $request, $threadSlug)
    {
        $user = $this->user();

        $thread = $this->repository->getBySlug($threadSlug, 
                    ['with' => ['user', 'user.photo', 'category']]);

        if (!$thread) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Pertanyaan tidak ditemukan'
                ]
            ]);
        }

        $thread = (new ForumThreadPresenter('detail', $user))->present($thread);

        return Api::responseData([
            'thread' => $thread['data']
        ]);
    }

    

    public function follow(Request $request, $slug)
    {
        $user = $this->user();

        if (!$user) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Silakan login terlebih dahulu'
                ]
            ]);
        }

        if (!($user->hasRole('forum_junior') || $user->hasRole('forum_senior'))) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Silakan registrasi di forum terlebih dahulu'
                ]
            ]);
        }

        $thread = $this->repository->getBySlug($slug);

        if (!$thread) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Pertanyaan tidak ditemukan'
                ]
            ]);
        }

        if ($thread->user_id == $user->id) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Tidak dapat mengikuti pertanyaan sendiri'
                ]
            ]);
        }

        if (!$this->repository->follow($thread, $user)) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Gagal mengikuti pertanyaan'
                ]
            ]);
        }

        return Api::responseData([
            'status' => true
        ]);
    }

    public function unfollow(Request $request, $slug)
    {
        $user = $this->user();

        if (!$user) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Silakan login terlebih dahulu'
                ]
            ]);
        }

        if (!($user->hasRole('forum_junior') || $user->hasRole('forum_senior'))) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Silakan registrasi di forum terlebih dahulu'
                ]
            ]);
        }

        $thread = $this->repository->getBySlug($slug);

        if (!$thread) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Pertanyaan tidak ditemukan'
                ]
            ]);
        }

        if (!$this->repository->unfollow($thread, $user)) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Gagal untuk membatalkan mengikuti pertanyaan'
                ]
            ]);
        }

        return Api::responseData([
            'status' => true
        ]);
    }

    public function report(Request $request, $slug)
    {
        $user = $this->user();

        if (!$user) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Silakan login terlebih dahulu'
                ]
            ]);
        }

        // if (!($user->hasRole('forum_junior') || $user->hasRole('forum_senior'))) {
        //     return Api::responseData([
        //         'status' => false,
        //         'meta' => [
        //             'message' => 'Silakan registrasi di forum terlebih dahulu'
        //         ]
        //     ]);
        // }

        $thread = $this->repository->getBySlug($slug);

        if (!$thread) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Pertanyaan tidak ditemukan'
                ]
            ]);
        }

        if ($thread->isReportedBy($user)) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Anda sudah melaporkan pertanyaan ini'
                ]
            ]);
        }

        $validator = Validator::make($request->all(), [
            'report' => 'required'
        ], [
            'report.required' => 'Deskrispi laporan harus diisi'
        ]);

        if ($validator->fails()) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => implode('. ', $validator->errors()->all())
                ]
            ]);
        }

        $this->repository->report($thread, $user, $request->report);

        return Api::responseData([
            'status' => true
        ]);
    }
}