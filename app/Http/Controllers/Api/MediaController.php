<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Entities\Media\Media;
use App\Entities\Room\Room;
use App\Entities\Media\ReviewMedia;
use App\Http\Requests;
use App\Http\Helpers\ApiResponse as Api;
use Validator;
use App\Entities\Media\SinglePhoto;

class MediaController extends BaseController
{
	const REGULAR_WATERMARKING = 'reguler';

    // Exclude watermarking for below image types:
    // 1. User related image
    // 2. Vacancy image
    protected $nonWatermarkedTypes = [
        'user_photo',
        'user_style',
        'company_photo',
    ];

    public function postIndex(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'media' => 'image'
        ]);

        if($validator->fails()) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => $validator->errors()->first('media')
                ]
            ]);
        }
        
        $file = $request->file('media');
        $mediaType = $request->input('media_type');
        $params = json_decode($request->input('params'), true);
		$options = [];
        
        // by default, there always be a watermark
        $needWatermark = true;
        if ($request->filled('need_watermark') ||
            // some old applocation is sending `watermarking` instead of `need_watermark`)
            $request->filled('watermarking'))
        {
            // if request is explicitly excluding watermark
            if ($request->need_watermark == 'false' || $request->watermarking == 'false')
            {
                $needWatermark = false;
            }
        }

        // if media_type is nonWatermarkedTypes
        if (in_array($mediaType, $this->nonWatermarkedTypes))
        {
            $needWatermark = false;
        }

		// extra parameter for watermarking_type:
		// "reguler" or "premium"
		if ($request->filled('watermarking_type'))
		{
			$options['watermarking_type'] = $request->watermarking_type;
		}
		else
		{
			$options['watermarking_type'] = self::REGULAR_WATERMARKING;
		}

        if($request->all() == null) return Api::responseData(compact('media', 'params'));

        $noNeedToKeepOriginalFile = [];

        $media = Media::postMedia(
            $file, 
            'upload', 
            \Auth::id(), 
            null, 
            null, 
            $mediaType, 
            $needWatermark, 
            null,
            in_array($mediaType, $noNeedToKeepOriginalFile) ? false : true,
			$options
        );

        return Api::responseData(compact('media', 'params'));
    }

    public function postPhotoAgent(Request $request)
    {
        $user = $this->user();
        if (is_null($user)) {
            return Api::responseData(["status" => false, "message" => "Login dulu"]);
        }

        $validator = Validator::make($request->all(), [
            'media' => 'image'
        ]);

        if($validator->fails()) {
            return Api::responseData([
                'status' => false,
                'message' => $validator->errors()->all(),
            ]);
        }
        $actionToUpload = (new SinglePhoto($request->all(), "agent", $user))->upload();
        $media = ["id" => $actionToUpload['id'],
            "url" => ["real" => $actionToUpload['media'],
                    "small" => $actionToUpload['media'],
                    "medium" => $actionToUpload['media'],
                    "large" => $actionToUpload['media']
                ]
        ];
        return Api::responseData(["status" => true, "media" => $media]);
    }
}
