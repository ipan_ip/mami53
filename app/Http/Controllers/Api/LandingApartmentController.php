<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Helpers\ApiResponse as Api;
use App\Entities\Landing\LandingApartment;
use App\Entities\Activity\ViewLandingTemp;
use App\Repositories\ApartmentProjectRepositoryEloquent;

class LandingApartmentController extends BaseController
{
    public function show(Request $request, $slug)
    {
        $landing = LandingApartment::with('tags', 'photo')->where('slug','=',$slug)->first();

        if(!$landing) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Data not found'
                ]
            ]);
        }

        $photo = null;
        if(!is_null($landing->photo_id)) {
            $photo = $landing->photo->getMediaUrl();
        }

        $breadcrumb = $landing->getBreadcrumb();
        $parentLandingUrlArray = explode('/', $breadcrumb[1]['url']);
        $parentLandingSlug = $parentLandingUrlArray[count($parentLandingUrlArray) - 1];
        $relatedLanding = LandingApartment::getParentSlugLanding($parentLandingSlug);

        $centerLatitude  = ($landing->latitude_1 + $landing->latitude_2) / 2;
        $centerLongitude = ($landing->longitude_1 + $landing->longitude_2) / 2;

        $data = [
            '_id'           => $landing->id,
            'heading_1'     => $landing->heading_1,
            'heading_2'     => $landing->heading_2,
            'latitude_1'    => $landing->latitude_1,
            'longitude_1'   => $landing->longitude_1,
            'latitude_2'    => $landing->latitude_2,
            'longitude_2'   => $landing->longitude_2,
            'description_1' => $landing->description_1,
            'description_2' => $landing->description_2,
            'description_3' => $landing->description_3,
            'price_min'     => $landing->price_min,
            'price_max'     => $landing->price_max,
            'place'         => $landing->place,
            'tag_ids'       => $landing->tags()->pluck('tag.id')->toArray(),
            'center_latitude'   => $centerLatitude,
            'center_longitude'  => $centerLongitude,
            'breadcrumb'    => $breadcrumb,
            'related_landing'   => $relatedLanding,
            'photos'         => [['photo_url' => $photo]]
        ];

        return Api::responseData([
            'data'=>$data
        ]);
    }

    public function viewCount(Request $request, $id)
    {
        // LandingApartment::find($id)->increment('view_count');

        $viewTemp = new ViewLandingTemp;
        $viewTemp->identifier_type = 'landing_apartment';
        $viewTemp->identifier = $id;
        $viewTemp->save();

        return Api::responseData([
            'status' => true
        ]);
    }

    public function showFilter(Request $request, $slug)
    {
        $landing = LandingApartment::with('tags')->where('slug','=',$slug)->first();

        if(!$landing) {
            return Api::responseData([
                'landing' => null
            ]);
        }

        return Api::responseData([
            'landing' => [
                'location' => $landing->location,
                'filters' => $landing->filters
            ]
        ]);

    }

    public function showProjectFilter(Request $request, $citySlug, $slug)
    {
        $repository = new ApartmentProjectRepositoryEloquent(app());

        // remove hypen from area slug
        $city = str_replace('-', ' ',  $citySlug);

        // get from slug
        $apartmentProject = $repository->getByCityAndSlug($city, $slug);

        if(!$apartmentProject) {
            // if not found, find from slug history
            $apartmentProject = $repository->getFromSlugHistory($city, $slug);
        }

        if(!$apartmentProject) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Data tidak ditemukan'
                ]
            ]);
        }

        if($apartmentProject->is_active == 0) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Data tidak aktif'
                ]
            ]);
        }

        $filters = [
            'furnished' => '',
            'include_apartment' => true,
            'price-range' => [0, 100000000],
            'rent_type' => 2,
            'tag_ids' => [],
            'unit_type' => ''
        ];

        return Api::responseData([
            'filters' => $filters,
            'apartment_project_id' => $apartmentProject->id,
            'apartment_project_code' => $apartmentProject->project_code
        ]);
    }
}
