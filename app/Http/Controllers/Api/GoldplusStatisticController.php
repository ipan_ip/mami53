<?php
namespace App\Http\Controllers\Api;

use App\Http\Helpers\ApiHelper as Api;
use App\Http\Requests;
use App\Repositories\GoldplusStatistic\OwnerGoldplusStatisticRepository;
use App\User;
use App\Http\Controllers\Api\BaseController;
use App\Entities\Owner\Goldplus\OwnerGoldplusStatistic;
use App\Entities\Owner\Goldplus\GoldplusStatisticType;
use Illuminate\Http\JsonResponse;
use App\Http\Requests\GoldplusStatisticRequest;
use RuntimeException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Entities\Room\Room;
use App\Entities\Level\KostLevel;

/**
 * class GoldplusStatisticController
 * This class purpose for handling Goldplus Statistic feature
 * 
 * For detail doc, please visit : https://mamikos.atlassian.net/wiki/spaces/U/pages/718012805/Tech+Doc+Goldplus+List+API+for+Owner+Goldplus+statistic+Dashboard
 * 
 * @author Angga Bayu Sejati<angga@mamiteam.com>
 */
class GoldplusStatisticController extends BaseController
{

    /**
     * @var OwnerGoldplusStatisticRepository 
     */
    protected $ownerGoldplusStatistic;

    /**
     * Constructor
     * 
     * @param OwnerGoldplusStatisticRepository ownerGoldplusStatistic
     * @return void
     */
    public function __construct(OwnerGoldplusStatisticRepository $ownerGoldplusStatistic)
    {
        parent::__construct();

        if (empty($ownerGoldplusStatistic) || is_null($ownerGoldplusStatistic)) {
            throw new RuntimeException(
                __('goldplus-statistic.error.runtime_exception.initiate_object_was_null')
            );
        }
        
        $this->ownerGoldplusStatistic = $ownerGoldplusStatistic;
    }

    /**
     * Get statistic filter list based on $gpOption
     * 
     * @param string $gpOption
     * @return \Illuminate\Http\JsonResponse
     */
    public function getStatisticFilterList(Request $request, string $gpOption): JsonResponse
    {
        $user = $this->user();
        if (empty($user) || is_null($user)) {
            throw new RuntimeException(
                __('goldplus-statistic.error.runtime_exception.user_id_was_null')
            );
        }

        $validator = Validator::make($request->all(),
            [
                'id' => 'required|exists:designer,song_id',
            ],
            [
                'id.required' => __('goldplus-statistic.validation.error.room_not_exists'),
                'id.exists'   => __('goldplus-statistic.validation.error.room_not_exists')
            ]
        );

        if($validator->fails()) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => $validator->errors()->first()
                ]
            ]);
        }

        $songId = $request->get('id');

        switch ($gpOption) {
            case GoldplusStatisticType::CHAT:
                return $this->getChatFilterList($user, $songId);
            break;
            case GoldplusStatisticType::VISIT:
                return $this->getVisitFilterList($user, $songId);
            break;
            case GoldplusStatisticType::UNIQUE_VISIT:
                return $this->getUniqueVisitFilterList($user, $songId);
            break;
            case GoldplusStatisticType::FAVORITE:
                return $this->getFavoriteFilterList($user, $songId);
            break;
            default :
                //Defensive programming
                throw new RuntimeException(
                    __('goldplus-statistic.error.filter.selected_filter_not_valid')
                );
        }
    }

    /**
     * Get filter list of GP
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    private function getChatFilterList(User $user, int $songId): JsonResponse
    {
        return Api::responseData([
            'data' => $this->ownerGoldplusStatistic
                ->getAvailableFilterStatistic($user->id, GoldplusStatisticType::CHAT, $songId)
        ]);
    }

    /**
     * Get filter list of GP
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    private function getVisitFilterList(User $user, int $songId): JsonResponse
    {
        return Api::responseData([
            'data' => $this->ownerGoldplusStatistic
                ->getAvailableFilterStatistic($user->id, GoldplusStatisticType::VISIT, $songId)
        ]);
    }

    /**
     * Get filter list Unique Visit of GP
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    private function getUniqueVisitFilterList(User $user, int $songId): JsonResponse
    {
        return Api::responseData([
            'data' => $this->ownerGoldplusStatistic
                ->getAvailableFilterStatistic($user->id, GoldplusStatisticType::UNIQUE_VISIT, $songId)
        ]);
    }

    /**
     * Get filter list favorite of GP
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    private function getFavoriteFilterList(User $user, int $songId): JsonResponse
    {
        return Api::responseData([
            'data' => $this->ownerGoldplusStatistic
                ->getAvailableFilterStatistic($user->id, GoldplusStatisticType::FAVORITE, $songId)
        ]);
    }

    /**
     * Get detail Goldplus Kost by ID (song_id) and kost level id
     * 
     * @param int $songId
     * @param int $kostLevel
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDetailKostGp(int $songId): JsonResponse
    {
        //Validate the URI params
        $this->validateUriParamDetailKostGp($songId);

        $user = $this->user();
        if (empty($user) || is_null($user)) {
            throw new RuntimeException(
                __('goldplus-statistic.error.runtime_exception.user_id_was_null')
            );
        }

        return Api::responseData([
            'data' => $this->ownerGoldplusStatistic
                ->getDetailOfGoldplusKost($user->id, $songId)
        ]);
    }

    /**
     * Validate uri param for detail kost GP
     * 
     * @param int $songId
     * @param int $kostLevel
     * 
     * @return void
     */
    private function validateUriParamDetailKostGp(int $songId)
    {
        if (empty($songId) || false === is_numeric($songId)) {
            throw new RuntimeException(
                __('goldplus-statistic.error.runtime_exception.invalid_song_id')
            );
        }

        $roomExists = Room::select('id')->where('song_id', $songId)->exists();
        if (false === $roomExists) {
            return Api::responseData([
                'status'    => false,
                'message'   => __('goldplus-statistic.validation.error.kost_not_exists')
            ]);
        }

        unset($roomExists);
    }

    /**
     * Get goldplus statistic data
     * 
     * @param int $songId
     * @param string $gpStatisticType
     * @param GoldplusStatisticRequest $goldplusStatisticRequest
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function getGoldplusStatistic(
        int $songId, 
        string $gpStatisticType,
        GoldplusStatisticRequest $goldplusStatisticRequest): JsonResponse
    {
        $user = $this->user();
        if (empty($user) || is_null($user)) {
            throw new RuntimeException(
                __('goldplus-statistic.error.runtime_exception.user_id_was_null')
            );
        }

        switch ($gpStatisticType) {
            case GoldplusStatisticType::VISIT:
                return $this->getGpVisitingStatistic($songId, $goldplusStatisticRequest, $user);
            break;
            case GoldplusStatisticType::UNIQUE_VISIT:
                return $this->getGpUniqueVisitingStatistic($songId, $goldplusStatisticRequest, $user);
            break;
            case GoldplusStatisticType::CHAT:
                return $this->getGpChatStatistic($songId, $goldplusStatisticRequest, $user);
            break;
            case GoldplusStatisticType::FAVORITE:
                return $this->getGpFavoriteStatistic($songId, $goldplusStatisticRequest, $user);
            break;
            default :
                //Defensive programming
                throw new RuntimeException(
                    __('goldplus-statistic.error.filter.selected_filter_not_valid')
                );
        }
    }

    /**
     * Get Goldplus visiting statistic
     * 
     * @param int $songId
     * @param GoldplusStatisticRequest $goldplusStatisticRequest
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    private function getGpVisitingStatistic(
        int $songId, 
        GoldplusStatisticRequest $goldplusStatisticRequest,
        User $user
        ): JsonResponse
    {
        $reportType = $goldplusStatisticRequest->filter_list;
        return Api::responseData([
            'data' => $this->ownerGoldplusStatistic
                ->getStatisticData(
                    GoldplusStatisticType::VISIT, 
                    $reportType,
                    $songId,
                    $user->id
                )
        ]);
    }

    /**
     * Get Goldplus visiting statistic
     * 
     * @param int $songId
     * @param GoldplusStatisticRequest $goldplusStatisticRequest
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    private function getGpUniqueVisitingStatistic(
        int $songId, 
        GoldplusStatisticRequest $goldplusStatisticRequest,
        User $user): JsonResponse
    {
        $reportType = $goldplusStatisticRequest->filter_list;
        return Api::responseData([
            'data' => $this->ownerGoldplusStatistic
                ->getStatisticData(
                    GoldplusStatisticType::UNIQUE_VISIT, 
                    $reportType,
                    $songId,
                    $user->id
                )
        ]);
    }

    /**
     * Get Goldplus Chat statistic
     * 
     * @param int $songId
     * @param GoldplusStatisticRequest $goldplusStatisticRequest
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    private function getGpChatStatistic(
        int $songId, 
        GoldplusStatisticRequest $goldplusStatisticRequest,
        User $user): JsonResponse
    {
        $reportType = $goldplusStatisticRequest->filter_list;
        return Api::responseData([
            'data' => $this->ownerGoldplusStatistic
                ->getStatisticData(
                    GoldplusStatisticType::CHAT, 
                    $reportType,
                    $songId,
                    $user->id
                )
        ]);
    }

    /**
     * Get Goldplus Favorite statistic
     * 
     * @param int $songId
     * @param GoldplusStatisticRequest $goldplusStatisticRequest
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    private function getGpFavoriteStatistic(
        int $songId, 
        GoldplusStatisticRequest $goldplusStatisticRequest,
        User $user
        ): JsonResponse
    {
        $reportType = $goldplusStatisticRequest->filter_list;
        return Api::responseData([
            'data' => $this->ownerGoldplusStatistic
                ->getStatisticData(
                    GoldplusStatisticType::FAVORITE, 
                    $reportType,
                    $songId,
                    $user->id
                )
        ]);
    }
}
