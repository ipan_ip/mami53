<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use App\Entities\Reward\Reward;
use App\Entities\Reward\RewardRedeem;
use App\Presenters\RewardPresenter;
use App\Http\Helpers\ApiResponse as Api;
use App\Repositories\Reward\RewardRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RewardController extends BaseController
{
    protected $repo;
    protected $presenter;

    protected const DEFAULT_PAGINATION = 20;

    public function __construct(RewardRepository $repo, RewardPresenter $presenter)
    {
        $this->repo = $repo;
        $this->presenter = $presenter;

        $this->repo->setPresenter($this->presenter);
    }

    public function index()
    {
        $user = $this->user();
        if (!$user) {
            return Api::responseUserFailed();
        }

        $startDate = request('start_date') ? Carbon::parse(request('start_date'))->startOfDay() : null;
        $endDate = request('end_date') ? Carbon::parse(request('end_date'))->endOfDay() : null;

        $this->presenter->setResponseType(RewardPresenter::RESPONSE_TYPE_REWARD_LIST);
        $list = $this->repo->getListByDate($user, $startDate, $endDate);

        // filter all_redeemed state
        $list['all_redeemed'] = false;
        if (!empty($list['data'])) {
            $redeemedReward = collect($list['data'])->filter(function ($item) {
                return !empty($item['redeem'])  && in_array(0, $item['remaining_quota']);
            })->count();
            $allReward = count($list['data']);
            $list['all_redeemed'] = ($redeemedReward === $allReward);
        }

        // filter to remove empty quota
        if (!empty($list['data']) && !$list['all_redeemed']) {
            foreach ($list['data'] as $key => $item) {
                if (in_array(0, $item['remaining_quota'])) {
                    unset($list['data'][$key]);
                }
            }

            // re-index the modified array
            $list['data'] = array_values($list['data']);
        }

        return Api::responseData($list);
    }

    public function show($id)
    {
        $user = $this->user();
        if (!$user) {
            return Api::responseUserFailed();
        }

        $rewardPresenter = new RewardPresenter($user);
        $this->repo->setPresenter($rewardPresenter);
        $rewardPresenter->setResponseType(RewardPresenter::RESPONSE_TYPE_REWARD_DETAIL);
        $detail = $this->repo->getDetail($id, $user);
        if (!$detail) {
            return Api::responseError(false, false, 200, 404);
        }

        return Api::responseData($detail);
    }

    public function redeem($id, Request $request)
    {
        $user = $this->user();
        if (!$user) {
            return Api::responseUserFailed();
        }

        $this->presenter->setResponseType(RewardPresenter::RESPONSE_TYPE_REWARD_DETAIL);
        $detail = $this->repo->getDetail($id, $user);
        if (!$detail) {
            return Api::responseError('Reward not found or doesnt exists', false, 200);
        }

        $validationRule = [];
        $specialType = ['shipping_goods', 'virtual_goods_pln', 'virtual_goods_pulsa'];
        if (in_array($detail['data']['type'], $specialType)) {
            $validationRule = [
                'recipient_name' => 'required',
                'recipient_phone' => 'required',
                'recipient_data' => 'required',
                'recipient_notes' => 'nullable',
            ];
        }
        $validator = Validator::make($request->all(), $validationRule);

        if ($validator->fails()) {
            return Api::responseError('Invalid Request', false, 200);
        }

        try {
            $redeem = $this->repo->redeem($id, $user, $request->all());
            $redeemId = $this->repo->getUniquePublicId();
    
            return Api::responseData(['redeem_id' => $redeemId]);
        } catch (\Throwable $th) {
            $errMessage = $th->getMessage() ?: 'Error happen';
            return Api::responseError($errMessage, false, 200);
        }
    }

    public function userRedeemList()
    {
        $user = $this->user();
        if (!$user) {
            return Api::responseUserFailed();
        }

        $targetStatus = request('target', 'all');
        $limit = (int) request('limit', self::DEFAULT_PAGINATION);

        $this->presenter->setResponseType(RewardPresenter::RESPONSE_TYPE_REDEEM_LIST);
        $redeemList = $this->repo->getUserRedeemList($user, $targetStatus, $limit);
        $redeemList = $redeemList ?? ['data' => []];

        $response = $this->transformDataPagination($redeemList);

        return Api::responseData($response);
    }
    
    public function userRedeemDetail($publicId)
    {
        $user = $this->user();
        if (!$user) {
            return Api::responseUserFailed();
        }

        $this->presenter->setResponseType(RewardPresenter::RESPONSE_TYPE_REDEEM_DETAIL);
        $redeemDetail = $this->repo->getUserRedeemDetail($user, $publicId);
        $redeemDetail = $redeemDetail ?? ['data' => null];

        return Api::responseData($redeemDetail);
    }

    private function transformDataPagination($data): array
    {
        $pagination = array_key_exists('meta', $data) ? $data['meta']['pagination'] : null;
        $transformedData = [
            'data' => $data['data'],
            'has_next' => ($pagination['current_page'] < $pagination['total_pages']),
            'current_page' => $pagination['current_page'],
            'limit' => $pagination['per_page'],
            'total_pages' => $pagination['total_pages']
        ];

        return $transformedData;
    }
}
