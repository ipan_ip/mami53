<?php
namespace App\Http\Controllers\Api;

use App\Http\Helpers\ApiResponse;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Helpers\ApiHelper as Api;

use App\Repositories\TagRepository;
use App\Validators\TagValidator;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;


class TagController extends BaseController
{
    public function __construct(TagRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;
    }

    public function index(Request $request)
	{
        if ($request->filled('categorized')) {
            $tags = $this->repository->getCategorizedTags();
        } else {
            $type = $request->get('type', 'all');

            $tags = $this->repository->getAllTags($type);
        }

        return Api::responseData(array('tags' => $tags));
	}

    public function getFacilities(Request $request, $type = '')
    {
    	$data = '';

    	if($type == '') {
    		$data = $this->repository->getFacilities();
    	} elseif ($type == 'marketplace') {
    		$data = $this->repository->getMarketplaceFacilities();
    	} elseif ($type == 'apartment') {
            $v = 0;
            if ($request->filled('v')) $v = 1;
    		$data = $this->repository->getApartmentFacilities($v);
    	} else if ($type == 'vacancy') {
            $data = $this->repository->getVacancyFacilities();
        }
    	
        return ApiResponse::responseData($data);
    }

    public function getFacilitiesFilter()
    {
        $data = $this->repository->getFacilitiesForFilter();

        return ApiResponse::responseData($data);
    }
}
