<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseController;
use App\Http\Helpers\ApiResponse as Api;
use Validator;
use App\Entities\Feedback\SurveyBrandAcknowledgement;

/**
 * #growthsprint1
 * Currently doesn't use repository pattern, since this will be only single function at the moment
 */
class SurveyBrandAcknowledgementController extends BaseController
{

    protected $validationMessages = [
        'answer.required' => 'Jawaban harus diisi',
    ];

    public function __construct()
    {
        parent::__construct();
    }

    public function submitSurvey(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'answer' => 'required'
        ], $this->validationMessages);

        if($validator->fails()) {
            return Api::responseData([
                'status'=>'false',
                'meta' => [
                    'message' => implode('. ', $validator->errors()->all()),
                    'messages' => $validator->errors()->all()
                ]
            ]);
        }

        $survey = new SurveyBrandAcknowledgement;
        $survey->answer = $request->answer;
        $survey->save();

        return Api::responseData([
            'status' => true
        ]);
    }
}