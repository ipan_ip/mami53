<?php

namespace App\Http\Controllers\Api\Singgahsini\Registration;

use App\Entities\Api\ErrorCode;
use App\Entities\Media\Media;
use App\Entities\Singgahsini\Registration\SinggahsiniRegistration;
use App\Http\Controllers\Controller;
use App\Http\Helpers\ApiResponse;
use App\Http\Helpers\ApiErrorHelper;
use App\Repositories\Singgahsini\RegisterRepository;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Validator as ValidatorResponse;

class RegistrationController extends Controller
{
    protected $validatorUploadPhotosMessage = [
        'photo.required' => 'Photo tidak boleh kosong',
        'photo.mimes' => 'Photo harus .png, .jpg atau .jpeg',
        'photo.max' => 'Ukuran photo terlalu besar, ukuran maksimal 2MB',
        'type.required' => 'Type tidak boleh kosong',
        'type.in' => 'Type tidak terdaftar'
    ];

    protected $validatorRegisterMessage = [
        'owner_name.required' => 'Nama owner tidak boleh kosong',
        'phone_number.required' => 'Nomor telepon owner tidak boleh kosong',
        'kost_name.required' => 'Nama kost tidak boleh kosong',
        'city.required' => 'Kota/kabupaten tidak boleh kosong',
        'address.required' => 'Alamat tidak boleh kosong',
        'kebutuhan.required' => 'Kebutuhan tidak boleh kosong',
        'province.required' => 'Provinsi tidak boleh kosong',
        'subdistrict.required' => 'Kecamatan tidak boleh kosong',
        'total_room.required' => 'Jumlah kamar tidak boleh kosong',
        'total_room.numeric' => 'Jumlah kamar tidak boleh huruf',
        'available_room.required' => 'Kamar tersedia tidak boleh kosong',
        'available_room.numeric' => 'Kamar tersedia tidak boleh huruf',
        'price_monthly.required' => 'Harga bulanan tidak boleh kosong',
        'price_monthly.numeric' => 'Harga tidak boleh huruf',
        'foto_bangunan_tampak_depan.required' => 'ID foto bangunan tampak depan tidak boleh kosong',
        'foto_dalam_kamar.required' => 'ID foto dalam kamar tidak boleh kosong',
        'general_facility.required' => 'Fasilitas umum tidak boleh kosong',
        'ac.required' => 'Pilihan AC tidak boleh kosong',
        'ac.in' => 'Pilihan AC tidak terdaftar',
        'bathroom.required' => 'Pilihan kamar mandi tidak boleh kosong',
        'bathroom.in' => 'Pilihan kamar mandi tidak terdaftar',
        'wifi.required' => 'Pilihan wifi tidak boleh kosong',
        'wifi.in' => 'Pilihan wifi tidak terdaftar',
    ];

    public const SUCCESS_UPLOAD_PHOTO = 'Berhasil upload photo.';
    public const SUCCESS_OWNER_REGISTRATION = 'Berhasil kirim data calon owner.';

    /**
     * Function to retrieve uploaded kost photo from candidate owner
     * 
     * @param Request $request
     * 
     * @return JsonResponse
     */
    public function uploadPhoto(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            "photo" => "required|max:2000|mimes:jpg,jpeg,png",
            "type" => [
                "required",
                Rule::in(array_keys(SinggahsiniRegistration::PHOTO_VALUE)),
            ],
        ], $this->validatorUploadPhotosMessage);

        if ($validator->fails()) {
            $response = [
                'status' => false,
                'messages' => $validator->errors()->all()
            ];
            return ApiResponse::responseData($response);
        }

        $media = Media::postMedia(
            $request->file('photo'),
            'upload',
            null,
            null,
            null,
            Media::SINGGAHSINI_REGISTRATION,
            false,
            null,
            false,
            []
        );

        return ApiResponse::responseData([
            'status' => true,
            'data' => [
                'message' => self::SUCCESS_UPLOAD_PHOTO,
                'photo_id' => $media['id'],
                'type' => $request->get('type')
            ]
        ]);
    }

    /**
     * Function to retrieve form data from candidate owner
     * 
     * @param Request $request
     * 
     * @return JsonResponse
     */
    public function register(Request $request): JsonResponse
    {
        $validator = $this->validateMainForm($request);

        if ($validator->fails()) {
            $response = ApiErrorHelper::generateErrorResponse(
                ErrorCode::SINGGAHSINI_REGISTRATION,
                $validator->errors()->all()
            );

			return ApiResponse::responseData($response);
        }

        $register = app()->make(RegisterRepository::class)->register($request);

        return ApiResponse::responseData(
            [
                'status' => true,
                'data' => [
                    'message' => self::SUCCESS_OWNER_REGISTRATION,
                    'registration_id' => $register->id
                ]
            ],
            false,
            true,
            201
        );
    }

    /**
     * Validate retrieved request
     * 
     * @param Request $request
     * 
     * @return ValidatorResponse
     */
    private function validateMainForm(Request $request): ValidatorResponse
    {
        return Validator::make($request->all(), [
            'owner_name' => 'required',
            'phone_number' => 'required',
            'kost_name' => 'required',
            'city' => 'required',
            'address' => 'required',
            'kebutuhan' => 'sometimes|required',
            'province' => 'sometimes|required',
            'subdistrict' => 'sometimes|required',
            'total_room' => 'sometimes|required|numeric',
            'available_room' => 'sometimes|required|numeric',
            'price_monthly' => 'sometimes|required|numeric',
            'foto_bangunan_tampak_depan' => 'sometimes|required',
            'foto_dalam_kamar' => 'sometimes|required',
            'general_facility' => 'sometimes|required',
            'ac' => [
                'sometimes', 
                'required',
                Rule::in(array_keys(SinggahsiniRegistration::AC)),
            ],
            'bathroom' => [
                'sometimes', 
                'required',
                Rule::in(array_keys(SinggahsiniRegistration::KAMAR_MANDI)),
            ],
            'wifi' => [
                'sometimes', 
                'required',
                Rule::in(array_keys(SinggahsiniRegistration::WIFI)),
            ]
            
        ], $this->validatorRegisterMessage);
    }
}
