<?php

namespace App\Http\Controllers\Api;

use App\Entities\Activity\Call;
use App\Entities\Booking\BookingUser;
use App\Entities\Dbet\DbetLinkRegistered;
use App\Entities\Room\Element\Price;
use App\Entities\Room\Room;
use App\Entities\Room\RoomFilter;
use App\Entities\Room\RoomOwner;
use App\Entities\Mamipay\MamipayContract;
use App\Http\Helpers\ApiHelper;
use App\Presenters\RoomPresenter;
use App\Repositories\RoomRepository;
use App\Transformers\Cms\RoomTransformer;
use App\Transformers\Room\RoomHasBookingRequestTransformer;
use App\User;
use App\Validators\RoomValidator;
use Illuminate\Http\Request;
use App\Http\Helpers\ApiResponse as Api;
use App\Http\Helpers\RegexHelper;
use App\Entities\Room\ContactUser;
use Illuminate\Support\Facades\Mail;
use App\Entities\Activity\ActivationCode;
use App\Entities\Activity\ActivationCodeType;
use App\Notifications\PhoneNumberVerification;
use App\Libraries\SMSLibrary;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Notification;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use App\Entities\Room\Element\Card;
use App\Entities\Owner\SurveySatisfaction;
use Illuminate\Support\Facades\Cache;
use App\Entities\Activity\TrackingFeature;
use App\Entities\Generate\Currency;
use App\Entities\Room\RoomPriceType;
use App\Entities\Tracker\OnlineUser;
use App\Entities\Booking\BookingDesigner;
use App\Entities\Booking\BookingDiscount;
use Illuminate\Http\JsonResponse;
use App\Entities\Owner\ActivityType;
use App\Enums\Goldplus\Permission\GoldplusPermission;
use App\Http\Helpers\GoldplusPermissionHelper;
use App\Jobs\KostIndexer\IndexKostJob;
use App\Jobs\Owner\MarkActivity;
use App\Rules\PriceProperty;
use App\Services\Thanos\ThanosService;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use App\Repositories\Room\RoomUnitRepository;
use Exception;

class OwnerController extends BaseController
{
    private $repository;
    private $validator;
    private $service;
    private $lastCodeRequested;

    protected $validationMessages = [
        'room_available.required'     => 'Ketersediaan Ruangan harus diisi',
        'price_yearly.numeric'        => 'Harga Sewa Tahunan harus berupa angka',
        'price_monthly.required'      => 'Harga Sewa Bulanan harus diisi',
        'price_monthly.numeric'       => 'Harga Sewa Bulanan harus berupa angka',
        'price_monthly.min'           => 'Harga Sewa Bulanan harus diisi dan harus lebih dari 10000',
        'price_weekly.numeric'        => 'Harga Sewa Mingguan harus berupa angka',
        'price_daily.numeric'         => 'Harga Sewa Harian harus berupa angka',
        '3_month.numeric'             => 'Harga Sewa 3 Bulanan harus berupa angka',
        '6_month.numeric'             => 'Harga Sewa 6 Bulanan harus berupa angka',
    ];

    /**
     * OwnerController constructor.
     * @param RoomRepository $repository
     * @param RoomValidator $validator
     * @param ThanosService $service
     */
    public function __construct(RoomRepository $repository, RoomValidator $validator, ThanosService $service)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->service    = $service;

        parent::__construct();
    }

    public function getProfileOwner(Request $request)
    {
        if ($this->user() == null) throw new \Exception(\Lang::get('api.authorization.signin_required'), 333);
        $v1 = null;
        if ($request->filled('v')) {
            if ($request->input('v') == '1') $v1 = 1;
        }
        $data = User::getDetailOwner($this->user()->id, $v1);

        return Api::responseData($data);
    }

    public function getNewsOwner(Request $request)
    {
        if ($this->user() == null) throw new \Exception(\Lang::get('api.authorization.signin_required'), 333);

        $response = $this->repository->getNewsOwner($this->user());
        return Api::responseData($response);
    }

    /**
     *  Request verification code for owners
     * 
     *  Allow owner's phone number verification ONLY for owner using older version of the app
     *  This function only exist due to backwards compatibility for owners using older version of the app.
     *  Please do not use this function anymore and remove it in the future
     * 
     *  @deprecated MamikosWeb 1.7.5
     *  @see self::postRequestVerification
     */
    public function postVerification(Request $request)
    {
        $user = $this->user();

        if(!$user) {
            return Api::responseData([
                'status'=>false,
                'meta'=> [
                    'message'=>'Silakan login terlebih dahulu'
                ]
            ]);
        }

        $data = User::SendCodeVerification($user, $for = 'owner_verification');

        return Api::responseData(array('status' => $data['status'], 'meta' => array('message' => $data['message'])));
    }

    /**
     *  Check if code request was too frequent
     * 
     *  @param string $phoneNumber Receiver of the code
     *  @param int $minutesPerRequest Number of minutes user need to wait before a new request
     */
    private function isTooFrequent(string $phoneNumber, int $minutesPerRequest) {
        $isAlreadyRequested = !empty($this->lastCodeRequested);
        
        // Check if user already has a code and is requesting a new one
        if ($isAlreadyRequested) {
            if ($this->lastCodeRequested->created_at->diffInMinutes(Carbon::now(), false) < $minutesPerRequest) {
                return true;
            }
        }
    }

    /**
     *  Delete existing code sent
     * 
     *  @param string $phoneNumber Receiver of the previous code
     */
    private function deletePreviousCode(string $phoneNumber) {
        $isAlreadyRequested = !empty($this->lastCodeRequested);

        if ($isAlreadyRequested) {
            // If code has not expired but user requested new code, delete the old one;
            ActivationCode::deleteLastCode($phoneNumber);
        }
    }

    /**
     *  Send veritifcation code to user
     *  
     *  @param User $user Receiver of the notification
     *  @param ActivationCode $activationCode Code to send
     *  @return boolean SMS status
     */
    private function sendVerificationCode(User $user, ActivationCode $activationCode) {
        Notification::send($user, new PhoneNumberVerification($activationCode));
    }

    /**
     *  Request verification code for owners
     *  
     *  Generate a new verification code and send it to currently authenticated user.
     *  Reject request if user already possess a verification code.
     */
    public function postRequestVerification(Request $request) {
        $validator = Validator::make($request->all(),
            [
                'phone_number' => 'required|min:8|max:14|unique:user,phone_number|regex:' . RegexHelper::phoneNumber(),
            ],
            [
                'phone_number.required' => __('api.input.phone_number.required'),
                'phone_number.regex' => __('api.input.phone_number.regex'),
                'phone_number.unique'   => __('api.input.phone_number.unique'),
            ]
        );

        if($validator->fails()) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => $validator->errors()->first()
                ]
            ]);
        }

        $phoneNumber = $request->phone_number;
        $this->lastCodeRequested = ActivationCode::where('phone_number', $phoneNumber)->where('for', ActivationCodeType::OWNER_VERIFICATION)->latest()->first();

        $minutesPerRequest = 1; // Minutes before new request is allowed
        if ($this->isTooFrequent($phoneNumber, $minutesPerRequest)) {
            return Api::responseData([
                'status' => false, 
                'meta' => [
                    'message' => __('api.owner.verification.code.too_frequent', ['minutes' => '01.00']) // The minute are formatted like this to conform with UX.
                ]
            ]);
        }

        $this->deletePreviousCode($phoneNumber);
        $activationCode = ActivationCode::generateActivationCode($phoneNumber, ActivationCodeType::OWNER_VERIFICATION);
        $activationCode->save();
        $this->sendVerificationCode((new User(['phone_number' => $phoneNumber])), $activationCode); // Create a placeholder model User for the phone number.

        return Api::responseData([
            'status' => true, 
            'meta' => [
                'message' => __('api.owner.verification.code.success')
            ]
        ]);
    }

    /**
     *  Verify Phone Number based on User's Request used by postVerification
     *
     *  @param Request 
     *  @see OwnerController::postVerification
     */
    public function postStoreVerification(Request $request)
    {
       // if ($this->user() == null) throw new \Exception(\Lang::get('api.authorization.signin_required'), 333);

        $user = $this->user();

        if(!$user) {
            return Api::responseData([
                'status'=>false,
                'meta'=> [
                    'message'=>'Silakan login terlebih dahulu'
                ]
            ]);
        }

       $response = ActivationCode::ownerVerification($this->user(), $request->all());
       
       // hide cache and change phone number user
       if ($response['status'] == true AND $request->filled('change_phone')) {
           if (Cache::has('changephoneuser:'.$user->id)) {
                $changePhoneNumber = User::changePhoneNumber($user, Cache::get('changephoneuser:'.$user->id));
                if ($changePhoneNumber) {
                    $deleteLatestCode = ActivationCode::deleteLastCode(Cache::get('changephoneuser:'.$user->id));
                }

                Cache::forget('changephoneuser:'.$user->id);
           }
       }

       return Api::responseData(array('status' => $response['status'], 'meta' => array('message' => $response['message'])));
    }

    /**
     *  Check whether verification code submitted againts a phone number is valid
     * 
     *  This function check if verification code match a given phone number. This will not
     *  verify the owner but simply check the validity of its phone number verification code.
     * 
     *  This method is case-sensitive to the verification code
     * 
     *  @param Request 
     *  @see OwnerController::postRequestVerification
     */
    public function postVerificationCheck(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'phone_number' => 'required|min:8|max:14|unique:user,phone_number|regex:' . RegexHelper::phoneNumber(),
                'code'              => 'required|alpha_num|size:4'
            ], 
            [
                'phone_number.required' => __('api.input.phone_number.required'),
                'phone_number.regex' => __('api.input.phone_number.regex'),
                'code.required'     => __('api.input.code.required'),
                'code.size'         => __('api.owner.verification.code.invalid'),
                'code.alpha_num'    => __('api.owner.verification.code.invalid'),
            ]
        );

        if($validator->fails()) {
            return Api::responseData([
                'status'=>false,
                'meta' => [
                    'message' => $validator->errors()->all()
                ]
            ]);
        }

        $code = ActivationCode::where([
            ['expired_at', '>', Carbon::now()],
            ['phone_number', '=', $request->phone_number],
            ['for', '=', ActivationCodeType::OWNER_VERIFICATION]
        ])
        ->whereRaw('BINARY `code`=?', $request->code) // Case-sensitive search so that H71E != H71e
        ->first();

        if (is_null($code)) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => __('api.owner.verification.code.invalid')
                ]
            ]);
        }

        return Api::responseData([
            'status' => true,
            'meta' => [
                'message' => __('api.owner.verification.code.success')
            ]
        ]);
    }

    public function editEmail(Request $request)
    {
        if ($this->user() == null) return Api::responseData(array('email' => false, 'message' => 'Gagal mengganti email'));

        $changeEmail = User::changeEmail($this->user()->id, $request->input('email'));

        return Api::responseData($changeEmail);
    }

    /**
     * Only for setting new password, don't use to update password
     *
     */
    public function setPassword(Request $request)
    {
        $user = $this->user();

        if(!$user) {
            return Api::responseData([
                'status'=>false,
                'meta'=> [
                    'message'=>'Silakan login terlebih dahulu'
                ]
            ]);
        }

        $passwordMinLength = 6;
        $validator = Validator::make($request->all(),
                        [
                            'password'=>'required|min:' . $passwordMinLength
                        ],
                        [
                            'password.required'=>'Password harus diisi',
                            'password.min'=>'Password minimal ' . $passwordMinLength . ' karacter'
                        ]);

        if($validator->fails()) {
            return Api::responseData([
                'status'=>false,
                'meta' => [
                    'message'=>implode('. ', $validator->errors()->all())
                ]
            ]);
        }

        // validate if user is already had password before
        if($user->password != '' && !is_null($user->password)) {
            return Api::responseData([
                'status'=>false,
                'meta'=>[
                    'message'=>'Anda sudah punya password sebelumnya'
                ]
            ]);
        }

        $user->password = Hash::make($request->password);
        $user->save();

        return Api::responseData([
            'status'=>true
        ]);
    }

    /**
     * Function to change password, only for user that already had password
     */
    public function updatePassword(Request $request)
    {
        $user = $this->user();

        if(!$user) {
            return Api::responseData([
                'status'=>false,
                'meta'=> [
                    'message'=>'Silakan login terlebih dahulu'
                ]
            ]);
        }

        $validator = Validator::make($request->all(),
                        [
                            'old_password'=>'required|min:6',
                            'new_password'=>'required|min:6'
                        ],
                        [
                            'old_password.required'=>'Password Lama harus diisi',
                            'new_password.required'=>'Password Baru harus diisi',
                            'old_password.min' => 'Password Lama minimal :min karakter',
                            'new_password.min' => 'Password Baru minimal :min karakter'
                        ]);

        if($validator->fails()) {
            return Api::responseData([
                'status'=>false,
                'meta' => [
                    'message'=>implode('. ', $validator->errors()->all())
                ]
            ]);
        }

        // validate old password
        if(!Hash::check($request->old_password, $user->password)) {
            return Api::responseData([
                'status'=>false,
                'meta' => [
                    'message'=>'Password Lama Anda tidak valid.'
                ]
            ]);
        }

        $user->password = Hash::make($request->new_password);
        $user->save();

        return Api::responseData([
            'status'=>true
        ]);
    }

    public function getListCallRoom(Request $request, $songId)
    {
        if ($this->user() == null) {
            return array('call' => array(),
                         'clickable'     => 'Untuk melihat nomor hp anda harus premium',
                         'feature' => 'list histori dan telp dengan user , silakan upgrade akun anda menjadi premium.',
                         'mamikos_phone' => '+6287738724848',
                         'count' => 0,
                         'email' => false
                        );
        }

        $room = Room::where('song_id', $songId)->first();
        $call   = ContactUser::getCallRoomUser($room->id, $this->user());
        OnlineUser::insertData($this->user());
        return Api::responseData($call);
    }

    public function insertOnlineData(Request $request)
    {
        if (is_null($this->user())) {
            return Api::responseData(['status' => false]);
        }
        OnlineUser::insertData($this->user());
        return Api::responseData(['status' => true]);
    }

    public function getChatOwner(Request $request, $songId)
    {
        $user = $this->user();
        $room = Room::with(['level'])->where('song_id', $songId)->first();
        
        if (is_null($user) or is_null($room)) {
           return Api::responseData([
                'chat' => [],
                'clickable' => 'Untuk melihat nomor hp anda harus premium',
                'feature' => 'list histori dan membalas chat, silakan upgrade akun anda menjadi premium.',
                'count' => 0,
                'page_total' => 0,
                'email' => false
            ]);
        }
        
        $chat = Call::getChatRoom($room->id, $user, true);
        
        // TODO: When PERFECT SENDBIRD ERA comes, please remove this codes - start
        $device = $this->device();
        if (is_null($device) === false && 
            $device->getIsSendBirdSupported() === false)
        {
            if (empty($chat['chat']) === false)
            {
                foreach ($chat['chat'] as $index => $channel)
                {
                    $chat['chat'][$index]['name'] = 'Mau chat? Yuk update ke versi terbaru!';
                }
            }
        }
        // TODO: When PERFECT SENDBIRD ERA comes, please remove this codes - end

        OnlineUser::insertData($user);
        return Api::responseData($chat);
    }

    public function pinnedAction(Request $request)
    {
        if ( $this->user()->id != null AND date('Y-m-d') <= $this->user()->date_owner_limit ) {
           $status = $request->input('status');
           // This argument is for check owner premium membership 
           // the value and argument is hardcoded in apps
           $flowVersion = $request->get('flow');

           $act = $this->repository->pinnedByOwner($this->user()->id, $request->all(), $flowVersion);

           if (isset($act['data']) && !empty($act['data'])) {

               if ($act['data']->is_active == 1) $addViewPromote = $act['data']->total;
               else $addViewPromote = 0;

               if ($act['data']->is_active == 0) {
                 $addView = 0;
               } else {
                 $addView = $act['data']->used;
               }

               $historyView  = ($act['data']->total/* + $act['data']->history*/) - $addView;
               $used_used    = ($act['data']->history - $addView) . '/' . $act['data']->total /*($historyView - $addView)*/;
               $usePercen    = ( $act['data']->history / $act['data']->total ) * 100;
               $view_promote = $act['data']->total;

               $used_promote = $act['data']->history - $addView;
           } else {
               $used_used    = '0/0';
               $usePercen    = 0;
               $view_promote = 0;
               $used_promote = 0;
           }

            if (Cache::has('popupallocated:'.$request->input('song_id'))) Cache::forget('popupallocated:'.$request->input('song_id'));


           $pinned = array(
                 'pinned'          => $act['status'],
                 'message'         => $act['message'],
                 'song_id'         => $request->input('song_id'),
                 'views'           => $request->input('views'),
                 'active'          => $request->input('status'),
                 'view_promote'    => $view_promote,
                 'used_promote'    => $used_promote,
                 'progress_promote'=> $used_used,
                 'percent_promote' => number_format($usePercen,1,',','')
            );

        } else {
           $pinned = array(
                 'pinned'          => false,
                 'message'         => 'Maaf akun anda sudah habis, silakan upgrade premium',
                 'song_id'         => $request->input('song_id'),
                 'views'           => $request->input('views'),
                 'active'          => $request->input('status'),
                 'view_promote'    => 0,
                 'used_promote'    => 0,
                 'progress_promote'=> '0/0',
                 'percent_promote' => 0
            );
        }

        return Api::responseData($pinned);
    }

    public function getClaimOwner(Request $request, $_id)
    {
        $room = Room::where('song_id', $_id)->firstOrFail();

        $data = [
            'room_title' => $room->name,
            'address' => $room->address
        ];

        return Api::responseData(['room' => $data]);
    }

    /**
    * This function will claim room by owner
    * Since this will be called by web, we need to check if user is real owner
    *
    * @param Request        $request
    *
    */
    public function postClaimRoomByOwner(Request $request)
    {
        $user = $this->user();

        if (! $user instanceof User || ! $user->isOwner()) {
            return Api::responseData(
                [
                    'message'=>'Access Denied'
                ],
                false,
                false,
                200,
                'Access Denied');
        }

        try {
            return $this->postClaimOwner($request);
        } catch (Exception $exception) {
            return Api::responseData(
                [
                    'message' => $exception->getMessage()
                ],
                false,
                false,
                200,
                'Exception happen'
            );
        }
    }

    public function postClaimOwner(Request $request)
    {
        ApiHelper::isValidInput(
            $request->all(),
            array(
                '_id' => 'required',
                'status' => 'required'
            )
        );
        $user = $this->user();

        if ($user != null) {

            if ($user->is_owner == 'false') {
                $data = array(
                        'status' => false,
                        'data'   => array()
                    );
                return Api::responseData($data);
            }
        }

        $room = Room::with('owners')->where('song_id', $request->get('_id'))->first();
        if ($room == null) {
            throw new \Exception('Room not found', 256);
        }
        if (count($room->owners) > 0) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'code' => 422,
                    'message' => 'Kos sudah diklaim oleh user lain'
                ]
            ]);
        }

        $ownerData = array(
            'user_id' => $user->id,
            'id' => $room->id,
            'owner_status' => $request->get('status')
        );

        $response = RoomOwner::add($ownerData, RoomOwner::ROOM_CLAIM_STATUS);
        return Api::responseData(['data' => $response]);
    }

    public function postSaveOwnerDesignerViaWeb(Request $request)
    {
        if(!$this->user()->is_owner) {
            return Api::responseData(
                ['message'=>'Access Denied'],
                false,
                false,
                200,
                'Access Denied');
        }

        return $this->postSaveOwnerDesigner($request);
    }

    public function postSaveOwnerDesigner(Request $request)
    {

        if ($this->user() == null) {
            $data = array(
                        'status' => false,
                        'stories'=> array()
                    );
            return Api::responseData($data);
        }

        if ($this->user() != null) {

            if ($this->user()->is_owner == 'false') {
                $data = array(
                        'status' => false,
                        'stories'=> array()
                    );
                return Api::responseData($data);
            }

        }

        $roomData = $request->all();
        $room = $this->repository->saveJson($roomData);

        // add info to owner room table
        $ownerData = array(
            'id' => $room->id,
            'user_id'     => $this->user()->id,
            'owner_status'=> $room->agent_status
        );

        if(in_array(strtolower($room->agent_status), ['pemilik kos', 'pengelola kos'])) {
            if (strpos(url()->current(), 'draft') == true ) {
                $owner = RoomOwner::add($ownerData, RoomOwner::ROOM_EARLY_EDIT_STATUS);
            } else if (strpos(url()->current(), 'save') == true) {
                $owner = RoomOwner::add($ownerData, RoomOwner::ROOM_ADD_STATUS);
            }
        }

        return Api::responseData(['stories' => $room]);
    }


    public function getListOwner(Request $request)
    {
        $user = $this->user();

        if (is_null($user)) {
            throw new \Exception(\Lang::get('api.authorization.signin_required'), 333);
        }
        
        $roomData = RoomOwner::ownerListRoom($request->all(), $user);

        return Api::responseData([
            'rooms' => $roomData['room'],
            'count' => $roomData['count']
        ]);
    }

    /**
     * Get data for update using new flow
     *
     * @param int $id Song ID
     */
    public function getKostDataForUpdate(Request $request, $id)
    {
        $user = $this->user();

        if(!$user) {
            return Api::responseData([
                'status'=>false,
                'meta'=>[
                    'message'=>'User tidak ditemukan',
                    'messages'=>['User tidak ditemukan']
                ]
            ]);
        }

        $room = Room::with(['owners', 'tags.types', 'tags.photo', 'tags.photoSmall', 'cards', 'cards.photo', 'level'])
                    ->where('song_id', $id)
                    ->first();

        if(!$room) {
            return Api::responseData([
                'status'=>false,
                'meta'=>[
                    'message'=>'Kost tidak ditemukan',
                    'messages'=>['Kost tidak ditemukan']
                ]
            ]);
        }

        $owner = (count($room->owners) > 0) ? $room->owners->first() : null;

        if(!$owner || (!is_null($owner) && $owner->user_id != $user->id)) {
            return Api::responseData([
                'status'=>false,
                'meta'=>[
                    'message'=>'Anda bukan pemilik dari kost ini',
                    'messages'=>['Anda bukan pemilik dari kost ini']
                ]
            ]);
        }

        $facility = $room->facility();
        $facilityCategorized = [
            'fac_room' => array_values(array_unique(array_merge($facility->room_ids(), $facility->priceIds()))),
            'fac_bath' => array_values(array_unique($facility->bath_ids())),
            'fac_share' => array_values(array_unique($facility->share_ids())),
            'fac_near' => array_values(array_unique($facility->near_ids())),
            'fac_parking' => array_values(array_unique($facility->park_ids()))
        ];

        $photos = Card::categorizeCardWithoutCardId($room);

        $prices = $room->getRealPrices();
        $isPriceFlashSale = $room->getFlashSaleRentType();

		return Api::responseData([
            'data' => [
                'name' => $room->name,
                'slug' => $room->share_url,
                'address' => $room->address,
                'latitude'=> $room->latitude,
                'longitude' => $room->longitude,
                'area_city' => $room->area_city,
                'area_subdistrict' => $room->area_subdistrict,
                'gender' => $room->gender,
                'size' => $room->size,
                'room_count' => $room->room_count,
                'room_available' => $room->room_available,
                'facilities' => $facilityCategorized,
                'price_remark' => $room->price_remark,
                'min_month' => array_values(array_unique($facility->keyword_ids())),
                'description' => $room->description,
                'remarks' => $room->remark,
                'photos' => $photos,
                'youtube_link' => !is_null($room->youtube_id) && $room->youtube_id != '' ? 'https://youtube.com/watch?v=' . $room->youtube_id : '',
                'owner_name' => !is_null($room->owner_name) ? $room->owner_name : '',
                'owner_phone' => !is_null($room->owner_phone) ? $room->owner_phone : '',
                'owner_email' => !is_null($room->owner_email) ? $room->owner_email : '',
				'building_year' => $room->building_year,

				'price_daily' => $prices['price_daily'],
				'price_weekly' => $prices['price_weekly'],
				'price_monthly' => $prices['price_monthly'],
				'price_3_month' => $prices['price_3_month'],
				'price_6_month' => $prices['price_6_month'],
                'price_yearly' => $prices['price_yearly'],
                'is_price_flash_sale' => $isPriceFlashSale
            ]
        ]);
    }

    /**
     * Get Apartment data for update using new flow
     *
     * @param int $id Song ID
     */
    public function getApartmentDataForUpdate(Request $request, $id)
    {
        $user = $this->user();

        if(!$user) {
            return Api::responseData([
                'status'=>false,
                'meta'=>[
                    'message'=>'User tidak ditemukan',
                    'messages'=>['User tidak ditemukan']
                ]
            ]);
        }

        $room = Room::with(['owners', 'tags', 'tags.photo', 'tags.photoSmall', 'cards',
                        'cards.photo', 'minMonth', 'price_components', 'apartment_project', 'room_price_type' => function($p) {
                            $p->where('is_active', 1);
                        }])
                    ->where('song_id', $id)
                    ->first();

        if(!$room) {
            return Api::responseData([
                'status'=>false,
                'meta'=>[
                    'message'=>'Kost tidak ditemukan',
                    'messages'=>['Kost tidak ditemukan']
                ]
            ]);
        }

        $owner = (count($room->owners) > 0) ? $room->owners->first() : null;

        if(!$owner || (!is_null($owner) && $owner->user_id != $user->id)) {
            return Api::responseData([
                'status'=>false,
                'meta'=>[
                    'message'=>'Anda bukan pemilik dari kost ini',
                    'messages'=>['Anda bukan pemilik dari kost ini']
                ]
            ]);
        }

        $facility = $room->facility();

        $photos = Card::categorizeCardWithoutCardId($room);

        $priceComponents  = $room->price_components;
        $priceComponentsFormatted = [];
        $priceComponentsFormattedApp = [];
        if(count($priceComponents) > 0) {
            foreach ($priceComponents as $priceComponent) {
                $priceFormatted = [
                    'name'=>$priceComponent->price_name,
                    'label'=>$priceComponent->price_label,
                    'price_reguler'=>$priceComponent->price_reguler,
                    'price_reguler_formatted' => 'Rp. ' . number_format($priceComponent->price_reguler, 0, ',', '.'),
                    'sale_price'=>$priceComponent->sale_price,
                    'sale_price_formatted' => 'Rp. ' . number_format($priceComponent->sale_price, 0, ',', '.')
                ];

                $priceComponentsFormatted[] = $priceFormatted;

                $priceComponentsFormattedApp[$priceComponent->price_name] = $priceFormatted;
            }
        }

        $defaultUnitType = \App\Entities\Apartment\ApartmentProject::UNIT_TYPE;

        if (!is_null($room->room_price_type)) {
            $priceDaily = $room->price_daily_usd;
            $priceWeekly = $room->price_weekly_usd;
            $priceMonthly = $room->price_monthly_usd;
            $priceYearly = $room->price_yearly_usd;
            $priceType = 'usd';
        }

        $dataResponse = [
            'project_name' => $room->apartment_project->name,
            'unit_name' => $room->name,
            'unit_number' => $room->unit_number,
            'unit_type' => !in_array($room->unit_type, $defaultUnitType) ? 'Lainnya' : $room->unit_type,
            'unit_type_name' => !in_array($room->unit_type, $defaultUnitType) ? $room->unit_type : '',
            'floor' => $room->floor,
            'size' => $room->size,
            'price_type' => isset($priceType) ? $priceType : 'idr',
            'price_daily' => isset($priceDaily) ? $priceDaily : round($room->price_daily),
            'price_weekly' => isset($priceWeekly) ? $priceWeekly : round($room->price_weekly),
            'price_monthly' => isset($priceMonthly) ? $priceMonthly : round($room->price_monthly),
            'price_yearly' => isset($priceYearly) ? $priceYearly : round($room->price_yearly),
            'price_shown' => $room->price_shown,
            'description' => $room->description,
            'fac_room' => array_merge(
                array_values(array_unique($facility->room_ids())),
                array_values(array_unique($facility->bath_ids())),
                array_values(array_unique($facility->share_ids()))
            ),
            'photos' => $photos,
            'youtube_link' => !is_null($room->youtube_id) && $room->youtube_id != '' ? 'https://youtube.com/watch?v=' . $room->youtube_id : '',
            'min_month' => array_values(array_unique($facility->keyword_ids())),
            'price_components' => $priceComponentsFormatted, // for web
            'is_furnished' => $room->furnished == 1 ? true : false, // soon to be deprecated
            'furnished' => !is_null($room->furnished) ? $room->furnished : 0 // because we need to handle new furnished type
        ];

        // price for app
        $dataResponse['price_maintenance'] = isset($priceComponentsFormattedApp['maintenance'])  ?
            $priceComponentsFormattedApp['maintenance'] : null;
        $dataResponse['price_parking'] = isset($priceComponentsFormattedApp['parking'])  ?
            $priceComponentsFormattedApp['parking'] : null;

        return Api::responseData([
            'data'=>$dataResponse
        ]);
    }

    /**
    * This function will remove the ownership of the room
    * Since it will be called via web, we need to first check if the owner is real owner of the room
    *
    * @param Request        $request
    * @param int            $songId         room identifier
    */
    public function deleteRoomByOwner(Request $request, $songId)
    {
        $room = Room::with('view_promote', 'owners', 'level')->where('song_id', $songId)->first();
        if (is_null($room)) {
            return Api::responseData(['status' => false,
                                      'meta' => ['message' => 'Kost tidak ditemukan'],
                                      '_id' => 0,
                                      'is_deleted' => 0
                                    ]);
        }

        if(!$room->ownedBy($this->user())) {
            // return error if the authenticated user is not the real owner of this room
            return Api::responseData(['status' => false,
                                      'meta' => ['message' => 'Anda bukan pemilik kos ini.'],
                                      '_id' => 0,
                                      'is_deleted' => 0
                                    ]);
        }

        if (!GoldplusPermissionHelper::isRoomAllow($room, GoldplusPermission::ROOM_DELETE)) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => __('api.access.forbidden'),
                ],
                '_id' => 0,
                'is_deleted' => 0,
            ]);
        }
        
        $deactivate = $room->deactivate('owner');
        if ($deactivate !== true) {
            return Api::responseData(['status' => false,
                                      'meta' => ['message' => $deactivate],
                                      '_id' => 0,
                                      'is_deleted' => 0
                                    ]);
        }

        return Api::responseData(array(
            '_id'        => $songId,
            'is_deleted' => 1
        ));
    }

    /**
     * Update available Room
     *
     * @param \Illuminate\Http\Request $request
     * @param string|integer $_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateRoomAvailable(Request $request, $_id): JsonResponse
    {
        $room = Room::where('song_id', $_id)->first();
        if (is_null($room)) {
            return Api::responseData([
                "status" => false,
                "meta" => [
                    "code" => 404,
                    "message" => "Kost / Apartemen tidak ditemukan"
                ]
            ]);
        }
        
        if (is_null($room->apartment_project_id)) {
            return $this->updateKost($request, $_id);
        } else {
            return $this->updateApartment($request, $_id);
        }
    }

    /**
     * Update Kamar Harga Kost
     *
     * @param \Illuminate\Http\Request $request
     * @param string|integer $_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateKost(Request $request, $_id)
    {
        $user = $this->user();
        if (is_null($user)) {
            return Api::responseError(false, false, 401, 401);
        }

        $room = Room::where('song_id', $_id)->first();
        if (is_null($room)) {
            return Api::responseError(false, false, 404, 404);
        }

        if (!GoldplusPermissionHelper::isRoomAllow($room, GoldplusPermission::PRICE_UPDATE)) {
            return Api::responseError(false, false, 403, 403);
        }

        if (!$room->ownedBy($user)) {
            return Api::responseData(
                ['message'=>'Access Denied'],
                false,
                false,
                200,
                'Access Denied'
            );
        }

        $validator = Validator::make(
            $request->all(),
            [
                'room_available'  => 'required|numeric',
                'price_monthly'   => 'required|numeric|min:10000',
                'price_yearly'    => ['numeric', new PriceProperty('Harga Tahunan harus lebih besar 10000')],
                'price_weekly'    => ['numeric', new PriceProperty('Harga Mingguan harus lebih besar 10000')],
                'price_daily'     => ['numeric', new PriceProperty('Harga Harian harus lebih besar 10000')],
                '3_month'         => ['numeric', new PriceProperty('Harga 3 Bulanan harus lebih besar 10000')],
                '6_month'         => ['numeric', new PriceProperty('Harga 6 Bulanan harus lebih besar 10000')],
            ],
            $this->validationMessages
        );

        $roomPriceUpdate = [
            'price_daily'   => $request->input('price_daily', 0),
            'price_weekly'  => $request->input('price_weekly', 0),
            'price_monthly' => $request->input('price_monthly', 0),
            'price_yearly'  => $request->input('price_yearly', 0),
            'price_3_month' => $request->input('3_month', 0),
            'price_6_month' => $request->input('6_month', 0),
        ];

        $flashSale = $room->getFlashSaleRentType();
        $originalPrice = $room->getRealPrices();

        $validator->after(function ($validator) use ($request, $room, $roomPriceUpdate, $flashSale, $originalPrice) {
            if (!BookingDiscount::validateListingPrice($roomPriceUpdate['price_monthly'], $room, 'monthly')) {
                $validator->errors()->add('price_monthly', 'Harga bulanan yang dimasukkan terlalu rendah dari diskon');
            }

            if (!BookingDiscount::validateListingPrice($roomPriceUpdate['price_daily'], $room, 'daily')) {
                $validator->errors()->add('price_daily', 'Harga harian yang dimasukkan terlalu rendah dari diskon');
            }

            if (!BookingDiscount::validateListingPrice($roomPriceUpdate['price_weekly'], $room, 'weekly')) {
                $validator->errors()->add('price_weekly', 'Harga mingguan yang dimasukkan terlalu rendah dari diskon');
            }
           
            if (!BookingDiscount::validateListingPrice($roomPriceUpdate['price_yearly'], $room, 'annually')) {
                $validator->errors()->add('price_yearly', 'Harga tahunan yang dimasukkan terlalu rendah dari diskon');
            }

            if (!BookingDiscount::validateListingPrice($roomPriceUpdate['price_3_month'], $room, 'quarterly')) {
                $validator->errors()->add('3_month', 'Harga 3 bulan yang dimasukkan terlalu rendah dari diskon');
            }
            
            if (!BookingDiscount::validateListingPrice($roomPriceUpdate['price_6_month'], $room, 'semiannually')) {
                $validator->errors()->add('6_month', 'Harga 6 bulan yang dimasukkan terlalu rendah dari diskon');
            }

            if (array_sum($roomPriceUpdate) == 0) {
                $validator->errors()->add('price', 'Harga sewa harus diisi salah satu');
            }

            if ($request->filled('room_available')) {
                if ($request->input('room_available') > $room->room_count) {
                    $validator->errors()->add('room_available', 'Jumlah kamar tersedia tidak boleh melebihi jumlah kamar total');
                } else {
                    $activeContract         = $room->getBookedAndActiveContract();
                    $activeContractCount    = $activeContract->count();
                    if ($request->input('room_available') > ($room->room_count - $activeContractCount)) {
                        $validator->errors()->add('room_available', 'Total kamar terisi tidak boleh kurang dari total kamar dengan kontrak.');
                    }
                }
            }

            if ($flashSale[Price::STRING_DAILY] && $roomPriceUpdate['price_daily'] != $originalPrice['price_daily']) {
                $validator->errors()->add('price_daily', 'Promo ngebut aktif, tidak bisa update');
            } elseif ($flashSale[Price::STRING_WEEKLY] && $roomPriceUpdate['price_weekly'] != $originalPrice['price_weekly']) {
                $validator->errors()->add('price_weekly', 'Promo ngebut aktif, tidak bisa update');
            } elseif ($flashSale[Price::STRING_MONTHLY] && $roomPriceUpdate['price_monthly'] != $originalPrice['price_monthly']) {
                $validator->errors()->add('price_monthly', 'Promo ngebut aktif, tidak bisa update');
            } elseif ($flashSale[Price::STRING_ANNUALLY] && $roomPriceUpdate['price_yearly'] != $originalPrice['price_yearly']) {
                $validator->errors()->add('price_yearly', 'Promo ngebut aktif, tidak bisa update');
            } elseif ($flashSale[Price::STRING_QUARTERLY] && $roomPriceUpdate['price_3_month'] != $originalPrice['price_3_month']) {
                $validator->errors()->add('3_month', 'Promo ngebut aktif, tidak bisa update');
            } elseif ($flashSale[Price::STRING_SEMIANNUALLY] && $roomPriceUpdate['price_6_month'] != $originalPrice['price_6_month']) {
                $validator->errors()->add('6_month', 'Promo ngebut aktif, tidak bisa update');
            }
        });

        if ($validator->fails()) {
            $response = [
                'status'=>false,
                'meta' => [
                    'message' => $validator->errors()->first()
                ],
                'messages'=>$validator->errors()->all()
            ];
            return Api::responseData($response);
        }

        if ($room->is_booking == 1 and isset($monthly_price)) {
            BookingDesigner::updatePrice($monthly_price, $room->id);
        }

        $room = $room->setRoomPrice($roomPriceUpdate);
        RoomPriceType::updateData($room, 'idr');

        //tracking update
        TrackingFeature::insertTo(['type' => 'room-update', 'identifier' => $room->id]);

        Call::unrepliedCallStatus($room->id);
        $price = new Price($room);
        
        $roomAvailableOld = isset($room) ? $room->room_available : null;
        $room->setAvailableCount($request->get('room_available'));

        $responseSurvey = false;
        if ($roomAvailableOld > 0 && $room->room_available == 0 && !is_null($this->user())) {
            $dataUpdate = [
                'user_id'       => $this->user()->id,
                'identifier'    => $room->id,
                'for'           => 'update_room',
                'content'       => null
            ];

            $responseSurvey = SurveySatisfaction::insertOrUpdate($dataUpdate);
            $responseSurvey = $responseSurvey['show_survey'];
        }

        MarkActivity::dispatchNow($user->id, ActivityType::TYPE_UPDATE_PROPERTY);

        // Check if there's any active discount
        $hasDiscount = count($room->getAllActiveDiscounts()) > 0 ? true : false;
        if ($hasDiscount) {
            $room->recalculateAllActiveDiscounts();
        }

        $room   = $room->load('hidden_by_thanos');
        $thanos = $room->hidden_by_thanos;

        if (!$room->isActive() && !is_null($thanos)) {
            try {
                $this->service->revert($room->id, 'api', \Illuminate\Support\Facades\Request::ip());
            } catch (\Exception $e) {
                Bugsnag::notifyException($e, function ($report) use ($room, $thanos) {
                    $report->setSeverity('error');
                    $report->setMetaData([
                        'designer_id'   => $room->id,
                        'thanos_id'     => $thanos->id
                    ]);
                });
            }
        }

        $data = [
            '_id'                   => $room->song_id,
            'status_kamar'          => $room->status,
            'room_available'        => $room->room_available,
            'price_daily'           => $roomPriceUpdate['price_daily'],
            'price_weekly_time'     => $roomPriceUpdate['price_weekly'],
            'price_monthly_time'    => $roomPriceUpdate['price_monthly'],
            'price_yearly_time'     => $roomPriceUpdate['price_yearly'],
            'price_title_time'      => $price->monthly_time(),
            'show_survey'           => $responseSurvey,
            'price_3_month'         => $roomPriceUpdate['price_3_month'],
            'price_6_month'         => $roomPriceUpdate['price_6_month'],
            'activity'              => [
                "logged_in_at" => Carbon::now()->toDateTimeString(),
                "updated_property_at" => Carbon::now()->toDateTimeString(),
            ]
        ];

        return Api::responseData($data);
    }

    /**
     * Update Kamar Harga Apartment
     *
     * @param \Illuminate\Http\Request $request
     * @param string|integer $_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateApartment(Request $request, $_id)
    {
        $user = $this->user();
        if (is_null($user)) {
            return Api::responseError(false, false, 401, 401);
        }

        $room = Room::where('song_id', $_id)->first();
        if (is_null($room)) {
            return Api::responseError(false, false, 404, 404);
        }

        if (!$room->ownedBy($user)) {
            return Api::responseData(
                ['message'=>'Access Denied'],
                false,
                false,
                200,
                'Access Denied'
            );
        }

        $validator = Validator::make(
            $request->all(),
            [
                'room_available'  => 'required|numeric',
                'price_monthly'   => ['numeric', new PriceProperty('Harga Bulanan harus lebih besar 10000')],
                'price_yearly'    => ['numeric', new PriceProperty('Harga Tahunan harus lebih besar 10000')],
                'price_weekly'    => ['numeric', new PriceProperty('Harga Mingguan harus lebih besar 10000')],
                'price_daily'     => ['numeric', new PriceProperty('Harga Harian harus lebih besar 10000')],
            ],
            $this->validationMessages
        );

        $roomPriceUpdate = [
            'price_daily'   => $request->input('price_daily', 0),
            'price_weekly'  => $request->input('price_weekly', 0),
            'price_monthly' => $request->input('price_monthly', 0),
            'price_yearly'  => $request->input('price_yearly', 0),
            'price_3_month'  => $request->input('3_month', 0),
            'price_6_month'  => $request->input('6_month', 0),
        ];

        $validator->after(function ($validator) use ($request, $room, $roomPriceUpdate) {
            if (array_sum($roomPriceUpdate) == 0) {
                $validator->errors()->add('price', 'Harga sewa harus diisi salah satu');
            }

            if ($request->filled('room_available')) {
                if ($request->input('room_available') > $room->room_count) {
                    $validator->errors()->add('room_available', 'Jumlah kamar tersedia tidak boleh melebihi jumlah kamar total');
                } else {
                    $activeContract         = $room->getBookedAndActiveContract();
                    $activeContractCount    = $activeContract->count();
                    if ($request->input('room_available') > ($room->room_count - $activeContractCount)) {
                        $validator->errors()->add('room_available', 'Total kamar terisi tidak boleh kurang dari total kamar dengan kontrak.');
                    }
                }
            }
        });

        if ($validator->fails()) {
            $response = [
                'status'=>false,
                'messages'=>$validator->errors()->all()
            ];
            return Api::responseData($response);
        }

        if ($request->filled('price_type') && $request->input('price_type') == 'usd') {
            $usdPrice = Cache::get('currencyusd');
            if (is_null($usdPrice)) {
                $usdCurrency = Currency::where('name', 'usd')->first();
                if (!is_null($usdPrice)) {
                    $usdPrice = $usdCurrency->exchange_rate;
                    Cache::put('currencyusd', $usdPrice, 60 * 24);
                } else {
                    return Api::responseData([
                        "status" => false,
                        "meta" => [
                            "code" => 404,
                            "message" => "Currency usd not found"
                        ]
                    ]);
                }
            }

            $roomPriceUpdate['price_daily']         = $roomPriceUpdate['price_daily'] * $usdPrice;
            $roomPriceUpdate['price_weekly']        = $roomPriceUpdate['price_weekly'] * $usdPrice;
            $roomPriceUpdate['price_monthly']       = $roomPriceUpdate['price_monthly'] * $usdPrice;
            $roomPriceUpdate['price_yearly']        = $roomPriceUpdate['price_yearly'] * $usdPrice;
            $roomPriceUpdate['price_daily_usd']     = $roomPriceUpdate['price_daily'];
            $roomPriceUpdate['price_weekly_usd']    = $roomPriceUpdate['price_weekly'];
            $roomPriceUpdate['price_monthly_usd']   = $roomPriceUpdate['price_monthly'];
            $roomPriceUpdate['price_daily_yearly']  = $roomPriceUpdate['price_yearly'];
            $price_type = 'usd';
        }

        if ($room->is_booking == 1 and isset($monthly_price)) {
            BookingDesigner::updatePrice($monthly_price, $room->id);
        }

        $room = $room->setRoomPrice($roomPriceUpdate);
        $price_type = isset($price_type) ? $price_type : 'idr';
        RoomPriceType::updateData($room, $price_type);

        //tracking update
        TrackingFeature::insertTo(['type' => 'room-update', 'identifier' => $room->id]);

        Call::unrepliedCallStatus($room->id);
        $price = new Price($room);
        
        $roomAvailableOld = isset($room) ? $room->room_available : null;
        $room->setAvailableCount($request->get('room_available'));

        MarkActivity::dispatchNow($user->id, ActivityType::TYPE_UPDATE_PROPERTY);

        $room   = $room->load('hidden_by_thanos');
        $thanos = $room->hidden_by_thanos;
        if (!$room->isActive() && !is_null($thanos)) {
            try {
                $this->service->revert($room->id, 'api', \Illuminate\Support\Facades\Request::ip());
            } catch (\Exception $e) {
                Bugsnag::notifyException($e, function ($report) use ($room, $thanos) {
                    $report->setSeverity('error');
                    $report->setMetaData([
                        'designer_id'   => $room->id,
                        'thanos_id'     => $thanos->id
                    ]);
                });
            }
        }

        $data = [
            '_id'                   => $room->song_id,
            'status_kamar'          => $room->status,
            'room_available'        => $room->room_available,
            'price_daily'           => $room->price_daily,
            'price_weekly_time'     => $room->price_weekly,
            'price_monthly_time'    => $room->price_monthly,
            'price_yearly_time'     => $room->price_yearly,
            'price_title_time'      => $price->monthly_time(),
            'show_survey'           => false,
            'price_3_month'         => 0,
            'price_6_month'         => 0,
            'activity'              => [
                "logged_in_at" => Carbon::now()->toDateTimeString(),
                "updated_property_at" => Carbon::now()->toDateTimeString(),
            ]
        ];

        return Api::responseData($data);
    }

    public function editRoomOwner($songId)
    {
        $room = Room::where('song_id', $songId)->first();

        if (is_null($room)) {
            return Api::responseData([
                'status'=>false,
                'meta'=>[
                    'message'=>'Kost tidak ditemukan',
                    'messages'=>['Kost tidak ditemukan']
                ]
            ]);
        }

        $incomplete = count($room->incomplete_check) < 1 ? null : array_values($room->incomplete_check);
        $room = $this->repository->editJson($room);
        $data = array_merge(['room' => $room], ['incomplete' => $incomplete]);

        return Api::responseData($data);
    }


    /**
    * This function will update room price and availability count
    * This function is actually a copy from :
    * app\Http\Controllers\Web\RoomControler@postUpdateOwner.
    * Since this will be called via API, then we need to re-create the function here
    *
    * @param Request        $request
    */
    public function updateRoomPriceByOwner(Request $request)
    {
        //move owner checking to method updateRoomAvailable()

        return $this->updateRoomAvailable(
            $request,
            $request->get('room_id')
        );
    }

    /**
    * This function will update the room by owner.
    * This function will be called by the web client.
    * So we need to make sure that the user is the real owner of the room.
    *
    * @param Request        $request
    * @param int            $songId     room identifier
    *
    */
    public function updateRoomByOwner(Request $request, $songId)
    {
        $room = Room::where('song_id', $songId)->firstOrFail();

        if(!$room->ownedBy($this->user())) {
            // return error if the authenticated user is not the real owner of this room
            return Api::responseData(
                ['message'=>'Access Denied'],
                false,
                false,
                200,
                'Access Denied');
        }

        return $this->updateRoomOwner($request, $songId);
    }

    public function updateRoomOwner(Request $request, $_id)
    {
        if ($this->user() != null) {

            if ($this->user()->is_owner == 'false') {
                $data = array(
                        'status' => false,
                        'data'   => array()
                    );
                return Api::responseData($data);
            }

        }

        $roomData = $request->all();
        $room = Room::where('song_id', $_id)->with('tags')->firstOrFail();

        $updateRoom = $this->repository->updateJson($room, $roomData);

        /**
         * If it was draft then it updated with save, change status
         * if it was save draft again then keep it that way
         */
        if (strpos(url()->current(), 'draft/update') == false) {

            $roomOwner = RoomOwner::where('designer_id', $room->id)
                ->where('user_id', $this->user()->id)->firstOrFail();
            $roomOwner->status = 'draft2';
            $roomOwner->save();
            $room->is_active = 'false';
            $room->save();
        } else if (strpos(url()->current(), 'draft/update') == true) {
            $roomOwnerUpdateDraft = RoomOwner::where('designer_id', $room->id)->where('user_id', $this->user()->id)->first();
            if (count($roomOwnerUpdateDraft) > 0) {
                if ($room->is_active == 'false' && $roomOwnerUpdateDraft->status != 'draft2') {
                    $roomOwnerUpdateDraft->status = 'draft1';
                } else {
                    $roomOwnerUpdateDraft->status = 'draft2';
                    $room->is_active = 'false';
                    $room->save();
                }
                $roomOwnerUpdateDraft->save();
            }

            //trancking update
            TrackingFeature::insertTo(['type' => 'room-update', 'identifier' => $room->id]);
        }

        $roomOwner = RoomOwner::where('user_id', $this->user()->id);
        $statusDesigner = $roomOwner->pluck('status','designer_id');

        $updatedRoom = $room->refresh()->loadMissing('photo');
        $price = new Price($updatedRoom);
        $statusKost = $updatedRoom->status;
        if ($statusKost == 'draft2') {
            $statusKost = 'edited';
        }

        $data = [
                '_id'                 => $updatedRoom->song_id,
                'price_title_time'    => $price->monthly_time(),
                'room_title'          => $updatedRoom->name,
                'address'             => $updatedRoom->address,
                'has_round_photo'     => $updatedRoom->has_photo_round,
                'gender'              => $updatedRoom->gender,
                'status'              => $statusKost,
                'room_available'      => $updatedRoom->room_available,
                'min_month'           => $updatedRoom->min_month,
                'photo_url'           => $updatedRoom->photo_url,
                'status_kost'         => $statusDesigner[$updatedRoom->id],
                'kamar_available'     => $updatedRoom->available_room,
                'view_count'          => $updatedRoom->view_count,
                'love_count'          => $updatedRoom->love_count,
                'message_count'       => $updatedRoom->message_count,
                'survey_count'        => Call::GetSurveyCount($updatedRoom->id),
                'available_survey_count'  => Call::GetAvailabilitySurveyCount($updatedRoom->id),
                'price_daily'         => $updatedRoom->price_daily,
                'price_weekly_time'   => $updatedRoom->price_weekly,
                'price_monthly_time'  => $updatedRoom->price_monthly,
                'price_yearly_time'   => $updatedRoom->price_yearly,
        ];
        return Api::responseData(['data' => $data]);
    }

    public function getSuggestedOwnerRoom(Request $request)
    {
        $user = $this->user();

        if(!$user) {
            return Api::responseData([
                'status'=>false,
                'meta'=> [
                    'message'=>'Silakan login terlebih dahulu'
                ]
            ]);
        }

        $filters['ids'] = RoomOwner::suggestedRoomOwner($user->id);

        if (empty($filters['ids'])) {
            return Api::responseData([
                'rooms' => []
            ]);
        }
        
        $this->repository->setPresenter(new \App\Presenters\RoomPresenter('list'));

        $this->repository->pushCriteria(new \App\Criteria\Room\MainFilterCriteria($filters));

        $response = $this->repository->getItemList(new RoomFilter($filters));

        return Api::responseData($response);
    }

    public function postSearchOwnerRoom(Request $request)
    {
        $this->repository->setPresenter(new \App\Presenters\RoomPresenter('list'));

        $filters = $request->input('filters');
        // $filters['ids'] = RoomOwner::suggestedRoomOwner($this->user()->id);

        $this->repository->pushCriteria(new \App\Criteria\Room\MainFilterCriteria($filters));

        $response = $this->repository->getItemList(new RoomFilter($filters));

        return Api::responseData($response);
    }

    public function getRoomReportOwner(Request $request, $songId)
    {
        $type = $request->get('type');

        $room = Room::with('contact')->where('song_id', $songId)->first();

        if ($room == NULL) {

           $response = [
                'id'                    => $songId,
                'type'                  => $type,
                'love_count'            => 0,
                'view_count'            => 0,
                'view_ads_count'        => 0,
                'survey_count'          => 0,
                'availability_count'    => 0,
                'telp_count'            => 0,
                'message_count'         => 0,
                'review_count'          => 0,
                'used_balance'          => 0,
           ];

        } else {

            $response = $this->repository->getRoomReport($room, $type);

        }

        return Api::responseData($response);
    }

    /**
     * Request booking registration for a room
     */
    public function requestBooking(Request $request, $roomId)
    {
        $user = $this->user();

        $room = Room::with(['owners', 'owners.user'])
                ->where('song_id', $roomId)
                ->first();

        if(!$room) {
            return Api::responseData([
                'status'=>false,
                'meta'=> [
                    'message' => 'Kamar tidak ditemukan'
                ]
            ]);
        }

        $owner = $room->owners()->first();

        if($owner->user_id != $user->id) {
            return Api::responseData([
                'status'=>false,
                'meta'=>[
                    'message'=>'Anda bukan pemilik dari kost ini'
                ]
            ]);
        }

        if($room->is_booking == 1) {
            return Api::responseData([
                'status'=>false,
                'meta'=>[
                    'message'=>'Kamar sudah terdaftar untuk booking'
                ]
            ]);
        }

        try {
            Mail::send('emails.request-booking', compact('room', 'owner'), function($message)
            {
                $message->to('booking@mamikos.com')->subject('Permintaan Partnership Booking');
            });
        } catch (\Exception $e) {
            return Api::responseData([
                'status'=>false,
                'meta'=>[
                    'message'=>'Proses gagal, mohon ulangi beberapa saat lagi.'
                ]
            ]);
        }

        return Api::responseData([
            'success' => true,
            'message'=> 'Permintaan Anda sedang diproses. Silakan tunggu tim MamiKos untuk menghubungi Anda.'
        ]);
    }

    /**
     * Get kost suggestion by name
     *
     * @param Request $request              Laravel Request
     *
     * @return Array                        Api Response
     */
    public function getSuggestionByName(Request $request)
    {
        $this->repository->setPresenter(new \App\Presenters\RoomPresenter('list'));

        $filters['property_type'] = 'kost';
        $filters['criteria'] = $request->get('name');

        if($request->filled('include_apartment')) {
            $filters['include_apartment'] = $request->get('input_apartment');
        }

        $request->merge([
            'limit'=>3,
            'offset'=>0
        ]);

        $this->repository->pushCriteria(new \App\Criteria\Room\MainFilterCriteria($filters));

        $response = $this->repository->getItemList(new RoomFilter($filters));

        return Api::responseData($response);
    }

    public function autoReply(Request $request)
    {
        $channel = (!empty($request->get('group_channel_url')) ? $request->get('group_channel_url') : $request->get('group_id'));
        if (is_null($channel)) {
            return Api::responseData([
                'status' => false,
                'meta' => ['message' => 'Channel URL harus diisi']
            ]);
        }
        $ownerChat = new \App\Entities\Activity\OwnerChat(['group_id' => $channel]);
        $chatResponse = $ownerChat->reply();

        return Api::responseData([
            'status' => !is_null($chatResponse),
            'response' => $chatResponse
        ]);
    }


    /**
     * Get Rooms Owner has Booking Request
     *
     * @param Request $request
     * @return mixed
     * @throws Exception
     */
    public function getRoomHasBookingRequest(Request $request)
    {
        $user = $this->user();
        if (is_null($user) || !$user->isOwner()) {
            return Api::responseData(
                ['message'=>'Access Denied'],
                false,
                false,
                200,
                'Access Denied');
        }

        $roomIds = DbetLinkRegistered::where('status', DbetLinkRegistered::STATUS_PENDING)->groupBy('designer_id')->pluck('designer_id');
        if ($roomIds) {
            $query = Room::with('owners', 'booking_users', 'dbet_link')
                ->whereIn('id', $roomIds)
                ->whereHas('owners', function($ownerQuery) use($user){
                    $ownerQuery->where('user_id', $user->id)
                        ->whereIn('status', ['add', 'verified', 'draft2']);
                });

            $dbetCode = $request->input('dbet_code');
            if ($dbetCode) {
                $query->whereHas('dbet_link', function($dbetLink) use($dbetCode){
                    $dbetLink->where('code', $dbetCode);
                });
            }

            $limit = (int)$request->input('limit') ?? 10;
            $rooms = $query->paginate($limit);

            $transformedRoom = (new RoomPresenter('has-booking-request'))->present($rooms);

            $result = [
                'data'       => $transformedRoom['data'],
                'pagination' => [
                    'limit'         => $rooms->perPage(),
                    'current_page'  => $rooms->currentPage(),
                    'total_items'   => $rooms->total(),
                    'has_more'      => $rooms->hasMorePages()
                ]
            ];

        } else {
            $result = ([
                'status' => false,
                'meta' =>[
                    'message' => 'Data tidak ditemukan'
                ],
            ]);
        }

        return Api::responseData($result);
    }
}
