<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Helpers\ApiResponse as Api;
use App\Http\Helpers\ApiHelper;
use App\Http\Requests;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Validator;
use App\User;
use App\Entities\Mamipay\MamipayBankAccount;
use App\Entities\Mamipay\MamipayTenantBank;

class UserAccountBankController extends BaseController
{
    protected $tenantBankValidatorMessage = [
        "bank.required"         => "Bank tidak boleh kosong",
        "name.required"         => "Nama Pemilik tidak boleh kosong",
        "bank_number.required"  => "Nomor Rekening boleh kosong",
        "bank_code.required"    => "Kode Bank tidak boleh kosong",
    ];

    public function getBankList()
    {
        $bankAccounts = MamipayBankAccount::select('id', 'bank_code', 'bank_name')->get();
        
        return Api::responseData([
            'status' => true,
            'data'   => $bankAccounts
        ]);
    }

    public function getUserBank()
    {
        $user = $this->user();

        $userBank = MamipayTenantBank::where('tenant_id', $user->id)->orderBy('primary', 'desc')->get();

        if ($userBank) {
            return Api::responseData([
                'status' => true,
                'data'   => $userBank
            ]);

        } else {
            return Api::responseData([
                'status' => true,
                'meta'   => ['message' => 'Sorry, Your Bank Account not found']
            ]);
        }
        
    }

    public function storeAccountBank(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'bank'        => 'required',
            'name'        => 'required',
            'bank_number' => 'required'
        ], $this->tenantBankValidatorMessage);

        if($validator->fails()) {
            $response = [
                "status" => false,
                "meta"   => [ 'message' => $validator->errors() ],
            ];

            return Api::responseData($response);
        }

        $user = $this->user();

        $credentials = [
            'tenant_id'   => $user->id,
            'bank'        => $request->bank,
            'name'        => $request->name,
            'bank_number' => $request->bank_number,
            'bank_code'   => $request->bank_code,
        ];

        $tenantAccountBank = MamipayTenantBank::create($credentials);

        if ($tenantAccountBank) {
            return Api::responseData([
                "status" => true, 
                "meta"   => [
                    "message" => "Success Created"
                ]
            ]);

        } else {
            return Api::responseData([
                "status" => false,
                "meta"   => [ 'message' => "Failed created" ],
            ]);
        }
    }

    public function setStatusAccountBank($userBankId, $status)
    {
        if ($status == "primary") {
            $accountBanks = MamipayTenantBank::where('primary', "true")->first();
            
            if ($accountBanks) {
                $accountBanks->primary = NULL;
                $accountBanks->update();
            }

            $setNewPrimaryAccount = MamipayTenantBank::find($userBankId);
            $setNewPrimaryAccount->primary = "true";
            $setNewPrimaryAccount->update();

            return Api::responseData([
                "status" => true, 
                "meta"   => [
                    "message" => "Success set primary account bank"
                ]
            ]);

        } elseif ($status == "remove") {
            $accountBanks = MamipayTenantBank::find($userBankId);
            $accountBanks->primary = NULL;
            $accountBanks->update();

            return Api::responseData([
                "status" => true, 
                "meta"   => [
                    "message" => "Success remove primary account bank"
                ]
            ]);

        } else {

            return Api::responseData([
                "status" => true, 
                "meta"   => [
                    "message" => "Sorry please check again"
                ]
            ]);

        }
    }

    public function removeAccountBank($accountBankId)
    {
        $accountBanks = MamipayTenantBank::find($accountBankId);
        $delete = $accountBanks->delete();

        if ($delete) {
            return Api::responseData([
                "status" => true, 
                "meta"   => [
                    "message" => "Success remove account bank"
                ]
            ]);
        } else {
            return Api::responseData([
                "status" => false, 
                "meta"   => [
                    "message" => "Failed remove account bank"
                ]
            ]);
        }
    }
    
}
