<?php

namespace App\Http\Controllers\Api\Room;

use App\Entities\Room\Element\Type;
use App\Entities\Room\Price;
use App\Entities\Room\Room;
use App\Entities\Room\RoomInputProgress;
use App\Entities\Room\RoomProgress;
use App\Http\Controllers\Api\BaseController;
use App\Http\Helpers\ApiResponse as Api;
use App\Http\Helpers\MamipayApiHelper;
use App\Repositories\Room\InputRepository;
use DB;
use Illuminate\Http\Request;
use Validator;

class InputController extends BaseController
{
	const FAILED_SAVE = "Gagal menyimpan data";
	const SUCCESS_SAVE = "Berhasil menyimpan data";
	const INVALID_STEP = "Invalid step number!";

	protected $repository;
	protected $validatorMessage = [
		"step.required"                 => "Gagal menyimpan data",
		"name.required"                 => "Nama kost tidak boleh kosong",
		"gender.required"               => "Jenis kost tidak boleh kosong",
		"gender.in"                     => "Jenis kost tidak ditemukan",
		"categories.required"           => "Tags tidak boleh kosong",
		"kos_photos.required"           => "Foto Kos tidak boleh kosong",
		"name_room_type.required"       => "Nama Tipe Kos tidak boleh kosong",
		'maximum_occupancy.required'    => "Jumlah maksimum penghuni tidak boleh kosong",
		'room_size.required'            => "Ukuran kamar tidak boleh kosong",
		'room_type_id.required'         => "Gagal mendapatkan data tipe kamar",
		'total_room.required'           => "Total Kamar tidak boleh kosong",
		'available_room_units.required' => 'Data kamar yang tersedia tidak boleh kosong',
		"bank_name.required"            => "Nama bank tidak boleh kosong",
		"bank_account_name.required"    => "Nama pemilik bank tidak boleh kosong",
		"bank_account_number.required"  => "Nomor rekening tidak boleh kosong",
		"price_monthly.required"        => "Harga bulanan tidak boleh kosong",
	];

	public function __construct(InputRepository $repository)
	{
		$this->repository = $repository;

		parent::__construct();
	}

	public function register(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'step'    => 'required|numeric',
			'room_id' => 'nullable',
		], $this->validatorMessage);

		$registerProgress = null;
		if ($request->filled('room_id'))
		{
			$registerProgress = RoomInputProgress::where('designer_id', $request->input('room_id'))->first();
		}

		$validator->after(function ($validator) use ($request, $registerProgress)
		{
			if (!$request->filled('room_id') && $request->input('step') > 1)
			{
				$validator->errors()->add('message', self::FAILED_SAVE);
			}

			if ($request->input('step') > 12)
			{
				$validator->errors()->add('message', self::INVALID_STEP);
			}
		});

		if ($validator->fails())
		{
			return Api::responseData([
				"status" => false,
				"meta"   => [
					"message" => $validator->errors()->all(),
				],
			]);
		}

		DB::beginTransaction();

		$step     = $request->input('step');
		$response = null;

		if ($step == 1)
		{
			$response = $this->addLocation($request);
		}
		else if ($step == 2)
		{
			$response = $this->addInformation($request);
		}
		else if ($step == 3)
		{
			$response = $this->addRule($request);
		}
		else if ($step == 4)
		{
			$response = $this->updateFacilitySetting($request);
		}
		else if ($step == 5)
		{
			$response = $this->addRoomPhotos($request);
		}
		else if ($step == 6)
		{
			$response = $this->addRoomType($request);
		}
		else if ($step == 7)
		{
			$response = $this->addRoomUnits($request);
		}
		else if ($step == 8)
		{
			$response = $this->updateFacilityTypeSetting($request);
		}
		else if ($step == 9)
		{
			$response = $this->addRoomTypePhotos($request);
		}
		else if ($step == 10)
		{
			$response = $this->addPrice($request);
		}
		else if ($step == 11)
		{
			$response = $this->addRoomTypeFacilityPhotos($request);
		}
		else if ($step == 12)
		{
			$response = $this->addBankAccount($request);
		}

        DB::commit();
        
        return Api::responseData($response);
    }

	public function addLocation(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'address'     => 'required',
			'latitude'    => 'required',
			'longitude'   => 'required',
			'city'        => 'nullable',
			'fac_near'    => 'nullable',
			'subdistrict' => 'nullable',
		], $this->validatorMessage);

		if ($validator->fails())
		{
			return [
				"status" => false,
				"meta"   => [
					"message" => [self::FAILED_SAVE],
				],
			];
		}

		$step = $request->input('step');
		if ($request->filled('room_id'))
		{
			$request->merge([
				'user' => $this->user(),
			]);
			$room = $this->repository->updateLocation($request->all());
		}
		else
		{
			$room = $this->repository->saveLocation($request->all());
			$this->repository->ownerClaim($room, $this->user());
		}
		$this->repository->saveOrUpdateInputProcess($room, $step);

		$roomResponse = [
			"id"          => $room->song_id,
			"latitude"    => $room->latitude,
			"longitude"   => $room->longitude,
			"city"        => $room->city,
			"subdistrict" => $room->subdistrict,
			"address"     => $room->address,
			"fac_near"    => $room->fac_near_other,
		];

		return [
			"status"    => true,
			"meta"      => [
				"message" => [self::SUCCESS_SAVE],
			],
			"step"      => $step,
			"next_step" => $step + 1,
			"room"      => $roomResponse,
		];
	}

	public function addInformation($request)
	{
		$validator = Validator::make($request->all(), [
			'name'          => 'required',
			'building_year' => 'nullable',
			'gender'        => 'required|in:0,1,2',
			'description'   => 'nullable',
			'room_id'       => 'required',
		], $this->validatorMessage);

		$room = Room::where('song_id', $request->input('room_id'))->first();
		$validator->after(function ($validator) use ($room)
		{
			if (is_null($room))
			{
				$validator->errors()->add('room', self::FAILED_SAVE);
			}
		});

		if ($validator->fails())
		{
			return [
				"status" => false,
				"meta"   => [
					"message" => $validator->errors()->all(),
				],
			];
		}

		$step = $request->input('step');
		$this->repository->saveInformation($room, $request->all());
		$this->repository->saveOrUpdateInputProcess($room, $step);

		$roomResponse = [
			"room_id"       => $room->song_id,
			"name"          => $room->name,
			"gender"        => $room->gender,
			"building_year" => $room->building_year,
			"description"   => $room->description,
		];

		return [
			"status"    => true,
			"meta"      => [
				"message" => [self::SUCCESS_SAVE],
			],
			"step"      => $step,
			"next_step" => $step + 1,
			"room"      => $roomResponse,
		];
	}

	public function addRule($request)
	{
		$validator = Validator::make($request->all(), [
			'room_id' => 'required',
			'rule'    => 'nullable',
		], $this->validatorMessage);

		$room = Room::where('song_id', $request->input('room_id'))->first();
		$validator->after(function ($validator) use ($room)
		{
			if (is_null($room))
			{
				$validator->errors()->add('room', self::FAILED_SAVE);
			}
		});

		if ($validator->fails())
		{
			return [
				"status" => false,
				"meta"   => [
					"message" => $validator->errors()->all(),
				],
			];
		}

		$step = $request->input('step');
		$this->repository->saveRule($room, $request->all());
		$this->repository->saveOrUpdateInputProcess($room, $step);

		$roomResponse = [
			"room_id"  => $room->song_id,
			"rule"     => $request->input('rule'),
			"document" => $request->input('document'),
		];

		$response = [
			"status"    => true,
			"meta"      => [
				"message" => [self::SUCCESS_SAVE],
			],
			"step"      => $step,
			"next_step" => $step + 1,
			"room"      => $roomResponse,
		];

		DB::commit();

		return $response;
	}

	public function updateFacilitySetting(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'room_id'    => 'required',
			'categories' => 'required',
		], $this->validatorMessage);

		$room = Room::where('song_id', $request->input('room_id'))->first();
		$validator->after(function ($validator) use ($room)
		{
			if (is_null($room))
			{
				$validator->errors()->add('room', self::FAILED_SAVE);
			}
		});

		if ($validator->fails())
		{
			return [
				"status" => false,
				"meta"   => [
					"message" => $validator->errors()->all(),
				],
			];
		}

		$step = $request->input('step');
		$this->repository->saveFacilitySetting($room, $request->all());
		$this->repository->saveOrUpdateInputProcess($room, $step);

		$roomResponse = [
			"room_id" => $room->song_id,
		];

		return [
			"status"    => true,
			"meta"      => [
				"message" => [self::SUCCESS_SAVE],
			],
			"step"      => $step,
			"next_step" => $step + 1,
			"room"      => $roomResponse,
		];
	}

	public function addRoomPhotos(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'kos_photos' => 'required',
		], $this->validatorMessage);

		$room = Room::where('song_id', (int) $request->room_id)->first();

		$validator->after(function ($validator) use ($room)
		{
			if (is_null($room))
			{
				$validator->errors()->add('room', self::FAILED_SAVE);
			}
		});

		if ($validator->fails())
		{
			return [
				"status" => false,
				"meta"   => [
					"message" => $validator->errors()->all(),
				],
			];
		}

		$step = $request->input('step');
		$this->repository->addRoomPhotos($room, $request->all());
		$this->repository->saveOrUpdateInputProcess($room, $step);

		$roomResponse = [
			"room_id"    => $room->song_id,
			"kos_photos" => $request->input('kos_photos'),
		];

		return [
			"status"    => true,
			"meta"      => [
				"message" => [self::SUCCESS_SAVE],
			],
			"step"      => $step,
			"next_step" => $step + 1,
			"room"      => $roomResponse,
		];
	}

	public function addRoomType(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'name_room_type'    => 'required',
			'maximum_occupancy' => 'required',
			'room_size'         => 'required',
		], $this->validatorMessage);

		$room = Room::where('song_id', (int) $request->room_id)->first();
		$validator->after(function ($validator) use ($room)
		{
			if (is_null($room))
			{
				$validator->errors()->add('room', self::FAILED_SAVE);
			}
		});

		if ($validator->fails())
		{
			return [
				"status" => false,
				"meta"   => [
					"message" => $validator->errors()->all(),
				],
			];
		}

		$step     = $request->input('step');
		$roomType = $this->repository->saveRoomType($room, $request->all());
		$this->repository->saveOrUpdateInputProcess($room, $step);

		$roomResponse = [
			"room_type_id"      => $roomType->id,
			'name_room_type'    => $request->input('name_room_type'),
			'maximum_occupancy' => $request->input('maximum_occupancy'),
			'tenant_type'       => $request->input('tenant_type'),
			'room_size'         => $request->filled('room_size') ? $request->input('room_size') : null,
		];

		return [
			"status"    => true,
			"meta"      => [
				"message" => [self::SUCCESS_SAVE],
			],
			"step"      => $step,
			"next_step" => $step + 1,
			"room"      => $roomResponse,
		];
	}

	public function addRoomUnits(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'room_type_id'         => 'required',
			'total_room'           => 'required',
			'available_room_units' => 'required',
		], $this->validatorMessage);

		$room = Room::with([
			'types' => function ($q) use ($request)
			{
				$q->find((int) $request->room_type_id);
			}])
			->where('song_id', (int) $request->room_id)
			->first();

		$validator->after(function ($validator) use ($room)
		{
			if (is_null($room))
			{
				$validator->errors()->add('room', self::FAILED_SAVE);
			}
		});

		if ($validator->fails())
		{
			return [
				"status" => false,
				"meta"   => [
					"message" => $validator->errors()->all(),
				],
			];
		}

		$step           = $request->input('step');
		$storeRoomUnits = $this->repository->saveRoomUnits($room, $request->all());

		$this->repository->saveOrUpdateInputProcess($room, $step);

		$roomResponse = [
			"room_type_id"         => $request->input('room_type_id'),
			"total_room"           => $request->input('total_room'),
			"available_room_units" => $storeRoomUnits,
		];

		return [
			"status"    => true,
			"meta"      => [
				"message" => [self::SUCCESS_SAVE],
			],
			"step"      => $step,
			"next_step" => $step + 1,
			"room"      => $roomResponse,
		];
	}

	public function updateFacilityTypeSetting(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'room_type_id' => 'required',
			'categories'   => 'required',
		], $this->validatorMessage);

		$room = Type::where('id', (int) $request->room_type_id)->first();

		$validator->after(function ($validator) use ($room)
		{
			if (is_null($room))
			{
				$validator->errors()->add('room', self::FAILED_SAVE);
			}
		});

		if ($validator->fails())
		{
			return [
				"status" => false,
				"meta"   => [
					"message" => $validator->errors()->all(),
				],
			];
		}

		$step         = $request->input('step');
		$categoryData = $this->repository->saveFacilityTypeSetting($room, $request->all());
		$this->repository->saveOrUpdateInputProcess($room, $step);

		$roomResponse = [
			"room_type_id" => $room->id,
			"categories"   => $categoryData,
		];

		return [
			"status"    => true,
			"meta"      => [
				"message" => [self::SUCCESS_SAVE],
			],
			"step"      => $step,
			"next_step" => $step + 1,
			"room"      => $roomResponse,
		];
	}

	public function addRoomTypePhotos(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'kos_type_photos' => 'required',
		], $this->validatorMessage);

		$room = Type::where('id', (int) $request->room_type_id)->first();
		$validator->after(function ($validator) use ($room)
		{
			if (is_null($room))
			{
				$validator->errors()->add('room', self::FAILED_SAVE);
			}
		});

		if ($validator->fails())
		{
			return [
				"status" => false,
				"meta"   => [
					"message" => $validator->errors()->all(),
				],
			];
		}

		$step = $request->input('step');
		$this->repository->addRoomTypePhotos($room, $request->all());
		$this->repository->saveOrUpdateInputProcess($room, $step);

		$roomResponse = [
			"room_type_id" => $room->id,
			"kos_photos"   => $request->input('kos_type_photos'),
		];

		return [
			"status"    => true,
			"meta"      => [
				"message" => [self::SUCCESS_SAVE],
			],
			"step"      => $step,
			"next_step" => $step + 1,
			"room"      => $roomResponse,
		];
	}

	public function addPrice($request)
	{
		$validator = Validator::make($request->all(), [
			'room_id'       => 'required',
			'price_monthly' => 'required',
			'room_type_id' => 'required'
		], $this->validatorMessage);

		$room = Room::with([ 'types' => function ($q) use ($request) {
					$q->find((int) $request->room_type_id);
				}])
			->where('song_id', (int) $request->room_id)
			->first();

		$validator->after(function ($validator) use ($room)
		{
			if (is_null($room))
			{
				$validator->errors()->add('room', self::FAILED_SAVE);
			}
		});

		if ($validator->fails())
		{
			return [
				"status" => false,
				"meta"   => [
					"message" => $validator->errors()->all(),
				],
			];
		}

		$user = $this->user();
		$step = $request->input('step');

		$priceData                       = [];
		$priceData[Price::PRICE_MONTHLY] = $request->input('price_monthly');

		if ($request->filled('price_daily'))
			$priceData[Price::PRICE_DAILY] = $request->input('price_daily');
		if ($request->filled('price_weekly'))
			$priceData[Price::PRICE_WEEKLY] = $request->input('price_weekly');
		if ($request->filled('price_annually'))
			$priceData[Price::PRICE_ANNUALLY] = $request->input('price_annually');
		if ($request->filled('price_semiannually'))
			$priceData[Price::PRICE_SEMIANNUALLY] = $request->input('price_semiannually');
		if ($request->filled('price_quarterly'))
			$priceData[Price::PRICE_QUARTERLY] = $request->input('price_quarterly');

		$options = [
			"room_type_id" => $request->input('room_type_id')
		];

		$this->repository->savePrice($room, $priceData, $options);
		$this->repository->saveAdditionalCost($room, $request->all());
		$this->repository->saveOrUpdateInputProcess($room, $step);

		$roomResponse = array_merge($request->all(), ["room_id" => $room->song_id]);

		return [
			"status"    => true,
			"meta"      => [
				"message" => [self::SUCCESS_SAVE],
			],
			"step"      => $step,
			"next_step" => $step + 1,
			"room"      => $roomResponse,
		];
	}

	public function addRoomTypeFacilityPhotos(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'facilities' => 'required',
		], $this->validatorMessage);

		$room = Type::where('id', (int) $request->room_type_id)->first();
		$validator->after(function ($validator) use ($room)
		{
			if (is_null($room))
			{
				$validator->errors()->add('room', self::FAILED_SAVE);
			}
		});

		if ($validator->fails())
		{
			return [
				"status" => false,
				"meta"   => [
					"message" => $validator->errors()->all(),
				],
			];
		}

		// Add user id into request array
		$request->merge([
			'user_id' => $this->user()->id,
		]);

		$step = $request->input('step');
		$this->repository->addRoomTypeFacilityPhotos($room, $request->all());
		$this->repository->saveOrUpdateInputProcess($room, $step);

		$roomResponse = [
			"room_type_id" => $room->id,
			"facilities"   => $request->input('facilities'),
		];

		return [
			"status"    => true,
			"meta"      => [
				"message" => [self::SUCCESS_SAVE],
			],
			"step"      => $step,
			"next_step" => $step + 1,
			"room"      => $roomResponse,
		];
	}

	public function addBankAccount(Request $request)
	{
		$user      = $this->user();
		$validator = Validator::make($request->all(), [
			'bank_name'           => 'required',
			'bank_account_number' => 'required',
			'bank_account_name'   => 'required',
			'room_id'             => 'required',
		], $this->validatorMessage);

		$room = Room::where('song_id', (int) $request->room_id)->first();
		$request->merge(["bank_code" => $request->input('bank_name')]);

		$checkValidBankAccountNumber = MamipayApiHelper::makeRequest($request->method(),
			$request,
			"/bank/checker",
			$user->id
		);

		$validator->after(function ($validator) use ($room, $checkValidBankAccountNumber)
		{
			if (is_null($room))
			{
				$validator->errors()->add('room', self::FAILED_SAVE);
			}

			if ($checkValidBankAccountNumber->status == false || !isset($checkValidBankAccountNumber->bank_status) || $checkValidBankAccountNumber->bank_status != "SUCCESS")
			{
				$validator->errors()->add('bank_name', "No rekening yang anda masukkan salah");
			}
		});

		if ($validator->fails())
		{
			return [
				"status" => false,
				"meta"   => [
					"message" => $validator->errors()->all(),
				],
			];
		}

		$step = $request->input('step');
		$this->repository->saveBankAccount($user, $request->all());
		$this->repository->saveOrUpdateInputProcess($room, $step);

		$roomResponse = array_merge($request->all(), ["room_id" => $room->song_id]);

		return [
			"status"    => true,
			"meta"      => [
				"message" => [self::SUCCESS_SAVE],
			],
			"step"      => $step,
			"next_step" => $step + 1,
			"room"      => $roomResponse,
		];
	}

	public function roomStepRegister(Request $request, $id)
	{
		if (!$request->filled('step'))
		{
			return Api::responseData([
				"status" => false,
				"meta"   => [
					"message" => ["Gagal memuat data"],
				],
			]);
		}

		$response = (new RoomProgress($id, $this->user(), $request->all()))->response($request->get('step'));
		return Api::responseData($response);
	}

	public function deleteRoomType($songId, $typeId)
	{
		$room = Room::where('song_id', $songId)
					->whereHas('types', function($p) use($typeId) {
						$p->where('id', $typeId);
					})
					->first();

		if (!is_null($room)) {
			$room->types()->delete();
			$status = true;
			$message = "Berhasil menghapus tipe kos";
		} else {
			$status = false;
			$message = "Data tipe tidak ditemukan";
		}
		
		return Api::responseData([
			"status" => $status,
			"meta" => [
				"message" => $message
			]
		]);
	}
}