<?php
namespace App\Http\Controllers\Api\Room;

use App\Http\Controllers\Api\BaseController;
use Illuminate\Http\Request;
use App\Http\Helpers\ApiResponse as Api;
use Validator;
use App\Entities\Media\MediaHandler;
use App\Entities\Media\Media;
use App\Entities\Room\Element\RoomTermDocument;
use App\Entities\Document\DocumentHandler;
use App\Entities\Document\Document;

class DocumentController extends BaseController
{
    protected $validatorMessage = [
        'file.required' => 'Dokumen tidak boleh kosong'
    ];

    public function upload(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'file' => 'required|file'
        ], $this->validatorMessage);

        if ($validator->fails()) {
            return Api::responseData([
                "status" => false,
                "meta" => [
                    "message" => $validator->errors()->all()
                ]
            ]);
        }

        $user = $this->user();
        $file = $request->file('file');
        $mediaType = $request->input('mediaType');
        $fileExtention = MediaHandler::getFileExtension($file);
        $file_name = $file->getClientOriginalName();

        if (in_array($fileExtention, RoomTermDocument::IMAGE_EXT)) {
            $document = Media::postMedia(
                $file, 
                'upload', 
                \Auth::id(), 
                null, 
                null, 
                'style', // Media type
                false, //Need watermark 
                null,
                true //Keep original file
            );

            $type = "image";
            $documentId = $document['id'];

        } else if (in_array($fileExtention, RoomTermDocument::DOCUMENT_EXT)) {
            $uploadDocument = DocumentHandler::fileUpload([
                "extension" => $fileExtention,
                "file" => $file,
                "path" => DOCUMENT::FILE_PATH
            ]);
            
            $type = "document";
            $document = Document::store([
                            "user_id" => $user->id,
                            "type" => Document::TYPE[0],
                            "file_name" => $uploadDocument,
                            "format" => $fileExtention
                        ]);

            $documentId = $document->id;
        } else {
            return Api::responseData([
                "status" => false,
                "meta" => ["message" => "Gagal upload dokumen"]
            ]);
        }
        
        return Api::responseData([
            "status" => true,
            "meta" => [
                "message" => "Sukses upload dokumen"
            ],
            "file" => [
                "id" => 0,
                "file_id" => $documentId,
                "type" => $type,
                "file_name" => $file_name
            ]
        ]);
    }
}