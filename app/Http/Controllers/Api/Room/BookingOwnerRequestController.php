<?php

namespace App\Http\Controllers\Api\Room;

use App\Entities\Booking\BookingOwnerRequestEnum;
use App\Entities\Mamipay\MamipayOwner;
use App\Entities\Room\BookingOwnerRequest;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Http\Controllers\Api\BaseController;
use App\Http\Helpers\ApiResponse;
use App\Services\Room\BookingOwnerRequestService;
use Illuminate\Support\Facades\DB;

class BookingOwnerRequestController extends BaseController
{
    const ROOM_OWNER_STATUS = [
        RoomOwner::ROOM_ADD_STATUS,
        RoomOwner::ROOM_VERIFY_STATUS,
        RoomOwner::ROOM_EDIT_STATUS,
    ];
    /**
     * Register all kost to BBK
     *
     * @param \App\Services\Room\BookingOwnerRequestService $service
     * @return \Illuminate\Http\JsonResponse
     */
    public function registerBulkBbk(BookingOwnerRequestService $service)
    {
        $user = $this->user();

        if (is_null($user)) {
            return ApiResponse::responseError(false, false, 401, 401);
        }

        $mamipayOwner = $user->mamipay_owner;
        if (is_null($mamipayOwner) || $mamipayOwner->status != MamipayOwner::STATUS_APPROVED) {
            return ApiResponse::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Gagal mengirim data, silakan ulangi.'
                ]
            ]);
        }

        $userId = $user->id;

        $kosts = Room::isKost()
        ->with(['booking_owner_requests' => function ($query) use ($userId) {
            $query->where('user_id', $userId);
        }])
        ->whereHas('owners', function ($query) use ($userId) {
            $query->where('user_id', $userId)
                ->whereIn('status', self::ROOM_OWNER_STATUS);
        })
        ->get();

        foreach ($kosts as $kost) {
            $bbkRequest = $kost->booking_owner_requests->first();

            if (is_null($bbkRequest)) {
                $data = [
                    'designer_id'   => $kost->id,
                    'user_id'       => $userId,
                    'status'        => BookingOwnerRequest::BOOKING_WAITING,
                    'requested_by'  => 'owner',
                    'registered_by' => BookingOwnerRequestEnum::SYSTEM
                ];
                $service->requestNewBbk($data);
            } elseif (in_array(
                $bbkRequest->status,
                [BookingOwnerRequest::BOOKING_REJECT, BookingOwnerRequest::BOOKING_NOT_ACTIVE]
            )) {
                $service->requestBbk($bbkRequest);
            }
        }

        return ApiResponse::responseData([
            'status' => true,
            'meta' => [
                'message' => 'Fitur Booking Langsung berhasil diajukan'
            ]
        ]);
    }

    /**
     * Get count of each status BBK of all kost
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBbkStatus()
    {
        $user = $this->user();

        if (is_null($user) || !$user->is_owner) {
            return ApiResponse::responseError(false, false, 401, 401);
        }

        $userId = $user->id;

        $totalKost = Room::isKost()
            ->whereHas('owners', function ($query) use ($userId) {
                $query->where('user_id', $userId)
                    ->whereIn('status', self::ROOM_OWNER_STATUS);
            })
            ->count();

        $bookingOwnerRequest = BookingOwnerRequest::select('status', DB::raw('count(*) as total'))
            ->where('user_id', $userId)
            ->whereHas('room', function ($query) use ($userId) {
                $query->isKost()->whereHas('owners', function ($query) use ($userId) {
                    $query->where('user_id', $userId)
                        ->whereIn('status', self::ROOM_OWNER_STATUS);
                });
            })
            ->groupBy('status')
            ->pluck('total', 'status')
            ->all();

        $approve    = isset($bookingOwnerRequest['approve']) ? $bookingOwnerRequest['approve'] : 0;
        $waiting    = isset($bookingOwnerRequest['waiting']) ? $bookingOwnerRequest['waiting'] : 0;
        $reject     = isset($bookingOwnerRequest['reject']) ? $bookingOwnerRequest['reject'] : 0;
        $other      = $totalKost - ($approve + $waiting + $reject);

        return ApiResponse::responseData([
            'bbk_status' => [
                'approve'   => $approve,
                'waiting'   => $waiting,
                'reject'    => $reject,
                'other'     => $other,
            ]
        ]);
    }
}
