<?php

namespace App\Http\Controllers\Api\Room;

use App\Entities\Facility\FacilityCategory;
use App\Entities\Room\Element\Type;
use App\Entities\Room\Room;
use App\Entities\Room\RoomTypeFacility;
use App\Http\Controllers\Controller;
use App\Http\Helpers\ApiResponse as Api;
use Illuminate\Http\Request;

class RoomFacilityController extends Controller
{
	const GENERAL_CATEGORY_NAME = 'Fasilitas Kos';
	const TYPE_CATEGORY_NAME = 'Fasilitas Tipe Kamar';

	/**
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function getSetting(Request $request)
	{
		// Getting room data
		$room = Room::select(['id', 'name'])
			->with('facility_setting')
			->where('song_id', (int) $request->id)
			->first();

		if (is_null($room))
		{
			return Api::responseData([
				"status" => false,
				"meta"   => [
					"message" => 'Failed fetching room data',
				],
			]);
		}

		// Getting active tags
		$activeTags = [];
		if (!is_null($room->facility_setting))
		{
			if ($room->facility_setting->active_tags !== '')
			{
				$activeTags = explode(',', $room->facility_setting->active_tags);
			}
		}

		// Getting filtered facility structure
		$generalCategory = FacilityCategory::with('types.tags')
			->where('name', self::GENERAL_CATEGORY_NAME)
			->first();

		if (is_null($generalCategory))
		{
			return Api::responseData([
				"status" => false,
				"meta"   => [
					"message" => 'Failed fetching Kos facility attributes',
				],
			]);
		}

		$data = [];

		foreach ($generalCategory->types as $index => $type)
		{
			$data[$index] = [
				'category_id'    => $type->id,
				'category_name'  => $type->name,
				'sub_categories' => [],
			];

			if ($type->tags->count())
			{
				foreach ($type->tags as $tag)
				{
					$data[$index]['sub_categories'][] = [
						'sub_category_id'   => $tag->id,
						'sub_category_name' => $tag->name,
						'is_selected'       => in_array($tag->id, $activeTags),
					];
				}
			}
		}

		return Api::responseData([
			'categories' => $data,
		]);
	}

	public function getTypeSetting(Request $request)
	{
		// Getting room data
		$room = Type::select(['id', 'name'])
			->with('facility_setting')
			->where('id', (int) $request->id)
			->first();

		if (is_null($room))
		{
			return Api::responseData([
				"status" => false,
				"meta"   => [
					"message" => 'Failed fetching kos type data',
				],
			]);
		}

		// Getting active tags
		$activeTags = [];
		if (!is_null($room->facility_setting))
		{
			if ($room->facility_setting->active_tags !== '')
			{
				$activeTags = explode(',', $room->facility_setting->active_tags);
			}
		}

		// Getting filtered facility structure
		$generalCategory = FacilityCategory::with('types.tags')
			->where('name', self::TYPE_CATEGORY_NAME)
			->first();

		if (is_null($generalCategory))
		{
			return Api::responseData([
				"status" => false,
				"meta"   => [
					"message" => 'Failed fetching Kos facility attributes',
				],
			]);
		}

		$data = [];

		foreach ($generalCategory->types as $index => $type)
		{
			$data[$index] = [
				'category_id'    => $type->id,
				'category_name'  => $type->name,
				'sub_categories' => [],
			];

			if ($type->tags->count())
			{
				foreach ($type->tags as $tag)
				{
					$data[$index]['sub_categories'][] = [
						'sub_category_id'   => $tag->id,
						'sub_category_name' => $tag->name,
						'is_selected'       => in_array($tag->id, $activeTags),
					];
				}
			}
		}

		return Api::responseData([
			'categories' => $data,
		]);
	}

	public function getTypeTags(int $id)
	{
		// Getting room data
		$room = Type::select(['id', 'name'])
			->with('facility_setting')
			->where('id', $id)
			->first();

		if (is_null($room))
		{
			return Api::responseData([
				"status" => false,
				"meta"   => [
					"message" => 'Failed fetching kos type data',
				],
			]);
		}

		// Getting active tags
		$activeTags = [];
		if (!is_null($room->facility_setting))
		{
			if ($room->facility_setting->active_tags !== '')
			{
				$activeTags = explode(',', $room->facility_setting->active_tags);
			}
		}

		// Getting filtered facility structure
		$generalCategory = FacilityCategory::with('types.tags')
			->where('name', self::TYPE_CATEGORY_NAME)
			->first();

		if (is_null($generalCategory))
		{
			return Api::responseData([
				"status" => false,
				"meta"   => [
					"message" => 'Failed fetching Kos facility attributes',
				],
			]);
		}

		$data = [];

		foreach ($generalCategory->types as $index => $type)
		{
			$data[$index] = [
				'category_id'   => $type->id,
				'category_name' => $type->name,
				'types'         => [],
			];

			if ($type->tags->count())
			{
				foreach ($type->tags as $tag)
				{
					if (in_array($tag->id, $activeTags))
					{
						$data[$index]['types'][] = [
							'type_id'   => $tag->id,
							'type_name' => $tag->name,
							'photos'    => $this->getRoomTypeTagPhotoUrls($tag->id, $room->id),
						];
					}
				}
			}
		}

		return Api::responseData([
			'facilities' => $data,
		]);
	}

	private function getRoomTypeTagPhotoUrls(int $tagId, int $roomTypeId)
	{
		$photos = [];

		$roomTypeFacilities = RoomTypeFacility::with('photo')
			->where('designer_type_id', $roomTypeId)
			->where('tag_id', $tagId)
			->get();

		if (!$roomTypeFacilities->count())
		{
			return $photos;
		}

		foreach ($roomTypeFacilities as $facility)
		{
			if (!is_null($facility->photo))
			{
				$photos[] = [
					'photo_id'  => $facility->photo_id,
					'photo_url' => $facility->photo->getMediaUrl()['medium'],
				];
			}
		}

		return $photos;
	}
}
