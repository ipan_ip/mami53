<?php

namespace App\Http\Controllers\Api\Room;

use App\Criteria\Room\RoomUnitCriteria;
use App\Http\Controllers\Api\BaseController;
use App\Http\Helpers\ApiResponse as Api;
use App\Presenters\Room\RoomUnitPresenter;
use App\Repositories\Room\RoomUnitRepository;
use App\Repositories\RoomRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;

class RoomUnitController extends BaseController
{

    /**
     * get list of units of a room
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Repositories\RoomRepository $roomRepository
     * @param \App\Repositories\Room\RoomUnitRepository $roomUnitRepository
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(
        Request $request,
        RoomRepository $roomRepository,
        RoomUnitRepository $roomUnitRepository
    ): JsonResponse {
        $user = $this->user();
        if (is_null($user)) {
            return Api::responseError(false, false, 401, 401);
        }

        $songId = $request->input('song_id');
        $limit  = $request->filled('limit') ? $request->input('limit') : 500;
        $offset = $request->filled('offset') ? $request->input('offset') : 0;

        $validator = Validator::make(
            [
                'song_id' => $songId,
                'limit' => $limit,
                'offset' => $offset,
            ],
            [
                'song_id' => 'required|numeric|min:1',
                'limit' => 'numeric|min:1|max:500',
                'offset' => 'numeric|min:0',
            ],
            Lang::get('validation')
        );

        if ($validator->fails()) {
            return Api::responseData([
                "status" => false,
                "meta" => [
                    "code" => 400,
                    "message" => implode("\n", $validator->errors()->all()),
                ]
            ]);
        }

        $room = $roomRepository->getRoomWithOwnerBySongId($songId);
        if (is_null($room) || ! is_null($room->apartment_project_id)) {
            return Api::responseError(false, false, 404, 404);
        }

        $owner = $room->owners->first();
        if (is_null($owner) || $user->id != $owner->user_id) {
            return Api::responseError(
                Lang::get('auth.unauthorized.resource', ['resource' => 'Kos']),
                'unauthorized',
                401,
                401
            );
        }

        if ($room->room_count <= 0) {
            return Api::responseData([
                'data' => [],
                'pagination' => [
                    'page'      => 1,
                    'limit'     => $limit,
                    'offset'    => $offset,
                    'total'     => 0,
                    'has-more'  => false,
                ]
            ]);
        }

        $total = $roomUnitRepository->where('designer_id', $room->id)->count();
        if ($total < 1) {
            $roomUnitRepository->backFillRoomUnit($room);
            $total = $roomUnitRepository->where('designer_id', $room->id)->count();
        }

        $roomUnitRepository->pushCriteria(new RoomUnitCriteria($limit, $offset));
        $roomUnits = $roomUnitRepository->with(['active_contract','level'])->where('designer_id', $room->id)->get();
        $roomIsBBK = $room->isBisaBooking();
        $roomIsGPLevel = $room->isRoomGPLevel();

        $transformedUnits = (new RoomUnitPresenter())->present($roomUnits);

        $page = ceil($offset / $limit) + 1;
        $responseData = array_merge($transformedUnits, [
            'room-data' => [
                'is_bbk'     => $roomIsBBK,
                'is_gp_kos'  => $roomIsGPLevel,
            ],
            'pagination' => [
                'page'      => $page,
                'limit'     => $limit,
                'offset'    => $offset,
                'total'     => $total,
                'has-more'  => $total > $limit * $page,
            ]
        ]);

        return Api::responseData($responseData);
    }
}
