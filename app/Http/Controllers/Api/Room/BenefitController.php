<?php

namespace App\Http\Controllers\Api\Room;

use App\Entities\Api\ErrorCode;
use App\Entities\Room\Room;
use App\Http\Controllers\Api\BaseController;
use App\Http\Helpers\ApiErrorHelper;
use App\Http\Helpers\ApiResponse as Api;
use App\Http\Helpers\FeatureFlagHelper;
use App\Http\Helpers\KostLevelValueHelper;

class BenefitController extends BaseController
{
    public function index($songId)
    {
        $room = Room::where('song_id', $songId)->first();

        if (is_null($room)) {
            return ApiErrorHelper::generateErrorResponse(ErrorCode::KOS_NOT_FOUND);
        }

        $benefit = KostLevelValueHelper::getKostValueWithoutId($room);

        //  from web with request variant or from app, show Booking Transaction as one of Kos Benefit
        if (
            Request('variant') == "true"
            || FeatureFlagHelper::isFromApp()
        ) {
            $kosBookingTransaction = KostLevelValueHelper::getKosBookingTransaction($room);
            if ($kosBookingTransaction) {
                array_unshift(
                    $benefit['benefits'],
                    KostLevelValueHelper::getKosBookingTransaction($room)
                );
            }
        }

        return Api::responseData($benefit);
    }
}
