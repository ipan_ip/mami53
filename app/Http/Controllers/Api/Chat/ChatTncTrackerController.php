<?php

namespace App\Http\Controllers\Api\Chat;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\BaseController;
use App\Http\Helpers\ApiResponse as Api;

use App\Entities\Activity\TrackingFeature;
use App\Entities\Tracker\ChatTncTracker;
use App\User;

use Validator;
use Carbon\Carbon;
use Auth;

class ChatTncTrackerController extends BaseController
{
    protected $validationMessages = [
        'seq.required' => 'Room ID harus diisi',
        'admin_id.required' => 'Admin ID harus diisi',
        'group_id.required' => 'Groupo ID harus diisi',
    ];

    /**
     * @param object $request->seq is Designer Id
     */
    public function store(Request $request)
    {
        $user = $this->user();

        if (is_null($user)) {
            return Api::responseData([
                'status' => false,
                'meta'   => [
                    'message' => 'Data user tidak ditemukan'
                ]
            ]);
        }

        // Validate request
        $validator = Validator::make($request->all(), 
            [
                'seq' => 'required',
                'group_id' => 'required',
                'admin_id' => 'required',
            ], $this->validationMessages);

        if ($validator->fails()) {
            return Api::responseData([
                    'status' => false,
                    'messages' => $validator->errors()->all()
                ]);
        }

        $storeLog = ChatTncTracker::report($request, $user);

        if ($storeLog) {
            return Api::responseData([
                'status' => true,
                'meta'   => [
                    'message' => 'Chat TnC Successfully Created'
                ]
            ]);
        } else {
            return Api::responseData([
                'status' => false,
                'meta'   => [
                    'message' => 'Chat TnC Failed Created'
                ]
            ]);
        }
    }

}