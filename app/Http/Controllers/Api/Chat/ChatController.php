<?php

namespace App\Http\Controllers\Api\Chat;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\BaseController;
use App\Entities\Activity\Question;
use App\Http\Helpers\ApiResponse as Api;
use App\Entities\Activity\TrackingFeature;

class ChatController extends BaseController
{
    public function list(Request $request)
    {
    	$for = 'kos';
        if ($request->filled('is_apartment')) $for = 'apartment';

    	$chats = Question::where('for', $for)
    					->where('status', 'true')
    					->whereNotNull('order')
    					->orderBy('order', 'asc')
    					->get();
    	$data = null;
    	$else = null;
    	foreach ($chats AS $key => $value) {
    		if (in_array($value->order, [1, 2, 3])) {
    			$data[] = array("id" => $value->id,
    						"question" => $value->question
    				);
    		} else {
    			$else[] = array("id" => $value->id,
    						"question" => $value->question
    				);
    		}
    	}
        TrackingFeature::insertOrUpdateView('chat-v2');
    	return Api::responseData(["status" => true, "main" => $data, "second" => $else]);
    }
}
