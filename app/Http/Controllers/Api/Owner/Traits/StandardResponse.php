<?php

namespace App\Http\Controllers\Api\Owner\Traits;

use App\Exceptions\Owner\ResourceNotFoundException;
use App\Exceptions\Owner\ResourceNotOwnedException;
use App\Http\Helpers\ApiResponse as Api;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

trait StandardResponse
{
    /**
     * response gateway
     *
     * @param array|\Illuminate\Contracts\Support\Arrayable $body
     * @param \Exception|null $exception
     * @return \Illuminate\Http\JsonResponse
     */
    public function response($body, ?Exception $exception = null, int $errorCode = 100001): JsonResponse
    {
        if (is_null($exception)) {
            return Api::responseData($body);
        }

        $exceptionMapping = [
            ResourceNotOwnedException::class => [
                'status' => 403,
                'severity' => 'FORBIDDEN'
            ],
            ResourceNotFoundException::class => [
                'status' => 404,
                'severity' => 'NOT FOUND'
            ],
            ModelNotFoundException::class => [
                'status' => 404,
                'severity' => 'NOT FOUND'
            ],
            AuthorizationException::class => [
                'status' => 404,
                'severity' => 'NOT FOUND'
            ],
            ValidationException::class => [
                'status' => 400,
                'severity' => 'INVALID REQUEST'
            ],
            Exception::class => [
                'status' => 400,
                'severity' => 'INVALID REQUEST'
            ],
        ];

        $exceptionClass = get_class($exception);

        if (isset($exceptionMapping[$exceptionClass])) {
            $messageExtractor = function ($exception): string {
                if ($exception instanceof ValidationException) {
                    return implode(', ', array_flatten($exception->errors()));
                }
                return $exception->getMessage();
            };

            try {
                $status = is_null($this->device()) ? $exceptionMapping[$exceptionClass]['status'] : 200; //app cannot use non 200 response yet
            } catch (Exception $e) {
                $status = $exceptionMapping[$exceptionClass]['status'];
            }

            return Api::responseError(
                $messageExtractor($exception),
                $exceptionMapping[$exceptionClass]['severity'],
                $status,
                $status
            );
        }

        Bugsnag::notifyException($exception);
        return Api::responseError(
            $exception->getMessage(),
            'INVALID REQUEST',
            400,
            400
        );
    }
}
