<?php

namespace App\Http\Controllers\Api\Owner;

use App\Enums\ABTest\ExperimentValue;
use App\Http\Controllers\Api\BaseController as MamiController;
use App\User;
use App\Http\Helpers\ApiResponse as Api;
use Illuminate\Support\Facades\Lang;

class ConfigController extends MamiController
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $user = $this->user();

            if (! $user instanceof User) {
                return Api::responseError(
                    false,
                    false,
                    401,
                    401
                );
            }

            if (! $user->isOwner()) {
                return Api::responseError(
                    Lang::get(
                        'auth.unauthorized.resource',
                        ['resource' => 'Config']
                    ),
                    false,
                    401,
                    401
                );
            }

            return $next($request);
        });
    }

    public function abTest()
    {
        $config = config('abtest.experiment.ox.force_bbk');
        return Api::responseData(
            [
                'status' => true,
                'data' => [
                    'force_bbk' => [
                        'experiment_id' => ! empty($config['id']) ? (int) $config['id'] : null,
                        'use_varian' => ! empty($config['use_varian']) ? (bool) $config['use_varian'] : false,
                        'varian' => ! empty($config['varian']) ? (string) $config['varian'] : ExperimentValue::CONTROL_VALUE,
                    ]
                ],
            ]
        );

    }
}