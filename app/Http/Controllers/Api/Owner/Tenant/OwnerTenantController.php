<?php

namespace App\Http\Controllers\Api\Owner\Tenant;

use App\Entities\Dbet\DbetLink;
use App\Entities\Room\BookingOwnerRequest;
use App\Exceptions\CustomErrorException;
use App\Http\Controllers\Api\BaseController;
use App\Http\Helpers\ApiResponse as Api;
use App\Http\Requests\Owner\Tenant\OwnerTenantLinkRequest;
use App\Presenters\Owner\Tenant\OwnerTenantPresenter;
use App\Repositories\Dbet\DbetLinkRegisteredRepository;
use App\Repositories\Dbet\DbetLinkRepository;
use App\Repositories\Room\RoomUnitRepository;
use App\Repositories\RoomRepository;
use App\Services\Booking\BookingService;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Illuminate\Http\JsonResponse;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * Class OwnerTenantController
 * @package App\Http\Controllers\Api\Owner\Tenant
 */
class OwnerTenantController extends BaseController
{
    protected $repository;
    protected $service;

    /**
     * OwnerTenantController constructor.
     */
    public function __construct()
    {
        $this->repository = app()->make(DbetLinkRepository::class);
        $this->service = app()->make(BookingService::class);
    }

    /**
     * @param OwnerTenantLinkRequest $request
     * @return JsonResponse
     */
    public function store(OwnerTenantLinkRequest $request): JsonResponse
    {
        $response = $this->makeDefaultResponse();
        try {
            // validate data
            $validator = $request->failedValidator;
            if ($validator && $validator->fails())
                throw new CustomErrorException(implode(", ",$validator->errors()->all()));

            // get data
            $room = $request->getRoom();

            // checking unique code is null
            if (optional($room)->unique_code === null) {
                $room->generateUniqueCode();
                $room = $room->fresh();
            }

            $user = $this->user();
            if (!$user)
                throw new CustomErrorException('User tidak ditemukan');

            // check ownership
            if (!$room->ownedBy($user))
                throw new CustomErrorException('Access Denied');

            DB::beginTransaction();
            $data = $this->repository->findByDesignerId($room->id);
            if ($data !== null) {
                $this->repository->update([
                    'is_required_identity' => $request->is_required_identity,
                    'is_required_due_date' => $request->is_required_due_date,
                    'due_date' => $request->due_date
                ], $data->id);

                $data = $data->fresh();
            } else {
                $data = $this->repository->create([
                    'designer_id' => $room->id,
                    'code' => $room->unique_code->code,
                    'is_required_identity' => $request->is_required_identity,
                    'is_required_due_date' => $request->is_required_due_date,
                    'due_date' => $request->due_date
                ]);
            }

            // transform data for response
            $transform = (new OwnerTenantPresenter('data'))->present($data);
            DB::commit();

            $response = $transform;

        } catch (CustomErrorException $e) {
            DB::rollBack();
            $response['meta']['message'] = $e->getMessage();
        } catch (Exception $e) {
            DB::rollBack();
            $response['meta']['message'] = $e->getMessage();
            Bugsnag::notifyException($e);
        }
        return Api::responseData($response);
    }


    /**
     * @param Request $request
     * @param $songId
     * @return JsonResponse
     */
    public function show(Request $request, $songId): JsonResponse
    {
        $response = $this->makeDefaultResponse();
        try {
            $user = $this->user();
            if (!$user)
                throw new CustomErrorException('User tidak ditemukan');

            // repository
            $roomRepo = app(RoomRepository::class);

            // get room
            $room = $roomRepo->getRoomWithOwnerBySongId($songId);
            if (!$room)
                throw new CustomErrorException('Kost tidak ditemukan');

            // get data dbet link
            $data = $this->repository->findByDesignerId($room->id);
            if (!$data)
                throw new CustomErrorException('Data tidak ditemukan');

            // transform data for response
            $transform = (new OwnerTenantPresenter('data'))->present($data);

            $response = $transform;
        } catch (CustomErrorException $e) {
            $response['meta']['message'] = $e->getMessage();
        } catch (Exception $e) {
            $response['meta']['message'] = $e->getMessage();
            Bugsnag::notifyException($e);
        }
        return Api::responseData($response);
    }

    /**
     * @param Request $request
     * @param $songId
     * @return JsonResponse
     */
    public function getDashboard(Request $request): JsonResponse
    {
        $response = $this->makeDefaultResponse();
        try {
            $user = $this->user();
            if (!$user)
                throw new CustomErrorException('User tidak ditemukan');

            // repository
            $roomUnitRepo   = app(RoomUnitRepository::class);
            $roomRepo       = app(RoomRepository::class);
            $dbetRepo       = app(DbetLinkRegisteredRepository::class);

            // default data
            $roomIds                        = [];
            $totalOccupiedWithContract      = 0;
            $totalOccupiedWithoutContract   = 0;
            $totalContractSubmission        = 0;

            // get room bbk
            $rooms = $roomRepo
                ->select('id')
                ->whereNull('apartment_project_id')
                ->whereHas('owners', function($ownerQuery) use($user){
                    $ownerQuery
                        ->where('user_id', $user->id)
                        ->whereIn('status', ['add', 'verified', 'draft2']);
                })
                ->where('is_booking',1)
                ->whereHas('booking_owner_requests',function($q){
                    $q->where('status', BookingOwnerRequest::BOOKING_APPROVE);
                })
                ->get();

            if ($rooms) {
                $roomIds = $rooms->pluck('id')->toArray();
            }

            if (!empty($roomIds)) {
                // get room occupied with contract
                $totalOccupiedWithContract = $roomUnitRepo
                    ->where('occupied', true)
                    ->whereIn('designer_id', $roomIds)
                    ->whereHas('active_contract')
                    ->count();

                // get room occupied
                $totalOccupiedRoom = $roomUnitRepo
                    ->where('occupied', true)
                    ->whereIn('designer_id', $roomIds)
                    ->count();

                // get room occupied without contract
                $totalOccupiedWithoutContract = $totalOccupiedRoom - $totalOccupiedWithContract;

                // get total submission
                $totalContractSubmission = $dbetRepo
                    ->whereIn('designer_id', $roomIds)
                    ->where('status', DbetLink::PENDING)
                    ->count();
            }

            $data = [
                'total_off_contract'                => $totalOccupiedWithContract,
                'total_off_contract_not_recorded'   => $totalOccupiedWithoutContract,
                'total_off_contract_submission'     => $totalContractSubmission,
                'enable_dbet_link'                  => $this->service->ownerEnableDbetLink($user)
            ];
            $tranform = (new OwnerTenantPresenter('dashboard', $data))->present([]);

            $response = $tranform;
        } catch (CustomErrorException $e) {
            $response['meta']['message'] = $e->getMessage();
        } catch (Exception $e) {
            $response['meta']['message'] = $e->getMessage();
            Bugsnag::notifyException($e);
        }
        return Api::responseData($response);
    }

    /**
     * Function to return default response
     * @return array
     */
    private function makeDefaultResponse(): array
    {
        return [
            'status' => false,
            'meta' => ['message' => 'General Error']
        ];
    }
}