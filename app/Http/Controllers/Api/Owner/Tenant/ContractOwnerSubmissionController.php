<?php

namespace App\Http\Controllers\Api\Owner\Tenant;

use App\Entities\Dbet\DbetLinkRegistered;
use App\Entities\Room\Element\RoomUnit;
use App\Exceptions\CustomErrorException;
use App\Http\Controllers\Api\BaseController;
use App\Http\Helpers\ApiResponse as Api;
use App\Presenters\Owner\Tenant\ContractSubmissionPresenter;
use App\Repositories\Dbet\DbetLinkRegisteredRepository;
use App\Repositories\RoomRepository;
use App\Services\Booking\ContractSubmissionService;
use App\Transformers\Owner\Tenant\ContractSubmissionOwnerDetailTransformer;
use App\User;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

/**
 * Class ContractOwnerSubmissionController
 * @package App\Http\Controllers\Api\Owner\Tenant
 */
class ContractOwnerSubmissionController extends BaseController
{
    protected $repository;
    protected $service;

    /**
     * OwnerTenantController constructor.
     */
    public function __construct(
        DbetLinkRegisteredRepository $dbetLinkRegisteredRepository,
        ContractSubmissionService $service
    ) {
        $this->repository = $dbetLinkRegisteredRepository;
        $this->service = $service;
        parent::__construct();
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $response = $this->makeDefaultResponse();
        try {
            $user = $this->user();
            if (!$user)
                throw new CustomErrorException('User tidak ditemukan');

            // default data
            $limit = $request->limit ?? 10;
            $page = $request->page ?? 1;
            $designerId = $request->designer_id ?? null;

            if ($designerId === null)
                throw new CustomErrorException('Data kost tidak boleh kosong');

            // repository
            $roomRepo = app(RoomRepository::class);

            // get room
            $room = $roomRepo->getRoomWithOwnerBySongId($designerId);
            if (!$room)
                throw new CustomErrorException('Kost tidak ditemukan');

            // check ownership
            if (!$room->ownedBy($user))
                throw new CustomErrorException('Access Denied');

            // params data
            $params = [];
            $params['limit']        = $limit;
            $params['designer_id']  = $room->id;
            $params['status']       = DbetLinkRegistered::STATUS_PENDING;

            // get data
            $data = $this->repository->getListContractSubmission($params);

            // transform data for response
            $transform = (new ContractSubmissionPresenter('owner-list'))->present($data);

            $response = [
                'status' => true,
                'data' => [
                    'data'          => $transform['data'] ?? null,
                    'current_page'  => $data->currentPage(),
                    'from'          => $data->firstItem(),
                    'last_page'     => $data->lastPage(),
                    'next_page_url' => $data->nextPageUrl(),
                    'per_page'      => $data->perPage(),
                    'prev_page_url' => $data->previousPageUrl(),
                    'to'            => $data->count(),
                    'total'         => $data->total(),
                    'has_more'      => $data->hasMorePages()
                ]
            ];
        } catch (CustomErrorException $e) {
            $response['meta']['message'] = $e->getMessage();
        } catch (Exception $e) {
            $response['meta']['message'] = $e->getMessage();
            Bugsnag::notifyException($e);
        }
        return Api::responseData($response);
    }

    /**
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function show(Request $request, $id): JsonResponse
    {
        $response = $this->makeDefaultResponse();
        try {
            $user = $this->user();
            if (!$user)
                throw new CustomErrorException('User tidak ditemukan');

            // get data dbet link
            $data = $this->repository->with(['room'])->find($id);
            if (!$data)
                throw new CustomErrorException('Data tidak ditemukan');

            // get room
            $room = $data->room;
            if (!$room)
                throw new CustomErrorException('Data kost ditemukan');

            // check ownership
            if (!$room->ownedBy($user))
                throw new CustomErrorException('Access Denied');

            // transform data for response
            $transform = (new ContractSubmissionPresenter('owner-detail'))->present($data);

            $response = $transform;
        } catch (ModelNotFoundException $e){
            $response['meta']['message'] = 'Data tidak ditemukan';
        } catch (CustomErrorException $e) {
            $response['meta']['message'] = $e->getMessage();
        } catch (Exception $e) {
            $response['meta']['message'] = $e->getMessage();
            Bugsnag::notifyException($e);
        }
        return Api::responseData($response);
    }

    /**
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function reject(Request $request, $id): JsonResponse
    {
        $response = $this->makeDefaultResponse();
        try {
            $user = $this->user();
            if (!$user)
                throw new CustomErrorException('User tidak ditemukan');

            // get data dbet link
            $data = $this->repository->with(['room'])->find($id);
            if (!$data)
                throw new CustomErrorException('Data tidak ditemukan');

            if ($data->status !== DbetLinkRegistered::STATUS_PENDING)
                throw new CustomErrorException('Data telah di proses');

            // update data
            $this->repository->update([
                'status' => DbetLinkRegistered::STATUS_REJECTED,
            ], $data->id);

            // send notif
            $this->service->ownerRejectNotification($data);

            $data = $data->fresh();

            $transform = (new ContractSubmissionPresenter('owner-detail'))->present($data);

            $response = $transform;
        } catch (ModelNotFoundException $e){
            $response['meta']['message'] = 'Data tidak ditemukan';
        } catch (CustomErrorException $e) {
            $response['meta']['message'] = $e->getMessage();
        } catch (Exception $e) {
            $response['meta']['message'] = $e->getMessage();
            Bugsnag::notifyException($e);
        }
        return Api::responseData($response);
    }

    /**
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function confirm(Request $request, $id): JsonResponse
    {
        $response = $this->makeDefaultResponse();
        try {
            $user = $this->user();
            if (!$user)
                throw new CustomErrorException('User tidak ditemukan');

            // get data dbet link
            $data = $this->repository->with(['room'])->find($id);
            if (!$data)
                throw new CustomErrorException('Data tidak ditemukan');

            if ($data->status !== DbetLinkRegistered::STATUS_PENDING)
                throw new CustomErrorException('Data telah di proses');

            if (empty($request->price_type))
                throw new CustomErrorException('Tipe harga tidak ditemukan');

            if (!in_array($request->price_type, ['revised', 'original']))
                throw new CustomErrorException('Tipe harga tidak ditemukan');

            if (!empty($request->designer_room_id)) {
                $roomUnitData = RoomUnit::find($request->designer_room_id);
                if (!$roomUnitData) {
                    throw new CustomErrorException('Kamar tidak ditemukan');
                }
            }

            DB::beginTransaction();
            // update data
            $this->repository->update([
                'status' => DbetLinkRegistered::STATUS_VERIFIED,
            ], $data->id);

            // data revised
            $isRevisedPrice = false;

            // create contract
            $payload = $this->createRequestAddTenantData($request, $data);
            $isRevisedPrice = $payload['is_revised_price'] ?? false;

            // unset data
            unset($payload['is_revised_price']);

            // hit api create contract
            $contract = $this->addTenantByConfirmSubmission($user, $payload);
            if($contract['status'] === false) {
                throw new CustomErrorException($contract['message']);
            }

            // send notif
            $this->service->ownerConfirmNotification($data, $request->price_type, $isRevisedPrice);

            DB::commit();

            $data = $data->fresh();

            $transform = (new ContractSubmissionPresenter('owner-detail'))->present($data);

            // output
            $result = $transform['data'] ?? [];
            $result['contract_id'] = (isset($contract['data']) && $contract['data']->contract->id) ? $contract['data']->contract->id : 0;
            $response = [
                'status' => true,
                'data' => $result
            ];

        } catch (ModelNotFoundException $e){
            $response['meta']['message'] = 'Data tidak ditemukan';
            DB::rollBack();
        } catch (CustomErrorException $e) {
            $response['meta']['message'] = $e->getMessage();
            DB::rollBack();
        } catch (Exception $e) {
            $response['meta']['message'] = $e->getMessage();
            Bugsnag::notifyException($e);
            DB::rollBack();
        }
        return Api::responseData($response);
    }

    private function addTenantByConfirmSubmission(User $user, array $data = [])
    {
        $client = new Client([
            'base_uri'  => Config::get('api.mamipay.url'),
            'headers'   => $this->createHeaderRequest('POST', 'tenant/add', $user->id)
        ]);

        $httpRequest = $client->post('tenant/add',
            [
                'body' => json_encode($data)
            ]
        );
        $httpRequestResult = json_decode($httpRequest->getBody()->getContents());

        if (!$httpRequestResult->status || $httpRequestResult->status === false) {
            return [
                'status' => false,
                'message' => $httpRequestResult->meta->message ?? 'Gagal membuat kontrak'
            ];
        }

        return [
            'status' => true,
            'data' => $httpRequestResult->data ?? null
        ];

    }

    private function createHeaderRequest($method, $targetPath, $uid): array
    {
        $xGitTime       = Carbon::now()->timestamp;
        $authString     = $method .' ' . 'api/v1/' . $targetPath . ' ' . $xGitTime;
        $key            = Config::get('api.secret_key');
        $authKeyToken   = hash_hmac('sha256', $authString, $key);

        return [
            'X-GIT-PF'      => 'web',
            'Authorization' => 'GIT ' . $authKeyToken . ':' . base64_encode('WEB-' . $uid),
            'X-GIT-Time'    => $xGitTime,
            'Content-Type'  => 'application/json'
        ];
    }

    private function createRequestAddTenantData(Request $request, DbetLinkRegistered $submission): array
    {
        // get data
        $room = optional($submission)->room;
        $user = optional($submission)->user;
        $roomUnit = optional($submission)->room_unit;

        $additionalCost = [];
        if ($submission->additional_price_detail !== null) {
            $additionalCost = json_decode($submission->additional_price_detail) ?? null;
            if ($additionalCost !== null && count($additionalCost) > 0) {
                $additionalCost = collect($additionalCost)->map(function ($data, $key) {
                    return [
                        'field_title' => $data->name ?? null,
                        'field_value' => $data->price ?? null,
                    ];
                });
            }
        }

        $submissionDetail = (new ContractSubmissionOwnerDetailTransformer)->transform($submission);

        // data
        $designerRoomName = optional($roomUnit)->name;
        $designerRoomId = optional($roomUnit)->id;
        if ($designerRoomId === null) {
            $designerRoomId = $request->designer_room_id ?? null;
            $roomUnitData = RoomUnit::find($designerRoomId);
            if ($roomUnitData) {
                $designerRoomName = optional($roomUnitData)->name;
            }
        }

        $amount = $submissionDetail['original_price'];
        if ($submissionDetail['is_revised_price'] === true && $request->price_type === 'revised') {
            $amount = $submissionDetail['revised_price'];
        }

        return [
            'add_tenant_from' => 'dbet_link',
            'additional_costs' => !empty($additionalCost) ? $additionalCost->toArray() : [],
            'amount' => $amount,
            'billing_date' => $submission->due_date,
            'designer_room_id' => $designerRoomId,
            'duration' => 1,
            'email' => optional($user)->email,
            'fine_amount' => 0,
            'fixed_billing' => false,
            'gender' => $submission->gender ?? null,
            'marital_status' => optional($user)->marital_status,
            'name' => $submission->fullname ?? null,
            'occupation' => $submission->jobs ?? null,
            'owner_accept' => true,
            'phone_number' => $submission->phone ?? null,
            'photo_document_id' => 0,
            'photo_id' => 0,
            'photo_identifier_id' => $submission->identity_id,
            'rent_type' => $submission->rent_count_type ?? null,
            'room_id' => $room->song_id,
            'room_number' => $designerRoomName,
            'save_cost_group' => false,
            'start_date' => Carbon::now()->format('Y-m-d'),
            'use_dp' => false,
            'is_revised_price' => $submissionDetail['is_revised_price']
        ];
    }

    /**
     * Function to return default response
     * @return array
     */
    private function makeDefaultResponse(): array
    {
        return [
            'status' => false,
            'meta' => ['message' => 'General Error']
        ];
    }
}