<?php

namespace App\Http\Controllers\Api\Owner\Data\Property;

use App\Entities\Property\Property;
use App\Entities\Room\Room;
use App\Http\Controllers\Api\Owner\BaseController;
use App\Services\Owner\Kost\InputService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class NameValidationController extends BaseController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            $validator = Validator::make(
                $request->all(),
                ['name' => 'required|min:1']
            );
            try {
                $validator->validate();
            } catch (ValidationException $exception) {
                return $this->response([], $exception, 100422);
            }
            return $next($request);
        });
    }

    public function property(Request $request): JsonResponse
    {
        return $this->response([
            'available' => ! (
                Property::where('owner_user_id', $this->user()->id)
                    ->where('name', $request->input('name'))
                    ->first() instanceof Property
            )
        ]);
    }

    public function kostType(int $id, Request $request, InputService $service): JsonResponse
    {
        try {
            $property = Property::with('rooms')
                ->where('owner_user_id', $this->user()->id)
                ->where('id', $id)
                ->firstOrFail();

            $name = $request->input('name');
            $sameNamedKost = $property->rooms->first(function ($room) use ($property, $name, $service) {
                if (!empty($room->unit_type) || !$property->has_multiple_type) {
                    return strtolower($room->unit_type) == strtolower($name);
                }

                return in_array(strtolower($room->name), [
                    strtolower("$property->name $name $room->area_subdistrict $room->area_city"),
                    strtolower($service->getNamePattern($property, $name, $room->area_subdistrict, $room->area_city)),
                    strtolower("$property->name $name")
                ]);
            });

            return $this->response([
                'available' => !$sameNamedKost instanceof Room
            ]);
        } catch (ModelNotFoundException $exception) {
            return $this->response([], $exception, 100403);
        }
    }
}
