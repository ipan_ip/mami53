<?php

namespace App\Http\Controllers\Api\Owner\Data\Property;

use App\Entities\Property\Property;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Http\Controllers\Api\Owner\BaseController;
use App\Transformers\Owner\Data\Property\PropertyTransformer;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class ListController extends BaseController
{
    /**
     * list index
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Transformers\Owner\Data\Property\PropertyTransformer $transformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request, PropertyTransformer $transformer): JsonResponse
    {
        $validator = Validator::make(
            $request->all(),
            [
                'filter' => 'required|in:all,multiple,single'
            ]
        );

        try {
            $data = $validator->validate();
        } catch (ValidationException $exception) {
            return $this->response([], $exception, 100422);
        }

        $userId = $this->user()->id;
        $validRoomIds = Room::isKost()
            ->whereHas('owners', function ($q) use ($userId) {
                $q->where('user_id', $userId)
                    ->whereIn('status', RoomOwner::ROOM_STATUS_VERIFIED_OR_WAITING);
            })
            ->whereHas('property', function ($q) use ($data) {
                if (in_array($data['filter'], ['multiple', 'single'])) {
                    $q->where(
                        'has_multiple_type',
                        $data['filter'] === 'multiple'
                    );
                }
            })
            ->pluck('id')
            ->toArray();

        $properties = Property::where('owner_user_id', $userId)
            ->whereHas('rooms', function ($q) use ($validRoomIds) {
                $q->whereIn('id', $validRoomIds);
            })
            ->with([
                'rooms' => function ($q) use ($validRoomIds) {
                    $q->whereIn('id', $validRoomIds)->with('property_icon.photo');
                }
            ])
            ->get();
        
        return $this->response([
            'data' => $properties->map(
                function ($property) use ($transformer): array {
                    return $transformer->transform($property);
                }
            )->toArray()
        ]);
    }
}
