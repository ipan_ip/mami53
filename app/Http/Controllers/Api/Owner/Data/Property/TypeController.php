<?php

namespace App\Http\Controllers\Api\Owner\Data\Property;

use App\Entities\Property\Property;
use App\Entities\Room\RoomOwner;
use App\Http\Controllers\Api\Owner\BaseController;
use App\Transformers\Owner\Data\Property\TypeTransformer;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;

class TypeController extends BaseController
{
    public function index(int $id, TypeTransformer $transformer): JsonResponse
    {
        $user = $this->user();
        try {
            $property = Property::with([
                'rooms' => function ($query) use ($user) {
                    return $query->isKost()->whereHas(
                        'owners',
                        function ($query) use ($user) {
                            return $query->where('user_id', $user->id)
                                ->whereIn('status', RoomOwner::ROOM_STATUS_VERIFIED_OR_WAITING);
                        }
                    );
                }
            ])->where('owner_user_id', $user->id)->where('id', $id)->firstOrFail();

            return $this->response([
                'data' => $property->rooms->map(
                    function ($room) use ($transformer) {
                        return $transformer->transform($room);
                    }
                )->toArray()
            ]);
        } catch (ModelNotFoundException $exception) {
            return $this->response([], $exception);
        }
    }
}
