<?php

namespace App\Http\Controllers\Api\Owner\Data\Kost;

use App\Entities\Room\Element\Tag\GroupType as TagGroup;
use App\Http\Controllers\Api\Owner\BaseController;
use App\Repositories\TagRepository;
use App\Transformers\Owner\Data\Kost\TagListTransformer;
use Illuminate\Http\JsonResponse;

class MinimumPaymentController extends BaseController
{
    /**
     * get list kost rules data
     *
     * @param \App\Repositories\TagRepository $repository
     * @param \App\Transformers\Owner\Data\Kost\TagListTransformer $transformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(
        TagRepository $repository,
        TagListTransformer $transformer
    ): JsonResponse {
        return $this->response([
            'data' => $transformer->transform(
                $repository->getTagByType(
                    [TagGroup::MINIMUM_PAYMENT_TAG_TYPE]
                )
            )
        ]);
    }
}
