<?php

namespace App\Http\Controllers\Api\Owner\Data\Kost;

use App\Entities\Property\Property;
use App\Entities\Room\Element\CreationFlag;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Exceptions\Owner\ResourceNotOwnedException;
use App\Http\Controllers\Api\Owner\BaseController;
use App\Http\Helpers\ApiHelper;
use App\Services\Owner\Kost\DataStageService;
use App\Services\Owner\Kost\DuplicateService;
use App\Transformers\Owner\Data\Kost\DataStageTransformer;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class DuplicateController extends BaseController
{
    /**
     * duplicate kost
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Services\Owner\Kost\DuplicateService $duplicateService
     * @param \App\Services\Owner\Kost\DataStageService $stageService
     * @param \App\Transformers\Owner\Data\Kost\DataStageTransformer $transformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function duplicate(
        Request $request,
        DuplicateService $duplicateService,
        DataStageService $stageService,
        DataStageTransformer $transformer
    ): JsonResponse {
        $validator = Validator::make(
            $request->all(),
            [
                'room_type'     => 'required_with:duplicate_from|string',
                'property_id'   => 'required|integer|min:1',
                'duplicate_from' => 'nullable|integer|min:1',
            ]
        );

        try {
            $data = $validator->validate();
            $property = Property::findOrFail($data['property_id']);

            $user = $this->user();
            if ($property->owner_user_id != $user->id) {
                return $this->response(
                    [],
                    new ResourceNotOwnedException(
                        Lang::get(
                            'auth.unauthorized.resource',
                            ['resource' => 'Property']
                        )
                    ),
                    100403
                );
            }

            if (is_null($request->input('duplicate_from'))) {
                $sourceRoom = Room::with('cards')
                    ->whereHas('property', function ($query) use ($property) {
                        $query->where('id', $property->id);
                    })
                    ->whereHas('owners', function ($q) use ($user) {
                        $q->where('user_id', $user->id)
                            ->whereIn('status', RoomOwner::ROOM_STATUS_VERIFIED_OR_WAITING);
                    })
                    ->orderBy('updated_at', 'desc')
                    ->firstOrFail();
            } else {
                $sourceRoom = Room::with('cards')
                    ->where('song_id', $request->input('duplicate_from'))
                    ->whereHas('property', function ($query) use ($property) {
                        $query->where('id', $property->id);
                    })
                    ->firstOrFail();
            }
        } catch (ValidationException $exception) {
            return $this->response([], $exception, 100422);
        } catch (ModelNotFoundException $exception) {
            return $this->response([], $exception, 100404);
        }

        if (is_null($request->input('duplicate_from'))) {
            $room = $duplicateService->duplicateKostWithoutRoomData(
                $sourceRoom,
                $user
            );

            $createFrom = CreationFlag::DUPLICATE_WITHOUT_TYPE;
        } else {
            $room = $duplicateService->duplicateKost(
                ApiHelper::removeEmoji($request->input('room_type')),
                $sourceRoom,
                $user
            );

            $createFrom = CreationFlag::DUPLICATE_FROM_TYPE;
        }

        $room->refresh();

        CreationFlag::updateOrCreate(
            [
                'create_from' => $createFrom,
                'log' => [
                    'user_id' => $user->id,
                    'property' => $property instanceof Property ? $property->toArray() : null,
                    'request-data' => $request->all()
                ]
            ],
            [
                'designer_id' => $room->id
            ]
        );

        return $this->response([
            'stages' => $transformer->transform(
                $stageService->getInputStages($room),
                $room->song_id
            ),
            'is_active_photo_booking' => $room->isPremiumPhoto()
        ]);
    }
}
