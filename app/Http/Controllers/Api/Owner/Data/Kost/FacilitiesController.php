<?php

namespace App\Http\Controllers\Api\Owner\Data\Kost;

use App\Entities\Room\Element\Tag\GroupType as FacilitiesGroup;
use App\Http\Controllers\Api\Owner\BaseController;
use App\Repositories\TagRepository;
use App\Transformers\Owner\Data\Kost\FacilitiesTransformer;
use Illuminate\Http\JsonResponse;

class FacilitiesController extends BaseController
{
    /**
     * get list kost facilities data for prefilled form
     *
     * @param \App\Repositories\TagRepository $repository
     * @param \App\Transformers\Owner\Data\Kost\FacilitiesTransformer $transformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(
        TagRepository $repository,
        FacilitiesTransformer $transformer
    ): JsonResponse {
        return $this->response([
            'data' => $transformer->transform(
                $repository->getTagByType(
                    array_keys(FacilitiesGroup::FACILITIES_MAPPING)
                )
            )
        ]);
    }
}
