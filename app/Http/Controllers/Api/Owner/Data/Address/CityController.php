<?php

namespace App\Http\Controllers\Api\Owner\Data\Address;

use App\Entities\Area\Province;
use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Api\Owner\Traits\StandardResponse;
use App\Libraries\AreaFormatter;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class CityController extends BaseController
{
    use StandardResponse;

    /**
     * get list of city based on province name
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $validator = Validator::make(
            $request->all(),
            [
                'province_name' => 'required'
            ]
        );

        try {
            $data = $validator->validate();
        } catch (ValidationException $exception) {
            return $this->response([], $exception, 100001);
        }

        $formattedName = AreaFormatter::provinceFormatter($data['province_name']);

        $provinces = Province::with('cities')->where(
            'name',
            $formattedName
        )->get();

        if ($provinces->isEmpty()) {
            return $this->response([], new ModelNotFoundException("Province not Found"));
        }

        return $this->response([
            'data' => $provinces->reduce(
                function ($carry, $province) {
                    return array_merge(
                        $carry,
                        $province->cities->map(
                            function ($city) {
                                return [
                                    'id' => $city->id,
                                    'name' => $city->name
                                ];
                            }
                        )->sortBy('name')->toArray()
                    );
                },
                []
            )
        ]);
    }
}
