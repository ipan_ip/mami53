<?php

namespace App\Http\Controllers\Api\Owner\Data\Address;

use App\Entities\Area\City;
use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Api\Owner\Traits\StandardResponse;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class SubdistrictController extends BaseController
{
    use StandardResponse;

    /**
     * get list of subdistricts based on city
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $validator = Validator::make(
            $request->all(),
            [
                'city' => 'required|string|min:1'
            ]
        );

        try {
            $data = $validator->validate();
        } catch (ValidationException $exception) {
            return $this->response([], $exception, 100001);
        }

        $cities = City::with('subdistricts')->where(function ($query) use ($data) {
            if (preg_match("/(Kabupaten|Kota)/i", $data['city'])) {
                return $query->where('name', $data['city']);
            }
            return $query->where('name', $data['city'])
                ->orWhere('name', "Kota {$data["city"]}")
                ->orWhere('name', "Kabupaten {$data["city"]}");
        })->get();

        if ($cities->isEmpty()) {
            return $this->response([], new ModelNotFoundException("City not Found"));
        }

        return $this->response([
            'data' => $cities->reduce(function ($carry, $city) {
                    return array_merge(
                        $carry,
                        $city->subdistricts->map(function ($subdistrict) {
                            return [
                                'id' => $subdistrict->id,
                                'name' => $subdistrict->name
                            ];
                        })->sortBy('name')->toArray()
                    );
                },
                []
            )
        ]);
    }
}
