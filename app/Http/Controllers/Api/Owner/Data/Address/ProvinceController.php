<?php

namespace App\Http\Controllers\Api\Owner\Data\Address;

use App\Entities\Area\Province;
use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Api\Owner\Traits\StandardResponse;

class ProvinceController extends BaseController
{
    use StandardResponse;
    /**
     *  Retrieve all provinces
     * 
     *  @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return $this->response([
            'data' => Province::select('id','name')->get()->toArray(),
        ]);
    }
}
