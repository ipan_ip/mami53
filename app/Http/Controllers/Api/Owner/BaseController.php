<?php

namespace App\Http\Controllers\Api\Owner;

use App\Exceptions\Owner\ResourceNotOwnedException;
use App\Http\Controllers\Api\BaseController as MamiController;
use App\Http\Controllers\Api\Owner\Traits\StandardResponse;
use App\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Facades\Lang;

class BaseController extends MamiController
{
    use StandardResponse;

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            $user = $this->user();

            if (!$user instanceof User) {
                return $this->response([], new AuthorizationException("User Not Logged In"));
            }

            if (! $user->isOwner() && ! $user->isAdmin()) {
                return $this->response(
                    [],
                    new ResourceNotOwnedException(
                        Lang::get(
                            'auth.unauthorized.resource',
                            ['resource' => 'Kost']
                        )
                    )
                );
            }

            return $next($request);
        });
    }
}
