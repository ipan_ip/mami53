<?php

namespace App\Http\Controllers\Api\Owner\Room;

use App\Entities\Mamipay\MamipayRoomPriceComponentAdditional;
use App\Enums\Mamipay\MamipayRoomPriceComponentType;
use App\Exceptions\CustomErrorException;
use App\Http\Controllers\Api\BaseController;
use App\Http\Helpers\ApiResponse as Api;
use App\Http\Requests\Owner\Room\RoomPriceComponentListRequest;
use App\Http\Requests\Owner\Room\RoomPriceComponentRequest;
use App\Http\Requests\Owner\Room\RoomPriceComponentSwitchRequest;
use App\Presenters\Mamipay\MamipayRoomPriceComponentPresenter;
use App\Repositories\Mamipay\MamipayRoomPriceComponentAdditionalRepository;
use App\Repositories\Mamipay\MamipayRoomPriceComponentRepository;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Exception;
use Illuminate\Support\Facades\DB;

/**
 * Class RoomPriceComponentController
 * @package App\Http\Controllers\Api\Owner\Room
 */
class RoomPriceComponentController extends BaseController
{
    protected $repository;
    protected $additionalRepo;

    /**
     * RoomPriceComponentController constructor.
     */
    public function __construct()
    {
        $this->repository = app()->make(MamipayRoomPriceComponentRepository::class);
        $this->additionalRepo = app()->make(MamipayRoomPriceComponentAdditionalRepository::class);
    }

    /**
     * @param RoomPriceComponentListRequest $request
     * @param $songId
     * @return JsonResponse
     */
    public function index(RoomPriceComponentListRequest $request, $songId): JsonResponse
    {
        $response = $this->makeDefaultResponse();

        try {
            $validator = $request->failedValidator;
            if ($validator && $validator->fails())
                throw new CustomErrorException(implode(", ",$validator->errors()->all()));

            // get data
            $room = $request->getRoom();
            $user = $this->user();

            // check ownership
            if (!$room->ownedBy($user))
                throw new CustomErrorException('Access Denied');

            // get data
            $lists = $this->repository->getListByRoomId($room->id);

            // map data response
            $mapLists = $this->mapListResponse($lists);

            // response data
            $response = [
                'data' => $mapLists
            ];

        } catch (CustomErrorException $e) {
            $response['meta']['message'] = $e->getMessage();
        } catch (Exception $e) {
            $response['meta']['message'] = $e->getMessage();
            Bugsnag::notifyException($e);
        }
        return Api::responseData($response);
    }

    /**
     * @param RoomPriceComponentRequest $request
     * @param $songId
     * @return JsonResponse
     */
    public function store(RoomPriceComponentRequest $request, $songId): JsonResponse
    {
        $response = $this->makeDefaultResponse();
        try {
            $validator = $request->failedValidator;
            if ($validator && $validator->fails())
                throw new CustomErrorException(implode(", ",$validator->errors()->all()));

            // get data
            $room   = $request->getRoom();
            $user   = $this->user();
            $label  = $this->getLabelAndName($request);
            $checkExistData = $this->checkExistData($request, $room);

            // check ownership
            if (!$room->ownedBy($user))
                throw new CustomErrorException('Access Denied');

            // check data is exist
            if ($checkExistData)
                throw new CustomErrorException('Data telah ada sebelum nya, silahkan update untuk merubah data');

            DB::beginTransaction();
            $data = null;
            // save data
            $data = $this->repository->create([
                'designer_id'   => optional($room)->id,
                'component'     => $request->type,
                'name'          => $label['name'] ?? null,
                'label'         => $label['label'] ?? null,
                'price'         => optional($request)->price ?? 0,
                'is_active'     => 1
            ]);

            // force update status active
            $this->repository->updateSwitchActive(optional($room)->id, 1, $request->type);

            if ($data && $request->type === MamipayRoomPriceComponentType::FINE) {
                $this->updateOrCreateComponentFine($data, $request);
            } elseif ($data && $request->type === MamipayRoomPriceComponentType::DP) {
                $this->updateOrCreateComponentDP($data, $request);
            }

            DB::commit();
            // transform data for response
            $transform = (new MamipayRoomPriceComponentPresenter('data'))->present($data);
            return Api::responseData(
                $transform,
                false,
                true,
                200,
                __('notification.room-price-component-response-message.created')
            );

        } catch (CustomErrorException $e) {
            $response['meta']['message'] = $e->getMessage();
            DB::rollBack();
        } catch (Exception $e) {
            $response['meta']['message'] = $e->getMessage();
            Bugsnag::notifyException($e);
            DB::rollBack();
        }
        return Api::responseData($response);
    }

    /**
     * @param RoomPriceComponentRequest $request
     * @param $priceComponentId
     * @return JsonResponse
     */
    public function update(RoomPriceComponentRequest $request, $priceComponentId): JsonResponse
    {
        $response = $this->makeDefaultResponse();
        try {
            $validator = $request->failedValidator;
            if ($validator && $validator->fails())
                throw new CustomErrorException(implode(", ",$validator->errors()->all()));

            // get data
            $data = $request->getData();
            $user = $this->user();
            $label = $this->getLabelAndName($request);

            // get room
            $room = optional($data)->room;
            if (!$room)
                throw new CustomErrorException('Kos tidak ditemukan');

            // check ownership
            if (!$room->ownedBy($user))
                throw new CustomErrorException('Access Denied');

            DB::beginTransaction();
            $update = $this->repository->update([
                'name'          => $label['name'] ?? null,
                'label'         => $label['label'] ?? null,
                'price'         => optional($request)->price,
                'is_active'     => 1
            ], $priceComponentId);

            if ($update && $data->component === MamipayRoomPriceComponentType::FINE) {
                $this->updateOrCreateComponentFine($data, $request);
            } elseif ($update && $data->component === MamipayRoomPriceComponentType::DP) {
                $this->updateOrCreateComponentDP($data, $request);
            }

            // refresh data after updated
            $data = $data->fresh();
            // transform data for response
            $transform = (new MamipayRoomPriceComponentPresenter('data'))->present($data);

            DB::commit();
            return Api::responseData(
                $transform,
                false,
                true,
                200,
                __('notification.room-price-component-response-message.created')
            );

        } catch (CustomErrorException $e) {
            $response['meta']['message'] = $e->getMessage();
            DB::rollBack();
        } catch (Exception $e) {
            $response['meta']['message'] = $e->getMessage();
            Bugsnag::notifyException($e);
            DB::rollBack();
        }
        return Api::responseData($response);
    }

    /**
     * @param $priceComponentId
     * @return JsonResponse
     */
    public function destroy($priceComponentId): JsonResponse
    {
        $response = $this->makeDefaultResponse();
        try {
            DB::beginTransaction();
            $data = $this->repository->find($priceComponentId);

            // get room
            $room = optional($data)->room;
            if (!$room)
                throw new CustomErrorException('Kos tidak ditemukan');

            // get data user
            $user = $this->user();

            // check ownership
            if (!$room->ownedBy($user))
                throw new CustomErrorException('Access Denied');

            // delete data
            $this->repository->delete($data->id);

            DB::commit();

            return Api::responseData(
                [],
                false,
                true,
                200,
                __('notification.room-price-component-response-message.deleted')
            );

        } catch (CustomErrorException $e) {
            $response['meta']['message'] = 'Data tidak ditemukan';
            DB::rollBack();
        } catch (ModelNotFoundException $e) {
            $response['meta']['message'] = 'Data tidak ditemukan';
            DB::rollBack();
        } catch (Exception $e) {
            $response['meta']['message'] = $e->getMessage();
            DB::rollBack();
            Bugsnag::notifyException($e);
        }
        return Api::responseData($response);
    }

    /**
     * @param RoomPriceComponentSwitchRequest $request
     * @param $songId
     * @param $type
     * @return JsonResponse
     */
    public function switch(RoomPriceComponentSwitchRequest $request, $songId, $type): JsonResponse
    {
        $response = $this->makeDefaultResponse();
        try {
            DB::beginTransaction();
            $validator = $request->failedValidator;
            if ($validator && $validator->fails())
                throw new CustomErrorException(implode(", ",$validator->errors()->all()));

            // get data
            $room   = $request->getRoom();
            $user   = $this->user();
            $status = $request->active;

            // check ownership
            if (!$room->ownedBy($user))
                throw new CustomErrorException('Access Denied');

            // switch data
            $this->repository->updateSwitchActive(optional($room)->id, $status, $type);

            $responseMessage = __('notification.room-price-component-response-message.switch_off');
            if ($status == 1) {
                $responseMessage = __('notification.room-price-component-response-message.switch_on');
            }

            DB::commit();

            return Api::responseData(
                [],
                false,
                true,
                200,
                $responseMessage
            );

        } catch (CustomErrorException $e) {
            $response['meta']['message'] = $e->getMessage();
            DB::rollBack();
        } catch (Exception $e) {
            $response['meta']['message'] = $e->getMessage();
            DB::rollBack();
            Bugsnag::notifyException($e);
        }
        return Api::responseData($response);
    }

    /**
     * Function to return default response
     * @return array
     */
    private function makeDefaultResponse()
    {
        return [
            'status' => false,
            'meta' => [
                'response_code' => 200,
                'message' => 'General Error'
            ]
        ];
    }

    /**
     * @param RoomPriceComponentRequest $request
     * @return array
     */
    private function getLabelAndName(RoomPriceComponentRequest $request): array
    {
        $data = [
            'name' => null,
            'label' => null
        ];
        switch ($request->type){
            case MamipayRoomPriceComponentType::ADDITIONAL:
                $data['name']   = optional($request)->name;
                $data['label']  = MamipayRoomPriceComponentType::MAP[MamipayRoomPriceComponentType::ADDITIONAL];
                break;
            case MamipayRoomPriceComponentType::FINE:
                $data['name']   = 'Denda';
                $data['label']  =  MamipayRoomPriceComponentType::MAP[MamipayRoomPriceComponentType::FINE];
                break;
            case MamipayRoomPriceComponentType::DEPOSIT:
                $data['name']   = 'Deposit';
                $data['label']  = MamipayRoomPriceComponentType::MAP[MamipayRoomPriceComponentType::DEPOSIT];
                break;
            case MamipayRoomPriceComponentType::DP:
                $data['name']   = 'DP ' . optional($request)->percentage . '%';
                $data['label']  = MamipayRoomPriceComponentType::MAP[MamipayRoomPriceComponentType::DP];
                break;
            default:
                break;
        }
        return $data;
    }

    /**
     * @param $data
     * @param $request
     */
    private function updateOrCreateComponentFine($data, $request)
    {
        $this->additionalRepo->updateOrCreate(
            [
                'mamipay_designer_price_component_id' => $data->id,
                'key' => MamipayRoomPriceComponentAdditional::KEY_FINE_MAXIMUM_LENGTH
            ],
            [
                'value' => optional($request)->fine_maximum_length
            ]
        );

        $this->additionalRepo->updateOrCreate(
            [
                'mamipay_designer_price_component_id' => $data->id,
                'key' => MamipayRoomPriceComponentAdditional::KEY_FINE_DURATION_TYPE
            ],
            [
                'value' => optional($request)->fine_duration_type
            ]
        );
    }

    /**
     * @param $data
     * @param $request
     */
    private function updateOrCreateComponentDP($data, $request)
    {
        $this->additionalRepo->updateOrCreate(
            [
                'mamipay_designer_price_component_id' => $data->id,
                'key' => MamipayRoomPriceComponentAdditional::KEY_PERCENTAGE
            ],
            [
                'value' => optional($request)->percentage
            ]
        );
    }

    private function checkExistData($request, $room): bool
    {
        if ($request->type !== MamipayRoomPriceComponentType::ADDITIONAL) {
            $data = $this->repository->where('designer_id', $room->id)
                                    ->where('component', $request->type)
                                    ->count();
            if ($data > 0)
                return true;
        }
        return false;
    }

    private function mapListResponse($lists)
    {
        $response = collect();
        $sort = 1;
        foreach (MamipayRoomPriceComponentType::KEYS as $type) {
            // get data by component
            $data   = $lists->where('component', $type);

            // get count active data
            $active = $data->where('is_active', 1)->count();

            $response->push([
                'type'          => $type,
                'label'         => MamipayRoomPriceComponentType::MAP[$type],
                'description'   => MamipayRoomPriceComponentType::MAP_DESC[$type],
                'active'        => $active !== 0 ? true : false,
                'sort'          => (int) $sort,
                'data'          => (new MamipayRoomPriceComponentPresenter('data'))->present($data)['data']
            ]);
            $sort++;
        }
        return $response;
    }
}