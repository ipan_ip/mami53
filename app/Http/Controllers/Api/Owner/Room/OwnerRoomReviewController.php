<?php

namespace App\Http\Controllers\Api\Owner\Room;

use App\Http\Controllers\Api\BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Helpers\ApiResponse;
use RuntimeException;
use App\Repositories\RoomRepository;
use App\Presenters\RoomPresenter;
use Illuminate\Http\JsonResponse;
use App\Http\Helpers\ApiErrorHelper;
use App\Entities\Api\ErrorCode;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Exception;

class OwnerRoomReviewController extends BaseController
{

    protected $roomRepository;

    public function __construct(RoomRepository $roomRepository)
    {
        parent::__construct();
        $this->roomRepository = $roomRepository;
    }
    
    public function index(Request $request): JsonResponse
    {
        $user = $this->user();

        if (empty($user) || $user->is_owner === 'false') {
            $response = ApiErrorHelper::generateErrorResponse(
                ErrorCode::INVALID_USER
            );

            return ApiResponse::responseData($response, false, false);
        }

        $validator = Validator::make($request->all(), [
            'limit' => 'required|integer',
            'offset' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return ApiResponse::responseData([
                'status' => false,
                'error' => [
                    'messages' => $validator->errors()->first()
                ]
            ]);
        }

        $rooms = $this->roomRepository->getRoomsWithReview($user->id);
        if (empty($rooms)) {
            throw new RuntimeException('Error getting room list return empty');
        }

        $transformedRooms = (new RoomPresenter(RoomPresenter::ROOM_REVIEW_SUMMARY))->present($rooms);

        return ApiResponse::responseData([
            'status' => true,
            'pagination' => [
                'current_page' => $rooms->currentPage(),
                'limit' => $request->limit,
                'has_more' => $rooms->hasMorePages(),
            ],
            'data' => $transformedRooms['data']
        ]);

    }
}