<?php

namespace App\Http\Controllers\Api\Owner\Input\Kost;

use App\Entities\Owner\ActivityType;
use App\Entities\Property\Property;
use App\Entities\Room\DataStage;
use App\Entities\Room\Element\Card\Group;
use App\Exceptions\Owner\ResourceNotOwnedException;
use App\Http\Controllers\Api\Owner\BaseController;
use App\Jobs\Owner\MarkActivity;
use App\Services\Owner\Kost\DataStageService;
use App\Services\Owner\Kost\InputService;
use App\Transformers\Owner\Data\Property\PropertyTransformer;
use App\Transformers\Owner\Input\Kost\RoomPhotoTransformer;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class RoomPhotoController extends BaseController
{
    /**
     * store new kost from Kost Photo Step
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Services\Owner\Kost\InputService $service
     * @param \App\Transformers\Owner\Input\Kost\RoomPhotoTransformer $transformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(
        Request $request,
        InputService $service,
        DataStageService $stageService,
        RoomPhotoTransformer $transformer
    ): JsonResponse {
        $validator = Validator::make(
            $request->all(),
            [
                'cover' => 'required|integer|min:1',
                'room_front' => 'required|array',
                'room_front.*' => 'required|integer',
                'inside_room' => 'required|array',
                'inside_room.*' => 'required|integer',
                'bathroom' => 'nullable|array',
                'bathroom.*' => 'nullable|integer',
                'other' => 'nullable|array',
                'other.*' => 'nullable|integer',
            ]
        );

        $validator->after(function ($validator) use ($request) {
            if (! in_array($request->input('cover'), (array) $request->input('inside_room'))) {
                $validator->errors()->add('cover', 'cover photo id must be included in inside_room parameter.');
            }
        });

        try {
            $validator->validate();
            $kostData = [
                'photo_id' => $request->input('cover'),
                'campaign_source' => $service->getCampaignSourceValue($request),
            ];

            $user = $this->user();
            $room = $service->createKost(
                $kostData,
                $user
            );

            $room = $service->saveCards(
                $room,
                $request->input('room_front'),
                Group::ROOM_FRONT
            );

            $room = $service->saveCards(
                $room,
                $request->input('inside_room'),
                Group::INSIDE_ROOM
            );

            if ($request->has('bathroom')) {
                $room = $service->saveCards(
                    $room,
                    (array) $request->input('bathroom'),
                    Group::BATHROOM
                );
            }

            if ($request->has('other')) {
                $room = $service->saveCards(
                    $room,
                    (array) $request->input('other'),
                    Group::OTHER
                );
            }

            $room = $service->setCoverPhoto($room, $request->input('cover'));

            $room->loadMissing('cards.photo');
            $stageService->registerDataStage($room, DataStage::STAGE_ROOM_PHOTO);
            MarkActivity::dispatch($user->id, ActivityType::TYPE_UPDATE_PROPERTY);

            return $this->response([
                'status' => true,
                'room' => $transformer->transform(
                    $room,
                    $service->filterCardsByGroup($room->cards, Group::ROOM_FRONT),
                    $service->filterCardsByGroup($room->cards, Group::INSIDE_ROOM),
                    $service->filterCardsByGroup($room->cards, Group::BATHROOM),
                    $service->filterCardsByGroup($room->cards, Group::OTHER)
                ),
            ]);
        } catch (ValidationException $exception) {
            return $this->response([], $exception, 100422);
        } catch (Exception $exception) {
            return $this->response([], $exception, 100400);
        }
    }

    /**
     * get Kost Data For Kost Photo Step
     *
     * @param integer $songId
     * @param \App\Services\Owner\Kost\InputService $service
     * @param \App\Transformers\Owner\Input\Kost\RoomPhotoTransformer $transformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function get(
        int $songId,
        InputService $service,
        RoomPhotoTransformer $transformer,
        PropertyTransformer $propertyTransformer
    ): JsonResponse {
        try {
            $room = $service->getData(
                $songId,
                $this->user(),
                ['owners', 'cards.photo', 'property']
            );

            return $this->response([
                'status' => true,
                'room' => $transformer->transform(
                    $room,
                    $service->filterCardsByGroup($room->cards, Group::ROOM_FRONT),
                    $service->filterCardsByGroup($room->cards, Group::INSIDE_ROOM),
                    $service->filterCardsByGroup($room->cards, Group::BATHROOM),
                    $service->filterCardsByGroup($room->cards, Group::OTHER)
                ),
                'property' => $room->property instanceof Property ?
                    $propertyTransformer->transform($room->property) :
                    null
            ]);
        } catch (ModelNotFoundException $exception) {
            return $this->response([], $exception, 100404);
        } catch (ResourceNotOwnedException $exception) {
            return $this->response([], $exception, 100403);
        }
    }

    /**
     * save kost data update on kost photo step
     *
     * @param integer $songId
     * @param \Illuminate\Http\Request $request
     * @param \App\Services\Owner\Kost\InputService $service
     * @param \App\Transformers\Owner\Input\Kost\RoomPhotoTransformer $transformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(
        int $songId,
        Request $request,
        InputService $service,
        DataStageService $stageService,
        RoomPhotoTransformer $transformer,
        PropertyTransformer $propertyTransformer
    ): JsonResponse {
        $validator = Validator::make(
            $request->all(),
            [
                'cover' => 'required|integer|min:1',
                'room_front' => 'required|array',
                'room_front.*' => 'required|integer',
                'inside_room' => 'required|array',
                'inside_room.*' => 'required|integer',
                'bathroom' => 'nullable|array',
                'bathroom.*' => 'nullable|integer',
                'other' => 'nullable|array',
                'other.*' => 'nullable|integer',
            ]
        );

        $validator->after(function ($validator) use ($request) {
            if (! in_array($request->input('cover'), $request->input('inside_room'))) {
                $validator->errors()->add('cover', 'cover photo id must be included in inside_room parameter.');
            }
        });

        try {
            $validator->validate();
            $user = $this->user();
            $room = $service->getData(
                $songId,
                $user,
                ['owners', 'cards.photo', 'property']
            );

            $updateParam = [];
            if (is_null($room->photo_id) || ! $room->isPremiumPhoto()) {
                $updateParam = ['photo_id' => $request->input('cover')];
            }

            $room = $service->updateKost(
                $room,
                $updateParam
            );

            $room = $service->saveCards(
                $room,
                $request->input('room_front'),
                Group::ROOM_FRONT
            );

            $room = $service->saveCards(
                $room,
                $request->input('inside_room'),
                Group::INSIDE_ROOM
            );

            $room = $service->saveCards(
                $room,
                (array) $request->input('bathroom'),
                Group::BATHROOM
            );

            $room = $service->saveCards(
                $room,
                (array) $request->input('other'),
                Group::OTHER
            );

            $room = $service->setCoverPhoto($room, $request->input('cover'));

            $room->loadMissing('cards.photo');

            $room = $service->updateKost(
                $room,
                [
                    'photo_count' => $room->cards->count()
                ]
            );

            $stageService->registerDataStage($room, DataStage::STAGE_ROOM_PHOTO);
            MarkActivity::dispatch($user->id, ActivityType::TYPE_UPDATE_PROPERTY);

            return $this->response([
                'status' => true,
                'room' => $transformer->transform(
                    $room,
                    $service->filterCardsByGroup($room->cards, Group::ROOM_FRONT),
                    $service->filterCardsByGroup($room->cards, Group::INSIDE_ROOM),
                    $service->filterCardsByGroup($room->cards, Group::BATHROOM),
                    $service->filterCardsByGroup($room->cards, Group::OTHER)
                ),
                'property' => $room->property instanceof Property ?
                    $propertyTransformer->transform($room->property) :
                    null
            ]);
        } catch (ValidationException $exception) {
            return $this->response([], $exception, 100422);
        } catch (ModelNotFoundException $exception) {
            return $this->response([], $exception, 100404);
        } catch (ResourceNotOwnedException $exception) {
            return $this->response([], $exception, 100403);
        }
    }
}
