<?php

namespace App\Http\Controllers\Api\Owner\Input\Kost;

use App\Entities\Mamipay\MamipayOwner;
use App\Entities\Room\RoomOwner;
use App\Exceptions\Owner\ResourceNotOwnedException;
use App\Http\Controllers\Api\Owner\BaseController;
use App\Services\Owner\Kost\DataStageService;
use App\Services\Owner\Kost\InputService;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;

class SubmitController extends BaseController
{
    /**
     * submit the kost draft data
     *
     * @param integer $songId
     * @param \App\Services\Owner\Kost\InputService $service
     * @param \App\Services\Owner\Kost\DataStageService $stageService
     * @return \Illuminate\Http\JsonResponse
     */
    public function submit(
        int $songId,
        InputService $service,
        DataStageService $stageService
    ): JsonResponse {
        try {
            $user = $this->user();
            $room = $service->getData(
                $songId,
                $user,
                ['data_stage', 'owners', 'creation_flag', 'address_note']
            );

            $roomOwner = $room->owners->first();
            if ($roomOwner->status != RoomOwner::ROOM_EARLY_EDIT_STATUS) {
                return $this->response([], new Exception("Kost status not draft."), 100111);
            }

            $isStageComplete = $stageService->isComplete($room);
            if (! $isStageComplete) {
                return $this->response([], new Exception("Stage not complete."), 100112);
            }

            $mamipayOwner = $user->mamipay_owner;
            $isMamipayOwner = (! is_null($mamipayOwner)) && ($mamipayOwner->status == MamipayOwner::STATUS_APPROVED);
            if (! $isMamipayOwner) {
                return $this->response([], new Exception("Mamipay profile not found."), 100113);
            }

            $service->submitFinalData($room, $user->id);

            return $this->response(['status' => true]);
        } catch (ModelNotFoundException $exception) {
            return $this->response([], $exception, 100404);
        } catch (ResourceNotOwnedException $exception) {
            return $this->response([], $exception, 100403);
        } catch (Exception $exception) {
            return $this->response([], $exception, 100400);
        }
    }

    public function markReady(
        int $songId,
        InputService $service
    ): JsonResponse {
        try {
            $user = $this->user();
            $room = $service->getData(
                $songId,
                $user,
                ['owners']
            );

            $service->markVerificationReady($room, $user->id);

            return $this->response([]);
        } catch (ResourceNotOwnedException $exception) {
            return $this->response([], $exception, 100403);
        } catch (ModelNotFoundException $exception) {
            return $this->response([], $exception, 100404);
        }
    }
}
