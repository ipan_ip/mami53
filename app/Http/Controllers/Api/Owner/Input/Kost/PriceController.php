<?php

namespace App\Http\Controllers\Api\Owner\Input\Kost;

use App\Entities\Booking\BookingDiscount;
use App\Entities\Owner\ActivityType;
use App\Entities\Property\Property;
use App\Entities\Room\DataStage;
use App\Entities\Room\Element\Price;
use App\Enums\Mamipay\MamipayRoomPriceComponentType;
use App\Exceptions\Owner\ResourceNotOwnedException;
use App\Http\Controllers\Api\Owner\BaseController;
use App\Jobs\Owner\MarkActivity;
use App\Services\Owner\Kost\DataStageService;
use App\Services\Owner\Kost\InputService;
use App\Services\Owner\Kost\PriceService;
use App\Transformers\Owner\Data\Property\PropertyTransformer;
use App\Transformers\Owner\Input\Kost\PriceTransformer;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class PriceController extends BaseController
{
    /**
     * submit the price data
     *
     * @param Request $request
     * @param \App\Services\Owner\Kost\InputService $service
     * @param \App\Services\Owner\Kost\PriceService $priceService
     * @param \App\Services\Owner\Kost\DataStageService $stageService
     * @param \App\Transformers\Owner\Input\Kost\PriceTransformer $transformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(
        Request $request,
        InputService $service,
        PriceService $priceService,
        DataStageService $stageService,
        PriceTransformer $transformer
    ): JsonResponse {
        $validator = Validator::make(
            $request->all(),
            [
                'price.daily'           => 'nullable|integer|min:10000|max:10000000',
                'price.weekly'          => 'nullable|integer|min:50000|max:50000000',
                'price.monthly'         => 'required|integer|min:50000|max:100000000',
                'price.yearly'          => 'nullable|integer|min:100000|max:100000000',
                'price.quarterly'       => 'nullable|integer|min:100000|max:100000000',
                'price.semiannually'    => 'nullable|integer|min:100000|max:100000000',
                'min_payment'           => 'nullable|integer|min:1',
                'additional_cost'       => 'array',
                'additional_cost.*.id'  => 'nullable|integer|min:1',
                'additional_cost.*.name' => 'required|string',
                'additional_cost.*.price' => 'required|integer|min:0',
                'additional_cost.*.is_active' => 'required|boolean',
                'deposit_fee'           => 'nullable',
                'deposit_fee.price'     => 'required_with:deposit_fee|integer|min:0',
                'deposit_fee.is_active' => 'required_with:deposit_fee|boolean',
                'down_payment'          => 'nullable',
                'down_payment.percentage' => 'required_with:down_payment|integer|in:10,20,30,40,50',
                'down_payment.is_active' => 'required_with:down_payment|boolean',
                'fine'                  => 'nullable',
                'fine.price'            => 'required_with:fine|integer|min:0',
                'fine.duration_type'    => 'required_with:fine|string|in:day,week,month',
                'fine.length'           => 'required_with:fine|integer|min:1',
                'fine.is_active'        => 'required_with:fine|boolean',
                'price_remark'          => 'nullable|string'
            ]
        );

        try {
            $validator->validate();
            $priceData = $this->parsePriceDataFromRequest($request);
            $user = $this->user();
            $designerPriceData = array_merge($priceData['prices'], ['price_remark' => $priceData['price_remark']]);

            $room = $service->createKost(
                $designerPriceData,
                $user
            );

            $room->syncPaymentTag($priceData['min_payment']);

            $priceService->saveAdditionalCostData($room, $priceData['additional_cost']);
            $priceService->savePriceComponent($room, $priceData['deposit_fee'], MamipayRoomPriceComponentType::DEPOSIT);
            $priceService->savePriceComponent($room, $priceData['down_payment'], MamipayRoomPriceComponentType::DP);
            $priceService->savePriceComponent($room, $priceData['fine'], MamipayRoomPriceComponentType::FINE);

            $room->loadMissing('designer_payment_tag');
            $room->refresh();

            $stageService->registerDataStage($room, DataStage::STAGE_PRICE);
            MarkActivity::dispatch($user->id, ActivityType::TYPE_UPDATE_PROPERTY);

            return $this->response([
                'status' => true,
                'room' => $transformer->transform($room),
            ]);
        } catch (ValidationException $exception) {
            return $this->response([], $exception, 100422);
        } catch (Exception $exception) {
            return $this->response([], $exception, 100400);
        }
    }

    /**
     * update the price data
     *
     * @param integer $songId
     * @param Request $request
     * @param \App\Services\Owner\Kost\InputService $service
     * @param \App\Services\Owner\Kost\PriceService $priceService
     * @param \App\Services\Owner\Kost\DataStageService $stageService
     * @param \App\Transformers\Owner\Input\Kost\PriceTransformer $transformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(
        int $songId,
        Request $request,
        InputService $service,
        PriceService $priceService,
        DataStageService $stageService,
        PriceTransformer $transformer,
        PropertyTransformer $propertyTransformer
    ): JsonResponse {
        $validator = Validator::make(
            $request->all(),
            [
                'price.daily'           => 'nullable|integer|min:10000|max:10000000',
                'price.weekly'          => 'nullable|integer|min:50000|max:50000000',
                'price.monthly'         => 'required|integer|min:50000|max:100000000',
                'price.yearly'          => 'nullable|integer|min:100000|max:100000000',
                'price.quarterly'       => 'nullable|integer|min:100000|max:100000000',
                'price.semiannually'    => 'nullable|integer|min:100000|max:100000000',
                'min_payment'           => 'nullable|integer|min:1',
                'additional_cost'       => 'array',
                'additional_cost.*.id'  => 'nullable|integer|min:1',
                'additional_cost.*.name' => 'required|string',
                'additional_cost.*.price' => 'required|integer|min:0',
                'additional_cost.*.is_active' => 'required|boolean',
                'deposit_fee'           => 'nullable',
                'deposit_fee.price'     => 'required_with:deposit_fee|integer|min:0',
                'deposit_fee.is_active' => 'required_with:deposit_fee|boolean',
                'down_payment'          => 'nullable',
                'down_payment.percentage' => 'required_with:down_payment|integer|in:10,20,30,40,50',
                'down_payment.is_active' => 'required_with:down_payment|boolean',
                'fine'                  => 'nullable',
                'fine.price'            => 'required_with:fine|integer|min:0',
                'fine.duration_type'    => 'required_with:fine|string|in:day,week,month',
                'fine.length'           => 'required_with:fine|integer|min:1',
                'fine.is_active'        => 'required_with:fine|boolean',
                'price_remark'          => 'nullable|string'
            ]
        );

        $user = $this->user();
        try {
            $room = $service->getData(
                $songId,
                $user,
                [
                    'owners',
                    'designer_payment_tag',
                    'active_discounts',
                    'discounts',
                    'mamipay_price_components.price_component_additionals',
                    'property'
                ]
            );
        } catch (ModelNotFoundException $exception) {
            return $this->response([], $exception, 100404);
        } catch (ResourceNotOwnedException $exception) {
            return $this->response([], $exception, 100403);
        }

        $priceData = $this->parsePriceDataFromRequest($request);

        $additionalCost = $request->additional_cost;

        $validator->after(function ($validator) use ($room, $priceData, $additionalCost, $priceService) {
            $inputToEnumMapping = [
                'price_daily' => Price::STRING_DAILY,
                'price_weekly' => Price::STRING_WEEKLY,
                'price_monthly' => Price::STRING_MONTHLY,
                'price_yearly' => Price::STRING_ANNUALLY,
                'price_quarterly' => Price::STRING_QUARTERLY,
                'price_semiannually' => Price::STRING_SEMIANNUALLY
            ];

            if (!empty($additionalCost)) {
                $priceIds = [];
                foreach ($additionalCost as $price){
                    if(isset($price['id']) && $price['id'] > 0){
                        $priceIds[]=$price['id'];
                    }
                }

                if(!empty($priceIds)){
                    $room->loadMissing('mamipay_price_components');
                    $mamipayPriceComponent = $room->mamipay_price_components;
                    $mamipayAdditionalCostIds = [];
                    if($mamipayPriceComponent->count())
                    {
                        $mamipayAdditionalCostIds = $mamipayPriceComponent->filter(function($price){
                            return $price->component == MamipayRoomPriceComponentType::ADDITIONAL;
                        })->pluck('id')->toArray();
                    }
                    $priceIds = collect($priceIds)->diff($mamipayAdditionalCostIds)->first();
                    if($priceIds){                        
                        $validator->errors()->add(
                            'additional_cost',
                            "additional_cost Ids must be valid"
                        );
                    }
                }
            }

            $flashSale = $room->getFlashSaleRentType();
            $originalPrice = $room->getRealPrices();

            collect($priceData['prices'])->filter(
                function ($price) {
                    return $price > 0;
                }
            )->each(
                function ($price, $inputName) use ($validator, $room, $inputToEnumMapping, $flashSale, $originalPrice) {
                    if (
                        ! BookingDiscount::validateListingPrice(
                            $price,
                            $room,
                            $inputToEnumMapping[$inputName]
                        )
                    ) {
                        $validator->errors()->add(
                            'price',
                            Lang::get(
                                'api.owner.input.kost.validation.price.lower-than-discount.' . $inputToEnumMapping[$inputName]
                            )
                        );
                    }

                    if (
                        $flashSale[$inputToEnumMapping[$inputName]] &&
                        isset($originalPrice[$inputName]) &&
                        $price != $originalPrice[$inputName]
                    ) {
                        $validator->errors()->add(
                            $inputToEnumMapping[$inputName],
                            Lang::get(
                                'api.owner.input.kost.validation.price.flash-sale.' . $inputToEnumMapping[$inputName]
                            )
                        );
                    }
                }
            );
        });

        try {
            $validator->validate();

            $designerPriceData = array_merge($priceData['prices'], ['price_remark' => $priceData['price_remark']]);

            $room = $service->updateKost(
                $room,
                $designerPriceData
            );

            $room->syncPaymentTag($priceData['min_payment']);

            if (count($room->getAllActiveDiscounts()) > 0) {
                $room->recalculateAllActiveDiscounts();
            }

            $priceService->saveAdditionalCostData($room, $priceData['additional_cost']);
            $priceService->savePriceComponent($room, $priceData['deposit_fee'], MamipayRoomPriceComponentType::DEPOSIT);
            $priceService->savePriceComponent($room, $priceData['down_payment'], MamipayRoomPriceComponentType::DP);
            $priceService->savePriceComponent($room, $priceData['fine'], MamipayRoomPriceComponentType::FINE);

            $room->refresh();

            $stageService->registerDataStage($room, DataStage::STAGE_PRICE);
            MarkActivity::dispatch($user->id, ActivityType::TYPE_UPDATE_PROPERTY);

            return $this->response([
                'status' => true,
                'room' => $transformer->transform($room),
                'property' => $room->property instanceof Property ?
                    $propertyTransformer->transform($room->property) :
                    null
            ]);
        } catch (ValidationException $exception) {
            return $this->response([], $exception, 100422);
        } catch (ModelNotFoundException $exception) {
            return $this->response([], $exception, 100404);
        } catch (ResourceNotOwnedException $exception) {
            return $this->response([], $exception, 100403);
        }
    }

    /**
     * get kos price data for prefilled form
     *
     * @param integer $songId
     * @param \App\Services\Owner\Kost\InputService $service
     * @param \App\Transformers\Owner\Input\Kost\PriceTransformer $transformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function get(
        int $songId,
        InputService $service,
        PriceTransformer $transformer,
        PropertyTransformer $propertyTransformer
    ): JsonResponse {
        try {
            $room = $service->getData(
                $songId,
                $this->user(),
                [
                    'active_discounts',
                    'discounts',
                    'designer_payment_tag',
                    'mamipay_price_components.price_component_additionals',
                    'owners',
                    'property'
                ]
            );

            return $this->response([
                'status' => true,
                'room' => $transformer->transform(
                    $room
                ),
                'property' => $room->property instanceof Property ?
                    $propertyTransformer->transform($room->property) :
                    null
            ]);
        } catch (ModelNotFoundException $exception) {
            return $this->response([], $exception, 100404);
        } catch (ResourceNotOwnedException $exception) {
            return $this->response([], $exception, 100403);
        }
    }

    /**
     * parse request on price step to create kost
     *
     * @param Request $request
     * @return array
     */
    private function parsePriceDataFromRequest(Request $request): array
    {
        $depositFee = is_null($request->input('deposit_fee')) ? null : [
            'price' => $request->input('deposit_fee.price'),
            'is_active' => $request->input('deposit_fee.is_active'),
        ];

        $downPayment = is_null($request->input('down_payment')) ? null : [
            'percentage' => $request->input('down_payment.percentage'),
            'is_active' => $request->input('down_payment.is_active'),
        ];

        $fine = is_null($request->input('fine')) ? null : [
            'price' => $request->input('fine.price'),
            'duration_type' => $request->input('fine.duration_type'),
            'length' => $request->input('fine.length'),
            'is_active' => $request->input('fine.is_active'),
        ];

        return [
            'prices' => [
                'price_daily'           => (int) $request->input('price.daily'),
                'price_weekly'          => (int) $request->input('price.weekly'),
                'price_monthly'         => (int) $request->input('price.monthly'),
                'price_yearly'          => (int) $request->input('price.yearly'),
                'price_quarterly'       => (int) $request->input('price.quarterly'),
                'price_semiannually'    => (int) $request->input('price.semiannually'),
            ],
            'min_payment' => (int) $request->input('min_payment'),
            'additional_cost' => (array) $request->input('additional_cost'),
            'deposit_fee' => $depositFee,
            'down_payment' => $downPayment,
            'fine' => $fine,
            'price_remark' => $request->input('price_remark')
        ];
    }
}
