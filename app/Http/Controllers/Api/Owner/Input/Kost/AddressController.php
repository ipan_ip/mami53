<?php

namespace App\Http\Controllers\Api\Owner\Input\Kost;

use App\Entities\Owner\ActivityType;
use App\Entities\Property\Property;
use App\Entities\Room\DataStage;
use App\Exceptions\Owner\ResourceNotOwnedException;
use App\Http\Controllers\Api\Owner\BaseController;
use App\Http\Helpers\ApiHelper;
use App\Jobs\Owner\MarkActivity;
use App\Libraries\AreaFormatter;
use App\Services\Owner\Kost\DataStageService;
use App\Services\Owner\Kost\InputService;
use App\Transformers\Owner\Data\Property\PropertyTransformer;
use App\Transformers\Owner\Input\Kost\AddressTransformer;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class AddressController extends BaseController
{
    /**
     * submit the address data
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Services\Owner\Kost\InputService $service
     * @param \App\Services\Owner\Kost\DataStageService $stageService
     * @param \App\Transformers\Owner\Input\Kost\AddressTransformer $transformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(
        Request $request,
        InputService $service,
        DataStageService $stageService,
        AddressTransformer $transformer
    ): JsonResponse {
        $validator = Validator::make(
            $request->all(),
            [
                'address' => 'required|string|max:250',
                'latitude' => 'required|numeric|CoordinateRule',
                'longitude' => 'required|numeric|CoordinateRule',
                'province' => 'required|string',
                'city' => 'required|string',
                'subdistrict' => 'required|string',
                'note' => 'nullable|string|max:300',
            ]
        );

        try {
            $validator->validate();

            if (
                Request('latitude') === 0
                && Request('longitude') === 0
            ) {
                throw ValidationException::withMessages([
                    'longlat' => 'Both Longitude and Latitude cannot be 0'
                ]);
            }

            $addressData = $this->parseAddressDataFromRequest($request);
            $addressData['campaign_source'] = $service->getCampaignSourceValue($request);

            $user = $this->user();
            $room = $service->createKost(
                $addressData,
                $user
            );

            $addressNote = $service->saveAddressNote(
                $room->id,
                AreaFormatter::provinceFormatter(
                    $request->input('province')
                ),
                $addressData['area_city'],
                $addressData['area_subdistrict'],
                ApiHelper::removeEmoji($request->input('note'))
            );

            $room->setRelation('address_note', $addressNote);

            $stageService->registerDataStage($room, DataStage::STAGE_ADDRESS);
            MarkActivity::dispatch($user->id, ActivityType::TYPE_UPDATE_PROPERTY);

            return $this->response([
                'status' => true,
                'room' => $transformer->transform($room),
            ]);
        } catch (ValidationException $exception) {
            return $this->response([], $exception, 100422);
        } catch (Exception $exception) {
            return $this->response([], $exception, 100400);
        }
    }

    /**
     * get kos address data for prefilled form
     *
     * @param integer $songId
     * @param \App\Services\Owner\Kost\InputService $service
     * @param \App\Transformers\Owner\Input\Kost\AddressTransformer $transformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function get(
        int $songId,
        InputService $service,
        AddressTransformer $transformer,
        PropertyTransformer $propertyTransformer
    ): JsonResponse {
        try {
            $room = $service->getData(
                $songId,
                $this->user(),
                [
                    'owners',
                    'address_note' => function ($query) {
                        $query->with(['province', 'city', 'subdistrict']);
                    },
                    'property'
                ]
            );

            return $this->response([
                'status' => true,
                'room' => $transformer->transform($room),
                'property' => $room->property instanceof Property ?
                    $propertyTransformer->transform($room->property) :
                    null
            ]);
        } catch (ModelNotFoundException $exception) {
            return $this->response([], $exception, 100404);
        } catch (ResourceNotOwnedException $exception) {
            return $this->response([], $exception, 100403);
        }
    }

    /**
     * Edit the address Data
     *
     * @param integer $songId
     * @param \Illuminate\Http\Request $request
     * @param \App\Services\Owner\Kost\InputService $service
     * @param \App\Services\Owner\Kost\DataStageService $stageService
     * @param \App\Transformers\Owner\Input\Kost\AddressTransformer $transformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(
        int $songId,
        Request $request,
        InputService $service,
        DataStageService $stageService,
        AddressTransformer $transformer,
        PropertyTransformer $propertyTransformer
    ): JsonResponse {
        $validator = Validator::make(
            $request->all(),
            [
                'address' => 'required|string|max:250',
                'latitude' => 'required|numeric|CoordinateRule',
                'longitude' => 'required|numeric|CoordinateRule',
                'province' => 'required|string',
                'city' => 'required|string',
                'subdistrict' => 'required|string',
                'note' => 'nullable|string|max:300',
            ]
        );

        try {
            $validator->validate();

            if (
                Request('latitude') === 0
                && Request('longitude') === 0
            ) {
                throw ValidationException::withMessages([
                    'longlat' => 'Both Longitude and Latitude cannot be 0'
                ]);
            }

            $user = $this->user();
            $addressData = $this->parseAddressDataFromRequest($request);
            $room = $service->getData(
                $songId,
                $user,
                [
                    'owners',
                    'address_note' => function ($query) {
                        $query->with(['province', 'city', 'subdistrict']);
                    },
                    'property'
                ]
            );
            if ($room->property instanceof Property) {
                $addressData['name'] = $service->getNamePattern(
                    $room->property,
                    $room->unit_type,
                    $addressData['area_subdistrict'],
                    $addressData['area_city']
                );
            }

            $room = $service->updateKost(
                $room,
                $addressData
            );
            $addressNote = $service->saveAddressNote(
                $room->id,
                AreaFormatter::provinceFormatter(
                    $request->input('province')
                ),
                $addressData['area_city'],
                $addressData['area_subdistrict'],
                ApiHelper::removeEmoji($request->input('note'))
            );

            $room->setRelation('address_note', $addressNote);

            $stageService->registerDataStage($room, DataStage::STAGE_ADDRESS);
            MarkActivity::dispatch($user->id, ActivityType::TYPE_UPDATE_PROPERTY);

            return $this->response([
                'status' => true,
                'room' => $transformer->transform($room),
                'property' => $room->property instanceof Property ?
                    $propertyTransformer->transform($room->property) :
                    null
            ]);
        } catch (ModelNotFoundException $exception) {
            return $this->response([], $exception, 100404);
        } catch (ValidationException $exception) {
            return $this->response([], $exception, 100422);
        } catch (ResourceNotOwnedException $exception) {
            return $this->response([], $exception, 100403);
        }
    }

    /**
     * parse request on address step to create kost
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function parseAddressDataFromRequest(Request $request): array
    {
        $cleanCity = trim(
            ApiHelper::removeEmoji(
                $request->input('city')
            )
        );

        return [
            'address' => trim(
                ApiHelper::removeEmoji(
                    $request->input('address')
                )
            ),
            'longitude' => $request->input('longitude'),
            'latitude' => $request->input('latitude'),
            'area_city' => $cleanCity,
            'area_subdistrict' => trim(
                ApiHelper::removeEmoji(
                    $request->input('subdistrict')
                )
            ),
            'area_big' => AreaFormatter::format($cleanCity),
            'guide' => ApiHelper::removeEmoji(
                $request->input('note')
            )
        ];
    }
}
