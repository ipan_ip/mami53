<?php

namespace App\Http\Controllers\Api\Owner\Input\Kost;

use App\Entities\Owner\ActivityType;
use App\Entities\Property\Property;
use App\Entities\Room\DataStage;
use App\Entities\Room\Element\Card\Group;
use App\Exceptions\Owner\ResourceNotOwnedException;
use App\Http\Controllers\Api\Owner\BaseController;
use App\Jobs\Owner\MarkActivity;
use App\Services\Owner\Kost\DataStageService;
use App\Services\Owner\Kost\InputService;
use App\Transformers\Owner\Data\Property\PropertyTransformer;
use App\Transformers\Owner\Input\Kost\KostPhotoTransformer;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class KostPhotoController extends BaseController
{
    /**
     * store new kost from Kost Photo Step
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Services\Owner\Kost\InputService $service
     * @param \App\Services\Owner\Kost\DataStageService $stageService
     * @param \App\Transformers\Owner\Input\Kost\KostPhotoTransformer $transformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(
        Request $request,
        InputService $service,
        DataStageService $stageService,
        KostPhotoTransformer $transformer
    ): JsonResponse {
        $validator = Validator::make(
            $request->all(),
            [
                'building_front' => 'required|array',
                'building_front.*' => 'required|integer',
                'inside_building' => 'required|array',
                'inside_building.*' => 'required|integer',
                'roadview_building' => 'required|array',
                'roadview_building.*' => 'required|integer',
            ]
        );

        try {
            $validator->validate();
            $kostData = [
                'campaign_source' => $service->getCampaignSourceValue($request),
            ];
            $user = $this->user();
            $room = $service->createKost(
                $kostData,
                $user
            );

            $room = $service->saveCards(
                $room,
                $request->input('building_front'),
                Group::BUILDING_FRONT
            );

            $room = $service->saveCards(
                $room,
                $request->input('inside_building'),
                Group::INSIDE_BUILDING
            );

            $room = $service->saveCards(
                $room,
                $request->input('roadview_building'),
                Group::ROADVIEW_BUILDING
            );

            $room->loadMissing('cards.photo');
            $stageService->registerDataStage($room, DataStage::STAGE_KOS_PHOTO);
            MarkActivity::dispatch($user->id, ActivityType::TYPE_UPDATE_PROPERTY);

            return $this->response([
                'status' => true,
                'room' => $transformer->transform(
                    $room,
                    $service->filterCardsByGroup($room->cards, Group::BUILDING_FRONT),
                    $service->filterCardsByGroup($room->cards, Group::INSIDE_BUILDING),
                    $service->filterCardsByGroup($room->cards, Group::ROADVIEW_BUILDING)
                ),
            ]);
        } catch (ValidationException $exception) {
            return $this->response([], $exception, 100422);
        } catch (Exception $exception) {
            return $this->response([], $exception, 100400);
        }
    }

    /**
     * get Kost Data For Kost Photo Step
     *
     * @param integer $songId
     * @param \App\Services\Owner\Kost\InputService $service
     * @param \App\Transformers\Owner\Input\Kost\KostPhotoTransformer $transformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function get(
        int $songId,
        InputService $service,
        KostPhotoTransformer $transformer,
        PropertyTransformer $propertyTransformer
    ): JsonResponse {
        try {
            $room = $service->getData(
                $songId,
                $this->user(),
                ['owners', 'cards.photo', 'property']
            );

            return $this->response([
                'status' => true,
                'room' => $transformer->transform(
                    $room,
                    $service->filterCardsByGroup($room->cards, Group::BUILDING_FRONT),
                    $service->filterCardsByGroup($room->cards, Group::INSIDE_BUILDING),
                    $service->filterCardsByGroup($room->cards, Group::ROADVIEW_BUILDING)
                ),
                'property' => $room->property instanceof Property ?
                    $propertyTransformer->transform($room->property) :
                    null
            ]);
        } catch (ModelNotFoundException $exception) {
            return $this->response([], $exception, 100404);
        } catch (ResourceNotOwnedException $exception) {
            return $this->response([], $exception, 100403);
        }
    }

    /**
     * save kost data update on kost photo step
     *
     * @param integer $songId
     * @param \Illuminate\Http\Request $request
     * @param \App\Services\Owner\Kost\InputService $service
     * @param \App\Services\Owner\Kost\DataStageService $stageService
     * @param \App\Transformers\Owner\Input\Kost\KostPhotoTransformer $transformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(
        int $songId,
        Request $request,
        InputService $service,
        DataStageService $stageService,
        KostPhotoTransformer $transformer,
        PropertyTransformer $propertyTransformer
    ): JsonResponse {
        $validator = Validator::make(
            $request->all(),
            [
                'building_front' => 'required|array',
                'building_front.*' => 'required|integer',
                'inside_building' => 'required|array',
                'inside_building.*' => 'required|integer',
                'roadview_building' => 'required|array',
                'roadview_building.*' => 'required|integer',
            ]
        );

        try {
            $validator->validate();
            $user = $this->user();
            // check kost photo data list here, if cover/main group are changed
            $room = $service->getData(
                $songId,
                $user,
                ['owners', 'cards']
            );

            $room = $service->saveCards(
                $room,
                $request->input('building_front'),
                Group::BUILDING_FRONT
            );

            $room = $service->saveCards(
                $room,
                $request->input('inside_building'),
                Group::INSIDE_BUILDING
            );

            $room = $service->saveCards(
                $room,
                $request->input('roadview_building'),
                Group::ROADVIEW_BUILDING
            );

            $room->loadMissing('cards.photo');

            $room = $service->updateKost(
                $room,
                [
                    'photo_count' => $room->cards->count()
                ]
            );

            $stageService->registerDataStage($room, DataStage::STAGE_KOS_PHOTO);
            MarkActivity::dispatch($user->id, ActivityType::TYPE_UPDATE_PROPERTY);

            return $this->response([
                'status' => true,
                'room' => $transformer->transform(
                    $room,
                    $service->filterCardsByGroup($room->cards, Group::BUILDING_FRONT),
                    $service->filterCardsByGroup($room->cards, Group::INSIDE_BUILDING),
                    $service->filterCardsByGroup($room->cards, Group::ROADVIEW_BUILDING)
                ),
                'property' => $room->property instanceof Property ?
                    $propertyTransformer->transform($room->property) :
                    null
            ]);
        } catch (ValidationException $exception) {
            return $this->response([], $exception, 100422);
        } catch (ModelNotFoundException $exception) {
            return $this->response([], $exception, 100404);
        } catch (ResourceNotOwnedException $exception) {
            return $this->response([], $exception, 100403);
        }
    }
}
