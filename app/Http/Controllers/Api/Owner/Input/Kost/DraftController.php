<?php

namespace App\Http\Controllers\Api\Owner\Input\Kost;

use App\Entities\Room\Room;
use App\Exceptions\Owner\ResourceNotFoundException;
use App\Exceptions\Owner\ResourceNotOwnedException;
use App\Http\Controllers\Api\Owner\BaseController;
use App\Services\Owner\Kost\DataStageService;
use App\Transformers\Owner\Data\Kost\DataStageTransformer;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Lang;

class DraftController extends BaseController
{
    private $transformer;

    public function __construct(DataStageTransformer $transformer)
    {
        parent::__construct();
        
        $this->transformer = $transformer;
    }

    /**
     * retrieve the draft
     *
     * @param integer $songId
     * @return \Illuminate\Http\JsonResponse
     */
    public function showStagesById(
        DataStageService $service,
        int $songId
    ): JsonResponse {
        $room = Room::with(['data_stage', 'owners', 'creation_flag', 'address_note'])->where('song_id', $songId)->first();

        if (!$room instanceof Room) {
            return $this->response(
                [],
                new ResourceNotFoundException(
                    Lang::get(
                        'api.owner.exception.not-owned',
                        [
                            'resource' => 'Kost'
                        ]
                    ),
                    404
                ),
                100404
            );
        }

        $user = $this->user();
        $userAsOwner = $room->owners->where('user_id', optional($user)->id)->first();

        if (is_null($userAsOwner)) {
            return $this->response(
                [],
                new ResourceNotOwnedException(
                    Lang::get(
                        'api.owner.exception.not-owned',
                        [
                            'user' => 'Anda',
                            'resource' => 'Kost'
                        ]
                    ),
                    403
                ),
                100403
            );
        }

        return $this->response([
            'stages' => $this->transformer->transform($service->getInputStages($room), $room->song_id),
            'is_active_photo_booking' => $room->isPremiumPhoto(),
            'duplicate_from' => $room->duplicate_from,
            'creation_flag' => optional($room->creation_flag)->create_from
        ]);
    }

    /**
     * get default stages
     *
     * @param \App\Services\Owner\Kost\DataStageService $service
     * @return \Illuminate\Http\JsonResponse
     */
    public function showDefaultStages(DataStageService $service): JsonResponse
    {
        return $this->response([
            'stages' => $this->transformer->transform($service->getInputStages(null), null),
            'is_active_photo_booking' => false
        ]);
    }
}
