<?php

namespace App\Http\Controllers\Api\Owner\Input\Kost;

use App\Entities\Owner\ActivityType;
use App\Entities\Property\Property;
use App\Entities\Room\DataStage;
use App\Entities\Room\Room;
use App\Exceptions\Owner\ResourceNotOwnedException;
use App\Http\Controllers\Api\Owner\BaseController;
use App\Jobs\Owner\MarkActivity;
use App\Repositories\Room\RoomUnitRepository;
use App\Services\Owner\Kost\DataStageService;
use App\Services\Owner\Kost\InputService;
use App\Transformers\Owner\Data\Property\PropertyTransformer;
use App\Transformers\Owner\Input\Kost\RoomAllotmentTransformer;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class RoomAllotmentController extends BaseController
{
    protected $validationMessages = [
        'room_count.lte' => 'Jumlah kamar tidak bisa lebih dari ' . Room::MAX_ROOM_COUNT,
        'size.regex' => 'Format size kamar tidak sesuai',
    ];

    /**
     * submit the room data
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Services\Owner\Kost\InputService $service
     * @param \App\Services\Owner\Kost\DataStageService $stageService
     * @param \App\Transformers\Owner\Input\Kost\RoomAllotmentTransformer $transformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(
        Request $request,
        InputService $service,
        DataStageService $stageService,
        RoomUnitRepository $roomUnitRepo,
        RoomAllotmentTransformer $transformer
    ): JsonResponse {
        $validator = Validator::make(
            $request->all(),
            [
                'size'              => 'required|string|regex:/^\d{1,2}(\.\d?)?x\d{1,2}(\.\d?)?$/',
                'room_count'        => 'required|integer|min:1|lte:' . Room::MAX_ROOM_COUNT,
                'room_available'    => 'required|integer|min:0',
            ],
            $this->validationMessages
        );

        $validator->after(function ($validator) use ($request) {
            if ($request->room_count < $request->room_available) {
                $validator->errors()->add(
                    'room_available',
                    'Jumlah kamar tersedia tidak boleh melebihi jumlah kamar total'
                );
            }
        });

        try {
            $validator->validate();
        } catch (ValidationException $exception) {
            return $this->response([], $exception, 100422);
        }

        $roomData = $this->parseRoomDataFromRequest($request);

        $user = $this->user();
        $room = $service->createKost($roomData, $user);
        $roomUnitRepo->backFillRoomUnit($room);

        $stageService->registerDataStage($room, DataStage::STAGE_ROOM_ALLOTMENT);
        MarkActivity::dispatch($user->id, ActivityType::TYPE_UPDATE_PROPERTY);

        return $this->response([
            'status' => true,
            'room' => $transformer->transform($room),
        ]);
    }

    /**
     * update the room data
     *
     * @param integer $songId
     * @param \Illuminate\Http\Request $request
     * @param \App\Services\Owner\Kost\InputService $service
     * @param \App\Services\Owner\Kost\DataStageService $stageService
     * @param \App\Transformers\Owner\Input\Kost\RoomAllotmentTransformer $transformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(
        int $songId,
        Request $request,
        InputService $service,
        DataStageService $stageService,
        RoomUnitRepository $roomUnitRepo,
        RoomAllotmentTransformer $transformer,
        PropertyTransformer $propertyTransformer
    ): JsonResponse {
        $validator = Validator::make(
            $request->all(),
            [
                'size'              => 'required|string|regex:/^\d{1,2}(\.\d?)?x\d{1,2}(\.\d?)?$/',
                'room_count'        => 'required|integer|min:1|lte:' . Room::MAX_ROOM_COUNT,
                'room_available'    => 'required|integer|min:0',
            ],
            $this->validationMessages
        );

        $validator->after(function ($validator) use ($request) {
            if ($request->room_count < $request->room_available) {
                $validator->errors()->add(
                    'room_available',
                    'Jumlah kamar tersedia tidak boleh melebihi jumlah kamar total'
                );
            }
        });

        try {
            $validator->validate();
            $roomData = $this->parseRoomDataFromRequest($request);
            $user = $this->user();
            $room = $service->updateKost(
                $service->getData($songId, $user, ['owners', 'room_unit', 'property']),
                $roomData
            );
            $room->refresh();

            if ($room->room_unit->isEmpty()) {
                $roomUnitRepo->backFillRoomUnit($room);
            }
            
            $stageService->registerDataStage($room, DataStage::STAGE_ROOM_ALLOTMENT);
            MarkActivity::dispatch($user->id, ActivityType::TYPE_UPDATE_PROPERTY);

            return $this->response([
                'status' => true,
                'room' => $transformer->transform($room),
                'property' => $room->property instanceof Property ?
                    $propertyTransformer->transform($room->property) :
                    null
            ]);
        } catch (ValidationException $exception) {
            return $this->response([], $exception, 100422);
        } catch (ModelNotFoundException $exception) {
            return $this->response([], $exception, 100404);
        } catch (ResourceNotOwnedException $exception) {
            return $this->response([], $exception, 100403);
        }
    }

    /**
     * get kos room data for prefilled form
     *
     * @param integer $songId
     * @param \App\Services\Owner\Kost\InputService $service
     * @param \App\Transformers\Owner\Input\Kost\RoomTransformer $transformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function get(
        int $songId,
        InputService $service,
        RoomAllotmentTransformer $transformer,
        PropertyTransformer $propertyTransformer
    ): JsonResponse {
        try {
            $room = $service->getData(
                $songId,
                $this->user(),
                ['owners', 'room_unit', 'property']
            );
            return $this->response([
                'status' => true,
                'room' => $transformer->transform($room),
                'property' => $room->property instanceof Property ?
                    $propertyTransformer->transform($room->property) :
                    null
            ]);
        } catch (ModelNotFoundException $exception) {
            return $this->response([], $exception, 100404);
        } catch (ResourceNotOwnedException $exception) {
            return $this->response([], $exception, 100403);
        }
    }

    /**
     * parse request on room step to create kost
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    private function parseRoomDataFromRequest(Request $request): array
    {
        return [
            'size'              => $request->input('size'),
            'room_count'        => $request->input('room_count'),
            'room_available'    => $request->input('room_available'),
        ];
    }
}
