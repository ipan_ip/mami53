<?php

namespace App\Http\Controllers\Api\Owner\Input\Kost;

use App\Entities\Owner\ActivityType;
use App\Entities\Property\Property;
use App\Entities\Room\DataStage;
use App\Entities\Room\Element\CreationFlag;
use App\Entities\Room\Room;
use App\Exceptions\Owner\ResourceNotFoundException;
use App\Exceptions\Owner\ResourceNotOwnedException;
use App\Http\Controllers\Api\Owner\BaseController;
use App\Jobs\Owner\MarkActivity;
use App\Services\Owner\Kost\DataStageService;
use App\Services\Owner\Kost\InputService;
use App\Transformers\Owner\Data\Property\PropertyTransformer;
use App\Transformers\Owner\Input\Kost\InformationTransformer;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class InformationController extends BaseController
{
    /**
     * submit the information data
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Services\Owner\Kost\InputService $service
     * @param \App\Services\Owner\Kost\DataStageService $stageService
     * @param \App\Transformers\Owner\Input\Kost\InformationTransformer $transformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(
        Request $request,
        InputService $service,
        DataStageService $stageService,
        InformationTransformer $transformer,
        PropertyTransformer $propertyTransformer
    ): JsonResponse {
        $user = $this->user();

        $validator = Validator::make(
            $request->all(),
            [
                'name' => [
                    'bail',
                    'required_if:property,null',
                    'string',
                    'min:5',
                    'max:250',
                    function ($attribute, $value, $fail) {
                        $nameExist = Room::where('name', $value)->first();
                        if ($nameExist instanceof Room) {
                            return $fail('Nama kos telah digunakan sebelumnya');
                        }
                    },
                ],
                'gender' => 'required|integer|between:0,2',
                'type_name' => 'bail|required_if:property.allow_multiple,true|string|min:1|max:250',
                'description' => 'nullable|string',
                'list_rules' => 'array',
                'list_rules.*' => 'integer|min:1',
                'photo_rules' => 'array|max:5',
                'photo_rules.*.id' => 'required|integer|min:1',
                'photo_rules.*.file_name' => 'nullable|string',
                'building_year' => 'nullable|integer',
                'manager_name'  => 'nullable|string',
                'manager_phone' => 'nullable|string',
                'remark' => 'nullable|string',
                'property' => [
                    function ($attribute, $value, $fail) use ($user) {
                        if (!is_null($value)) {
                            $subValidator = Validator::make(
                                $value,
                                [
                                    'id' => 'nullable|integer',
                                    'name' => 'required|string',
                                    'allow_multiple' => 'required|boolean',
                                ]
                            );

                            if ($subValidator->fails()) {
                                return $fail($subValidator->errors()->all());
                            }

                            $propertyExist = Property::where(
                                'name',
                                $value['name']
                            )->where(
                                'owner_user_id',
                                $user->id
                            )->first();

                            if ($propertyExist instanceof Property) {
                                if (empty($value['id']) || $propertyExist->id !== $value['id']) {
                                    return $fail("Nama $attribute Sudah digunakan");
                                }
                            }
                        }
                    }
                ],
            ]
        );

        try {
            $validator->validate();
            $property = !is_null($request->input('property')) ?
                $service->handleProperty($request->input('property'), $user) :
                null;
        } catch (ResourceNotFoundException $exception) {
            return $this->response([], $exception, 100404);
        } catch (ResourceNotOwnedException $exception) {
            return $this->response([], $exception, 100403);
        } catch (ValidationException $exception) {
            return $this->response([], $exception, 100422);
        }

        $data = $this->parseInformationDataFromRequest($request, $user);

        if ($property instanceof Property) {
            $data['property_id'] = $property->id;
            $data['name'] = $service->getNamePattern($property, $data["unit_type"]);
        }

        $room = $service->createKost(
            $data,
            $user,
            $property
        );
        $room->syncRoomRuleTags(
            is_null($request->input('list_rules')) ? [] : $request->input('list_rules')
        );

        $room = $service->syncRoomTermPhotos(
            is_null($request->input('photo_rules')) ? [] : $request->input('photo_rules'),
            $room
        );

        $stageService->registerDataStage($room, DataStage::STAGE_DATA);
        MarkActivity::dispatch($user->id, ActivityType::TYPE_UPDATE_PROPERTY);

        CreationFlag::updateOrCreate(
            [
                'create_from' => CreationFlag::CREATE_NEW,
                'log' => [
                    'user_id' => $user->id,
                    'property' => $property instanceof Property ? $property->toArray() : null,
                    'request-data' => $request->all()
                ]
            ],
            [
                'designer_id' => $room->id
            ]
        );

        return $this->response([
            'status' => true,
            'room' => $transformer->transform($room),
            'property' => $property instanceof Property ?
                $propertyTransformer->transform($property) :
                null
        ]);
    }

    /**
     * update the information data
     *
     * @param integer $songId
     * @param \Illuminate\Http\Request $request
     * @param \App\Services\Owner\Kost\InputService $service
     * @param \App\Services\Owner\Kost\DataStageService $stageService
     * @param \App\Transformers\Owner\Input\Kost\InformationTransformer $transformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(
        int $songId,
        Request $request,
        InputService $service,
        DataStageService $stageService,
        InformationTransformer $transformer,
        PropertyTransformer $propertyTransformer
    ): JsonResponse {
        $user = $this->user();

        $validator = Validator::make(
            $request->all(),
            [
                'name' => [
                    'bail',
                    'required_if:property,null',
                    'string',
                    'min:5',
                    'max:250',
                    function ($attribute, $value, $fail) use ($songId) {
                        $nameExist = Room::where('name', $value)->where('song_id', '!=', $songId)->first();
                        if ($nameExist instanceof Room) {
                            return $fail('Nama kos telah digunakan sebelumnya');
                        }
                    },
                ],
                'gender' => 'required|integer|between:0,2',
                'type_name' => 'bail|required_if:property.allow_multiple,true|string|min:1|max:250',
                'description' => 'nullable|string',
                'list_rules' => 'array',
                'list_rules.*' => 'integer|min:1',
                'photo_rules' => 'array|max:5',
                'photo_rules.*.id' => 'required|integer|min:1',
                'photo_rules.*.file_name' => 'nullable|string',
                'building_year' => 'nullable|integer',
                'manager_name' => 'nullable|string',
                'manager_phone' => 'nullable|string',
                'remark' => 'nullable|string',
                'property' => [
                    function ($attribute, $value, $fail) use ($user) {
                        if (!is_null($value)) {
                            $subValidator = Validator::make(
                                $value,
                                [
                                    'id' => 'required|integer',
                                    'name' => 'required|string',
                                    'allow_multiple' => 'required|boolean',
                                ]
                            );

                            if ($subValidator->fails()) {
                                return $fail($subValidator->errors()->all());
                            }

                            $propertyExist = Property::where('name', $value['name'])->where('owner_user_id', $user->id)->first();

                            if ($propertyExist instanceof Property) {
                                if (empty($value['id']) || $propertyExist->id !== $value['id']) {
                                    return $fail("Nama $attribute Sudah digunakan");
                                }
                            }
                        }
                    }
                ]
            ]
        );

        try {
            $validator->validate();
            $room = $service->getData(
                $songId,
                $user,
                [
                    'address_note' => function ($query) {
                        $query->with(['province', 'city', 'subdistrict']);
                    },
                    'designer_rule_tags',
                    'property',
                    'room_term.room_term_document.media',
                    'owners'
                ]
            );

            $property = $room->property;

            if (!is_null($request->input('property'))) {
                $property = $service->handleProperty($request->input('property'), $user);
            }

            $data = $this->parseInformationDataFromRequest($request, $user);

            if ($property instanceof Property) {
                $data['property_id'] = $property->id;
                $addressNote = optional($room->address_note);

                $data['name'] = $service->getNamePattern(
                    $property,
                    $data["unit_type"],
                    (string) (optional($addressNote->subdistrict)->name ?: $room->area_subdistrict),
                    (string) (optional($addressNote->city)->name ?: $room->area_city)
                );
            }

            $room = $service->updateKost($room, $data);

            $room->syncRoomRuleTags(
                is_null($request->input('list_rules')) ? [] : $request->input('list_rules')
            );

            $room = $service->syncRoomTermPhotos(
                is_null($request->input('photo_rules')) ? [] : $request->input('photo_rules'),
                $room
            );
            $stageService->registerDataStage($room, DataStage::STAGE_DATA);
            MarkActivity::dispatch($user->id, ActivityType::TYPE_UPDATE_PROPERTY);

            return $this->response([
                'status' => true,
                'room' => $transformer->transform($room),
                'property' => $room->property instanceof Property ?
                    $propertyTransformer->transform($room->property) :
                    null
            ]);
        } catch (ValidationException $exception) {
            return $this->response([], $exception, 100422);
        } catch (ModelNotFoundException $exception) {
            return $this->response([], $exception, 100404);
        } catch (ResourceNotFoundException $exception) {
            return $this->response([], $exception, 100404);
        } catch (ResourceNotOwnedException $exception) {
            return $this->response([], $exception, 100403);
        }
    }

    /**
     * get kos information data for prefilled form
     *
     * @param integer $songId
     * @param \App\Services\Owner\Kost\InputService $service
     * @param \App\Transformers\Owner\Input\Kost\InformationTransformer $transformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function get(
        int $songId,
        InputService $service,
        InformationTransformer $transformer,
        PropertyTransformer $propertyTransformer
    ): JsonResponse {
        try {
            $room = $service->getData(
                $songId,
                $this->user(),
                [
                    'address_note' => function ($query) {
                        $query->with(['province', 'city', 'subdistrict']);
                    },
                    'designer_rule_tags',
                    'owners',
                    'property',
                    'room_term.room_term_document.media'
                ]
            );

            return $this->response([
                'status' => true,
                'room' => $transformer->transform($room),
                'property' => $room->property instanceof Property ?
                    $propertyTransformer->transform($room->property) :
                    null
            ]);
        } catch (ModelNotFoundException $exception) {
            return $this->response([], $exception, 100404);
        } catch (ResourceNotOwnedException $exception) {
            return $this->response([], $exception, 100403);
        }
    }

    /**
     * parse request on information step to create kost
     *
     * @param \Illuminate\Http\Request $request
     * @param User $user
     * @return array
     */
    private function parseInformationDataFromRequest(Request $request, User $user): array
    {
        return [
            'gender'        => $request->input('gender'),
            'name'          => trim($request->input('name')),
            'description'   => $request->input('description'),
            'building_year' => $request->input('building_year'),
            'owner_name'  => $user->name,
            'owner_phone' => $user->phone_number,
            'manager_name'  => $request->input('manager_name'),
            'manager_phone' => $request->input('manager_phone'),
            'remark'        => $request->input('remark'),
            'unit_type' => trim($request->input('type_name'))
        ];
    }
}
