<?php

namespace App\Http\Controllers\Api\Owner\Input\Kost;

use App\Entities\Owner\ActivityType;
use App\Entities\Property\Property;
use App\Entities\Room\DataStage;
use App\Entities\Room\Element\Tag\GroupType as TagGroup;
use App\Exceptions\Owner\ResourceNotOwnedException;
use App\Http\Controllers\Api\Owner\BaseController;
use App\Jobs\KostIndexer\IndexKostJob;
use App\Jobs\Owner\MarkActivity;
use App\Services\Owner\Kost\DataStageService;
use App\Services\Owner\Kost\InputService;
use App\Transformers\Owner\Data\Property\PropertyTransformer;
use App\Transformers\Owner\Input\Kost\FacilitiesTransformer;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class FacilitiesController extends BaseController
{
    /**
     * store new kost from Facilities Step
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Services\Owner\Kost\InputService $service
     * @param \App\Services\Owner\Kost\DataStageService $stageService
     * @param \App\Transformers\Owner\Input\Kost\FacilitiesTransformer $transformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(
        Request $request,
        InputService $service,
        DataStageService $stageService,
        FacilitiesTransformer $transformer
    ): JsonResponse {
        $validator = Validator::make(
            $request->all(),
            [
                'public' => 'required|array',
                'public.*' => 'required|integer',
                'bedroom' => 'required|array',
                'bedroom.*' => 'required|integer',
                'bathroom' => 'required|array',
                'bathroom.*' => 'required|integer',
                'parking' => 'nullable|array',
                'parking.*' => 'nullable|integer',
                'near' => 'nullable|array',
                'near.*' => 'nullable|integer'
            ]
        );

        try {
            $validator->validate();
            $user = $this->user();
            $room = $service->createKost(
                [
                    'campaign_source' => $service->getCampaignSourceValue($request),
                ],
                $user
            );
            $room->attachTags($this->parseRequestForData($request));
            $room->loadMissing(['tags.types']);
            $stageService->registerDataStage($room, DataStage::STAGE_FACILITY);
            MarkActivity::dispatch($user->id, ActivityType::TYPE_UPDATE_PROPERTY);

            return $this->response([
                'status' => true,
                'room' => $transformer->transform($room),
            ]);
        } catch (ValidationException $exception) {
            return $this->response([], $exception, 100422);
        } catch (Exception $exception) {
            return $this->response([], $exception, 100400);
        }
    }

    /**
     * get Kost Data For Facilities Step
     *
     * @param integer $songId
     * @param \App\Services\Owner\Kost\InputService $service
     * @param \App\Transformers\Owner\Input\Kost\FacilitiesTransformer $transformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function get(
        int $songId,
        InputService $service,
        FacilitiesTransformer $transformer,
        PropertyTransformer $propertyTransformer
    ): JsonResponse {
        try {
            $room = $service->getData(
                $songId,
                $this->user(),
                [
                    'owners',
                    'tags' => function ($query) {
                        $query->with('types')->whereHas(
                            'types',
                            function ($query) {
                                $query->whereIn('name', array_keys(TagGroup::FACILITIES_MAPPING));
                            }
                        );
                    },
                    'property'
                ]
            );

            return $this->response([
                'status' => true,
                'room' => $transformer->transform($room),
                'property' => $room->property instanceof Property ?
                    $propertyTransformer->transform($room->property) :
                    null
            ]);
        } catch (ModelNotFoundException $exception) {
            return $this->response([], $exception, 100404);
        } catch (ResourceNotOwnedException $exception) {
            return $this->response([], $exception, 100403);
        }
    }

    /**
     * save kost data update on Facilities step
     *
     * @param integer $songId
     * @param \Illuminate\Http\Request $request
     * @param \App\Services\Owner\Kost\InputService $service
     * @param \App\Services\Owner\Kost\DataStageService $stageService
     * @param \App\Transformers\Owner\Input\Kost\FacilitiesTransformer $transformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(
        int $songId,
        Request $request,
        InputService $service,
        DataStageService $stageService,
        FacilitiesTransformer $transformer,
        PropertyTransformer $propertyTransformer
    ): JsonResponse {
        $validator = Validator::make(
            $request->all(),
            [
                'public' => 'nullable|array',
                'public.*' => 'nullable|integer',
                'bedroom' => 'required|array',
                'bedroom.*' => 'required|integer',
                'bathroom' => 'nullable|array',
                'bathroom.*' => 'nullable|integer',
                'parking' => 'nullable|array',
                'parking.*' => 'nullable|integer',
                'near' => 'nullable|array',
                'near.*' => 'nullable|integer'
            ]
        );

        try {
            $validator->validate();
            $user = $this->user();
            $room = $service->getData(
                $songId,
                $user,
                [
                    'owners',
                    'tags' => function ($query) {
                        $query->with('types')
                        ->whereHas(
                            'types',
                            function ($query) {
                                $query->whereIn('name', array_keys(TagGroup::FACILITIES_MAPPING));
                            }
                        )
                        ->whereDoesntHave(
                            'types',
                            function ($query) {
                                $query->where('name', TagGroup::RULE_TAG_TYPE);
                            }
                        );
                    },
                    'property'
                ]
            );

            $room = $service->syncFacilities($room, $this->parseRequestForData($request));
            $room = $service->updateKost($room, []);
            $stageService->registerDataStage($room, DataStage::STAGE_FACILITY);
            MarkActivity::dispatch($user->id, ActivityType::TYPE_UPDATE_PROPERTY);

            return $this->response([
                'status' => true,
                'room' => $transformer->transform($room),
                'property' => $room->property instanceof Property ?
                    $propertyTransformer->transform($room->property) :
                    null
            ]);
        } catch (ValidationException $exception) {
            return $this->response([], $exception, 100422);
        } catch (ModelNotFoundException $exception) {
            return $this->response([], $exception, 100404);
        } catch (ResourceNotOwnedException $exception) {
            return $this->response([], $exception, 100403);
        }
    }

    /**
     * parse request for data operation
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    private function parseRequestForData(Request $request): array 
    {
        return array_merge(
            (array) $request->input('public'),
            (array) $request->input('bedroom'),
            (array) $request->input('bathroom'),
            (array) $request->input('parking'),
            (array) $request->input('near')
        );
    }
}
