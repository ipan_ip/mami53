<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseController;
use App\Http\Helpers\ApiResponse as Api;
use Validator;
use App\Entities\Feedback\SurveyFeedback;

/**
 * #growthsprint1
 * Currently doesn't use repository pattern, since this will be only single function at the moment
 */
class SurveyFeedbackController extends BaseController
{

    protected $validationMessages = [
        'rating.required' => 'Rating harus diisi',
        'rating.numeric' => 'Rating harus berupa angka',
        'rating.min' => 'Rating harus 1 - 4',
        'rating.max' => 'Rating harus 1 - 4',
        'feedback.required' => 'Feedback harus diisi',
        'feedback.max' => 'Feedback maksimal :max karakter',
        'input_as.required' => 'Tipe Penginput harus diisi'
    ];

    public function __construct()
    {
        parent::__construct();
    }

    public function submitFeedback(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'rating' => 'required|numeric|min:1|max:4',
            'feedback' => 'max:1000',
            'input_as' => 'required|max:50'
        ], $this->validationMessages);

        if($validator->fails()) {
            return Api::responseData([
                'status'=>'false',
                'meta' => [
                    'message' => implode('. ', $validator->errors()->all()),
                    'messages' => $validator->errors()->all()
                ]
            ]);
        }

        $user = $this->user();

        $feedback = new SurveyFeedback;
        $feedback->user_id = !is_null($user) ? $user->id : null;
        $feedback->rating = $request->input('rating');
        $feedback->feedback = $request->input('feedback');
        $feedback->input_as = $request->input('input_as');
        $feedback->save();

        return Api::responseData([
            'status' => true
        ]);
    }
}