<?php
namespace App\Http\Controllers\Api;

use App\Http\Helpers\ApiResponse;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Helpers\ApiHelper;
use Validator;
use App\Repositories\ApartmentProjectRepository;
use App\Entities\Apartment\ApartmentProjectFilter;
use App\Entities\Apartment\ApartmentProjectCluster;


class ApartmentProjectController extends BaseController
{
	protected $repository;

	public function __construct(ApartmentProjectRepository $repository)
	{
		$this->repository = $repository;
	}

	public function show(Request $request)
	{
		$validator = Validator::make($request->all(), 
						[
							'seq'=>'required|numeric',
							'code'=>'required|max:10'
						],
						[
							'seq.required'=>'Seq harus diisi',
							'seq.numeric'=>'Seq harus berupa angka',
							'code.required'=>'Kode harus diisi',
							'code.max'=>'Kode maksimal :max karakter'
						]);

		if($validator->fails()) {
			return ApiResponse::responseData([
				'status'=>false,
				'meta'=> [
					'message'=>implode('. ', $validator->errors()->all()),
					'messages'=>$validator->errors()->all()
				]
			]);
		}

		$apartmentProject = $this->repository->getByIdAndCode($request->seq, $request->code);

		if(!$apartmentProject) {
			return ApiResponse::responseData([
				'status'=>false,
				'meta'=>[
					'message'=>'Apartemen tidak ditemukan',
					'messages'=>['Apartemen tidak ditemukan']
				]
			]);
		}

		$apartmentProjectData = (new \App\Presenters\ApartmentProjectPresenter())->present($apartmentProject)['data'];

		return ApiResponse::responseData([
			'data'=>$apartmentProjectData
		]);
	}

	public function suggestions(Request $request)
	{
		$validator = Validator::make($request->all(),
			[
				'name'=>'required'
			], 
			[
				'name.required'=>'Nama harus diisi'
			]);

		if($validator->fails()) {
			return ApiResponse::responseData([
				'status'=>false,
				'messages'=>$validator->errors()->all()
			]);
		}

		$apartmentSuggestions = $this->repository->getSuggestions($request->name);

		$data = [];
		foreach($apartmentSuggestions as $apartment) {
			$data[] = [
				'_id' => $apartment->id,
				'name' =>  $apartment->name
			];
		}

		return ApiResponse::responseData([
			'data'=>$data
		]);
	}


	/**
	 * Search Apartment Project List
	 * The flow will follow kost search
	 */
	public function listSearch(Request $request)
	{
		$this->repository->setPresenter(new \App\Presenters\ApartmentProjectPresenter('list'));

		$filters = $request->input('filters');

		$filters['location']    = $request->input('location');
        $filters['sorting']     = $request->input('sorting');
        // $filters['titik_tengah'] = $this->getTitikTengah($filters['location']);

        $this->repository->pushCriteria(new \App\Criteria\ApartmentProject\MainFilterCriteria($filters));

        $params = [];
        if($request->filled('limit')) $params['limit'] = $request->input('limit');
        if($request->filled('offset')) $params['offset'] = $request->input('offset');

        $response = $this->repository->getItemList(new ApartmentProjectFilter($filters), $params);

        return ApiResponse::responseData($response);
	}

	/**
	 * Search Apartment Project Cluster
	 * The flow will follow kost cluster search
	 */
	public function clusterSearch(Request $request)
	{
		$filter  = $request->input('filters');
 
        if (!empty($filter['place'])){
            $filter['place'] = $filter['place']; 
        } else {
            $filter['place'] = null;
        }

        $filter['location']     = $request->input('location');
        // $filter['titik_tengah'] = $this->getTitikTengah($filter['location']);

        $response = $this->repository->getCluster($filter);

        // $searchHistory = (new UserSearchHistory)->storeHistoryFilter($filter, $this->user(), $this->device());

        // if ( count($response['rooms']) == 0 ) {
        //       (new ZeroResult)->createZeroResult($searchHistory->id);
        // }

        return ApiResponse::responseData($response);
	}

	/**
	 * Detail cluster Project
	 * The flow will follow detail cluster search
	 */
	public function detailCluster(Request $request)
	{
		$location = $request->input('location');
        $point = $request->input('point');

        $location = ApartmentProjectCluster::getRangeSingleCluster($location, $point, 7);
        
        // $sort = 'asc';
        // if ($request->filled('sorting')) {
        //     $sorting =  $request->input('sorting');
        //     if (isset($sorting['direction']) AND in_array($sorting['direction'], ['asc', 'desc', 'rand'])) $sort = $sorting['direction']; 
        // }

        // $request->merge(array("sorting"  => array('field'=>'price','direction'=> $sort)));
        $request->merge(array("location" => $location));

        return $this->listSearch($request);
	}
}