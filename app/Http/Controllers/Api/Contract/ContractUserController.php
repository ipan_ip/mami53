<?php
namespace App\Http\Controllers\Api\Contract;

use App\Entities\Booking\BookingUserDraft;
use App\Entities\Mamipay\MamipayContract;
use App\Exceptions\CustomErrorException;
use App\Http\Controllers\Api\BaseController;
use App\Http\Helpers\ApiResponse as Api;
use App\Http\Requests\Booking\BookingUserDraftRequest;
use App\Jobs\SendPushNotificationQueue;
use App\Presenters\Booking\BookingUserDraftPresenter;
use App\Presenters\Contract\ContractPresenter;
use App\Repositories\Booking\BookingUserDraftRepository;
use App\Repositories\Contract\ContractRepository;
use Illuminate\Http\JsonResponse;
use Exception;
use Bugsnag;
use Illuminate\Http\Request;

class ContractUserController extends BaseController
{
    protected $repository;

    public function __construct(ContractRepository $repository)
    {
        $this->repository = $repository;
        parent::__construct();
    }

    public function index(Request $request): JsonResponse
    {
        // force default response to false
        $response = $this->makeDefaultResponse();
        try {
            $user = $this->user();
            if (is_null($user)) throw new CustomErrorException('Data user tidak ditemukan');

            // params filter and other
            $params = [];
            $params['limit']    = $request->limit ?? 10;
            $params['order']    = $request->order ?? 'start_date';
            $params['sort']     = $request->sort ? strtoupper($request->sort) : 'DESC';
            $params['status']   = MamipayContract::STATUS_TERMINATED;

            $lists = $this->repository->getContractListUser($user, $params);
            $tranform = (new ContractPresenter('user-list'))->present($lists);

            $response = [
                'data' => [
                    'data'          => $tranform['data'] ?? null,
                    'current_page'  => $lists->currentPage(),
                    'from'          => $lists->firstItem(),
                    'last_page'     => $lists->lastPage(),
                    'next_page_url' => $lists->nextPageUrl(),
                    'per_page'      => $lists->perPage(),
                    'prev_page_url' => $lists->previousPageUrl(),
                    'to'            => $lists->count(),
                    'total'         => $lists->total(),
                    'has_more'      => $lists->hasMorePages()
                ]
            ];

        } catch (CustomErrorException $e) {
            $response['meta']['message'] = $e->getMessage();
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
            $response['meta']['message'] = $e->getMessage();
        }
        return Api::responseData($response);
    }

    public function show(Request $request, $id): JsonResponse
    {
        // force default response to false
        $response = $this->makeDefaultResponse();
        try {
            $user = $this->user();
            if (is_null($user))
                throw new CustomErrorException('Data user tidak ditemukan');

            $contract = $this->repository->getContractDetailUser($user, $id);
            if (!$contract)
                throw new CustomErrorException('Data contract tidak ditemukan');

            $tranform = (new ContractPresenter('user-detail'))->present($contract);

            $response = [
                'data' => $tranform['data']
            ];

        } catch (CustomErrorException $e) {
            $response['meta']['message'] = $e->getMessage();
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
            $response['meta']['message'] = $e->getMessage();
        }
        return Api::responseData($response);
    }

    /**
     * Function to return default response
     * @return array
     */
    private function makeDefaultResponse(): array
    {
        return [
            'status' => false,
            'meta' => ['message' => 'General Error']
        ];
    }
}