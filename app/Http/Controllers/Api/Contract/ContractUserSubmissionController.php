<?php
namespace App\Http\Controllers\Api\Contract;

use App\Entities\Dbet\DbetLinkRegistered;
use App\Exceptions\CustomErrorException;
use App\Http\Controllers\Api\BaseController;
use App\Http\Helpers\ApiResponse as Api;
use App\Http\Requests\Owner\Tenant\ContractSubmissionRequest;
use App\Presenters\Owner\Tenant\ContractSubmissionPresenter;
use App\Repositories\Dbet\DbetLinkRegisteredRepository;
use App\Repositories\Dbet\DbetLinkRepository;
use App\Services\Booking\BookingService;
use App\Services\Booking\ContractSubmissionService;
use App\Services\Sendbird\GroupChannelService;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Exception;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ContractUserSubmissionController extends BaseController
{
    protected $dbetLinkRepository;
    protected $dbetLinkRegisteredRepository;
    protected $service;

    public function __construct(
        DbetLinkRepository $dbetLinkRepository,
        DbetLinkRegisteredRepository $dbetLinkRegisteredRepository,
        BookingService $service,
        GroupChannelService $grpChnlSrv
    ) {
        $this->dbetLinkRepository = $dbetLinkRepository;
        $this->dbetLinkRegisteredRepository = $dbetLinkRegisteredRepository;
        $this->service = $service;
        $this->grpChnlSrv = $grpChnlSrv;
        parent::__construct();
    }

    /**
     * @param Request $request
     * @param $songId
     * @return JsonResponse
     */
    public function showLink(Request $request, $code): JsonResponse
    {
        $response = $this->makeDefaultResponse();
        try {
            $user = $this->user();
            if (!$user)
                throw new CustomErrorException('User tidak ditemukan');

            // get data dbet link
            $data = $this->dbetLinkRepository->findByCode($code);
            if ($data === null)
                throw new CustomErrorException('Data tidak ditemukan');

            // transform data for response
            $transform = (new ContractSubmissionPresenter('data-link-tenant'))->present($data);

            // output
            $result = $transform['data'] ?? [];
            $result['user'] = $this->getUserData($user);
            $response = [
                'status' => true,
                'data' => $result
            ];

        } catch (CustomErrorException $e) {
            $response['meta']['message'] = $e->getMessage();
        } catch (ModelNotFoundException $e) {
            $response['meta']['message'] = 'Data tidak ditemukan';
        } catch (Exception $e) {
            $response['meta']['message'] = $e->getMessage();
            Bugsnag::notifyException($e);
        }
        return Api::responseData($response);
    }

    /**
     * @param ContractSubmissionRequest $request
     * @return JsonResponse
     */
    public function store(ContractSubmissionRequest $request): JsonResponse
    {
        $response = $this->makeDefaultResponse();
        try {
            // validate data
            $validator = $request->failedValidator;
            if ($validator && $validator->fails())
                throw new CustomErrorException(implode(", ",$validator->errors()->all()));

            // get data
            $dbetLink = $request->getDbetLink();
            $room = $dbetLink->room;
            $user = $this->user();

            if ($dbetLink == null)
                throw new CustomErrorException('DBET link data tidak ditemukan');

            if (!$user)
                throw new CustomErrorException('User tidak ditemukan');

            // contract submission service
            $contractSubmissionService = app()->make(ContractSubmissionService::class);

            DB::beginTransaction();

            $groupChannelUrl = $this->getGroupChannelUrl($user, $room);

            // prepare store data
            $req = [];
            $req['user_id']             = $user->id;
            $req['designer_id']         = $dbetLink->designer_id;
            $req['fullname']            = $request->fullname ?? null;
            $req['designer_room_id']    = $request->designer_room_id ?? null;
            $req['fullname']            = $request->fullname ?? null;
            $req['gender']              = $request->gender ?? null;
            $req['phone']               = $request->phone ?? null;
            $req['jobs']                = $request->job ?? null;
            $req['work_place']          = $request->work_place ?? null;
            $req['identity_id']         = $request->identity_id ?? null;
            $req['due_date']            = $request->due_date ?? null;
            $req['rent_count_type']     = $request->rent_count_type ?? null;
            $req['price']               = $request->price ?? null;
            $req['status']              = DbetLinkRegistered::STATUS_PENDING;
            $req['group_channel_url']   = $groupChannelUrl;

            if (!empty($request->additional_price) && count($request->additional_price) > 0) {
                $req['additional_price_detail'] = json_encode($request->additional_price);
            }

            // store data
            $data = $this->dbetLinkRegisteredRepository->create($req);

            $contractSubmissionService->tenantRequestNotification($user, $data);

            // transform data for response
            $transform = (new ContractSubmissionPresenter('data'))->present($data);
            DB::commit();

            $response = $transform;
        } catch (CustomErrorException $e) {
            DB::rollBack();
            $response['meta']['message'] = $e->getMessage();
        } catch (Exception $e) {
            DB::rollBack();
            $response['meta']['message'] = $e->getMessage();
            Bugsnag::notifyException($e);
        }
        return Api::responseData($response);
    }

    private function getGroupChannelUrl($user, $room)
    {
        try {
            $groupChannel = $this->$grpChnlSrv->findOrCreateChannelForTenantAndRoom($user, $room);
            return $groupChannel->channel_url;
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
            return '';
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function homepageShortcut(Request $request): JsonResponse
    {
        $response = $this->makeDefaultResponse();
        try {

            $user = $this->user();
            if (!$user)
                throw new CustomErrorException('User tidak ditemukan');

            // get data dbet link
            $data = $this->dbetLinkRegisteredRepository->getHomepageShortcutData($user->id);
            if ($data === null)
                throw new CustomErrorException('Data tidak ditemukan');

            // transform data for response
            $transform = (new ContractSubmissionPresenter('data'))->present($data);

            $response = $transform;

        } catch (CustomErrorException $e) {
            $response['meta']['message'] = $e->getMessage();
        } catch (Exception $e) {
            $response['meta']['message'] = $e->getMessage();
            Bugsnag::notifyException($e);
        }
        return Api::responseData($response);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function onboarding(Request $request): JsonResponse
    {
        $response = $this->makeDefaultResponse();
        try {
            $response = [
                'data' => [
                    'dynamic_onboarding_label' => __('booking.tenant.add_tenant_from_link.onboarding'),
                    'privacy_link' => __('booking.tenant.add_tenant_from_link.privacy_link')
                ]
            ];
        } catch (Exception $e) {
            $response['meta']['message'] = $e->getMessage();
            Bugsnag::notifyException($e);
        }
        return Api::responseData($response);
    }

    /**
     * @param User $user
     * @return array
     */
    private function getUserData(User $user): array
    {
        try {
            // get tenant data
            $tenant = optional($user)->mamipay_tenant;
            $tenantIdentity = optional($tenant)->photoIdentifier;

            return [
                'id'            => $user->id,
                'name'          => $user->name,
                'phone'         => optional($user)->phone_number,
                'gender'        => optional($user)->gender,
                'email'         => $user->email,
                'job'           => $user->job,
                'work_place'    => $user->work_place,
                'identity_id'   => $tenantIdentity->id ?? 0,
                'identity'      => ($tenantIdentity !== null) ? $tenantIdentity->cached_urls : null
            ];
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * Function to return default response
     * @return array
     */
    private function makeDefaultResponse(): array
    {
        return [
            'status' => false,
            'meta' => ['message' => 'General Error']
        ];
    }
}