<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Http\Helpers\ApiResponse as Api;
use App\Entities\Career\Vacancy;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;

class CareerController extends BaseController
{ 
    public function vacancy(Request $request) {
        try {
            $result = Vacancy::with(['position:id,name', 'locations:name', 'requirements:career_vacancy_id,content', 'description:career_vacancy_id,content'])->get();

            if (!empty($result)) {
                return Api::responseData([
                    'status' => true,
                    'data'   => $result
                ]);

            } else {
                return Api::responseData([
                    'status' => false,
                    'meta'   => ['message' => 'Maaf data tidak tersedia']
                ]);
            }

        }  catch (RequestException $e) {
            throw new \Exception(\GuzzleHttp\Psr7\str($e->getResponse()), 256);
        }  catch(\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }
}
