<?php

namespace App\Http\Controllers\Api\Giant;

use Illuminate\Http\Request;
use Cache;
use App\Libraries\SMSLibrary;
use App\Http\Helpers\ApiResponse as Api;
use App\Repositories\Giant\GiantDataRepository;

class RegisterController extends GiantbaseController
{
    protected $repository;

    public function __construct(GiantDataRepository $repository)
    {
        $this->repository = $repository;

        parent::__construct();
    }

    public function postVerification(Request $request)
    {
        $agent = $this->agent();
        if (is_null($agent)) {
            return Api::responseData([ 'status' => false ]); 
        }
        $randomId = rand(111111,999999);

        SMSLibrary::smsVerification($agent->phone_number, "Kode verifikasi agent anda: ".$randomId);
        Cache::put("agentcodeverification:".$agent->phone_number, $randomId,60*24);
        return Api::responseData(['status' => true]);
    }

    public function postLogin(Request $request)
    {
        $agent = $this->agent();
        if (is_null($agent)) {
            return Api::responseData([ 'status' => false, "message" => "Agen tidak ditemukan" ]); 
        }
        $randomId = Cache::get("agentcodeverification:".$agent->phone_number);
        
        if ($randomId != $request->input('code')) {
            return Api::responseData(['status' => false, "message" => "Kode yang anda masukkan salah"]);
        }
        
        return Api::responseData(['status' => true, "message" => "Anda berhasil login"]);
    }

    public function listData(Request $request)
    {
        $agent = $this->agent();
        if (is_null($agent)) {
            return Api::responseData([ 'status' => false ]); 
        }
        $response = $this->repository->getListData($request->all(), $agent);
        return Api::responseData(["status" => true, "data" => $response]);
    }

    public function getEdited(Request $request)
    {
        $agent = $this->agent();
        if (is_null($agent)) {
            return Api::responseData([ 'status' => false ]); 
        }
        $response = $this->repository->getListDataEdited($request->all(), $agent);
        return Api::responseData(["status" => true, "data" => $response]);
    }
}
