<?php

namespace App\Http\Controllers\Api\Giant;

use Illuminate\Http\Request;
use App\Http\Helpers\ApiResponse as Api;
use App\Repositories\Giant\GiantDataRepository;
use Validator;
use App\Entities\Room\Room;
use App\Entities\Agent\DummyPhoto;
use App\Entities\Agent\DummyDesigner;
use App\Entities\Agent\DummyReview;

class UpdateController extends GiantbaseController
{
    protected $repository;
    protected $ValidatorMessages = [
        "file.required" => "Data tidak boleh kosong",
        "latitude.required" => "Latitude tidak boleh kosong",
        "longitude.required" => "Longitude tidak boleh kosong",
        "id.required" => "Kos tidak ditemukan",   
    ];

    public function __construct(GiantDataRepository $repository)
    {
        $this->repository = $repository;

        parent::__construct();
    }

    public function deletePhoto(Request $request, $id)
    {
        $photo = DummyPhoto::where('id', $id)->first();
        if (is_null($photo)) {
            return Api::responseData(["status" => false, "message" => "Data tidak ditemukan"]);
        }
        $photo->delete();
        return Api::responseData(["status" => true, "message" => "Photo berhasil dihapus"]);
    }

    public function updateReview(Request $request, $id)
    {
        // validate
        $validator = Validator::make($request->all(), 
        [
            'clean'   => 'required|integer|between:1,4',
            'happy'   => 'required|integer|between:1,4',
            'safe'    => 'required|integer|between:1,4',
            'pricing' => 'required|integer|between:1,4',
            'room_facilities'   => 'required|integer|between:1,4',
            'public_facilities' => 'required|integer|between:1,4',
            'content'           => 'required|nullable',
        ], $this->ValidatorMessages);
    
        $agent = null; 
        if (!is_null($agent = $this->agent())) $agent = $this->agent();

        $dummy = DummyReview::where('agent_id', $agent->id)->where('id', $id)->first();

        $validator->after(function ($validator) use($dummy, $agent) {
            if (is_null($agent) OR is_null($dummy)) {
                $validator->errors()->add('field', 'Data tidak ditemukan');
            }
        });

        if($validator->fails()) {
            return Api::responseData([
                    'status'=> false,
                    'message' => Api::validationErrorsToString($validator->errors())
                ]);
        }
        
        $response = $this->repository->updateReviewAgent($agent, $request->all(), $dummy);
        
        $designer = DummyDesigner::where('designer_id', $dummy->designer_id)->first();
        $designer->statuses = 'submit';
        $designer->save();

        return Api::responseData(['status' => $response['status'], "message" => $response['message']]);
    }

    public function updateCheckin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'latitude' => 'required',
            'longitude' => 'required',
            'id' => 'required'
        ], $this->ValidatorMessages);

        $room = Room::Active()->where('expired_phone', 0)->where('song_id', $request->input('id'))->first();

        $validator->after(function ($validator) use($room) {
            if (is_null($room)) {
                $validator->errors()->add('field', 'Data tidak ditemukan');
            }
        });

        if ($validator->fails()) {
            return Api::responseData([
                'status'=> false,
                'message' => Api::validationErrorsToString($validator->errors())
            ]);
        }

        $response = $this->repository->updateCheckinAgent($request->all(), $room, $this->agent());

        return Api::responseData(['status' => $response['status'], "message" => $response['message']]);
    }
}
