<?php

namespace App\Http\Controllers\Api\Giant;

use Illuminate\Http\Request;
use App\Http\Helpers\ApiResponse as Api;
use App\Repositories\Giant\GiantDataRepository;
use Validator;
use App\Entities\Room\Room;
use App\Entities\Agent\DummyPhoto;
use App\Entities\Agent\DummyDesigner;

class DataController extends GiantbaseController
{
    protected $ValidatorMessages = [
        //"file.required" => "Data tidak boleh kosong",
        "latitude.required" => "Latitude tidak boleh kosong",
        "longitude.required" => "Longitude tidak boleh kosong",
        "id.required" => "Kos tidak ditemukan",   
    ];

    protected $repository;

    public function __construct(GiantDataRepository $repository)
    {
        $this->repository = $repository;

        parent::__construct();
    }

    public function postCheckin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            //'file' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'id' => 'required'
        ], $this->ValidatorMessages);

        $room = Room::Active()->where('expired_phone', 0)->where('song_id', $request->input('id'))->first();

        $validator->after(function ($validator) use($room) {
            if (is_null($room)) {
                $validator->errors()->add('field', 'Data tidak ditemukan');
            }
        });

        if ($validator->fails()) {
            return Api::responseData([
                'status'=> false,
                'message' => Api::validationErrorsToString($validator->errors())
            ]);
        }

        $response = $this->repository->postCheckinAgent($request->all(), $room, $this->agent());

        return Api::responseData(['status' => $response['status'], "message" => $response['message']]);
    }

    public function postMediaGiant(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'file' => 'required',
            'type' => 'required|in:'.implode(",", DummyPhoto::TYPE),
            'id' => 'required',
        ], $this->ValidatorMessages);

        $room = Room::Active()->where('expired_phone', 0)->where('song_id', $request->input('id'))->first();

        $validator->after(function ($validator) use($room) {
            if (is_null($room)) {
                $validator->errors()->add('field', 'Data kos tidak ditemukan.');
            }
        });

        if ($validator->fails()) {
            return Api::responseData([
                'status'=> false,
                'message' => Api::validationErrorsToString($validator->errors())
            ]);
        }

        $response = $this->repository->postMediaGiant($room, $request, $this->agent());
        return Api::responseData(['status' => $response['status'], 
                                "message" => $response['message'],
                                "id" => $response['id'],
                                "photo" => $response['photo'],
                                ]);
    }

    public function addMasterReview(Request $request)
    {
        // validate
        $validator = Validator::make($request->all(), 
        [
            'id'      => 'required',
            'clean'   => 'required|integer|between:1,4',
            'happy'   => 'required|integer|between:1,4',
            'safe'    => 'required|integer|between:1,4',
            'pricing' => 'required|integer|between:1,4',
            'room_facilities'   => 'required|integer|between:1,4',
            'public_facilities' => 'required|integer|between:1,4',
            'content'           => 'required|nullable',
        ], $this->ValidatorMessages);
    
        $agent = null; 
        if (!is_null($agent = $this->agent())) $agent = $this->agent();

        $room = Room::where('song_id', $request->input('id'))
                ->first();

        $validator->after(function ($validator) use($room, $agent) {
            if (is_null($agent) OR is_null($room)) {
                $validator->errors()->add('field', 'Agen atau kos tidak ditemukan');
            }
        });

        if($validator->fails()) {
            return Api::responseData([
                    'status'=> false,
                    'message' => Api::validationErrorsToString($validator->errors())
                ]);
        }
        
        $response = $this->repository->addNewReviewAgent($agent, $request->all(), $room);

        return Api::responseData(['status' => $response['status'], "message" => $response['message']]);
    }

    public function postData(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                #"room_available" => "required",
                "id" => "required",
            ], $this->ValidatorMessages);
        
            $agent = null; 
            if (!is_null($agent = $this->agent())) $agent = $this->agent();
    
            $room = Room::where('song_id', $request->input('id'))
                    ->first();
    
            $validator->after(function ($validator) use($room, $agent) {
                if (is_null($agent) OR is_null($room)) {
                    $validator->errors()->add('field', 'Agen atau kos tidak ditemukan');
                }
            });
    
            if($validator->fails()) {
                return Api::responseData([
                        'status'=> false,
                        'message' => Api::validationErrorsToString($validator->errors())
                    ]);
            }

            $response = $this->repository->updateData($agent, $room, $request->all());

            return Api::responseData(['status' => $response['status'], 'message' => $response['message']]); 
    }

    public function getListPhoto(Request $request, $id)
    {
        $room = Room::with(['cards'])->where('song_id', $id)->first();
        if (is_null($room)) {
            return Api::responseData(['status' => false, "message" => "Data tidak ditemukan"]);
        }

        /* 'cover'         =>  'bangunan-tampak-depan-cover',
        'bangunan'      =>  'bangunan-ruang-utama',
        'kamar'         =>  'dalam-kamar',
        'kamar-mandi'   =>  'dalam-kamar-mandi',
        'lainnya'       =>  'lainnya',
        'speed-test'    =>  'speed-test', */
    
        foreach ($room->list_card AS $key => $value) {
            if(!isset($value['description'])) {
                continue;
            }
            
            if (strpos($value['description'],"cover") !== false AND isset($value['id'])) {
                $categorizedPhoto['cover'][] = ["id" => $value['id'], "photo_id" => $value['photo_id'], "photo" => $value['photo_url']['small']];
            } elseif (strpos($value['description'],"bangunan") !== false AND isset($value['id'])) { 
                $categorizedPhoto['bangunan'][] =  ["id" => $value['id'], "photo_id" => $value['photo_id'], "photo" => $value['photo_url']['small']];
            } elseif (strpos($value['description'],"kamar") !== false AND strpos($value['description'],"kamar-mandi") == false AND isset($value['id'])) {
                $categorizedPhoto['kamar'][] =  ["id" => $value['id'], "photo_id" => $value['photo_id'], "photo" => $value['photo_url']['small']];
            } elseif (strpos($value['description'],"kamar-mandi") !== false AND isset($value['id'])) { 
                $categorizedPhoto['kamar-mandi'][] =  ["id" => $value['id'], "photo_id" => $value['photo_id'], "photo" => $value['photo_url']['small']];
            } else if (isset($value['id'])) { 
                $categorizedPhoto['lainnya'][] =  ["id" => $value['id'], "photo_id" => $value['photo_id'], "photo" => $value['photo_url']['small']];
            }
        }

        if (!isset($categorizedPhoto)) {
            $categorizedPhoto = ["cover" => null, "bangunan" => null, "kamar" => null, "kamar-mandi" => null, "lainnya" => null];
        }
        
        return Api::responseData(["status" => true, "data" => $categorizedPhoto]);
    }

    public function getEditData(Request $request, $song_id)
    {
        $agent = $this->agent();
        if (is_null($agent)) {
            return Api::responseData(["status" => false, "message" => "Agen tidak ditemukan"]);
        }
        $room = Room::where('song_id', $song_id)->first();
        if (is_null($room)) {
            return Api::responseData(["status" => false, "message" => "Data tidak ditemukan."]);
        }

        $dummy = DummyDesigner::with(['room', 'room.dummy_review', 'room.dummy_photo'])->whereIn('statuses', ['reject', 'photo', 'checkin'])->where('agent_id', $agent->id)->where('designer_id', $room->id)->first();
        if (is_null($dummy)) {
            return Api::responseData(["status" => false, "message" => "Data tidak ditemukan"]);
        }

        $data = [ "id" => $dummy->room->song_id,
                "latitude" => $dummy->latitude,
                "longitude" => $dummy->longitude,
                "room_available" => $dummy->room_available,
                "card_delete" => array_map("intval", explode(",", $dummy->card_delete)),
            ];
        
        $checkin_photo_get = $dummy->room->dummy_photo->where('type', 'checkin')->first();
        $checkin_photo = ["id" => $checkin_photo_get->id, "photo" => url('uploads/checkin/'.$checkin_photo_get->file_name)];
        $review_data = $dummy->room->dummy_review[0];
        $review = [
                    "id" => $review_data->id,
                    "song_id" => $room->song_id,
                    "clean" => $review_data->cleanliness,
                    "happy" => $review_data->comfort,
                    "safe" => $review_data->safe,
                    "pricing" => $review_data->price,
                    "room_facilities" => $review_data->room_facilities,
                    "public_facilities" => $review_data->public_facilities,
                    "content" => $review_data->content,
                    "photo" => url("uploads/cache/data/dummy/". $review_data->photo),
                ];

        $photos = null;
        foreach ($dummy->room->dummy_photo->where('type', '!=', 'checkin') AS $key => $value) {
            $photos[] = [
                "id" => $value->id,
                "type" => $value->type,
                "photo" => url('uploads/cache/data/dummy/'.$value->file_name),
            ];
        }

        return Api::responseData(["status" => true, "data" => $data, "checkin_photo" => $checkin_photo, "review" => $review, "photos" => $photos]);
    }
}
