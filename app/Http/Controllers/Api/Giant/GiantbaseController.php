<?php

namespace App\Http\Controllers\Api\Giant;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GiantbaseController extends Controller
{
    protected $agent = null;

    public function __construct()
    {

    }

    protected function agent()
    {
        return app()->agent;
    }
}
