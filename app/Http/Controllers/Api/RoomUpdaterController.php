<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Helpers\ApiResponse as Api;
use App\Http\Helpers\ApiHelper;
use App\Http\Requests;
use App\Repositories\RoomUpdaterRepository;
use Validator;
use App\Entities\Room\Element\ResponseMessage;

class RoomUpdaterController extends BaseController
{
    protected $repository;

    protected $validationMessages = [
        'name.required'               => 'Nama Kost harus diisi',
        'photo.*.name.max'            => 'Nama foto maksimal :max karakter',
        'gender.required'             => 'Tipe kost harus diisi',
        'monthly.required'            => 'Harga bulanan harus diisi',
        'monthly.numeric'             => 'Harga bulanan harus berupa angka',
        'yearly.required'             => 'Harga tahunan harus diisi',
        'yearly.numeric'              => 'Harga tahunan harus berupa angka',
        'size.required'               => 'Ukuran harus diisi',
        'card.source.required'        => 'Tidak boleh kosong',
        'card.type.required'          => 'Tipe gambar jangan kosong',
        'card.photo_id.required'      => 'Photo tidak boleh kosong',
        'card.description.required'   => 'Deskripsi tidak boleh kosong',
        'fac_bath_other.required'     => 'Deskripsi kamar mandi tidak boleh kosong',
        'fac_bath_other.max'          => 'Deskripsi kamar max :max karakter',
        'fac_bath.required'           => 'Fasilitas kaman mandi tidak boleh kosong',
        'fac_room.require'            => 'Fasilitas kamar tidak boleh kosong',
        'fac_room_other.required'     => 'Deskripsi kamar tidak boleh kosong',
        'fac_room_other.max'          => 'Deskripsi kamar max :max karakter',
        'fac_share.required'          => 'Fasilitas umum harus diisi',
        'fac_share_other.required'    => 'Deskripsi umum harus diisi',
        'fac_share_other.max'         => 'Deskripsi umum max :max karakter',
        'fac_park.required'           => 'Fasilitas parkir tidak boleh kosong',
        'fac_near.required'           => 'Akses lingkungan tidak boleh kosong',
        'fac_near_other.required'     => 'Deskripsi akses lingkungan tidak boleh kosong',
        'fac_near_other.max'          => 'Deskripsi akses lingkungan max :max karakter',
        'remarks.required'            => 'Keterangan lainnya tidak boleh karakter',
        'remarks.max'                 => 'Keterangan lainnya max :max karakter',
        'description.required'        => 'Deskripsi kos tidak boleh kosong',
        'description.max'             => 'Deskripsi kos max :max karakter',
        'price_remark.required'       => 'Keterangan harga tidak boleh kosong',
        'price_remark.max'            => 'Keterangan harga max :max karakter',
        'weekly.numeric'              => 'Harga mingguan harus berupa angka',
        'daily.numeric'               => 'Harga harian harus berupa angka'  
    ];

    function __construct(RoomUpdaterRepository $repository)
    {
        $this->repository = $repository;
    }

    public function updatePhoto(Request $request, $roomId)
    {
        $room = $this->repository->findByField('song_id', $roomId);
        
        if (count($room) > 0) $room = $room[0];
        else $room = null;

        $checkOwner = $this->repository->checkOwnership($room, $this->user());

        if ($checkOwner == false) {
            return Api::responseData([
                     'status'=>false,
                     'messages'=> [ResponseMessage::FAILED_UPDATE_KOST]
                 ]);
        }

        // validate
         $validator = Validator::make($request->all(), 
             [
                 'card.source'      => 'required',
                 'card.type'        => 'required',
                 'card.photo_id'    => 'required',
                 'card.description' => 'required'
             ], $this->validationMessages);

         if($validator->fails()) {
             return Api::responseData([
                     'status'=>false,
                     'messages'=>$validator->errors()->all()
                 ]);
         }

        $response = $this->repository->updateCards($request->input('cards'), $room->id);

        return Api::responseData([
                     'status'   =>  $response['status'],
                     'messages' =>  [$response['message']]
                 ]);
    }

    public function updateNameType(Request $request, $roomId)
    {
        $room = $this->repository->findByField('song_id', $roomId);
        
        if (count($room) > 0) $room = $room[0];
        else $room = null;

        $checkOwner = $this->repository->checkOwnership($room, $this->user());

        if ($checkOwner == false) {
            return Api::responseData([
                     'status'=>false,
                     'messages'=> [ResponseMessage::FAILED_UPDATE_KOST]
                 ]);
        }

        $validator = Validator::make($request->all(),
            [
               'name' => 'required',
               'gender' => 'required'  
            ], $this->validationMessages);

         if($validator->fails()) {
             return Api::responseData([
                     'status'  => false,
                     'messages'=> $validator->errors()->all()
                 ]);
         }

         $data = array(
                    "name"      => $request->input('name'),
                    "gender"    => $request->input('gender'),
                    "is_active" => 'false',
                    "user_id"   => $this->user()->id
            );

         $response = $this->repository->updateKost($room, $data);

         return Api::responseData([
                     'status'   =>  $response['status'],
                     'messages' =>  [$response['message']]
                 ]);

    }

    public function getNameType(Request $request, $roomId)
    {
        $room = $this->repository->findByField('song_id', $roomId);
        
        if (count($room) > 0) $room = $room[0];
        else $room = null;

        $checkOwner = $this->repository->checkOwnership($room, $this->user());

        if ($checkOwner == false) {
            return Api::responseData([
                     'status'  => false,
                     'messages'=> [ResponseMessage::FAILED_GET_DATA_KOST],
                     'data'    => array()
                 ]);
        }

        $response = $this->repository->getRoomNameType($room);
        return Api::responseData([
                     'status'  => true,
                     'messages'=> [ResponseMessage::SUCCESS_GET_DATA_KOST],
                     'data'    => $response
                 ]);
    }

    public function updatePriceRoom(Request $request, $roomId)
    {
        $room = $this->repository->findByField('song_id', $roomId);
        
        if (count($room) > 0) $room = $room[0];
        else $room = null;

        $checkOwner = $this->repository->checkOwnership($room, $this->user());

        if ($checkOwner == false) {
            return Api::responseData([
                     'status'=>false,
                     'messages'=> [ResponseMessage::FAILED_UPDATE_KOST]
                 ]);
        }

        $validator = Validator::make($request->all(),
            [
               'yearly'  => 'required|numeric',
               'monthly' => 'required|numeric',
               'weekly'  => 'numeric',
               'daily'   => 'numeric'
            ], $this->validationMessages);

         if($validator->fails()) {
             return Api::responseData([
                     'status'=>false,
                     'messages'=>$validator->errors()->all()
                 ]);
         }

         $data = array(
                    "price_daily"   => $request->input('daily'),
                    "price_weekly"  => $request->input('weekly'),
                    "price_monthly" => $request->input('monthly'),
                    "price_yearly"  => $request->input('yearly'),
                    "is_active"     => 'false',
                    "user_id"       => $this->user()->id  
            );

         $response = $this->repository->updateKost($room, $data);
         
         return Api::responseData([
                     'status'   =>  $response['status'],
                     'messages' =>  [$response['message']]
                 ]);

    }

    public function getPriceRoom (Request $request, $roomId)
    {
        $room = $this->repository->findByField('song_id', $roomId);
        
        if (count($room) > 0) $room = $room[0];
        else $room = null;

        $checkOwner = $this->repository->checkOwnership($room, $this->user());

        if ($checkOwner == false) {
            return Api::responseData([
                     'status'  => false,
                     'messages'=> [ResponseMessage::FAILED_GET_DATA_KOST],
                     'data'    => array()
                 ]);
        }

        $response = $this->repository->getPriceRoom($room);
        return Api::responseData([
                     'status'  => true,
                     'messages'=> [ResponseMessage::SUCCESS_GET_DATA_KOST],
                     'data'    => $response
                 ]);
    }

    public function updateRoomSize(Request $request, $roomId)
    {
        $room = $this->repository->findByField('song_id', $roomId);
    
        if (count($room) > 0) $room = $room[0];
        else $room = null;

        $checkOwner = $this->repository->checkOwnership($room, $this->user());

        if ($checkOwner == false) {
            return Api::responseData([
                     'status'=>false,
                     'messages'=> [ResponseMessage::FAILED_UPDATE_KOST]
                 ]);
        }

        $validator = Validator::make($request->all(),
            [
               'size'  => 'required'
            ], $this->validationMessages);

         if($validator->fails()) {
             return Api::responseData([
                     'status'=>false,
                     'messages'=>$validator->errors()->all()
                 ]);
         }

         $data = array(
                    "user_id"   => $this->user()->id,
                    "size"      => $request->input('size'),
                    "is_active" => 'false'
            );

         $response = $this->repository->updateKost($room, $data);
         
         return Api::responseData([
                     'status'   =>  $response['status'],
                     'messages' =>  [$response['message']]
                 ]);

    }

    public function getRoomSize(Request $request, $roomId)
    {
        $room = $this->repository->findByField('song_id', $roomId);
        
        if (count($room) > 0) $room = $room[0];
        else $room = null;

        $checkOwner = $this->repository->checkOwnership($room, $this->user());

        if ($checkOwner == false) {
            return Api::responseData([
                     'status'  => false,
                     'messages'=> [ResponseMessage::FAILED_GET_DATA_KOST],
                     'data'    => array()
                 ]);
        }

        return Api::responseData([
                     'status'  => true,
                     'messages'=> [ResponseMessage::SUCCESS_GET_DATA_KOST],
                     'data'    => array("size" => $room->size)
                 ]);    
    }

    public function updateBathroom(Request $request, $roomId)
    {
        $room = $this->repository->findByField('song_id', $roomId);
    
        if (count($room) > 0) $room = $room[0];
        else $room = null;

        $checkOwner = $this->repository->checkOwnership($room, $this->user());

        if ($checkOwner == false) {
            return Api::responseData([
                     'status'=>false,
                     'messages'=> [ResponseMessage::FAILED_UPDATE_KOST]
                 ]);
        }

        $validator = Validator::make($request->all(),
            [
               'fac_bath'       => 'required',
               'fac_bath_other' => 'required|max:500'
            ], $this->validationMessages);

         if($validator->fails()) {
             return Api::responseData([
                     'status'=>false,
                     'messages'=>$validator->errors()->all()
                 ]);
         }
         
         $data = array(
                   "fac_bath"       => $request->input('fac_bath'),
                   "fac_bath_other" => $request->input('fac_bath_other'),
                   "user_id"        => $this->user()->id
            );

         $response = $this->repository->updateBathroom($room, $data);

         return Api::responseData([
                     'status'   =>  $response['status'],
                     'messages' =>  [$response['message']]
                 ]);
    }

    public function getBathroom(Request $request, $roomId)
    {
       $room = $this->repository->findByField('song_id', $roomId);
    
        if (count($room) > 0) $room = $room[0];
        else $room = null;

        $checkOwner = $this->repository->checkOwnership($room, $this->user());

        if ($checkOwner == false) {
            return Api::responseData([
                     'status'=>false,
                     'messages'=> [ResponseMessage::FAILED_GET_DATA_KOST]
                 ]);
        }

        $response = $this->repository->getFacBath($room);
        return Api::responseData([
                     'status'  => true,
                     'messages'=> [ResponseMessage::SUCCESS_GET_DATA_KOST],
                     'data'    => $response
                 ]);    
    }

    public function updateFacRoom(Request $request, $roomId)
    {
        $room = $this->repository->findByField('song_id', $roomId);
    
        if (count($room) > 0) $room = $room[0];
        else $room = null;

        $checkOwner = $this->repository->checkOwnership($room, $this->user());

        if ($checkOwner == false) {
            return Api::responseData([
                     'status'=>false,
                     'messages'=> [ResponseMessage::FAILED_UPDATE_KOST]
                 ]);
        }

        $validator = Validator::make($request->all(),
            [
               'fac_room'       => 'required',
               'fac_room_other' => 'required|max:500'
            ], $this->validationMessages);

         if($validator->fails()) {
             return Api::responseData([
                     'status'=>false,
                     'messages'=>$validator->errors()->all()
                 ]);
         }
         
         $data = array(
                   "fac_room"       => $request->input('fac_room'),
                   "fac_room_other" => $request->input('fac_room_other'),
                   "user_id"        => $this->user()->id
            );

         $response = $this->repository->updateFacRoom($room, $data);

         return Api::responseData([
                     'status'   =>  $response['status'],
                     'messages' =>  [$response['message']]
                 ]);
    }

    public function getFacRoom(Request $request, $roomId)
    {
        $room = $this->repository->findByField('song_id', $roomId);
    
        if (count($room) > 0) $room = $room[0];
        else $room = null;

        $checkOwner = $this->repository->checkOwnership($room, $this->user());

        if ($checkOwner == false) {
            return Api::responseData([
                     'status'=>false,
                     'messages'=> [ResponseMessage::FAILED_GET_DATA_KOST]
                 ]);
        }

        $response = $this->repository->getFacRoom($room);
        return Api::responseData([
                     'status'  => true,
                     'messages'=> [ResponseMessage::SUCCESS_GET_DATA_KOST],
                     'data'    => $response
                 ]);    
    }

    public function updateFacShare(Request $request, $roomId)
    {
        $room = $this->repository->findByField('song_id', $roomId);
    
        if (count($room) > 0) $room = $room[0];
        else $room = null;

        $checkOwner = $this->repository->checkOwnership($room, $this->user());

        if ($checkOwner == false) {
            return Api::responseData([
                     'status'=>false,
                     'messages'=> [ResponseMessage::FAILED_UPDATE_KOST]
                 ]);
        }

        $validator = Validator::make($request->all(),
            [
               'fac_share'       => 'required',
               'fac_share_other' => 'required|max:500'
            ], $this->validationMessages);

         if($validator->fails()) {
             return Api::responseData([
                     'status'=>false,
                     'messages'=>$validator->errors()->all()
                 ]);
         }
         
         $data = array(
                   "fac_share"       => $request->input('fac_share'),
                   "fac_share_other" => $request->input('fac_share_other'),
                   "user_id"         => $this->user()->id
            );

         $response = $this->repository->updateFacShare($room, $data);

         return Api::responseData([
                     'status'   =>  $response['status'],
                     'messages' =>  [$response['message']]
                 ]);
    }

    public function getFacShare(Request $request, $roomId)
    {
        $room = $this->repository->findByField('song_id', $roomId);
    
        if (count($room) > 0) $room = $room[0];
        else $room = null;

        $checkOwner = $this->repository->checkOwnership($room, $this->user());

        if ($checkOwner == false) {
            return Api::responseData([
                     'status'=>false,
                     'messages'=> [ResponseMessage::FAILED_GET_DATA_KOST]
                 ]);
        }

        $response = $this->repository->getFacShare($room);
        return Api::responseData([
                     'status'  => true,
                     'messages'=> [ResponseMessage::SUCCESS_GET_DATA_KOST],
                     'data'    => $response
                 ]);    
    }

    public function updateParking(Request $request, $roomId)
    {
        $room = $this->repository->findByField('song_id', $roomId);
    
        if (count($room) > 0) $room = $room[0];
        else $room = null;

        $checkOwner = $this->repository->checkOwnership($room, $this->user());

        if ($checkOwner == false) {
            return Api::responseData([
                     'status'=>false,
                     'messages'=> [ResponseMessage::FAILED_UPDATE_KOST]
                 ]);
        }

        $validator = Validator::make($request->all(),
            [
               'fac_park'       => 'required'
            ], $this->validationMessages);

         if($validator->fails()) {
             return Api::responseData([
                     'status'=>false,
                     'messages'=>$validator->errors()->all()
                 ]);
         }
         
         $data = array(
                   "fac_park"       => $request->input('fac_park'),
                   "user_id"        => $this->user()->id
            );

         $response = $this->repository->updateParking($room, $data);

         return Api::responseData([
                     'status'   =>  $response['status'],
                     'messages' =>  [$response['message']]
                 ]);
    }

    public function getParking(Request $request, $roomId)
    {
        $room = $this->repository->findByField('song_id', $roomId);
    
        if (count($room) > 0) $room = $room[0];
        else $room = null;

        $checkOwner = $this->repository->checkOwnership($room, $this->user());

        if ($checkOwner == false) {
            return Api::responseData([
                     'status'=>false,
                     'messages'=> [ResponseMessage::FAILED_GET_DATA_KOST]
                 ]);
        }

        $response = $this->repository->getParking($room);
        return Api::responseData([
                     'status'  => true,
                     'messages'=> [ResponseMessage::SUCCESS_GET_DATA_KOST],
                     'data'    => $response
                 ]);    
    }

    public function updateFacNear(Request $request, $roomId)
    {
        $room = $this->repository->findByField('song_id', $roomId);
    
        if (count($room) > 0) $room = $room[0];
        else $room = null;

        $checkOwner = $this->repository->checkOwnership($room, $this->user());

        if ($checkOwner == false) {
            return Api::responseData([
                     'status'=>false,
                     'messages'=> [ResponseMessage::FAILED_UPDATE_KOST]
                 ]);
        }

        $validator = Validator::make($request->all(),
            [
               'fac_near'       => 'required',
               'fac_near_other' => 'required|max:500'
            ], $this->validationMessages);

         if($validator->fails()) {
             return Api::responseData([
                     'status'=>false,
                     'messages'=>$validator->errors()->all()
                 ]);
         }
         
         $data = array(
                   "fac_near"       => $request->input('fac_near'),
                   "fac_near_other" => $request->input('fac_near_other'),
                   "user_id"        => $this->user()->id
            );

         $response = $this->repository->updateFacNear($room, $data);

         return Api::responseData([
                     'status'   =>  $response['status'],
                     'messages' =>  [$response['message']]
                 ]);
    }

    public function updateOtherInformation(Request $request, $roomId)
    {
        $room = $this->repository->findByField('song_id', $roomId);
    
        if (count($room) > 0) $room = $room[0];
        else $room = null;

        $checkOwner = $this->repository->checkOwnership($room, $this->user());

        if ($checkOwner == false) {
            return Api::responseData([
                     'status'=>false,
                     'messages'=> [ResponseMessage::FAILED_UPDATE_KOST]
                 ]);
        }

        $validator = Validator::make($request->all(),
            [
               'remarks'       => 'required|max:2000',
               'description'   => 'required|max:1000',
               'price_remark'  => 'required|max:500' 
            ], $this->validationMessages);

         if($validator->fails()) {
             return Api::responseData([
                     'status'=>false,
                     'messages'=>$validator->errors()->all()
                 ]);
         }
         
         $data = array(
                   "remarks"        => $request->input('remarks'),
                   "description"    => $request->input('description'),
                   "price_remark"   => $request->input('price_remark'),
                   "user_id"        => $this->user()->id
            );

         $response = $this->repository->updateOtherInformation($room, $data);

         return Api::responseData([
                     'status'   =>  $response['status'],
                     'messages' =>  [$response['message']]
                 ]);        
    }

    public function getOtherInformation(Request $request, $roomId)
    {
        $room = $this->repository->findByField('song_id', $roomId);
        
        if (count($room) > 0) $room = $room[0];
        else $room = null;

        $checkOwner = $this->repository->checkOwnership($room, $this->user());

        if ($checkOwner == false) {
            return Api::responseData([
                     'status'  => false,
                     'messages'=> [ResponseMessage::FAILED_GET_DATA_KOST],
                     'data'    => array()
                 ]);
        }
        
        $response = $this->repository->getOtherInformation($room);  
        return Api::responseData([
                     'status'  => true,
                     'messages'=> [ResponseMessage::SUCCESS_GET_DATA_KOST],
                     'data'    => $response
                 ]);    
    }

    public function getFacNear(Request $request, $roomId)
    {
        $room = $this->repository->findByField('song_id', $roomId);
        
        if (count($room) > 0) $room = $room[0];
        else $room = null;

        $checkOwner = $this->repository->checkOwnership($room, $this->user());

        if ($checkOwner == false) {
            return Api::responseData([
                     'status'  => false,
                     'messages'=> [ResponseMessage::FAILED_GET_DATA_KOST],
                     'data'    => array()
                 ]);
        }
        
        $response = $this->repository->getFacNear($room);  
        return Api::responseData([
                     'status'  => true,
                     'messages'=> [ResponseMessage::SUCCESS_GET_DATA_KOST],
                     'data'    => $response
                 ]);    
    }

}