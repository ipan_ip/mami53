<?php

namespace App\Http\Controllers\Api;

use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Http\Helpers\ApiResponse;
use Carbon\Carbon;

class OwnerInformationController extends BaseController
{
    public function index()
    {
        $user = $this->user();

        if (is_null($user) || !$user->is_owner) {
            return ApiResponse::responseError(false, false, 401, 401);
        }

        if (Carbon::now()->lessThan(Carbon::create(2020, 8, 7, 0, 0, 0))) {
            $bbkCharging = $this->bbkChargingBefore7Aug($user->id);
        } else {
            $bbkCharging = $this->bbkCharging($user->id);
        }

        return ApiResponse::responseData([
            'announcement' => [
                'bbk_charging' => $bbkCharging,
            ]
        ]);
    }

    private function bbkCharging(int $userId): bool
    {
        $chargedRooms = Room::active()
        ->availableBooking()
        ->whereHas('owners', function ($query) use ($userId) {
            $query->where('user_id', $userId)
                ->where('status', RoomOwner::ROOM_VERIFY_STATUS);
        })
        ->count();

        return $chargedRooms > 0;
    }

    // temporary, used in 1-6 aug 2020
    private function bbkChargingBefore7Aug(int $userId): bool
    {
        $chargedRooms = Room::whereHas('owners', function ($query) use ($userId) {
            $query->where('user_id', $userId);
        })
        ->whereHas('level', function ($query) {
            $query->where('name', 'Booking Fee');
        })
        ->count();

        return $chargedRooms > 0;
    }
}
