<?php 

namespace App\Http\Controllers\Api;

use App\Entities\ShortlinkGeneral\ShortlinkGeneral;
use App\Http\Helpers\ApiResponse;
use Illuminate\Http\Request;
use App\Http\Requests;

class ShortlinkController extends BaseController
{

    public function storeShortLinkGeneral(Request $request)
    {

        $shortUrl  = env('SHORT_URL', 'https://mamikos.com/s/').substr(hash('sha512', time()), 0, 5);

        $credentials = [
            'short_url'    => $shortUrl,
            'original_url' => $request->url
        ];

        $storeShortUrl = ShortlinkGeneral::create($credentials);

        if ($storeShortUrl) {
            return ApiResponse::responseData([
                'status'  => true,
                'meta'    => [
                    'message' => 'Successfully created short link general'
                ]
            ]);

        } else {
            return ApiResponse::responseData([
                'status'  => false,
                'meta'    => [
                    'message' => 'Failed created short link general'
                ]
            ]);
        }
    }

    public function storeCustomShortlinkGeneral(Request $request) 
    {
        $shortUrl  = env('SHORT_URL', 'https://mamikos.com/s/');

        $urlCustom      = $request->url_custom;
        $shortUrlCustom = $shortUrl.$urlCustom;
        $checkUrlCustom = ShortlinkGeneral::where('short_url', $shortUrlCustom)->first();
        
        if ($checkUrlCustom) {
            return ApiResponse::responseData([
                'status'  => false,
                'meta'    => [
                    'message' => 'Url has registered'
                ]
            ]);

        } else {
            $credentials = [
                'short_url'     => $shortUrlCustom,
                'original_url'  => $request->original_url
            ];
    
            $storeShortUrl = ShortlinkGeneral::create($credentials);

            if ($storeShortUrl) {
                return ApiResponse::responseData([
                    'status' => true,
                    'meta'   => [
                        'message' => 'Successfully created short link general'
                    ]
                ]);
    
            } else {
                return ApiResponse::responseData([
                    'status' => false,
                    'meta'   => [
                        'message' => 'Failed created short link general'
                    ]
                ]);
            }
        }

    }

    public function redirectUrl($string)
    {
        $checkUrlValid = ShortlinkGeneral::where('short_url', $string)->first();

        if ($checkUrlValid) {
            return redirect($checkUrlValid->original_url);

        } else {
            return redirect('/');

        }
    }
}
