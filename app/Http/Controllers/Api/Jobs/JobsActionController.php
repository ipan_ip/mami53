<?php

namespace App\Http\Controllers\Api\Jobs;

use App\Http\Controllers\Api\BaseController;
use App\Entities\Landing\LandingVacancy;
use App\Entities\Vacancy\Vacancy;
use App\Entities\Vacancy\VacancyApply;
use App\Entities\Tracker\InputAdsTracker;
use App\Http\Helpers\ApiResponse as Api;
use App\Http\Helpers\UtilityHelper;
use App\Repositories\Jobs\JobsRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class JobsActionController extends BaseController
{
    const HARUS_LOGIN = 'Anda belum login. Silahkan login terlebih dahulu.';
    const LOWONGAN_TIDAK_DITEMUKAN = 'Saat ini lowongan pekerjaan tidak ditemukan.';
    const SUDAH_KIRIM_CV = 'Anda sudah mengirim cv untuk lowongan ini.';

    protected $repository;

    public $validationMessages = [
        'place_name.required'    => 'Nama kantor tidak boleh kosong',
        'place_name.max'         => 'Nama kantor mak :max karakter',
        'latitude.required'      => 'Alamat tidak boleh kosong',
        'longitude.required'     => 'Alamat tidak boleh kosong',
        'address.required'       => 'Alamat tidak boleh kosong',
        'jobs_name.required'     => 'Nama pekerjaan tidak boleh kosong',
        'jobs_name.max'          => 'Nama pekerjaan max :max karakter',
        'jobs_position.max'      => 'Posisi pekerjaan mak :max karakter',
        'jobs_position.required' => 'Posisi pekerjaan tidak boleh kosong',
        'jobs_type.required'     => 'Status pekerjaan harus dipilih',
        'jobs_type.in'           => 'Status pekerjaan tidak tersedia',
        'jobs_description.required' => 'Deskripsi pekerjaan tidak boleh kosong',
        'jobs_salary.required'      => 'Gaji tidak boleh kosong',
        'jobs_salary.numeric'       => 'Gaji harus berupa angka',
        'jobs_max_salary.required'  => 'Gaji tidak boleh kosong',
        'jobs_max_salary.numeric'       => 'Gaji harus berupa angka',
        'insert_username.required'  => 'Nama penanggung jawab tidak boleh kosong',
        'insert_phone.required'     => 'Nomer hp tidak boleh kosong',
        'insert_email.required'     => 'Email tidak boleh kosong',
        'jobs_salary_time.required' => 'Waktu pembayaran tidak boleh kosong',
        'jobs_salary_time.in'       => 'Waktu pembayaran tidak tersedia',
        'cv.required'               => 'File tidak boleh kosong',
        'cv.max'                    => 'Ukuran file max 2MB',
        'cv.mimes'                  => 'Hanya diperbolehkan file tipe pdf',
        'user.required'             => 'Nama user tidak boleh kosong',
        'phone.required'            => 'No hp tidak boleh kosong',
        'email.required'            => 'Email tidak boleh kosong',
        'identifier.required'       => 'Data pekerjaan tidak ditemukan',
        'education.required'        => 'Pendidikan tidak boleh kosong',
        'skill.required'            => 'Kolom kemampuan / skill tidak boleh kosong',
        'job_experience.required'   => 'Kolom pengalaman kerja tidak boleh kosong',
        'last_salary.required'      => 'Penghasilan terakhir tidak boleh kosong',
        'last_salary.numeric'       => 'Penghasilan terkhir harus berupa angka',
        'expectation_salary.required' => 'Kolom gaji yang diinginkan tidak boleh kosong',
        'expectation_salary.numeric'  => 'Kolom gaji yang diinginkan harus berupa angka',
        'verification_status.required'=> 'Status verifikasi tidak sesuai',
        'verification_for.required'   => 'Verifikasi tidak boleh kosong',
        'photo_id.numeric'          => 'Format ID foto tidak valid',
        'expired_date.required_if'  => 'Tanggal Lowongan Ditutup harus diisi',
        'expired_date.date_format'  => 'Format Tanggal Lowongan Ditutup tidak valid (YYYY-MM-DD)'
    ];

    public function __construct(JobsRepository $repository)
    {
        $this->repository = $repository;

        parent::__construct();
    }

    public function addNewJobs(Request $request) 
    {
    	 $validator = Validator::make($request->all(), 
            [
                'place_name'   => 'required|max:190',
                'latitude'     => 'required',
                'longitude'    => 'required',
                'address'      => 'required',
                'jobs_name'    => 'required|max:150',
                //'jobs_position'=> 'required|max:100',
                'jobs_type'    => 'required|in:'.implode(",", Vacancy::VACANCY_TYPE_TYPE),
                'jobs_description' => 'required',
                'jobs_salary'      => 'required|numeric',
                'jobs_salary_time' => 'required|in:'.implode(",", Vacancy::SALARY_TIME),
                'insert_username'  => 'required',
                'insert_phone'     => 'required',
                'insert_email'     => 'required',
                'jobs_max_salary'  => 'required|numeric',
                'last_education'   => 'nullable',
                'photo_id'         => 'nullable|numeric',
                'with_expired_date'=> 'nullable',
                'expired_date'     => 'required_if:with_expired_date,1|multi_date_format:Y-m-d,Y-m-j,Y-n-d,Y-n-j'
            ], $this->validationMessages);

        if($validator->fails()) {
            return Api::responseData([
                    'status'=>false,
                    'messages'=>$validator->errors()->all()
                ]);
        }

        $response = $this->repository->insertNewJobs($this->user(), $request->all());

        return Api::responseData($response);
    }

    public function getLandingJobs($slug)
    {
        $landingFirst = LandingVacancy::where('slug', $slug)->first();
        $breadcrumb = ["breadcrumb" => $landingFirst->getBreadcrumb()];
        $landing = $landingFirst->toArray();
        //dd($landing);
        $location = [[$landing['longitude_1'], $landing['latitude_1']], [$landing['longitude_2'], $landing['latitude_2'] ]];
        $centerPoint = UtilityHelper::getCenterPoint($location);
        $centerPointName = ["longitude" => $centerPoint[0], "latitude" => $centerPoint[1]];

        $landing = array_merge($landing, $centerPointName);
        $data = array("landing" => array_merge($landing, $breadcrumb));
        return Api::responseData($data);
    }

    public function addFile(Request $request)
    {
        $user = $this->user();
        if (is_null($user)) {
            return Api::responseData([
                'status'   => false,
                'messages' => self::HARUS_LOGIN
            ]);
        }

        $validator = Validator::make(
            $request->all(),
            [
                'cv' => 'required|mimes:pdf',
            ],
            $this->validationMessages
        );

        if ($validator->fails()) {
            return Api::responseData([
                'status'   => false,
                'messages' => $validator->errors()->all()
            ]);
        }

        // validation max file size upload
        $cv = $request->file('cv');
        $size = number_format($cv->getSize() / 1048576);
        if ($size > 2) {
            return Api::responseData([
                'status'   => false,
                'messages' => ['Ukuran file max 2MB']
            ]);
        }

        $response = $this->repository->uploadCv($request->all(), $user);

        return Api::responseData(["name" => $response, "status" => true]);
    }

    public function addCv(Request $request)
    {
        $user = $this->user();
        if (is_null($user)) {
            return Api::responseData([
                'status'   => false,
                'messages' => self::HARUS_LOGIN
            ]);
        }

        $validator = Validator::make(
            $request->all(),
            [
                'user'               => 'required',
                'phone'              => 'required',
                'address'            => 'required',
                'identifier'         => 'required',
                'education'          => 'required',
                'skill'              => 'required',
                'job_experience'     => 'required',
                'last_salary'        => 'required|numeric',
                'expectation_salary' => 'required|numeric',
                'cv'                 => 'nullable|max:5000'
            ],
            $this->validationMessages
        );

        $vacancyApply = $this->repository->getVacancyApplyByVacancyId($request, $user);
        $vacancy = $this->repository->getVacancyById($request);

        $validator->after(function ($validator) use ($vacancyApply, $vacancy) {
            if (is_null($vacancy)) {
                $validator->errors()->add('messages', self::LOWONGAN_TIDAK_DITEMUKAN);
            }

            if ($vacancyApply > 0) {
                $validator->errors()->add('messages', self::SUDAH_KIRIM_CV);
            }
        });

        if( $validator->fails() ) {
            return Api::responseData([
                'status'   => false,
                'messages' => $validator->errors()->all()
            ]);
        }

        $response = $this->repository->jobsApply($request->all(), $this->user(), $vacancy);

        if ($request->filled('utm')) {
            InputAdsTracker::saveFromCampaignQuery(
                $request->utm, $this->user(), $response['data']['seq'], 'apply'
            );
        }

        return Api::responseData($response);
    }

    public function editData(Request $request, $id)
    {
        $response = $this->repository->edit($id, $this->user());

        return Api::responseData($response);
    }

    public function postEditData(Request $request, $id)
    {
        $validator = Validator::make($request->all(), 
        [
            'place_name'   => 'required|max:190',
            'latitude'     => 'required',
            'longitude'    => 'required',
            'address'      => 'required',
            'jobs_name'    => 'required|max:150',
            //'jobs_position'=> 'required|max:100',
            'jobs_type'    => 'required|in:'.implode(",", Vacancy::VACANCY_TYPE_TYPE),
            'jobs_description' => 'required',
            'jobs_salary'      => 'required|numeric',
            'jobs_salary_time' => 'required|in:'.implode(",", Vacancy::SALARY_TIME),
            'insert_username'  => 'required',
            'insert_phone'     => 'required',
            'insert_email'     => 'required',
            'jobs_max_salary'  => 'required|numeric',
            'last_education'   => 'nullable',
            'photo_id'         => 'nullable|numeric',
            'with_expired_date'=> 'nullable',
            'expired_date'     => 'required_if:with_expired_date,1|multi_date_format:Y-m-d,Y-m-j,Y-n-d,Y-n-j'
        ], $this->validationMessages);

        if($validator->fails()) {
            return Api::responseData([
                    'status'=>false,
                    'messages'=>$validator->errors()->all()
                ]);
        }

        $response = $this->repository->updateJobs($this->user(), $request->all(), $id);

        return Api::responseData($response);
    }

    public function deactiveJobsOwner(Request $request, $slug)
    {
        $responseError = ["status" => false, "meta" => ["message" => "Harus login terlebih dahulu"]];
        if (is_null($this->user())) return Api::responseData($responseError);
        $response = $this->repository->deactiveJobsOwner($slug, $this->user());

        if (!$response) return Api::responseData($responseError);

        return Api::responseData(["status" => $response]);
    }

    public function activeJobsOwner(Request $request, $slug)
    {
        $responseError = ["status" => false, "meta" => ["message" => "Harus login terlebih dahulu"]];
        if (is_null($this->user())) return Api::responseData($responseError);
        $response = $this->repository->activeJobsOwner($slug, $this->user());
        
        return Api::responseData([
                        "status" => $response['status'], 
                        "meta" => [
                            "message" => $response['message']
                        ]
                    ]);   
    }

    public function deleteJobsOwner(Request $request, $slug)
    {
        $responseError = ["status" => false, "meta" => ["message" => "Harus login terlebih dahulu"]];
        if (is_null($this->user())) return Api::responseData($responseError);
        $response = $this->repository->deleteJobsOwner($slug, $this->user());

        if (!$response) return Api::responseData($responseError);

        return Api::responseData(["status" => $response]);   
    }

    public function verificationFromUser(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'verification_status' => 'required',
                'verification_for'    => 'required'
            ], $this->validationMessages);

        if($validator->fails()) {
            return Api::responseData([
                    'status'=>false,
                    'messages'=>$validator->errors()->all()
                ]);
        }

        $response = $this->repository->deleteFromOwner($request->all());

        return Api::responseData($response);
    }

    public function updateCv(Request $request, $id)
    {
        $validator = Validator::make($request->all(),[
            'user'      => 'required',
            'phone'     => 'required',
            'address'   => 'required',
            'education' => 'required',
            'skill'     => 'required',
            'job_experience' => 'required',
            'last_salary'    => 'required|numeric',
            'expectation_salary' => 'required|numeric',
            'cv'        => 'nullable|max:5000',
        ], $this->validationMessages);

        $vacancyApply = VacancyApply::where('id', $id)->where('user_id', $this->user()->id)->count();

        $validator->after(function ($validator) use($vacancyApply) {
            if ($vacancyApply < 1) {
                $validator->errors()->add('messages', 'Data tidak ditemukan.');
            }
        });

        if($validator->fails()) {
            return Api::responseData([
                    'status'   => false,
                    'messages' => $validator->errors()->all()
                ]);
        }

        $response = $this->repository->updateApply($request->all(), $this->user(), $id);

        return Api::responseData($response);
    }
}
