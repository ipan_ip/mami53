<?php

namespace App\Http\Controllers\Api\Jobs\Company;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\BaseController;
use App\Http\Helpers\ApiResponse as Api;
use App\Repositories\Jobs\Company\CompanyRepository;
use App\Entities\Vacancy\CompanyFilters;

class ProfileController extends BaseController
{
    protected $repository;

    public function __construct(CompanyRepository $repository)
    {
        $this->repository = $repository;

        parent::__construct();
    }

    public function postListCompanyProfile(Request $request)
    {
    	$this->repository->setPresenter(new \App\Presenters\CompanyPresenter('list'));

        $filters                = $request->input('filters');
        //$filters['location']    = $request->input('location');
        //dd($filters);
        $this->repository->pushCriteria(new \App\Criteria\Jobs\CompanyMainFilterCriteria($filters));

        $response = $this->repository->getItemList(new CompanyFilters($filters));
        return Api::responseData($response);
    }

    public function getDetailProfile(Request $request, $slug)
    {
    	$this->repository->setPresenter(new \App\Presenters\CompanyPresenter(null,$this->user()));

        $Company = $this->repository->with('vacancy', 'industry', 'photo')->findByField('slug', $slug);
		if (count($Company["data"]) == 0) $Company = null;
		else $Company = (object) $Company["data"][0];
        return Api::responseData(["data" => $Company]);
    }
}
