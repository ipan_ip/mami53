<?php

namespace App\Http\Controllers\Api\Jobs;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseController;
use App\Http\Helpers\ApiResponse as Api;
use App\Repositories\Jobs\JobsRepository;
use Validator;
use App\Http\Helpers\UtilityHelper;
use App\Entities\Landing\LandingVacancy;
use App\Entities\Vacancy\VacancyFilters;
use App\Entities\Vacancy\VacancyCluster;
use App\Entities\Vacancy\VacancySearchHistoryTemp;
use Session;
use App\Entities\Activity\ViewLandingTemp;
use App\Entities\Room\Room;
use App\Entities\Vacancy\Vacancy;

class JobsViewController extends BaseController
{

	protected $repository;

	public function __construct(JobsRepository $repository)
    {
        $this->repository = $repository;

        parent::__construct();
    }

    public function detailJobs(Request $request, $slug)
    {
    	$response = $this->repository->getDetailJobs($slug, $this->user(), 'app');

    	return Api::responseData($response);
    }

    public function jobsListCluster(Request $request)
    {
        ini_set('max_execution_time', 100000);
        $filter  = $request->input('filters');

        $filter['location']     = $request->input('location');

        if ($filter['location'] != null)
        {
            $filter['titik_tengah'] = UtilityHelper::getCenterPoint($filter['location']);
        }

        $response = $this->repository->getClusterList($filter);

        (new VacancySearchHistoryTemp)->storeHistoryFilter($filter, count($response['vacancy']) == 0, $this->user(), $this->device(), Session::getId());

        return Api::responseData($response);
    }

    public function jobsListSearch(Request $request)
    {
        $this->repository->setPresenter(new \App\Presenters\JobsPresenter('list', $this->user()));

        // Determine filters.
        $filters                = $request->input('filters');
        $filters['location']    = $request->input('location');
        $filters['sorting']     = $request->input('sorting');
        
        if ($filters['location'] != null)
        {
            $filters['titik_tengah'] = UtilityHelper::getCenterPoint($filters['location']);
        }

        $this->repository->pushCriteria(new \App\Criteria\Jobs\MainFilterCriteria($filters));

        $response = $this->repository->getJobsItemList(new VacancyFilters($filters));

        (new VacancySearchHistoryTemp)->storeHistoryFilter($filters, count($response['vacancy']) == 0, $this->user(), $this->device(), Session::getId());
        
        return Api::responseData($response);
    }

    public function ownerList(Request $request)
    {
        if (is_null($this->user())) return Api::responseData(["status" => false]);

        $data = $this->repository->getListJobs($this->user());
        
        return Api::responseData($data);    
    }

    public function ownerJobsDetail(Request $request, $slug)
    {
        if (is_null($this->user())) return Api::responseData(["status" => false]);
        $data = $this->repository->getDetailJobsOwner($this->user(), $slug);
        return Api::responseData($data);
    }

    public function ownerJobsReport(Request $request, $slug)
    {
        $response = $this->repository->ownerJobsReport($request->all(), $slug);

        return Api::responseData($response);
    }

    public function singleCluster(Request $request)
    {
        $location = $request->input('location');
        $point = $request->input('point');

        $location = VacancyCluster::getRangeSingleCluster($location, $point, 7);
        
        $sort = 'new';
        $sortingArray = ["new" => "desc", "old" => "asc"];
        if ($request->filled('sorting') AND in_array($request->input('sorting'), array_keys($sortingArray))) {
            $sorting = $request->input('sorting');
            $sorting = $sortingArray[$sorting]; 
            if (isset($sorting['direction']) AND in_array($sorting['direction'], ['asc', 'desc', 'rand'])) $sort = $sorting['direction']; 
        }

        $request->merge(array("sorting"  => array('field'=>'price','direction'=> $sort)));
        $request->merge(array("location" => $location));

        return $this->jobsListSearch($request);
    }

    public function getListApply(Request $request, $id)
    {
        $user = $this->user();
        if (is_null($user)) Api::responseData(["status" => false]);

        $response = $this->repository->listApplyOwner($this->user(), $id);
        return Api::responseData($response);
    }

    public function detailApply(Request $request, $id)
    {
        $user = $this->user();
        if (is_null($user)) Api::responseData(["status" => false]);

        $response = $this->repository->detailApply($id);
        return Api::responseData($response);
    }

    public function getDataCV(Request $request, $id)
    {
        $user = $this->user();
        if (is_null($user)) Api::responseData(['status' => false]);

        $response = $this->repository->getDataCv($user, $id);
        return Api::responseData($response);
    }

    public function viewCount(Request $request, $id)
    {
        // LandingVacancy::find($id)->increment('view_count');
        
        $viewTemp = new ViewLandingTemp;
        $viewTemp->identifier_type = 'landing_vacancy';
        $viewTemp->identifier = $id;
        $viewTemp->save();

        return Api::responseData([
            'status' => true
        ]);
    }
}