<?php
namespace App\Http\Controllers\Api;

use App\Http\Helpers\LocationHelper;
use App\Presenters\RoomPresenter;
use App\Services\Booking\BookingDraftService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Jenssegers\Agent\Agent;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Response;
use RuntimeException;
use Session;
use Carbon\Carbon;
use Cache;

use App\Entities\Activity\Call;
use App\Entities\Activity\Question;
use App\Entities\Activity\Tracking;
use App\Entities\Activity\TrackingFeature;
use App\Entities\Activity\ChatAdmin;
use App\Entities\DownloadExam\DownloadExamInput;
use App\Entities\Geocode\GeocodeCache;
use App\Entities\Recommendation\RecommendationUser;
use App\Entities\Room\Room;
use App\Entities\Room\RoomCluster;
use App\Entities\Room\RoomFilter;
use App\Entities\User\UserFindRoom;
use App\Entities\User\UserSearchHistoryTemp;
use App\Entities\Landing\Landing;
use App\Entities\Fake\Faker;
use App\Entities\Config\AppConfig;
use App\Entities\Premium\AdHistory;
use App\Libraries\RequestHelper;
use App\Enums\Room\PropertyType;
use App\Http\Helpers\ApiHelper;
use App\Http\Helpers\ApiResponse as Api;
use App\Http\Helpers\OpenSslEncryptor;
use App\Http\Helpers\UtilityHelper;
use App\Http\Helpers\RatingHelper;
use App\Libraries\CacheHelper;
use App\Presenters\CardPresenter;
use App\Presenters\CardPremiumPresenter;
use App\Repositories\Card\CardRepository;
use App\Repositories\CardPremium\CardPremiumRepository;
use App\Repositories\RoomRepository;
use App\Services\Sendbird\GroupChannelService;
use App\Validators\RoomValidator;

use App\Http\Helpers\GoldplusPermissionHelper;
use App\Enums\Goldplus\Permission\GoldplusPermission;
use App\Entities\Level\KostLevelMap;
use App\Entities\Level\KostLevel;
use App\Jobs\KostIndexer\IndexKostJob;
use App\Libraries\KostSearch\KostSearcher;
use App\Http\Controllers\Api\PropertySearchController;

class RoomController extends BaseController
{
    /**
     * @var RoomRepository
     */
    protected $repository;

    /**
     * @var RoomValidator
     */
    protected $validator;

    // instance of App\Services\Sendbird\GroupChannelService
    protected $groupChnlSvc;

    // Cache Time for /garuda/stories/list
    const CACHE_TIME_LIST = 5;

    //Default owner_id for excluding owner in chat room (especially for GP3 and GP4 kost)
    const DEFAULT_EXCLUDED_OWNER_ID = '';

    //Default GP3 group ID
    const GP3_GROUP_ID = 3;

    /**
     * Antiscraping Encryption Information
     * key and iv for openssl encryption/decryption
     * DON'T REUSE THIS for other purpose except for rooms of /garuda/stories/list
     * This key and iv will be exposed to public via Front-end, it means it has low security
     */
    const ANTISCRAPING_ENCRYPTION = [
        'encryptable_routes' => [ '/garuda/stories/list' , '/garuda/stories/list2' ],
        'key' => '39c852d0d0bc42ef83f7d3d708f42368', 
        'iv' => '5df5a10ebb035097'
    ];

    public function __construct(
        RoomRepository $repository,
        RoomValidator $validator,
        GroupChannelService $groupChnlSvc
    )
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->groupChnlSvc = $groupChnlSvc;

        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->setPresenter(app('App\Presenters\RoomPresenter'));

        $stories = $this->repository->paginate(20);

        return Api::responseData(compact('stories'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $songId)
    {
        $cacheEnabled = config('cache.api_kost_detail_enabled');
        if ($cacheEnabled === true)
        {
            // Checking Cache
            $cacheKey = CacheHelper::getKeyFromRequest($request);
            $cachedResponse = Cache::get($cacheKey);
            if (is_null($cachedResponse) === false)
            {
                return $cachedResponse;
            }
        }

        $this->repository->setPresenter(new RoomPresenter);

        $room = $this
            ->repository
            ->with([
                'minMonth',
                'survey',
                'love',
                'tags.photo',
                'tags.photoSmall',
                'tags.types',
                'review',
                'avg_review',
                'owners',
                'level',
                'booking_acceptance_rate',
                'booking_owner_requests',
                'mamipay_price_components'
            ])
            ->findByField('song_id', $songId);

        $data = collect($room['data'])->first();
        if (
            $request->filled('v') 
            && ($request->input('v') == 2) 
            && (!is_null($data))
        ) {
            $room = collect($room['data'])->first();
            $data = $this->checkPrice($room);
        }

        try {
            // Faker check
            $currentUser = $this->user();
            if (is_null($currentUser) == false) {
                if (Faker::GetIsFaker($currentUser->id)) {
                    // handle faker (do not allow to invite owner into chat room)
                    $data['enable_chat'] = false;
                }

                // checking if room have feature booking, save to user draft last seen
                $bookingDraftService = new BookingDraftService();
                $bookingDraftService->saveRecentRoomViewed($songId, $currentUser);
            }
        } catch (\Exception $exception) {
            Bugsnag::notifyException($exception);
        }

        //Field song_id is always has interger type value
        //------------------------------------------------------------------
        //Tested in tinker
        //
        //>>> $songId = "123A";
        //=> "123A"
        //>>> if (false === is_int($songId)) {
        //...    throw new \RuntimeException('Invalid parameter value.');
        //... }
        //RuntimeException with message 'Invalid parameter value.'
        if (false === is_int(intval($songId))) {
            throw new \RuntimeException('Invalid parameter value.');
        }

        //Then, casting to intval should be no problem
        $songIdInt = (int) $songId;

        if (false === $this->isIncludedOwnerAllowed($songIdInt)) {
            $data['owner_id'] = self::DEFAULT_EXCLUDED_OWNER_ID;
        }
        
        $responseModel = ['story' => $data];
        // Put Cache
        if ($cacheEnabled === true)
        {
            $responseModelForCache = array_merge(['cached_at' => Carbon::now()->toIso8601String()], $responseModel);
            Cache::add($cacheKey, Api::responseData($responseModelForCache), config('cache.api_kost_detail_time'));
        }
        return Api::responseData($responseModel);
    }

    /**
     * Check is included owner is allowed in chatroom
     * 
     * @param int $songId
     * @return boolean
     */
    private function isIncludedOwnerAllowed(int $songId): bool
    {
        $kostLevelMap  = KostLevelMap::select('level_id')
            ->where('kost_id', $songId)->first();

        $levelId = 0;
        if (!empty($kostLevelMap->level_id)) {
            $levelId = $kostLevelMap->level_id;
        }

        $isIncludeOwnerInChatAllowed = GoldplusPermissionHelper::isAllow(
            $levelId,
            GoldplusPermission::CHAT_READ
        );

        return $isIncludeOwnerInChatAllowed;
    }

    public function checkPrice($data)
    {
        if (empty($data['price_monthly_time'])) {
            $data['price_monthly_time'] = "-/Bulan";
        }

        if (empty($data['price_yearly_time'])) {
            $data['price_yearly_time'] = "-/Tahun";
        }

        if (empty($data['price_weekly_time'])) {
            $data['price_weekly_time'] = "-/Minggu";
        }

        if (empty($data['price_daily_time'])) {
            $data['price_daily_time'] = "-/Hari";
        }

        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $slug
     * @return \Illuminate\Http\Response
     */
    public function showBySlug(Request $request, $slug)
    {
        $this->repository->setPresenter(new RoomPresenter);

        $room = $this->repository->with(['tags.types', 'review'])->findRoomBySlug($slug);

        if ($room == null) {
            return Api::responseData(array('story' => null));
        }

        $songId = $room->song_id;

        $currentUser = $this->user();
        if (is_null($currentUser) == false) {
            // checking if room have feature booking, save to user draft last seen
            $bookingDraftService = new BookingDraftService();
            $bookingDraftService->saveRecentRoomViewed(optional($room)->song_id, $currentUser);
        }

        $room = $this->repository->with(['level'])->findByField('song_id', $room->song_id);
        $story = collect($room['data'])->first();

        if (false === $this->isIncludedOwnerAllowed($songId)) {
            $story['owner_id'] = self::DEFAULT_EXCLUDED_OWNER_ID;
        }
        
        return Api::responseData(array('story' => $story));
    }

    /**
     * Showing list of grid made by clustering.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function listCluster(Request $request)
    {
        ini_set('max_execution_time', 100000);

        RequestHelper::optimizeLocation($request);
        $filter = $request->input('filters');
        
        $cacheEnabled = config('cache.api_cluster_enabled');
        if ($cacheEnabled === true) 
        {
            // Checking Cache
            $cacheKey = CacheHelper::getKeyFromRequest($request);
            $cachedResponse = Cache::get($cacheKey);
            if (is_null($cachedResponse) === false)
            {
                (new UserSearchHistoryTemp)->storeHistoryFilter($filter, false, $this->user(), $this->device(), Session::getId());
                return $cachedResponse;
            }
        }

        if (empty($filter['place'])) {
            $filter['place'] = null;
        }

        // Flash Sale landings handler
        if (
            $request->filled('landings')
            && !empty($request->landings)
        ) {
            $filter['landings'] = $request->landings;
        } else {
            $filter['landings'] = [];
        }

        $filter['location'] = $request->input('location');

        if (
            empty($filter['location'])
            && empty($filter['landings'])
        ) {
            return ApiHelper::responseError(
                'Location parameter is mandatory and cannot be empty',
                'INVALID REQUEST',
                400,
                'KOS-12498'
            );
        }

        if ($filter['location'] != null)
        {
            $centerPoint = UtilityHelper::getCenterPoint($filter['location']);
            $filter['titik_tengah'] = $centerPoint[0] . ', ' . $centerPoint[1];
        }

        $response = $this->repository->getClusterList($filter);

        (new UserSearchHistoryTemp)->storeHistoryFilter($filter, count($response['rooms']) == 0, $this->user(), $this->device(), Session::getId());

        if ($request->filled('geo')) {
            // try to reduce geocode caching to prevent transaction failure
            $randomSampling = rand(1, 100);

            if ($randomSampling % 2 == 0) {
                $geo = $request->input('geo');

                (new GeocodeCache)->cacheGeocode([
                    'lat' => $centerPoint[1],
                    'lng' => $centerPoint[0],
                    'country' => isset($geo['country']) ? $geo['country'] : null,
                    'postal_code' => isset($geo['postal_code']) ? $geo['postal_code'] : null,
                    'city' => isset($geo['city']) ? $geo['city'] : null,
                    'subdistrict' => isset($geo['subdistrict']) ? $geo['subdistrict'] : null,
                ]);
            }
        }

        if ($cacheEnabled === true)
        {
            // set Cache
            $responseModelForCache  = array_merge(['cached_at' => Carbon::now()->toIso8601String()], $response);
            Cache::add($cacheKey, Api::responseData($responseModelForCache), config('cache.api_cluster_time'));
        }
        return Api::responseData($response);
    }

    public function listFromZero(Request $request)
    {
        ini_set('max_execution_time', 100000);
        $this->repository->setPresenter(new RoomPresenter('list'));

        // Determine filters.
        $filters = $request->input('filters');

        if (empty($filters['place'])) {
            $filters['place'] = null;
        }

        /* Get center-point coordinate from 'location' data */
        if (isset($filters['location'])) {
            $filters['titik_tengah'] = LocationHelper::getCenterLocationForTracker(
                LocationHelper::expandRangeLocation($request->location)
            );
        } else {
            $filters['titik_tengah'] = null;
        }

        $filters['sorting'] = $request->input('sorting');
        $filters['suggestion_limit'] = true;

        $this->repository->pushCriteria(new \App\Criteria\Room\MainFilterCriteria($filters));

        if (
            $request->filled('include_promoted')
            && $request->input('include_promoted') == true
        ) {
            $response = $this->repository->getItemListWithPromoted(new RoomFilter($filters));
        } else {
            $response = $this->repository->getItemList(new RoomFilter($filters));
        }

        (new UserSearchHistoryTemp)->storeHistoryFilter($filters, count($response['rooms']) == 0, $this->user(), $this->device(), Session::getId());

        return Api::responseData($response);
    }

    /**
     * Showing detail cluster with list of room.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function detailCluster(Request $request)
    {
        $location = $request->input('location');
        $point = $request->input('point');

        $location = RoomCluster::getRangeSingleCluster($location, $point, 7);

        $field = 'price';
        $sort = 'asc';
        if ($request->filled('sorting')) {
            $sorting = $request->input('sorting');
            $field = isset($sorting['fields']) ? $sorting['fields'] : $sorting['field'];
            if (isset($sorting['direction']) and in_array($sorting['direction'], ['asc', 'desc', 'rand', '-']))  {
                $sort = $sorting['direction'];
            }
        }

        $request->merge(array("sorting" => array('field' => $field, 'direction' => $sort)));
        $request->merge(array("location" => $location));

        return $this->listSearch($request);
    }

    /**
     * Displaying specific resource based on some criteria
     *
     * @param  Request $request
     * @return JsonResponse
     */
    public function listSearch(Request $request)
    {
        RequestHelper::optimizeLocation($request);

        $user = $this->user();
        if (KostSearcher::shouldReroute($user) === true &&
            KostSearcher::canSupport($request) === true)
        {
            return app(PropertySearchController::class)->searchKost($request);
        }

        $filters = $request->input('filters');

        // Checking Cache
        $cacheKey = CacheHelper::getKeyFromRequest($request);
        $cachedResponse = Cache::get($cacheKey);
        if (!is_null($cachedResponse)) {
            (new UserSearchHistoryTemp())->storeHistoryFilter(
                $filters,
                false,
                $this->user(),
                $this->device(),
                Session::getId()
            );

            if (!RatingHelper::isSupportFiveRating()) {
                $cachedResponse = RatingHelper::normalizeRatingToInt($cachedResponse);
            }

            return $cachedResponse;
        }

        $this->repository->setPresenter(new RoomPresenter('list'));

        if (empty($filters['place'])) {
            $filters['place'] = null;
        }

        $filters['location'] = $request->input('location');
        $filters['sorting'] = $request->input('sorting');

        //new param form mobile gps (only 1 latitude and 1 longitude)
        $filters['user_location'] = $request->input('user_location');

        if ($request->filled('referrer')
            && $request->input('referrer') == "promo"
            && !is_null($filters['place'])) {
            $place = implode(" ", $filters['place']);
            $location = Landing::where('slug', "LIKE", "%" . strtolower($place) . "%")->first();
            if (!is_null($location)) {
                $filters['location'] = [
                    [
                        $location->longitude_1,
                        $location->latitude_1
                    ],
                    [
                        $location->longitude_2,
                        $location->latitude_2
                    ]
                ];
            }
        }

        // Flash Sale landings handler
        if (
            $request->filled('landings')
            && !empty($request->landings)
        ) {
            $filters['landings'] = $request->landings;
        }

        // Filter request which does NOT have "location" data
        $filters['titik_tengah'] = isset($filters['location']) ? LocationHelper::getCenterLocationForTracker(
            $filters['location']
        ) : null;

        if (
            $request->filled('remove_unavailable')
            && $request->input(
                'remove_unavailable'
            )
        ) {
            if (isset($filters['include_pinned'])) {
                $response = $this->repository->getHomeListWithPromoted(new RoomFilter($filters));
            } else {
                $response = $this->repository->getItemListWithPromoted(new RoomFilter($filters));
            }
        } elseif (isset($filters['include_pinned'])) {
            $response = $this->repository->getHomeListWithPromoted(new RoomFilter($filters));
        } elseif ($request->filled('include_promoted') && $request->input('include_promoted') == true) {
            $response = $this->repository->getItemListWithPromoted(new RoomFilter($filters));
        } else {
            $response = $this->repository->getItemListWithoutPromoted(new RoomFilter($filters));
        }

        (new UserSearchHistoryTemp())->storeHistoryFilter(
            $filters,
            count($response['rooms']) == 0,
            $this->user(),
            $this->device(),
            Session::getId()
        );

        // force mobile version to rating int type if not support 5 rating
        if (!RatingHelper::isSupportFiveRating()) {
            $response['rooms'] = RatingHelper::roomsRatingToInt($response['rooms']);
        }

        // try to encrypt rooms only request is from /garuda/stories/list
        if (in_array($request->getPathInfo(), self::ANTISCRAPING_ENCRYPTION['encryptable_routes'])
            && isset($response['rooms'])) {
            $jsonString = json_encode($response['rooms']);
            $response['rooms'] = OpenSslEncryptor::encrypt(
                $jsonString,
                self::ANTISCRAPING_ENCRYPTION['key'],
                self::ANTISCRAPING_ENCRYPTION['iv']
            );
        }

        // set Cache
        $responseFinal = Api::responseData($response);
        Cache::add($cacheKey, $responseFinal, self::CACHE_TIME_LIST);

        return $responseFinal;
    }

    public function freeSearch(Request $request)
    {
        $this->repository->setPresenter(new RoomPresenter('list'));

        $filters = $request->input('filters');
        $filters['titik_tengah'] = "0,0";
        $filters['location'] = [[0, 0], [0, 0]];
        $filters['disable_sorting'] = true;

        $roomFilter = new RoomFilter($filters);

        if (!isset($filters['criteria']) || (isset($filters['criteria']) && trim($filters['criteria']) == '')) {
            $response = array(
                'page' => 1,
                'next-page' => 2,
                'limit' => 20,
                'offset' => 0,
                'has-more' => false,
                'total' => 0,
                'total-str' => "0",
                'toast' => null,
                'filter-str' => $roomFilter ? $roomFilter->getString() : "",
                'rooms' => []
            );
        } else {
            $this->repository->pushCriteria(new \App\Criteria\Room\MainFilterCriteria($filters));
            $response = $this->repository->getItemList($roomFilter);
        }

        (new UserSearchHistoryTemp)->storeHistoryFilter($filters, count($response['rooms']) == 0, $this->user(), $this->device(), Session::getId());

        return Api::responseData($response);
    }

    /**
     * Displaying specific resource based on favorited criteria
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function listFavorite(Request $request)
    {
        $user = $this->user();
        if (is_null($user)) {
            return Api::responseData([
                'status' => false,
                'meta'   => [
                    'message' => 'Data user tidak ditemukan'
                ]
            ]);
        }

        $this->repository->pushCriteria(new \App\Criteria\Room\LoveCriteria($user));
        $this->repository->setPresenter(new RoomPresenter('list'));

        $liveFavorite = $this->repository->getItemList();

        return Api::responseData($liveFavorite);
    }

    /**
     * Displaying specific resource based on favorited criteria
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function listVisit(Request $request)
    {
        $this->repository->pushCriteria(new \App\Criteria\Room\ViewCriteria($this->device()));
        $this->repository->setPresenter(new RoomPresenter('list'));

        $response = $this->repository->getItemList();

        return Api::responseData($response);
    }

    /**
     * Displaying specific resource based on favorited criteria
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function listCall(Request $request)
    {
        $this->repository->setPresenter(new RoomPresenter('list'));

        $this->repository->pushCriteria(new \App\Criteria\Room\CallCriteria($this->device(), $this->user()));

        $response = $this->repository->getItemList();

        return Api::responseData($response);
    }

    public function listSurvey(Request $request)
    {
        $this->repository->setPresenter(new RoomPresenter('list'));

        $this->repository->pushCriteria(new \App\Criteria\Room\SurveyCriteria($this->user()));

        $response = $this->repository->getItemList();

        return Api::responseData($response);
    }

    public function listTelp(Request $request)
    {
        $this->repository->setPresenter(new RoomPresenter('list'));

        $this->repository->pushCriteria(new \App\Criteria\Room\TelpCriteria($this->user()));

        $response = $this->repository->getItemList();

        return Api::responseData($response);
    }

    public function listView(Request $request)
    {
        if (isset($_COOKIE['idkamar'])) {
            $idkamar = $_COOKIE['idkamar'];
        } else {
            $idkamar = null;
        }

        $this->repository->setPresenter(new RoomPresenter('list'));
        $this->repository->pushCriteria(new \App\Criteria\Room\ViewWebCriteria($idkamar));

        $response = $this->repository->getItemList();

        return Api::responseData($response);
    }

    public function listNewestLive(Request $request)
    {

        $this->repository->setPresenter(new RoomPresenter('list'));

        $this->repository->pushCriteria(new \App\Criteria\Room\NewestLiveCriteria());

        $filters = [];

        if ($request->filled('filters')) {
            $filters = $request->get('filters');
        }

        $this->repository->pushCriteria(new \App\Criteria\Room\MainFilterCriteria($filters));

        $response = $this->repository->getItemList();

        return Api::responseData($response);
    }

    /**
     * Display the formatted location of specified resource.
     *
     * @param Request $request
     * @param $locationId
     * @return JsonResponse
     */
    public function getRoomLocation(Request $request, $locationId)
    {
        $location = $this->repository->getLocationByLocationId($locationId);

        return Api::responseData(
            [
                'status' => $location['status'],
                'location' => $location['location']
            ]
        );
    }

    /**
     * Get room suggestion
     */
    public function getSuggestions(Request $request, $songId)
    {
        $room = Room::active()->where('song_id', $songId)->first();

        if (!$room) {
            return Api::responseData([
                'promoted_suggestions' => [],
                'normal_suggestions' => [],
            ]);
        }

        // cache breadcrumb first to be used in get related functions
        $room->cached_breadcrumbs = $room->breadcrumb;
        $room->breadcrumb_loaded = true;

        $premiumSuggestion = $room->related_promoted;
        //$normalSuggestion = $room->related_room;
        $normalSuggestion = $room->getRelatedByRadius(PropertyType::KOS, false);

        return Api::responseData([
            'promoted_suggestions' => $premiumSuggestion,
            'normal_suggestions' => $normalSuggestion['data'],
        ]);
    }

    /**
     * Save love room made by user to the database
     *
     * @param int $songId
     */
    public function postFavorite($songId)
    {
        $room = $this->repository->findByField('song_id', $songId)->first();

        if (!$room) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Gagal melakukan favorite',
                ],
            ]);
        }

        $user = $this->user();
        if (!$user) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Gagal melakukan favorite, silakan login terlebih dahulu',
                ],
            ]);
        }

        $addFavorite = $this->repository->addFavorite($songId, $user);
        if ($addFavorite == null) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Gagal melakukan favorite',
                ],
            ]);
        }

        return Api::responseData(
            [
                'success' => $addFavorite,
                '_id' => $songId,
            ]
        );
    }

    /**
     * Save view room made by user to the database
     *
     * @param int $songId
     */
    public function postView(Request $request, $songId)
    {
        $fromAds = false;
        if ($request->filled('from_ads') && $request->input('from_ads') == true) {
            $fromAds = true;
        }

        return Api::responseData(
            array('success' => $this->repository->addView($songId, $this->user(), $this->device(), $fromAds, AdHistory::TYPE_CLICK))
        );
    }

    /**
     * Save call room made by user to the database
     *
     * @param int $songId
     */
    public function postCall(Request $request, $songId)
    {
        $device = $this->device();
        if (is_null($device) === false && 
            $device->getIsSendBirdSupported() === false)
        {
            $message = $device->platform == 'android' 
                ? 'Harap update aplikasi Anda. Fitur ini dapat digunakan pada versi 3.2.0'
                : 'Harap update aplikasi Anda. Fitur ini dapat digunakan pada versi 3.1.0';
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => $message,
                ],
            ]);  
        }

        $room = Room::with('level')->where('song_id', $songId)->first();
        if (is_null($room)) {
            return Api::badRequest("Room with song_id " . $songId . " is not found.");
        }
        $adminId = ChatAdmin::getConsultantOrAdminIdForRoom($room);
        
        $user = $this->user();
        $userStatus = $this->checkCallingUser($user);
        if (!is_null($userStatus)) {
            return $userStatus;
        }

        $limitStatus = $this->checkDailyQuestionLimit($user, $songId);
        if (!is_null($limitStatus)) {
            return $limitStatus;
        }

        $channel = $this->getChannelForTenantAndRoom($user, $room);
        if (empty($channel)) {
            throw new \Exception("fail to find or create group channel for chatting");
        }

        $callData = $request->all('phone', 'email', 'note', 'name', 'schedule');
        $callData['add_from'] = $this->getAddFrom();
        $callData['chat_admin_id'] = $adminId;
        $callData['group_channel_url'] = $channel->channel_url;
        
        $call = $this->repository->addCall($songId, $callData, $user, $this->device());
        if (!$call) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Gagal melakukan hubungi kost',
                ],
            ]);
        }

        $this->trackPostCall($request, $call);

        return Api::responseData([
            'status' => true,
            'result' => $call ? true : false,
            'call' => [
                'note' => $callData['note'],
                'schedule' => $callData['schedule'],
            ],
            'admin_id' => $call->chat_admin_id,
            'call_id' => $call->id,
            'admin_list' => ChatAdmin::getAdminIdsForTenants(),
            'group_channel_url' => $call->chat_group_id
        ]);
    }

    private function getAddFrom()
    {
        $agent = new Agent();
        return Tracking::checkDevice($agent);
    }

    private function trackPostCall($request, $call)
    {
        if ($request->filled('utm')) {
            \App\Entities\Tracker\InputAdsTracker::saveFromCampaignQuery($request->utm, $this->user(), $call->id, 'chat');
        }
    }

    private function getChannelForTenantAndRoom($user, $room)
    {
        $channel = $this->groupChnlSvc
            ->findOrCreateChannelForTenantAndRoom($user, $room);

        if (empty($channel->channel_url)) {
            throw new \RuntimeException('Channel url is empty.');
        }

        $this->groupChnlSvc->adjustChannelToGpRule($channel, $room);

        return $channel;
    }

    private function checkCallingUser($user)
    {
        if (is_null($user)) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'User tidak ditemukan',
                ],
            ]);
        }

        // block suspicious user :
        // 1. Gerobak.id (325904) : suspicious for data scraping.
        // 2. Bony Ekalaya (472522, 307528) : suspicious for scaming other kost resident.
        // 3. Dian Ayu Anggraini (352263) : scam owner and other kost resident
        $suspiciousUserIds = [325904, 472522, 307528, 352263];
        if (in_array($user->id, $suspiciousUserIds)) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Pesan gagal dikirim',
                ],
            ]);
        }

        return null;
    }

    private function checkDailyQuestionLimit($user, $songId)
    {
        // Check chat question room limit
        $isDailyQuestionRoomLimitExceeded = Call::getIsDailyQuestionRoomLimitExceeded($user, Carbon::today(), $songId);
        if ($isDailyQuestionRoomLimitExceeded) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Maaf, Anda sudah mencapai batas pertanyaan chat untuk kost/apartemen berbeda hari ini. Silakan coba lagi besok.',
                ]
            ]);
        }

        // Check chat question message limit
        $isDailyQuestionLimitExceeded = Call::getIsDailyQuestionLimitExceeded($user, Carbon::today());
        if ($isDailyQuestionLimitExceeded) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Maaf, Anda sudah mencapai batas pertanyaan chat untuk hari ini. Silakan coba lagi besok.',
                ]
            ]);
        }

        return null;
    }

    public function recountSortScore($id)
    {
        try {
            $room = Room::with(['owners', 'cards', 'level'])
                ->where('is_active', 'true')
                ->where('id', $id)
                ->first();

            if (is_null($room)) {
                $message = 'Could not find room with ID: ' . $id . ' or this room is currently inactive/deleted.';
                return Api::responseError($message, false, 200);
            }

            $room->updateSortScore();

            return Api::responseData(
                [
                    'success' => true
                ]
            );
        } catch (Exception $exception) {
            Bugsnag::notifyException($exception);

            return Api::responseData(
                [
                    'status' => false,
                    'meta' => [
                        'message' => "Sorry, something went wrong on the system. Please try again later.",
                    ]
                ]
            );
        }
    }

    public function postTelp(Request $request, $songId)
    {
        $user = $this->user();

        if ($user == null) {
            return Api::responseData(array("contact" => array()));
        }

        $data = $request->all();
        $data['user_id'] = $user->id;
        $data['song_id'] = $songId;

        $telp = $this->repository->postTelp($data);

        return Api::responseData($telp);
    }

    public function postTelpFinish(Request $request, $songId)
    {
        $user = $this->user();

        if ($user == null) {
            return Api::responseData(array("telp" => false));
        }

        $data = $request->all();
        $data['user_id'] = $user->id;
        $data['song_id'] = $songId;

        $finish = $this->repository->finishTelp($data);

        return Api::responseData(array("telp" => $finish));
    }

    /**
     * Save user filter to database. We used to call it subscribe.
     * Deprecated.
     * Use SubscribeController@subscribeFilter instead
     */
    public function subscribeFilter(Request $request)
    {
        $user = $this->user();
        $device = $this->device();

        $alarmDetail = $request->all('filters', 'location', 'phone', 'email');

        $alarm = $this->repository->saveFilterToAlarm($user, $device, $alarmDetail);

        if ($request->filled('user_detail')) {
            UserFindRoom::saveFromSubscribe($user, $request->input('user_detail'));
        }

        return Api::responseData(['result' => $alarm]);
    }

    /**
     * Handle subscribe filter from download soal
     * Deprecated
     * Use SubscribeController@subscribeDownload instead
     */
    public function subscribeDownload(Request $request)
    {
        $user = $this->user();

        $alarmDetail = $request->all('filters', 'location', 'phone', 'email');

        $alarm = $this->repository->saveFilterToAlarm($user, null, $alarmDetail);

        $inputResult = null;

        if ($request->filled('user_detail')) {
            $userDetail = $request->input('user_detail');
            if (isset($userDetail['type']) && $userDetail['type'] == 'test-kerja') {
                $userDetail['landing'] = '20000';
            } elseif (isset($userDetail['type']) && $userDetail['type'] == 'download-bbm') {
                $userDetail['landing'] = '30000';
                $userDetail['name'] = $user->name;
                $userDetail['email'] = $user->email;
                $userDetail['phone'] = $user->phone_number;
            } elseif (isset($userDetail['type']) && $userDetail['type'] == 'seleksi-mandiri') {
                $userDetail['landing'] = '40000';
                $userDetail['name'] = $user->name;
                $userDetail['email'] = $user->email;
                $userDetail['phone'] = $user->phone_number;
            } elseif (isset($userDetail['type']) && $userDetail['type'] == 'daftar-ulang') {
                $userDetail['landing'] = '50000';
                $userDetail['name'] = $user->name;
                $userDetail['email'] = $user->email;
                $userDetail['phone'] = $user->phone_number;
            } elseif (isset($userDetail['type']) && $userDetail['type'] == 'landing-commuter') {
                $userDetail['landing'] = '60000';
                $userDetail['name'] = $user->name;
                $userDetail['email'] = $user->email;
                $userDetail['phone'] = $user->phone_number;
            } else {
                $userDetail['landing'] = '10000';
            }

            $userDetail['notes'] = '';
            $userDetail['start_date'] = null;
            $userDetail['duration'] = 1;
            $inputResult = UserFindRoom::saveFromSubscribe($user, $userDetail);
        } elseif ($request->filled('form_detail')) {
            $userId = is_null($user) ? null : $user->id;

            $formDetail = $request->input('form_detail');
            $examInput = new DownloadExamInput;
            $examInput->download_exam_id = $formDetail['exam_id'];
            $examInput->user_id = $userId;
            $examInput->name = $formDetail['name'];
            $examInput->current_location = $formDetail['current_location'];
            $examInput->destination = $formDetail['destination'];

            if (isset($formDetail['university'])) {
                $examInput->university = $formDetail['university'];
            }

            $examInput->gender = $formDetail['gender'];
            $examInput->email = $formDetail['email'];
            $examInput->phone_number = $formDetail['phone_number'];
            $examInput->save();

            $inputResult = $examInput;
        }

        return Api::responseData(['result' => $alarm, 'input_result' => $inputResult]);
    }

    /**
     * Update user ID in download_exam_input
     * Deprecated (Refactor purpos)
     * Use SubscribeController@subscribeDownloadUpdate instead
     */
    public function subscribeDownloadUpdate(Request $request)
    {
        $type = $request->filled('type') ? $request->input('type') : 'download_exam';

        $user = $this->user();

        if (!$user) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Silakan login terlebih dahulu',
                ],
            ]);
        }

        if ($type == 'download_exam') {
            $downloadExamInput = DownloadExamInput::find($request->input('_seq'));

            if (!$downloadExamInput) {
                return Api::responseData([
                    'status' => false,
                    'meta' => [
                        'message' => 'Data tidak ditemukan',
                    ],
                ]);
            }

            if (!is_null($downloadExamInput->user_id)) {
                return Api::responseData([
                    'status' => false,
                    'meta' => [
                        'message' => 'Data sudah diupdate sebelumnya.',
                    ],
                ]);
            }

            $downloadExamInput->user_id = $user->id;
            $downloadExamInput->save();
        }

        return Api::responseData(['status' => true]);

    }

    /**
     * Posting owner contact.
     */
    public function postOwnerContact(Request $request)
    {
        $ownerData = $request->all('email', 'phone_number');

        $this->repository->ownerRegister($ownerData);

        return Api::responseData(['success' => true]);
    }

    /**
     * API for home.
     */
    public function getHome(Request $request)
    {
        $user = $this->user();
        $isApp = !is_null($this->device());

        $home = $this->repository->getListHome($user, $isApp);

        return Api::responseData(array('homes' => $home));
    }

    /**
     * Get recommendation for specific user.
     */
    public function recommendation(Request $request)
    {
        $user = $this->user();
        $offset = $request->input('offset', 0);

        $userRecommendation = new RecommendationUser($user);

        return Api::responseData($userRecommendation->getRecommendation($offset));
    }

    /**
     * Save new kost from user
     *
     */
    public function postSubmitKost(Request $request)
    {
        $roomData = $request->all();

        $room = $this->repository->saveJson($roomData);

        return Api::responseData(['stories' => $room]);
    }

    /**
     * Checking that a name of room is already exist or not.
     * The purpose of checking is make sure no room with same name (duplicate).
     */
    public function availableName(Request $request)
    {
        $nameProposed = $request->input('name');

        $room = Room::where('name', $nameProposed)->first();

        return Api::responseData([
            'is_used' => !is_null($room) > 0 ? true : false,
            'slug' => !is_null($room) ? $room->share_url : '',
        ]);
    }

    /**
     * Send SMS to Kost Owner
     */
    public function postSms(Request $request, $songId)
    {

    }

    /**
     * This API is used to automatically reply the user who called postCall API
     * @param Request $request
     * @param $songId
     * @return JsonResponse
     */
    public function postReply(Request $request, $songId)
    {
        $user = $this->user();
        $room = $this->repository->findByField('song_id', $songId)->first();
        if (is_null($room)) {
            Bugsnag::notifyException(new RuntimeException('Room not found'));

            return Api::responseData([
                'status' => false,
                'response' => null,
            ]);
        }

        $is_apartment = true;
        if (is_null($room->apartment_project_id) or $room->apartment_project_id < 1) {
            $is_apartment = false;
        }

        $channel = (!empty($request->get('group_channel_url')) ? $request->get('group_channel_url') : $request->get('group_id'));
        $replyData = [
            'group_id' => $channel,
            'note' => $request->get('note'),
            'admin_id' => $request->get('admin_id'),
            'call_id' => $request->get('call_id'),
            'id_question' => $request->get('id_question'),
            'is_apartment' => $is_apartment,
        ];

        $response = $this->repository->autoReplyChat($room, $replyData, $user);

        $fromAds = false;
        if ($request->filled('from_ads') && $request->input('from_ads') == true) {
            $fromAds = true;
        }

        if ($fromAds) {
            $fromAds = AppConfig::statusPremiumChat();
        }

        $this->repository->addView($songId, $this->user(), $this->device(), $fromAds, 'chat');

        TrackingFeature::insertOrUpdateView('chat', $request->get('id_question'));

        return Api::responseData([
            'status' => true,
            'success' => true,
            'response' => $response,
        ]);
    }

    public function getChat(Request $request)
    {
        $for = 'kos';
        if ($request->filled('is_apartment')) {
            $for = 'apartment';
        }
        
        $room = null;
        if ($request->filled("id")) {
            $room = Room::Active()->where('song_id', $request->input('id'))->first();
        }
        
        $response = (new Question)->getQuestion($for, $room);

        TrackingFeature::insertOrUpdateView('chat-v1');

        return Api::responseData([
            'success' => true,
            'version' => "20181103",
            'question' => $response,
        ]);
    }

    public function getPhoneOwner(Request $request, $songId)
    {
        return Api::responseData($this->repository->getPhoneOwner($songId));
    }

    public function getProfile(Request $request)
    {
        $user = $this->user();

        if (is_null($user)) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Proses Gagal. Silakan login terlebih dahulu',
                ],
            ]);
        }

        return Api::responseData(['profile' => $this->repository->dataProfile($this->user())]);
    }

    public function getRoomTypeNumber($roomId)
    {
        $typeNumbers = $this->repository->getRoomTypeNumber($roomId);

        return Api::responseData([
            'data' => $typeNumbers
        ]);
    }

    /** 
     * Get photos by given song_id
     * 
     * @param int $songId
     * @return JsonResponse
     */
    public function getCardsBySongId(int $songId): JsonResponse
    {
        $room = $this->repository->where('song_id', $songId)->first();
        if (is_null($room)) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Data tidak ditemukan',
                ],
            ]);
        }

        $listCard = [];
        if ($room->isPremiumPhoto()) {
            $cardRepository = app()->make(CardPremiumRepository::class);
            $cards = $cardRepository->getListCard($room->id);

            $listCard = (new CardPremiumPresenter('list'))->present($cards);
        } else {
            $cardRepository = app()->make(CardRepository::class);
            $cards = $cardRepository->getListCard($room->id);
            
            $listCard = (new CardPresenter('list'))->present($cards);
        }

        return Api::responseData($listCard);
    }

    public function compareNearbyRoom(Request $request, int $songId): JsonResponse
    {
        $type = $request->filled('type') ? $request->input('type') : 'all';
        $room = $this->repository->where('song_id', $songId)->first();
        if (is_null($room)) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Data tidak ditemukan',
                ],
            ]);
        }

        $radius = 3000;
        $comparePrice = $room->getNearbyPriceByRadius($radius);
        $dataPrice = [];
        $dataPriceType = [];
        $nearbyProperty = [];
        $nearbyCount = 0;
        $propertySearch = $this->repository->getAdHistoryStats($room, $radius);
        $propertySearchValue = function($value) {
            return $value['value'];
        };
        $propertySearchMap = array_map($propertySearchValue, $propertySearch);
        $propertySearchMin = min($propertySearchMap);
        $propertySearchMax = max($propertySearchMap);

        $params = ['limit' => 3];

        $nearbyProperty = $room->getRelatedByRadius($type, null, $radius, $params);

        // This will show only daily, monthly, yearly
        $comparePrice = array_slice($comparePrice, 0, 3);

        foreach ($comparePrice as $key => $price) {
            if (count($comparePrice[$key]) == 0) {
                continue;
            }
            
            $data = [
                'type' => $key,
                'average' => $price['average'],
                'min' => $price['min'],
                'max' => $price['max']
            ];
            
            array_push($dataPriceType, $key);
            array_push($dataPrice, $data);
        }

        return Api::responseData([
            'status' => true,
            'result' => [
                'data_price' => $dataPrice,
                'data_price_type' => $dataPriceType,
                'nearby_property' => $nearbyProperty['data'],
                'nearby_property_count' => $nearbyProperty['total'],
                'nearby_property_state' => $type,
                'property_search' => $propertySearch,
                'property_search_stats' => [
                    'max' => $propertySearchMax,
                    'min' => $propertySearchMin
                ]
            ]
        ]);
    }

    public function getNearbyBySongId(Request $request, $songId)
    {
        $room = $this->repository->where('song_id', $songId)->first();
        if (is_null($room)) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Data tidak ditemukan',
                ],
            ]);
        }
        
        $offset = $request->filled('page') ? $request->input('page') : '0';
        $limit = $request->filled('limit') ? $request->input('limit') : '10';
        $type = $request->filled('type') ? $request->input('type') : 'kos';
        $radius = $request->filled('radius') ? $request->input('radius') : '3000';

        $result = [];
        $params = [
            'page' => intval($offset),
            'limit' => intval($limit)
        ];
        if ($type == 'apartment') {
            $result = $room->getRelatedByRadius(PropertyType::APARTMENT, null, $radius, $params);
        } else {
            $result = $room->getRelatedByRadius(PropertyType::KOS, null, $radius, $params);
        }

        return Api::responseData([
            'data' => $result
        ]);
    }

    public function getRecommendationRooms($songId)
    {
        if (cache::has($songId)) {
            return Api::responseData(cache::get($songId));
        }

        $room = Room::where('song_id', $songId)->first();

        if (is_null($room)) {
            $message = 'Data Not Found';
            return Api::responseError($message, false, 404);
        }

        $recommendationRooms = $room->getRecommendationByRadius();

        cache::add($songId, $recommendationRooms, 5);

        return Api::responseData($recommendationRooms);
    }

    public function getKosRules($songId): JsonResponse
    {
        $data = $this->repository->getKosRulesData($songId);

        if (empty($data)) {
            $message = 'Data Not Found';
            return ApiHelper::responseError($message, false, 404);
        }

        return  Api::responseData($data);
    }

    public function indexKostSearch($id)
    {
        try {
            if (!is_numeric($id)) {
                return ApiHelper::responseError(false, false, 400);
            }
            IndexKostJob::dispatch($id);
            return Api::responseData(
                [
                    'success' => true
                ]
            );
        } catch (Exception $exception) {
            Bugsnag::notifyException($exception);
            return Api::responseData(
                [
                    'status' => false,
                    'meta' => [
                        'message' => "Sorry, something went wrong on the system. Please try again later.",
                    ]
                ]
            );
        }
    }
}
