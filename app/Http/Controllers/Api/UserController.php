<?php

namespace App\Http\Controllers\Api;

use App\Entities\Activity\ActivationCode;
use App\Entities\Activity\ActivationCodeType;
use App\Entities\Booking\BookingUser;
use App\Entities\Device\UserDevice;
use App\Entities\Media\Media;
use App\Entities\Notif\SettingNotif;
use App\Entities\Room\Room;
use App\Entities\User\UserNotificationCounter;
use App\Http\Helpers\ApiHelper;
use App\Http\Helpers\ApiResponse as Api;
use App\Http\Helpers\ApiResponse;
use App\Http\Requests;
use App\Repositories\AppRepository;
use App\User;
use Illuminate\Http\Request;
use SendBird;
use Validator;
use App\Http\Helpers\RegexHelper;
use App\Http\Requests\UserEditNameRequest;
use Illuminate\Http\JsonResponse;
use App\Jobs\MoEngage\SendMoEngageUserPropertiesQueue;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Exception;

class UserController extends BaseController
{
    /**
    * This functions mimics Laravel Caffeine (https://github.com/GeneaLabs/laravel-caffeine) function 
    * This basically to prevent token from expired
    * Useful when user stay on the same page for a long time
    *
    * @param Request        $request
    */
    public function drip(Request $request)
    {
        return response('', 204);
    }

    /**
    * Change User Display Name
    */
    public function changeUserChatName(Request $request)
    {
        $user = $this->user();

        if (is_null($user)) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'User tidak ditemukan'
                ]
            ]);
        }

        $userId = $user->id;
        $chatName = $request->input('name');
        SendBird::updateUser($userId, $chatName);

        return Api::response(['status' => true], 200);
    }

    public function getSettings(Request $request)
    {
        $user = $this->user();

        if (is_null($user)) {
            return Api::responseData([
                'status' => false,
                'message' => 'User not found',
                'settings' => []
            ]);
        }

        $isOwner = $user->is_owner == 'true';

        $settings = SettingNotif::getUserNotificationSettings($user->id, $isOwner);

        // Filter owner's Name (make it NULL if it's contain number/interger)
        $name = $user->name;
        if ($isOwner)
            $name = !is_null($user) ? User::filter_name($user) : null;

        $gender = !is_null($user) ? !trim($user->gender) ? null : $user->gender : null;
        
        // Get user photo_url
        if (!is_null($user->photo)) {
            $photoData = User::photo_url($user->photo);
            $photoUrl = $photoData['small'];
        } else {
            // *** Notes! This feature is being used since v.1.1.2, and this command must be executed at first: "php artisan storage:link" ***
            $photoUrl =  env("APP_URL", "https://mamikos.com") . "/storage/placeholder.png";
        }

        $userdata = [
            'id' => $user->id,
            'phone' => $user->phone_number,
            'email' => $user->email,
            'username' => $name,
            'photo_url_string' => $photoUrl,
            'gender' => $gender,
            'agree_on_tc' => $user->agree_on_tc == 'true' ? true : false
        ];

        return Api::responseData([
            'status'    => true,
            'settings'  => $settings,
            'user'      => (object) $userdata
        ]);

    }

    public function changeSettings(Request $request)
    {
        $user = $this->user();

        if( is_null($user)) {
            return Api::responseData([
                'status' => false,
                'message' => 'User not found',
            ]);
        }

        $settings = $request->input('settings');

        if (!is_array($settings)) {
            return Api::responseData([
                'status' => false,
                'message' => 'Format request tidak tepat',
            ]);
        }

        $isOwner = $user->is_owner == 'true';

        $newSettings = SettingNotif::changeUserNotificationSettings($settings, $user->id, $isOwner);

        return Api::responseData([
            'status' => true,
            'settings' => $newSettings
        ]);
    }

    public function getCounter(Request $request)
    {
        $type = $request->input('type');
        $user = $this->user();

        if( $type == '') {
            $type = 'all';
        }

        $notificationCounter = UserNotificationCounter::getCounterByType($type, $user);

        $user = User::with(['notification' => function($p) {
                $p->where('read', 'false');
            }
        ], 'room_owner', 'room_owner.room')->where('id', $user->id)->first();

        $newBookingTotal = 0;
        if (count($user->room_owner) > 0) {
            $owners = $user->room_owner()->pluck('designer_id')->toArray();
            $roomIds = Room::whereIn('id', $owners)->pluck('song_id')->toArray();
            $newBookingTotal = BookingUser::where('is_open', 0)->whereIn('designer_id', $roomIds)->count();
        }
        return Api::responseData([
            'counter'            => $notificationCounter,
            'notification_count' => $user->notification->count(),
            'booking_total' => $newBookingTotal,
        ]);
    }

    public function readCounter(Request $request)
    {
        $type = $request->input('type');
        $user = $this->user();

        if ($type == '') {
            $type = 'all';
        }

        UserNotificationCounter::clearCounterByType($type, $user);

        $notificationCounter = UserNotificationCounter::getCounterByType('all', $user);

        return Api::responseData([
            'counter' => $notificationCounter
        ]);

    }


    /**
     * Check if phone number is already registered in database
     *
     * @param Request $request          Laravel Request
     * 
     * @return bool                     True / false
     */
    public function phoneNumberCheck(Request $request)
    {
        $validator = Validator::make($request->all(), [
                'phone_number' => 'required|regex:'.RegexHelper::phoneNumberFormat()
            ],
            [
                'phone_number.required' => 'Nomor HP harus diisi',
                'phone_number.regex' => 'Format Nomor HP tidak valid (08xxxxxxxx)'
            ]);

        if ($validator->fails()) {
            return Api::responseData([
                'status' => false,
                'messages' => $validator->errors()->all()
            ]);
        }

        $phone = preg_replace(RegexHelper::numericOnly(), "", $request->get('phone_number'));

        $isRegistered = User::isRegisteredOwner($phone);

        return Api::responseData([
            'phone_registered' => $isRegistered
        ]);
    }

    /**
     *  Enable editing of user's profile name
     *
     *  @param Request $request
     *
     *  @return JsonResponse
     */
    public function editName(UserEditNameRequest $request): JsonResponse
    {
        $validator = $request->failedValidator;

        if (!is_null($validator) && $validator->fails()) {
            return ApiResponse::responseData([
                'status' => false,
                'meta' => [
                    'message' => $validator->errors()->all()
                ]
            ]);
        }

        $owner = $this->user();

        $owner->name = $request->name;
        $owner->save();

        //ADD USER PROPERTIES FOR MoEngage Tracking
        try {
            SendMoEngageUserPropertiesQueue::dispatch(
                (int) $owner->id,
                ['first_name' => $owner->name]
            )->delay(now()->addMinutes(15));
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
        }

        return ApiResponse::responseData(['status' => true]);
    }
}
