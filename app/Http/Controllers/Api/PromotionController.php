<?php

namespace App\Http\Controllers\Api;

use App\Entities\Room\Room;
use App\Entities\Room\RoomFilter;
use Illuminate\Http\Request;
use App\Http\Helpers\ApiResponse as Api;
use App\Repositories\PromotionRepository;

class PromotionController extends BaseController
{
    protected $repository;

    public function __construct(PromotionRepository $repository)
    {
        $this->repository = $repository;
        parent::__construct();
    }

    public function postPromotion(Request $request)
    {
        if ($this->user() == null) {
            $message = "Gagal mengirim promo.";
            $status  = false;
        } else {
            $user = $this->user();

            if ($user->date_owner_limit == null or $user->date_owner_limit < date('Y-m-d')) {
                $message = "Akun anda belum premium.";
                $status  = false;
            } else {
                $send    = $this->repository->postPromotion($request->all());
                $status  = $send['status'];
                $message = $send['message'];
            }
        }

        $response = [
            "meta"  => ["message" => $message],
            "promo" => $status,
        ];

        return Api::responseData($response);
    }

    public function getPromotionOwner(Request $request, $songId)
    {
        return Api::responseData($this->repository->getPromotionOwner($songId));
    }

    public function updatepromotion(Request $request)
    {
        return true;
        /*if ($this->user() == null) {
           $message = "Gagal update promo.";
           $status  = false;
        } else {
           $send   = $this->repository->postPromotion($request->all());
           $status = $send['status'];
           $message = $send['message'];
        }

        $response = array( "meta"    => array("message" => $message),
                           "promo"   => $status 
                         );

        return Api::responseData($response);*/
    }

    /** 
     * Get promo filter (cities list on promo-kost page)
     * 
     * @param \Illuminate\Http\Request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPromoFilters(Request $request)
    {
        $cities = [
            'all'                   => 'Semua Kota',
            'Jakarta Barat'         => 'Jakarta Barat',
            'Jakarta Selatan'       => 'Jakarta Selatan',
            'Jakarta Timur'         => 'Jakarta Timur',
            'Jakarta Utara'         => 'Jakarta Utara',
            'Jakarta Pusat'         => 'Jakarta Pusat',
            'Tangerang'             => 'Tangerang',
            'Tangerang Selatan'     => 'Tangerang Selatan',
            'Bogor'                 => 'Bogor',
            'Bekasi'                => 'Bekasi',
            'Depok'                 => 'Depok',
            'Yogyakarta'            => 'Yogyakarta',
            'Semarang'              => 'Semarang',
            'Solo'                  => 'Solo',
            'Purwokerto'            => 'Purwokerto',
            'Pekalongan'            => 'Pekalongan',
            'Bantul'                => 'Bantul',
            'Sleman'                => 'Sleman',
            'Bandung'               => 'Bandung',
            'Surabaya'              => 'Surabaya',
            'Makassar'              => 'Makassar',
            'Malang'                => 'Malang',
            'Medan'                 => 'Medan',
        ];

        if ($request->filled('type') && $request->get('type') == 'daily') {
            $cities['Bali']       = 'Bali';
            $cities['Palembang']  = 'Palembang';
            $cities['Batam']      = 'Batam';
            $cities['Pekanbaru']  = 'Pekanbaru';
            $cities['Balikpapan'] = 'Balikpapan';
            $cities['Padang']     = 'Padang';
            $cities['Pontianak']  = 'Pontianak';
            $cities['Manado']     = 'Manado';
            $cities['Denpasar']   = 'Denpasar';
            $cities['Surakarta']  = 'Surakarta';
        }

        return Api::responseData(['cities' => $cities]);
    }

    public function getCitiesWithPromoted()
    {
        $cities = [
            'Semua Kota',
            'Jakarta Barat',
            'Jakarta Selatan',
            'Jakarta Timur',
            'Jakarta Utara',
            'Tangerang',
            'Tangerang Selatan',
            'Bogor',
            'Bekasi',
            'Depok',
            'Yogyakarta',
            'Sleman',
            'Bandung',
            'Surabaya',
            'Makassar',
            'Malang',
        ];

        return Api::responseData(
            [
                'cities' => $cities,
            ]
        );
    }

    public function getCitiesPromotedHardCode()
    {
        $cities = [
            'Jakarta Pusat',
            'Jakarta Barat',
            'Jakarta Selatan',
            'Jakarta Timur',
            'Jakarta Utara',
            'Tangerang',
            'Tangerang Selatan',
            'Bogor',
            'Bekasi',
            'Depok',
            'Yogyakarta',
            'Sleman',
            'Solo',
            'Purwokerto',
            'Pekalongan',
            'Bantul',
            'Semarang',
            'Bandung',
            'Surabaya',
            'Makassar',
            'Medan',
            'Malang'
        ];

        return Api::responseData(
            [
                'cities' => $cities
            ]
        );

    }
}
