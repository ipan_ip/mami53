<?php

namespace App\Http\Controllers\Api;

use App\Entities\Room\Element\Type;
use App\Entities\Room\Room;
use App\Http\Helpers\ApiResponse as Api;
use App\Presenters\RoomPresenter;
use Exception;

class RoomFacilityController extends BaseController
{
	public function getDetails(int $roomId)
	{
		$room = Room::where('song_id', $roomId)->first();
		if (is_null($room))
		{
			return self::sendResponse(false, 'Invalid room ID!');
		}

		try
		{
			$facilityPhotos = (new RoomPresenter('facilities'))->present($room);

			return self::sendResponse(true, $facilityPhotos);
		}
		catch (Exception $e)
		{
			return self::sendResponse(false, $e->getMessage());
		}
	}

	public function getDetailsForTypes(int $roomId, int $roomTypeId)
	{
		$roomType = Type::find($roomTypeId);

		if (is_null($roomType))
		{
			return self::sendResponse(false, 'Invalid room type ID!');
		}

		try
		{
			$facilityPhotos = (new RoomPresenter('facilities-type'))->present($roomType);

			return self::sendResponse(true, $facilityPhotos);
		}
		catch (Exception $e)
		{
			return self::sendResponse(false, $e->getMessage());
		}
	}

	private static function sendResponse(bool $status, $message)
	{
		if (is_array($message))
		{
			return Api::responseData($message);
		}

		return Api::responseData([
			'status' => $status,
			'meta'   => [
				'message' => $message,
			],
		]);
	}
}
