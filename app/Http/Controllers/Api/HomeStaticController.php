<?php

namespace App\Http\Controllers\Api;

use App\Http\Helpers\ApiHelper as Api;
use App\Http\Helpers\FeatureFlagHelper;
use App\Repositories\Landing\HomeStaticLandingRepository;
use App\Repositories\RoomRepository;
use App\Entities\Landing\HomeStaticLanding;


class HomeStaticController extends BaseController
{
    protected $repository;

    public function __construct(HomeStaticLandingRepository $repository)
    {
        $this->repository = $repository;

        parent::__construct();
    }

    public function getRecommendationRooms($id)
    {
        $subdistrict = HomeStaticLanding::with('landing_kost')->where('parent_id', $id)->get();

        return Api::responseData(compact('subdistrict'));
    }

    public function getRecommendationCities()
    {
        if (FeatureFlagHelper::isSupportNewRecommendationCity()) {
            $user = $this->user();
            $city = $this->repository->getCities($user);
        } else {
            $roomRepo = app()->make(RoomRepository::class);
            $city = $roomRepo->getCity();
        }

        return Api::responseData(array('cities' => $city));
    }

}
