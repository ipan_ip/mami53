<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Helpers\ApiResponse as Api;
use App\Repositories\PremiumRepository;
use App\Repositories\Premium\AdHistoryRepositoryEloquent;
use App\Repositories\Promoted\AdsDisplayTrackerRepositoryEloquent;
use App\Entities\Activity\Love;
use App\Entities\Premium\AdHistory;
use App\Entities\Premium\AdsInteractionTracker;
use App\Entities\Room\Room;
use App\User;
use Illuminate\Support\Facades\Mail;
use App\Entities\Premium\PremiumStopReason;
use Jenssegers\Agent\Agent;
use App\Entities\Activity\Tracking;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use StatsLib;
use App\Services\PremiumService;
use App\Entities\Premium\PremiumPackage;
use App\Http\Requests\Premium\PremiumPurchaseRequest;
use App\Services\Premium\PremiumRequestService;
use App\Http\Requests\Premium\PremiumBalanceRequest;
use App\Http\Helpers\ApiErrorHelper;
use App\Entities\Api\ErrorCode;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;

class PremiumController extends BaseController
{
    protected $repository;
    protected $premiumService;

    protected $premiumRequestService;
    protected $validationMessages = [
        'name.required' => 'Nama Pemilik Rekening harus diisi',
        'name.max' => 'Nama Pemilik Rekening maksimal :max karakter',
        'bank.required' => 'Nama Bank harus diisi',
        'transfer_date.required' => 'Tanggal transfer tidak boleh kosong',
        'transfer_date.date' => 'Tanggal transfer tidak sesuai format',
        'transfer_date.after_or_equal' => 'Tanggal transfer maksimal 2 minggu sebelumnya',
        'transfer_date.before_or_equal' => 'Tanggal transfer minimal hari ini',
    ];

    public function __construct(
        PremiumRepository $repository,
        PremiumRequestService $premiumRequestService,
        PremiumService $premiumService
    ) {
        $this->repository = $repository;
        $this->premiumRequestService = $premiumRequestService;
        $this->premiumService = $premiumService;
        parent::__construct();
    }

    public function getListBank(Request $request)
    {
    	$bank = $this->repository->getBankAccount();

        return Api::responseData(['bank_accounts' => $bank]);
    }

    public function getPremiumPackage(Request $request)
    {
        $agent = new Agent();
        $user  = $this->user();
        (new Tracking)->trackingInsert($agent, $user, PremiumPackage::PACKAGE_TYPE);

        if ($request->filled('v')) {
            $packages = $this->premiumService->getPremiumPackages(PremiumPackage::PACKAGE_TYPE, $user);
        } else {
            $packages = $this->repository->getPremiumPackage(PremiumPackage::PACKAGE_TYPE, $user, $request->all());
        }
        
        return Api::responseData($packages);
    }

    public function getPremiumBalance(Request $request)
    {
        $package = $this->repository->getPremiumPackage('balance', $this->user(), $request->all());

        $agent = new Agent();
        $user  = $this->user();
        $type  = "top_up";

        (new Tracking)->trackingInsert($agent, $user, $type);
        return Api::responseData($package);
    }

    public function postConfirm(Request $request)
    {
        $twoWeekBefore = Carbon::now()->subWeeks("2")->format('Y-m-d');
        $validator = Validator::make($request->all(), [
                'name' => 'required|max:190',
                'bank' => 'required',
                'transfer_date' => 'required|date|after_or_equal:'.$twoWeekBefore.'|before_or_equal:'.date('Y-m-d'),
            ], $this->validationMessages);

        if ($validator->fails()) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'messages' => $validator->errors()->all()
                ]
            ]);

        }

        $data = $request->input();

        if ($request->filled('buy_balance_id')) {
            $data['buy_balance_id'] = $request->input('buy_balance_id');
        } else {
            $data['buy_balance_id'] = 0;
        }

        if ($request->filled('premium_id')) {
            $data['premium_id'] = $data['premium_id'];
        } else {
            $data['premium_id'] = 0;
        }

        $confirm = $this->repository->confirm($data, $this->user()->id);

        return Api::responseData(['data' => $confirm]);
    }

    public function requestOwner(Request $request)
    {
        $data = $request->input();
        $data['user_id'] = $this->user()->id;
        (new Tracking)->deleteRequest('package', $data['user_id']);
        if ($request->filled('is_cancel')) {

            $data['is_cancel'] = $request->input('is_cancel');

        } else {

            $data['is_cancel'] = false;

        }

        $proses = $this->repository->prosesRequestOwner($data, $this->user());

        return Api::responseData($proses);
    }

    public function premiumPurchase(PremiumPurchaseRequest $request)
    {
        $validator = $request->failedValidator;
        if (!is_null($validator) && $validator->fails()) {
            return Api::responseData([
                'status' => false,
                'message' => implode(",", $validator->errors()->all()),
            ]);
        }

        $user = $this->user();
        if (is_null($user) || $user->is_owner === User::IS_NOT_OWNER) {
            return Api::responseData([
                "status" => false,
                "message" => "Akun pemilik tidak ditemukan"
            ]);
        }

        $premiumPurchase = $this->premiumRequestService->premiumPackageRequest($request->all(), $user);
        return Api::responseData($premiumPurchase);
    }

    public function buyBalance(PremiumBalanceRequest $request)
    {
        $validator = $request->failedValidator;
        if (!is_null($validator) && $validator->fails()) {
            return Api::responseData([
                'status' => false,
                'message' => implode(",", $validator->errors()->all()),
            ]);
        }

        $user = $this->user();
        if ($user->is_owner === User::IS_NOT_OWNER) {
            return Api::responseData([
                "status" => false,
                "message" => "Akun pemilik tidak ditemukan"
            ]);
        }

        (new Tracking)->deleteRequest('balance', $user->id);
        return Api::responseData($this->premiumRequestService->premiumBalanceRequest($request->all(), $user));
    }

    public function requestDirection(Request $request)
    {
        $validator = Validator::make($request->all(), [
                          'request-item'=>'required',
                          'name'=>'required',
                          'phone' => 'required',
                          'email' => 'required',
                          'status'=>'required',
                          'kost-name'=>'required',
                          'kost-url'=>'url'
                      ], [
                          'request-item.required'=>'Jenis permintaan harus diisi',
                          'name.required'=>'Nama harus diisi',
                          'phone.required'=>'Nomor telepon harus diisi',
                          'email.required'=>'Email harus diisi',
                          'status.required'=>'Status harus diisi',
                          'kost-name.required'=>'Nama kost harus diisi',
                          'kost-url.url'=>'URL kost tidak valid'
                      ]);

        if($validator->fails()) {
            return Api::responseData([
                      'status'=>false,
                      'meta'=>[
                          'message'=>implode(', ', $validator->errors()->all())
                      ]
                  ]);
        }

        $requestItems = $request->input('request-item');
        $userName = $request->input('name');
        $userPhone = $request->input('phone');
        $userEmail = $request->input('email');
        $userStatus = $request->input('status');
        $kostName = $request->input('kost-name');
        $kostUrl = $request->input('kost-url');

        Mail::send('emails.premium-direction', compact('requestItems', 'userName', 'userPhone', 'userEmail', 'userStatus', 'kostName', 'kostUrl'), function($message)
        {
            $message->to('team@mamikos.com')->subject('Permintaan Penjelasan Premium Membership');
        });

        if(in_array('Ingin Daftarkan Kost Saya Untuk Layanan Standarisasi', $requestItems)) {
            Mail::send('emails.franchise-request', compact('requestItems', 'userName', 'userPhone', 'userEmail', 'userStatus', 'kostName', 'kostUrl'), function($message)
            {
                $message->to('franchise@mamikos.com')->subject('Permintaan Pendaftaran Standardisasi Kost');
            });
        }

        return Api::responseData(['status'=>true]);
    }

    public function registerNetwork(Request $request)
    {
        $validator = Validator::make($request->all(), [
                          'name'=>'required',
                          'status' => 'required',
                          'address' => 'required',
                          'phone' => 'required',
                          'email' => 'required|email',
                          'input-address'=>'nullable',
                          'detail-kost' => 'nullable'
                      ], [
                          'name.required'=>'Nama harus diisi',
                          'status.required'=>'Status harus diisi',
                          'address'=>'Alamat harus diisi',
                          'phone.required'=>'Nomor telepon harus diisi',
                          'email.required'=>'Email harus diisi',
                          'email.email' => 'Format email yang Anda masukkan tidak valid'
                      ]);

        if($validator->fails()) {
            return Api::responseData([
                      'status'=>false,
                      'meta'=>[
                          'message'=>implode(', ', $validator->errors()->all())
                      ]
                  ]);
        }

        $userName = $request->input('name');
        $userPhone = $request->input('phone');
        $userEmail = $request->input('email');
        $userStatus = $request->input('status');
        $kostAddress = $request->input('address');
        $kostInputAddress = $request->input('input-address');
        $kostAddressDetail = $request->input('detail-kost');

        Mail::send('emails.network-request', compact('userName', 'userPhone', 'userEmail', 'userStatus',
            'kostAddress', 'kostInputAddress', 'kostAddressDetail'), function($message)
        {
            $message->to('franchise@mamikos.com')->subject('Permintaan Bergabung Mamikos Network');
        });

        return Api::responseData(['status'=>true]);
    }

    public function stopReason(Request $request)
    {
        $user = $this->user();
        $reason = $request->input('reason');

        $stopReason = new PremiumStopReason;
        $stopReason->user_id = $user->id;
        $stopReason->reason = $reason;
        $stopReason->save();

        return Api::responseData(['status'=>true]);
    }

    public function cancelPurchase(Request $request)
    {
       $user = $this->user();

       $data = array(
                 "for" => "request",
                 "user_id" => $user->id,
                 "reason"  => $request->input('reason')
        );

       PremiumStopReason::create($data);
       return Api::responseData(['status'=>true]);
    }

    public function autoApproveTrial(Request $request)
    {
        $response = $this->autoApproveAction($request);
        return Api::responseData([
            'status' => $response['status'], 
            'message' => $response['message']
        ]);
    }

    public function autoApproveAction(Request $request)
    {
        $user = $this->user();
        if (is_null($user) || $user->is_owner == 'false') {
            return [
                'status' => false, 
                'message' => "User tidak ditemukan"
            ];
        }

        if (!is_null($user->date_owner_limit)) {
            return [
                'status' => false, 
                'message' => "Anda tidak bisa melakukan trial"
            ];
        }

        $response = $this->repository->autoApproveTrial($user, $request->all());
        return $response;
    }

    public function getConfirmation()
    {
        $user = $this->user();
        if (is_null($user) || $user->is_owner == 'false') {
            return Api::responseData([
                'status' => false,
                'message' => 'Anda belum login '
            ]);
        }
        $response = $this->repository->getConfirmation($user);
        return Api::responseData([
            "status" => true,
            "message" => "",
            "membership" => $response['membership'],
            "bank" => $response["bank"]
        ]);
    }

    public function history(Request $request)
    {
        $user = $this->user();
        if (is_null($user) || $user->is_owner !== User::IS_OWNER) {
            return Api::responseData([
                "status" => false,
                "message" => "Anda belum login"
            ]);
        }

        $premiumRequestHistory = $this->repository->getHistory($user);

        return Api::responseData($premiumRequestHistory);
    }

    public function mostFavorite(Request $request)
    {
        $user = $this->user();
        $range = $request->filled('range') ? $request->input('range') : StatsLib::Range30Days;

        $adHistoryRepo = new AdHistoryRepositoryEloquent(new AdHistory);
        $mostClicks = $adHistoryRepo->getRoomsMostClick($user, $range);

        $result = [];

        if (!is_null($mostClicks)) {
            $result = array_slice($mostClicks, 0, 3);
        }

        return Api::responseData([
            "status" => true,
            "data" => $result
        ]);
    }

    public function getRoomsStatsisticSummary(Request $request)
    {
        $now = Carbon::now();
        $range = $request->filled('range') ? $request->input('range') : StatsLib::RangeToday;

        // it's used to get summary total_click, used_balance and ads_cost
        $adHistoryRepo = new AdHistoryRepositoryEloquent(new AdHistory);
        $adHistoryStats = $adHistoryRepo->getAdsRoomStatsSummary($this->user(), $range);

        $totalClick = [
            'type' => AdhistoryRepositoryEloquent::CLICK_STATS_KEY,
            'text' => 'Total Klik',
            'value' => (int) $adHistoryStats['click']
        ];
        $usedBalance = [
            'type' => AdHistoryRepositoryEloquent::BURNING_BALANCE_STATS_KEY,
            'text' => 'Saldo Terpakai',
            'value' => (int) $adHistoryStats['burning_balance']  
        ];
        $adsCost = [
            'type' => AdHistoryRepositoryEloquent::ADS_COST_STATS_KEY,
            'text' => 'Biaya Iklan',
            'value' => (int) $adHistoryStats['ads_cost']  
        ];
        
        $totalFavorit = AdsInteractionTracker::getLoveStatisticSummaryFromAdsRooms($this->user(), $range);

        $nowFmt = $now->format('d M Y');
        $nowSimpleFmt = $now->format('d M');
        $yesterdayFmt = $now->copy()->subDay()->format('d M Y');
        $sevenDayFmt = $now->copy()->subDays(6)->format('d M');
        $last30DayFmt = $now->copy()->subDays(29)->format('d M');

        return Api::ResponseData([
            'status' => true,
            'result' => [
                'data' => [
                    $totalClick,
                    $usedBalance,
                    $totalFavorit,
                    $adsCost
                ],
                'range_filter' => [
                    [
                        'range' => StatsLib::RangeYesterday,
                        'text'  => 'Kemarin (' . $yesterdayFmt . ')'
                    ],
                    [
                        'range' => StatsLib::RangeToday,
                        'text'  => 'Hari ini (' . $nowFmt . ')'
                    ],
                    [
                        'range' => StatsLib::Range7Days,
                        'text'  => '7 Hari Terakhir (' . $sevenDayFmt . ' - ' . $nowSimpleFmt . ')'
                    ],
                    [
                        'range' => StatsLib::Range30Days,
                        'text'  => '30 Hari Terakhir (' . $last30DayFmt . ' - ' . $nowSimpleFmt . ')'
                    ]
                ]
            ]
        ]);
    }

    public function getRoomsStatistic(Request $request)
    {
        $user = $this->user();
        $type = $request->filled('type') ? $request->input('type') : AdHistoryRepositoryEloquent::CLICK_STATS_KEY;
        $range = $request->filled('range') ? $request->input('range') : StatsLib::RangeToday;

        $result = [];

        // it's used to get summary total_click, used_balance and ads_cost
        $adHistoryRepo = new AdHistoryRepositoryEloquent(new AdHistory);

        switch ($type)
        {
            case AdHistoryRepositoryEloquent::BURNING_BALANCE_STATS_KEY :
                $result = $adHistoryRepo->getBurningBalanceStats($user, $range);
                break;
            case AdHistoryRepositoryEloquent::ADS_COST_STATS_KEY :
                $result = $adHistoryRepo->getAdsCostsStats($user, $range);
                break;
            case AdsInteractionTracker::TOTAL_FAVORIT_STATS_KEY :
                $result = AdsInteractionTracker::getLoveStatisticFromAdsRooms($user, $range);
                break;
            default: // default is total_click
                $result = $adHistoryRepo->getRoomsClickStatistic($user, $range);
                break;
        }

        return Api::responseData([
            'status' => true,
            'result' => [
                'data' => $result,
            ]
        ]);
    }

    public function getRoomStatsisticSummary(Request $request, $songId)
    {
        $now = Carbon::now();
        $user = $this->user();
        $range = $request->filled('range') ? $request->input('range') : StatsLib::RangeToday;

        $room = Room::select('id')->where('song_id', $songId)->first();

        if (is_null($room)) {
            return Api::ResponseData([
                'status' => false,
                'meta' => [
                    'message' => 'Data tidak ditemukan'
                ]
            ]);
        }

        $roomId = [$room->id];

        // it's used to get summary total_click, used_balance and ads_cost
        $adHistoryRepo = new AdHistoryRepositoryEloquent(new AdHistory);
        $adHistoryStats = $adHistoryRepo->getAdsRoomStatsSummary($user, $range, $roomId);

        $totalClick = [
            'type' => AdhistoryRepositoryEloquent::CLICK_STATS_KEY,
            'text' => 'Total Klik',
            'value' => (int) $adHistoryStats['click']
        ];
        $usedBalance = [
            'type' => AdHistoryRepositoryEloquent::BURNING_BALANCE_STATS_KEY,
            'text' => 'Saldo Terpakai',
            'value' => (int) $adHistoryStats['burning_balance']  
        ];
        $adsCost = [
            'type' => AdHistoryRepositoryEloquent::ADS_COST_STATS_KEY,
            'text' => 'Biaya Iklan',
            'value' => (int) $adHistoryStats['ads_cost']  
        ];

        $totalFavorit = AdsInteractionTracker::getLoveStatisticSummaryFromAdsRooms($user, $range, $roomId);

        $nowFmt = $now->format('d M Y');
        $nowSimpleFmt = $now->format('d M');
        $yesterdayFmt = $now->copy()->subDay()->format('d M Y');
        $sevenDayFmt = $now->copy()->subDays(6)->format('d M');
        $last30DayFmt = $now->copy()->subDays(29)->format('d M');

        return Api::ResponseData([
            'status' => true,
            'result' => [
                'data' => [
                    $totalClick,
                    $usedBalance,
                    $totalFavorit,
                    $adsCost
                ],
                'range_filter' => [
                    [
                        'range' => StatsLib::RangeYesterday,
                        'text'  => 'Kemarin (' . $yesterdayFmt . ')'
                    ],
                    [
                        'range' => StatsLib::RangeToday,
                        'text'  => 'Hari ini (' . $nowFmt . ')'
                    ],
                    [
                        'range' => StatsLib::Range7Days,
                        'text'  => '7 Hari Terakhir (' . $sevenDayFmt . ' - ' . $nowSimpleFmt . ')'
                    ],
                    [
                        'range' => StatsLib::Range30Days,
                        'text'  => '30 Hari Terakhir (' . $last30DayFmt . ' - ' . $nowSimpleFmt . ')'
                    ]
                ]
            ]
        ]);
    }

    public function getRoomStatistic(Request $request, $songId)
    {
        $user = $this->user();
        $type = $request->filled('type') ? $request->input('type') : AdHistoryRepositoryEloquent::CLICK_STATS_KEY;
        $range = $request->filled('range') ? $request->input('range') : StatsLib::RangeToday;

        $result = [];

        $room = Room::where('song_id', $songId)->first();

        if (is_null($room)) {
            return Api::ResponseData([
                'status' => false,
                'meta' => [
                    'message' => 'Data tidak ditemukan'
                ]
            ]);
        }

        $roomId = [$room->id];

        // it's used to get summary total_click, used_balance and ads_cost
        $adHistoryRepo = new AdHistoryRepositoryEloquent(new AdHistory);

        switch ($type)
        {
            case AdHistoryRepositoryEloquent::BURNING_BALANCE_STATS_KEY :
                $result = $adHistoryRepo->getBurningBalanceStats($user, $range, $roomId);
                break;
            case AdHistoryRepositoryEloquent::ADS_COST_STATS_KEY :
                $result = $adHistoryRepo->getAdsCostsStats($user, $range, $roomId);
                break;
            case AdsInteractionTracker::TOTAL_FAVORIT_STATS_KEY :
                $result = AdsInteractionTracker::getLoveStatisticFromAdsRooms($user, $range, $roomId);
                break;
            default: // default is total_click
                $result = $adHistoryRepo->getRoomsClickStatistic($user, $range, $roomId);
                break;
        }

        return Api::responseData([
            'status' => true,
            'result' => [
                'data' => $result,
            ]
        ]);
    }

    public function getInvoice()
    {
        $user = $this->user();

        if (empty($user) || !$user->isOwner()) {
            $response = ApiErrorHelper::generateErrorResponse(
                ErrorCode::INVALID_USER
            );

            return Api::responseData($response, false, false);
        }

        $invoice = $this->repository->getDetailInvoice($user);

        return Api::responseData($invoice);
    }

}
