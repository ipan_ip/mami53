<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Repositories\OwnerDataRepository;
use App\Http\Helpers\ApiResponse as Api;

class OwnerLoyaltyController extends BaseController
{
    private $repository;

    public function __construct(OwnerDataRepository $repository)
    {
        $this->repository = $repository;

        parent::__construct();
    }

    public function getData(Request $request)
    {
    	if (is_null($this->user()) || $this->user()->is_owner == 'false') { 
            return Api::responseData([
                "status" => false,
                "message" => "Gagal mendapatkan data."
            ]);
        }

        $data = $this->repository->loyaltyData($this->user()->phone_number);

        return Api::responseData($data);
    }
}
