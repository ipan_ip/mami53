<?php

namespace App\Http\Controllers\Api\RoomAllotment;

use App\Entities\Owner\ActivityType;
use App\Entities\Room\Room;
use App\Enums\Goldplus\Permission\GoldplusPermission;
use App\Exceptions\RoomAllotmentException;
use App\Http\Controllers\Api\BaseController;
use App\Http\Helpers\ApiResponse as Api;
use App\Http\Helpers\GoldplusPermissionHelper;
use App\Jobs\Owner\MarkActivity;
use App\Jobs\Owner\OwnerRevertThanos;
use App\Presenters\Room\RoomUnitPresenter;
use App\Services\RoomUnit\RoomUnitService;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Closure;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RoomAllotmentController extends BaseController
{
    protected $validationMessages = [
        'id.required' => 'ID tidak boleh kosong',
        'song_id.required' => 'Room ID tidak boleh kosong',
        'name.required' => 'Nomor/Nama tidak boleh kosong',
        'name.max' => 'Nomor/Nama max :max karakter',
        'floor.max' => 'Lantai max :max karakter',
        'occupied.required' => 'Field Sudah berpenghuni tidak boleh kosong',
    ];

    /**
     * Add room allotment
     *
     * @param Request $request
     * @param RoomUnitService $service
     * @param RoomUnitPresenter $presenter
     * @return JsonResponse
     * @throws RoomAllotmentException
     */
    public function addRoomUnit(
        Request $request,
        RoomUnitService $service,
        RoomUnitPresenter $presenter
    ): JsonResponse {
        $data = $request->all();

        $validator = Validator::make(
            $data,
            [
                'song_id' => 'required',
                'name' => 'required|max:50',
                'floor' => 'max:50',
                'occupied' => 'required',
            ],
            $this->validationMessages
        );

        if ($validator->fails()) {
            return Api::responseData([
                "status" => false,
                "meta" => [
                    "code" => 400,
                    "message" => $validator->errors()->all()
                ]
            ]);
        }

        $user = $this->user();
        $room = Room::with('owners')->where('song_id', $request->input('song_id'))->first();
        if (is_null($user) || is_null($room) || !$room->ownedBy($user)) {
            return Api::responseData([
                "status" => false,
                "meta" => [
                    "code" => 401,
                    "message" => "Unauthorized"
                ]
            ]);
        }
        if (!GoldplusPermissionHelper::isRoomAllow($room, GoldplusPermission::ROOM_UNIT_UPDATE)) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'code' => 403,
                    'message' => __('api.access.forbidden'),
                ],
            ]);
        }
        OwnerRevertThanos::dispatch($room->id);
        MarkActivity::dispatchNow($user->id, ActivityType::TYPE_UPDATE_PROPERTY);

        $data['name'] = trim($data['name']);
        $data['floor'] = trim($data['floor']);

        try {
            $unit = $service->addRoomUnit($data);
            $roomIsGPLevel = $room->isRoomGPLevel();
            $roomIsBBK = $room->isBisaBooking();

            return Api::responseData(
                [
                    'unit' => $presenter->present($unit),
                    'room-data' => [
                        'is_bbk'     => $roomIsBBK,
                        'is_gp_kos'  => $roomIsGPLevel,
                    ],
                ],
                true,
                true,
                200,
                'Kamar berhasil ditambahkan'
            );
        } catch (ModelNotFoundException $modelException) {
            Bugsnag::notifyError(
                ModelNotFoundException::class,
                $modelException->getMessage(),
                $this->getBugsnagCallbackReport($data)
            );

            return Api::responseData([
                "status" => false,
                "meta" => [
                    "code" => 400,
                    "message" => $modelException->getMessage()
                ]
            ]);
        }
    }


    /**
     * Update room allotment
     *
     * @param RoomUnitService $service
     * @param Request $request
     * @param int $id
     * @param RoomUnitPresenter $presenter
     * @return JsonResponse
     * @throws \Exception
     */
    public function editRoomUnit(
        RoomUnitService $service,
        Request $request,
        int $id,
        RoomUnitPresenter $presenter
    ): JsonResponse {
        $data = $request->all();

        $validator = Validator::make(
            $data,
            [
                'song_id' => 'required',
                'name' => 'required|max:50',
                'floor' => 'max:50',
                'occupied' => 'required',
            ],
            $this->validationMessages
        );

        if ($validator->fails()) {
            return Api::responseData([
                "status" => false,
                "meta" => [
                    "code" => 400,
                    "message" => $validator->errors()->all()
                ]
            ]);
        }

        $user = $this->user();
        $room = Room::with('owners')->where('song_id', $request->input('song_id'))->first();
        if (is_null($user) || is_null($room) || !$room->ownedBy($user)) {
            return Api::responseData([
                "status" => false,
                "meta" => [
                    "code" => 401,
                    "message" => "Unauthorized"
                ]
            ]);
        }
        if (!GoldplusPermissionHelper::isRoomAllow($room, GoldplusPermission::ROOM_UNIT_UPDATE)) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'code' => 403,
                    'message' => __('api.access.forbidden'),
                ],
            ]);
        }
        OwnerRevertThanos::dispatch($room->id);
        MarkActivity::dispatchNow($user->id, ActivityType::TYPE_UPDATE_PROPERTY);

        try {
            $room = Room::with([
                'room_unit' => function ($query) use ($id) {
                    return $query->where('id', $id);
                }
            ])->where('song_id', $data['song_id'])->first();

            if ($room == null) {
                return Api::responseData([
                    "status" => false,
                    "meta" => [
                        "code" => 404,
                        "message" => "Room not found"
                    ]
                ]);
            }

            $unit = $room->room_unit->first();
            if ($unit === null) {
                return Api::responseData([
                    "status" => false,
                    "meta" => [
                        "code" => 404,
                        "message" => "Room Unit not found"
                    ]
                ]);
            }
            $isOccupied = $unit->isOccupied();
            $data['name'] = trim($data['name']);
            $data['floor'] = trim($data['floor']);

            $saved = $service->updateSingleUnit($room, $unit, $data);
            $transformedUnits = $presenter->present($saved);

            $roomIsGPLevel = $room->isRoomGPLevel();
            $roomIsBBK = $room->isBisaBooking();
            $response['room-data'] = [
                'is_bbk'    => $roomIsBBK,
                'is_gp_kos' => $roomIsGPLevel,
            ];

            if ($data['occupied'] != $isOccupied) {
                if ($saved->occupied) {
                    $response['unit'] = $transformedUnits;
                    $massage = 'Kamar Terisi Bertambah 1';
                } else {
                    $response['unit'] = $transformedUnits;
                    $massage = 'Kamar Kosong Bertambah 1';
                }
            } else {
                $response['unit'] = $transformedUnits;
                $massage = 'Anda berhasil update data kamar';
            }

            return Api::responseData($response,
                true,
                true,
                200,
                $massage);

        } catch (ModelNotFoundException $modelException) {
            Bugsnag::notifyError(
                ModelNotFoundException::class,
                $modelException->getMessage(),
                $this->getBugsnagCallbackReport(array_merge($data, ['unit_id' => $id]))
            );

            return Api::responseData([
                "status" => false,
                "meta" => [
                    "code" => 404,
                    "message" => $modelException->getMessage()
                ]
            ]);
        } catch (QueryException $e) {
            Bugsnag::notifyError('Info', $e->getMessage());

            return Api::responseData([
                "status" => false,
                "meta" => [
                    "code" => 400,
                    "message" => "Nama sudah pernah digunakan"
                ]
            ]);
        }
    }


    /**
     * @param RoomUnitService $service
     * @param int $id
     * @return JsonResponse
     */
    public function deleteRoomUnit(
        RoomUnitService $service,
        int $id
    ): JsonResponse {
        $user = $this->user();
        $room = Room::with('owners')->whereHas('room_unit', function ($q) use ($id) {
            $q->where('id', $id);
        })->first();
        if (is_null($user) || is_null($room) || !$room->ownedBy($user)) {
            return Api::responseData([
                "status" => false,
                "meta" => [
                    "code" => 401,
                    "message" => "Unauthorized"
                ]
            ]);
        }
        if (!GoldplusPermissionHelper::isRoomAllow($room, GoldplusPermission::ROOM_UNIT_DELETE)) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'code' => 403,
                    'message' => __('api.access.forbidden'),
                ],
            ]);
        }
        OwnerRevertThanos::dispatch($room->id);
        MarkActivity::dispatchNow($user->id, ActivityType::TYPE_UPDATE_PROPERTY);

        try {
            $service->deleteRoomUnit($id);
            return Api::responseData([
                'status' => true,
                'messages' => 'Room Unit ' . $id . ' berhasil dihapus'
            ]);
        } catch (ModelNotFoundException $exception) {
            Bugsnag::notifyError(
                ModelNotFoundException::class,
                $exception->getMessage(),
                $this->getBugsnagCallbackReport(['unit_id' => $id])
            );

            return Api::responseData([
                "status" => false,
                "meta" => [
                    "code" => 404,
                    "message" => $exception->getMessage()
                ]
            ]);
        }
    }

    /**
     * Update Bulk room unit
     *
     * @param RoomUnitService $service
     * @param Request $request
     * @return JsonResponse
     * @throws ModelNotFoundException
     * @throws RoomAllotmentException
     */
    public function updateBulkRoomUnit(
        RoomUnitService $service,
        Request $request
    ): JsonResponse {
        $data = $request->all();

        $validator = Validator::make(
            $data,
            [
                'song_id' => 'required',
            ],
            $this->validationMessages
        );

        if ($validator->fails()) {
            return Api::responseData([
                "status" => false,
                "meta" => [
                    "code" => 400,
                    "message" => $validator->errors()->all()
                ]
            ]);
        }

        $user = $this->user();
        $room = Room::with('owners')->where('song_id', $request->input('song_id'))->first();
        if (is_null($user) || is_null($room) || !$room->ownedBy($user)) {
            return Api::responseData([
                "status" => false,
                "meta" => [
                    "code" => 401,
                    "message" => "Unauthorized"
                ]
            ]);
        }
        if (!GoldplusPermissionHelper::isRoomAllow($room, GoldplusPermission::ROOM_UNIT_UPDATE)) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'code' => 403,
                    'message' => __('api.access.forbidden'),
                ],
            ]);
        }
        OwnerRevertThanos::dispatch($room->id);
        MarkActivity::dispatchNow($user->id, ActivityType::TYPE_UPDATE_PROPERTY);

        try {
            $data['add'] = isset($data['add']) && is_array($data['add']) ? $data['add'] : [];
            $data['update'] = isset($data['update']) && is_array($data['update']) ? $data['update'] : [];
            $data['delete'] = isset($data['delete']) && is_array($data['delete']) ? $data['delete'] : [];

            $result = $service->updateBulk(
                $data['song_id'],
                $data['add'],
                $data['update'],
                $data['delete']
            );

            return Api::responseData([
                'status' => true,
                'success' => $result['success'],
                'failed' => $result['failed']
            ]);
        } catch (ModelNotFoundException $notFoundException) {
            Bugsnag::notifyError(
                ModelNotFoundException::class,
                $notFoundException->getMessage(),
                $this->getBugsnagCallbackReport($data)
            );

            return Api::responseData([
                "status" => false,
                "meta" => [
                    "code" => 404,
                    "message" => $notFoundException->getMessage()
                ]
            ]);
        } catch (RoomAllotmentException $allotmentException) {
            Bugsnag::notifyError(
                RoomAllotmentException::class,
                $allotmentException->getMessage(),
                $this->getBugsnagCallbackReport($data)
            );

            return $allotmentException->render();
        }
    }

    private function getBugsnagCallbackReport($data = []): Closure {
        return static function ($report) use ($data) {
            $report->setSeverity('info');
            $report->setMetaData([
                'data' => $data,
            ]);
        };
    }
}
