<?php
namespace App\Http\Controllers\Api;

use App\Http\Helpers\ApiResponse;
use App\Presenters\ApartmentPresenter;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Helpers\ApiHelper;
use Illuminate\Http\Response;
use Validator;
use App\Repositories\RoomRepository;
use App\Repositories\ApartmentProjectRepositoryEloquent;
use App\Entities\Room\RoomFilter;
use App\Entities\User\UserSearchHistoryTemp;
use App\Entities\User\UserSearchHistory;
use App\Entities\User\ZeroResult;
use App\Entities\Apartment\ApartmentProject;
use App\Entities\Apartment\ApartmentProjectSlug;
use Session;


class ApartmentController extends BaseController
{
    protected $repository;

    public function __construct(RoomRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display Apartment
     *
     * @param Request $request
     * @param $songId
     * @return JsonResponse
     * @throws Exception
     */
    public function show(Request $request, $songId)
    {
        $source = $request->filled('source') ? $request->get('source') : 'default';

        $this->repository->setPresenter(
            new ApartmentPresenter(
                'detail',
                [
                    'source' => $source,
                    'user' => $this->user()
                ]
            )
        );

        $room = $this
            ->repository
            ->with(
                [
                    'minMonth',
                    'tags.photo',
                    'tags.photoSmall',
                    'review',
                    'avg_review',
                    'owners',
                    'apartment_project',
                    'apartment_project.tags',
                    'apartment_project.tags.photo',
                    'apartment_project.tags.photoSmall'
                ]
            )
            ->scopeQuery(
                function ($q) use ($songId) {
                    if (is_numeric($songId)) {
                        return $q->where('song_id', $songId);
                    } else {
                        return $q->where('slug', $songId);
                    }
                }
            )
            ->all();

        return ApiResponse::responseData(
            [
                'apartment' => collect($room['data'])->first()
            ]
        );
    }

    /**
     * Copy of @show function with different parameters
     * to handle deeplink from web
     */
    public function showUnit(Request $request, $slugApartmentProject, $slug)
    {
        $apartmentProject = ApartmentProject::where('slug', $slugApartmentProject)
                                        ->where('is_active', 1)
                                        ->first();

        if(!$apartmentProject) {
            // if apartment project not found, then check from apartment project slug history
            $slugHistory = ApartmentProjectSlug::where('slug', $slugApartmentProject)
                                        ->orderBy('created_at', 'desc')
                                        ->first();

            // set $useApartmentHistory variable to true and replace apartment project data 
            // if data found in slug history
            if($slugHistory) {
                $apartmentProject = ApartmentProject::find($slugHistory->apartment_project_id);
            }
        }

        if (!$apartmentProject) {
            return ApiResponse::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Data tidak ditemukan'	
                ]
            ]);
        }

        $source = 'default';

        if($request->filled('source')) $source = $request->get('source');

        $this->repository->setPresenter(new ApartmentPresenter('detail', ['source'=>$source]));

        $room = $this->repository->with(['minMonth', 'tags.photo', 'tags.photoSmall', 'review', 'avg_review', 'owners', 'apartment_project', 'apartment_project.tags', 'apartment_project.tags.photo', 'apartment_project.tags.photoSmall'])->findWhere([
                'apartment_project_id' => $apartmentProject->id,
                'slug' => $slug
            ]);

        return ApiResponse::responseData(array('apartment' => collect($room['data'])->first()));
    }


    public function listApartmentByProject(Request $request, $projectId, $projectCode)
    {
        $apartmentProjectRepository = new ApartmentProjectRepositoryEloquent(app());
        $apartmentProject = $apartmentProjectRepository->getByIdAndCode($projectId, $projectCode);

        if(!$apartmentProject) {
            return ApiResponse::responseData([
                'status'=>false,
                'meta'=>[
                    'message'=>'Apartemen tidak ditemukan'
                ]
            ]);
        }

        $this->repository->setPresenter(new ApartmentPresenter('list'));

        $filters = $request->filters;
        $filters['include_apartment'] = true;
        $filters['property_type'] = 'apartment';
        $filters['sorting'] = $request->input('sorting');

        // get central point
        $centerLat = $apartmentProject->latitude;
        $centerLon = $apartmentProject->longitude;
        $filters['titik_tengah'] = $centerLat." , ".$centerLon;

        $this->repository->pushCriteria(new \App\Criteria\Apartment\UnitInProjectCriteria($projectId));
        $this->repository->pushCriteria(new \App\Criteria\Room\MainFilterCriteria($filters));

        if($request->filled('include_promoted') && $request->input('include_promoted') == true) {
            $response = $this->repository->getItemListWithPromoted(new RoomFilter($filters));
        } else {
            $response = $this->repository->getItemList(new RoomFilter($filters));
        }

        (new UserSearchHistoryTemp)->storeHistoryFilter($filters, count($response['rooms']) == 0, $this->user(), $this->device(), Session::getId());

        return ApiResponse::responseData($response);
    }
}