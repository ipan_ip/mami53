<?php

namespace App\Http\Controllers\Api\Marketplace;

use App\Http\Helpers\RegexHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseController;
use App\Http\Helpers\ApiResponse as Api;
use Validator;
use Auth;

use App\Repositories\Marketplace\MarketplaceInputRepository;
use App\Entities\Marketplace\MarketplaceProduct;
/**
* 
*/
class MarketplaceInputController extends BaseController
{
	
	protected $repository;

	protected $validationMessages = [
		'title.required' => 'Judul Iklan harus diisi',
		'title.max' => 'Judul Iklan maksimal :max karakter',
		'price.required' => 'Harga harus diisi',
		'price.numeric' => 'Harga harus berupa angka',
		'price_unit.max' => 'Satuan makimal :max karakter',
		'contact_phone.required' => 'Nomor Kontak harus diisi',
		'contact_phone.regex' => 'Format Nomor Kontak tidak valid (08xxxxxxxx)',
		'latitude.required' => 'Lokasi harus diisi',
		'latitude.numeric' => 'Lokasi tidak valid',
		'longitude.required' => 'Lokasi harus diisi',
		'longitude.numeric' => 'Lokasi tidak valid',
		'address.max' => 'Alamat maksimal :max karakter',
		'price_unit.in' => "Unit produk tidak ada dalam pilihan",
		'product_condition.required' => 'Kondisi produk boleh kosong',
		'product_condition.in' => 'Kondisi produk ada dalam pilihan',
		'product_type.required' => 'Tipe tidak boleh kosong',
		'product_type.in' => 'Tipe produk tidak ada dalam pilihan'
	];

	function __construct(MarketplaceInputRepository $repository)
	{
		$this->repository = $repository;

		parent::__construct();
	}

	/**
	 * Function to save new product
	 * 
	 * @param Request $request
	 */
	public function store(Request $request)
	{
		$user = $this->user();

		if(!$user) {
			return Api::responseData([
				'status' => false,
				'meta' => [
					'messages' => ['Anda harus login terlebih dahulu'],
					'message' => 'Anda harus login terlebih dahulu'
				]
			]);
		}

		$validator = Validator::make($request->all(),
			[
				'title' 			=> 'required|max:190',
				'price' 			=> 'required|numeric',
				'price_unit' 		=> 'nullable',
				'product_condition' => 'nullable|in:'.implode(",", MarketplaceProduct::CODITION_PRODUCT),
				'product_type'		=> 'required|in:'.implode(",", MarketplaceProduct::TYPE_PRODUCT),
				'user_name' 		=> 'nullable',
				'contact_phone' 	=> 'required|regex:/^(08[1-9]\d{7,10})$/',
				'latitude' 			=> 'required|numeric',
				'longitude' 		=> 'required|numeric',
				'address'			=> 'max:250',
				'photos' 			=> 'nullable',
				'description' 		=> 'nullable'
			], $this->validationMessages);

		$needName = false;

		$validator->after(function($validator) use ($request, $user, &$needName) {
			// check if user name is still using phone number
			if(preg_match(RegexHelper::phoneNumberFormat(), $user->name) == 1 && $request->user_name == '') {
				$validator->errors()->add('user_name', 'Silakan masukkan nama Anda terlebih dahulu');
				$needName = true;
			}
		});
		

		if($validator->fails()) {
			$errorMessages = $validator->errors()->all();

			return Api::responseData([
				'status'=>false,
				'need_name'=> $needName,
				'meta' => [
					'messages' => $errorMessages,
					'message' => implode('. ', $errorMessages)
				]
			]);
		}

		$params = [
			'title' => $request->title,
			'price' => $request->price,
			'price_unit' => $request->price_unit,
			'user_name' => $request->user_name,
			'contact_phone' => $request->contact_phone,
			'latitude' => $request->latitude,
			'longitude' => $request->longitude,
			'address' => $request->address,
			'photos' => $request->photos,
			'description' => $request->description,
			'type' => $request->product_type,
			'condition' => $request->product_condition
		];

		$newProduct = $this->repository->storeNewProduct($user, $params);

		return Api::responseData(['status'=>true]);
	}

	public function update(Request $request, $id)
	{
		$user = $this->user();

		$marketplaceProduct = $this->repository
								->with(['styles', 'styles.photo'])->find($id);

		if(!$marketplaceProduct) {
			return Api::responseData([
				'status' => false,
				'meta' => [
					'messages' => ['Produk tidak ditemukan'],
					'message' => 'Produk tidak ditemukan'
				]
			]);
		}

		if($marketplaceProduct->user_id != $user->id) {
			return Api::responseData([
				'status' => false,
				'meta' => [
					'messages' => ['Anda bukan pemilik dari produk ini'],
					'message' => 'Anda bukan pemilik dari produk ini'
				]
			]);
		}

		$user = $this->user();

		$validator = Validator::make($request->all(),
			[
				'title' 			=> 'required|max:190',
				'price' 			=> 'required|numeric',
				'price_unit' 		=> 'nullable',
				'product_condition'	=> 'nullable|in:'.implode(",", MarketplaceProduct::CODITION_PRODUCT),
				'product_type'		=> 'required|in:'.implode(",", MarketplaceProduct::TYPE_PRODUCT),
				'user_name' 		=> 'nullable',
				'contact_phone' 	=> 'required|regex:/^(08[1-9]\d{7,10})$/',
				'latitude' 			=> 'required|numeric',
				'longitude' 		=> 'required|numeric',
				'address'			=> 'max:250',
				'photos' 			=> 'nullable',
				'description' 		=> 'nullable'
			], $this->validationMessages);

		if($validator->fails()) {
			$errorMessages = $validator->errors()->all();

			return Api::responseData([
				'status'=>false,
				'meta' => [
					'messages' => $errorMessages,
					'message' => implode('. ', $errorMessages)
				]
			]);
		}

		$params = [
			'title' => $request->title,
			'price' => $request->price,
			'price_unit' => $request->price_unit,
			'contact_phone' => $request->contact_phone,
			'latitude' => $request->latitude,
			'longitude' => $request->longitude,
			'address' => $request->address,
			'photos' => $request->photos,
			'description' => $request->description,
			'product_condition' => $request->product_condition,
			'product_type' => $request->product_type
		];

		$this->repository->updateProduct($marketplaceProduct, $params);

		return Api::responseData(['status'=>true, "message" => "Success update"]);
	}

	public function activate(Request $request, $id)
	{
		$user = $this->user();

		$marketplaceProduct = $this->repository->find($id);

		if(!$marketplaceProduct) {
			return Api::responseData([
				'status' => false,
				'meta' => [
					'messages' => ['Produk tidak ditemukan'],
					'mesage' => 'Produk tidak ditemukan'
				]
			]);
		}

		if($marketplaceProduct->user_id != $user->id) {
			return Api::responseData([
				'status' => false,
				'meta' => [
					'messages' => ['Anda bukan pemilik dari produk ini'],
					'message' => 'Anda bukan pemilik dari produk ini'
				]
			]);
		}

		$marketplaceProduct = $this->repository->makeActive($marketplaceProduct);

		return Api::responseData([
			'status' => true
		]);
	}

	public function deactivate(Request $request, $id)
	{
		$user = $this->user();

		$marketplaceProduct = $this->repository->find($id);

		if(!$marketplaceProduct) {
			return Api::responseData([
				'status' => false,
				'meta' => [
					'messages' => ['Produk tidak ditemukan'],
					'mesage' => 'Produk tidak ditemukan'
				]
			]);
		}

		if($marketplaceProduct->user_id != $user->id) {
			return Api::responseData([
				'status' => false,
				'meta' => [
					'messages' => ['Anda bukan pemilik dari produk ini'],
					'message' => 'Anda bukan pemilik dari produk ini'
				]
			]);
		}

		$marketplaceProduct = $this->repository->makeDeactive($marketplaceProduct);

		return Api::responseData([
			'status' => true
		]);
	}

	public function getDataForUpdate(Request $request, $id)
	{
		$user = $this->user();

		$marketplaceProduct = $this->repository
								->with(['user', 'styles', 'styles.photo'])->find($id);

		if(!$marketplaceProduct) {
			return Api::responseData([
				'status' => false,
				'meta' => [
					'messages' => ['Produk tidak ditemukan'],
					'message' => 'Produk tidak ditemukan'
				]
			]);
		}

		if($marketplaceProduct->user_id != $user->id) {
			return Api::responseData([
				'status' => false,
				'meta' => [
					'messages' => ['Anda bukan pemilik dari produk ini'],
					'message' => 'Anda bukan pemilik dari produk ini'
				]
			]);
		}

		$productData = (new \App\Presenters\MarketplaceProductPresenter('update'))->present($marketplaceProduct)['data'];

		

		return Api::responseData([
        	'data' => $productData
        ]);
	}

	public function markSoldOut(Request $request, $id, $state)
	{
		$user = $this->user();

		if (!$user) {
			return Api::responseData([
				'status' => false,
				'meta' => [
					'message' => 'Silakan login terlebih dahulu'
				]
			]);
		}

		$marketplaceProduct = $this->repository->find($id);

		if(!$marketplaceProduct) {
			return Api::responseData([
				'status' => false,
				'meta' => [
					'messages' => ['Produk tidak ditemukan'],
					'message' => 'Produk tidak ditemukan'
				]
			]);
		}

		if($marketplaceProduct->user_id != $user->id) {
			return Api::responseData([
				'status' => false,
				'meta' => [
					'messages' => ['Anda bukan pemilik dari produk ini'],
					'message' => 'Anda bukan pemilik dari produk ini'
				]
			]);
		}

		$this->repository->markSoldOut($marketplaceProduct, $state);

        return Api::responseData(['status'=>true]);
	}
}