<?php

namespace App\Http\Controllers\Api\Marketplace;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseController;
use App\Http\Helpers\ApiResponse as Api;
use Validator;
use Auth;

use App\Repositories\Marketplace\MarketplaceRepository;
use App\Entities\Marketplace\MarketplaceCluster;


/**
* 
*/
class MarketplaceOwnerController extends BaseController
{
    protected $repository;

    function __construct(MarketplaceRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getOwnerList(Request $request)
    {
        $user = $this->user();

        if(!$user) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Silakan login terlebih dulu'
                ]
            ]);
        }

        $filters = $request->filters;

        $filters['sorting']     = $request->filled('sorting') ? $request->get('sorting') : ['field'=> '-', 'direction' => 'asc'];

        $filters['limit']       = $request->filled('limit') ? $request->input('limit') : 20;
        $filters['offset']      = $request->filled('offset') ? $request->input('offset') : 0;

        $this->repository->setPresenter(new \App\Presenters\MarketplaceProductPresenter('list'));

        $marketplaceProducts = $this->repository->getListByowner($user, $filters);

        return Api::responseData($marketplaceProducts);
    }
}