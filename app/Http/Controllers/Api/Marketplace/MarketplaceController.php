<?php

namespace App\Http\Controllers\Api\Marketplace;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseController;
use App\Http\Helpers\ApiResponse as Api;
use Validator;
use Auth;

use App\Repositories\Marketplace\MarketplaceRepository;
use App\Entities\Marketplace\MarketplaceCluster;
use App\Entities\Marketplace\MarketplaceProduct;
use App\Entities\Marketplace\MarketplacePhoneClick;
use App\Entities\Marketplace\MarketplaceSearchHistoryTemp;
use App\Entities\Activity\Question;
use App\Entities\Activity\Tracking;
use App\Entities\Activity\MarketplaceChat;
use App\Entities\Activity\ChatAdmin;
use Jenssegers\Agent\Agent;
use Session;

/**
* 
*/
class MarketplaceController extends BaseController
{
    protected $repository;

    function __construct(MarketplaceRepository $repository)
    {
        $this->repository = $repository;
    }

    public function listSearch(Request $request)
    {
        $filters = $request->filters;

        $filters['location']    = $request->input('location');
        $filters['sorting']     = $request->input('sorting');

        $filters['limit']       = $request->filled('limit') ? $request->input('limit') : 20;
        $filters['offset']      = $request->filled('offset') ? $request->input('offset') : 0;

        $this->repository->setPresenter(new \App\Presenters\MarketplaceProductPresenter('list'));

        $marketplaceProducts = $this->repository->getList($filters);

        (new MarketplaceSearchHistoryTemp)->storeHistoryFilter($filters, count($marketplaceProducts['data']) == 0, $this->user(), $this->device(), Session::getId());

        return Api::responseData($marketplaceProducts);
    }

    public function clusterSearch(Request $request)
    {
        $filters = $request->input('filters');

        $filters['location']    = $request->input('location');

        $response = $this->repository->getCluster($filters);

        (new MarketplaceSearchHistoryTemp)->storeHistoryFilter($filters, count($response['products']) == 0, $this->user(), $this->device(), Session::getId());

        return Api::responseData($response);
    }

    /**
     * Detail cluster Project
     * The flow will follow detail cluster search
     */
    public function detailCluster(Request $request)
    {
        $location = $request->input('location');
        $point = $request->input('point');

        $location = MarketplaceCluster::getRangeSingleCluster($location, $point, 7);

        $sort = 'asc';
        if ($request->filled('sorting')) {
            $sorting =  $request->input('sorting');
            if (isset($sorting['direction']) AND in_array($sorting['direction'], ['asc', 'desc', 'rand'])) $sort = $sorting['direction']; 
        }

        $request->merge(array("sorting"  => array('field'=>'price','direction'=> $sort)));
        $request->merge(array("location" => $location));

        return $this->listSearch($request);
    }

    /**
     * Detail Product
     *
     */
    public function show(Request $request, $id)
    {

        $marketplaceProduct = $this->repository->with(['styles', 'styles.photo', 'user'])->findByField('id', $id)->first();

        if(!$marketplaceProduct) {
            return Api::responseData([
                'status'=>false,
                'meta' => [
                    'message' => 'Produk tidak ditemukan'
                ]
            ]);
        }

        if($marketplaceProduct->is_active == 0) {
            return Api::responseData([
                'status'=>false,
                'meta' => [
                    'message' => 'Produk sudah tidak aktif'
                ]
            ]);
        }

        $product = (new \App\Presenters\MarketplaceProductPresenter)->present($marketplaceProduct)['data'];

        return Api::responseData([
            'product' => $product
        ]);
    }

    /**
     * Get Chat Question for Marketplace Chat
     */
    public function chatQuestions()
    {
        $response = (new Question)->getQuestion('marketplace');

        return Api::responseData([
            'success' => true,
            'question' => $response,
        ]);
    }

    /**
     * Log user chat for marketplace
     *
     * @param int   $id
     */
    public function postCall(Request $request, $id)
    {
        $device = $this->device();
        if (is_null($device) === false && 
            $device->getIsSendBirdSupported() === false)
        {
            $message = $device->platform == 'android' 
                ? 'Harap update aplikasi Anda. Fitur ini dapat digunakan pada versi 3.2.0'
                : 'Harap update aplikasi Anda. Fitur ini dapat digunakan pada versi 3.1.0';
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => $message,
                ],
            ]);  
        }
        
        $user = $this->user();
        $userStatus = $this->checkCallingUser($user);
        if (!is_null($userStatus)) {
            return $userStatus;
        }
        
        $agent = new Agent();
        $add_from = Tracking::checkDevice($agent);

        $callData = $request->all('name', 'note');
        $callData['add_from'] = $add_from;

        $call = $this->repository->addCall($id, $callData, $user, $this->device());
        if (!$call) {
            return Api::responseData([
                'status'=>false,
                'meta'=>[
                    'message'=>'Gagal menghubungi penjual'
                ]
            ]);
        }

        return Api::responseData([
            'status' => true,
            'result' => $call ? true : false,
            'admin_id' => $call->chat_admin_id,
            'call_id' => $call->id,
            'admin_list' => ChatAdmin::getAdminIdsForMarketplace()
        ]);
    }

    private function checkCallingUser($user)
    {
        if (is_null($user)) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'User tidak ditemukan',
                ],
            ]);
        }

        // block suspicious user :
        // 1. Gerobak.id (325904) : suspicious for data scraping.
        // 2. Bony Ekalaya (472522, 307528) : suspicious for scaming other kost resident.
        // 3. Dian Ayu Anggraini (352263) : scam owner and other kost resident
        $suspiciousUserIds = [325904, 472522, 307528, 352263];
        if (in_array($user->id, $suspiciousUserIds)) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Pesan gagal dikirim',
                ],
            ]);
        }

        return null;
    }

    /**
     * Dummy API to handle auto reply
     * This currently do nothing, but it's created to make the flow stay the same as room chat
     */
    public function postReply(Request $request, $id)
    {
        $product = MarketplaceProduct::find($id);
        if (!$product) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Produk tidak ditemukan'
                ]
            ]);
        }

        $channel = (!empty($request->get('group_channel_url')) ? $request->get('group_channel_url') : $request->get('group_id'));
        $replyData = [
            'call_id'      => $request->get('call_id'),
            'group_id'     => $channel,
            'note'         => $request->get('note'),
            'admin_id'     => $request->get('admin_id'),
            'id_question'  => $request->get('id_question')
        ];
        $response = (new MarketplaceChat($product, $replyData))->reply();

        return Api::responseData([
            'status' => true,
            'response' => $response
        ]);
    }

    public function trackPhoneClick(Request $request, $productId)
    {
        $product = MarketplaceProduct::find($productId);

        if (!$product) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Produk tidak ditemukan'
                ]
            ]);
        }

        $click = new MarketplacePhoneClick;
        $click->marketplace_product_id = $product->id;
        $click->user_id = $this->user()->id;
        $click->save();

        return Api::responseData([
            'status' => true
        ]);
    }
}