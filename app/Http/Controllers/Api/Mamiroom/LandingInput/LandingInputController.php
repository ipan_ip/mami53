<?php

namespace App\Http\Controllers\Api\Mamiroom\LandingInput;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Entities\Mamiroom\LandingInput\LandingInput;
use App\Http\Helpers\ApiResponse;
use App\Entities\Mamiroom\LandingInput\LandingInputPhoto;
use DB;

class LandingInputController extends Controller
{
    protected $validatorMessage = [
        "name.required" => "Nama kos tidak boleh kosong",
        "name.min" => "Nama kos minimal :min karakter",
        "name.max" => "Nama kos maksimal :max karakter",
        "address.required" => "Alamat tidak boleh kosong",
        "address.max" => "Alamat maksimal :max karakter",
        "room_total.required" => "Total kamar tidak boleh kosong",
        "room_available.required" => "Total kamar tersedia tidak boleh kosong",
        "price_monthly.required" => "Harga bulanan tidak boleh kosong",
        "room_condition.required" => "Kondisi kamar tidak boleh kosong",
        "is_register.required" => "Pilih status akun anda sudah terdaftar atau belum",
        "owner_name.required" => "Nama pemilik tidak boleh kosong",
        "owner_phone.required" => "No pemilik tidak boleh kosong",
        "photo.required" => "Photo tidak boleh kosong",
        "photo.mimes" => "Photo harus .png, .jpg atau .jpeg",
    ];

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "name" => "required|min:5|max:220",
            "address" => "required|max:220",
            "room_total" => "required|numeric",
            "room_available" => "required|numeric",
            "price_monthly" => "required|numeric",
            "room_condition" => "required",
            "is_register" => "required|numeric",
            "owner_name" => "required",
            "owner_phone" => "required",
            "facility" => "required",
            "location_detail" => "nullable",
        ], $this->validatorMessage);

        $validator->after(function($validator) use ($request)
        {
            if (count($request->input('facility')) < 1) {
                $validator->errors()->add('facility', 'Pilih minimal satu fasilitas');
            }
        });

        if ($validator->fails()) {
			$response = [
				'status' => false,
                'messages' => $validator->errors()->all()
			];
			return ApiResponse::responseData($response);
        }

        DB::beginTransaction();
        $storeToLandingInput = LandingInput::store($request);
        DB::commit();
        if ($storeToLandingInput) $status = true;
        else $status = false;

        return ApiResponse::responseData(["status" => $status, "message" => ["Berhasil mengirim data kos."]]);
    }

    public function uploadPhoto(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "photo" => "required|max:50000|mimes:jpg,jpeg,png"
        ], $this->validatorMessage);

        if ($validator->fails()) {
			$response = [
				'status' => false,
                'messages' => $validator->errors()->all()
			];
			return ApiResponse::responseData($response);
        }

        DB::beginTransaction();
        $storePhoto = LandingInputPhoto::upload($request->file('photo'));
        DB::commit();

        if ($storePhoto) $status = true;
        else $status = false;

        return ApiResponse::responseData([
                                "status" => $status, 
                                "message" => ["Berhasil upload photo."],
                                "photo_id" => $storePhoto->id
                ]);
    }
}
