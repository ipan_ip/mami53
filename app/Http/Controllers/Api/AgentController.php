<?php
namespace App\Http\Controllers\Api;

use App\Http\Helpers\ApiResponse as Api;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Repositories\RoomRepository;
use App\Entities\Agent\AgentRegistration;
use App\Entities\Room\Element\InputTracker;


class AgentController extends BaseController
{
    protected $repository;

    public function register(Request $request, RoomRepository $repository)
    {
        $this->repository = $repository;
        $user = $this->user();

        $filters = [
            'price_range'=>[0, 10000000],
            'gender'=>[0,1,2],
            'rent_type'=>2
        ];

        $alarmDetail = [
            'filter'=>$filters,
            'location'=>$request->input('location'),
            'phone'=>$request->input('phone'),
            'email'=>$request->input('email')
        ];

        // $alarm = $this->repository->saveFilterToAlarm($user, null, $alarmDetail);

        $agentRegistration = new AgentRegistration;
        $agentRegistration->user_id = $user->id;
        $agentRegistration->name = $request->input('name');
        $agentRegistration->age = $request->input('age');
        $agentRegistration->address = $request->input('address');
        $agentRegistration->education = $request->input('education');
        $agentRegistration->phone = $request->input('phone');
        $agentRegistration->email = $request->input('email');
        $agentRegistration->work_experience = $request->input('work_experience');
        $agentRegistration->area_interest = $request->input('area_interest');
        $agentRegistration->last_salary = $request->input('last_salary');
        $agentRegistration->bank_name = $request->input('bank_name');
        $agentRegistration->bank_account = $request->input('bank_account');
        $agentRegistration->photo_indoor = $request->input('photo_indoor');
        $agentRegistration->photo_outdoor = $request->input('photo_outdoor');
        $agentRegistration->save();

        if($request->filled('input_source') && !is_null($request->input_source) && $request->input_source != '') {
            $inputTracker = new InputTracker;
            $inputTracker->user_id = $user->id;
            $inputTracker->designer_id = null;
            $inputTracker->input_source = $request->input_source;
            $inputTracker->save();
        }

        return Api::responseData([ 'result' => $agentRegistration ]);
    }

}
