<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entities\Room\Survey;
use App\Entities\Room\Room;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Helpers\ApiResponse as Api;
use Validator;
use App\Repositories\SurveyRepository;
use App\User;
use Bugsnag;
use App\Entities\Classes\FeatureSurvey;

class SurveyController extends BaseController
{

    protected $repository;
    protected $messageValidator = [
        "type.required" => "Tipe tidak sesuai",
        "content" => "Konten harus anda pilih",
        "content.max" => "Konten mak :max karakter"
    ];

    public function __construct(SurveyRepository $repository)
    {
        $this->repository = $repository;
        parent::__construct();
    }
    
    public function postSurvey(Request $request)
    {
    	$surveyData = $request->input();
        
        if ($this->user() == null)  {
            // $survey = array("survey" => false, "message_survey" => "Gagal mengirim survey."); 
            $survey = [
                'survey'=> false, 
                'status'=> false,
                'message'=>'Gagal mengirim survey',
                'meta'=>[
                    'message'=>'Gagal mengirim survey'
                ]
            ];
        } else {
            try {
                $survey = $this->repository->addSurvey($this->user(), $surveyData);
            } catch(\Exception $e) {
                Bugsnag::notifyException($e);

                $survey = [
                    'survey'=> false, 
                    'status'=> false,
                    'message'=>'Gagal mengirim survey, mohon periksa kembali inputan Anda, dan coba lagi',
                    'meta'=>[
                        'message'=>'Gagal mengirim survey, mohon periksa kembali inputan Anda, dan coba lagi'
                    ]
                ];
            }
            
        }

        return Api::responseData($survey);
    }

    public function getSurvey(Request $request, $songId)
    {
        $user = $this->user();
        
        if(is_null($user)) {
            return Api::responseData([
                'status'=>false,
                'meta'=>[
                    'message'=>'Session Anda di halaman ini telah berakhir, silakan muat ulang halaman terlebih dulu'
                ]
            ]);
        }

        $survey = $this->repository->getSurvey($this->user()->id, $songId);

        return Api::responseData($survey);
    }


    public function listSurvey(Request $request)
    {
        $user = $this->user();

        if ($user == null) return Api::responseData(array("survey" => array()));

        $survey = $this->repository->getSurveyList($user->id);

        return Api::responseData($survey);        
    }

    public function updateSurvey(Request $request)
    {
        $editTimeSurvey = $this->repository->updateTimeSurvey($request->input());
        
        return Api::responseData($editTimeSurvey);
    }

    public function getOwnerSurvey(Request $request, $songId)
    {
        if ($this->user() == null) {
            return Api::responseData([
                'survey'        => array(),
                'clickable'     => 'Untuk melihat nomor hp anda harus premium',
                'feature'       => 'list histori survey dan chat dengan user , silakan upgrade akun anda menjadi premium.',
                'count'         => 0,
                'page_total'    => 0,
                'email'         => false
            ]);
        }

        $listSurveyOwner = $this->repository->getListSurveyOwner($songId, $this->user());

        return Api::responseData([
            'survey'        => $listSurveyOwner['data'],
            'clickable'     => $listSurveyOwner['clickable'],
            'feature'       => $listSurveyOwner['feature'],
            'count'         => $listSurveyOwner['count'],
            'page_total'    => $listSurveyOwner['page_total'],
            'email'         => $listSurveyOwner['email']
         ]);
    }

    public function editSurveyOwner(Request $request, $id)
    {
        $editData = $this->repository->getEditSurvey($id);

        return Api::responseData(['survey' => $editData]);
    }

    public function updateSurveyOwner(Request $request)
    {
        $surveyData = $request->input();

        $update = $this->repository->updateSurveyOwner($surveyData);
        
        return Api::responseData(['survey' => $update]);
    }

    public function userSurvey(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "type" => "required|in:room_list",
            "content" => "required|max:20"
        ], $this->messageValidator);

        if ($validator->fails()) {
            return Api::responseData(["status" => false, 
                "message" => "Gagal mengirim survey"]);
        }

        (new FeatureSurvey($request->all(), $this->user()))->send();
        return Api::responseData(["status" => true, "message" => "Berhasil mengirim survey"]);

    }

}
