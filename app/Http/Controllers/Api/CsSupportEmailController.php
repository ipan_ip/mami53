<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Helpers\ApiHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class CsSupportEmailController extends Controller
{


    public function sendEmail(Request $request)
    {
        $validation = Validator::make(
            $request->all(),
            [
                'email' => 'required|email',
                'name' => 'required|string',
                'phone_number' => 'required|numeric',
                'category' => 'required|string',
                'kost_name' => 'string',
                'kost_location' => 'string',
                'detail_problem' => 'required|string',
                'attachment' => 'nullable|file'
            ]
        );

        if ($validation->fails()) {
            return ApiHelper::responseData(['errors'=>$validation->errors()], false, true, 400);
        }

        $emailData = $request->only(
            ['email', 'name', 'phone_number', 'category', 'kost_name', 'kost_location', 'detail_problem']
        );

        $emailData['attachment'] = $request->file('attachment') ?? null;
        $emailData['email_sender'] = Config::get('cs-support.email_sender');

        Mail::to(Config::get('cs-support.email'))->send(new \App\Mail\CsSupport($emailData));

        return ApiHelper::responseData(['message' => 'Email successfully send']);
    }
}
