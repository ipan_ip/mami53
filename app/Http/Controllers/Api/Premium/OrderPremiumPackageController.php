<?php

namespace App\Http\Controllers\Api\Premium;

use App\Entities\Mamipay\MamipayInvoices;
use App\Entities\Premium\BalanceRequest;
use App\Entities\Premium\PremiumPackage;
use App\Entities\Premium\PremiumRequest;
use App\Entities\Premium\AccountConfirmation;
use App\Events\Premium\BalanceOrderCanceled;
use App\Events\Premium\PackageOrderCanceled;
use App\Http\Controllers\Api\BaseController;
use App\Http\Helpers\ApiResponse;
use App\Http\Requests\Premium\PremiumPurchaseRequest;
use App\Repositories\Premium\PremiumPackageRepository;
use App\Services\Premium\PremiumRequestService;
use Exception;

class OrderPremiumPackageController extends BaseController
{

    private $orderService, $packageRepo;

    public function __construct(
        PremiumRequestService $orderService,
        PremiumPackageRepository $packageRepo
    ){
        $this->orderService = $orderService;
        $this->packageRepo = $packageRepo;
    }

    public function buyPremiumPackage(PremiumPurchaseRequest $request)
    {
        $validator = $request->failedValidator;
        if (!is_null($validator) && $validator->fails()) {
            return ApiResponse::responseData([
                'status' => false,
                'meta' => [
                    'code' => 404,
                    'message' => implode(',', $validator->errors()->all())
                ]
            ]);
        }

        $user = $this->user();
        if (!$user->isOwner()) {
            return ApiResponse::responseData([
                'status' => false,
                'meta' => [
                    'code' => 401,
                    'message' => 'Akun bukan pemilik kos'
                ]
            ]);
        }

        $package = $this->packageRepo->getPackageById($request->input('package_id'));

        if (is_null($package)) {
            return ApiResponse::responseData([
                'status' => false,
                'meta' => [
                    'code' => 404,
                    'message' => 'Paket premium tidak ditemukan'
                ]
            ]);
        }

        $orderParam = [
            'package_id' => $package->id
        ];

        $expiredTime = date('Y-m-d H:i:s', strtotime('+2 hour'));
        $invoiceParam = [
            'code_product' => MamipayInvoices::PREM_CODE_PRODUCT,
            'order_type' => '',
            'order_id' => '',
            'amount' => $package->totalPrice(),
            'expiry_time' => $expiredTime
        ];

        $orderStatus = false;
        $orderStatusMessage = '';
        $premiumRequestId = 0;
        $balanceRequestId = 0;

        if ($package->for == PremiumPackage::BALANCE_TYPE) {
            
            // send event to mamipay to expired the invoice
            event(new BalanceOrderCanceled($user));

            $orderDetail = $this->orderService->premiumBalanceRequest($orderParam, $user);


            $invoiceParam['order_type'] = BalanceRequest::ORDER_TYPE;
            $invoiceParam['order_id'] = $orderDetail['balance_id'];
            $balanceRequestId = $orderDetail['balance_id'];
            $orderStatus = $orderDetail['action'];
            $orderStatusMessage = (empty($orderDetail['message']) ? "Gagal melakukan pembelian Saldo Iklan" : $orderDetail['message']);
        } else {
            // send event to mamipay to expired the invoice
            event(new PackageOrderCanceled($user));
            
            // delete unconfirmed package request
            PremiumRequest::deleteFalseExpired($user);

            $orderDetail = $this->orderService->premiumPackageRequest($orderParam, $user);

            $invoiceParam['order_type'] = PremiumRequest::ORDER_TYPE;
            $invoiceParam['order_id'] = $orderDetail['active_package'];
            $premiumRequestId = $orderDetail['active_package'];
            $orderStatus = $orderDetail['status'];
            $orderStatusMessage = (empty($orderDetail['message']) ? "Gagal melakukan pembelian Paket Premium" : $orderDetail['message']);
        }

        if (!$orderStatus) {
            return ApiResponse::responseData([
                'status' => false,
                'meta' => [
                    'code' => 500,
                    'message' => $orderStatusMessage
                ]
            ]);
        }

        try {
            $mamipayResponse = MamipayInvoices::requestCreateInvoice($invoiceParam, $user);

            AccountConfirmation::store([
                'user_id'       => $user->id,
                'bank'          => '',
                'name'          => 'mamipay',
                'total'         => $package->totalPrice(),
                'transfer_date' => date('Y-m-d'),
                'premium_request_id' => $premiumRequestId,
                'view_balance_request_id' => $balanceRequestId
            ], 'mamipay', $package->for);

            $response = [
                'detail_package' => $this->buildPackageDetail($package),
                'expired_date' => $expiredTime,
                'invoice_number' => $mamipayResponse->data->invoice_number,
                'invoice_url' => \Config::get('api.mamipay.invoice') . 'select-payment/' . $mamipayResponse->data->id
            ];
    
            return ApiResponse::responseData($response);
        } catch (Exception $e) {
            if ($premiumRequestId > 0) {
                PremiumRequest::find($premiumRequestId)->delete();
            }

            if ($balanceRequestId > 0) {
                BalanceRequest::find($balanceRequestId)->delete();
            }

            return ApiResponse::responseData([
                'status' => false,
                'meta' => [
                    'code' => 500,
                    'message' => $e->getMessage()
                ]
            ]);
        }

    }

    private function buildPackageDetail(PremiumPackage $package)
    {
        return [
            "id"   => $package->id,
            "type"  => $package->for,
            "name"  => $package->compiledName(),
            "bonus" => $package->bonus,
            "days_count" => $package->total_day,
            "is_recommendation" => $package->is_recommendation != 0,
            "active_count" => $package->activeCountString(),
            "views" => $package->view
        ];   
    }

}