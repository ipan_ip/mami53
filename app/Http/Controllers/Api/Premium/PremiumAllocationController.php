<?php
namespace App\Http\Controllers\Api\Premium;

use DB;
use App\Http\Helpers\ApiResponse as Api;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseController;
use App\Repositories\Premium\PremiumAllocationRepository;
use App\Entities\Room\RoomOwner;
use App\Entities\Premium\ClickPricing;
use App\Repositories\Premium\ClickPricingRepository;
use App\Repositories\PremiumRepositoryEloquent;
use App\Entities\Room\Room;
use App\Http\Requests\Premium\PremiumAllocationRequest;
use Illuminate\Http\JsonResponse;

class PremiumAllocationController extends BaseController
{

    protected $repository;
    public function __construct(PremiumAllocationRepository $repository)
    {
        $this->repository = $repository;
        parent::__construct();
    }
    
    /**
     * Balance allocation process
     *
     * @param  PremiumAllocationRequest $request
     * @param  int $id
     * 
     * @return JsonResponse
     */
    public function allocation(PremiumAllocationRequest $request, $id): JsonResponse
    {
        $validator = $request->failedValidator;
        if (!is_null($validator) && $validator->fails()) {
            return Api::responseData([
                'status' => false,
                'message' => implode(",", $validator->errors()->all()),
            ]);
        }

        $user = $this->user();
        if ($user->is_owner === 'false') {
            return Api::responseData([
                "status" => false,
                "message" => "Gagal alokasi saldo premium"
            ]);
        }

        $roomOwner = RoomOwner::with('room')
                            ->where('user_id', $user->id)
                            ->where('status', 'verified')
                            ->whereHas('room', function($q) use($id) {
                                $q->where('song_id', $id);
                            })
                            ->first();
        
        if (is_null($roomOwner)) {
            return Api::responseData([
                "status" => false,
                "message" => "Anda bukan pemilik kos ini"
            ]);
        }

        $response = $this->repository->premiumAllocation($request->all(), $roomOwner);

        return Api::responseData($response);
    }

    public function deactivateRoomAllocation(Request $request, $id)
    {
        DB::beginTransaction();
        $user = $this->user();
        if (is_null($user) || $user->is_owner === 'false') {
            return Api::responseData([
                "status" => false,
                "message" => "Akun tidak ditemukan"
            ]);
        }
        
        $room = Room::Active()
                ->with('view_promote')
                ->where('song_id', $id)
                ->first();

        if (
            is_null($room) ||
            count($room->view_promote) === 0
        ) {
            return Api::responseData([
                "status" => false,
                "message" => "Gagal menonaktifkan iklan premium"
            ]);
        }

        $this->repository->deactivatePremiumPromote($room->view_promote()->first());
        DB::commit();

        return Api::responseData([
            "message" => "Berhasil menonaktifkan iklan premium"
        ]);
    }

    public function settingStatus(Request $request)
    {
        DB::beginTransaction();
        $user = $this->user();
        if (is_null($user) || $user->is_owner === 'false') {
            return Api::responseData([
                "status" => false,
                "message" => "Akun tidak ditemukan"
            ]);
        }
        
        $premiumRepository = new PremiumRepositoryEloquent(app());
        $response = $premiumRepository->settingDailyAllocationStatus($user);
        
        DB::commit();
        return Api::responseData($response);
    }
}