<?php
namespace App\Http\Controllers\Api\Premium;

use App\Http\Helpers\ApiResponse as Api;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseController;
use App\User;
use App\Services\Premium\AbTestPremiumService;

class AbTestPremiumController extends BaseController
{

    protected $service;
    public function __construct(AbTestPremiumService $service)
    {
        $this->service = $service;
    }

    public function checkOwner(Request $request)
    {
        $user = $this->user();
        if (is_null($user)) {
            return Api::responseData([
                'status' => false,
                'result' => null
            ]);
        }

        if ($user->is_owner == User::IS_NOT_OWNER) {
            return Api::responseData(['result' => null]);
        }
        
        return Api::responseData(['result' => $this->service->getUserData($user)]);
    }
}