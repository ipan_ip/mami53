<?php

namespace App\Http\Controllers\Api\Premium;

use Illuminate\Http\Request;
use App\Http\Helpers\ApiResponse as Api;
use App\Http\Controllers\Api\BaseController;
use App\Repositories\Premium\PremiumPlusInvoiceRepository;

class PremiumPlusInvoiceController extends BaseController
{

    protected $repository;

    public function __construct(PremiumPlusInvoiceRepository $repository)
    {
        $this->repository = $repository;

        parent::__construct();
    }

    public function detailInvoice(Request $request, $invoiceId) 
    {
        $response = $this->repository->getDetail($this->user(), $invoiceId);

        return Api::responseData($response);
    }

}