<?php

namespace App\Http\Controllers\Api;

use App\Http\Helpers\ApiHelper;
use App\Http\Requests\ListReviewsRequest;
use App\Libraries\CacheHelper;
use Illuminate\Http\Request;

use App\Entities\Room\Review;
use App\Entities\Room\Room;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Helpers\ApiResponse as Api;
use App\Repositories\ReviewRepository;
use Validator;
use App\Entities\Room\Element\ResponseMessage;
use App\Enums\Goldplus\Permission\GoldplusPermission;
use App\Http\Helpers\GoldplusPermissionHelper;
use App\Http\Helpers\RatingHelper;
use Cache;

class ReviewController extends BaseController{

    protected $repository;

    protected $validationMessages = [
        'song_id.required'             => 'Kos tidak ditemukan.',
        'id.required'                  => 'Data tidak ditemukan',
        'content.required'             => 'Balasan tidak boleh kosong',
        'clean.required'               => 'Rating kebersihan kosong',
        'happy.required'               => 'Rating kenyamanan kosong',
        'safe.required'                => 'Rating keamanan tidak boleh kosong',
        'pricing.required'             => 'Rating harga tidak boleh kosong',
        'room_facility.required'       => 'Rating fasilitas tidak boleh kosong',
        'public_facility.required'     => 'Rating fasilitas publik tidak boleh kosong',
        'content.required'             => 'Review tidak boleh kosong',
        '*.between'                    => 'Rating harus 1 - 5',
        '*.integer'                    => 'Rating harus angka',
        'anonim.required'              => 'Masih ada yang kosong'
    ];

    public function __construct(ReviewRepository $repository)
    {
        $this->repository = $repository;

        parent::__construct();
    }

    public function postReview(Request $request)
    {
        $scale = 4;
        if ($request->filled('scale')) {
            $scale = RatingHelper::isScaleAllowed($request->input('scale')) ? $request->input('scale') : 4;
        }

        $room = Room::where('song_id', $request->input('id'))->first();
        if (is_null($room)) {
            return Api::responseData(['review' => null]);
        }

        $data           = $request->all();
        $data['scale']  = $scale;
        $review         = $this->repository->addReviewUser($this->user(), $data, $room->id);

        return Api::responseData([
            'review' => $review
        ]);
    }

    public function addNewReview(Request $request)
    {
        $validator = Validator::make(
            $request->all(), 
            [
                'id'                   => 'required',
                'clean'                => 'required|integer|between:1,5',
                'happy'                => 'required|integer|between:1,5',
                'safe'                 => 'required|integer|between:1,5',
                'pricing'              => 'required|integer|between:1,5',
                'room_facilities'      => 'required|integer|between:1,5',
                'public_facilities'    => 'required|integer|between:1,5',
                'content'              => 'required|nullable',
                'anonim'               => 'required|integer|between:0,1'
            ], 
            $this->validationMessages
        );

        $user = $this->user();
        $room = Room::where('song_id', $request->input('id'))->first();
        
        $validator->after(function ($validator) use ($room, $user) {
            if (is_null($user) || is_null($room)) {
                $validator->errors()->add('field', 'Login dulu untuk menambah review');
            }
        });

        if($validator->fails()) {
            return Api::responseData([
                'status'=> false,
                'meta'  => array("message" => Api::validationErrorsToString($validator->errors()))
            ]);
        }

        $data = $request->all();
        $scale = RatingHelper::isScaleAllowed($request->input('scale')) ? $request->input('scale') : 4;
        $data['scale'] = $scale;

        $response = $this->repository->addNewReview($data, $user, $room);
        return Api::responseData($response);
    }

    public function replyReview(Request $request)
    {
        if ($this->user() == null) return Api::responseData(array("reply" => false));

        $review = $this->repository->replyReview($this->user(), $request->all());

        return Api::responseData(array("reply" => $review));
    }

    public function getOwnerReview(Request $request, $songId)
    {
        $user = $this->user();
        if (is_null($user)) {
            return Api::responseData(
                [
                    "review" => array(),
                    "count" => 0,
                    "page_total" => 0,
                    "email" => false
                ]
            );
        }

        $userId = $user->id;
        $email = (!filter_var($this->user()->email, FILTER_VALIDATE_EMAIL) === false);

        $room = Room::with(
            [
                'owners' => function ($q) use ($userId) {
                    if ($userId != 0) {
                        $q->where('user_id', $userId);
                    }
                }
            ]
        )
            ->where('song_id', $songId)
            ->first();

        if (is_null($room)) {
            return Api::responseData(
                [
                    "review" => array(),
                    "count" => 0,
                    "page_total" => 0,
                    "email" => $email,
                    "owner" => false
                ]
            );
        }

        $owner = false;
        if (
            !is_null($userId)
            && $room->owners->count() > 0
        ) {
            $owner = true;
        }

        $response = array_merge(
            $this->repository->getReviewOwner($room, $user, $email),
            [
                "owner" => $owner
            ]
        );

        return Api::responseData($response);
    }

    public function updateReview(Request $request)
    {
        $scale = 4;
        if ($request->filled('scale')) {
            $scale = RatingHelper::isScaleAllowed($request->input('scale')) ? $request->input('scale') : 4;
        }

        $room = Room::where('song_id',$request->input('id'))->first();
        if (is_null($room)) {
            return Api::responseData(['review' => null]);
        }

        $data           = $request->all();
        $data['scale']  = $scale;
        $review         = $this->repository->updateReview($this->user(), $data, $room->id);

        return Api::responseData([
            'review' => $review
        ]);
    }

    public function getReview(Request $request, $songId)
    {
        if ($this->user() == null) $userId = null;
        else $userId = $this->user()->id;  

        $room  = Room::with(['avg_review', 'owners' => function($q) use($userId){
           if ($userId != 0) $q->where('user_id', $userId);
        }])->where('song_id', $songId)->first();
        
        $owner = false;

        if ( count($room) == 0) return Api::responseData(array('rating' => 0, 'all_rating' => array(), "owner" => $owner, 'review' => null));
        
        if ($userId != null) $owner = count($room->owners) > 0 ? true : false;
        
        $review = $this->repository->getListReview($room->id, $userId, $owner, $request->all());
        
        $rating     = array("rating" => $room->string_rating);
        $all_rating = array("all_rating" => $room->all_rating);

        $data = array_merge($rating, $all_rating, array("owner" => $owner), $review);

        return Api::responseData($data);
    }

    public function replyThread(Request $request)
    {
          // validate
         $validator = Validator::make($request->all(), 
             [
                 'song_id' => 'required',
                 'id'      => 'required',
                 'content' => 'required'
             ], $this->validationMessages);
         
         $user = null; 
         if (!is_null($user = $this->user())) $user = $this->user();

         $review = Review::with(['room', 'room.owners' => function($p) use($user) {
                            if (!is_null($user)) $p->where('user_id', $user->id);
                         }])->where('id', $request->input('id'))->first();

         $validator->after(function ($validator) use($review, $user) {
              if (is_null($review) OR is_null($user)) {
                $validator->errors()->add('field', 'Something is wrong with this field!');
              } else {
                if ($review->user_id != $user->id AND count($review->room->owners) < 1) { 
                  $validator->errors()->add('field', 'Something is wrong with this field!');
                }
              }
         });

         if($validator->fails()) {
             return Api::responseData([
                     'status'=> false,
                     'meta'  => array("message" => Api::validationErrorsToString($validator->errors()))
                 ]);
         }

         $response = $this->repository->reviewReplyAllUser($request->all(), $user);

         return Api::responseData($response);
    }

    public function listRepltThread(Request $request, $id)
    {
          $user = null;
          if (!is_null($this->user())) $user = $this->user();

          $data = explode(",", base64_decode($id));

          $response = $this->repository->getListThread($data, $user);

          return Api::responseData($response);
    }

    public function listRepltThreadApi(Request $request)
    {
        $user = null;
        if (!is_null($this->user())) $user = $this->user();

        if ($request->filled('token')) $data = explode(",", base64_decode($request->input('token')));
        else $data = array(0);

        $response = $this->repository->getListThread($data, $user);
        return Api::responseData($response);
    }

    public function getList(Request $request, $songId)
    {
          if ($this->user() == null) $userId = null;
          else $userId = $this->user()->id;  

          $room = Room::active()->with(['avg_review', 'owners' => function($q) use($userId){
                      if ($userId != 0) $q->where('user_id', $userId);
                    }])->where('song_id', $songId)->first();

          $ResponseMessage = new ResponseMessage();

          if (is_null($room)) {
            return Api::responseData(["status" => false, "meta" => ["message" => $ResponseMessage::FAILED_TO_FIND_KOS]]);
          }

          if (
              count($room->owners) > 0 &&
              !GoldplusPermissionHelper::isRoomAllow($room, GoldplusPermission::REVIEW_READ)
          ) {
              // Only apply GP Limitting for owner request.
              return Api::responseData([
                  'status' => false,
                  'meta' => [
                      'message' => __('api.access.forbidden'),
                  ]
              ]);
          }
          
          $response = $this->repository->listReview($this->user(), $room, $request->all());

          return Api::responseData($response);

    }

    public function getKosReviews(int $songId, Request $request): \Illuminate\Http\JsonResponse
    {
        $validator = Validator::make(
            $request->all(),
            [
                'sort'                 => 'string',
                'last_id'              => 'integer',
                'limit'                => 'integer',
            ]
        );

        if ($validator->fails()) {
            $message = 'Bad Request';
            return ApiHelper::responseError($message, false, 403);
        }

        $user = $this->user();

        $reviews = $this->repository->getReviewsByKosID($songId, $request, $user);

        if (empty($reviews)) {
            $message = 'Kost Data Not Found';
            return ApiHelper::responseError($message, false, 404);
        }

        return Api::responseData($reviews);
    }
}
