<?php
namespace App\Http\Controllers\Api;

use App\Http\Helpers\ApiResponse;
use Illuminate\Http\Request;
use App\Libraries\MessengerLibrary;

class MessengerController extends BaseController
{
	protected $eventOptions = [
		'submit-kost' => 'submitKost'
	];

	public function event(Request $request, $event)
	{
		if(isset($this->eventOptions[$event])) {
			$function = $this->eventOptions[$event];
			return $this->$function($request);
		} else {
			abort(404);
		}
	}

	public function submitKost(Request $request)
	{
		$psid = $request->input('psid');

		$message = "Kost Anda berhasil disimpan.\n" .
					"Silakan tunggu tim Mamikos untuk memverifikasi kost tersebut.\n" .
					"Jika kost sudah berhasil terverifikasi, maka Anda akan mendapat pulsa Rp. 10.000";

		$messenger = new MessengerLibrary();

		$messenger->sendTextMessage($message, $psid);

		return ApiResponse::responseData([
			'status' => true
		]);
	}
}