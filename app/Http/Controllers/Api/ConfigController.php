<?php

namespace App\Http\Controllers\Api;

use App\Entities\Activity\Question;
use App\Http\Helpers\ApiHelper as Api;
use App\Repositories\Level\KostLevelRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ConfigController extends BaseController
{
    private $levelRepository;

    public function __construct(KostLevelRepository $levelRepository)
    {
        $this->levelRepository = $levelRepository;

        parent::__construct();
    }

    public function general()
    {
        $kostLevelAll      = $this->levelRepository->getAll()->toArray();
        $kostLevelResponse = [
            'list'     => [],
            'campaign' => [
                'internal' => (bool)config('kostlevel.campaign.internal'),
            ],
        ];
        foreach ($kostLevelAll as $level) {
            $levelConfig = [];
            if ($level['id'] == config('kostlevel.id.super')) {
                $levelConfig['type'] = 'super';
            } elseif ($level['id'] == config('kostlevel.id.oke')) {
                $levelConfig['type'] = 'oke';
            }
            if (!empty($levelConfig)) {
                $levelConfig['id']           = $level['id'];
                $levelConfig['name']         = $level['name'];
                $kostLevelResponse['list'][] = $levelConfig;
            }
        }

        $response = [
            'kost-level' => $kostLevelResponse,
        ];
        return Api::responseData($response);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function getChatPretext(Request $request)
    {
        if (!$request->filled('string') || !$request->filled('for')) {
            return Api::responseError('Invalid Request Parameter', 'INVALID_REQUEST', 400);
        }

        $pretext = Question::getQuestionByString($request->all());

        return Api::responseData(
            [
                'chat-pretext' => $pretext
            ]
        );
    }
}
