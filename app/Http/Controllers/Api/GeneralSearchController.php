<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Web\MamikosController;
use App\Http\Helpers\ApiResponse as Api;
use App\Http\Helpers\ApiHelper;
use App\Entities\Search\GeneralSearch;
use App\Entities\Search\InputKeyword;
use App\Entities\Search\InputKeywordSuggestion;
use App\Services\GeneralSearch\GeneralSearchService;
use Bugsnag;
use Exception;
use Validator;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class GeneralSearchController extends MamikosController
{
    /**
     *  Instance of GeneralSearchService
     *
     *  @var GeneralSearchService
     */
    private $service;

    private const BAD_REQUEST_MESSAGE = 'Invalid place name!';

    public function __construct(GeneralSearchService $service)
    {
        $this->service = $service;
    }

	public function saveCriteria(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'keywords'	=> 'required',
			'gender'	=> 'nullable',
			'rent_type' => 'required',
			'price_min'	=> 'required|numeric',
			'price_max'	=> 'required|numeric',
			'latitude'	=> 'nullable|numeric',
			'longitude'	=> 'nullable|numeric',
			'administrative_type' => 'nullable|max:100',
			'place_id' => 'nullable',
			'title' => 'nullable',
			'subtitle' => 'nullable'
		]);

		if($validator->fails()) {
			return Api::responseData([
				'status'=>false
			]);
		}

		$genderOption = array_values(GeneralSearch::GENDER_OPTIONS);

		$gender = isset(array_flip($genderOption)[$request->gender]) ?
					array_flip($genderOption)[$request->gender] : null;
		$rentType = isset(array_flip(GeneralSearch::RENT_TYPE_OPTIONS)[$request->rent_type]) ?
						array_flip(GeneralSearch::RENT_TYPE_OPTIONS)[$request->rent_type] : null;
		$keywords = ApiHelper::removeEmoji($request->keywords);

		// get the existing one
		$existingCriteria = GeneralSearch::where('keywords', substr($keywords, 0, 190))
										->where('gender', $gender)
										->where('rent_type', $rentType)
										->where('price_min', $request->price_min)
										->where('price_max', $request->price_max)
										->first();

		if (!is_null($request->title) && $request->title != '') {
            InputKeywordSuggestion::updateBySuggestionAndArea($request);
		}

		if($existingCriteria) {
			if (is_null($existingCriteria->latitude)) $existingCriteria->latitude = $request->latitude;
			if (is_null($existingCriteria->longitude)) $existingCriteria->longitude = $request->longitude;
			$existingCriteria->save();

			return Api::responseData([
				'slug' => $existingCriteria->slug,
				'slug_array' => $existingCriteria->slug_array
			]);
		}

		$criteria = new GeneralSearch;
		$criteria->keywords = $keywords;
		$criteria->gender = $gender;
		$criteria->rent_type = $rentType;
		$criteria->price_min = $request->price_min;
		$criteria->price_max = $request->price_max;
		$criteria->latitude = $request->latitude;
		$criteria->longitude = $request->longitude;
		$criteria->administrative_type = !is_null($request->administrative_type) ?
			$request->administrative_type : '';
		$criteria->save();

		return Api::responseData([
			'slug' => $criteria->slug,
			'slug_array' => $criteria->slug_array
		]);
	}

	public function saveCriteriaApp(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'keywords' => 'required',
			'place_id' => 'required',
			'latitude' => 'nullable|numeric',
			'longitude' => 'nullable|numeric',
			'administrative_type' => 'nullable|max:100'
		]);

		if ($validator->fails()) {
			return Api::responseData([
				'status' => false
			]);
		}

		$inputKeywordSuggestions = InputKeywordSuggestion::where('suggestion', $request->keywords['title'])
			->where('area', $request->keywords['subtitle'])
			->get();

		if (count($inputKeywordSuggestions) > 0) {

			foreach ($inputKeywordSuggestions as $suggestion) {
				$suggestion->latitude = $request->latitude;
				$suggestion->longitude = $request->longitude;
				$suggestion->place_id = $request->place_id;
				$suggestion->administrative_type = $request->administrative_type;
				$suggestion->save();
			}

		}

		return Api::responseData([
			'status' => true
		]);
	}

	public function show(Request $request)
	{
		$slug = $request->get('slug');

		if(is_null($slug) || $slug == '') {
			return Api::responseData([
				'data' => []
			]);
		}

		$criteria = (new GeneralSearch)->getBySlug($slug);

		if(!$criteria) {
			return Api::responseData([
				'status'=>false
			]);
		}

		if($request->filled('hit')) {
			$criteria->increment('total_view');
		}

		$data = [
			'_id' => $criteria->id,
			'keywords' => $criteria->keywords,
			'gender' => $criteria->gender,
			'rent_type' => $criteria->rent_type,
			'price_min' => $criteria->price_min,
			'price_max' => $criteria->price_max,
			'latitude' => $criteria->latitude,
			'longitude' => $criteria->longitude,
			'zoom' => $criteria->zoom_level
		];

		return Api::responseData([
			'criteria' => $data
		]);
	}

	public function saveUserInput(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'keyword' => 'required',
			'suggestions' => 'nullable'
		]);

		if ($validator->fails()) {
			return Api::responseData([
				'status'=>false
			]);
		}

		$keyword = ApiHelper::removeEmoji($request->keyword);

		$existingKeyword = InputKeyword::where('keyword', trim($keyword))->first();
		
		if ($existingKeyword) {
			$existingKeyword->increment('total');
		} else {

			$inputKeyword = new InputKeyword;
			$inputKeyword->keyword = trim($keyword);
			$inputKeyword->total = 1;
			$inputKeyword->save();
			
			if (is_array($request->suggestions)) {
				foreach ($request->suggestions as $suggestion) {
					$keywordSuggestion = new InputKeywordSuggestion;
					$keywordSuggestion->input_id 	= $inputKeyword->id;
					$keywordSuggestion->suggestion 	= $suggestion['title'];
					$keywordSuggestion->area 		= isset($suggestion['subtitle']) ? $suggestion['subtitle'] : null;
					$keywordSuggestion->latitude 	= isset($suggestion['latitude']) ? $suggestion['latitude'] : null;
					$keywordSuggestion->longitude 	= isset($suggestion['longitude']) ? $suggestion['longitude'] : null;
					$keywordSuggestion->place_id 	= isset($suggestion['place_id']) ? $suggestion['place_id'] : null;	
					$keywordSuggestion->save();
				}
			}
		}

		return Api::responseData([
			'status' => true
		]);
	}

    public function getPlaceGeolocation($string = '')
    {
        $responseData = null;

        try {
            if (empty($string)) {
                return $this->sendErrorResponse(self::BAD_REQUEST_MESSAGE);
            }

            $responseData = $this->service->getGeoLocation($string);

        } catch (Exception $exception) {
            Bugsnag::notifyException($exception);
        }
        return Api::responseData(
            [
                'status' => true,
                'landing' => $responseData
            ]
        );
    }

    /**
     * @param $message
     * @param string $severity
     * @param int $code
     * @return JsonResponse
     */
    private function sendErrorResponse($message, $severity = 'Bad Request', $code = 400)
    {
        return Api::responseError(
            $message,
            $severity,
            $code
        );
    }
}