<?php

namespace App\Http\Controllers\Api;

use App\Http\Helpers\ApiResponse as Api;
use App\Repositories\FinderRepository;
use Illuminate\Http\Request;
use Validator;

class FinderController extends BaseController
{
    protected $validatorMessage = [
        "city.required" => "Kota tidak boleh kosong",
        "gender.required" => "Tipe kost tidak boleh kosong",
        "price_range.required" => "Harga tidak boleh kosong",
        "check_in.required" => "Waktu masuk tidak boleh kosong",
        "check_in.date" => "Format waktu masuk harus YYYY-MM-DD",
        "check_in.after_or_equal" => "Tanggal masuk paling cepat adalah hari ini",
        "check_in.before" => "Tanggal masuk paling lambat adalah 2 bulan kedepan",
        "duration.required" => "Waktu masuk tidak boleh kosong",
        "name.required" => "Nama tidak boleh kosong",
        "email.required" => "Email tidak boleh kosong",
        "email.email" => "Email tidak valid",
        "phone_number.required" => "Nomor hp tidak boleh kosong",
        "phone_number.numeric" => "Nomor hp harus berupa angka"
    ];
    /**
     * @var FinderRepository
     */
    protected $repository;

    /**
     * OwnerTestimonialController constructor.
     *
     * @param \App\Repositories\FinderRepository $repository
     */
    public function __construct(FinderRepository $repository)
    {
        $this->repository = $repository;

        parent::__construct();
    }

    public function storeFinder(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'city' => 'required',
                'gender' => 'required',
                'price_range' => 'required',
                'check_in' => 'required|date|after_or_equal:today|before:2 months',
                'duration' => 'required',
                'name' => 'required',
                'email' => 'required|email',
                'phone_number' => 'required|numeric',
            ],
            $this->validatorMessage
        );

        if ($validator->fails()) {
            $response = [
                "status" => false,
                "meta" => ['message' => $validator->errors()],
            ];

            return Api::responseData($response);
        }

        $facilities = $this->repository->getFacility($request->facility);

        $minPrice = $this->repository->getMinPrice($request->price_range);

        $maxPrice = $this->repository->getMaxPrice($request->price_range);

        $gender = $this->repository->getGender($request->gender);

        $formData = [
            'city' => $request->city,
            'gender' => $gender,
            'min_price' => $minPrice,
            'max_price' => $maxPrice,
            'facility' => $facilities,
            'note' => $request->note,
            'check_in' => $request->check_in,
            'duration' => $request->duration,
            'name' => $request->name,
            'email' => $request->email,
            'phone_number' => $request->phone_number,
        ];

        $storeFinder = $this->repository->storeFinderData($formData);

        if ($storeFinder) {
            return Api::responseData(
                [
                    "status" => true,
                    "meta" => [
                        "message" => "Success Created"
                    ]
                ]
            );
        } else {
            return Api::responseData(
                [
                    "status" => false,
                    "meta" => ['message' => "Failed created"],
                ]
            );
        }
    }

}
