<?php

namespace App\Http\Controllers\Api;

use App\Entities\Room\Room;
use App\Http\Helpers\ApiResponse as Api;
use App\Presenters\RoomPresenter;
use App\Repositories\RoomRepository;
use Illuminate\Http\Request;

class RoomMetaController extends BaseController
{
    /**
     * @var RoomRepository
     */
    protected $repository;

    public function __construct(RoomRepository $repository)
    {
        $this->repository = $repository;

        parent::__construct();
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function getRoomMeta(Request $request)
    {
        $room     = $this->repository->getRoomForChatMeta($request->id);

        if (is_null($room)) {
            return $this->returnError();
        }

        $roomMeta = (new RoomPresenter('meta'))->present($room);

        if (is_null($roomMeta)) {
            return $this->returnError();
        }

        return Api::responseData($roomMeta);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    private function returnError()
    {
        return Api::responseError("Room data could not be found!", "INVALID REQUEST", 400);
    }
}
