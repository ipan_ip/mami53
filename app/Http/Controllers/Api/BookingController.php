<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\BookingRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Helpers\ApiResponse as Api;

class BookingController extends BaseController
{

	protected $repository;

    public function __construct(BookingRepository $repository)
    {
        $this->repository = $repository;

        parent::__construct();
    }

    public function postBooking(Request $request)
    {

      if (count($this->user()) == 0) {
          $userId = NULL;          
      }	else {
          $userId = $this->user()->id;
      }

      return Api::responseData(["booking" => $this->repository->postBooking($userId, $request->all())]);
    }
}
