<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Requests;

class RecommendationControler extends BaseController
{

	protected $recommendationDate = array();
	protected $deviceId = null;
	protected $userId = null;
	protected $userAndDeviceId = array();
	protected $rentType = "2";
	protected $fields = array('_id','status','gender','photo_url','price-title','room-title','address','min_month');

	/**
	 * Display a listing of the resource.
	 * GET /api/tag.php
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->setUserAndDevice();

		$pagination	= ApiHelper::inputPagination(2, 0);
		$offset = $pagination['offset'];
		$count = $this->getCountRecommendation();

		$time1 		= time() - $offset * 60 * 60 * 24;
		$time2 		= time() - ($offset + 1) * 60 * 60 * 24;

		$recommendations = array();

		if ($recommendation = $this->dailyRecommendation($time1, $offset)) {
			$recommendations[] = $recommendation;
		}

		if ($recommendation = $this->dailyRecommendation($time2, $offset + 1)) {
			$recommendations[] = $recommendation;
		}

        return ApiHelper::responseData([
                'recommendations' => $recommendations,
                'count' => $count,
		    ]);
	}

	private function dailyRecommendation($time, $offset)
	{
		$alarm 	= $this->getAlarmRecommendationTable($time, $offset);
		$admin 	= $this->getAdminRecommendation($time, $offset);

		$recommendations = array();

		if (sizeof($alarm)) {
			$recommendations[] = [
				'title'     => 'Rekomendasi Sesuai Filter ('. sizeof($alarm) .' Kost)',
				'rooms'     => $alarm
			];
		}
		if (sizeof($admin)) {
			$recommendations[] = [
				'title'     => 'Rekomendasi Mamikos ('. sizeof($admin) .' Kost)',
				'rooms'     => $admin
			];
		}

		if (! $recommendations) return null;

		return array(
				'id' => date('Ymd', $time),
				'day_title' => date('d/m/Y',$time) .' (' . (sizeof($alarm) + sizeof($admin)) . ' Kost)',
				'recommendations' => $recommendations
			);
	}



	private function getAlarmRecommendationTable($date = null, $offset = 0)
	{
		if (! isset($this->recommendationDate[$offset])) return [];

		$date = $this->recommendationDate[$offset];

		$rowDesignerId = Recommendation::where('date', $date)
								->where(function($query){
									$query->whereDeviceId($this->deviceId)
										->orWhere('user_id',$this->userId);
									})
								->take(5)->get(['designer_id'])->toArray();

		$designerIds = array_filter(array_fetch($rowDesignerId, 'designer_id'));

		$rowDesignerSongId = Designer::whereIn('id', $designerIds)->get(['song_id'])
										->toArray();
		$designerSongIds = array_filter(array_fetch($rowDesignerSongId, 'song_id'));

		$filters = array(
				'ids' => $designerSongIds,
				'rent_type'	=> $this->rentType
			);

		return Designer::getDesigner($this->userAndDeviceId, $filters, null, $this->fields);
	}

	private function getAdminRecommendation($date = null)
	{
		if(! $date) $date = time();

		$pagination = ApiHelper::forcePagination(5,0);

		return Designer::getDesigner($this->userAndDeviceId, ['admin_recommend' => date('Y-m-d', $date)], $pagination , $this->fields);
	}

	public function getCountRecommendation()
	{
		$recommendationDate = Recommendation::select(DB::raw('distinct date'))
										->where(function($query){
											$query->whereDeviceId($this->deviceId)
												->orWhere('user_id',$this->userId);
											})
										->orderBy('date', 'desc')
										->get()->toArray();

		$this->recommendationDate = array_fetch($recommendationDate, 'date');

		$count = sizeof($this->recommendationDate);

		if ($count > 10) return 10;

		return $count;
	}

	private function setUserAndDevice()
	{
		$this->userId     = ApiHelper::activeUserId();
		$this->deviceId   = ApiHelper::activeDeviceId();
		$this->userAndDeviceId = array(
			'userId' => $this->userId,
			'deviceId' => $this->deviceId
			);

		$device = UserDevice::findOrFail($this->deviceId);

		if ($device) {
			$this->rentType = $device->rent_type;
		}
	}
}
