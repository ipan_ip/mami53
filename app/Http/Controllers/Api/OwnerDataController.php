<?php

namespace App\Http\Controllers\Api;

use App\Channel\MoEngage\MoEngageEventDataApi;
use App\Criteria\Room\OwnerCriteria;
use App\Entities\Activity\Tracking;
use App\Entities\Classes\UpdateRoom;
use App\Entities\MoEngage\EmailVerificationEventProps;
use App\Entities\Owner\ActivityType;
use App\Entities\Owner\SurveySatisfaction;
use App\Entities\Room\Room;
use App\Entities\Tracker\NotifClick;
use App\Http\Helpers\ApiResponse as Api;
use App\Http\Helpers\RegexHelper;
use App\Jobs\Owner\MarkActivity;
use App\Jobs\Owner\UpdateKostUpdatedDate;
use App\Presenters\RoomPresenter;
use App\Repositories\OwnerDataRepository;
use App\Repositories\RoomRepositoryEloquent;
use App\Repositories\RoomUpdaterRepositoryEloquent;
use App\User;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Jenssegers\Agent\Agent;
use RuntimeException;
use App\Entities\Room\RoomOwner;

class OwnerDataController extends BaseController
{

    private $repository;

    protected $messageValidator = [
        "room_count.required" => "Jumlah kamar tidak boleh kosong",
        "room_count.numeric"  => "Harus berupa angka",
        "room_id.required"    => "Kos tidak ditemukan",
        "room_id.numeric"     => "Kos harus berupa angka",
        "data_for.required"   => "Gagal melakukan survey",
        "data_for.in"         => "Gagal melakukan survey",
        "reason.required"     => "Belum memilih alasan",
        "premium_id"          => "Gagal melakukan survey",
        "email.required"      => "Email tidak boleh kosong",
        "email.email"         => "Format email tidak sesuai"
    ];

    /**
     * Constanta for strtotime 120 minutes
     * This constanta needed for set email verification link expiration
     */
    const EMAIL_VERIFICATION_LINK_EXPIRED_VALUE = '120';
    const SEMESTER = [1, 2];
    const PREFIX_CACHE_GET_DASHBOARD_TOTAL_INCOME = 'OwnerDataController-getDashboardTotalIncome-';
    const PREFIX_CACHE_GET_FINANCE_KOST = 'OwnerDataController-getFinanceKost-';

    public function __construct(OwnerDataRepository $repository)
    {
        $this->repository = $repository;

        parent::__construct();
    }

    public function getProfile(Request $request)
    {
        $user = $this->user();

    	if (! $user instanceof User) {
            return Api::responseData([
                "status"=> false,
                "meta" => array(
                    "message" => "No User data",
                    "code" => 333
                ),
                "user" => null,
                "membership" => null,
                "promo" => null
            ]);
        }

        if (! $user->isOwner()) {
            return Api::responseData([
                "status"=> false,
                "meta" => array(
                    "message" => "No owner data",
                    "code" => 333
                ),
                "user" => null,
                "membership" => null,
                "promo" => null
            ]);
        }

        $v1 = null;
        if ($request->filled('v')) {
            if ($request->input('v') == '1') $v1 = 1;
            if ($request->input('v') == '2') $v1 = 2;
        }

        $data = $this->repository->profile(
            $this->user()->id,
            $v1
        );

        return Api::responseData($data)
            ->header('Cache-Control', 'no-cache, no-store, must-revalidate')
            ->header('Pragma', 'no-cache')
            ->header('expires', '0');
    }

    /**
     * Get owned kos by owner
     *
     * @param Request
     * @return JsonResponse
     */
    public function getOwnedKos(Request $request, RoomUpdaterRepositoryEloquent $roomUpdaterRepository): JsonResponse
    {
        $user = $this->user();

        if (is_null($user)) {
            return Api::responseData(
                ["data" => []],
                false,
                false,
                401,
                "Unauthorized Access"
            );
        }

        $requestData = $request->all();

        $validator = Validator::make(
            $requestData,
            [
                'limit' => 'nullable|integer',
                'offset' => 'nullable|integer',
                'room_id' => 'nullable|integer',
                'exclude_unverified_flag' => 'nullable',
            ]
        );

        if ($validator->fails()) {
            return Api::responseData(["data" => []], false, false, 422, "Unprocessable Entity");
        }

        $roomStatuses = [];
        if ($request->has('exclude_unverified_flag')) {
            $roomStatuses = [
                RoomOwner::ROOM_ADD_STATUS,
                RoomOwner::ROOM_VERIFY_STATUS,
            ];
        }

        $roomUpdaterRepository->setPresenter(new RoomPresenter('list-owner-property'));
        $roomUpdaterRepository->pushCriteria(new OwnerCriteria($user, 'kos', $roomStatuses, $requestData));

        $roomAmmount = $roomUpdaterRepository->getOwnerTotalKos();

        $limit  = $request->filled('limit') ? (int) $request->input('limit') : 20;
        $offset = $request->filled('offset') ? (int) $request->input('offset') : 0;
        $roomId = $request->input('room_id');

        $page = ceil($offset / $limit) + 1;
        $take = $limit;

        $data = [];

        if ($roomId > 0) {
            $firstData = $roomUpdaterRepository->getOwnerKosBySongId($roomId);
            if (! empty($firstData)) {
                if ($limit == 1) {
                    return Api::responseData([
                        'page' => (int) $page,
                        'next-page' => (int) ($page + 1),
                        'limit' => (int) $limit,
                        'offset' => (int) $offset,
                        'has-more' => $roomAmmount > $limit * $page,
                        'total' => (int) $roomAmmount,
                        'toast' => null,
                        'data' => [
                            'rooms' => [
                                $firstData
                            ]
                        ]
                    ]);
                }

                $take--;
                $data[] = $firstData;
            }
        }

        $listData = $roomUpdaterRepository->getKosOwnerItem($take, $offset, $roomId);

        return Api::responseData(
            [
                'page' => (int) $page,
                'next-page' => (int) ($page + 1),
                'limit' => (int) $limit,
                'offset' => (int) $offset,
                'has-more' => $roomAmmount > $limit * $page,
                'total' => (int) $roomAmmount,
                'toast' => null,
                'data' => [
                    'rooms' => array_merge($data, $listData)
                ],
            ]
        )
        ->header('Cache-Control', 'no-cache, no-store, must-revalidate')
        ->header('Pragma', 'no-cache')
        ->header('expires', '0');
    }

    public function getListKos(Request $request)
    {
        if (is_null($this->user())) return Api::responseData(["status" => false]);

		$data = $this->repository->getListKos($this->user(), $request->all());

        if ($request->filled('room_id')) {
            $room_list = $this->toTopRoomUpdate($data['rooms'], $request->all());
            if ($room_list) {
                unset($data['rooms']);
                $data = array_merge($data, ["rooms" => $room_list]);
            } else {
                $firstData = $this->repository->getFirstData($request->all());
                if (!is_null($firstData)) {
                    array_shift($data['rooms']);
                }
                $data['rooms'] = array_merge([$firstData], $data['rooms']);
            }
            $data = $data;
        }

        return Api::responseData($data);
    }

    public function detailKosOwner(Request $request, $id)
    {
        if (is_null($this->user())) return Api::responseData(["status" => false]);

        $data = $this->repository->getDetailKos($this->user(), $id);
        return Api::responseData($data);
    }

    public function getListAllCanPromote(Request $request)
    {
        if (is_null($this->user())) {
            return Api::responseData(["status" => false]);
        }

        $data = $this->repository->listAllCanPromote($this->user());

        return Api::responseData($data);
    }

    public function getListRoomActive(Request $request, RoomRepositoryEloquent $roomRepository, $slug)
    {
        $roomType = ["kos", "apartment"];
        if (is_null($this->user()) || !in_array($slug, $roomType)) {
            return Api::responseData([
                "status" => false
            ]);
        }

        $roomRepository->setPresenter(new \App\Presenters\RoomPresenter('active'));
        $roomRepository->pushCriteria(new \App\Criteria\Room\OwnerCriteria($this->user(), $slug, ["verified"]));

        return Api::responseData($roomRepository->getItemListOwner());
    }

    public function getListApartment(Request $request)
    {
        if (is_null($this->user())) return Api::responseData(["status" => false]);

        $data = $this->repository->ListApartment($this->user(), $request->all());

        if ($request->filled('room_id')) {
            $room_list = $this->toTopRoomUpdate($data['rooms'], $request->all());
            if ($room_list) {
                unset($data['rooms']);
                $data = array_merge($data, ["rooms" => $room_list]);
            } else {
                $firstData = $this->repository->getFirstData($request->all());
                if (!is_null($firstData)) {
                    array_shift($data['rooms']);
                }
                $data['rooms'] = array_merge([$firstData], $data['rooms']);
            }
            $data = $data;
        }

        return Api::responseData($data);
    }

    public function getNotificationOwner(Request $request)
    {
        if (is_null($this->user())) return Api::responseData(["status" => false]);

        if (strpos(url()->current(), 'kay.mamikos.com') == false) {
            $via = "web";
        } else {
            $via = "app";
        }

        $data = $this->repository->notificationOwner($this->user(), $via, $request->all());

        return Api::responseData($data);
    }

    public function surveyUpdateRoom(Request $request)
    {
        if (is_null($this->user())) return Api::responseData(["status" => false]);

        $validator = Validator::make($request->all(),
            [
                'room_count' => 'required|numeric',
                'room_id'    => 'required|numeric',
                'data_for'   => 'required|in:'.implode(",", SurveySatisfaction::SURVEY_TYPE)
            ], $this->messageValidator);

        if ($validator->fails()) {
             return Api::responseData([
                     'status'=> false,
                     'meta'  => array("message" => Api::validationErrorsToString($validator->errors()))
                 ]);
        }

        $data = $this->repository->updateRoomSurvey($request->all(), $this->user());
        if ($data) $message = "Terima kasih sudah mengisi survey pemilik.";
        else $message = "Data tidak ditemukan";

        $resp = ["status" => $data, "meta" => ["message" => $message ]];

        return Api::responseData($resp);
    }

    public function surveyPremium(Request $request)
    {
        if (is_null($this->user())) return Api::responseData(["status" => false]);

        $validator = Validator::make($request->all(),
            [
                'reason'        => 'required',
                'premium_id'    => 'required|numeric',
                'data_for'      => 'required|in:'.implode(",", SurveySatisfaction::SURVEY_TYPE),
                'price'         => 'numeric',
                'room_count'    => 'numeric'
            ], $this->messageValidator);

        if ($validator->fails()) {
             return Api::responseData([
                     'status'=> false,
                     'meta'  => array("message" => Api::validationErrorsToString($validator->errors()))
                 ]);
        }

        $response = $this->repository->surveyPremiumOwner($this->user(), $request->all());
        return Api::responseData(["status" => true, "room_survey" => $response["room_survey"]]);
    }

    public function getListKosForUpdate(Request $request)
    {
        if (is_null($this->user())) {
            return Api::responseData(["status" => "false"]);
        }

        $data = $this->repository->getListDataUpdate($this->user(), $request->all());

        if ($request->filled('room_id')) {
            $room_list = $this->toTopRoomUpdate($data['rooms'], $request->all());
            if ($room_list) {
                unset($data['rooms']);
                $data = array_merge($data, ["rooms" => $room_list]);
            } else {
                $firstData = $this->repository->getFirstData($request->all(), 'update');
                if (!is_null($firstData)) {
                    array_shift($data['rooms']);
                }
                $data['rooms'] = array_merge([$firstData], $data['rooms']);
            }
            $data = $data;
        } else {
            NotifClick::insertData($this->user());
        }

        return Api::responseData($data);
    }

    public function toTopRoomUpdate($data, $request)
    {
        if (!isset($request['room_id'])) {
            return false;
        }

        foreach ($data as $key => $value) {
            if ($request['room_id'] == $value['_id']) {
                $idToTop = $key;
            }
        }

        if (!isset($idToTop)) {
            return false;
        }

        $dataToTop = $data[$idToTop];
        unset($data[$idToTop]);
        $data = array_merge([$dataToTop], $data);

        return $data;
    }


    public function searchUpdate(Request $request)
    {
        $user = $this->user();
        if (is_null($user)) {
            return Api::responseData(["status" => false, "data" => null, "message" => "Anda belum login."]);
        }

        $response = $this->repository->searchData($user, $request);
        return Api::responseData(["status" => $response["status"], "data" => $response["data"], "message" => $response["message"]]);
    }

    public function simpleUpdateNotif(Request $request, $id)
    {
        $user = $this->user();
        if (is_null($user)) {
            return Api::responseData(["status" => false, "message" => "Gagal"]);
        }
        $validator = Validator::make($request->all(), [
            'room_total' => 'required|numeric',
            'min_month' => 'required|numeric'
        ], $this->messageValidator);

        $room = Room::with('designer_tags')->where('song_id', $id)->first();
        if (is_null($room)) {
            return Api::responseData(["status" => false, "message" => "Data tidak ditemukan"]);
        }

        if ($validator->fails()) {
            return Api::responseData([
                    'status'=> false,
                    'meta'  => array("message" => Api::validationErrorsToString($validator->errors()))
                ]);
       }

       $response = (new UpdateRoom($room, $request->all()))->simple();

       return Api::responseData(["status" => $response["status"], "message" => $response["message"]]);
    }

    public function postVerificationEmail(Request $request)
    {
        $user = $this->user();
        if (is_null($user)) {
            return Api::responseData(["status" => false, "message" => "Data user tidak ditemukan"]);
        }

        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
        ], $this->messageValidator);

        $email = $request->input('email');

        /**
         * RFC 2822 email validation
         */
        $validEmail = (bool) preg_match(RegexHelper::validEmail(), $email) ? true : false;

        $user_cheker = User::where('email', $email)->where('id', '!=', $user->id)->first();

        $validator->after(function($validator) use ($user_cheker, $validEmail)
        {
            if (!is_null($user_cheker)) {
                $validator->errors()->add('email', 'Email sudah digunakan oleh akun lain');
            }

            if (false === $validEmail) {
                $validator->errors()->add('email', __('api.input.email.email'));
            }
        });

        if ($validator->fails()) {
            return Api::responseData([
                    'status'    => false,
                    'message'   => Api::validationErrorsToString($validator->errors())
                ]);
        }

        $expiredTime = date('Y-m-d H:i:s', strtotime('+'.OwnerDataController::EMAIL_VERIFICATION_LINK_EXPIRED_VALUE.' minutes'));
        $encode = base64_encode($email."&".$user->id."&".$expiredTime."&".$user->phone_number);
        $to = $request->input('email');
        $subject = "Verifikasi email";
        $link = url("verification/email/".$encode);

        if (is_null($user->name)) {
            $name = $user->phone_number;
        } else {
            $name = $user->name;
        }
        Mail::send('emails.user.verification', [
            'subject' => $subject,
            'recipient' => $to,
            'url' => $link,
            'username' => $name
        ], function ($message) use ($subject, $to) {
            $message->to($to)
                ->subject($subject);
        });
        return Api::responseData(["status" => true, "message" => "Buka email anda untuk verifikasi"]);
    }

    /**
     * Build event properties array for moengage tracker
     *
     * @param string $eventAt
     * @param string $phone
     * @param string $email
     * @param string $reason
     *
     * @return array
     */
    private function buildEventPropertiesArray(string $eventAt, string $phone, string $email, string $reason): array
    {
        $array = [
            EmailVerificationEventProps::INTERFACE_DEVICE     => Tracking::checkDevice(new Agent()),
            EmailVerificationEventProps::CREATED_AT           => $eventAt,
            EmailVerificationEventProps::OWNER_PHONE_NUMBER   => $phone,
            EmailVerificationEventProps::OWNER_EMAIL          => $email,
            EmailVerificationEventProps::FAILED_REASON        => $reason,
        ];

        return $array;
    }

    /**
     * Handle Moengage tracking event
     *
     * @param User $user
     * @param Array $eventProperties
     *
     * @return void
     */
    private function handleMoengageEventTracking(User $user, array $eventProperties): void
    {
        //Initialize var and do reportEmailVerification
        $moengageEventDataApi = new MoEngageEventDataApi();
        $moengageEventDataApi->reportEmailVerification($user, $eventProperties);
    }

    /**
     * We need temporary saving for this link, rather than create new table for checking purpose
     *
     * The link contain an user_id, email, expired_at and phone
     * If there is an link that contain:
     * - expired_at that has <= than expired that expected (less than 120 minutes)
     * - New email value with same user_id
     *
     * Then, we must block the action.
     * Main purpose => Preventing old link email verification could be validated again.
     *
     * @param string $email
     *
     * @return void
     */
    private function checkValidEmailVerificationLink(string $email, string $eventAt, string $phone, $userObj): void
    {
        $emailFromCache = Cache::get($email);
        if (!empty($emailFromCache)) {
            $reason = __('verify-link-not-valid');
            $eventProperties = $this->buildEventPropertiesArray($eventAt, $phone, $email, $reason);
            $this->handleMoengageEventTracking($userObj, $eventProperties);

            abort(404);
        }
    }

    /**
     * Set temporary email to cache => 120 minutes of live temp
     * This purpose for preventing old link email verification could be validated again.
     *
     * @param string $email
     *
     * @return void
     */
    private function setEmailTempCache(string $email): void
    {
        $expiredAt = Carbon::now()->addMinutes(OwnerDataController::EMAIL_VERIFICATION_LINK_EXPIRED_VALUE);
        Cache::put($email, $email, $expiredAt);
    }

    public function verificationEmailAction(Request $request, $id)
    {
        $decode_data = base64_decode($id);
        $data = explode("&", $decode_data);

        if (count($data) != 4) {
            return abort(404);
        }

        $email = $data[0];
        $userId = $data[1];
        $expired = $data[2];
        $phone = $data[3];

        //Initialize vars
        $eventProperties    = [];
        $eventAt            = Carbon::now()->format('Y-m-d H:i:s');
        $userObj            = User::find($userId);
        $reason = '';

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $reason = __('moengage-tracking.email-not-valid');
            $eventProperties = $this->buildEventPropertiesArray($eventAt, $phone, $email, $reason);
            $this->handleMoengageEventTracking($userObj, $eventProperties);
            return abort(404);
        }

        if (date('Y-m-d H:i:s') > $expired) {
            $reason = __('verify-link-not-valid');
            $eventProperties = $this->buildEventPropertiesArray($eventAt, $phone, $email, $reason);
            $this->handleMoengageEventTracking($userObj, $eventProperties);

            return abort(404);
        }

        //Check validation of email verification link
        $this->checkValidEmailVerificationLink($email, $eventAt, $phone, $userObj);

        $userData = User::where('email', $email)->first();
        if (!is_null($userData)) {
            $reason = __('email-does-not-exist');
            $eventProperties = $this->buildEventPropertiesArray($eventAt, $phone, $email, $reason);
            $this->handleMoengageEventTracking($userObj, $eventProperties);

            return abort(404);
        }

        $user = User::where('id', $userId)
                        ->where('phone_number', $phone)
                        ->first();

        if (is_null($user)) {
            return abort(404);
        }

        $user->email = $email;
        $user->save();

        //Set cache with email name
        //Set temporary email to cache => 120 minutes of live temp
        //This purpose for preventing old link email verification could be validated again.
        $this->setEmailTempCache($email);

        //Send to Moengage tracker
        $eventProperties = $this->buildEventPropertiesArray($eventAt, $phone, $email, $reason);
        $this->handleMoengageEventTracking($userObj, $eventProperties);

        return redirect(config('owner.dashboard_url') . '?email_verification=success');
    }

    /**
     * mark all property to be updated -> modify kost_updated_date on designer
     */
    public function updateAllProperty()
    {
        $user = $this->user();

        if (is_null($user)) {
            return Api::responseData(["activity" => null], false, false, 401, "Anda harus Login");
        }

        if (! $user->isOwner()) {
            return Api::responseData(["activity" => null], false, false, 404, "Akses ini hanya untuk owner");
        }

        UpdateKostUpdatedDate::dispatch($user->id);

        MarkActivity::dispatchNow($user->id, ActivityType::TYPE_UPDATE_PROPERTY);

        return Api::responseData([
            "activity" => [
                "logged_in_at" => Carbon::now()->toDateTimeString(),
                "updated_property_at" => Carbon::now()->toDateTimeString(),
            ]
        ]);
    }

    /**
     * Owner Dahsboard to get total omset
     *
     * Yusuf: add new result field for expense (financial report's need)
     *
     * @param month @param year (opsional) or it will all time
     * @return json
     */
    public function getDashboardTotalIncome(Request $request)
    {
        $user = $this->user();
        if (is_null($user)) {
            return Api::responseData(["status" => false, "data" => null, "message" => "Anda belum login."]);
        }

        $month = $request->input('month');
        $year = $request->input('year');
        //filter for semester request
        $semester = $request->input('semester');

        // for /income?year=2020&semester=1(ganjil) or semester=2(genap)
        if (
            is_numeric($semester)
            && is_numeric($year)
            && is_null($month)
        ) {
            // make separate function to avoid error from curent script
            return $this->getDashboardTotalIncomeSemester($user->id, $semester, $year);
        }

        // for /income
        if (is_null($month) && is_null($year)) {
            $result = $this->getIncomeFromMonthYear($user->id);
            return Api::responseData([
                "status" => true,
                "total_amount" => $result['total_amount'],
                'total_expense' => $result['total_expense']
            ]);
        }

        // for /income?year=2020
        if (is_null($month) && is_numeric($year)) {
            $result = $this->getIncomeFromMonthYear($user->id, $month, $year);
            return Api::responseData([
                "status" => true,
                "total_amount" => $result['total_amount'],
                'total_expense' => $result['total_expense']
            ]);
        }

        // validation : must be numeric to avoid sql injection
        if ( !is_numeric($month) || !is_numeric($year) ) {
            return Api::responseData(["status" => false, "data" => null, "message" => "invalid parameter"]);
        }

        // for /income?year=2020&month=2
        if (
            ($month < date('n') && $year == date('Y'))
            || ($month <= 12 && $year < date('Y'))
        ) {
            //caching from previous month and year
            $result = Cache::rememberForever(
                self::PREFIX_CACHE_GET_DASHBOARD_TOTAL_INCOME.$user->id.$month.$year,
                function () use ($user, $month, $year) {
                    return $this->getIncomeFromMonthYear($user->id, $month, $year);
            });
            return Api::responseData([
                "status" => true,
                "total_amount" => $result['total_amount'],
                'total_expense' => $result['total_expense']
            ]);
        }

        $result = $this->getIncomeFromMonthYear($user->id, $month, $year);
        return Api::responseData([
            "status" => true,
            "total_amount" => $result['total_amount'],
            'total_expense' => $result['total_expense']
        ]);
    }

    /**
     * Owner Dahsboard to get total omset from certain semester
     *
     * @param mixed userId
     * @param int semester
     * @param int year
     *
     * @return JsonResponse
     */
    private function getDashboardTotalIncomeSemester(
        $userId,
        int $semester,
        int $year
    ): JsonResponse {
        // validation : must be numeric to avoid sql injection
        if ( !in_array($semester, self::SEMESTER) || !is_numeric($year) ) {
            return Api::responseData(["status" => false, "data" => null, "message" => "invalid parameter"]);
        }

        $income = $this->repository->getTotalIncomeSemester($userId, $semester, $year);
        $expense = $this->repository->getTotalPremiumExpenseSemester($userId, $semester, $year);

        return Api::responseData([
            'status' => true,
            'total_amount' => $income,
            'total_expense' => $expense
        ]);
    }

    /**
     * Call repository method
     *
     * @param mixed userId
     * @param mixed month
     * @param mixed year
     *
     * @return array
     */
    private function getIncomeFromMonthYear(
        $userId,
        $month = null,
        $year = null
    ): array {
        $income = $this->repository->getTotalIncome($userId, $month, $year);
        $expense = $this->repository->getTotalPremiumExpense($userId, $month, $year);
        return [
            'total_amount' => $income,
            'total_expense' => $expense,
        ];
    }

    /**
     * Financial Report to get kost from owner with its income
     *
     * @param mixed request
     *
     * @return JsonResponse
     */
    public function getListKostsIncome(Request $request): JsonResponse
    {
        $user = $this->user();
        if (is_null($user)) {
            return Api::responseData(["status" => false, "data" => null, "message" => "Anda belum login."]);
        }

        $offset = $request->input('offset') ? $request->input('offset') : 0;
        $limit = $request->input('limit')? $request->input('limit') : 10;
        $keyword = $request->input('keyword');

        // search by keyword according to kost's name
        if (! empty($keyword)) {
            $kosts = $this->repository->getFinanceKostFromName($user->id, $keyword);
            return Api::responseData(["status" => true, "data" => $kosts ]);
        }

        $offset = $offset*$limit;
        $kosts = $this->repository->getFinanceListKosts($user->id, $offset, $limit);
        return Api::responseData(["status" => true, "data" => $kosts ]);
    }

    /**
     * Financial Report for specific kost
     *
     * @param mixed request
     * @param int id
     *
     * @return JsonResponse
     */
    public function getFinanceKost(Request $request, int $id): ?JsonResponse
    {
        // verify room is owned by current login owner
        $user = $this->user();
        if (is_null($user)) {
            Bugsnag::notifyException(new \RunTimeException('User data is null.'));
            return null;
        }

        $status = $this->repository->verifyKostWithItsOwnerBySongId($user->id, $id);
        if (! $status) {
            return Api::responseData([
                'status' => false,
                'data' => null,
                'message' => 'Kost '.$id.' bukan milik '.$user->name
            ]);
        }

        $month = $request->input('month');
        $year = $request->input('year');
        //filter for semester request
        $semester = $request->input('semester');

        // for /finance/kost/{id}?year=2020&semester=1(ganjil) or semester=2(genap)
        if (
            is_numeric($semester)
            && is_numeric($year)
            && empty($month)
        ) {
            // make separate function to avoid error from curent script
            return $this->getFinanceKostSemester($id, $semester, $year);
        }

        $result = $this->filterFinanceKost_AllTimeYearly($id, $month, $year);
        if (! is_null($result)) {
            return $result;
        }

        // for /finance/kost/{id}?year=2020&month=2
        if (
            ($month < date('n') && $year === date('Y'))
            || ($month <= 12 && $year < date('Y'))
        ) {
            //caching from previous month and year
            $room = Cache::rememberForever(
                self::PREFIX_CACHE_GET_FINANCE_KOST.$id.$month.$year,
                function () use ($id, $month, $year) {
                    return $this->repository->getFinanceSpecificKost($id, $month, $year);
            });
            return Api::responseData([
                'status' => true,
                'data' => $room
            ]);
        }

        $room = $this->repository->getFinanceSpecificKost($id, $month, $year);
        return Api::responseData([
            'status' => true,
            'data' => $room
        ]);
    }

    /**
     * Map if for 2 parameters $month, $year
     *
     * @param $id
     * @param $month
     * @param $year
     *
     * @return ?JsonResponse
     */
    private function filterFinanceKost_AllTimeYearly(
        $id,
        $month,
        $year
    ): ?JsonResponse {
        // for all time finance/kost/{id}
        if (is_null($month) && is_null($year)) {
            $room = $this->repository->getFinanceSpecificKost($id);
            return Api::responseData([
                'status' => true,
                'data' => $room
            ]);
        }

        // for /finance/kost/{id}?year=2020
        if (is_null($month) && is_numeric($year)) {
            $room = $this->repository->getFinanceSpecificKost($id, $month, $year);
            return Api::responseData([
                'status' => true,
                'data' => $room
            ]);
        }

        // validation : must be numeric to avoid sql injection
        if ( !is_numeric($month) || !is_numeric($year) ) {
            return Api::responseData([
                'status' => false,
                'data' => null,
                'message' => 'invalid parameter'
            ]);
        }

        return null;
    }

    /**
     * Owner Dahsboard to get total omset from certain semester
     *
     * @param int kostId
     * @param int semester
     * @param int year
     *
     * @return JsonResponse
     */
    private function getFinanceKostSemester(
        int $kostId,
        int $semester,
        int $year
    ): JsonResponse {
        // validation : must be numeric to avoid sql injection
        if (!in_array($semester, self::SEMESTER) || !is_numeric($year)) {
            return Api::responseData([
                'status' => false,
                'data' => null,
                'message' => 'invalid parameter'
            ]);
        }

        return Api::responseData([
            'status' => true,
            'data' => $this->repository->getFinanceSpecificKostSemester($kostId, $semester, $year)
        ]);
    }

    /**
    * List tenants already paid
    *
    * @param mixed request
    * @param int $id
    *
    * @return JsonResponse
    */
    public function getListTenant(Request $request, int $id): ?JsonResponse
    {
        $user = $this->user();
        if (is_null($user)) {
            Bugsnag::notifyException(new \RunTimeException('User data is null.'));
            return null;
        }

        $status = $this->repository->verifyKostWithItsOwnerBySongId($user->id, $id);
        if (! $status) {
            return Api::responseData([
                'status' => false,
                'data' => null,
                'message' => 'Kost '.$id.' bukan milik '.$user->name
            ]);
        }

        $offset = ! is_null($request->input('offset')) ? $request->input('offset') : 0;
        $limit = ! is_null($request->input('limit')) ? $request->input('limit') : 3;

        $tenants = $this->repository->getListTenants($id, $offset, $limit);
        return Api::responseData(["status" => true, "data" => $tenants ]);
    }

    /**
     * Financial Report to get list expense from specific kost
     *
     * @param mixed request
     * @param int id
     *
     * @return JsonResponse
     */
    public function getListKostsExpense(Request $request, int $id): ?JsonResponse
    {
        $user = $this->user();
        if (is_null($user)) {
            Bugsnag::notifyException(new \RunTimeException('User data is null.'));
            return null;
        }

        $status = $this->repository->verifyKostWithItsOwnerBySongId($user->id, $id);
        if (! $status) {
            return Api::responseData([
                'status' => false,
                'data' => null,
                'message' => 'Kost '.$id.' bukan milik '.$user->name
            ]);
        }

        $offset = $request->input('offset') ? (int) $request->input('offset') : 0;
        $limit = $request->input('limit')? (int) $request->input('limit') : 3;

        $offset = $offset*$limit;
        $expenses = $this->repository->getListPremiumExpensesSpecificKost($id, $offset, $limit);
        return Api::responseData(["status" => true, "data" => $expenses ]);
    }
}
