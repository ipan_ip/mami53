<?php

namespace App\Http\Controllers\Api;

use App\Entities\Landing\Landing;
use App\Entities\Room\RoomFilter;
use App\Http\Helpers\ApiResponse as Api;
use App\Http\Helpers\LocationHelper;
use App\Repositories\Room\RoomFilterRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class RoomFilterController extends BaseController
{
    /**
     * @var RoomFilterRepository
     */
    protected $repository;

    public function __construct(RoomFilterRepository $repository)
    {
        $this->repository = $repository;

        parent::__construct();
    }

    /**
     * Count Rooms based on some criteria
     *
     * @param  Request $request
     * @return JsonResponse
     */
    public function countFilteredRooms(Request $request)
    {
        $filters = $request->input('filters');

        if (empty($filters['place'])) {
            $filters['place'] = null;
        }

        $filters['location'] = $request->input('location');
        $filters['sorting'] = $request->input('sorting');

        //new param form mobile gps (only 1 latitude and 1 longitude)
        $filters['user_location'] = $request->input('user_location');

        if ($request->filled('referrer')
            && $request->input('referrer') == "promo"
            && !is_null($filters['place'])) {
            $place = implode(" ", $filters['place']);
            $location = Landing::where('slug', "LIKE", "%" . strtolower($place) . "%")->first();
            if (!is_null($location)) {
                $filters['location'] = [
                    [
                        $location->longitude_1,
                        $location->latitude_1
                    ],
                    [
                        $location->longitude_2,
                        $location->latitude_2
                    ]
                ];
            }
        }

        // Flash Sale landings handler
        if (
            $request->filled('landings')
            && !empty($request->landings)
        ) {
            $filters['landings'] = $request->landings;
        }

        // Filter request which does NOT have "location" data
        $filters['titik_tengah'] = isset($filters['location']) ? LocationHelper::getCenterLocationForTracker(
            $filters['location']
        ) : null;

        if (isset($filters['include_pinned'])) {
            $response = $this->repository->countHomeList(new RoomFilter($filters));
        } else {
            $response = $this->repository->countItemList(new RoomFilter($filters));
        }

        return Api::responseData(
            [
                'property_total' => $response
            ]
        );
    }
}
