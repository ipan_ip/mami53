<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\User;
use App\Http\Helpers\ApiResponse as Api;
use App\Entities\Mamipay\MamipayTenant;
use App\Entities\Mamipay\MamipayTenantBank;
use App\Libraries\MamipayApi;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;

class UserWalletController extends BaseController
{ 
    protected $walletValidatorMessage = [
        'total_withdraw' => 'Jumlah penarikan tidak boleh kosong',
        'bank_account'   => 'Akun bank harus di isi'
    ];

    public function getBalance()
    {
        $user = $this->user();

        if (!$user) {
            return Api::responseData([
                'status' => false,
                'meta'   => [
                    'message' => 'Silakan login terlebih dahulu',
                ],
            ]);
        }

        $tenantVa = MamipayTenant::where('user_id', $user->id)->first();

        $tenantBanks = MamipayTenantBank::where('tenant_id', $user->id)->get();
        $isBank = false;
        if ($tenantBanks) {
            $isBank = true;
        }

        $client  = new Client(['base_uri' => ENV('MAMIDOMPET')]);

        try {
            $response = $client->get('saldo/user/'.$user->id, [
                'headers' => [
                    'Content-Type'  => 'application/json',
                    'Authorization' => ENV('DOMPET_ACCESS_TOKEN')
                ]
            ]); 

            $responseResult = json_decode($response->getBody()->getContents());

            $result = [
                "amount" => $responseResult->saldo->amount,
                "va"     => [
                    "va_number_bni"  => $tenantVa->va_number_bni,
                    "va_number_hana" => $tenantVa->va_number_hana
                ],
                "is_bank" => $isBank
            ];

            if (!empty($responseResult->saldo)) {
                return Api::responseData([
                    'status' => true,
                    'data'   => $result
                ]);

            } else {
                return Api::responseData([
                    'status' => false,
                    'meta'   => ['message' => 'Maaf data tidak tersedia']
                ]);
            }

        }  catch (RequestException $e) {
            throw new \Exception(\GuzzleHttp\Psr7\str($e->getResponse()), 256);
        }  catch(\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function getWalletHistory($action, $transactionId = null)
    {
        $user = $this->user();

        if (!$user) {
            return Api::responseData([
                'status' => false,
                'meta'   => [
                    'message' => 'Silakan login terlebih dahulu',
                ],
            ]);
        }

        $client  = new Client(['base_uri' => ENV('MAMIDOMPET')]);

        $param = null;
        if (!is_null($transactionId)) {
            $param = $transactionId;
        } else {
            $param = $user->id;
        }

        try {
            $response = $client->get('transaction/'.$action.'/user/'.$param, [
                'headers' => [
                    'Content-Type'  => 'application/json',
                    'Authorization' => ENV('DOMPET_ACCESS_TOKEN')
                ]
            ]); 

            $responseResult = json_decode($response->getBody()->getContents());
            
            $result = [];
            if (!empty($responseResult->transactions)) {
                $transactions = $responseResult->transactions;
                foreach($transactions as $transaction) {
                    $result[] = [
                        "id"                => $transaction->id,
                        "trx_number"        => $transaction->trx_number,
                        "wallet_id"         => $transaction->wallet_id,
                        "user_id"           => $transaction->user_id,
                        "amount"            => $transaction->amount,
                        "action"            => $transaction->action,
                        "remark"            => $transaction->remark,
                        "reference"         => $transaction->reference,
                        "balance"           => $transaction->balance,
                        "verified"          => $transaction->verified,
                        "transaction_date"  => Carbon::createFromTimestampMs($transaction->transaction_date)->format('d F Y H:i:s')
                    ];
                }

            } elseif (!empty($responseResult->transaction)) {
                $result = [
                    "id"                => $responseResult->transaction->id,
                    "trx_number"        => $responseResult->transaction->trx_number,
                    "wallet_id"         => $responseResult->transaction->wallet_id,
                    "user_id"           => $responseResult->transaction->user_id,
                    "amount"            => $responseResult->transaction->amount,
                    "action"            => $responseResult->transaction->action,
                    "remark"            => $responseResult->transaction->remark,
                    "reference"         => $responseResult->transaction->reference,
                    "balance"           => $responseResult->transaction->balance,
                    "verified"          => $responseResult->transaction->verified,
                    "transaction_date"  => Carbon::createFromTimestampMs($responseResult->transaction->transaction_date)->format('d F Y H:i:s')
                ];

            } else {
                $result = null;
            }

            if ($result != null) {
                return Api::responseData([
                    'status' => true,
                    'data'   => $result
                ]);

            } else {
                return Api::responseData([
                    'status' => false,
                    'meta'   => ['message' => 'Maaf data tidak tersedia']
                ]);
            }

        }  catch (RequestException $e) {
            throw new \Exception(\GuzzleHttp\Psr7\str($e->getResponse()), 256);
        }  catch(\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function withdraw(Request $request)
    {
        $user = $this->user();

        if (!$user) {
            return Api::responseData([
                'status' => false,
                'meta'   => [
                    'message' => 'Silakan login terlebih dahulu',
                ],
            ]);
        }

        $validator = Validator::make($request->all(), [
            'amount'        => 'required',
            'bank_account'  => 'required',
            'remark'        => 'required'
        ], $this->walletValidatorMessage);

        if($validator->fails()) {
            $response = [
                "status" => false,
                "meta"   => [ 'message' => $validator->errors() ],
            ];

            return Api::responseData($response);
        }

        // Process Withdraw from wallet
        $mamipayUri = 'internal/tenant/withdraw/' .$user->id;
        $mamipayApi = new MamipayApi();
        $postWithdraw = $mamipayApi->post($request->all(), $mamipayUri);

        if ($postWithdraw->status == "200") {
            return Api::responseData([
                'status' => true,
                'meta'   => [
                    'message' => 'Success',
                ],
            ]);
        } else {
            return Api::responseData([
                'status' => false,
                'meta'   => [
                    'message' => 'Failed',
                ],
            ]);
        }
    }

    public function createVABNI(Request $request)
    {
        $user = $this->user();

        if (!$user) {
            return Api::responseData([
                'status' => false,
                'meta'   => [
                    'message' => 'Silakan login terlebih dahulu',
                ],
            ]);
        }

        $tenantVa = MamipayTenant::where('user_id', $user->id)->first();
        if (empty($tenantVa->va_number_bni)) {
            $mamipayAPI = new MamipayApi();
            $uri = "/internal/tenant/create-va/".$user->id;
            $getFromMamipayApi = $mamipayAPI->get($uri);

            if (!empty($getFromMamipayApi->data)) {
                return Api::responseData([
                    'status' => true,
                    'data'   => $getFromMamipayApi->data
                ]);

            } else {
                return Api::responseData([
                    'status' => false,
                    'meta'   => ['message' => 'Maaf data tidak tersedia']
                ]);
            }
        } else {
            return Api::responseData([
                'status' => false,
                'meta'   => ['message' => 'VA sudah tersedia']
            ]);
        }
    }
}
