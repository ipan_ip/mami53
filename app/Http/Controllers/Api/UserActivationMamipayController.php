<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Helpers\ApiResponse as Api;
use App\Http\Helpers\ApiHelper;
use App\Http\Requests;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Validator;
use App\User;
use App\Entities\Mamipay\MamipayMedia;
use App\Entities\Mamipay\MamipayTenant;
use App\Libraries\SMSLibrary;
use Jenssegers\Agent\Agent;
use App\Entities\Activity\ActivationCode;
use App\Entities\Activity\Tracking;
use Illuminate\Support\Facades\Hash;
use Config;

class UserActivationMamipayController extends BaseController 
{
    protected $userValidatorMessage = [
        "phone_number.required"       => "No tidak boleh kosong",
        "verification_code.required"  => "Verifikasi Kode tidak boleh kosong",
        "pin.required"                => "Pin tidak boleh kosong",
        "pin_confirm.required"        => "Pin Confirm tidak boleh kosong",
        "pin_latest.required"         => "Pin terkahir Anda tidak boleh kosong",
    ];

    protected $mamipayURL;
    protected $mamipayToken;

    public function __construct()
    {
        $this->mamipayURL   = Config::get('api.mamipay.url');
        $this->mamipayToken = Config::get('api.mamipay.token');
    }

    public function sendCodeOtp(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone_number' => 'required'
        ], $this->userValidatorMessage);

        $user = $this->user();

        if($validator->fails()) {
            $response = [
                "status" => false,
                "meta"   => [ 'message' => implode(",",$validator->messages()->all()) ],
            ];

            return Api::responseData($response);
        }

        $checkValidPhone = SMSLibrary::validateDomesticIndonesianMobileNumber($request->phone_number);
        if (!$checkValidPhone OR is_null($user)) {
            $response = [
                'status' => false,
                'meta'   => [
                    "message" => "Nomor yang Anda masukkan tidak valid"
                ]
            ];

        } else {
            $lastCodeRequest = ActivationCode::where('phone_number', $request->phone_number)
                                                ->where('for', $request->for)
                                                ->orderBy('id', 'desc')
                                                ->first();

            if ($lastCodeRequest) {
                if (Carbon::parse($lastCodeRequest->created_at)->diffInMinutes(Carbon::now(), false) <= 0) {
                    return [
                        'status' => false,
                        'meta'   => [
                            "message" => 'Kode verifikasi hanya bisa diminta tiap 3 menit sekali'
                        ]
                    ];

                }
            }

            $code    = ApiHelper::random_string('alnum','4');
            $message = "<#> ".$code." adalah kode verifikasi Mamikos anda. WASPADA PENIPUAN! JANGAN MEMBERITAHU KODE VERIFIKASI INI KE SIAPAPUN termasuk pihak Mamikos. "."\n\nb0lUOxG7rtF";
            $agent   = new Agent();
            
            SMSLibrary::smsVerification($request->phone_number, $message);

            $ActivationCode = array(
                "phone_number" => $request->phone_number,
                "device"       => Tracking::checkDevice($agent),
                "code"         => $code,
                "for"          => $request->for,
                "expired_at"   => Carbon::now()->addHours(6)->format('Y-m-d H:i:s')
            );

            ActivationCode::create($ActivationCode);

            $response = [
                "status" => true,
                "meta"   => ['message' => 'Success']
            ];

            return Api::responseData($response);
        }

    }

    public function checkCodeOtp(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'verification_code' => 'required',
            'phone_number'      => 'required'
        ], $this->userValidatorMessage);

        $user = $this->user();

        if($validator->fails()) {
            $response = [
                "status" => true,
                "meta"   => ['message' => implode(",",$validator->messages()->all())]
            ];

            return Api::responseData($response);
        }

        $activationCodes = ActivationCode::where('phone_number', $request->phone_number)
                                        ->whereRaw('expired_at > NOW()')
                                        ->where('code', $request->verification_code)
                                        ->first();

        if ($activationCodes) { 
            $response = [
                "status" => true,
                "meta"   => ['message' => "Verification code valid"]
            ];

            return Api::responseData($response);
        } else {
            $response = [
                "status" => true,
                "meta"   => ['message' => "Verification code valid"]
            ];

            return Api::responseData($response);
        }
        
    }

    public function activationUserMamipay(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone_number'      => 'required',
            'verification_code' => 'required'
        ], $this->userValidatorMessage);

        $user = $this->user();

        if($validator->fails()) {
            $response = [
                "status" => true,
                "meta"   => ['message' => implode(",",$validator->messages()->all())]
            ];

            return Api::responseData($response);
        }

        $activationCodes = ActivationCode::where('phone_number', $request->phone_number)
                                        ->whereRaw('expired_at > NOW()')
                                        ->where('code', $request->verification_code)
                                        ->first();

        if ($activationCodes) {
            $createMamipaytenant = $this->createMamipayTenant($user);

            $activationCodes = ActivationCode::where('phone_number', $request->phone_number)
                                        ->whereRaw('expired_at > NOW()')
                                        ->where('code', $request->verification_code)
                                        ->update(['expired_at' => Carbon::now()]);   

            return $createMamipaytenant;           

        } else {
            return Api::responseData([
                "status" => false,
                "meta"   => ['message' => 'Maaf kode verifikasi tidak valid'] 
            ]);
        }
    }

    public function createMamipayTenant($user)
    {
        // get mamipay media for photo identifier
        $userMamipayMedia       = new MamipayMedia();
        $getUserMediaIdentifier = $userMamipayMedia->getUserMedia($user->id, "identifier", "photo_ktp");
        $getUserMediaDocument   = $userMamipayMedia->getUserMedia($user->id, "document", "selfie_ktp");

        $body = [
            'name'                => $user->name,
            'phone_number'        => $user->phone_number,
            'gender'              => $user->gender,
            'email'               => $user->email,
            'occupation'          => $user->job,            
            'photo_id'            => "",
            'parent_name'         => "",
            'parent_phone_number' => isset($user->phone_number_additional) ? $user->phone_number_additional : "",
            'photo_identifier_id' => isset($getUserMediaIdentifier->id) ? $getUserMediaIdentifier->id : null,
            'photo_document_id'   => isset($getUserMediaDocument->id) ? $getUserMediaDocument->id : null,
            'marital_status'      => isset($user->marital_status) ? $user->marital_status : "",
        ];

        // send data tenant to mamipay create tenant API
        $client   = new Client(['base_uri' => $this->mamipayURL]);

        try {
            $response = $client->request('POST', 'internal/tenant/add', [
                'headers' => [
                    'Content-Type'  => 'application/x-www-form-urlencoded',
                    'Authorization' => $this->mamipayToken
                ], 
                'form_params' => $body
            ]); 

            return Api::responseData([
                "status" => true, 
            ]);

        }  catch (RequestException $e) {
            throw new \Exception(\GuzzleHttp\Psr7\str($e->getResponse()), 256);
        }  catch(\Exception $e) {
            throw new \Exception($e->getMessage());
        }

    }

    public function storePin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'pin'           => 'required',
            'pin_confirm'   => 'required'
        ], $this->userValidatorMessage);

        if($validator->fails()) {
            $response = [
                "status" => true,
                "meta"   => [
                    'message' => implode(",",$validator->messages()->all())
                ]
            ];
            return Api::responseData($response);
        }

        $user = $this->user();

        if (!$user) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Silakan login terlebih dahulu',
                ],
            ]);
        }

        if ($request->pin == $request->pin_confirm) {
            $tenant = MamipayTenant::where('user_id', $user->id)->first();

            if ($tenant) {
                $tenant->pin = Hash::make($request->input('pin'));
                $update      = $tenant->update();
        
                if ($update) {
                    return Api::responseData([
                        "status" => true, 
                    ]);
        
                } else {
                    return Api::responseData([
                        "status" => false,
                        "meta"   => ['message' => 'Maaf silakan coba lagi'] 
                    ]);
                }

            } else {
                return Api::responseData([
                    "status" => false,
                    "meta"   => ['message' => 'Anda belum melakukan registrasi'] 
                ]);
                
            }
            
        } else {
            return Api::responseData([
                "status" => false,
                "meta"   => ['message' => 'Pin anda tidak sama'] 
            ]);
        }
        
    }

    public function changePin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'pin'           => 'required',
            'pin_confirm'   => 'required',
            'pin_latest'    => 'required'
        ], $this->userValidatorMessage);

        if($validator->fails()) {
            $response = [
                "status" => true,
                "meta"   => ['message' => implode(",",$validator->messages()->all())]
            ];

            return Api::responseData($response);
        }

        $user = $this->user();

        if (!$user) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Silakan login terlebih dahulu',
                ],
            ]);
        }
        
        $tenant = MamipayTenant::where('user_id', $user->id)->first();

        if (Hash::check($request->pin_latest, $tenant->pin)) {
            $tenant->pin = Hash::make($request->pin);
            $update      = $tenant->update();
    
            if ($update) {
                return Api::responseData([
                    "status" => true, 
                ]);
    
            } else {
                return Api::responseData([
                    "status" => false,
                    "meta"   => ['message' => 'Maaf silakan coba lagi'] 
                ]);
            }

        } else {
            return Api::responseData([
                "status" => false,
                "meta"   => ['message' => 'Pin anda tidak sama'] 
            ]);

        }
    }

    public function checkValidPin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'pin'   => 'required',
        ], $this->userValidatorMessage);

        if($validator->fails()) {
            $response = [
                "status" => true,
                "meta"   => ['message' => implode(",",$validator->messages()->all())]
            ];

            return Api::responseData($response);
        }

        $user = $this->user();

        if (!$user) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Silakan login terlebih dahulu',
                ],
            ]);
        }

        $tenant = MamipayTenant::where('user_id', $user->id)->first();

        if (Hash::check($request->pin, $tenant->pin)) {
            return Api::responseData([
                "status" => true,
                "meta"   => ['message' => 'Pin anda valid'] 
            ]);

        } else {
            return Api::responseData([
                "status" => false,
                "meta"   => ['message' => 'Pin anda tidak valid'] 
            ]);

        }


    }

}
