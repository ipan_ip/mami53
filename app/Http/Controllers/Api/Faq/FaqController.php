<?php

namespace App\Http\Controllers\Api\Faq;

use App\Entities\Landing\Landing;
use App\Http\Controllers\Api\BaseController;
use App\Repositories\Landing\LandingRepository;
use App\Http\Helpers\ApiResponse;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Cache;

/**
 * Retrieve FAQ for landing page city and near kampus
 */
class FaqController extends BaseController
{
    const PREFIX_CACHE = 'FAQController-show';
    const REGEX_CITY_KAMPUS = '/kost-(dekat-)?([a-z-]+)-(murah)/';
    const BANNED_CHARACTER = [
        'kost',
        'dekat',
        'murah',
        '-'
    ];
    
    /**
     * Show Faq data from cache or retrieve new from db
     * 
     * @param string $slug
     * 
     * @return JsonResponse
     */
    public function show(string $slug): JsonResponse
    {
        $landingRepository = app()->make(LandingRepository::class);
        $data =  Cache::remember(
            self::PREFIX_CACHE.'-'.$slug,
            now()->endOfDay(),
            function () use ($slug, $landingRepository) {
                return $this->getData($slug, $landingRepository);
        });
        return ApiResponse::responseData([
            'data' => [
                'location' => $this->getCityKampusFromSlug($slug),
                'faqs' => $data
            ]
        ]);
    }

    /**
     * Retrieve FAQ data from database
     * 
     * @param string $slug
     * @param mixed $landingRepository
     * 
     * @return array
     */
    public function getData(string $slug, LandingRepository $landingRepository): ?array
    {
        $landingDetail = $landingRepository->getLandingDetailBySlug(
            $slug
        );

        if (! $landingDetail instanceof Landing) {
            return null;
        }

        $cheapestDaily = $landingRepository->getCheapestDaily(
            $landingDetail
        );

        $cheapestMonthly = $landingRepository->getCheapestMonthly(
            $landingDetail
        );

        $cheapestWeekly = $landingRepository->getCheapestWeekly(
            $landingDetail
        );

        $cheapestYearly = $landingRepository->getCheapestYearly(
            $landingDetail
        );

        $availableBisaBooking = $landingRepository->getAvailableBisaBooking(
            $landingDetail
        );

        $cheapestPutri = $landingRepository->getCheapestPutri(
            $landingDetail
        );

        $cheapestPutra = $landingRepository->getCheapestPutra(
            $landingDetail
        );

        $cheapestCampur = $landingRepository->getCheapestCampur(
            $landingDetail
        );

        $cheapestBebas24Jam = $landingRepository->getCheapestBebas24Jam(
            $landingDetail
        );

        $cheapestPasutri = $landingRepository->getCheapestPasutri(
            $landingDetail
        );

        $cheapestKamarMandiDalam = $landingRepository->getCheapestKamarMandiDalam(
            $landingDetail
        );

        $cheapestWifi = $landingRepository->getCheapestWifi(
            $landingDetail
        );

        $result = [
            'cheapest_daily' => $cheapestDaily,
            'cheapest_monthly' => $cheapestMonthly,
            'cheapest_weekly' => $cheapestWeekly,
            'cheapest_yearly' => $cheapestYearly,
            'available_bisaBooking' => $availableBisaBooking,
            'cheapest_putri' => $cheapestPutri,
            'cheapest_putra' => $cheapestPutra,
            'cheapest_campur' => $cheapestCampur,
            'cheapest_bebas24Jam' => $cheapestBebas24Jam,
            'cheapest_pasutri' => $cheapestPasutri,
            'cheapest_kamarMandiDalam' => $cheapestKamarMandiDalam,
            'cheapest_wifi' => $cheapestWifi,
        ];

        return $result;
    }

    /**
     * Retrieve City/Kampus from slug like [kost-dekat-ui-depok-murah, kost-dekat-malang-murah]
     * 
     * @param string $slug
     * 
     * @return string
     */
    private function getCityKampusFromSlug(string $slug): string
    {
        $matches = [];
        preg_match(self::REGEX_CITY_KAMPUS, $slug, $matches);

        if (empty($matches)) {
            $slugArray = explode('-', $slug);
            $bannedCharacter = self::BANNED_CHARACTER;
            foreach ($slugArray as $key => $value) {
                if (in_array($value, $bannedCharacter)) {
                    unset($slugArray[$key]);
                }
            }
            return implode(' ', $slugArray);
        }

        if (isset($matches[2])) {
            return str_replace('-', ' ', $matches[2]);
        }

        return str_replace('-', ' ', $slug);
    }
}
