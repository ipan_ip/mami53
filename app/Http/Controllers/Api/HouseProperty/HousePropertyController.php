<?php

namespace App\Http\Controllers\Api\HouseProperty;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Controllers\Api\BaseController;
use App\Http\Helpers\ApiResponse as Api;
use App\Repositories\HouseProperty\HousePropertyRepository;
use App\Entities\HouseProperty\HousePropertyFilter;
use App\Http\Helpers\UtilityHelper;
use App\Entities\HouseProperty\HousePropertyCall;
use Jenssegers\Agent\Agent;
use App\Entities\Activity\Tracking;
use App\Entities\Activity\ChatAdmin;
use Validator;
use App\Entities\HouseProperty\HousePropertyCluster;

class HousePropertyController extends BaseController
{

    protected $repository;
    protected $validatorMessage = [

    ];

	public function __construct(HousePropertyRepository $repository)
    {
        $this->repository = $repository;

        parent::__construct();
    }

    public function detail(Request $request, $type, $songId)
    {
        if (!in_array($type, ["villa", "kontrakan"])) {
            return Api::responseData(["status" => false, "meta" => ["message" => "Data tidak ditemukan"]]);
        }

        $response = $this->repository->getDetailHouseProperty($type, $songId, "song_id");
        return Api::responseData($response);
    }

    public function listSearch(Request $request)
    {
        ini_set('max_execution_time', 100000);
        $this->repository->setPresenter(new \App\Presenters\HousePropertyPresenter('list'));

        $filters = $request->input('filters');
        $filters['location'] = $request->input('location');
        $filters['sorting'] = $request->input('sorting');
        
        if (isset($filters['location']) && count($filters['location']) > 0) {
            $filters['titik_tengah'] = UtilityHelper::getCenterPoint($filters['location']);
        }
        
        $this->repository->pushCriteria(new \App\Criteria\HouseProperty\MainFilterCriteria($filters));
        $response = $this->repository->getItemList(new HousePropertyFilter($filters));
        return Api::responseData($response);
    }

    public function postCluster(Request $request)
    {
        ini_set('max_execution_time', 100000);
        $filters = $request->input('filters');
        $filters['location'] = $request->input('location');
        $filters['sorting'] = $request->input('sorting');
        
        if (isset($filters['location']) && count($filters['location']) > 0) {
            $filters['titik_tengah'] = UtilityHelper::getCenterPoint($filters['location']);
        }
        $response = $this->repository->getClusterList($filters);
        return Api::responseData($response);
    }

    public function postClusterList(Request $request)
    {
        $location = $request->input('location');
        $point = $request->input('point');

        $location = HousePropertyCluster::getRangeSingleCluster($location, $point, 7);
        
        $sort = 'new';
        $sortingArray = ["new" => "desc", "old" => "asc"];
        if ($request->filled('sorting') AND in_array($request->input('sorting'), array_keys($sortingArray))) {
            $sorting = $request->input('sorting');
            $sorting = $sortingArray[$sorting]; 
            if (isset($sorting['direction']) AND in_array($sorting['direction'], ['asc', 'desc', 'rand'])) $sort = $sorting['direction']; 
        }

        $request->merge(array("sorting"  => array('field'=>'price','direction'=> $sort)));
        $request->merge(array("location" => $location));

        return $this->listSearch($request);
    }

    public function postCall(Request $request, $songId)
    {
        $device = $this->device();
        if (is_null($device) === false && 
            $device->getIsSendBirdSupported() === false)
        {
            $message = $device->platform == 'android' 
                ? 'Harap update aplikasi Anda. Fitur ini dapat digunakan pada versi 3.2.0'
                : 'Harap update aplikasi Anda. Fitur ini dapat digunakan pada versi 3.1.0';
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => $message,
                ],
            ]);  
        }
        
        $user = $this->user();
        $userStatus = $this->checkCallingUser($user);
        if (!is_null($userStatus)) {
            return $userStatus;
        }
        $limitStatus = $this->checkDailyQuestionLimit($user, $songId);
        if (!is_null($limitStatus)) {
            return $limitStatus;
        }

        $agent = new Agent();
        $add_from = Tracking::checkDevice($agent);

        $callData = $request->all('phone', 'email', 'note', 'name', 'schedule', 'group_chat_id', 'group_channel_url');
        $callData['add_from'] = $add_from;

        $call = $this->repository->addCall($songId, $callData, $user, $this->device());
        if (!$call) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Gagal',
                ],
            ]);
        }

        return Api::responseData([
            'status' => true,
            'result' => $call ? true : false,
            'call' => [
                'note' => $callData['note'],
            ],
            'admin_id' => $call->chat_admin_id,
            'call_id' => $call->id,
            'admin_list' => ChatAdmin::getAdminIdsForTenants(),
        ]);
    }

    private function checkCallingUser($user)
    {
        if (is_null($user)) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'User tidak ditemukan',
                ],
            ]);
        }

        // block suspicious user :
        // 1. Gerobak.id (325904) : suspicious for data scraping.
        // 2. Bony Ekalaya (472522, 307528) : suspicious for scaming other kost resident.
        // 3. Dian Ayu Anggraini (352263) : scam owner and other kost resident
        $suspiciousUserIds = [325904, 472522, 307528, 352263];
        if (in_array($user->id, $suspiciousUserIds)) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Pesan gagal dikirim',
                ],
            ]);
        }

        return null;
    }

    private function checkDailyQuestionLimit($user, $songId)
    {
        // Check chat question room limit
        $isDailyQuestionRoomLimitExceeded = HousePropertyCall::getIsDailyQuestionRoomLimitExceeded($user, Carbon::today(), $songId);
        if ($isDailyQuestionRoomLimitExceeded) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Maaf, Anda sudah mencapai batas pertanyaan chat untuk villa/kontrakan berbeda hari ini. Silakan coba lagi besok.',
                ]
            ]);
        }

        // Check chat question message limit
        $isDailyQuestionLimitExceeded = HousePropertyCall::getIsDailyQuestionLimitExceeded($user, Carbon::today());
        if ($isDailyQuestionLimitExceeded) {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Maaf, Anda sudah mencapai batas pertanyaan chat untuk hari ini. Silakan coba lagi besok.',
                ]
            ]);
        }

        return null;
    }

    public function postChatReply(Request $request, $songId)
    {
        $house = $this->repository->findByField('song_id', $songId);
        if (count($house) == 0) {
            return Api::responseData([
                'status' => false,
                'meta' => ['message' => 'Gagal memulai chat']
            ]);
        }

        $channel = (!empty($request->get('group_channel_url')) ? $request->get('group_channel_url') : $request->get('group_id'));
        if (is_null($channel) || is_null($request->get('admin_id')) || is_null($request->get('call_id')) || is_null($request->get('id_question'))) {
            return Api::responseData([
                'status' => false,
                'meta' => ['message' => 'There are empty required param']
            ]);
        }
        $replyData = [
            'group_id' => $channel,
            'note' => $request->get('note'),
            'admin_id' => $request->get('admin_id'),
            'call_id' => $request->get('call_id'),
            'question_id' => $request->get('id_question')
        ];
        
        $response = $this->repository->autoReplyChat($house[0], $replyData);
        return Api::responseData([
            'status' => true,
            'success' => true,
            'response' => $response,
        ]);
    }
}
