<?php
namespace App\Http\Controllers\Api;

use App\Libraries\SMSLibrary;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Http\Helpers\ApiResponse as Api;
use App\Http\Helpers\ApiHelper;
use App\Entities\Activity\ActivationCode;
use App\Entities\Activity\ActivationCodeType;
use App\Entities\Owner\ActivityType;
use App\User;
use App\Entities\User\UserVerificationAccount;
use App\Entities\Device\UserDevice;
use App\Entities\User\UserLoginTracker;
use App\Entities\Notif\SettingNotif;
use Illuminate\Support\Facades\Hash;
use Jenssegers\Agent\Agent;
use App\Entities\Activity\Tracking;
use Validator;
use Carbon\Carbon;
use App\Entities\User\LinkCode;
use App\Entities\Mamipay\MamipayUserDevice;
use App\Http\Helpers\OAuthHelper;
use Auth;
use Illuminate\Support\Facades\Session;
use App\Http\Helpers\PhoneNumberHelper;
use App\Http\Helpers\RegexHelper;
use App\Rules\GoogleRecaptcha;
use App\Entities\Activity\TrackingPlatforms;
use App\Entities\User\SocialAccountType;
use App\Jobs\Owner\MarkActivity;
use App\Jobs\MoEngage\SendMoEngageOwnerGoldPlusStatus;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Exception;

class AuthController extends BaseController
{

    public const NOT_LOGIN_STATE = 'not_login';

    public const DEFAULT_USER_ID_NOT_LOGIN = 0;

    public function postDeviceRegister(Request $request)
    {
        $deviceDetail = array(
            'identifier'            => ApiHelper::removeEmoji($request->input('device_identifier')),
            'uuid'                  => $request->input('device_uuid'),
            'platform'              => $request->input('device_platform'),
            'platform_version_code' => $request->input('device_platform_version_code'),
            'model'                 => $request->input('device_model'),
            'email'                 => $request->input('device_email'),
            'app_version_code'      => $request->input('app_version_code'),
        );

        // Check identifier already registered
        $userDevice = UserDevice::where('identifier', $deviceDetail['identifier'])->orderBy('created_at', 'desc')->first();
        if (is_null($userDevice)) {
            $userDevice = UserDevice::register($deviceDetail);
        } else {
            $userDevice->uuid = $deviceDetail['uuid'];
            $userDevice->platform = $deviceDetail['platform'];
            $userDevice->platform_version_code = $deviceDetail['platform_version_code'];
            $userDevice->model = $deviceDetail['model'];
            $userDevice->email = $deviceDetail['email'];
            $userDevice->app_version_code = $deviceDetail['app_version_code'];
            $userDevice->update();
        }

        return Api::responseData(
            array('register' => $userDevice)
        );
    }

    public function postSocial(Request $request)
    {
        ApiHelper::isValidInput($request->all(), array(
                'social_token'                 => 'required',
                'social_id'                    => 'required',
                'social_type'                  => SocialAccountType::getValidationFilterForRequiredIn(),
            ));

        /**
         * Only when Register
         * Store it to User Email for Later to Be Updated in User Profile
         */
        $userDetail = array(
            'phone_number'  => $request->input('phone_number'),
            'email'         => $request->input('email'),
            'age'           => $request->input('age', '20'),
            'gender'        => $request->input('gender'),
            'fb_location'   => $request->input('fb_location', ''),
            'fb_education'  => $request->input('fb_education', '')
        );

        $deviceDetail = array(
            'identifier'            => $request->input('device_identifier'),
            'uuid'                  => $request->input('device_uuid'),
            'platform'              => $request->input('device_platform'),
            'platform_version_code' => $request->input('device_platform_version_code'),
            'model'                 => $request->input('device_model'),
            'email'                 => $request->input('device_email'),
            'app_version_code'      => $request->input('app_version_code'),
        );

        $authSocial = \DB::transaction(function() use ($userDetail, $deviceDetail, $request)
        {
            $authSocial = array(
                'type'  => $request->input('social_type'),
                'token' => $request->input('social_token'),
                'id'    => $request->input('social_id'),
                'name'  => $request->input('social_name')
            );

            return User::authSocial($authSocial, $userDetail, $deviceDetail);
        });

        return Api::responseData($authSocial);
    }

    public function postLogout()
    {
        if ($this->device() == null) {
           return Api::responseData(['success' => false]);
        } else {
           $userDevice = $this->device();
        }

        if ($userDevice->user_id == 0) {
            Api::responseData(['success' => false]);
        }

        if(!is_null($this->user())) {
            $user = $this->user();
            $user->last_login = null;
            $user->save();
        }

        $userDevice->user_id        = self::DEFAULT_USER_ID_NOT_LOGIN;
        $userDevice->login_status   = self::NOT_LOGIN_STATE;

        $this->handleLogoutOfMamipayUserDevice($userDevice->device_token);

        return Api::responseData(['success' => $userDevice->save()]);
    }

    /**
     * Handle logout flow with table mamipay_user_device
     * 
     * @param string $deviceToken
     * 
     * @return void
     */
    private function handleLogoutOfMamipayUserDevice(string $deviceToken): void
    {
        MamipayUserDevice::where('device_token', $deviceToken)->update(
            [
                'user_id'       => self::DEFAULT_USER_ID_NOT_LOGIN,
                'login_status'  => self::NOT_LOGIN_STATE,
            ]
        );
    }

    public function getUser(Request $request)
    {
        $user = array(
            'user_id'   => $this->user()->id,
            'name'      => $this->user()->name
            );

        return Api::responseData(compact('user'));
    }

    /**
     * This function below is for the purpose of
     * Owner authentication
     */

    /**
     * This API is used to verify the phone number of the user
     * which will send verification through an short message
     * 
     * Owner registration feature was changed and this method might have been deprecated, please remove in the future
     * 
     * @param Request $request
     * @return mixed
     * @deprecated
     * @see App\Http\Controllers\Api\OwnerController@postVerification
     */
    public function postVerifyOwner(Request $request) {
        // ApiHelper::isValidInput(
        //     $request->all(),
        //     array(
        //         'phone_number'                 => 'required',
        //         'password'                     => 'required',
        //         'is_forget'                    => 'required'
        //     )
        // );
        $password = "required|min:6";
        if ($request->filled('password') AND $request->input('password') == 'false') {
            $password = "nullable";
        }
        $validator = Validator::make($request->all(),
            [
                'phone_number'      => 'required|min:8|max:14|regex:'.RegexHelper::phoneNumber(),
                'password'          => $password,
                'is_forget'         => 'required'
            ], 
            [
                'phone_number.required' => 'No. Handphone harus diisi',
                'phone_number.regex' => 'Format No. Handphone tidak valid (08xxxxxxxx)',
                'password.required' => 'Password harus diisi',
                'password.min' => 'Password minimal :min karakter',
                'is_forget.required' => 'Parameter is_forget harus diisi'
            ]);

        $validator->after(function($validator) use ($request) {
            if($request->get('phone_number') != '') {
                $lastCodeRequest = ActivationCode::where('phone_number', $request->get('phone_number'))
                                                ->orderBy('created_at', 'desc')
                                                ->first();

                if($lastCodeRequest) {
                    if(Carbon::parse($lastCodeRequest->created_at)->diffInMinutes(Carbon::now(), false) <= 0) {
                        $validator->errors()->add('phone_number', 'Kode verifikasi hanya bisa diminta tiap 3 menit sekali');
                    }
                }

                $isTenant = User::where('phone_number', $request->get('phone_number'))->where('is_owner', 'false')->exists();
                if ($isTenant) {
                    $validator->errors()->add('phone_number', __('api.auth.owner.phone_number.legacy.forget_password'));
                }
            }
        });

        if($validator->fails()) {
            return Api::responseData([
                'status'=>false,
                'meta' => [
                    'message'=>implode(', ', $validator->errors()->all())
                ]
            ]);
        }
        
        $phone = preg_replace(RegexHelper::numericOnly(), "", $request->get('phone_number'));

        $userDetail = array(
            'phone_number'    => $phone,
            'password'        => $request->get('password')
        );

        // check phone if it is registered
        $isRegistered = User::isRegisteredOwner($userDetail['phone_number']);

        $verificationCode = ApiHelper::random_string('alnum','4');

        $isForget = $request->get('is_forget');
        // set message for forgot password and verify
        $message = "<#> ";
        if ($isForget == true) {
            $codeFor = "forgot_password";
        } else {
            $codeFor = "register";
        }

        $agent = new Agent();

        $messageToSend = $message . $verificationCode ." adalah kode verifikasi Mamikos Anda.". "\n\nb0lUOxG7rtF";
        $sendStatus = null;

        //if (($userDetail && $isForget) || ($userDetail && $isRegistered)) {
        if ($isForget && $isRegistered) {
            $smsLibrary = \App::make(SMSLibrary::class);
            $sendStatus = $smsLibrary->smsVerification($userDetail['phone_number'], $messageToSend);
        }

        if ($isForget && $isRegistered && $sendStatus == true) {
            $ActivationCode = array(
                "phone_number" => $request->get('phone_number'),
                "device"       => Tracking::checkDevice($agent),
                "code"         => $verificationCode,
                "for"          => $codeFor,
                'code_hashed'  => md5($verificationCode),
                'expired_at'   => Carbon::now()->addHours(2)->format('Y-m-d H:i:s')
            );

            ActivationCode::create($ActivationCode);

            $data = array(
                'code'  => md5($verificationCode),
                'unique' => $verificationCode,
                'is_registered' => $isRegistered
            );

            return Api::responseData($data);
        } elseif($isRegistered) {
            $data = array(
                'code'  => '',
                'is_registered' => $isRegistered
            );

            return Api::responseData($data);
        } else {
            return Api::responseData(["meta" => ["message" => "Nomer anda tidak terdaftar sebagai pemilik di Mamikos"],
                                "code" => "",
                                "message" => "Nomer anda tidak terdaftar sebagai pemilik di Mamikos",
                                "is_registered" => false,
                                "status" => false]);
        }
    }


    /**
     *  This function handles owner registration via web
     *
     *  @deprecated
     */
    public function postRegisterOwnerViaWeb(Request $request)
    {
        return $this->postRegisterOwner($request, false);
    }

    /**
     *  Return null if $email is default ios device email
     * 
     *  In the legacy apps, iOS could not retrieve device_email and use a default email instead
     * 
     *  @param str $email Email to check
     *  @return mixed
     */
    private function handleIosDefaultEmail(str $email)
    {
        if ($email === User::IOS_DEFAULT_EMAIL) {
            return null;
        }

        return $email;
    }

    /**
    * This function handles owner registration.
    *
    * This function also accept query string v2, that determines whether we need validate verification code
    * via backend or not
    *
    * Owner registration feature was changed and this method might have been deprecated, please remove in the future.
    * For owner registration please see: AuthController@autoRegisterOwner
    * For owner phone number verification please see: OwnerController@postStoreVerification
    *
    * @param Request        $request
    * @param int            $viaDevice  Default to true. If it's called via web, then this param should be false.
    * @deprecated
    * @see App\Http\Controllers\Api\OwnerController@postStoreVerification
    * @see App\Http\Controllers\Api\AuthController@autoRegisterOwner

    **/
    public function postRegisterOwner(Request $request, $viaDevice = true) {
        // ApiHelper::isValidInput(
        //     $request->all(),
        //     array(
        //         'phone_number'                 => 'required',
        //         'password'                     => 'required'
        //     )
        // );

        $validationMessages = [
            'phone_number.required'=>'No. Handphone harus diisi',
            'phone_number.regex'=>'Format No. Handphone tidak valid (08xxxxxxxx)',
            'password.required'=>'Password harus diisi',
            'password.min'=>'Password minimal :min karakter',
            'verification_code.required' => 'Kode Verifikasi harus diisi',
            'verification_code.size' => 'Kode Verifikasi harus :size karakter'
        ];

        $validationRules = [
            'phone_number' => 'required|regex:/^(08[1-9]\d{7,10})$/',
            'password' => 'required|min:6',
        ];

        if($request->filled('verification_version') && $request->get('verification_version') == 2)  {
            $validationRules['verification_code'] = 'required|size:4';
        }

        $validator = Validator::make($request->all(), $validationRules, $validationMessages);

        if($validator->fails()) {
            return Api::responseData([
                'status'=>false,
                'meta' => [
                    'message'=>implode(', ', $validator->errors()->all())
                ]
            ]);
        }

        // second validation (code validation)
        if($request->filled('verification_version') && $request->get('verification_version') == 2) {
            $activationCodes = ActivationCode::where('phone_number', $request->get('phone_number'))
                                            ->whereRaw('expired_at > NOW()')
                                            ->where('for', 'register')
                                            ->get();

            if(count($activationCodes) > 0) {
                $match = false;
                foreach($activationCodes as $activationCode) {
                    if($activationCode->code_hashed == md5($request->get('verification_code'))) {
                        $match = true;
                        break;
                    }
                }

                if(!$match) {
                    return Api::responseData([
                        'status'=>false,
                        'meta' => [
                            'message'=> 'Kode verifikasi Anda tidak valid'
                        ]
                    ]);
                } else {
                    // make all available codes expired
                    ActivationCode::where('phone_number', $request->get('phone_number'))
                                            ->whereRaw('expired_at > NOW()')
                                            ->where('for', 'register')
                                            ->update(['expired_at' => Carbon::now()]);
                }

            } else {
                return Api::responseData([
                    'status'=>false,
                    'meta' => [
                        'message'=> 'Kode verifikasi Anda tidak valid'
                    ]
                ]);
            }
        }
        
        $phone = preg_replace(RegexHelper::numericOnly(), "", $request->get('phone_number'));

        $userDetail = array(
            'phone_number'    => $phone,
            'password'        => $request->get('password'),
            'name'            => $request->get('phone_number'),
            'email'           => $this->handleIosDefaultEmail($request->get('device_email')),
            'phone_country_code'    => null,
            'photo_id'        => null,
        );

        if($viaDevice) {
            $deviceDetail = array(
                'identifier'            => $request->get('device_identifier'),
                'uuid'                  => $request->get('device_uuid'),
                'platform'              => $request->get('device_platform'),
                'platform_version_code' => $request->get('device_platform_version_code'),
                'model'                 => $request->get('device_model'),
                'email'                 => $request->get('device_email'),
                'app_version_code'      => $request->get('app_version_code'),
            );
        } else {
            $deviceDetail = null;
        }

        $data = User::registerOwner($userDetail,$deviceDetail);

        if ($data != null) {
            return Api::responseData($data);
        } else {
            return Api::responseData(
                ['message'=>'This user is already registered as owner'], 
                false, 
                false, 
                200, 
                'This user is already registered as owner');
        }
    }

    /**
    * This function handles owner login via web
    */
    public function postLoginOwnerViaWeb(Request $request)
    {
        return $this->postLoginOwner($request, false);
    }

    /**
    * This function handles owner login in general
    *
    * @param Request        $request
    * @param bool           $viaDevice      Default to true. If it's called via web, this should be set to false.
    */
    public function postLoginOwner(Request $request, $viaDevice = true)
    {
        $validator = Validator::make(
            $request->all(), 
            [
                'phone_number'  => 'sometimes|required|string',
                'password'      => 'required|string'
            ], 
            [
                'phone_number.required' => __('api.input.phone_number.required'),
                'phone_number.string'   => __('api.input.phone_number.invalid'),
                'password.required'     => __('api.input.password.required'),
                'password.string'       => __('api.input.password.string'),
            ]
        );

        if ($validator->fails()) {
            return ApiHelper::validationError($validator->errors()->first());
        }

        // In the new flow, owner could login using email OR phone number.
        if ($request->filled('phone_number')) {
            $user = User::where('is_owner', 'true')->where('phone_number', $request->get('phone_number'))->first();
            $message = __('api.auth.owner.phone_number.invalid');
        }
        else {
            $user = User::where('is_owner', 'true')->where('email', $request->get('device_email'))->first();
            $message = __('api.auth.owner.email.invalid');
        }

        // User does not exist
        if (is_null($user)) {
            return Api::responseData([ 'message' => $message ], false, false, 200, $message);
        }

        // TODO: Remove this if checks if old flow is no longer used (all owner has been verified)
        // Check whether the user is owner registering using old or new flow
        if ($user && $user->user_verification_account()->exists() && $user->user_verification_account->is_verify_owner) {
            // Owner registered using the new flow
            return $this->newOwnerLoginFlow($request, $user, $viaDevice);
        }
        else {
            // Owner registered using the old flow
            return $this->oldOwnerLoginFlow($request, $viaDevice);
        }
    }

    /**
     *  Authenticate owner using the new flow for MamikosWeb >= 1.7.7
     * 
     *  @param Request $reques
     *  @param User $user User to log in
     *  @param bool $viaDevice Whether the request is coming from app/web
     */
    private function newOwnerLoginFlow(Request $request, User $rowUser, $viaDevice): JsonResponse
    {
        // Prevent user with unverified phone number from logging in.
        if ($rowUser->user_verification_account->is_verify_phone_number === false) {
            $message = __('api.auth.owner.phone_number.unverified');
            return Api::responseData([ 'message' => $message], false, false, 200, $message);
        }

        if (!Hash::check($request->password, $rowUser->password)) {
            if ($request->filled('phone_number')) {
                $message = __('api.auth.owner.phone_number.invalid');
            }
            else {
                $message = __('api.auth.owner.email.invalid');
            }

            return Api::responseData([ 'message' => $message], false, false, 200, $message);
        }

        $userDeviceId = null;

        if($viaDevice) {
            $userDeviceId = $this->updateUserDevice($request, $rowUser);
        }

        $user = Auth::loginUsingId($rowUser->id, true);

        if ($user->isOwner()) {
            MarkActivity::dispatch($user->id, ActivityType::TYPE_LOGIN);
            SendMoEngageOwnerGoldPlusStatus::dispatch($user->id);
        }

        // track login user
        $agent = new Agent();
        $deviceType = Tracking::checkDevice($agent);
        UserLoginTracker::saveTracker($user, $deviceType, $userDeviceId, $agent->device());

        $user = Session::get('api.user');

        $token = app()->make(OAuthHelper::class)->issueTokenFor($rowUser);

        $response = Api::responseData([
            'login' => [
                'status' => 'login',
                'is_device_last_login' => true,
                'user_id' => $rowUser->id,
                'user_token' => $rowUser->getAttributeValue('token'),
                'gender' => $rowUser->gender,
                'chat_setting' => SettingNotif::getSingleUserNotificationSetting($rowUser->id, 'app_chat', $rowUser->is_owner == 'true' ? true : false),
                'is_verify' => ($rowUser->is_verify == 1),
                'is_using_old_flow' => true,
                'registered_at' => $rowUser->created_at->format('Y-m-d\\TH:i:s'),
                'redirect_path' => $this->getRedirectPath(),
                'oauth' => $token
            ]
        ]);
        return $response;
    }

    /**
     * getRedirectPath gets redirect URL path from session
     // * with DEFAULT_LOGIN_REDIRECT_PATH as default value
     *
     * @return string
     *
     */
    private function getRedirectPath() : ?string
    {
        if (session()->has('url.intended')) {
            $redirectURL = session()->get('url.intended');
            session()->forget('url.intended');
            return $redirectURL;
        }
        return config('owner.dashboard_url');
    }

    private function updateUserDevice(Request $request, User $rowUser)
    {
        $deviceDetail = [
            'identifier'            => $request->get('device_identifier'),
            'uuid'                  => $request->get('device_uuid'),
            'platform'              => $request->get('device_platform'),
            'platform_version_code' => $request->get('device_platform_version_code'),
            'model'                 => $request->get('device_model'),
            'email'                 => $request->get('device_email'),
            'app_version_code'      => $request->get('app_version_code'),
        ];

        $sessionUserDeviceId = empty(ApiHelper::activeDeviceId()) ? null : ApiHelper::activeDeviceId();
        $userDeviceId = UserDevice::insertOrUpdate($deviceDetail, $rowUser->id, $sessionUserDeviceId);

        UserDevice::isLastLogin(
            $rowUser->id,
            $deviceDetail['identifier'],
            $deviceDetail['platform'],
            $deviceDetail['uuid']
        );
    }

    /**
     *  Perform owner login for the old flow before MamikosWeb 1.7.7
     * 
     *  TODO: Remove this when old owner login flow is no longer used
     * 
     *  @param Request $request
     *  @param bool $viaDevice Whether request is coming from device/web
     * 
     *  @deprecated MamikosWeb 1.7.7
     */
    private function oldOwnerLoginFlow(Request $request, bool $viaDevice)
    {
        $phoneNumber = preg_replace(RegexHelper::numericOnly(), "", $request->get('phone_number'));
        $password    = $request->get('password');
        $email       = $request->get('device_email');

        if($viaDevice) {
            $deviceDetail = array(
                'identifier'            => $request->get('device_identifier'),
                'uuid'                  => $request->get('device_uuid'),
                'platform'              => $request->get('device_platform'),
                'platform_version_code' => $request->get('device_platform_version_code'),
                'model'                 => $request->get('device_model'),
                'email'                 => $request->get('device_email'),
                'app_version_code'      => $request->get('app_version_code'),
            );
        } else {
            $deviceDetail = null;
        }

        $user = User::authenticateOwner($phoneNumber, $email, $password, $deviceDetail);

        if ($user['login']['status'] == 'login') {
            //Add redirect_path on old owner login flow
            $user['login']['redirect_path'] = $this->getRedirectPath();
            
            $response   = Api::responseData($user);
            $token      = $user['login']['oauth'];

            if (isset($user['login']['user_id'])) {
                SendMoEngageOwnerGoldPlusStatus::dispatch($user['login']['user_id']);
            }

            return $response;
        } else {
            return Api::responseData(
                [ 'message' => $user['login']['status'] ],
                false,
                false,
                200, 
                $user['login']['status']
            );
        }
    }

    private function getOwnerRegistrationRules(): array
    {
        return [
            'email' => 'required|email|unique:user,email',
            'name' => 'required|string|min:3|max:255|regex:' . RegexHelper::name(),
            'password' => 'required|string',
            'phone_number' => 'required|min:8|max:14|unique:user,phone_number|regex:' . RegexHelper::phoneNumber(),
            'verification_code' => 'required|alpha_num|size:4'
        ];
    }

    private function getOwnerRegistrationMessages(): array
    {
        return [
            'email.required' => __('api.input.email.required'),
            'email.email' => __('api.input.email.email'),
            'email.unique' => __('api.input.email.unique'),
            'name.required' => __('api.input.name.required'),
            'name.alpha' => __('api.input.name.alpha'),
            'name.min' => __('api.input.name.min'),
            'name.max' => __('api.input.name.max'),
            'name.regex' => __('api.input.name.alpha'),
            'password.required' => __('api.input.password.required'),
            'password.string' => __('api.input.password.string'),
            'password.min' => __('api.input.password.min'),
            'password.max' => __('api.input.password.max'),
            'phone_number.required' => __('api.input.phone_number.required'),
            'phone_number.min' => __('api.input.phone_number.min'),
            'phone_number.max' => __('api.input.phone_number.max'),
            'phone_number.unique' => __('api.input.phone_number.unique'),
            'phone_number.regex' => __('api.input.phone_number.regex'),
            'phone_number.numeric' => __('api.input.phone_number.invalid'),
            'verification_code.required'     => __('api.input.code.required'),
            'verification_code.size'         => __('api.owner.verification.code.invalid'),
            'verification_code.alpha_num'    => __('api.owner.verification.code.invalid'),
        ];
    }

    /**
     *  Register owner using the new flow
     *
     *  In this new flow. Email and phone number must now be unique.
     *  Accordingly OTP verification code is submitted along with registration information.
     *  Due to the way mobile apps process the request during registration.
     *
     *  @param Request $request
     */
    public function registerOwner(Request $request)
    {
        $rules = $this->getOwnerRegistrationRules();

        if ($request->filled('_token')) {
            $rules += ['g-recaptcha-response' => ['required', new GoogleRecaptcha()]];
        }

        // We will give a chance to use `device_email` for `email` only for Android 3.4.3 and 3.4.4
        // Android 3.4.3 will be the first and last version to use this method without `email`
        if ($request->get('device_platform') === TrackingPlatforms::Android)
        {
            // if `email` is not input, we can give a chance that device_email can be used for that.
            if (empty($request->get('email')))
            {
                $request['email'] = $request->get('device_email');
            }
        }

        $validator = Validator::make($request->all(), $rules, $this->getOwnerRegistrationMessages());

        $activationCode = ActivationCode::where([
            ['code', '=', $request->get('verification_code')],
            ['expired_at', '>', Carbon::now()],
            ['phone_number', '=', $request->get('phone_number')],
            ['for', '=', ActivationCodeType::OWNER_VERIFICATION]
        ])->first();
        
        $validator->after(function ($validator) use ($activationCode) {
            if (is_null($activationCode)) {
                $validator->errors()->add('verification_code', __('api.owner.verification.code.invalid'));
            }
        });

        if ($validator->fails()) {
            $messages = [];

            foreach ($validator->errors()->getMessages() as $key => $message) {
                $messages[$key] = $message[0];
            }

            return ApiHelper::responseData([
                'status' => false,
                'messages' => $messages
            ]);
        }

        $userDetail = [
            'phone_number'          => $request->get('phone_number'),
            'name'                  => $request->get('name'),
            'email'                 => $request->get('email'),
            'phone_country_code'    => null,
            'photo_id'              => null,
            'password'              => $request->get('password'),
            'is_verified'           => 1 // TODO: Remove this when owner phone and email verification has been moved to UserVerificationAccount
        ];

        // If CSRF token (specific to web) is not filled then retrieve user device information.
        if(!$request->filled('_token')) {
            $deviceDetail = [
                'identifier'            => $request->get('device_identifier'),
                'uuid'                  => $request->get('device_uuid'),
                'platform'              => $request->get('device_platform'),
                'platform_version_code' => $request->get('device_platform_version_code'),
                'model'                 => $request->get('device_model'),
                'email'                 => $request->get('device_email'),
                'app_version_code'      => $request->get('app_version_code'),
            ];
        } else {
            $deviceDetail = null;
        }

        $user = User::registerOwner($userDetail, $deviceDetail);

        // From MamikosWeb 1.7.7 we move owner verification status to UserVerificationAccount
        $userVerification = new UserVerificationAccount();
        $userVerification->is_verify_phone_number = true;
        $userVerification->is_verify_owner = true;

        User::find($user['user_id'])->user_verification_account()->save($userVerification);

        if (is_null($user)) {
            return Api::responseData([
                'status' => false,
                'message' => ['Gagal menambahkan user']
            ]);
        }

        ActivationCode::deleteLastCode($request->get('phone_number'));

        return Api::responseData($user);
    }

    /**
     * Function for check is invalid email
     * 
     * @param string $email
     * 
     * @return boolean
     */
    private function isInvalidEmail($email): bool
    {
       return (bool) preg_match(RegexHelper::validEmail(), $email) ? false : true;
    }


    /**
     *  Auto Register user only using phone number
     * 
     *  This flow is used by the "Old Flow" Owner Registration.
     *
     * @param Request $request          Laravel Request object
     * @return Array                    API response
     * @deprecated
     */
    public function autoRegisterOwner(Request $request)
    {
        $requestValidation = [];
        $requestValidation['phone_number'] = 'required|regex:'.RegexHelper::phoneNumberFormat();
        if ($request->filled('version') && $request->get('version') == 2) {
            $requestValidation['email'] = 'nullable';
            $requestValidation['password'] = 'required|min:5';
            $requestValidation['name'] = 'required';
        }

        $validatorMessages = [
            'phone_number.required' => __('api.input.phone_number.required'),
            'phone_number.regex' => __('api.input.phone_number.regex'),
            'password.required' => __('api.input.password.required'),
            'password.min' => __('api.input.password.min'),
            'name.required' => __('api.input.name.required')
        ];

        $validator = Validator::make($request->all(), $requestValidation, $validatorMessages);

        if ($validator->fails()) {
            return Api::responseData([
                    'status' => false,
                    'messages' => $validator->errors()->all()
                ]);
        }

        $email = $request->get('device_email');

        if ($this->isInvalidEmail($email) && !empty($email)) {
            $validator->errors()->add('message', __('api.input.email.email'));

            return Api::responseData([
                'status' => false,
                'messages' => $validator->errors()->all()
            ]);
        }

        $phoneNumber = PhoneNumberHelper::localize($request->get('phone_number'));
        $isRegistered = User::isRegisteredOwner($phoneNumber);

        $validator->after(function ($validator) use ($isRegistered) {
            if ($isRegistered) {
                $validator->errors()->add('message', 'Nomor HP sudah terdaftar');
            }
        });
        
        if ($validator->fails()) {
            return Api::responseData([
                    'status' => false,
                    'messages' => $validator->errors()->all()
                ]);
        }

        $userDetail = [
            'phone_number'    => $phoneNumber,
            'name'            => $request->get('name'),
            'email'           => null,
            'phone_country_code' => null,
            'photo_id'        => null,
            'password' => $request->get('password'),
            'is_verified'     => '0'
        ];
        
        if(!$request->filled('_token')) {
            $deviceDetail = [
                'identifier'            => $request->get('device_identifier'),
                'uuid'                  => $request->get('device_uuid'),
                'platform'              => $request->get('device_platform'),
                'platform_version_code' => $request->get('device_platform_version_code'),
                'model'                 => $request->get('device_model'),
                'email'                 => $email,
                'app_version_code'      => $request->get('app_version_code'),
            ];
        } else {
            $deviceDetail = null;
        }

        $registeredUser = User::registerOwner($userDetail,$deviceDetail);

        if (is_null($registeredUser)) {
            $registeredUser = [
                    'status' => false,
                    'message' => ['Gagal menambahkan user']
                ];
        }

        return Api::responseData($registeredUser);
    }

    /**
     *  Store owner password after registration
     *
     *  @deprecated
     */
    public function putPassword(Request $request)
    {
        // ApiHelper::isValidInput(
        //     $request->all(),
        //     array(
        //         'phone_number' => 'required',
        //         'password' => 'required',
        //     )
        // );


        $validationMessages = [
            'phone_number.required'=>'No. Handphone harus diisi',
            'phone_number.regex'=>'Format No. Handphone tidak valid (08xxxxxxxx)',
            'password.required'=>'Password harus diisi',
            'password.min'=>'Password minimal :min karakter',
            'verification_code.required' => 'Kode Verifikasi harus diisi',
            'verification_code.size' => 'Kode Verifikasi harus :size karakter'
        ];

        $validationRules = [
            'phone_number' => 'required|regex:/^(08[1-9]\d{7,10})$/',
            'password' => 'required|min:6',
        ];

        if($request->filled('verification_version') && $request->get('verification_version') == 2)  {
            $validationRules['verification_code'] = 'required|size:4';
        }

        $validator = Validator::make($request->all(), $validationRules, $validationMessages);

        if($validator->fails()) {
            return Api::responseData([
                'status'=>false,
                'meta' => [
                    'message'=>implode(', ', $validator->errors()->all())
                ]
            ]);
        }

        // second validation (code validation)
        if($request->filled('verification_version') && $request->get('verification_version') == 2) {
            $activationCodes = ActivationCode::where('phone_number', $request->get('phone_number'))
                                            ->whereRaw('expired_at > NOW()')
                                            ->where('for', 'forgot_password')
                                            ->get();

            if(count($activationCodes) > 0) {
                $match = false;
                foreach($activationCodes as $activationCode) {
                    if($activationCode->code_hashed == md5($request->get('verification_code'))) {
                        $match = true;
                        break;
                    }
                }

                if(!$match) {
                    return Api::responseData([
                        'status'=>false,
                        'meta' => [
                            'message'=> 'Kode verifikasi Anda tidak valid'
                        ]
                    ]);
                } else {
                    // make all available codes expired
                    ActivationCode::where('phone_number', $request->get('phone_number'))
                                            ->whereRaw('expired_at > NOW()')
                                            ->where('for', 'forgot_password')
                                            ->update(['expired_at' => Carbon::now()]);
                }

            } else {
                return Api::responseData([
                    'status'=>false,
                    'meta' => [
                        'message'=> 'Kode verifikasi Anda tidak valid'
                    ]
                ]);
            }
        }

        $phone = preg_replace(RegexHelper::numericOnly(), "", $request->get('phone_number'));

        // just allow owner to change password
        $user = User::where('phone_number', $phone)
                        ->where('is_owner', 'true')
                        ->first();

        if(!$user) {
            return Api::responseData(
                ['status'=>false, 'message'=>'User tidak ditemukan']
            );
        }

        $user->password = Hash::make($request->get('password'));
        $user->save();

        return ApiHelper::responseData(['data' => $user]);
    }

    public function postPassword(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'phone_number' => 'nullable|regex:/^(08[1-9]\d{7,10})$/',
            'strcode'  => 'required',
            'n_pass'   => 'required|min:6',
            'v_pass'   => 'required|min:6' 
        ], [
            'phone_number.regex' => 'Format No HP tidak valid (08xxxxxxxx)',
            'strcode.required' => 'Field tidak lengkap',
            'n_pass.required' => 'Password Baru harus diisi',
            'n_pass.min' => 'Password Baru minimal :min karakter',
            'v_pass.required' => 'Konfirmasi Password harus diisi',
            'v_pass.min' => 'Konfirmasi Password minimal :min karakter'
        ]);

        /* check valid code */
        $code = LinkCode::where('link', $request->input('strcode'))->first();

        $validator->after(function ($validator) use($request, $code) {
            if ($request->input('n_pass') != $request->input('v_pass')) {
                $validator->errors()->add('n_pass', 'Password baru dan konfirmasi password baru berbeda');
            }

            if(count($code) == 0) {
                $validator->errors()->add('field', 'Something is wrong with this field!');
            }

            if($request->phone_number != '' && !is_null($request->phone_number)) {
                $user = User::where('is_owner', 'true')
                            ->where('phone_number', $request->phone_number)
                            ->first();

                if($user) {
                    $validator->errors()->add('phone_number', 'No. HP sudah terdaftar');
                }
            }
        });

        if ($validator->fails()) {
            //return redirect()->back()->withErrors($validator)->withInput();
            $response  = [
                "status" => false,
                'meta' => [
                    'message' => implode(', ', $validator->errors()->all())
                ]
            ];
            return Api::responseData($response);
        }

        $code->is_active = 0;
        $code->save();

        /* update Password */
        $user           = User::find($code->user_id);
        $user->token    = Hash::make($request->input('n_pass'));
        $user->password = Hash::make($request->input('n_pass'));

        if($request->phone_number != '' && !is_null($request->phone_number)) {
            $user->phone_number = $request->phone_number;
            $user->name = $request->phone_number;
        }

        $user->save();

        Auth::loginUsingId($user->id, true);
        Session::get('api.user');
        
        return Api::responseData(array("status" => true)); 
    }
}
