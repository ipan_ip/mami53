<?php
namespace App\Http\Controllers\Api\SendBird;

use Illuminate\Http\Request;
use App\Http\Requests;
use SendBird;
use Validator;
use App\Http\Helpers\ApiResponse;
use App\Http\Controllers\Api\BaseController;
use Bugsnag;
use Auth;

use App\User;
use App\Entities\Room\Room;
use App\Entities\Consultant\Consultant;
use App\Entities\Activity\ChatAdmin;
use App\Entities\Activity\Call;

class SendBirdController extends BaseController
{
	public function createChannel(Request $request)
	{
		$user = $this->user();

		if (is_null($user)) {
			return ApiResponse::responseData([
				'status' => false,
				'meta' => [
					'message' => 'User tidak ditemukan'
				]
			]);
		}

		$room = Room::where('song_id', $request->room_id)->first();
		if (is_null($room)) {
			return ApiResponse::responseData([
				'status' => false,
				'meta' => [
					'message' => 'Data kost tidak ditemukan'
				]
			]);
		}

		$consultant = Consultant::getAssignedTo($room->id, $room->area_city, $room->is_mamirooms);
		$chatAdmin = is_null($consultant) ? ChatAdmin::getRandomAdminIdForTenants() : $consultant->user_id;
		$channelName = $user->name.' : '.$room->name;
		$userIds = [$user->id, (int) $chatAdmin];

		$owner = $room->owners->first();
		if (!is_null($owner)) {
			array_push($userIds, $owner->user_id);
		}

		$newChannel = SendBird::createGroupChannelAlwaysNew('', $channelName, $userIds);
		if (is_null($newChannel)) {
			Bugsnag::notifyException(new Exception('Failed create channel - '.$channelName));
			return ApiResponse::responseData([
				'status' => false,
				'meta' => [
					'message' => 'Gagal membuat room chat'
				]
			]);
		}

		$callData = [
			'add_from' => '',
			'phone' => $user->phone_number,
			'email' => $user->email,
			'name' => $user->name,
			'group_channel_url' => $newChannel['channel_url'],
			'chat_admin_id' => (int) $chatAdmin
		];

		Call::store($room, $user, $callData, null);

		return ApiResponse::responseData([
			'status' => true,
			'data' => [
				'group_channel_url' => $newChannel['channel_url'],
				'admin_id' => $chatAdmin,
				'user_id' => $user->id,
			]
		]);
	}

}