<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Helpers\ApiHelper as Api;
use App\Http\Requests;
use App\Entities\Landing\Landing;
use App\Entities\Activity\ViewLandingTemp;

class LandingController extends BaseController
{
    public function show(Request $request, $slug)
    {
        $rowLanding     = Landing::with('tags')->whereSlug($slug)->first();

        if ($rowLanding == null) {
            return Api::responseData(array('landing' => null));
        }

        $landingFilter = array(
            'location' => $rowLanding->location,
            'filters' => $rowLanding->filters
            );

        return Api::responseData(array('landing' => $landingFilter));
    }

    public function viewCount(Request $request, $id)
    {
        // Landing::find($id)->increment('view_count');

        $viewTemp = new ViewLandingTemp;
        $viewTemp->identifier_type = 'landing';
        $viewTemp->identifier = $id;
        $viewTemp->save();

        return Api::responseData([
            'status' => true
        ]);
    }
}
