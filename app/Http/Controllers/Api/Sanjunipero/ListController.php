<?php
namespace App\Http\Controllers\Api\Sanjunipero;

use App\Libraries\CacheHelper;
use App\Repositories\Sanjunipero\RoomRepository as SanjuniperoRoom;
use App\Http\Controllers\Api\BaseController;
use App\Http\Helpers\ApiResponse as Api;
use App\Transformers\Room\ListTransformer;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class ListController extends BaseController
{
    //Laravel 5.7 use minutes as unit of time, 1440minutes = 1 day
    public const CACHE_TIME = 1440;

    private $request, $repo;

    public function __construct(Request $request, SanjuniperoRoom $repository)
    {
        $this->request = $request;
        $this->repo = $repository;
    }

    /**
     * Main function to show list kost
     * 
     * @return array|JsonResponse
     */
    public function index()
    {
        //CREATE CACHE BASED ON REQUEST
        $cacheKey = CacheHelper::getKeyFromQueryString($this->request);
        //CHECKING CACHE
        if (Cache::has($cacheKey)) {
            return Cache::get($cacheKey);
        }

        $offset = !empty($this->request->get('offset')) ? $this->request->get('offset') : 0;
        $limit = !empty($this->request->get('limit')) ? $this->request->get('limit') : 20;
        $sortingField = !empty($this->request->get('sorting_field')) ? $this->request->get('sorting_field') : 'price';
        $sortingDirection = !empty($this->request->get('sorting_direction')) ? $this->request->get('sorting_direction') : 'DESC';
        $rentType = !empty($this->request->get('rent_type')) ? (int) $this->request->get('rent_type') : 2;

        $query = $this->repo->attribute($this->repo->base());

        //CUSTOM FILTERING
        $customQueryString = $this->setCustomFilter();

        //DATA
        $data = $this->repo->show(
                $this->setFilter($query),
                $customQueryString,
                $sortingField,
                $rentType,
                $sortingDirection,
                $offset,
                $limit
            )
            ->map(function($item){
                return (new ListTransformer)->transform($item);
            });
        $roomsCount = $this->repo->getCount(
            $this->setFilter($this->repo->base()),
            $customQueryString
        );

        $response = Api::responseData([
            'limit' => $limit,
            'offset' => $offset,
            'total' => $roomsCount,
            'has-more' => $roomsCount > $limit + ($offset*$limit),
            'rooms' => $data->toArray()
        ]);

        Cache::put($cacheKey, $response, self::CACHE_TIME);

        return $response;
    }

    /**
     * Set query following filter by location, gender, price-range
     * 
     * @param Builder $query
     * 
     * @return Builder
     */
    private function setFilter(Builder $query): Builder
    {
        $genders = !empty($this->request->get('gender')) ? explode(',',$this->request->get('gender')) : [];
        $priceRange = !empty($this->request->get('price_range')) ? $this->request->get('price_range') : '';
        $rentType = !empty($this->request->get('rent_type')) ? $this->request->get('rent_type') : '';

        $query = $this->setLocation($query, $this->request);
        $query = $this->repo->filterByGender($query, $genders);
        $query = $this->repo->filterByPriceRange(
            $query,
            $priceRange,
            $rentType
        );

        return $query;
    }

    /**
     * Set query following custom filter by level, mamirooms, premium, mami-checker, virtual-tour
     * 
     * @return string
     */
    private function setCustomFilter(): string
    {
        $queryArray = [];
        $queryString = '';
        $request = [
            'level_info' => !empty($this->request->get('level_info')) ? $this->request->get('level_info') : null,
            'mamirooms' => ($this->request->get('mamirooms') == 'true') ? true : false,
            'include_promoted' => ($this->request->get('include_promoted') == 'true') ? true : false,
            'mamichecker' => ($this->request->get('mamichecker') == 'true') ? true : false,
            'virtual_tour' => ($this->request->get('virtual_tour') == 'true') ? true : false
        ];

        $queryArray = $this->repo->filtering($request);
        if (! empty($queryArray)) {
            //CONVERT ARRAY TO STRING
            $queryArray = '('.implode(' or ', $queryArray).')';
            //REMOVE WHITESPACE
            $queryString = trim(preg_replace("/\r|\n/", "", $queryArray));
        }

        return $queryString;
    }

    /**
     * Set parameter location
     * 
     * @param Builder $query
     * 
     * @return Builder
     */
    private function setLocation(Builder $query): Builder
    {
        if (
            !is_array($this->request->get('location'))
            && !empty($this->request->get('location'))
        ) {
            $location = explode(',', $this->request->get('location'));
            list($longitude1, $latitude1, $longitude2, $latitude2) = $location;
            $query = $this->repo->filterLocation(
                $query,
                $longitude1,
                $latitude1,
                $longitude2,
                $latitude2
            );
        }

        return $query;
    }
}