<?php
namespace App\Http\Controllers\Api;

use App\Http\Helpers\ApiResponse;
use Illuminate\Http\Request;

use App\Http\Requests;
use Validator;
use App\Entities\Room\Room;
use App\Entities\Room\Element\Report;


class ReportController extends BaseController
{
    protected $validationMessage = [
        'room_id.required'=>'ID kost harus diisi',
        'report_type.required'=>'Tipe laporan harus diisi',
        'report_description.required'=>'Deskripsi laporan harus diisi',
        'report_description.min'=>'Deskripsi laporan minimal :min karakter'
    ];

    public function reportType()
    {
        return ApiResponse::responseData([
            'types' => Report::REPORT_TYPE_LABEL
            ]);
    }

    public function postReport(Request $request, $roomId)
    {
        $user = $this->user();

        if(is_null($user)) {
            return ApiResponse::responseData([
                'status'=>false,
                'meta'=>[
                    'message'=>'Silakan login terlebih dulu'
                ]
            ]);
        }

        $validator = Validator::make($request->all(),
                        [
                            'report_type'=>'required',
                            'report_description'=>'required|min:10',
                        ], $this->validationMessage);

        if($validator->fails()) {
            return ApiResponse::responseData([
                'status'=>false,
                'meta'=>[
                    'message'=>implode('. ', $validator->errors()->all())
                ]
            ]);
        }

        $room = Room::where('song_id', $roomId)->first();

        if(!$room) {
            return ApiResponse::responseData([
                'status'=>false,
                'meta'=>[
                    'message'=>'Kamar tidak ditemukan'
                ]
            ]);
        }

        $reportExist = Report::where('designer_id', $room->id)
                            ->where('user_id', $user->id)
                            ->first();

        if($reportExist) {
            $message = (! is_null($room->apartment_project_id)) ? 
                'Anda sudah memberikan laporan untuk apartment ini.'
                : 'Anda sudah memberikan laporan untuk kos ini.';
            return ApiResponse::responseData([
                'status' => false,
                'meta' => [
                    'message' => $message
                ]
            ]);
        }

        if(is_array($request->get('report_type'))) {
            $first = true;
            $parentId = 0;

            foreach ($request->get('report_type') as $key => $type) {
                $report = new Report;
                if(!$first) {
                    $report->report_parent_id = $parentId;
                }
                $report->designer_id = $room->id;
                $report->user_id = $user->id;
                $report->report_type = $type;
                $report->report_description = $request->get('report_description');
                $report->save();

                if($first) {
                    $parentId = $report->id;
                }

                $first = false;
            }
        } else {
            $report = new Report;
            $report->designer_id = $room->id;
            $report->user_id = $user->id;
            $report->report_type = $request->get('report_type');
            $report->report_description = $request->get('report_description');
            $report->save();
        }
        

        return ApiResponse::responseData([
            'status'=>true
        ]);
    }

}