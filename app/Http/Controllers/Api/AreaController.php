<?php
namespace App\Http\Controllers\Api;

use App\Http\Helpers\ApiHelper;
use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\AreaCreateRequest;
use App\Http\Requests\AreaUpdateRequest;
use App\Repositories\AreaRepository;
use App\Validators\AreaValidator;
use App\Entities\Area\AreaCity;
use Cache;


class AreaController extends BaseController
{

    public function __construct(AreaRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getCampus(Request $request)
    {

        if($request->filled('top')) {
            $repo = $this->repository;
            $topCampus = Cache::remember('top-campus', 60 * 24, function() use ($repo) {
                return $repo->getTopCampus();
            });

            return ApiHelper::responseData(
                $topCampus
            );
        }

        if($request->filled('search')) {
            return ApiHelper::responseData([
                'suggestion' => $this->repository->getSuggestion($request->input('search'), 'campus')
            ]);
        }

        $campus = $this->repository->getCampus();

        return ApiHelper::responseData(compact('campus'));
    }


    public function getArea(Request $request)
    {
        if($request->filled('top')) {
            $repo = $this->repository;
            $topCampus = Cache::remember('top-area', 60 * 24, function() use ($repo) {
                return $repo->getTopArea();
            });

            return ApiHelper::responseData(
                $topCampus
            );
        }

        if($request->filled('search')) {
            return ApiHelper::responseData([
                'suggestion' => $this->repository->getSuggestion($request->input('search'), 'area')
            ]);
        }


        $area = $this->repository->getListArea();

        return ApiHelper::responseData(['campus' => $area]);
    }

    public function getAreaCity()
    {
        $cities = AreaCity::all();

        return ApiHelper::responseData(['cities' => $cities]);
    }

    public function getMrt(Request $request)
    {
        if($request->filled('top')) {
            $repo = $this->repository;
            $topMrt = Cache::remember('top-mrt', 60 * 24, function() use ($repo) {
                return $repo->getTopMrt();
            });

            return ApiHelper::responseData(
                $topMrt
            );
        }

        if($request->filled('search')) {
            return ApiHelper::responseData([
                'suggestion' => $this->repository->getSuggestion($request->input('search'), 'mrt/halte')
            ]);
        }

        $mrt = $this->repository->getListMrt();

        return ApiHelper::responseData(['campus' => $mrt]);
    }

}
