<?php

namespace App\Http\Controllers\Api\Me\GoldPlus;

use App\Entities\Level\KostLevel;
use App\Entities\Property\PropertyContract;
use App\Http\Controllers\Api\BaseController;
use App\Http\Helpers\ApiResponse;
use Illuminate\Http\JsonResponse;
use App\Presenters\Property\PropertyContractPresenter;

class ContractController extends BaseController
{

    /**
     * Get the currently goldplus active contract kost levels.
     * @param PropertyContractPresenter $presenter
     * @return JsonResponse
     */
    public function activeContract(PropertyContractPresenter $presenter): JsonResponse
    {
        $userId = optional($this->user())->id;
        if (is_null($userId)) {
            return ApiResponse::responseError(
                [],
                false,
                401,
                401
            );
        }

        $contract = PropertyContract::with(['property_level'])
            ->where('status', 'active')
            ->whereHas('properties', function ($properties) use ($userId) {
                $properties->where('owner_user_id', $userId);
            })
            ->whereHas('property_level', function ($propertyLevel) {
                $propertyLevel->whereHas('kost_level', function ($kostLevel) {
                    $kostLevel->whereIn('id', KostLevel::getGoldplusLevelIdsByGroups());
                });
            })
            ->first();

        return ApiResponse::responseData(
            $presenter->withType(PropertyContractPresenter::GOLDPLUS_CONTRACT)->present($contract)
        );
    }
}
