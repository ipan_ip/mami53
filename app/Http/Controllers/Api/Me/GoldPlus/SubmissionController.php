<?php
namespace App\Http\Controllers\Api\Me\GoldPlus;

use App\Http\Controllers\Api\BaseController;
use App\Entities\GoldPlus\Submission;
use App\Entities\Room\RoomOwner;
use App\Http\Helpers\ApiResponse;
use Illuminate\Http\JsonResponse;
use App\Entities\Level\KostLevel;
use App\Repositories\Mamipay\InvoiceRepository;
use App\Transformers\GoldPlus\SubmissionStatusTransformer;
use App\Libraries\CacheHelper;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;

class SubmissionController extends BaseController
{
    protected $invoiceRepository;

    public const CACHE_TIME_FOR_CHECK_HAS_ROOM = 10;

    public function __construct(InvoiceRepository $invoiceRepository)
    {
        $this->invoiceRepository = $invoiceRepository;

        parent::__construct();
    }

    public function index()
    {
        $submissions = Submission::whereNotDone()->where('user_id', $this->user()->id)->get()->toArray();
        foreach($submissions as $key => $sub) {
            $submissions[$key]['listing_ids'] = explode(',', $sub['listing_ids']);
        }
        return ApiResponse::responseData(['submissions' => $submissions]);
    }

    /**
     * Get the current goldplus submission and subscription status.
     * @return JsonResponse
     */
    public function status(Request $request): JsonResponse
    {
        /** @var array */
        $data = $this->getGoldplusStatusData($this->user()->id, $request);

        return ApiResponse::responseData(
            [
                'data' => [
                    'gp_status' => (new SubmissionStatusTransformer())->transform($data)
                ]
            ]
        );
    }

    /**
     * Retrieve the goldplus submission and subscription status data by user id.
     * @param int $userId
     * @param Request $request
     * @return array
     */
    private function getGoldplusStatusData(int $userId, Request $request): array
    {
        $hasRoom = $this->hasRoom($userId, $request);
        if (!$hasRoom) {
            return [];
        }

        $invoiceId = $this->invoiceRepository->getActiveGoldplusInvoiceId($userId);
        if (!is_null($invoiceId)) {
            return [
                'key' => 'waiting',
                'invoice_id' => $invoiceId
            ];
        }

        $kostLevelGp = $this->getKostLevelGp($userId);
        if (!is_null($kostLevelGp)) {
            return [
                'key' => "gp$kostLevelGp"
            ];
        }

        if ($this->hasActiveGpSubmission($userId)) {
            $key = 'review';
        } else {
            $key = 'register';
        }

        return [
            'key' => $key
        ];
    }

    /**
     * get GP level as flag marking goldplus owner status
     * @param int $userId
     * @return int|null
     */
    public function getKostLevelGp(int $userId): ?int
    {
        $gpLevelIds = KostLevel::getGoldplusLevelIdsByGroups();
        $kostLevel = KostLevel::whereLevelsBelongToOwner($gpLevelIds, $userId)
            ->orderBy('name', 'desc')
            ->first();

        return !is_null($kostLevel) 
            ? (int)filter_var($kostLevel->name, FILTER_SANITIZE_NUMBER_INT) 
            : null;
    }
    
    /**
     * Check owner has active Goldplus submission
     * @param int $userId
     * @return bool
     */
    public function hasActiveGpSubmission(int $userId): bool
    {
        return Submission::whereNotDone()->where('user_id', $userId)->exists();
    }

    /**
     * Check owner has active listing room
     * @param int $userId
     * @param Request $request
     * @return bool
     */
    public function hasRoom(int $userId, Request $request): bool
    {
        $requestKey = CacheHelper::getKeyFromRequest($request);
        $cacheKey = 'user-id:'. $userId . ' request:' . $requestKey;

        $hasRoom = Cache::remember($cacheKey, self::CACHE_TIME_FOR_CHECK_HAS_ROOM, function () use ($userId) {
            return RoomOwner::where('user_id', $userId)
                ->whereNotIn('owner_status', RoomOwner::APARTMENT_TYPE)
                ->whereIn('status', [
                    RoomOwner::ROOM_ADD_STATUS, 
                    RoomOwner::ROOM_VERIFY_STATUS
                ])->exists();
        });
        
        return $hasRoom;
    }
}