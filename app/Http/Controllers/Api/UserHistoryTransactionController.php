<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\User;
use App\Http\Helpers\ApiResponse as Api;
use App\Entities\Mamipay\MamipayTenant;
use App\Libraries\MamipayApi;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;
use Barryvdh\DomPDF\Facade as PDF;
class UserHistoryTransactionController extends BaseController
{
    public function getHistoryInvoice()
    {
        $user = $this->user();

        if (!$user) {
            return Api::responseData([
                'status' => false,
                'meta'   => [
                    'message' => 'Silakan login terlebih dahulu',
                ],
            ]);
        }

        $userTenant = MamipayTenant::where('user_id', $user->id)->first();

        if ($userTenant) {
            $mamipayUri = 'internal/tenant/' .$userTenant->id. '/invoice/history';
            $mamipayApi = new MamipayApi();
            
            $getFromMamipayApi = $mamipayApi->get($mamipayUri);

            if (!empty($getFromMamipayApi->data)) {
                return Api::responseData([
                    'status' => true,
                    'data' => $getFromMamipayApi->data
                ]);
            } else {
                return Api::responseData([
                    'status' => false,
                    'meta'   => ['message' => 'Maaf data tidak tersedia']
                ]);
            }

        } else {
            return Api::responseData([
                'status' => false,
                'meta'   => [
                    'message' => 'Maaf data transaksi anda tidak ada',
                ],
            ]);
        }
    }

    public function getHistoryInvoiceDetail($invoiceId)
    {
        $user = $this->user();

        if (!$user) {
            return Api::responseData([
                'status' => false,
                'meta'   => [
                    'message' => 'Silakan login terlebih dahulu',
                ],
            ]);
        }

        $userTenant = MamipayTenant::where('user_id', $user->id)->first();

        if ($userTenant) {
            $mamipayUri = 'internal/tenant/invoice/'.$invoiceId;            
            $mamipayApi = new MamipayApi();
            
            $getFromMamipayApi = $mamipayApi->get($mamipayUri);
            
            if (!empty($getFromMamipayApi->data)) {
                $invoice        = $getFromMamipayApi->data;

                // total invoice
                if ($invoice->additional_costs > 0) {
                    $totalItem = [];
                    foreach ($invoice->additional_costs as $item) {
                        $totalItem[] = $item->cost_value;
                    }
                    $invoice->total = array_sum($totalItem) + $invoice->amount;
                } else {
                    $invoice->total = $invoice->amount;
                }
                
                if(!file_exists(public_path('invoice'))) mkdir(public_path('invoice'));
                $createInvoice  = PDF::loadView('transaction-invoice', ['invoice' => $invoice])->save(public_path('invoice/invoice-'.$invoiceId.$user->id.'.pdf'));
                $urlInvoice     = url('invoice/invoice-'.$invoiceId.$user->id.'.pdf');

                $result = [
                    "contract_id"           => $invoice->contract_id,
                    "shortlink"             => $invoice->shortlink,
                    "invoice_number"        => $invoice->invoice_number,
                    "name"                  => $invoice->name,
                    "scheduled_date"        => $invoice->scheduled_date,
                    "amount"                => $invoice->amount,
                    "total"                 => $invoice->total,
                    "status"                => $invoice->status,
                    "paid_at"               => $invoice->paid_at,
                    "total_paid_amount"     => $invoice->total_paid_amount,
                    "transfer_status"       => $invoice->transfer_status,
                    "transfer_reference"    => $invoice->transfer_reference,
                    "transfer_amount"       => $invoice->transfer_amount,
                    "transferred_at"        => $invoice->transferred_at,
                    "created_at"            => $invoice->created_at,
                    "additional_costs"      => $invoice->additional_costs,
                    "invoice"               => $urlInvoice
                ];

                return Api::responseData([
                    'status' => true,
                    'data'   => $result
                ]);
                
            } else {
                return Api::responseData([
                    'status' => false,
                    'meta'   => ['message' => 'Maaf data tidak tersedia']
                ]);
            }

        } else {
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => 'Maaf data transaksi anda tidak ada',
                ],
            ]);
        }
    }
}
