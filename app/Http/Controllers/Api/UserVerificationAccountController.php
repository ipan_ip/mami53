<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;
use Carbon\Carbon;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Mail;

use App\User;
use App\Http\Helpers\ApiResponse as Api;
use App\Repositories\UserDataRepository;
use App\Libraries\SMSLibrary;
use App\Entities\Media\Media;
use App\Entities\User\UserVerificationAccount;
use App\Entities\Activity\ActivationCode;
use App\Entities\User\UserMedia;
use App\Http\Helpers\ApiHelper;
use Jenssegers\Agent\Agent;
use App\Entities\Activity\Tracking;
use App\Http\Helpers\ApiResponse;
use App\Repositories\Mamipay\MamipayTenantRepository;
use Jenssegers\Date\Date;
use Config;
use Illuminate\Http\JsonResponse;

class UserVerificationAccountController extends BaseController
{
    protected $messageValidator = [
        "email.required"  => "Email tidak boleh kosong",
        "email.email"     => "Format email tidak sesuai",
        "media.required"  => "Gagal upload media",
        "media.mimes"     => "Jenis file tidak diperkenankan, harap menggunakan file JPG atau PNG",
        "media.max"       => "Ukuran file tidak boleh melebihi 8MB",
        "photo.required"  => "Gagal upload media",
        "photo.mimes"     => "Jenis file tidak diperkenankan, harap menggunakan file JPG atau PNG",
        "photo.max"       => "Ukuran file tidak boleh melebihi 8MB"
    ];

    protected $mamipayURL;
    protected $mamipayToken;

    const EXPLODE_CHAR_FOR_VERIFY_LINK = '&';

    protected $mamipayTenantRepository;

    public function __construct(UserDataRepository $repository, MamipayTenantRepository $mamipayTenantRepository)
    {
        parent::__construct();
        $this->repository = $repository;
        $this->mamipayURL   = Config::get('api.mamipay.url');
        $this->mamipayToken = Config::get('api.mamipay.token');
        $this->mamipayTenantRepository = $mamipayTenantRepository;
    }

    public function verificationMobilePhone(Request $request)
    {
	    $user = $this->user();

        if(!$user) {
            return Api::responseData([
                'status' =>false,
                'meta'   => [
                    'message'=>'Silakan login terlebih dahulu'
                ]
            ]);
        }

        $data = User::SendCodeVerification($user, $for = 'user_verification');

        return Api::responseData(array('status' => $data['status'], 'meta' => array("message" => $data['message'])));

    }

    /**
     * Check is owner email?
     * 
     * @param string $email
     * @param \App\User $user
     * 
     * @return bool
     */
    private static function isOwnerEmail(string $email, User $user): bool
    {
        return User::where('email', $email) 
            ->where('id', '!=', $user->id)
            ->where('is_owner', 'true')
            ->exists();
    }

    public function sendVerificationEmail(Request $request)
    {
        $user = $this->user();

        if (is_null($user)) {
            return Api::responseData([
                'status' =>false,
                'meta'   => [
                    'message' => 'Data user tidak ditemukan'
                ]
            ]);
        }      

        $validator = Validator::make($request->all(), [ 'email' => 'required|email', ], $this->messageValidator);

        $checkUser = User::where('email', $request->email)
                            ->where('id', '!=', $user->id)
                            ->where('is_owner', $user->is_owner)
                            ->first();
        
        $isOwnerEmail = self::isOwnerEmail($request->email, $user);
        $validator->after(function($validator) use ($checkUser, $isOwnerEmail) {
            if (!is_null($checkUser)) {
                $validator->errors()->add('email', __('api.tenant.verification.email.exists'));
            }

            if ($isOwnerEmail) {
                $validator->errors()->add('email', __('api.tenant.verification.email.exists'));
            }
        });

        if ($validator->fails()) {
            return Api::responseData([
                'status' => false,
                'meta'   => [
                    'message' => Api::validationErrorsToString($validator->errors())
                ]
            ]);
        }

        /* email to owner or user*/
        $encode   = base64_encode($request->email."&".$user->id."&".date("Y-m-d H:i:s", strtotime("+2 hours"))."&".$user->phone_number);
        $emailVerifikasi = [
            'subject'   => "Verifikasi email",
            'recipient' => $request->email,
            'link'      => url("verification/user/email/".$encode),
            'username'  => $user->name
        ];
        $templateEmailVerifikasi = "emails.user.verification-account-user";
        self::sendEmail($emailVerifikasi, $templateEmailVerifikasi);

        // Store Encode Email
        $verifyEmail = UserVerificationAccount::where('user_id', $user->id)->first();
        if (is_null($verifyEmail)) {
            $verifyEmail = new UserVerificationAccount;
            $verifyEmail->user_id = $user->id;
            $verifyEmail->verify_link = $encode;
            $verifyEmail->save();
        } else {
            $verifyEmail->verify_link = $encode;
            $verifyEmail->update();
        }

        // This nofication mail could not be sent, when email address is invalid.
        try
        {
            // If this verification is for changing email, notify it to old email
            if ($user->email != null 
                && $user->email != $request->email) {
                self::sendEmailNotice($user->email, $user->name, $request->ip());
            }
        }
        catch (\Swift_RfcComplianceException $se)
        {
            Bugsnag::notifyException($se);
        }
        catch (Exception $e)
        {
            Bugsnag::notifyException($e);
        }
        
        return Api::responseData([
            'status' => true,
            'meta'   => [
                'message' => 'Buka email anda untuk verifikasi'
            ]
        ]);
    }

    public static function sendEmailNotice($email, $name, $ipAddress)
    {
        if (!$email)
            return;

        if ($name == null)
            $name = "";

        if ($ipAddress == null)
            $ipAddress = "";

        $agent = new Agent();
        Date::setLocale('id');
        $date = Date::now()->format('l, d F Y H:i');
        $emailAlert = [
            'subject'   => "Perubahan Email Akun Mamikos Anda",
            'recipient' => $email,
            'username'  => $name,
            'device'    => Tracking::checkDevice($agent),
            'location'  => $ipAddress,
            'date'      => $date
        ];   
        $templateEmailAlert = 'emails.user.notice-verification-account-user';
        self::sendEmail($emailAlert, $templateEmailAlert);
    }

    public static function sendEmail($request, $template)
    {
        $subject = $request['subject'];
        $to      = $request['recipient'];

        Mail::send($template, $request, function ($message) use ($subject, $to) {
            $message->to($to)
                ->subject($subject);
        });
    }

    /**
     * Check is valid base64 data
     * 
     * @param String $str
     * 
     * @return bool
     */
    private function isBase64(String $str): bool
    {
        return (bool) preg_match('/^[a-zA-Z0-9\/\r\n+]*={0,2}$/', $str);
    }

    /**
     * Method for checking is valid date
     * 
     * @param String $date
     * @param String $format
     * 
     * @return bool
     */
    private function isValidDateFormat(String $date, $format = 'Y-m-d H:i:s'): bool
    {
        $d = \DateTime::createFromFormat($format, $date);
        // The Y ( 4 digits year ) returns TRUE for any integer with any number of digits so changing the comparison from == to === fixes the issue.
        return $d && $d->format($format) === $date;
    }

    /**
     * Check email verification by link verifier
     * 
     * @param Request $request
     * @param String $verifyLink
     * 
     * @return mixed
     */
    public function verificationEmail(Request $request, String $verifyLink)
    {
        //Check is valid base64?
        if (empty(trim($verifyLink)) || false === $this->isBase64($verifyLink)) {
            Bugsnag::notifyError('Verification-Email-Error', 'Empty/Non Base 64 Verification Link');
            abort(404);
        }

        $base64Str              = base64_decode($verifyLink);
        $data                   = explode(self::EXPLODE_CHAR_FOR_VERIFY_LINK, $base64Str);
        
        if(false === is_array($data)) {
            Bugsnag::notifyError('Verification-Email-Error', 'Non Array Decoded Verification Link');
            return abort(404);
        }
    
        $err        = false;
        $errMessage = '';

        //If array $data hasn't value count = 4
        if (count($data) != 4) {
            Bugsnag::notifyError('Verification-Email-Error', 'Count Data Not Match Verification Link');
            return abort(404);
        }

        //Initialize vars
        $email      = (!empty($data[0])) ? $data[0]: '';
        $userId     = (!empty($data[1])) ? $data[1] : null;

        //Check is user_id is null?
        //If yes, abort!
        if ($userId === null) {
            Bugsnag::notifyError('Verification-Email-Error', 'Null User ID');
            return abort(404);
        }

        //Initialize var expired date and phone
        $expired    = (!empty($data[2])) ? $data[2] : null;
        $phone      = (!empty($data[3])) ? $data[3] : null;

        //check valid email
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $err = true;
            $errMessage .= __('moengage-tracking.email-not-valid').',';
        }

        //check valid date
        if (false === $this->isValidDateFormat($expired)) {
            $err = true;
            $errMessage .= __('moengage-tracking.date-not-valid').',';
        }

        //Check date expirated
        if (date('Y-m-d H:i:s', now()->timestamp) > $expired) {
            $err = true;
            $errMessage .= __('moengage-tracking.verify-link-not-valid').',';
        }

        //Fold the error messages
        $errMessage = substr($errMessage, 0, strlen($errMessage)-1);

        //Initiate vars
        $isOwner = false;
        $user = [];
        $userData = User::where('email', $email)->first();

        //Is user data is empty
        if (empty($userData)) {

            //Get user data from $userId
            $user = User::find($userId);

            //If empty, then abort!
            if(empty($user->id)) {
                Bugsnag::notifyError('Verification-Email-Error', 'User Not Found');
                return abort(404);
            }

            //Initialize var and save the user data
            $user->email = $email;
            $user->save();

            //Check user type is owner?
            if ($user->is_owner === 'true') {
                $isOwner = true;
            }
        }

        //Check is user type is owner
        if ((!empty($userData->is_owner) && $userData->is_owner === 'true') 
            || (!empty($user->is_owner) && $user->is_owner === 'true')) {
            $isOwner = true;
        }

        //Check is empty user data?
        if (empty($user) && empty($userData)) {
            Bugsnag::notifyError('Verification-Email-Error', 'Empty User and Empty User Data');
            return abort(404);
        }
        
        //Specify the user is owner
        if ($err === true) {
            Bugsnag::notifyError('Verification-Email-Error', $errMessage);
            if ($isOwner === true && !empty($errMessage)) { 
                //abort operation
                return abort(404);
            } else {
                
                //abort operation
                //There are no case for tracking if the user is not owner
                return abort(404);
            }
        }

        /**
         * Save or Update operation for making sure database has correct data
         */
        $userVerificationAccount = UserVerificationAccount::where('user_id', $userId)->first();
        if (!empty($userVerificationAccount)) {
            if (
                !$userVerificationAccount->is_verify_email &&
                $userVerificationAccount->verify_link == $verifyLink
            ) {
                // Only need to update when it is not verified yet and verify_link is matched.
                // There are cases when the link is being hit more than once.
                // So we don't need to re-update it in that case.
                $userVerificationAccount->is_verify_email = true;
                $userVerificationAccount->verify_link    = null;
                $userVerificationAccount->update();
            }
        } else {
            $userVerificationAccount = new UserVerificationAccount();
            $userVerificationAccount->user_id         = $userId;
            $userVerificationAccount->is_verify_email = true;
            $userVerificationAccount->verify_link    = null;
            $userVerificationAccount->save();
        }

        //redirect to success page
        return redirect('/success/sukses-email-verifikasi');
    }

    public function checkCodeOtp(Request $request)
    {
        $user = $this->user();

        if(!$user) {
            return Api::responseData([
                'status' => false,
                'meta'   => [
                    'message' => 'Silakan login terlebih dahulu'
                ]
            ]);
        }

        $activationCodes = ActivationCode::where('phone_number', $request->phone_number)
										->whereRaw('BINARY `code`= ?', [$request->verification_code])
                                        ->whereRaw('expired_at > NOW()')
                                        ->first();

        if ($activationCodes) {
            // update data user
            $user->phone_number = $request->phone_number;
            $user->update();

            $this->mamipayTenantRepository->syncMamipayTenantWithUserTenant($user);

            // update user verification
            $userVerificationAccount = UserVerificationAccount::where('user_id', $user->id)->first();
            if ($userVerificationAccount) {
                $userVerificationAccount->is_verify_phone_number = true;
                $userVerificationAccount->update();
            } else {
                $userVerificationAccount = new UserVerificationAccount();
                $userVerificationAccount->user_id                = $user->id;
                $userVerificationAccount->is_verify_phone_number = true;
                $userVerificationAccount->save();
            }
            
            // make all available codes expired
            ActivationCode::where('phone_number', $user->phone_number)
                                    ->whereRaw('expired_at > NOW()')
                                    ->update(['expired_at' => Carbon::now()]);              
    
            return Api::responseData([
                'status' => true,
                'meta'   => [
                    'message' => 'Verification Success'
                ]
            ]);
        }
        
        return Api::responseData([
            'status' => false,
            'meta'   => [
                'message' => 'Verification failed'
            ]
        ]);
    }

    public function verificationIdentityCard(Request $request)
    {
        $user = $this->user();

        if ($user->is_owner == 'true') return Api::responseUserFailed(array("data" => null, "meta" => [ 'message' => array("User tidak ditemukan")]));

        if ($request->filled('photo_ktp') && $request->filled('photo_selfie_with_ktp')) {
            $response = $this->uploadIdentityCardMamipay($request->all(), $user);
            
            return Api::responseData($response); 
        } else {
            return Api::responseData([
                'status' => false,
                'meta'   => [
                    'message' => 'Something error'
                ]
            ]);
        }
    }

    public function uploadIdentityCard(Request $request)
    {
        $user = $this->user();

        if (!$user) return Api::responseUserFailed(array("data" => null, "message" => array("User tidak ditemukan")));

        // form validation
        if (isset($request['photo'])) {
            $validator = Validator::make($request->all(), [
                'photo'  => 'required|mimes:jpeg,bmp,png,jpg|max:8192', // Max size: 8MB
            ], $this->messageValidator); 
            
            if($validator->fails()) {
                $response = [
                    "status"  => false,
                    "message" => $validator->errors()->all(),
                    "data"    => null 
                ];
                return Api::responseData($response);
            }   
        }

        if ((isset($request['media']))) {
            $validator = Validator::make($request->all(), [
                'media'  => 'required|mimes:jpeg,bmp,png,jpg|max:8192', // Max size: 8MB
            ], $this->messageValidator);   

            if($validator->fails()) {
                $response = [
                    "status"  => false,
                    "message" => $validator->errors()->all(),
                    "data"    => null 
                ];
                return Api::responseData($response);
            }
        }

        $response = $this->repository->uploadIdentityCard($request->all(), $user);
        if (is_null($response)) {
            $response = [
                "status"  => false,
                "message" => "Gagal verifikasi kartu identitas",
                "data"    => null 
            ];

            return Api::responseData($response);
        }

        UserVerificationAccount::resetIdentityCardStatus($user);

        return Api::responseData($response); 
    }

    /**
     * Construct the available user verification options.
     *
     * @return JsonResponse
     */
    public function getIdentityCardOptions(): JsonResponse
    {
        $types = [];
        foreach (__('api.user-verification.options') as $key => $detail) {
            $types[] = [
                'key' => $key,
                'title' => $detail['title'],
                'subtitle' => $detail['subtitle'] ?? null,
            ];
        }
        return ApiResponse::responseData([
            'data' => $types
        ]);
    }

    public function getIdentityCard()
    {
        $user = $this->user();

        if (!$user) return Api::responseUserFailed(array("data" => null, "message" => array("User tidak ditemukan")));

		$photoIdentity =  UserMedia::where('user_id', $user->id)
			->where('description', UserMedia::PHOTO_IDENTITY_CARD)
			->orderBy('id', 'desc')
			->first();

		$medias = [];

		if ($photoIdentity) {
			$photoIdentity['url'] = $photoIdentity->getMediaUrl();

			$medias[] = $photoIdentity;
		}

		$selfieIdentity = UserMedia::where('user_id', $user->id)
			->where('description', UserMedia::SELFIE_IDENTITY_CARD)
			->orderBy('id', 'desc')
			->first();

		if ($selfieIdentity) {
			$selfieIdentity['url'] = $selfieIdentity->getMediaUrl();

			$medias[] = $selfieIdentity;
		}

        $response = [
            "status"  => true,
            "data"    => $medias
        ];

        return Api::responseData($response);
    }

    public function updateIdentityCardData(Request $request, $id) 
    {
        $user = $this->user();
        if (!$user) {
            return Api::responseData(array(
                "status" => false,
                "meta" => [
                    "message" => "Silahkan login terlebih dahulu."
                ],
            ));
        }
        
        $media = UserMedia::find($id);
        if (is_null($media)) {
            return Api::responseData(array(
                "status" => false,                
                "meta" => [
                    "message" => "Maaf, data foto tidak ditemukan. Silakan refresh halaman dan coba lagi."
                ],
            ));
        }

        $userMedia = UserMedia::updateMedia($request, $media);
        if (is_null($userMedia)) {
            $response = [
                "status" => false,
                "meta" => ["message" => "Failed updated, media not found"],
            ];

            return Api::responseData($response);
        }

        UserVerificationAccount::resetIdentityCardStatus($user);

        $response = [
            "status" => true,
            "meta" => ["message" => "Successfully updated"],
            "data" => $userMedia
        ];

		return Api::responseData($response);
    }

	public function detailIdentityCard($id)
	{
		$user = $this->user();
		if (!$user) {
			return Api::responseData(array(
				"status" => false,
				"meta" => [
					"message" => "Silahkan login terlebih dahulu."
				],
			));
		}

		$media = UserMedia::find($id);
		if (is_null($media)) {
			return Api::responseData(array(
				"status" => false,
				"meta" => [
					"message" => "Maaf, data foto tidak ditemukan. Silakan refresh halaman dan coba lagi."
				],
			));
		}

		return Api::responseData(['data' => $media]);
	}

    public function removeIdentityCard($id)
	{
		$user = $this->user();
		if (!$user) {
			return Api::responseData(array(
				"status" => false,
				"meta" => [
					"message" => "Silahkan login terlebih dahulu."
				],
			));
		}

		$media = UserMedia::find($id);
		if (is_null($media)) {
			return Api::responseData(array(
				"status" => false,
				"meta" => [
					"message" => "Maaf, data foto tidak ditemukan. Silakan refresh halaman dan coba lagi."
				],
			));
		}

		$media->delete();

		return Api::responseData(array(
			"status" => true,
			"meta" => [
				"message" => "Berhasil menghapus photo"
			],
		));
	}

    public function uploadIdentityCardMamipay(Request $request)
    {
        $user = $this->user();

        $client   = new Client;
        try {
            $response = $client->request('POST', $this->mamipayURL.'internal/media/upload', [
                'headers' => [
                    'Authorization' => $this->mamipayToken,
                ],
                'multipart' => [
                    [
                        'name'     => 'type',
                        'contents' => $request->input('type')
                    ],
                    [
                        'name'     => 'description',
                        'contents' => $request->input('description')
                    ],
                    [
                        'name'     => 'user_id',
                        'contents' => $user->id
                    ],
                    [
                        'name'     => 'media',
                        'contents' => fopen( $request->file('media')->getPathname(), 'r' ),
                    ]
                ]
            ]);

            if (json_decode($response->getBody(), true)['status'] == true) {
                $data = ['identity_card' => UserVerificationAccount::IDENTITY_CARD_STATUS_WAITING];
                UserVerificationAccount::addOrUpdate($data, $user);
            }

            return Api::responseData([
                "status" => true, 
                "media" => json_decode($response->getBody(), true)['media']
            ]);
     
            $responseJSON = $response->getBody()->getContents();

            $responseResult = json_decode($response->getBody()->getContents(), true)['media'];

        } catch (RequestException $e) {
            throw new \Exception(\GuzzleHttp\Psr7\str($e->getResponse()), 256);
        }  catch(\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function getIdentityCardMamipay($mediaId)
    {
        $client   = new Client(['base_uri' => $this->mamipayURL]);

        try {
            $response = $client->get('internal/media/show/'. $mediaId, [
                'headers' => [
                    'Content-Type'  => 'application/json',
                    'Authorization' => $this->mamipayToken
                ]
            ]); 

            $responseResult = json_decode($response->getBody()->getContents());

            return Api::responseData([
                "status" => true, 
                "media" => json_decode($response->getBody(), true)['media']
            ]);
        } catch(\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }   
}
