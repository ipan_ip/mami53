<?php

namespace App\Http\Controllers\Api;

use App\Entities\Room\Room;
use App\Presenters\RoomPresenter;
use App\Repositories\RoomRepository;
use Monolog\Handler\RotatingFileHandler;
use App\User;
use DB;
use Mail;

class SendEmailRecommendationKostController extends BaseController
{
    protected $repository;

    public function __construct(RoomRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        // log
        $logger = \Log::getLogger();
        $logger->popHandler();
        $logger->pushHandler(new RotatingFileHandler(storage_path('logs/schedule_send_email.log'), 7));
        $logger->info('send email recommendation kost ' . date('Y-m-d H:i:s'));

        // Get Recommendation Rooms
        $recommendationRoomsJogja       = $this->getRooms("Yogyakarta");
        $recommendationRoomsSurabaya    = $this->getRooms("Surabaya");
        $recommendationRoomsJakarta     = $this->getRooms("Jakarta");

        try {
            // get user register  >1 month and user never chat
            User::select('email', 'name')->whereNotNull('email')
                ->whereRaw(DB::raw('created_at > DATE_SUB(DATE(NOW()), INTERVAL 1 MONTH)'))
                ->whereNotIn('id', function($query) {
                    $query->select('user_id')
                        ->from('call')
                        ->whereRaw(DB::raw('created_at > CURDATE() - INTERVAL 30 DAY'))
                        ->whereNotNull('user_id')
                        ->groupBy('user_id');
                })->chunk(200, function ($users) use ($recommendationRoomsJakarta, $recommendationRoomsJogja, $recommendationRoomsSurabaya) {
                    foreach ($users as $user) {
                        $userEmail      = $user->email;
                        $userName       = $user->name;

                        if ($userEmail != null) {
                            Mail::send('emails.recommendation-kost', [
                                'userEmail'       => $userEmail,
                                'userName'        => $userName,
                                'roomsJogja'      => $recommendationRoomsJogja,
                                'roomsSurabaya'   => $recommendationRoomsSurabaya,
                                'roomsJakarta'    => $recommendationRoomsJakarta,
                                'emailSubject'    => "Bingung cari kost? Kasih tahu Mami dengan chat di sini! 💖"

                            ], function ($message) use ($user)  {
                                $message->to($user->email)
                                    ->subject("Hai ".$user->name.", Bingung cari kost? Kasih tahu Mami dengan chat di sini! 💖");
                            });
                        }
                    }
                });
        } catch (\ErrorException $e) {
            $logger->info('send email recommendation kost '. $e->getMessage()  . ' ' . date('Y-m-d H:i:s'));

        }
    }

    public function getRooms($area)
    {
        $recommendationRooms = Room::where('area_city', 'like', $area."%")
            ->whereBetween('price', ['0', '2500000'])
            ->whereRaw(DB::raw('is_active = true AND deleted_at IS NULL AND price_monthly IS NOT NULL'))
            ->orderBy('price', 'asc')
            ->limit(3)->get();

        if (count($recommendationRooms) > 0) {
            $listTransform  = (new RoomPresenter('email-recommendation'))->present($recommendationRooms);
            $result         = $listTransform['data'];

            return $result;

        } else {
            return $recommendationRooms;

        }

    }

}