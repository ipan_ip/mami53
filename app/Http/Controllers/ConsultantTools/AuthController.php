<?php

namespace App\Http\Controllers\ConsultantTools;

use App\Entities\Activity\Action;
use App\Entities\Activity\ActivityLog;
use App\Entities\User\UserRole;
use App\Http\Helpers\OAuthHelper;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AuthController extends Controller
{
    public function login()
    {
        if (config('app.env') === 'production') {
            if (isset($_SERVER['HTTP_X_FORWARDED_SERVER']) && $_SERVER['HTTP_X_FORWARDED_SERVER'] !== 'consultant.mamikos.com') {
                abort(404);
            }
        }

        if (Auth::check()) {
            return redirect()->route('consultant-tools.dashboard');
        }

        return view('web._consultant.consultant-index', [
            'from_consultant_tools' => true
        ]);
    }

    public function postLogin(Request $request)
    {
        $user = User::where('email', $request->input('email'))
            ->where('role', UserRole::Administrator)
            ->first();

        if (!isset($user) || !$user->can('access-consultant')) {
            return response()->json(
                [
                    'status' => 'error',
                    'message' => "Data yang Kamu Masukkan Salah" . PHP_EOL . "Harap periksa kembali."
                ]
            );
        }

        if (Auth::attempt(['email' => $request->input('email'), 'password' => md5($request->input('password'))])) {
            ActivityLog::Log(Action::LOGIN, 'logged in');
            $user = Auth::user();
            $token = app()->make(OAuthHelper::class)->issueTokenFor($user);

            return response()->json(
                [
                    'status' => 'success',
                    'message' => 'logged in',
                    'oauth' => $token,
                    'user_id' => $user->id,
                    'consultant' => $user->consultant,
                    'consultant_roles' => $user->consultant ? $user->consultant->roles : null
                ]
            );
        }

        return response()->json(
            [
                'status' => 'error',
                'message' => "Data yang Kamu Masukkan Salah" . PHP_EOL . "Harap periksa kembali."
            ]
        );
    }

    public function logout()
    {
        ActivityLog::Log(Action::LOGOUT, 'logged out');
        $token = Auth::user()->oauth_token();
        DB::table('oauth_refresh_tokens')
            ->where('access_token_id', $token->id)
            ->update([
                         'revoked' => true
                     ]);

        $token->revoke();
        return response()->json(
            [
                'status' => 'success',
                'message' => 'logged out'
            ]
        );
    }
}
