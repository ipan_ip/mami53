<?php

namespace App\Http\Controllers\ConsultantTools;

use Carbon\Carbon;
use Elasticsearch;
use Illuminate\Http\Request;
use App\Entities\Media\Media;
use App\Libraries\SMSLibrary;
use App\Http\Helpers\ApiHelper;
use App\Entities\SLA\SLAVersion;
use App\Entities\Level\KostLevel;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use App\Entities\Activity\AppSchemes;
use App\Entities\Booking\BookingUser;
use App\Entities\Room\Element\RoomUnit;
use App\Presenters\BookingUserPresenter;
use Illuminate\Support\Facades\Validator;
use App\Entities\Consultant\Booking\Status;
use App\Repositories\SLA\SLAVersionRepository;
use App\Services\Consultant\MamipayApiService;
use App\Http\Helpers\BookingNotificationHelper;
use App\Repositories\Booking\BookingUserRepository;
use App\Repositories\SLA\SLAVersionRepositoryEloquent;
use App\Http\Controllers\ConsultantTools\BaseController;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Entities\Consultant\Elasticsearch\BookingUserIndexQuery;

class BookingManagementController extends BaseController
{
    /**
     *  Instance of SLA version repository
     *
     * @var SLAVersionRepository
     */
    protected $slaRepository;

    /**
     *  Instance of BookingUserRepository
     *
     * @var BookingUserRepository
     */
    protected $bookingRepository;

    /**
     *  Instance of MamipayApiService
     *
     *  @var MamipayApiService
     */
    protected $mamipay;


    public function __construct(SLAVersionRepository $slaRepository, BookingUserRepository $bookingRepository, MamipayApiService $mamipay)
    {
        parent::__construct();

        $this->slaRepository = $slaRepository;
        $this->bookingRepository = $bookingRepository;
        $this->mamipay = $mamipay;
    }

    /**
     *  List available booking for a given consultant
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $query = (new BookingUserIndexQuery())->sort('booking_created', 'desc');

        if (!is_null($request->is_instant_booking) && !empty($request->is_instant_booking)) {
            $query->filters('is_instant_booking', $request->is_instant_booking);
        }

        if (!is_null($request->kost_level) && !empty($request->kost_level)) {
            $query->filters('kost_level_id', $request->kost_level);
        }

        if (!is_null($request->status) && !empty($request->status)) {
            $query->filter('status', $request->status);
        }

        if (!is_null($request->search) && !empty($request->search)) {
            $query->search(
                [
                    ['tenant_name' => $request->search],
                    ['tenant_phone' => $request->search],
                    ['kost_name' => $request->search],
                    ['owner_phone' => $request->search]
                ]
            );
        }

        $offset = $request->input('offset', 0);
        if (!is_null($offset) && !empty($offset)) {
            $query->offset($offset);
        }

        $mappedAreaCity = !is_null($this->consultant) ? $this->consultant->mapping : null;
        if (!is_null($mappedAreaCity) && ($mappedAreaCity->isNotEmpty())) {
            $query->filters('area_city', $mappedAreaCity->pluck('area_city')->toArray());
        }

        $results = Elasticsearch::search($query->getQuery())['hits'];
        $total = $results['total'];
        $results = collect($results['hits']);
        $photos = Media::whereIn('id', $results->pluck('_source.user_photo_id'))->get();

        $results = $results->map(
            function ($result, $key) use ($photos) {
                $result = $result['_source'];

                $photo = $photos->where('id', $result['user_photo_id'])->first();
                $result['photo'] = (!is_null($photo) ? $photo->getMediaUrl() : null);

                $checkIn = new Carbon($result['checkin_date']);
                $bookingDate = new Carbon($result['booking_created']);
                $daysToCheckIn = $checkIn->diffInDays((new Carbon($result['booking_created']))->startOfDay());
                $confirmed = new Carbon($result['booking_confirmed']);

                if (!isset($result['status'])) {
                    $result['status'] = null;
                }

                if (!isset($result['due_date']) || is_null($result['due_date'])) {
                    switch ($result['status']) {
                        case Status::BOOKED:
                            $config = $this->slaRepository
                                ->getSLAVersion(BookingUser::BOOKING_STATUS_BOOKED, $bookingDate)
                                ->getConfigRuleByDayLength(SLAVersion::SLA_VERSION_TWO, $daysToCheckIn);
                            $result['due_date'] = $bookingDate->addMinutes($config->value)->toDateTimeString();
                            break;
                        case Status::CONFIRMED:
                            $config = $this->slaRepository
                                ->getSLAVersion(BookingUser::BOOKING_STATUS_CONFIRMED, $bookingDate)
                                ->getConfigRuleByDayLength(SLAVersion::SLA_VERSION_THREE, $daysToCheckIn);
                            $result['due_date'] = $confirmed->addMinutes($config->value)->toDateTimeString();
                            break;
                            defualt:
                            $result['due_date'] = null;
                            break;
                    }
                }

                return $result;
            }
        );

        return ApiHelper::responseData(
            [
                'total' => $total,
                'limit' => 10,
                'offset' => $offset,
                'data' => $results->toArray()
            ]
        );
    }

    /**
     *  List available booking status and kost level
     *
     * @return JsonResponse
     */
    public function getStatusAndLevel()
    {
        $bookingStatus = [
            ['key' => 'Butuh Konfirmasi', 'label' => 'Butuh Konfirmasi'],
            ['key' => 'Tunggu Pembayaran', 'label' => 'Tunggu Pembayaran'],
            ['key' => 'Terbayar', 'label' => 'Terbayar'],
            ['key' => 'Pembayaran Diterima', 'label' => 'Pembayaran Diterima'],
            ['key' => 'Sewa Berakhir', 'label' => 'Sewa Berakhir'],
            ['key' => 'Ditolak Pemilik', 'label' => 'Ditolak Pemilik'],
            ['key' => 'Dibatalkan', 'label' => 'Dibatalkan'],
            ['key' => 'Kadaluwarsa oleh Penyewa', 'label' => 'Kadaluwarsa oleh Penyewa'],
            ['key' => 'Kadaluwarsa oleh Pemilik', 'label' => 'Kadaluwarsa oleh Pemilik'],
            ['key' => 'Ditolak Admin', 'label' => 'Ditolak Admin'],
        ];

        $kostLevel = KostLevel::orderBy('name')
            ->get(['id', 'name'])
            ->toArray();

        return ApiHelper::responseData(
            [
                'data' => [
                    'status' => $bookingStatus,
                    'level' => $kostLevel
                ]
            ]
        );
    }

    public function contractDetail(Request $request, int $bookingId): JsonResponse
    {
        try {
            $booking = $this->bookingRepository
                ->setPresenter(
                    new BookingUserPresenter(BookingUserPresenter::CONSULTANT_BOOKING_MANAGEMENT_CONTRACT_DETAIL)
                )
                ->with('invoices', 'invoices.additional_costs', 'contract', 'room', 'room.level')
                ->find($bookingId);

            return ApiHelper::responseData(
                [
                    'data' => $booking['data']
                ]
            );
        } catch (ModelNotFoundException $e) {
            return ApiHelper::responseData(['message' => __('api.exception.not_found')], false, false, 404);
        }
    }

    public function rejectBooking(Request $request, $bookingId)
    {
        try {
            $bookingUser = BookingUser::findOrFail($bookingId);

            if ($bookingUser->status !== BookingUser::BOOKING_STATUS_BOOKED) {
                return ApiHelper::responseData(
                    ['status' => false, 'message' => 'Action cannot be done'],
                    false,
                    false,
                    400
                );
            }

            $this->bookingRepository->rejectBookingByAdmin($bookingUser, $request);

            // Send Notification
            $template = [
                "title" => "Booking Kost Ditolak!",
                "message" => "Booking kost ditolak, klik disini untuk melihat alasan.",
                "scheme" => AppSchemes::getBookingListPage()
            ];
            $notification = BookingNotificationHelper::getInstance();
            $notification->sendNotif($bookingUser->user_id, $template, $toMamipayOwner = false);

            // Send SMS
            $this->sendSMSToUser(
                $bookingUser->contact_phone,
                "Booking kost ditolak, cek aplikasi mamikos kamu untuk melihat alasan."
            );
        } catch (\Exception $exception) {
            return ApiHelper::responseData(
                [
                    'message' => 'Booking not found'
                ],
                false,
                false,
                404
            );
        }

        return ApiHelper::responseData(['status' => true, 'message' => 'Booking rejected']);
    }

    private function sendSMSToUser($userMobilePhoneNumber, $message)
    {
        $isValid = SMSLibrary::validateDomesticIndonesianMobileNumber($userMobilePhoneNumber);
        if ($isValid) {
            SMSLibrary::smsVerification($userMobilePhoneNumber, $message);
        }
    }

    /**
     *  Accept booking data
     *
     *  @param Request $request
     *
     *  @return JsonResponse
     */
    public function acceptBooking(Request $request, int $bookingId): JsonResponse
    {
        $validator = Validator::make(
            $request->all(),
            [
                'designer_room_id' => 'nullable',
                'deposit_amount' => 'nullable|numeric',
                'fine_amount' => 'nullable|numeric',
                'fine_maximum_length' => 'required_with:fine_amount|integer',
                'fine_duration_type' => 'required_with:fine_amount',

                'additional_costs' => 'nullable|array',
                'additional_costs.*.field_title' => 'string',
                'additional_costs.*.field_value' => 'numeric',

                'use_dp' => 'nullable|boolean',
                'dp_amount' => 'required_if:use_dp,==,true|integer',
            ]
        );

        if ($validator->fails()) {
            return ApiHelper::responseData(
                [
                    'status' => false,
                    'errors' => $validator->errors()
                ],
                false,
                false,
                501
            );
        }

        try {
            $booking = $this->bookingRepository
                ->with('user.mamipay_tenant', 'designer')
                ->find($bookingId);
        } catch (ModelNotFoundException $e) {
            return ApiHelper::responseData(['message' => __('api.exception.not_found')], false, false, 404);
        }

        $input = $request->input();
        $input['room_id'] = $booking->designer->song_id;
        $input['start_date'] = $booking->checkin_date;
        $input['duration'] = $booking->stay_duration;
        $input['rent_type'] = $booking->rent_count_type;

        $durationType = $booking->rent_count_type ? $booking->rent_count_type : 'monthly';
        $amount = $booking->booking_user_room ? $booking->booking_user_room->price : 0;
        if (!$amount) {
            $amount = $booking->room->{'price_' . $durationType} ? $booking->room->{'price_' . $durationType} : 0;
        }
        $input['amount'] = $amount;

        if ($booking->user->mamipay_tenant) {
            $input['name'] = $booking->user->mamipay_tenant->name;
            $input['phone_number'] = $booking->user->mamipay_tenant->phone_number;
            $input['email'] = $booking->user->mamipay_tenant->email;
            $input['photo_identifier_id'] = $booking->user->mamipay_tenant->photo_identifier_id;
            $input['photo_document_id'] = $booking->user->mamipay_tenant->photo_document_id;
            $input['occupation'] = $booking->user->mamipay_tenant->occupation ? $booking->user->mamipay_tenant->occupation : '-';
            $input['marital_status'] = $booking->user->mamipay_tenant->marital_status;
            $input['photo_marriage_document_id'] = $booking->user->mamipay_tenant->photo_marriage_document_id;

            $gender = $booking->user->mamipay_tenant->gender ? $booking->user->mamipay_tenant->gender : 'male';
            switch ($gender) {
                case 'perempuan':
                    $input['gender'] = 'female';
                    break;
                default:
                    $input['gender'] = 'male';
            }
        } else {
            $input['name'] = $booking->user->name;
            $input['phone_number'] = $booking->user->phone_number;
            $input['email'] = $booking->user->email;
            $input['occupation'] = $booking->user->job ? ucfirst($booking->user->job) : '-';
            $input['marital_status'] = $booking->user->marital_status;

            $gender = $booking->user->gender ? $booking->user->gender : 'male';
            switch ($gender) {
                case 'perempuan':
                    $input['gender'] = 'female';
                    break;
                default:
                    $input['gender'] = 'male';
            }
        }

        $input['parent_name'] = '';
        $input['parent_phone_number'] = '';
        $input['owner_accept'] = true;

        if (array_key_exists('fine_amount', $input) && $input['fine_amount'] == null) unset($input['fine_amount']);

        switch ($input['rent_type']) {
            case 'daily':
                $input['rent_type'] = 'day';
                break;
            case 'weekly':
                $input['rent_type'] = 'week';
                break;
            case 'monthly':
                $input['rent_type'] = 'month';
                break;
            case 'quarterly':
                $input['rent_type'] = '3_month';
                break;
            case 'semiannually':
                $input['rent_type'] = '6_month';
                break;
            case 'yearly':
                $input['rent_type'] = 'year';
                break;
            default:
                $input['rent_type'] = 'month';
                break;
        }

        if ($request->use_dp) {
            $input['dp_date'] = now()->addDay()->format('Y-m-d');
            $input['dp_settlement_date'] = $booking->checkin_date;
        }

        // detech room number
        if (!empty($request->designer_room_id)) {
            $roomUnit = RoomUnit::find($request->designer_room_id);
            if (!$roomUnit) {
                return ApiHelper::responseData(
                    [
                        'status' => false,
                        'errors' => 'Kamar tidak valid.'
                    ],
                    false,
                    false,
                    501
                );
            }

            $input['room_number'] = $roomUnit->name;
        }

        $user = Auth::user();
        $response = $this->mamipay->makeRequest('POST', $input, 'owner/booking/accept/' . $booking->id, $user->id);

        if (!$response->status) {
            return ApiHelper::responseData(
                [
                    'status' => false,
                    'errors' => $response->meta->message
                ],
                false,
                false,
                501
            );
        }

        return ApiHelper::responseData(
            [
                'status' => true,
                'message' => 'Booking berhasil diterima'
            ]
        );
    }

    public function bookingAndPropertyDetail($bookingId)
    {
        try {
            $booking = $this->bookingRepository
                ->setPresenter(
                    new BookingUserPresenter(BookingUserPresenter::CONSULTANT_BOOKING_AND_PROPERTY_DETAIL)
                )
                ->with('room', 'room.level', 'room.photo', 'statuses')
                ->find($bookingId);

            return ApiHelper::responseData(
                [
                    'data' => $booking['data']
                ]
            );
        } catch (ModelNotFoundException $e) {
            return ApiHelper::responseData(['message' => __('api.exception.not_found')], false, false, 404);
        }
    }


    public function tenantDetail(int $bookingId): JsonResponse
    {
        try {
            $booking = $this->bookingRepository
                ->setPresenter(new BookingUserPresenter(BookingUserPresenter::CONSULTANT_BOOKING_MANAGEMENT_TENANT_DETAIL))
                ->with('user', 'user.mamipay_tenant')
                ->find($bookingId);

            return ApiHelper::responseData(
                [
                    'data' => $booking['data']
                ]
            );
        } catch (ModelNotFoundException $e) {
            return ApiHelper::responseData(
                [
                    'status' => false,
                    'message' => __('api.exception.not_found')
                ],
                false,
                false,
                404
            );
        }
    }
}
