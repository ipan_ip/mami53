<?php

namespace App\Http\Controllers\ConsultantTools;

use App\Entities\Consultant\Consultant;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class BaseController extends Controller
{
    const DEFAULT_PAGINATION = 10;
    protected $consultantHasMapping, $consultant;

    public function __construct()
    {
        $this->middleware(function($request,$next){
            $consultant = Auth::user()->consultant;

            if ($consultant) {
                // We add this to the request object for better testability.
                $consultant = Consultant::where('user_id', Auth::guard('passport')->user()->id)
                    ->withCount(
                        [
                            'consultant_rooms' => function ($mapping) {
                                $mapping->whereNull('deleted_at');
                            }
                        ]
                    )
                    ->first();

                $this->consultantHasMapping = !is_null($consultant) && ($consultant->consultant_rooms_count > 0);
                $this->consultant = $consultant;
            }

            return $next($request);
        });
    }
}
