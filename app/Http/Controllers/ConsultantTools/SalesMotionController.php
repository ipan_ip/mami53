<?php

namespace App\Http\Controllers\ConsultantTools;

use App\Entities\Consultant\SalesMotion\SalesMotion;
use App\Entities\Consultant\SalesMotion\SalesMotionProgress;
use App\Http\Helpers\ApiResponse;
use Illuminate\Http\Request;
use App\Http\Helpers\ApiHelper;
use App\Presenters\Consultant\SalesMotion\SalesMotionPresenter;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Repositories\Consultant\SalesMotion\SalesMotionRepository;
use Illuminate\Support\Facades\Auth;

class SalesMotionController extends BaseController
{
    /**
     *  Instance of sales motion repository
     *
     * @var SalesMotionRepository
     */
    protected $salesMotionRepository;

    public function __construct(SalesMotionRepository $salesMotionRepository)
    {
        $this->salesMotionRepository = $salesMotionRepository;
    }

    public function detail($id)
    {
        try {
            $salesMotion = $this->salesMotionRepository
                ->setPresenter(new SalesMotionPresenter(SalesMotionPresenter::PRESENTER_TYPE_CONSULTANT_DETAIL))
                ->find($id);

            return ApiHelper::responseData(
                [
                    'sales_motion' => $salesMotion['data']
                ]
            );
        } catch (ModelNotFoundException $e) {
            return ApiHelper::responseData(
                [
                    'status' => false,
                    'message' => 'Sales motion tidak bisa ditemukan'
                ],
                false,
                false,
                404
            );
        }
    }

    public function list(Request $request)
    {
        $consultant = Auth::user()->consultant;

        $input = $request->input();
        $input['limit'] = $input['limit'] ?? self::DEFAULT_PAGINATION;
        $input['offset'] = $input['offset'] ?? 0;

        $data = $this->salesMotionRepository->getConsultantList($consultant, $input);

        $presentedData = (new SalesMotionPresenter(SalesMotionPresenter::PRESENTER_TYPE_CONSULTANT_LIST))->present(
            $data['list']
        );

        return ApiResponse::responseData(
            [
                'status' => true,
                'total' => $data['total'],
                'data' => $presentedData
            ]
        );
    }
}
