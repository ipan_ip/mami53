<?php

namespace App\Http\Controllers\ConsultantTools;

use App\Entities\Media\Media;
use App\Http\Helpers\ApiHelper;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class MediaController extends BaseController
{
    /**
     *  Allows photo upload for consultant tools
     *
     *  @param Request $request
     *
     *  @return JsonResponse
     */
    public function upload(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'file' => 'required|image'
        ]);

        if ($validator->fails()) {
            return ApiHelper::responseData([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        $media = Media::postMedia($request->file, 'upload', Auth::id(), 'user_id', null, Media::CONSULTANT_PHOTO_TYPE);

        return ApiHelper::responseData([
            'status' => true,
            'id' => $media['id'],
            'url' => $media['url']
        ]);
    }

    /**
     *  Delete $media database record and file
     *
     *  @param Request $request
     *  @param Media $media Media to delete
     *
     *  @return JsonResponse
     */
    public function delete(Request $request, int $mediaId): JsonResponse
    {
        try {
            $media = Media::findOrFail($mediaId);
            $media->deleteFile();
            $media->delete();

            return ApiHelper::responseData([]);
        } catch (ModelNotFoundException $e) {
            return ApiHelper::responseData(['message' => __('api.exception.not_found')], false, false, 404);
        }
    }
}
