<?php

namespace App\Http\Controllers\ConsultantTools;

use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayInvoice;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Entities\Mamipay\MamipayTenant;
use App\Http\Controllers\ConsultantTools\BaseController;
use App\Http\Helpers\ApiResponse;
use App\Http\Helpers\MamipayApiHelper;

class ActiveTenantController extends BaseController
{
    private $request;
    private $offset;

    const SORT_CREATED_AT = 'created_at';
    const SORT_SCHEDULED_AT = 'scheduled_date';

    /**
     *  User is an active tenant if: contract active, register by themselves(not by owner/consultant).
     */
    public function index(Request $request)
    {
        $this->offset = empty($request->input('offset')) ? 0 : $request->input('offset');
        $this->request = $request;

        $tenants = MamipayTenant::select(
            [
                'mamipay_tenant.id',
                'mamipay_tenant.name',
                'room.name as room_name',
                'contract.created_at',
                'invoice.scheduled_date',
            ]
        );

        // Function call to $this->query need to be before the scope function calls to prevent wrong query
        $tenants = $this->query($tenants);
        $tenants->joinWhereActive()
            ->joinRoom()
            ->joinNextInvoice();

        if ($this->consultantHasMapping) {
            $tenants->whereRoomAssignedTo($this->consultant->id);
        }

        $count = $tenants->count();
        $tenants = $this->sort($tenants);
        $tenants = $this->paginate($tenants);
        $tenants = $tenants->get();

        return ApiResponse::responseData(
            [
                'total' => $count,
                'limit' => self::DEFAULT_PAGINATION,
                'offset' => $this->offset,
                'data' => $tenants
            ]
        );
    }

    /**
     *  Show detail data of an active tenant
     *
     * @param Request $request Incoming request to the application
     * @param int $id Active MamipayTenant id
     * @return json data of MamipayTenant
     */
    public function show(Request $request, int $id)
    {
        $tenant = MamipayTenant::whereHas('contracts', function ($contracts) {
            $contracts->where('status', MamipayContract::STATUS_ACTIVE);
        })
        ->with([
            'contracts' => function ($contracts) {
                $contracts->where('status', MamipayContract::STATUS_ACTIVE);
            },
            'contracts.type_kost',
            'contracts.type_kost.room'
        ]);

        $consultantId = !is_null($this->consultant) ? $this->consultant->id : null;
        if ($this->consultantHasMapping) {
            $tenant->whereHas('contracts.type_kost.room.consultant_rooms', function ($mapping) use ($consultantId) {
                $mapping->where('consultant_id', $consultantId)
                    ->whereNull('deleted_at');
            });
        }

        $tenant = $tenant->findOrFail($id);
        $contract = $tenant->contracts->first();
        $lastInvoice = MamipayInvoice::where('contract_id', $contract->id)
            ->orderBy('scheduled_date', 'desc')
            ->first();

        $tenantDetail = MamipayApiHelper::makeRequest(
            $request->method(),
            null,
            ('tenant/show/' . $id),
            Auth::user()->id
        )->tenant;

        return ApiResponse::responseData(
            [
                'data' => [
                    'id' => $tenant->id,
                    'name' => $tenant->name,
                    'gender' => $tenant->gender,
                    'phone_number' => $tenant->phone_number,
                    'email' => $tenant->email,
                    'parent_phone_number' => $tenant->parent_phone_number,
                    'occupation' => $tenant->occupation,
                    'room_name' => $contract->type_kost->room->name,
                    'start_date' => $contract->start_date,
                    'end_date' => $contract->end_date,
                    'duration' => $contract->duration,
                    'duration_unit' => $contract->duration_unit,
                    'contract_id' => $contract->id,
                    'scheduled_date' => $lastInvoice->scheduled_date,
                    'amount' => $lastInvoice->amount,
                    'photo_identifier' => $tenantDetail->tenant_photo_identifier,
                    'photo_document' => $tenantDetail->tenant_photo_document
                ]
            ]
        );
    }

    /**
     *  Query tenants for similar: name, email or phone
     *
     * @param Builder $query Current query of tenant
     * @return Builder Tenants who had similar match
     */
    private function query(Builder $query): Builder
    {
        $keyword = $this->request->input('q');

        if (!empty($keyword)) {
            return $query->where('mamipay_tenant.email', 'like', "%$keyword%")
                ->orWhere('mamipay_tenant.name', 'like', "%$keyword%")
                ->orWhere('mamipay_tenant.phone_number', 'like', "%$keyword%");
        }

        return $query;
    }

    /**
     *  Sort the tenant based on the current request
     *
     * @param Builder $query Current query of tenant
     * @return Builder Sorted list of tenants
     */
    private function sort(Builder $query): Builder
    {
        $sort = !empty($this->request->input('sort')) ? $this->request->input('sort') : self::SORT_CREATED_AT;

        switch ($sort) {
            case self::SORT_CREATED_AT:
                return $query->orderBy('contract.created_at', 'desc');
            case self::SORT_SCHEDULED_AT:
                return $query->orderBy('invoice.scheduled_date', 'asc');
        }
    }

    /**
     *  Paginate the current query of tenants
     *
     * @param Builder $query Current query of tenant
     * @return Builder Paginated tenant list
     */
    private function paginate(Builder $query): Builder
    {
        $query = $query->limit(self::DEFAULT_PAGINATION);

        if ($this->offset !== 0) {
            return $query->offset($this->offset);
        }

        return $query;
    }
}