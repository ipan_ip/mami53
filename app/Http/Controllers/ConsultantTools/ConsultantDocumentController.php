<?php

namespace App\Http\Controllers\ConsultantTools;

use App\Entities\Consultant\Document\ConsultantDocument;
use App\Entities\Consultant\Document\DocumentType;
use App\Http\Helpers\ApiHelper;
use App\Services\Consultant\ConsultantDocumentService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ConsultantDocumentController extends BaseController
{
    /**
     *  Instance of ConsultantDocumentService
     *
     *  @var ConsultantDocumentService
     */
    protected $documentService;

    public function __construct(ConsultantDocumentService $documentService)
    {
        parent::__construct();
        $this->documentService = $documentService;
    }

    /**
     *  Upload a document for sales motion progress
     *
     *  @param Request $request
     *
     *  @return JsonResponse
     */
    public function uploadSalesMotionProgress(Request $request): JsonResponse
    {
        $validator = Validator::make(
            $request->all(),
            [
                'file' => 'required|file|mimes:pdf,docx,doc|max:5120'
            ]
        );

        if ($validator->fails()) {
            return ApiHelper::responseData(['messages' => $validator->errors()], false, false, 501);
        }

        $document = $this->documentService->store($request->file, DocumentType::SALES_MOTION_PROGRESS());

        return ApiHelper::responseData([
            'document_id' => $document->id
        ]);
    }

    /**
     *  Download a $document file
     *
     *  @param Request $request
     *  @param $document Document to download
     */
    public function download($document)
    {
        $doc = ConsultantDocument::find($document);
        return Storage::download($doc->file_location);
    }

    /**
     *  Delete an existing document
     *
     *  @param Request $request
     *  @param int $documentId Id of document to delete
     *
     *  @return JsonResponse
     */
    public function delete(Request $request, int $documentId): JsonResponse
    {
        try {
            $this->documentService->delete($documentId);

            return ApiHelper::responseData([]);
        } catch (ModelNotFoundException $e) {
            return ApiHelper::responseData(['message' => __('api.exception.not_found')], false, false, 404);
        }
    }
}
