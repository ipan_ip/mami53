<?php

namespace App\Http\Controllers\ConsultantTools;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Entities\Consultant\ConsultantNote;
use App\Entities\Consultant\PotentialTenant;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayInvoice;
use App\Http\Helpers\ApiResponse;
use App\Entities\Room\RoomOwner;
use App\Entities\Mamipay\MamipayTenant;
use App\Http\Helpers\MamipayApiHelper;
use App\Http\Requests\CreateContractRequest;
use App\Http\Requests\ModifyAdditionalCostRequest;
use App\Http\Requests\UpdateInvoiceAmountRequest;
use App\Presenters\MamipayTenantPresenter;
use App\Presenters\PotentialTenantPresenter;
use App\Services\Consultant\RoomIndexService;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Carbon\Carbon;
use Intervention\Image\Exception\NotFoundException;
use mysql_xdevapi\Exception;

class ContractManagementController extends BaseController
{
    private const FILTER_CREATED_BY_CONSULTANT = 'consultant';
    private const FILTER_CREATED_BY_TENANT = 'tenant';
    private const FILTER_CREATED_BY_OWNER = 'owner';

    protected $roomIndexService;

    public function __construct(RoomIndexService $roomIndexService)
    {
        parent::__construct();
        $this->roomIndexService = $roomIndexService;
    }

    public function list(Request $request)
    {
        $invoices = DB::table('mamipay_contract_invoice as invoice')
            ->leftJoin('mamipay_contract_invoice_cost','mamipay_contract_invoice_cost.invoice_id','invoice.id')
            ->leftJoin(
                'mamipay_contract_invoice as invoice2',
                function ($join) use ($request) {
                    $join->on('invoice.contract_id', 'invoice2.contract_id')
                        ->on('invoice2.id', '>', 'invoice.id')
                        ->whereNull('invoice2.deleted_at');

                    if ($request->has('date_from')) {
                        $join->where('invoice.scheduled_date', '>=', $request->input('date_from'))
                            ->where('invoice2.scheduled_date', '>=', $request->input('date_from'));
                    }

                    if ($request->has('date_to')) {
                        $join->where('invoice.scheduled_date', '<=', $request->input('date_to'))
                            ->where('invoice2.scheduled_date', '<=', $request->input('date_to'));
                    }
                }
            )
            ->select(
                'invoice.id',
                'invoice.contract_id',
                'invoice.scheduled_date',
                'invoice.status',
                'invoice.invoice_number',
                DB::RAW(
                    'IF(invoice.transfer_reference = "' . MamipayInvoice::PAYMENT_STATUS_NOT_IN_MAMIPAY . '",invoice.transfer_reference,null) as transfer_reference'
                ),
                'invoice.amount',
                DB::RAW('IFNULL(invoice.from_booking,0) as from_booking'),
                DB::RAW('SUM(cost_value) as cost_value')
            )
            ->whereNull('invoice.deleted_at')
            ->whereNull('invoice2.id')
            ->groupBy('invoice.id',
                'invoice.contract_id',
                'invoice.scheduled_date',
                'invoice.status',
                'invoice.invoice_number',
                      DB::RAW(
                          'IF(invoice.transfer_reference = "' . MamipayInvoice::PAYMENT_STATUS_NOT_IN_MAMIPAY . '",invoice.transfer_reference,null)'
                      ),
                      'invoice.amount',
                      DB::RAW('IFNULL(invoice.from_booking,0)')
            );

        $contracts = MamipayContract::join('mamipay_tenant as tenant', 'mamipay_contract.tenant_id', 'tenant.id')
            ->join('mamipay_contract_type_kost', 'mamipay_contract.id', 'mamipay_contract_type_kost.contract_id')
            ->join('designer', 'designer.id', 'mamipay_contract_type_kost.designer_id')
            ->joinSub(
                $invoices,
                'invoice',
                function ($join) {
                    $join->on('invoice.contract_id', 'mamipay_contract.id');
                }
            )
            ->leftJoin('consultant', 'mamipay_contract.consultant_id', 'consultant.id')
            ->select(
                'mamipay_contract.id',
                'tenant.name',
                'designer.name as property_name',
                'mamipay_contract.status as contract_status',
                'mamipay_contract_type_kost.room_number as room_number',
                'invoice.id as invoice_id',
                'invoice.status',
                'invoice.transfer_reference as paid_from',
                'invoice.scheduled_date',
                'mamipay_contract.end_date',
                'invoice.amount as price',
                'invoice.from_booking',
                'consultant.name as consultant_name',
                'invoice.cost_value'
            )
            ->where('mamipay_contract.status', '!=', MamipayContract::STATUS_BOOKED);

        if ($request->has('date_from')) {
            $contracts = $contracts->where('invoice.scheduled_date', '>=', $request->input('date_from'));
        }

        if ($request->has('date_to')) {
            $contracts = $contracts->where('invoice.scheduled_date', '<=', $request->input('date_to'));
        }

        $consultantId = !is_null($this->consultant) ? $this->consultant->id : null;
        if ($this->consultantHasMapping) {
            $contracts->join('consultant_designer', function ($join) use ($consultantId) {
                $join->on('consultant_designer.designer_id', '=', 'designer.id')
                    ->where('consultant_designer.consultant_id', $consultantId)
                    ->whereNull('consultant_designer.deleted_at');
            });
        }

        if ($request->has('search')) {
            $search = $request->input('search');
            $contracts = $contracts->where(
                function ($query) use ($search) {
                    $query->where('designer.name', 'like', '%' . $search . '%')
                        ->orWhere('tenant.phone_number', 'like', '%' . $search . '%')
                        ->orWhere('tenant.name', 'like', '%' . $search . '%')
                        ->orWhere('invoice.invoice_number', 'like', '%' . $search . '%');
                }
            );
        }

        if ($request->has('contract_status') && count($request->input('contract_status'))) {
            $contracts = $contracts->whereIn('mamipay_contract.status', $request->input('contract_status'));
        }

        if ($request->has('paid_status') && count($request->input('paid_status')) > 0) {
            $filter = $request->input('paid_status');
            $contracts = $contracts->where(
                function ($query) use ($filter) {
                    if (in_array(MamipayInvoice::PAYMENT_STATUS_NOT_IN_MAMIPAY, $filter)) {
                        $query->whereIn('invoice.status', $filter)
                            ->orWhere('invoice.transfer_reference', MamipayInvoice::PAYMENT_STATUS_NOT_IN_MAMIPAY);
                    } else {
                        $query->whereIn('invoice.status', $filter);
                    }
                }
            );
        }

        if ($request->has('created_by') && count($request->input('created_by')) > 0) {
            $filter = $request->input('created_by');
            $filterConsultant = in_array(self::FILTER_CREATED_BY_CONSULTANT, $filter);
            $filterOwner = in_array(self::FILTER_CREATED_BY_OWNER, $filter);
            $filterBooking = in_array(self::FILTER_CREATED_BY_TENANT, $filter);
            if (!($filterConsultant && $filterOwner && $filterBooking)) {
                $contracts = $contracts->where(
                    function ($query) use ($filterConsultant, $filterBooking, $filterOwner) {
                        $query->when(
                            $filterConsultant,
                            function ($query) {
                                $query->orWhereNotNull('mamipay_contract.consultant_id');
                            }
                        )
                            ->when(
                                $filterBooking,
                                function ($query) {
                                    $query->orWhere('invoice.from_booking', 1);
                                }
                            )
                            ->when(
                                $filterOwner,
                                function ($query) {
                                    $query->orWhere(
                                        function ($q) {
                                            $q->where('invoice.from_booking', 0)
                                                ->whereNull('mamipay_contract.consultant_id');
                                        }
                                    );
                                }
                            );
                    }
                );
            }
        }

        if ($request->has('is_booking') && $request->input('is_booking')) {
            $contracts = $contracts->where('designer.is_booking', 1);
        }

        $total = $contracts->count();

        $contracts = $contracts->orderBy('mamipay_contract.created_at', 'desc')
            ->limit(self::DEFAULT_PAGINATION)
            ->offset($request->has('offset') ? $request->input('offset') : 0)
            ->get()->map(
                function ($contract) {
                    $contract->price += $contract->cost_value;

                    unset($contract->cost_value);

                    return $contract;
                }
            );


        return ApiResponse::responseData(
            [
                'total' => $total,
                'limit' => self::DEFAULT_PAGINATION,
                'offset' => $request->input('offset') ? $request->input('offset') : 0,
                'data' => $contracts
            ]
        );
    }

    /**
     *  Search for non active or potential tenant using phone number
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function searchTenant(Request $request): JsonResponse
    {
        $phoneNumber = $request->input('phone_number', '');

        $tenant = MamipayTenant::wherePhoneNumber($phoneNumber)
            ->first();

        if (!is_null($tenant)) {
            $contractsCount = $tenant
                ->contracts()
                ->where(
                    function ($query) {
                        $query->where('status', MamipayContract::STATUS_ACTIVE)
                            ->orWhere('status', MamipayContract::STATUS_BOOKED);
                    }
                )
                ->count();

            if ($contractsCount > 0) {
                // Error message below should never be returned by frontend. Since the flow should block this case before creating contract.
                return ApiResponse::responseData(
                    [
                        'status' => false,
                        'message' => [
                            'title' => __('api.input.tenant.active.title'),
                            'message' => __('api.input.tenant.active.message')
                        ]
                    ]
                );
            }
        }

        if (isset($tenant)) {
            return $this->formatTenantResponse($tenant);
        }

        $tenant = PotentialTenant::where('phone_number', 'like', ('%' . $phoneNumber))
            ->first();

        if (isset($tenant)) {
            $tenant = (new PotentialTenantPresenter('contract-management'))->present($tenant);

            return ApiResponse::responseData($tenant);
        }

        return $this->formatNotFoundResponse($phoneNumber);
    }

    /**
     *  Create contract for potential/existing tenant
     *
     * @param CreateContractRequest $request
     */
    public function createContract(CreateContractRequest $request)
    {
        $validator = $request->failedValidator;

        if (!is_null($validator) && $validator->fails()) {
            return ApiResponse::responseData(
                [
                    'status' => false,
                    'messages' => $validator->errors()->getMessages()
                ]
            );
        }

        $mamipayResponse = MamipayApiHelper::makeRequest(
            $request->method(),
            $request,
            'contract/consultant/save',
            Auth::id()
        );

        $response = [
            'status' => $mamipayResponse->status,
            'meta' => (array)$mamipayResponse->meta,
        ];

        if (isset($mamipayResponse->data)) {
            $response['data'] = [
                'contract' => (array)$mamipayResponse->data->contract
            ];

            $contractId = $mamipayResponse->data->contract->id;

            if (!empty($request->note)) {
                $note = new ConsultantNote();
                $note->content = $request->note;
                $note->activity = ConsultantNote::NOTE_CREATE_CONTRACT;
                $note->consultant_id = Auth::user()->consultant->id;
                $note->noteable_type =  'mamipay_contract';
                $note->noteable_id = $contractId;
                $note->save();
            }
        }

        return ApiResponse::responseData($response);
    }

    /**
     *  Upload tenant identity card when creating contract
     *
     * @param Request $request
     */
    public function uploadIdentifier(Request $request)
    {
        $request->request->add(['type' => 'document']);
        $response = MamipayApiHelper::makeRequest('POST', $request, 'media/upload', Auth::id());

        return ApiResponse::responseObj($response);
    }

    /**
     *  Upload tenant selfie with identity card
     *
     * @param Request $request
     */
    public function uploadSelfie(Request $request)
    {
        $request->request->add(['type' => 'document']);
        $response = MamipayApiHelper::makeRequest('POST', $request, 'media/upload', Auth::id());

        return ApiResponse::responseObj($response);
    }

    /**
     *  Format mamipay tenant model into appropriate json response
     *
     * @param MamipayTenant $tenant Mamipay tenant to transform
     *
     * @return JsonResponse
     */
    private function formatTenantResponse(MamipayTenant $tenant): JsonResponse
    {
        if ($tenant->has_active_contract) {
            return ApiResponse::responseData(
                [
                    'status' => false,
                    'message' => [
                        'title' => __('api.input.tenant.active.title'),
                        'body' => __('api.input.tenant.active.message')
                    ]
                ]
            );
        }

        $tenantDetail = MamipayApiHelper::makeRequest(
            'GET',
            null,
            ('tenant/show/' . $tenant->id),
            Auth::user()->id
        )->tenant;

        $tenant = (new MamipayTenantPresenter('contract-management'))->present($tenant);
        $tenant['data']['tenant']['photo_identifier'] = $tenantDetail->tenant_photo_identifier;
        $tenant['data']['tenant']['photo_document'] = $tenantDetail->tenant_photo_document;
        return ApiResponse::responseData($tenant);
    }

    /**
     *  Create not found response for tenant phone number search
     *
     * @param string $number Number searched
     *
     * @return JsonResponse
     */
    private function formatNotFoundResponse(string $number): JsonResponse
    {
        $number = [
            'data' => [
                'tenant' => [
                    'id' => null,
                    'name' => null,
                    'photo' => null,
                    'photo_id' => null,
                    'photo_identifier' => null,
                    'photo_identifier_id' => null,
                    'photo_document' => null,
                    'photo_document_id' => null,
                    'photo_document_name' => null,
                    'phone_number' => $number,
                    'room_number' => null,
                    'email' => null,
                    'occupation' => null,
                    'gender' => null,
                    'gender_value' => null,
                    'marital_status' => null,
                    'parent_name' => null,
                    'parent_phone_number' => null,
                    'price' => null,
                    'due_date' => null,
                ]
            ]
        ];

        return ApiResponse::responseData($number);
    }

    public function searchProperty(Request $request, $name)
    {
        $consultantId = !is_null($this->consultant) ? $this->consultant->id : null;
        if ($this->consultantHasMapping) {
            $rooms = $this->roomIndexService->searchKostWithActiveMamipay($name, $consultantId)['hits']['hits'];
        } else {
            $rooms = $this->roomIndexService->searchKostWithActiveMamipay($name)['hits']['hits'];
        }

        $results = [];
        foreach ($rooms as $room) {
            $results[] = [
                'id' => $room['_source']['song_id'],
                'name' => $room['_source']['title']
            ];
        }

        return ApiResponse::responseData(
            [
                'data' => $results
            ]
        );
    }

    public function getInvoiceYear(int $contract)
    {
        $invoices = MamipayInvoice::where('contract_id', $contract)->orderBy('id', 'asc')->get();

        return ApiResponse::responseData(
            [
                'data' => [
                    'first_year' => $invoices->isNotEmpty() ? Carbon::parse($invoices[0]->scheduled_date)->format('Y') : Carbon::now()->format('Y'),
                    'last_year' => $invoices->isNotEmpty() ? Carbon::parse($invoices[$invoices->count() - 1]->scheduled_date)->format('Y') : Carbon::now()->format('Y')
                ]
            ]
        );
    }

    public function invoice(int $contract, int $year)
    {
        $invoices = MamipayInvoice::leftJoin(
            'mamipay_contract_invoice_cost as cost',
            function ($join){
                $join->on('cost.invoice_id','mamipay_contract_invoice.id')
                    ->whereNull('cost.deleted_at');
            }
        )
            ->where('contract_id', $contract)
            ->whereRaw('YEAR(scheduled_date) = "' . $year . '"')
            ->select(
                'mamipay_contract_invoice.id',
                'scheduled_date',
                'status',
                DB::RAW(
                    'IF(transfer_reference = "' . MamipayInvoice::PAYMENT_STATUS_NOT_IN_MAMIPAY . '",transfer_reference,null) as transfer_reference'
                ),
                DB::RAW('SUM(cost.cost_value) as total_cost'),
                'amount',
                'shortlink'
            )
            ->groupBy(
                [
                    'mamipay_contract_invoice.id',
                    'scheduled_date',
                    'status',
                    DB::RAW(
                        'IF(transfer_reference = "' . MamipayInvoice::PAYMENT_STATUS_NOT_IN_MAMIPAY . '",transfer_reference,null)'
                    ),
                    'amount',
                    'shortlink'
                ]
            )
            ->orderBy('scheduled_date', 'asc')
            ->get()
            ->map(
                function ($invoice) {
                    $invoice->note = $invoice->consultant_note ? $invoice->consultant_note->content : '';
                    $invoice->link = config('api.mamipay.invoice') . $invoice->shortlink;
                    $invoice->additional_costs;
                    $invoice->total_cost += $invoice->amount;
                    unset($invoice->shortlink);

                    return $invoice;
                }
            );

        return ApiResponse::responseData(['data' => $invoices]);
    }

    public function invoiceNote(Request $request, int $invoiceId)
    {
        $invoice = MamipayInvoice::find($invoiceId);
        if (is_null($invoice) || is_null(Auth::user()->consultant)) {
            return ApiResponse::responseError('Invalid Request', 'INVALID_REQUEST', 401);
        }

        if (!is_null($invoice->consultant_note)) {
            $invoice->consultant_note->delete();
        }

        $invoice->consultant_note()->create(
            [
                'content' => $request->input('content'),
                'consultant_id' => Auth::user()->consultant->id,
                'activity' => 'invoice-' . $invoice->status
            ]
        );

        return ApiResponse::responseData(
            [
                'status' => 'success'
            ]
        );
    }

    public function tenantInfo(int $contractId)
    {
        $contract = MamipayContract::find($contractId);
        if (is_null($contract)) {
            return ApiResponse::responseError('Invalid Request', 'INVALID_REQUEST', 401);
        }
        $tenant = $contract ? $contract->tenant : null;

        return ApiResponse::responseData(
            [
                'data' => [
                    'id' => $tenant ? $tenant->id : null,
                    'name' => $tenant ? $tenant->name : null,
                    'profile_picture' => $tenant && $tenant->user && $tenant->user->photo ? $tenant->user->photo->getMediaUrl(
                    ) : null
                ]
            ]
        );
    }

    public function reminder(Request $request, int $invoiceId)
    {
        $invoice = MamipayInvoice::find($invoiceId);
        if (is_null($invoice)) {
            return ApiResponse::responseError('Invalid Request', 'INVALID_REQUEST', 401);
        }

        try {
            $mamipayResponse = MamipayApiHelper::makeRequest(
                $request->method(),
                $request,
                'payment-reminder/' . $invoiceId,
                Auth::id()
            );
        } catch (Exception $exception) {
            Bugsnag::notifyException($exception);

            return ApiResponse::responseError('Failed to process', 'ERROR', 500);
        }

        $response = [
            'status' => $mamipayResponse->status,
            'meta' => (array)$mamipayResponse->meta,
        ];


        return ApiResponse::responseData($response);
    }

    /**
     *  Set invoice with given id as manual payment
     *
     *  @param Request $request
     *  @param int $invoiceId Id of invoice to update
     *
     *  @return JsonResponse
     */
    public function setInvoiceAsManualPayment(Request $request, int $invoiceId): JsonResponse
    {
        // Handle case where invoice could not be found
        $invoice = MamipayInvoice::with('contract.type_kost.room.consultant_rooms')->find($invoiceId);
        if (is_null($invoice)) {
            Bugsnag::notifyException(new NotFoundException('Consultant tried to access contract outside mapped area'));

            return ApiResponse::responseData([
                'status' => false,
                'message' => 'Invoice could not be found'
            ]);
        }

        // Prevent editing contract outside consultant area city
        $consultantId = !is_null($this->consultant) ? $this->consultant->id : null;
        if (!is_null($consultantId) && !$this->isAllowedToEditInvoice($invoice, $consultantId)) {
            Bugsnag::notifyException(new NotFoundException('Consultant tried to access contract outside mapped area'));

            return ApiResponse::responseData([
                'status' => false,
                'message' => 'You are not allowed to edit contract outside your area city'
            ]);
        }

        $response = MamipayApiHelper::makeRequest('PUT', $request, ('set-manual-payment/' . $invoice->id), Auth::id());
        return ApiResponse::responseObj($response);
    }

    /**
     *  Terminate contract from booking with specified id
     *
     *  @param Request $request
     *  @param int $contractId
     *
     *  @return JsonResponse
     */
    public function terminate(Request $request, int $contractId): JsonResponse
    {
        $response = MamipayApiHelper::makeRequest('PUT', $request, ('contract/consultant/terminate/' . $contractId), Auth::id());

        return ApiResponse::responseObj($response);
    }

    /**
     *  Check whether room of the contract was assigned to the consultant
     *
     *  @param MamipayInvoice $invoice
     *  @param int $consultantId
     *
     *  @return bool
     */
    private function isAllowedToEditInvoice(MamipayInvoice $invoice, ?int $consultantId): bool
    {
        if (is_null($consultantId)) {
            return false;
        }

        $contract = $invoice->contract;

        if (!is_null($contract)) {
            $typeKost = $contract->type_kost;

            if (!is_null($typeKost)) {
                $room = $typeKost->room;

                return $room->consultant_rooms()->where('consultant_id', $consultantId)->exists();
            }
        }

        return false;
    }

    /**
     *  Create, update or delete existing additional cost
     *
     *  @param Request $request
     *
     *  @return JsonResponse
     */
    public function modifyAdditionalCost(ModifyAdditionalCostRequest $request): JsonResponse
    {
        $validator = $request->failedValidator;

        if (!is_null($validator) && $validator->fails()) {
            return ApiResponse::responseData([
                'status' => false,
                'messages' => $validator->errors()->getMessages()
            ]);
        }

        $additionalCostUpdate = $request->additional_costs;

        $additionalCostUpdate->whereStrict('action', 'UPDATE')->each(function ($item, $Key) use ($request) {
            $request['cost_value'] = $item['cost_value'];
            MamipayApiHelper::makeRequest(
                'PUT',
                $request,
                ('additional-cost-field/invoice/update/' . $item['additional_cost_id']),
                Auth::id()
            );
        });

        $additionalCostUpdate->whereStrict('action', 'DELETE')->each(function ($item, $key) use ($request) {
            $request['cost_value'] = null;
            MamipayApiHelper::makeRequest(
                'DELETE',
                $request,
                ('additional-cost-field/invoice/delete/' . $item['additional_cost_id']),
                Auth::id()
            );
        });

        $additionalCostUpdate->whereStrict('action', 'CREATE')->each(function ($item, $key) use ($request) {
            $request['invoice_id'] = $item['invoice_id'];
            $request['cost_value'] = $item['cost_value'];
            $request['cost_title'] = $item['cost_title'];
            $request['cost_type'] = $item['cost_type'];
            MamipayApiHelper::makeRequest('POST', $request, 'additional-cost-field/invoice/create', Auth::id());
        });

        return ApiResponse::responseData(['status' => true]);
    }

    /**
     *  Update invoice amount
     *
     *  @param UpdateInvoiceAmountRequest $request
     *  @param int $id Id of invoice to update
     *
     *  @return JsonResponse
     */
    public function updateAmount(UpdateInvoiceAmountRequest $request, int $id): JsonResponse
    {
        try {
            $validator = $request->failedValidator;

            if (!is_null($validator) && $validator->fails()) {
                return ApiResponse::responseData([
                    'status' => false,
                    'messages' => $validator->errors()->getMessages()
                ]);
            }

            DB::beginTransaction();
            $invoice = MamipayInvoice::find($id);
            $invoice->amount = $request->amount;
            $invoice->save();
            DB::commit();

            return ApiResponse::responseData(['status' => true]);
        } catch (\Exception $e) {
            DB::rollBack();
            Bugsnag::notifyException($e);
            return ApiResponse::responseData(['status' => false, 'message' => __('api.transaction.fail')]);
        }
    }

    /**
     *  Cancel contract from booking with specified id
     *
     *  @param Request $request
     *  @param int $contractId
     */
    public function cancel(Request $request, int $contractId): JsonResponse
    {
        $isExist = MamipayContract::where('id', $contractId)->has('booking_users')->exists();

        if (!$isExist) {
            return ApiResponse::responseData(['status' => false]);
        }

        $response = MamipayApiHelper::makeRequest('PUT', $request, ('contract/consultant/cancel/' . $contractId), Auth::id());

        return ApiResponse::responseObj($response);
    }
}
