<?php

namespace App\Http\Controllers\ConsultantTools;

use App\Entities\Consultant\Elasticsearch\PotentialPropertyQuery;
use App\Http\Helpers\ApiHelper;
use App\Http\Requests\Consultant\PotentialProperty\CreateRequest;
use App\Http\Requests\Consultant\PotentialProperty\Request as PotentialPropertyRequest;
use App\Presenters\PotentialPropertyPresenter;
use App\Repositories\Consultant\PotentialPropertyRepository;
use App\Transformers\Consultant\PotentialProperty\DetailTransformer;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Elasticsearch;

class PotentialPropertyController extends BaseController
{
    protected const SORT_LAST_UPDATED = 'last_updated';
    protected const SORT_LOW_PRIORITY = 'priority_low';
    protected const SORT_HIGH_PRIORITY = 'priority_high';

    /**
     *  Instance of potential property repository
     *
     * @var App\Repositories\Consultant\PotentialPropertyRepository
     */
    protected $potentialPropertyRepository;

    /**
     *  Inject dependency to this controller
     */
    public function __construct(PotentialPropertyRepository $potentialProperty)
    {
        parent::__construct();
        $this->potentialPropertyRepository = $potentialProperty;
    }

    /**
     *  Show available potential property
     *
     * @param Request $request
     * @param int $ownerId Id of owner that owned the property
     *
     * @return JsonResponse
     */
    public function index(Request $request, ?int $ownerId = null): JsonResponse
    {
        $query = new PotentialPropertyQuery();
        $query->filter('created_by', $request->created_by);
        $query->filter('is_priority', (!is_null($request->priority) ? $request->priority === 'high' : null));

        if (!is_null($ownerId)) {
            $query->filter('owner_id', $ownerId);
        }

        $keyword = $request->input('search', null);
        if ($request->has('search') && $keyword) {
            $query->search([
                ['name' => $keyword],
                ['area_city' => $keyword],
                ['province' => $keyword]
            ]);
        }

        if ($request->has('sort_by')) {
            switch ($request->input('sort_by', null)) {
                case self::SORT_LAST_UPDATED:
                    $query->sort('updated_at', 'desc');
                    break;
                case self::SORT_LOW_PRIORITY:
                    $query->sort('is_priority', 'asc');
                    break;
                case self::SORT_HIGH_PRIORITY:
                    $query->sort('is_priority', 'desc');
                    break;
                default:
                    $query->sort('updated_at', 'desc');
                    break;
            }
        }

        if ($request->has('offset')) {
            $query->offset($request->input('offset', null));
        }

        $query = $query->getQuery();

        $indexes = Elasticsearch::search($query);

        return ApiHelper::responseData(
            (new PotentialPropertyPresenter(PotentialPropertyPresenter::LIST))->presentIndexes($indexes)
        );
    }

    public function detail(Request $request, $id): JsonResponse
    {
        try {
            $property = $this->potentialPropertyRepository->setPresenter(new PotentialPropertyPresenter('detail'))
                ->with('photo')
                ->find($id);

            return ApiHelper::responseData(
                [
                    'data' => $property['data']
                ]
            );
        } catch (ModelNotFoundException $e) {
            return ApiHelper::responseData(
                [
                    'status' => false,
                    'message' => 'Potential property tidak bisa ditemukan'
                ],
                false,
                false,
                404
            );
        }
    }

    /**
     *  Delete an existing potential property
     *
     *  @param int $id Id of potential property to delete
     *
     *  @return JsonResponse
     */
    public function delete(int $id): JsonResponse
    {
        try {
            $property = $this->potentialPropertyRepository->with('potential_owner')->find($id);

            $owner = $property->potential_owner;

            if (!is_null($owner)) {
                $owner->total_property = ($owner->total_property - 1);
                $owner->total_room = ($owner->total_room - $property->total_room);
                $owner->save();
            }

            $property->delete();

            return ApiHelper::responseData([]);
        } catch (ModelNotFoundException $e) {
            return ApiHelper::responseData(['message' => __('api.exception.not_found')], false, false, 404);
        }
    }

    /**
     *  Create a potential property
     *
     *  @param CreateRequest $request
     *
     *  @return JsonResponse
     */
    public function create(CreateRequest $request): JsonResponse
    {
        if ($request->isValidationFailed()) {
            return ApiHelper::responseData(
                [
                    'messages' => $request->failedValidator->errors()
                ],
                false,
                false,
                501
            );
        }

        $input = $request->inputs();
        $input['created_by'] = Auth::id();

        $property = $this->potentialPropertyRepository->create($input);
        $property->load(['potential_owner', 'photo']);

        // Update owner total room
        $owner = $property->potential_owner;
        $owner->total_room = ($owner->total_room + $input['total_room']);
        $owner->total_property = ($owner->total_property + 1);
        $owner->save();

        return ApiHelper::responseData(['data' => (new DetailTransformer())->transform($property)]);
    }

    /**
     *  Update a potential property
     *
     *  @param PotentialPropertyRequest $request
     *  @param int $id Id of potential property to updatye
     *
     *  @return JsonResponse
     */
    public function update(PotentialPropertyRequest $request, int $id): JsonResponse
    {
        try {
            $property = $this->potentialPropertyRepository->find($id);

            if ($request->isValidationFailed()) {
                return ApiHelper::responseData(
                    [
                        'messages' => $request->failedValidator->errors()
                    ],
                    false,
                    false,
                    501
                );
            }

            $property->load(['potential_owner', 'photo']);
            $input = $request->inputs();
            $input['updated_by'] = Auth::id();

            // Update owner total room
            $owner = $property->potential_owner;
            $owner->total_room = (($owner->total_room - $property->total_room) + $input['total_room']);
            $owner->save();

            $property->update($input);
            return ApiHelper::responseData(['data' => (new DetailTransformer())->transform($property)]);
        } catch (ModelNotFoundException $e) {
            return ApiHelper::responseData(['message' => __('api.exception.not_found')], false, false, 404);
        }
    }
}
